/********************************************************************
 * $Id: mplsutil.h,v 1.29 2018/01/03 11:31:22 siva Exp $
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : mplsutil.h 
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  : 23-MAY-2002
 *    DESCRIPTION            : This file contains all the util functions 
 *                             prototypes that is needed by other modules.
 *---------------------------------------------------------------------------*/

#ifndef _MPLSUTIL_H
#define _MPLSUTIL_H
#include "ip.h"
#include "vcm.h"
#include "mpls.h"
#include "mplsapi.h"
#define HOST_ORDER 0x01
#define NET_ORDER 0x10
#define MPLS_SUPPRESS_WARNING(x) x = x

/* MPLS's equivalent for the standard struct in_addr (netinet/in.h)  */
typedef struct
{
    UINT4    u4Addr;
} tMplsUtlInAddr;

typedef enum
{
    ADDR_OTHER = 0,
    IPV4,
    IPV6,
    NSAP,
    HDLC,
    BBN1822,
    ALL802,
    E163,
    E164,
    F69,
    X121,
    IPX,
    APPLETALK,
    DECNETIV,
    BANYANVINES,
    E164WITHNSAP,
    RESERVED = 65535
}eGenAddrType;

typedef struct _GenLabel
{
    UINT4               u4Lbl;
}
tGenLabel;

/*GLOBAL L3VPN and
 * RSVP-TE TnlInfo.*/
typedef struct _L3VpnGlobalTnlInfo
{
    UINT4 u4TnlIndex;
    UINT4 u4TnlXcIndex;
    UINT4 u4TnlProXcIndex;
    UINT1 u1TnlOperStatus;
    UINT1 u1TnlAdminStatus;
    UINT1 u1TnlProInUse;
    UINT1 u1Padding[1];
}tL3VpnGlobalTnlInfo;

/* L3VPN and 
RSVP-TE will build message base on this.*/
typedef struct _L3VpnRsvpTeLspEventInfo
{
    UINT4 u4TnlIndex;
    UINT4 u4TnlXcIndex;
    UINT4 u4EventType;
    UINT1 u1TnlOperStatus;
    UINT1 u1TnlAdminStatus;
    UINT1 u1Protection;
    UINT1 u1Padding[1];
}tL3VpnRsvpTeLspEventInfo;

/*RpTe Msg for L3VPN */
#define L3VPN_RSVPTE_TNL_UP          1
#define L3VPN_RSVPTE_TNL_DEL         2
#define L3VPN_RSVPTE_LSP_DOWN        3
#define L3VPN_RSVPTE_LSP_UP          4
#define L3VPN_RSVPTE_TNL_DOWN        5
#define L3VPN_RSVPTE_MAX_TNL        150
#define L3VPN_PROT_AVAIL           0x01
#define L3VPN_PROT_IN_USE          0x02
#define L3VPN_PROT_NOT_AVAIL       0x80
#define L3VPN_PROT_NOT_APPLICABLE  0x00




typedef struct _AtmLabel
{
    UINT2               u2Vpi;
    UINT2               u2Vci;
}
tAtmLabel;

/* Later this structure can be modified to hold FR label too */
typedef union _Label
{
    UINT4               u4GenLbl;
    tAtmLabel           AtmLbl;
}
uLabel;

typedef enum {
 INDEX_DF =0,
 INDEX_CS1,
 INDEX_CS2,
 INDEX_CS3,
 INDEX_CS4,
 INDEX_CS5,
 INDEX_CS6,
 INDEX_CS7,
 INDEX_EF,
 INDEX_AF1,
 INDEX_AF2,
 INDEX_AF3,
 INDEX_AF4,
 INDEX_EF1
} eIndexForPsc;

#define IPV4_MIN_PRFX_LEN     8
#define IPV4_MAX_PRFX_LEN    32
#define IPV6_MAX_PRFX_LEN    128
#define IPV4_ADDR_LENGTH      4
#define IPV6_ADDR_LENGTH     16
#define LDP_MAX_LDPID_LEN     6
#define ROUTER_ID_LENGTH      4
#define MPLS_DEFAULT_CXT_ID   0

/* Macro Added from rtm6.h due to compilation error, To Remove Later */
#define  RTM6_QUERIED_FOR_NEXT_HOP                   0x01

typedef UINT1       tIpv4Addr[IPV4_ADDR_LENGTH];
typedef UINT1       tIpv6Addr[IPV6_ADDR_LENGTH];
typedef UINT1       tLdpId[LDP_MAX_LDPID_LEN];

#define L2VPN_STAT_PWVC_ILLEGAL_C_BIT           0x00000024
#define L2VPN_STAT_PWVC_WRONG_C_BIT             0x00000025
#define LDP_STAT_PWVC_ILLEGAL_C_BIT_VAL         0x00000024
#define LDP_STAT_PWVC_WRONG_C_BIT_VAL           0x00000025
#define LDP_STAT_PWVC_UNASSIGNED_TAI            0x00000029
#define LDP_STAT_PWVC_LBL_WDRAW_UNSUPPORTED     0x0000002B
#define LDP_STAT_PWVC_GEN_MISCONF_ERR           0x0000002A

#define LDP_STAT_PWVC_WRONG_C_BIT_VAL_RFC_4906  0x20000002

#define LDP_GET_PWVC_C_BIT(u2PwVcType, i1CBit)\
i1CBit = ((u2PwVcType & LDP_PWVC_C_BIT_MASK) != 0) ? LDP_TRUE : LDP_FALSE

#define CPY_TO_SNMP(pSnmpStruct, pu1Strg, u1Len) \
               MEMCPY((pSnmpStruct)->pu1_OctetList, (pu1Strg), (u1Len)) ; \
                              (pSnmpStruct)->i4_Length = (u1Len);

#define CPY_FROM_SNMP(pu1Strg, pSnmpStruct, u1Len) \
         MEMCPY((pu1Strg), (pSnmpStruct)->pu1_OctetList, (u1Len))

#define LEN_OF_OCTET_STRING(pSnmpStruct) \
           (STRLEN ((UINT1 *) \
                           (((tSNMP_OCTET_STRING_TYPE *)pSnmpStruct)->pu1_OctetList)))

#define LEN_OF_OID(pSnmpStruct) \
            (((tSNMP_OID_TYPE *)pSnmpStruct)->u4_Length)

#define CPY_TO_OID(pOidStruct, pu1Strg, u1Len)\
            MEMCPY((UINT1 *)((pOidStruct)->pu4_OidList), (pu1Strg), (u1Len * 4));\
                    (pOidStruct)->u4_Length = (u1Len);

#define CPY_FROM_OID(pu1Strg, pOidStruct, u1Len) \
        MEMCPY((pu1Strg), ((UINT1 *)(pOidStruct)->pu4_OidList), (u1Len))
    
#define MEM_CMP(s1, s2, n)         memcmp (s1, s2, n)

#ifdef MPLS_IPV6_WANTED
#define SNMP_OCTET_STRING_LIST(x) (x->pu1_OctetList)
#endif

/* This structure is added during GR related changes
 * However since this is a generic change, hence not putting 
 * it under any compilation flag */
typedef struct _MplsIpAddress
{
    uGenAddr     IpAddress;
    INT4         i4IpAddrType;
}tMplsIpAddress;


#if 0

typedef union _GenAddr
{
       UINT1             au1Ipv4Addr[4];
#ifdef MPLS_IPV6_WANTED
       tIp6Addr          Ip6Addr; /* Support for Ipv6 address */
#endif
} uGenAddr;

typedef struct _GenAddress
{
    uGenAddr Addr;
    UINT2    u2AddrType;
    UINT1    u1Pad[2];
}tGenAddr;

typedef union _GenU4Addr
{
        UINT4             u4Addr;
#ifdef MPLS_IPV6_WANTED
        tIp6Addr          Ip6Addr;   /* Support for Ipv6 address */
#endif
} uGenU4Addr;

typedef struct _GenU4Address
{
        uGenU4Addr Addr;
        UINT2    u2AddrType;
        UINT1    u1Pad[2];
}tGenU4Addr;
   
#endif
typedef uGenAddr  tMplsRouterId;

UINT1 SLLInsertInOrder (UINT2 au2Offset[], UINT2 au2Length[],
                        UINT1 au1ByteOrder[],  UINT2 u2ArrLength,
                        tTMO_SLL *pList, VOID *pNode, UINT2 u2NodeOffset);
UINT1 SLLSearch (UINT2 au2Offset[], UINT2 au2Length[], UINT1 au1ByteOrder[],
                 UINT2 u2ArrLength, tTMO_SLL *pList, VOID *pNode,
                 UINT2 u2NodeOffset, VOID **ppNode);
UINT1 HASHInsertInOrder (UINT2 au2Offset[], UINT2 au2Length[],
                         UINT1 au1ByteOrder[], UINT2 u2ArrLength,
                         tTMO_HASH_TABLE *pTable, VOID *pNode,
                         UINT4 u4HashIndex, UINT2 u2NodeOffset);
UINT1 HASHSearch (UINT2 au2Offset[], UINT2 au2Length[], UINT1 au1ByteOrder[],
                  UINT2 u2ArrLength, tTMO_HASH_TABLE * pTable,
                  VOID *pNode, UINT4 u4HashIndex, UINT2 u2NodeOffset,
                  VOID **ppNode);
INT4 SLL_MEMCMP (UINT1 *pu1One, UINT1 *pu1Two, UINT2 u2Len,
                 UINT1 u1ByteOrder);

CHR1* MplsUtlInetNtoa (tMplsUtlInAddr InAddr);

VOID MplsPutMplsHdr (UINT1 *pBuf, tMplsHdr *pHdr);
VOID MplsReadMplsHdr (UINT1 *pBuf, tMplsHdr *pHdr);

INT4 MplsInitSemCreate (VOID);
INT4 MplsInitSemDelete (VOID);
VOID MplsInitComplete (UINT4 u4Status);
INT4 MplsInitVerify (VOID);
UINT4 MplsGetDefaultIpNetmask (UINT4 u4HAddr);
#define MPLS_INIT_SEM_NAME (CONST UINT1*)"MSEQ"
typedef enum{
    MPLS_TASK_INIT_FAILURE = 0,
    MPLS_TASK_INIT_SUCCESS,
    MPLS_TASK_INIT_COMPLETE
}tMplsTskStatus;
VOID
MplsIpRtChgEventHandler ARG_LIST ((tNetIpv4RtInfo * pNetIpv4RtInfo, tNetIpv4RtInfo * pNetIpv4RtInfo1, UINT1 u1CmdType));
VOID
MplsIpIfChgEventHandler ARG_LIST ((tNetIpv4IfInfo * pIfInfo, UINT4 u4BitMap));
UINT1
MplsRegisterWithIP (VOID);
UINT1
MplsDeRegisterWithIP (VOID);
VOID LdpIpRtChgEventHandler ARG_LIST ((tNetIpv4RtInfo * pNetIpv4RtInfo, 
                 UINT1 u1CmdType));
VOID
RpteHandleRtChgNotification (tNetIpv4RtInfo * pNetIpv4RtInfo, UINT1 u1CmdType);
VOID
MplsIpRtChgNotification (tNetIpv4RtInfo * pNetIpv4RtInfo, UINT1 u1CmdType);
VOID
MplsRtrIfChgNotification (UINT4 u4IfIndex, UINT1 u1OperStatus);
VOID
L2VpnHandleRtChgNotification (tNetIpv4RtInfo * pNetIpv4RtInfo, UINT1 u1CmdType);

INT4 MplsGetL3Intf (UINT4 u4MplsIfOrMplsTnlIf, UINT4 *pu4L3Intf);
INT4
MplsPortEventNotification (UINT4 u4DestModId, tMplsEventNotif *pMplsEventNotif);
INT4
MplsPortGetTrafficClassFromPhb (INT4 i4PhbValue, UINT1 *pu1TrafficClass);
INT4 
MplsPortVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias);
VOID
MplsPortFmNotifyFaults (tSNMP_VAR_BIND * pTrapMsg, CHR1 *pc1SysLogMsg);
INT4
MplsPortIsVcmSwitchExist PROTO ((UINT1 *pu1Alias, UINT4 *pu4VcNum));
INT4
MplsPortGetVcmSystemMode PROTO ((UINT2 u2ProtocolId));
VOID
MplsGetIfUnnumPeerMac (UINT4 u4IfIndex, UINT1 *pu1MacAddr);
INT4
MplsGetPhyPortFromLogicalIfIndex (UINT4 u4LogIfIndex, 
                                  UINT1 *pu1MacAddr, 
          UINT4 *pu4PhyIfIndex);
VOID 
MplsPortTlmNotifyDiffServParams (VOID);

UINT1
MplsValidateDiffServTcType (UINT4 u4TcType);

VOID
L3VpnRsvpTeLspStatusChangeEventHandler(tL3VpnRsvpTeLspEventInfo RsvpTeL3VpnLspInfo);

/* Below structures and functions are common declaration for L3Vpn and Rsvpte */
typedef struct _L3VpnRsvpMapLabelEntry
{
    tRBNodeEmbd     L3VpnRsvpMapLabelNode;
    UINT4           u4Prefix;               /* destip */
    UINT4           u4Mask;                 /* dest IpMask*/
    UINT4           u4TnlIndex;             /* dest tunnel */
    UINT4           u4TnlXcIndex;           /* working u4TnlXcIndex */
    UINT4           u4TnlProXcIndex;        /* Protection u4TnlProXcIndex*/
    UINT4           u4TnlLabel;             /* working LSP LABEL */
    UINT4           u4TnlProLabel;          /* Protection  LSP LABEL */
    UINT1           u1RowStatus;            /* tunnel row status */
    UINT1           u1TnlSgnlPrtcl;         /* Signal protocol ie.LDP/RSVP  */
    UINT1           u1TnlProInUse;          /* Protection lsp in use or not */
    UINT1           u1TnlWorkOper;          /* Working lsp operstatus */
    UINT1           u1PrefixType;           /* destIp Address type (IpV4 or IpV6) */
    UINT1           u1MaskType;             /* destIpMask Address type (IpV4 or IpV6) */
    UINT1           au1Pad[2];
}tL3VpnRsvpMapLabelEntry;

typedef struct _L3VpnRsvpTeEventInfo
{
    UINT4 u4TnlIfIndex;
    UINT4 u4WorkTnlIfIndex;
    UINT4 u4ProTnlIfIndex;
    UINT4 u4TnlXcIndex;
    UINT4 u4ProTnlXcIndex;
    UINT4 u4OrgTnlXcIndex;
    UINT4 u4EventType;
    UINT1 u1TnlTnlRole;
    UINT1 u1ProTnlTnlRole;
    UINT1 u1TnlOperStatus;
    UINT1 u1ProTnlOperStatus;
    UINT1 u1TnlLocalProtectInUse;
    UINT1 u1Pad[3];
}tL3VpnRsvpTeEventInfo;/* __L3VPNTDFS_H__ */
#define RSVP_MAX_CIDR                  32
extern UINT4        u4CidrSubnetMask[RSVP_MAX_CIDR + 1];
/***************************l3vpnbgp.c*********************/

tL3VpnRsvpMapLabelEntry  *
L3vpnGetRsvpMapLabelEntry(UINT1 u1PrefixType, UINT4 u4Prefix, UINT1 u1MaskType, UINT4 u4Mask);
tL3VpnRsvpMapLabelEntry   *
L3VpnCreateRsvpMapLabelTable(UINT1 u1PrefixType, UINT4 u4Prefix, UINT1 u1MaskType, UINT4 u4Mask);
UINT4
L3VpnDeleteRsvpMapLabelTable(tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry);
/*************************l3vpnutil.c ******************************/
tL3VpnRsvpMapLabelEntry *
L3vpnGetRsvpMapLabelTable(tL3VpnRsvpMapLabelEntry * pL3VpnRsvpMapLabelEntry);

tL3VpnRsvpMapLabelEntry *
L3vpnGetFirstRsvpMapLabelTable(VOID);

tL3VpnRsvpMapLabelEntry *
L3vpnGetNextRsvpMapLabelTable (tL3VpnRsvpMapLabelEntry *
        pCurrentL3VpnRsvpMapLabelEntry);

tL3VpnRsvpMapLabelEntry*
L3VpnFetchRsvpMapLabelTableFromTnlIndex(UINT4 u4TunnelIndex);

/*VOID L3VpnFetchPrefixListInfoFromTnlIndex(UINT4 u4TunnelIndex, UINT4 **pu4PrefixList);*/

INT1
L3VpnValidateTunnelInfofromRsvpMapLabelTable(UINT4 u4TunnelIndex);

INT1
L3VpnBgpNotificationForTnlBind(UINT4 u4TnlIndex, UINT4 u4EventType);
/*************************l3vpnutil.c end******************************/
/***************************l3vpnshcli.c**************/
INT4
L3VpnRsvpShowMapLabelTable (INT4 CliHandle);

INT4
L3VpnRsvpShowMapLabel (INT4 CliHandle,
        tL3VpnRsvpMapLabelEntry * pL3VpnRsvpMapLabelEntry);

extern INT1
nmhSetFsMplsL3VpnRsvpTeMapTnlIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhSetFsMplsL3VpnRsvpTeMapRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsMplsL3VpnRsvpTeMapTnlIndex ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhTestv2FsMplsL3VpnRsvpTeMapRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

extern INT1
nmhGetFsMplsL3VpnRsvpTeMapTnlIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

extern UINT4
L3VpnGetGlobalTnlTable (UINT4 u4TnlIndex,tL3VpnGlobalTnlInfo *pL3VpnGlobalTnlInfo);
extern UINT4
L3VpnAddGlobalTnlTable (tL3VpnRsvpTeLspEventInfo * pL3VpnRsvpTeLspEventInfo);


VOID
MplsGetPrefix(uGenU4Addr *pDestAddr,uGenU4Addr*pMask,UINT2 u2AddrType,uGenU4Addr *pPrefix);

UINT1
MplsValidateIPv6Addr (UINT4 *pu4ErrorCode,
                        tSNMP_OCTET_STRING_TYPE * pTestValIpv6Addr);

#ifdef MPLS_IPV6_WANTED
UINT1 MplsRegisterWithIpv6(VOID);
UINT1 MplsDeRegisterWithIpv6 (VOID);
VOID  MplsIpv6RtChgEventHandler (tNetIpv6HliParams * pNetIpv6HlParams);
VOID  LdpIpv6RtChgEventHandler (tNetIpv6HliParams * pNetIpv6HlParams);
UINT1 MplsGetIpv6Subnetmasklen (UINT1 *pu1IpAddress);
INT4  MplsGetIPV6Subnetmask (UINT1 u1Prefixlen, UINT1 *pu1Max);
VOID  MplsIpv6RtChgNotification(tNetIpv6HliParams * pNetIpv6HlParams);
VOID
L2VpnHandleIpv6RtChgNotification (tNetIpv6HliParams * pNetIpv6HlParams);
#endif

VOID  LdpCopyAddr(uGenU4Addr *pGenU4Addr1,uGenU4Addr *pGenU4Addr2,UINT2 u2AddrType);
#ifdef HVPLS_WANTED
INT4
L2vpnHandleVplsNotification(INT4 i4SetValVplsStatusNotifEnable);
INT4
L2VpnIsSpokePw(UINT4 u4PwVcIndex);
#endif
#endif
/*---------------------------------------------------------------------------*/
/*                        End of file mplsutil.h                             */
/*---------------------------------------------------------------------------*/
