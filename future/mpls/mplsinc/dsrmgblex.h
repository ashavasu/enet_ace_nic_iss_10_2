/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: dsrmgblex.h,v 1.2 2007/02/01 14:57:05 iss Exp $
 *
 * Description: This file contains the extern declaration for global 
 *              variables declared and used for the MPLS-DS-RM Module.
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : dsrmgblex.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS-DS-RM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : contains the extern declaration for global
 *                             variables declared and used for the MPLS-DS-RM
 *                             Module. 
 *---------------------------------------------------------------------------*/

#ifndef _DSRMGBLEX_H
#define _DSRMGBLEX_H

extern tDiffServRMGblInfo gDiffServRMGblInfo;

#endif/*_DSRMGBLEX_H */

