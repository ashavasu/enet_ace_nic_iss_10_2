
/* $Id: fsmplsnc.h,v 1.1 2016/07/13 12:34:09 siva Exp $
    ISS Wrapper header
    module Aricent-MPLS-MIB

 */
#ifndef _H_i_Aricent_MPLS_MIB
#define _H_i_Aricent_MPLS_MIB

/********************************************************************
* FUNCTION NcFsMplsAdminStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsAdminStatusSet (
                INT4 i4FsMplsAdminStatus );

/********************************************************************
* FUNCTION NcFsMplsAdminStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsAdminStatusTest (UINT4 *pu4Error,
                INT4 i4FsMplsAdminStatus );

/********************************************************************
* FUNCTION NcFsMplsQosPolicySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsQosPolicySet (
                INT4 i4FsMplsQosPolicy );

/********************************************************************
* FUNCTION NcFsMplsQosPolicyTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsQosPolicyTest (UINT4 *pu4Error,
                INT4 i4FsMplsQosPolicy );

/********************************************************************
* FUNCTION NcFsMplsFmDebugLevelSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsFmDebugLevelSet (
                INT4 i4FsMplsFmDebugLevel );

/********************************************************************
* FUNCTION NcFsMplsFmDebugLevelTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsFmDebugLevelTest (UINT4 *pu4Error,
                INT4 i4FsMplsFmDebugLevel );

/********************************************************************
* FUNCTION NcFsMplsTeDebugLevelSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTeDebugLevelSet (
                UINT4 u4FsMplsTeDebugLevel );

/********************************************************************
* FUNCTION NcFsMplsTeDebugLevelTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTeDebugLevelTest (UINT4 *pu4Error,
                UINT4 u4FsMplsTeDebugLevel );

/********************************************************************
* FUNCTION NcFsMplsLsrLabelAllocationMethodSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLsrLabelAllocationMethodSet (
                INT4 i4FsMplsLsrLabelAllocationMethod );

/********************************************************************
* FUNCTION NcFsMplsLsrLabelAllocationMethodTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLsrLabelAllocationMethodTest (UINT4 *pu4Error,
                INT4 i4FsMplsLsrLabelAllocationMethod );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspPreConfExpPhbMapIndexSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspPreConfExpPhbMapIndexSet (
                INT4 i4FsMplsDiffServElspPreConfExpPhbMapIndex );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspPreConfExpPhbMapIndexTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspPreConfExpPhbMapIndexTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServElspPreConfExpPhbMapIndex );

/********************************************************************
* FUNCTION NcFsMplsLdpLsrIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpLsrIdSet (
                UINT1 *pau1FsMplsLdpLsrId );

/********************************************************************
* FUNCTION NcFsMplsLdpLsrIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpLsrIdTest (UINT4 *pu4Error,
                UINT1 *pau1FsMplsLdpLsrId );

/********************************************************************
* FUNCTION NcFsMplsLdpForceOptionSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpForceOptionSet (
                INT4 i4FsMplsLdpForceOption );

/********************************************************************
* FUNCTION NcFsMplsLdpForceOptionTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpForceOptionTest (UINT4 *pu4Error,
                INT4 i4FsMplsLdpForceOption );

/********************************************************************
* FUNCTION NcFsMplsRsvpTeGrMaxWaitTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsRsvpTeGrMaxWaitTimeSet (
                INT4 i4FsMplsRsvpTeGrMaxWaitTime );

/********************************************************************
* FUNCTION NcFsMplsRsvpTeGrMaxWaitTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsRsvpTeGrMaxWaitTimeTest (UINT4 *pu4Error,
                INT4 i4FsMplsRsvpTeGrMaxWaitTime );

/********************************************************************
* FUNCTION NcFsMplsLdpGrMaxWaitTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrMaxWaitTimeSet (
                INT4 i4FsMplsLdpGrMaxWaitTime );

/********************************************************************
* FUNCTION NcFsMplsLdpGrMaxWaitTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrMaxWaitTimeTest (UINT4 *pu4Error,
                INT4 i4FsMplsLdpGrMaxWaitTime );

/********************************************************************
* FUNCTION NcFsMplsMaxIfTableEntriesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxIfTableEntriesGet (
                UINT4 *pu4FsMplsMaxIfTableEntries );

/********************************************************************
* FUNCTION NcFsMplsMaxFTNEntriesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxFTNEntriesGet (
                UINT4 *pu4FsMplsMaxFTNEntries );

/********************************************************************
* FUNCTION NcFsMplsMaxInSegmentEntriesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxInSegmentEntriesGet (
                UINT4 *pu4FsMplsMaxInSegmentEntries );

/********************************************************************
* FUNCTION NcFsMplsMaxOutSegmentEntriesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxOutSegmentEntriesGet (
                UINT4 *pu4FsMplsMaxOutSegmentEntries );

/********************************************************************
* FUNCTION NcFsMplsMaxXCEntriesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxXCEntriesGet (
                UINT4 *pu4FsMplsMaxXCEntries );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspMapEntriesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspMapEntriesGet (
                UINT4 *pu4FsMplsDiffServElspMapEntries );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsEntriesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsEntriesGet (
                UINT4 *pu4FsMplsDiffServParamsEntries );

/********************************************************************
* FUNCTION NcFsMplsMaxHopListsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxHopListsGet (
                UINT4 *pu4FsMplsMaxHopLists );

/********************************************************************
* FUNCTION NcFsMplsMaxPathOptPerListGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxPathOptPerListGet (
                UINT4 *pu4FsMplsMaxPathOptPerList );

/********************************************************************
* FUNCTION NcFsMplsMaxHopsPerPathOptionGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxHopsPerPathOptionGet (
                UINT4 *pu4FsMplsMaxHopsPerPathOption );

/********************************************************************
* FUNCTION NcFsMplsMaxArHopListsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxArHopListsGet (
                UINT4 *pu4FsMplsMaxArHopLists );

/********************************************************************
* FUNCTION NcFsMplsMaxHopsPerArHopListGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxHopsPerArHopListGet (
                UINT4 *pu4FsMplsMaxHopsPerArHopList );

/********************************************************************
* FUNCTION NcFsMplsMaxRsvpTrfcParamsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxRsvpTrfcParamsGet (
                UINT4 *pu4FsMplsMaxRsvpTrfcParams );

/********************************************************************
* FUNCTION NcFsMplsMaxCrLdpTrfcParamsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxCrLdpTrfcParamsGet (
                UINT4 *pu4FsMplsMaxCrLdpTrfcParams );

/********************************************************************
* FUNCTION NcFsMplsMaxDServElspsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxDServElspsGet (
                UINT4 *pu4FsMplsMaxDServElsps );

/********************************************************************
* FUNCTION NcFsMplsMaxDServLlspsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxDServLlspsGet (
                UINT4 *pu4FsMplsMaxDServLlsps );

/********************************************************************
* FUNCTION NcFsMplsMaxTnlsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsMaxTnlsGet (
                UINT4 *pu4FsMplsMaxTnls );

/********************************************************************
* FUNCTION NcFsMplsLsrMaxLdpEntitiesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLsrMaxLdpEntitiesGet (
                INT4 *pi4FsMplsLsrMaxLdpEntities );

/********************************************************************
* FUNCTION NcFsMplsLsrMaxLocalPeersGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLsrMaxLocalPeersGet (
                INT4 *pi4FsMplsLsrMaxLocalPeers );

/********************************************************************
* FUNCTION NcFsMplsLsrMaxRemotePeersGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLsrMaxRemotePeersGet (
                INT4 *pi4FsMplsLsrMaxRemotePeers );

/********************************************************************
* FUNCTION NcFsMplsLsrMaxIfacesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLsrMaxIfacesGet (
                INT4 *pi4FsMplsLsrMaxIfaces );

/********************************************************************
* FUNCTION NcFsMplsLsrMaxLspsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLsrMaxLspsGet (
                INT4 *pi4FsMplsLsrMaxLsps );

/********************************************************************
* FUNCTION NcFsMplsLsrMaxVcMergeCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLsrMaxVcMergeCountGet (
                INT4 *pi4FsMplsLsrMaxVcMergeCount );

/********************************************************************
* FUNCTION NcFsMplsLsrMaxVpMergeCountGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLsrMaxVpMergeCountGet (
                INT4 *pi4FsMplsLsrMaxVpMergeCount );

/********************************************************************
* FUNCTION NcFsMplsLsrMaxCrlspTnlsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLsrMaxCrlspTnlsGet (
                INT4 *pi4FsMplsLsrMaxCrlspTnls );

/********************************************************************
* FUNCTION NcFsMplsActiveRsvpTeTnlsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsActiveRsvpTeTnlsGet (
                INT4 *pi4FsMplsActiveRsvpTeTnls );

/********************************************************************
* FUNCTION NcFsMplsActiveLspsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsActiveLspsGet (
                INT4 *pi4FsMplsActiveLsps );

/********************************************************************
* FUNCTION NcFsMplsActiveCrLspsGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsActiveCrLspsGet (
                INT4 *pi4FsMplsActiveCrLsps );

/********************************************************************
* FUNCTION NcFsMplsCrlspDebugLevelSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspDebugLevelSet (
                INT4 i4FsMplsCrlspDebugLevel );

/********************************************************************
* FUNCTION NcFsMplsCrlspDebugLevelTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspDebugLevelTest (UINT4 *pu4Error,
                INT4 i4FsMplsCrlspDebugLevel );

/********************************************************************
* FUNCTION NcFsMplsCrlspDumpTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspDumpTypeSet (
                INT4 i4FsMplsCrlspDumpType );

/********************************************************************
* FUNCTION NcFsMplsCrlspDumpTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspDumpTypeTest (UINT4 *pu4Error,
                INT4 i4FsMplsCrlspDumpType );

/********************************************************************
* FUNCTION NcFsMplsCrlspDumpDirectionSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspDumpDirectionSet (
                INT4 i4FsMplsCrlspDumpDirection );

/********************************************************************
* FUNCTION NcFsMplsCrlspDumpDirectionTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspDumpDirectionTest (UINT4 *pu4Error,
                INT4 i4FsMplsCrlspDumpDirection );

/********************************************************************
* FUNCTION NcFsMplsCrlspPersistanceSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspPersistanceSet (
                INT4 i4FsMplsCrlspPersistance );

/********************************************************************
* FUNCTION NcFsMplsCrlspPersistanceTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspPersistanceTest (UINT4 *pu4Error,
                INT4 i4FsMplsCrlspPersistance );

/********************************************************************
* FUNCTION NcFsMplsL2VpnTrcFlagSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnTrcFlagSet (
                INT4 i4FsMplsL2VpnTrcFlag );

/********************************************************************
* FUNCTION NcFsMplsL2VpnTrcFlagTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnTrcFlagTest (UINT4 *pu4Error,
                INT4 i4FsMplsL2VpnTrcFlag );

/********************************************************************
* FUNCTION NcFsMplsL2VpnCleanupIntervalSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnCleanupIntervalSet (
                INT4 i4FsMplsL2VpnCleanupInterval );

/********************************************************************
* FUNCTION NcFsMplsL2VpnCleanupIntervalTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnCleanupIntervalTest (UINT4 *pu4Error,
                INT4 i4FsMplsL2VpnCleanupInterval );

/********************************************************************
* FUNCTION NcFsMplsL2VpnAdminStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnAdminStatusSet (
                INT4 i4FsMplsL2VpnAdminStatus );

/********************************************************************
* FUNCTION NcFsMplsL2VpnAdminStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnAdminStatusTest (UINT4 *pu4Error,
                INT4 i4FsMplsL2VpnAdminStatus );

/********************************************************************
* FUNCTION NcFsMplsLocalCCTypesCapabilitiesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
/*extern INT1 NcFsMplsLocalCCTypesCapabilitiesSet (
                ncx_list_t ncx_list_tFsMplsLocalCCTypesCapabilities );
*/
/********************************************************************
* FUNCTION NcFsMplsLocalCCTypesCapabilitiesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
/*extern INT1 NcFsMplsLocalCCTypesCapabilitiesTest (UINT4 *pu4Error,
                ncx_list_t ncx_list_tFsMplsLocalCCTypesCapabilities );
*/
/********************************************************************
* FUNCTION NcFsMplsLocalCVTypesCapabilitiesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
/*extern INT1 NcFsMplsLocalCVTypesCapabilitiesSet (
                ncx_list_t ncx_list_tFsMplsLocalCVTypesCapabilities );
*/
/********************************************************************
* FUNCTION NcFsMplsLocalCVTypesCapabilitiesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
/*extern INT1 NcFsMplsLocalCVTypesCapabilitiesTest (UINT4 *pu4Error,
                ncx_list_t ncx_list_tFsMplsLocalCVTypesCapabilities );
*/
/********************************************************************
* FUNCTION NcFsMplsRouterIDSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsRouterIDSet (
                UINT1 *pau1FsMplsRouterID );

/********************************************************************
* FUNCTION NcFsMplsRouterIDTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsRouterIDTest (UINT4 *pu4Error,
                UINT1 *pau1FsMplsRouterID );

/********************************************************************
* FUNCTION NcFsMplsHwCCTypeCapabilitiesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
/*extern INT1 NcFsMplsHwCCTypeCapabilitiesGet (
                ncx_list_t *pncx_list_tFsMplsHwCCTypeCapabilities );
*/
/********************************************************************
* FUNCTION NcFsMplsL2VpnMaxPwVcEntriesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnMaxPwVcEntriesSet (
                UINT4 u4FsMplsL2VpnMaxPwVcEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnMaxPwVcEntriesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnMaxPwVcEntriesTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL2VpnMaxPwVcEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnMaxPwVcMplsEntriesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnMaxPwVcMplsEntriesSet (
                UINT4 u4FsMplsL2VpnMaxPwVcMplsEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnMaxPwVcMplsEntriesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnMaxPwVcMplsEntriesTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL2VpnMaxPwVcMplsEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnMaxPwVcMplsInOutEntriesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnMaxPwVcMplsInOutEntriesSet (
                UINT4 u4FsMplsL2VpnMaxPwVcMplsInOutEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnMaxPwVcMplsInOutEntriesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnMaxPwVcMplsInOutEntriesTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL2VpnMaxPwVcMplsInOutEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnMaxEthernetPwVcsSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnMaxEthernetPwVcsSet (
                UINT4 u4FsMplsL2VpnMaxEthernetPwVcs );

/********************************************************************
* FUNCTION NcFsMplsL2VpnMaxEthernetPwVcsTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnMaxEthernetPwVcsTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL2VpnMaxEthernetPwVcs );

/********************************************************************
* FUNCTION NcFsMplsL2VpnMaxPwVcEnetEntriesSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnMaxPwVcEnetEntriesSet (
                UINT4 u4FsMplsL2VpnMaxPwVcEnetEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnMaxPwVcEnetEntriesTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnMaxPwVcEnetEntriesTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL2VpnMaxPwVcEnetEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnActivePwVcEntriesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnActivePwVcEntriesGet (
                UINT4 *pu4FsMplsL2VpnActivePwVcEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnActivePwVcMplsEntriesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnActivePwVcMplsEntriesGet (
                UINT4 *pu4FsMplsL2VpnActivePwVcMplsEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnActivePwVcEnetEntriesGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnActivePwVcEnetEntriesGet (
                UINT4 *pu4FsMplsL2VpnActivePwVcEnetEntries );

/********************************************************************
* FUNCTION NcFsMplsL2VpnNoOfPwVcEntriesCreatedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnNoOfPwVcEntriesCreatedGet (
                UINT4 *pu4FsMplsL2VpnNoOfPwVcEntriesCreated );

/********************************************************************
* FUNCTION NcFsMplsL2VpnNoOfPwVcEntriesDeletedGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL2VpnNoOfPwVcEntriesDeletedGet (
                UINT4 *pu4FsMplsL2VpnNoOfPwVcEntriesDeleted );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspInfoListIndexNextGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspInfoListIndexNextGet (
                INT4 *pi4FsMplsDiffServElspInfoListIndexNext );

/********************************************************************
* FUNCTION NcFsMplsTnlModelSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTnlModelSet (
                INT4 i4FsMplsTnlModel );

/********************************************************************
* FUNCTION NcFsMplsTnlModelTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTnlModelTest (UINT4 *pu4Error,
                INT4 i4FsMplsTnlModel );

/********************************************************************
* FUNCTION NcFsMplsRsrcMgmtTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsRsrcMgmtTypeSet (
                INT4 i4FsMplsRsrcMgmtType );

/********************************************************************
* FUNCTION NcFsMplsRsrcMgmtTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsRsrcMgmtTypeTest (UINT4 *pu4Error,
                INT4 i4FsMplsRsrcMgmtType );

/********************************************************************
* FUNCTION NcFsMplsTTLValSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTTLValSet (
                INT4 i4FsMplsTTLVal );

/********************************************************************
* FUNCTION NcFsMplsTTLValTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTTLValTest (UINT4 *pu4Error,
                INT4 i4FsMplsTTLVal );

/********************************************************************
* FUNCTION NcFsMplsDsTeStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeStatusSet (
                INT4 i4FsMplsDsTeStatus );

/********************************************************************
* FUNCTION NcFsMplsDsTeStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeStatusTest (UINT4 *pu4Error,
                INT4 i4FsMplsDsTeStatus );

/********************************************************************
* FUNCTION NcFsMplsSimulateFailureSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsSimulateFailureSet (
                INT4 i4FsMplsSimulateFailure );

/********************************************************************
* FUNCTION NcFsMplsSimulateFailureTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsSimulateFailureTest (UINT4 *pu4Error,
                INT4 i4FsMplsSimulateFailure );

/********************************************************************
* FUNCTION NcFsMplsiTTLValGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsiTTLValGet (
                INT4 *pi4FsMplsiTTLVal );

/********************************************************************
* FUNCTION NcFsMplsOTTLValGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsOTTLValGet (
                INT4 *pi4FsMplsOTTLVal );

/********************************************************************
* FUNCTION NcFsMplsprviTTLValGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsprviTTLValGet (
                INT4 *pi4FsMplsprviTTLVal );

/********************************************************************
* FUNCTION NcFsMplsprvOTTLValGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsprvOTTLValGet (
                INT4 *pi4FsMplsprvOTTLVal );

/********************************************************************
* FUNCTION NcFsMplsLdpGrCapabilitySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrCapabilitySet (
                INT4 i4FsMplsLdpGrCapability );

/********************************************************************
* FUNCTION NcFsMplsLdpGrCapabilityTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrCapabilityTest (UINT4 *pu4Error,
                INT4 i4FsMplsLdpGrCapability );

/********************************************************************
* FUNCTION NcFsMplsLdpGrForwardEntryHoldTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrForwardEntryHoldTimeSet (
                INT4 i4FsMplsLdpGrForwardEntryHoldTime );

/********************************************************************
* FUNCTION NcFsMplsLdpGrForwardEntryHoldTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrForwardEntryHoldTimeTest (UINT4 *pu4Error,
                INT4 i4FsMplsLdpGrForwardEntryHoldTime );

/********************************************************************
* FUNCTION NcFsMplsLdpGrMaxRecoveryTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrMaxRecoveryTimeSet (
                INT4 i4FsMplsLdpGrMaxRecoveryTime );

/********************************************************************
* FUNCTION NcFsMplsLdpGrMaxRecoveryTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrMaxRecoveryTimeTest (UINT4 *pu4Error,
                INT4 i4FsMplsLdpGrMaxRecoveryTime );

/********************************************************************
* FUNCTION NcFsMplsLdpGrNeighborLivenessTimeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrNeighborLivenessTimeSet (
                INT4 i4FsMplsLdpGrNeighborLivenessTime );

/********************************************************************
* FUNCTION NcFsMplsLdpGrNeighborLivenessTimeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrNeighborLivenessTimeTest (UINT4 *pu4Error,
                INT4 i4FsMplsLdpGrNeighborLivenessTime );

/********************************************************************
* FUNCTION NcFsMplsLdpGrProgressStatusGet
* 
* Wrapper for nmhGet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpGrProgressStatusGet (
                INT4 *pi4FsMplsLdpGrProgressStatus );

/********************************************************************
* FUNCTION NcFsMplsLdpConfigurationSequenceTLVEnableSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpConfigurationSequenceTLVEnableSet (
                INT4 i4FsMplsLdpConfigurationSequenceTLVEnable );

/********************************************************************
* FUNCTION NcFsMplsLdpConfigurationSequenceTLVEnableTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsLdpConfigurationSequenceTLVEnableTest (UINT4 *pu4Error,
                INT4 i4FsMplsLdpConfigurationSequenceTLVEnable );

/********************************************************************
* FUNCTION NcFsMplsCrlspTnlRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspTnlRowStatusSet (
                UINT4 u4FsMplsCrlspTnlIndex,
                INT4 i4FsMplsCrlspTnlRowStatus );

/********************************************************************
* FUNCTION NcFsMplsCrlspTnlRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspTnlRowStatusTest (UINT4 *pu4Error,
                UINT4 u4FsMplsCrlspTnlIndex,
                INT4 i4FsMplsCrlspTnlRowStatus );

/********************************************************************
* FUNCTION NcFsMplsCrlspTnlStorageTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspTnlStorageTypeSet (
                UINT4 u4FsMplsCrlspTnlIndex,
                INT4 i4FsMplsCrlspTnlStorageType );

/********************************************************************
* FUNCTION NcFsMplsCrlspTnlStorageTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsCrlspTnlStorageTypeTest (UINT4 *pu4Error,
                UINT4 u4FsMplsCrlspTnlIndex,
                INT4 i4FsMplsCrlspTnlStorageType );

/********************************************************************
* FUNCTION NcFsMplsVplsVsiSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsVsiSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsMplsVplsVsi );

/********************************************************************
* FUNCTION NcFsMplsVplsVsiTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsVsiTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsMplsVplsVsi );

/********************************************************************
* FUNCTION NcFsMplsVplsVpnIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsVpnIdSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT1 *pau1FsMplsVplsVpnId );

/********************************************************************
* FUNCTION NcFsMplsVplsVpnIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsVpnIdTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT1 *pau1FsMplsVplsVpnId );

/********************************************************************
* FUNCTION NcFsMplsVplsNameSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsNameSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT1 *pFsMplsVplsName );

/********************************************************************
* FUNCTION NcFsMplsVplsNameTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsNameTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT1 *pFsMplsVplsName );

/********************************************************************
* FUNCTION NcFsMplsVplsDescrSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsDescrSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT1 *pFsMplsVplsDescr );

/********************************************************************
* FUNCTION NcFsMplsVplsDescrTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsDescrTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT1 *pFsMplsVplsDescr );

/********************************************************************
* FUNCTION NcFsMplsVplsFdbHighWatermarkSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsFdbHighWatermarkSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT4 u4FsMplsVplsFdbHighWatermark );

/********************************************************************
* FUNCTION NcFsMplsVplsFdbHighWatermarkTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsFdbHighWatermarkTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT4 u4FsMplsVplsFdbHighWatermark );

/********************************************************************
* FUNCTION NcFsMplsVplsFdbLowWatermarkSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsFdbLowWatermarkSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT4 u4FsMplsVplsFdbLowWatermark );

/********************************************************************
* FUNCTION NcFsMplsVplsFdbLowWatermarkTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsFdbLowWatermarkTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT4 u4FsMplsVplsFdbLowWatermark );

/********************************************************************
* FUNCTION NcFsMplsVplsRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsRowStatusSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsMplsVplsRowStatus );

/********************************************************************
* FUNCTION NcFsMplsVplsRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsRowStatusTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsMplsVplsRowStatus );

/********************************************************************
* FUNCTION NcFsMplsVplsL2MapFdbIdSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsL2MapFdbIdSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsMplsVplsL2MapFdbId );

/********************************************************************
* FUNCTION NcFsMplsVplsL2MapFdbIdTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsL2MapFdbIdTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsMplsVplsL2MapFdbId );

/********************************************************************
* FUNCTION NcFsmplsVplsMtuSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsmplsVplsMtuSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT4 u4FsmplsVplsMtu );

/********************************************************************
* FUNCTION NcFsmplsVplsMtuTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsmplsVplsMtuTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                UINT4 u4FsmplsVplsMtu );

/********************************************************************
* FUNCTION NcFsmplsVplsStorageTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsmplsVplsStorageTypeSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsmplsVplsStorageType );

/********************************************************************
* FUNCTION NcFsmplsVplsStorageTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsmplsVplsStorageTypeTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsmplsVplsStorageType );

/********************************************************************
* FUNCTION NcFsmplsVplsSignalingTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsmplsVplsSignalingTypeSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsmplsVplsSignalingType );

/********************************************************************
* FUNCTION NcFsmplsVplsSignalingTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsmplsVplsSignalingTypeTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsmplsVplsSignalingType );

/********************************************************************
* FUNCTION NcFsmplsVplsControlWordSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsmplsVplsControlWordSet (
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsmplsVplsControlWord );

/********************************************************************
* FUNCTION NcFsmplsVplsControlWordTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsmplsVplsControlWordTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsInstanceIndex,
                INT4 i4FsmplsVplsControlWord );

/********************************************************************
* FUNCTION NcFsMplsPortBundleStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsPortBundleStatusSet (
                INT4 i4IfIndex,
                INT4 i4FsMplsPortBundleStatus );

/********************************************************************
* FUNCTION NcFsMplsPortBundleStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsPortBundleStatusTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4FsMplsPortBundleStatus );

/********************************************************************
* FUNCTION NcFsMplsPortMultiplexStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsPortMultiplexStatusSet (
                INT4 i4IfIndex,
                INT4 i4FsMplsPortMultiplexStatus );

/********************************************************************
* FUNCTION NcFsMplsPortMultiplexStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsPortMultiplexStatusTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4FsMplsPortMultiplexStatus );

/********************************************************************
* FUNCTION NcFsMplsPortAllToOneBundleStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsPortAllToOneBundleStatusSet (
                INT4 i4IfIndex,
                INT4 i4FsMplsPortAllToOneBundleStatus );

/********************************************************************
* FUNCTION NcFsMplsPortAllToOneBundleStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsPortAllToOneBundleStatusTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4FsMplsPortAllToOneBundleStatus );

/********************************************************************
* FUNCTION NcFsMplsPortRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsPortRowStatusSet (
                INT4 i4IfIndex,
                INT4 i4FsMplsPortRowStatus );

/********************************************************************
* FUNCTION NcFsMplsPortRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsPortRowStatusTest (UINT4 *pu4Error,
                INT4 i4IfIndex,
                INT4 i4FsMplsPortRowStatus );

/********************************************************************
* FUNCTION NcFsMplsVplsAcMapPortIfIndexSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsAcMapPortIfIndexSet (
                UINT4 u4FsMplsVplsAcMapVplsIndex,
                UINT4 u4FsMplsVplsAcMapAcIndex,
                UINT4 u4FsMplsVplsAcMapPortIfIndex );

/********************************************************************
* FUNCTION NcFsMplsVplsAcMapPortIfIndexTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsAcMapPortIfIndexTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsAcMapVplsIndex,
                UINT4 u4FsMplsVplsAcMapAcIndex,
                UINT4 u4FsMplsVplsAcMapPortIfIndex );

/********************************************************************
* FUNCTION NcFsMplsVplsAcMapPortVlanSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsAcMapPortVlanSet (
                UINT4 u4FsMplsVplsAcMapVplsIndex,
                UINT4 u4FsMplsVplsAcMapAcIndex,
                UINT4 u4FsMplsVplsAcMapPortVlan );

/********************************************************************
* FUNCTION NcFsMplsVplsAcMapPortVlanTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsAcMapPortVlanTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsAcMapVplsIndex,
                UINT4 u4FsMplsVplsAcMapAcIndex,
                UINT4 u4FsMplsVplsAcMapPortVlan );

/********************************************************************
* FUNCTION NcFsMplsVplsAcMapRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsAcMapRowStatusSet (
                UINT4 u4FsMplsVplsAcMapVplsIndex,
                UINT4 u4FsMplsVplsAcMapAcIndex,
                INT4 i4FsMplsVplsAcMapRowStatus );

/********************************************************************
* FUNCTION NcFsMplsVplsAcMapRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsVplsAcMapRowStatusTest (UINT4 *pu4Error,
                UINT4 u4FsMplsVplsAcMapVplsIndex,
                UINT4 u4FsMplsVplsAcMapAcIndex,
                INT4 i4FsMplsVplsAcMapRowStatus );

/********************************************************************
* FUNCTION NcFsMplsTunnelCRLDPResPeakDataRateSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTunnelCRLDPResPeakDataRateSet (
                UINT4 u4MplsTunnelResourceIndex,
                UINT4 u4FsMplsTunnelCRLDPResPeakDataRate );

/********************************************************************
* FUNCTION NcFsMplsTunnelCRLDPResPeakDataRateTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTunnelCRLDPResPeakDataRateTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelResourceIndex,
                UINT4 u4FsMplsTunnelCRLDPResPeakDataRate );

/********************************************************************
* FUNCTION NcFsMplsTunnelCRLDPResCommittedDataRateSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTunnelCRLDPResCommittedDataRateSet (
                UINT4 u4MplsTunnelResourceIndex,
                UINT4 u4FsMplsTunnelCRLDPResCommittedDataRate );

/********************************************************************
* FUNCTION NcFsMplsTunnelCRLDPResCommittedDataRateTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTunnelCRLDPResCommittedDataRateTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelResourceIndex,
                UINT4 u4FsMplsTunnelCRLDPResCommittedDataRate );

/********************************************************************
* FUNCTION NcFsMplsTunnelCRLDPResPeakBurstSizeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTunnelCRLDPResPeakBurstSizeSet (
                UINT4 u4MplsTunnelResourceIndex,
                UINT4 u4FsMplsTunnelCRLDPResPeakBurstSize );

/********************************************************************
* FUNCTION NcFsMplsTunnelCRLDPResPeakBurstSizeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTunnelCRLDPResPeakBurstSizeTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelResourceIndex,
                UINT4 u4FsMplsTunnelCRLDPResPeakBurstSize );

/********************************************************************
* FUNCTION NcFsMplsTunnelCRLDPResCommittedBurstSizeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTunnelCRLDPResCommittedBurstSizeSet (
                UINT4 u4MplsTunnelResourceIndex,
                UINT4 u4FsMplsTunnelCRLDPResCommittedBurstSize );

/********************************************************************
* FUNCTION NcFsMplsTunnelCRLDPResCommittedBurstSizeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTunnelCRLDPResCommittedBurstSizeTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelResourceIndex,
                UINT4 u4FsMplsTunnelCRLDPResCommittedBurstSize );

/********************************************************************
* FUNCTION NcFsMplsTunnelCRLDPResExcessBurstSizeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTunnelCRLDPResExcessBurstSizeSet (
                UINT4 u4MplsTunnelResourceIndex,
                UINT4 u4FsMplsTunnelCRLDPResExcessBurstSize );

/********************************************************************
* FUNCTION NcFsMplsTunnelCRLDPResExcessBurstSizeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsTunnelCRLDPResExcessBurstSizeTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelResourceIndex,
                UINT4 u4FsMplsTunnelCRLDPResExcessBurstSize );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspMapPhbDscpSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspMapPhbDscpSet (
                INT4 i4FsMplsDiffServElspMapIndex,
                INT4 i4FsMplsDiffServElspMapExpIndex,
                INT4 i4FsMplsDiffServElspMapPhbDscp );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspMapPhbDscpTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspMapPhbDscpTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServElspMapIndex,
                INT4 i4FsMplsDiffServElspMapExpIndex,
                INT4 i4FsMplsDiffServElspMapPhbDscp );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspMapStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspMapStatusSet (
                INT4 i4FsMplsDiffServElspMapIndex,
                INT4 i4FsMplsDiffServElspMapExpIndex,
                INT4 i4FsMplsDiffServElspMapStatus );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspMapStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspMapStatusTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServElspMapIndex,
                INT4 i4FsMplsDiffServElspMapExpIndex,
                INT4 i4FsMplsDiffServElspMapStatus );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsServiceTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsServiceTypeSet (
                INT4 i4FsMplsDiffServParamsIndex,
                INT4 i4FsMplsDiffServParamsServiceType );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsServiceTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsServiceTypeTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServParamsIndex,
                INT4 i4FsMplsDiffServParamsServiceType );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsLlspPscDscpSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsLlspPscDscpSet (
                INT4 i4FsMplsDiffServParamsIndex,
                INT4 i4FsMplsDiffServParamsLlspPscDscp );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsLlspPscDscpTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsLlspPscDscpTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServParamsIndex,
                INT4 i4FsMplsDiffServParamsLlspPscDscp );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsElspTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsElspTypeSet (
                INT4 i4FsMplsDiffServParamsIndex,
                INT4 i4FsMplsDiffServParamsElspType );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsElspTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsElspTypeTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServParamsIndex,
                INT4 i4FsMplsDiffServParamsElspType );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsElspSigExpPhbMapIndexSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsElspSigExpPhbMapIndexSet (
                INT4 i4FsMplsDiffServParamsIndex,
                INT4 i4FsMplsDiffServParamsElspSigExpPhbMapIndex );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsElspSigExpPhbMapIndexTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsElspSigExpPhbMapIndexTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServParamsIndex,
                INT4 i4FsMplsDiffServParamsElspSigExpPhbMapIndex );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsStatusSet (
                INT4 i4FsMplsDiffServParamsIndex,
                INT4 i4FsMplsDiffServParamsStatus );

/********************************************************************
* FUNCTION NcFsMplsDiffServParamsStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServParamsStatusTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServParamsIndex,
                INT4 i4FsMplsDiffServParamsStatus );

/********************************************************************
* FUNCTION NcFsMplsDiffServClassTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServClassTypeSet (
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServClassType );

/********************************************************************
* FUNCTION NcFsMplsDiffServClassTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServClassTypeTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServClassType );

/********************************************************************
* FUNCTION NcFsMplsDiffServServiceTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServServiceTypeSet (
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServServiceType );

/********************************************************************
* FUNCTION NcFsMplsDiffServServiceTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServServiceTypeTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServServiceType );

/********************************************************************
* FUNCTION NcFsMplsDiffServLlspPscSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServLlspPscSet (
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServLlspPsc );

/********************************************************************
* FUNCTION NcFsMplsDiffServLlspPscTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServLlspPscTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServLlspPsc );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspTypeSet (
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServElspType );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspTypeTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServElspType );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspListIndexSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspListIndexSet (
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServElspListIndex );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspListIndexTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspListIndexTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServElspListIndex );

/********************************************************************
* FUNCTION NcFsMplsDiffServRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServRowStatusSet (
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServRowStatus );

/********************************************************************
* FUNCTION NcFsMplsDiffServRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServRowStatusTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServRowStatus );

/********************************************************************
* FUNCTION NcFsMplsDiffServStorageTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServStorageTypeSet (
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServStorageType );

/********************************************************************
* FUNCTION NcFsMplsDiffServStorageTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServStorageTypeTest (UINT4 *pu4Error,
                UINT4 u4MplsTunnelIndex,
                UINT4 u4MplsTunnelInstance,
                UINT4 u4MplsTunnelIngressLSRId,
                UINT4 u4MplsTunnelEgressLSRId,
                INT4 i4FsMplsDiffServStorageType );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspInfoPHBSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspInfoPHBSet (
                INT4 i4FsMplsDiffServElspInfoListIndex,
                INT4 i4FsMplsDiffServElspInfoIndex,
                INT4 i4FsMplsDiffServElspInfoPHB );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspInfoPHBTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspInfoPHBTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServElspInfoListIndex,
                INT4 i4FsMplsDiffServElspInfoIndex,
                INT4 i4FsMplsDiffServElspInfoPHB );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspInfoResourcePointerSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspInfoResourcePointerSet (
                INT4 i4FsMplsDiffServElspInfoListIndex,
                INT4 i4FsMplsDiffServElspInfoIndex,
                UINT1 *pFsMplsDiffServElspInfoResourcePointer );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspInfoResourcePointerTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspInfoResourcePointerTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServElspInfoListIndex,
                INT4 i4FsMplsDiffServElspInfoIndex,
                UINT1 *pFsMplsDiffServElspInfoResourcePointer );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspInfoRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspInfoRowStatusSet (
                INT4 i4FsMplsDiffServElspInfoListIndex,
                INT4 i4FsMplsDiffServElspInfoIndex,
                INT4 i4FsMplsDiffServElspInfoRowStatus );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspInfoRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspInfoRowStatusTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServElspInfoListIndex,
                INT4 i4FsMplsDiffServElspInfoIndex,
                INT4 i4FsMplsDiffServElspInfoRowStatus );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspInfoStorageTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspInfoStorageTypeSet (
                INT4 i4FsMplsDiffServElspInfoListIndex,
                INT4 i4FsMplsDiffServElspInfoIndex,
                INT4 i4FsMplsDiffServElspInfoStorageType );

/********************************************************************
* FUNCTION NcFsMplsDiffServElspInfoStorageTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDiffServElspInfoStorageTypeTest (UINT4 *pu4Error,
                INT4 i4FsMplsDiffServElspInfoListIndex,
                INT4 i4FsMplsDiffServElspInfoIndex,
                INT4 i4FsMplsDiffServElspInfoStorageType );

/********************************************************************
* FUNCTION NcFsMplsDsTeClassTypeDescriptionSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeClassTypeDescriptionSet (
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT1 *pFsMplsDsTeClassTypeDescription );

/********************************************************************
* FUNCTION NcFsMplsDsTeClassTypeDescriptionTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeClassTypeDescriptionTest (UINT4 *pu4Error,
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT1 *pFsMplsDsTeClassTypeDescription );

/********************************************************************
* FUNCTION NcFsMplsDsTeClassTypeRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeClassTypeRowStatusSet (
                UINT4 u4FsMplsDsTeClassTypeIndex,
                INT4 i4FsMplsDsTeClassTypeRowStatus );

/********************************************************************
* FUNCTION NcFsMplsDsTeClassTypeRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeClassTypeRowStatusTest (UINT4 *pu4Error,
                UINT4 u4FsMplsDsTeClassTypeIndex,
                INT4 i4FsMplsDsTeClassTypeRowStatus );

/********************************************************************
* FUNCTION NcFsMplsDsTeClassTypeBwPercentageSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeClassTypeBwPercentageSet (
                UINT4 u4FsMplsDsTeClassTypeIndex,
                INT4 i4FsMplsDsTeClassTypeBwPercentage );

/********************************************************************
* FUNCTION NcFsMplsDsTeClassTypeBwPercentageTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeClassTypeBwPercentageTest (UINT4 *pu4Error,
                UINT4 u4FsMplsDsTeClassTypeIndex,
                INT4 i4FsMplsDsTeClassTypeBwPercentage );

/********************************************************************
* FUNCTION NcFsMplsDsTeTcTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTcTypeSet (
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTcIndex,
                INT4 i4FsMplsDsTeTcType );

/********************************************************************
* FUNCTION NcFsMplsDsTeTcTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTcTypeTest (UINT4 *pu4Error,
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTcIndex,
                INT4 i4FsMplsDsTeTcType );

/********************************************************************
* FUNCTION NcFsMplsDsTeTcDescriptionSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTcDescriptionSet (
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTcIndex,
                UINT1 *pFsMplsDsTeTcDescription );

/********************************************************************
* FUNCTION NcFsMplsDsTeTcDescriptionTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTcDescriptionTest (UINT4 *pu4Error,
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTcIndex,
                UINT1 *pFsMplsDsTeTcDescription );

/********************************************************************
* FUNCTION NcFsMplsDsTeTcMapRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTcMapRowStatusSet (
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTcIndex,
                INT4 i4FsMplsDsTeTcMapRowStatus );

/********************************************************************
* FUNCTION NcFsMplsDsTeTcMapRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTcMapRowStatusTest (UINT4 *pu4Error,
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTcIndex,
                INT4 i4FsMplsDsTeTcMapRowStatus );

/********************************************************************
* FUNCTION NcFsMplsDsTeTeClassDescSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTeClassDescSet (
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTeClassPriority,
                UINT1 *pFsMplsDsTeTeClassDesc );

/********************************************************************
* FUNCTION NcFsMplsDsTeTeClassDescTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTeClassDescTest (UINT4 *pu4Error,
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTeClassPriority,
                UINT1 *pFsMplsDsTeTeClassDesc );

/********************************************************************
* FUNCTION NcFsMplsDsTeTeClassNumberSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTeClassNumberSet (
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTeClassPriority,
                UINT4 u4FsMplsDsTeTeClassNumber );

/********************************************************************
* FUNCTION NcFsMplsDsTeTeClassNumberTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTeClassNumberTest (UINT4 *pu4Error,
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTeClassPriority,
                UINT4 u4FsMplsDsTeTeClassNumber );

/********************************************************************
* FUNCTION NcFsMplsDsTeTeClassRowStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTeClassRowStatusSet (
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTeClassPriority,
                INT4 i4FsMplsDsTeTeClassRowStatus );

/********************************************************************
* FUNCTION NcFsMplsDsTeTeClassRowStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsDsTeTeClassRowStatusTest (UINT4 *pu4Error,
                UINT4 u4FsMplsDsTeClassTypeIndex,
                UINT4 u4FsMplsDsTeTeClassPriority,
                INT4 i4FsMplsDsTeTeClassRowStatus );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfNameSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfNameSet (
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                UINT1 *pau1FsMplsL3VpnVrfName );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfNameTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfNameTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                UINT1 *pau1FsMplsL3VpnVrfName );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrDestTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrDestTypeSet (
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrDestType );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrDestTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrDestTypeTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrDestType );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrDestSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrDestSet (
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                UINT1 *pau1FsMplsL3VpnVrfEgressRteInetCidrDest );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrDestTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrDestTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                UINT1 *pau1FsMplsL3VpnVrfEgressRteInetCidrDest );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrPfxLenSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrPfxLenSet (
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                UINT4 u4FsMplsL3VpnVrfEgressRteInetCidrPfxLen );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrPfxLenTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrPfxLenTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                UINT4 u4FsMplsL3VpnVrfEgressRteInetCidrPfxLen );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrLabelPolicySet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrLabelPolicySet (
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrLabelPolicyTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrLabelPolicyTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrNHopTypeSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrNHopTypeSet (
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrNHopType );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrNHopTypeTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrNHopTypeTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrNHopType );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrNextHopSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrNextHopSet (
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                UINT1 *pau1FsMplsL3VpnVrfEgressRteInetCidrNextHop );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrNextHopTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrNextHopTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                UINT1 *pau1FsMplsL3VpnVrfEgressRteInetCidrNextHop );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrIfIndexSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrIfIndexSet (
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrIfIndex );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrIfIndexTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrIfIndexTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrIfIndex );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrMetric1Set
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrMetric1Set (
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrMetric1 );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrMetric1Test
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrMetric1Test (UINT4 *pu4Error,
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrMetric1 );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrStatusSet
* 
* Wrapper for nmhSet API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrStatusSet (
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrStatus );

/********************************************************************
* FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrStatusTest
* 
* Wrapper for nmhTest API's
* 
* RETURNS:
*     error status
********************************************************************/
extern INT1 NcFsMplsL3VpnVrfEgressRteInetCidrStatusTest (UINT4 *pu4Error,
                UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                INT4 i4FsMplsL3VpnVrfEgressRteInetCidrStatus );

/* END i_Aricent_MPLS_MIB.c */


#endif
