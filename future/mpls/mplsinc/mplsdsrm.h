/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsdsrm.h,v 1.3 2013/07/10 13:30:30 siva Exp $
 *
 * Description: This file contains the structure type
 *              definitions declared and used for the MPLS-DS-RM Module.
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : mplsdsrm.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS-DS-RM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the structure type
 *                             definitions declared and used for the 
 *                             MPLS-DS-RM Module.
 *----------------------------------------------------------------------------*/

#ifndef _MPLSDSRM_H
#define _MPLSDSRM_H

typedef struct _RMIfInfo
{
     UINT1 au1RMSupportedPhbs[TE_DS_MAX_NO_OF_PHBS];
     UINT2 u2Resvd;
     UINT1 au1RMSupportedPSC[TE_DS_MAX_NO_PSCS];
     UINT1 au1Pad[3];
     UINT1 au1RMSupportedClassType[TE_DS_MAX_NO_OF_CLASSTYPES];
}
tRMIfInfo;

/*Resource manager related global structure */
typedef struct _DiffServRMGblInfo
{
      tRMIfInfo aRMIfInfo[TE_DS_MAX_IFACES];
      UINT1     u1RsrcMgmtType;
      UINT1     u1Resvd;
      UINT2     u2Resvd;
}
tDiffServRMGblInfo;

#define RESOURCE_MANAGEMENT_TYPE(gDiffServRMGblInfo)\
                             (gDiffServRMGblInfo.u1RsrcMgmtType)

/* Maximum number of Interfaces */

#define TE_DS_CLASSTYPE_RESOURCES  0
#define TE_DS_PEROABASED_RESOURCES 1
#define TE_DS_AGGREGATE_RESOURCES  2 

/******************************************************************************/
/*tRMIfInfo macros */
#define RM_SUPPORTED_PHB(x,y) \
        gDiffServRMGblInfo.aRMIfInfo[x].au1RMSupportedPhbs[y]

#define RM_SUPPORTED_PSC(x,y) \
        gDiffServRMGblInfo.aRMIfInfo[x].au1RMSupportedPSC[y]

#define RM_SUPPORTED_CLASSTYPE(x,y)\
        gDiffServRMGblInfo.aRMIfInfo[x].au1RMSupportedClassType[y]
/******************************************************************************/


#endif /* _MPLSDSRM_H */
/*---------------------------------------------------------------------------*/
/*                        End of file mplsdsrm.h                             */
/*---------------------------------------------------------------------------*/

