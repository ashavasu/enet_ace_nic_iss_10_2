/********************************************************************
 *                                                                  *
 * $RCSfile: tcextrn.h,v $
 *                                                                  *
 * $Date: 2007/10/26 09:45:19 $                                                          *
 *                                                                  *
 * $Revision: 1.4 $                                                     *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tcextrn.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the structure type
 *                             definitions declared and exported to the 
 *                             Other Modules.
 *---------------------------------------------------------------------------*/

#ifndef _TCEXTRN_H
#define _TCEXTRN_H

#include "rsvp.h"
#include "teextrn.h"

#define TC_SUCCESS                              0
#define TC_FAILURE                              1

#define TC_INVALID_APPID                        0
#define TC_CRLDP_APPID                          1
#define TC_RSVPTE_APPID                         2

#define TC_NO_ERR                             100
#define TC_ERR_APPID_DOES_NOT_EXIST           101
#define TC_ERR_INTERFACEID_DOES_NOT_EXIST     102
#define TC_ERR_INVALID_RES_POOL_TYPE          103
#define TC_ERR_INVALID_RES_SPEC_TYPE          104
#define TC_ERR_BANDWIDTH_NOT_AVAILABLE        105
#define TC_ERR_RESOURCE_NODE_ALLOC_FAILURE    106
#define TC_ERR_HANDLE_NOT_AVAILABLE           107
#define TC_ERR_UPDATE_RES_INFO_DATABASE       108
#define TC_ERR_HANDLE_NOT_FOUND               109
#define TC_ERR_FILTER_SPEC_NODE_ALLOC_FAILURE 110
#define TC_ERR_ADD_FILTER_SPEC                111
#define TC_ERR_FILTER_SPEC_NOT_FOUND          112

#define tTcCRLDPTrfcParams                    tCRLDPTrfcParams
#define tTcFlowSpec                           tFlowSpec
#define tTcAdSpec                             tAdSpec
#define tTcTSpec                              tTSpec

/* Currently this structure has only the Peak Data Rate.  Should be enhanced
 * to contain all the Configurable Resource Information at the Interface based
 * on which Admission Control will be performed */
typedef struct _TcResInfo
{
    UINT4 u4PeakDataRate;
} tTcResInfo;

typedef struct _TcFilterInfo
{
    UINT4 u4SrcAddr;
    UINT4 u4DestAddr;
    UINT4 u4TnlId;
    UINT4 u4TnlInstance;
} tTcFilterInfo;

typedef union _TcResSpec
{
    tTcCRLDPTrfcParams CrldpTrfcParams;
    tTcFlowSpec RsvpTrfcParams;
    tTcResInfo  TcStdTrfcParams;
    /* Other Formats for Specifying Resource Requirements -- to be added 
     * to this Union */
} tuTcResSpec;

typedef UINT4 tTcResHandle;

typedef enum 
{
        TC_CRLDP_TYPE_RES_SPEC           = 0,  
        TC_RSVPTE_TYPE_RES_SPEC          = 1,     
        TC_STD_TYPE_RES_SPEC             = 2
        /* One Resource Specification Format Value has to be 
         * added for each Res Spec Type in tuTcResSpec Union */
} teResSpec;

typedef enum
{
        TC_GENERAL_DF                    = 0,
        TC_DIFFSERV_CLASS_CS1            = 1,
        TC_DIFFSERV_CLASS_CS2            = 2,
        TC_DIFFSERV_CLASS_CS3            = 3,
        TC_DIFFSERV_CLASS_CS4            = 4,
        TC_DIFFSERV_CLASS_CS5            = 5,
        TC_DIFFSERV_CLASS_CS6            = 6,
        TC_DIFFSERV_CLASS_CS7            = 7,
        TC_DIFFSERV_CLASS_EF             = 8,
        TC_DIFFSERV_CLASS_AF1            = 9,
        TC_DIFFSERV_CLASS_AF2            = 10,
        TC_DIFFSERV_CLASS_AF3            = 11,
        TC_DIFFSERV_CLASS_AF4            = 12,
        TC_DIFFSERV_CLASS_CS8            = 13,
        TC_INTSERV                       = 14,
        TC_DIFFSERV_CLASS_CLASSTYPE_0    = 15,
        TC_DIFFSERV_CLASS_CLASSTYPE_1    = 16
        /* Other Resource Pools configurable at the TC should be added here */
} teResPoolType;

typedef UINT1 (*tTcAdmCtrlAlgoFnPtr)(UINT4 u4AppId, UINT4 u4InterfaceId, 
               UINT1 u1ResPoolType, tTcResInfo *pConvResReq, 
               UINT1 *pu1ErrorCode);

typedef UINT1 (*tTcAppCallBackFnPtr)(VOID);

VOID TcShutdown ARG_LIST ((VOID));

#endif /* _TCEXTRN_H */
/*---------------------------------------------------------------------------*/
/*                        End of file tcextrn.h                              */
/*---------------------------------------------------------------------------*/
