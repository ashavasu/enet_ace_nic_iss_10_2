/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mpcmnapi.h,v 1.6 2012/11/23 11:35:05 siva Exp $
 *
 * Description: This file contains common API information
 *******************************************************************/

#include "mplsapi.h"

INT4
MplsOamIfHandlePathStatusChange (tMplsApiInInfo *pInMplsApiInfo, 
                                 tMplsApiOutInfo *pOutMplsApiInfo);
INT4
MplsOamIfUpdateSessionParams (tMplsApiInInfo *pInMplsApiInfo);
INT4
MplsOamIfRegWithMegForNotif (tMplsApiInInfo *pInMplsApiInfo);
INT4
MplsOamTxPacketHandleFromApp (tMplsApiInInfo *pInMplsApiInfo);
INT4
MplsCmnExtGetLspInfo (tMplsApiInInfo *pInMplsApiInfo,
                tMplsApiOutInfo *pOutMplsApiInfo);
INT4
MplsCmnExtGetNodeId (tMplsApiInInfo *pInMplsApiInfo,
               tMplsApiOutInfo *pOutMplsApiInfo);
INT4
MplsCmnExtGetRevPathFromFwdPath (tMplsApiInInfo *pInMplsApiInfo,
                           tMplsApiOutInfo *pOutMplsApiInfo);
INT4
MplsCmnExtGetServicePointerOid (tMplsApiInInfo *pInMplsApiInfo,
                          tMplsApiOutInfo *pOutMplsApiInfo);
INT4
MplsOamIfGetMegInfo (tMplsApiInInfo *pInMplsApiInfo, 
                   tMplsApiOutInfo *pOutMplsApiInfo);
INT4
MplsCmnExtGetTunnelInfo (tMplsApiInInfo *pInMplsApiInfo, 
                   tMplsApiOutInfo *pOutMplsApiInfo);
INT4
MplsCmnExtGetPathFromInLblInfo (tMplsApiInInfo *pInMplsApiInfo,
                              tMplsApiOutInfo *pOutMplsApiInfo, 
                              BOOL1 *pbIsTnlOrLspExists);
INT4
MplsOamIfGetMegIdFromMegName (tMplsApiInInfo *pInMplsApiInfo, 
                            tMplsApiOutInfo *pOutMplsApiInfo);
INT4
MplsOamIfNotifyMegStatusToApp (UINT4 u4SrcModId, UINT4 u4DstModId,
                             tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry,
                             UINT1 u1MegStatus);
INT4
MplsOamIfHdlPathStatusChgForMeg (tMplsApiInInfo *pInMplsApiInfo, 
                                 tMplsApiOutInfo *pOutMplsApiInfo);
INT4
MplsOamIfHdlPathStatusChgForTnl (tMplsApiInInfo *pInMplsApiInfo);
INT4
MplsOamIfHdlPathStatusChgForLsp (tMplsApiInInfo *pInMplsApiInfo);
INT4
MplsOamIfCheckAndUpdateTnlForMeg (UINT4 u4ContextId, tMplsMegId *pMplsMegId,
                                tMplsTnlLspId *pTnlId,
                                tMplsOamPathStatus *pMplsOamPathStatus);
INT4
MplsCmnExtGetRouterId  (tMplsApiInInfo *pInMplsApiInfo,
                  tMplsApiOutInfo *pOutMplsApiInfo);
INT4
MplsOamIfUpdateLpsStatus (tMplsApiInInfo *pInMplsApiInfo);

INT4
MplsOamIfUpdateOamAppStatus (tMplsApiInInfo * pInMplsApiInfo);
INT4
MplsOamIfRegAppCallBack (tMplsApiInInfo * pInMplsApiInfo);

INT4
MplsOamIfGetOamSessionId (UINT4 u4MegIndex, UINT4 u4MeIndex, 
                          UINT4 u4MpIndex, UINT4 *pu4OamSessionId);

extern UINT4 MplsFTNRowStatus [13];
