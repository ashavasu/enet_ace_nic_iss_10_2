/********************************************************************
 *                                                                  *
 * $Id: lsppext.h,v 1.1 2014/03/06 12:55:31 siva Exp $
 *                                                                  *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : lsppext.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LSPP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains definitions that
 *                             are exported to the the Other modules.
 *---------------------------------------------------------------------------*/
#ifndef _LSPPEXT_H
#define _LSPPEXT_H

VOID IssMplsOamEchoSrcGlobalConfig (tCliHandle CliHandle);

#endif /* _LSPPEXT_H */

