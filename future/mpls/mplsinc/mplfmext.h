/********************************************************************
 *                                                                  *
 * $Id: mplfmext.h,v 1.15 2014/11/08 11:59:52 siva Exp $
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : mplfmext.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS FM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains structures used by MPLS-FM,
 *                             which are exported to the MPLS signalling 
 *                             protocols
 *----------------------------------------------------------------------------*/

#ifndef _MPLFMEXT_H
#define _MPLFMEXT_H

#include "mplsutil.h"

/* Porting related typedefs */
typedef tCRU_BUF_CHAIN_HEADER    tMplsBufChainHeader;
typedef tTMO_SLL_NODE            tMplsSllNode;
typedef tTMO_SLL                 tMplsSll;

typedef struct _IfTableEntry {
     tMplsSllNode      MplsSllNode;
     UINT4             u4IfIndex;
     UINT2             u2IfType;
     UINT2             u2MplsEnabled;
     UINT4             u4DefConnHandle;
     UINT4             u4DefLabel;
     UINT4             u4SrcAddr;
     tMacAddr          SrcMacAddr;
     UINT2             u2IfTableStatus;
     tMplsElspIfParams ElspIfParams;
} tIfTableEntry;

typedef struct _MplsTrfcParms {
   UINT4 u4PeakDataRate;
   UINT4 u4PeakBurstSize;
   UINT4 u4CommittedDataRate;
   UINT4 u4CommittedBurstSize;
   UINT4 u4ExcessBurstSize;
} tMplsTrfcParms;

/* Structure to store the Application flow information */
typedef struct _ApplInfo {
   UINT4   u4ProtocolId;
   UINT2   u2DestPortNum;
   UINT2   u2SrcPortNum;
} tApplInfo;


typedef struct _FecParams {
   UINT1           u1FecType;
   UINT1           u1Tos;
   UINT2           u2Resvd; 
   uGenU4Addr        DestMask;
   uGenU4Addr        DestAddrPrefix;
   uGenU4Addr        SrcAddr;
   UINT4           u4TnlInstance;
   UINT4           u4TnlId;
   UINT4           u4IngressId;
   UINT4           u4EgressId;
   tMplsTrfcParms  MplsTrfcParms;
   tApplInfo       *pApplInfo;
   UINT2           u2AddrType;
   UINT1	   u1Pad[2];
} tFecParams;

typedef struct _StackTnlInfo {
   UINT1        u1StackTnlBit;
   UINT1        u1IsIf;
   UINT2        u2IsFrrMPByPassTnl;
   UINT4        u4LspId;
   UINT4        u4TnlId;
   UINT4        u4TnlInstance;
   UINT4        u4IngressId;
   UINT4        u4EgressId;
} tStackTnlInfo;

typedef struct _NHLFE {
   UINT1               u1Operation;
   UINT1               u1Free;
   UINT2               u2Rsvd2;
   UINT4               u4OutLabel;
   UINT4               u4OutIfIndex;
   tMplsDiffServParams *pDiffServParams;
   UINT4               u4NhlfeInfoIndex;
   UINT1               u1NHAddrType;
   UINT1               u1Creator;
   UINT2               u2Rsvd3;
   UINT4               u4ConnHandle;
   uGenU4Addr            NextHopAddr;
   tMplsSll            FtnLinksList;
   UINT1               u1NumDecTtl;
   UINT1               u1OperStatus;
   UINT1               u1MergeFlag;
   UINT1               u1RemoteBit;
   UINT4               u4MandFlg;
   tStackTnlInfo       StackTnlInfo;
   struct _NHLFE       *pNhlfe;
} tNHLFE;

typedef struct _LspInfo {
   UINT4         u4IfIndex;
   UINT4         u4InTopLabel;
   tFecParams    FecParams;
   tNHLFE        *pNhlfe;
   tStackTnlInfo StackTnlInfo;
   eDirection    Direction;
   UINT4         u4BkpXcIndex;
   UINT4         u4TnlLabel;
   UINT1         u1Owner;
   BOOL1         bIsUsedByL2vpn;
   BOOL1         bIsLspDestroy;
#ifdef LDP_GR_WANTED
   UINT1         u1IsNPBlock; /* MPLS_TRUE or MPLS_FALSE */ 
#else
   UINT1         u1Pad;
#endif
} tLspInfo;

#ifdef LANAI_WANTED
typedef struct _VcTableEntry
{
    tMplsSllNode        MplsSllNode;
    tIfTableEntry      *pIfEntry;
    tMplsTrfcParms     *pMplsTrfcParms;
    UINT4               u4ConnHandle;
    UINT4               u4Label;
    UINT2               u2Vci;
    UINT2               u2Vpi;
    UINT4               u4PortNumber;
    UINT1               u1TrafficType;
    UINT1               u1DirectionType;
    UINT1               u1EncapsType;
    UINT1               u1ConnType;
    UINT2               u2ProtocolType;
    UINT2               u2UsedCount;
} tVcTableEntry;

/* Structure to store the infomations received  from ATM */
typedef struct _MplsAtmInfo {
   tVcTableEntry *pVcEntry;
} tMplsAtmInfo;

/* Structure for passing the Atm related information to the Atm driver.
 * This structure is filled by the Higher Layer(MPLS-FM)
 */
typedef struct _AtmConnInfo
{
  UINT4   u4Handle;                   /* Higher Layer Connection Handle */
  UINT4   u4PortNumber;               /* Port Number */
  UINT2   u2Vpi;                      /* Virtual Path Identifier */
  UINT2   u2Vci;                      /* Virtual Channel Identifier */
  UINT1   u1DirectionType;            /* Packet transmit mode  */
  UINT1   u1EncapsType;               /* Encapsulation to be done on packet */
  UINT1   u1ConnType;                 /* Type of VC connection MPLS_PVC */
  UINT1   u1ProtocolType;             /* Type of Protocol lying above MPLS. */
  UINT1   u1TrafficType;              /* Type of bit rate eg. MPLS_ABR */
  UINT1   u1Rsvd;                     /* Reserved for four byte alignment */
  UINT2   u2Mtu;                      /* Maximum Size of the packet */
  tMplsTrfcParms *pMplsTrfcParms;     /* QoS Traffic Parameters required */
  UINT1   (*pCallBackFn) 
          (tCRU_BUF_CHAIN_HEADER *pBuf,UINT4 u4IfIndex, UINT1 u1EncapType);
                                /* Fn(). invoked to send packet to MPLS */
} tAtmConnInfo;
#endif
BOOL1 MplsIsMplsInitialised (VOID);
#endif /*_MPLFMEXT_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file mplfmext.h                             */
/*---------------------------------------------------------------------------*/
