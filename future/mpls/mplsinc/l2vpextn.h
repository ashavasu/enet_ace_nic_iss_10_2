/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: l2vpextn.h,v 1.39 2017/06/08 11:40:31 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : l2vpextn.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : L2VPN
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the interface structure type
 *                             definitions 
 *---------------------------------------------------------------------------*/
#ifndef _L2VPEXTN_H 
#define _L2VPEXTN_H

#include "mplsapi.h"
#include "teextrn.h"


#if defined(VPLS_GR_WANTED) ||defined (LDP_GR_WANTED)
#define tL2VpnPwHwList  tMplsL2VpnPwHwList 
#define tL2VpnVplsHwList tMplsL2VpnVplsHwList
#endif
INT4
L2vpnExtGetPwServicePointerOid (tMplsApiInInfo *pInMplsApiInfo,
                                tMplsApiOutInfo *pOutMplsApiInfo);

UINT4 L2VpnTeEventHandler ARG_LIST ((UINT1 *pMsg, UINT4 u4EvtType));

UINT4
L2VpnNonTeEventHandler ARG_LIST ((UINT4 u4XcIndex,tGenU4Addr* pPrefix, UINT4 u4TnlBkpMPLabel1,
                                  UINT4 u4TnlBkpMPLabel2, UINT4 u4TnlBkpMPLabel3,
                                        UINT4 u4EvtType));
VOID L2VpnDeInit ARG_LIST ((VOID));
INT4 L2VpnInit ARG_LIST ((VOID));
VOID L2vpnDeleteMemPools ARG_LIST ((VOID));
VOID MplsHandlePwArpResolveTmrEvent ARG_LIST ((VOID *pArg));
#ifndef OFFSET_OF
#define OFFSET_OF(typeName, memberName) \
    ((size_t) ((VOID *) &((typeName *) 0)->memberName - (VOID *) 0))
#endif  /* OFFSET_OF */

#ifndef CONTAINER_OF
#define CONTAINER_OF(memberPtr, typeName, memberName) \
    ((VOID *) memberPtr - OFFSET_OF (typeName, memberName))
#endif  /* CONTAINER_OF */

#define TMO_SLL_Copy_Adjust(pList)  (((tTMO_SLL *) pList)->Tail->pNext = (VOID *) (pList))


#define L2VPN_PWVC_UP              1
#define L2VPN_PWVC_DOWN            2
#define L2VPN_PWVC_DELETE          5

/* macros for the msgs within lbl - map, withdraw, rel, wildcard withdraw ....*/
#define L2VPN_LDP_LBL_MAP_MSG           0x00000001
#define L2VPN_LDP_LBL_MAP_REQ_MSG       0x00000002
#define L2VPN_LDP_LBL_WDRAW_MSG         0x00000003
#define L2VPN_LDP_LBL_WC_WDRAW_MSG      0x00000004
#define L2VPN_LDP_LBL_REL_MSG           0x00000005
#define L2VPN_LDP_ADDR_WDRAW_MSG        0x00000006
#define L2VPN_LDP_ICCP_RG_CONNECT_MSG           0x00000007
#define L2VPN_LDP_ICCP_RG_DISCONNECT_MSG        0x00000008
#define L2VPN_LDP_ICCP_RG_NOTIFICATION_MSG      0x00000009
#define L2VPN_LDP_ICCP_RG_DATA_MSG              0x0000000A

#define L2VPN_LDP_INVALID_REQ_ID        0xffffffff
#define L2VPN_LDP_INVALID_LABEL         0xffffffff

#define L2VPN_LDP_LABEL_MSG_EVENT 1
#define L2VPN_PWVC_CBIT_STATUS_CODE_NONE 0

#define L2VPN_LDP_MAX_AI_LEN L2VPN_MAX_AI_LEN

/* PSN - MPLS Messages */
#define L2VPN_MPLS_PWVC_TE_TNL_UP                 0x04000001
#define L2VPN_MPLS_PWVC_TE_TNL_DOWN               0x04000002
#define L2VPN_MPLS_PWVC_LSP_UP                    0x04000004
#define L2VPN_MPLS_PWVC_LSP_DOWN                  0x04000008
#define L2VPN_MPLS_PWVC_LSP_DESTROY               0x04000010
#define L2VPN_MPLS_PWVC_VC_UP                     0x04000020
#define L2VPN_MPLS_PWVC_VC_DOWN                   0x04000040
#define L2VPN_MPLS_PWVC_TE_TNL_REESTB             0x04000080
#define L2VPN_MPLS_PWVC_TE_FRR_MP_ADD             0x04000100
#define L2VPN_MPLS_PWVC_TE_FRR_MP_DEL             0x04000200
#define L2VPN_MPLS_PWVC_TE_FRR_PLR_ADD            0x04000400
#define L2VPN_MPLS_PWVC_TE_FRR_PLR_DEL            0x04000800
#define L2VPN_MPLS_PWVC_P2MP_TE_TNL_DEST_ADD      0x04001000
#define L2VPN_MPLS_PWVC_P2MP_TE_TNL_DEST_DELETE   0x04002000

/* Msg Events To LDP  - ssn, lbl and notif */
#define L2VPN_LDP_SSN_CREATE_EVT                   0x08000001
#define L2VPN_LDP_SSN_REL_EVT                      0x08000002
#define L2VPN_LDP_LBL_MSG_EVT                      0x08000004
#define L2VPN_LDP_NOTIF_EVT                        0x08000008
#define L2VPN_LDP_ADDR_WDRAW_EVT                   0x08000010
#define L2VPN_LDP_DEREG_WITH_LDP                   0x08000020
#define L2VPN_LDP_ICCP_RG_EVT                      0x08000080
#define L2VPN_LDP_READY_EVENT        0x08000100
/* Session messages from LDP */
#define L2VPN_LDP_PWVC_SSN_UP                      0x08000012
#define L2VPN_LDP_PWVC_SSN_DOWN                    0x08000014
#define L2VPN_LDP_GR_PWVC_SSN_UP                   0x08000016
/* Label (Advertisment) messages from LDP */
#define L2VPN_LDP_PWVC_LBL_MSG                     0x08000018
/* Notification messages from LDP */
#define L2VPN_LDP_PWVC_NOTIF_MSG                   0x08000020
/* Address Withdraw message from LDP */
#define L2VPN_LDP_PWVC_ADDR_WDRAW_MSG              0x08000024
#define L2VPN_LDP_PWVC_GR_SHUTDOWN                 0x08000032
#define L2VPN_LDP_PWVC_GR_HELPER_MARK_STALE        0x08000036
#define L2VPN_LDP_PWVC_GR_DELETE_STALE_ENTRIES     0x08000040
#define L2VPN_LDP_PWVC_GR_FWD_HOLD_EXPIRY          0x08000044
#define L2VPN_LDP_PWVC_GR_SET_RECOVER_TMR          0x08000048
#define L2VPN_LDP_PWVC_GR_START        0x08000052
/* Notification events from LDP */
#define L2VPN_LDP_PWVC_UP                          0x00000010
#define L2VPN_LDP_PWVC_DOWN                        0x00000012
#define L2VPN_LDP_RSRC_AVAILABLE                   0x00000014
#define L2VPN_LDP_RSRC_UNAVAILABLE                 0x00000018
#define L2VPN_LDP_PEER_NO_PW_SUPPORT               0x00000020
#define L2VPN_LDP_NOTIFICATION_MSG                 0x00000022
/* Notification events to LDP */
#define L2VPN_LDP_REL_LBL_RES                      0x00000024
#define L2VPN_LDP_NOTIF_MSG_EVT                    0x00000026


enum
{
    L2VPN_PSN_NO_DEL_L3FTN_MPLS_TNL_IF = 0, /* For not deleting L3FTN and MPLS Tnl from HW*/

    L2VPN_PSN_DEL_L3FTN   =  1,    /* For deleting the L3FTN from the h/w */

    L2VPN_PSN_DEL_MPLS_TNL_IF   =  2,    /* For deleting the MPLS Tnl If */

    L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF   =  3,     /* For deleting L3FTN and MPLS Tnl If */
};


typedef struct _PwVcMplsTeTnlIndex
{
   tTeRouterId       TnlLclLSR;
   tTeRouterId       TnlPeerLSR;
   UINT4             u4TnlIndex;
   UINT4             u4LspId;
   UINT4             u4TnlXcIndex;
   UINT4             u4TnlIfIndex;
    /* Following three labels carry the information for FRR tnls
     * 1:1 - Bkp tnl label for link/node protection - u4TnlBkpMPLabel1
     * N:1 - Bypass tnl label for link/node protection - u4TnlBkpMPLabel1
     *       Prot tnl label for link/node protection - u4TnlBkpMPLabel2
     * LDP over RSVP
     *  1:1 - Bkp tnl label for link/node protection - u4TnlBkpMPLabel1
     *        LDP label - u4TnlBkpMPLabel2
     *  N:1 - Bypass tnl label for link/node protection - u4TnlBkpMPLabel1
     *        Prot tnl label for link/node protection - u4TnlBkpMPLabel2
     * These labels are used during ILM entry deletion from hw */
   UINT4             u4TnlBkpMPLabel1;
   UINT4             u4TnlBkpMPLabel2;
   UINT4             u4TnlBkpMPLabel3;
   UINT2             u2TnlInstance;
   UINT2             u2BkpTnlInstance;
   UINT1             u1AssociateFlag;
   UINT1             u1TnlRole;
   UINT1             au1NextHop[MAC_ADDR_LEN];
   UINT1             u1DeleteTnlAction;
   UINT1             u1TnlSwitchingType;
   UINT1             au1Rsvd[2];
} tPwVcMplsTeTnlIndex;

typedef struct _PwVcSsnEvtInfo
{
   uGenU4Addr      PeerAddr;
   UINT4             u4LocalLdpEntityID;
   UINT4             u4LdpEntityIndex;
   UINT4             u4PeerLdpId;
   UINT2             u2RecoveryTime;
   UINT2             u2AddrType;
   UINT1             padByte;
   UINT1             au1Pad[2];
   BOOL1             b1IsActive;
}tPwVcSsnEvtInfo;

typedef struct _PwVcNotifEvtInfo
{
   uGenU4Addr        PeerAddr;
   UINT4             u4LocalLdpEntityID;
   UINT4             u4LdpEntityIndex;
   UINT4             u4Label;
   UINT4             u4VcID;
   UINT4             u4IfIndex;
   UINT4             u4GroupID;
   UINT2             u2AddrType;
   UINT1             au1Agi[L2VPN_LDP_MAX_AI_LEN];
   UINT1             au1Saii[L2VPN_LDP_MAX_AI_LEN];
   UINT1             au1Taii[L2VPN_LDP_MAX_AI_LEN];
   INT1              i1PwOwner;
   INT1              i1ControlWord;
   
   UINT1          u1AgiLen;
   UINT1          u1SaiiLen;
   UINT1          u1TaiiLen;
   UINT1             u1VcType;
   
   UINT1             u1MsgCode;
   UINT1             u1PwStatusCode; /* PW Forwarding or Not Forwarding */
   
   UINT1             u1SaiiType; /* Local AII type applicable for 
                                    GEN FEC TLV */
   UINT1             u1TaiiType; /* Remote AII type applicable for 
                                    GEN FEC TLV */
   UINT1             u1AgiType; /* AGI type applicable for 
                                   GEN FEC TLV */
   UINT1             au1Pad[3]; /* Padding */
}tPwVcNotifEvtInfo;

typedef struct _PwVcLblMsgInfo
{
   UINT4             u4LocalLdpEntityID;
   UINT4             u4LdpEntityIndex;
   uGenU4Addr        PeerAddr;
   UINT4             u4PeerLdpId;
   UINT4             u4Label;
   UINT4             u4LblReqId;

   UINT4             u4GroupID;
   UINT4             u4PwVcID;
   UINT4             u4PwStatusCode;
   
   UINT4             u4StaleLabel;
   UINT4             u4StaticLabel;   /*Statically configured label */
   INT1              *pi1LocalIfString;
   UINT2             u2IfMtu;
   UINT2             u2AddrType;
   UINT2             u2ReqVlanId;

   UINT1             au1Agi[L2VPN_LDP_MAX_AI_LEN];
   UINT1             au1Saii[L2VPN_LDP_MAX_AI_LEN];
   UINT1             au1Taii[L2VPN_LDP_MAX_AI_LEN];
   
   UINT1             u1SaiiType; /* Local AII type applicable for 
                                    GEN FEC TLV */
   UINT1             u1TaiiType; /* Remote AII type applicable for
                                    GEN FEC TLV */
   UINT1             u1AgiType; /* AGI type applicable for 
                                   GEN FEC TLV */
   UINT1          u1AgiLen;
   
   UINT1          u1SaiiLen;
   UINT1          u1TaiiLen;
   UINT1             u1RmtStatusCapable; /* Peer PW Status TLV */
   UINT1             u1MsgType; /* Lbl Rel Msg , Lbl Map Msg, */
   
   UINT1             u1MplsType;
   INT1              i1PwOwner; /* Fec 128 or Fec 129 */
   /* PwVcControlWord is a Boolean value */
   INT1              i1ControlWord;
   INT1              i1PwType;
   
   /*VCCV related parameters*/
   UINT1             u1LocalCCAdvert;
   UINT1             u1LocalCVAdvert;
   /* PwVcLocalIfString is a Boolean value */
   INT1              i1StatusCode; /* 0 - None, 1 - Wrong C bit, 
                                      2 - Illegal C bit */
   UINT1             u1LocalCapabAdvert; /* Status Indication/VCCV */
   
   BOOL1             b1IsActive;
   UINT1             au1Pad;
}
tPwVcLblMsgInfo;


/* PW Redundancy */

enum
{
    LDPSESSION_EXTENSION_CAPABILITYANNOUNCEMENT     = 0,
    LDPSESSION_EXTENSION_P2MP                       = 1,
    LDPSESSION_EXTENSION_MP2MP                      = 2,
    LDPSESSION_EXTENSION_MBB                        = 3,
    LDPSESSION_EXTENSION_FAULTTOLERANCE             = 4,
    LDPSESSION_EXTENSION_ICCP                       = 5,

    LDPICCP_STATUS_CONNSENT                 = 1,
    LDPICCP_STATUS_WAITINGFORCONN           = 2,
    LDPICCP_STATUS_CONNTXNAKD               = 3,
    LDPICCP_STATUS_CONNRXNAKD               = 4,
    LDPICCP_STATUS_CONNECTED                = 5,
    LDPICCP_STATUS_DISCONNECTED             = 6,
    LDPICCP_STATUS_CAPWITHDRAWN             = 7,
    LDPICCP_STATUS_NOTYETKNOWN              = 8,
    LDPICCP_STATUS_SESSIONDOWN              = 9,
    LDPICCP_STATUS_DEFAULT                  = LDPICCP_STATUS_NOTYETKNOWN,

    LDPICCP_APPSTATUS_CONNSENT              = 1,
    LDPICCP_APPSTATUS_WAITINGFORCONN        = 2,
    LDPICCP_APPSTATUS_CONNTXNAKD            = 3,
    LDPICCP_APPSTATUS_CONNRXNAKD            = 4,
    LDPICCP_APPSTATUS_CONNECTED             = 5,
    LDPICCP_APPSTATUS_DISCONNECTED          = 6,
    LDPICCP_APPSTATUS_APPWITHDRAWN          = 7,
    LDPICCP_APPSTATUS_NOTYETKNOWN           = 8,
    LDPICCP_APPSTATUS_SESSIONDOWN           = 9,
    LDPICCP_APPSTATUS_DEFAULT               = LDPICCP_APPSTATUS_NOTYETKNOWN,

    LDPICCP_REQUEST_UNSOLICITED             = 0,

    L2VPNRED_ICCP_MSG_RG_CONNECT        = 0x0001,
    L2VPNRED_ICCP_MSG_RG_NOTIFY         = 0x0008,
    L2VPNRED_ICCP_MSG_RG_DATA           = 0x0010,
    L2VPNRED_ICCP_MSG_RG_DISCONNECT     = 0x0080,

    L2VPNRED_ICCP_MSG_PW_CONNECT        = 0x0002,
    L2VPNRED_ICCP_MSG_PW_DATA           = 0x0020,
    L2VPNRED_ICCP_MSG_PW_DISCONNECT     = 0x0100,

    L2VPNRED_ICCP_MSG_MLACP_CONNECT     = 0x0004,
    L2VPNRED_ICCP_MSG_MLACP_DATA        = 0x0040,
    L2VPNRED_ICCP_MSG_MLACP_DISCONNECT  = 0x0200,

    L2VPNRED_ICCP_DATA_NONE             = 0x00,
    L2VPNRED_ICCP_DATA_CONFIG           = 0x01,
    L2VPNRED_ICCP_DATA_STATE            = 0x02,
    L2VPNRED_ICCP_DATA_SYNCHRONIZED     = 0x04,
    L2VPNRED_ICCP_DATA_PURGED           = 0x08,

    L2VPNRED_ICCP_REQ_NONE              = 0x00,
    L2VPNRED_ICCP_REQ_CONFIG            = 0x01,
    L2VPNRED_ICCP_REQ_STATE             = 0x02,
    L2VPNRED_ICCP_REQ_SERVICE_NAME      = 0x04,
    L2VPNRED_ICCP_REQ_ALL               = 0x08,

    L2VPNRED_ICCP_REQ_PW_CONFIG         = 0x8000,
    L2VPNRED_ICCP_REQ_PW_STATE          = 0x4000,
    L2VPNRED_ICCP_REQ_PW_SERVICE_NAME   = 0x0001,
    L2VPNRED_ICCP_REQ_PW_TYPE_MASK      = 0x3FFF,

    L2VPNRED_ICCP_REQ_MLACP_CONFIG      = 0x8000,
    L2VPNRED_ICCP_REQ_MLACP_STATE       = 0x4000,
    L2VPNRED_ICCP_REQ_MLACP_SYSTEM      = 0x0000,
    L2VPNRED_ICCP_REQ_MLACP_LAGG        = 0x0001,
    L2VPNRED_ICCP_REQ_MLACP_PORT        = 0x0002,
    L2VPNRED_ICCP_REQ_MLACP_TYPE_MASK   = 0x3FFF,

    L2VPN_PWVC_AGI1_LEN                 = 8,
    L2VPN_PWVC_AII1_LEN                 = 4,
    L2VPN_PWVC_AII2_LEN                 = 12,
    L2VPN_PWVC_ICCP_NAME_MAX_LEN        = 40,
};

typedef struct _L2vpnIccpRoId
{
    UINT1               au1NodeId[4];
    UINT1               au1PwIndex[4];
}tL2vpnIccpRoId;

typedef CHR1    tL2vpnIccpServiceName[L2VPN_PWVC_ICCP_NAME_MAX_LEN + 1];

typedef struct _L2vpnPwAgi
{
    UINT1               u1Type;
    union
    {
        UINT1           au1Agi1[L2VPN_PWVC_AGI1_LEN];
    } u;
    UINT1  au1Pad [3];
}tL2vpnPwAgi;

typedef struct _L2vpnPwAii
{
    UINT1               u1Type;
    union
    {
        UINT1           au1Aii1[L2VPN_PWVC_AII1_LEN];
        UINT1           au1Aii2[L2VPN_PWVC_AII2_LEN];
    } u;
    UINT1  au1Pad [3];
}tL2vpnPwAii;

typedef struct _L2vpnPwFec
{
    UINT1               u1Type;
    union
    {
        struct
        {
            UINT1       au1PeerId[4];
            UINT1       au1GroupId[4];
            UINT1       au1PwId[4];
        } Fec128;

        struct
        {
            tL2vpnPwAgi Agi;
            tL2vpnPwAii SAii;
            tL2vpnPwAii TAii;
        } Fec129;
    } u;
    UINT1  au1Pad [3];
}tL2vpnPwFec;

/**
 * PW Redundancy ICCP PW
 */
typedef struct _L2vpnIccpPwEntry
{
    tRBNodeEmbd             RbNode;
    tRBNodeEmbd             RgNode;
    tL2vpnPwFec             Fec;
    tL2vpnIccpRoId          RoId;
    tMplsRouterId           RouterId;
    VOID                   *pRgParent;  /* tL2vpnRedundancyPwEntry/tL2vpnRedundancyNodeEntry */
    UINT4                   u4RgIndex;
    UINT4                   u4LocalStatus;
    UINT4                   u4RemoteStatus;
    UINT2                   u2Priority;
    UINT2                   u2Status;
    tL2vpnIccpServiceName   Name;
    BOOL1                   b1IsLocal;  /* INTERNAL/EXTERNAL */
    UINT1                   au1Pad [2];
}tL2vpnIccpPwEntry;


typedef struct tPwRedDataEvtPwFec
{
    tTMO_SLL_NODE       ListNode;
    union {
        tL2vpnPwFec             Fec;
        tL2vpnIccpServiceName   Name;
    } u;
}tPwRedDataEvtPwFec;

typedef struct _PwRedDataEvtPwData
{
    tTMO_SLL_NODE       ListNode;
    tL2vpnIccpPwEntry   Data;
    UINT1               u1Type;         /* CONFIG + STATE + PURGE ... */
    UINT1               au1Pad [3];
}tPwRedDataEvtPwData;

typedef struct _PwRedDataEvtArgs
{
    tTMO_SLL            RequestList;
    tTMO_SLL            DataList;
    UINT2               u2RequestNum;
    UINT2               u2ResponseNum;
    UINT1               u1RequestType;  /* CONFIG + STATE + PW + ALL ... */
    UINT1               au1Pad [3];
}tPwRedDataEvtArgs;

typedef struct _L2VpnLdpIccpEvtInfo
{
    uGenAddr        PeerAddr;
    UINT4           u4MessageId;
    UINT4           u4GroupId;
    UINT4           u4LocalLdpEntityId;
    UINT4           u4LocalLdpEntityIndex;
    UINT4           u4PeerLdpId;
    UINT2           u2MsgType;
    UINT1           u1AddrType;
    UINT1           au1Pad[1];

    union
    {
        tPwRedDataEvtArgs       PwData;
        /* Add Other ICCCP messages here */
    } u;
}tL2VpnLdpIccpEvtInfo;

/*      PW Redundancy       */

typedef struct _L2VpnLdpPwVcEvtInfo
{
    UINT4            u4EvtType;
    union
    {
        tPwVcSsnEvtInfo    SsnInfo;                 
        tPwVcLblMsgInfo    LblInfo;
        tPwVcNotifEvtInfo   NotifInfo;
        tL2VpnLdpIccpEvtInfo    IccpInfo;
    }
    unEvtInfo;
}
tL2VpnLdpPwVcEvtInfo;
typedef struct _L2vpLabelArgs
{
    uGenU4Addr          PeerAddr;
    uGenU4Addr          Prefix;
    UINT4               u4InLabel;
    UINT4               u4InIfIndex;
    UINT4               u4TnlXcIndex;
    UINT4               u4BkpTnlXcIndex1;
    UINT4               u4BkpTnlXcIndex2;
    UINT4               u4EntityId;
    UINT2               u2AddrType;
    BOOL1               bIsLspOnTgtSsn;
    UINT1               au1Pad;
}
tL2vpnLabelArgs;

/* Macro's for the tPwVcLblMsgInfo structure i,e LABEL msgs related */
#ifdef MPLS_IPV6_WANTED

#define LDP_L2VPN_EVT_LBL_PEER_IPV6(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.PeerAddr.Ip6Addr.u1_addr)

#endif

#define LDP_L2VPN_EVT_LBL_PEER_IPV4(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.PeerAddr.u4Addr)

#define LDP_L2VPN_EVT_LBL_PEER(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.PeerAddr)


#define LDP_L2VPN_EVT_LBL_ADDR_TYPE(pL2VpnLdpPwVcEvtInfo)\
((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u2AddrType)


#define LDP_L2VPN_EVT_LBL_STALE_LBLVAL(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u4StaleLabel)

#define LDP_L2VPN_EVT_LBL_LBLVAL(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u4Label)

#define LDP_L2VPN_EVT_LBL_LCL_ENTID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u4LocalLdpEntityID)

#define LDP_L2VPN_EVT_LBL_LCL_ENT_INDEX(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u4LdpEntityIndex)

#define LDP_L2VPN_EVT_LBL_PEER_ENTITYID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u4PeerLdpId)

#define LDP_L2VPN_EVT_LBL_LCL_ROLE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.b1IsActive)

#define LDP_L2VPN_EVT_LBL_GRP_ID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u4GroupID)

#define LDP_L2VPN_EVT_LBL_PWVC_ID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u4PwVcID)

#define LDP_L2VPN_EVT_LBL_IF_MTU(pL2VpnLdpPwVcEvtInfo)\
  ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u2IfMtu) 

#define LDP_L2VPN_EVT_LBL_CNTRL_WORD(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.i1ControlWord)

#define LDP_L2VPN_EVT_LBL_PW_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.i1PwType)

#define LDP_L2VPN_EVT_LBL_LCL_ADVERT(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1LocalCapabAdvert)

#define LDP_L2VPN_EVT_LBL_LCL_CC_ADVERT(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1LocalCCAdvert)

#define LDP_L2VPN_EVT_LBL_LCL_CV_ADVERT(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1LocalCVAdvert)

#define LDP_L2VPN_EVT_LBL_MSG_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1MsgType)

#define LDP_L2VPN_EVT_LBL_PW_MPLS_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1MplsType)

#define LDP_L2VPN_EVT_LBL_STAT_CODE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.i1StatusCode)

#define LDP_L2VPN_EVT_LBL_LCL_IFSTR(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.pi1LocalIfString)

#define LDP_L2VPN_EVT_LBL_REQ_VLANID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u2ReqVlanId)

#define LDP_L2VPN_EVT_LBL_REQ_MSGID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u4LblReqId)

#define LDP_L2VPN_EVT_LBL_PW_STATUS_CODE(pL2VpnLdpPwVcEvtInfo)\
    ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u4PwStatusCode)

#define LDP_L2VPN_EVT_LBL_AGI(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.au1Agi)

#define LDP_L2VPN_EVT_LBL_AGI_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1AgiType)

#define LDP_L2VPN_EVT_LBL_SAII_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1SaiiType)

#define LDP_L2VPN_EVT_LBL_TAII_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1TaiiType)

#define LDP_L2VPN_EVT_LBL_AGI_LEN(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1AgiLen)

#define LDP_L2VPN_EVT_LBL_SAII(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.au1Saii)

#define LDP_L2VPN_EVT_LBL_SAII_LEN(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1SaiiLen)

#define LDP_L2VPN_EVT_LBL_TAII(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.au1Taii)

#define LDP_L2VPN_EVT_LBL_TAII_LEN(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1TaiiLen)

#define LDP_L2VPN_EVT_LBL_PW_OWNER(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.i1PwOwner)

#define LDP_L2VPN_EVT_LBL_RMT_STATUS_CAPABLE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo.u1RmtStatusCapable)

   /* tPwVcLblMsgInfo Macro finishes */

   /* Macro's related to tPwVcSsnEvtInfo i,e SESSION related */

   /* Accessing from tl2VpnLdpPwVcEvtInfo */
#define LDP_L2VPN_EVT_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->u4EvtType)

#define LDP_L2VPN_EINFO_SSNINFO(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.LblInfo)

#define LDP_L2VPN_EINFO_LBLINFO(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo)

#define LDP_L2VPN_EINFO_NOTIFINFO(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.SsnInfo)
#ifdef MPLS_IPV6_WANTED

#define LDP_L2VPN_EVT_SSN_PEER_ADDR_IPV6(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.SsnInfo.PeerAddr.Ip6Addr.u1_addr)
#endif

#define LDP_L2VPN_EVT_SSN_PEER_ADDR_IPV4(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.SsnInfo.PeerAddr.u4Addr)


#define LDP_L2VPN_EVT_SSN_PEER_ADDR(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.SsnInfo.PeerAddr)

#define LDP_L2VPN_EVT_SSN_ADDR_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.SsnInfo.u2AddrType)

#define LDP_L2VPN_EVT_SSN_LCL_ENTITYID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.SsnInfo.u4LocalLdpEntityID)

#define LDP_L2VPN_EVT_SSN_PEER_ENTITYID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.SsnInfo.u4PeerLdpId)

#define LDP_L2VPN_EVT_SSN_LCL_ENT_INDEX(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.SsnInfo.u4LdpEntityIndex)

#define LDP_L2VPN_EVT_SSN_RECOVERY_TIME(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.SsnInfo.u2RecoveryTime)

#define LDP_L2VPN_EVT_SSN_LCL_ROLE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.SsnInfo.b1IsActive)
   /* tPwVcSsnEvtInfo macro's finishes */
   /* 's for tPwVcNotifEvtInfo i,e NOTIFICATION related*/
#ifdef MPLS_IPV6_WANTED

#define LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV6(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.PeerAddr.Ip6Addr.u1_addr)

#endif

#define LDP_L2VPN_EVT_NOTIF_PEER_ADDR_IPV4(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.PeerAddr.u4Addr)


#define LDP_L2VPN_EVT_NOTIF_PEER_ADDR(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.PeerAddr)

#define LDP_L2VPN_EVT_NOTIF_ADDR_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u2AddrType)

#define LDP_L2VPN_EVT_NOTIF_ENTITY_ID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u4LocalLdpEntityID)

#define LDP_L2VPN_EVT_NOTIF_ENTITY_INDEX(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u4LdpEntityIndex)


#define LDP_L2VPN_EVT_NOTIF_LABEL(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u4Label)

#define LDP_L2VPN_EVT_NOTIF_VC_ID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u4VcID)

#define LDP_L2VPN_EVT_NOTIF_VC_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u1VcType)

#define LDP_L2VPN_EVT_NOTIF_MSG_CODE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u1MsgCode)

#define LDP_L2VPN_EVT_NOTIF_IF_INDEX(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u4IfIndex)

#define LDP_L2VPN_EVT_NOTIF_AGI(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.au1Agi)

#define LDP_L2VPN_EVT_NOTIF_AGI_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u1AgiType)

#define LDP_L2VPN_EVT_NOTIF_SAII_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u1SaiiType)

#define LDP_L2VPN_EVT_NOTIF_TAII_TYPE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u1TaiiType)

#define LDP_L2VPN_EVT_NOTIF_AGI_LEN(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u1AgiLen)

#define LDP_L2VPN_EVT_NOTIF_SAII(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.au1Saii)

#define LDP_L2VPN_EVT_NOTIF_SAII_LEN(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u1SaiiLen)

#define LDP_L2VPN_EVT_NOTIF_TAII(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.au1Taii)

#define LDP_L2VPN_EVT_NOTIF_TAII_LEN(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u1TaiiLen)

#define LDP_L2VPN_EVT_LBL_NOTIF_OWNER(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.i1PwOwner)

#define LDP_L2VPN_EVT_LBL_NOTIF_PW_STATUS_CODE(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u1PwStatusCode)

#define LDP_L2VPN_EVT_LBL_NOTIF_CW(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.i1ControlWord)

#define LDP_L2VPN_EVT_LBL_NOTIF_GRP_ID(pL2VpnLdpPwVcEvtInfo)\
   ((pL2VpnLdpPwVcEvtInfo)->unEvtInfo.NotifInfo.u4GroupID)
   /* Notif macro's finsihes here */

typedef struct _PwRedTest
{
    UINT4 u4PeerAddr;
    UINT1 u1SimulateFailure;
    UINT1 au1Pad [3];
}tL2vpnPwRedTest;


#endif /*_L2VPEXTN_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file l2vpextn.h                             */
/*---------------------------------------------------------------------------*/
