/********************************************************************
* Copyright (C) 2007 Aricent Inc . All Rights Reserved
*
* $Id: fsmpfrlw.h,v 1.5 2013/12/11 10:07:40 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsFrrDetourIncoming ARG_LIST((INT4 *));

INT1
nmhGetFsMplsFrrDetourOutgoing ARG_LIST((INT4 *));

INT1
nmhGetFsMplsFrrDetourOriginating ARG_LIST((INT4 *));

INT1
nmhGetFsMplsFrrSwitchover ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsFrrConfIfs ARG_LIST((INT4 *));

INT1
nmhGetFsMplsFrrActProtectedIfs ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsFrrConfProtectionTuns ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsFrrActProtectionTuns ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsFrrActProtectedLSPs ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsFrrRevertiveMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsFrrDetourMergingEnabled ARG_LIST((INT4 *));

INT1
nmhGetFsMplsFrrDetourEnabled ARG_LIST((INT4 *));

INT1
nmhGetFsMplsFrrCspfRetryInterval ARG_LIST((INT4 *));

INT1
nmhGetFsMplsFrrCspfRetryCount ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsFrrNotifsEnabled ARG_LIST((INT4 *));

INT1
nmhGetFsMplsFrrMakeAfterBreakEnabled ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsFrrRevertiveMode ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsFrrDetourMergingEnabled ARG_LIST((INT4 ));

INT1
nmhSetFsMplsFrrDetourEnabled ARG_LIST((INT4 ));

INT1
nmhSetFsMplsFrrCspfRetryInterval ARG_LIST((INT4 ));

INT1
nmhSetFsMplsFrrCspfRetryCount ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsFrrNotifsEnabled ARG_LIST((INT4 ));

INT1
nmhSetFsMplsFrrMakeAfterBreakEnabled ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsFrrRevertiveMode ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsFrrDetourMergingEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsFrrDetourEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsFrrCspfRetryInterval ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsFrrCspfRetryCount ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsFrrNotifsEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsFrrMakeAfterBreakEnabled ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsFrrRevertiveMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsFrrDetourMergingEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsFrrDetourEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsFrrCspfRetryInterval ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsFrrCspfRetryCount ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsFrrNotifsEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsFrrMakeAfterBreakEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsFrrConstTable. */
INT1
nmhValidateIndexInstanceFsMplsFrrConstTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsFrrConstTable  */

INT1
nmhGetFirstIndexFsMplsFrrConstTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsFrrConstTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsFrrConstIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsFrrConstProtectionMethod ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsFrrConstProtectionType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsFrrConstSetupPrio ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsFrrConstHoldingPrio ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsFrrConstSEStyle ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsFrrConstInclAnyAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsFrrConstInclAllAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsFrrConstExclAnyAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsFrrConstHopLimit ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsFrrConstBandwidth ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsFrrConstRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsFrrConstProtectionMethod ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsFrrConstProtectionType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsFrrConstSetupPrio ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsFrrConstHoldingPrio ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsFrrConstSEStyle ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsFrrConstInclAnyAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsFrrConstInclAllAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsFrrConstExclAnyAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsFrrConstHopLimit ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsFrrConstBandwidth ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsFrrConstRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsFrrConstProtectionMethod ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsFrrConstProtectionType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsFrrConstSetupPrio ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsFrrConstHoldingPrio ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsFrrConstSEStyle ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsFrrConstInclAnyAffinity ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsFrrConstInclAllAffinity ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsFrrConstExclAnyAffinity ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsFrrConstHopLimit ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsFrrConstBandwidth ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsFrrConstRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsFrrConstTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsTunnelExtTable. */
INT1
nmhValidateIndexInstanceFsMplsTunnelExtTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTunnelExtTable  */

INT1
nmhGetFirstIndexFsMplsTunnelExtTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTunnelExtTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTunnelExtProtIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelExtProtectionType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelExtBkpTunIdx ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelExtBkpInst ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelExtBkpIngrLSRId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelExtBkpEgrLSRId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelExtOne2OnePlrId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTunnelExtOne2OnePlrSenderAddrType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelExtOne2OnePlrSenderAddr ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTunnelExtOne2OnePlrAvoidNAddrType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelExtOne2OnePlrAvoidNAddr ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTunnelExtDetourActive ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelExtDetourMerging ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelExtFacRouteDBProtTunStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelExtFacRouteDBProtTunResvBw ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelExtProtectionMethod ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelExtMaxGblRevertTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTunnelExtMaxGblRevertTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTunnelExtMaxGblRevertTime ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTunnelExtTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsBypassTunnelIfTable. */
INT1
nmhValidateIndexInstanceFsMplsBypassTunnelIfTable ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsBypassTunnelIfTable  */

INT1
nmhGetFirstIndexFsMplsBypassTunnelIfTable ARG_LIST((INT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsBypassTunnelIfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsBypassTunnelRowStatus ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsBypassTunnelRowStatus ARG_LIST((INT4  , UINT4  , UINT4  , UINT4 ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsBypassTunnelRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  , UINT4 ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsBypassTunnelIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsFrrTunARHopTable. */
INT1
nmhValidateIndexInstanceFsMplsFrrTunARHopTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsFrrTunARHopTable  */

INT1
nmhGetFirstIndexFsMplsFrrTunARHopTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsFrrTunARHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsFrrTunARHopProtectType ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsFrrTunARHopProtectTypeInUse ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsFrrTunARHopLabel ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsFrrTunARBwProtAvailable ARG_LIST((UINT4  , UINT4 ,INT4 *));
