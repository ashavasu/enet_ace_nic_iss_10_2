/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: mplsdefs.h,v 1.51 2014/11/08 11:59:52 siva Exp $
 ******************************************************************************
 *    FILE  NAME             : mplsdefs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS-FM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains constants used by MPLS-FM
 *---------------------------------------------------------------------------*/

#ifndef _MPLSDEFS_H
#define _MPLSDEFS_H

#define MPLS_MAX_INCARNS 1        /* Max number of incarnations */
#define MPLS_DEF_INCARN  0


#define CONVERT_TO_INTEGER(x,y) \
{\
    y = (UINT4)((x[0] << 24) | (x[1] << 16) | (x[2] << 8) | x[3]);\
    y = OSIX_HTONL (y);\
}
/*macros used for ipv6*/
#define MPLS_IPV6_U4_ADDR(Genaddr)     Genaddr.Ip6Addr.u1_addr
#define MPLS_IPV4_U4_ADDR(Genaddr)     Genaddr.u4Addr
 
#define MPLS_IPV6_ADDR(Genaddr)     Genaddr.Ip6Addr.u1_addr
#define MPLS_IPV4_ADDR(Genaddr)     Genaddr.au1Ipv4Addr

#define MPLS_P_IPV6_ADDR(pGenaddr)     pGenaddr->Ip6Addr.u1_addr
#define MPLS_P_IPV4_ADDR(pGenaddr)     pGenaddr->au1Ipv4Addr

#define MPLS_P_IPV6_U4_ADDR(pGenaddr)     pGenaddr->Ip6Addr.u1_addr
#define MPLS_P_IPV4_U4_ADDR(pGenaddr)     pGenaddr->u4Addr

/* Macros used indices in the au4IfInfo */
#define MPLS_EVENT       0
#define MPLS_INCARN      1
#define MPLS_APP_INFO    2
#define MPLS_BUF         3

/* Major events */
#define MPLS_IP_GRP_RCV_EVENT         0x0001
#define MPLS_LL_ATM_GRP_RCV_EVENT     0x0002
#define MPLS_LL_ENET_GRP_RCV_EVENT    0x0003
#define MPLS_MLIB_GRP_EVENT           0x0004
#define MPLS_SNMP_GRP_EVENT           0x0005
#define MPLS_ARP_EVENT                0x0006
#define MPLS_MAJOR_EVENT_MASK         0x00ff

#define MPLS_ARP_RESOLVE_WAITING           0x00000001
#define MPLS_ARP_RESOLVED                  0x00000002
#define MPLS_ARP_RESOLVE_FAILED            0x00000003
#define MPLS_ARP_RESOLVE_UNKNOWN           0x00000004

/* 
 * Minor events 
 * Least significant byte specifies Major event 
 */
#define LDP_TSK_NAME                   "LDPT"

/* IP Group event macros */
#define MPLS_IP_PKT_RCV_EVENT               0x0101

/* ATM Group event macros */
#define MPLS_LL_ATM_PKT_RCV_EVENT           0x0102

/* Ethernet Group event macros */
#define MPLS_LL_ENET_PKT_RCV_EVENT          0x0103

/* MLIB Operations  */
#define MPLS_MLIB_ILM_CREATE                0x0104
#define MPLS_MLIB_ILM_DELETE                0x0204
#define MPLS_MLIB_ILM_MODIFY                0x0304
#define MPLS_MLIB_FTN_CREATE                0x0404
#define MPLS_MLIB_FTN_DELETE                0x0504
#define MPLS_MLIB_FTN_MODIFY                0x0604 
#define MPLS_MLIB_TNL_CREATE                0x0704
#define MPLS_MLIB_TNL_MODIFY                0x0804
#define MPLS_MLIB_TNL_DELETE                0x0904

/* SNMP Group event macros */
#define MPLS_SNMP_ADMIN_STATUS_UP_EVENT     0x0105
#define MPLS_SNMP_ADMIN_STATUS_DOWN_EVENT   0x0205

/* MPLS-FM Queue event macros */
#define MPLS_IN_MSG_Q_EVENT                 0x00000001
#define MPLS_ROUTE_CHG_NOTIF                0x00000002
#define MPLS_TNL_FRR_NOTIF                  0x00000004
#define MPLS_RM_EVENT                       0x00000008
#define MPLS_TIMER_EVENT                    0x00000010
#define MPLS_INTF_CHG_NOTIF                 0x00000020
#define MPLS_DEF_IN_Q_DEPTH   32
#define MPLS_MAX_IN_Q_DEPTH   5000
#define MPLS_IN_Q_DEPTH       gu4MplsQueDepth
#define MPLS_Q_NAME_LEN       5
#define MPLS_TASK_NAME_LEN    5
#define MPLS_Q_NAME           "MPFM"
#define MPLS_FRR_Q_NAME       "MPFR"
#define MPLS_ROUTE_CHG_Q_NAME "MPRT"
#define MPLS_IF_CHG_Q_NAME    "MPIF"
#define MPLS_FM_TASK_NAME     "MFWD"

#define MPLS_DEF_BUF_OFF_SET  MPLS_ZERO
#define MPLS_DEF_BUF_SIZE     16

/* MPLS FM Semaphore related macros*/
#define MFM_CONFIG_SEM        ((const UINT1 *)"CONF")
#define MFM_DS_SEM            ((const UINT1 *)"DS")
#define MFM_TASK_NAME         ((const UINT1 *)"MFWD")
#define MFM_SEM_COUNT         1
#define MFM_SEM_MODE          OSIX_DEFAULT_SEM_MODE

/* FM-Tnl Support macros */
#define MPLS_DBASE_TYPE       5    /* Specifies the type, for creating a new
                                   TrieInstance for Ftn and ILM Dbases. */
#define MPLS_HASH_SIZE        128
#define MPLS_UINT1_SIZE       1
#define MPLS_IP_TOS_OFFSET    1
#define MPLS_TOS_DTR_MASK     0x1c    /* Mask for extracting D, T & R bits from
                                       ServiceType field present in IP Hdr. */
#define MPLS_HOST_ADDR_NETMASK 0xffffffff
#define MPLS_ALL_APPIDS       -1
#define MPLS_INITIAL_TNL_INSTANCE  0

/* Qos Related */
#define MPLS_MAX_QOS_POLICIES              3
#define MPLS_STD_IP                        MPLS_QOS_STD_IP    /* Qos Policy One   */
#define MPLS_RFC1349                       MPLS_QOS_RFC_1349    /* Qos Policy Two   */
#define MPLS_DIFF_SERV                     MPLS_QOS_DIFFSERV    /* Qos Policy Three */

#define MPLS_RSVP_PROTID                   46
#define MPLS_HL_PROTID_OFFSET              9  /* Offset in IP Pkt for gettin the
                                                 HigherLayer(HL) Protocol Id */
#define MPLS_FEC_NETWORK_PREFIX_TYPE 0x02
#define MPLS_FEC_HOST_ADDR_TYPE 0x03
#define MPLS_FEC_CRLSP_TYPE     0x04    /* Map it on to LDP value */
#define MPLS_FEC_RSVP_TYPE      0x05
#define MPLS_FEC_SNMP_TNL_TYPE  0x06


/* FEC Classification types */
#define MPLS_FEC_DEST_ADDR        0x01    /* Destination address Prefix type */
#define MPLS_FEC_SRC_ADDR         0x02    /* Source address type             */
#define MPLS_FEC_CR_LDP_ADDR_LIST 0x04    /* CR-LDP tunnel address list type */
#define MPLS_FEC_APPL_FLOW        0x08    /* Application flow related type   */

/* Protocol Ids defined in the RFC 1700 */
#define MPLS_IPV4_PROT_ID   0x0800
#define MPLS_TCP_PROT_ID     6
#define MPLS_UDP_PROT_ID    17
#define MPLS_IP_OVER_ATM_PPP    33

#define MPLS_PROT_ID        0x0900
#define MPLS_UCAST_ETH      0x8847
#define MPLS_MCAST_ETH      0x8848
#define MPLS_IP_PRTCL_ID    0x0800
#define MPLS_ARP_PRTCL_ID   0x0806

/* Operation to be performed on the Label Stack */

/* u1Operation can be only MPLS_OPR_PUSH, MPLS_OPR_POP or 
 * MPLS_OPR_POP_PUSH.  Other values are not used.  So can be removed 
 * Values of existing constants are changed here */
#define MPLS_OPR_PUSH          1
#define MPLS_OPR_POP           2 
#define MPLS_OPR_POP_PUSH      3 

/* Defualt Label values */
#define MPLS_NULL_LABEL                0
#define MPLS_PHP_DISABLED              1
#define MPLS_ETH_INVALID_LABEL_MASK    0xfff00000

/* Route Alert Label/Option Related Values */
#define MPLS_ROUTE_ARERT_OP_PRESENT    1
#define MPLS_ROUTE_ARERT_OP_NOTPRESENT 2
#define MPLS_IP_HDR_OFFSET             20
#define MPLS_IP_HDR_LEN                20
#define MPLS_ROUTE_ALERT_OPTION        20
#define MPLS_ROUTE_ALERT_OPTION_MASK   0x1f

#ifdef MPLS_IPV6_WANTED
#define MPLS_IPV6_HDR_LEN                40
#endif

#define MPLS_IS_RESERVED_LABEL(u4Label) \
            (u4Label == MPLS_ROUTER_ALERT_LABEL) || \
            (u4Label == MPLS_GAL_LABEL)

/* ATM related macros */
#define MPLS_DEFAULT_VPI_IP       0    /* Default VPI to send packets to IP */
#define MPLS_DEFAULT_VCI_IP      32    /* Default VCI to send packets to IP */
#define MPLS_IPV4_DEF_ATM_LABEL  32
#define MPLS_ATM_INVALID_LABEL_MASK  0xf0000000

#define MPLS_LABEL_LEN             32
#define MPLS_LABEL_OCTET_LEN        4
#define MPLS_IP_ADDR_LEN           32
#define MPLS_IP_ADDR_OCTETS         4
#define MPLS_IPMASK_OCTETS          4
#define MPLS_IP_SRC_ADDR_OFFSET    12
#define MPLS_IP_DEST_ADDR_OFFSET   16
#define MPLS_TTL_OFFSET_IN_IP       8
#define MPLS_TTL_OFFSET_IN_LABEL    3
#define MPLS_TTL_LEN                1
#define MPLS_TTL_NULL               0
#define MPLS_TTL_MIN_VAL            0
#define MPLS_TTL_MAX_VAL            255
#define MPLS_CHKSUM_LEN             2
#define MPLS_CHKSUM_OFFSET_IN_IP    10
#define MPLS_MAC_HDR_LEN            14
#define MPLS_ETH_CKSUM_LEN          4
#define MPLS_ETH_ADDR_LEN           6
#define MPLS_TRIE_KEY_OCTETS       MPLS_LABEL_OCTET_LEN + MPLS_IF_INDEX_LEN
#define MPLS_TRIE_KEY_SIZE         MPLS_LABEL_LEN + MPLS_IF_INDEX_BIT_LEN

#define MPLS_IP_BROADCAST_MASK   0xffffffff
#define MPLS_IP_MULTICAST_MASK   0xe0000000
#define MPLS_4TH_BYTE_MASK   0xff000000
#define MPLS_3RD_BYTE_MASK   0x00ff0000
#define MPLS_2ND_BYTE_MASK   0x0000ff00
#define MPLS_1ST_BYTE_MASK   0x000000ff
#define MPLS_3_BYTE_SHIFT    24
#define MPLS_2_BYTE_SHIFT    16
#define MPLS_1_BYTE_SHIFT     8
#define MPLS_LOWER_NIBLE_MASK 0x0f

#define MPLS_IP_NULL_ADDR        0x00000000
#define MPLS_IP_BROADCAST_ADDR   0xffffffff
#define MPLS_IP_LOOPBACK_ADDR    0x7f000001
#define MPLS_IP_MCAST_START_ADDR 0xe0000000
#define MPLS_IP_MCAST_END_ADDR   0xe00000ff

 /* Constants defining the type of Ethernet PAcket */
#define MPLS_LL_ETH_TYPE    0x06
#define MPLS_LINK_UCAST      1
#define MPLS_LINK_MCAST      2
#define MPLS_LINK_BCAST      3
#define MPLS_LL_VLAN_TYPE    0x14 

/*  MEM pool related macros */
#define MPLS_INVALID_POOL_ID 0xffffffff
#define MPLS_INVALID_Q_ID    0xffffffff

/* Value to be decrement in the TTL field */
#define MPLS_ENET_INGRESS_TTL_DECR_VALUE  0
#define MPLS_ENET_EGRESS_TTL_DECR_VALUE   0
#define MPLS_ENET_INTER_TTL_DECR_VALUE    1

/* Lower Layer Interface related */
#define MPLS_INVALID_IFTYPE     255
#define MPLS_LL_ENET_INTERFACE  1
#define MPLS_LL_ATM_INTERFACE   2
#define MPLS_LL_FR_INTERFACE    3
#define MPLS_LL_VLAN_INTERFACE  4

/* SNMP configuration Related */
#define MPLS_ADMIN_STATUS_UP         1    /* Used configure the Admin status */
#define MPLS_ADMIN_STATUS_DOWN       2
#define MPLS_ADMIN_STATUS_UP_IN_PRGRS   3
#define MPLS_ADMIN_STATUS_DOWN_IN_PRGRS 4
 /* MIB Table Row status */
#define MPLS_STATUS_ACTIVE            1
#define MPLS_STATUS_NOT_INSERVICE     2
#define MPLS_STATUS_NOT_READY         3
#define MPLS_STATUS_CREATE_AND_GO     4
#define MPLS_STATUS_CREATE_AND_WAIT   5
#define MPLS_STATUS_DESTROY           6
#define MPLS_SNMP_TRUE        1
#define MPLS_SNMP_FALSE       2
/* MPLS-FM General macros */
#define MPLS_STATUS_UP         1
#define MPLS_STATUS_DOWN       0
#define MPLS_ENABLE            1    /* To specify the interface is MPLS enabled */
#define MPLS_DISABLE           2    /* To specify the interface is MPLS disabled */

#define MPLS_ZERO  0
#define MPLS_ONE   1
#define MPLS_TWO   2
#define MPLS_THREE   3
#define MPLS_FOUR  4

/* Enqueue Message length */
#define MPLS_ENQ_MSG_LEN          4

#define MPLS_IF_INDEX_LEN         4
#define MPLS_IF_INDEX_BIT_LEN    32

#define MPLS_VCI_MASK         0x0000ffff
#define MPLS_VPI_MASK         0x1fff0000

#define MPLS_LABEL_MASK       0xfffff000
#define MPLS_LABEL_LSN_MASK   0x0000f000
#define MPLS_BOS_MASK         0x00000100    /* Bottom of the stack */
#define MPLS_NOT_BOS          0
#define MPLS_V_BITS_MASK      0x3000
#define MPLS_VP_BITS_MASK     0x1000
#define MPLS_ATMX_OPR_ADD   1
#define MPLS_ATMX_OPR_MDFY  2
#define MPLS_ATMX_OPR_DEL   3


#define MPLS_ATMX_MERGE     1
#define MPLS_ATMX_NONMERGE  2
/* Mask Prefix Length */
#define MPLS_ADDR_PREFIX_LEN_8  8
#define MPLS_ADDR_PREFIX_LEN_16 16
#define MPLS_ADDR_PREFIX_LEN_24 24
#define MPLS_ADDR_PREFIX_LEN_32 32
/* Default Min Label Value */
#define MPLS_LABEL_MIN_VAL      16
/* Flags for Configuration check of FTN Table*/
#define MPLS_FTN_TABLE_FEC_TYPE_FLAG         0x00000001
#define MPLS_FTN_TABLE_TNL_ID_FLAG           0x00000002
#define MPLS_FTN_TABLE_TNL_INSTANCE_FLAG     0x00000004
#define MPLS_FTN_TABLE_NHLFE_INFO_INDEX_FLAG 0x00000008
#define MPLS_FTN_TABLE_TNL_MAND_FLAG         0x00000006
/* Flags for Configuration check of FTN Table */
#define MPLS_ILM_TABLE_NHLFE_INFO_INDEX_FLAG 0x00000001
/* Flags for Configuration check of TNL Table */
#define MPLS_TNL_TABLE_INGRESS_ID_FLAG       0x00000001
#define MPLS_TNL_TABLE_NHLFE_INFO_INDEX_FLAG 0x00000002
#define MPLS_TNL_TABLE_MAND_FLAG             0x00000003
/* Flags for Configuration check of NHLFE Table */
#define MPLS_NHLFE_TABLE_LABEL_STACK_OPER_FLAG      0x00000001
#define MPLS_NHLFE_TABLE_OUT_LABEL_FLAG             0x00000002
#define MPLS_NHLFE_TABLE_OUT_IF_INDEX_FLAG          0x00000004
#define MPLS_NHLFE_TABLE_NEXT_HOP_IPV4_ADDR_FLAG    0x00000008
#define MPLS_NHLFE_TABLE_NUM_DEC_TTL_FLAG           0x00000010
#define MPLS_NHLFE_TABLE_STACK_TNL_ID_FLAG          0x00000020
#define MPLS_NHLFE_TABLE_STACK_TNL_INSTANCE_FLAG    0x00000040
#define MPLS_NHLFE_TABLE_DIFFSERV_PARAMS_INDEX_FLAG 0x00000080
#define MPLS_NHLFE_TABLE_MAND_FLAG                  0x0000001f
#define MPLS_NHLFE_TABLE_STACK_TNL_FLAG             0x00000060

#define MPLS_MAX_UINT1                    0xff
#define MPLS_MAX_UINT2                    0xffff
#define MPLS_MAX_UINT4                    0xffffffff

/* Default Max entries */
#define MPLS_MAX_HLS     1        /* Maximum higher layers per incarnation */

#define MPLS_NEXT_HOPS_PER_IF   4
#define MPLS_Q_DEPTH            8

#define MPLS_DEF_CHAN_RATE                 5000
#define MPLS_DEF_LABELS_PER_IF            MPLS_LABELS_PER_IF
#define MPLS_DEF_NEXT_HOPS_PER_IF         MPLS_NEXT_HOPS_PER_IF
#define MPLS_DEF_VC_TABLE_ENTRIES       MAX_LANAI_VC_TABLE_ENTRIES     
/* FM-Tnl Support macros */
#define MPLS_MAX_TNLS                     64000 /* Ensure this is less than
                                                   MAX UINT2 */
                                             /* Max Allowed tnls per incarn */
#define MPLS_MAX_TNL_INSTANCES             8 /* Max Allowed instances per tnl */
#define MPLS_MAX_FTN_PER_TNL               16
#define MPLS_MAX_TOS_VAL                   255    /* TODO Give the max value among 
                                                   RFC 1349, StdIp and DiffServ.
                                                   Diff TosValues are -
                                                   0x0, 0x1, 0x2, 0x4 and 0x8 */
#define MPLS_DIFF_TOS_VALS                 16    /* Specifies the no of Tos values,
                                                   supported as of now. This is 
                                                   limited to the Max Number of Apps
                                                   supported by FutureTrie. */

#define MPLS_MIN_DELAY                  0x8
#define MPLS_MAX_THROUGHPUT             0x4
#define MPLS_MAX_RELIABILITY            0x2
#define MPLS_MIN_MONETARYCOST           0x1
#define MPLS_BEST_OF_SERVICE            0    /* One of the Tos values. This 
                                               provides Normal(BestOf) Service */
#define MPLS_INVALID_TRIE_APPID           -1
#define MPLS_INVALID_TNL_ID               (0)

#define MPLS_DEF_IP_INFO_ENTRIES            MPLS_Q_DEPTH
#define MPLS_DEF_ATM_INFO_ENTRIES           MPLS_Q_DEPTH
#define MPLS_DEF_LSP_INFO_ENTRIES           MPLS_Q_DEPTH

#define MPLS_DEF_LABEL_STACK_ENTRIES         8

/*  MPLS Trap values */
#define MPLS_INTERFACES_INIT_FAILURE         1
#define MPLS_INTERFACES_CLOSE_FAILURE        2
#define MPLS_TRIE_INIT_FAILURE               3
#define MPLS_TRIE_SHUTDOWN_FAILURE           4
#define MPLS_CREATE_MEM_POOLS_FAILURE        5
#define MPLS_ILM_INIT_FAILURE                6
#define MPLS_FTN_INIT_FAILURE                7
#define MPLS_MAC_ADDR_TABLE_INIT_FAILURE     8
/* FM-Tnl Support macro */
#define MPLS_TNL_INIT_FAILURE                9
#define MPLS_NHLFE_INFO_TABLE_INIT_FAILURE  10
#define MPLS_DIFFSERV_TABLE_INIT_FAILURE    11
#define MPLS_INVALID_NHLFE_INFO_INDEX       (-1)
#define MPLS_INVALID_DIFFSERV_PARAMS_INDEX  (-1)

#define MPLS_IPV4                  1

/* Atm related definitions */
/* ----------------------- */

/* Direction of data flow in Atm */

#define MPLS_BIDIREC               CFA_BIDIRECTIONAL
#define MPLS_UNIDIREC_SEND         CFA_UNIDIREC_SEND
#define MPLS_UNIDIREC_RECEIVE      CFA_UNIDIREC_RECEIVE

/* Type of VC connection */

#define MPLS_PVC                   1 
#define MPLS_SVC                   2

/* Flag Values for Atm encapsulation */

#define MPLS_NULL_ENCAPS           CFA_ENCAP_VC_MUX
#define MPLS_LLC_ENCAPS            CFA_ENCAP_LLC_SNAP

/* Traffic Type */

#define MPLS_CBR                   CFA_CBR   /* ATM Constant Bit Rate */
#define MPLS_UBR                   CFA_UBR   /* ATM Unspecified Bit Rate */
#define MPLS_ABR                   CFA_ABR   /* ATM Available Bit Rate */
#define MPLS_VBR                   CFA_VBR   /* ATM Variable Bit Rate */
#define MPLS_MAC_ADDR_LEN          CFA_ENET_ADDR_LEN

#define MPLS_ISSNMP      TRUE
#define MPLS_NOTSNMP     FALSE

#ifdef ISS_WANTED
#define MPLS_INIT_COMPLETE(u4Status)  lrInitComplete(u4Status)
#else
#define MPLS_INIT_COMPLETE(u4Status)
#endif

#define MPLS_DEF_TTL     255

#define MPLS_ARP_RESOLVE_TIMEOUT  2
#define  MPLS_OFFSET(x,y)        FSAP_OFFSETOF(x,y)

/* Sub module Id's */
#define MPLS_OWNER_RSVP        1
#define MPLS_OWNER_ROUTER      2

#define MPLS_OBJECT_NAME_LEN   256

enum
{
    MPLS_FTN_ARP_TIMER       = 1,  /* FTN Arp Timer */
    MPLS_ILM_ARP_TIMER       = 2,  /* ILM Arp Timer */
    MPLS_TNL_ARP_TIMER       = 3,  /* Tunnel Arp Timer */
    MPLS_PW_ARP_TIMER        = 4,  /* PW Arp Timer */
    MPLS_TNL_RETRIG_TIMER    = 5,  /* Tunnel Retrigger Timer */
    MPLS_RPTE_MAX_WAIT_TIMER = 6,  /* RSVPTE GR Max Wait Timer */
#ifdef MPLS_L3VPN_WANTED
    MPLS_L3VPN_ARP_TIMER     = 7,  /* L3VPN Egress Map Arp Timer */
    MPLS_MAX_TIMERS          = 8
#else
    MPLS_MAX_TIMERS          = 7
#endif
};

#define RPTE_SIM_FAILURE_BW_RESV          1
#define RPTE_SIM_FAILURE_CT_ZERO          2
#define RPTE_SIM_FAILURE_NO_LBL_REQ       3
#define RPTE_SIM_FAILURE_NO_SESSION       4
#define RPTE_SIM_FAILURE_UNKNOWN_CNUM     5
#define RPTE_SIM_FAILURE_UNKNOWN_CTYPE    6
#define RPTE_SIM_FAILURE_MULTI_CT_OBJ     7
#define RPTE_SIM_FAILURE_LSP_TNL_IFID_CTYPE_1 8
#define RPTE_SIM_FAILURE_NO_RESTART_CAP_OBJ   9
#define RPTE_SIM_FAILURE_NO_ILM               10
#define RPTE_SIM_FAILURE_NO_SUGGESTED_LBL_OBJ 11
#define RPTE_SIM_FAILURE_NO_SUGGESTED_LBL  12
#define RPTE_SIM_FIALURE_NO_RBIT           13
#define RPTE_SIM_FAILURE_NO_ACK            14
#define RPTE_SIM_FAILURE_NACK              15
#define RPTE_SIM_FAILURE_RESTART_ZERO      16
#define RPTE_SIM_FAILURE_RECOVERY_ZERO     17
#define LDP_SIM_FAILURE_MPLS_FWD_STATE_LOST  18
#define LDP_SIM_FAILURE_SEND_NEW_LABEL       19
#define LDP_SIM_FAILURE_DONT_SEND_LBL_MAP    20
#define LDP_SIM_FAILURE_LDP_QUEUE_FULL       21
#define LDP_SIM_FAILURE_CRLSP_FEC_NOT_SUPPORTED 22
#define LDP_SIM_FAILURE_ON_DEMAND_NOT_SUPPORTED 23
#define LDP_SIM_FAILURE_UNSOLICIT_NOT_SUPPORTED 24
#define LDP_SIM_FAILURE_PDU_LENGTH_500          25
#define LDP_SIM_FAILURE_LBL_MERGE_CAPABLE       26
#define LDP_SIM_FAILURE_LBL_REQ_MERGE           27
#define LDP_SIM_FAILURE_WILDCARD_FEC            28
#define LDP_SIM_FAILURE_PROPAGATE_LBL_RELEASE   29
#define LDP_SIM_FAILURE_PROPAGATE_LBL_ABORT_REQ 30
#define LDP_SIM_FAILURE_DISABLE_PROXY_EGRESS    31
#define LDP_SIM_FAILURE_GEN_FEC_NOT_SUPPORTED   32
#define LDP_SIM_FAILURE_WRONG_C_BIT_VAL_RFC_4906 33
#define LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC     34
#define L2VPN_SIM_FAILURE_CTRLW_MANDATORY      35
#define LDP_SIM_FAILURE_SAME_PREFIX_AND_NETWORK   36
#define L2VPN_SIM_FAILURE_WDRAW_METHOD_NOT_SUPPORT 37
#define LDP_SIM_FAILURE_HOLD_TRANSACTION           38


#define MPLS_DEF_VRF   0


/* GR Related Macros */
#define MPLS_MIN_GR_MAX_WAIT_TIME 30
#define MPLS_MAX_GR_MAX_WAIT_TIME 1200
#define MPLS_DEF_GR_MAX_WAIT_TIME 600

#define MPLS_RPTE_TNL_RETRIGGER_EVENT      1
#define MPLS_RPTE_MAX_WAIT_TMR_EVENT       3

/*The below macro can be used only if destination is an array */
#define MPLS_STRLEN_MIN(d,s) (((sizeof(d) - 1) <= (STRLEN(s))) ? (sizeof(d) - 1) : (STRLEN(s)))

/* Default Ach Channel Types */
#define MPLS_ACH_CHANNEL_DEF_CC_BFD      0x0007
#define MPLS_ACH_CHANNEL_DEF_CV_BFD      0x0008
#define MPLS_ACH_CHANNEL_DEF_CC_IPV4     0x0021
#define MPLS_ACH_CHANNEL_DEF_CV_IPV4     0x0022
#define MPLS_ACH_CHANNEL_DEF_CC_IPV6     0x0057
#define MPLS_ACH_CHANNEL_DEF_CV_IPV6     0x0058

#define MPLS_ACH_CHANNEL_DEF_CC_BFD_NEW  0x0022
#define MPLS_ACH_CHANNEL_DEF_CV_BFD_NEW  0x0023
#define MPLS_ACH_CHANNEL_DEF_CV_IPV4_NEW 0x0056
#define MPLS_ACH_CHANNEL_DEF_CV_IPV6_NEW 0x7ff7

#define MPLS_ONE_BYTE_BITS		8
#define MPLS_ONE_BYTE_MAX_VAL		0xFF


#endif /*_MPLSDEFS_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file mplsdefs.h                             */
/*---------------------------------------------------------------------------*/
