/* $Id: fsmplswr.h,v 1.20 2016/03/04 11:03:21 siva Exp $ */
#ifndef _FSMPLSWR_H
#define _FSMPLSWR_H

VOID RegisterFSMPLRTR (VOID);
VOID UnRegisterFSMPLRTR (VOID);
VOID RegisterFSMPLSRtr (VOID);
VOID UnRegisterFSMPLSRtr (VOID);
VOID RegisterFSMPLSLdpObj (VOID);
VOID RegisterFSMPLSRsvpteObj (VOID);
VOID RegisterFSMPLSRtrObj (VOID);
VOID RegisterFSMPLSL2vpnObj (VOID);
VOID UnRegisterFSMPLSLdpObj (VOID);
VOID UnRegisterFSMPLSRsvpteObj (VOID);
VOID UnRegisterFSMPLSRtrObj (VOID);
VOID UnRegisterFSMPLSL2vpnObj (VOID);
VOID RegisterFSMPLSScalarObj (VOID);
VOID UnRegisterFSMPLSScalarObj (VOID);
VOID AddSysEntry (VOID);
VOID DeleteSysEntry (VOID);

#ifdef MPLS_L3VPN_WANTED
VOID RegisterFSMPLSL3vpnObj (VOID);
VOID UnRegisterFSMPLSL3vpnObj (VOID);
#endif

INT4 FsMplsAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsQosPolicyGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFmDebugLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTeDebugLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrLabelAllocationMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspPreConfExpPhbMapIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsQosPolicySet(tSnmpIndex *, tRetVal *);
INT4 FsMplsFmDebugLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTeDebugLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrLabelAllocationMethodSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspPreConfExpPhbMapIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsQosPolicyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsFmDebugLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTeDebugLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrLabelAllocationMethodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspPreConfExpPhbMapIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsQosPolicyDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsFmDebugLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsTeDebugLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsLsrLabelAllocationMethodDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsDiffServElspPreConfExpPhbMapIndexDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMplsLdpEntityTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsLdpEntityPHPRequestMethodGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityTransAddrTlvEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityTransportAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityLdpOverRsvpEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelInstanceGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelIngressLSRIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelEgressLSRIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelInstanceGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelIngressLSRIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelEgressLSRIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityIpv6TransAddrTlvEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityIpv6TransportAddrKindGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityIpv6TransportAddressGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityBfdStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityPHPRequestMethodSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityTransAddrTlvEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityTransportAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityLdpOverRsvpEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelInstanceSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelIngressLSRIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelEgressLSRIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelInstanceSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelIngressLSRIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelEgressLSRIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityIpv6TransAddrTlvEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityIpv6TransportAddrKindSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityIpv6TransportAddressSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityBfdStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityPHPRequestMethodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityTransAddrTlvEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityTransportAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityLdpOverRsvpEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelInstanceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelIngressLSRIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityOutTunnelEgressLSRIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelInstanceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelIngressLSRIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityInTunnelEgressLSRIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityIpv6TransAddrTlvEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityIpv6TransportAddrKindTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityIpv6TransportAddressTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityBfdStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpEntityTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMplsLdpLsrIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpForceOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsRsvpTeGrMaxWaitTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrMaxWaitTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpLsrIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpForceOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsRsvpTeGrMaxWaitTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrMaxWaitTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpLsrIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpForceOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsRsvpTeGrMaxWaitTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrMaxWaitTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpLsrIdDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsLdpForceOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsRsvpTeGrMaxWaitTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsLdpGrMaxWaitTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsMaxIfTableEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxFTNEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxInSegmentEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxOutSegmentEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxXCEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspMapEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxHopListsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxPathOptPerListGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxHopsPerPathOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxArHopListsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxHopsPerArHopListGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxRsvpTrfcParamsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxCrLdpTrfcParamsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxDServElspsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxDServLlspsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsMaxTnlsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrMaxLdpEntitiesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrMaxLocalPeersGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrMaxRemotePeersGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrMaxIfacesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrMaxLspsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrMaxVcMergeCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrMaxVpMergeCountGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrMaxCrlspTnlsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsActiveRsvpTeTnlsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsActiveLspsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsActiveCrLspsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspDebugLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspDumpTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspDebugLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspDumpTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspDebugLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspDumpTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspDebugLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsCrlspDumpTypeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMplsCrlspTnlTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsCrlspTnlRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspTnlStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspTnlRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspTnlStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspTnlRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspTnlStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspTnlTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMplsCrlspDumpDirectionGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspPersistanceGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspDumpDirectionSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspPersistanceSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspDumpDirectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspPersistanceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsCrlspDumpDirectionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsCrlspPersistanceDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMplsTunnelRSVPResTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsTunnelRSVPResTokenBucketRateGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResTokenBucketSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResPeakDataRateGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResMinimumPolicedUnitGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResMaximumPacketSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResTokenBucketRateSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResTokenBucketSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResPeakDataRateSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResMinimumPolicedUnitSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResMaximumPacketSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResTokenBucketRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResTokenBucketSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResPeakDataRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResMinimumPolicedUnitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResMaximumPacketSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelRSVPResTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsTunnelCRLDPResTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsTunnelCRLDPResPeakDataRateGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResCommittedDataRateGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResPeakBurstSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResCommittedBurstSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResExcessBurstSizeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResPeakDataRateSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResCommittedDataRateSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResPeakBurstSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResCommittedBurstSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResExcessBurstSizeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResPeakDataRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResCommittedDataRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResPeakBurstSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResCommittedBurstSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResExcessBurstSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTunnelCRLDPResTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsDiffServElspMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsDiffServElspMapPhbDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspMapStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspMapPhbDscpSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspMapStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspMapPhbDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspMapStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsDiffServParamsTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsDiffServParamsServiceTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsLlspPscDscpGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsElspTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsElspSigExpPhbMapIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsServiceTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsLlspPscDscpSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsElspTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsElspSigExpPhbMapIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsServiceTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsLlspPscDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsElspTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsElspSigExpPhbMapIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServParamsTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsDiffServTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsDiffServClassTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServServiceTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServLlspPscGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspListIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServClassTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServServiceTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServLlspPscSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspListIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServClassTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServServiceTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServLlspPscTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspListIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsDiffServElspInfoTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsDiffServElspInfoPHBGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoResourcePointerGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoPHBSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoResourcePointerSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoPHBTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoResourcePointerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDiffServElspInfoTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMplsDiffServElspInfoListIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTnlModelGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsRsrcMgmtTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTTLValGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTnlModelSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsRsrcMgmtTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTTLValSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTnlModelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsRsrcMgmtTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTTLValTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTnlModelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsRsrcMgmtTypeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsTTLValDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsDsTeStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMplsDsTeClassTypeTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsDsTeClassTypeDescriptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeClassTypeRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeClassTypeBwPercentageGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeClassTypeDescriptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeClassTypeRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeClassTypeBwPercentageSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeClassTypeDescriptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeClassTypeRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeClassTypeBwPercentageTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeClassTypeTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsDsTeClassTypeToTcMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsDsTeTcTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTcDescriptionGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTcMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTcTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTcDescriptionSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTcMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTcTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTcDescriptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTcMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeClassTypeToTcMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsDsTeTeClassTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsDsTeTeClassDescGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTeClassNumberGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTeClassRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTeClassDescSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTeClassNumberSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTeClassRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTeClassDescTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTeClassNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTeClassRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsDsTeTeClassTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMplsL2VpnTrcFlagGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnCleanupIntervalGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnTrcFlagSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnCleanupIntervalSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnTrcFlagTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnCleanupIntervalTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnTrcFlagDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsL2VpnCleanupIntervalDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsL2VpnAdminStatusDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMplsL2VpnPwTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsL2VpnPwModeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnVplsIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCapabAdvertGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCCSelectedGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCVSelectedGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCCAdvertGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCVAdvertGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwRemoteCCAdvertGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwRemoteCVAdvertGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwOamEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwGenAGITypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwGenLocalAIITypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwGenRemoteAIITypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnProactiveOamSsnIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwAIIFormatGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnIsStaticPwGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnNextFreePwEnetPwInstanceGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwSynchronizationStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwModeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnVplsIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCapabAdvertSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCCAdvertSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCVAdvertSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwRemoteCCAdvertSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwRemoteCVAdvertSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwOamEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwGenAGITypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwGenLocalAIITypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwGenRemoteAIITypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwAIIFormatSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnVplsIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCapabAdvertTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCCAdvertTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwLocalCVAdvertTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwRemoteCCAdvertTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwRemoteCVAdvertTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwOamEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwGenAGITypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwGenLocalAIITypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwGenRemoteAIITypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwAIIFormatTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnPwTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsVplsConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsVplsVsiGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsVpnIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsDescrGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsFdbHighWatermarkGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsFdbLowWatermarkGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsL2MapFdbIdGet(tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsMtuGet(tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsSignalingTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsControlWordGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsVsiSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsVpnIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsNameSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsDescrSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsFdbHighWatermarkSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsFdbLowWatermarkSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsL2MapFdbIdSet(tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsMtuSet(tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsSignalingTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsControlWordSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsVsiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsVpnIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsDescrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsFdbHighWatermarkTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsFdbLowWatermarkTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsL2MapFdbIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsMtuTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsSignalingTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsmplsVplsControlWordTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsPwMplsInboundTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsPwMplsInboundTunnelIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsPwMplsInboundTunnelInstanceGet(tSnmpIndex *, tRetVal *);
INT4 FsPwMplsInboundTunnelEgressLSRGet(tSnmpIndex *, tRetVal *);
INT4 FsPwMplsInboundTunnelIngressLSRGet(tSnmpIndex *, tRetVal *);
INT4 FsPwMplsInboundTunnelIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsPwMplsInboundTunnelEgressLSRSet(tSnmpIndex *, tRetVal *);
INT4 FsPwMplsInboundTunnelIngressLSRSet(tSnmpIndex *, tRetVal *);
INT4 FsPwMplsInboundTunnelIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPwMplsInboundTunnelEgressLSRTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPwMplsInboundTunnelIngressLSRTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsPwMplsInboundTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMplsLocalCCTypesCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLocalCVTypesCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsRouterIDGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsHwCCTypeCapabilitiesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLocalCCTypesCapabilitiesSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLocalCVTypesCapabilitiesSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsRouterIDSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLocalCCTypesCapabilitiesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLocalCVTypesCapabilitiesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsRouterIDTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLocalCCTypesCapabilitiesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsLocalCVTypesCapabilitiesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsRouterIDDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMplsPortTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsPortBundleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPortMultiplexStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPortAllToOneBundleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPortRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPortBundleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPortMultiplexStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPortAllToOneBundleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPortRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsPortBundleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsPortMultiplexStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsPortAllToOneBundleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsPortRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsPortTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsVplsAcMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsVplsAcMapPortIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsAcMapPortVlanGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsAcMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsAcMapPortIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsAcMapPortVlanSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsAcMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsAcMapPortIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsAcMapPortVlanTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsAcMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsVplsAcMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMplsL2VpnMaxPwVcEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcMplsEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcMplsInOutEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxEthernetPwVcsGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcEnetEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnActivePwVcEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnActivePwVcMplsEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnActivePwVcEnetEntriesGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnNoOfPwVcEntriesCreatedGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnNoOfPwVcEntriesDeletedGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcEntriesSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcMplsEntriesSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcMplsInOutEntriesSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxEthernetPwVcsSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcEnetEntriesSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcEntriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcMplsEntriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcMplsInOutEntriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxEthernetPwVcsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcEnetEntriesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL2VpnMaxPwVcEntriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsL2VpnMaxPwVcMplsEntriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsL2VpnMaxPwVcMplsInOutEntriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsL2VpnMaxEthernetPwVcsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsL2VpnMaxPwVcEnetEntriesDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsSimulateFailureGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsiTTLValGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsOTTLValGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsprviTTLValGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsprvOTTLValGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsSimulateFailureSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsSimulateFailureTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsSimulateFailureDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsLdpGrCapabilityGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrForwardEntryHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrMaxRecoveryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrNeighborLivenessTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrProgressStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrCapabilitySet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrForwardEntryHoldTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrMaxRecoveryTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrNeighborLivenessTimeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrCapabilityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrForwardEntryHoldTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrMaxRecoveryTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrNeighborLivenessTimeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpGrCapabilityDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsLdpGrForwardEntryHoldTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsLdpGrMaxRecoveryTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsMplsLdpGrNeighborLivenessTimeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 GetNextIndexFsMplsLdpPeerTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsLdpPeerGrReconnectTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpPeerGrRecoveryTimeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpPeerGrProgressStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpConfigurationSequenceTLVEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpConfigurationSequenceTLVEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpConfigurationSequenceTLVEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLdpConfigurationSequenceTLVEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#ifdef MPLS_L3VPN_WANTED
INT4 GetNextIndexFsMplsL3VpnVrfEgressRteTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsL3VpnVrfNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrDestTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrDestGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrPfxLenGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrLabelPolicyGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrNHopTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrNextHopGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrMetric1Get(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRtePathAttrLabelGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfNameSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrDestTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrDestSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrPfxLenSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrLabelPolicySet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrNHopTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrNextHopSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrMetric1Set(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrDestTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrDestTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrPfxLenTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrLabelPolicyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrNHopTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrNextHopTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrMetric1Test(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteInetCidrStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsL3VpnVrfEgressRteTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif
#endif /* _FSMPLSWR_H */
