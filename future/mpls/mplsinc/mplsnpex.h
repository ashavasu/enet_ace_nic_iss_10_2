
/********************************************************************
 *                                                                  *
 * $RCSfile: mplsnpex.h,v $
 *                                                                  *
 * $Date: 2002/04/19 14:02:12 
 *                                                                  *
 * $Revision: 1.1.1.1 
 *                                                                  *
 *******************************************************************/


/*************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ************************************************************************
 *    FILE  NAME             : mplsnpex.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS NP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Generic Network Processor
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains data structures,functions 
 *                             defined and exported for the Network 
 *                             Processor support.
 *------------------------------------------------------------------------*/

#ifndef _MPLS_NPEX_H
#define _MPLS_NPEX_H

#ifdef NPAPI_WANTED
/* Data Structures */

typedef struct _NpIlmEntry {
   UINT4    u4InstanceId;    /* Instance Identifier */    
   UINT4    u4IncIfIndex;    /* Incoming Interface Index */   
   UINT4    u4IncLabel;      /* Incoming Label */                    
   UINT4    u4OutIfIndex;    /* Outgoing Interface Index */            
   UINT4    u4OutLabel;      /* Outgoing Label */        
   UINT4    u4NextHopAddr;   /* Address of the NextHop */    
   tMacAddr DestMacAddr;     /* MAC Addr in case Ethernet Iface */
   UINT2    u2OutIfType;     /* Outgoing Interface Type (ATM/ETH/FR) */
   UINT1    u1LblOperation;  /* Operation to be performed */      
   UINT1    u1NumDecTtl;     /* Number to be decremented from Outgoing TTL */
   UINT1    u1Modify;        /* Flag for conveying modification information */
   UINT1    u1Rsvd;
}tNpIlmEntry;

typedef struct _NpFtnEntry {
   tMplsTrfcParms MplsTrfcParams; /* MPLS Traffic Parameters */
   UINT4          u4InstanceId;   /* Instance Identifier */
   UINT4          u4OutIfIndex;   /* Outgoing Interface Index */
   UINT4          u4OutLabel;     /* Outgoing Label */
   UINT4          u4NextHopAddr;  /* Address of the NextHop */
   UINT4          u4SrcAddr;      /* Source Address */
   UINT4          u4DestAddr;     /* Destination Address */
   UINT4          u4DestMask;     /* Destination Mask */
   tMacAddr       DestMacAddr;    /* MAC Addr in case Ethernet Iface */
   UINT2          u2OutIfType;    /* Outgoing Interface Type (ATM/ETH/FR) */
   UINT1          u1Tos;          /* Type Of Service */
   UINT1          u1NumDecTtl;    /* Number to be decremented from 
                                     Outgoing TTL */
   UINT1          u1Modify;       /* Flag for conveying modification 
                                     information */
   UINT1          u1StackTnl;     /* Flag for conveying Tunnel Stacking
                                     information */
}tNpFtnEntry;



/* Function Prototypes */

UINT1 
mplsNpCreateIf ARG_LIST ((UINT4 u4IfIndex, UINT2 u2IfType));

UINT1 
mplsNpDeleteIf ARG_LIST ((UINT4 u4IfIndex, UINT2 u2IfType));

UINT1 
mplsNpEnableIf ARG_LIST ((UINT4 u4IfIndex));

UINT1 
mplsNpDisableIf ARG_LIST ((UINT4 u4IfIndex));

UINT1 
mplsNpCreateGblLblSpace ARG_LIST ((UINT4 u4MinLabel, UINT4 u4MaxLabel));

UINT1 
mplsNpDeleteGblLblSpace ARG_LIST ((VOID));

UINT1 
mplsNpCreatePerIfLblSpace ARG_LIST ((UINT4 u4IfIndex, UINT4 u4MinLabel,
                                     UINT4 u4MaxLabel));

UINT1 
mplsNpDeletePerIfLblSpace ARG_LIST ((UINT4 u4IfIndex));

UINT1 
mplsNpILMAdd ARG_LIST ((tNpIlmEntry NpIlmEntry));

UINT1 
mplsNpILMDel ARG_LIST ((tNpIlmEntry NpIlmEntry));

UINT1 
mplsNpFTNToNhlfeAdd ARG_LIST ((tNpFtnEntry NpFtnEntry));

UINT1 
mplsNpFTNToNhlfeDel ARG_LIST ((tNpFtnEntry NpFtnEntry));

UINT1 
mplsNpFTNToTnlMapAdd ARG_LIST ((tNpFtnEntry NpFtnEntry));

UINT1 
mplsNpFTNToTnlMapDel ARG_LIST ((tNpFtnEntry NpFtnEntry));

#endif /* NPAPI_WANTED */
#endif /* MPLS_NPEX_H */
