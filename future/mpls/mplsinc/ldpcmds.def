/*****************************************************************************/
/* Copyright (C) 2014 Aricent Inc . All Rights Reserved
 * $Id: ldpcmds.def,v 1.31 2017/06/08 11:40:31 siva Exp $
 ******************************************************************************/

/*****************************************************************************
 *                        MPLS LDP CONFIGURATION                            *
 *****************************************************************************/
DEFINE GROUP: MPLS_LDP_CFG

COMMAND : entity <integer>
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_ENTITY, $1);
SYNTAX  : entity <entity_index>
HELP    : To create a ldp entity.
CXT_HELP: entity Configure LDP Entity parameter|
          Entity-Index Entity index (1 - 16)|
          <CR> To create a ldp entity.

COMMAND : no entity <integer>
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_ENTITY, $2);
SYNTAX  : no entity <entity_index>
HELP    : To delete a ldp entity.
CXT_HELP: no Negate a command or set its defaults|
          entity Configure LDP Entity parameter|
          Entity-Index Entity index(1 - 16)|
          <CR> To delete a ldp entity.

COMMAND : label allocation {ordered | independent}
ACTION  : 
         {
             INT4 i4LabelAllocation = 0;
             if($2 != NULL)
             {
                 i4LabelAllocation  = MPLS_LDP_ORDERED_MODE;
             }
             else if($3 != NULL)
             {
                 i4LabelAllocation  = MPLS_LDP_INDEPENDENT_MODE;
             }
             cli_process_ldp_cmd (CliHandle,CLI_MPLS_LDP_LABEL_ALLOCATION_MODE, 
                                   i4LabelAllocation);
         }
SYNTAX  : label allocation { ordered | independent }
HELP    : To configure the label allocation method.
CXT_HELP: label Label properties|
          allocation Configure Label allocation method|
          ordered Use Ordered allocation|
          independent Use Independent allocation|
          <CR> To configure the label allocation method.

COMMAND : no label allocation
ACTION  : cli_process_ldp_cmd (CliHandle, 
                               CLI_MPLS_LDP_NO_LABEL_ALLOCATION_MODE);
SYNTAX  : no label allocation
HELP    : To reset the label allocation
CXT_HELP: no Negate a command or set its defaults|
          label Label properties|
          allocation Configure Label allocation method|
          <CR> To reset the label allocation

COMMAND : router-id {vlan <integer (1-4094)> | <iftype> <ifnum> | loopback
          <integer (0-100)> | <ucast_addr>} [force]
ACTION  :
         {
             INT4   i4ForceOption = FALSE;
             UINT4  u4IfIndexOrIpAddr = CFA_INVALID_INDEX;
             UINT4  *pu4IfIndexOrIpAddr = NULL;
             UINT4  u4IfType;

             if ($8 != NULL)
             {
                 i4ForceOption = TRUE;
             }

             if ($1 != NULL)
             {
                 u4IfType = CFA_L3IPVLAN;
                 CfaCliGetIfIndex ($1, $2, &u4IfIndexOrIpAddr);
             }
             else if ($3 != NULL)
             {
                 u4IfType = CFA_ENET;
                 CfaCliGetIfIndex ($3, $4, &u4IfIndexOrIpAddr);
             }
             else if ($5 != NULL)
             {
                 u4IfType = CFA_LOOPBACK;
                 CfaCliGetIfIndex ($5, $6, &u4IfIndexOrIpAddr);
             }
             else
             {
                 u4IfType = CFA_NONE;
                 pu4IfIndexOrIpAddr = $7;
             }
	     if(u4IfType != CFA_NONE)
	     {
             	cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_LSR_ID, u4IfType,
                                  u4IfIndexOrIpAddr, i4ForceOption, CLI_ENABLE);
	     }
	     else
	     {

             	cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_LSR_ID, u4IfType,
                                  pu4IfIndexOrIpAddr, i4ForceOption, CLI_ENABLE);
 	     }
         }

SYNTAX  : router-id {vlan <vlan-id (1-4094)> | <interface-type> <interface-id> | 
          loopback <loopback-id (0-100)> | <ip-address>} [force]
HELP    : It sets the LDP LSR Identifier. When this command is given force option, 
          all the LDP Sessions are torn down and restarted.
CXT_HELP: router-id Select interface to prefer for LDP identifier address|
          vlan VLAN|
          <1-4094> Vlan Identifier|
          DYNiftype|
          DYNifnum| 
          loopback Loopback interface|
          <0-100> Loopback interface number|
          A.B.C.D IP address|
          force Forcibly change the LDP router id|
          <CR> It sets the LDP LSR Identifier. When this command is given force option, all the LDP Sessions are torn down and restarted.

COMMAND : no router-id [force]
ACTION  :
         {
             INT4 i4ForceOption = FALSE;
             UINT4 u4IfIndexOrIpAddr = CFA_NONE;
             UINT4  *pu4IfIndexOrIpAddr = NULL;

             if ($2 != NULL)
             {
                 i4ForceOption = TRUE;
             }


             pu4IfIndexOrIpAddr = &u4IfIndexOrIpAddr;
             cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_LSR_ID, 0,pu4IfIndexOrIpAddr,
                                  i4ForceOption, CLI_DISABLE);
         }
SYNTAX  : no router-id [force]
HELP    : When this command is executed, LDP continues to use last LSR ID.If last LSR ID is associated with any interface ip address then LSR ID gets updated as per the interface status change.
CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
          router-id Router ID  related configuration|
          force Forcibly changes the LSR ID with highest IP address on the system|
          <CR> When this command is executed, LDP continues to use Last LSR ID.If last LSR ID is associated with any Interface ip address then LSR ID gets updated as per the Interface status change.

COMMAND  : mpls ldp graceful-restart {help-neighbour | full}
ACTION   :
    {
           INT4 i4GrCapability = LDP_GR_CAPABILITY_NONE;

           if ($3 != NULL)
           {
               i4GrCapability = LDP_GR_CAPABILITY_HELPER;
           }
           else if ($4 != NULL)
           {
               i4GrCapability = LDP_GR_CAPABILITY_FULL;
           }
           cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_GR_MODE, i4GrCapability);
    }
SYNTAX   : mpls ldp graceful-restart {help-neighbour | full}
HELP     : Enable the graceful restart Mode. help-neighbour: graceful restart support is in strict helper mode. full: graceful restart is fully supported.
CXT_HELP : mpls Configure MPLS parameters|
           ldp Label Distribution Protocol|
           graceful-restart Configure LDP Graceful Restart|
           help-neighbor graceful restart support is in strict helper mode|
           full graceful restart is fully supported|
           <CR> Enable the graceful restart Mode.

COMMAND  : no mpls ldp graceful-restart
ACTION   :
    {
           cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_GR_NO_MODE, LDP_GR_CAPABILITY_NONE);
    }
SYNTAX   : no mpls ldp graceful-restart
HELP     : Disable the graceful restart Mode.
CXT_HELP : no Negate a command or set its defaults|
           mpls Configure MPLS parameters|
           ldp Label Distribution Protocol|
           graceful-restart Configure LDP Graceful Restart|
           <CR> Disable the graceful restart Mode.

COMMAND  : mpls ldp graceful-restart timers { neighbor-liveness <integer> | forwarding-holding <integer>  | max-recovery <integer> } 
ACTION   :
    {
           UINT4 u4NbrTimer = 0;
           UINT4 u4fwdTimer = 0;
           UINT4 u4RcrTimer = 0;
           if ($4 != NULL)
           {
               u4NbrTimer = *(UINT4 *) $5;
               cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NBR_LIVENESS_TIMER, u4NbrTimer);
           }
           else if ($6 != NULL)
           {
               u4fwdTimer = *(UINT4 *) $7;
               cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_FWD_HOLDING_TIMER, u4fwdTimer);
           }
           else if ($8 != NULL)
           {
               u4RcrTimer =  *(UINT4 *) $9;
               cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_RECOVERY_TIMER, u4RcrTimer);
           }

    }

SYNTAX   : mpls ldp graceful-restart timers { neighbor-liveness <seconds (5-300)> | forwarding-holding <seconds (30-600)>  | max-recovery <seconds (15-600)> }
HELP     : Set the neighbor-liveness[Amount of time that the node should wait for an LDP session to be reestablished], forwarding-holding[Amount of time that the MPLS forwarding state must be preserved after the restart of LDP Component] and max-recovery timer[The amount of time that the node should hold stale label-FEC bindings after an LDP session has been reestablished] values.
CXT_HELP : mpls Configure MPLS parameters|
           ldp Label Distribution Protocol|
           graceful-restart Configure LDP Graceful Restart|
           timers timer value|
           neighbor-liveness Neighbor-Liveness time|
           <5-300> neighbor-liveness time in seconds|
           forwarding-holding Forwarding State Holding time|
           <30-600> forwarding-holding time in seconds|
           max-recovery Max-Recovery time|
           <15-600> max-recovery timer in seconds|
           <CR> Set the neighbor-liveness values.

COMMAND  : no mpls ldp graceful-restart timers { neighbor-liveness | forwarding-holding | max-recovery }
ACTION   :
    {
           if ($5 != NULL)
           {
               cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_NBR_LIVENESS_TIMER, -1);
           }
           else if ($6 != NULL)
           {
               cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_FWD_HOLDING_TIMER, -1);
           }
           else if ($7 != NULL)
           {
               cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_RECOVERY_TIMER, -1);
           }
    }
SYNTAX   : no mpls ldp graceful-restart timers { neighbor-liveness | forwarding-holding | max-recovery }
HELP     : Reset the neighbor-liveness, forwarding-holding and max-recovery timer values.
CXT_HELP : no Negate a command or set its defaults|
           mpls Configure MPLS parameters|    
           ldp Label Distribution Protocol|
           graceful-restart Configure LDP Graceful Restart|
           timers timer value|
           neighbor-liveness Neighbor-Liveness time|
           forwarding-holding Forwarding State Holding time|    
           max-recovery Max-Recovery time|
           <CR> Reset the neighbor-liveness, forwarding-holding and max-recovery timer values.

COMMAND  : configuration-sequence-tlv { enable | disable }
ACTION   :
    {
           INT4 i4ConfigSeqTLVEnable = MPLS_SNMP_FALSE;

           if ($1 != NULL)
           {
               i4ConfigSeqTLVEnable = MPLS_SNMP_TRUE;
           }
           else if ($2 != NULL)
           {
               i4ConfigSeqTLVEnable = MPLS_SNMP_FALSE;
           }
           cli_process_ldp_cmd (CliHandle, CLI_MPLS_CONFIG_SEQ_TLV, i4ConfigSeqTLVEnable);
    }
SYNTAX   : configuration-sequence-tlv { enable | disable }
HELP     : Enable/Disable Configuration Sequence TLV option
CXT_HELP : configuration-sequence-tlv Configuration Sequence TLV option|
           enable Enable Configuration Sequence TLV option globally|
           disable Disable Configuration Sequence TLV option globally|
           <CR> Enable/Disable Configuration Sequence TLV option

COMMAND : exit
ACTION  : CliChangePath("..");
SYNTAX  : exit
HELP    : To go the global configuration mode.
CXT_HELP: exit Exit from MPLS LDP Configuration mode|
          <CR> To go the global configuration mode.

END GROUP
/*****************************************************************************
 *                        MPLS LDP ENTITY CONFIGURATION ENDS                *
 *****************************************************************************/

/*****************************************************************************
 *                        MPLS LDP ENTITY CONFIGURATION                       *
 *****************************************************************************/
DEFINE GROUP: MPLS_LDP_ENT_CFG

COMMAND : transport-address tlv { interface | loopback <integer (0-100)>}
ACTION  :
          {
              UINT4 u4TransIfType = 0;
              UINT4 u4IfIndex = 0;

              if ($2 != NULL)
              {
                  u4TransIfType = MPLS_LDP_INTERFACE_ADDRESS_TYPE;
              }
              else
              {
                  u4TransIfType = MPLS_LDP_LOOPBACK_ADDRESS_TYPE;
                  CfaCliGetIfIndex ($3, $4, &u4IfIndex);
              }
              cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_TRANSPORT_ADDRESS,
                                   u4TransIfType, u4IfIndex, CLI_ENABLE);
          }
SYNTAX  : transport-address tlv { interface | loopback <loopback-id (0-100)>}
HELP    : Sets the transport-address TLV option and transport-address to be
          carried in the Transport-address TLV in LDP Hello Message.
CXT_HELP: transport-address Specify interface LDP transport address|
          tlv Configure transport address value to use in LDP Hello message|
          interface Use interface address for LDP transport address|
          loopback Loopback interface|
          (0-100) Loopback interface number|
          <CR> Sets the transport-address TLV option and transport-address to be carried in the Transport-address TLV in LDP Hello Message.

#ifdef MPLS_IPV6_WANTED

COMMAND : transport-address-ipv6 tlv { interface | loopback <integer (0-100)>}
ACTION  :
          {
              UINT4 u4TransIfType = 0;
              UINT4 u4IfIndex = 0;

              if ($2 != NULL)
              {
                  u4TransIfType = MPLS_LDP_INTERFACE_ADDRESS_TYPE;
              }
              else
              {
                  u4TransIfType = MPLS_LDP_LOOPBACK_ADDRESS_TYPE;
                  CfaCliGetIfIndex ($3, $4, &u4IfIndex);
              }
              cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_IPV6_TRANSPORT_ADDRESS,
                                   u4TransIfType, u4IfIndex, CLI_ENABLE);
          }
SYNTAX  : transport-address-ipv6 tlv { interface | loopback <loopback-id (0-100)>}
HELP    : Sets the IPv6 transport-address TLV option and IPv6 transport-address to be
          carried in the Transport-address TLV in IPv6 LDP Hello Message.
CXT_HELP: transport-address-ipv6 Configures interface IPv6 LDP transport address|
          tlv Transport address  TLV in IPv6 LDP Hello message|
          interface Interface address is used as LDP transport address|
          loopback IPv6 Loopback interface|
          (0-100) Loopback interface number|
          <CR> Sets the IPv6 transport-address TLV option and IPv6 transport-address to be carried in the Transport-address TLV in IPv6 LDP Hello Message.

COMMAND : no transport-address-ipv6 tlv
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_IPV6_TRANSPORT_ADDRESS,
                               MPLS_LDP_LOOPBACK_ADDRESS_TYPE, 0, CLI_DISABLE);
SYNTAX  : no transport-address-ipv6 tlv
HELP    : Resets the IPv6 Transport Address TLV Option. This implies that transport-address TLV is not carried in IPv6 LDP Hello
          Message.
CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
          transport-address-ipv6 Interface IPv6 LDP transport address|
          tlv Transport address  TLV in IPv6 LDP Hello message |
          <CR> Resets the IPv6 Transport Address TLV Option. This implies that transport-address TLV is not carried in IPv6 LDP Hello Message.

#endif
COMMAND : no transport-address tlv
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_TRANSPORT_ADDRESS,
                               MPLS_LDP_LOOPBACK_ADDRESS_TYPE, 0, CLI_DISABLE);
SYNTAX  : no transport-address tlv
HELP    : Resets the Transport Address TLV Option. When this command is
          configured, transport-address TLV is not carried in the LDP Hello
          Message.
CXT_HELP: no Negate a command or set its defaults|
          transport-address Specify interface LDP transport address|
          tlv Configure transport address value to use in LDP Hello message|
          <CR> Resets the Transport Address TLV Option.

COMMAND : discovery hello holdtime <integer (0-65535)>
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_HELLO, $3);
SYNTAX  : discovery hello holdtime <seconds (0-65535)>
HELP    : To configure the hello holdtime for the discovery process.
CXT_HELP: discovery Configure LDP Discovery parameters|
          hello LDP discovery Hello|
          holdtime LDP discovery Hello holdtime|
          <0-65535> Hold time in seconds|
          <CR> To configure the hello holdtime for the discovery process.

COMMAND : no discovery hello holdtime
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_HELLO);         
SYNTAX  : no discovery hello holdtime
HELP    : To disable the hello holdtime for the discovery process.
CXT_HELP: no Negate a command or set its defaults|
          discovery Configure LDP Discovery parameters|
          hello LDP discovery Hello|
          holdtime LDP discovery Hello holdtime|
          <CR> To disable the hello holdtime for the discovery process.

COMMAND : holdtime <integer (1-65535)> 
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_KEEPALIVE, $1);
SYNTAX  : holdtime <seconds (1-65535)> 
HELP    : To configure the keep alive holdtime.
CXT_HELP: holdtime Configure keep alive holdtime|
          <1-65535> Keep alive hold time in seconds|
          <CR> To configure the keep alive holdtime.
          
COMMAND : no holdtime
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_KEEPALIVE);
SYNTAX  : no holdtime
HELP    : To disable the keep alive holdtime.
CXT_HELP: no Negate a command or set its defaults|
          holdtime Configure keep alive holdtime|
          <CR> To disable the keep alive holdtime.

COMMAND : max-hops <integer (0-255)>
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_MAX_HOPS, $1);
SYNTAX  : max-hops <number (0-255)>
HELP    : To configure the maximum no. of hops possible.
CXT_HELP: max-hops Limit hop count for LDP LSP setup|
          <0-255> Maximum hop count value|
          <CR> To configure the maximum no. of hops possible.

COMMAND : no max-hops
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_MAX_HOPS);
SYNTAX  : no max-hops
HELP    : To revert back to default maximum number of hops.
CXT_HELP: no Negate a command or set its defaults|
          max-hops Limit hop count for LDP LSP setup|
          <CR> To revert back to default maximum number of hops.
COMMAND : neighbor <random_str> targeted [te [in <integer (1-100)>] [out <integer (1-100)>]]
ACTION  :
         {
          if ($3 != NULL)
          {
              if (($4 == NULL) && ($6 == NULL))
              {
                  CliPrintf (CliHandle,
                             "\r%% Incoming or/and Outgoing tunnels should be specified \r\n");
                  return CLI_FAILURE;
              }
          }
          cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_TARGETED_HELLO, $1, $5, $7);
         }
SYNTAX  : neighbor <random_str> targeted [te [in <tnl-id (1-100)>] [out <tnl-id (1-100)>]]
HELP    : Establishes the targeted session with another LDP for the specified ip address.
CXT_HELP: neighbor Configures neighbor related parameters|
          (A.B.C.D/<string>)IPv4 or IPv6 address for LDP neighbor|
          targeted Targeted session|
          te LDP over RSVP targeted session|
          in Incoming tunnel|
          (1-100) Tunnel interface number|
          out Outgoing tunnel|
          (1-100) Tunnel interface number|
          <CR> Establishes the targeted session with another LDP for the specified ip address.

COMMAND : no neighbor <random_str> targeted
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_TARGETED_HELLO, $2);
SYNTAX  : no neighbor <random_str> targeted
HELP    : Deletes the targeted LDP session with the peer.
CXT_HELP: no Disables the configuration / deletes the entry / resets to default value|
          neighbor  neighbor related configuration|
          (A.B.C.D/<string>)IPv4 or IPv6 address for LDP neighbor|
          targeted Targeted session|
          <CR> Deletes the targeted LDP session with the peer.

COMMAND : label distribution {ondemand | unsolicited}
ACTION  : 
          {
              INT4 i4LabelDistMode = 0;
              if($2 != NULL)
              {
                  i4LabelDistMode = MPLS_LDP_ON_DEMAND_MODE;
              }
              else if($3 != NULL)
              {
                  i4LabelDistMode = MPLS_LDP_UNSOLICITED_MODE;
              }
              cli_process_ldp_cmd(CliHandle, CLI_MPLS_LDP_LABEL_DIST_METHOD, 
                                              i4LabelDistMode);
          }
SYNTAX  : label distribution {ondemand | unsolicited}
HELP    : To configure the label distribution method.
CXT_HELP: label Label properties|
          distribution Configure Label distribution method|
          ondemand Use Ondemand distribution|
          unsolicited Use Unsolicited distribution|
          <CR> To configure the label distribution method.

COMMAND : no label distribution
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_LABEL_DIST_METHOD);
SYNTAX  : no label distribution
HELP    : To disable the label distribution method.
CXT_HELP: no Negate a command or set its defaults|
          label Label properties|
          distribution Configure Label distribution method|
          <CR> To disable the label distribution method.

COMMAND : label retention {liberal | conservative}
ACTION  : 
          {
              INT4 i4LabelRetention = 0;
              if($2 != NULL)
              {
                  i4LabelRetention = MPLS_LDP_LIBERAL_MODE;
              }
              else if($3 != NULL) 
              {
                  i4LabelRetention = MPLS_LDP_CONSERVATIVE_MODE;
              }
              cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_LABEL_RETENTION_MODE,                                                i4LabelRetention);
          }
SYNTAX  : label retention {liberal | conservative}
HELP    : To configure the label retention method.
CXT_HELP: label Label properties|
          retention Configuration Label Retention mode|
          liberal Use Liberal retention mode|
          conservative Use conservative retention mode|
          <CR> To configure the label retention method.

COMMAND : no label retention
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_LABEL_RETENTION_MODE);
SYNTAX  : no label retention
HELP    : To reset the label retention method.
CXT_HELP: no Negate a command or set its defaults|
          label Label properties|
          retention Configuration Label Retention mode|
          <CR> To reset the label retention method.

COMMAND : encapsulate {explicit-null | implicit-null}
ACTION  : 
         {
             UINT4 i4PhpMode = 0;
             if($1 != NULL)
             {
                 i4PhpMode = MPLS_LDP_EXPLICIT_NULL_LABEL;
             }
             else if($2 != NULL)
             {
                 i4PhpMode = MPLS_LDP_IMPLICIT_NULL_LABEL;
             }
             cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_PHP_MODE, i4PhpMode);
         }
SYNTAX  : encapsulate {explicit-null | implicit-null}
HELP    : To enable Penultimate Hop Popping Action
CXT_HELP: encapsulate Configure Penultimate Pop action|
          explicit-null Advertise Explicit Null label|
          implicit-null Advertise Implicit Null label|
          <CR> To enable Penultimate Hop Popping Action

COMMAND : no encapsulate
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_NO_PHP_MODE);
SYNTAX  : no encapsulate
HELP    : To disable Penultimate Hop Popping Action.
CXT_HELP: no Negate a command or set its defaults|
          encapsulate Configure Penultimate Pop action|
          <CR> To disable Penultimate Hop Popping Action.

COMMAND : ldp [label range min <integer> max <integer>] [interface {vlan <integer (1-4094)> | <iftype> <ifnum> | port-channel <integer (1-65535)>}]
ACTION  : 
         {
             UINT4 u4MinLabel = (UINT4) CLI_MPLS_LDP_ZERO;
             UINT4 u4MaxLabel = (UINT4) CLI_MPLS_LDP_ZERO;

             UINT4 u4IfIndex = CFA_INVALID_INDEX;
             UINT4 u4MplsIfIndex = 0;

             if ($7 != NULL)
             {
             if ($8 != NULL)
             {
                 CfaCliGetIfIndex ($8, $9, &u4IfIndex);
             }
             else if($10 != NULL)
             {
                 CfaCliGetIfIndex ($10, $11, &u4IfIndex);
             }
             else
             {
                  CfaCliGetPoIndex ($12, $13, &u4IfIndex);
             }
#ifdef CFA_WANTED
             if (CfaUtilGetMplsIfFromIfIndex (u4IfIndex, &u4MplsIfIndex, TRUE) ==
                 CFA_FAILURE)
             {
                 CliPrintf (CliHandle,
                            "\r%% MPLS is not enabled on the interface\r\n");
                 return CLI_FAILURE;
             } 
#endif
             }
             if($1 != NULL)
             {
                 u4MinLabel = *(UINT4 *) $4;
                 u4MaxLabel = *(UINT4 *) $6;
                 if (u4MinLabel > u4MaxLabel)
                 {
                     CliPrintf (CliHandle,
                                "\r%% MPLS LDP Min/Max label range is invalid\r\n");
                     return CLI_FAILURE;
                 }
             }
             cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_INTERFACE_ENABLE,  
                                  u4MplsIfIndex, u4MinLabel, u4MaxLabel);
         }
SYNTAX  : ldp [label range min <min label> max <max label>] [interface {vlan <vlan-id (1-4094)> |
            <interface-type> <interface-id> | port-channel <integer (1-65535)>}]
HELP    : Sets the ldp as the label distribution protocol for interface 
          configuration Label range are Basic(100 - 100000) 
          Targeted Hello (160001- 200000).  
CXT_HELP: ldp Label Distribution Protocol|
          label Label properties|
          range Label range|
          min Minimum label value|
          <100-100000,160001-200000> Label Value 100 - 100000 - Non targeted 160001 - 200000 - targeted|
          max Maximum label value|
          <100-100000,160001-200000> Label Value 100 - 100000 - Non targeted 160001 - 200000 - targeted|
          interface Select an interface to configure|
          vlan Use VLAN|
          <1-4094> Vlan Identifier|
          DYNiftype|
          DYNifnum|
          <CR> To set the ldp as the label distribution protocol for interface

COMMAND : no ldp [label range min <integer> max <integer>] 
ACTION  :
         { 
             UINT4 u4MinLabel = (UINT4) CLI_MPLS_LDP_ZERO;
             UINT4 u4MaxLabel =  (UINT4) CLI_MPLS_LDP_ZERO;
             
             if($2 != NULL)
             {
                 u4MinLabel = *(UINT4 *) $5;
                 u4MaxLabel = *(UINT4 *) $7;
                 
                 if (u4MinLabel > u4MaxLabel)
                 {
                     CliPrintf (CliHandle,
                                "\r%% MPLS LDP Min/Max label range is invalid\r\n");
                     return CLI_FAILURE;
                 }
             }
             cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_INTERFACE_DISABLE, 
                                                    u4MinLabel, u4MaxLabel);
          }          
SYNTAX  : no ldp [label range min <min label> max <max label>]
HELP    : To disable the ldp as label distribution protocol in the interface
          Label range are Basic (100 - 100K), Targeted (160K + 1 - 200K).
CXT_HELP: no Negate a command or set its defaults|
          ldp Label Distribution Protocol|
          label Label properties|
          range Label range|
          min Minimum label value|
          <100-100000,160001-200000> Label Value 100 - 100000 - Non targeted 160001 - 200000 - targeted|
          max Maximum label value|
          <100-100000,160001-200000> Label Value 100 - 100000 - Non targeted 160001 - 200000 - targeted|
          <CR> To disable the ldp as label distribution protocol in the interface.

COMMAND  : mpls ldp path-vector maxlength <integer (0-255)>
ACTION   :
    {
           INT4 i4PathVector = *(UINT4 *) $4;
           cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_PATH_VECTOR, i4PathVector);
    }
SYNTAX   :  mpls ldp path-vector maxlength <integer (0-255)>          
HELP     :  To set the maximum number of router IDs permitted in a path vector type, length, value (TLV) used to perform path vector loop detection.       
CXT_HELP :  mpls Configure MPLS parameters|
            ldp Label Distribution Protocol|
            path-vector path vector configuration|
            maxlength maximum number of router IDs permitted in path vector TLV|
            (0-255) Number from 0 to 255, inclusive, that defines the maximum number of 4-octet router IDs permitted in the path vector|
            <CR> To set the maximum number of router IDs permitted in a path vector type, length, value (TLV) used to perform path vector loop detection.


COMMAND  : no mpls ldp path-vector maxlength
ACTION   :
    {
           INT4 i4PathVector = 0;
           cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_PATH_VECTOR, i4PathVector);
    }
SYNTAX   :  no mpls ldp path-vector maxlength
HELP     :  To reset the maximum number of router IDs permitted in a path vector type, length, value (TLV) used to perform path vector loop detection.       
CXT_HELP :  no Negate a command or set its defaults|
            mpls Configure MPLS parameters|
            ldp Label Distribution Protocol|
            path-vector path vector configuration|
            maxlength maximum number of router IDs permitted in path vector TLV|
            <CR> To reset the maximum number of router IDs permitted in a path vector type, length, value (TLV) used to perform path vector loop detection.

#ifdef MPLS_LDP_BFD_WANTED

COMMAND  : enable bfd
ACTION   :
    {
          cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_BFD_STATUS, NULL, LDP_BFD_ENABLED);
    }
SYNTAX   : enable bfd
HELP     : Enables BFD monitoring for the LDP Entity.
CXT_HELP : enable Enables the feature |
           bfd BFD related configuration |
           <CR> Enables BFD monitoring for the LDP Entity.


COMMAND  : disable bfd
ACTION   :
    {
          cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_BFD_STATUS, NULL, LDP_BFD_DISABLED);
    }
SYNTAX   : disable bfd
HELP     : Disables BFD monitoring for the LDP Entity.
CXT_HELP : disable Disables the feature |
           bfd BFD related configuration |
           <CR> Disables BFD monitoring for the LDP Entity.

#endif 

COMMAND : shutdown
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_ENTITY_SHUT);
SYNTAX  : shutdown
HELP    : To make the LDP entity not in service or not ready.
CXT_HELP: shutdown Shutdown the selected LDP Entity|
          <CR> To make the LDP entity not in service or not ready.

COMMAND : no shutdown
ACTION  : cli_process_ldp_cmd (CliHandle, CLI_MPLS_LDP_ENTITY_NO_SHUT);
SYNTAX  : no shutdown
HELP    : To make the LDP entity active.
CXT_HELP: no Negate a command or set its defaults|
          shutdown Shutdown the selected LDP Entity|
          <CR> To make the LDP entity active.

COMMAND : exit
ACTION  :
{
    CLI_SET_MPLS_LDP_ENTITY_MODE (-1);
    CliChangePath("..");
}
SYNTAX  : exit
HELP    : To go to the ldp configuration mode.
CXT_HELP: exit Exit from LDP Entity configuration mode|
          <CR> To go to the ldp configuration mode.

END GROUP
/*****************************************************************************
 *                        MPLS LDP ENTITY CONFIGURATION  ENDS                *
 *****************************************************************************/
