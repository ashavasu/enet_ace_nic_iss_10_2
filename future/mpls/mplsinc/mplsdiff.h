
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsdiff.h,v 1.9 2016/04/09 09:55:26 siva Exp $
 *              
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : mplsdiff.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS-FM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file DiffServ related definitions
 *----------------------------------------------------------------------------*/

#ifndef MPLSDIFF_H
#define MPLSDIFF_H

#include "mpls.h"

/* Macros for sizing */

#define MPLS_GBL_DIFFSERV_PARAMS_SIZE (sizeof (tMplsDiffServParams) * \
                                       MAX_MPLSRTR_MPLS_DIFFSERV_PARAMS)
#define MPLS_DIFFSERV_PARAMS_TOKEN_STACK_SIZE (sizeof (UINT4) * \
                                               MAX_MPLSRTR_MPLS_DIFFSERV_PARAMS)
#define MPLS_GBL_ELSPMAP_TABLE_SIZE (sizeof (tElspMapRow) * \
                                     MAX_MPLSRTR_ELSPMAP_ROW_ENTRIES)
#define MPLS_ELSPMAP_TOKEN_STACK_SIZE (sizeof (UINT4) * \
                                       MAX_MPLSRTR_ELSPMAP_ROW_ENTRIES)


#define MPLS_EXP_MASK          0x000000e00

#define MPLS_DSCP_BITMASK    0xfc
#define MPLS_2_BIT_SHIFT      2
#define MPLS_9_BIT_SHIFT      9
#define MPLS_12_BIT_SHIFT     12

#define MPLS_DIFFSERV_DROPPREC_BITMASK     0x1c

#define MPLS_GEN_LSP                       0
#define MPLS_DIFFSERV_ELSP                 1
#define MPLS_DIFFSERV_LLSP                 2
#define MPLS_INTSERV_LSP                   3
#define MPLS_DIFFSERV_BOTH                 4 
      /* This means both Elsp and Llsp are supported.  Positioning of
       * definition is improper!  */

#define UNIFORM_MODEL                      1
#define PIPE_MODEL                         2
#define SHORT_PIPE_MODEL                   3
#define MPLS_DEF_TTL_VAL                   255

#define MPLS_UNKNOWN                       2

/* Type defined values for setting creator of data structures */
#define MPLS_CTRL_PROTO                    100
#define MPLS_SNMP                          200

#define MPLS_MAX_CLASS_TYPE_ENTRIES 8
#define MPLS_MAX_TRAFFIC_CLASS_ENTRIES 22
#define MPLS_MAX_TE_CLASS_ENTRIES 8
#define MPLS_MAX_DSTE_MAX_STRING_LEN 255
#define MPLS_MAX_PRIORITY 8
#define MPLS_MIN_PRIORITY 0
#define MPLS_MAX_CLASS_TYPE_PERCENT 100
#define MPLS_INVALID_EXP_PHB_MAP_INDEX (-1)

#define MPLS_CALC_TOS_CHKSUM(u2ChkSum, u1IpTos, u1Tos) \
{    \
   /* Hopefully this will work ... Hey, but who knows?!! */ \
   UINT4 u4Sum = ((u2ChkSum) + ((u1IpTos) - (u1Tos)));\
   u2ChkSum = (UINT2)(u4Sum + (u4Sum >> 16)) ;  \
}    \

/*Object Ids for all the entries in ClassType Table*/
#define MPLS_DSTE_CLASS_TYPE_DESCRIPTION 1
#define MPLS_DSTE_CLASS_TYPE_ROW_STATUS 2
#define MPLS_DSTE_CLASS_TYPE_BW_PERCENT 3

#define MPLS_DSTE_TC_MAP_TYPE 4
#define MPLS_DSTE_TC_MAP_DESC 5
#define MPLS_DSTE_TC_MAP_ROW_STATUS 6

#define MPLS_DSTE_TE_CLASS_DESC 7
#define MPLS_DSTE_TE_CLASS_NUMBER 8
#define MPLS_DSTE_TE_CLASS_ROW_STATUS 9

/* Struct used to store Dscp Values obtained from Incoming Packet */
typedef struct _MplsDscpRecord
{
    UINT1               u1TopOutgoingDscp;
    UINT1               u1TnlDscp;
    UINT1               u1IsIngres;
    UINT1               u1IsEgres;
}
tMplsDscpRecord;

/* Struct used to define EXP->PHB mapping Entry for Signaled or Pre-Configured 
 * E-LSP 
 */
typedef struct _ElspPhbMapEntry
{
    UINT1 u1IsExpValid; /* Flag Indicating whether the Exp Value is valid */
    UINT1 u1ElspPhbDscp;/* PHB Id corresponding to a given EXP value */
    UINT1 u1RowStatus;  /* Row Status for Snmp */
    UINT1 u1Rsvd;
}
tElspPhbMapEntry;

typedef struct _ElspMapRow
{
    UINT4               u4ElspExpPhbMapIndex;    /* Index for Snmp access */
    tElspPhbMapEntry    aElspExpPhbMapArray[MPLS_DIFFSERV_MAX_EXP];
    UINT1               u1Creator;
    UINT1               u1UsedCount;
    UINT2               u2Rsvd2;
}
tElspMapRow;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */

typedef struct _ElspMapRowSize
{
    UINT1               au1ElspMapRow[MPLS_GBL_ELSPMAP_TABLE_SIZE];
}
tElspMapRowSize;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */

typedef struct _ElspMapTokenStackSize
{
    UINT1               au1ElspMapToken[MPLS_ELSPMAP_TOKEN_STACK_SIZE];
}
tElspMapTokenStackSize;

typedef struct _MplsElspParams
{
    UINT1               u1ElspType;
    /* Valid in case of Diffserv with Elsp support.  Can take values 
     * PRECONF_ELSP, SIGNALED_ELSP.  Incase of PreConfigured Elsp 
     * Exp->Phb mapping will be directly taken from the global structure 
     * which contains preconfigured mapping table.  In case of Signaled Elsp 
     * the pointer to the Exp->Phb map table pElspExpPhbMap
     * is consulted as described below.  Global structure should also contain a 
     * preconfigured exp->phb map for each outgoing interface which is 
     * different from this node's preconfigured mapping 
     */
    UINT1               u1Rsvd1;
    UINT2               u2Rsvd2;
    tElspMapRow         *pElspSigMap; /* Pointer to the Signaled Elsp Mapping */
}
tMplsElspParams;

typedef union _MplsLspParams
{
    tMplsElspParams     ElspParams;
    UINT1               u1LlspPscDscp;
    /* Valid in case of DIFFSERV with LLSP support.  For correct design, 
     * this is needed here
     */
}
tuMplsLspParams;

/* define the DiffServ related parameters in a separate struct and include it in
 * tNHLFE structure.  Makes it easy to include it in other Data Structures 
 * like CRLSP Info 
 */
typedef struct _MplsDiffServParams
{
    UINT4 u4DiffServParamsIndex; /* Index value for SNMP */
    UINT1 u1ServiceType;         /* can take values GEN_LSP, 
                                  * DIFFSERV_ELSP, DIFFSERV_LLSP, INTSERV 
                                  */
    UINT1 u1DiffServParamsStatus;/* Row Status for SNMP */
    UINT1 u1Creator;             /* Creator field for Snmp */
    UINT1 u1UsedCount; 
    tuMplsLspParams unLspParams;
}
tMplsDiffServParams;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */
typedef struct _MplsDiffServParamsSize
{
    UINT1        au1MplsDiffServParams[MPLS_GBL_DIFFSERV_PARAMS_SIZE];
}
tMplsDiffServParamsSize;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */
typedef struct _MplsDiffServParamTokenStack
{
UINT1        au1MplsDiffServParamTokenStack[MPLS_DIFFSERV_PARAMS_TOKEN_STACK_SIZE];
}
tMplsDiffServParamTokenStack;

/* add a flag and a pointer to a mapping here which is valid in case of 
 * diffserv Elsp
 */
typedef struct _MplsElspIfParams
{
    UINT1               u1ElspIsOutIfPreConfMapSame;
    /* Indicates if the preconfigured exp->phb mappings for 
     * elsps at the LSR which is attached to this interface
     * is the same as this node or not.  If same, then the Preconf
     * exp->phb mapping from the mpls global data structure can be
     * used for this.  Otherwise we refer to the pointer to exp->phb
     * mapping for the adjacent lsr which is defined below
     */
    UINT1               u1Rsvd1;
    UINT2               u2Rsvd2;
    tElspMapRow        *pOutIfPreConfMap; /* Pointer to Outgoing I/f Elsp Map */
}
tMplsElspIfParams;

/* type of Diffserv support ELSP or LLSP, valid only when u1QosPolicy is
 * MPLS_DIFF_SERV
 */
typedef struct _DiffServGlobal
{
    UINT1                 u1Rsvd1;
    UINT1                 u1TnlModel; /* can be UNIFORM_MODEL, 
                    * PIPE_MODEL or SHORT_PIPE_MODEL */
    UINT2                 u2Rsvd2;
    tElspMapRow          *pElspPreConfMap;   /* Pointer to PreConf Mapping */
    UINT4                 u4DiffServElspMapEntries; /* Maximum no. of Elsp to 
           * Phb mappings */
    UINT4                 u4DiffServParamsEntries;  /* Maximum no. of 
                  * DiffServParams */
    tElspMapRow         **ppaElspMapTable;
    UINT4                *pau4ElspMapTokenStack;
    UINT4                 u4ElspMapTokenStackTop;
    tMplsDiffServParams **ppaDiffServParamsTable;
    UINT4                *pau4DiffServParamsTokenStack;
    UINT4                 u4DiffServParamsTokenStackTop;
    UINT4                 u4DiffServStatus;
}
tDiffServGlobal;

typedef struct _MplsDsTeClassType {
    /* Maps to mib object - fsMplsDsTeClassTypeRowStatus */
    INT4          i4DsTeClassTypeRowStatus;
    /* Maps to mib object - fsMplsDsTeClassTypeBwPercentage */
    INT4          i4DsTeClassTypeBwPercent;
    /* Maps to mib object - fsMplsDsTeClassTypeDescription */
    UINT1         au1DsTeClassTypeDesc [SNMP_MAX_OID_LENGTH];
} tMplsDsTeClassType;

typedef struct _MplsDsTeClassTypeToTcMap {
    /* Maps to mib object - fsMplsDsTeTcType */
    UINT4        u4DsTeTcType;
    /* Maps to mib object - fsMplsDsTeTcDescription */
    UINT1        au1DsTeClassTypeToTcDesc [SNMP_MAX_OID_LENGTH];
    /* Maps to mib object - fsMplsDsTeTcMapRowStatus */
    INT4         i4DsTeClassTypeToTcRowStatus;
} tMplsDsTeClassTypeTcMap;

typedef struct _MplsDsTeClass {
    /* Maps to mib object - fsMplsDsTeTeClassNumber */
    UINT4        u4TeClassNumber;
    /* Maps to mib object - fsMplsDsTeTeClassDesc */
    UINT1        au1DsTeCDesc [SNMP_MAX_OID_LENGTH];
    /* Maps to mib object - fsMplsDsTeTeClassRowStatus */
    INT4         i4DsTeCRowStatus;
} tMplsDsTeClassPrio;

typedef struct _DsTeGlobal
{
    /* Stores the class type related information */
    tMplsDsTeClassType         aMplsDsTeClassType
        [MPLS_MAX_CLASS_TYPE_ENTRIES];
    /* Stores the class type to traffic class mapping related information */
    tMplsDsTeClassTypeTcMap    aMplsDsTeClassTypeTcMap
        [MPLS_MAX_CLASS_TYPE_ENTRIES][MPLS_MAX_TRAFFIC_CLASS_ENTRIES];
    /* Stores the class type to TE class mapping related information */
    tMplsDsTeClassPrio         aMplsDsTeClassPrio
        [MPLS_MAX_CLASS_TYPE_ENTRIES][MPLS_MAX_TE_CLASS_ENTRIES];
}tDsTeGlobal;

/* Pool Id for allocating DiffServ Params stored inside Nhlfe */
#define MPLS_DIFFSERV_ELSP_MAP_POOL_ID \
 MPLSRTRMemPoolIds[MAX_MPLSRTR_ELSPMAP_ROW_ENTRIES_SIZING_ID]
#define MPLS_DIFFSERV_PARAMS_POOL_ID \
 MPLSRTRMemPoolIds[MAX_MPLSRTR_MPLS_DIFFSERV_PARAMS_SIZING_ID]
#define MPLS_DIFFSERV_ELSP_MAP_ENTRIES(x) \
        (gMplsIncarn[x].DiffServGlobal.u4DiffServElspMapEntries)

#define MPLS_DIFFSERV_ELSP_MAP_TABLE(x) \
        (gMplsIncarn[x].DiffServGlobal.ppaElspMapTable)

#define MPLS_DIFFSERV_ELSP_MAP_ENTRY(x, y) \
        (gMplsIncarn[x].DiffServGlobal.ppaElspMapTable[y])

#define MPLS_DIFFSERV_ELSP_TOKEN_STACK(x, y) \
        (gMplsIncarn[x].DiffServGlobal.pau4ElspMapTokenStack[y])

#define MPLS_DIFFSERV_ELSP_TOKEN_STACK_TOP(x) \
        (gMplsIncarn[x].DiffServGlobal.u4ElspMapTokenStackTop)

#define MPLS_DIFFSERV_PARAMS_ENTRIES(x) \
        (gMplsIncarn[x].DiffServGlobal.u4DiffServParamsEntries)

#define MPLS_DIFFSERV_PARAMS_ENTRY(u4IncarnId, u4DiffParamsId) \
(gMplsIncarn[u4IncarnId].DiffServGlobal.ppaDiffServParamsTable[u4DiffParamsId])

#define MPLS_DIFFSERV_PARAMS_TOKEN_STACK(u4IncarnId, u4Top) \
  (gMplsIncarn[u4IncarnId].DiffServGlobal.pau4DiffServParamsTokenStack[u4Top])

#define MPLS_DIFFSERV_PARAMS_TOKEN_STACK_TOP(u4IncarnId) \
        (gMplsIncarn[u4IncarnId].DiffServGlobal.u4DiffServParamsTokenStackTop)

#define MPLS_DIFFSERV_TNL_MODEL(u4Incarn)  \
        (gMplsIncarn[u4Incarn].DiffServGlobal.u1TnlModel)

#define MPLS_TTL_VALUE(u4Incarn)  \
        (gMplsIncarn[u4Incarn].u1TTL)

#define MPLS_DIFFSERV_PARAMS_TABLE_POOL_ID \
 MPLSRTRMemPoolIds[MAX_MPLSRTR_DIFFSERV_PARAMS_TABLE_SIZING_ID]
#define MPLS_DIFFSERV_PARAMS_TOKEN_STACK_POOL_ID \
 MPLSRTRMemPoolIds[MAX_MPLSRTR_DIFFSERV_PARAMS_TOKEN_STACK_SIZING_ID]
#define MPLS_ELSPMAP_TABLE_POOL_ID \
 MPLSRTRMemPoolIds[MAX_MPLSRTR_ELSPMAP_TABLE_SIZING_ID]
#define MPLS_ELSPMAP_TOKEN_STACK_POOL_ID \
 MPLSRTRMemPoolIds[MAX_MPLSRTR_ELSPMAP_TOKEN_STACK_SIZING_ID]



/* Diffserv related conditions */

#define MPLS_DSTE_STATUS(x) \
    (gMplsIncarn[x].DiffServGlobal.u4DiffServStatus)

#define MPLS_DIFFSERV_CLASS_TYPE_ENTRY(u4Incarn, u4ClassId)\
        (gMplsIncarn[u4Incarn].DsTeGlobal.aMplsDsTeClassType[u4ClassId])

#define MPLS_DIFFSERV_TRAFFIC_CLASS_MAP(u4Incarn, u4ClassId, u4TrafficClassId)\
        (gMplsIncarn[u4Incarn].DsTeGlobal.aMplsDsTeClassTypeTcMap[u4ClassId][u4TrafficClassId])

#define MPLS_DIFFSERV_TE_CLASS_MAP(u4Incarn, u4ClassId, u4TeClassPrio)\
        (gMplsIncarn[u4Incarn].DsTeGlobal.aMplsDsTeClassPrio[u4ClassId][u4TeClassPrio])

#define MPLS_DIFFSERV_TYPE(u4Incarn) \
        (gMplsIncarn[u4Incarn].DiffServGlobal.u1DiffServType)

#define MPLS_LSP_SERVICE_TYPE(pDiffServParams) \
        ((pDiffServParams)->u1ServiceType)

#define MPLS_ELSP_TYPE(pDiffServParams) \
        ((pDiffServParams)->unLspParams.ElspParams.u1ElspType)

#define MPLS_SIG_ELSP_MAP(pDiffServParams) \
        ((pDiffServParams)->unLspParams.ElspParams.pElspSigMap)

#define MPLS_PRECONF_ELSP_MAP(u4Incarn) \
        (gMplsIncarn[u4Incarn].DiffServGlobal.pElspPreConfMap)

#define MPLS_IF_PRECONF_ELSP_MAP(pOutIfEntry) \
        ((pOutIfEntry)->ElspIfParams.pOutIfPreConfMap)

#define MPLS_LLSP_DSCP(pDiffServParams) \
        ((pDiffServParams)->unLspParams.u1LlspPscDscp)

#define IS_MPLS_DIFFSERV_ENABLED(u4Incarn) \
        (MPLS_QOS_POLICY(u4Incarn) == MPLS_DIFF_SERV)

#define IS_MPLS_DIFFSERV_ELSP_ENABLED(u4Incarn) MPLS_TRUE

#define IS_MPLS_DIFFSERV_LLSP_ENABLED(u4Incarn) MPLS_TRUE

#define IS_MPLS_DIFFSERV_ELSP(pDiffServParams) \
        (MPLS_LSP_SERVICE_TYPE(pDiffServParams) == MPLS_DIFFSERV_ELSP)

#define IS_MPLS_DIFFSERV_SIG_ELSP(pDiffServParams) \
        (IS_MPLS_DIFFSERV_ELSP(pDiffServParams) && \
        (MPLS_ELSP_TYPE(pDiffServParams) == MPLS_DIFFSERV_SIG_ELSP))

#define IS_MPLS_DIFFSERV_PRECONF_ELSP(pDiffServParams) \
        (IS_MPLS_DIFFSERV_ELSP(pDiffServParams) && \
        (MPLS_ELSP_TYPE(pDiffServParams) == MPLS_DIFFSERV_PRECONF_ELSP))

#define IS_MPLS_DIFFSERV_LLSP(pDiffServParams) \
        (MPLS_LSP_SERVICE_TYPE(pDiffServParams) == MPLS_DIFFSERV_LLSP)

#define MPLS_DS_PROC_GET_DSCP_FROM_TOS(u1Tos, pu1Dscp) \
    *pu1Dscp = (UINT1)((u1Tos & MPLS_DSCP_BITMASK) >> MPLS_2_BIT_SHIFT);

#define MPLS_DS_PROC_GET_TOS_FROM_DSCP(u1Dscp, pu1Tos) \
    *pu1Tos = (UINT1)((u1Dscp << MPLS_2_BIT_SHIFT) & MPLS_DSCP_BITMASK);

#define MPLS_DS_PROC_MARK_EGRES(pDscpRecord, u1Flag) \
    (pDscpRecord)->u1IsEgres = u1Flag;

#define MPLS_DS_PROC_MARK_INGRES(pDscpRecord, u1Flag) \
    (pDscpRecord)->u1IsIngres = u1Flag;

/* Exported Prototypes from DiffServ Module */
UINT4 MplsDiffServGetPreConfMap ARG_LIST((UINT4 u4Incarn, tElspPhbMapEntry
aElspPhbMapEntry[]));

#endif /* MPLSDIFF_H */
/*---------------------------------------------------------------------------*/
/*                        End of file mplsdiff.h                             */
/*---------------------------------------------------------------------------*/
