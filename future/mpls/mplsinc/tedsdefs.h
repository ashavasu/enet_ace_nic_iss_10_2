/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: tedsdefs.h,v 1.7 2016/07/22 09:45:46 siva Exp $
 *******************************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tedsdef.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TE 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains constant definitions for the 
 *                             diffServ feature in TE module.   
 *---------------------------------------------------------------------------*/

#ifndef _TE_DS_DEF_H
#define _TE_DS_DEF_H

#define TE_NON_DIFFSERV_LSP      MPLS_GEN_LSP
#define TE_DS_ELSP               MPLS_DIFFSERV_ELSP
#define TE_DS_LLSP               MPLS_DIFFSERV_LLSP
#define TE_INTSERV               MPLS_INTSERV_LSP

#define MAX_EXP_BIT_VAL_8                   8  /* For SNMP backward compatibility, to remove in 9_2_0 */

#define TE_DS_PRECONF_ELSP       MPLS_DIFFSERV_PRECONF_ELSP
#define TE_DS_SIG_ELSP           MPLS_DIFFSERV_SIG_ELSP

#define TE_DS_ELSP_PHB_VALID     1

#define TE_DS_AVG_PHBS           4
#define TE_DS_AVG_PSCS           4
/***************************************************************************/
/*ClassType values */
#define TE_DS_CLASS_TYPE0        0
#define TE_DS_CLASS_TYPE1        1
#define TE_DS_CLASS_TYPE2        2
#define TE_DS_CLASS_TYPE3        3
#define TE_DS_CLASS_TYPE4        4

/*****************************************************************************/
/*DiffServ possible PHBs and PSCs*/
#define TE_DS_DF_DSCP            0x00
#define TE_DS_CS1_DSCP           0x08
#define TE_DS_CS2_DSCP           0x10
#define TE_DS_CS3_DSCP           0x18
#define TE_DS_CS4_DSCP           0x20
#define TE_DS_CS5_DSCP           0x28
#define TE_DS_CS6_DSCP           0x30
#define TE_DS_CS7_DSCP           0x38
#define TE_DS_EF_DSCP            0x2e
#define TE_DS_AF11_DSCP          0x0a
#define TE_DS_AF12_DSCP          0x0c
#define TE_DS_AF13_DSCP          0x0e
#define TE_DS_AF21_DSCP          0x12
#define TE_DS_AF22_DSCP          0x14
#define TE_DS_AF23_DSCP          0x16
#define TE_DS_AF31_DSCP          0x1a
#define TE_DS_AF32_DSCP          0x1c
#define TE_DS_AF33_DSCP          0x1e
#define TE_DS_AF41_DSCP          0x22
#define TE_DS_AF42_DSCP          0x24
#define TE_DS_AF43_DSCP          0x26
#define TE_DS_EF1_DSCP           0x3e
#define TE_DS_AF1_PSC_DSCP       TE_DS_AF11_DSCP
#define TE_DS_AF2_PSC_DSCP       TE_DS_AF21_DSCP
#define TE_DS_AF3_PSC_DSCP       TE_DS_AF31_DSCP
#define TE_DS_AF4_PSC_DSCP       TE_DS_AF41_DSCP

/* Flags to indicate whether the optional tlv is present or not */
#define TE_DS_DIFFSERV_TLV        0x0400
#define TE_DS_RSRC_PEROA_TLV      0x0200
#define TE_DS_CLASSTYPE_TLV       0x0100

#define TE_DS_MIN_EXP                   0
#define TE_DS_ELSPINFO_TABLE_DEF_OFFSET 14

/* Definitions used in CR-LDP/RSVP-TE */
#define TE_DS_MAX_NO_OF_CLASSTYPES      8
#define TE_DS_MAX_NO_OF_PHBS            14
#define TE_DS_MAX_NO_PSCS               13

/* Over Ride*/
#define TE_DS_OVERRIDE_SET               1
#define TE_DS_OVERRIDE_NOT_SET           2


#endif

/*---------------------------------------------------------------------------*/
/*                        End of file tedsdef.h                              */
/*----------------------------------------------------------------------------*/
