#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# $Id: make.h,v 1.10 2016/07/18 11:19:37 siva Exp $
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : MAKE.H                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       :                                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : Eg: GNU MAKE                                  |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX Portable                                |
# |                                                                          |
# |   DATE                   : 20th Febraury 2002                            |
# |                                                                          |
# |   DESCRIPTION            : Provide the following information in order -  |
# +--------------------------------------------------------------------------+

###########################################################################
#               COMPILATION SWITCHES                                      #
###########################################################################

# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME        = FutureMPLS

MPLS_RSVPTE         = YES
MPLS_LDP            = YES
MPLS_L2VPN          = YES
LANAI               = NO

############################################################################
#                         Directories                                      #
############################################################################

MPLS_BASE_DIR        = ${BASE_DIR}/mpls
MPLS_BASE_OBJ_DIR    = ${BASE_DIR}/mpls/obj
MPLS_COMN_INC_DIR    = ${MPLS_BASE_DIR}/mplsinc

MPLS_RTR_BASE_DIR    = ${MPLS_BASE_DIR}/mplsrtr
MPLS_RTR_INC_DIR     = ${MPLS_BASE_DIR}/mplsrtr/inc
MPLS_RTR_SRC_DIR     = ${MPLS_BASE_DIR}/mplsrtr/src
MPLS_RTR_OBJ_DIR     = ${MPLS_BASE_DIR}/mplsrtr/obj

MPLS_FM_BASE_DIR     = ${MPLS_BASE_DIR}/mplsfm
MPLS_FM_INC_DIR      = ${MPLS_FM_BASE_DIR}/inc
MPLS_FM_SRC_DIR      = ${MPLS_FM_BASE_DIR}/src
MPLS_FM_OBJ_DIR      = ${MPLS_FM_BASE_DIR}/obj

MPLS_TE_BASE_DIR     = ${MPLS_BASE_DIR}/te
MPLS_TE_INC_DIR      = ${MPLS_TE_BASE_DIR}/inc
MPLS_TE_SRC_DIR      = ${MPLS_TE_BASE_DIR}/src
MPLS_TE_OBJ_DIR      = ${MPLS_TE_BASE_DIR}/obj

MPLS_TC_BASE_DIR   = ${MPLS_BASE_DIR}/tc
MPLS_TC_INC_DIR    = ${MPLS_TC_BASE_DIR}/inc
MPLS_TC_SRC_DIR    = ${MPLS_TC_BASE_DIR}/src
MPLS_TC_OBJ_DIR    = ${MPLS_TC_BASE_DIR}/obj

MPLS_LDP_BASE_DIR    = ${MPLS_BASE_DIR}/ldp
MPLS_LDP_INC_DIR     = ${MPLS_LDP_BASE_DIR}/inc
MPLS_LDP_SRC_DIR     = ${MPLS_LDP_BASE_DIR}/src
MPLS_LDP_OBJ_DIR     = ${MPLS_LDP_BASE_DIR}/obj

MPLS_RSVPTE_BASE_DIR = ${MPLS_BASE_DIR}/rsvpte
MPLS_RSVPTE_INC_DIR  = ${MPLS_RSVPTE_BASE_DIR}/inc
MPLS_RSVPTE_SRC_DIR  = ${MPLS_RSVPTE_BASE_DIR}/src
MPLS_RSVPTE_OBJ_DIR  = ${MPLS_RSVPTE_BASE_DIR}/obj

MPLS_LBLMGR_BASE_DIR = ${MPLS_BASE_DIR}/lblmgr
MPLS_LBLMGR_INC_DIR  = ${MPLS_LBLMGR_BASE_DIR}/inc
MPLS_LBLMGR_SRC_DIR  = ${MPLS_LBLMGR_BASE_DIR}/src
MPLS_LBLMGR_OBJ_DIR  = ${MPLS_LBLMGR_BASE_DIR}/obj

MPLS_L2VPN_BASE_DIR   = ${MPLS_BASE_DIR}/l2vpn
MPLS_L2VPN_INC_DIR    = ${MPLS_L2VPN_BASE_DIR}/inc
MPLS_L2VPN_SRC_DIR    = ${MPLS_L2VPN_BASE_DIR}/src
MPLS_L2VPN_OBJ_DIR    = ${MPLS_L2VPN_BASE_DIR}/obj


MPLS_L3VPN_BASE_DIR   = ${MPLS_BASE_DIR}/l3vpn
MPLS_L3VPN_INC_DIR    = ${MPLS_L3VPN_BASE_DIR}/inc
MPLS_L3VPN_SRC_DIR    = ${MPLS_L3VPN_BASE_DIR}/src
MPLS_L3VPN_OBJ_DIR    = ${MPLS_L3VPN_BASE_DIR}/obj


MPLS_OAM_BASE_DIR   = ${MPLS_BASE_DIR}/oam
MPLS_OAM_INC_DIR    = ${MPLS_OAM_BASE_DIR}/inc
MPLS_OAM_SRC_DIR    = ${MPLS_OAM_BASE_DIR}/src
MPLS_OAM_OBJ_DIR    = ${MPLS_OAM_BASE_DIR}/obj

MPLSTEST_BASE_DIR   = ${MPLS_BASE_DIR}/test
MPLSTEST_INC_DIR    = ${MPLSTEST_BASE_DIR}/inc
MPLSTEST_SRC_DIR    = ${MPLSTEST_BASE_DIR}/src
MPLSTEST_OBJ_DIR    = ${MPLSTEST_BASE_DIR}/obj

MPLS_CDM_BASE_DIR   = ${MPLS_BASE_DIR}/mplsdb
MPLS_CDM_OBJ_DIR    = ${MPLS_BASE_DIR}/mplsdb


MPLS_LSPP_OBJ_DIR   = ${MPLS_BASE_DIR}/lspp/obj
MPLS_LSPP_BASE_DIR  = ${MPLS_BASE_DIR}/lspp
MPLS_LSPP_INC_DIR   = ${MPLS_LSPP_BASE_DIR}/inc
MPLS_LSPP_SRC_DIR   = ${MPLS_LSPP_BASE_DIR}/src

MPLS_RFC6374_OBJ_DIR   = ${MPLS_BASE_DIR}/pm/rfc6374/obj
MPLS_RFC6374_BASE_DIR  = ${MPLS_BASE_DIR}/pm/rfc6374
MPLS_RFC6374_INC_DIR   = ${MPLS_RFC6374_BASE_DIR}/inc
MPLS_RFC6374_SRC_DIR   = ${MPLS_RFC6374_BASE_DIR}/src

############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

MPLS_PROJECT_INCLUDES  = -I${MPLS_COMN_INC_DIR} \
                         -I${MPLS_FM_INC_DIR} \
                         -I${MPLS_TE_INC_DIR} \
                         -I${MPLS_LDP_INC_DIR} \
                         -I${MPLS_RSVPTE_INC_DIR} \
                         -I${MPLS_STAT_INC_DIR} \
                         -I${MPLS_L2VPN_INC_DIR} \
                         -I${MPLS_TE_INC_DIR} \
                         -I${MPLSTEST_INC_DIR} \
                         -I${MPLS_OAM_INC_DIR} \

MPLS_GLOBAL_INCLUDES = $(MPLS_PROJECT_INCLUDES) \
                       $(COMMON_INCLUDE_DIRS)

#############################################################################

PROJECT_DEPENDENCIES    = $(COMMON_DEPENDENCIES) \
                          $(PROJECT_BASE_DIR)/make.h \
                          $(PROJECT_BASE_DIR)/Makefile
