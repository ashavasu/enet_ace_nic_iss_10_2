/********************************************************************
 ** Copyright (C) 2008 Aricent Inc . All Rights Reserved
 **
 ** $Id: mplstest.c,v 1.5 2014/02/27 13:49:47 siva Exp $
 **
 ** Description: MPLS API  UT Test cases.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/
#ifndef __MPLS_TEST_C__
#define __MPLS_TEST_C__

#include "lr.h"
#include "tcp.h"
#include "mpls.h"
#include "mplcmndb.h"
#include "mplsftn.h"
#include "oaminc.h"
#include "mpcmnapi.h"
#include "mplslsr.h"
#include "l2vpextn.h"
#include "arp.h"
#include "iss.h"
#include "utlmacro.h"
#include "mplscli.h"
#include "mplsutl.h"
#include "teincs.h"
#include "fsmptelw.h"
#include "fslsrlw.h"
#include "teclip.h"

VOID                CallUTCases (VOID);
VOID                MplsTestExecUT (INT4 i4UtId);
VOID                FormPacket (tCRU_BUF_CHAIN_HEADER ** ppBuf);
PRIVATE VOID        DeleteConf1 (VOID);
PRIVATE VOID        CreateConf1 (VOID);
INT4                Test_OAM_HANDLE_DB_MEG (VOID);
INT4                Test_OAM_HANDLE_DB_TNL (VOID);
INT4                Test_OAM_HANDLE_DB_PW (VOID);
INT4                Test_OAM_HANDLE_BFD_MEG (VOID);
INT4                Test_OAM_HANDLE_BFD_TNL (VOID);
INT4                Test_OAM_HANDLE_BFD_PW (VOID);
INT4                Test_MplsOamConsolidateAndNotify (VOID);

INT4                Test_MplsOamHandlePathStatusChgForPw (VOID);
INT4                Test_MplsOamUpdateSessionParams_Meg (VOID);
INT4                Test_MplsOamUpdateSessionParams_Tnl (VOID);
INT4                Test_MplsOamUpdateSessionParams_Pw (VOID);
INT4                Test_MplsOamRegWithMegForNotif_Arp (VOID);
INT4                Test_MplsOamGetMegInfo_MegIdFromMegName (VOID);
INT4                Test_MplsOamGetMegInfo_MegNameFromMegId (VOID);
INT4                Test_MplsOamGetMegInfo_MegInfoFromMegId (VOID);
INT4                Test_MplsGetTunnelInfo (VOID);
INT4                Test_MplsGetPwInfo_PwIndex (VOID);
INT4                Test_Test_MplsGetPwInfo_FromVcId (VOID);
INT4                Test_Test_MplsGetPwInfo_FromGenType2 (VOID);
INT4                Test_Test_MplsGetPwInfo_FromPwIdFec (VOID);
INT4                Test_Test_MplsGetPwInfo_FromGenFec (VOID);
INT4                Test_MplsGetLspInfo (VOID);
INT4                Test_MplsGetNodeId_RouterId (VOID);
INT4                Test_MplsGetNodeId_FromGlobalNodeId (VOID);
INT4                Test_MplsGetNodeId_FromLocalNum (VOID);
INT4                Test_MplsGetNodeId_LocalNumFrmGlobalNodeId (VOID);

INT4                Test_MplsGetServicePointerOid (VOID);
INT4                Test_MplsGetRevPathFromFwdPath (VOID);
INT4                Test_MplsGetPathInfoFromInLblInfo (VOID);

PRIVATE INT4        Test_TeCmnExtGetDirectionFromTnl (VOID);
PRIVATE INT4        Test_TeCmnExtUpdateArpResolveStatus (VOID);
PRIVATE INT4        Test_TeCmnExtSetOperStatusAndProgHw (VOID);
PRIVATE INT4        Test_TeCmnExtGetBkpTnlFrmInUseProtTnl (VOID);
PRIVATE INT4        Test_TeCmnExtUpdateTnlProtStatus (VOID);
PRIVATE INT4        Test_FsTeMibIndexValidationAndRetrieving (VOID);
PRIVATE INT4        Test_nmhGetFsMplsTunnelType (VOID);
PRIVATE INT4        Test_nmhGetFsMplsTunnelLSRIdMapInfo (VOID);
PRIVATE INT4        Test_nmhGetFsMplsTunnelMode (VOID);
PRIVATE INT4        Test_nmhGetFsMplsTunnelProactiveSessIndex (VOID);
PRIVATE INT4        Test_GmplsSpecificAndDummyNmh (VOID);
PRIVATE INT4        Test_nmhSetFsMplsTunnelType (VOID);
PRIVATE INT4        Test_nmhSetFsMplsTunnelLSRIdMapInfo (VOID);
PRIVATE INT4        Test_nmhSetFsMplsTunnelMode (VOID);
PRIVATE INT4        Test_nmhTestv2FsMplsTunnelType (VOID);
PRIVATE INT4        Test_nmhTestv2FsMplsTunnelLSRIdMapInfo (VOID);
PRIVATE INT4        Test_nmhTestv2FsMplsTunnelMode (VOID);
PRIVATE INT4        Test_nmhGetFsMplsTpTunnelDestTunnelIndex (VOID);
PRIVATE INT4        Test_nmhGetFsMplsTpTunnelDestTunnelLspNum (VOID);
PRIVATE INT4        Test_nmhSetFsMplsTpTunnelDestTunnelIndex (VOID);
PRIVATE INT4        Test_nmhSetFsMplsTpTunnelDestTunnelLspNum (VOID);
PRIVATE INT4        Test_nmhTestv2FsMplsTpTunnelDestTunnelIndex (VOID);
PRIVATE INT4        Test_nmhTestv2FsMplsTpTunnelDestTunnelLspNum (VOID);
PRIVATE INT4        Test_FsLsrMibIndexValidationAndRetrieving (VOID);
PRIVATE INT4        Test_FsLsrMibDummyNmh (VOID);
PRIVATE INT4        Test_nmhGetFsMplsInSegmentDirection (VOID);
PRIVATE INT4        Test_nmhGetFsMplsOutSegmentDirection (VOID);
PRIVATE INT4        Test_nmhSetFsMplsInSegmentDirection (VOID);
PRIVATE INT4        Test_nmhSetFsMplsOutSegmentDirection (VOID);
PRIVATE INT4        Test_nmhTestv2FsMplsInSegmentDirection (VOID);
PRIVATE INT4        Test_nmhTestv2FsMplsOutSegmentDirection (VOID);
PRIVATE INT4        Test_TeCliGetTnlIndices (VOID);
PRIVATE INT4        Test_TeUpdateTnlStatus (VOID);
PRIVATE INT4        Test_TeValidateAndGetIngressId (VOID);
PRIVATE INT4        Test_VerifyAllStaticTunnels (VOID);
PRIVATE INT4        Test_TeCmnExtNotifyTnlStatusToMeg (VOID);
INT4                Test_MplsOamTxPacketHandleFromApp (VOID);
INT4                Test_MplsOamUpdateLpsStatus (VOID);
INT4                Test_MplsUtilFillMplsAchTlvHdr (VOID);
INT4                Test_MplsUtilFillMplsAchHdr (VOID);
INT4                Test_MplsUtilFillMplsHdr (VOID);
INT4                Test_MplsUtlFillMplsAchTlv (VOID);

VOID
CallUTCases (VOID)
{
    UINT1               u1Count = 1;

    for (u1Count = 1; u1Count < 70; u1Count++)
    {
        MplsTestExecUT (u1Count);
    }
}

VOID
MplsTestExecUT (INT4 i4UtId)
{
    UINT4               u4RetVal = OSIX_FAILURE;
    switch (i4UtId)
    {
        case 1:
            u4RetVal = Test_OAM_HANDLE_DB_MEG ();
            break;
        case 2:
            u4RetVal = Test_OAM_HANDLE_DB_TNL ();
            break;
        case 3:
            u4RetVal = Test_OAM_HANDLE_DB_PW ();
            break;
        case 4:
            u4RetVal = Test_OAM_HANDLE_BFD_MEG ();
            break;
        case 5:
            u4RetVal = Test_OAM_HANDLE_BFD_TNL ();
            break;
        case 6:
            u4RetVal = Test_OAM_HANDLE_BFD_PW ();
            break;
        case 7:
            u4RetVal = Test_MplsGetTunnelInfo ();
            break;
        case 8:
            u4RetVal = Test_MplsGetLspInfo ();
            break;
        case 9:
            u4RetVal = Test_MplsGetNodeId_RouterId ();
            break;
        case 10:
            u4RetVal = Test_MplsGetNodeId_FromGlobalNodeId ();
            break;
        case 11:
            u4RetVal = Test_MplsGetNodeId_FromLocalNum ();
            break;
        case 12:
            u4RetVal = Test_MplsGetNodeId_LocalNumFrmGlobalNodeId ();
            break;
        case 13:
            u4RetVal = Test_MplsUtlFillMplsAchTlv ();
            break;
        case 14:
            u4RetVal = Test_MplsOamUpdateSessionParams_Meg ();
            break;
        case 15:
            u4RetVal = Test_MplsOamUpdateSessionParams_Tnl ();
            break;
        case 16:
            u4RetVal = Test_MplsOamUpdateSessionParams_Pw ();
            break;
        case 17:
            u4RetVal = Test_MplsOamRegWithMegForNotif_Arp ();
            break;
        case 18:
            u4RetVal = Test_MplsOamGetMegInfo_MegIdFromMegName ();
            break;
        case 19:
            u4RetVal = Test_MplsOamGetMegInfo_MegNameFromMegId ();
            break;
        case 20:
            u4RetVal = Test_MplsOamGetMegInfo_MegInfoFromMegId ();
            break;
        case 21:
            u4RetVal = Test_MplsGetPwInfo_PwIndex ();
            break;
        case 22:
            u4RetVal = Test_Test_MplsGetPwInfo_FromVcId ();
            break;
        case 23:
            u4RetVal = Test_Test_MplsGetPwInfo_FromGenType2 ();
            break;
        case 24:
            u4RetVal = Test_Test_MplsGetPwInfo_FromGenFec ();
            break;
        case 25:
            u4RetVal = Test_Test_MplsGetPwInfo_FromPwIdFec ();
            break;
        case 26:
            u4RetVal = Test_MplsGetServicePointerOid ();
            break;
        case 27:
            u4RetVal = Test_MplsGetRevPathFromFwdPath ();
            break;
        case 28:
            u4RetVal = Test_MplsGetPathInfoFromInLblInfo ();
            break;
        case 29:
            u4RetVal = Test_MplsOamTxPacketHandleFromApp ();
            break;
        case 30:
            u4RetVal = Test_MplsOamUpdateLpsStatus ();
            break;
        case 31:
            u4RetVal = Test_MplsUtilFillMplsHdr ();
            break;
        case 32:
            u4RetVal = Test_MplsUtilFillMplsAchHdr ();
            break;
        case 33:
            u4RetVal = Test_MplsUtilFillMplsAchTlvHdr ();
            break;
        case 34:
            CreateConf1 ();
            u4RetVal = Test_TeCmnExtGetDirectionFromTnl ();
            DeleteConf1 ();
            break;
        case 35:
            CreateConf1 ();
            u4RetVal = Test_TeCmnExtUpdateArpResolveStatus ();
            DeleteConf1 ();
            break;
        case 36:
            CreateConf1 ();
            u4RetVal = Test_TeCmnExtSetOperStatusAndProgHw ();
            DeleteConf1 ();
            break;
        case 37:
            CreateConf1 ();
            u4RetVal = Test_TeCmnExtGetBkpTnlFrmInUseProtTnl ();
            DeleteConf1 ();
            break;
        case 38:
            CreateConf1 ();
            u4RetVal = Test_TeCmnExtUpdateTnlProtStatus ();
            DeleteConf1 ();
            break;
        case 39:
            CreateConf1 ();
            u4RetVal = Test_FsTeMibIndexValidationAndRetrieving ();
            DeleteConf1 ();
            break;
        case 40:
            CreateConf1 ();
            u4RetVal = Test_nmhGetFsMplsTunnelType ();
            DeleteConf1 ();
            break;
        case 41:
            CreateConf1 ();
            u4RetVal = Test_nmhGetFsMplsTunnelLSRIdMapInfo ();
            DeleteConf1 ();
            break;
        case 42:
            CreateConf1 ();
            u4RetVal = Test_nmhGetFsMplsTunnelMode ();
            DeleteConf1 ();
            break;
        case 43:
            CreateConf1 ();
            u4RetVal = Test_nmhGetFsMplsTunnelProactiveSessIndex ();
            DeleteConf1 ();
            break;
        case 44:
            CreateConf1 ();
            u4RetVal = Test_GmplsSpecificAndDummyNmh ();
            DeleteConf1 ();
            break;
        case 45:
            CreateConf1 ();
            u4RetVal = Test_nmhSetFsMplsTunnelType ();
            DeleteConf1 ();
            break;
        case 46:
            CreateConf1 ();
            u4RetVal = Test_nmhSetFsMplsTunnelLSRIdMapInfo ();
            DeleteConf1 ();
            break;
        case 47:
            CreateConf1 ();
            u4RetVal = Test_nmhSetFsMplsTunnelMode ();
            DeleteConf1 ();
            break;
        case 48:
            CreateConf1 ();
            u4RetVal = Test_nmhTestv2FsMplsTunnelType ();
            DeleteConf1 ();
            break;
        case 49:
            CreateConf1 ();
            u4RetVal = Test_nmhTestv2FsMplsTunnelLSRIdMapInfo ();
            DeleteConf1 ();
            break;
        case 50:
            CreateConf1 ();
            u4RetVal = Test_nmhTestv2FsMplsTunnelMode ();
            DeleteConf1 ();
            break;
        case 51:
            CreateConf1 ();
            u4RetVal = Test_nmhGetFsMplsTpTunnelDestTunnelIndex ();
            DeleteConf1 ();
            break;
        case 52:
            CreateConf1 ();
            u4RetVal = Test_nmhGetFsMplsTpTunnelDestTunnelLspNum ();
            DeleteConf1 ();
            break;
        case 53:
            CreateConf1 ();
            u4RetVal = Test_nmhSetFsMplsTpTunnelDestTunnelIndex ();
            DeleteConf1 ();
            break;
        case 54:
            CreateConf1 ();
            u4RetVal = Test_nmhSetFsMplsTpTunnelDestTunnelLspNum ();
            DeleteConf1 ();
            break;
        case 55:
            CreateConf1 ();
            u4RetVal = Test_nmhTestv2FsMplsTpTunnelDestTunnelIndex ();
            DeleteConf1 ();
            break;
        case 56:
            CreateConf1 ();
            u4RetVal = Test_nmhTestv2FsMplsTpTunnelDestTunnelLspNum ();
            DeleteConf1 ();
            break;
        case 57:
            CreateConf1 ();
            u4RetVal = Test_FsLsrMibIndexValidationAndRetrieving ();
            DeleteConf1 ();
            break;
        case 58:
            CreateConf1 ();
            u4RetVal = Test_FsLsrMibDummyNmh ();
            DeleteConf1 ();
            break;
        case 59:
            CreateConf1 ();
            u4RetVal = Test_nmhGetFsMplsInSegmentDirection ();
            DeleteConf1 ();
            break;
        case 60:
            CreateConf1 ();
            u4RetVal = Test_nmhGetFsMplsOutSegmentDirection ();
            DeleteConf1 ();
            break;
        case 61:
            CreateConf1 ();
            u4RetVal = Test_nmhSetFsMplsInSegmentDirection ();
            DeleteConf1 ();
            break;
        case 62:
            CreateConf1 ();
            u4RetVal = Test_nmhSetFsMplsOutSegmentDirection ();
            DeleteConf1 ();
            break;
        case 63:
            CreateConf1 ();
            u4RetVal = Test_nmhTestv2FsMplsInSegmentDirection ();
            DeleteConf1 ();
            break;
        case 64:
            CreateConf1 ();
            u4RetVal = Test_nmhTestv2FsMplsOutSegmentDirection ();
            DeleteConf1 ();
            break;
        case 65:
            CreateConf1 ();
            u4RetVal = Test_TeCliGetTnlIndices ();
            DeleteConf1 ();
            break;
        case 66:
            CreateConf1 ();
            u4RetVal = Test_TeCmnExtNotifyTnlStatusToMeg ();
            DeleteConf1 ();
            break;
        case 67:
            CreateConf1 ();
            u4RetVal = Test_TeUpdateTnlStatus ();
            DeleteConf1 ();
            break;
        case 68:
            CreateConf1 ();
            u4RetVal = Test_TeValidateAndGetIngressId ();
            DeleteConf1 ();
            break;
        case 69:
            CreateConf1 ();
            u4RetVal = Test_VerifyAllStaticTunnels ();
            DeleteConf1 ();
            break;
        default:
            printf ("%% No test case is defined for ID: %d\r\n", i4UtId);
            break;
    }

    /* Display the result */
    if (u4RetVal == OSIX_FAILURE)
    {
        printf ("UNIT TEST ID: %-2d %-2s\r\n", i4UtId, "FAILED");
    }
    else
    {
        printf ("UNIT TEST ID: %-2d %-2s\r\n", i4UtId, "PASSED");
    }
}

INT4
Test_OAM_HANDLE_DB_MEG (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      NULL, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (MplsApiHandleExternalRequest (100, &InMplsApiInfo, NULL)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;

    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    /* Failure case:  Oam module disabled  */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Out Api Info NULL  Failure case-1 */
    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* dont create meg entry  Failure case-2 */
    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid Path Type  Failure case-3 */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = 100;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Create Meg entry with Megindex -1 , Meindex -1 MpIndex -1 */
    /* Success case 1 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("set service type lsp");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success Case - Down indication for MEg */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success Case - Down indication for MEg */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success Case - Up indication for MEg */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200010 20.0.0.2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Failure case: No Meg Entry Created and mapped with the service */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case: No Meg Entry Created and mapped with the service */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* Failure case: No Meg Entry Created and mapped with the service */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("mpls oam mep service tp lsp 1 1 5 6");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Failure Case: Meg Entry Created and mapped with the service 
     * Gioven an invalid Path Type*/
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_NONTE_LSP;
    InMplsApiInfo.InServicePathId.TnlId.u4SrcTnlNum = 2;
    InMplsApiInfo.InServicePathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InServicePathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case: Meg Entry Created and mapped with the service 
     * Gioven an invalid Path Type*/
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InServicePathId.TnlId.u4SrcTnlNum = 2;
    InMplsApiInfo.InServicePathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InServicePathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case: Path Status as Invalid */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = 100;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InServicePathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InServicePathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* Failure Case: Invalid source */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = ELPS_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InServicePathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InServicePathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* Failure Case: Invalid source */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = ELPS_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InServicePathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InServicePathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success Case: Meg Entry Created and mapped with the service */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InServicePathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InServicePathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: Path Status as Down */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InServicePathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InServicePathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InServicePathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls1");
    CliExecuteAppCmd ("service tp1");
    CliExecuteAppCmd ("mpls oam mep service tp1 pw 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Failure Case: Path Type and Service Type mismatch */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls1");
    CliExecuteAppCmd ("set service type pw");
    CliExecuteAppCmd ("mpls oam mep service tp1 pw 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: Pw Case Path Status as Down */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: Pw Case Path Status as down */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: Pw Case Path Status as down */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliExecuteAppCmd ("mpls oam meg mpls1");
    CliExecuteAppCmd ("no service tp1");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls1");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_OAM_HANDLE_DB_TNL (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 10;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 7;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 8;

    /* Failure case : Tunnel Entry not created  */
    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success Case : Tunnel Entry created and Status as UP */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200010 20.0.0.2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success Case : Tunnel Entry created and Status as Down */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Failure Case : Ftn Entry not created and Status as Down */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_NONTE_LSP;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.LspId.PeerAddr.u4_addr[0] = 0x23000001;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls binding ipv4 35.0.0.0 255.0.0.0 te 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Failure Case : Ftn Entry created and Status as Up */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_NONTE_LSP;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.LspId.PeerAddr.u4_addr[0] = 0x23000001;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls binding ipv4 35.0.0.0 255.0.0.0");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("no int mplstunnel 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_OAM_HANDLE_DB_PW (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    /* Failure Case dont create the entry table */
    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case create the entry table  with Status UP */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success case create the entry table  with Status Dowm */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = MPLSDB_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_OAM_HANDLE_BFD_MEG (VOID)
{
    UINT4               u4MainReqType = MPLS_OAM_HANDLE_PATH_STATUS_CHG;
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200010 20.0.0.2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("mpls oam mep service tp lsp 1 1 5 6");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    /* Failure case : No session index updated */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case : Session Index update for the Meg */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 1;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_SESSION_PARAMS,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success case : Update Path status as UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success case : Update Path status as DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success case : Update Path status as DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success case : Update Path status as UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success case: Valid Meg index with Pro-active Session Id as 0 */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_SESSION_PARAMS,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("mpls oam mep service tp pw 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Failure Case: Path Type and Service Type mismatch */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("set service type pw");
    CliExecuteAppCmd ("mpls oam mep service tp pw 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success case : Session Index update for the Meg */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 1;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_SESSION_PARAMS,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: Pw Case Path Status as Up */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: Pw Case Path Status as down */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: Pw Case Path Status as down */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Success Case: Pw Case Path Status as Up */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Success Case: Pw Case Path Status as down */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: Pw Case Path Status as UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: Pw Case Path Status as down */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: Pw Case Path Status as UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 0;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 0;
    InMplsApiInfo.InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InServicePathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success case: Valid Meg index with Pro-active Session Id as 0 */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_SESSION_PARAMS,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_OAM_HANDLE_BFD_TNL (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 10;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 7;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 8;

    /* Failure Case : Tunnel Entry doesnt exist */
    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case : Tunnel Entry exist, but proactive session not updated */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200010 20.0.0.2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_SESSION_PARAMS,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* success case : Valid Tunnel Entry with Proactive Session Index as 1  */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 1;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_SESSION_PARAMS,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success case: Tunnel Entry with Status UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;
    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success case: Tunnel Entry with Status DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;
    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Success case: Tunnel Entry with Status DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success case: Tunnel Entry with Status UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Success case: Tunnel Entry with Status UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success case: Tunnel Entry with Status DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Success case: Tunnel Entry with Status UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_HANDLE_PATH_STATUS_CHG,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* success case : Valid Tunnel Entry with Proactive Session Index as 0  */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_SESSION_PARAMS,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;

}

INT4
Test_OAM_HANDLE_BFD_PW (VOID)
{
    UINT4               u4MainReqType = MPLS_OAM_HANDLE_PATH_STATUS_CHG;
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    /* Failure Case: Pw Entry doesn't exist */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case : create the MPLS PW entry without Proactive Session index */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: PW Entry created with Proactive Session Index as 1 */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 1;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_SESSION_PARAMS,
                                      &InMplsApiInfo, &OutMplsApiInfo)
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: PW entry with  Path status as UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: PW entry with  Path status as DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: PW entry with  Path status as DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: PW entry with  Path status as UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd ("pseudowire-oam pwid 1 oam disable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Failure Case: PW entry with Oam status disabled  Path status as UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Failure Case: PW entry with Oam Status disabled  Path status as DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd ("pseudowire-oam pwid 1 oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Success Case: PW entry with Oam status enabled  Path status as UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: PW entry with Oam Status enabled  Path status as DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly inactive");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Success Case: PW entry with Admin down  Path status as UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: PW entry with Admin down  Path status as DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Success Case: PW entry with Admin UP  Path status as UP */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Success Case: PW entry with Admin UP  Path status as DOWN */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Success Case: PW Entry created with Proactive Session Index as 0 */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_SESSION_PARAMS,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls l2transport detail");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;

}

INT4
Test_MplsOamUpdateSessionParams_Meg (VOID)
{
    UINT4               u4MainReqType = MPLS_OAM_UPDATE_SESSION_PARAMS;
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = 100;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    /* Failure case:  Invalid Path Type is given  */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_NONTE_LSP;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    /* Failure case:  Not Supported Path Type is given  */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /* Failure case:  Oam module disabled  */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Failure case:  Me Table not configured  */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /* Success case: Valid Meg index with Pro-active Session Id as 1 */

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("set service type lsp");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 1;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success case: Valid Meg index with Pro-active Session Id as 0 */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsOamUpdateSessionParams_Tnl (VOID)
{

    UINT4               u4MainReqType = MPLS_OAM_UPDATE_SESSION_PARAMS;
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 10;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 8;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 9;

    /* Failure case : Tunnel Entry not created */

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /* success case : Valid Tunnel Entry with Proactive Session Index as 1  */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200010 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200009 vlan 2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 1;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* success case : Valid Tunnel Entry with Proactive Session Index as 0  */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_MplsOamUpdateSessionParams_Pw (VOID)
{
    UINT4               u4MainReqType = MPLS_OAM_UPDATE_SESSION_PARAMS;
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    /* Failure Case: PW Entry not created */
    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return (OSIX_FAILURE);
    }

    /* Success Case: PW Entry created with Proactive Session Index as 1 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 1;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success Case: PW Entry created with Proactive Session Index as 0 */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InOamSessionParams.u4ProactiveSessIndex = 0;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return (OSIX_SUCCESS);

}

INT4
Test_MplsOamRegWithMegForNotif_Arp (VOID)
{
    UINT4               u4MainReqType = MPLS_OAM_REGISTER_MEG_FOR_NOTIF;
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_APP_REGISTER;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = ELPS_MODULE;
    InMplsApiInfo.InRegParams.u4Events = 1;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InRegParams.u4ModId = MPLS_ELPS_APP_ID;

    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    /* failure case with out configuration with oam disable */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* Failure case with  and u4Modid other than ELPS */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InRegParams.u4ModId = 5;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /*  Failure case with  and u4Event =0   */

    InMplsApiInfo.InRegParams.u4ModId = MPLS_ELPS_APP_ID;
    InMplsApiInfo.InRegParams.u4Events = 0;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case with path type other than MEGID */
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InRegParams.pFnRcvPkt = NULL;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;
    InMplsApiInfo.InRegParams.u4Events = 1;
    InMplsApiInfo.InRegParams.u4ModId = MPLS_ELPS_APP_ID;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case with out the configurations of MplsMe table */

    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;
    InMplsApiInfo.u4SubReqType = MPLS_APP_REGISTER;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InRegParams.u4Events = 1;
    InMplsApiInfo.InRegParams.pFnRcvPkt = NULL;
    InMplsApiInfo.InRegParams.u4ModId = MPLS_ELPS_APP_ID;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case with MPLS_APP_REGISTER  */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("set service type lsp");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_APP_REGISTER;
    InMplsApiInfo.InRegParams.u4Events = 1;
    InMplsApiInfo.InRegParams.u4ModId = MPLS_ELPS_APP_ID;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InRegParams.pFnRcvPkt = NULL;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success case with the MPLS_APP_DEREGISTER */
    InMplsApiInfo.u4SubReqType = MPLS_APP_DEREGISTER;
    InMplsApiInfo.InRegParams.u4Events = 1;
    InMplsApiInfo.InRegParams.u4ModId = MPLS_ELPS_APP_ID;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InRegParams.pFnRcvPkt = NULL;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsOamGetMegInfo_MegIdFromMegName (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    UINT4               u4MainReqType = MPLS_OAM_GET_MEG_INFO;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    /* Failure case : Out Api Info as NULL */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo, NULL) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    InMplsApiInfo.u4SubReqType = MPLS_GET_MEG_ID_FROM_MEG_NAME;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MegName, "mpls");
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MeName, "tp");

    /* failure case : oam disable */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case : Oam enable but entry doesnt exist */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    InMplsApiInfo.u4SubReqType = MPLS_GET_MEG_ID_FROM_MEG_NAME;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MegName, "mpls");
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MeName, "tp");

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case with config */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("set service type lsp");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    InMplsApiInfo.u4SubReqType = MPLS_GET_MEG_ID_FROM_MEG_NAME;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MegName, "mpls");
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MeName, "tp");

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsOamGetMegInfo_MegNameFromMegId (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;
    UINT4               u4MainReqType = MPLS_OAM_GET_MEG_INFO;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_MEG_NAME_FROM_MEG_ID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;

    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    /* Failure case : Meg not created */

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case with out Me entry Table */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("set service type lsp");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case with Meg and Me  Table updation */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200010 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200009 vlan 2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("mpls oam mep service tp lsp 1 1 5 6");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_MEG_NAME_FROM_MEG_ID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;

    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (STRCMP (OutMplsApiInfo.OutPathId.MegMeName.au1MegName, "mpls") != 0)
    {
        return OSIX_FAILURE;
    }
    if (STRCMP (OutMplsApiInfo.OutPathId.MegMeName.au1MeName, "tp") != 0)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no mpls oam mep service tp");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsOamGetMegInfo_MegInfoFromMegId (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;
    UINT4               u4MainReqType = MPLS_OAM_GET_MEG_INFO;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

#if 0
    /* Failure case with service type as Section */

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
#endif
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200010 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200009 vlan 2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("mpls oam mep service tp lsp 1 1 5 6");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_MEG_INFO_FROM_MEG_ID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    /* Success case with Meg and Me  Table updation and service type as 
     * OAM_SERVICE_TYPE_LSP for Me Table entry*/

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success case with Meg and Me  Table updation and service type as
     *      * OAM_SERVICE_TYPE_PW for Me Table entry*/
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no mpls oam mep service tp");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls1");
    CliExecuteAppCmd ("service tp1");
    CliExecuteAppCmd ("set service type pw");
    CliExecuteAppCmd ("mpls oam mep service tp1 pw 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_MEG_INFO_FROM_MEG_ID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    InMplsApiInfo.InPathId.MegId.u4MegIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MeIndex = 1;
    InMplsApiInfo.InPathId.MegId.u4MpIndex = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls1");
    CliExecuteAppCmd ("no mpls oam mep service tp1");
    CliExecuteAppCmd ("no service tp1");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls1");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsGetTunnelInfo (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    UINT4               u4MainReqType = MPLS_GET_TUNNEL_INFO;
    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    /* Failure case: Invalid node type given */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo, NULL) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case: Invalid node type given */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case: Invalid Source Global id - Node id given */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsGlobalNodeId.u4GlobalId = 10;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsGlobalNodeId.u4NodeId = 10;

    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case: Invalid Destination Global id - Node id given */
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsGlobalNodeId.u4GlobalId = 100;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsGlobalNodeId.u4NodeId = 10;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsGlobalNodeId.u4GlobalId = 20;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsGlobalNodeId.u4NodeId = 10;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case: valid Source & Destination Global id - Node id given 
     * but Tunnel entry not created*/
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsGlobalNodeId.u4GlobalId = 100;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsGlobalNodeId.u4NodeId = 10;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsGlobalNodeId.u4GlobalId = 200;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsGlobalNodeId.u4NodeId = 20;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200010 20.0.0.2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Success case with TeTnl Table creation with local map num */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Success case with TeTnl Table creation with global- node id */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsGlobalNodeId.u4GlobalId = 100;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsGlobalNodeId.u4NodeId = 10;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsGlobalNodeId.u4GlobalId = 200;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsGlobalNodeId.u4NodeId = 20;

    InMplsApiInfo.InPathId.TnlId.SrcNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.u4NodeType =
        MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsGetPwInfo_PwIndex (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    UINT4               u4MainReqType = MPLS_GET_PW_INFO;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.PwId.u4PwIndex = 1;

    /* Failure case with out creating the Tables. */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case with creating the Entry and configurations */

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.PwId.u4PwIndex = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;

    }
    return OSIX_SUCCESS;
}

INT4
Test_Test_MplsGetPwInfo_FromVcId (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;
    UINT4               u4MainReqType = MPLS_GET_PW_INFO;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_PW_INFO_FROM_VCID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.PwId.u4VcId = 2;

    /* Failure case with out creating the Tables. */

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }
    /* Success case with creating the Entry and configurations */
    InMplsApiInfo.u4SubReqType = MPLS_GET_PW_INFO_FROM_VCID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;

    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_Test_MplsGetPwInfo_FromGenType2 (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;
    UINT4               u4MainReqType = MPLS_GET_PW_INFO;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_PW_INFO_FROM_GENTYPE2;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;

    InMplsApiInfo.InPathId.PwId.au1Agi[0] = 0;
    InMplsApiInfo.InPathId.PwId.SrcNodeId.MplsGlobalNodeId.u4NodeId = 0;
    InMplsApiInfo.InPathId.PwId.SrcNodeId.MplsGlobalNodeId.u4GlobalId = 0;
    InMplsApiInfo.InPathId.PwId.u4SrcAcId = 0;
    InMplsApiInfo.InPathId.PwId.DstNodeId.MplsGlobalNodeId.u4NodeId = 0;
    InMplsApiInfo.InPathId.PwId.DstNodeId.MplsGlobalNodeId.u4GlobalId = 0;
    InMplsApiInfo.InPathId.PwId.u4DstAcId = 0;

    /* Failure case with out creating the Tables. */

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Success case with creating the Entry and configurations */

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;

    }

    return OSIX_SUCCESS;
}

INT4
Test_Test_MplsGetPwInfo_FromGenFec (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;
    UINT4               u4MainReqType = MPLS_GET_PW_INFO;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_PW_INFO_FROM_GENFEC;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;

    InMplsApiInfo.InPathId.PwId.au1Agi[0] = 0;
    InMplsApiInfo.InPathId.PwId.au1Saii[0] = 0;
    InMplsApiInfo.InPathId.PwId.au1Taii[0] = 0;
    InMplsApiInfo.InPathId.PwId.DstNodeId.MplsRouterId.u4_addr[0] = 0;

    /* Failure case with out creating the Tables. */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Success case with creating the Entry and configurations */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;

    }

    return OSIX_SUCCESS;
}

INT4
Test_Test_MplsGetPwInfo_FromPwIdFec (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;
    UINT4               u4MainReqType = MPLS_GET_PW_INFO;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_PW_INFO_FROM_PWIDFEC;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;

    InMplsApiInfo.InPathId.PwId.u2PwVcType = 0;
    InMplsApiInfo.InPathId.PwId.u4VcId = 0;
    InMplsApiInfo.InPathId.PwId.DstNodeId.MplsRouterId.u4_addr[0] = 0;

    /* Failure case with out creating the Tables. */
    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Success case with creating the Entry and configurations */
    if (MplsApiHandleExternalRequest
        (u4MainReqType, &InMplsApiInfo, &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;

    }

    return OSIX_SUCCESS;

}

INT4
Test_MplsGetLspInfo (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;
    UINT4               u4MainReqType = MPLS_GET_LSP_INFO;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_FTN_INDEX_FROM_FEC;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.LspId.PeerAddr.u4_addr[0] = 0x21000001;

    /* Failure case : Out Api is NULL */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case dont create Ftn Table Entry */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case create FTn table with ip */
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("mpls static binding ipv4 33.0.0.0 255.0.0.0 input vlan 2 200001");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_FTN_INDEX_FROM_FEC;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.LspId.PeerAddr.u4_addr[0] = 0x21000001;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    InMplsApiInfo.u4SubReqType = MPLS_GET_LSP_INFO_FROM_FTN_INDEX;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InPathId.LspId.PeerAddr.u4_addr[0] = 0x21000001;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("no mpls static binding ipv4 33.0.0.0 255.0.0.0 input 200001");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsGetNodeId_RouterId (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    UINT4               u4MainReqType = MPLS_GET_NODE_ID;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_ROUTER_ID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;

    /* Success case : Router Id configured */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls router-id 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls router-id");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;

}

INT4
Test_MplsGetNodeId_FromGlobalNodeId (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;
    UINT4               u4MainReqType = MPLS_GET_NODE_ID;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_GLOBAL_NODE_ID;
    InMplsApiInfo.u4ContextId = 1;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Failure case : Oam not enabled */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_GLOBAL_NODE_ID;
    InMplsApiInfo.u4ContextId = 1;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    /* Failure case : Invalid context id */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case Create Global Config */
    InMplsApiInfo.u4SubReqType = MPLS_GET_GLOBAL_NODE_ID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls global-id 1 node-id 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

INT4
Test_MplsGetNodeId_FromLocalNum (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    UINT4               u4MainReqType = MPLS_GET_NODE_ID;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_GLBNODEID_FROM_LOCALNUM;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InLocalNum = 10;

    /* Failure Case : Local map Num invalid */
    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case :Valid  Localmap Num */

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_GLBNODEID_FROM_LOCALNUM;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InLocalNum = 5;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (OutMplsApiInfo.OutNodeId.u4GlobalId != 100)
    {
        return OSIX_FAILURE;
    }

    if (OutMplsApiInfo.OutNodeId.u4NodeId != 10)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsGetNodeId_LocalNumFrmGlobalNodeId (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    UINT4               u4MainReqType = MPLS_GET_NODE_ID;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    InMplsApiInfo.u4SubReqType = MPLS_GET_LOCALNUM_FROM_GLBNODEID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InNodeId.MplsGlobalNodeId.u4GlobalId = 100;
    InMplsApiInfo.InNodeId.MplsGlobalNodeId.u4NodeId = 10;

    /* Failure Case : Invalid Global and Node Id */

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case : valid Global and Node Id */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_LOCALNUM_FROM_GLBNODEID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InNodeId.MplsGlobalNodeId.u4GlobalId = 100;
    InMplsApiInfo.InNodeId.MplsGlobalNodeId.u4NodeId = 10;

    if (MplsApiHandleExternalRequest (u4MainReqType, &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (OutMplsApiInfo.OutLocalNum != 5)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliGiveAppContext ();
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();

    return OSIX_SUCCESS;

}

/* Selva */

INT4
Test_MplsGetServicePointerOid (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;
    UINT4               au4MeOid[] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 9, 1, 3, 1, 3, 1, 1, 1 };
    UINT4               au4MebaseOid[] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 9, 1, 3, 1, 3 };
    UINT4               au4TnlOid[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 2, 1, 5, 1, 0, 10, 10 };
    UINT4               au4TnlbaseOid[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 2, 1, 5 };
    UINT4               au4FtnOid[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 8, 1, 3, 1, 2, 10 };
    UINT4               au4PwOid[] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 5, 1, 2, 1, 2, 10 };
    UINT4               au4FtnbaseOid[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 8, 1, 3, 1, 2 };

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 1: MPLS_GET_BASE_OID_FROM_MEG_NAME */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Failure case 1 - Oam module not enabled */
    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_OID_FROM_MEG_NAME;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    /* Fill the meg name here */
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MegName, "mpls");
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MeName, "tp");

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure case 1 - Oam module is enabled and
     * Meg Entry doesnt exist */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam enable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_OID_FROM_MEG_NAME;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    /* Fill the meg name here */
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MegName, "mpls");
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MeName, "tp");

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case 1 - Oam module is enabled and
     * Meg Entry exist
     * Create Meg entry with MegId - 1, MeId - 1, MpId - 1*/
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("set service type lsp");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_OID_FROM_MEG_NAME;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    /* Fill the meg name here */
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MegName, "mpls");
    STRCPY (InMplsApiInfo.InPathId.MegMeName.au1MeName, "tp");

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (MEMCMP (au4MeOid, OutMplsApiInfo.OutServiceOid.au4ServiceOidList,
                OutMplsApiInfo.OutServiceOid.u2ServiceOidLen) != 0)
    {
        return OSIX_FAILURE;
    }

    /* Case 2 - MPLS_GET_BASE_OID_FROM_TNL_INDEX */
    /* Failure case 1 - Tunnel Entry doesnt exist */
    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_OID_FROM_TNL_INDEX;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    /* Fill the Tunnel Index here */
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 0;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 10;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 10;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case 1 - Tunnel Entry exist */
    /* Create Tunnel with given index below */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_OID_FROM_TNL_INDEX;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    /* Fill the Tunnel Index here */
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (MEMCMP (au4TnlOid, OutMplsApiInfo.OutServiceOid.au4ServiceOidList,
                OutMplsApiInfo.OutServiceOid.u2ServiceOidLen) != 0)
    {
        return OSIX_FAILURE;
    }

    /* Case 3 - MPLS_GET_BASE_OID_FROM_FEC */
    /* Failure case 1 - FTN Entry doesnt exist */
    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_OID_FROM_FEC;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    /* Fill the FTN Index here */
    InMplsApiInfo.InPathId.LspId.PeerAddr.u4_addr[0] = 10;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case 1 - FTN Entry exist */
    /* Create FTN entry with given index below */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls binding ipv4 35.0.0.0 255.0.0.0 te 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_OID_FROM_FEC;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    /* Fill the FTN Index here */
    InMplsApiInfo.InPathId.LspId.PeerAddr.u4_addr[0] = 0x23000001;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (MEMCMP (au4FtnOid, OutMplsApiInfo.OutServiceOid.au4ServiceOidList,
                OutMplsApiInfo.OutServiceOid.u2ServiceOidLen) != 0)
    {
        return OSIX_FAILURE;
    }

    /* Case 4 - MPLS_GET_BASE_OID_FROM_VCID */

    /* Failure case 1 - PW Entry doesnt exist with this VC ID */
    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_OID_FROM_VCID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    /* Fill the VC-id here */
    InMplsApiInfo.InPathId.PwId.u4VcId = 10;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success case 1 - PW Entry exist with this VC-id */
    /* Create PW entry with given VC-id below */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 2");
    CliExecuteAppCmd ("tunnel mpls destination 5 source 6");
    CliExecuteAppCmd ("tunnel mode mpls traffic-eng");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 50");
    CliExecuteAppCmd ("tunnel mpls static in-label 200040 vlan 2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_OID_FROM_VCID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    /* Fill the VC-id here */
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (MEMCMP (au4PwOid, OutMplsApiInfo.OutServiceOid.au4ServiceOidList,
                OutMplsApiInfo.OutServiceOid.u2ServiceOidLen) != 0)
    {
        return OSIX_FAILURE;
    }

    /* Case 5 - MPLS_GET_BASE_PW_OID */
    /* Success case */
    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_PW_OID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = ELPS_MODULE;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (MEMCMP (au4PwOid, OutMplsApiInfo.OutServiceOid.au4ServiceOidList,
                OutMplsApiInfo.OutServiceOid.u2ServiceOidLen) != 0)
    {
        return OSIX_FAILURE;
    }

    /* Case 6 - MPLS_GET_BASE_FTN_OID */
    /* Success case */
    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_FTN_OID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (MEMCMP (au4FtnbaseOid, OutMplsApiInfo.OutServiceOid.au4ServiceOidList,
                OutMplsApiInfo.OutServiceOid.u2ServiceOidLen) != 0)
    {
        return OSIX_FAILURE;
    }

    /* Case 7 - MPLS_GET_BASE_TNL_OID */
    /* Success case */
    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_TNL_OID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = ELPS_MODULE;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (MEMCMP (au4TnlbaseOid, OutMplsApiInfo.OutServiceOid.au4ServiceOidList,
                OutMplsApiInfo.OutServiceOid.u2ServiceOidLen) != 0)
    {
        return OSIX_FAILURE;
    }

    /* Case 8 - MPLS_GET_BASE_MEG_OID */
    /* Success case */
    InMplsApiInfo.u4SubReqType = MPLS_GET_BASE_MEG_OID;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (MEMCMP (au4MebaseOid, OutMplsApiInfo.OutServiceOid.au4ServiceOidList,
                OutMplsApiInfo.OutServiceOid.u2ServiceOidLen) != 0)
    {
        return OSIX_FAILURE;
    }

    /* Case 9 - Failure case - Invalid Subtype */
    InMplsApiInfo.u4SubReqType = 100;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = ELPS_MODULE;

    if (MplsApiHandleExternalRequest (MPLS_GET_SERVICE_POINTER_OID,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls binding ipv4 35.0.0.0 255.0.0.0");
    CliExecuteAppCmd ("no interface mplstunnel 2");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsGetRevPathFromFwdPath (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));
    /* MPLS_GET_REV_PATH_FROM_FWD_PATH */

    /* Case 1 - Failure case - Invalid NodeType IPv6 */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;
    InMplsApiInfo.InNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV6;
    InMplsApiInfo.InNodeId.MplsRouterId.u4_addr[0] = 10;

    if (MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 2 - Failure case - Invalid NodeType Global Node Id */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;
    InMplsApiInfo.InNodeId.u4NodeType = MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
    InMplsApiInfo.InNodeId.MplsRouterId.u4_addr[0] = 10;

    if (MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 3 - Failure case - Invalid Address */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;
    InMplsApiInfo.InNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InNodeId.MplsRouterId.u4_addr[0] = 0;
    if (MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 4 - Failure case - Ftn Entry not created */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;
    InMplsApiInfo.InNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InNodeId.MplsRouterId.u4_addr[0] = 10;

    if (MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 5 - Failure case - Ftn Entry created  but Tunnel not created */
    /* Create a Ftn Entry with below input */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;
    InMplsApiInfo.InNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InNodeId.MplsRouterId.u4_addr[0] = 10;

    if (MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 6 - Failure case - Ftn Entry created , Tunnel  created
     * but Node Map Table not configured for Source*/
    /* Create a Ftn Entry with below input  and map it with a tunnel entry */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;
    InMplsApiInfo.InNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InNodeId.MplsRouterId.u4_addr[0] = 10;
    if (MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 7 - Failure case - Ftn Entry created , Tunnel  created
     * but Node Map Table not configured for Destination*/
    /* Create a Ftn Entry with below input and map it with a tunnel entry */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;
    InMplsApiInfo.InNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InNodeId.MplsRouterId.u4_addr[0] = 10;

    if (MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 8 - Failure case - Ftn Entry created  but XcEntry not created */
    /* Create a Ftn Entry with below input */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;
    InMplsApiInfo.InNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InNodeId.MplsRouterId.u4_addr[0] = 10;

    if (MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 9 - Success case - Ftn Entry created , Tunnel  created
     * its Source and destination Map node table created*/
    /* Create a Ftn Entry with below input  i4ActionType == REDIRECTTUNNEL */

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("mpls binding ipv4 35.0.0.0 255.0.0.0 te 1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;
    InMplsApiInfo.InNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InNodeId.MplsRouterId.u4_addr[0] = 0x23000001;

    if (MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 10 - Success case - Ftn Entry created , XcEntry created with
     * Insegment mapped.
     * Create a Ftn Entry with below input i4ActionType == REDIRECTLSP*/
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("mpls static binding ipv4 33.0.0.0 255.0.0.0 input vlan 2 200001");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = LSPPING_MODULE;
    InMplsApiInfo.InNodeId.u4NodeType = MPLS_ADDR_TYPE_IPV4;
    InMplsApiInfo.InNodeId.MplsRouterId.u4_addr[0] = 0x21000001;

    if (MplsApiHandleExternalRequest (MPLS_GET_REV_PATH_FROM_FWD_PATH,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls binding ipv4 35.0.0.0 255.0.0.0");
    CliExecuteAppCmd
        ("no mpls static binding ipv4 33.0.0.0 255.0.0.0 input 200001");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;

}

INT4
Test_MplsGetPathInfoFromInLblInfo (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    /* MPLS_GET_PATH_FROM_INLBL_INFO */

    if (MplsApiHandleExternalRequest (MPLS_GET_PATH_FROM_INLBL_INFO,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 1 - Failure Case - No Insegment entry/Pw entry
     * created with the below info */
    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InInLblInfo.u4InIf = 1;
    InMplsApiInfo.InInLblInfo.u4Inlabel = 10;

    if (MplsApiHandleExternalRequest (MPLS_GET_PATH_FROM_INLBL_INFO,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 2 - Success Case - Insegment entry created with the below info &
     * mapped with a XcIndex. Tunnel Entry not created pTeTnlInfo == NULL*/

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("mpls static binding ipv4 33.0.0.0 255.0.0.0 input vlan 2 200001");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InInLblInfo.u4InIf = 1285;
    InMplsApiInfo.InInLblInfo.u4Inlabel = 200001;

    if (MplsApiHandleExternalRequest (MPLS_GET_PATH_FROM_INLBL_INFO,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (OutMplsApiInfo.u4PathType != MPLS_PATH_TYPE_NONTE_LSP)
    {
        return OSIX_FAILURE;
    }

    /* Case 3 - Success Case - Insegment entry created with the below info &
     * mapped with a XcIndex. Tunnel Entry is also created pTeTnlInfo != NULL
     *     */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200010 20.0.0.2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 2");
    CliExecuteAppCmd ("tunnel mpls destination 5 source 6");
    CliExecuteAppCmd ("tunnel mode mpls traffic-eng");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 50");
    CliExecuteAppCmd ("tunnel mpls static in-label 200040 vlan 2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls binding ipv4 35.0.0.0 255.0.0.0 te 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InInLblInfo.u4InIf = 1287;
    InMplsApiInfo.InInLblInfo.u4Inlabel = 200040;

    if (MplsApiHandleExternalRequest (MPLS_GET_PATH_FROM_INLBL_INFO,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 4 - Success Case - Insegment entry created with the below info &
     * mapped with a XcIndex. Tunnel Entry is also created pTeTnlInfo != NULL
     * and  Meg & Me table configured for this Tunnel
     *     */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("set service type lsp");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("mpls oam mep service tp lsp 2 1 6 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InInLblInfo.u4InIf = 1287;
    InMplsApiInfo.InInLblInfo.u4Inlabel = 200040;

    if (MplsApiHandleExternalRequest (MPLS_GET_PATH_FROM_INLBL_INFO,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliExecuteAppCmd ("no mpls binding ipv4 35.0.0.0 255.0.0.0");
    CliExecuteAppCmd
        ("no mpls static binding ipv4 33.0.0.0 255.0.0.0 input 200001");
    CliExecuteAppCmd ("no interface mplstunnel 2");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Case 5 - Failure Case - No Insegment entry created and  
     * Pw entry also not created with the below info with the 
     * Meg and Me table configuration*/

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InInLblInfo.u4InIf = 1285;
    InMplsApiInfo.InInLblInfo.u4Inlabel = 200030;

    if (MplsApiHandleExternalRequest (MPLS_GET_PATH_FROM_INLBL_INFO,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 6 - Success Case - No Insegment entry created but  Pw entry created
     * with the below info with the Meg and Me table not configured*/

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InInLblInfo.u4InIf = 1285;
    InMplsApiInfo.InInLblInfo.u4Inlabel = 200020;

    if (MplsApiHandleExternalRequest (MPLS_GET_PATH_FROM_INLBL_INFO,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 7 - Success Case - No Insegment entry created but  Pw entry created
     * with the below info with the Meg and Me table configured*/
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("service tp");
    CliExecuteAppCmd ("mpls oam mep service tp pw 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.u4SubReqType = 0;
    InMplsApiInfo.u4ContextId = 0;
    InMplsApiInfo.u4SrcModId = BFD_MODULE;
    InMplsApiInfo.InInLblInfo.u4InIf = 1285;
    InMplsApiInfo.InInLblInfo.u4Inlabel = 200020;

    if (MplsApiHandleExternalRequest (MPLS_GET_PATH_FROM_INLBL_INFO,
                                      &InMplsApiInfo,
                                      &OutMplsApiInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg mpls");
    CliExecuteAppCmd ("no service tp");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg mpls");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;

}

INT4
Test_MplsOamTxPacketHandleFromApp (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    pBuf = CRU_BUF_Allocate_MsgBufChain (20, 0);

    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    FormPacket (&pBuf);

    /* Case 1 - Failure case. Invalid OutIfIndex is given */
    InMplsApiInfo.InPktInfo.u4OutIfIndex = 1285;
    InMplsApiInfo.InPktInfo.pBuf = pBuf;
    MEMSET (InMplsApiInfo.InPktInfo.au1DstMac, 0, 6);

    if (MplsApiHandleExternalRequest (MPLS_PACKET_HANDLE_FROM_APP,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 2 - Failure case. Valid OutIfIndex is given  but 
     * zero mac-address and invalid next hop is given*/
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("mpls binding ipv4 35.0.0.0 255.0.0.0 te 1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show mac-address-table");
    CliExecuteAppCmd ("show ip arp");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.InPktInfo.u4OutIfIndex = 1285;
    MEMSET (InMplsApiInfo.InPktInfo.au1DstMac, 0, 6);
    InMplsApiInfo.InPktInfo.NextHopIpAddr.u4_addr[0] = 0x23000001;
    InMplsApiInfo.InPktInfo.bIsUnNumberedIf = FALSE;
    InMplsApiInfo.InPktInfo.pBuf = pBuf;

    if (MplsApiHandleExternalRequest (MPLS_PACKET_HANDLE_FROM_APP,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    DumpPkt (pBuf, 20);
    /* Case 3 - Success case. Valid OutIfIndex is given with  
     * zero mac-address and valid next hop is given*/
    InMplsApiInfo.InPktInfo.u4OutIfIndex = 1285;
    MEMSET (InMplsApiInfo.InPktInfo.au1DstMac, 0, 6);
    InMplsApiInfo.InPktInfo.NextHopIpAddr.u4_addr[0] = 0x14000002;
    InMplsApiInfo.InPktInfo.bIsUnNumberedIf = FALSE;
    InMplsApiInfo.InPktInfo.bIsMplsLabelledPacket = TRUE;
    InMplsApiInfo.InPktInfo.pBuf = pBuf;

    if (MplsApiHandleExternalRequest (MPLS_PACKET_HANDLE_FROM_APP,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pBuf = NULL;
    pBuf = CRU_BUF_Allocate_MsgBufChain (20, 0);

    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    FormPacket (&pBuf);

    StrToMac ((UINT1 *) "00:11:22:33:44:55", InMplsApiInfo.InPktInfo.au1DstMac);
    /* Case 4 - Success case. Valid OutIfIndex is given with  
     * correct destination mac-address and valid next hop is given
     * Labled packet*/
    InMplsApiInfo.InPktInfo.u4OutIfIndex = 1285;
    InMplsApiInfo.InPktInfo.NextHopIpAddr.u4_addr[0] = 0x14000002;
    InMplsApiInfo.InPktInfo.bIsUnNumberedIf = FALSE;
    InMplsApiInfo.InPktInfo.bIsMplsLabelledPacket = TRUE;
    InMplsApiInfo.InPktInfo.pBuf = pBuf;

    if (MplsApiHandleExternalRequest (MPLS_PACKET_HANDLE_FROM_APP,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pBuf = NULL;
    pBuf = CRU_BUF_Allocate_MsgBufChain (20, 0);

    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    FormPacket (&pBuf);

    /* Case 5 - Success case. Valid OutIfIndex is given with  
     * correct destination mac-address and valid next hop is given
     * Un-Labled packet*/
    InMplsApiInfo.InPktInfo.u4OutIfIndex = 1285;
    MEMSET (InMplsApiInfo.InPktInfo.au1DstMac, 0, 6);
    InMplsApiInfo.InPktInfo.NextHopIpAddr.u4_addr[0] = 0x14000002;
    InMplsApiInfo.InPktInfo.bIsUnNumberedIf = FALSE;
    InMplsApiInfo.InPktInfo.bIsMplsLabelledPacket = FALSE;
    InMplsApiInfo.InPktInfo.pBuf = pBuf;

    if (MplsApiHandleExternalRequest (MPLS_PACKET_HANDLE_FROM_APP,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    pBuf = NULL;
    pBuf = CRU_BUF_Allocate_MsgBufChain (20, 0);

    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    FormPacket (&pBuf);

    /* Case 6 - Failure Case. UnNumberedIf as False */
    InMplsApiInfo.InPktInfo.u4OutIfIndex = 1285;
    MEMSET (InMplsApiInfo.InPktInfo.au1DstMac, 0, 6);
    InMplsApiInfo.InPktInfo.NextHopIpAddr.u4_addr[0] = 0x14000002;
    InMplsApiInfo.InPktInfo.bIsUnNumberedIf = TRUE;
    InMplsApiInfo.InPktInfo.bIsMplsLabelledPacket = TRUE;
    InMplsApiInfo.InPktInfo.pBuf = pBuf;

    if (MplsApiHandleExternalRequest (MPLS_PACKET_HANDLE_FROM_APP,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls binding ipv4 35.0.0.0 255.0.0.0");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsOamUpdateLpsStatus (VOID)
{
    tMplsApiInInfo      InMplsApiInfo;
    tMplsApiOutInfo     OutMplsApiInfo;

    MEMSET (&InMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (&OutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    /* Case 1 - Failure Case . Invalid Tunnel Entry is given */
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 0;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 0;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 0;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 0;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 2 - Failure Case . Invalid Pw Entry is given */
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 5;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 3 - Failure Case . Invalid Path Type is given */
    InMplsApiInfo.InPathId.u4PathType = 100;
    InMplsApiInfo.InPathId.PwId.u4VcId = 5;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 4 - Failure Case . Valid Tunnel Entry is created
     * with the below info but invalid Req. type is given*/
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default ");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2 ");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("arp 20.0.0.2 00:11:22:33:44:55 vlan 2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd ("end ");
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("int mplstunnel 1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd (" no shutdown ");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("mpls binding ipv4 35.0.0.0 255.0.0.0 te 1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;
    InMplsApiInfo.u4SubReqType = 100;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 4 - Failure Case . Valid Tunnel Entry is created
     *      * with the below info but invalid Path type is given*/
    InMplsApiInfo.InPathId.u4PathType = 100;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_AVAILABLE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 5 - Success Case . Valid Tunnel Entry is created
     *      * with the below info with Req. type as MPLS_LPS_PROT_AVAILABLE */
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_AVAILABLE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 5 - Success Case . Valid Tunnel Entry is created
     *      * with the below info with Req. type as MPLS_LPS_PROT_AVAILABLE */
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_IN_USE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 6 - Success Case . Valid Tunnel Entry is created
     *      * with the below info with Req. type as MPLS_LPS_PROT_NOT_APPLICABLE */
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_NOT_APPLICABLE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 7 - Success Case . Valid Tunnel Entry is created
     *      * with the below info with Req. type as MPLS_LPS_PROT_NOT_AVAILABLE */
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_NOT_AVAILABLE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 8 - Success Case . Valid Tunnel Entry is created
     *      * with the below info with Req. type as MPLS_LPS_PROT_IN_USE */
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
    InMplsApiInfo.InPathId.TnlId.u4SrcTnlNum = 1;
    InMplsApiInfo.InPathId.TnlId.u4LspNum = 1;
    InMplsApiInfo.InPathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = 5;
    InMplsApiInfo.InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = 6;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_IN_USE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 9 - Failure Case . Valid Pw Entry is created
     *      * with the below info but invalid Req. type is given*/
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021 mplstype vconly");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;
    InMplsApiInfo.u4SubReqType = 100;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 10 - Success Case . Valid Pw Entry is created
     *      * with the below info with Req. type MPLS_LPS_PROT_AVAILABLE*/
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_AVAILABLE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 11 - Success Case . Valid Pw Entry is created
     *      * with the below info with Req. type MPLS_LPS_PROT_NOT_APPLICABLE*/
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_NOT_APPLICABLE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 12 - Success Case . Valid Pw Entry is created
     *      * with the below info with Req. type MPLS_LPS_PROT_NOT_AVAILABLE*/
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_NOT_AVAILABLE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 10 - Success Case . Valid Pw Entry is created
     *      * with the below info with Req. type MPLS_LPS_PROT_AVAILABLE*/
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_AVAILABLE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 10 - Success Case . Valid Pw Entry is created
     *      * with the below info with Req. type MPLS_LPS_PROT_AVAILABLE*/
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_IN_USE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 13 - Success Case . Valid Pw Entry is created
     *      * with the below info with Req. type MPLS_LPS_PROT_IN_USE*/
    InMplsApiInfo.InPathId.u4PathType = MPLS_PATH_TYPE_PW;
    InMplsApiInfo.InPathId.PwId.u4VcId = 1;
    InMplsApiInfo.u4SubReqType = MPLS_LPS_PROT_IN_USE;

    if (MplsApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                      &InMplsApiInfo, NULL) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no mpls binding ipv4 35.0.0.0 255.0.0.0");
    CliExecuteAppCmd ("no interface mplstunnel 1");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 100");
    CliExecuteAppCmd
        ("no mpls l2transport manual 20.0.0.2 locallabel 200020 remotelabel 200021");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_MplsUtilFillMplsHdr (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tMplsHdr            MplsHdr;
    INT4                i4LabelCount = 0;

    MEMSET (&MplsHdr, 0, sizeof (tMplsHdr));

    pBuf = CRU_BUF_Allocate_MsgBufChain (20, 0);

    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    for (i4LabelCount = 4; i4LabelCount >= 0; i4LabelCount--)
    {
        MplsHdr.u4Lbl = 1;
        MplsHdr.Ttl = 1;
        MplsHdr.Exp = 0;
        MplsHdr.SI = 1;
        MplsUtlFillMplsHdr (&MplsHdr, pBuf);
    }
    DumpPkt (pBuf, 20);

    /* pBuf is NULL */

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    pBuf = NULL;

    for (i4LabelCount = 4; i4LabelCount >= 0; i4LabelCount--)
    {
        MplsHdr.u4Lbl = 1;
        MplsHdr.Ttl = 1;
        MplsHdr.Exp = 0;
        MplsHdr.SI = 1;
        MplsUtlFillMplsHdr (&MplsHdr, pBuf);
    }

    MplsUtlFillMplsHdr (&MplsHdr, NULL);
    MplsUtlFillMplsHdr (NULL, NULL);
    MplsUtlFillMplsHdr (NULL, pBuf);
    return OSIX_SUCCESS;
}

INT4
Test_MplsUtilFillMplsAchHdr (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tMplsAchHdr         AchHdr;

    MEMSET (&AchHdr, 0, sizeof (tMplsAchHdr));

    pBuf = CRU_BUF_Allocate_MsgBufChain (20, 0);

    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    AchHdr.u4AchType = MPLS_ACH_TYPE;
    AchHdr.u1Version = MPLS_ACH_VERSION;
    AchHdr.u1Rsvd = MPLS_ACH_RSVD;
    AchHdr.u2ChannelType = 9;
    MplsUtlFillMplsAchHdr (&AchHdr, pBuf);

    MplsUtlFillMplsAchHdr (&AchHdr, NULL);
    MplsUtlFillMplsAchHdr (NULL, NULL);
    MplsUtlFillMplsAchHdr (NULL, pBuf);
    DumpPkt (pBuf, 20);

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    pBuf = NULL;

    return OSIX_SUCCESS;
}

INT4
Test_MplsUtilFillMplsAchTlvHdr (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tMplsAchTlvHdr      AchTlvHdr;

    MEMSET (&AchTlvHdr, 0, sizeof (tMplsAchTlvHdr));

    pBuf = CRU_BUF_Allocate_MsgBufChain (20, 0);

    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    AchTlvHdr.u2TlvHdrLen = 5;
    AchTlvHdr.u2Rsvd = 0;
    MplsUtlFillMplsAchTlvHdr (&AchTlvHdr, pBuf);

    DumpPkt (pBuf, 20);

    AchTlvHdr.u2TlvHdrLen = 5;
    AchTlvHdr.u2Rsvd = 0;
    MplsUtlFillMplsAchTlvHdr (&AchTlvHdr, NULL);

    AchTlvHdr.u2TlvHdrLen = 5;
    AchTlvHdr.u2Rsvd = 0;
    MplsUtlFillMplsAchTlvHdr (NULL, pBuf);

    AchTlvHdr.u2TlvHdrLen = 5;
    AchTlvHdr.u2Rsvd = 0;
    MplsUtlFillMplsAchTlvHdr (NULL, NULL);

    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    pBuf = NULL;

    return OSIX_SUCCESS;
}

INT4
Test_MplsUtlFillMplsAchTlv (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tMplsAchTlv         AchTlv;

    MEMSET (&AchTlv, 0, sizeof (tMplsAchTlv));

    pBuf = CRU_BUF_Allocate_MsgBufChain (20, 0);

    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }
    AchTlv.u2TlvType = 100;
    AchTlv.u2TlvLen = 2;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    AchTlv.u2TlvType = 100;
    AchTlv.u2TlvLen = 2;
    MplsUtlFillMplsAchTlv (&AchTlv, NULL);

    AchTlv.u2TlvType = 100;
    AchTlv.u2TlvLen = 2;
    MplsUtlFillMplsAchTlv (NULL, pBuf);

    MEMSET (&AchTlv, 0, sizeof (tMplsAchTlv));

    AchTlv.u2TlvType = MPLS_ACH_NULL_TLV;
    AchTlv.u2TlvLen = 2;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    MEMSET (&AchTlv, 0, sizeof (tMplsAchTlv));

    AchTlv.u2TlvType = MPLS_ACH_SOURCE_V4_ADDR_TLV;
    AchTlv.u2TlvLen = 4;
    AchTlv.unAchValue.SrcIpAddr.u4_addr[0] = 0x23000001;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    MEMSET (&AchTlv, 0, sizeof (tMplsAchTlv));

    AchTlv.u2TlvType = MPLS_ACH_SOURCE_V6_ADDR_TLV;
    AchTlv.u2TlvLen = 4;
    AchTlv.unAchValue.SrcIpAddr.u4_addr[0] = 3;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    MEMSET (&AchTlv, 0, sizeof (tMplsAchTlv));

    AchTlv.u2TlvType = MPLS_ACH_ICC_ID_TLV;
    AchTlv.u2TlvLen = 4;
    AchTlv.unAchValue.au1Icc[0] = 1;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    MEMSET (&AchTlv, 0, sizeof (tMplsAchTlv));

    AchTlv.u2TlvType = MPLS_ACH_GLOBAL_ID_TLV;
    AchTlv.u2TlvLen = 4;
    AchTlv.unAchValue.u4GlobalId = 100;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    MEMSET (&AchTlv, 0, sizeof (tMplsAchTlv));

    AchTlv.u2TlvType = MPLS_ACH_IF_ID_TLV;
    AchTlv.u2TlvLen = 4;
    AchTlv.unAchValue.IfId.u4IfNum = 1;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    MEMSET (&AchTlv, 0, sizeof (tMplsAchTlv));

    AchTlv.u2TlvType = MPLS_ACH_AUTH_TLV;
    AchTlv.u2TlvLen = 4;
    AchTlv.unAchValue.AuthTlv.u2AuthData = 2;
    AchTlv.unAchValue.AuthTlv.u1AuthLen = 1;
    AchTlv.unAchValue.AuthTlv.u1AuthType = 1;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    MEMSET (&AchTlv, 0, sizeof (tMplsAchTlv));

    AchTlv.u2TlvType = MPLS_ACH_MIP_TLV;
    AchTlv.u2TlvLen = 4;
    AchTlv.unAchValue.MipTlv.u4IfNum = 3;
    AchTlv.unAchValue.MipTlv.GlobalNodeId.u4NodeId = 10;
    AchTlv.unAchValue.MipTlv.GlobalNodeId.u4GlobalId = 100;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    MEMSET (&AchTlv, 0, sizeof (tMplsAchTlv));

    AchTlv.u2TlvType = MPLS_ACH_IP_LSP_MEP_TLV;
    AchTlv.u2TlvLen = 4;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    AchTlv.u2TlvType = MPLS_ACH_IP_PW_MEP_TLV;
    AchTlv.u2TlvLen = 4;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    AchTlv.u2TlvType = MPLS_ACH_ICC_MEP_TLV;
    AchTlv.u2TlvLen = 4;
    MplsUtlFillMplsAchTlv (&AchTlv, pBuf);

    return OSIX_SUCCESS;

}

#define DATA_ASSIGN_1_BYTE(pMsg, u4Offset, u1Value) \
{\
       UINT1  u1LinearBuf = (UINT1) u1Value;\
       CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u1LinearBuf), u4Offset, 1);\
       u4Offset += 1; \
}

VOID
FormPacket (tCRU_BUF_CHAIN_HEADER ** ppBuf)
{
    UINT4               u4Offset = 16;
    UINT1               u1FirstByte = 0x01;
    UINT1               u1SecByte = 0x02;
    UINT1               u1ThirdByte = 0x03;
    UINT1               u1FourthByte = 0x04;
    tMplsAchTlvHdr      AchTlvHdr;
    tMplsAchHdr         AchHdr;
    tMplsHdr            MplsHdr;
    INT4                i4LabelCount = 0;

    /* Psc packet */
    DATA_ASSIGN_1_BYTE (*ppBuf, u4Offset, u1FirstByte);
    DATA_ASSIGN_1_BYTE (*ppBuf, u4Offset, u1SecByte);
    DATA_ASSIGN_1_BYTE (*ppBuf, u4Offset, u1ThirdByte);
    DATA_ASSIGN_1_BYTE (*ppBuf, u4Offset, u1FourthByte);

    CRU_BUF_Move_ValidOffset (*ppBuf, 16);

    /* Fill the MPLS ach Tlv Header */
    AchTlvHdr.u2TlvHdrLen = 0;
    AchTlvHdr.u2Rsvd = 0;
    MplsUtlFillMplsAchTlvHdr (&AchTlvHdr, *ppBuf);

    /* Fill the MPLS Ach Header */
    AchHdr.u4AchType = MPLS_ACH_TYPE;
    AchHdr.u1Version = MPLS_ACH_VERSION;
    AchHdr.u1Rsvd = MPLS_ACH_RSVD;
    AchHdr.u2ChannelType = 9;
    MplsUtlFillMplsAchHdr (&AchHdr, *ppBuf);

    /* Fill the Gal label */
    MplsHdr.u4Lbl = MPLS_GAL_LABEL;
    MplsHdr.Ttl = 1;
    MplsHdr.Exp = 0;
    MplsHdr.SI = 1;
    MplsUtlFillMplsHdr (&MplsHdr, *ppBuf);

    for (i4LabelCount = 3; i4LabelCount >= 0; i4LabelCount--)
    {
        MplsHdr.u4Lbl = (UINT4) i4LabelCount;
        MplsHdr.Ttl = 1;

        if (i4LabelCount == 0)
        {
            MplsHdr.Exp = MPLS_TC_BEST_EFFORT;
            MplsHdr.SI = 1;
        }
        else
        {
            MplsHdr.Exp = 1;
            MplsHdr.SI = 0;
        }

        MplsUtlFillMplsHdr (&MplsHdr, *ppBuf);
    }

    return;
}

INT4
Test_TeCmnExtGetDirectionFromTnl ()
{
    tTeTnlInfo          TeTnlInfo;
    UINT4               u4Direction = 0;

    /* CASE 1 */
    TeTnlInfo.u4TnlMode = TE_TNL_MODE_COROUTED_BIDIRECTIONAL;
    TeTnlInfo.u1TnlRole = TE_EGRESS;

    TeCmnExtGetDirectionFromTnl (&TeTnlInfo, &u4Direction);

    if (u4Direction == MPLS_DIRECTION_FORWARD)
    {
        return OSIX_FAILURE;
    }

    u4Direction = 0;

    /* CASE 2 */
    TeTnlInfo.u4TnlMode = TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL;
    TeTnlInfo.u1TnlRole = TE_EGRESS;

    TeCmnExtGetDirectionFromTnl (&TeTnlInfo, &u4Direction);

    if (u4Direction == MPLS_DIRECTION_REVERSE)
    {
        return OSIX_FAILURE;
    }

    u4Direction = 0;

    /* CASE 3 */
    TeTnlInfo.u4TnlMode = TE_TNL_MODE_UNIDIRECTIONAL;
    TeTnlInfo.u1TnlRole = TE_INGRESS;

    TeCmnExtGetDirectionFromTnl (&TeTnlInfo, &u4Direction);

    if (u4Direction == MPLS_DIRECTION_REVERSE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
Test_TeCmnExtUpdateArpResolveStatus ()
{
    tTeTnlInfo          TeTnlInfo;
    UINT1               u1ArpResolveStatus = 0;

    /* CASE 1 */
    TeTnlInfo.u1TnlRole = TE_INGRESS;
    u1ArpResolveStatus = MPLS_ARP_RESOLVED;
    TeCmnExtUpdateArpResolveStatus (&TeTnlInfo, MPLS_MLIB_TNL_CREATE, 0,
                                    u1ArpResolveStatus);

    if (TeTnlInfo.u1FwdArpResolveStatus != u1ArpResolveStatus)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2 */
    TeTnlInfo.u1TnlRole = TE_EGRESS;
    u1ArpResolveStatus = MPLS_ARP_RESOLVE_WAITING;
    TeCmnExtUpdateArpResolveStatus (&TeTnlInfo, MPLS_MLIB_TNL_CREATE, 0,
                                    u1ArpResolveStatus);

    if (TeTnlInfo.u1RevArpResolveStatus != u1ArpResolveStatus)
    {
        return OSIX_FAILURE;
    }

    /* CASE 3  */
    TeTnlInfo.u1TnlRole = 0;
    u1ArpResolveStatus = MPLS_ARP_RESOLVE_FAILED;

    TeCmnExtUpdateArpResolveStatus (&TeTnlInfo, MPLS_MLIB_ILM_CREATE,
                                    MPLS_DIRECTION_FORWARD, u1ArpResolveStatus);
    if (TeTnlInfo.u1FwdArpResolveStatus != u1ArpResolveStatus)
    {
        return OSIX_FAILURE;
    }

    /* CASE 4 */
    u1ArpResolveStatus = MPLS_ARP_RESOLVED;
    TeCmnExtUpdateArpResolveStatus (&TeTnlInfo, MPLS_MLIB_ILM_CREATE,
                                    MPLS_DIRECTION_REVERSE, u1ArpResolveStatus);
    if (TeTnlInfo.u1RevArpResolveStatus != u1ArpResolveStatus)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
Test_TeCmnExtSetOperStatusAndProgHw ()
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4XcIndex = 0;
    UINT1               u1Temp = 0;
    UINT1               u1Temp1 = 0;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mode corouted-bi");
    CliExecuteAppCmd
        ("tunnel mpls static out-label 200001 20.0.0.2 direction forward");
    CliExecuteAppCmd
        ("tunnel mpls static in-label 200002 vlan 2 direction reverse");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 11");
    CliExecuteAppCmd ("tunnel mpls destination 5 source 6");
    CliExecuteAppCmd ("tunnel mode corouted-bi");
    CliExecuteAppCmd
        ("tunnel mpls static out-label 200003 20.0.0.2 direction reverse");
    CliExecuteAppCmd
        ("tunnel mpls static in-label 200004 vlan 2 direction forward");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 12");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 7");
    CliExecuteAppCmd ("tunnel mode corouted-bi");
    CliExecuteAppCmd
        ("tunnel mpls static in-label 200005 vlan 2 out-label 200006 25.0.0.2 direction forward");
    CliExecuteAppCmd
        ("tunnel mpls static in-label 200007 vlan 3 out-label 200008 20.0.0.2 direction reverse");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 1: Ingress uni-bidirectional tunnel, oper up XC failure. */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    u4XcIndex = pTeTnlInfo->u4TnlXcIndex;
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_UNIDIRECTIONAL;
    pTeTnlInfo->u4TnlXcIndex = 0;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_COROUTED_BIDIRECTIONAL;
    pTeTnlInfo->u4TnlXcIndex = u4XcIndex;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 2: Intermediate uni-directional tunnel, oper up, XC failure. */
    pTeTnlInfo = TeGetTunnelInfo (12, 1, 7, 6);
    u4XcIndex = pTeTnlInfo->u4TnlXcIndex;
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_UNIDIRECTIONAL;
    pTeTnlInfo->u4TnlXcIndex = 0;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_COROUTED_BIDIRECTIONAL;
    pTeTnlInfo->u4TnlXcIndex = u4XcIndex;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 3: Egress uni-directional tunnel, oper up, XC failure. */
    pTeTnlInfo = TeGetTunnelInfo (11, 1, 6, 5);
    u4XcIndex = pTeTnlInfo->u4TnlXcIndex;
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_UNIDIRECTIONAL;
    pTeTnlInfo->u4TnlXcIndex = 0;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_COROUTED_BIDIRECTIONAL;
    pTeTnlInfo->u4TnlXcIndex = u4XcIndex;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 4: Ingress uni-bidirectional tunnel, oper up, ARP resolve waiting. */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_UNIDIRECTIONAL;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if (pTeTnlInfo->u1FwdArpResolveStatus != MPLS_ARP_RESOLVE_WAITING)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_COROUTED_BIDIRECTIONAL;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 5: Intermediate uni-bidirectional tunnel, oper up, ARP resolve waiting. */
    pTeTnlInfo = TeGetTunnelInfo (12, 1, 7, 6);
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_UNIDIRECTIONAL;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if (pTeTnlInfo->u1FwdArpResolveStatus != MPLS_ARP_RESOLVE_WAITING)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_COROUTED_BIDIRECTIONAL;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 6: Egress uni-bidirectional tunnel, oper up, tnl oper up failure. */
    pTeTnlInfo = TeGetTunnelInfo (11, 1, 6, 5);
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_UNIDIRECTIONAL;
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_COROUTED_BIDIRECTIONAL;
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 7: Ingress uni-directional tunnel, oper up success. */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;arp 20.0.0.2 00:11:22:33:44:55 vlan 2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_UNIDIRECTIONAL;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;
    pTeTnlInfo->u4TnlMode = TE_TNL_MODE_COROUTED_BIDIRECTIONAL;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no arp 20.0.0.2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 8: Ingress corouted-bidirectional tunnel, oper up, XC failure. */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    u4XcIndex = pTeTnlInfo->u4TnlXcIndex;
    pTeTnlInfo->u4TnlXcIndex = 0;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u4TnlXcIndex = u4XcIndex;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 9: Intermediate corouted-bidirectional tunnel, oper up, XC failure. */
    pTeTnlInfo = TeGetTunnelInfo (12, 1, 7, 6);
    u4XcIndex = pTeTnlInfo->u4TnlXcIndex;
    pTeTnlInfo->u4TnlXcIndex = 0;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u4TnlXcIndex = u4XcIndex;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 10: Egress corouted-bidirectional tunnel, oper up, XC failure. */
    pTeTnlInfo = TeGetTunnelInfo (11, 1, 6, 5);
    u4XcIndex = pTeTnlInfo->u4TnlXcIndex;
    pTeTnlInfo->u4TnlXcIndex = 0;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u4TnlXcIndex = u4XcIndex;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 11: Egress corouted-bidirectional tunnel, oper up, ARP resolve waiting. */
    pTeTnlInfo = TeGetTunnelInfo (11, 1, 6, 5);
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if (pTeTnlInfo->u1RevArpResolveStatus != MPLS_ARP_RESOLVE_WAITING)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 12: Intermediate corouted-bidirectional tunnel, oper up, ARP resolve waiting. */
    pTeTnlInfo = TeGetTunnelInfo (12, 1, 7, 6);
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if (pTeTnlInfo->u1RevArpResolveStatus != MPLS_ARP_RESOLVE_WAITING)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 13: Egress corouted-bidirectional tunnel, oper up, tnl oper up failure. */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;arp 20.0.0.2 00:11:22:33:44:55 vlan 2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (11, 1, 6, 5);
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no arp 20.0.0.2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 14: Ingress corouted-bidirectional tunnel, oper down, success */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_DOWN) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 15: Intermediate corouted-bidirectional tunnel, oper down, success */
    pTeTnlInfo = TeGetTunnelInfo (12, 1, 7, 6);
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_DOWN) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    /* CASE 16: Egress corouted-bidirectional tunnel, oper down, tnl oper up failure. */
    pTeTnlInfo = TeGetTunnelInfo (11, 1, 6, 5);
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    u1Temp = pTeTnlInfo->u1FwdArpResolveStatus;
    u1Temp1 = pTeTnlInfo->u1RevArpResolveStatus;
    pTeTnlInfo->u1FwdArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    pTeTnlInfo->u1FwdArpResolveStatus = u1Temp;
    pTeTnlInfo->u1RevArpResolveStatus = u1Temp1;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliExecuteAppCmd ("c t;no interface mplstunnel 11;end");
    CliExecuteAppCmd ("c t;no interface mplstunnel 12;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_TeCmnExtGetBkpTnlFrmInUseProtTnl ()
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeUpdateInfo       TeUpdateInfo;
    tTeTnlIndices       ProtTnlInfo;
    tTeTnlIndices       BkpUpTnlInfo;
    UINT4               u4Source = 0;
    UINT4               u4Dest = 0;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 1: Tunnel not exist */
    ProtTnlInfo.u4TnlIndex = 1;
    ProtTnlInfo.u4TnlInstance = 1;
    u4Source = OSIX_HTONL (5);
    u4Dest = OSIX_HTONL (6);
    MEMCPY (ProtTnlInfo.TnlIngressLsrId, &u4Source, ROUTER_ID_LENGTH);
    MEMCPY (ProtTnlInfo.TnlEgressLsrId, &u4Dest, ROUTER_ID_LENGTH);
    if (TeCmnExtGetBkpTnlFrmInUseProtTnl (&ProtTnlInfo, &BkpUpTnlInfo)
        == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 2: Tunnel exist but not prot in use */
    ProtTnlInfo.u4TnlIndex = 10;
    ProtTnlInfo.u4TnlInstance = 1;
    u4Source = OSIX_HTONL (5);
    u4Dest = OSIX_HTONL (6);
    MEMCPY (ProtTnlInfo.TnlIngressLsrId, &u4Source, ROUTER_ID_LENGTH);
    MEMCPY (ProtTnlInfo.TnlEgressLsrId, &u4Dest, ROUTER_ID_LENGTH);
    if (TeCmnExtGetBkpTnlFrmInUseProtTnl (&ProtTnlInfo, &BkpUpTnlInfo)
        == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 3: Tunnel exist and prot in use */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);

    TeUpdateInfo.u4BkpTnlIndex = 10;
    TeUpdateInfo.u4BkpTnlInstance = 2;
    u4Source = OSIX_HTONL (5);
    u4Dest = OSIX_HTONL (6);
    MEMCPY (TeUpdateInfo.BkpTnlIngressLsrId, &u4Source, ROUTER_ID_LENGTH);
    MEMCPY (TeUpdateInfo.BkpTnlEgressLsrId, &u4Dest, ROUTER_ID_LENGTH);
    TeUpdateInfo.u1LocalProtection = 0x01;

    TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo);

    TeUpdateInfo.u1LocalProtection = 0x02;
    TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo);

    ProtTnlInfo.u4TnlIndex = 10;
    ProtTnlInfo.u4TnlInstance = 1;
    u4Source = OSIX_HTONL (5);
    u4Dest = OSIX_HTONL (6);
    MEMCPY (ProtTnlInfo.TnlIngressLsrId, &u4Source, ROUTER_ID_LENGTH);
    MEMCPY (ProtTnlInfo.TnlEgressLsrId, &u4Dest, ROUTER_ID_LENGTH);
    if (TeCmnExtGetBkpTnlFrmInUseProtTnl (&ProtTnlInfo, &BkpUpTnlInfo)
        == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if ((BkpUpTnlInfo.u4TnlIndex != TeUpdateInfo.u4BkpTnlIndex) ||
        (BkpUpTnlInfo.u4TnlInstance != TeUpdateInfo.u4BkpTnlInstance) ||
        (MEMCMP (BkpUpTnlInfo.TnlIngressLsrId, TeUpdateInfo.BkpTnlIngressLsrId,
                 ROUTER_ID_LENGTH) != 0) ||
        (MEMCMP (BkpUpTnlInfo.TnlEgressLsrId, TeUpdateInfo.BkpTnlEgressLsrId,
                 ROUTER_ID_LENGTH) != 0))
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_TeCmnExtUpdateTnlProtStatus ()
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeUpdateInfo       TeUpdateInfo;
    UINT4               u4Source = 0;
    UINT4               u4Dest = 0;
    UINT1               u1Temp = 0;
    UINT1               u1Temp1 = 0;
    UINT1               u1Temp2 = 0;
    BOOL1               bFlag = FALSE;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 1: Default state */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);

    TeUpdateInfo.u1LocalProtection = 0x10;

    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 2: Local protection in use state */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    u1Temp = pTeTnlInfo->u1TnlLocalProtectInUse;
    TeUpdateInfo.u1LocalProtection = LOCAL_PROT_IN_USE;
    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) != TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1TnlLocalProtectInUse = u1Temp;

    /* CASE 3: Local protection available state */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    u1Temp = pTeTnlInfo->u1TnlLocalProtectInUse;
    TeUpdateInfo.u4BkpTnlIndex = 11;
    TeUpdateInfo.u4BkpTnlInstance = 2;
    u4Source = OSIX_HTONL (5);
    u4Dest = OSIX_HTONL (4);
    MEMCPY (TeUpdateInfo.BkpTnlIngressLsrId, &u4Source, ROUTER_ID_LENGTH);
    MEMCPY (TeUpdateInfo.BkpTnlEgressLsrId, &u4Dest, ROUTER_ID_LENGTH);
    TeUpdateInfo.u1LocalProtection = LOCAL_PROT_AVAIL;

    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if (pTeTnlInfo->u1TnlLocalProtectInUse != LOCAL_PROT_AVAIL)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if ((pTeTnlInfo->u4BkpTnlIndex != TeUpdateInfo.u4BkpTnlIndex) ||
        (pTeTnlInfo->u4BkpTnlInstance != TeUpdateInfo.u4BkpTnlInstance) ||
        (MEMCMP
         (pTeTnlInfo->BkpTnlIngressLsrId, TeUpdateInfo.BkpTnlIngressLsrId,
          ROUTER_ID_LENGTH) != 0)
        ||
        (MEMCMP
         (pTeTnlInfo->BkpTnlEgressLsrId, TeUpdateInfo.BkpTnlEgressLsrId,
          ROUTER_ID_LENGTH) != 0))
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1TnlLocalProtectInUse = u1Temp;

    /* CASE 4: Local prot not applicable, previous status not prot in use */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    u1Temp = pTeTnlInfo->u1TnlLocalProtectInUse;
    TeUpdateInfo.u4BkpTnlIndex = 0;
    TeUpdateInfo.u4BkpTnlInstance = 0;
    u4Source = OSIX_HTONL (0);
    u4Dest = OSIX_HTONL (0);
    MEMCPY (TeUpdateInfo.BkpTnlIngressLsrId, &u4Source, ROUTER_ID_LENGTH);
    MEMCPY (TeUpdateInfo.BkpTnlEgressLsrId, &u4Dest, ROUTER_ID_LENGTH);
    TeUpdateInfo.u1LocalProtection = LOCAL_PROT_NOT_APPLICABLE;
    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (pTeTnlInfo->u1TnlLocalProtectInUse != LOCAL_PROT_NOT_APPLICABLE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((pTeTnlInfo->u4BkpTnlIndex != TeUpdateInfo.u4BkpTnlIndex) ||
        (pTeTnlInfo->u4BkpTnlInstance != TeUpdateInfo.u4BkpTnlInstance) ||
        (MEMCMP
         (pTeTnlInfo->BkpTnlIngressLsrId, TeUpdateInfo.BkpTnlIngressLsrId,
          ROUTER_ID_LENGTH) != 0)
        ||
        (MEMCMP
         (pTeTnlInfo->BkpTnlEgressLsrId, TeUpdateInfo.BkpTnlEgressLsrId,
          ROUTER_ID_LENGTH) != 0))
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1TnlLocalProtectInUse = u1Temp;

    /* CASE 5: Local prot not applicable, previous status prot in use
     * Tunnel is currently oper down, OAM not capable, 
     * u1CPOrMgmtOperStatus is down */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    u1Temp = pTeTnlInfo->u1TnlLocalProtectInUse;
    pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_IN_USE;
    TeUpdateInfo.u4BkpTnlIndex = 0;
    TeUpdateInfo.u4BkpTnlInstance = 0;
    u4Source = OSIX_HTONL (0);
    u4Dest = OSIX_HTONL (0);
    MEMCPY (TeUpdateInfo.BkpTnlIngressLsrId, &u4Source, ROUTER_ID_LENGTH);
    MEMCPY (TeUpdateInfo.BkpTnlEgressLsrId, &u4Dest, ROUTER_ID_LENGTH);
    TeUpdateInfo.u1LocalProtection = LOCAL_PROT_NOT_APPLICABLE;
    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (pTeTnlInfo->u1TnlLocalProtectInUse != LOCAL_PROT_NOT_APPLICABLE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((pTeTnlInfo->u4BkpTnlIndex != TeUpdateInfo.u4BkpTnlIndex) ||
        (pTeTnlInfo->u4BkpTnlInstance != TeUpdateInfo.u4BkpTnlInstance) ||
        (MEMCMP
         (pTeTnlInfo->BkpTnlIngressLsrId, TeUpdateInfo.BkpTnlIngressLsrId,
          ROUTER_ID_LENGTH) != 0)
        ||
        (MEMCMP
         (pTeTnlInfo->BkpTnlEgressLsrId, TeUpdateInfo.BkpTnlEgressLsrId,
          ROUTER_ID_LENGTH) != 0))
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1TnlLocalProtectInUse = u1Temp;

    /* CASE 6: Local prot not applicable, previous status prot in use
     * Tunnel is currently oper down, OAM not capable, 
     * u1CPOrMgmtOperStatus is up */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    u1Temp = pTeTnlInfo->u1TnlLocalProtectInUse;
    u1Temp1 = pTeTnlInfo->u1CPOrMgmtOperStatus;
    pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_IN_USE;
    pTeTnlInfo->u1CPOrMgmtOperStatus = TE_OPER_UP;
    TeUpdateInfo.u4BkpTnlIndex = 0;
    TeUpdateInfo.u4BkpTnlInstance = 0;
    u4Source = OSIX_HTONL (0);
    u4Dest = OSIX_HTONL (0);
    MEMCPY (TeUpdateInfo.BkpTnlIngressLsrId, &u4Source, ROUTER_ID_LENGTH);
    MEMCPY (TeUpdateInfo.BkpTnlEgressLsrId, &u4Dest, ROUTER_ID_LENGTH);
    TeUpdateInfo.u1LocalProtection = LOCAL_PROT_NOT_APPLICABLE;
    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (pTeTnlInfo->u1TnlLocalProtectInUse != LOCAL_PROT_NOT_APPLICABLE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((pTeTnlInfo->u4BkpTnlIndex != TeUpdateInfo.u4BkpTnlIndex) ||
        (pTeTnlInfo->u4BkpTnlInstance != TeUpdateInfo.u4BkpTnlInstance) ||
        (MEMCMP
         (pTeTnlInfo->BkpTnlIngressLsrId, TeUpdateInfo.BkpTnlIngressLsrId,
          ROUTER_ID_LENGTH) != 0)
        ||
        (MEMCMP
         (pTeTnlInfo->BkpTnlEgressLsrId, TeUpdateInfo.BkpTnlEgressLsrId,
          ROUTER_ID_LENGTH) != 0))
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (pTeTnlInfo->u1TnlOperStatus == TE_OPER_DOWN)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1TnlLocalProtectInUse = u1Temp;
    pTeTnlInfo->u1CPOrMgmtOperStatus = u1Temp1;
    pTeTnlInfo->u1TnlOperStatus = TE_OPER_DOWN;

    /* CASE 7: Local prot not applicable, previous status prot in use
     * Tunnel is currently oper down, OAM not capable, 
     * u1CPOrMgmtOperStatus is up, Te Global admin DOWN */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    u1Temp = pTeTnlInfo->u1TnlLocalProtectInUse;
    u1Temp1 = pTeTnlInfo->u1CPOrMgmtOperStatus;
    pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_IN_USE;
    pTeTnlInfo->u1CPOrMgmtOperStatus = TE_OPER_UP;
    TeUpdateInfo.u4BkpTnlIndex = 0;
    TeUpdateInfo.u4BkpTnlInstance = 0;
    u4Source = OSIX_HTONL (0);
    u4Dest = OSIX_HTONL (0);
    MEMCPY (TeUpdateInfo.BkpTnlIngressLsrId, &u4Source, ROUTER_ID_LENGTH);
    MEMCPY (TeUpdateInfo.BkpTnlEgressLsrId, &u4Dest, ROUTER_ID_LENGTH);
    TeUpdateInfo.u1LocalProtection = LOCAL_PROT_NOT_APPLICABLE;
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    pTeTnlInfo->u1TnlLocalProtectInUse = u1Temp;
    pTeTnlInfo->u1CPOrMgmtOperStatus = u1Temp1;

    /* CASE 8: Local prot not applicable, previous status prot in use
     * Tunnel is currently oper down, OAM capable, 
     * u1CPOrMgmtOperStatus is down, OAM oper status is UP */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    bFlag = pTeTnlInfo->bIsOamEnabled;
    u1Temp = pTeTnlInfo->u1TnlLocalProtectInUse;
    u1Temp1 = pTeTnlInfo->u1CPOrMgmtOperStatus;
    u1Temp2 = pTeTnlInfo->u1OamOperStatus;
    pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_IN_USE;
    pTeTnlInfo->u1CPOrMgmtOperStatus = TE_OPER_DOWN;
    pTeTnlInfo->u1OamOperStatus = TE_OPER_UP;
    pTeTnlInfo->bIsOamEnabled = TRUE;
    TeUpdateInfo.u1LocalProtection = LOCAL_PROT_NOT_APPLICABLE;
    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1TnlLocalProtectInUse = u1Temp;
    pTeTnlInfo->u1CPOrMgmtOperStatus = u1Temp1;
    pTeTnlInfo->u1OamOperStatus = u1Temp2;
    pTeTnlInfo->bIsOamEnabled = bFlag;

    /* CASE 9: Local prot not applicable, previous status prot in use
     * Tunnel is currently oper down, OAM capable, 
     * u1CPOrMgmtOperStatus is up, OAM oper status is down */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    bFlag = pTeTnlInfo->bIsOamEnabled;
    u1Temp = pTeTnlInfo->u1TnlLocalProtectInUse;
    u1Temp1 = pTeTnlInfo->u1CPOrMgmtOperStatus;
    u1Temp2 = pTeTnlInfo->u1OamOperStatus;
    pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_IN_USE;
    pTeTnlInfo->u1CPOrMgmtOperStatus = TE_OPER_UP;
    pTeTnlInfo->u1OamOperStatus = TE_OPER_DOWN;
    pTeTnlInfo->bIsOamEnabled = TRUE;
    TeUpdateInfo.u1LocalProtection = LOCAL_PROT_NOT_APPLICABLE;
    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1TnlLocalProtectInUse = u1Temp;
    pTeTnlInfo->u1CPOrMgmtOperStatus = u1Temp1;
    pTeTnlInfo->u1OamOperStatus = u1Temp2;
    pTeTnlInfo->bIsOamEnabled = bFlag;

    /* CASE 10: Local prot not applicable, previous status prot in use
     * Tunnel is currently oper down, OAM capable, 
     * u1CPOrMgmtOperStatus is up, OAM oper status is up */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    bFlag = pTeTnlInfo->bIsOamEnabled;
    u1Temp = pTeTnlInfo->u1TnlLocalProtectInUse;
    u1Temp1 = pTeTnlInfo->u1CPOrMgmtOperStatus;
    u1Temp2 = pTeTnlInfo->u1OamOperStatus;
    pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_IN_USE;
    pTeTnlInfo->u1CPOrMgmtOperStatus = TE_OPER_UP;
    pTeTnlInfo->u1OamOperStatus = TE_OPER_UP;
    pTeTnlInfo->bIsOamEnabled = TRUE;
    TeUpdateInfo.u1LocalProtection = LOCAL_PROT_NOT_APPLICABLE;
    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1TnlLocalProtectInUse = u1Temp;
    pTeTnlInfo->u1CPOrMgmtOperStatus = u1Temp1;
    pTeTnlInfo->u1OamOperStatus = u1Temp2;
    pTeTnlInfo->bIsOamEnabled = bFlag;

    /* CASE 11: Local prot not available, previous status not prot in use */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    TeUpdateInfo.u1LocalProtection = LOCAL_PROT_NOT_AVAIL;
    if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_TeCmnExtNotifyTnlStatusToMeg ()
{
    tTeUpdateInfo       TeUpdateInfo;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    BOOL1               bFlag = FALSE;
    UINT4               u4Temp = 0;
    UINT4               u4Temp1 = 0;
    UINT4               u4Temp2 = 0;

    /* CASE 1: Not CP or MGMT OPR STATUS RELATED */
    TeUpdateInfo.u1TeUpdateOpr = TE_OAM_OPR_STATUS_RELATED;
    if (TeCmnExtNotifyTnlStatusToMeg (NULL, &TeUpdateInfo,
                                      TE_OAM_OPR_STATUS_RELATED) == TE_FAILURE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 2: CP OR MGMT OPR STATUS RELATED, tunnel oper down and no meg. */
    TeUpdateInfo.u1TeUpdateOpr = TE_CP_OR_MGMT_OPR_STATUS_RELATED;
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    bFlag = pTeTnlInfo->bIsOamEnabled;
    pTeTnlInfo->bIsOamEnabled = FALSE;
    if (TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, &TeUpdateInfo,
                                      TE_CP_OR_MGMT_OPR_STATUS_RELATED) ==
        TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->bIsOamEnabled = bFlag;

    /* CASE 3: CP OR MGMT OPR STATUS RELATED, tunnel oper up and no meg */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10;no shutdown;end");
    CliExecuteAppCmd ("c t;arp 20.0.0.2 00:11:22:33:44:55 vlan 2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    TeUpdateInfo.u1TeUpdateOpr = TE_CP_OR_MGMT_OPR_STATUS_RELATED;
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    bFlag = pTeTnlInfo->bIsOamEnabled;
    pTeTnlInfo->bIsOamEnabled = FALSE;
    if (TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, &TeUpdateInfo,
                                      TE_CP_OR_MGMT_OPR_STATUS_RELATED) ==
        TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->bIsOamEnabled = bFlag;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 4: CP OR MGMT OPR STATUS RELATED, tunnel oper up and meg present 
     * Meg index zero */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg MEG1");
    CliExecuteAppCmd ("service ME1");
    CliExecuteAppCmd ("mpls oam mep service ME1 lsp 10 1 5 6");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    TeUpdateInfo.u1TeUpdateOpr = TE_CP_OR_MGMT_OPR_STATUS_RELATED;
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    bFlag = pTeTnlInfo->bIsOamEnabled;
    pTeTnlInfo->bIsOamEnabled = FALSE;
    u4Temp = pTeTnlInfo->u4MegIndex;
    u4Temp1 = pTeTnlInfo->u4MeIndex;
    u4Temp2 = pTeTnlInfo->u4MpIndex;
    pTeTnlInfo->u4MegIndex = 0;
    pTeTnlInfo->u4MeIndex = 0;
    pTeTnlInfo->u4MpIndex = 0;
    if (TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, &TeUpdateInfo,
                                      TE_CP_OR_MGMT_OPR_STATUS_RELATED) ==
        TE_FAILURE)
    {
        pTeTnlInfo->bIsOamEnabled = bFlag;
        pTeTnlInfo->u4MegIndex = u4Temp;
        pTeTnlInfo->u4MeIndex = u4Temp1;
        pTeTnlInfo->u4MpIndex = u4Temp2;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t");
        CliExecuteAppCmd ("mpls oam meg MEG1");
        CliExecuteAppCmd ("no mpls oam mep service ME1");
        CliExecuteAppCmd ("no service ME1");
        CliExecuteAppCmd ("exit");
        CliExecuteAppCmd ("no mpls oam meg MEG1");
        CliExecuteAppCmd ("end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->bIsOamEnabled = bFlag;
    pTeTnlInfo->u4MegIndex = u4Temp;
    pTeTnlInfo->u4MeIndex = u4Temp1;
    pTeTnlInfo->u4MpIndex = u4Temp2;

    /* CASE 5: CP OR MGMT OPR STATUS RELATED, tunnel oper up and meg present
     * Meg index is NON ZERO */
    TeUpdateInfo.u1TeUpdateOpr = TE_CP_OR_MGMT_OPR_STATUS_RELATED;
    bFlag = pTeTnlInfo->bIsOamEnabled;
    pTeTnlInfo->bIsOamEnabled = FALSE;
    if (TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, &TeUpdateInfo,
                                      TE_CP_OR_MGMT_OPR_STATUS_RELATED) ==
        TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t");
        CliExecuteAppCmd ("mpls oam meg MEG1");
        CliExecuteAppCmd ("no mpls oam mep service ME1");
        CliExecuteAppCmd ("no service ME1");
        CliExecuteAppCmd ("exit");
        CliExecuteAppCmd ("no mpls oam meg MEG1");
        CliExecuteAppCmd ("end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->bIsOamEnabled = bFlag;

    /* CASE 6: CP OR MGMT OPR STATUS RELATED, tunnel oper down.
     * OAM capable */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10;shutdown;end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    TeUpdateInfo.u1TeUpdateOpr = TE_CP_OR_MGMT_OPR_STATUS_RELATED;
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    bFlag = pTeTnlInfo->bIsOamEnabled;
    pTeTnlInfo->bIsOamEnabled = TRUE;
    if (TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, &TeUpdateInfo,
                                      TE_CP_OR_MGMT_OPR_STATUS_RELATED) ==
        TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->bIsOamEnabled = bFlag;

    /* CASE 7: CP OR MGMT OPR STATUS RELATED, tunnel oper up.
     * OAM capable */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10;no shutdown;end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    TeUpdateInfo.u1TeUpdateOpr = TE_CP_OR_MGMT_OPR_STATUS_RELATED;
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    bFlag = pTeTnlInfo->bIsOamEnabled;
    pTeTnlInfo->bIsOamEnabled = TRUE;
    if (TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, &TeUpdateInfo,
                                      TE_CP_OR_MGMT_OPR_STATUS_RELATED) ==
        TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->bIsOamEnabled = bFlag;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls oam meg MEG1");
    CliExecuteAppCmd ("no mpls oam mep service ME1");
    CliExecuteAppCmd ("no service ME1");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no mpls oam meg MEG1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t;no arp 20.0.0.2;end");
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_FsTeMibIndexValidationAndRetrieving ()
{
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    nmhValidateIndexInstanceFsMplsTunnelTable (0, 0, 0, 0);
    nmhGetFirstIndexFsMplsTunnelTable (&u4TunnelIndex, &u4TunnelInstance,
                                       &u4IngressId, &u4EgressId);
    nmhGetNextIndexFsMplsTunnelTable (u4TunnelIndex, &u4TunnelIndex,
                                      u4TunnelInstance, &u4TunnelInstance,
                                      u4IngressId, &u4IngressId,
                                      u4EgressId, &u4EgressId);
    nmhValidateIndexInstanceFsMplsTpTunnelTable (0, 0, 0, 0);
    nmhGetFirstIndexFsMplsTpTunnelTable (&u4TunnelIndex, &u4TunnelInstance,
                                         &u4IngressId, &u4EgressId);
    nmhGetNextIndexFsMplsTpTunnelTable (u4TunnelIndex, &u4TunnelIndex,
                                        u4TunnelInstance, &u4TunnelInstance,
                                        u4IngressId, &u4IngressId,
                                        u4EgressId, &u4EgressId);

    return OSIX_SUCCESS;
}

INT4
Test_nmhGetFsMplsTunnelType ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    UINT1               au1Temp[1];

    MEMSET (au1Temp, 0, 1);
    Value.pu1_OctetList = au1Temp;

    /* Case 1: Tunnel Not present */
    if (nmhGetFsMplsTunnelType (0, 0, 0, 0, &Value) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 2: Tunnel present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (nmhGetFsMplsTunnelType (10, 1, 5, 6, &Value) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhGetFsMplsTunnelLSRIdMapInfo ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    UINT1               au1Temp[1];

    MEMSET (au1Temp, 0, 1);
    Value.pu1_OctetList = au1Temp;

    /* Case 1: Tunnel Not present */
    if (nmhGetFsMplsTunnelLSRIdMapInfo (0, 0, 0, 0, &Value) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 2: Tunnel present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (nmhGetFsMplsTunnelLSRIdMapInfo (10, 1, 5, 6, &Value) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhGetFsMplsTunnelMode ()
{
    INT4                i4Value = 0;

    /* Case 1: Tunnel Not present */
    if (nmhGetFsMplsTunnelMode (0, 0, 0, 0, &i4Value) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 2: Tunnel present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (nmhGetFsMplsTunnelMode (10, 1, 5, 6, &i4Value) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhGetFsMplsTunnelProactiveSessIndex ()
{
    UINT4               u4Value = 0;

    /* Case 1: Tunnel Not present */
    if (nmhGetFsMplsTunnelProactiveSessIndex (0, 0, 0, 0, &u4Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 2: Tunnel present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (nmhGetFsMplsTunnelProactiveSessIndex (10, 1, 5, 6, &u4Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_GmplsSpecificAndDummyNmh ()
{
    INT4                i4Value = 0;
    UINT4               u4Value = 0;
    tSNMP_OID_TYPE      OidValue;
    tSNMP_OCTET_STRING_TYPE Value;
    UINT4               u4ErrorCode = 0;
    tSnmpIndexList      SnmpIndexList;
    tSNMP_VAR_BIND      SnmpVarBind;

    nmhGetFsMplsTunnelMBBStatus (0, 0, 0, 0, &i4Value);
    nmhGetFsMplsTunnelDisJointType (0, 0, 0, 0, &i4Value);
    nmhGetFsMplsTunnelAttPointer (0, 0, 0, 0, &OidValue);
    nmhSetFsMplsTunnelMBBStatus (0, 0, 0, 0, 0);
    nmhSetFsMplsTunnelDisJointType (0, 0, 0, 0, 0);
    nmhSetFsMplsTunnelAttPointer (0, 0, 0, 0, &OidValue);
    nmhTestv2FsMplsTunnelMBBStatus (&u4ErrorCode, 0, 0, 0, 0, 0);
    nmhTestv2FsMplsTunnelDisJointType (&u4ErrorCode, 0, 0, 0, 0, 0);
    nmhTestv2FsMplsTunnelAttPointer (&u4ErrorCode, 0, 0, 0, 0, &OidValue);
    nmhDepv2FsMplsTunnelTable (&u4ErrorCode, &SnmpIndexList, &SnmpVarBind);
    nmhDepv2FsMplsTpTunnelTable (&u4ErrorCode, &SnmpIndexList, &SnmpVarBind);
    nmhGetFsTunnelAttributeIndexNext (&u4Value);
    nmhValidateIndexInstanceFsTunnelAttributeTable (u4Value);
    nmhGetFirstIndexFsTunnelAttributeTable (&u4Value);
    nmhGetNextIndexFsTunnelAttributeTable (u4Value, &u4Value);
    nmhGetFsTunnelAttributeName (u4Value, &Value);
    nmhGetFsTunnelAttributeSetupPrio (u4Value, &i4Value);
    nmhGetFsTunnelAttributeHoldingPrio (u4Value, &i4Value);
    nmhGetFsTunnelAttributeIncludeAnyAffinity (u4Value, &Value);
    nmhGetFsTunnelAttributeIncludeAllAffinity (u4Value, &Value);
    nmhGetFsTunnelAttributeExcludeAnyAffinity (u4Value, &Value);
    nmhGetFsTunnelAttributeSessionAttributes (u4Value, &Value);
    nmhGetFsTunnelAttributeBandwidth (u4Value, &i4Value);
    nmhGetFsTunnelAttributeTeClassType (u4Value, &i4Value);
    nmhGetFsTunnelAttributeRowStatus (u4Value, &i4Value);
    nmhSetFsTunnelAttributeName (u4Value, &Value);
    nmhSetFsTunnelAttributeSetupPrio (u4Value, i4Value);
    nmhSetFsTunnelAttributeHoldingPrio (u4Value, i4Value);
    nmhSetFsTunnelAttributeIncludeAnyAffinity (u4Value, &Value);
    nmhSetFsTunnelAttributeIncludeAllAffinity (u4Value, &Value);
    nmhSetFsTunnelAttributeExcludeAnyAffinity (u4Value, &Value);
    nmhSetFsTunnelAttributeSessionAttributes (u4Value, &Value);
    nmhSetFsTunnelAttributeBandwidth (u4Value, i4Value);
    nmhSetFsTunnelAttributeTeClassType (u4Value, i4Value);
    nmhSetFsTunnelAttributeRowStatus (u4Value, i4Value);
    nmhTestv2FsTunnelAttributeName (&u4ErrorCode, u4Value, &Value);
    nmhTestv2FsTunnelAttributeSetupPrio (&u4ErrorCode, u4Value, i4Value);
    nmhTestv2FsTunnelAttributeHoldingPrio (&u4ErrorCode, u4Value, i4Value);
    nmhTestv2FsTunnelAttributeIncludeAnyAffinity (&u4ErrorCode, u4Value,
                                                  &Value);
    nmhTestv2FsTunnelAttributeIncludeAllAffinity (&u4ErrorCode, u4Value,
                                                  &Value);
    nmhTestv2FsTunnelAttributeExcludeAnyAffinity (&u4ErrorCode, u4Value,
                                                  &Value);
    nmhTestv2FsTunnelAttributeSessionAttributes (&u4ErrorCode, u4Value, &Value);
    nmhTestv2FsTunnelAttributeBandwidth (&u4ErrorCode, u4Value, i4Value);
    nmhTestv2FsTunnelAttributeTeClassType (&u4ErrorCode, u4Value, i4Value);
    nmhTestv2FsTunnelAttributeRowStatus (&u4ErrorCode, u4Value, i4Value);
    nmhDepv2FsTunnelAttributeTable (&u4ErrorCode, &SnmpIndexList, &SnmpVarBind);

    return OSIX_SUCCESS;
}

INT4
Test_nmhSetFsMplsTunnelType ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    UINT1               au1Temp[1];

    MEMSET (au1Temp, 0, 1);
    Value.pu1_OctetList = au1Temp;

    /* CASE 1: Tunnel not present */
    if (nmhSetFsMplsTunnelType (0, 0, 0, 0, &Value) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Tunnel present but not active */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsTunnelType (10, 1, 5, 6, &Value) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 3: Tunnel present and active */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsTunnelType (10, 1, 5, 6, &Value) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhSetFsMplsTunnelLSRIdMapInfo ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    UINT1               au1Temp[1];

    MEMSET (au1Temp, 0, 1);
    Value.pu1_OctetList = au1Temp;

    /* CASE 1: Tunnel not present */
    if (nmhSetFsMplsTunnelLSRIdMapInfo (0, 0, 0, 0, &Value) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Tunnel present but not active */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsTunnelLSRIdMapInfo (10, 1, 5, 6, &Value) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 3: Tunnel present and active */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsTunnelLSRIdMapInfo (10, 1, 5, 6, &Value) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhSetFsMplsTunnelMode ()
{
    INT4                i4Value = 0;

    /* CASE 1: Tunnel not present */
    if (nmhSetFsMplsTunnelMode (0, 0, 0, 0, i4Value) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Tunnel present but not active */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsTunnelMode (10, 1, 5, 6, i4Value) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 3: Tunnel present and active */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsTunnelMode (10, 1, 5, 6, i4Value) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2FsMplsTunnelType ()
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE Value;
    UINT1               au1Temp[1];

    MEMSET (au1Temp, 0, 1);
    Value.pu1_OctetList = au1Temp;

    /* CASE 1: TE GBL ADMIN DOWN */
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 0, 0, 0, 0, &Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Range Validation for tunnel Index */
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 0, 0, 0, 0, &Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 65540, 0, 0, 0, &Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 3: Tunnel Instance greater than 65535 */
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 65540, 0, 0, &Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 4: Tunnel not present */
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 0, 0, &Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 5: Tunnel Type validations */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    Value.pu1_OctetList[0] = TE_TNL_TYPE_MPLS;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = TE_TNL_TYPE_MPLSTP;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = TE_TNL_TYPE_MPLSTP + TE_TNL_TYPE_GMPLS;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = TE_TNL_TYPE_MPLSTP + TE_TNL_TYPE_HLSP;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = TE_TNL_TYPE_GMPLS + TE_TNL_TYPE_HLSP;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = TE_TNL_TYPE_MPLS + TE_TNL_TYPE_HLSP;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = TE_TNL_TYPE_GMPLS;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = TE_TNL_TYPE_HLSP;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = 0;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 6: Tunnel ACTIVE */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    Value.pu1_OctetList[0] = TE_TNL_TYPE_HLSP;
    if (nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2FsMplsTunnelLSRIdMapInfo ()
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE Value;
    UINT1               au1Temp[1];

    MEMSET (au1Temp, 0, 1);
    Value.pu1_OctetList = au1Temp;

    /* CASE 1: TE GBL ADMIN DOWN */
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 0, 0, 0, 0, &Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Range Validation for tunnel Index */
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 0, 0, 0, 0, &Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 65540, 0, 0, 0, &Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 3: Tunnel Instance greater than 65535 */
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo
        (&u4ErrorCode, 10, 65540, 0, 0, &Value) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 4: Tunnel not present */
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 10, 1, 0, 0, &Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 5: Input validations */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    Value.pu1_OctetList[0] = TE_TNL_INGRESSID_MAP_INFO;
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = TE_TNL_EGRESSID_MAP_INFO;
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0]
        = TE_TNL_INGRESSID_MAP_INFO + TE_TNL_EGRESSID_MAP_INFO;
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = MPLS_ZERO;
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = 0x40;
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    Value.pu1_OctetList[0] = TE_TNL_INGRESSID_MAP_INFO;
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 6: Tunnel ACTIVE */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    Value.pu1_OctetList[0] = MPLS_ZERO;
    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode, 10, 1, 5, 6, &Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2FsMplsTunnelMode ()
{
    UINT4               u4ErrorCode = 0;
    INT4                i4Value = 0;

    /* CASE 1: TE GBL ADMIN DOWN */
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode, 0, 0, 0, 0, i4Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Range Validation for tunnel Index */
    if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode, 0, 0, 0, 0, i4Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode, 65540, 0, 0, 0, i4Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 3: Tunnel Instance greater than 65535 */
    if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode, 10, 65540, 0, 0, i4Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 4: Tunnel not present */
    if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode, 10, 1, 0, 0, i4Value)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 5: Input validations */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    i4Value = TE_TNL_MODE_UNIDIRECTIONAL;
    if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    i4Value = TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL;
    if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    i4Value = TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL;
    if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    i4Value = MPLS_ZERO;
    if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        return OSIX_FAILURE;
    }

    /* CASE 6: Tunnel ACTIVE */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    i4Value = TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL;
    if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhGetFsMplsTpTunnelDestTunnelIndex ()
{
    UINT4               u4Value = 0;

    /* Case 1: Tunnel Not present */
    if (nmhGetFsMplsTpTunnelDestTunnelIndex (0, 0, 0, 0, &u4Value) ==
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 2: Tunnel present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (nmhGetFsMplsTpTunnelDestTunnelIndex (10, 1, 5, 6, &u4Value) ==
        SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhGetFsMplsTpTunnelDestTunnelLspNum ()
{
    UINT4               u4Value = 0;

    /* Case 1: Tunnel Not present */
    if (nmhGetFsMplsTpTunnelDestTunnelLspNum (0, 0, 0, 0, &u4Value) ==
        SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Case 2: Tunnel present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown ");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (nmhGetFsMplsTpTunnelDestTunnelLspNum (10, 1, 5, 6, &u4Value) ==
        SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhSetFsMplsTpTunnelDestTunnelIndex ()
{

    /* CASE 1: Tunnel not present */
    if (nmhSetFsMplsTpTunnelDestTunnelIndex (0, 0, 0, 0, 2) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Tunnel present but not active */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsTpTunnelDestTunnelIndex (10, 1, 5, 6, 2) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 3: Tunnel present and active */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsTpTunnelDestTunnelIndex (10, 1, 5, 6, 2) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhSetFsMplsTpTunnelDestTunnelLspNum ()
{

    /* CASE 1: Tunnel not present */
    if (nmhSetFsMplsTpTunnelDestTunnelLspNum (0, 0, 0, 0, 2) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Tunnel present but not active */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsTpTunnelDestTunnelLspNum (10, 1, 5, 6, 2) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE 3: Tunnel present and active */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsTpTunnelDestTunnelLspNum (10, 1, 5, 6, 2) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2FsMplsTpTunnelDestTunnelIndex ()
{
    UINT4               u4ErrorCode = 0;

    /* CASE 1: TE GBL ADMIN DOWN */
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    if (nmhTestv2FsMplsTpTunnelDestTunnelIndex (&u4ErrorCode, 0, 0, 0, 0, 0)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Range Validation for tunnel Index */
    if (nmhTestv2FsMplsTpTunnelDestTunnelIndex (&u4ErrorCode, 0, 0, 0, 0, 0)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMplsTpTunnelDestTunnelIndex (&u4ErrorCode, 65540, 0, 0, 0, 0)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 3: Tunnel Instance greater than 65535 */
    if (nmhTestv2FsMplsTpTunnelDestTunnelIndex
        (&u4ErrorCode, 10, 65540, 0, 0, 0) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 4: Input validations */
    if (nmhTestv2FsMplsTpTunnelDestTunnelIndex (&u4ErrorCode, 10, 1, 0, 0, 0)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMplsTpTunnelDestTunnelIndex
        (&u4ErrorCode, 10, 1, 0, 0, 65540) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        return OSIX_FAILURE;
    }

    /* CASE 5: Tunnel not present */
    if (nmhTestv2FsMplsTpTunnelDestTunnelIndex (&u4ErrorCode, 10, 1, 0, 0, 1)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 5: Tunnel Type validations */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhTestv2FsMplsTpTunnelDestTunnelIndex (&u4ErrorCode, 10, 1, 5, 6, 1)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 6: Tunnel ACTIVE */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhTestv2FsMplsTpTunnelDestTunnelIndex (&u4ErrorCode, 10, 1, 5, 6, 1)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2FsMplsTpTunnelDestTunnelLspNum ()
{
    UINT4               u4ErrorCode = 0;

    /* CASE 1: TE GBL ADMIN DOWN */
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    if (nmhTestv2FsMplsTpTunnelDestTunnelLspNum (&u4ErrorCode, 0, 0, 0, 0, 0)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Range Validation for tunnel Index */
    if (nmhTestv2FsMplsTpTunnelDestTunnelLspNum (&u4ErrorCode, 0, 0, 0, 0, 0)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }
    if (nmhTestv2FsMplsTpTunnelDestTunnelLspNum
        (&u4ErrorCode, 65540, 0, 0, 0, 0) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 3: Tunnel Instance greater than 65535 */
    if (nmhTestv2FsMplsTpTunnelDestTunnelLspNum
        (&u4ErrorCode, 10, 65540, 0, 0, 0) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 4: Input validations */
    if (nmhTestv2FsMplsTpTunnelDestTunnelLspNum
        (&u4ErrorCode, 10, 1, 0, 0, 65540) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        return OSIX_FAILURE;
    }

    /* CASE 5: Tunnel not present */
    if (nmhTestv2FsMplsTpTunnelDestTunnelLspNum (&u4ErrorCode, 10, 1, 0, 0, 1)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 5: success case */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhTestv2FsMplsTpTunnelDestTunnelIndex (&u4ErrorCode, 10, 1, 5, 6, 1)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 6: Tunnel ACTIVE */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhTestv2FsMplsTpTunnelDestTunnelLspNum (&u4ErrorCode, 10, 1, 5, 6, 1)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_FsLsrMibIndexValidationAndRetrieving ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    UINT1               au1Temp[4] = { 0, 0, 0, 0 };

    Value.pu1_OctetList = au1Temp;
    Value.i4_Length = 4;

    nmhValidateIndexInstanceFsMplsInSegmentTable (&Value);
    nmhGetFirstIndexFsMplsInSegmentTable (&Value);
    nmhGetNextIndexFsMplsInSegmentTable (&Value, &Value);
    nmhValidateIndexInstanceFsMplsOutSegmentTable (&Value);
    nmhGetFirstIndexFsMplsOutSegmentTable (&Value);
    nmhGetNextIndexFsMplsOutSegmentTable (&Value, &Value);

    return OSIX_SUCCESS;
}

INT4
Test_FsLsrMibDummyNmh ()
{
    UINT4               u4ErrorCode = 0;
    tSnmpIndexList      SnmpIndexList;
    tSNMP_VAR_BIND      SnmpVarBind;

    nmhDepv2FsMplsInSegmentTable (&u4ErrorCode, &SnmpIndexList, &SnmpVarBind);
    nmhDepv2FsMplsOutSegmentTable (&u4ErrorCode, &SnmpIndexList, &SnmpVarBind);

    return OSIX_SUCCESS;
}

INT4
Test_nmhGetFsMplsInSegmentDirection ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    INT4                i4Direction = 0;
    UINT1               au1Temp[4] = { 0, 0, 0, 1 };

    Value.pu1_OctetList = au1Temp;
    Value.i4_Length = 4;

    /* CASE 1: Insegment entry not present. */
    if (nmhGetFsMplsInSegmentDirection (&Value, &i4Direction) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Insegment entry present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 7 source 6");
    CliExecuteAppCmd
        ("tunnel mpls static in-label 200001 vlan 2 out-label 200002 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhGetFsMplsInSegmentDirection (&Value, &i4Direction) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhGetFsMplsOutSegmentDirection ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    INT4                i4Direction = 0;
    UINT1               au1Temp[4] = { 0, 0, 0, 1 };

    Value.pu1_OctetList = au1Temp;
    Value.i4_Length = 4;

    /* CASE 1: Insegment entry not present. */
    if (nmhGetFsMplsOutSegmentDirection (&Value, &i4Direction) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Insegment entry present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 7 source 6");
    CliExecuteAppCmd
        ("tunnel mpls static in-label 200001 vlan 2 out-label 200002 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhGetFsMplsOutSegmentDirection (&Value, &i4Direction) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhSetFsMplsInSegmentDirection ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    INT4                i4Direction = 2;
    UINT1               au1Temp[4] = { 0, 0, 0, 1 };

    Value.pu1_OctetList = au1Temp;
    Value.i4_Length = 4;

    /* CASE 1: Insegment entry not present. */
    if (nmhSetFsMplsInSegmentDirection (&Value, i4Direction) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Insegment entry present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 7 source 6");
    CliExecuteAppCmd
        ("tunnel mpls static in-label 200001 vlan 2 out-label 200002 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsInSegmentDirection (&Value, i4Direction) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;

}

INT4
Test_nmhSetFsMplsOutSegmentDirection ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    INT4                i4Direction = 2;
    UINT1               au1Temp[4] = { 0, 0, 0, 1 };

    Value.pu1_OctetList = au1Temp;
    Value.i4_Length = 4;

    /* CASE 1: Insegment entry not present. */
    if (nmhSetFsMplsOutSegmentDirection (&Value, i4Direction) == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Insegment entry present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 7 source 6");
    CliExecuteAppCmd
        ("tunnel mpls static in-label 200001 vlan 2 out-label 200002 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhSetFsMplsOutSegmentDirection (&Value, i4Direction) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;

}

INT4
Test_nmhTestv2FsMplsInSegmentDirection ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    UINT1               au1Temp[4] = { 0, 0, 0, 0 };
    UINT4               u4ErrorCode = 0;
    tInSegment         *pInSegment = NULL;

    Value.pu1_OctetList = au1Temp;
    Value.i4_Length = 4;

    /* CASE 1: Index validation */
    if (nmhTestv2FsMplsInSegmentDirection (&u4ErrorCode, &Value, 2)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    au1Temp[0] = 255;
    au1Temp[1] = 255;
    au1Temp[2] = 255;
    au1Temp[3] = 255;
    Value.pu1_OctetList = au1Temp;
    if (nmhTestv2FsMplsInSegmentDirection (&u4ErrorCode, &Value, 2)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Input Validation */
    au1Temp[0] = 0;
    au1Temp[1] = 0;
    au1Temp[2] = 0;
    au1Temp[3] = 1;
    Value.pu1_OctetList = au1Temp;
    if (nmhTestv2FsMplsInSegmentDirection (&u4ErrorCode, &Value, 3)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMplsInSegmentDirection (&u4ErrorCode, &Value, 1)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_NAME)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMplsInSegmentDirection (&u4ErrorCode, &Value, 2)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_NAME)
    {
        return OSIX_FAILURE;
    }

    /* CASE 3: Insegment entry active. */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 7 source 6");
    CliExecuteAppCmd
        ("tunnel mpls static in-label 200001 vlan 2 out-label 200002 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    au1Temp[0] = 0;
    au1Temp[1] = 0;
    au1Temp[2] = 0;
    au1Temp[3] = 1;
    Value.pu1_OctetList = au1Temp;
    pInSegment = MplsGetInSegmentTableEntry (1);
    pInSegment->u1RowStatus = 3;
    if (nmhTestv2FsMplsInSegmentDirection (&u4ErrorCode, &Value, 2)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pInSegment->u1RowStatus = 1;

    /* CASE 4: Insegment entry not active. */
    if (nmhTestv2FsMplsInSegmentDirection (&u4ErrorCode, &Value, 2)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2FsMplsOutSegmentDirection ()
{
    tSNMP_OCTET_STRING_TYPE Value;
    tOutSegment        *pOutSegment = NULL;
    UINT1               au1Temp[4] = { 0, 0, 0, 0 };
    UINT4               u4ErrorCode = 0;

    Value.pu1_OctetList = au1Temp;
    Value.i4_Length = 4;

    /* CASE 1: Index validation */
    if (nmhTestv2FsMplsOutSegmentDirection (&u4ErrorCode, &Value, 2)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    au1Temp[0] = 255;
    au1Temp[1] = 255;
    au1Temp[2] = 255;
    au1Temp[3] = 255;
    Value.pu1_OctetList = au1Temp;
    if (nmhTestv2FsMplsOutSegmentDirection (&u4ErrorCode, &Value, 2)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Input Validation */
    au1Temp[0] = 0;
    au1Temp[1] = 0;
    au1Temp[2] = 0;
    au1Temp[3] = 1;
    Value.pu1_OctetList = au1Temp;
    if (nmhTestv2FsMplsOutSegmentDirection (&u4ErrorCode, &Value, 3)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMplsOutSegmentDirection (&u4ErrorCode, &Value, 1)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_NAME)
    {
        return OSIX_FAILURE;
    }

    if (nmhTestv2FsMplsOutSegmentDirection (&u4ErrorCode, &Value, 2)
        == SNMP_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_NAME)
    {
        return OSIX_FAILURE;
    }

    /* CASE 3: Insegment entry active. */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 7 source 6");
    CliExecuteAppCmd
        ("tunnel mpls static in-label 200001 vlan 2 out-label 200002 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    au1Temp[0] = 0;
    au1Temp[1] = 0;
    au1Temp[2] = 0;
    au1Temp[3] = 1;
    Value.pu1_OctetList = au1Temp;
    pOutSegment = MplsGetOutSegmentTableEntry (1);
    pOutSegment->u1RowStatus = 3;
    if (nmhTestv2FsMplsOutSegmentDirection (&u4ErrorCode, &Value, 2)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pOutSegment->u1RowStatus = 1;

    /* CASE 4: Outsegment entry not active. */
    if (nmhTestv2FsMplsOutSegmentDirection (&u4ErrorCode, &Value, 2)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

INT4
Test_TeCliGetTnlIndices ()
{
    UINT4               u4TnlIndex = 0;
    UINT4               u4TnlInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    gaTnlCliArgs[0].u4TnlIndex = 10;
    gaTnlCliArgs[0].u4TnlInstance = 1;
    gaTnlCliArgs[0].u4IngressId = 0;
    gaTnlCliArgs[0].u4EgressId = 6;

    /* CASE 1: Ingress Id is zero */
    if (TeCliGetTnlIndices (0, &u4TnlIndex, &u4TnlInstance, &u4IngressId,
                            &u4EgressId) == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if ((u4TnlIndex != gaTnlCliArgs[0].u4TnlIndex) ||
        (u4TnlInstance != gaTnlCliArgs[0].u4TnlInstance) ||
        (u4IngressId != gaTnlCliArgs[0].u4IngressId) ||
        (u4EgressId != gaTnlCliArgs[0].u4EgressId))
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: Egress Id is zero */
    gaTnlCliArgs[0].u4IngressId = 5;
    gaTnlCliArgs[0].u4EgressId = 0;
    if (TeCliGetTnlIndices (0, &u4TnlIndex, &u4TnlInstance, &u4IngressId,
                            &u4EgressId) == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if ((u4TnlIndex != gaTnlCliArgs[0].u4TnlIndex) ||
        (u4TnlInstance != gaTnlCliArgs[0].u4TnlInstance) ||
        (u4IngressId != gaTnlCliArgs[0].u4IngressId) ||
        (u4EgressId != gaTnlCliArgs[0].u4EgressId))
    {
        return OSIX_FAILURE;
    }

    /* CASE 3: Ingress Id and Egress Id is zero, Tunnel not present */
    gaTnlCliArgs[0].u4IngressId = 0;
    gaTnlCliArgs[0].u4EgressId = 0;
    if (TeCliGetTnlIndices (0, &u4TnlIndex, &u4TnlInstance, &u4IngressId,
                            &u4EgressId) == CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 4: Ingress Id and Egress Id is zero, Tunnel present */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (TeCliGetTnlIndices (0, &u4TnlIndex, &u4TnlInstance, &u4IngressId,
                            &u4EgressId) == CLI_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((u4TnlIndex != 10) || (u4TnlInstance != 1) || (u4IngressId != 5)
        || (u4EgressId != 6))
    {
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_TeUpdateTnlStatus ()
{
    tTeTnlInfo         *pTeTnlInfo = 0;
    tTeUpdateInfo       TeUpdateInfo;
    tTeTnlInfo          TeTnlInfo;
    UINT4               u4Source = 0;
    UINT4               u4Dest = 0;

    /* CASE 1: Te admin down */
    if (TeUpdateTnlStatus (NULL, NULL) == TE_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: pTeTnlInfo -> NULL */
    if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 3: pTeUpdateInfo -> NULL */
    if (TeUpdateTnlStatus (&TeTnlInfo, NULL) == TE_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 4: Tunnel not present */
    TeTnlInfo.u4TnlIndex = 10;
    TeTnlInfo.u4TnlInstance = 1;
    u4Source = OSIX_HTONL (0);
    MEMCPY (TeTnlInfo.TnlIngressLsrId, &u4Source, ROUTER_ID_LENGTH);
    u4Dest = OSIX_HTONL (0);
    MEMCPY (TeTnlInfo.TnlEgressLsrId, &u4Dest, ROUTER_ID_LENGTH);
    if (TeUpdateTnlStatus (&TeTnlInfo, &TeUpdateInfo) == TE_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 5: CP OR MGMT */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    TeUpdateInfo.u1TeUpdateOpr = TE_CP_OR_MGMT_OPR_STATUS_RELATED;
    if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 6: OAM */
    TeUpdateInfo.u1TeUpdateOpr = TE_OAM_OPR_STATUS_RELATED;
    if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 7: Local prot */
    TeUpdateInfo.u1TeUpdateOpr = TE_LOCAL_PROT_RELATED;
    if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 8: Role -> Intermediate */
    TeUpdateInfo.u1TeUpdateOpr = TE_TNL_ROLE_RELATED;
    TeUpdateInfo.u1TnlRole = TE_INTERMEDIATE;
    if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 9: Role -> Egress  */
    TeUpdateInfo.u1TeUpdateOpr = TE_TNL_ROLE_RELATED;
    TeUpdateInfo.u1TnlRole = TE_EGRESS;
    if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE 10: Role -> Other */
    TeUpdateInfo.u1TeUpdateOpr = TE_TNL_ROLE_RELATED;
    TeUpdateInfo.u1TnlRole = TE_INGRESS;
    if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u1TnlRole = TE_INGRESS;

    /* CASE 11: Other */
    TeUpdateInfo.u1TeUpdateOpr = 0;
    if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_TeValidateAndGetIngressId ()
{
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4IngressLocalMapNum = 0;
    UINT4               u4EgressLocalMapNum = 0;

    /* CASE 1: */
    u4IngressId = 10;
    u4EgressLocalMapNum = 10;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 2: */
    u4EgressId = 10;
    u4IngressLocalMapNum = 10;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 3: */
    u4IngressId = 0;
    u4EgressLocalMapNum = 0;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 4: */
    u4IngressId = 0;
    u4IngressLocalMapNum = 0;
    u4EgressId = 20;
    u4EgressLocalMapNum = 0;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 5: */
    u4EgressId = 0x14000002;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (u4IngressId != 0x14000001)
    {
        return OSIX_FAILURE;
    }

    /* CASE 6 */
    u4IngressId = 0;
    u4IngressLocalMapNum = 0;
    u4EgressId = 0;
    u4EgressLocalMapNum = 8;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 7: */
    u4EgressLocalMapNum = 6;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    if (u4IngressLocalMapNum != 5)
    {
        return OSIX_FAILURE;
    }

    /* CASE 8: */
    u4EgressId = 8;
    u4IngressId = 8;
    u4EgressLocalMapNum = 0;
    u4IngressLocalMapNum = 0;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 8: */
    u4EgressLocalMapNum = 8;
    u4IngressLocalMapNum = 8;
    u4EgressId = 0;
    u4IngressId = 0;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 9: */
    u4EgressId = 0;
    u4IngressId = 0;
    u4IngressLocalMapNum = 20;
    u4EgressLocalMapNum = 0;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 10: */
    u4IngressLocalMapNum = 5;
    u4EgressLocalMapNum = 20;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* CASE 11: */
    u4IngressLocalMapNum = 5;
    u4EgressLocalMapNum = 6;
    if (TeValidateAndGetIngressId (0, u4EgressId, u4EgressLocalMapNum,
                                   &u4IngressId, &u4IngressLocalMapNum)
        == CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
Test_VerifyAllStaticTunnels ()
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 11");
    CliExecuteAppCmd ("tunnel mpls destination 5 source 6");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("interface mplstunnel 11");
    CliExecuteAppCmd ("no tunnel mpls destination 5 source 6");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 11");
    CliExecuteAppCmd ("tunnel mpls destination 5 source 6");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls static out-label 200003 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 11");
    CliExecuteAppCmd ("tunnel mpls static in-label 200004 vlan 2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");
    CliExecuteAppCmd ("c t; no interface mplstunnel 11; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 11");
    CliExecuteAppCmd ("tunnel mpls destination 5 source 6");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");
    CliExecuteAppCmd ("c t; no interface mplstunnel 11; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 11");
    CliExecuteAppCmd ("tunnel mpls destination 5 source 6");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mode associated-bidirectional");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 11");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 11");
    CliExecuteAppCmd ("tunnel mode associated-bidirectional");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 10");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");
    CliExecuteAppCmd ("c t; no interface mplstunnel 11; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 11");
    CliExecuteAppCmd ("tunnel mpls destination 5 source 6");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mode associated-bidirectional");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 11 lsp-num 1");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 11");
    CliExecuteAppCmd ("tunnel mode associated-bidirectional");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 10 lsp-num 1");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");
    CliExecuteAppCmd ("c t; no interface mplstunnel 11; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel type gmpls");
    CliExecuteAppCmd ("tunnel type mpls");
    CliExecuteAppCmd ("tunnel type h-lsp");
    CliExecuteAppCmd ("tunnel type mpls-tp");
    CliExecuteAppCmd ("tunnel type gmpls mpls");
    CliExecuteAppCmd ("tunnel type gmpls h-lsp");
    CliExecuteAppCmd ("tunnel type gmpls mpls-tp");
    CliExecuteAppCmd ("tunnel type mpls h-lsp");
    CliExecuteAppCmd ("tunnel type mpls mpls-tp");
    CliExecuteAppCmd ("tunnel type h-lsp mpls-tp");
    CliExecuteAppCmd ("tunnel type gmpls mpls h-lsp");
    CliExecuteAppCmd ("tunnel type gmpls mpls mpls-tp");
    CliExecuteAppCmd ("tunnel type gmpls h-lsp mpls-tp");
    CliExecuteAppCmd ("tunnel type mpls h-lsp mpls-tp");
    CliExecuteAppCmd ("tunnel type gmpls mpls h-lsp mpls-tp");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 11");
    CliExecuteAppCmd ("no tunnel mpls destination tnl-num");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 11 lsp-num 1");
    CliExecuteAppCmd ("no tunnel mpls destination tnl-num lsp-num");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 11 lsp-num 1");
    CliExecuteAppCmd ("no tunnel mpls destination tnl-num");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 11 lsp-num 1");
    CliExecuteAppCmd ("no tunnel mpls destination lsp-num");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 11");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 12");
    CliExecuteAppCmd ("no tunnel mpls destination tnl-num");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 11 lsp-num 1");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 12 lsp-num 2");
    CliExecuteAppCmd ("no tunnel mpls destination tnl-num");
    CliExecuteAppCmd ("no tunnel mpls destination lsp-num");
    CliExecuteAppCmd ("tunnel mpls destination tnl-num 11 lsp-num 1");
    CliExecuteAppCmd ("no tunnel mpls destination tnl-num lsp-num");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel type gmpls mpls h-lsp mpls-tp");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no tunnel type");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel type gmpls mpls h-lsp mpls-tp");
    CliExecuteAppCmd ("no tunnel type");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel signalling protocol rsvp");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel signalling protocol crldp");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mode unidirectional");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mode unidirectional");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mode associated-bidirectional");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mode associated-bidirectional");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mode corouted-bidirectional");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mode corouted-bidirectional");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no tunnel mode");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t; no interface mplstunnel 10; end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mode corouted-bidirectional");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 1000");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5 lsp-num 2");
    CliExecuteAppCmd ("tunnel mode corouted-bidirectional");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 1000");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5 lsp-num 3");
    CliExecuteAppCmd ("tunnel mode corouted-bidirectional");
    CliExecuteAppCmd ("tunnel mpls static out-label 200003 25.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200004 vlan 3");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 1000");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no tunnel mpls destination 6 source 5 lsp-num 2");
    CliExecuteAppCmd ("no tunnel mpls destination 6 source 5 lsp-num 3");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5 lsp-num 2");
    CliExecuteAppCmd ("tunnel mode corouted-bidirectional");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 1000");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5 lsp-num 3");
    CliExecuteAppCmd ("tunnel mode corouted-bidirectional");
    CliExecuteAppCmd ("tunnel mpls static out-label 200003 25.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200004 vlan 3");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 1000");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("no interface mplstunnel 10");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("tunnel mode corouted-bidirectional");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 1000");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");

    CliExecuteAppCmd ("c t;mpls binding ipv4 25.0.0.0 255.0.0.0 te 10;end");

    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");

    CliExecuteAppCmd ("c t;no mpls binding ipv4 25.0.0.0 255.0.0.0;end");

    CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5 lsp-num 2");
    CliExecuteAppCmd ("tunnel mode corouted-bidirectional");
    CliExecuteAppCmd ("tunnel mpls static out-label 200001 20.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static in-label 200002 vlan 2");
    CliExecuteAppCmd ("tunnel mpls traffic-eng bandwidth 1000");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t;mpls binding ipv4 25.0.0.0 255.0.0.0 te 10;end");

    CliExecuteAppCmd ("c t;no mpls binding ipv4 25.0.0.0 255.0.0.0;end");

    CliExecuteAppCmd ("c t;mpls binding ipv4 25.0.0.0 255.0.0.0 te 10 2");

    CliExecuteAppCmd ("c t;no mpls binding ipv4 25.0.0.0 255.0.0.0;end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("mpls binding ipv4 25.0.0.0 255.0.0.0 te 10 destination 6 source 5");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t;no mpls binding ipv4 25.0.0.0 255.0.0.0;end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("mpls binding ipv4 25.0.0.0 255.0.0.0 te 10 destination 6 source 5 lsp-num 2");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t;no mpls binding ipv4 25.0.0.0 255.0.0.0;end");

    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

PRIVATE VOID
CreateConf1 (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("sh sp;set gm dis;set gv dis;sh ga;exit");
    CliExecuteAppCmd ("int gi 0/1;map swi default;no shut;exit");
    CliExecuteAppCmd ("int gi 0/2;map swi default;no shut;exit");
    CliExecuteAppCmd ("int gi 0/3;map swi default;no shut;exit");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 1;no ports;exit");
    CliExecuteAppCmd ("vlan 2;ports gi 0/1;exit");
    CliExecuteAppCmd ("vlan 3;ports gi 0/2;exit");
    CliExecuteAppCmd ("vlan 4;ports gi 0/3;exit");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.0.0.0;no shutdown;mpls ip;exit");
    CliExecuteAppCmd ("int vlan 3");
    CliExecuteAppCmd ("ip address 25.0.0.1 255.0.0.0;no shutdown;mpls ip;exit");
    CliExecuteAppCmd ("int vlan 4");
    CliExecuteAppCmd ("ip address 35.0.0.1 255.0.0.0;no shutdown;mpls ip;end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 7 global-id 300 node-id 30");
    CliExecuteAppCmd ("end ");
    CliGiveAppContext ();
    MGMT_LOCK ();
}

PRIVATE VOID
DeleteConf1 (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10 ");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 7 global-id 300 node-id 30");
    CliExecuteAppCmd ("no mpls global-id icc-id node-id");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface vlan 2;no mpls ip;exit");
    CliExecuteAppCmd ("interface vlan 3;no mpls ip;exit");
    CliExecuteAppCmd ("interface vlan 4;no mpls ip;exit");
    CliExecuteAppCmd ("no interface vlan 2");
    CliExecuteAppCmd ("no interface vlan 3");
    CliExecuteAppCmd ("no interface vlan 4");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2;no ports;exit");
    CliExecuteAppCmd ("vlan 3;no ports;exit");
    CliExecuteAppCmd ("vlan 4;no ports;exit");
    CliExecuteAppCmd ("no vlan 2");
    CliExecuteAppCmd ("no vlan 3");
    CliExecuteAppCmd ("no vlan 4");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("int gi 0/1;shut;no map switch default;exit");
    CliExecuteAppCmd ("int gi 0/2;shut;no map switch default;exit");
    CliExecuteAppCmd ("int gi 0/3;shut;no map switch default;exit");
    CliExecuteAppCmd ("no int gi 0/1;no int gi 0/2;no int gi 0/3;");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
}

#endif /* __MPLS_TEST_C__ */
