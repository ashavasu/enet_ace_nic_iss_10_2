
/********************************************************************
 ** Copyright (C) 2008 Aricent Inc . All Rights Reserved
 **
 ** $Id: mptstcli.c,v 1.1 2010/10/19 15:23:24 prabuc Exp $
 **
 ** Description: MPLS API  UT Test cases cli file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/



#include "lr.h"
#include "cli.h"
#include "mptstcli.h"
#include "mplscli.h"

#define MAX_ARGS 5

extern VOID  MplsTestExecUT (INT4 i4UtId);
extern VOID  CallUTCases(VOID);

/*  Function is called from tptestcmd.def file */


INT4
cli_process_mplstp_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...)
{
    va_list            ap;
    UINT4             *args[MAX_ARGS];                                                               
    INT1               argno = 0;                                                                    
    INT4               i4TestcaseNo ;                                                                
    va_start (ap, u4Command);                                                                    
    UNUSED_PARAM (CliHandle);                                                                    
    /* Walk through the arguements and store in args array.
     *      * */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);

    switch (u4Command)

    {
        case CLI_MPLS_TP_UT:

            if (args[1] == NULL)
            {
                i4TestcaseNo = 0;
            }
            else
            {
                i4TestcaseNo = *args[1];
            }
            MplsUt (i4TestcaseNo);
            break;
    }
    return CLI_SUCCESS;
}
    
/* Function to call the test case functions in mplstest.c */

INT4
MplsUt(INT4 i4TestType)
{
    if(i4TestType == 0)
    {
        /* Calls all the test cases */
        CallUTCases();
        return CLI_SUCCESS;
    }
    else                                                                                             
    {   /* Calls the particular test case only */
        MplsTestExecUT(i4TestType);
        return CLI_SUCCESS;                                                                      
    }                                                                                            
}

