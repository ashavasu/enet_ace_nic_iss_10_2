
/********************************************************************
 ** Copyright (C) 2008 Aricent Inc . All Rights Reserved
 **
 ** $Id: p2mptest.c,v 1.3 2014/02/27 13:49:46 siva Exp $
 **
 ** Description: MPLS P2MP API  UT Test cases cli file.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "iss.h"
#include "mplscli.h"
#include "teincs.h"
#include "stdtelw.h"
#include "tpmlwg.h"
#include "mplslsr.h"
#include "stdlsrlw.h"
#include "fsmptelw.h"

#define MAX_ARGS 5

INT4                MplsP2mpUt (INT4 u4GlobalId);
VOID                CallP2mpUTCases (VOID);
INT4                MplsP2mpTestExecUT (INT4 i4UtId);
VOID                DeleteConf3 (VOID);
VOID                CreateConf3 (VOID);

INT4                Test_TeTestAllMplsTeP2mpTunnelDestTable (VOID);
INT4                Test_TeSetAllMplsTeP2mpTunnelDestTable (VOID);
INT4                Test_TeGetAllMplsTeP2mpTunnelDestTable (VOID);
INT4                Test_TeInitializeMplsTeP2mpTunnelDestTable (VOID);
INT4                Test_TeAddP2mpDestEntry (VOID);
INT4                Test_TeRemoveP2mpDestEntry (VOID);
INT4                Test_TeCheckP2mpTableEntry (VOID);
INT4
               Test_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (VOID);
INT4                Test_nmhTestv2MplsTunnelXCPointer (VOID);
INT4                Test_nmhTestv2MplsTunnelSignallingProto (VOID);
INT4                Test_nmhTestv2MplsTunnelHopTableIndex (VOID);
INT4                Test_nmhTestv2MplsTunnelPathInUse (VOID);
INT4                Test_nmhTestv2MplsTunnelRowStatus (VOID);
INT4                Test_nmhTestv2MplsTunnelType (VOID);
INT4                Test_MplsDbUpdateP2mpTunnelInHw (VOID);
INT4                Test_MplsCheckP2mpTnlRole (VOID);
INT4                Test_TeP2mpExtSetOperStatusAndProgHw (VOID);
INT4                Test_nmhSetMplsXCRowStatus (VOID);
INT4                Test_nmhTestv2MplsXCRowStatus (VOID);
INT4                Test_TeCliSetMplsTunnelRowStatus (VOID);
INT4                Test_TeSetCfgParams (VOID);
INT4                Test_nmhSetMplsTunnelAdminStatus (VOID);
INT4                Test_nmhSetMplsTunnelRowStatus (VOID);
INT4                Test_TeCliShowTnlDetail (VOID);
INT4                Test_TeCliP2mpTunnelCreate (VOID);
INT4                Test_TeCliP2mpAddDestinationAtIngress (VOID);
INT4                Test_TeCliP2mpStaticBind (VOID);
INT4                Test_TeCliP2mpDeleteDestination (VOID);
INT4                Test_TeCliShowP2mpBranchStatistics (VOID);

INT4                Test_TeTestAllMplsTeP2mpTunnelTable (VOID);
INT4                Test_TeSetAllMplsTeP2mpTunnelTable (VOID);
INT4                Test_TeGetAllMplsTeP2mpTunnelTable (VOID);
INT4                Test_TeInitializeMplsTeP2mpTunnelTable (VOID);
INT4                Test_TeAddP2mpBranchEntry (VOID);
INT4                Test_TeRemoveP2mpBranchEntry (VOID);
INT4                Test_TeCliP2mpStaticBindEgress (VOID);
INT4                Test_TeCliP2mpConfigureBudNode (VOID);
INT4                Test_MplsP2mpILMHwAdd (VOID);
INT4                Test_MplsP2mpILMHwDel (VOID);
INT4                Test_MplsGetOutSegmentPerfStats (VOID);
INT4                Test_TeGetFirstMplsTeP2mpTunnelTable (VOID);
INT4                Test_TeGetNextMplsTeP2mpTunnelTable (VOID);
INT4                Test_TeGetFirstMplsTeP2mpTunnelDestTable (VOID);
INT4                Test_TeGetNextMplsTeP2mpTunnelDestTable (VOID);
INT4                Test_TeGetFirstMplsTeP2mpTunnelBranchPerfTable (VOID);
INT4                Test_TeGetNextMplsTeP2mpTunnelBranchPerfTable (VOID);
INT4                Test_nmhValidateIndexInstanceMplsTeP2mpTunnelTable (VOID);
INT4
 
 
           Test_nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable (VOID);
INT4                Test_MplsCheckP2mpILMEntry (VOID);
INT4                Test_TeGetAllMplsTeP2mpTunnelBranchPerfTable (VOID);
INT4                Test_MplsCheckXCForP2mp (VOID);
INT4                Test_TeGetMplsTeP2mpTunnelNotificationEnable (VOID);
INT4                Test_TeSetMplsTeP2mpTunnelNotificationEnable (VOID);
INT4                Test_TeTestMplsTeP2mpTunnelNotificationEnable (VOID);
INT4                Test_TeGetMplsTeP2mpTunnelSubGroupIDNext (VOID);
INT4                Test_TeGetMplsTeP2mpTunnelConfigured (VOID);
INT4                Test_TeGetMplsTeP2mpTunnelActive (VOID);
INT4                Test_TeGetMplsTeP2mpTunnelTotalMaxHops (VOID);
INT4                Test_TeCliP2mpTunnelDelete (VOID);
INT4                Test_TeCliShowP2mpDestinations (VOID);
VOID                CreateConf2 (VOID);
VOID                DeleteConf2 (VOID);

VOID                CleanupEntries (VOID);
extern tOutSegment *MplsCheckP2mpILMEntry (UINT4 u4OutLabel);

/* Function is called from mpshwcds.def file */
INT4
cli_process_mpls_p2mp_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[MAX_ARGS];
    INT1                argno = 0;
    INT4                i4TestcaseNo = 0;
    va_start (ap, u4Command);
    UNUSED_PARAM (CliHandle);
    /* Walk through the arguements and store in args array */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);

    switch (u4Command)

    {
        case CLI_MPLS_P2MP_UT:

            if (args[1] == NULL)
            {
                i4TestcaseNo = 0;
            }
            else
            {
                i4TestcaseNo = *args[1];
            }
            MplsP2mpUt (i4TestcaseNo);
            break;
    }
    return CLI_SUCCESS;
}

INT4
MplsP2mpUt (INT4 i4TestType)
{
    if (i4TestType == 0)
    {
        CleanupEntries ();
        /* Calls all the test cases */
        CallP2mpUTCases ();
        return CLI_SUCCESS;
    }
    else
    {
        /* Calls the particular test case only */
        printf ("Test id is %d\r\n", i4TestType);
        MplsP2mpTestExecUT (i4TestType);
        return CLI_SUCCESS;
    }
}

VOID
CallP2mpUTCases (VOID)
{
    UINT1               u1Count = 1;
    UINT1               u1SuccessCount = 0;
    UINT4               u4RetVal = OSIX_FAILURE;
    /*max(60) u1Count needs to be modified if the stubs are increased */
    for (u1Count = 1; u1Count <= 60; u1Count++)
    {
        printf ("Test id is %d\r\n", u1Count);
        u4RetVal = MplsP2mpTestExecUT (u1Count);
        if (u4RetVal == OSIX_SUCCESS)
            u1SuccessCount++;
    }
    printf ("\r\nP2MP UT STUB RESULTS\r\n");
    printf ("\r\nTotal cases:%d\r\n", u1Count - 1);
    printf ("\r\nTotal Passed cases:%d\r\n", u1SuccessCount);
}

INT4
MplsP2mpTestExecUT (INT4 i4UtId)
{
    UINT4               u4RetVal = OSIX_FAILURE;
    switch (i4UtId)
    {
        case 1:
            CreateConf3 ();
            u4RetVal = Test_TeTestAllMplsTeP2mpTunnelDestTable ();
            DeleteConf3 ();
            break;

        case 2:
            CreateConf3 ();
            u4RetVal = Test_TeSetAllMplsTeP2mpTunnelDestTable ();
            DeleteConf3 ();
            break;

        case 3:
            CreateConf3 ();
            u4RetVal = Test_TeGetAllMplsTeP2mpTunnelDestTable ();
            DeleteConf3 ();
            break;

        case 4:
            CreateConf3 ();
            u4RetVal = Test_TeInitializeMplsTeP2mpTunnelDestTable ();
            DeleteConf3 ();
            break;

        case 5:
            u4RetVal = Test_TeAddP2mpDestEntry ();
            /*u4RetVal = OSIX_SUCCESS; */
            break;

        case 6:
            u4RetVal = Test_TeRemoveP2mpDestEntry ();
            /*u4RetVal = OSIX_SUCCESS; */
            break;

        case 7:
            CreateConf3 ();
            u4RetVal = Test_TeCheckP2mpTableEntry ();
            DeleteConf3 ();
            break;

        case 8:
            CreateConf3 ();
            u4RetVal =
                Test_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable ();
            DeleteConf3 ();
            break;

        case 9:
            CreateConf3 ();
            u4RetVal = Test_nmhTestv2MplsTunnelXCPointer ();
            DeleteConf3 ();
            break;

        case 10:
            CreateConf3 ();
            u4RetVal = Test_nmhTestv2MplsTunnelSignallingProto ();
            DeleteConf3 ();
            break;

        case 11:
            CreateConf3 ();
            u4RetVal = Test_nmhTestv2MplsTunnelHopTableIndex ();
            DeleteConf3 ();
            break;

        case 12:
            CreateConf3 ();
            u4RetVal = Test_nmhTestv2MplsTunnelPathInUse ();
            DeleteConf3 ();
            break;

        case 13:
            u4RetVal = Test_nmhTestv2MplsTunnelRowStatus ();
            /*u4RetVal = OSIX_SUCCESS; */
            break;

        case 14:
            CreateConf3 ();
            u4RetVal = Test_nmhTestv2MplsTunnelType ();
            DeleteConf3 ();
            break;

        case 15:
            CreateConf3 ();
            u4RetVal = Test_MplsDbUpdateP2mpTunnelInHw ();
            DeleteConf3 ();
            break;

        case 16:
            CreateConf3 ();
            u4RetVal = Test_MplsCheckP2mpTnlRole ();
            DeleteConf3 ();
            break;

        case 17:
            CreateConf3 ();
            u4RetVal = Test_TeP2mpExtSetOperStatusAndProgHw ();
            DeleteConf3 ();
            break;

        case 18:
            CreateConf3 ();
            u4RetVal = Test_nmhSetMplsXCRowStatus ();
            DeleteConf3 ();
            break;

        case 19:
            CleanupEntries ();
            u4RetVal = Test_nmhTestv2MplsXCRowStatus ();
            /*u4RetVal = OSIX_SUCCESS; */
            break;

        case 20:
            CreateConf3 ();
            u4RetVal = Test_TeCliSetMplsTunnelRowStatus ();
            DeleteConf3 ();
            break;

        case 21:
            u4RetVal = Test_TeSetCfgParams ();
            break;

        case 22:
            CleanupEntries ();
            u4RetVal = Test_nmhSetMplsTunnelAdminStatus ();
            /*u4RetVal = OSIX_SUCCESS; */
            break;

        case 23:
            u4RetVal = Test_nmhSetMplsTunnelRowStatus ();
            /*u4RetVal = OSIX_SUCCESS; */
            break;

        case 24:
            CreateConf3 ();
            u4RetVal = Test_TeCliShowTnlDetail ();
            DeleteConf3 ();
            break;

        case 25:
            u4RetVal = Test_TeCliP2mpTunnelCreate ();
            break;

        case 26:
            u4RetVal = Test_TeCliP2mpAddDestinationAtIngress ();
            break;

        case 27:
            u4RetVal = Test_TeCliP2mpStaticBind ();
            break;

        case 28:
            u4RetVal = Test_TeCliP2mpDeleteDestination ();
            break;

        case 29:
            u4RetVal = Test_TeCliShowP2mpBranchStatistics ();
            break;

        case 30:
            CreateConf3 ();
            u4RetVal = Test_TeTestAllMplsTeP2mpTunnelTable ();
            DeleteConf3 ();
            break;

        case 31:
            CreateConf3 ();
            u4RetVal = Test_TeSetAllMplsTeP2mpTunnelTable ();
            DeleteConf3 ();
            break;

        case 32:
            CreateConf3 ();
            u4RetVal = Test_TeGetAllMplsTeP2mpTunnelTable ();
            DeleteConf3 ();
            break;

        case 33:
            CreateConf3 ();
            u4RetVal = Test_TeInitializeMplsTeP2mpTunnelTable ();
            DeleteConf3 ();
            break;

        case 34:
            CreateConf3 ();
            u4RetVal = Test_TeAddP2mpBranchEntry ();
            DeleteConf3 ();
            break;

        case 35:
            CreateConf3 ();
            u4RetVal = Test_TeRemoveP2mpBranchEntry ();
            DeleteConf3 ();
            break;

        case 36:
            CreateConf3 ();
            u4RetVal = Test_TeCliP2mpStaticBindEgress ();
            DeleteConf3 ();
            break;

        case 37:
            CreateConf3 ();
            u4RetVal = Test_TeCliP2mpConfigureBudNode ();
            DeleteConf3 ();
            break;

        case 38:
            CreateConf3 ();
            u4RetVal = Test_MplsP2mpILMHwAdd ();
            DeleteConf3 ();
            break;

        case 39:
            CreateConf3 ();
            u4RetVal = Test_MplsP2mpILMHwDel ();
            DeleteConf3 ();
            break;

        case 40:
            CreateConf3 ();
            u4RetVal = Test_MplsGetOutSegmentPerfStats ();
            DeleteConf3 ();
            break;

        case 41:
            CreateConf3 ();
            u4RetVal = Test_TeGetFirstMplsTeP2mpTunnelTable ();
            DeleteConf3 ();
            break;

        case 42:
            CreateConf3 ();
            u4RetVal = Test_TeGetNextMplsTeP2mpTunnelTable ();
            DeleteConf3 ();
            break;

        case 43:
            CreateConf3 ();
            u4RetVal = Test_TeGetFirstMplsTeP2mpTunnelDestTable ();
            DeleteConf3 ();
            break;

        case 44:
            CreateConf3 ();
            u4RetVal = Test_TeGetNextMplsTeP2mpTunnelDestTable ();
            DeleteConf3 ();
            break;

        case 45:
            CreateConf3 ();
            u4RetVal = Test_TeGetFirstMplsTeP2mpTunnelBranchPerfTable ();
            DeleteConf3 ();
            break;

        case 46:
            CreateConf3 ();
            u4RetVal = Test_TeGetNextMplsTeP2mpTunnelBranchPerfTable ();
            DeleteConf3 ();
            break;

        case 47:
            CreateConf3 ();
            u4RetVal = Test_nmhValidateIndexInstanceMplsTeP2mpTunnelTable ();
            DeleteConf3 ();
            break;

        case 48:
            CreateConf3 ();
            u4RetVal =
                Test_nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable ();
            DeleteConf3 ();
            break;

        case 49:
            CreateConf3 ();
            u4RetVal = Test_MplsCheckP2mpILMEntry ();
            DeleteConf3 ();
            break;

        case 50:
            CreateConf3 ();
            u4RetVal = Test_TeGetAllMplsTeP2mpTunnelBranchPerfTable ();
            DeleteConf3 ();
            break;

        case 51:
            CreateConf3 ();
            u4RetVal = Test_MplsCheckXCForP2mp ();
            DeleteConf3 ();
            break;

        case 52:
            u4RetVal = Test_TeGetMplsTeP2mpTunnelNotificationEnable ();
            break;

        case 53:
            u4RetVal = Test_TeSetMplsTeP2mpTunnelNotificationEnable ();
            break;

        case 54:
            u4RetVal = Test_TeTestMplsTeP2mpTunnelNotificationEnable ();
            break;

        case 55:
            u4RetVal = Test_TeGetMplsTeP2mpTunnelSubGroupIDNext ();
            break;

        case 56:
            u4RetVal = Test_TeGetMplsTeP2mpTunnelConfigured ();
            break;

        case 57:
            u4RetVal = Test_TeGetMplsTeP2mpTunnelActive ();
            break;

        case 58:
            u4RetVal = Test_TeGetMplsTeP2mpTunnelTotalMaxHops ();
            break;

        case 59:
            u4RetVal = Test_TeCliP2mpTunnelDelete ();
            break;

        case 60:
            u4RetVal = Test_TeCliShowP2mpDestinations ();
            break;

        default:
            printf ("%% No test case is defined for ID: %d\r\n", i4UtId);
            break;
    }

    /* Display the result */
    if (u4RetVal == OSIX_FAILURE)
    {
        printf ("\r\nUNIT TEST ID: %-2d %-2s\r\n", i4UtId, "FAILED");
        return OSIX_FAILURE;
    }
    else
    {
        printf ("\r\nUNIT TEST ID: %-2d %-2s\r\n", i4UtId, "PASSED");
        return OSIX_SUCCESS;
    }
}

INT4
Test_TeTestAllMplsTeP2mpTunnelDestTable ()
{
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;
    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;

    static UINT1        au1P2mpTnlDestination[MPLS_INDEX_LENGTH];
    static UINT1        au1SrcSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    UINT4               u4ErrorCode = 0;

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlDestination.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    P2mpTnlSrcSubGroupOrigin.pu1_OctetList = au1SrcSubGroupOrigin;
    P2mpTnlSrcSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1SrcSubGroupOrigin, 0, MPLS_INDEX_LENGTH);
    P2mpTnlDestSubGroupOrigin.pu1_OctetList = au1DestSubGroupOrigin;
    P2mpTnlDestSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1DestSubGroupOrigin, 0, MPLS_INDEX_LENGTH);

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: ENTRY\r\n");

    MPLS_INTEGER_TO_OCTETSTRING (0x0, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&XCIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1E000001, (&P2mpTnlDestination));

    /* CASE 1: TE GBL ADMIN DOWN */
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    if (nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                MPLS_STATUS_CREATE_AND_WAIT) ==
        SNMP_SUCCESS)
    {
        gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-1: TE GBL ADMIN DOWN - Failed");
        return OSIX_FAILURE;
    }
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-1: TE GBL ADMIN DOWN - Wrong error code - Failed");
        return OSIX_FAILURE;
    }

    /* CASE-2: Add destination without creating the tunnel entry */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                MPLS_STATUS_CREATE_AND_WAIT) ==
        SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-2: Add destination without creating the tunnel "
                "entry - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-2: Add destination without creating the tunnel "
                "entry - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-3: Make row status as Active without create and wait */
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6,
                                    TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-3: Make destination active without create and wait - "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-3: Make destination active without create and wait - "
                "Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-3: Make destination active without create and wait - "
                "Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                MPLS_STATUS_ACTIVE) ==
        SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-3: Make destination active without create and wait - "
                "Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-3: Make destination active without create and wait - "
                "Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-4: Associate out-index without creating destination row */
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
    if (nmhTestv2MplsTeP2mpTunnelDestBranchOutSegment
        (&u4ErrorCode, 10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, &OutSegmentIndex) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-4: Able to associate out-index to non-existent "
                "destination row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-4: Associate out-index without creating destination row "
                "Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-5: Associate hop-table index without creating destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestHopTableIndex (&u4ErrorCode, 10, 1, 5, 6,
                                                    LSR_ADDR_IPV4,
                                                    &P2mpTnlSrcSubGroupOrigin,
                                                    0, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestSubGroupOrigin,
                                                    1, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestination,
                                                    1) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-5: Able to associate hop-table index to non-existent "
                "destination row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-5: Associate hop-table index without creating destination "
                "row. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-6: Associate dest path in use without creating destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestPathInUse (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                1) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-6: Able to associate dest path in use to non-existent "
                "destination row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-6: Associate dest path in use without creating destination "
                "row. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-7: Make admin up without creating destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestAdminStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlSrcSubGroupOrigin, 0,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlDestSubGroupOrigin, 1,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlDestination,
                                                  TE_ADMIN_UP) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-7: Able to admin up a non-existent " "destination row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-7: Make admin up without creating destination "
                "row. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-8: Assign storage type without creating destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestStorageType (&u4ErrorCode, 10, 1, 5, 6,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlSrcSubGroupOrigin, 0,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlDestSubGroupOrigin, 1,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlDestination,
                                                  TE_STORAGE_VOLATILE) ==
        SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-8: Able to assign storage type to a non-existent "
                "destination row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-8: Assign storage type without creating destination "
                "row. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-9: Activate destination row status without associating out-index */
    if (nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                MPLS_STATUS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-9: Activate dest row without associating out-index - "
                "Failed to set status as create and wait");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-9: Activate dest row without associating out-index - "
                "Failed to set status as create and wait");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                MPLS_STATUS_ACTIVE) ==
        SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-9: Activate dest row without associating out-index - "
                "Row status active without valid out-segment index - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "Activate dest row without associating out-index - "
                "Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-10: Assign invalid out-segment to the destination row */
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x32, (&OutSegmentIndex));
    if (nmhTestv2MplsTeP2mpTunnelDestBranchOutSegment
        (&u4ErrorCode, 10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, &OutSegmentIndex) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-10: Able to assign invalid out-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-10: Assign invalid out-segment to destination "
                "row. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-11: Assign invalid hop-table index to the destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestHopTableIndex (&u4ErrorCode, 10, 1, 5, 6,
                                                    LSR_ADDR_IPV4,
                                                    &P2mpTnlSrcSubGroupOrigin,
                                                    0, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestSubGroupOrigin,
                                                    1, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestination,
                                                    0) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-11: Able to assign invalid hop-table index - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-11: Assign invalid hop-table index to destination "
                "row. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-12: Assign invalid dest path in use to the destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestPathInUse (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                75000) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-12: Able to assign invalid dest path - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-12: Assign invalid dest path to destination "
                "row. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-13: Make admin up when destination row is inactive */
    if (nmhTestv2MplsTeP2mpTunnelDestAdminStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlSrcSubGroupOrigin, 0,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlDestSubGroupOrigin, 1,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlDestination,
                                                  TE_ADMIN_UP) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-13: Able to admin up when destination row is "
                "inactive - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-13: Make admin up when destination row is "
                "inactive. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-14: Re-create already existing destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                MPLS_STATUS_CREATE_AND_WAIT) ==
        SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-14: Able to re-create exisiting destination row. "
                "Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-14: Able to re-create exisiting destination row. "
                "Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-15: Activate destination row without associating out-index */
    if (nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                MPLS_STATUS_ACTIVE) ==
        SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-15: Able to activate destination row without "
                "associating out-index - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-15: Able to activate destination row without "
                "associating out-index. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-16: Assign invalid storage type to the destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestStorageType (&u4ErrorCode, 10, 1, 5, 6,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlSrcSubGroupOrigin, 0,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlDestSubGroupOrigin, 1,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlDestination,
                                                  TE_STORAGE_READONLY) ==
        SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-16: Able to assign invalid storage type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-16: Assign invalid storage type to destination "
                "row. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-17: Assign a valid out-segment to the destination row */
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
    if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-17: Unable to create XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-17: Unable to create XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhTestv2MplsTeP2mpTunnelDestBranchOutSegment
        (&u4ErrorCode, 10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, &OutSegmentIndex) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-17: Unable to assign a valid out-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestBranchOutSegment (10, 1, 5, 6,
                                                    LSR_ADDR_IPV4,
                                                    &P2mpTnlSrcSubGroupOrigin,
                                                    0, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestSubGroupOrigin,
                                                    1, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestination,
                                                    &OutSegmentIndex) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-17: Unable to assign a valid out-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-18: Activate destination row */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             MPLS_STATUS_ACTIVE) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-18: Unable to activate destination row without - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-19: Assign valid hop-table index to the active destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestHopTableIndex (&u4ErrorCode, 10, 1, 5, 6,
                                                    LSR_ADDR_IPV4,
                                                    &P2mpTnlSrcSubGroupOrigin,
                                                    0, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestSubGroupOrigin,
                                                    1, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestination,
                                                    1) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-19: Able to assign hop-table index when dest row "
                "status is active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-19: Able to assign hop-table index when dest row "
                "status is active. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-20: Assign valid dest path in use to the active destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestPathInUse (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                1) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-20: Able to assign dest path in use when dest row "
                "status is active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-20: Able to assign dest path in use when dest row "
                "status is active. Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-21: Make admin up the active destination row */
    if (nmhTestv2MplsTeP2mpTunnelDestAdminStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlSrcSubGroupOrigin, 0,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlDestSubGroupOrigin, 1,
                                                  LSR_ADDR_IPV4,
                                                  &P2mpTnlDestination,
                                                  TE_ADMIN_UP) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-21: Unable to admin up when destination row is "
                "active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-22: Add destinations more than that supported per tunnel */
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x28000001, (&P2mpTnlDestination));
    if (nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlSrcSubGroupOrigin, 0,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestSubGroupOrigin, 1,
                                                LSR_ADDR_IPV4,
                                                &P2mpTnlDestination,
                                                MPLS_STATUS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-22: Add dest rows more than max limit - "
                "Failed to set second dest row status as create and wait");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
                "CASE-22: Add dest rows more than max limit - "
                "Failed to set second dest row status as create and wait");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    /* Add third destination */
    /* Make MAX_DEST_PER_P2MP_LSP to 2 */
    /*Uncomment the below code only if MAX_DEST_PER_P2MP_LSP is 2 */
    /*MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
       MPLS_INTEGER_TO_OCTETSTRING (0x32000001, (&P2mpTnlDestination));    
       if (nmhTestv2MplsTeP2mpTunnelDestRowStatus(&u4ErrorCode, 10, 1, 5, 6,
       LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 
       0,LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4, &P2mpTnlDestination, MPLS_STATUS_CREATE_AND_WAIT) == SNMP_SUCCESS)
       {
       printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
       "CASE-22: Add dest rows more than max limit - "
       "Able to set third dest row status as create and wait - Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;interface mplstunnel 10");
       CliExecuteAppCmd ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
       CliExecuteAppCmd ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
       CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex, LSR_DESTROY);
       return OSIX_FAILURE;
       }
       if (u4ErrorCode != SNMP_ERR_NO_CREATION)
       {
       printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: "
       "CASE-22: Add dest rows more than max limit - "
       "Failed to set second dest row status as create and wait");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;interface mplstunnel 10");
       CliExecuteAppCmd ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
       CliExecuteAppCmd ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
       CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex, LSR_DESTROY);
       return OSIX_FAILURE;
       } */

    /* All test cases successfully executed */
    printf
        ("\r\nTest_TeTestAllMplsTeP2mpTunnelDestTable: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
    CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                           LSR_DESTROY);
    return OSIX_SUCCESS;
}

INT4
Test_TeSetAllMplsTeP2mpTunnelDestTable ()
{
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;
    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;

    static UINT1        au1P2mpTnlDestination[MPLS_INDEX_LENGTH];
    static UINT1        au1SrcSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    UINT4               u4ErrorCode = 0;

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlDestination.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    P2mpTnlSrcSubGroupOrigin.pu1_OctetList = au1SrcSubGroupOrigin;
    P2mpTnlSrcSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1SrcSubGroupOrigin, 0, MPLS_INDEX_LENGTH);
    P2mpTnlDestSubGroupOrigin.pu1_OctetList = au1DestSubGroupOrigin;
    P2mpTnlDestSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1DestSubGroupOrigin, 0, MPLS_INDEX_LENGTH);

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: ENTRY");

    MPLS_INTEGER_TO_OCTETSTRING (0x0, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&XCIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1E000001, (&P2mpTnlDestination));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6,
                                    TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-23: Create a row in destination table */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-23: Failed to set dest row status as create and wait");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-24: Associate out-segment to destination entry */
    if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-24: Test XC row creation - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-24: Unable to create XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestBranchOutSegment
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, &OutSegmentIndex) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-24: Unable to assign a valid out-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-25: Assign valid hop-table index to the destination row */
    if (nmhSetMplsTeP2mpTunnelDestHopTableIndex
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, 1) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-25: Unable to assign hop-table index - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-26: Assign valid dest path in use to the destination row */
    if (nmhSetMplsTeP2mpTunnelDestPathInUse
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, 1) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-26: Unable to assign dest path in use - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-27: Assign valid storage type to the destination row */
    if (nmhSetMplsTeP2mpTunnelDestStorageType
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, TE_STORAGE_VOLATILE) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-27: Unable to assign valid storage type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-28: Make the destination row as active */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-28: Unable to set dest row status as active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-29: Make the admin status as active */
    if (nmhSetMplsTeP2mpTunnelDestAdminStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, TE_ADMIN_UP) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-27: Unable to set admin status as active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* Activate the tunnel */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-30: Create a second destination entry */
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x28000001, (&P2mpTnlDestination));
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-30: Failed to set second dest row status as create and wait");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-31: Associate previous out-segment to second destination entry */
    if (nmhSetMplsTeP2mpTunnelDestBranchOutSegment
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, &OutSegmentIndex) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-31: Unable to assign a valid out-segment to second "
                "destination entry - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-32: Make the second destination row as active */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-32: Unable to set second dest row status as active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-33: Make the admin status of second destination as active */
    if (nmhSetMplsTeP2mpTunnelDestAdminStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, TE_ADMIN_UP) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-33: Unable to set admin status as active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-34: Make the second destination row as not in service */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_NOT_INSERVICE) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-34: Unable to set second dest row status as not in "
                "service - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-35: Create a third destination entry */
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x32000001, (&P2mpTnlDestination));
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-35: Failed to set third dest row status as create and wait");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-36: Associate new out-segment to third destination entry */
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
    if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-36: Test XC row creation for third dest - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 50.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-36: Unable to create XC row for third dest entry");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 50.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestBranchOutSegment
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, &OutSegmentIndex) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-36: Unable to assign new out-segment to third "
                "destination entry - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 50.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-37: Make the third destination row as active */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-37: Unable to set third dest row status as active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 50.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-38: Make the third destination as admin up */
    if (nmhSetMplsTeP2mpTunnelDestAdminStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, TE_ADMIN_UP) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-38: Unable to set admin status of third destination "
                "as active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 50.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-39: Make the third destination as admin down */
    if (nmhSetMplsTeP2mpTunnelDestAdminStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, TE_ADMIN_DOWN) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-39: Unable to set admin down the third destination "
                "as active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 50.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-40: Make the third destination row as not in service */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_NOT_INSERVICE) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-40: Unable to set third dest row status as not in "
                "service - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 50.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-41: Destroy third destination row. Branch entry should be deleted */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-41: Unable to destroy third dest row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 50.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-42: Destroy second destination row */
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x28000001, (&P2mpTnlDestination));
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-42: Unable to destroy second dest row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-43: Destroy first destination row. Branch entry should be deleted */
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x1E000001, (&P2mpTnlDestination));
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: "
                "CASE-43: Unable to destroy first dest row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf
        ("\r\nTest_TeSetAllMplsTeP2mpTunnelDestTable: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
    nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                           LSR_DESTROY);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
    nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                           LSR_DESTROY);
    return OSIX_SUCCESS;
}

INT4
Test_TeGetAllMplsTeP2mpTunnelDestTable ()
{
    INT4                i4RowStatus = 0;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;
    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;

    static UINT1        au1P2mpTnlDestination[MPLS_INDEX_LENGTH];
    static UINT1        au1SrcSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlDestination.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    P2mpTnlSrcSubGroupOrigin.pu1_OctetList = au1SrcSubGroupOrigin;
    P2mpTnlSrcSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1SrcSubGroupOrigin, 0, MPLS_INDEX_LENGTH);
    P2mpTnlDestSubGroupOrigin.pu1_OctetList = au1DestSubGroupOrigin;
    P2mpTnlDestSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1DestSubGroupOrigin, 0, MPLS_INDEX_LENGTH);

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelDestTable: ENTRY");

    /* CASE-44: Get destination entry without creating the tunnel entry */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if (nmhGetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, &i4RowStatus) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelDestTable: "
                "CASE-44: Able to get destination entry without creating the tunnel "
                "- Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-45: Fetch destination entry without creating the destination entry */
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6,
                                    TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelDestTable: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelDestTable: "
                "Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelDestTable: "
                "Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if (nmhGetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, &i4RowStatus) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelDestTable: "
                "CASE-45: Able to get destination entry without entry being "
                "present - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-46: Fetch the destination entry */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelDestTable: "
                "CASE-46: Destination entry creation failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhGetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, &i4RowStatus) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelDestTable: "
                "CASE-46: Unable to get destination entry - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, 6) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelDestTable: "
                "CASE-46: Destination entry creation failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf
        ("\r\nTest_TeGetAllMplsTeP2mpTunnelDestTable: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_TeInitializeMplsTeP2mpTunnelDestTable ()
{
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    tP2mpDestEntry      P2mpTunnelDestTable;

    static UINT1        au1P2mpTnlDestination[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    MPLS_INTEGER_TO_OCTETSTRING (0x1E000001, (&P2mpTnlDestination));

    printf ("\r\nTest_TeInitializeMplsTeP2mpTunnelDestTable: ENTRY");

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6,
                                    TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeInitializeMplsTeP2mpTunnelDestTable: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeInitializeMplsTeP2mpTunnelDestTable: "
                "Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeInitializeMplsTeP2mpTunnelDestTable: "
                "Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-47: Pass NULL pointer as argument to initialize dest entry */
    if ((TeInitializeMplsTeP2mpTunnelDestTable (NULL)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeInitializeMplsTeP2mpTunnelDestTable: "
                "CASE-47: Able to initialize a NULL pointer - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-48:  Pass pointer as argument to initialize dest entry */
    if ((TeInitializeMplsTeP2mpTunnelDestTable (&P2mpTunnelDestTable)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeInitializeMplsTeP2mpTunnelDestTable: "
                "CASE-48: Able to initialize a NULL pointer - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf
        ("\r\nTest_TeInitializeMplsTeP2mpTunnelDestTable: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_TeAddP2mpDestEntry ()
{
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;
    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;

    static UINT1        au1P2mpTnlDestination[MPLS_INDEX_LENGTH];
    static UINT1        au1SrcSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    tTeTnlInfo         *pTeTnlInfo = NULL;

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlDestination.i4_Length = MPLS_INDEX_LENGTH;
    P2mpTnlSrcSubGroupOrigin.pu1_OctetList = au1SrcSubGroupOrigin;
    P2mpTnlSrcSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    P2mpTnlDestSubGroupOrigin.pu1_OctetList = au1DestSubGroupOrigin;
    P2mpTnlDestSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    MPLS_INTEGER_TO_OCTETSTRING (0x1E000001, (&P2mpTnlDestination));

    printf ("\r\nTest_TeAddP2mpDestEntry: ENTRY");

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6,
                                    TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeAddP2mpDestEntry: Unable to create tunnel row "
                "- Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeAddP2mpDestEntry: Unable to set tunnel type "
                "- Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((TeCheckP2mpTableEntry (10, 1, 5, 6, &pTeTnlInfo)) != TE_SUCCESS)
    {
        printf ("\r\nTest_TeAddP2mpDestEntry: "
                "Unable to fetch tunnel entry - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-51: Create destination row without creating P2MP tunnel row */
    if ((TeAddP2mpDestEntry (pTeTnlInfo, 0x1E000001)) != NULL)
    {
        printf ("\r\nTest_TeAddP2mpDestEntry: "
                "CASE-51: Able to create destination row when P2MP tunnel "
                "row is not created - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-52: Create destination row when dest list SLL is NULL.
     * Removed after using static SLL */

    /* CASE-53: Create destination row */
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeAddP2mpDestEntry: "
                "CASE-53: Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             MPLS_STATUS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeAddP2mpDestEntry: "
                "CASE-53: Unable to create destination row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_TeAddP2mpDestEntry: All test cases passed");
    nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                         &P2mpTnlSrcSubGroupOrigin, 0,
                                         LSR_ADDR_IPV4,
                                         &P2mpTnlDestSubGroupOrigin, 1,
                                         LSR_ADDR_IPV4, &P2mpTnlDestination,
                                         MPLS_STATUS_DESTROY);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_TeRemoveP2mpDestEntry ()
{
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;
    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;

    static UINT1        au1P2mpTnlDestination[MPLS_INDEX_LENGTH];
    static UINT1        au1SrcSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    tTeTnlInfo         *pTeTnlInfo = NULL;

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlDestination.i4_Length = MPLS_INDEX_LENGTH;
    P2mpTnlSrcSubGroupOrigin.pu1_OctetList = au1SrcSubGroupOrigin;
    P2mpTnlSrcSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    P2mpTnlDestSubGroupOrigin.pu1_OctetList = au1DestSubGroupOrigin;
    P2mpTnlDestSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    MPLS_INTEGER_TO_OCTETSTRING (0x1E000001, (&P2mpTnlDestination));

    printf ("\r\nTest_TeRemoveP2mpDestEntry: ENTRY");

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6,
                                    TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: Unable to create tunnel row "
                "- Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: Unable to set tunnel type "
                "- Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((TeCheckP2mpTableEntry (10, 1, 5, 6, &pTeTnlInfo)) != TE_SUCCESS)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: "
                "Unable to fetch tunnel entry - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-58: Delete destination row without creating P2MP tunnel row */
    if ((TeRemoveP2mpDestEntry (pTeTnlInfo, 0x1E000001)) == TE_SUCCESS)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: "
                "CASE-58: Able to delete destination row when P2MP tunnel "
                "row is not created - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-60: Delete the destination row without creating it */
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: "
                "CASE-59: Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((TeRemoveP2mpDestEntry (pTeTnlInfo, 0x1E000001)) == TE_SUCCESS)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: "
                "CASE-60: Able to delete destination row when destination "
                "row is not created - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-61: Delete the destination row */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             MPLS_STATUS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: "
                "CASE-61: Unable to create row for 30.0.0.1 - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    printf ("\r\nDest 30.0.0.1 added. Number of dest in SLL = %d",
            TMO_SLL_Count (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))));

    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x28000001, (&P2mpTnlDestination));
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             MPLS_STATUS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: "
                "CASE-61: Unable to create row for 40.0.0.1 - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    printf ("\r\nDest 40.0.0.1 added. Number of dest in SLL = %d",
            TMO_SLL_Count (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))));

    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x32000001, (&P2mpTnlDestination));
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             MPLS_STATUS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: "
                "CASE-61: Unable to create row for 50.0.0.1 - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    printf ("\r\nDest 50.0.0.1 added. Number of dest in SLL = %d",
            TMO_SLL_Count (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))));

    /* Delete the destination rows */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             MPLS_STATUS_DESTROY) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: "
                "CASE-61: Unable to delete row for 50.0.0.1 - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 50.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    printf ("\r\nDest 50.0.0.1 removed. Number of dest in SLL = %d",
            TMO_SLL_Count (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))));

    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x28000001, (&P2mpTnlDestination));
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             MPLS_STATUS_DESTROY) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: "
                "CASE-61: Unable to delete row for 40.0.0.1 - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    printf ("\r\nDest 40.0.0.1 removed. Number of dest in SLL = %d",
            TMO_SLL_Count (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))));

    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x1E000001, (&P2mpTnlDestination));
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             MPLS_STATUS_DESTROY) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeRemoveP2mpDestEntry: "
                "CASE-61: Unable to delete row for 30.0.0.1 - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    printf ("\r\nDest 30.0.0.1 removed. Number of dest in SLL = %d",
            TMO_SLL_Count (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))));

    /* All test cases successfully executed */
    printf ("\r\nTest_TeRemoveP2mpDestEntry: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_TeCheckP2mpTableEntry ()
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    printf ("\r\nTest_TeCheckP2mpTableEntry: ENTRY");

    /* CASE-62: Create a row in P2MP tunnel table without creating 
     * row in MPLS tunnel table */
    if ((nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                             MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeCheckP2mpTableEntry: "
                "CASE-62: Test routine to create a row in P2MP tunnel table "
                "without row in tunnel table passed - Test case failed");
        return OSIX_FAILURE;
    }

    /* CASE-63: Create a row in P2MP tunnel table when tunnel type is not P2MP */
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6,
                                    TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeCheckP2mpTableEntry: "
                "CASE-63: Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                             MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_SUCCESS)
    {
        printf ("\r\nTest_TeCheckP2mpTableEntry: "
                "CASE-63: Test routine to create a row in P2MP tunnel table "
                "for non-P2MP tunnel type passed - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-64: Create a row in P2MP tunnel table when tunnel type is P2MP */
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeCheckP2mpTableEntry: "
                "CASE-64: Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                             MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeCheckP2mpTableEntry: "
                "CASE-63: Test routine to create a row in P2MP tunnel table "
                "for P2MP tunnel type failed - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_TeCheckP2mpTableEntry: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable ()
{
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;
    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGroupOrigin;
    static UINT1        au1P2mpTnlDestination[MPLS_INDEX_LENGTH];
    static UINT1        au1P2mpTnlSrcSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1P2mpTnlDestSubGroupOrigin[MPLS_INDEX_LENGTH];
    UINT4               u4Index = 0;

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlSrcSubGroupOrigin.pu1_OctetList = au1P2mpTnlSrcSubGroupOrigin;
    P2mpTnlDestSubGroupOrigin.pu1_OctetList = au1P2mpTnlDestSubGroupOrigin;

    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1P2mpTnlSrcSubGroupOrigin, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1P2mpTnlDestSubGroupOrigin, 0, MPLS_INDEX_LENGTH);

    printf
        ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: ENTRY");

    /* CASE-65: Validate destination row creation without creating tunnel row */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (10, 1, 5, 0,
                                                           LSR_ADDR_IPV4, 0, 0,
                                                           LSR_ADDR_IPV4, 0, 1,
                                                           LSR_ADDR_IPV4,
                                                           &P2mpTnlDestination)
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
                "CASE-64: Creating row in P2MP destination table "
                "without presence of tunnel row passed - Test case failed");
        return OSIX_FAILURE;
    }

    /* CASE-66: Validate destination source sub group origin type is of 
     * IPv4 address type */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (10, 1, 5, 6,
                                                           LSR_ADDR_IPV6, 0, 0,
                                                           LSR_ADDR_IPV4, 0, 1,
                                                           LSR_ADDR_IPV4,
                                                           &P2mpTnlDestination)
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
                "CASE-66: DestSrcSubGroupOriginType = IPv6. Validation passed "
                "- Test case failed");
        return OSIX_FAILURE;
    }
    u4Index = 0x1;
    MPLS_INTEGER_TO_OCTETSTRING (u4Index, (&P2mpTnlSrcSubGroupOrigin));

    /* CASE-67: Validate destination source sub group origin has 0.0.0.0 address */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (10, 1, 5, 6,
                                                           LSR_ADDR_IPV4,
                                                           &P2mpTnlSrcSubGroupOrigin,
                                                           0, LSR_ADDR_IPV4, 0,
                                                           1, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestination)
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
                "CASE-67: DestSrcSubGroupOrigin does not have 0.0.0.0. "
                "Validation passed - Test case failed");
        return OSIX_FAILURE;
    }
    u4Index = 0x0;
    MPLS_INTEGER_TO_OCTETSTRING (u4Index, (&P2mpTnlSrcSubGroupOrigin));

    /* CASE-68: Validate destination source sub group id is zero */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (10, 1, 5, 6,
                                                           LSR_ADDR_IPV4,
                                                           &P2mpTnlSrcSubGroupOrigin,
                                                           1, LSR_ADDR_IPV4, 0,
                                                           1, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestination)
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
                "CASE-68: DestSrcSubGroupId is non-zero. Validation passed "
                "- Test case failed");
        return OSIX_FAILURE;
    }

    /* CASE-69: Validate destination sub group origin type is of IPv6 address 
     * type */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (10, 1, 5, 6,
                                                           LSR_ADDR_IPV4,
                                                           &P2mpTnlSrcSubGroupOrigin,
                                                           0, LSR_ADDR_IPV6, 0,
                                                           1, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestination)
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
                "CASE-69: DestSubGroupOriginType = IPv6. Validation passed "
                "- Test case failed");
        return OSIX_FAILURE;
    }
    u4Index = 0x1;
    MPLS_INTEGER_TO_OCTETSTRING (u4Index, (&P2mpTnlDestSubGroupOrigin));

    /* CASE-70: Validate destination sub group origin is zero */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (10, 1, 5, 6,
                                                           LSR_ADDR_IPV4,
                                                           &P2mpTnlSrcSubGroupOrigin,
                                                           0, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestSubGroupOrigin,
                                                           1, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestination)
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
                "CASE-70: DestSubGroupOrigin does not have 0.0.0.0. "
                "Validation passed - Test case failed");
        return OSIX_FAILURE;
    }
    u4Index = 0x0;
    MPLS_INTEGER_TO_OCTETSTRING (u4Index, (&P2mpTnlDestSubGroupOrigin));

    /* CASE-71: Validate destination sub group id is zero */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (10, 1, 5, 6,
                                                           LSR_ADDR_IPV4,
                                                           &P2mpTnlSrcSubGroupOrigin,
                                                           0, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestSubGroupOrigin,
                                                           0, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestination)
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
                "CASE-71: DestSubGroupId != 1. Validation passed "
                "- Test case failed");
        return OSIX_FAILURE;
    }

    /* CASE-72: Validate if P2MP destination is of IPv6 address type */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (10, 1, 5, 6,
                                                           LSR_ADDR_IPV4,
                                                           &P2mpTnlSrcSubGroupOrigin,
                                                           0, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestSubGroupOrigin,
                                                           1, LSR_ADDR_IPV6,
                                                           &P2mpTnlDestination)
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
                "CASE-72: DestinationType = IPv6. Validation passed - "
                "Test case failed");
        return OSIX_FAILURE;
    }

    /* CASE-73: Validate if P2MP destination is unicast address */
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    u4Index = 0xF0000001;
    MPLS_INTEGER_TO_OCTETSTRING (u4Index, (&P2mpTnlDestination));
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (10, 1, 5, 6,
                                                           LSR_ADDR_IPV4,
                                                           &P2mpTnlSrcSubGroupOrigin,
                                                           0, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestSubGroupOrigin,
                                                           1, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestination)
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
                "CASE-73: Destination IP = Non-unicast address. Validation passed - "
                "Test case failed");
        return OSIX_FAILURE;
    }
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    u4Index = 0x1E000001;
    MPLS_INTEGER_TO_OCTETSTRING (u4Index, (&P2mpTnlDestination));

    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (10, 1, 5, 6,
                                                           LSR_ADDR_IPV4,
                                                           &P2mpTnlSrcSubGroupOrigin,
                                                           0, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestSubGroupOrigin,
                                                           1, LSR_ADDR_IPV4,
                                                           &P2mpTnlDestination)
        != SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
                "Test case failed for success case");
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable: "
            "All test cases passed");
    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2MplsTunnelXCPointer ()
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    tSNMP_OID_TYPE      TunnelXCPointer;

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };
    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };

    UINT4               u4ErrorCode = 0;
    UINT4               u4XCIndex = 1;
    UINT4               u4InIndex = 1;
    UINT4               u4OutIndex = 1;

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: ENTRY");

    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&XCIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6,
                                    TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: "
                "Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: "
                "Unable to create XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 11] = u4XCIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 6] = u4InIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 1] = u4OutIndex;
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;

    /* CASE-74: Add XC pointer at ingress or branch LSR of P2MP tunnel */
    if ((nmhTestv2MplsTunnelXCPointer (&u4ErrorCode, 10, 1, 5, 6,
                                       &TunnelXCPointer)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: "
                "CASE-74: Test routine for XC pointer addition passed "
                "at ingress/transit of P2MP tunnel - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: "
                "CASE-74: Test routine for XC pointer addition failed at "
                "ingress/transit of P2MP tunnel - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-75: Add XC pointer at egress LSR of P2MP tunnel */
    if ((nmhSetMplsTunnelRole (10, 1, 5, 6, TE_EGRESS)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: "
                "CASE-75: Unable to set tunnel role as egress - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhTestv2MplsTunnelXCPointer (&u4ErrorCode, 10, 1, 5, 6,
                                       &TunnelXCPointer)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: "
                "CASE-75: Test routine for XC pointer addition failed at "
                "egress of P2MP tunnel - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                           LSR_DESTROY);
    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2MplsTunnelSignallingProto ()
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    printf ("\r\nTest_nmhTestv2MplsTunnelSignallingProto: ENTRY");

    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6,
                                    TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelSignallingProto: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelSignallingProto: "
                "Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-76: For a P2MP tunnel, set signaling protocol as RSVP */
    if ((nmhTestv2MplsTunnelSignallingProto (&u4ErrorCode, 10, 1, 5, 6,
                                             TE_SIGPROTO_RSVP)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: "
                "CASE-76: Test routine passed for RSVP as signaling protocol "
                "for P2MP tunnel - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-77: For a P2MP tunnel, set signaling protocol to NONE */
    if ((nmhTestv2MplsTunnelSignallingProto (&u4ErrorCode, 10, 1, 5, 6,
                                             TE_SIGPROTO_NONE)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: "
                "CASE-77: Test routine failed for no signaling protocol "
                "for P2MP tunnel - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhTestv2MplsTunnelXCPointer: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2MplsTunnelHopTableIndex ()
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    printf ("\r\nTest_nmhTestv2MplsTunnelHopTableIndex: ENTRY");

    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_CREATEANDWAIT))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelHopTableIndex: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-78: For non-P2MP tunnel, set valid hop table index */
    if ((nmhTestv2MplsTunnelHopTableIndex (&u4ErrorCode, 10, 1, 5, 6, 1))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelHopTableIndex: "
                "CASE-78: Test routine failed for valid hop table index "
                "for non-P2MP tunnel - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-79: For P2MP tunnel, set valid hop table index */
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelHopTableIndex: "
                "CASE-79: Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhTestv2MplsTunnelHopTableIndex (&u4ErrorCode, 10, 1, 5, 6, 1))
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelHopTableIndex: "
                "CASE-79: Test routine passed for valid hop table index "
                "for P2MP tunnel - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhTestv2MplsTunnelHopTableIndex: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2MplsTunnelPathInUse ()
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    printf ("\r\nTest_nmhTestv2MplsTunnelPathInUse: ENTRY");

    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_CREATEANDWAIT))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelPathInUse: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-80: For non-P2MP tunnel, set valid tunnel path */
    if ((nmhTestv2MplsTunnelPathInUse (&u4ErrorCode, 10, 1, 5, 6, 1))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelPathInUse: "
                "CASE-80: Test routine failed for valid path for non-P2MP "
                "tunnel - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-81: For P2MP tunnel, set valid tunnel path */
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelPathInUse: "
                "CASE-81: Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhTestv2MplsTunnelPathInUse (&u4ErrorCode, 10, 1, 5, 6, 1))
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelPathInUse: "
                "CASE-81: Test routine passed for valid path for P2MP tunnel - "
                "Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhTestv2MplsTunnelPathInUse: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2MplsTunnelRowStatus ()
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    tSNMP_OID_TYPE      TunnelXCPointer;

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };
    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };

    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4ErrorCode = 0;
    UINT4               u4XCIndex = 1;
    UINT4               u4InIndex = 1;
    UINT4               u4OutIndex = 1;

    printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: ENTRY");

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&XCIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6,
                                       TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "Test routine to create tunnel row failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_CREATEANDWAIT))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-82: Make the non-P2MP tunnel row as active without associating 
     * XC index */
    if ((nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, TE_ACTIVE))
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-82: Test routine succeeded in making non-P2MP tunnel row "
                "as active without valid XC index - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-82: Test routine failed in making non-P2MP tunnel row "
                "as active without valid XC index - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-83: Make the non-P2MP tunnel row as active after associating 
     * valid XC index */
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-83: Unable to create XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 11] = u4XCIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 6] = u4InIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 1] = u4OutIndex;
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;
    if ((nmhSetMplsTunnelXCPointer (10, 1, 5, 6, &TunnelXCPointer))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-83: Failed to update XC pointer");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, TE_ACTIVE))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-83: Test routine failed to make non-P2MP tunnel row "
                "with valid XC index as active - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-84: Make the MPLS tunnel row as active for a P2MP tunnel */
    /* Make XC index as 0 before setting tunnel type as P2MP */
    nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                           LSR_DESTROY);
    nmhSetMplsInSegmentRowStatus (&InSegmentIndex, LSR_DESTROY);
    nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, LSR_DESTROY);

    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 11] = 0;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 6] = 0;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 1] = 0;
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;
    if ((nmhSetMplsTunnelXCPointer (10, 1, 5, 6, &TunnelXCPointer))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-84: Failed to set tunnel XC index to zero");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-84: Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, TE_ACTIVE))
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-84: Test routine succeeded in making MPLS tunnel row "
                "as active for P2MP tunnel - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-84: Test routine to make MPLS tunnel row as active "
                "for P2MP tunnel - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-85: Destroy P2MP tunnel when destination is associated */
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-85: Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((TeCheckP2mpTableEntry (10, 1, 5, 6, &pTeTnlInfo)) != TE_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-85: Unable to fetch tunnel entry - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((TeAddP2mpDestEntry (pTeTnlInfo, 0x1E000001)) == NULL)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-85: Unable to create P2MP destination row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, TE_DESTROY))
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-85: Test routine succeeded in destroying MPLS P2MP "
                "tunnel row when destination is associated - Test case failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-85: Test routine failed in destroying MPLS P2MP "
                "tunnel row when destination is associated - Wrong error code "
                "- Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((TeRemoveP2mpDestEntry (pTeTnlInfo, 0x1E000001)) == TE_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-85: Unable to delete P2MP destination row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-86: Destroy P2MP tunnel when branch out-segment is associated */
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-86: Unable to create XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((TeAddP2mpBranchEntry (pTeTnlInfo, 0x1)) == NULL)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-86: Unable to create P2MP branch out-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, TE_DESTROY))
        == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-86: Test routine succeeded in destroying MPLS P2MP "
                "tunnel row when branch out-segment is associated - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-86: Test routine failed in destroying MPLS P2MP "
                "tunnel row when branch out-segment is associated - Wrong "
                "error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((TeRemoveP2mpBranchEntry (pTeTnlInfo, 0x1)) == TE_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-86: Unable to delete P2MP branch out-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-87: Destroy P2MP tunnel when there are no destination and 
     * branch out-segment entries */
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_DESTROY)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-87: Test routine failed in destroying XC - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsInSegmentRowStatus (&InSegmentIndex, LSR_DESTROY))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-87: Test routine failed in destroying In-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, LSR_DESTROY))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-87: Test routine failed in destroying out-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          TE_DESTROY)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-87: Unable to delete P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if ((nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, TE_DESTROY))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: "
                "CASE-87: Test routine failed in destroying MPLS P2MP "
                "tunnel row when it does not have destination or branch "
                "out-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhTestv2MplsTunnelRowStatus: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2MplsTunnelType ()
{
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OID_TYPE      TunnelXCPointer;

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };

    UINT4               u4ErrorCode = 0;
    UINT4               u4TempP2mpTunnelCount = 0;
    UINT4               u4XCIndex = 1;
    UINT4               u4InIndex = 1;
    UINT4               u4OutIndex = 1;

    UINT1               au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };
    tTeTnlInfo         *pTeTnlInfo = NULL;

    printf ("\r\nTest_nmhTestv2MplsTunnelType: ENTRY");

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;

    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&XCIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));

    /* CASE-88: TE GBL ADMIN DOWN - Set tunnel type */
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_DOWN;
    if ((nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6,
                                    &MplsTeTnlType)) == SNMP_SUCCESS)
    {
        gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-88: TE GBL ADMIN DOWN - Failed");
        return OSIX_FAILURE;
    }
    gTeGblInfo.TeParams.u1TeAdminStatus = TE_ADMIN_UP;
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-88: TE GBL ADMIN DOWN - Wrong error code - Failed");
        return OSIX_FAILURE;
    }

    /* CASE-89: Set tunnel type without creating the tunnel entry */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6,
                                    &MplsTeTnlType)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-89: Test routine passed for configuring tunnel type "
                "without the presence of tunnel entry - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-89: Test routine failed for configuring tunnel type "
                "without presence of tunnel entry - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-90: Set tunnel type as P2MP when MAX P2MP tunnel limit has reached */
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_CREATEANDWAIT))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-90: Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    u4TempP2mpTunnelCount = TE_P2MP_CONFIGURED_TNLS (gTeGblInfo);
    TE_P2MP_CONFIGURED_TNLS (gTeGblInfo) = MAX_TE_P2MP_TUNNEL_INFO;
    if ((nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6,
                                    &MplsTeTnlType)) == SNMP_SUCCESS)
    {
        TE_P2MP_CONFIGURED_TNLS (gTeGblInfo) = u4TempP2mpTunnelCount;
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-90: Test routine passed for configuring P2MP tunnel type "
                "when MAX P2MP tunnel limit has reached - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        TE_P2MP_CONFIGURED_TNLS (gTeGblInfo) = u4TempP2mpTunnelCount;
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-90: Test routine failed for configuring P2MP tunnel type "
                "when MAX P2MP tunnel limit has reached - Wrong error code - "
                "Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    TE_P2MP_CONFIGURED_TNLS (gTeGblInfo) = u4TempP2mpTunnelCount;

    /* CASE-91: Set tunnel type as P2MP when tunnel is previously HLSP */
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = (TE_TNL_TYPE_MPLS + TE_TNL_TYPE_HLSP);
    }

    MplsTeTnlType.pu1_OctetList[TE_ZERO] = TE_TNL_TYPE_P2MP;
    if ((nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6,
                                    &MplsTeTnlType)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-91: Test routine passed for configuring P2MP tunnel type "
                "when it is previously HLSP - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-91: Test routine failed for configuring P2MP tunnel type "
                "when it is previously HLSP - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* Configure tunnel type to default */
    TE_TNL_TYPE (pTeTnlInfo) = 0;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] = TE_TNL_TYPE_MPLS;
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-91: Unable to set default tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-92: Set tunnel type as P2MP when signaling protocol is associated */
    if ((nmhSetMplsTunnelSignallingProto (10, 1, 5, 6, TE_SIGPROTO_RSVP))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-92: Unable to set RSVP as signaling protocol - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    MplsTeTnlType.pu1_OctetList[TE_ZERO] = TE_TNL_TYPE_P2MP;
    if ((nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6,
                                    &MplsTeTnlType)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-91: Test routine passed for configuring P2MP tunnel type "
                "when RSVP is the signaling protocol - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-91: Test routine failed for configuring P2MP tunnel type "
                "when RSVP is the signaling protocol - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTunnelSignallingProto (10, 1, 5, 6, TE_SIGPROTO_NONE))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-92: Unable to set signaling protocol to none - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-93: Set tunnel type as P2MP when XC index is associated to tunnel */
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 11] = u4XCIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 6] = u4InIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 1] = u4OutIndex;
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;
    if ((nmhSetMplsTunnelXCPointer (10, 1, 5, 6, &TunnelXCPointer))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-93: Failed to update XC pointer");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    MplsTeTnlType.pu1_OctetList[TE_ZERO] = TE_TNL_TYPE_P2MP;
    if ((nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6,
                                    &MplsTeTnlType)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-93: Test routine passed for configuring P2MP tunnel type "
                "when XC index is associated to tunnel - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-93: Test routine failed for configuring P2MP tunnel type "
                "when XC index is associated to tunnel - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-94: Set tunnel type as P2MP when row status is active */
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_ACTIVE)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-94: Unable to activate the tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    MplsTeTnlType.pu1_OctetList[TE_ZERO] = TE_TNL_TYPE_P2MP;
    if ((nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6,
                                    &MplsTeTnlType)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-94: Test routine passed for configuring P2MP tunnel type "
                "when tunnel row is active - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-94: Test routine failed for configuring P2MP tunnel type "
                "when tunnel row is active - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-95: Set tunnel type as P2MP when tunnel has valid hop table index */
    /* Make tunnel row as not in service */
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_NOTINSERVICE))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-95: Unable to activate the tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* Make XC index as 0 */
    nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                           LSR_DESTROY);
    nmhSetMplsInSegmentRowStatus (&InSegmentIndex, LSR_DESTROY);
    nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, LSR_DESTROY);

    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 11] = 0;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 6] = 0;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 1] = 0;
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;
    if ((nmhSetMplsTunnelXCPointer (10, 1, 5, 6, &TunnelXCPointer))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-95: Failed to set tunnel XC index to zero");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if ((nmhSetMplsTunnelHopTableIndex (10, 1, 5, 6, 1)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-95: Unable to set valid hop table index - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    MplsTeTnlType.pu1_OctetList[TE_ZERO] = TE_TNL_TYPE_P2MP;
    if ((nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6,
                                    &MplsTeTnlType)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-95: Test routine passed for configuring P2MP tunnel type "
                "when tunnel has valid hop table index - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-95: Test routine failed for configuring P2MP tunnel type "
                "when tunnel has valid hop table index - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTunnelHopTableIndex (10, 1, 5, 6, 0)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-95: Unable to set hop table index to 0 - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* CASE-96: Set tunnel type as P2MP when tunnel has valid path in use */
    if ((nmhSetMplsTunnelPathInUse (10, 1, 5, 6, 1)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-96: Unable to set valid path in use - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    MplsTeTnlType.pu1_OctetList[TE_ZERO] = TE_TNL_TYPE_P2MP;
    if ((nmhTestv2FsMplsTunnelType (&u4ErrorCode, 10, 1, 5, 6,
                                    &MplsTeTnlType)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-96: Test routine passed for configuring P2MP tunnel type "
                "when tunnel has valid path in use - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-96: Test routine failed for configuring P2MP tunnel type "
                "when tunnel has valid path in use - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTunnelPathInUse (10, 1, 5, 6, 0)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsTunnelType: "
                "CASE-96: Unable to set path in use to 0 - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhTestv2MplsTunnelType: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_MplsDbUpdateP2mpTunnelInHw ()
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tOutSegment        *pOutSegment = NULL;

    printf ("\r\nTest_MplsDbUpdateP2mpTunnelInHw: ENTRY");

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_CREATEANDWAIT))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_MplsDbUpdateP2mpTunnelInHw: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
        TE_TNL_ROLE (pTeTnlInfo) = TE_INTERMEDIATE;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_MplsDbUpdateP2mpTunnelInHw: "
                "Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 30.0.0.1 in-label 200010 vlan 3 out-label 200040 100.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 40.0.0.1 in-label 200010 vlan 3 out-label 200040 100.0.0.2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pOutSegment = MplsGetOutSegmentTableEntry (1);
    OUTSEGMENT_HW_STATUS (pOutSegment) = MPLS_TRUE;
    /*Create entry in hw when oper up */
    if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo, 1) != TE_SUCCESS)
    {
        printf ("\r\nTest_MplsDbUpdateP2mpTunnelInHw: "
                "Unable to create in hw - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t ");
        CliExecuteAppCmd ("interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /*Delete entry in hw when oper down */
    OUTSEGMENT_HW_STATUS (pOutSegment) = MPLS_FALSE;
    if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo, 2) != TE_SUCCESS)
    {
        printf ("\r\nTest_MplsDbUpdateP2mpTunnelInHw: "
                "Unable to delete in hw - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t ");
        CliExecuteAppCmd ("interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    printf ("\r\nTest_MplsDbUpdateP2mpTunnelInHw: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
    CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_MplsCheckP2mpTnlRole ()
{
    tTeTnlInfo          TeTnlInfo, *pTeTnlInfo = NULL;

    /*Check when the tunnel pointer is NULL */
    if (MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo) != MPLS_FAILURE)
    {
        printf ("\r\n Failed for pointer NULL case");
        return OSIX_FAILURE;
    }

    MEMSET (&TeTnlInfo, 0, sizeof (tTeTnlInfo));
    pTeTnlInfo = &TeTnlInfo;

    /*Check when the tunnel pointer is not NULL */
    if (MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo) != MPLS_FAILURE)
    {
        printf ("\r\n Failed for pointer not NULL case");
        return OSIX_FAILURE;
    }

    TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;

    /*Check when the tunnel is p2mp */
    /*Check when the tunnel role is egress */
    TE_TNL_ROLE (pTeTnlInfo) = TE_EGRESS;
    if (MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo) != MPLS_FAILURE)
    {
        printf ("\r\n Failed for p2mp tunnel egress case");
        return OSIX_FAILURE;
    }

    /*Check when the tunnel role is ingress */
    TE_TNL_ROLE (pTeTnlInfo) = TE_INGRESS;
    if (MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo) == MPLS_FAILURE)
    {
        printf ("\r\n Failed for p2mp tunnel ingress case");
        return OSIX_FAILURE;
    }

    printf ("\r\nTest_MplsCheckP2mpTnlRole: ENTRY");
    printf ("\r\nTest_MplsCheckP2mpTnlRole: All test cases passed");
    return OSIX_SUCCESS;
}

INT4
Test_TeP2mpExtSetOperStatusAndProgHw ()
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tOutSegment        *pOutSegment = NULL;

    printf ("\r\nTest_TeP2mpExtSetOperStatusAndProgHw: ENTRY");

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_CREATEANDWAIT))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_TeP2mpExtSetOperStatusAndProgHw: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
        TE_TNL_ROLE (pTeTnlInfo) = TE_INTERMEDIATE;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_TeP2mpExtSetOperStatusAndProgHw: "
                "Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 30.0.0.1 in-label 200010 vlan 3 out-label 200040 100.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 40.0.0.1 in-label 200010 vlan 3 out-label 200040 100.0.0.2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pOutSegment = MplsGetOutSegmentTableEntry (1);
    OUTSEGMENT_HW_STATUS (pOutSegment) = MPLS_TRUE;

    /*Create entry in hw when oper up */
    if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo, 1) != TE_SUCCESS)
    {
        printf ("\r\nTest_TeP2mpExtSetOperStatusAndProgHw: "
                "Unable to create in hw - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t ");
        CliExecuteAppCmd ("interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /*Delete entry in hw when oper down */
    OUTSEGMENT_HW_STATUS (pOutSegment) = MPLS_FALSE;
    if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo, 2) != TE_SUCCESS)
    {
        printf ("\r\nTest_TeP2mpExtSetOperStatusAndProgHw: "
                "Unable to delete in hw - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t ");
        CliExecuteAppCmd ("interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    printf ("\r\nTest_TeP2mpExtSetOperStatusAndProgHw: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
    CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_nmhSetMplsXCRowStatus ()
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OID_TYPE      TunnelXCPointer;

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };

    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4XCIndex = 1;
    UINT4               u4InIndex = 1;
    UINT4               u4OutIndex = 1;

    printf ("\r\nTest_nmhSetMplsXCRowStatus: ENTRY");

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;

    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    MPLS_INTEGER_TO_OCTETSTRING (0x0, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&XCIndex));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_CREATEANDWAIT))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsXCRowStatus: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsXCRowStatus: " "Unable to create XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 11] = u4XCIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 6] = u4InIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 1] = u4OutIndex;
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;
    if ((nmhSetMplsTunnelXCPointer (10, 1, 5, 6, &TunnelXCPointer))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsXCRowStatus: "
                "Unable to set valid XC pointer - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-106: For P2P tunnel, activate the XC row */
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_ACTIVE)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsXCRowStatus: "
                "CASE-106: Unable to activate the XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    if (pOutSegment != NULL)
    {
        pXcEntry = (tXcEntry *) (pOutSegment->pXcIndex);
        printf ("CASE-106: XC Active. XC-tnl ptr = %p",
                XC_TNL_TBL_PTR (pXcEntry));
    }

    /* CASE-107: For P2P tunnel, destroy the XC row */
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_DESTROY)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsXCRowStatus: "
                "CASE-107: Unable to destroy the XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    if (pOutSegment->pXcIndex != NULL)
    {
        pXcEntry = (tXcEntry *) (pOutSegment->pXcIndex);
        printf ("CASE-107: XC destroyed. Out-seg tnl ptr = %p",
                XC_TNL_TBL_PTR (pXcEntry));
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhSetMplsXCRowStatus: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, LSR_DESTROY);
    return OSIX_SUCCESS;
}

INT4
Test_nmhTestv2MplsXCRowStatus ()
{
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;
    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    tSNMP_OID_TYPE      TunnelXCPointer;

    static UINT1        au1P2mpTnlDestination[MPLS_INDEX_LENGTH];
    static UINT1        au1SrcSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };
    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };

    UINT4               u4ErrorCode = 0;
    UINT4               u4XCIndex = 1;
    UINT4               u4InIndex = 1;
    UINT4               u4OutIndex = 1;

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlDestination.i4_Length = MPLS_INDEX_LENGTH;
    P2mpTnlSrcSubGroupOrigin.pu1_OctetList = au1SrcSubGroupOrigin;
    P2mpTnlSrcSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    P2mpTnlDestSubGroupOrigin.pu1_OctetList = au1DestSubGroupOrigin;
    P2mpTnlDestSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    printf ("\r\nTest_nmhTestv2MplsXCRowStatus: ENTRY");

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    MPLS_INTEGER_TO_OCTETSTRING (0x1E000001, (&P2mpTnlDestination));
    MPLS_INTEGER_TO_OCTETSTRING (0x0, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&XCIndex));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_CREATEANDWAIT))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "Unable to create XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 11] = u4XCIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 6] = u4InIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 1] = u4OutIndex;
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;
    if ((nmhSetMplsTunnelXCPointer (10, 1, 5, 6, &TunnelXCPointer))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "Unable to set valid XC pointer - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-108: For P2P tunnel, delete the XC when it is associated to tunnel */
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 11] = 0;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 6] = 0;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 1] = 0;
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;
    if ((nmhSetMplsTunnelXCPointer (10, 1, 5, 6, &TunnelXCPointer))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "CASE-108: Unable to set XC pointer to 0 - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                &OutSegmentIndex, LSR_DESTROY)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "CASE-108: Unable to delete XC row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* Delete the P2P tunnel */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Create a P2MP tunnel */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_CREATEANDWAIT))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "Create a P2MP tunnel - Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;end");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             TE_CREATEANDWAIT) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "Unable to create destination row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "Unable to create XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestBranchOutSegment (10, 1, 5, 6, LSR_ADDR_IPV4,
                                                    &P2mpTnlSrcSubGroupOrigin,
                                                    0, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestSubGroupOrigin,
                                                    1, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestination,
                                                    &OutSegmentIndex) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "Unable to associate branch out-segment to destination row - "
                "Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-109: For P2MP tunnel, delete the XC when branch index 
     * is associated to a P2MP destination */
    if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   LSR_DESTROY)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "CASE-109: Test routine passed for deleting XC "
                "when branch entry is present - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "CASE-109: Test routine failed for deleting XC when branch "
                "entry is present - Wrong error code - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-110: For P2MP tunnel, delete the XC when no destination 
     * is associated */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             TE_DESTROY) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "CASE-110: Unable to delete destination row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   LSR_DESTROY)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
                "CASE-110: Test routine failed to delete XC when destinations "
                "are not assocaited - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhTestv2MplsXCRowStatus: All test cases passed");
    nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                           LSR_DESTROY);
    nmhSetMplsInSegmentRowStatus (&InSegmentIndex, LSR_DESTROY);
    nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, LSR_DESTROY);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_TeCliSetMplsTunnelRowStatus ()
{
    printf ("\r\nTest_TeCliSetMplsTunnelRowStatus: ENTRY");

    /* CASE-111: Create a P2P tunnel at ingress LSR and associate static 
     * out-label */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Delete the P2P tunnel */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-112: Create a P2MP tunnel at egress LSR and associate static 
     * in-label */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 5.0.0.0");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* All test cases successfully executed */
    printf ("\r\nTest_TeCliSetMplsTunnelRowStatus: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_TeSetCfgParams ()
{
    /* CASE-113: Test_TeSetCfgParams */
    printf ("\r\nTest_TeSetCfgParams: CASE-113");
    printf ("\r\nTE_MAX_HOP_PER_PO = %d", TE_MAX_HOP_PER_PO (gTeGblInfo));
    printf ("\r\nTE_P2MP_TNL_MAX_HOP = %d", TE_P2MP_TNL_MAX_HOP (gTeGblInfo));
    printf ("\r\nP2MP Notification = %d",
            TE_P2MP_TNL_NOTIFICATION (gTeGblInfo));
    if ((TE_P2MP_TNL_MAX_HOP (gTeGblInfo) == TE_MAX_HOP_PER_PO (gTeGblInfo))
        && (TE_P2MP_TNL_NOTIFICATION (gTeGblInfo) == TE_SNMP_FALSE))
    {
        return OSIX_SUCCESS;
    }
    else
    {
        return OSIX_FAILURE;
    }
}

INT4
Test_nmhSetMplsTunnelAdminStatus ()
{
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;
    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    tSNMP_OID_TYPE      TunnelXCPointer;

    static UINT1        au1P2mpTnlDestination[MPLS_INDEX_LENGTH];
    static UINT1        au1SrcSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };
    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };

    UINT4               u4XCIndex = 1;
    UINT4               u4InIndex = 1;
    UINT4               u4OutIndex = 0;

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlDestination.i4_Length = MPLS_INDEX_LENGTH;
    P2mpTnlSrcSubGroupOrigin.pu1_OctetList = au1SrcSubGroupOrigin;
    P2mpTnlSrcSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    P2mpTnlDestSubGroupOrigin.pu1_OctetList = au1DestSubGroupOrigin;
    P2mpTnlDestSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: ENTRY");

    MPLS_INTEGER_TO_OCTETSTRING (0x0, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&XCIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1E000001, (&P2mpTnlDestination));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6,
                                    TE_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             TE_CREATEANDWAIT) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "Failed to set dest row status as create and wait");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "Unable to create XC row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestBranchOutSegment (10, 1, 5, 6, LSR_ADDR_IPV4,
                                                    &P2mpTnlSrcSubGroupOrigin,
                                                    0, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestSubGroupOrigin,
                                                    1, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestination,
                                                    &OutSegmentIndex) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "Unable to assign a valid out-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* Create second destination and associate to new out-segment */
    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));

    MPLS_INTEGER_TO_OCTETSTRING (0x28000001, (&P2mpTnlDestination));
    if (nmhSetMplsTeP2mpTunnelDestRowStatus (10, 1, 5, 6, LSR_ADDR_IPV4,
                                             &P2mpTnlSrcSubGroupOrigin, 0,
                                             LSR_ADDR_IPV4,
                                             &P2mpTnlDestSubGroupOrigin, 1,
                                             LSR_ADDR_IPV4, &P2mpTnlDestination,
                                             TE_CREATEANDWAIT) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "Failed to set second dest row status as create and wait");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "Unable to create second row in XC table");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestBranchOutSegment (10, 1, 5, 6, LSR_ADDR_IPV4,
                                                    &P2mpTnlSrcSubGroupOrigin,
                                                    0, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestSubGroupOrigin,
                                                    1, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestination,
                                                    &OutSegmentIndex) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "Unable to assign a valid out-segment to second destination - "
                "Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* Make P2MP tunnel row as active */
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, TE_ACTIVE))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "Unable to make the tunnel row as ACTIVE - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE-114: Make the P2MP tunnel as admin up at ingress */
    if (nmhSetMplsTunnelAdminStatus (10, 1, 5, 6, TE_ADMIN_UP) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-114: Unable to make the tunnel as admin up at ingress "
                "LSR - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    printf ("\r\nCASE:114 - No. of active tunnels = %d",
            TE_P2MP_ACTIVE_TNLS (gTeGblInfo));

    /* CASE-115: Make the P2MP tunnel as admin down at ingress */
    if (nmhSetMplsTunnelAdminStatus (10, 1, 5, 6, TE_ADMIN_DOWN) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-115: Unable to make the tunnel as admin down at ingress "
                "LSR - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    printf ("\r\nCASE:115 - No. of active tunnels = %d",
            TE_P2MP_ACTIVE_TNLS (gTeGblInfo));

    /* CASE-116: Make the P2MP tunnel as admin up at transit LSR */
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, TE_NOTINSERVICE))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-116: Unable to make the tunnel row as NOT IN SERVICE - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTunnelRole (10, 1, 5, 6, TE_INTERMEDIATE) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-116: Unable to make the tunnel role as intermediate - "
                "Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, TE_ACTIVE))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-116: Unable to make the tunnel row as ACTIVE - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);

        return OSIX_FAILURE;
    }
    if (nmhSetMplsTunnelAdminStatus (10, 1, 5, 6, TE_ADMIN_UP) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-116: Unable to make the tunnel as admin up at transit "
                "LSR - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    printf ("\r\nCASE:115 - No. of active tunnels = %d",
            TE_P2MP_ACTIVE_TNLS (gTeGblInfo));

    /* CASE-117: Make the P2MP tunnel as admin down at transit LSR */
    if (nmhSetMplsTunnelAdminStatus (10, 1, 5, 6, TE_ADMIN_DOWN) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-117: Unable to make the tunnel as admin down at transit "
                "LSR - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        MPLS_INTEGER_TO_OCTETSTRING (0x2, (&OutSegmentIndex));
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    printf ("\r\nCASE:117 - No. of active tunnels = %d",
            TE_P2MP_ACTIVE_TNLS (gTeGblInfo));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
       LSR_DESTROY)) == SNMP_FAILURE)
       {
       printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
       "CASE-117: Unable to delete XC row for Out-index 2 - Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       return OSIX_FAILURE;
       }

       MPLS_INTEGER_TO_OCTETSTRING(0x1, (&OutSegmentIndex));
       if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
       LSR_DESTROY)) == SNMP_FAILURE)
       {
       printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
       "CASE-117: Unable to delete XC row for Out-index 1 - Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       return OSIX_FAILURE;
       } */

    /* CASE-118: Make the P2MP tunnel as admin up at egress LSR */
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x0, (&OutSegmentIndex));
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-118: Unable to create XC row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    /*if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, TE_NOTINSERVICE))
       == SNMP_FAILURE)
       {
       printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
       "CASE-118: Unable to make the tunnel row as NOT IN SERVICE - Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       return OSIX_FAILURE;
       }
       if (nmhSetMplsTunnelRole (10, 1, 5, 6, TE_EGRESS) == SNMP_FAILURE)
       {
       printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
       "CASE-118: Unable to make the tunnel role as egress - "
       "Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex, LSR_DESTROY);
       return OSIX_FAILURE;
       } */
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 11] = u4XCIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 6] = u4InIndex;
    au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET - 1] = u4OutIndex;
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;
    /*if ((nmhSetMplsTunnelXCPointer (10, 1, 5, 6, &TunnelXCPointer))
       == SNMP_FAILURE)
       {
       printf ("\r\nTest_nmhTestv2MplsXCRowStatus: "
       "CASE-118: Unable to set valid XC pointer - Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex, LSR_DESTROY);
       return OSIX_FAILURE;
       }
       if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
       LSR_ACTIVE)) == SNMP_FAILURE)
       {
       printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
       "CASE-118: Unable to make XC row as ACTIVE - Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex, LSR_DESTROY);
       return OSIX_FAILURE;
       }
       if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, TE_ACTIVE))
       == SNMP_FAILURE)
       {
       printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
       "CASE-118: Unable to make the tunnel row as ACTIVE - Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex, LSR_DESTROY);
       return OSIX_FAILURE;
       }
       if (nmhSetMplsTunnelAdminStatus (10, 1, 5, 6, TE_ADMIN_UP) == SNMP_FAILURE)
       {
       printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
       "CASE-118: Unable to make the tunnel as admin up at egress "
       "LSR - Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex, LSR_DESTROY);
       return OSIX_FAILURE;
       } */
    printf ("\r\nCASE:118 - No. of active tunnels = %d",
            TE_P2MP_ACTIVE_TNLS (gTeGblInfo));

    /* CASE-119: Make the P2MP tunnel as admin down at egress LSR */
    /*if (nmhSetMplsTunnelAdminStatus (10, 1, 5, 6, TE_ADMIN_DOWN) == SNMP_FAILURE)
       {
       printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
       "CASE-119: Unable to make the tunnel as admin down at egress "
       "LSR - Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex, LSR_DESTROY);
       return OSIX_FAILURE;
       } */
    printf ("\r\nCASE:119 - No. of active tunnels = %d",
            TE_P2MP_ACTIVE_TNLS (gTeGblInfo));

    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_DESTROY)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-119: Unable to delete XC row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    /* Configure XC for ingress tunnel before deleting destinations */
    /*MPLS_INTEGER_TO_OCTETSTRING(0x0, (&InSegmentIndex));
       MPLS_INTEGER_TO_OCTETSTRING(0x1, (&OutSegmentIndex));
       if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
       LSR_CREATEANDWAIT)) == SNMP_FAILURE)
       {
       printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
       "CASE-119: Unable to create XC row for ingress LSR - Failed");
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
       CliGiveAppContext();
       MGMT_LOCK();
       return OSIX_FAILURE;
       } */
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, TE_NOTINSERVICE))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-119: Unable to make the tunnel row as NOT IN SERVICE - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTunnelRole (10, 1, 5, 6, TE_INGRESS) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: "
                "CASE-119: Unable to make the tunnel role as egress - "
                "Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhSetMplsTunnelAdminStatus: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_nmhSetMplsTunnelRowStatus ()
{
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;
    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;

    static UINT1        au1P2mpTnlDestination[MPLS_INDEX_LENGTH];
    static UINT1        au1SrcSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestSubGroupOrigin[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    printf ("\r\nTest_nmhSetMplsTunnelRowStatus: ENTRY");

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    MplsTeTnlType.pu1_OctetList[TE_ZERO] |= TE_TNL_TYPE_P2MP;

    P2mpTnlSrcSubGroupOrigin.pu1_OctetList = au1SrcSubGroupOrigin;
    P2mpTnlSrcSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1SrcSubGroupOrigin, 0, MPLS_INDEX_LENGTH);
    P2mpTnlDestSubGroupOrigin.pu1_OctetList = au1DestSubGroupOrigin;
    P2mpTnlDestSubGroupOrigin.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1DestSubGroupOrigin, 0, MPLS_INDEX_LENGTH);

    MEMSET (au1P2mpTnlDestination, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (0x1E000001, (&P2mpTnlDestination));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&OutSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (0x1, (&XCIndex));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                LSR_CREATEANDWAIT)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelRowStatus: "
                "Unable to create XC row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_CREATEANDWAIT))
        == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelRowStatus: "
                "Unable to create tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType (10, 1, 5, 6, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelRowStatus: "
                "Unable to set tunnel type - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelRowStatus: "
                "Unable to create P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE:120 - Destroy tunnel row when P2MP destinations are associated */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, TE_CREATEANDWAIT) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelRowStatus: "
                "CASE-120: Failed to set dest row status as create and wait");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if (nmhSetMplsTeP2mpTunnelDestBranchOutSegment (10, 1, 5, 6,
                                                    LSR_ADDR_IPV4,
                                                    &P2mpTnlSrcSubGroupOrigin,
                                                    0, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestSubGroupOrigin,
                                                    1, LSR_ADDR_IPV4,
                                                    &P2mpTnlDestination,
                                                    &OutSegmentIndex) ==
        SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelRowStatus: "
                "CASE-120: Unable to assign a valid out-segment - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_DESTROY)) == SNMP_SUCCESS)
    {
        printf ("\r\nTest_nmhSetMplsTunnelRowStatus: "
                "CASE-120: Able to delete tunnel row when P2MP destinations "
                "are associated - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* CASE:121 - Destroy tunnel row when P2MP destinations are not associated */
    if (nmhSetMplsTeP2mpTunnelDestRowStatus
        (10, 1, 5, 6, LSR_ADDR_IPV4, &P2mpTnlSrcSubGroupOrigin, 0,
         LSR_ADDR_IPV4, &P2mpTnlDestSubGroupOrigin, 1, LSR_ADDR_IPV4,
         &P2mpTnlDestination, TE_DESTROY) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelRowStatus: "
                "CASE-121: Failed to delete dest row");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd ("exit;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6,
                                          TE_DESTROY)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelRowStatus: "
                "Unable to delete P2MP tunnel row - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }
    if ((nmhSetMplsTunnelRowStatus (10, 1, 5, 6, TE_DESTROY)) == SNMP_FAILURE)
    {
        printf ("\r\nTest_nmhSetMplsTunnelRowStatus: "
                "CASE-121: Unable to delete tunnel row when P2MP destinations "
                "are not associated - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               LSR_DESTROY);
        return OSIX_FAILURE;
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_nmhSetMplsTunnelRowStatus: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                           LSR_DESTROY);
    nmhSetMplsInSegmentRowStatus (&InSegmentIndex, LSR_DESTROY);
    nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, LSR_DESTROY);
    return OSIX_SUCCESS;
}

INT4
Test_TeCliShowTnlDetail ()
{
    printf ("\r\nTest_TeCliShowTnlDetail: ENTRY");

    /* CASE-122: Display P2P tunnel information in detail */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-123: Display P2MP tunnel information in detail */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no interface mplstunnel 10");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 5.0.0.0");

    CliExecuteAppCmd ("tunnel mpls static point-to-multipoint destination "
                      "30.0.0.1 in-label 200001 vlan 3 out-label 200040 100.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static point-to-multipoint destination "
                      "40.0.0.1 in-label 200001 vlan 3 out-label 200040 100.0.0.2");
    CliExecuteAppCmd ("tunnel mpls static point-to-multipoint destination "
                      "20.0.0.1 in-label 200001 vlan 3 out-label 200040 100.0.0.2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* All test cases successfully executed */
    printf ("\r\nTest_TeCliShowTnlDetail: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 40.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 20.0.0.1;end");
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_TeCliP2mpTunnelCreate ()
{
    printf ("\r\nTest_TeCliP2mpTunnelCreate: ENTRY");
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeP2mpTnlInfo     *pTeP2mpTnlInfo = NULL;
    UINT4               u4IngrTnlIp = 0;
    UINT4               u4EgrsTnlIp = 0;

    /* CASE-124: Create P2MP tunnel entry when it already exists */
    printf ("\r\nCASE-124:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-125: Create P2MP tunnel through CLI */
    printf ("\r\nCASE-125:");
    if ((TeCheckP2mpTableEntry (10, 1, 0x0A000001, 6, &pTeTnlInfo)) !=
        TE_SUCCESS)
    {
        printf ("\r\nTest_TeCliP2mpTunnelCreate: "
                "CASE-125: Unable to fetch P2MP tunnel entry - Failed");
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }
    pTeP2mpTnlInfo = TE_P2MP_TNL_INFO (pTeTnlInfo);
    printf ("\r\nTest_TeCliP2mpTunnelCreate: Tunnel Information");
    MEMCPY ((UINT1 *) &u4IngrTnlIp, &TE_TNL_INGRESS_LSRID (pTeTnlInfo), 4);
    u4IngrTnlIp = OSIX_HTONL (u4IngrTnlIp);
    MEMCPY ((UINT1 *) &u4EgrsTnlIp, &TE_TNL_EGRESS_LSRID (pTeTnlInfo), 4);
    u4EgrsTnlIp = OSIX_HTONL (u4EgrsTnlIp);
    printf ("\r\nTnl Index: %d  Instance: %d", TE_TNL_TNL_INDEX (pTeTnlInfo),
            TE_TNL_TNL_INSTANCE (pTeTnlInfo));
    printf ("\r\nSource: 0x%x  P2MP ID: %d", u4IngrTnlIp, u4EgrsTnlIp);
    printf ("\r\nType: 0x%x", TE_TNL_TYPE (pTeTnlInfo));
    printf ("\r\nP2MP Integrity: %d, Branch Role: %d",
            TE_P2MP_TNL_INTEGRITY (pTeP2mpTnlInfo),
            TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo));

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no interface mplstunnel 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
    return OSIX_SUCCESS;
}

INT4
Test_TeCliP2mpAddDestinationAtIngress ()
{
    tOutSegment        *pOutSegment = NULL;

    printf ("\r\nTest_TeCliP2mpAddDestinationAtIngress: ENTRY");

    CreateConf2 ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface loopback 0");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("ip address 10.0.0.1 255.255.255.255");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-126: Add destination whose address is same as local IP address */
    printf ("\r\nCASE-126:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 10.0.0.1 out-label 200001 2.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-127: Add destinations without adding static route for next-hop 
     * address */
    printf ("\r\nCASE-127:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 out-label 200001 2.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Make VLAN interface as up */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 out-label 200001 2.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-128: Add P2MP destination without creating tunnel entry */
    printf ("\r\nCASE-128:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 out-label 200001 2.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-129: Add destination for default tunnel type */
    printf ("\r\nCASE-129:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 20.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 out-label 200001 2.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-130: Add destination with only out-label and next-hop at 
     * intermediate LSR */
    printf ("\r\nCASE-130:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no tunnel mpls destination 20.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 30.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 out-label 200001 2.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-131: Add second destination with same out-label but different 
     * next-hop address */
    /* Add first destination */
    printf ("\r\nCASE-131:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 30.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 out-label 200001 2.0.0.2");
    /* Add second destination with different next-hop */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 out-label 200001 4.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-132: Add second destination with same out-label and next-hop */
    printf ("\r\nCASE-132:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 out-label 200001 2.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-133: Add third destination with different out-label and next-hop.
     * Check in-segment, XC and out-segment entries */
    printf ("\r\nCASE-133:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 102.0.0.1 out-label 200002 3.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pOutSegment = MplsGetOutSegmentTableEntry (1);
    if (NULL != pOutSegment)
    {
        printf ("\r\nTest_TeCliP2mpAddDestination: CASE-133");
        printf ("\r\nFirst Out-Label = %d", OUTSEGMENT_LABEL (pOutSegment));
    }
    pOutSegment = MplsGetOutSegmentTableEntry (2);
    if (NULL != pOutSegment)
    {
        printf ("\r\nTest_TeCliP2mpAddDestination: CASE-133");
        printf ("\r\nSecond Out-Label = %d", OUTSEGMENT_LABEL (pOutSegment));
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_TeCliP2mpAddDestinationAtIngress: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 101.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 102.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("exit;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    DeleteConf2 ();

    return OSIX_SUCCESS;
}

INT4
Test_TeCliP2mpStaticBind ()
{
    tInSegment         *pInSegment = NULL;
    tOutSegment        *pOutSegment = NULL;

    printf ("\r\nTest_TeCliP2mpStaticBind: ENTRY");

    CreateConf2 ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface loopback 0");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.255.255.255");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-134: Input next-hop address to be same as local IP address */
    printf ("\r\nCASE-134:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 20.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-135: Add destination without adding static route for next-hop 
     * address */
    printf ("\r\nCASE-135:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface vlan 3");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-136: Add P2MP destination without creating tunnel entry */
    printf ("\r\nCASE-136:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface vlan 3");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-137: Add destination for default tunnel type */
    printf ("\r\nCASE-137:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 30.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-138: Add destination with in-label and out-label at ingress LSR */
    printf ("\r\nCASE-138:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no tunnel mpls destination 30.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 20.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-139: Add second destination with same out-label and next-hop 
     * address, but different in-label */
    printf ("\r\nCASE-139:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 20.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    /* Add first destination */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    /* Add second destination with different in-label */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 in-label 200010 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-140: Add second destination with same out-label and next-hop 
     * address, but different incoming interface */
    printf ("\r\nCASE-140:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 in-label 200001 vlan 4 out-label 200002 3.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-141: Add second destination with same out-label but different 
     * next-hop address */
    printf ("\r\nCASE-141:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 in-label 200001 vlan 2 out-label 200002 4.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-142: Add second destination with same in-label and out-label */
    printf ("\r\nCASE-142:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-143: Add third destination with same in-label but different 
     * out-label and next-hop. Check in-segment, XC and out-segment entries */
    printf ("\r\nCASE-143:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 102.0.0.1 in-label 200001 vlan 2 out-label 200004 4.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (1);
    if (NULL != pInSegment)
    {
        printf ("\r\nTest_TeCliP2mpStaticBind: CASE-143");
        printf ("\r\nIn-Label = %d", INSEGMENT_LABEL (pInSegment));
    }
    pOutSegment = MplsGetOutSegmentTableEntry (1);
    if (NULL != pOutSegment)
    {
        printf ("\r\nFirst Out-Label = %d", OUTSEGMENT_LABEL (pOutSegment));
    }
    pOutSegment = MplsGetOutSegmentTableEntry (2);
    if (NULL != pOutSegment)
    {
        printf ("\r\nSecond Out-Label = %d", OUTSEGMENT_LABEL (pOutSegment));
    }

    /* All test cases successfully executed */
    printf ("\r\nTest_TeCliP2mpStaticBind: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 101.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 102.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("exit;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    DeleteConf2 ();

    return OSIX_SUCCESS;
}

INT4
Test_TeCliP2mpDeleteDestination ()
{
    printf ("\r\nTest_TeCliP2mpDeleteDestination: ENTRY");

    CreateConf2 ();

    /* CASE-144: Delete destination when tunnel entry is not present */
    printf ("\r\nCASE-144:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-145: Delete destination when tunnel type is not P2MP */
    printf ("\r\nCASE-145:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 30.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-146: Delete first of the 2 destinations at ingress LSR associated 
     * to same branch */
    printf ("\r\nCASE-146:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface loopback 0");
    CliExecuteAppCmd ("shutdown");
    /* Configure 10.0.0.1 as the local loopback IP */
    CliExecuteAppCmd ("ip address 10.0.0.1 255.255.255.255");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no tunnel mpls destination 30.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 out-label 200002 2.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 102.0.0.1 out-label 200002 2.0.0.2");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 101.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-147: Delete the second destination at ingress LSR */
    printf ("\r\nCASE-147:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 102.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-148: Delete first of the 2 destinations at intermediate LSR 
     * associated to same branch */
    printf ("\r\nCASE-148:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("end");
    /* Configure 20.0.0.1 as the local loopback IP */
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface loopback 0");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("ip address 20.0.0.1 255.255.255.255");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-149: Delete the second destination at intermediate LSR */
    printf ("\r\nCASE-149:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 101.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-150: Delete destination whose address is same as local IP address 
     * for LSR configured as non-bud */
    printf ("\r\nCASE-150:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 20.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* All test cases successfully executed */
    printf ("\r\nTest_TeCliP2mpDeleteDestination: All test cases passed");

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("exit;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    DeleteConf2 ();

    return OSIX_SUCCESS;
}

INT4
Test_TeCliShowP2mpBranchStatistics ()
{
    printf ("\r\nTest_TeCliShowP2mpBranchStatistics: ENTRY");

    /* CASE-151: Fetch branch statistics for a tunnel that does not exist */
    printf ("\r\nCASE-151:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd
        ("show mpls traffic-eng point-to-multipoint branch statistics tunnel 10");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-152: Fetch branch statistics for a non-P2MP tunnel */
    printf ("\r\nCASE-152:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 20.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd
        ("show mpls traffic-eng point-to-multipoint branch statistics tunnel 10");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-153: Fetch branch statistics for a P2MP tunnel that does not have 
     * any destinations */
    printf ("\r\nCASE-153:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no tunnel mpls destination 20.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd
        ("show mpls traffic-eng point-to-multipoint branch statistics tunnel 10");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-154: Fetch branch statistics for a P2MP tunnel after asssociating 
     * destinations to it */
    printf ("\r\nCASE-154:");
    CreateConf2 ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 102.0.0.1 in-label 200001 vlan 2 out-label 200003 4.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 103.0.0.1 in-label 200001 vlan 2 out-label 200003 4.0.0.2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd
        ("show mpls traffic-eng point-to-multipoint branch statistics tunnel 10");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* All test cases successfully executed */
    printf ("\r\nTest_TeCliShowP2mpBranchStatistics: All test cases passed");

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 101.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 102.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 103.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("exit;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    DeleteConf2 ();

    return OSIX_SUCCESS;
}

INT4
Test_TeTestAllMplsTeP2mpTunnelTable ()
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeP2mpTnlInfo      TeP2mpTnlInfo;
    UINT4               u4ErrorCode = 0;
    INT4                i4Value = 0;

    printf ("\r\nTest_TeTestAllMplsTeP2mpTunnelTable: ENTRY");

    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));

    /*Case:1 Check when TE module is DOWN */
    TE_ADMIN_STATUS (gTeGblInfo) = TE_ADMIN_DOWN;
    if (nmhTestv2MplsTeP2mpTunnelP2mpIntegrity
        (&u4ErrorCode, 0, 0, 0, 0, i4Value) == SNMP_SUCCESS)
    {
        printf ("\r\nFailed for TE module down case");
        return OSIX_FAILURE;
    }
    TE_ADMIN_STATUS (gTeGblInfo) = TE_ADMIN_UP;
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        printf ("\r\nFailed for TE module down case error code");
        return OSIX_FAILURE;
    }

    /*Case:2 Check when the tunnel entry is not present */
    if (nmhTestv2MplsTeP2mpTunnelP2mpIntegrity
        (&u4ErrorCode, 10, 0, 0, 0, i4Value) == SNMP_SUCCESS)
    {
        printf ("\r\nFailed for tunnel entry not present case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        printf ("\r\nFailed for tunnel entry not present case errorcode");
        return OSIX_FAILURE;
    }

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:3 Check when tunnel is present but not P2MP */
    if (nmhTestv2MplsTeP2mpTunnelP2mpIntegrity
        (&u4ErrorCode, 10, 1, 5, 6, i4Value) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for not p2mp tunnel case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for not p2mp tunnel case errorcode");
        return OSIX_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }
    TE_TNL_ROW_STATUS (pTeTnlInfo) = ACTIVE;

    /*Case:4 Check when tunnel is active */
    if (nmhTestv2MplsTeP2mpTunnelP2mpIntegrity
        (&u4ErrorCode, 10, 1, 5, 6, i4Value) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for p2mp tunnel active case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for p2mp tunnel active case errorcode");
        return OSIX_FAILURE;
    }
    TE_TNL_ROW_STATUS (pTeTnlInfo) = NOT_READY;
    TE_TNL_OWNER (pTeTnlInfo) = TE_TNL_OWNER_OTHER;

    /*Case:5 Check when owner is not SNMP */
    if (nmhTestv2MplsTeP2mpTunnelP2mpIntegrity
        (&u4ErrorCode, 10, 1, 5, 6, i4Value) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for p2mp tunnel owner not snmp case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for p2mp tunnel owner not snmp case errorcode");
        return OSIX_FAILURE;
    }
    TE_TNL_OWNER (pTeTnlInfo) = TE_TNL_OWNER_SNMP;

    /*Case:6 Check when row is not created for integrity */
    if (nmhTestv2MplsTeP2mpTunnelP2mpIntegrity
        (&u4ErrorCode, 10, 1, 5, 6, i4Value) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for Integrity row not created case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for Integrity set value wrong case errorcode");
        return OSIX_FAILURE;
    }

    /*Case:7 Check when row is not created for branch role */
    if (nmhTestv2MplsTeP2mpTunnelBranchRole (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for branch role row not created case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for branch role row not created case errorcode");
        return OSIX_FAILURE;
    }

    /*Case:8 Check when row is not created for storage type */
    if (nmhTestv2MplsTeP2mpTunnelStorageType
        (&u4ErrorCode, 10, 1, 5, 6, i4Value) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for storage type row not created case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for storage type row not created case errorcode");
        return OSIX_FAILURE;
    }

    /*Case:9 Check when value is wrong for row status case */
    if (nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for Integrity test case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for rowstatus wrong value case errorcode");
        return OSIX_FAILURE;
    }

    i4Value = 5;
    /*Case:10 Check when value is create wait for row status - success case */
    if (nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status create test case");
        return OSIX_FAILURE;
    }

    i4Value = 1;
    /*Case:11 Check when value is active for row status */
    if (nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status active case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_NO_CREATION)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for rowstatus active case errorcode");
        return OSIX_FAILURE;
    }

    i4Value = 2;
    /*Case:12 Check when value is not in service for row status */
    if (nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status notinservice test case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for rowstatus notinservice case errorcode");
        return OSIX_FAILURE;
    }

    i4Value = 6;
    /*Case:13 Check when value is destroy for row status */
    if (nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status destroy test case");
        return OSIX_FAILURE;
    }
    if (u4ErrorCode != SNMP_ERR_INCONSISTENT_VALUE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for rowstatus destroy value case errorcode");
        return OSIX_FAILURE;
    }

    TE_P2MP_TNL_INFO (pTeTnlInfo) = &TeP2mpTnlInfo;

    i4Value = 1;
    /*Case:14 Check for the success */
    if (nmhTestv2MplsTeP2mpTunnelP2mpIntegrity
        (&u4ErrorCode, 10, 1, 5, 6, i4Value) == SNMP_FAILURE)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for Integrity success case");
        return OSIX_FAILURE;
    }

    i4Value = 2;
    if (nmhTestv2MplsTeP2mpTunnelStorageType
        (&u4ErrorCode, 10, 1, 5, 6, i4Value) == SNMP_FAILURE)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for StorageType success case");
        return OSIX_FAILURE;
    }

    i4Value = 1;
    if (nmhTestv2MplsTeP2mpTunnelBranchRole (&u4ErrorCode, 10, 1, 5, 6, i4Value)
        == SNMP_FAILURE)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for BranchRole success case");
        return OSIX_FAILURE;
    }

    TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nsuccess for Test_TeTestAllMplsTeP2mpTunnelTable");
    return OSIX_SUCCESS;
}

INT4
Test_TeSetAllMplsTeP2mpTunnelTable ()
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeP2mpTnlInfo      TeP2mpTnlInfo;
    INT4                i4Value = 5;

    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));

    printf ("\r\nTest_TeSetAllMplsTeP2mpTunnelTable: ENTRY");

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    TE_P2MP_TNL_INFO (pTeTnlInfo) = &TeP2mpTnlInfo;
    /*Case:15 Check when tunnel info is present for row status set */
    if (nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, i4Value) == SNMP_SUCCESS)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status create-wait set case");
        return OSIX_FAILURE;
    }
    TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;

    /*Case:16 Check when value is create-wait for row status */
    if (nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, i4Value) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status create-wait fail case");
        return OSIX_FAILURE;
    }

    i4Value = 1;
    /*Case:17 Check when setting Integrity */
    if (nmhSetMplsTeP2mpTunnelP2mpIntegrity (10, 1, 5, 6, i4Value)
        == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for Integrity fail case");
        return OSIX_FAILURE;
    }

    /*Case:18 Check when setting BranchRole */
    if (nmhSetMplsTeP2mpTunnelBranchRole (10, 1, 5, 6, i4Value) == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for BranchRole fail case");
        return OSIX_FAILURE;
    }

    i4Value = 2;
    /*Case:19 Check when setting StorageType */
    if (nmhSetMplsTeP2mpTunnelStorageType (10, 1, 5, 6, i4Value)
        == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for StorageType fail case");
        return OSIX_FAILURE;
    }

    i4Value = 1;
    /*Case:20 Check when setting RowStatus active */
    if (nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, i4Value) == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status active fail case");
        return OSIX_FAILURE;
    }

    i4Value = 2;
    /*Case:21 Check when setting RowStatus not-in-service */
    if (nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, i4Value) == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status not-in-service fail case");
        return OSIX_FAILURE;
    }

    i4Value = 6;
    /*Case:22 Check when setting RowStatus active */
    if (nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, i4Value) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status active fail case");
        return OSIX_FAILURE;
    }

    nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nsuccess for Test_TeSetAllMplsTeP2mpTunnelTable");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetAllMplsTeP2mpTunnelTable ()
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    INT4                i4Value = 5;
    tSNMP_OCTET_STRING_TYPE TeP2mpTunnelP2mpXcIndex;

    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];

    TeP2mpTunnelP2mpXcIndex.pu1_OctetList = au1XCIndex;
    TeP2mpTunnelP2mpXcIndex.i4_Length = MPLS_INDEX_LENGTH;

    printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelTable: ENTRY");

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    if (nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, i4Value) == SNMP_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status create-wait fail case");
        return OSIX_FAILURE;
    }

    /*Case:23 Check when setting RowStatus active */
    if (nmhGetMplsTeP2mpTunnelP2mpIntegrity (10, 1, 5, 6, &i4Value)
        == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status active fail case");
        return OSIX_FAILURE;
    }

    /*Case:24 Check when setting RowStatus active */
    if (nmhGetMplsTeP2mpTunnelBranchRole (10, 1, 5, 6, &i4Value)
        == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status active fail case");
        return OSIX_FAILURE;
    }

    /*Case:25 Check when setting RowStatus active */
    if (nmhGetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, &i4Value) == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status active fail case");
        return OSIX_FAILURE;
    }

    /*Case:26 Check when setting RowStatus active */
    if (nmhGetMplsTeP2mpTunnelStorageType (10, 1, 5, 6, &i4Value)
        == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status active fail case");
        return OSIX_FAILURE;
    }

    /*Case:27 Check when setting RowStatus active */
    if (nmhGetMplsTeP2mpTunnelP2mpXcIndex
        (10, 1, 5, 6, &TeP2mpTunnelP2mpXcIndex) == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for row status active fail case");
        return OSIX_FAILURE;
    }

    nmhSetMplsTeP2mpTunnelRowStatus (10, 1, 5, 6, 6);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nsuccess for Test_TeGetAllMplsTeP2mpTunnelTable");
    return OSIX_SUCCESS;
}

INT4
Test_TeInitializeMplsTeP2mpTunnelTable ()
{
    tTeP2mpTnlInfo      TeP2mpTnlInfo, *pTeP2mpTnlInfo = NULL;

    printf ("\r\nTest_TeInitializeMplsTeP2mpTunnelTable: ENTRY");

    /*Case:28 Check when pointer is null for intializing p2mp tunnel */
    if (TeInitializeMplsTeP2mpTunnelTable (pTeP2mpTnlInfo) == SNMP_SUCCESS)
    {
        printf ("\r\nFailed for case 1:TeInitializeMplsTeP2mpTunnelTable");
        return OSIX_FAILURE;
    }

    /*Case:29 Check when pointer is not null for intializing p2mp tunnel */
    if (TeInitializeMplsTeP2mpTunnelTable (&TeP2mpTnlInfo) == SNMP_FAILURE)
    {
        printf ("\r\nFailed for case 2:TeInitializeMplsTeP2mpTunnelTable");
        return OSIX_FAILURE;
    }
    printf ("\r\nSuccess for Test_TeInitializeMplsTeP2mpTunnelTable");
    return OSIX_SUCCESS;
}

INT4
Test_TeAddP2mpBranchEntry ()
{
    tTeTnlInfo          TeTnlInfo, *pTeTnlInfo = NULL;
    tTeP2mpTnlInfo     *pTeP2mpTnlInfo = NULL;
    tOutSegment        *pOutSegment = NULL;
    tTeP2mpTnlInfo      TeP2mpTnlInfo;
    UINT4               u4OutIndex = 0;
    tXcEntry            XcEntry;
    tTMO_SLL_NODE       Tail;

    printf ("\r\nTest_TeAddP2mpBranchEntry: ENTRY");

    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));
    MEMSET (&XcEntry, 0, sizeof (tXcEntry));
    MEMSET (&TeTnlInfo, 0, sizeof (tTeTnlInfo));
    MEMSET (&Tail, 0, sizeof (tTMO_SLL_NODE));

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    /*Case:33 Check when p2mp tunnel entry is not present */
    if (TeAddP2mpBranchEntry (pTeTnlInfo, u4OutIndex) != NULL)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for case 1:TeAddP2mpBranchEntry");
        return OSIX_FAILURE;
    }

    TeP2mpTnlInfo.TeP2mpBranchEntry.Tail = &Tail;
    TE_P2MP_TNL_INFO (pTeTnlInfo) = &TeP2mpTnlInfo;

    /*Case:34 Check when p2mp tunnel entry is present */
    if (TeAddP2mpBranchEntry (pTeTnlInfo, u4OutIndex) != NULL)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for case 2:TeAddP2mpBranchEntry");
        return OSIX_FAILURE;
    }

    pTeP2mpTnlInfo = TE_P2MP_TNL_INFO (pTeTnlInfo);
    TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo) = TE_P2MP_NOT_BRANCH;

    pOutSegment = MplsGetOutSegmentTableEntry (10);
    if (pOutSegment != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
    }
    pOutSegment = MplsCreateOutSegmentTableEntry (10);

    if (pOutSegment == NULL)
    {
        printf ("Failed for out segment creation\r\n");
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        return OSIX_FAILURE;
    }

    /*Case:35 Check when p2mp tunnel entry is present */
    if (TeAddP2mpBranchEntry (pTeTnlInfo, 10) != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for case 3:TeAddP2mpBranchEntry");
        return OSIX_FAILURE;
    }

    pOutSegment->pXcIndex = &XcEntry;
    /*Case:35 Check when p2mp tunnel entry is present */
    if (TeAddP2mpBranchEntry (pTeTnlInfo, 10) == NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for case 4:TeAddP2mpBranchEntry");
        return OSIX_FAILURE;
    }

    TeRemoveP2mpBranchEntry (pTeTnlInfo, 10);
    MplsDeleteOutSegmentTableEntry (pOutSegment);
    TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nSuccess for Test_TeAddP2mpBranchEntry");
    return OSIX_SUCCESS;
}

INT4
Test_TeRemoveP2mpBranchEntry ()
{
    tTeTnlInfo          TeTnlInfo, *pTeTnlInfo = NULL;
    tTeP2mpTnlInfo     *pTeP2mpTnlInfo = NULL;
    tOutSegment        *pOutSegment = NULL;
    tTeP2mpTnlInfo      TeP2mpTnlInfo;
    UINT4               u4OutIndex = 10;
    tXcEntry            XcEntry;
    tTMO_SLL_NODE       Tail;

    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));
    MEMSET (&XcEntry, 0, sizeof (tXcEntry));
    MEMSET (&TeTnlInfo, 0, sizeof (tTeTnlInfo));
    MEMSET (&Tail, 0, sizeof (tTMO_SLL_NODE));

    printf ("\r\nTest_TeRemoveP2mpBranchEntry: ENTRY");

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    /*Case:39 Check when pointer is null for delete branch entry */
    if (TeRemoveP2mpBranchEntry (pTeTnlInfo, u4OutIndex) != TE_FAILURE)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for case 1:TeRemoveP2mpBranchEntry");
        return OSIX_FAILURE;
    }

    TeP2mpTnlInfo.TeP2mpBranchEntry.Tail = &Tail;
    TE_P2MP_TNL_INFO (pTeTnlInfo) = &TeP2mpTnlInfo;
    pTeP2mpTnlInfo = TE_P2MP_TNL_INFO (pTeTnlInfo);
    TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo) = TE_P2MP_NOT_BRANCH;

    /*Case:40 Check when pointer is not null for delete branch entry */
    if (TeRemoveP2mpBranchEntry (pTeTnlInfo, u4OutIndex) != TE_FAILURE)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for case 2:TeRemoveP2mpBranchEntry");
        return OSIX_FAILURE;
    }

    pOutSegment = MplsGetOutSegmentTableEntry (10);
    if (pOutSegment != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
    }

    pOutSegment = MplsCreateOutSegmentTableEntry (10);

    /*Case:41 Check when removing branch entry */
    if (TeRemoveP2mpBranchEntry (pTeTnlInfo, u4OutIndex) != TE_FAILURE)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for case 3:TeRemoveP2mpBranchEntry");
        return OSIX_FAILURE;
    }
    TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
    MplsDeleteOutSegmentTableEntry (pOutSegment);

    /*Case:41 Check when removing branch entry */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 5.0.0.0");
    CliExecuteAppCmd
        ("t m static point-to-multipoint destination 30.1.1.1 in-label 200010 vlan 2 out-label 200003 200.0.0.2");
    CliExecuteAppCmd
        ("no t m static point-to-multipoint destination 30.1.1.1;exit;no i mp 10");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    printf ("\r\nSuccess for Test_TeRemoveP2mpBranchEntry");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetAllMplsTeP2mpTunnelBranchPerfTable ()
{
    tOutSegment        *pOutSegment = NULL;
    tSNMP_OCTET_STRING_TYPE P2mpBranch;
    static UINT1        au1P2mpBranch[MPLS_INDEX_LENGTH] = { 1, 1, 1, 1 };
    UINT4               u4Value = 0;

    printf ("\r\nTest_TeGetAllMplsTeP2mpTunnelBranchPerfTable: ENTRY");

    P2mpBranch.pu1_OctetList = au1P2mpBranch;
    P2mpBranch.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1P2mpBranch, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (20, (&P2mpBranch));

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:60 Check when out segment not present */
    if (nmhGetMplsTeP2mpTunnelBranchPerfPackets
        (10, 1, 5, 6, &P2mpBranch, &u4Value) == SNMP_SUCCESS)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\n1.Failed for outsegment not present case");
        return OSIX_FAILURE;
    }

    /*Case:61 Check when out segment present */
    MplsCreateOutSegmentTableEntry (20);
    if (nmhGetMplsTeP2mpTunnelBranchPerfPackets
        (10, 1, 5, 6, &P2mpBranch, &u4Value) == SNMP_FAILURE)
    {
        pOutSegment = MplsGetOutSegmentTableEntry (1);
        if (pOutSegment != NULL)
        {
            MplsDeleteOutSegmentTableEntry (pOutSegment);
        }
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\n2.Failed for outsegment present case");
        return OSIX_FAILURE;
    }

    pOutSegment = MplsGetOutSegmentTableEntry (20);
    if (pOutSegment != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
    }
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nSuccess for Test_TeGetAllMplsTeP2mpTunnelBranchPerfTable");
    return OSIX_SUCCESS;
}

INT4
Test_TeCliP2mpStaticBindEgress ()
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    INT4                i4TnlRole = 0;
    tTeP2mpTnlInfo      TeP2mpTnlInfo;

    printf ("\r\nTest_TeCliP2mpStaticBindEgress: ENTRY");
    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    /*Case:62 Check when tunnel not exists */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination egress in-label 200001 vlan 2");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    /*Case:63 Check when tunnel is not p2mp */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination egress in-label 200001 vlan 2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
        TE_P2MP_TNL_INFO (pTeTnlInfo) = &TeP2mpTnlInfo;
        TE_TNL_ROW_STATUS (pTeTnlInfo) = TE_ACTIVE;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    /*Case:64 Check when tunnel is active */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination egress in-label 200001 vlan 2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    TE_TNL_ROW_STATUS (pTeTnlInfo) = TE_NOTREADY;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    /*Case:65 Check when tunnel is not active and success case */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination egress in-label 200001 vlan 2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (nmhGetMplsTunnelRole (10, 1, 5, 6, &i4TnlRole) != SNMP_SUCCESS)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for nmhGetMplsTunnelRole");
        return OSIX_FAILURE;
    }
    /*Case:66 Check whether the tunnel role is egress */
    if (i4TnlRole != TE_EGRESS)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed because tunnel role is not egress");
        return OSIX_FAILURE;
    }

    TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nSuccess for Test_TeCliP2mpStaticBindEgress");
    return OSIX_SUCCESS;
}

INT4
Test_TeCliP2mpConfigureBudNode ()
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeP2mpTnlInfo      TeP2mpTnlInfo, *pTeP2mpTnlInfo = NULL;
    INT4                i4TnlRole = 0;

    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));

    printf ("\r\nTest_TeCliP2mpConfigureBudNode: ENTRY");

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    /*Case:67 Check when tunnel not exists */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    /*Case:68 Check when tunnel is not p2mp */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
        TE_P2MP_TNL_INFO (pTeTnlInfo) = &TeP2mpTnlInfo;
        TE_TNL_ROLE (pTeTnlInfo) = TE_EGRESS;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    /*Case:69 Check when tunnel role is egress */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    TE_TNL_ROLE (pTeTnlInfo) = TE_INGRESS;
    pTeP2mpTnlInfo = TE_P2MP_TNL_INFO (pTeTnlInfo);
    TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo) = TE_P2MP_BUD;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    /*Case:70 Check when tunnel branch role is already bud */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo) = TE_P2MP_NOT_BRANCH;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    /*Case:71 Check when tunnel branch role is not bud */
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (nmhGetMplsTeP2mpTunnelBranchRole (10, 1, 5, 6, &i4TnlRole) !=
        SNMP_SUCCESS)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for nmhGetMplsTeP2mpTunnelBranchRole");
        return OSIX_FAILURE;
    }
    /*Case:72 Check whether the tunnel branch role is bud */
    if (i4TnlRole != TE_P2MP_BUD)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed because tunnel branch role is not bud");
        return OSIX_FAILURE;
    }

    TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nSuccess for Test_TeCliP2mpConfigureBudNode");
    return OSIX_SUCCESS;
}

INT4
Test_MplsP2mpILMHwAdd ()
{
    tOutSegment         OutSegment, *pOutSegment = NULL;
    tTeTnlInfo          TeTnlInfo;
    tInSegment          InIndex;
    tXcEntry            XcEntry;

    printf ("\r\nTest_MplsP2mpILMHwAdd: ENTRY");

    MEMSET (&OutSegment, 0, sizeof (tOutSegment));
    MEMSET (&InIndex, 0, sizeof (tInSegment));
    MEMSET (&XcEntry, 0, sizeof (tXcEntry));
    MEMSET (&TeTnlInfo, 0, sizeof (tTeTnlInfo));

    /*Case:73 Check when the out segment is not exists */
    if (MplsP2mpILMHwAdd (pOutSegment, NULL) != MPLS_FAILURE)
    {
        printf ("\r\nCase1: Failed for out segment not exists");
        return OSIX_FAILURE;
    }

    pOutSegment = &OutSegment;
    /*Case:74 Check when the XC entry is not exists */
    if (MplsP2mpILMHwAdd (pOutSegment, NULL) != MPLS_FAILURE)
    {
        printf ("\r\nCase2: Failed for XC entry not exists");
        return OSIX_FAILURE;
    }

    pOutSegment->pXcIndex = &XcEntry;
    /*Case:75 Check when tunnel is not associated to out segment */
    if (MplsP2mpILMHwAdd (pOutSegment, NULL) != MPLS_FAILURE)
    {
        printf ("\r\nCase3: Failed for tunnel not associated with out segment");
        return OSIX_FAILURE;
    }

    XcEntry.pTeTnlInfo = &TeTnlInfo;
    pOutSegment->pXcIndex = &XcEntry;
    /*Case:76 Check when tunnel is associated to the out segment */
    if (MplsP2mpILMHwAdd (pOutSegment, NULL) != MPLS_FAILURE)
    {
        printf ("\r\nCase4: Failed for tunnel associated with out segment");
        return OSIX_FAILURE;
    }

    XcEntry.pInIndex = &InIndex;
    pOutSegment->pXcIndex = &XcEntry;
    pOutSegment->u1HwStatus = MPLS_TRUE;
    /*Case:77 Check when tunnel has both in segment and out segment */
    if (MplsP2mpILMHwAdd (pOutSegment, NULL) == MPLS_FAILURE)
    {
        printf ("\r\nCase5: Failed for tunnel with in segment and out segment");
        return OSIX_FAILURE;
    }

    printf ("\r\nSuccess for Test_MplsP2mpILMHwAdd");
    return OSIX_SUCCESS;
}

INT4
Test_MplsP2mpILMHwDel ()
{
    tOutSegment         OutSegment, *pOutSegment = NULL;
    tTeTnlInfo          TeTnlInfo;
    tInSegment          InIndex;
    tXcEntry            XcEntry;

    printf ("\r\nTest_MplsP2mpILMHwDel: ENTRY");

    MEMSET (&OutSegment, 0, sizeof (tOutSegment));
    MEMSET (&InIndex, 0, sizeof (tInSegment));
    MEMSET (&XcEntry, 0, sizeof (tXcEntry));

    /*Case:78 Check when the out segment is not exists */
    if (MplsP2mpILMHwDel (pOutSegment, NULL) != MPLS_FAILURE)
    {
        printf ("\r\nCase1: Failed for out segment not exists");
        return OSIX_FAILURE;
    }

    pOutSegment = &OutSegment;
    /*Case:79 Check when the XC entry is not exists */
    if (MplsP2mpILMHwDel (pOutSegment, NULL) != MPLS_FAILURE)
    {
        printf ("\r\nCase2: Failed for XC entry not exists");
        return OSIX_FAILURE;
    }

    pOutSegment->pXcIndex = &XcEntry;
    /*Case:80 Check when in segment does not exists */
    if (MplsP2mpILMHwDel (pOutSegment, NULL) != MPLS_FAILURE)
    {
        printf ("\r\nCase3: Failed for in segment not present");
        return OSIX_FAILURE;
    }

    XcEntry.pTeTnlInfo = &TeTnlInfo;
    pOutSegment->pXcIndex = &XcEntry;
    /*Case:81 Check when tunnel is associated to the out segment */
    if (MplsP2mpILMHwDel (pOutSegment, NULL) != MPLS_FAILURE)
    {
        printf ("\r\nCase4: Failed for tunnel associated with out segment");
        return OSIX_FAILURE;
    }

    XcEntry.pInIndex = &InIndex;
    pOutSegment->pXcIndex = &XcEntry;
    pOutSegment->u1HwStatus = MPLS_FALSE;
    /*Case:81 Check when both in segment and out segment exists */
    if (MplsP2mpILMHwDel (pOutSegment, NULL) == MPLS_FAILURE)
    {
        printf ("\r\nCase5: Failed for when in segment and out segment exists");
        return OSIX_FAILURE;
    }

    printf ("\r\nSuccess for Test_MplsP2mpILMHwDel");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetFirstMplsTeP2mpTunnelTable ()
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeP2mpTnlInfo      TeP2mpTnlInfo;
    INT4                i4Retval = 0;

    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));
    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));

    printf ("\r\nTest_TeGetFirstMplsTeP2mpTunnelTable: ENTRY");

    /*Case:87 Check when p2mp tunnel not exists */
    i4Retval = TeGetFirstMplsTeP2mpTunnelTable (&TeMplsTeP2mpTunnelTable);
    if (SNMP_FAILURE != i4Retval)
    {
        printf ("\r\nCase1: Failed for tunnel not exists case");
        return OSIX_FAILURE;
    }

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:88 Check when tunnel is not p2mp */
    i4Retval = TeGetFirstMplsTeP2mpTunnelTable (&TeMplsTeP2mpTunnelTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase2: Failed for tunnel is not p2mp case");
        return OSIX_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
        TE_P2MP_TNL_INFO (pTeTnlInfo) = &TeP2mpTnlInfo;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    /*Case:89 Check when tunnel is p2mp */
    i4Retval = TeGetFirstMplsTeP2mpTunnelTable (&TeMplsTeP2mpTunnelTable);
    if (SNMP_FAILURE == i4Retval)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase3: Failed for tunnel is p2mp case");
        return OSIX_FAILURE;
    }

    TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nSuccess for Test_TeGetFirstMplsTeP2mpTunnelTable");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetNextMplsTeP2mpTunnelTable ()
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeMplsTeP2mpTunnelTable TeOldMplsTeP2mpTunnelTable;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeP2mpTnlInfo      TeP2mpTnlInfo, TeNextP2mpTnlInfo;
    INT4                i4Retval = 0;

    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));
    MEMSET (&TeNextP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));
    MEMSET (&TeOldMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));

    printf ("\r\nTest_TeGetNextMplsTeP2mpTunnelTable: ENTRY");

    /*Case:90 Check when tunnel not exists */
    i4Retval = TeGetNextMplsTeP2mpTunnelTable
        (&TeOldMplsTeP2mpTunnelTable, &TeMplsTeP2mpTunnelTable);
    if (SNMP_FAILURE != i4Retval)
    {
        printf ("\r\nCase1: Failed for tunnel not exists case");
        return OSIX_FAILURE;
    }

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5;exit");
    CliExecuteAppCmd ("interface mplstunnel 20");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    TeOldMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = 10;
    TeOldMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance = 1;
    TeOldMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId = 5;
    TeOldMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId = 6;

    /*Case:91 Check when tunnel is not p2mp */
    i4Retval = TeGetNextMplsTeP2mpTunnelTable
        (&TeOldMplsTeP2mpTunnelTable, &TeMplsTeP2mpTunnelTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd
            ("c t;no interface mplstunnel 10;no interface mplstunnel 20;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase2: Failed for tunnel is not p2mp case");
        return OSIX_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
        TE_P2MP_TNL_INFO (pTeTnlInfo) = &TeP2mpTnlInfo;
    }
    else
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd
            ("c t;no interface mplstunnel 10;no interface mplstunnel 20;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    /*Case:92 Check when tunnel is p2mp */
    i4Retval = TeGetNextMplsTeP2mpTunnelTable
        (&TeOldMplsTeP2mpTunnelTable, &TeMplsTeP2mpTunnelTable);
    if (SNMP_FAILURE != i4Retval)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd
            ("c t;no interface mplstunnel 10;no interface mplstunnel 20;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase3: Failed for tunnel is p2mp case");
        return OSIX_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (20, 1, 5, 6);
    if (NULL != pTeTnlInfo)
    {
        TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_P2MP;
        TE_P2MP_TNL_INFO (pTeTnlInfo) = &TeNextP2mpTnlInfo;
    }
    else
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd
            ("c t;no interface mplstunnel 10;no interface mplstunnel 20;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nFailed for tunnel info not present");
        return OSIX_FAILURE;
    }

    /*Case:93 Check when next tunnel is p2mp */
    i4Retval = TeGetNextMplsTeP2mpTunnelTable
        (&TeOldMplsTeP2mpTunnelTable, &TeMplsTeP2mpTunnelTable);
    if (SNMP_FAILURE == i4Retval)
    {
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
        if (NULL != pTeTnlInfo)
            TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd
            ("c t;no interface mplstunnel 10;no interface mplstunnel 20;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase4: Failed for next tunnel is p2mp case");
        return OSIX_FAILURE;
    }

    TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
    pTeTnlInfo = TeGetTunnelInfo (10, 1, 5, 6);
    if (NULL != pTeTnlInfo)
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd
        ("c t;no interface mplstunnel 10;no interface mplstunnel 20;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nSuccess for Test_TeGetNextMplsTeP2mpTunnelTable");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetFirstMplsTeP2mpTunnelDestTable ()
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeP2mpTnlInfo      TeP2mpTnlInfo;
    INT4                i4Retval = 0;

    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));
    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    printf ("\r\nTest_TeGetFirstMplsTeP2mpTunnelDestTable: ENTRY");

    /*Case:94 Check when tunnel not exists */
    i4Retval =
        TeGetFirstMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable);
    if (SNMP_FAILURE != i4Retval)
    {
        printf ("\r\nCase1: Failed for tunnel not exists case");
        return OSIX_FAILURE;
    }

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:95 Check when tunnel is not p2mp */
    i4Retval =
        TeGetFirstMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase2: Failed for tunnel is not p2mp case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("no interface mplstunnel 10");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 5.0.0.0");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:96 Check when tunnel is p2mp but no destination */
    i4Retval =
        TeGetFirstMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase3: Failed for tunnel is p2mp but no destination case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 30.0.0.1 in-label 200010 vlan 3 out-label 200040 100.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 40.0.0.1 in-label 200010 vlan 3 out-label 200040 100.0.0.2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:97 Check when tunnel is p2mp with destination */
    i4Retval =
        TeGetFirstMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable);
    if (SNMP_FAILURE == i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase4: Failed for tunnel is p2mp with destination case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 40.0.0.1;end");
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nSuccess for Test_TeGetFirstMplsTeP2mpTunnelDestTable");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetNextMplsTeP2mpTunnelDestTable ()
{
    tTeP2mpTnlInfo      TeP2mpTnlInfo;
    tTeMplsTeP2mpTunnelDestTable TeOldMplsTeP2mpTunnelDestTable;
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    INT4                i4Retval = 0;

    MEMSET (&TeOldMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));

    printf ("\r\nTest_TeGetNextMplsTeP2mpTunnelDestTable: ENTRY");

    /*Case:98 Check when tunnel not exists */
    i4Retval =
        TeGetNextMplsTeP2mpTunnelDestTable (&TeOldMplsTeP2mpTunnelDestTable,
                                            &TeMplsTeP2mpTunnelDestTable);
    if (SNMP_FAILURE != i4Retval)
    {
        printf ("\r\nCase1: Failed for tunnel not exists case");
        return OSIX_FAILURE;
    }

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    TeOldMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = 10;
    TeOldMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance = 1;
    TeOldMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        TE_HTONL (5);
    TeOldMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        TE_HTONL (6);

    /*Case:99 Check when tunnel is not p2mp */
    i4Retval =
        TeGetNextMplsTeP2mpTunnelDestTable (&TeOldMplsTeP2mpTunnelDestTable,
                                            &TeMplsTeP2mpTunnelDestTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase2: Failed for tunnel is not p2mp case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("no interface mplstunnel 10");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 5.0.0.0");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    TeOldMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        TE_HTONL (0x5000000);

    /*Case:100 Check when tunnel is p2mp with no destination */
    i4Retval =
        TeGetNextMplsTeP2mpTunnelDestTable (&TeOldMplsTeP2mpTunnelDestTable,
                                            &TeMplsTeP2mpTunnelDestTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf
            ("\r\nCase3: Failed for tunnel is p2mp without destination case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 30.0.0.1 in-label 200010 vlan 3 out-label 200040 100.0.0.2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:101 Check when tunnel is p2mp with destination but next destination not exists */
    i4Retval =
        TeGetNextMplsTeP2mpTunnelDestTable (&TeOldMplsTeP2mpTunnelDestTable,
                                            &TeMplsTeP2mpTunnelDestTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase4: Failed for tunnel is p2mp with destination case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 40.0.0.1 in-label 200010 vlan 3 out-label 200040 100.0.0.2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    i4Retval =
        TeGetFirstMplsTeP2mpTunnelDestTable (&TeOldMplsTeP2mpTunnelDestTable);
    /*Case:102 Check when tunnel is p2mp with destination */
    i4Retval =
        TeGetNextMplsTeP2mpTunnelDestTable (&TeOldMplsTeP2mpTunnelDestTable,
                                            &TeMplsTeP2mpTunnelDestTable);
    if (SNMP_FAILURE == i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase5: Failed for tunnel is p2mp with correct case");
        return OSIX_FAILURE;

    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 40.0.0.1;end");
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nSuccess for Test_TeGetNextMplsTeP2mpTunnelDestTable");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetFirstMplsTeP2mpTunnelBranchPerfTable ()
{
    tTeMplsTeP2mpTunnelBranchPerfTable TeMplsTeP2mpTunnelBranchTable;
    tTeP2mpTnlInfo      TeP2mpTnlInfo;
    INT4                i4Retval = 0;

    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));
    MEMSET (&TeMplsTeP2mpTunnelBranchTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));

    printf ("\r\nTest_TeGetFirstMplsTeP2mpTunnelBranchPerfTable: ENTRY");

    /*Case:103 Check when tunnel not exists */
    i4Retval =
        TeGetFirstMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchTable);
    if (SNMP_FAILURE != i4Retval)
    {
        printf ("\r\nCase1: Failed for tunnel not exists case");
        return OSIX_FAILURE;
    }

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:104 Check when tunnel is not p2mp */
    i4Retval =
        TeGetFirstMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase2: Failed for tunnel is not p2mp case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("no interface mplstunnel 10");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 5.0.0.0");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:105 Check when tunnel is p2mp but no destination */
    i4Retval =
        TeGetFirstMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase3: Failed for tunnel is p2mp but no destination case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 40.0.0.1 in-label 200010 vlan 3 out-label 200040 100.0.0.2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:106 Check when tunnel is p2mp with destination */
    i4Retval =
        TeGetFirstMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchTable);
    if (SNMP_FAILURE == i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase4: Failed for tunnel is p2mp with destination case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 40.0.0.1;end");
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nSuccess for Test_TeGetFirstMplsTeP2mpTunnelBranchPerfTable");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetNextMplsTeP2mpTunnelBranchPerfTable ()
{
    tTeP2mpTnlInfo      TeP2mpTnlInfo;
    tTeMplsTeP2mpTunnelBranchPerfTable TeNewMplsTeP2mpTunnelBranchTable;
    tTeMplsTeP2mpTunnelBranchPerfTable TeMplsTeP2mpTunnelBranchTable;
    INT4                i4Retval = 0;

    MEMSET (&TeMplsTeP2mpTunnelBranchTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));
    MEMSET (&TeNewMplsTeP2mpTunnelBranchTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));
    MEMSET (&TeP2mpTnlInfo, 0, sizeof (tTeP2mpTnlInfo));

    printf ("\r\nTest_TeGetNextMplsTeP2mpTunnelBranchPerfTable: ENTRY");

    /*Case:107 Check when tunnel not exists */
    i4Retval =
        TeGetNextMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchTable, &TeNewMplsTeP2mpTunnelBranchTable);
    if (SNMP_FAILURE != i4Retval)
    {
        printf ("\r\nCase1: Failed for tunnel not exists case");
        return OSIX_FAILURE;
    }

    /*Input */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 6 source 5");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    TeMplsTeP2mpTunnelBranchTable.MibObject.u4MplsTunnelIndex = 10;
    TeMplsTeP2mpTunnelBranchTable.MibObject.u4MplsTunnelInstance = 1;
    TeMplsTeP2mpTunnelBranchTable.MibObject.u4MplsTunnelIngressLSRId =
        TE_HTONL (5);
    TeMplsTeP2mpTunnelBranchTable.MibObject.u4MplsTunnelEgressLSRId =
        TE_HTONL (6);

    /*Case:108 Check when tunnel is not p2mp */
    i4Retval =
        TeGetNextMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchTable, &TeNewMplsTeP2mpTunnelBranchTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase2: Failed for tunnel is not p2mp case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t ");
    CliExecuteAppCmd ("no interface mplstunnel 10");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 5.0.0.0");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:109 Check when tunnel is p2mp with no branch */
    i4Retval =
        TeGetNextMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchTable, &TeNewMplsTeP2mpTunnelBranchTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase3: Failed for tunnel is p2mp with no branch case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 30.0.0.1 in-label 200010 vlan 3 out-label 200040 100.0.0.2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Case:110 Check when tunnel is p2mp with branch but next destination not exists */
    i4Retval =
        TeGetNextMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchTable, &TeNewMplsTeP2mpTunnelBranchTable);
    if (SNMP_FAILURE != i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase4: Failed for tunnel is p2mp with branch case");
        return OSIX_FAILURE;
    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 40.0.0.1 in-label 200010 vlan 3 out-label 200050 50.0.0.2;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    i4Retval =
        TeGetFirstMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchTable);
    /*Case:111 Check when tunnel is p2mp with destination */
    i4Retval =
        TeGetNextMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchTable, &TeNewMplsTeP2mpTunnelBranchTable);
    if (SNMP_FAILURE == i4Retval)
    {
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("c t;interface mplstunnel 10");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
        CliExecuteAppCmd
            ("no tunnel mpls static point-to-multipoint destination 40.0.0.1;end");
        CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
        CliGiveAppContext ();
        MGMT_LOCK ();
        printf ("\r\nCase5: Failed for tunnel is p2mp success case");
        return OSIX_FAILURE;

    }

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 30.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 40.0.0.1;end");
    CliExecuteAppCmd ("c t;no interface mplstunnel 10;exit");
    CliGiveAppContext ();
    MGMT_LOCK ();
    printf ("\r\nSuccess for Test_TeGetNextMplsTeP2mpTunnelBranchPerfTable");
    return OSIX_SUCCESS;
}

INT4
Test_nmhValidateIndexInstanceMplsTeP2mpTunnelTable ()
{
    printf ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelTable: ENTRY");

    /*Case:82 Check when egress id is zero */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelTable (10, 1, 5, 0)
        == SNMP_SUCCESS)
    {
        printf ("\r\n1.Failed for validate p2mp tunnel table");
        return OSIX_FAILURE;
    }

    /*Case:83 Check when ingress id is zero */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelTable (10, 1, 0, 6)
        == SNMP_SUCCESS)
    {
        printf ("\r\n2.Failed for validate p2mp tunnel table");
        return OSIX_FAILURE;
    }

    /*Case:84 Check when all indices are correct */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelTable (10, 1, 5, 6)
        == SNMP_FAILURE)
    {
        printf ("\r\n3.Failed for validate p2mp tunnel table");
        return OSIX_FAILURE;
    }

    printf
        ("\r\nSuccess for Test_nmhValidateIndexInstanceMplsTeP2mpTunnelTable");
    return OSIX_SUCCESS;
}

INT4
Test_nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable ()
{
    tSNMP_OCTET_STRING_TYPE P2mpBranch;
    static UINT1        au1P2mpBranch[MPLS_INDEX_LENGTH];
    UINT4               u4Index = 0;

    printf
        ("\r\nTest_nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable: ENTRY");

    P2mpBranch.pu1_OctetList = au1P2mpBranch;
    P2mpBranch.i4_Length = MPLS_INDEX_LENGTH;
    MEMSET (au1P2mpBranch, 0, MPLS_INDEX_LENGTH);
    MPLS_INTEGER_TO_OCTETSTRING (u4Index, (&P2mpBranch));

    /*Case:85 Check when all indices are correct */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable (10, 1, 5, 6,
                                                                 &P2mpBranch) ==
        SNMP_SUCCESS)
    {
        printf ("\r\n1.Failed for validate p2mp branch perf table");
        return OSIX_FAILURE;
    }

    u4Index = 1;
    MPLS_INTEGER_TO_OCTETSTRING (u4Index, (&P2mpBranch));
    /*Case:86 Check when all indices are correct */
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable (10, 1, 5, 6,
                                                                 &P2mpBranch) ==
        SNMP_FAILURE)
    {
        printf ("\r\n2.Failed for validate p2mp branch perf table");
        return OSIX_FAILURE;
    }

    printf
        ("\r\nSuccess for Test_nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable");
    return OSIX_SUCCESS;
}

INT4
Test_MplsCheckP2mpILMEntry ()
{
#ifdef NPAPI_WANTED
    tOutSegment        *pOutSegment = NULL;
    UINT4               u4OutIndex = 1;
    UINT4               u4OutLabel = 1;

    printf ("\r\nTest_MplsCheckP2mpILMEntry: ENTRY");

    /*Case:58 Check when out segment not present case */
    if ((MplsCheckP2mpILMEntry (u4OutLabel)) != NULL)
    {
        printf ("\r\n1.Failed for MplsCheckP2mpILMEntry");
        return OSIX_FAILURE;
    }

    MplsCreateOutSegmentTableEntry (u4OutIndex);
    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    if (pOutSegment == NULL)
    {
        printf ("\r\n3.Failed for MplsCheckP2mpILMEntry");
        return OSIX_FAILURE;
    }
    pOutSegment->u4Label = u4OutLabel;
    /*Case:59 Check when out segment present case */
    if (MplsCheckP2mpILMEntry (u4OutLabel) == NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
        printf ("\r\n2.Failed for MplsCheckP2mpILMEntry");
        return OSIX_FAILURE;
    }
    MplsDeleteOutSegmentTableEntry (pOutSegment);
#endif

    printf ("\r\nSuccess for Test_MplsCheckP2mpILMEntry");
    return OSIX_SUCCESS;
}

INT4
Test_MplsGetOutSegmentPerfStats ()
{
    tOutSegment        *pOutSegment = NULL;
    UINT4               u4OutIndex = 7;

    printf ("\r\nTest_MplsGetOutSegmentPerfStats: ENTRY");

    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    if (pOutSegment != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
    }
    /*Case:56 Check when out segment not present */
    if (MplsGetOutSegmentPerfStats (u4OutIndex) == MPLS_SUCCESS)
    {
        printf ("\r\n1.Failed for MplsGetOutSegmentPerfStats");
        return OSIX_FAILURE;
    }

    MplsCreateOutSegmentTableEntry (u4OutIndex);
    /*Case:57 Check when out segment present */
    if (MplsGetOutSegmentPerfStats (u4OutIndex) == MPLS_FAILURE)
    {
        pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
        if (pOutSegment != NULL)
        {
            MplsDeleteOutSegmentTableEntry (pOutSegment);
        }
        printf ("\r\n2.Failed for MplsGetOutSegmentPerfStats");
        return OSIX_FAILURE;
    }

    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    if (pOutSegment != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
    }
    printf ("\r\nSuccess for Test_MplsGetOutSegmentPerfStats");
    return OSIX_SUCCESS;
}

INT4
Test_MplsCheckXCForP2mp ()
{
    tXcEntry            XcEntry, *pXcEntry = NULL;
    tTeTnlInfo          TeTnlInfo;

    printf ("\r\nTest_MplsCheckXCForP2mp: ENTRY");

    MEMSET (&XcEntry, 0, sizeof (tXcEntry));
    MEMSET (&TeTnlInfo, 0, sizeof (tTeTnlInfo));

    pXcEntry = &XcEntry;
    /*Case:42 Check when out segment tunnel pointer is null */
    if (MplsCheckXCForP2mp (pXcEntry) == MPLS_SUCCESS)
    {
        printf ("\r\n1.Failed for MplsCheckXCForP2mp");
        return OSIX_FAILURE;
    }

    /*Case:43 Check when out segment is associated to non-p2mp tunnel */
    XC_TNL_TBL_PTR (pXcEntry) = &TeTnlInfo;
    if (MplsCheckXCForP2mp (pXcEntry) == MPLS_SUCCESS)
    {
        printf ("\r\n2.Failed for MplsCheckXCForP2mp");
        return OSIX_FAILURE;
    }

    /*Case:44 Check when out segment is associated to p2mp tunnel */
    TeTnlInfo.u4TnlType = TE_TNL_TYPE_P2MP;
    XC_TNL_TBL_PTR (pXcEntry) = &TeTnlInfo;
    if (MplsCheckXCForP2mp (pXcEntry) == MPLS_FAILURE)
    {
        printf ("\r\n3.Failed for MplsCheckXCForP2mp");
        return OSIX_FAILURE;
    }

    printf ("\r\nSuccess for Test_MplsCheckXCForP2mp");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetMplsTeP2mpTunnelNotificationEnable ()
{
    INT4                i4Value = 0;

    printf ("\r\nTest_TeGetMplsTeP2mpTunnelNotificationEnable: ENTRY");

    /*Case:45 Check get value of notification of p2mp */
    if (nmhGetMplsTeP2mpTunnelNotificationEnable (&i4Value) == SNMP_FAILURE)
    {
        printf ("\r\nFailed for TeGetMplsTeP2mpTunnelNotificationEnable");
        return OSIX_FAILURE;
    }

    printf ("\r\nSuccess for Test_TeGetMplsTeP2mpTunnelNotificationEnable");
    return OSIX_SUCCESS;
}

INT4
Test_TeSetMplsTeP2mpTunnelNotificationEnable ()
{
    INT4                i4Value = 1;

    printf ("\r\nTest_TeSetMplsTeP2mpTunnelNotificationEnable: ENTRY");

    /*Case:46 Check true value setting of notification of p2mp */
    if (nmhSetMplsTeP2mpTunnelNotificationEnable (i4Value) == SNMP_FAILURE)
    {
        printf ("\r\n1:Failed for TeSetMplsTeP2mpTunnelNotificationEnable");
        return OSIX_FAILURE;
    }

    /*Case:47 Check false value setting of notification of p2mp */
    i4Value = 2;
    if (nmhSetMplsTeP2mpTunnelNotificationEnable (i4Value) == SNMP_FAILURE)
    {
        nmhSetMplsTeP2mpTunnelNotificationEnable (2);
        printf ("\r\n2:Failed for TeSetMplsTeP2mpTunnelNotificationEnable");
        return OSIX_FAILURE;
    }

    /*Case:48 Check wrong value setting of notification of p2mp */
    i4Value = 3;
    if (nmhSetMplsTeP2mpTunnelNotificationEnable (i4Value) == SNMP_SUCCESS)
    {
        printf ("\r\n3:Failed for TeSetMplsTeP2mpTunnelNotificationEnable");
        return OSIX_FAILURE;
    }

    printf ("\r\nSuccess for Test_TeSetMplsTeP2mpTunnelNotificationEnable");
    return OSIX_SUCCESS;
}

INT4
Test_TeTestMplsTeP2mpTunnelNotificationEnable ()
{
    INT4                i4Value = 0;
    UINT4               u4ErrorCode = 0;

    printf ("\r\nTest_TeTestMplsTeP2mpTunnelNotificationEnable: ENTRY");

    /*Case:49 Check for wrong value for test of p2mp notification */
    if (nmhTestv2MplsTeP2mpTunnelNotificationEnable (&u4ErrorCode, i4Value)
        == SNMP_SUCCESS)
    {
        printf ("\r\n1.Failed for TeTestMplsTeP2mpTunnelNotificationEnable");
        return OSIX_FAILURE;
    }

    /*Case:50 Check for wrong value errorcode for test of p2mp notification */
    if (u4ErrorCode != SNMP_ERR_WRONG_VALUE)
    {
        printf ("\r\n2.Failed for TeTestMplsTeP2mpTunnelNotificationEnable");
        return OSIX_FAILURE;
    }

    /*Case:51 Check for correct value for test of p2mp notification */
    i4Value = 1;
    if (nmhTestv2MplsTeP2mpTunnelNotificationEnable (&u4ErrorCode, i4Value)
        == SNMP_FAILURE)
    {
        printf ("\r\n3.Failed for TeTestMplsTeP2mpTunnelNotificationEnable");
        return OSIX_FAILURE;
    }

    printf ("\r\nSuccess for Test_TeTestMplsTeP2mpTunnelNotificationEnable");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetMplsTeP2mpTunnelSubGroupIDNext ()
{
    UINT4               u4Value = 0;

    printf ("\r\nTest_TeGetMplsTeP2mpTunnelSubGroupIDNext: ENTRY");

    /*Case:52 Check for get of SubGroupIDNext */
    if (nmhGetMplsTeP2mpTunnelSubGroupIDNext (&u4Value) == SNMP_FAILURE)
    {
        printf ("\r\nFailed for TeGetMplsTeP2mpTunnelSubGroupIDNext");
        return OSIX_FAILURE;
    }

    printf ("\r\nSuccess for Test_TeGetMplsTeP2mpTunnelSubGroupIDNext");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetMplsTeP2mpTunnelConfigured ()
{
    UINT4               u4Value = 0;

    printf ("\r\nTest_TeGetMplsTeP2mpTunnelConfigured: ENTRY");

    /*Case:53 Check for get of p2mp tunnel configured */
    if (nmhGetMplsTeP2mpTunnelConfigured (&u4Value) == SNMP_FAILURE)
    {
        printf ("\r\nFailed for TeGetMplsTeP2mpTunnelConfigured");
        return OSIX_FAILURE;
    }

    printf ("\r\nSuccess for Test_TeGetMplsTeP2mpTunnelConfigured");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetMplsTeP2mpTunnelActive ()
{
    UINT4               u4Value = 0;

    printf ("\r\nTest_TeGetMplsTeP2mpTunnelActive: ENTRY");

    /*Case:54 Check for get of p2mp tunnel active */
    if (nmhGetMplsTeP2mpTunnelActive (&u4Value) == SNMP_FAILURE)
    {
        printf ("\r\nFailed for TeGetMplsTeP2mpTunnelActive");
        return OSIX_FAILURE;
    }

    printf ("\r\nSuccess for Test_TeGetMplsTeP2mpTunnelActive");
    return OSIX_SUCCESS;
}

INT4
Test_TeGetMplsTeP2mpTunnelTotalMaxHops ()
{
    UINT4               u4Value = 0;

    printf ("\r\nTest_TeGetMplsTeP2mpTunnelTotalMaxHops: ENTRY");

    /*Case:55 Check for get of TotalMaxHops */
    if (nmhGetMplsTeP2mpTunnelTotalMaxHops (&u4Value) == SNMP_FAILURE)
    {
        printf ("\r\nFailed for TeGetMplsTeP2mpTunnelTotalMaxHops");
        return OSIX_FAILURE;
    }

    printf ("\r\nSuccess for Test_TeGetMplsTeP2mpTunnelTotalMaxHops");
    return OSIX_SUCCESS;
}

INT4
Test_TeCliP2mpTunnelDelete (VOID)
{
    printf ("\r\nTest_TeCliP2mpTunnelDelete: ENTRY");

    /* CASE-301: Delete a P2MP tunnel when it is not created */
    printf ("\r\nCASE-301:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-302: Create a P2P tunnel and delete a P2MP tunnel */
    printf ("\r\nCASE-302:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 2.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 0x02000001 source 10.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-303: Delete a P2MP tunnel when destinations are associated to it */
    printf ("\r\nCASE-303:");
    CreateConf2 ();

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no tunnel mpls destination 2.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 102.0.0.1 in-label 200001 vlan 2 out-label 200003 4.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 103.0.0.1 in-label 200001 vlan 2 out-label 200003 4.0.0.2");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-304: Create a P2MP tunnel and delete it */
    printf ("\r\nCASE-304:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 101.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 102.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 103.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* All test cases successfully executed */
    printf ("\r\nTest_TeCliP2mpTunnelDelete: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 101.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 102.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 103.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("exit;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    DeleteConf2 ();

    printf ("\r\nTest_TeCliP2mpTunnelDelete: EXIT");
    return OSIX_SUCCESS;
}

INT4
Test_TeCliShowP2mpDestinations (VOID)
{
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];

    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;

    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    printf ("\r\nTest_TeCliShowP2mpDestinations: ENTRY");

    CreateConf2 ();

    /* CASE-305: Create a P2P tunnel and invoke show */
    printf ("\r\nCASE-305:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("tunnel mpls destination 2.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-306: Create a P2MP tunnel and invoke show */
    printf ("\r\nCASE-306:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd ("no tunnel mpls destination 2.0.0.1 source 10.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-307: Associate destinations for P2MP tunnel 
     * without out-label and invoke show */
    printf ("\r\nCASE-307:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 100.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 101.0.0.1 in-label 200001 vlan 2 out-label 200002 3.0.0.2");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    MPLS_INTEGER_TO_OCTETSTRING (1, (&OutSegmentIndex));
    /* Following line has been commented out after testing */
/*
    nmhSetMplsOutSegmentRowStatus ((&OutSegmentIndex), MPLS_STATUS_DESTROY);
*/
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* CASE-308: Add out-labels for the configured 
     * destinations and invoke show */
    printf ("\r\nCASE-308:");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 100.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 101.0.0.1");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 102.0.0.1 in-label 200001 vlan 2 out-label 200003 4.0.0.2");
    CliExecuteAppCmd
        ("tunnel mpls static point-to-multipoint destination 103.0.0.1 in-label 200001 vlan 2 out-label 200003 4.0.0.2");
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("show mpls traffic-eng tunnels");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* All test cases successfully executed */
    printf ("\r\nTest_TeCliShowP2mpDestinations: All test cases passed");
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t;interface mplstunnel 10");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 102.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls static point-to-multipoint destination 103.0.0.1");
    CliExecuteAppCmd
        ("no tunnel mpls destination point-to-multipoint 6 source 10.0.0.1");
    CliExecuteAppCmd ("exit;no interface mplstunnel 10;end");
    CliGiveAppContext ();
    MGMT_LOCK ();

    DeleteConf2 ();

    printf ("\r\nTest_TeCliShowP2mpDestinations: EXIT");
    return OSIX_SUCCESS;
}

VOID
CreateConf2 (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("map switch default");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");

    CliExecuteAppCmd ("interface gigabitethernet 0/2");
    CliExecuteAppCmd ("map switch default");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");

    CliExecuteAppCmd ("interface gigabitethernet 0/3");
    CliExecuteAppCmd ("map switch default");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");

    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd ("ports gigabitethernet 0/1");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("vlan 3");
    CliExecuteAppCmd ("ports gigabitethernet 0/2");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("vlan 4");
    CliExecuteAppCmd ("ports gigabitethernet 0/3");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("ip address 2.0.0.1 255.255.255.0");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");

    CliExecuteAppCmd ("interface vlan 3");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("ip address 3.0.0.1 255.255.255.0");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("exit");

    CliExecuteAppCmd ("interface vlan 4");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("ip address 4.0.0.1 255.255.255.0");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");

    CliGiveAppContext ();
    MGMT_LOCK ();
}

VOID
DeleteConf2 (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");

    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("no mpls ip");
    CliExecuteAppCmd ("no ip address 2.0.0.1 255.255.255.0");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no interface vlan 2");

    CliExecuteAppCmd ("interface vlan 3");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("no mpls ip");
    CliExecuteAppCmd ("no ip address 3.0.0.1 255.255.255.0");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no interface vlan 3");

    CliExecuteAppCmd ("interface vlan 4");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("no mpls ip");
    CliExecuteAppCmd ("no ip address 4.0.0.1 255.255.255.0");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no interface vlan 4");

    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2");
    CliExecuteAppCmd ("no ports");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("vlan 3");
    CliExecuteAppCmd ("no ports");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("vlan 4");
    CliExecuteAppCmd ("no ports");
    CliExecuteAppCmd ("end");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface gigabitethernet 0/1");
    CliExecuteAppCmd ("no map switch default");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no interface gigabitethernet 0/1");

    CliExecuteAppCmd ("interface gigabitethernet 0/2");
    CliExecuteAppCmd ("no map switch default");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no interface gigabitethernet 0/2");

    CliExecuteAppCmd ("interface gigabitethernet 0/3");
    CliExecuteAppCmd ("no map switch default");
    CliExecuteAppCmd ("shutdown");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("no interface gigabitethernet 0/3");
    CliExecuteAppCmd ("end");

    CliGiveAppContext ();
    MGMT_LOCK ();
}

VOID
CreateConf3 (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("sh sp;set gm dis;set gv dis;sh ga;exit");
    CliExecuteAppCmd ("int gi 0/1;map swi default;no shut;exit");
    CliExecuteAppCmd ("int gi 0/2;map swi default;no shut;exit");
    CliExecuteAppCmd ("int gi 0/3;map swi default;no shut;exit");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2;ports gi 0/1;exit");
    CliExecuteAppCmd ("vlan 3;ports gi 0/2;exit");
    CliExecuteAppCmd ("vlan 4;ports gi 0/3;exit");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("interface vlan 2");
    CliExecuteAppCmd
        ("ip address 100.0.0.1 255.0.0.0;no shutdown;mpls ip;exit");
    CliExecuteAppCmd ("int vlan 3");
    CliExecuteAppCmd
        ("ip address 200.0.0.1 255.0.0.0;no shutdown;mpls ip;exit");
    CliExecuteAppCmd ("int vlan 4");
    CliExecuteAppCmd ("ip address 50.0.0.1 255.0.0.0;no shutdown;mpls ip;end");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls global-id 100 icc-id ARI123 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 5 global-id 100 node-id 10 ");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd
        ("mpls node-map-id local-map-num 7 global-id 300 node-id 30");
    CliExecuteAppCmd ("end ");
    CliGiveAppContext ();
    MGMT_LOCK ();
}

VOID
DeleteConf3 (VOID)
{
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 5 global-id 100 node-id 10 ");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 6 global-id 200 node-id 20");
    CliExecuteAppCmd
        ("no mpls node-map-id local-map-num 7 global-id 300 node-id 30");
    CliExecuteAppCmd ("no mpls global-id icc-id node-id");
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("interface vlan 2;no mpls ip;exit");
    CliExecuteAppCmd ("interface vlan 3;no mpls ip;exit");
    CliExecuteAppCmd ("interface vlan 4;no mpls ip;exit");
    CliExecuteAppCmd ("no interface vlan 2");
    CliExecuteAppCmd ("no interface vlan 3");
    CliExecuteAppCmd ("no interface vlan 4");
    CliExecuteAppCmd ("switch default");
    CliExecuteAppCmd ("vlan 2;no ports;exit");
    CliExecuteAppCmd ("vlan 3;no ports;exit");
    CliExecuteAppCmd ("vlan 4;no ports;exit");
    CliExecuteAppCmd ("no vlan 2");
    CliExecuteAppCmd ("no vlan 3");
    CliExecuteAppCmd ("no vlan 4");
    CliExecuteAppCmd ("exit");
    CliExecuteAppCmd ("int gi 0/1;shut;no map switch default;exit");
    CliExecuteAppCmd ("int gi 0/2;shut;no map switch default;exit");
    CliExecuteAppCmd ("int gi 0/3;shut;no map switch default;exit");
    CliExecuteAppCmd ("no int gi 0/1;no int gi 0/2;no int gi 0/3;");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
}

VOID
CleanupEntries (VOID)
{
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tOutSegment        *pOutSegment = NULL;

    pXcEntry = MplsGetXCTableEntry (1, 0, 1);
    if (pXcEntry != NULL)
    {
        MplsDeleteXCTableEntry (pXcEntry);
    }
    pXcEntry = MplsGetXCTableEntry (1, 0, 2);
    if (pXcEntry != NULL)
    {
        MplsDeleteXCTableEntry (pXcEntry);
    }
    pXcEntry = MplsGetXCTableEntry (1, 1, 0);
    if (pXcEntry != NULL)
    {
        MplsDeleteXCTableEntry (pXcEntry);
    }

    pOutSegment = MplsGetOutSegmentTableEntry (1);
    if (pOutSegment != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
    }
    pOutSegment = MplsGetOutSegmentTableEntry (2);
    if (pOutSegment != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
    }
    pInSegment = MplsGetInSegmentTableEntry (1);
    if (pInSegment != NULL)
    {
        MplsDeleteInSegmentTableEntry (pInSegment);
    }
}
