
/********************************************************************
 ** Copyright (C) 2008 Aricent Inc . All Rights Reserved
 **
 ** $Id: mptstcli.h,v 1.2 2010/11/16 07:23:08 prabuc Exp $
 **
 ** Description: MPLS API  UT Test cases.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"

INT4 cli_process_mplstp_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);

INT4
MplsUt(INT4 u4GlobalId );

/* MPLS_P2MP_LSP_CHANGES - S */
INT4
cli_process_mpls_p2mp_test_cmd PROTO ((tCliHandle CliHandle, UINT4 u4Command,...));
/* MPLS_P2MP_LSP_CHANGES - E */

