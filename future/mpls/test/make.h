##########################################################################
# Copyright (C) Future Software Limited,2006			         #
#                                                                        #
#								         #
# Description : Contains information fro creating the make file          #
#		for this module      				         #
#								         #
##########################################################################

#include the make.h and make.rule from LR
include ../../LR/make.h
include ../../LR/make.rule

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)

#project directories
MPLS_INCL_DIR           = $(BASE_DIR)/mpls/mplsinc
MPLS_UTIL_DIR           = $(BASE_DIR)/util/mpls
PROJECT_SOURCE_DIR      = ${PROJECT_BASE_DIR}/src
PROJECT_TEST_DIR        = ${PROJECT_BASE_DIR}/test
PROJECT_INCLUDE_DIR     = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR      = ${PROJECT_BASE_DIR}/obj
MPLS_CDM_DIR            = $(BASE_DIR)/mpls/mplsdb
MPLS_L2VPN_DIR          = $(BASE_DIR)/mpls/l2vpn/inc
MPLS_FM_INCL_DIR        = ${BASE_DIR}/mpls/mplsfm/inc
MPLS_LDP_INCL_DIR       = ${BASE_DIR}/mpls/ldp/inc
MPLS_TE_INCL_DIR        = ${BASE_DIR}/mpls/te/inc
MPLS_RSVPTE_INCL_DIR    = ${BASE_DIR}/mpls/rsvpte/inc
MPLS_OAM_INCL_DIR       = ${BASE_DIR}/mpls/oam/inc

MPLSTEST_BASE_DIR  = ${BASE_DIR}/mpls/test
MPLSTEST_SRC_DIR   = ${MPLSTEST_BASE_DIR}/src
MPLSTEST_INC_DIR   = ${MPLSTEST_BASE_DIR}/inc
MPLSTEST_OBJ_DIR   = ${MPLSTEST_BASE_DIR}/obj

MPLSTEST_INCLUDES  = -I$(MPLSTEST_INC_DIR) \
                     -I$(PROJECT_INCLUDE_DIR) \
                     -I$(MPLS_INCL_DIR) \
                     -I$(MPLS_UTIL_DIR) \
                     -I$(MPLS_FM_INCL_DIR) \
                     -I$(MPLS_LDP_INCL_DIR) \
                     -I$(MPLS_TE_INCL_DIR) \
                     -I$(MPLS_RSVPTE_INCL_DIR) \
                     -I$(MPLS_CDM_DIR) \
                     -I$(MPLS_L2VPN_DIR) \
                     -I$(MPLS_OAM_INCL_DIR) \
                     $(COMMON_INCLUDE_DIRS)


##########################################################################
#                    End of file make.h					 #
##########################################################################
