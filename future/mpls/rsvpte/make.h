#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 05/11/2001                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureRSVPTE       |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+


# Set the PROJ_BASE_DIR as the directory where you untar the project files

PROJECT_NAME        = FutureRSVPTE
PROJECT_BASE_DIR    = ${BASE_DIR}/mpls/rsvpte
MPLS_INCL_DIR       = $(BASE_DIR)/mpls/mplsinc
PROJECT_SOURCE_DIR  = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR  = ${PROJECT_BASE_DIR}/obj

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/rptedbg.h \
                $(PROJECT_INCLUDE_DIR)/rptegbl.h \
                $(PROJECT_INCLUDE_DIR)/rpteport.h \
                $(PROJECT_INCLUDE_DIR)/rptetdf1.h \
                $(PROJECT_INCLUDE_DIR)/rsvpprot.h \
                $(PROJECT_INCLUDE_DIR)/rpteprot.h \
                $(PROJECT_INCLUDE_DIR)/rptedefs.h \
                $(PROJECT_INCLUDE_DIR)/rpteincs.h \
                $(PROJECT_INCLUDE_DIR)/rptetdf2.h \
                $(PROJECT_INCLUDE_DIR)/rsvptdf1.h \
                $(PROJECT_INCLUDE_DIR)/rpteextn.h \
                $(PROJECT_INCLUDE_DIR)/rptete.h \
                $(PROJECT_INCLUDE_DIR)/rptedste.h \
                $(PROJECT_INCLUDE_DIR)/rptdstdf.h \
                $(PROJECT_INCLUDE_DIR)/rptedsmc.h \
                $(PROJECT_INCLUDE_DIR)/rptdsdef.h \
                $(PROJECT_INCLUDE_DIR)/rptdsprt.h \
                $(PROJECT_INCLUDE_DIR)/rptemacs.h \
                $(PROJECT_INCLUDE_DIR)/rsvpglob.h \
                $(PROJECT_INCLUDE_DIR)/rsvptdf2.h

PROJECT_FINAL_INCLUDES_DIRS    = -I$(PROJECT_INCLUDE_DIR) \
                    -I$(MPLS_INCL_DIR) \
                    $(COMMON_INCLUDE_DIRS)

PROJECT_FINAL_INCLUDE_FILES    = $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES    = $(COMMON_DEPENDENCIES) \
                $(PROJECT_FINAL_INCLUDE_FILES) \
                $(MPLS_BASE_DIR)/make.h \
                $(PROJECT_BASE_DIR)/Makefile \
                $(PROJECT_BASE_DIR)/make.h


