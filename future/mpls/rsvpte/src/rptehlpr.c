/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptehlpr.c,v 1.5 2014/12/24 10:58:29 siva Exp $
 *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptehlpr.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for the GR functionalities.
 *
 *---------------------------------------------------------------------------*/
#include "rpteincs.h"

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteHlprStartRestartTmr
 * Description     : This function starts the Restart Timer
 * Input (s)       : pNbrEntry - Pointer to neighbor entry Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteHlprStartRestartTmr (tNbrEntry * pNbrEntry)
{
    tTimerParam        *pTmrParam = NULL;
    pTmrParam = (tTimerParam *) & (pNbrEntry->GrTimerParam);
    TIMER_PARAM_ID (pTmrParam) = RPTE_GR_NBR_RESTART_TIMER;
    pTmrParam->u.pNbrEntry = pNbrEntry;
    pNbrEntry->GrTimer.u4Data = (FS_ULONG) pTmrParam;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprStartRestartTmr : ENTRY \n");

    if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST,
                           &(pNbrEntry->GrTimer),
                           (pNbrEntry->u2RestartTime *
                            SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        /* Start Timer failed. */
        RSVPTE_DBG (RSVPTE_GR_PRCS, " Helper Restart timer failed \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprStartRestartTmr : INTMD-EXIT \n");
    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprStartRestartTmr : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteHlprRecoveryProcedure                            */
/* Description     : This function starts recovery procedure in Hlpr      */
/* Input (s)       : pNbrEntry - Pointer to the neighbor entry              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteHlprRecoveryProcedure (tNbrEntry * pNbrEntry)
{
    UINT4               u4MsgId = RPTE_ZERO;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &pNbrEntry->GrTimer);
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprRecoveryProcedure : ENTRY \n");

    /* If the recovery time is zero, do the recovery time out process */
    if (pNbrEntry->u2RecoveryTime == RPTE_ZERO)
    {
        RpteHlprProcessRecoveryTimeOut (pNbrEntry);
        return;
    }
    pNbrEntry->u1GrProgressState = RPTE_GR_RECOVERY_IN_PROGRESS;
    RpteHlprStartRecoveryTmr (pNbrEntry);

    /*case 1 -> scan the downstream tunnels and send path message with
     * recovery label object incase of nodal fault
     * */

    /*case 2 -> scan the downstream tunnels and just refresh the path
     * message in case of control channel fault
     * */

    UTL_DLL_OFFSET_SCAN (&(pNbrEntry->DnStrTnlList), pRsvpTeTnlInfo,
                         pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlOperStatus == RPTE_OPER_UP)
        {
            RptePhPathRefresh (pRsvpTeTnlInfo);
        }
    }
    /* case 3 -> scan the upstream tunnels and just refresh the resv message
     * in case of control channel fault
     * */

    if (pNbrEntry->u1GrFaultType == RPTE_GR_CONTROL_CHANNEL_FAULT)
    {
        UTL_DLL_OFFSET_SCAN (&(pNbrEntry->UpStrTnlList), pRsvpTeTnlInfo,
                             pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
        {
            if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlOperStatus == RPTE_OPER_UP)
            {
                RpteRhResvRefresh (pRsvpTeTnlInfo);
            }
        }
        return;

    }

    /* case 4 -> node is capable of sending recovery path message with S Refresh
     * support and the neighbor is capable of receiving recovery path message,
     * with S Refresh support, then send S Refresh Recovery path message
     * */

    if ((gpRsvpTeGblInfo->RsvpTeCfgParams.u1RecoveryPathCapability &
         RPTE_GR_RECOVERY_PATH_TX) &&
        (pNbrEntry->u1RecoveryPathCapability & RPTE_GR_RECOVERY_PATH_RX))
    {
        if ((gpRsvpTeGblInfo->RsvpTeCfgParams.u1RecoveryPathCapability &
             RPTE_GR_RECOVERY_PATH_SREFRESH) &&
            (pNbrEntry->
             u1RecoveryPathCapability & RPTE_GR_RECOVERY_PATH_SREFRESH))
        {

            UTL_DLL_OFFSET_SCAN (&(pNbrEntry->UpStrTnlList), pRsvpTeTnlInfo,
                                 pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
            {
                if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlOperStatus == RPTE_OPER_UP)
                {
                    u4MsgId = RPTE_TNL_IN_PATH_MSG_ID (pRsvpTeTnlInfo);
                    RpteRRSRefreshAddMsgId (pNbrEntry, u4MsgId,
                                            RPTE_MSG_ID_RECOVERY_PATH);
                }
            }
            if (u4MsgId != RPTE_ZERO)
            {
                NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) = RPTE_TRUE;
                RpteRRSRefreshThreshold (pNbrEntry, RPTE_MSG_ID_RECOVERY_PATH);
            }
        }
        /* case 5 -> node is capable of sending recovery path message 
         * and the neighbor is capable of receiving recovery path message,
         * then send Recovery path message
         * */
        else
        {
            UTL_DLL_OFFSET_SCAN (&(pNbrEntry->UpStrTnlList), pRsvpTeTnlInfo,
                                 pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
            {
                if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlOperStatus == RPTE_OPER_UP)
                {
                    RpteHlprGenerateRecoveryPath (pRsvpTeTnlInfo);
                }
            }
        }
    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprRecoveryProcedure : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteHlprStartRecoveryTmr                             */
/* Description     : This function starts the Recovery Timer for the        */
/*                   RsvpTeGblInfo                                          */
/* Input (s)       : pRsvpTeGblInfo - Pointer to the RsvpTeGblInfo          */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteHlprStartRecoveryTmr (tNbrEntry * pNbrEntry)
{
    tTimerParam        *pTmrParam = NULL;
    pTmrParam = (tTimerParam *) & (pNbrEntry->GrTimerParam);
    TIMER_PARAM_ID (pTmrParam) = RPTE_GR_NBR_RECOVERY_TIMER;
    pTmrParam->u.pNbrEntry = pNbrEntry;
    pNbrEntry->GrTimer.u4Data = (FS_ULONG) pTmrParam;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprStartRecoveryTmr : ENTRY \n");
    if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST,
                           &(pNbrEntry->GrTimer),
                           (pNbrEntry->u2RecoveryTime *
                            SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        /* Start Timer failed. */
        RSVPTE_DBG (RSVPTE_GR_PRCS, " Helper Recovery timer failed \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteHlprStartRecoveryTmr : INTMD-EXIT \n");
    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprStartRecoveryTmr : EXIT \n");
    return;

}

/****************************************************************************/
/* Function Name   : RpteHlprProcessRestartTimeOut                        */
/* Description     : This function handles the restart timer expiry         */
/* Input (s)       : pNbrEntry - Pointer to the neighbor entry              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RpteHlprProcessRestartTimeOut (tNbrEntry * pNbrEntry)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprProcessRestartTimeOut : ENTRY \n");
    MEMSET (&RpteEnqueueMsgInfo, RPTE_ZERO, sizeof (tRpteEnqueueMsgInfo));

    pNbrEntry->u1GrProgressState = RPTE_GR_ABORTED;

    UTL_DLL_OFFSET_SCAN (&(pNbrEntry->DnStrTnlList), pRsvpTeTnlInfo,
                         pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus !=
            TNL_GR_FULLY_SYNCHRONIZED)
        {
            RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pRsvpTeTnlInfo;
            RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
        }

    }
    UTL_DLL_OFFSET_SCAN (&(pNbrEntry->UpStrTnlList), pRsvpTeTnlInfo,
                         pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus !=
            TNL_GR_FULLY_SYNCHRONIZED)
        {
            RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pRsvpTeTnlInfo;
            RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
        }

    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprProcessRestartTimeOut : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteHlprProcessRecoveryTimeOut                       */
/* Description     : This function handles the recovery timer expiry        */
/* Input (s)       : pNbrEntry - Pointer to the neighbor entry              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RpteHlprProcessRecoveryTimeOut (tNbrEntry * pNbrEntry)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprProcessRecoveryTimeOut : ENTRY \n");
    MEMSET (&RpteEnqueueMsgInfo, RPTE_ZERO, sizeof (tRpteEnqueueMsgInfo));

    pNbrEntry->u1GrProgressState = RPTE_GR_COMPLETED;

    UTL_DLL_OFFSET_SCAN (&(pNbrEntry->DnStrTnlList), pRsvpTeTnlInfo,
                         pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus !=
            TNL_GR_FULLY_SYNCHRONIZED)
        {
            RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pRsvpTeTnlInfo;
            RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
        }

    }
    UTL_DLL_OFFSET_SCAN (&(pNbrEntry->UpStrTnlList), pRsvpTeTnlInfo,
                         pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus !=
            TNL_GR_FULLY_SYNCHRONIZED)
        {
            RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pRsvpTeTnlInfo;
            RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
        }

    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprProcessRecoveryTimeOut : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteHlprGenerateRecoveryPath                         */
/* Description     : This function is used to generate RecoveryPath msg     */
/* Input (s)       : pNbrEntry - Pointer to the neighbor entry              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RpteHlprGenerateRecoveryPath (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tPktMap            *pPktMap = NULL;
    tIfEntry           *pIfEntry = NULL;
    tObjHdr            *pObjHdr = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    UINT2		u2RetVal = RSVPTE_ZERO;
    UINT2               u2RecoveryPathSize = RPTE_ZERO;
    UINT2               u2RROSize = RSVPTE_ZERO;
    UINT2               u2SsnObjLen = RSVPTE_ZERO;
    UINT2               u2TmpVar = RSVPTE_ZERO;
    UINT4               u4TmpAddr = RSVPTE_ZERO;
    UINT1               u1RroNeeded = RPTE_FALSE;
    UINT1               u1EroNeeded = RPTE_FALSE;
    UINT1               au1TmpSsnName[LSP_TNLNAME_LEN] = { RSVPTE_ZERO };
    UINT1              *pPdu = NULL;
    UINT1               u1TmpVar = RSVPTE_ZERO;
    UINT4               u4RecoveryPathMsgId = RSVPTE_ZERO;
    UINT4               u4Val = RSVPTE_ZERO;
    tRsvpTeSsnObj       SsnObj;
    tRsvpHopObj         RsvpHopObj;
    tIfIdRsvpHopObj     IfIdRsvpHopObj;
    tIfIdRsvpNumHopObj  IfIdRsvpNumHopObj;
    tAdSpec             NewAdSpec;
    tRsvpTeArHopInfo    RsvpTeArHopInfo;
    tIfEntry           *pIfMapEntry = NULL;
    tAdSpecObj         *pAdSpecObjHdr = NULL;
    tAdSpecWithoutGsAndClsObj *pAdSpecWithoutGsAndClsObjHdr = NULL;
    tSenderTspec       *pSenderTspec = NULL;
    tTimeValuesObj     *pTimeValuesObjHdr = NULL;
    tTimerBlock        *pTimerBlock = NULL;
    tRpteKey            RpteKey;
    tuTrieInfo         *pTrieInfo = NULL;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprGenerateRecoveryPath : ENTRY \n");

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
        TNL_GR_FULLY_SYNCHRONIZED)
    {
        return;
    }

    if ((pPktMap = MemAllocMemBlk (RSVPTE_PKT_MAP_POOL_ID)) == NULL)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS, " Memory Allocation failed \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteHlprGenerateRecoveryPath : INTMD-EXIT \n");
        return;
    }
    if ((pIfMapEntry = MemAllocMemBlk (RSVPTE_IF_ENTRY_POOL_ID)) == NULL)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS, " Memory Allocation failed \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteHlprGenerateRecoveryPath : INTMD-EXIT \n");
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        return;
    }
    MEMSET (&SsnObj, RSVPTE_ZERO, sizeof (tRsvpTeSsnObj));
    MEMSET (&RsvpHopObj, RSVPTE_ZERO, sizeof (tRsvpHopObj));
    MEMSET (&IfIdRsvpNumHopObj, RSVPTE_ZERO, sizeof (tIfIdRsvpNumHopObj));
    MEMSET (&IfIdRsvpHopObj, RSVPTE_ZERO, sizeof (tIfIdRsvpHopObj));
    MEMSET (pIfMapEntry, RSVPTE_ZERO, sizeof (tIfEntry));
    MEMSET (&NewAdSpec, RSVPTE_ZERO, sizeof (tAdSpec));
    MEMSET (&RsvpTeArHopInfo, RSVPTE_ZERO, sizeof (tRsvpTeArHopInfo));
    MEMSET (au1TmpSsnName, RSVPTE_ZERO, LSP_TNLNAME_LEN);
    RpteUtlInitPktMap (pPktMap);
    RPTE_PKT_MAP_TNL_INFO (pPktMap) = pRsvpTeTnlInfo;

    /* Message Id Related code changes */

    if ((gu4RMDFlags & RPTE_RMD_RECOVERY_PATH_MSG) &&
        (NBR_ENTRY_RMD_CAPABLE (RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo))
         == RPTE_ENABLED))
    {
        if (pRsvpTeTnlInfo->pRPTimerBlock == NULL)
        {
            /* Allocate memory if timer block is not present */
            pTimerBlock = (tTimerBlock *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TIMER_BLK_POOL_ID);

            if (pTimerBlock == NULL)
            {
                /* If the allocation of the Timer Block fails
                 * the message will be sent without Msg Id */
                pTimerBlock = NULL;
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
            }
            else
            {
                pRsvpTeTnlInfo->pRPTimerBlock = pTimerBlock;
                /* Back Off Timer value is set to the initial value */
                TIMER_BLK_BACKOFF_INTERVAL (pTimerBlock) =
                    (IF_ENTRY_REFRESH_INTERVAL
                     (pRsvpTeTnlInfo->pPsb->pIncIfEntry) /
                     RPTE_BACK_OFF_FACTOR) / DEFAULT_CONV_TO_SECONDS;

                /* Msg Id is generated for the outgoing RecoveryPath
                 * message */
                RpteMIHGenerateMsgId (&u4RecoveryPathMsgId);

                /* This is for verification purpose, the message id 
                 * in the recently received path message is not copied
                 * into the RecoveryPath message 
                 * */
                pRsvpTeTnlInfo->pTeTnlInfo->u4OutRecoveryPathMsgId =
                    u4RecoveryPathMsgId;

                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;

                /* Since RMD is enabled for RecoveryPath, back off timer is
                 * started for the same. */
                TIMER_PARAM_RPTE_TNL ((&TIMER_BLK_TIMER_PARAM
                                       (pTimerBlock))) = pRsvpTeTnlInfo;
                TIMER_BLK_MSG_ID (pTimerBlock) = u4RecoveryPathMsgId;
                TIMER_PARAM_ID ((&TIMER_BLK_TIMER_PARAM (pTimerBlock)))
                    = RPTE_GR_RECOVERY_PATH;
                RSVP_TIMER_NAME ((&TIMER_BLK_APP_TIMER (pTimerBlock))) =
                    (FS_ULONG) & TIMER_BLK_TIMER_PARAM (pTimerBlock);

                pTrieInfo = (tuTrieInfo *)
                    RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TRIE_INFO_POOL_ID);

                if (pTrieInfo == NULL)
                {
                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                    MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                        (UINT1 *) pTimerBlock);
                    pTimerBlock = NULL;
                    pTrieInfo = NULL;
                }
                else
                {
                    MEMSET (pTrieInfo, RSVPTE_ZERO, sizeof (tuTrieInfo));
                    RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo) = RPTE_GR_RECOVERY_PATH;
                    RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = pTimerBlock;

                    RpteKey.u4_Key = u4RecoveryPathMsgId;

                    if (RpteTrieAddEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                          &RpteKey, pTrieInfo) != RPTE_SUCCESS)
                    {
                        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap)
                            = RPTE_ZERO;
                        MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                            (UINT1 *) pTrieInfo);
                        RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = NULL;
                        MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                            (UINT1 *) pTimerBlock);
                        pTimerBlock = NULL;
                        pTrieInfo = NULL;
                    }
                }
            }
        }
        else
        {
            pTimerBlock = pRsvpTeTnlInfo->pRPTimerBlock;

            RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;
            u4RecoveryPathMsgId = TIMER_BLK_MSG_ID (pTimerBlock);

            RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
            if (RpteTrieLookupEntry
                (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                 &pTrieInfo) != RPTE_SUCCESS)
            {
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
            }
        }

        if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
        {
            if (RpteUtlStartTimer
                (RSVP_GBL_TIMER_LIST,
                 &TIMER_BLK_APP_TIMER (pTimerBlock),
                 TIMER_BLK_BACKOFF_INTERVAL (pTimerBlock) *
                 SYS_NUM_OF_TIME_UNITS_IN_A_SEC) != TMR_SUCCESS)
            {
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = NULL;
                MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                    (UINT1 *) pTrieInfo);
                pTrieInfo = NULL;
                pTimerBlock = NULL;
            }
        }

    }
    else
    {
        /* In the case of RP Timer Block already present,
         * Stop the timer, Delete the Msg Id already present in
         * the Data base and continue. */
        pTimerBlock = pRsvpTeTnlInfo->pRPTimerBlock;

        if (pTimerBlock != NULL)
        {
            TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                          (pTimerBlock));
            RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);

            RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                 RPTE_LCL_MSG_ID_DB_PREFIX,
                                 RpteMIHReleaseTrieInfo);

            MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                (UINT1 *) pTimerBlock);
            pTimerBlock = NULL;
            pTrieInfo = NULL;
        }
        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
    }

    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) = gu4Epoch;
        RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_MSG_ID_RECOVERY_PATH;
        RPTE_PKT_MAP_MSG_ID (pPktMap) = u4RecoveryPathMsgId;
        u2RecoveryPathSize = (UINT2)(u2RecoveryPathSize + RPTE_MSG_ID_OBJ_LEN);
    }

    PKT_MAP_RSVP_PKT (pPktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);

    pIfEntry = PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));

    /*This function is used to calculate the Recoverypath message size */
    u2RetVal =
        RpteUtlCalculatePathMsgSize (pRsvpTeTnlInfo, &u2SsnObjLen, &u2RROSize,
                                     &u1EroNeeded, &u1RroNeeded,
                                     RECOVERY_PATH_MSG);
    u2RecoveryPathSize = (UINT2)(u2RecoveryPathSize + u2RetVal);
    RpteUpdateDSPathObjsSize (pRsvpTeTnlInfo, &u2RecoveryPathSize);

    if (PKT_MAP_RSVP_PKT (pPktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS, " Memory Allocation failed \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteHlprGenerateRecoveryPath : INTMD-EXIT \n");
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
        return;
    }
    MEMSET (PKT_MAP_RSVP_PKT (pPktMap), RSVPTE_ZERO, u2RecoveryPathSize);
    PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    /* Copy Source address i.e this node's address */
    if (IF_ENTRY_ADDR (pIfEntry) != RPTE_ZERO)
    {
        PKT_MAP_SRC_ADDR (pPktMap) = IF_ENTRY_ADDR (pIfEntry);
    }
    else
    {
        CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                            u4TmpAddr);
        u4TmpAddr = OSIX_NTOHL (u4TmpAddr);

        PKT_MAP_SRC_ADDR (pPktMap) = u4TmpAddr;
    }

    PKT_MAP_OUT_IF_ENTRY (pPktMap) = pIfEntry;
    /* Copy Destination address i.e Prev node's address */
    PKT_MAP_DST_ADDR (pPktMap) = RSVP_HOP_ADDR
        (&RSB_FWD_RSVP_HOP (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)));

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    MEMSET (pRsvpHdr, RSVPTE_ZERO, sizeof (tRsvpHdr));

    /* Rsvp Msg Header */
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = RECOVERY_PATH_MSG;

    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (u2RecoveryPathSize);

    pObjHdr =
        (tObjHdr *) (VOID *) (PKT_MAP_RSVP_PKT (pPktMap) + sizeof (tRsvpHdr));

    /* Msg Id Obj update, if present */
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        /* Filling the Header */
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_MSG_ID_OBJ_LEN);
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_MESSAGE_ID_CLASS_NUM_TYPE);

        pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

        /* Filling the Epoch and Flags fields */
        u4Val = (RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) |
                 (UINT4)(RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) << RPTE_3_BYTE_SHIFT));
        u4Val = OSIX_HTONL (u4Val);
        MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4Val, RPTE_FLAG_EPOCH_LEN);

        pPdu += RPTE_FLAG_EPOCH_LEN;

        /* Filling the Message Identifier field */
        u4Val = OSIX_HTONL (RPTE_PKT_MAP_MSG_ID (pPktMap));
        MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4Val, RPTE_MSG_ID_LEN);
        pPdu += RPTE_MSG_ID_LEN;

        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* Ssn Obj updation */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    u4TmpAddr = RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo);

    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);    /* reserved field */
    RPTE_PUT_2_BYTES (pPdu,
                      (UINT2) OSIX_HTONS (RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo)));
    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo)), u4TmpAddr);
    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;

    pObjHdr = (tObjHdr *) (VOID *) pPdu;

    /* Time values Obj updation */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tTimeValuesObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_TIME_VALUES_CLASS_NUM_TYPE);
    pTimeValuesObjHdr = (tTimeValuesObj *) (VOID *) pObjHdr;
    TIME_VALUES_PERIOD (&TIME_VALUES_OBJ (pTimeValuesObjHdr)) =
        OSIX_HTONL (IF_ENTRY_REFRESH_INTERVAL (pIfEntry));
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* Rsvp Hop Obj updation */
    RptePvmFillRsvpHopObj (pRsvpTeTnlInfo, &pObjHdr, RECOVERY_PATH_MSG);
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* ERO Obj IPv4 Type updation */
    if (u1EroNeeded == RPTE_TRUE)
    {
        RptePvmFillEROForRecoveryPathMsg (pRsvpTeTnlInfo,
                                          pRsvpTeTnlInfo->u2RecoveryPathEroSize,
                                          &pObjHdr);
    }

    /* Generic Label Request Object updation */
    RptePvmFillGenericLabelReqObj (pRsvpTeTnlInfo, &pObjHdr, RECOVERY_PATH_MSG);

    /* Label set obj updation */
    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1LabelSetEnabled == RPTE_ENABLED)
    {
        RptePvmFillLabelSetObj (pRsvpTeTnlInfo, &pObjHdr, RECOVERY_PATH_MSG);
    }
    /* Ssn Attr Obj */

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (u2SsnObjLen);

    /* As per section 4.7 in RFC 3209, PATH message should be sent
     * with all three affinity values, if atleast one of the affinity is
     * set as non-zero.Otherwise, it should be sent without any affinity.
     */
    ((RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pRsvpTeTnlInfo) == RSVPTE_ZERO) &&
     (RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pRsvpTeTnlInfo) == RSVPTE_ZERO) &&
     (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo) == RSVPTE_ZERO)) ?
(OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
OSIX_HTONS (RPTE_SSN_ATTR_CLASS_NUM_TYPE)) : (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS (RPTE_RA_SSN_ATTR_CLASS_NUM_TYPE));

    pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    if ((RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pRsvpTeTnlInfo) != RSVPTE_ZERO) ||
        (RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pRsvpTeTnlInfo) != RSVPTE_ZERO) ||
        (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo) != RSVPTE_ZERO))
    {
        RPTE_PUT_4_BYTES (pPdu,
                          OSIX_HTONL (RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF
                                      (pRsvpTeTnlInfo)));
        RPTE_PUT_4_BYTES (pPdu,
                          OSIX_HTONL (RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF
                                      (pRsvpTeTnlInfo)));
        RPTE_PUT_4_BYTES (pPdu,
                          OSIX_HTONL (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF
                                      (pRsvpTeTnlInfo)));
    }
    RPTE_PUT_1_BYTE (pPdu, RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pRsvpTeTnlInfo));
    RPTE_PUT_1_BYTE (pPdu, RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pRsvpTeTnlInfo));
    RPTE_PUT_1_BYTE (pPdu, u1TmpVar);
    RPTE_PUT_1_BYTE (pPdu, (UINT1) STRLEN ((INT1 *) RSVPTE_TNL_NAME
                                           (RPTE_TE_TNL (pRsvpTeTnlInfo))));
    /*
     * Copy Ssn Name based on its length, if required add NULL Padded string
     * at the end.
     */
    if ((STRLEN ((INT1 *) RSVPTE_TNL_NAME
                 (RPTE_TE_TNL (pRsvpTeTnlInfo))) % WORD_BNDRY) != RSVPTE_ZERO)
    {
        MEMCPY (&au1TmpSsnName, &RSVPTE_TNL_NAME
                (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                STRLEN ((INT1 *) RSVPTE_TNL_NAME
                        (RPTE_TE_TNL (pRsvpTeTnlInfo))));

        MEMCPY (pPdu, &au1TmpSsnName, LSP_TNLNAME_LEN);
    }
    else
    {
        MEMCPY (pPdu, &RSVPTE_TNL_NAME (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                STRLEN ((INT1 *) RSVPTE_TNL_NAME
                        (RPTE_TE_TNL (pRsvpTeTnlInfo))));
    }
    pObjHdr = NEXT_OBJ (pObjHdr);
    /* Adminstatus object updation */
    RptePvmFillAdminStatusObj (pRsvpTeTnlInfo, &pObjHdr, PATH_MSG);

    RptePvmFillNotifyReqObj (&pObjHdr, pRsvpTeTnlInfo, PATH_MSG);

    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE) ||
        (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_FULL_REROUTE))
    {
        RptePvmFillProtectionObj (&pObjHdr, pRsvpTeTnlInfo);
        RptePvmFillAssociationObj (&pObjHdr, pRsvpTeTnlInfo);
    }
    RpteUpdateDSPathObjs (pRsvpTeTnlInfo, &pObjHdr);

    pPdu = (UINT1 *) pObjHdr;
    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (RPTE_IPV4_SNDR_TMP_OBJ_LEN));
    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE));

    CONVERT_TO_INTEGER (PSB_RPTE_SNDR_TMP
                        (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).TnlSndrAddr,
                        u4TmpAddr);
    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);
    pPdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);    /* reserved field */
    u2TmpVar =
        OSIX_HTONS (PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                    u2LspId);
    MEMCPY (pPdu, (UINT1 *) &u2TmpVar, sizeof (UINT2));
    pPdu += sizeof (UINT2);
    pObjHdr = (tObjHdr *) (VOID *) pPdu;

    /* Copy Sender Tspec */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tSenderTspecObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_SENDER_TSPEC_CLASS_NUM_TYPE);

    pPdu = ((UINT1 *) pObjHdr + sizeof (tObjHdr));

    pSenderTspec = (tSenderTspec *) (VOID *) pPdu;
    MEMCPY (pSenderTspec,
            &pRsvpTeTnlInfo->pPsb->SenderTspecObj.SenderTspec,
            sizeof (tSenderTspec));
    pPdu = (pPdu + sizeof (tSenderTspec));
    pObjHdr = (tObjHdr *) (VOID *) pPdu;

    /* Fill LSP_TUNNEL_INTERFACE_ID Object */
    RptePvmFillLSPTnlIfIdObj (&pObjHdr, pRsvpTeTnlInfo, PATH_MSG);
    /* If we use Int-Serv Sndr TSpec, we are using default AdSpec. */
    if (pRsvpTeTnlInfo->pPsb->AdSpec.MsgHdr.u2HdrLen != RSVPTE_ZERO)
    {
        if ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS) ||
            (pRsvpTeTnlInfo->pPsb->AdSpec.MsgHdr.u2HdrLen ==
             IPV4_ADSPEC_WITH_GS_AND_CLS_MSG_HDR_LEN))
        {

            pAdSpecObjHdr = (tAdSpecObj *) (VOID *) pObjHdr;

            OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tAdSpecObj));
            OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
                OSIX_HTONS (IPV4_ADSPEC_CLASS_NUM_TYPE);
            MEMCPY (&ADSPEC_OBJ (pAdSpecObjHdr),
                    &PSB_ADSPEC (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                    sizeof (tAdSpec));
        }
        else
        {
            pAdSpecWithoutGsAndClsObjHdr =
                (tAdSpecWithoutGsAndClsObj *) (VOID *) pObjHdr;

            OBJ_HDR_LENGTH (pObjHdr) =
                OSIX_HTONS (sizeof (tAdSpecWithoutGsAndClsObj));
            OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
                OSIX_HTONS (IPV4_ADSPEC_CLASS_NUM_TYPE);
            MEMCPY (&ADSPEC_OBJ (pAdSpecWithoutGsAndClsObjHdr),
                    &PSB_ADSPEC (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                    sizeof (tAdSpecWithoutGsAndCls));
            pAdSpecWithoutGsAndClsObjHdr->CtrlLoadSrvSpec =
                OSIX_HTONL (IPV4_ADSPEC_CLS_SRV_SPEC);
        }

        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* RRO Obj IPv4 Type updation */
    if (u1RroNeeded == RPTE_TRUE)
    {
        RpteRhUpdateRroPktMap (pRsvpTeTnlInfo, pObjHdr, u2RROSize);
        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* Upstream label obj updation */
    RptePvmFillUpstreamGenericLblObj (pRsvpTeTnlInfo, &pObjHdr,
                                      RECOVERY_PATH_MSG);

    RptePvmFillRecoveryLblObj (pRsvpTeTnlInfo, &pObjHdr, RECOVERY_PATH_MSG);

    /* Upstream label obj updation */
    RpteUpdateDSElspTpObj (pRsvpTeTnlInfo, &pObjHdr);

    PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

    /* Path Refresh Sent on IfIndex */
    RptePbSendMsg (pPktMap);
    MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
    MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);

    RpteHlprStartRecoveryPathRefreshTmr (pRsvpTeTnlInfo);
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteHlprGenerateRecoveryPath : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteHlprStartRecoveryPathRefreshTmr
 * Description     : This function starts the refresh time for Recovery path message
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteHlprStartRecoveryPathRefreshTmr (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTimerParam        *pTmrParam = NULL;

    pTmrParam = (tTimerParam *) & pRsvpTeTnlInfo->RecoveryPathTmrParam;

    TIMER_PARAM_ID (pTmrParam) = RPTE_GR_RECOVERY_PATH_REFRESH;
    pTmrParam->u.pRsvpTeTnlInfo = (tRsvpTeTnlInfo *) pRsvpTeTnlInfo;
    RSVP_TIMER_NAME (&pRsvpTeTnlInfo->RecoveryPathTimer) = (FS_ULONG) pTmrParam;

    /* As per RFC 5063, the period between resend attempts SHOULD be such 
     * that at least 3 attempts are completed before the expiry of 3/4 
     * the Recovery Time interval 
     * */

    if (RpteUtlStartTimer
        (RSVP_GBL_TIMER_LIST, &pRsvpTeTnlInfo->RecoveryPathTimer,
         (UINT4) ((pRsvpTeTnlInfo->pUStrNbr->u2RecoveryTime / 4) *
                  SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: Sterting of ResvRefreshTimer failed ..\n");
    }
    return;
}
