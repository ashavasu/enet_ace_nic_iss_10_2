/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rpteror.c,v 1.31 2014/12/24 10:58:29 siva Exp $
 *
 * Description: This file contain routines belonging to Refresh Reduction
 *              Module and Message Id Handler Module.
 ********************************************************************/

#include "rpteincs.h"

/****************************************************************************/
/* Function Name   : RpteRRProcessPkt
 * Description     : This function processes the received RSVP packet. This
 *                   function expects that the readoffset of the buffer
 *                   points to the Ip header in the message
 * Input (s)       : tMsgBuf  - Pointer to the Input Msg Buf
 *                   u1Encaps - Encapsulation Type 
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteRRProcessPkt (tMsgBuf * pMsgBuf, UINT1 u1Encaps)
{
    UINT4               u4IfIndex;
    tCRU_INTERFACE      IfId;
    tPktMap             PktMap;
    UINT1               u1MsgType;
    INT1                i1RetVal;

    tIfEntry           *pIfEntry = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tRsvpIpHdr         *pIpHdr = NULL;
    tCRU_BUF_DATA_DESC *pDataDescptr = NULL;

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessPkt : ENTRY \n");

    MEMSET (&PktMap, RSVPTE_ZERO, sizeof (tPktMap));

    /*
     * Here the assumption is that the First Valid byte in MsgBuf
     * points to IpHdr
     *
     *  Raw Ip Encapsulated Packet
     *  +------------+-----------------+
     *  | IP HDR     | RSVP Packet     |
     *  +------------+-----------------+
     *  ^ Current Read offset
     *
     *
     *  UDP Encapsulated Packet
     *  +------------+------------+------------------+
     *  | IP HDR     |  UDP HDR   |  RSVP Packet     |
     *  +------------+------------+------------------+
     *  ^ Current Read offset
     */
    if (pMsgBuf == NULL)
    {
        RSVPTE_DBG (RSVPTE_RR_PRCS, "-E- : Received NULL Msg Buffer.\n");
        RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessPkt : INTMD-EXIT \n");
        return;
    }

    RpteUtlInitPktMap (&PktMap);
    IfId = BUF_GET_INTERFACE_ID (pMsgBuf);
    u4IfIndex = IF_INDEX (IfId);
    PKT_MAP_ENCAPS_TYPE (&PktMap) = u1Encaps;

    PKT_MAP_MSG_BUF (&PktMap) = pMsgBuf;

    if (RpteUtlBufChainedData (pMsgBuf) == RPTE_YES)
    {
        /* Fragmented Buffer */
        PKT_MAP_IP_PKT (&PktMap) = MemAllocMemBlk (RSVPTE_IP_PKT_POOL_ID);

        if (PKT_MAP_IP_PKT (&PktMap) == NULL)
        {
            RSVPTE_DBG (RSVPTE_RR_PRCS,
                        "-E- : Memory allocation failed for IpPkt in PktMap\n");
            RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessPkt : INTMD-EXIT \n");
            RpteUtlCleanPktMap (&PktMap);
            return;
        }
        PKT_MAP_RSVP_PKT_ALLOC (&PktMap) = RPTE_PKTMAP_IP_ALLOC;

        /* Move ReadOffset to RsvpHdr */
        if (BUF_COPY_FROM_CHAIN (pMsgBuf, PKT_MAP_IP_PKT (&PktMap),
                                 RSVP_DEF_OFFSET,
                                 BUF_GET_CHAIN_VALID_BYTE_COUNT (pMsgBuf))
            != sizeof (PKT_MAP_IP_PKT (&PktMap)))
        {
            BUF_RELEASE (pMsgBuf);
            RpteUtlCleanPktMap (&PktMap);
            RSVPTE_DBG (RSVPTE_MAIN_MEM,
                        "Error while copying From Buffer Chain\n");
            return;
        }

    }
    else
    {
        pDataDescptr = CRU_BUF_GetFirstDataDesc (pMsgBuf);
        if (pDataDescptr != NULL)
        {
            PKT_MAP_IP_PKT (&PktMap) = CRU_BUF_GetDataPtr (pDataDescptr);
        }
        else
        {
            RSVPTE_DBG (RSVPTE_RR_PRCS,
                        "-E- : Error in Data received - NULL pointer \n");
            RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessPkt : INTMD-EXIT \n");
            RpteUtlCleanPktMap (&PktMap);
            return;
        }
    }
    RSVPTE_DBG (RSVPTE_RR_PRCS, "RSVP: Rcvd Msg Dump IP+(UDP)+RSVP Pkt : \n");

    pIpHdr = (tRsvpIpHdr *) (VOID *) PKT_MAP_IP_PKT (&PktMap);
    if (PKT_MAP_ENCAPS_TYPE (&PktMap) == UDP_ENCAP)
    {
        /* UDP Socket So, no Ip+Udp Headers */
        PKT_MAP_RSVP_PKT (&PktMap) = ((UINT1 *) (pIpHdr));
        PKT_MAP_IP_PKT (&PktMap) = NULL;
        /* IP Options NOT Present in the Pkt */
        PKT_MAP_OPT_LENGTH (&PktMap) = RSVPTE_ZERO;
        PKT_MAP_OPT (&PktMap) = NULL;
        PKT_MAP_SRC_ADDR (&PktMap) = RPTE_SRC_ADDR (pMsgBuf);
        PKT_MAP_DST_ADDR (&PktMap) = RPTE_DEST_ADDR (pMsgBuf);
    }
    else
    {
        PKT_MAP_RSVP_PKT (&PktMap) =
            ((UINT1 *) (pIpHdr)) + IP_HDR_LENGTH (pIpHdr);

        if (IP_HDR_LENGTH (pIpHdr) > (UINT1) (sizeof (tRsvpIpHdr)))
        {
            /* Options Present in the Pkt */
            PKT_MAP_OPT_LEN (&PktMap) =
                (INT2)((INT2)IP_HDR_LENGTH (pIpHdr) - sizeof (tRsvpIpHdr));
            PKT_MAP_OPT (&PktMap) = ((UINT1 *) (pIpHdr)) + sizeof (tRsvpIpHdr);
        }

        PKT_MAP_SRC_ADDR (&PktMap) = OSIX_NTOHL (IP_HDR_SRC_ADDR (pIpHdr));
        PKT_MAP_DST_ADDR (&PktMap) = OSIX_NTOHL (IP_HDR_DST_ADDR (pIpHdr));
    }
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (&PktMap);

    /* RX TTl is taken from RsvpHdr instead of IfEntry
       if(pIfEntry->u1Ttl!=0)
       {

       PKT_MAP_RX_TTL (&PktMap) = pIfEntry->u1Ttl;
       }
       else
       {
       PKT_MAP_RX_TTL (&PktMap)=RSVP_HDR_SEND_TTL(pRsvpHdr);
       }
     */

    PKT_MAP_RX_TTL (&PktMap) = RSVP_HDR_SEND_TTL (pRsvpHdr);

    u1MsgType = RSVP_HDR_MSG_TYPE (pRsvpHdr);

    if (RpteGetIfEntry ((INT4)u4IfIndex, &pIfEntry) == RPTE_FAILURE)
    {
        if ((u1MsgType != NOTIFY_MSG) || (u1MsgType != ACK_MSG))
        {
            RSVPTE_DBG1 (RSVPTE_RR_PRCS,
                         "-E- : Rsvp Iface Not Configured on If %x\n",
                         u4IfIndex);
            RpteUtlCleanPktMap (&PktMap);
            RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessPkt : INTMD-EXIT \n");
            return;
        }
    }

    if ((u1MsgType != NOTIFY_MSG) || (u1MsgType != ACK_MSG))
    {
        if ((IF_ENTRY_STATUS (pIfEntry) != ACTIVE) ||
            (IF_ENTRY_ENABLED (pIfEntry) != RPTE_ENABLED))
        {
            RSVPTE_DBG1 (RSVPTE_RR_PRCS,
                         "-E- : Rsvp Interface Not Enabled on If %x\n",
                         u4IfIndex);
            RpteUtlCleanPktMap (&PktMap);
            RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessPkt : INTMD-EXIT \n");
            return;
        }
    }

    RSVPTE_DBG1 (RSVPTE_RR_PRCS, "Rsvp Enabled on Interface %x\n", u4IfIndex);
    PKT_MAP_IF_ENTRY (&PktMap) = pIfEntry;

    if (u1MsgType == BUNDLE_MSG)
    {
        if (gu1RRCapable != RPTE_ENABLED)
        {
            RSVPTE_DBG1 (RSVPTE_RR_PRCS,
                         "-E- : Invalid Msg Type - Msg Type = %d\n", u1MsgType);
            RpteUtlCleanPktMap (&PktMap);
            RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessPkt : INTMD-EXIT \n");
            return;
        }
        i1RetVal = RpteRRProcessBundleMessage (&PktMap);
    }
    else
    {
        if (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) != RSVPTE_DUMP_DIR_NONE)
        {
            RpteDumpIncomRpteMsg (&PktMap);
        }
        i1RetVal = RptePvmProcessPkt (&PktMap);
    }

    if (i1RetVal == RPTE_FAILURE)
    {
        u1MsgType = DISCARD_MSG;
    }

    /* If-Stats update */
    RPTE_IF_STAT_ENTRY_NUM_MSG_RCVD (pIfEntry)++;

    switch (u1MsgType)
    {
        case HELLO_MSG:
            RPTE_IF_STAT_ENTRY_NUM_HELLO_RCVD (pIfEntry)++;
            break;

        case PATHERR_MSG:
            RPTE_IF_STAT_ENTRY_PATH_ERR_RCVD (pIfEntry)++;
            break;

        case PATHTEAR_MSG:
            RPTE_IF_STAT_ENTRY_PATH_TEAR_RCVD (pIfEntry)++;
            break;

        case RESVERR_MSG:
            RPTE_IF_STAT_ENTRY_RESV_ERR_RCVD (pIfEntry)++;
            break;

        case RESVTEAR_MSG:
            RPTE_IF_STAT_ENTRY_RESV_TEAR_RCVD (pIfEntry)++;
            break;

        case RESVCONF_MSG:
            RPTE_IF_STAT_ENTRY_RESV_CONF_RCVD (pIfEntry)++;
            break;

        case BUNDLE_MSG:
            RPTE_IF_STAT_ENTRY_BUNDLE_MSG_RCVD (pIfEntry)++;
            break;

        case SREFRESH_MSG:
            RPTE_IF_STAT_ENTRY_SREFRESH_MSG_RCVD (pIfEntry)++;
            break;
        case PATH_MSG:
            RPTE_IF_STAT_ENTRY_PATH_RCVD (pIfEntry)++;
            break;
        case RESV_MSG:
            RPTE_IF_STAT_ENTRY_RESV_RCVD (pIfEntry)++;
            break;
        case NOTIFY_MSG:
            (IF_ENTRY_STATS_INFO (pIfEntry)).u4IfNumNotifyMsgRcvd++;
            break;
        case RECOVERY_PATH_MSG:
            (IF_ENTRY_STATS_INFO (pIfEntry)).u4IfNumRecoveryPathRcvd++;
            break;
        case DISCARD_MSG:
            RPTE_IF_STAT_ENTRY_PKT_DISCRDED (pIfEntry)++;
	default :
	    break;
    }
    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessPkt : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRRProcessBundleMessage
 * Description     : This function processes the Bundle message received
 *                   by function RpteRRProcessPkt.
 *                   Each sub message of the bundle is extracted and passed
 *                   onto the Packet Validation Module.
 * Input (s)       : pPktMap - Pointer to PktMap Structure
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
INT1
RpteRRProcessBundleMessage (tPktMap * pPktMap)
{
    UINT2               u2CalCheckSum;
    UINT2               u2CheckSum;
    tPktMap             SubPktMap;
    tRsvpHdr           *pRsvpHdr = NULL;
    UINT1              *pu1EndOfPkt = NULL;
    UINT1              *pu1Msg = NULL;

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessBundleMessage : ENTRY \n");

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    pu1EndOfPkt = (UINT1 *) pRsvpHdr + OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr));

    pRsvpHdr = (tRsvpHdr *) (VOID *) ((UINT1 *) pRsvpHdr + sizeof (tRsvpHdr));

    while (pRsvpHdr < (tRsvpHdr *) (VOID *) pu1EndOfPkt)
    {
        switch (RSVP_HDR_MSG_TYPE (pRsvpHdr))
        {
            case PATH_MSG:
            case RESV_MSG:
            case PATHERR_MSG:
            case RESVERR_MSG:
            case PATHTEAR_MSG:
            case RESVTEAR_MSG:
            case RESVCONF_MSG:
            case HELLO_MSG:
            case SREFRESH_MSG:
            case ACK_MSG:
            case NOTIFY_MSG:
                break;

            default:
                RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                             "Invalid Msg Type in Bundle Msg - Msg Type "
                             "= %d\n", RSVP_HDR_MSG_TYPE (pRsvpHdr));
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_RR_ETEXT,
                            "RpteRRProcessBundleMessage : INTMD-EXIT \n");
                return RPTE_FAILURE;
        }
        pRsvpHdr = (tRsvpHdr *) (VOID *) ((UINT1 *) pRsvpHdr +
                                          OSIX_NTOHS (RSVP_HDR_LENGTH
                                                      (pRsvpHdr)));
    }

    if (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) != RSVPTE_DUMP_DIR_NONE)
    {
        RpteDumpIncomRpteMsg (pPktMap);
    }

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    pu1EndOfPkt = (UINT1 *) pRsvpHdr + OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr));

    if (RSVP_HDR_CHECK_SUM (pRsvpHdr) != RSVPTE_ZERO)
    {
        u2CheckSum = RSVP_HDR_CHECK_SUM (pRsvpHdr);
        RSVP_HDR_CHECK_SUM (pRsvpHdr) = RSVPTE_ZERO;
        u2CalCheckSum = (UINT2) RpteUtlCheckSum ((UINT1 *) pRsvpHdr,
                                                 (UINT2)
                                                 OSIX_NTOHS (RSVP_HDR_LENGTH
                                                             (pRsvpHdr)));
        if (u2CheckSum != u2CalCheckSum)
        {
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG2 (RSVPTE_PVM_PRCS,
                         "Invalid Checksum RxCSum = %x CalCsum = %x\n",
                         u2CheckSum, u2CalCheckSum);
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RpteRRProcessBundleMessage : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
    }

    /* In case of TTL mismatch, discard the packet */
    if (RSVP_HDR_SEND_TTL (pRsvpHdr) != PKT_MAP_RX_TTL (pPktMap))
    {
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RR_PRCS,
                    "RR : Non Rsvp Router - path err sent - TTL mismatch "
                    "problem \n");
        RSVPTE_DBG (RSVPTE_RR_ETEXT,
                    "RpteRRProcessBundleMessage : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    pRsvpHdr = (tRsvpHdr *) (VOID *) ((UINT1 *) pRsvpHdr + sizeof (tRsvpHdr));

    while (pRsvpHdr < (tRsvpHdr *) (VOID *) pu1EndOfPkt)
    {
        /* Allocating memory for the sub messages in the bundle */
        pu1Msg = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);
        if (pu1Msg == NULL)
        {
            RpteUtlCleanPktMap (pPktMap);
            return RPTE_FAILURE;
        }

        RpteUtlInitPktMap (&SubPktMap);

        /* Copying the basic PktMap Entries from the main PktMap Entry */
        PKT_MAP_IF_ENTRY (&SubPktMap) = PKT_MAP_IF_ENTRY (pPktMap);
        PKT_MAP_ENCAPS_TYPE (&SubPktMap) = PKT_MAP_ENCAPS_TYPE (pPktMap);

        PKT_MAP_IP_PKT (&SubPktMap) = PKT_MAP_IP_PKT (pPktMap);
        PKT_MAP_MSG_BUF (&SubPktMap) = NULL;
        PKT_MAP_RSVP_PKT_ALLOC (&SubPktMap) = RPTE_PKTMAP_RSVP_ALLOC;

        PKT_MAP_DST_ADDR (&SubPktMap) = PKT_MAP_DST_ADDR (pPktMap);
        PKT_MAP_SRC_ADDR (&SubPktMap) = PKT_MAP_SRC_ADDR (pPktMap);
        PKT_MAP_RX_TTL (&SubPktMap) = RSVP_HDR_SEND_TTL (pRsvpHdr);
        PKT_MAP_TOS (&SubPktMap) = PKT_MAP_TOS (pPktMap);

        PKT_MAP_OPT_LEN (&SubPktMap) = PKT_MAP_OPT_LEN (pPktMap);
        PKT_MAP_OPT (&SubPktMap) = PKT_MAP_OPT (pPktMap);

        /* Copying the RSVP Message */
        MEMCPY (pu1Msg, (UINT1 *) pRsvpHdr,
                OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr)));
        PKT_MAP_RSVP_PKT (&SubPktMap) = pu1Msg;

        if (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) != RSVPTE_DUMP_DIR_NONE)
        {
            RpteDumpIncomRpteMsg (&SubPktMap);
        }
        RptePvmProcessPkt (&SubPktMap);

        /* Only SREFRESH message is bundled with BUNDLE message.
         * Hence, SREFRESH message received counter is incremented
         * after Processing the BUNDLE and SREFRESH messages */
        if (RSVP_HDR_MSG_TYPE (pRsvpHdr) == SREFRESH_MSG)
        {
            RPTE_IF_STAT_ENTRY_SREFRESH_MSG_RCVD (PKT_MAP_IF_ENTRY
                                                  (&SubPktMap))++;
        }
        pRsvpHdr = (tRsvpHdr *) (VOID *) ((UINT1 *) pRsvpHdr +
                                          OSIX_NTOHS (RSVP_HDR_LENGTH
                                                      (pRsvpHdr)));
    }
    RpteUtlCleanPktMap (pPktMap);
    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessBundleMessage : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteMIHSendAckMsg
 * Description     : This function forms and sends an Acknowledgement message
 *                   with a single Message ID Acknowledgement Object.  This
 *                   function is invoked in response to an incoming Message
 *                   ID object received with the Acknowledgement Desired
 *                   flag set.
 *                   
 * Input (s)       : pNbrEntry - Pointer to the neighbour entry to which
 *                               the Ack message needs to be sent.
 *                   pMsgIdObj - Pointer to the message id object that
 *                               contains the message id for which the
 *                               Ack is to be sent.
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHSendAckMsg (const tNbrEntry * pNbrEntry, tPktMap * pInPktMap)
{
    tPktMap             PktMap;
    UINT2               u2AckSize;
    UINT2               u2MsgIdAckLen;
    UINT4               u4Epoch;
    UINT4               u4MsgId;

    tIfEntry           *pIfEntry = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tObjHdr            *pObjHdr = NULL;
    tMsgIdObj          *pMsgIdObj = NULL;
    UINT1              *pu1Pdu = NULL;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHSendAckMsg : ENTRY \n");

    if (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_NO_ACK)
    {
        return;
    }

    pMsgIdObj = &RPTE_PKT_MAP_MSG_ID_OBJ (pInPktMap);

    /* Initialize packet map */
    RpteUtlInitPktMap (&PktMap);

    u2MsgIdAckLen = sizeof (tObjHdr) + RPTE_FLAG_EPOCH_LEN + RPTE_MSG_ID_LEN;

    u2AckSize = (UINT2) (sizeof (tRsvpHdr) + u2MsgIdAckLen);

    PKT_MAP_RSVP_PKT (&PktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);

    if (PKT_MAP_RSVP_PKT (&PktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_MIH_PRCS,
                    "Failed to allocate memory for the packet \n");
        RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHSendAckMsg : INTMD-EXIT\n");
        return;
    }

    MEMSET (PKT_MAP_RSVP_PKT (&PktMap), RPTE_ZERO, u2AckSize);

    PKT_MAP_RSVP_PKT_ALLOC (&PktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    if (pNbrEntry != NULL)
    {
        pIfEntry = NBR_ENTRY_IF_ENTRY (pNbrEntry);
        PKT_MAP_SRC_ADDR (&PktMap) = IF_ENTRY_ADDR (pIfEntry);
        PKT_MAP_DST_ADDR (&PktMap) = NBR_ENTRY_ADDR (pNbrEntry);
        PKT_MAP_OUT_IF_ENTRY (&PktMap) = pIfEntry;
        PKT_MAP_RA_OPT_NEEDED (&PktMap) = RPTE_NO;
    }
    else                        /* ACK for Notify message */
    {
        PKT_MAP_SRC_ADDR (&PktMap) = PKT_MAP_DST_ADDR (pInPktMap);
        PKT_MAP_DST_ADDR (&PktMap) = PKT_MAP_SRC_ADDR (pInPktMap);
        PKT_MAP_RA_OPT_NEEDED (&PktMap) = RPTE_YES;
    }

    PKT_MAP_OPT_LEN (&PktMap) = RPTE_ZERO;

    /* Filling the Transmit TTL value with the default value */
    PKT_MAP_TX_TTL (&PktMap) = DEFAULT_TTL;

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (&PktMap);
    MEMSET (pRsvpHdr, RPTE_ZERO, sizeof (tRsvpHdr));

    /* Rsvp Msg Header */
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = ACK_MSG;
    RSVP_HDR_SEND_TTL (pRsvpHdr) = DEFAULT_TTL;
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (u2AckSize);

    pObjHdr =
        (tObjHdr *) (VOID *) (PKT_MAP_RSVP_PKT (&PktMap) + sizeof (tRsvpHdr));

    /* Message Id Ack Object */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (u2MsgIdAckLen);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS
        (RPTE_MESSAGE_ID_ACK_CLASS_NUM_TYPE);

    pu1Pdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

    /* Filling the Epoch Value */

    if (pMsgIdObj->u1Flags == RPTE_MSG_ID_RECOVERY_PATH)
    {
        /* As per RFC 5063, Recovery path flag should be included
         * in the ACK message of RecoveryPath message
         * */
        u4Epoch = OSIX_HTONL ((RPTE_MSG_ID_OBJ_EPOCH (pMsgIdObj) |
                               (RPTE_MSG_ID_RECOVERY_PATH <<
                                RPTE_3_BYTE_SHIFT)));
    }
    else
    {
        u4Epoch = OSIX_HTONL (RPTE_MSG_ID_OBJ_EPOCH (pMsgIdObj));
    }
    MEMCPY ((UINT1 *) pu1Pdu, (UINT1 *) &u4Epoch, RPTE_FLAG_EPOCH_LEN);
    pu1Pdu += RPTE_FLAG_EPOCH_LEN;

    /* Filling the Message Id */
    u4MsgId = OSIX_HTONL (RPTE_MSG_ID_OBJ_MSG_ID (pMsgIdObj));
    MEMCPY ((UINT1 *) pu1Pdu, (UINT1 *) &u4MsgId, RPTE_MSG_ID_LEN);

    PKT_MAP_BUILD_PKT (&PktMap) = RPTE_YES;

    RptePbSendMsg (&PktMap);

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHSendAckMsg : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteMIHSendNackMsg
 * Description     : This function forms and sends an ACK Message with 
 *                   one/more Message ID NACK objects for the Message IDs
 *                   specified in the pMsgIdList.  This function is invoked
 *                   in response to an Srefresh message with Message IDs 
 *                   corresponding to an unknown state.
 *                  
 * Input (s)       : pNbrEntry  - Pointer to the neighbour entry to which
 *                                SRefresh Nack needs to be sent.
 *                   pMsgIdList - Pointer to the list of message ids for
 *                                which SRefresh Nack is to be sent.
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHSendNackMsg (const tNbrEntry * pNbrEntry, tTMO_SLL * pMsgIdList)
{
    tPktMap             PktMap;
    UINT2               u2AckSize;
    UINT2               u2NackListLen;
    UINT4               u4MsgId;
    UINT4               u4Epoch;

    tIfEntry           *pIfEntry = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tObjHdr            *pObjHdr = NULL;
    tMsgIdObjNode      *pMsgIdObjNode = NULL;
    UINT1              *pu1Pdu = NULL;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHSendNackMsg : ENTRY\n");

    pIfEntry = NBR_ENTRY_IF_ENTRY (pNbrEntry);

    /* Initialize packet map */
    RpteUtlInitPktMap (&PktMap);

    /* Assumption is SLL_Count of MsgIdList will not be zero */
    u2NackListLen = (UINT2) (TMO_SLL_Count (pMsgIdList) *
                             (RPTE_FLAG_EPOCH_LEN + RPTE_MSG_ID_LEN +
                              sizeof (tObjHdr)));

    u2AckSize = (UINT2) (sizeof (tRsvpHdr) + u2NackListLen);
    PKT_MAP_RSVP_PKT (&PktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);

    if (PKT_MAP_RSVP_PKT (&PktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_MIH_PRCS,
                    "Failed to allocate memory for the packet \n");
        RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHSendNackMsg : INTMD-EXIT\n");
        return;
    }

    MEMSET (PKT_MAP_RSVP_PKT (&PktMap), RPTE_ZERO, u2AckSize);

    PKT_MAP_RSVP_PKT_ALLOC (&PktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    PKT_MAP_SRC_ADDR (&PktMap) = IF_ENTRY_ADDR (pIfEntry);
    PKT_MAP_DST_ADDR (&PktMap) = NBR_ENTRY_ADDR (pNbrEntry);

    PKT_MAP_RA_OPT_NEEDED (&PktMap) = RPTE_NO;
    PKT_MAP_OPT_LEN (&PktMap) = RPTE_ZERO;

    PKT_MAP_OUT_IF_ENTRY (&PktMap) = pIfEntry;

    /* Filling the Transmit TTL value with the default value */
    PKT_MAP_TX_TTL (&PktMap) = DEFAULT_TTL;

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (&PktMap);
    MEMSET (pRsvpHdr, RPTE_ZERO, sizeof (tRsvpHdr));

    /* Rsvp Msg Header */
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = ACK_MSG;
    RSVP_HDR_SEND_TTL (pRsvpHdr) = DEFAULT_TTL;
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (u2AckSize);

    pObjHdr =
        (tObjHdr *) (VOID *) (PKT_MAP_RSVP_PKT (&PktMap) + sizeof (tRsvpHdr));

    /* Scan the MsgId list to construct the Nack object */
    TMO_SLL_Scan ((tTMO_SLL *) pMsgIdList, pMsgIdObjNode, tMsgIdObjNode *)
    {
        /* Filling Nack Object */
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS ((sizeof (tObjHdr) +
                                                RPTE_FLAG_EPOCH_LEN +
                                                RPTE_MSG_ID_LEN));
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_MESSAGE_ID_NACK_CLASS_NUM_TYPE);

        pu1Pdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

        if (pMsgIdObjNode->MsgIdObj.u1Flags == RPTE_MSG_ID_RECOVERY_PATH)
        {
            /* As per RFC 5063, Recovery path flag should be included
             * in the NACK message of RecoveryPath message
             *  */
            u4Epoch = OSIX_HTONL ((RPTE_MSG_ID_OBJ_NODE_EPOCH (pMsgIdObjNode) |
                                   (RPTE_MSG_ID_RECOVERY_PATH <<
                                    RPTE_3_BYTE_SHIFT)));
        }
        else
        {
            u4Epoch = OSIX_HTONL (RPTE_MSG_ID_OBJ_NODE_EPOCH (pMsgIdObjNode));
        }
        MEMCPY ((UINT1 *) pu1Pdu, (UINT1 *) &u4Epoch, RPTE_FLAG_EPOCH_LEN);
        pu1Pdu += RPTE_FLAG_EPOCH_LEN;

        u4MsgId = OSIX_HTONL (RPTE_MSG_ID_OBJ_NODE_MSG_ID (pMsgIdObjNode));
        MEMCPY ((UINT1 *) pu1Pdu, (UINT1 *) &u4MsgId, RPTE_MSG_ID_LEN);

        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    PKT_MAP_BUILD_PKT (&PktMap) = RPTE_YES;

    RptePbSendMsg (&PktMap);
    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHSendNackMsg : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteMIHProcessMsgId
 * Description     : This function determines if an incoming Epoch value, 
 *                   Message ID value pair from a given Neighbour Entry 
 *                   corresponds to an existing known state, an Out of Order 
 *                   message, an Epoch Change or a Trigger Message.  It looks
 *                   up the Neighbour's Message ID database to determine the
 *                   category to which the incoming value belongs.
 *                   
 * Input (s)       : pNbrEntry  - Pointer to the neighbour entry with which
 *                                the Epcoh value and the message id is
 *                                associated.
 *                   u4Epoch    - Epoch Value received from the neighbour.
 *                   u4MsgId    - Message Id received from the neighbour.
 * Output (s)      : ppTrieInfo - Points to the Trie info retrieved when 
 *                                u4MsgId is already present in the
 *                                Neighbour Message Id data base.
 * Returns         : RPTE_TRIG_MSG      or
 *                   RPTE_REFRESH_MSG   or
 *                   RPTE_EPOCH_CHANGE  or
 *                   RPTE_MSG_ID_NOT_IN_DB
 */
/****************************************************************************/
UINT4
RpteMIHProcessMsgId (tNbrEntry * pNbrEntry, UINT4 u4Epoch, UINT4 u4MsgId,
                     tuTrieInfo ** ppTrieInfo)
{
    tRpteKey            RpteKey;
    INT4                i4RetVal;
    tuTrieInfo         *pTrieInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgId : ENTRY\n");

    *ppTrieInfo = NULL;

    if (pNbrEntry == NULL)
    {
        return RPTE_MSG_ID_NOT_IN_DB;
    }

    /* If the Nbr Epoch value is uninitialised, set it to the
     * incoming Epoch value and treat the message as a trigger
     * message */
    if (NBR_ENTRY_EPOCH_VALUE (pNbrEntry) == RPTE_UN_INIT_EPOCH)
    {
        NBR_ENTRY_EPOCH_VALUE (pNbrEntry) = u4Epoch;
        NBR_ENTRY_MAX_MSG_ID (pNbrEntry) = u4MsgId;
        RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgId : EXIT\n");
        return RPTE_TRIG_MSG;
    }

    if (NBR_ENTRY_EPOCH_VALUE (pNbrEntry) == u4Epoch)
    {
        /* Check is done as specified in the section 4.5 of RFC2961 */
        if ((INT4) u4MsgId > (INT4) NBR_ENTRY_MAX_MSG_ID (pNbrEntry))
        {
            NBR_ENTRY_MAX_MSG_ID (pNbrEntry) = u4MsgId;
            RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgId : EXIT\n");
            return RPTE_TRIG_MSG;
        }
        else
        {
            /* Search the neighbour message id database to see if the msgid
             * is present */
            RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (pNbrEntry);
            RpteKey.u8_Key[RPTE_ONE] = u4MsgId;
            i4RetVal = RpteTrieLookupEntry (&RPTE_64_BIT_TRIE_ROOT_NODE,
                                            &RpteKey, &pTrieInfo);

            if (i4RetVal == RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgId : EXIT\n");
                return RPTE_MSG_ID_NOT_IN_DB;
            }
            else
            {
                pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);

                if ((pRsvpTeTnlInfo != NULL) &&
                    (RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
                     RPTE_IN_PATH_MSG_ID_PRESENT) &&
                    (RPTE_TNL_IN_PATH_MSG_ID (pRsvpTeTnlInfo) == u4MsgId))
                {
                    /* MsgId is present in the data base. */
                    *ppTrieInfo = pTrieInfo;

                    RSVPTE_DBG (RSVPTE_MIH_ETEXT,
                                "RpteMIHProcessMsgId : EXIT\n");
                    return RPTE_REFRESH_MSG;
                }
                else
                {
                    return RPTE_TRIG_MSG;
                }
            }
        }
    }
    else
    {
        /* Epoch Value Change. Flush the data base */
        RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (pNbrEntry);
        RpteKey.u8_Key[RPTE_ONE] = RPTE_ZERO;
        RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_NBR_MSG_ID_DB_NBR_PREFIX,
                             RpteMIHReleaseTrieInfo);

        NBR_ENTRY_EPOCH_VALUE (pNbrEntry) = u4Epoch;
        NBR_ENTRY_MAX_MSG_ID (pNbrEntry) = u4MsgId;

        RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgId : EXIT\n");
        return RPTE_EPOCH_CHANGE;
    }
}

/****************************************************************************/
/* Function Name   : RpteMIHResetMsgId
 * Description     : This function is called by Trie DeleteNode routine.
 *                   The Msg Id corresponding to the message type is reset.
 *                   
 * Input (s)       : pTrieInfo - Handle returned by the Trie DeleteNode
 *                              routine.
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHResetMsgId (tuTrieInfo * pTrieInfo)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHResetMsgId : ENTRY\n");

    /* This function is called only in the case of deleting message id
     * belonging to a path and resv state.
     * So the TrieInfo will contain the RsvpTeTnlInfo. It is 
     * extracted. */
    pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);

    switch (RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo))
    {
        case RPTE_PATH_INCOMING:

            RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &=
                (RPTE_IN_PATH_MSG_ID_NOT_PRESENT &
                 RPTE_USTR_MSG_ID_NOT_PRESENT);
            break;

        case RPTE_RESV_INCOMING:

            RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &=
                (RPTE_IN_RESV_MSG_ID_NOT_PRESENT &
                 RPTE_DSTR_MSG_ID_NOT_PRESENT);
            break;

        case RPTE_PATH_REFRESH:

            RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &=
                RPTE_OUT_PATH_MSG_ID_NOT_PRESENT;
            break;

        case RPTE_RESV_REFRESH:

            RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &=
                RPTE_OUT_RESV_MSG_ID_NOT_PRESENT;
            break;

        default:
            break;
    }
    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHResetMsgId : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteMIHReleaseTrieInfo
 * Description     : This function is called by Trie DeleteNode routine.
 *                   It releases the TrieInfo and resets the MsgId.
 *                   
 * Input (s)       : pTrieInfo - Handle returned by the Trie DeleteNode
 *                              routine.
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHReleaseTrieInfo (tuTrieInfo * pTrieInfo)
{

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHReleaseTrieInfo : ENTRY\n");

    switch (RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo))
    {
        case RPTE_PATH_INCOMING:
        case RPTE_RESV_INCOMING:
        case RPTE_PATH_REFRESH:
        case RPTE_RESV_REFRESH:
            RpteMIHResetMsgId (pTrieInfo);
            break;

        case RPTE_PATHERR:
        case RPTE_PATHTEAR:
        case RPTE_RESVERR:
        case RPTE_RESVTEAR:
        default:
            break;
    }

    MEMSET (pTrieInfo, RSVPTE_ZERO, sizeof (tuTrieInfo));
    MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID, (UINT1 *) pTrieInfo);

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHReleaseTrieInfo : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteMIHProcessMsgIdAcks
 * Description     : This function handles a list of incoming Message ID ACKs 
 *                   received from a given Neighbour received in response to
 *                   messages sent out with Message ID objects with ACK 
 *                   Desired Flag set.  If the Message ID Ack corresponds to
 *                   a message which installs a refreshable state (Path, 
 *                   Resv) the Ack Timer switches into Refresh Mode.  On the
 *                   other hand, if the Message ID Ack corresponds to a 
 *                   message which installs a non-refreshable state (PathErr,
 *                   ResvErr, PathTear, ResvTear), the Ack Timer gets deleted.
 *                   
 * Input (s)       : pNbrEntry      - Pointer to the neighbour entry from
 *                                    from which the Acks were received.
 *                   pMsgIdAckList  - Pointer to the list of Msg Id Objects
 *                                    for which Acks were received.
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHProcessMsgIdAcks (tNbrEntry * pNbrEntry, tTMO_SLL * pMsgIdAckList,
                         UINT4 u4NotifyAddr)
{
    INT4                i4RetVal;
    tRpteKey            RpteKey;
    tMsgIdObjNode      *pMsgIdObjNode = NULL;
    tuTrieInfo         *pTrieInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tIfEntry           *pIfEntry = NULL;
    tTimerParam        *pTimerParam = NULL;
    tTimerBlock        *pTimerBlock = NULL;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgIdAcks : ENTRY\n");

    /* Ack received for Error or Tear messages will result in deletion
     * of msgid from the database. All other associated memory will also
     * be released. */
    TMO_SLL_Scan (pMsgIdAckList, pMsgIdObjNode, tMsgIdObjNode *)
    {
        if (RpteNhGetAndDelNotifyRecipient (u4NotifyAddr,
                                            RPTE_MSG_ID_OBJ_NODE_MSG_ID
                                            (pMsgIdObjNode)) == RPTE_SUCCESS)
        {
            continue;
        }
        /* Scanning the local msg id data base */
        RpteKey.u4_Key = (UINT4)RPTE_MSG_ID_OBJ_NODE_MSG_ID (pMsgIdObjNode);
        i4RetVal = RpteTrieLookupEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                        &pTrieInfo);

        if (i4RetVal == MPLS_FAILURE)
        {
            /* Msg Id not present in the local msg id data base.
             * Process the next msg id */
            continue;
        }

        switch (RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo))
        {
            case RPTE_PATH_REFRESH:

                /* Extract the RsvpTe Tnl Info from the TRIE Info */
                pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);
                RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo) = pNbrEntry;

                /* Under normal circumstances, the timer type will be
                 * BackOff. */
                if (RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) !=
                    RPTE_NORMAL_TIMER)
                {
                    /* Set the PSB Timer type to normal and reset the refresh
                     * interval */
                    pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB
                                                 (pRsvpTeTnlInfo));
                    RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) =
                        RPTE_NORMAL_TIMER;
                    RPTE_TNL_PSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                        RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                                         DEFAULT_CONV_TO_SECONDS);
                    RptePhStartPathRefreshTmr (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
                }
                break;

            case RPTE_RESV_REFRESH:

                /* Extract the RsvpTe Tnl Info from the TRIE Info */
                pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);

                /* Under normal circumstances, the timer type will be
                 * BackOff. */
                if (RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) !=
                    RPTE_NORMAL_TIMER)
                {
                    /* Set the RSB Timer type to normal and reset the refresh
                     * interval */
                    pIfEntry = RSB_IN_IF_ENTRY (RSVPTE_TNL_RSB
                                                (pRsvpTeTnlInfo));

                    RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) =
                        RPTE_NORMAL_TIMER;
                    RPTE_TNL_RSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                        RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                                         DEFAULT_CONV_TO_SECONDS);
                    RpteRhStartResvRefreshTmr (RSVPTE_TNL_RSB (pRsvpTeTnlInfo));
                }
                break;

            case RPTE_PATHERR:

                pTimerBlock = RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo);
                pTimerParam = (tTimerParam *) & TIMER_BLK_TIMER_PARAM
                    (pTimerBlock);

                pRsvpTeTnlInfo = TIMER_PARAM_RPTE_TNL (pTimerParam);

                /* Timer associated with the error message is stopped */
                TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                              (pTimerBlock));

                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
                break;

            case RPTE_RESVERR:

                pTimerBlock = RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo);
                pTimerParam = (tTimerParam *) & TIMER_BLK_TIMER_PARAM
                    (pTimerBlock);

                pRsvpTeTnlInfo = TIMER_PARAM_RPTE_TNL (pTimerParam);

                /* Timer associated with the error message is stopped */
                TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                              (pTimerBlock));

                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TNL_RE_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
                break;

            case RPTE_PATHTEAR:

                pTimerBlock = RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo);
                pTimerParam = (tTimerParam *) & TIMER_BLK_TIMER_PARAM
                    (pTimerBlock);

                pRsvpTeTnlInfo = TIMER_PARAM_RPTE_TNL (pTimerParam);

                /* Timer associated with the tear message is deleted */
                TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                              (pTimerBlock));

                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TNL_PT_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;

                RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                if (RPTE_TE_TNL (pRsvpTeTnlInfo)->u1TnlRelStatus ==
                    TE_SIGMOD_TNLREL_AWAITED)
                {
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
                }
                else
                {
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
                }
                break;

            case RPTE_RESVTEAR:

                pTimerBlock = RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo);
                pTimerParam = (tTimerParam *) & TIMER_BLK_TIMER_PARAM
                    (pTimerBlock);

                pRsvpTeTnlInfo = TIMER_PARAM_RPTE_TNL (pTimerParam);

                /* Timer associated with the tear message is deleted */
                TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                              (pTimerBlock));

                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TNL_RT_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
                RpteUtlDeleteRsb (RSVPTE_TNL_RSB (pRsvpTeTnlInfo));

                RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                if (RPTE_TE_TNL (pRsvpTeTnlInfo)->u1TnlRelStatus ==
                    TE_SIGMOD_TNLREL_AWAITED)
                {
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
                }
                else
                {
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
                }
                break;

            case RPTE_GR_RECOVERY_PATH:

                pTimerBlock = RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo);
                pTimerParam = (tTimerParam *) & TIMER_BLK_TIMER_PARAM
                    (pTimerBlock);

                pRsvpTeTnlInfo = TIMER_PARAM_RPTE_TNL (pTimerParam);

                /* Timer associated with the error message is stopped */
                TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                              (pTimerBlock));

                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                pRsvpTeTnlInfo->pRPTimerBlock = NULL;
                break;

            default:
                break;
        }
    }
    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgIdAcks : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteMIHProcessMsgIdNacks
 * Description     : This function handles a list of incoming Message ID NACK
 *                   objects received from the given Neighbour in response
 *                   to an Srefresh Message sent to that Neighbour.  A lookup
 *                   is performed on the local Message ID Database to 
 *                   determine the installed State corresponding to the
 *                   Message ID NACK Object and the state is retransmitted
 *                   as a Path or Resv Trigger message.
 *
 * Input (s)       : pNbrEntry      - Pointer to the neighbour entry from
 *                                    from which the Nacks were received.
 *                   pMsgIdAckList  - Pointer to the list of Msg Id Objects
 *                                    for which the Nacks were received.
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHProcessMsgIdNacks (tNbrEntry * pNbrEntry, tTMO_SLL * pMsgIdNackList)
{
    tRpteKey            RpteKey;
    UINT4               u4MsgId;
    INT4                i4RetVal;
    tMsgIdObjNode      *pMsgIdObjNode = NULL;
    tuTrieInfo         *pTrieInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgIdNacks : ENTRY\n");

    TMO_SLL_Scan (pMsgIdNackList, pMsgIdObjNode, tMsgIdObjNode *)
    {
        u4MsgId = RPTE_MSG_ID_OBJ_NODE_MSG_ID (pMsgIdObjNode);
        if ((pMsgIdObjNode->MsgIdObj.u1Flags == RPTE_MSG_ID_RECOVERY_PATH) &&
            (pNbrEntry != NULL))
        {
            /* Scanning the local msg id data base */
            RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (pNbrEntry);
            RpteKey.u8_Key[RPTE_ONE] = u4MsgId;
            i4RetVal =
                RpteTrieLookupEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     &pTrieInfo);
        }
        else
        {
            RpteKey.u4_Key = u4MsgId;
            i4RetVal =
                RpteTrieLookupEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     &pTrieInfo);
        }
        if (i4RetVal == RPTE_FAILURE)
        {
            /* Msg Id not present in the local msg id data base */
            continue;
        }
        /*If the Nack message is for S Refresh RecoveryPath message,
         * then send seperate Recovery path message for each message
         * id in the list as per RFC 5063 section 5.3.3
         * */

        if (pMsgIdObjNode->MsgIdObj.u1Flags == RPTE_MSG_ID_RECOVERY_PATH)
        {
            pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);
            RpteHlprGenerateRecoveryPath (pRsvpTeTnlInfo);
        }

        switch (RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo))
        {
            case RPTE_PATH_REFRESH:

                /* Extract the RsvpTe Tnl Info from the TRIE Info */
                pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);

                /* Delete the msg id from the local msg id data base */
                RpteKey.u4_Key = u4MsgId;
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);
                /* After removing out path message id from trie, teTnlInfo
                 * pointer has to be removed from RBTree. RBTree removal has
                 * to be done only for out path message id, since only out
                 * path message id is used for SRefresh RecoveryPath message
                 * support
                 * */
                TeSigDelFromTeMsgIdBasedTable (pRsvpTeTnlInfo->pTeTnlInfo);

                RptePhPathRefresh (pRsvpTeTnlInfo);
                break;

            case RPTE_RESV_REFRESH:

                /* Extract the RsvpTe Tnl Info from the TRIE Info */
                pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);

                /* Delete the msg id from the local msg id data base */
                RpteKey.u4_Key = u4MsgId;
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                RpteRhResvRefresh (pRsvpTeTnlInfo);
                break;

            default:
                /* Should not happen since srefresh is sent only for path
                 * and resv refresh messages. Error Condition */
                break;
        }
    }
    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgIdNacks : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteMIHProcessSRefreshMsg
 * Description     : This function handles an incoming Srefresh Message
 *                   
 * Input (s)       : pPktMap - Pointer to the Packet Map Structure. 
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHProcessSRefreshMsg (tPktMap * pPktMap)
{
    UINT4               u4MsgIdsLen;
    UINT4               u4Epoch;
    UINT4               u4Index;
    UINT4               u4RetVal;
    UINT4               u4MsgId;
    UINT1               u1Flag;
    tTMO_SLL            MsgIdList;
    tRpteKey            RpteKey;

    tMsgIdListObj      *pMsgIdListObj = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tuTrieInfo         *pTrieInfo = NULL;
    tMsgIdObjNode      *pMsgIdObjNode = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessSRefreshMsg - ENTRY\n");

    TMO_SLL_Init (&MsgIdList);

    pMsgIdListObj = RPTE_PKT_MAP_MSG_ID_LIST (pPktMap);
    u4MsgIdsLen = OSIX_NTOHS (OBJ_HDR_LENGTH (&RPTE_MSG_ID_LIST_OBJ_HDR
                                              (pMsgIdListObj)));
    u4MsgIdsLen = u4MsgIdsLen - RSVP_OBJ_HDR_LEN - RPTE_FLAG_EPOCH_LEN;
    u4Epoch = OSIX_NTOHL (RPTE_MSG_ID_LIST_EPOCH (pMsgIdListObj));
    u1Flag = (UINT1) ((u4Epoch & RPTE_WORD_UPPER_1_BYTE_MASK)
                      >> RPTE_3_BYTE_SHIFT);
    u4Epoch = (u4Epoch & RPTE_WORD_LOWER_3_BYTE_MASK);

    pNbrEntry = RPTE_PKT_MAP_NBR_ENTRY (pPktMap);

    /* if the flag is RPTE_MSG_ID_RECOVERY_PATH, its S Refresh RecoveryPath
     * message. Get the message id's from the message and check whether
     * TE database is available for the message id. if TE database is not
     * available, send NACK message for the particular message id
     * */
    if (u1Flag == RPTE_MSG_ID_RECOVERY_PATH)
    {
        for (u4Index = RPTE_ZERO; (u4Index < u4MsgIdsLen / RPTE_MSG_ID_LEN);
             u4Index += RPTE_ONE)
        {
            u4MsgId =
                OSIX_NTOHL (RPTE_MSG_ID_LIST_MSG_ID (pMsgIdListObj, u4Index));
            if ((gi4MplsSimulateFailure == RPTE_SIM_FAILURE_NACK) ||
                (RpteGrProcessSRefreshRecoveryPathMsg (u4MsgId) ==
                 RPTE_FAILURE))
            {
                /* Since the message id received in the SRefresh RecoveryPath
                 * msg is not present in the TE database, Nack is sent */
                pMsgIdObjNode = (tMsgIdObjNode *)
                    RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_MSG_ID_LIST_POOL_ID);

                if (pMsgIdObjNode == NULL)
                {
                    RSVPTE_DBG (RSVPTE_MIH_MEM,
                                "Err - Memory allocation failed in "
                                "Constructing Nack list.\n");
                    continue;
                }

                TMO_SLL_Init_Node (&(pMsgIdObjNode->NextMsgIdObjNode));
                RPTE_MSG_ID_OBJ_NODE_EPOCH (pMsgIdObjNode) = u4Epoch;
                RPTE_MSG_ID_OBJ_NODE_MSG_ID (pMsgIdObjNode) = u4MsgId;
                RPTE_MSG_ID_OBJ_NODE_FLAGS (pMsgIdObjNode) = u1Flag;
                TMO_SLL_Add ((tTMO_SLL *) & (MsgIdList),
                             &(pMsgIdObjNode->NextMsgIdObjNode));
            }
        }
        if (TMO_SLL_Count (&MsgIdList) != RPTE_ZERO)
        {
            RpteMIHSendNackMsg (pNbrEntry, &MsgIdList);

            /* Release the Msg Id list Objects */
            pMsgIdObjNode = (tMsgIdObjNode *) TMO_SLL_First (&MsgIdList);
            while (pMsgIdObjNode != NULL)
            {
                TMO_SLL_Delete (&MsgIdList, &(pMsgIdObjNode->NextMsgIdObjNode));
                MemReleaseMemBlock (RSVPTE_MSG_ID_LIST_POOL_ID,
                                    (UINT1 *) pMsgIdObjNode);

                pMsgIdObjNode = (tMsgIdObjNode *) TMO_SLL_First (&MsgIdList);
            }
        }
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessSRefreshMsg - EXIT\n");
        return;
    }

    /* If the Nbr Epoch value is uninitialised, send NACK for all
     * Message ID's recvied in the List */
    if ((NBR_ENTRY_EPOCH_VALUE (pNbrEntry) == RPTE_UN_INIT_EPOCH) ||
        (NBR_ENTRY_EPOCH_VALUE (pNbrEntry) != u4Epoch))
    {
        NBR_ENTRY_EPOCH_VALUE (pNbrEntry) = u4Epoch;

        for (u4Index = RPTE_ZERO; (u4Index < u4MsgIdsLen / RPTE_MSG_ID_LEN);
             u4Index += RPTE_ONE)
        {
            u4MsgId = OSIX_NTOHL (RPTE_MSG_ID_LIST_MSG_ID (pMsgIdListObj,
                                                           u4Index));

            pMsgIdObjNode = (tMsgIdObjNode *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_MSG_ID_LIST_POOL_ID);
            if (pMsgIdObjNode == NULL)
            {
                RSVPTE_DBG (RSVPTE_MIH_MEM,
                            "Err - Memory allocation failed in "
                            "Constructing Nack list.\n");
                continue;
            }

            TMO_SLL_Init_Node (&(pMsgIdObjNode->NextMsgIdObjNode));
            RPTE_MSG_ID_OBJ_NODE_EPOCH (pMsgIdObjNode) = u4Epoch;
            RPTE_MSG_ID_OBJ_NODE_MSG_ID (pMsgIdObjNode) = u4MsgId;

            TMO_SLL_Add ((tTMO_SLL *) & (MsgIdList),
                         &(pMsgIdObjNode->NextMsgIdObjNode));

            NBR_ENTRY_MAX_MSG_ID (pNbrEntry) = u4MsgId;
        }
        if (NBR_ENTRY_EPOCH_VALUE (pNbrEntry) != u4Epoch)
        {

            /* Epoch Value has changed. Remove MessageIDs from data base */
            RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (pNbrEntry);
            RpteKey.u8_Key[RPTE_ONE] = RPTE_ZERO;
            RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                                 RPTE_NBR_MSG_ID_DB_NBR_PREFIX,
                                 RpteMIHReleaseTrieInfo);

        }

        if (TMO_SLL_Count (&MsgIdList) != RPTE_ZERO)
        {
            RpteMIHSendNackMsg (pNbrEntry, &MsgIdList);

            /* Release the Msg Id list Objects */
            pMsgIdObjNode = (tMsgIdObjNode *) TMO_SLL_First (&MsgIdList);
            while (pMsgIdObjNode != NULL)
            {
                TMO_SLL_Delete (&MsgIdList, &(pMsgIdObjNode->NextMsgIdObjNode));
                MemReleaseMemBlock (RSVPTE_MSG_ID_LIST_POOL_ID,
                                    (UINT1 *) pMsgIdObjNode);

                pMsgIdObjNode = (tMsgIdObjNode *) TMO_SLL_First (&MsgIdList);
            }
        }

        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessSRefreshMsg - EXIT\n");
        return;
    }
    /* Extract the individual message ids and preform the refresh
     * action. */
    for (u4Index = RPTE_ZERO; (u4Index < u4MsgIdsLen / RPTE_MSG_ID_LEN);
         u4Index += RPTE_ONE)
    {
        u4MsgId = OSIX_NTOHL (RPTE_MSG_ID_LIST_MSG_ID (pMsgIdListObj, u4Index));

        u4RetVal = RpteMIHProcessMsgIdList (pNbrEntry, u4MsgId, &pTrieInfo);

        if (u4RetVal == RPTE_MSG_ID_NOT_IN_DB)
        {
            /* Since the message id received in the SRefresh message
             * is not present in the message id database, Nack is sent */
            pMsgIdObjNode = (tMsgIdObjNode *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_MSG_ID_LIST_POOL_ID);

            if (pMsgIdObjNode == NULL)
            {
                RSVPTE_DBG (RSVPTE_MIH_MEM,
                            "Err - Memory allocation failed in "
                            "Constructing Nack list.\n");
                continue;
            }

            TMO_SLL_Init_Node (&(pMsgIdObjNode->NextMsgIdObjNode));
            RPTE_MSG_ID_OBJ_NODE_EPOCH (pMsgIdObjNode) = u4Epoch;
            RPTE_MSG_ID_OBJ_NODE_MSG_ID (pMsgIdObjNode) = u4MsgId;

            TMO_SLL_Add ((tTMO_SLL *) & (MsgIdList),
                         &(pMsgIdObjNode->NextMsgIdObjNode));
        }
        else
        {
            switch (RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo))
            {
                case RPTE_PATH_INCOMING:

                    /* Extract the RsvpTe Tnl Info from the TRIE Info */
                    pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);
                    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL)
                    {
                        /* This situation will happen when PATH is 
                         * received after RSVP is disabled */
                        break;
                    }

                    PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                        RpteUtlComputeLifeTime (PKT_MAP_IF_ENTRY (pPktMap),
                                                PSB_IN_REFRESH_INTERVAL
                                                (RSVPTE_TNL_PSB
                                                 (pRsvpTeTnlInfo)));
                    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) ==
                        RPTE_EGRESS)
                    {
                        RpteRhPhStartMaxWaitTmr (pRsvpTeTnlInfo,
                                                 PSB_TIME_TO_DIE
                                                 (RSVPTE_TNL_PSB
                                                  (pRsvpTeTnlInfo)));
                    }
                    break;

                case RPTE_RESV_INCOMING:
                    /* Extract the RsvpTe Tnl Info from the TRIE Info */
                    pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);
                    if (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) == NULL)
                    {
                        /* This situation will happen when RESV is 
                         * received after RSVP is disabled */
                        break;
                    }

                    RSB_TIME_TO_DIE (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)) =
                        RpteUtlComputeLifeTime (PKT_MAP_IF_ENTRY (pPktMap),
                                                RSB_IN_REFRESH_INTERVAL
                                                (RSVPTE_TNL_RSB
                                                 (pRsvpTeTnlInfo)));
                    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) ==
                        RPTE_INGRESS)
                    {
                        RpteRhPhStartMaxWaitTmr (pRsvpTeTnlInfo,
                                                 RSB_TIME_TO_DIE
                                                 (RSVPTE_TNL_RSB
                                                  (pRsvpTeTnlInfo)));
                    }
                    break;

                    /* Added for SRefresh RecoveryPath Support */
                case RPTE_PATH_REFRESH:
                    if (RpteGrProcessSRefreshRecoveryPathMsg (u4MsgId) ==
                        RPTE_FAILURE)
                    {
                        /* Since the message id received in the SRefresh RecoveryPath
                         * msg is not present in the TE database, Nack is sent */
                        pMsgIdObjNode = (tMsgIdObjNode *)
                            RSVP_ALLOCATE_MEM_BLOCK
                            (RSVPTE_MSG_ID_LIST_POOL_ID);

                        if (pMsgIdObjNode == NULL)
                        {
                            RSVPTE_DBG (RSVPTE_MIH_MEM,
                                        "Err - Memory allocation failed in "
                                        "Constructing Nack list.\n");
                            continue;
                        }

                        TMO_SLL_Init_Node (&(pMsgIdObjNode->NextMsgIdObjNode));
                        RPTE_MSG_ID_OBJ_NODE_EPOCH (pMsgIdObjNode) = u4Epoch;
                        RPTE_MSG_ID_OBJ_NODE_MSG_ID (pMsgIdObjNode) = u4MsgId;

                        TMO_SLL_Add ((tTMO_SLL *) & (MsgIdList),
                                     &(pMsgIdObjNode->NextMsgIdObjNode));
                    }
                    break;

                default:
                    break;
            }
        }
    }

    if (TMO_SLL_Count (&MsgIdList) != RPTE_ZERO)
    {
        RpteMIHSendNackMsg (pNbrEntry, &MsgIdList);

        /* Release the Msg Id list Objects */
        pMsgIdObjNode = (tMsgIdObjNode *) TMO_SLL_First (&MsgIdList);
        while (pMsgIdObjNode != NULL)
        {
            TMO_SLL_Delete (&MsgIdList, &(pMsgIdObjNode->NextMsgIdObjNode));
            MemReleaseMemBlock (RSVPTE_MSG_ID_LIST_POOL_ID,
                                (UINT1 *) pMsgIdObjNode);

            pMsgIdObjNode = (tMsgIdObjNode *) TMO_SLL_First (&MsgIdList);
        }
    }
    RpteUtlCleanPktMap (pPktMap);
    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessSRefreshMsg - EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRRNbrAutoDiscovery
 * Description     : This function is used for Auto Discovery of a 
 *                   Neighbouring LSRs RR Capability depending on the
 *                   RR bit in the Version Flag of an incoming message
 * Input (s)       : pNbrEntry         - Pointer to the neighbour entry
 *                   u1RsvpVersionFlag - RSVP Version flag received in the
 *                                       message.
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteRRNbrAutoDiscovery (tNbrEntry * pNbrEntry, UINT1 u1RsvpVersionFlag)
{
    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRNbrAutoDiscovery - ENTRY\n");

    if (gu1RRCapable == RPTE_ENABLED)
    {
        /* Only when the global RR is enabled, the RSVP Hdr Flag is
         * analysed */
        NBR_ENTRY_RR_CAPABLE (pNbrEntry) = RPTE_ENABLED;
        if (RSVP_IS_RR_BIT_SET (u1RsvpVersionFlag) == RPTE_FALSE)
        {
            NBR_ENTRY_RR_STATE (pNbrEntry) = RPTE_DISABLED;
        }
        else
        {
            NBR_ENTRY_RR_STATE (pNbrEntry) = RPTE_ENABLED;
        }
    }
    else
    {
        NBR_ENTRY_RR_CAPABLE (pNbrEntry) = RPTE_DISABLED;
        NBR_ENTRY_RR_STATE (pNbrEntry) = RPTE_DISABLED;
    }

    if (gu1MsgIdCapable == RPTE_ENABLED)
    {
        NBR_ENTRY_RMD_CAPABLE (pNbrEntry) = RPTE_ENABLED;
    }
    else
    {
        NBR_ENTRY_RMD_CAPABLE (pNbrEntry) = RPTE_DISABLED;
    }

    /* Epoch value is set to uninitialised value */
    NBR_ENTRY_EPOCH_VALUE (pNbrEntry) = RPTE_UN_INIT_EPOCH;

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRNbrAutoDiscovery - EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRRUpdateNbrEntry
 * Description     : This function is used for updating the neighbour's RR
 *                   capability by examining the RR-bit in the version flag
 *                   of the incoming message.
 *
 * Input (s)       : pNbrEntry         - Pointer to the neighbour entry
 *                   u1RsvpVersionFlag - RSVP Version flag received in the
 *                                       message.
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteRRUpdateNbrEntry (tNbrEntry * pNbrEntry, UINT1 u1RsvpVersionFlag)
{
    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRUpdateNbrEntry - ENTRY\n");

    /* The RR Bit needs to be analysed only when the
     * Neighbour_RR_Capable is enabled */
    if (NBR_ENTRY_RR_CAPABLE (pNbrEntry) == RPTE_ENABLED)
    {
        /* Depending on the RR Bit the Neighbour_RR_State is
         * updated */
        if (RSVP_IS_RR_BIT_SET (u1RsvpVersionFlag) == RPTE_FALSE)
        {
            NBR_ENTRY_RR_STATE (pNbrEntry) = RPTE_DISABLED;
        }
        else if (RSVP_IS_RR_BIT_SET (u1RsvpVersionFlag) == RPTE_TRUE)
        {
            NBR_ENTRY_RR_STATE (pNbrEntry) = RPTE_ENABLED;
        }
    }

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRUpdateNbrEntry - EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRRSRefreshThreshold
 * Description     : This function is invoked when the SRefresh List of
 *                   the neighbour reaches it's threshold. The SRefresh
 *                   Message is constructed and sent to PB module.
 * Input (s)       : pNbrEntry - Pointer to the neighbour entry
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteRRSRefreshThreshold (tNbrEntry * pNbrEntry, UINT1 u1Flags)
{
    tPktMap             PktMap;
    UINT2               u2SRefSize;
    UINT2               u2MsgIdListLen;
    UINT2               u2MsgSize;
    UINT4               u4Epoch;
    UINT4               u4MsgId;

    UINT1              *pu1Pdu = NULL;
    tIfEntry           *pIfEntry = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tObjHdr            *pObjHdr = NULL;
    tMsgIdObjNode      *pMsgIdObjNode = NULL;

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRSRefreshThreshold - ENTRY\n");

    if ((NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) != RPTE_TRUE) ||
        (TMO_SLL_Count (&NBR_ENTRY_SREFRESH_LIST (pNbrEntry)) == RPTE_ZERO))
    {
        RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRSRefreshThreshold - IMTD-EXIT\n");
        return;
    }

    pIfEntry = NBR_ENTRY_IF_ENTRY (pNbrEntry);
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &NBR_ENTRY_SREFRESH_TMR (pNbrEntry));

    /* Initialize packet map */
    RpteUtlInitPktMap (&PktMap);

    u2MsgIdListLen = (UINT2) (sizeof (tObjHdr) + RPTE_FLAG_EPOCH_LEN +
                              (TMO_SLL_Count (&NBR_ENTRY_SREFRESH_LIST
                                              (pNbrEntry)) * RPTE_MSG_ID_LEN));

    u2SRefSize = (UINT2) (sizeof (tRsvpHdr) + u2MsgIdListLen);
    u2MsgSize = (UINT2) (sizeof (tRsvpHdr) + u2SRefSize);

    PKT_MAP_RSVP_PKT (&PktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);

    if (PKT_MAP_RSVP_PKT (&PktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_RR_PRCS,
                    "Failed to allocate memory for the packet \n");
        RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRSRefreshThreshold : INTMD-EXIT\n");

        /* Since malloc for the linear buffer failed, the Srefresh List 
         * is freed */
        pMsgIdObjNode = (tMsgIdObjNode *)
            TMO_SLL_First (&(NBR_ENTRY_SREFRESH_LIST (pNbrEntry)));

        while (pMsgIdObjNode != NULL)
        {
            TMO_SLL_Delete (&(NBR_ENTRY_SREFRESH_LIST (pNbrEntry)),
                            &(pMsgIdObjNode->NextMsgIdObjNode));

            MemReleaseMemBlock (RSVPTE_MSG_ID_LIST_POOL_ID,
                                (UINT1 *) pMsgIdObjNode);

            pMsgIdObjNode = (tMsgIdObjNode *)
                TMO_SLL_First (&(NBR_ENTRY_SREFRESH_LIST (pNbrEntry)));
        }
        return;
    }

    MEMSET (PKT_MAP_RSVP_PKT (&PktMap), RPTE_ZERO, u2MsgSize);

    PKT_MAP_RSVP_PKT_ALLOC (&PktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    PKT_MAP_SRC_ADDR (&PktMap) = IF_ENTRY_ADDR (pIfEntry);
    PKT_MAP_DST_ADDR (&PktMap) = NBR_ENTRY_ADDR (pNbrEntry);

    PKT_MAP_RA_OPT_NEEDED (&PktMap) = RPTE_NO;
    PKT_MAP_OPT_LEN (&PktMap) = RPTE_ZERO;

    PKT_MAP_OUT_IF_ENTRY (&PktMap) = pIfEntry;
    RPTE_PKT_MAP_NBR_ENTRY (&PktMap) = pNbrEntry;

    /* Filling the Transmit TTL value with the default value */
    PKT_MAP_TX_TTL (&PktMap) = DEFAULT_TTL;

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (&PktMap);
    MEMSET (pRsvpHdr, RPTE_ZERO, sizeof (tRsvpHdr));

    /* Rsvp Bundle Header */
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = BUNDLE_MSG;
    RSVP_HDR_SEND_TTL (pRsvpHdr) = DEFAULT_TTL;
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (u2MsgSize);
    pRsvpHdr = (tRsvpHdr *) (VOID *) ((UINT1 *) pRsvpHdr + sizeof (tRsvpHdr));

    /* Rsvp SRefresh Msg Header */
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS) |
                                    RSVP_HDR_FLAG_WITH_RR);
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = SREFRESH_MSG;
    RSVP_HDR_SEND_TTL (pRsvpHdr) = DEFAULT_TTL;
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (u2SRefSize);

    pObjHdr = (tObjHdr *) (VOID *) ((UINT1 *) pRsvpHdr + sizeof (tRsvpHdr));

    /* Message Id List Object */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (u2MsgIdListLen);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS
        (RPTE_MESSAGE_ID_LIST_CLASS_NUM_TYPE);

    pu1Pdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

    /* Filling the Epoch Value */
    u4Epoch = (gu4Epoch | (UINT4)(u1Flags << RPTE_3_BYTE_SHIFT));
    u4Epoch = OSIX_HTONL (u4Epoch);

    MEMCPY ((UINT1 *) pu1Pdu, (UINT1 *) &u4Epoch, RPTE_FLAG_EPOCH_LEN);
    pu1Pdu = pu1Pdu + RPTE_FLAG_EPOCH_LEN;

    /* Filling the Message Ids */
    TMO_SLL_Scan ((tTMO_SLL *) (&NBR_ENTRY_SREFRESH_LIST (pNbrEntry)),
                  pMsgIdObjNode, tMsgIdObjNode *)
    {
        u4MsgId = OSIX_HTONL (RPTE_MSG_ID_OBJ_NODE_MSG_ID (pMsgIdObjNode));
        MEMCPY ((UINT1 *) pu1Pdu, (UINT1 *) &u4MsgId, RPTE_MSG_ID_LEN);
        pu1Pdu = pu1Pdu + RPTE_MSG_ID_LEN;
    }

    PKT_MAP_BUILD_PKT (&PktMap) = RPTE_YES;

    RptePbSendMsg (&PktMap);

    NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) = RPTE_FALSE;

    /* Free the Srefresh List */
    pMsgIdObjNode = (tMsgIdObjNode *)
        TMO_SLL_First (&(NBR_ENTRY_SREFRESH_LIST (pNbrEntry)));

    while (pMsgIdObjNode != NULL)
    {
        TMO_SLL_Delete (&(NBR_ENTRY_SREFRESH_LIST (pNbrEntry)),
                        &(pMsgIdObjNode->NextMsgIdObjNode));

        MemReleaseMemBlock (RSVPTE_MSG_ID_LIST_POOL_ID,
                            (UINT1 *) pMsgIdObjNode);

        pMsgIdObjNode = (tMsgIdObjNode *)
            TMO_SLL_First (&(NBR_ENTRY_SREFRESH_LIST (pNbrEntry)));
    }

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRSRefreshThreshold - EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRRProcessSRefreshTimeOut 
 * Description     : This function is invoked in the case of SRefresh Timer
 *                   timing out. RpteRRSRefreshThreshold is called for
 *                   sending the SRefreshMessage.
 * Input (s)       : pAppTimer - Pointer to the App Timer
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteRRProcessSRefreshTimeOut (const tTmrAppTimer * pAppTimer)
{
    tTimerParam        *pTmrParam = NULL;

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessSRefreshTimeOut - ENTRY\n");

    pTmrParam = (tTimerParam *) RSVP_TIMER_NAME (pAppTimer);
    RpteRRSRefreshThreshold (TIMER_PARAM_NBR_ENTRY (pTmrParam), RPTE_ZERO);

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRProcessSRefreshTimeOut - EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRRStartSRefresh
 * Description     : This function starts the SRefresh Timer for the
 *                   neighbour.
 * Input (s)       : pNbrEntry - Pointer to the Neighbour Entry
 * Output (s)      : None
 * Returns         : RPTE_SUCCESS or RPTE_FAILURE
 */
/****************************************************************************/
INT1
RpteRRStartSRefresh (tNbrEntry * pNbrEntry)
{
    tTimerParam        *pTimerParam = NULL;

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRStartSRefresh : ENTRY\n");

    /* Start Srefresh Timer */
    pTimerParam = &NBR_ENTRY_SREFRESH_TMR_PARAM (pNbrEntry);

    TIMER_PARAM_NBR_ENTRY (pTimerParam) = pNbrEntry;
    TIMER_PARAM_ID (pTimerParam) = RPTE_SREFRESH;
    RSVP_TIMER_NAME (&NBR_ENTRY_SREFRESH_TMR (pNbrEntry)) =
        (FS_ULONG) pTimerParam;
    if (RpteUtlStartTimer
        (RSVP_GBL_TIMER_LIST, &NBR_ENTRY_SREFRESH_TMR (pNbrEntry),
         (((IF_ENTRY_REFRESH_INTERVAL (NBR_ENTRY_IF_ENTRY (pNbrEntry)) /
            (RPTE_TWO * DEFAULT_CONV_TO_SECONDS))) *
          SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) = RPTE_FALSE;
        RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRStartSRefresh : INTMD-EXIT\n");
        return RPTE_FAILURE;
    }
    NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) = RPTE_TRUE;
    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRStartSRefresh : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteRRSRefreshAddMsgId
 * Description     : This function adds the given message id to the SRefresh
 *                   list of the neighbour.
 *                  
 * Input (s)       : pNbrEntry - Pointer to the neighbour entry
 *                   u4MsgId   - Message id that is to be added to the
 *                               SRefresh list
 * Output (s)      : None
 * Returns         : RPTE_SUCCESS or RPTE_FAILURE
 */
/****************************************************************************/
INT1
RpteRRSRefreshAddMsgId (tNbrEntry * pNbrEntry, UINT4 u4MsgId, UINT1 u1Flags)
{
    tMsgIdObjNode      *pMsgIdObjNode = NULL;

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRSRefreshAddMsgId - ENTRY\n");

    /* Add New Msg Id to Srefresh List in pNbrEntry */
    pMsgIdObjNode = (tMsgIdObjNode *)
        RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_MSG_ID_LIST_POOL_ID);
    if (pMsgIdObjNode == NULL)
    {
        RSVPTE_DBG (RSVPTE_RR_MEM,
                    "Err - Memory allocation failed in SRefresh "
                    "Add Msg Id\n");
        RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRSRefreshAddMsgId - INTMD-EXIT\n");
        return RPTE_FAILURE;
    }

    TMO_SLL_Init_Node (&(pMsgIdObjNode->NextMsgIdObjNode));
    RPTE_MSG_ID_OBJ_NODE_MSG_ID (pMsgIdObjNode) = u4MsgId;
    RPTE_MSG_ID_OBJ_NODE_FLAGS (pMsgIdObjNode) = u1Flags;

    TMO_SLL_Add ((tTMO_SLL *) (&NBR_ENTRY_SREFRESH_LIST (pNbrEntry)),
                 &(pMsgIdObjNode->NextMsgIdObjNode));

    if (NBR_ENTRY_SREFRESH_LIST_SIZE (pNbrEntry) + RPTE_MSG_ID_LEN
        == NBR_ENTRY_IF_MTU (pNbrEntry))
    {
        RpteRRSRefreshThreshold (pNbrEntry, u1Flags);
    }

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRSRefreshAddMsgId - ENTRY\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteRRSendSRefreshMsg
 * Description     : This function is used to accumulate the Message IDs of
 *                   Tunnels who Path or Resv state are to be refreshed in 
 *                   an outgoing Srefresh message
 * Input (s)       : pRsvpTeTnlInfo - Pointer to the tunnel Info for which
 *                                    a refresh needs to be sent.
 *                   u1MsgType      - Message Type (Path / Resv)
 * Output (s)      : None
 * Returns         : RPTE_SUCCESS or RPTE_FAILURE
 */
/****************************************************************************/
INT1
RpteRRSendSRefreshMsg (const tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 u1MsgType)
{
    UINT4               u4MsgId;

    tNbrEntry          *pNbrEntry = NULL;

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRSendSRefreshMsg - ENTRY\n");

    switch (u1MsgType)
    {
        case PATH_MSG:

            u4MsgId = RPTE_TNL_OUT_PATH_MSG_ID (pRsvpTeTnlInfo);
            pNbrEntry = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
            break;

        case RESV_MSG:

            u4MsgId = RPTE_TNL_OUT_RESV_MSG_ID (pRsvpTeTnlInfo);
            pNbrEntry = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);
            break;

        default:
            /* Should not happen since srefresh is sent only for path
             * and resv refresh messages. Error Condition */
            return RPTE_FAILURE;
    }

    if (NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) == RPTE_TRUE)
    {
        /* If the total size of the SRefresh message goes beyond
         * the Interface MTU, then SRefresh Msg is sent out
         * immediately. */
        if ((NBR_ENTRY_SREFRESH_LIST_SIZE (pNbrEntry) + RPTE_MSG_ID_LEN)
            > NBR_ENTRY_IF_MTU (pNbrEntry))
        {
            RpteRRSRefreshThreshold (pNbrEntry, RPTE_ZERO);
            if (RpteRRStartSRefresh (pNbrEntry) != RPTE_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_RR_ETEXT,
                            "RpteRRSendSRefreshMsg - INTMD-EXIT\n");
                return RPTE_FAILURE;
            }
        }
    }
    else
    {
        if (RpteRRStartSRefresh (pNbrEntry) != RPTE_SUCCESS)
        {
            RSVPTE_DBG (RSVPTE_RR_ETEXT,
                        "RpteRRSendSRefreshMsg - INTMD-EXIT\n");
            return RPTE_FAILURE;
        }
    }

    /* Add the new message id to the SRefresh Message */
    if (RpteRRSRefreshAddMsgId (pNbrEntry, u4MsgId, RPTE_ZERO) != RPTE_SUCCESS)
    {
        /* If SRefresh Add Msg Id has failed for the first MsgId
         * then SRefresh timer must be stopped */
        if (TMO_SLL_Count (&NBR_ENTRY_SREFRESH_LIST (pNbrEntry)) == RPTE_ZERO)
        {
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &NBR_ENTRY_SREFRESH_TMR (pNbrEntry));
            NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) = RPTE_FALSE;
        }
        RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRSendSRefreshMsg - INTMD-EXIT\n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRSendSRefreshMsg - EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteRRCalculateBackOffDelay
 * Description     : This function calculates the New Delay for a given
 *                   input Current Delay after providing the required Backoff
 *                   
 * Input (s)       : pIfEntry    - Pointer to the IfEntry on which the
 *                                 packet needs to be sent.
 *                   u4CurrDelay - Current delay.
 * Output (s)      : pu4NewDelay - Pointer to the new delay.
 * Returns         : RPTE_SUCCESS or RPTE_FAILURE
 */
/****************************************************************************/
INT4
RpteRRCalculateBackOffDelay (const tIfEntry * pIfEntry, UINT4 u4CurrDelay,
                             UINT4 *pu4NewDelay)
{
    UINT4               u4NewDelay;
    UINT4               u4BackOffThreshold;

    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRCalculateBackOffDelay - ENTRY\n");

    u4NewDelay = (UINT4) ((u4CurrDelay * RPTE_BACKOFF_DELTA) +
                          RPTE_ROUNDOFF_VALUE);
    u4BackOffThreshold = (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) *
                          RPTE_BACK_OFF_FACTOR) / DEFAULT_CONV_TO_SECONDS;

    if (u4NewDelay <= u4BackOffThreshold)
    {
        *pu4NewDelay = u4NewDelay;
        RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRCalculateBackOffDelay - EXIT\n");
        return RPTE_SUCCESS;
    }

    /* In the case of Back Off Timer reaching max val */
    RSVPTE_DBG (RSVPTE_RR_ETEXT, "RpteRRCalculateBackOffDelay - INTMD-EXIT\n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteMIHProcessRMDTimeOut
 * Description     : This function handles the expiry of the Reliable Message
 *                   Delivery (RMD) ACK Timer Timeout.  It performs the 
 *                   required Backoff and retransmits the Message until the
 *                   Backoff Timer Duration exceeds a threshold
 *                   
 * Input (s)       : pExpiredTmr - Pointer to the expired timer parameter.
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHProcessRMDTimeOut (const tTmrAppTimer * pExpiredTmr)
{
    tRsvpTeSsnObj       RsvpTeSsnObj;
    tRsvpTeSndrTmpObj   RsvpTeSndrTmpObj;
    tRsvpTeFilterSpecObj RsvpTeFilterSpecObj;
    tFlowSpecObj        FlowSpecObj;
    tPktMap             PktMap;
    tRpteKey            RpteKey;
    tErrorSpecObj       ErrorSpecObj;
    tStyleObj           StyleObj;
    UINT4               u4TimerType;
    UINT2               u2FSpecObjLength = 0;

    tTimerBlock        *pTimerBlock = NULL;
    tTimerParam        *pTimerParam = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsb               *pRsb = NULL;
    tObjHdr            *pObjHdr = NULL;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessRMDTimeOut - ENTRY\n");

    RpteUtlInitPktMap (&PktMap);
    MEMSET (&RsvpTeSsnObj, RPTE_ZERO, sizeof (tRsvpTeSsnObj));
    MEMSET (&RsvpTeSndrTmpObj, RPTE_ZERO, sizeof (tRsvpTeSndrTmpObj));
    MEMSET (&RsvpTeFilterSpecObj, RPTE_ZERO, sizeof (tRsvpTeFilterSpecObj));
    MEMSET (&FlowSpecObj, RPTE_ZERO, sizeof (tFlowSpecObj));
    MEMSET (&StyleObj, RPTE_ZERO, sizeof (tStyleObj));
    PktMap.pRsvpTeSsnObj = &RsvpTeSsnObj;
    PktMap.pRsvpTeSndrTmpObj = &RsvpTeSndrTmpObj;
    PktMap.pStyleObj = &StyleObj;
    PktMap.pRsvpTeFilterSpecObj = &RsvpTeFilterSpecObj;
    PktMap.pFlowSpecObj = &FlowSpecObj;

    /* Control reaches here when RMD Timer times out for any of the */
    /* Tear or Error messages */

    pTimerParam = (tTimerParam *) RSVP_TIMER_NAME (pExpiredTmr);
    pTimerBlock = (tTimerBlock *) (VOID *) ((UINT1 *) pTimerParam - RPTE_OFFSET
                                            (tTimerBlock, TmrParam));
    pRsvpTeTnlInfo = TIMER_PARAM_RPTE_TNL (pTimerParam);
    u4TimerType = TIMER_PARAM_ID ((tTimerParam *) RSVP_TIMER_NAME
                                  (pExpiredTmr));

    switch (u4TimerType)
    {
            /* Relevant details are filled in the PktMap before
             * calling send of the corresponding message.
             * If the calculated BackOff time has exceeded the limit
             * RMD timer is stopped and the timer block is 
             * released. */

        case RPTE_PATHERR:

            PKT_MAP_OUT_IF_ENTRY (&PktMap) = PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                                           (pRsvpTeTnlInfo));
            PKT_MAP_DST_ADDR (&PktMap) =
                RSVP_HOP_ADDR (&PSB_RSVP_HOP (pRsvpTeTnlInfo->pPsb));
            PKT_MAP_RPTE_SSN_TNL_ID (&PktMap) =
                (UINT2) RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);
            PKT_MAP_RPTE_SNDR_TMP_LSPID (&PktMap) =
                (UINT2) RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);
            PKT_MAP_RPTE_SSN_TNL_ID (&PktMap) =
                OSIX_HTONS (PKT_MAP_RPTE_SSN_TNL_ID (&PktMap));
            PKT_MAP_RPTE_SNDR_TMP_LSPID (&PktMap) =
                OSIX_HTONS (PKT_MAP_RPTE_SNDR_TMP_LSPID (&PktMap));
            MEMCPY ((UINT1 *) &PKT_MAP_RPTE_ST_TNL_SNDR_ADDR (&PktMap),
                    (UINT1 *)
                    &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                    RSVPTE_IPV4ADR_LEN);
            MEMCPY ((UINT1 *) &PKT_MAP_RPTE_SSN_DEST_ADDR (&PktMap),
                    (UINT1 *)
                    &(RSVPTE_TNL_EGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                    RSVPTE_IPV4ADR_LEN);
            MEMCPY ((UINT1 *) &PKT_MAP_RPTE_SSN_EXTN_TNL_ID (&PktMap),
                    (UINT1 *)
                    &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                    RSVPTE_IPV4ADR_LEN);
            if (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) == NULL)
            {
                PKT_MAP_IF_ENTRY (&PktMap) = PKT_MAP_OUT_IF_ENTRY (&PktMap);
            }
            else
            {
                PKT_MAP_IF_ENTRY (&PktMap) = PSB_OUT_IF_ENTRY
                    (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
            }

            PKT_MAP_ERROR_SPEC_OBJ (&PktMap) = &ErrorSpecObj;
            MEMCPY ((UINT1 *) &ERROR_SPEC_OBJ (PKT_MAP_ERROR_SPEC_OBJ
                                               (&PktMap)),
                    (UINT1 *) &TIMER_BLK_ERROR_SPEC (pTimerBlock),
                    sizeof (tErrorSpec));

            PKT_MAP_BUILD_PKT (&PktMap) = RPTE_YES;
            RPTE_PKT_MAP_NBR_ENTRY (&PktMap) = RPTE_TNL_USTR_NBR
                (pRsvpTeTnlInfo);
            RPTE_PKT_MAP_TNL_INFO (&PktMap) = pRsvpTeTnlInfo;

            if (RpteRRCalculateBackOffDelay (PKT_MAP_OUT_IF_ENTRY (&PktMap),
                                             TIMER_BLK_BACKOFF_INTERVAL
                                             (pTimerBlock),
                                             &TIMER_BLK_BACKOFF_INTERVAL
                                             (pTimerBlock)) == RPTE_SUCCESS)
            {
                RptePvmConstructAndSndPEMsg (&PktMap);
            }
            else
            {
                /* Release from the local msg id data base */
                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);
                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
            }
            break;

        case RPTE_RESVERR:
            pRsb = RSVPTE_TNL_RSB (pRsvpTeTnlInfo);

            PKT_MAP_DST_ADDR (&PktMap) =
                RSVP_HOP_ADDR (&RSB_NEXT_RSVP_HOP (pRsb));
            PKT_MAP_RPTE_SSN_TNL_ID (&PktMap) =
                (UINT2) RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);
            PKT_MAP_RPTE_FLTR_SPEC_LSPID (&PktMap) =
                (UINT2) RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);
            PKT_MAP_RPTE_SSN_TNL_ID (&PktMap) =
                OSIX_HTONS (PKT_MAP_RPTE_SSN_TNL_ID (&PktMap));
            PKT_MAP_RPTE_FLTR_SPEC_LSPID (&PktMap) =
                OSIX_HTONS (PKT_MAP_RPTE_FLTR_SPEC_LSPID (&PktMap));
            MEMCPY ((UINT1 *) &PKT_MAP_RPTE_FS_TNL_SNDR_ADDR (&PktMap),
                    (UINT1 *)
                    &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                    RSVPTE_IPV4ADR_LEN);
            MEMCPY ((UINT1 *) &PKT_MAP_RPTE_SSN_DEST_ADDR (&PktMap),
                    (UINT1 *)
                    &(RSVPTE_TNL_EGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                    RSVPTE_IPV4ADR_LEN);
            MEMCPY ((UINT1 *) &PKT_MAP_RPTE_SSN_EXTN_TNL_ID (&PktMap),
                    (UINT1 *)
                    &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                    RSVPTE_IPV4ADR_LEN);

            /* Filling style object in PktMap */
            pObjHdr = (tObjHdr *) PKT_MAP_STYLE_OBJ (&PktMap);
            OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tStyleObj));
            OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
                OSIX_HTONS (IPV4_STYLE_CLASS_NUM_TYPE);
            STYLE_OBJ ((tStyleObj *) pObjHdr) = RSB_STYLE (pRsb);

            /* Filling Flow Spec Object in PktMap */
            pObjHdr = (tObjHdr *) PKT_MAP_FLOW_SPEC_OBJ (&PktMap);

            if (((tFlowSpecObj *) PKT_MAP_FLOW_SPEC_OBJ
                 (&PktMap))->FlowSpec.SrvHdr.u1SrvID == FLOWSPEC_CLS_HDR_NUMBER)
            {
                u2FSpecObjLength = sizeof (tFlowSpecObj) - sizeof (tGsRSpec);
            }
            else
            {
                u2FSpecObjLength = sizeof (tFlowSpecObj);
            }

            OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (u2FSpecObjLength);
            OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
                OSIX_HTONS (IPV4_FLOW_SPEC_CLASS_NUM_TYPE);
            FLOW_SPEC_OBJ ((tFlowSpecObj *) (VOID *) pObjHdr) =
                RSB_FLOW_SPEC (pRsb);

            PKT_MAP_OUT_IF_ENTRY (&PktMap) = RSB_OUT_IF_ENTRY (pRsb);
            PKT_MAP_DST_ADDR (&PktMap) = RSVP_HOP_ADDR
                (&RSB_NEXT_RSVP_HOP (pRsb));

            if (RSB_IN_IF_ENTRY (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)) == NULL)
            {
                PKT_MAP_IF_ENTRY (&PktMap) = PKT_MAP_OUT_IF_ENTRY (&PktMap);
            }
            else
            {
                PKT_MAP_IF_ENTRY (&PktMap) = RSB_IN_IF_ENTRY
                    (RSVPTE_TNL_RSB (pRsvpTeTnlInfo));
            }

            RPTE_PKT_MAP_NBR_ENTRY (&PktMap) = RPTE_TNL_DSTR_NBR
                (pRsvpTeTnlInfo);
            RPTE_PKT_MAP_TNL_INFO (&PktMap) = pRsvpTeTnlInfo;

            PKT_MAP_BUILD_PKT (&PktMap) = RPTE_YES;

            PKT_MAP_ERROR_SPEC_OBJ (&PktMap) = &ErrorSpecObj;
            MEMCPY ((UINT1 *)
                    &ERROR_SPEC_OBJ (PKT_MAP_ERROR_SPEC_OBJ (&PktMap)),
                    (UINT1 *) &TIMER_BLK_ERROR_SPEC (pTimerBlock),
                    sizeof (tErrorSpec));

            if (RpteRRCalculateBackOffDelay (PKT_MAP_OUT_IF_ENTRY (&PktMap),
                                             TIMER_BLK_BACKOFF_INTERVAL
                                             (pTimerBlock),
                                             &TIMER_BLK_BACKOFF_INTERVAL
                                             (pTimerBlock)) == RPTE_SUCCESS)
            {
                RptePvmConstructAndSndREMsg (&PktMap);
            }
            else
            {
                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TNL_RE_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
            }
            break;

        case RPTE_PATHTEAR:

            PKT_MAP_OUT_IF_ENTRY (&PktMap) = PSB_OUT_IF_ENTRY
                (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
            RPTE_PKT_MAP_NBR_ENTRY (&PktMap) = RPTE_TNL_DSTR_NBR
                (pRsvpTeTnlInfo);
            PKT_MAP_TX_TTL (&PktMap) = TIMER_BLK_TX_TTL_VALUE (pTimerBlock);
            RPTE_PKT_MAP_TNL_INFO (&PktMap) = pRsvpTeTnlInfo;
            PKT_MAP_BUILD_PKT (&PktMap) = RPTE_YES;

            PKT_MAP_DST_ADDR (&PktMap) =
                RSVP_HOP_ADDR (&PSB_RSVP_HOP (pRsvpTeTnlInfo->pPsb));
            PKT_MAP_RPTE_SSN_TNL_ID (&PktMap) =
                (UINT2) RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);
            PKT_MAP_RPTE_SNDR_TMP_LSPID (&PktMap) =
                (UINT2) RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);
            PKT_MAP_RPTE_SSN_TNL_ID (&PktMap) =
                OSIX_HTONS (PKT_MAP_RPTE_SSN_TNL_ID (&PktMap));
            PKT_MAP_RPTE_SNDR_TMP_LSPID (&PktMap) =
                OSIX_HTONS (PKT_MAP_RPTE_SNDR_TMP_LSPID (&PktMap));
            MEMCPY ((UINT1 *) &PKT_MAP_RPTE_ST_TNL_SNDR_ADDR (&PktMap),
                    (UINT1 *)
                    &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                    RSVPTE_IPV4ADR_LEN);
            MEMCPY ((UINT1 *) &PKT_MAP_RPTE_SSN_DEST_ADDR (&PktMap),
                    (UINT1 *)
                    &(RSVPTE_TNL_EGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                    RSVPTE_IPV4ADR_LEN);
            MEMCPY ((UINT1 *) &PKT_MAP_RPTE_SSN_EXTN_TNL_ID (&PktMap),
                    (UINT1 *)
                    &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                    RSVPTE_IPV4ADR_LEN);

            if (RpteRRCalculateBackOffDelay (PKT_MAP_OUT_IF_ENTRY (&PktMap),
                                             TIMER_BLK_BACKOFF_INTERVAL
                                             (pTimerBlock),
                                             &TIMER_BLK_BACKOFF_INTERVAL
                                             (pTimerBlock)) == RPTE_SUCCESS)
            {
                RptePhConstructAndSndPTMsg (&PktMap, pRsvpTeTnlInfo);
            }
            else
            {
                /* Release from the local msg id data base */
                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TNL_PT_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
                RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                if (RPTE_TE_TNL (pRsvpTeTnlInfo)->u1TnlRelStatus ==
                    TE_SIGMOD_TNLREL_AWAITED)
                {
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
                }
                else
                {
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
                }
            }
            break;

        case RPTE_RESVTEAR:

            if (RpteRRCalculateBackOffDelay (PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                                           (pRsvpTeTnlInfo)),
                                             TIMER_BLK_BACKOFF_INTERVAL
                                             (pTimerBlock),
                                             &TIMER_BLK_BACKOFF_INTERVAL
                                             (pTimerBlock)) == RPTE_SUCCESS)
            {
                RpteRthSendResvTear (RSVPTE_TNL_RSB (pRsvpTeTnlInfo));
            }
            else
            {
                /* Release from the local msg id data base */
                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TNL_RT_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
                RpteUtlDeleteRsb (pRsvpTeTnlInfo->pRsb);
                if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS)
                {
                    RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                }
                if (RPTE_TE_TNL (pRsvpTeTnlInfo)->u1TnlRelStatus ==
                    TE_SIGMOD_TNLREL_AWAITED)
                {
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
                }
                else
                {
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
                }
            }
            break;

        case RPTE_GR_RECOVERY_PATH:
            if (RpteRRCalculateBackOffDelay (PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                                           (pRsvpTeTnlInfo)),
                                             TIMER_BLK_BACKOFF_INTERVAL
                                             (pTimerBlock),
                                             &TIMER_BLK_BACKOFF_INTERVAL
                                             (pTimerBlock)) == RPTE_SUCCESS)
            {
                RpteHlprGenerateRecoveryPath (pRsvpTeTnlInfo);
            }
            else
            {
                /* Release from the local msg id data base */
                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);
                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                pRsvpTeTnlInfo->pRPTimerBlock = NULL;
            }
            break;

        default:
            break;
    }
    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessRMDTimeOut - EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteMIHGenerateEpoch
 * Description     : Generates the Epcoh value, which this node will use
 *                   in the messages sent to the neighbours.
 * Input (s)       : None
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHGenerateEpoch (VOID)
{
    UINT4               u4TempEpoch;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHGenerateEpoch - ENTRY\n");

    /* Generate Epcoh until a value not equal to the previous
     * value is obtained */
    do
    {
        OsixGetSysTime ((tOsixSysTime *) & u4TempEpoch);
        u4TempEpoch = u4TempEpoch & RPTE_EPOCH_BIT_MASK;
    }
    while (u4TempEpoch == gu4Epoch);

    gu4Epoch = u4TempEpoch;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHGenerateEpoch - EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteMIHGenerateMsgId
 * Description     : Generates a new message id
 * Input (s)       : None
 * Output (s)      : pu4MsgId - Pointer to the generated message id. 
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHGenerateMsgId (UINT4 *pu4MsgId)
{
    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHGenerateMsgId - ENTRY\n");

    *pu4MsgId = ++gu4MsgId;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHGenerateMsgId - EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteMIHInitMsgId
 * Description     : Initialises the message id.
 * Input (s)       : None
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteMIHInitMsgId (VOID)
{
    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHInitMsgId - ENTRY\n");

    gu4MsgId = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHInitMsgId - EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteMIHProcessMsgIdList
 * Description     : This function determines if an incoming Message ID 
 *                   value corresponds to an existing known state by searching 
 *                   the Neighbour's Message ID database.
 *                   
 * Input (s)       : pNbrEntry  - Pointer to the neighbour entry with which
 *                                message id is associated.
 *                   u4MsgId    - Message Id received from the neighbour.
 * Output (s)      : ppTrieInfo - Points to the Trie info retrieved when 
 *                                u4MsgId is located in Neighbour Message Id i
 *                                data base.
 * Returns         : RPTE_REFRESH_MSG or RPTE_MSG_ID_NOT_IN_DB
 */
/****************************************************************************/
UINT4
RpteMIHProcessMsgIdList (tNbrEntry * pNbrEntry, UINT4 u4MsgId,
                         tuTrieInfo ** ppTrieInfo)
{
    tRpteKey            RpteKey;
    INT4                i4RetVal;
    tuTrieInfo         *pTrieInfo = NULL;

    RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgIdList : ENTRY\n");

    *ppTrieInfo = NULL;

    /* Search the neighbour message id database to see if the msgid
     * is present */
    RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (pNbrEntry);
    RpteKey.u8_Key[RPTE_ONE] = u4MsgId;
    i4RetVal = RpteTrieLookupEntry (&RPTE_64_BIT_TRIE_ROOT_NODE,
                                    &RpteKey, &pTrieInfo);

    if (i4RetVal == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgIdList : EXIT\n");
        return RPTE_MSG_ID_NOT_IN_DB;
    }
    else
    {
        /* MsgId is present in the data base. */
        *ppTrieInfo = pTrieInfo;

        RSVPTE_DBG (RSVPTE_MIH_ETEXT, "RpteMIHProcessMsgIdList : EXIT\n");
        return RPTE_REFRESH_MSG;
    }
}

/*---------------------------------------------------------------------------*/
/*                        End of file rpteror.c                              */
/*---------------------------------------------------------------------------*/
