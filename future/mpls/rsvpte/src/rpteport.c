/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rpteport.c,v 1.104 2018/02/19 11:48:01 siva Exp $
 *
 * Description: This file contains porting related external interface
 *              routines.
 ********************************************************************/

#include "rpteincs.h"
#include "utilrand.h"
/****************************************************************************/
/*
 * Function Name : RpteTriggerInternalEvent
 * Description   : This routine generates an event to the RSVP-TE Task,
 *                 indicating it is an internal event, with the event type,
 *                 and the data that is associated with the event.
 * Input(s)      : pInfo. - Pointer to the Info associated with the event
 *                 u2EventType - The type of the Internal event.
 * Output(s)     : In case of successful operation event indicated to the
 *                 RSVP-TE Task.
 *                 Parameters info.
 * Return(s)     : RPTE_SUCCESS or RPTE_FAILURE.
 */
/****************************************************************************/
UINT1
RpteTriggerInternalEvent (const void *pInfo, UINT2 u2EventType)
{
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;

    MEMSET (&RpteEnqueueMsgInfo, 0, sizeof (tRpteEnqueueMsgInfo));

    RpteEnqueueMsgInfo.u4RpteEvent = (UINT4) u2EventType;
    RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = (tRsvpTeTnlInfo *) (FS_ULONG) pInfo;

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteTriggerInternalEvent : ENTRY \n");

    if (RPTE_PROC_EVT_FUNC[u2EventType] (&RpteEnqueueMsgInfo) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RpteTriggerInternalEvent: INTMD-EXIT\n");
        return RPTE_FAILURE;
    }

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteTriggerInternalEvent : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/*
 * Function Name : RpteEnqueueMsgToRsvpQ
 * Description   : Enqueues the message to RSVP-TE Message Queue and generates
 *                 respective event.
 * Input(s)      : aTaskEnqMsgInfo - Enqueued message.
 * Output(s)     : None.
 * Return(s)     : RPTE_SUCCESS or RPTE_FAILURE
 */
/****************************************************************************/
UINT1
RpteEnqueueMsgToRsvpQ (tRpteEnqueueMsgInfo * pRpteEnqueueMsgInfo)
{
    tRpteEnqueueMsgInfo *pRpteEvntInfo = NULL;
    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteEnqueueMsgToRsvpQ : ENTRY \n");
    if (RSVP_INITIALISED != TRUE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : RSVP-TE module is in shutdown state\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteEnqueueMsgToRsvpQ : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    pRpteEvntInfo = (tRpteEnqueueMsgInfo *)
        RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_INTERNAL_EVNT_POOL_ID);

    if (pRpteEvntInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Memory Allocation Failed - "
                    "Failed to EnQ the Msg to RSVP-TE Q\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteEnqueueMsgToRsvpQ : "
                    "INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    MEMSET (pRpteEvntInfo, RPTE_ZERO, sizeof (tRpteEnqueueMsgInfo));

    MEMCPY (pRpteEvntInfo, pRpteEnqueueMsgInfo, sizeof (tRpteEnqueueMsgInfo));

    if (OsixQueSend (RSVP_GBL_QID, (UINT1 *) (&pRpteEvntInfo),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)

    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to EnQ the Msg to RSVP-TE Q\n");
        RSVP_RELEASE_MEM_BLOCK (RSVPTE_INTERNAL_EVNT_POOL_ID, pRpteEvntInfo);
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteEnqueueMsgToRsvpQ : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (OsixEvtSend (RSVP_TSK_ID, RPTE_INTERNAL_EVENT) != OSIX_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to send Msg EnQd event to RSVP-TE\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteEnqueueMsgToRsvpQ : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteEnqueueMsgToRsvpQ : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************
 * Function Name : RpteEnqueueMsgToRsvpQFromCspf
 * Description   : Enqueues the message from CSPF to RSVP-TE Message Queue 
 *                 and generates respective event.
 * Input(s)      : pCspfCompInfo - Pointer to CSPF Computation Information.
 *                 pInpath       - Pointer to Backup Path Computed by CSPF.
 * Output(s)     : None.
 * Return(s)     : RPTE_SUCCESS or RPTE_FAILURE
 */
/****************************************************************************/
VOID
RpteEnqueueMsgToRsvpQFromCspf (tCspfCompInfo * pCspfCompInfo, VOID *pInPath)
{
    tRpteCspfInfo      *pRpteCspfInfo = NULL;
    tOsTeAppPath       *pPath = (tOsTeAppPath *) pInPath;

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteEnqueueMsgToRsvpQFromCspf : ENTRY \n");
    if (RSVP_INITIALISED != TRUE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "RSVP-TE module is in shutdown state\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteEnqueueMsgToRsvpQFromCspf : "
                    "INTMD-EXIT \n");
        return;
    }

    pRpteCspfInfo = (tRpteCspfInfo *)
        RSVP_ALLOCATE_MEM_BLOCK (RPTE_CSPF_POOL_ID);

    if (pRpteCspfInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Memory Allocation Failed - "
                    "Failed to send Msg EnQd event to RSVP-TE\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteEnqueueMsgToRsvpQFromCspf : "
                    "INTMD-EXIT \n");
        return;
    }

    MEMSET (pRpteCspfInfo, RPTE_ZERO, sizeof (tRpteCspfInfo));

    MEMCPY (&(pRpteCspfInfo->CspfCompInfo), pCspfCompInfo,
            sizeof (tCspfCompInfo));
    MEMCPY (&(pRpteCspfInfo->AppPath), pPath, sizeof (tOsTeAppPath));

    if (OsixQueSend (RSVP_GBL_CSPF_QID, (UINT1 *) (&pRpteCspfInfo),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        RSVP_RELEASE_MEM_BLOCK (RPTE_CSPF_POOL_ID, pRpteCspfInfo);
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to EnQ the Msg to RSVP-TE Q\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RpteEnqueueMsgToRsvpQFromCspf : INTMD-EXIT \n");
        return;
    }

    if (OsixEvtSend (RSVP_TSK_ID, CSPF_RESPONSE_RXED_EVENT) != OSIX_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to send Msg EnQd event to RSVP-TE\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RpteEnqueueMsgToRsvpQFromCspf : INTMD-EXIT \n");
        return;
    }

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteEnqueueMsgToRsvpQ : EXIT \n");
    return;
}

/************************************************************************
 *   Function Name   : RsvpProcessCspfMsg
 *   Description     : Receive and Process the CSPF response packets 
 *                     from OSPF-TE
 *   Input(s)        : pRpteCspfInfo - Pointer to CSPF info - Backup path
 *                     information.
 *   Output(s)       : None
 *   Returns         : None
 **************************************************************************/
VOID
RsvpProcessCspfMsg (tRpteCspfInfo * pRpteCspfInfo)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tCspfCompInfo      *pCspfCompInfo = NULL;
    tOsTeAppPath       *pCspfPath = NULL;

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RsvpProcessCspfMsg : ENTRY \n");

    RSVPTE_DBG (RSVPTE_PORT_DBG, "CSPF message processing started\n");
    pCspfCompInfo = &(pRpteCspfInfo->CspfCompInfo);
    pCspfPath = &(pRpteCspfInfo->AppPath);

    /* Checking whether we have received the backup path for the correct
     * tunnel. */
    if (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo, pCspfCompInfo->u4TnlIndex,
                                pCspfCompInfo->u4TnlInst,
                                pCspfCompInfo->u4TnlSrcAddr,
                                pCspfCompInfo->u4TnlDestAddr,
                                &pRsvpTeTnlInfo) != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_PORT_DBG,
                    "Err : Tunnel Entry Not Found in Hash Table\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RsvpProcessCspfMsg : INTMD-EXIT \n");
        return;
    }
    if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
        (pCspfCompInfo->u1PathInfo & OSPF_TE_BACKUP_PATH_BIT_MASK) &&
        /*check if RSVPTE_TNL_FRR_ONE2ONE_METHOD */
        (((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS) &&
          (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL) &&
          ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).u1Flags)
           == RSVPTE_TNL_FRR_ONE2ONE_METHOD))
         ||
         ((RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo) != NULL) &&
          ((RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u1ProtMethod)
           == RSVPTE_TNL_FRR_ONE2ONE_METHOD))))
    {
        if (pCspfPath->u1PathReturnCode == CSPF_PATH_FOUND)
        {
            if (pCspfPath->u2HopCount < (OSPF_TE_MAX_NEXT_HOPS - 1))
            {
                pCspfPath->aNextHops[pCspfPath->u2HopCount].u4NextHopIpAddr =
                    pCspfCompInfo->u4DestIpAddr;
                pCspfPath->u2HopCount++;
            }
        }

        RsvpProcessCspfMsgForFRRTnl (pRsvpTeTnlInfo, pCspfCompInfo, pCspfPath);
    }
    else
    {
        RsvpProcessCspfMsgForNormalTnl (pRsvpTeTnlInfo, pCspfPath);
    }

    return;
}

/****************************************************************************/
/* Function Name   : RpteCspfCompForBkpOrProtTnl                            */
/* Description     : This function computes the backup path through CSPF    */
/* Input (s)       : pCtTnlInfo - RSVP Tunnel info.                         */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteCspfCompForBkpOrProtTnl (tRsvpTeTnlInfo * pCtTnlInfo)
{
#ifdef OSPFTE_WANTED
    tCspfCompInfo      *pCspfCompInfo = NULL;
    tRsvpTeArHopInfo   *pTnlArHopInfo = NULL;
    tRsvpTeTnlInfo     *pTempCtTnlInfo = NULL;
    UINT4               u4StrictAddr = RPTE_ZERO;
    UINT4               u4TempVal = 0;

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteCspfCompForBkpOrProtTnl : ENTRY\n");

    /* Because the tunnel does not exist, stopping the timer. */
    if ((RPTE_TE_TNL (pCtTnlInfo) == NULL) ||
        (RpteCheckTnlInTnlTable
         (gpRsvpTeGblInfo, RSVPTE_TNL_TNLINDX (pCtTnlInfo),
          RSVPTE_TNL_TNLINST (pCtTnlInfo),
          OSIX_NTOHL (RSVPTE_TNL_INGRESS (pCtTnlInfo)),
          OSIX_NTOHL (RSVPTE_TNL_EGRESS (pCtTnlInfo)),
          &pTempCtTnlInfo) != RPTE_SUCCESS))
    {
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_CSPF_TIMER (pCtTnlInfo));
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteCspfCompForBkpOrProtTnl : EXIT\n");
        return;
    }

    /* Because CSPF Computation has been Successful, stopping the timer */
    if ((RPTE_FRR_CSPF_INFO (pTempCtTnlInfo) & RPTE_FRR_CSPF_COMPUTED) ==
        RPTE_FRR_CSPF_COMPUTED)
    {
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_CSPF_TIMER (pTempCtTnlInfo));
        RSVPTE_DBG (RSVPTE_PORT_DBG,
                    "RESV: CSPF Computation Successful. So, Stopping the "
                    "timer. \n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteCspfCompForBkpOrProtTnl : EXIT\n");
        return;
    }

    if (RSVPTE_OUT_ARHOP_LIST_INFO (pTempCtTnlInfo) == NULL)
    {
        RPTE_FRR_CSPF_INFO (pTempCtTnlInfo) |= RPTE_FRR_CSPF_FAILED;
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_CSPF_TIMER (pTempCtTnlInfo));
        RSVPTE_DBG (RSVPTE_PORT_DBG, "RESV: Tunnel AR Hop is not present. \n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RpteCspfCompForBkpOrProtTnl : INTMD - EXIT\n");
        return;
    }
    /* Get the First Hop Address in Tunnel Actual Route Hop Info */
    pTnlArHopInfo = (tRsvpTeArHopInfo *)
        (TMO_SLL_First (&RSVPTE_TNL_OUTARHOP_LIST (pTempCtTnlInfo)));
    if (pTnlArHopInfo == NULL)
    {
        RPTE_FRR_CSPF_INFO (pTempCtTnlInfo) |= RPTE_FRR_CSPF_FAILED;
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_CSPF_TIMER (pTempCtTnlInfo));
        RSVPTE_DBG (RSVPTE_PORT_DBG, "RESV: Tunnel AR Hop Null. \n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RpteCspfCompForBkpOrProtTnl : INTMD - EXIT\n");
        return;
    }

    if (((RPTE_FRR_CSPF_INFO (pTempCtTnlInfo) & RPTE_FRR_CSPF_NODE_FAILED) ==
         RPTE_ZERO) && (RpteUtlIsNodeProtDesired (pTempCtTnlInfo) == RPTE_YES))
    {
        /* For Node protection, the Next Hop Address in Actual Route Hop Info 
         * is required.*/
        pTnlArHopInfo =
            (tRsvpTeArHopInfo *)
            TMO_SLL_Next (&RSVPTE_TNL_OUTARHOP_LIST (pTempCtTnlInfo),
                          (&(pTnlArHopInfo->NextHop)));
        if (pTnlArHopInfo == NULL)
        {
            pTnlArHopInfo = (tRsvpTeArHopInfo *)
                (TMO_SLL_First (&RSVPTE_TNL_OUTARHOP_LIST (pTempCtTnlInfo)));
            RPTE_FRR_CSPF_INFO (pTempCtTnlInfo) |= RPTE_FRR_CSPF_NODE_FAILED;
        }
        if (pTnlArHopInfo != NULL)
        {
            CONVERT_TO_INTEGER (pTnlArHopInfo->IpAddr.au1Ipv4Addr,
                                u4StrictAddr);
        }
    }
    else
    {
        /* For Link Protection, the current Hop Address in Actual Route Hop Info
         * is sufficient.*/
        CONVERT_TO_INTEGER (pTnlArHopInfo->IpAddr.au1Ipv4Addr, u4StrictAddr);
    }

    /* Filling the CspfCompInfo Structure to invoke CSPF API to compute
     * Alternate Path */
    pCspfCompInfo =
        (tCspfCompInfo *) MemAllocMemBlk (RPTE_CSPF_COMP_INFO_POOL_ID);
    if (pCspfCompInfo == NULL)
    {
        return;
    }

    MEMSET (pCspfCompInfo, RSVPTE_ZERO, sizeof (tCspfCompInfo));
    pCspfCompInfo->u4TnlIndex = RSVPTE_TNL_TNLINDX (pTempCtTnlInfo);
    pCspfCompInfo->u4TnlInst = RSVPTE_TNL_TNLINST (pTempCtTnlInfo);
    pCspfCompInfo->u4TnlSrcAddr =
        OSIX_NTOHL (RSVPTE_TNL_INGRESS (pTempCtTnlInfo));
    pCspfCompInfo->u4TnlDestAddr =
        OSIX_NTOHL (RSVPTE_TNL_EGRESS (pTempCtTnlInfo));
    CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                        u4TempVal);
    pCspfCompInfo->u4SrcIpAddr = OSIX_NTOHL (u4TempVal);
    if (RSVPTE_TNL_GBL_REVERT_FLAG (pTempCtTnlInfo) == RPTE_FALSE)
    {
        pCspfCompInfo->u4DestIpAddr = OSIX_NTOHL (u4StrictAddr);
        pCspfCompInfo->u1PathInfo = OSPF_TE_BACKUP_PATH_BIT_MASK;
        pCspfCompInfo->u1Diversity = OSPF_TE_NODE_DISJOINT_BIT_MASK;
    }
    else
    {
        pCspfCompInfo->u4DestIpAddr = pCspfCompInfo->u4TnlDestAddr;
        pCspfCompInfo->u1PathInfo = OSPF_TE_PRIMARY_PATH;
    }

    /* Enqueueing the Message in OSPF TE Queue and Indicating OSPF TE about
     * the Queue Object */
    if (OspfTeEnqueueMsgFromRsvpTe (pCspfCompInfo) != OSPF_TE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_PORT_DBG,
                    "RESV: Msg Posting to OSPF-TE Unsuccessful. \n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteCspfCompForBkpOrProtTnl "
                    "- INTMD EXIT\n");
        MemReleaseMemBlock (RPTE_CSPF_COMP_INFO_POOL_ID,
                            (UINT1 *) pCspfCompInfo);
        return;
    }
    RPTE_FRR_CSPF_INTVL_COUNT (pTempCtTnlInfo)++;
    RPTE_FRR_CSPF_INFO (pTempCtTnlInfo) |= RPTE_FRR_CSPF_STARTED;

    /* Starting the Timer */
    RpteStartCspfIntervalTimer (pTempCtTnlInfo);
    MemReleaseMemBlock (RPTE_CSPF_COMP_INFO_POOL_ID, (UINT1 *) pCspfCompInfo);
#else
    UNUSED_PARAM (pCtTnlInfo);
#endif
    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteCspfCompForBkpOrProtTnl : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteCspfCompForNormalTnl                               */
/* Description     : This function computes the backup path through CSPF    */
/* Input (s)       : pCtTnlInfo - RSVP Tunnel info.                         */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

INT1
RpteCspfCompForNormalTnl (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
#ifdef OSPFTE_WANTED
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    UINT4               u4HopIpAddr = RPTE_ZERO;
    tCspfCompInfo      *pCspfCompInfo;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    UINT4               u4TempVal = RPTE_ZERO;
    tTeTnlSrlg         *pTeTnlSrlg = NULL;
    tTMO_SLL           *pEROList = NULL;
    tTMO_SLL           *pBackupEROList = NULL;
    tRsvpTeTnlInfo     *pRsvpTeMapTnlInfo = NULL;
    tTeCHopListInfo    *pTeCHopListInfo = NULL;
    tTeCHopInfo        *pTeCHopInfo = NULL;
    tTeArHopListInfo   *pTeArHopListInfo = NULL;
    tTeArHopInfo       *pTeArHopInfo = NULL;
    UINT2               u2SrlgCount = RPTE_ZERO;
    UINT4               u4NoOfSrlg = RPTE_ZERO;
    UINT2               u2HopCount = RPTE_ZERO;
    UINT4               au4ExcludeHop[OSPF_TE_MAX_NEXT_HOPS];
    UINT1               u1ExcludeHopCount = RPTE_ZERO;
    UINT4               u4NextHopIpAddr = RPTE_ZERO;
    UINT4               u4NextHopAddr = RPTE_ZERO;
    tBandWidth          oldBandwidth = RPTE_ZERO;

    pRsvpTeMapTnlInfo = pRsvpTeTnlInfo->pMapTnlInfo;

    pCspfCompInfo
        = (tCspfCompInfo *) MemAllocMemBlk (RPTE_CSPF_COMP_INFO_POOL_ID);

    if (pCspfCompInfo == NULL)
    {
        return RPTE_FAILURE;
    }

    MEMSET (pCspfCompInfo, RSVPTE_ZERO, sizeof (tCspfCompInfo));
    MEMSET (au4ExcludeHop, RPTE_ZERO, sizeof (au4ExcludeHop));
    MEMSET (&pCspfCompInfo->osTeExpRoute, RPTE_ZERO, sizeof (tOsTeExpRoute));
    MEMSET (&pCspfCompInfo->osTeAlternateRoute, RPTE_ZERO,
            sizeof (tOsTeExpRoute));

    pTeTnlInfo = pRsvpTeTnlInfo->pTeTnlInfo;

    /* Fill Tunnel index value  */
    pCspfCompInfo->u4TnlIndex = RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);
    /* Fill Tunnel instance value  */
    pCspfCompInfo->u4TnlInst = RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);
    /* Fill Tunnel source address  */
    pCspfCompInfo->u4TnlSrcAddr =
        OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo));
    /* Fill Tunnel destination address  */
    pCspfCompInfo->u4TnlDestAddr =
        OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));

    /* Fill CSPF destination address */
    pCspfCompInfo->u4DestIpAddr =
        OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));

    /* Fill source address of the node from which request is asked  */
    CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                        u4TempVal);
    pCspfCompInfo->u4SrcIpAddr = OSIX_NTOHL (u4TempVal);
    /* Fill Tunnel include-all affinity value  */
    pCspfCompInfo->u4IncludeAllSet =
        RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo);
    /* Fill Tunnel include-any affinity value  */
    pCspfCompInfo->u4IncludeAnySet =
        RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pRsvpTeTnlInfo);
    /* Fill Tunnel Exclude-any affinity value  */
    pCspfCompInfo->u4ExcludeAnySet =
        RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pRsvpTeTnlInfo);
    /* Fill Tunnel setup priority value  */
    pCspfCompInfo->u1Priority =
        RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pRsvpTeTnlInfo);
    /* Fill Tunnel switching type value  */
    pCspfCompInfo->u1SwitchingCap = pTeTnlInfo->GmplsTnlInfo.u1SwitchingType;
    /* Fill Tunnel encoding type value  */
    pCspfCompInfo->u1EncodingType = pTeTnlInfo->GmplsTnlInfo.u1EncodingType;

    if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
    {
        if (MplsApiValDiffservTeParams (RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE
                                        (pRsvpTeTnlInfo),
                                        RSVPTE_TNL_OR_ATTRLIST_SET_PRIO
                                        (pRsvpTeTnlInfo), RPTE_ZERO, RPTE_ZERO,
                                        MPLS_DSTE_TE_CLASS_FLAG,
                                        &(pCspfCompInfo->u1Priority)) !=
            RPTE_SUCCESS)
        {
            pCspfCompInfo->u1Priority =
                RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pRsvpTeTnlInfo);
        }
    }

    /* Fill bandwidth information */
    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo), &pTeTrfcParams) == TE_SUCCESS)
    {
        pCspfCompInfo->bandwidth
            = (tBandWidth) TE_RSVPTE_TPARAM_PDR (pTeTrfcParams);
        RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (pCspfCompInfo->bandwidth);
    }

    if (pRsvpTeMapTnlInfo != NULL)
    {
        /* Get the old bandwidth if map tunnel is present */

        if (TeCheckTrfcParamInTrfcParamTable
            (TE_TNL_TRFC_PARAM_INDEX (pRsvpTeMapTnlInfo->pTeTnlInfo),
             &pTeTrfcParams) == TE_SUCCESS)
        {
            oldBandwidth = (tBandWidth) TE_RSVPTE_TOLDPARAM_PDR (pTeTrfcParams);
            RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (oldBandwidth);
        }

        u1ExcludeHopCount = pRsvpTeMapTnlInfo->u1NoOfExcludeHop;

        for (u2HopCount = RPTE_ZERO; u2HopCount < u1ExcludeHopCount;
             u2HopCount++)
        {
            au4ExcludeHop[u2HopCount]
                = pRsvpTeMapTnlInfo->au4ExcludeHop[u2HopCount];
        }

        u2HopCount = RPTE_ZERO;

        pTeCHopListInfo = pRsvpTeMapTnlInfo->pTeTnlInfo->pTeCHopListInfo;
        pTeArHopListInfo = pRsvpTeMapTnlInfo->pTeTnlInfo->pTeOutArHopListInfo;

    }

    /* Fill SRLG values */
    if (RSVPTE_SRLG_LIST (pRsvpTeTnlInfo) != NULL)
    {
        u4NoOfSrlg = TMO_SLL_Count (RSVPTE_SRLG_LIST (pRsvpTeTnlInfo));

        TMO_SLL_Scan ((tTMO_SLL *) (RSVPTE_SRLG_LIST (pRsvpTeTnlInfo)),
                      pTeTnlSrlg, tTeTnlSrlg *)
        {
            if (u2SrlgCount >= OSPF_TE_MAX_SRLG_NO)
            {
                break;
            }
            pCspfCompInfo->osTeSrlg.aSrlgNumber[u2SrlgCount] =
                pTeTnlSrlg->u4SrlgNo;
            u2SrlgCount++;
            pCspfCompInfo->osTeSrlg.u4NoOfSrlg = u4NoOfSrlg;
        }
        pCspfCompInfo->u1SrlgType =
            RSVPTE_TNL_OR_ATTRLIST_SRLG_TYPE (pRsvpTeTnlInfo);
    }

    RSVPTE_DBG2 (RSVPTE_REOPT_DBG, "RpteCspfCompForNormalTnl : "
                 "Reoptimization Trigger State(%d), MapTunnel Info(%x)\n",
                 RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo),
                 pRsvpTeMapTnlInfo);
    /* Fill Path Info required appropriately. */
    if ((pTeTnlInfo->u1TnlRerouteFlag == TRUE) ||
        (pTeTnlInfo->bIsMbbRequired == TRUE) ||
        (RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo) == TRUE))
    {
        pCspfCompInfo->u1PathInfo
            = (OSPF_TE_MAKE_BEFORE_BREAK_PATH_BIT_MASK |
               OSPF_TE_BACKUP_PATH_BIT_MASK |
               OSPF_TE_RESOURCE_AFFINITY_BIT_MASK);

        pCspfCompInfo->oldbandwidth = oldBandwidth;
        pCspfCompInfo->u4ExcludeAnySet = pRsvpTeTnlInfo->u4RsrcClassColor;
        pCspfCompInfo->u1RsrcSetType = OSPF_TE_RSRC_TYPE_EXCLUDE_ANY;

    }
    else if ((pTeTnlInfo->u1TnlPathType == RPTE_TNL_PROTECTION_PATH) &&
             (pTeTnlInfo->pTeBackupPathInfo == NULL) &&
             (TE_TNL_ROLE (pTeTnlInfo) != TE_INTERMEDIATE))
    {
        pCspfCompInfo->u1PathInfo = OSPF_TE_BACKUP_PATH_BIT_MASK;

        pCspfCompInfo->u1Diversity =
            (OSPF_TE_NODE_DISJOINT_BIT_MASK | OSPF_TE_LINK_DISJOINT_BIT_MASK |
             OSPF_TE_SRLG_DISJOINT_BIT_MASK);
    }
    else
    {
        if ((RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo) |
             RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pRsvpTeTnlInfo) |
             RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pRsvpTeTnlInfo)) != RPTE_ZERO)
        {
            pCspfCompInfo->u1PathInfo = OSPF_TE_RESOURCE_AFFINITY_BIT_MASK;
        }
        else
        {
            pCspfCompInfo->u1PathInfo = OSPF_TE_PRIMARY_PATH_BIT_MASK;
        }

    }

    /* Fill Backup ERO For One-To-One Protection Tunnel */
    if ((pTeTnlInfo->pTeBackupPathInfo != NULL) &&
        (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE)
        && (pTeTnlInfo->u1TnlPathType == RPTE_TNL_PROTECTION_PATH))

    {
        pBackupEROList = &pTeTnlInfo->pTeBackupPathInfo->ErHopList;
        RpteFillExpRoute (pBackupEROList, pCspfCompInfo, pTeTnlInfo);

    }

    /* If CHopListInfo is present fill it in osTeExpRoute */
    else if (pTeCHopListInfo != NULL)
    {
        TMO_SLL_Scan (&pTeCHopListInfo->CHopList, pTeCHopInfo, tTeCHopInfo *)
        {
            CONVERT_TO_INTEGER (pTeCHopInfo->RouterId.au1Ipv4Addr,
                                u4NextHopIpAddr);

            CONVERT_TO_INTEGER (pTeCHopInfo->IpAddr.au1Ipv4Addr, u4NextHopAddr);
            u4NextHopIpAddr = OSIX_NTOHL (u4NextHopIpAddr);
            u4NextHopAddr = OSIX_NTOHL (u4NextHopAddr);

            if ((u4NextHopIpAddr != pCspfCompInfo->u4TnlDestAddr) ||
                (pTeTnlInfo->u1TnlPathType != RPTE_TNL_PROTECTION_PATH))
            {
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].u4RouterId
                    = u4NextHopIpAddr;

                /*Next Hop Interface address should also be copied here */
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].
                    u4NextHopIpAddr = u4NextHopAddr;

                /* Remote Identifier should also be copied here. */
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].
                    u4RemoteIdentifier = pTeCHopInfo->u4UnnumIf;
                u2HopCount++;
            }
        }

        pCspfCompInfo->osTeExpRoute.u2Info = OSPF_TE_PRIMARY_PATH;
        pCspfCompInfo->osTeExpRoute.u2HopCount = u2HopCount;

        u2HopCount = RPTE_ZERO;

    }

    else if (pTeArHopListInfo != NULL)
    {
        TMO_SLL_Scan (&pTeArHopListInfo->ArHopList, pTeArHopInfo,
                      tTeArHopInfo *)
        {
            CONVERT_TO_INTEGER (pTeArHopInfo->IpAddr.au1Ipv4Addr,
                                u4NextHopIpAddr);
            u4NextHopIpAddr = OSIX_NTOHL (u4NextHopIpAddr);

            if ((u4NextHopIpAddr != pCspfCompInfo->u4TnlDestAddr) ||
                (pTeTnlInfo->u1TnlPathType != RPTE_TNL_PROTECTION_PATH))
            {
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].u4RouterId
                    = u4NextHopIpAddr;
                /* Remote Identifier should also be copied here. */

                u2HopCount++;
            }
        }

        pCspfCompInfo->osTeExpRoute.u2Info = OSPF_TE_PRIMARY_PATH;
        pCspfCompInfo->osTeExpRoute.u2HopCount = u2HopCount;

        u2HopCount = RPTE_ZERO;
    }

    /* Exclude Hop array will be filled in 2 scenarios,
     *
     * 1. Receive a PATH ERROR or Notify Message for the already 
     *    established tunnel.
     * 2. Receive a PATH ERROR or Notify Message for the currently establishing 
     *    tunnel.
     *
     * But it should not be filled when error node is not seen in
     * administrator configured ERO. */
    if (u1ExcludeHopCount != RPTE_ZERO)
    {
        for (u2HopCount = RPTE_ZERO; u2HopCount < u1ExcludeHopCount;
             u2HopCount++)
        {
            if ((RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo) == TRUE) ||
                ((pRsvpTeMapTnlInfo != NULL) &&
                 (RPTE_REOPTIMIZE_ERR_LINK_ADDR (pRsvpTeMapTnlInfo) !=
                  RPTE_ZERO)))
            {
                if (pRsvpTeMapTnlInfo != NULL)
                {
                    RSVPTE_DBG2 (RSVPTE_REOPT_DBG, "RpteCspfCompForNormalTnl : "
                                 "Reoptimization Maintainance Type(%d) "
                                 "Exclude Hop(%x)\n",
                                 pRsvpTeMapTnlInfo->u1ReoptMaintenanceType,
                                 OSIX_NTOHL (au4ExcludeHop[u2HopCount]));

                    if (RpteIsFirstHopAsLoose (pRsvpTeTnlInfo) == RPTE_SUCCESS)
                    {

                        pEROList =
                            &pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo->ErHopList;

                        pRsvpTeErHopInfo =
                            (tRsvpTeHopInfo *) TMO_SLL_First (pEROList);

                        if (pRsvpTeErHopInfo != NULL)
                        {
                            CONVERT_TO_INTEGER (pRsvpTeErHopInfo->IpAddr.
                                                au1Ipv4Addr, u4HopIpAddr);
                            u4HopIpAddr = OSIX_NTOHL (u4HopIpAddr);

                            pCspfCompInfo->u4DestIpAddr = u4HopIpAddr;
                        }
                    }

                    if (pRsvpTeMapTnlInfo->u1ReoptMaintenanceType ==
                        RPTE_REOPT_NODE_MAINTENANCE)
                    {
                        pCspfCompInfo->osTeAlternateRoute.aNextHops[u2HopCount].
                            u4RouterId = OSIX_NTOHL (au4ExcludeHop[u2HopCount]);
                    }
                    else if (pRsvpTeMapTnlInfo->u1ReoptMaintenanceType ==
                             RPTE_REOPT_LINK_MAINTENANCE)
                    {
                        /* Remote Identifier should also be copied here. */
                        pCspfCompInfo->osTeAlternateRoute.aNextHops[u2HopCount].
                            u4NextHopIpAddr =
                            OSIX_NTOHL (au4ExcludeHop[u2HopCount]);
                    }
                }
            }
            else
            {
                /* Remote Identifier should also be copied here. */
                pCspfCompInfo->osTeAlternateRoute.aNextHops[u2HopCount].
                    u4RouterId = au4ExcludeHop[u2HopCount];
            }

        }

        pCspfCompInfo->osTeAlternateRoute.u2Info = OSPF_TE_FAILED_LINK;
        pCspfCompInfo->osTeAlternateRoute.u2HopCount = u2HopCount;

        u2HopCount = RPTE_ZERO;
    }
    else if ((pTeTnlInfo->pTePathListInfo != NULL) &&
             (pTeTnlInfo->bIsOamPathChange == FALSE))
    {
        /* Fill the explicit route */
        if (pTeTnlInfo->pTePathInfo != NULL)
        {
            pEROList = &pTeTnlInfo->pTePathInfo->ErHopList;
            RpteFillExpRoute (pEROList, pCspfCompInfo, pTeTnlInfo);
        }

    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
    {
        pCspfCompInfo->u1PathInfo |= OSPF_TE_BI_DIRECTIONAL_PATH_BIT_MASK;
    }

    pRsvpTeTnlInfo->u1CSPFPathRequested = RPTE_TRUE;

    /* Enqueueing the Message in OSPF TE Queue and Indicating OSPF TE about
     * the Queue Object */
    if (OspfTeEnqueueMsgFromRsvpTe (pCspfCompInfo) != OSPF_TE_SUCCESS)
    {
        RpteUtlFillErrorTable (RPTE_ZERO, RPTE_ZERO,
                               pCspfCompInfo->u4SrcIpAddr, pRsvpTeTnlInfo);
        RpteStartCspfIntervalTimer (pRsvpTeTnlInfo);

        MemReleaseMemBlock (RPTE_CSPF_COMP_INFO_POOL_ID,
                            (UINT1 *) pCspfCompInfo);

        RSVPTE_DBG (RSVPTE_PORT_DBG,
                    "RESV: Msg Posting to OSPF-TE Unsuccessful. \n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteCspfCompForBkpOrProtTnl "
                    "- INTMD EXIT\n");
        return RPTE_FAILURE;
    }

    RpteStartCspfIntervalTimer (pRsvpTeTnlInfo);
    MemReleaseMemBlock (RPTE_CSPF_COMP_INFO_POOL_ID, (UINT1 *) pCspfCompInfo);

#else
    UNUSED_PARAM (pRsvpTeTnlInfo);
#endif

    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteCspfCompForReoptimizeTnlAtMidNode                  */
/* Description     : This function computes the backup path through CSPF    */
/* Input (s)       : pCtTnlInfo - RSVP Tunnel info.                         */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

INT1
RpteCspfCompForReoptimizeTnlAtMidNode (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
#ifdef OSPFTE_WANTED
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    UINT4               u4HopIpAddr = RPTE_ZERO;
    tCspfCompInfo      *pCspfCompInfo;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    UINT4               u4TempVal = RPTE_ZERO;
    tTeTnlSrlg         *pTeTnlSrlg = NULL;
    tTMO_SLL           *pEROList = NULL;
    tTeCHopListInfo    *pTeCHopListInfo = NULL;
    tTeCHopInfo        *pTeCHopInfo = NULL;
    tTeArHopListInfo   *pTeArHopListInfo = NULL;
    tTeArHopInfo       *pTeArHopInfo = NULL;
    UINT2               u2SrlgCount = RPTE_ZERO;
    UINT4               u4NoOfSrlg = RPTE_ZERO;
    UINT2               u2HopCount = RPTE_ZERO;
    UINT4               au4ExcludeHop[OSPF_TE_MAX_NEXT_HOPS];
    UINT1               u1ExcludeHopCount = RPTE_ZERO;
    UINT4               u4NextHopIpAddr = RPTE_ZERO;
    UINT4               u4NextHopAddr = RPTE_ZERO;
    tBandWidth          oldBandwidth = RPTE_ZERO;

    pCspfCompInfo
        = (tCspfCompInfo *) MemAllocMemBlk (RPTE_CSPF_COMP_INFO_POOL_ID);

    if (pCspfCompInfo == NULL)
    {
        return RPTE_FAILURE;
    }

    MEMSET (pCspfCompInfo, RSVPTE_ZERO, sizeof (tCspfCompInfo));
    MEMSET (au4ExcludeHop, RPTE_ZERO, sizeof (au4ExcludeHop));
    MEMSET (&pCspfCompInfo->osTeExpRoute, RPTE_ZERO, sizeof (tOsTeExpRoute));
    MEMSET (&pCspfCompInfo->osTeAlternateRoute, RPTE_ZERO,
            sizeof (tOsTeExpRoute));

    pTeTnlInfo = pRsvpTeTnlInfo->pTeTnlInfo;

    /* Fill Tunnel index value  */
    pCspfCompInfo->u4TnlIndex = RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);
    /* Fill Tunnel instance value  */
    pCspfCompInfo->u4TnlInst = RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);
    /* Fill Tunnel source address  */
    pCspfCompInfo->u4TnlSrcAddr =
        OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo));
    /* Fill Tunnel destination address  */
    pCspfCompInfo->u4TnlDestAddr =
        OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));

    /* Fill CSPF destination address */
    pCspfCompInfo->u4DestIpAddr =
        OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));

    /* Fill source address of the node from which request is asked  */
    CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                        u4TempVal);
    pCspfCompInfo->u4SrcIpAddr = OSIX_NTOHL (u4TempVal);
    /* Fill Tunnel include-all affinity value  */
    pCspfCompInfo->u4IncludeAllSet =
        RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo);
    /* Fill Tunnel include-any affinity value  */
    pCspfCompInfo->u4IncludeAnySet =
        RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pRsvpTeTnlInfo);
    /* Fill Tunnel Exclude-any affinity value  */
    pCspfCompInfo->u4ExcludeAnySet =
        RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pRsvpTeTnlInfo);
    /* Fill Tunnel setup priority value  */
    pCspfCompInfo->u1Priority =
        RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pRsvpTeTnlInfo);
    /* Fill Tunnel switching type value  */
    pCspfCompInfo->u1SwitchingCap = pTeTnlInfo->GmplsTnlInfo.u1SwitchingType;
    /* Fill Tunnel encoding type value  */
    pCspfCompInfo->u1EncodingType = pTeTnlInfo->GmplsTnlInfo.u1EncodingType;

    /* Fill bandwidth information */
    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo), &pTeTrfcParams) == TE_SUCCESS)
    {
        pCspfCompInfo->bandwidth
            = (tBandWidth) TE_RSVPTE_TPARAM_PDR (pTeTrfcParams);
        RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (pCspfCompInfo->bandwidth);
    }

    /* Get the old bandwidth if map tunnel is present */
    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pRsvpTeTnlInfo->pTeTnlInfo),
         &pTeTrfcParams) == TE_SUCCESS)
    {
        oldBandwidth = (tBandWidth) TE_RSVPTE_TOLDPARAM_PDR (pTeTrfcParams);
        RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (oldBandwidth);
    }

    u1ExcludeHopCount = pRsvpTeTnlInfo->u1NoOfExcludeHop;

    for (u2HopCount = RPTE_ZERO; u2HopCount < u1ExcludeHopCount; u2HopCount++)
    {
        au4ExcludeHop[u2HopCount] = pRsvpTeTnlInfo->au4ExcludeHop[u2HopCount];
    }

    u2HopCount = RPTE_ZERO;

    pTeCHopListInfo = pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo;
    pTeArHopListInfo = pRsvpTeTnlInfo->pTeTnlInfo->pTeOutArHopListInfo;

    /* Fill SRLG values */
    if (RSVPTE_SRLG_LIST (pRsvpTeTnlInfo) != NULL)
    {
        u4NoOfSrlg = TMO_SLL_Count (RSVPTE_SRLG_LIST (pRsvpTeTnlInfo));

        TMO_SLL_Scan ((tTMO_SLL *) (RSVPTE_SRLG_LIST (pRsvpTeTnlInfo)),
                      pTeTnlSrlg, tTeTnlSrlg *)
        {
            if (u2SrlgCount >= OSPF_TE_MAX_SRLG_NO)
            {
                break;
            }
            pCspfCompInfo->osTeSrlg.aSrlgNumber[u2SrlgCount] =
                pTeTnlSrlg->u4SrlgNo;
            u2SrlgCount++;
            pCspfCompInfo->osTeSrlg.u4NoOfSrlg = u4NoOfSrlg;
        }
        pCspfCompInfo->u1SrlgType =
            RSVPTE_TNL_OR_ATTRLIST_SRLG_TYPE (pRsvpTeTnlInfo);
    }

    pCspfCompInfo->u1PathInfo
        = (OSPF_TE_MAKE_BEFORE_BREAK_PATH_BIT_MASK |
           OSPF_TE_BACKUP_PATH_BIT_MASK | OSPF_TE_RESOURCE_AFFINITY_BIT_MASK);

    pCspfCompInfo->oldbandwidth = oldBandwidth;
    pCspfCompInfo->u4ExcludeAnySet = pRsvpTeTnlInfo->u4RsrcClassColor;
    pCspfCompInfo->u1RsrcSetType = OSPF_TE_RSRC_TYPE_EXCLUDE_ANY;

    /* If CHopListInfo is present fill it in osTeExpRoute */
    if (pTeCHopListInfo != NULL)
    {
        TMO_SLL_Scan (&pTeCHopListInfo->CHopList, pTeCHopInfo, tTeCHopInfo *)
        {
            CONVERT_TO_INTEGER (pTeCHopInfo->RouterId.au1Ipv4Addr,
                                u4NextHopIpAddr);

            CONVERT_TO_INTEGER (pTeCHopInfo->IpAddr.au1Ipv4Addr, u4NextHopAddr);
            u4NextHopIpAddr = OSIX_NTOHL (u4NextHopIpAddr);
            u4NextHopAddr = OSIX_NTOHL (u4NextHopAddr);

            if ((u4NextHopIpAddr != pCspfCompInfo->u4TnlDestAddr) ||
                (pTeTnlInfo->u1TnlPathType != RPTE_TNL_PROTECTION_PATH))
            {
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].u4RouterId
                    = u4NextHopIpAddr;

                /*Next Hop Interface address should also be copied here */
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].
                    u4NextHopIpAddr = u4NextHopAddr;

                /* Remote Identifier should also be copied here. */
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].
                    u4RemoteIdentifier = pTeCHopInfo->u4UnnumIf;
                u2HopCount++;
            }
        }

        pCspfCompInfo->osTeExpRoute.u2Info = OSPF_TE_PRIMARY_PATH;
        pCspfCompInfo->osTeExpRoute.u2HopCount = u2HopCount;

        u2HopCount = RPTE_ZERO;

    }

    else if (pTeArHopListInfo != NULL)
    {
        TMO_SLL_Scan (&pTeArHopListInfo->ArHopList, pTeArHopInfo,
                      tTeArHopInfo *)
        {
            CONVERT_TO_INTEGER (pTeArHopInfo->IpAddr.au1Ipv4Addr,
                                u4NextHopIpAddr);
            u4NextHopIpAddr = OSIX_NTOHL (u4NextHopIpAddr);

            if ((u4NextHopIpAddr != pCspfCompInfo->u4TnlDestAddr) ||
                (pTeTnlInfo->u1TnlPathType != RPTE_TNL_PROTECTION_PATH))
            {
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].u4RouterId
                    = u4NextHopIpAddr;
                /* Remote Identifier should also be copied here. */

                u2HopCount++;
            }
        }

        pCspfCompInfo->osTeExpRoute.u2Info = OSPF_TE_PRIMARY_PATH;
        pCspfCompInfo->osTeExpRoute.u2HopCount = u2HopCount;

        u2HopCount = RPTE_ZERO;
    }

    /* Exclude Hop array will be filled in 2 scenarios,
     *
     * 1. Receive a PATH ERROR or Notify Message for the already 
     *    established tunnel.
     * 2. Receive a PATH ERROR or Notify Message for the currently establishing 
     *    tunnel.
     *
     * But it should not be filled when error node is not seen in
     * administrator configured ERO. */
    if (u1ExcludeHopCount != RPTE_ZERO)
    {
        for (u2HopCount = RPTE_ZERO; u2HopCount < u1ExcludeHopCount;
             u2HopCount++)
        {
            RSVPTE_DBG2 (RSVPTE_REOPT_DBG,
                         "RpteCspfCompForReoptimizeTnlAtMidNode : "
                         "Reoptimization Maintainance Type(%d) "
                         "Exclude Hop(%x)\n",
                         pRsvpTeTnlInfo->u1ReoptMaintenanceType,
                         OSIX_NTOHL (au4ExcludeHop[u2HopCount]));

            if (RpteIsFirstHopAsLoose (pRsvpTeTnlInfo) == RPTE_SUCCESS)
            {

                pEROList = &pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo->ErHopList;

                pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_First (pEROList);

                if (pRsvpTeErHopInfo != NULL)
                {
                    CONVERT_TO_INTEGER (pRsvpTeErHopInfo->IpAddr.au1Ipv4Addr,
                                        u4HopIpAddr);
                    u4HopIpAddr = OSIX_NTOHL (u4HopIpAddr);

                    pCspfCompInfo->u4DestIpAddr = u4HopIpAddr;
                }
            }

            if (pRsvpTeTnlInfo->u1ReoptMaintenanceType ==
                RPTE_REOPT_NODE_MAINTENANCE)
            {
                pCspfCompInfo->osTeAlternateRoute.aNextHops[u2HopCount].
                    u4RouterId = OSIX_NTOHL (au4ExcludeHop[u2HopCount]);
            }
            else if (pRsvpTeTnlInfo->u1ReoptMaintenanceType ==
                     RPTE_REOPT_LINK_MAINTENANCE)
            {
                /* Remote Identifier should also be copied here. */
                pCspfCompInfo->osTeAlternateRoute.aNextHops[u2HopCount].
                    u4NextHopIpAddr = OSIX_NTOHL (au4ExcludeHop[u2HopCount]);
            }

        }

        pCspfCompInfo->osTeAlternateRoute.u2Info = OSPF_TE_FAILED_LINK;
        pCspfCompInfo->osTeAlternateRoute.u2HopCount = u2HopCount;

        u2HopCount = RPTE_ZERO;
    }
    else if ((pTeTnlInfo->pTePathListInfo != NULL) &&
             (pTeTnlInfo->bIsOamPathChange == FALSE))
    {
        /* Fill the explicit route */
        if (pTeTnlInfo->pTePathInfo != NULL)
        {
            pEROList = &pTeTnlInfo->pTePathInfo->ErHopList;
            RpteFillExpRoute (pEROList, pCspfCompInfo, pTeTnlInfo);
        }

    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
    {
        pCspfCompInfo->u1PathInfo |= OSPF_TE_BI_DIRECTIONAL_PATH_BIT_MASK;
    }

    pRsvpTeTnlInfo->u1CSPFPathRequested = RPTE_TRUE;

    /* Enqueueing the Message in OSPF TE Queue and Indicating OSPF TE about
     * the Queue Object */
    if (OspfTeEnqueueMsgFromRsvpTe (pCspfCompInfo) != OSPF_TE_SUCCESS)
    {
        RpteUtlFillErrorTable (RPTE_ZERO, RPTE_ZERO,
                               pCspfCompInfo->u4SrcIpAddr, pRsvpTeTnlInfo);
        RpteStartCspfIntervalTimer (pRsvpTeTnlInfo);

        MemReleaseMemBlock (RPTE_CSPF_COMP_INFO_POOL_ID,
                            (UINT1 *) pCspfCompInfo);

        RSVPTE_DBG (RSVPTE_PORT_DBG,
                    "RESV: Msg Posting to OSPF-TE Unsuccessful. \n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteCspfCompForBkpOrProtTnl "
                    "- INTMD EXIT\n");
        return RPTE_FAILURE;
    }

    RpteStartCspfIntervalTimer (pRsvpTeTnlInfo);
    MemReleaseMemBlock (RPTE_CSPF_COMP_INFO_POOL_ID, (UINT1 *) pCspfCompInfo);

#else
    UNUSED_PARAM (pRsvpTeTnlInfo);
#endif

    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteFrrGblRevertiveOptPathTrigger                      */
/* Description     : This function computes the optimised primary path      */
/* Input (s)       : pCtTnlInfo - RSVP Tunnel info.                         */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteFrrGblRevertiveOptPathTrigger (tRsvpTeTnlInfo * pCtTnlInfo)
{
    tRsvpTeArHopInfo   *pTnlArHopInfo = NULL;
    UINT1               u1Found = RPTE_FALSE;

    RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                "RpteFrrGblRevertiveOptPathTrigger : ENTRY\n");

    /* Because the tunnel does not exist, stopping the timer. */
    if ((RPTE_TE_TNL (pCtTnlInfo) == NULL) ||
        (RpteCheckTnlInTnlTable
         (gpRsvpTeGblInfo, RSVPTE_TNL_TNLINDX (pCtTnlInfo),
          RSVPTE_TNL_TNLINST (pCtTnlInfo),
          OSIX_NTOHL (RSVPTE_TNL_INGRESS (pCtTnlInfo)),
          OSIX_NTOHL (RSVPTE_TNL_EGRESS (pCtTnlInfo)),
          &pCtTnlInfo) != RPTE_SUCCESS))
    {
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_GBL_REVERT_TIMER (pCtTnlInfo));
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RpteFrrGblRevertiveOptPathTrigger : EXIT\n");
        return;
    }

    /* Get the First Hop Address in Tunnel Actual Route Hop Info */
    pTnlArHopInfo = (tRsvpTeArHopInfo *)
        (TMO_SLL_First (&RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo)));
    if (pTnlArHopInfo == NULL)
    {
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_GBL_REVERT_TIMER (pCtTnlInfo));
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RpteFrrGblRevertiveOptPathTrigger : EXIT\n");
        return;
    }

    while (pTnlArHopInfo != NULL)
    {
        if (((pTnlArHopInfo->u1Flags) & LOCAL_PROT_IN_USE) == LOCAL_PROT_IN_USE)
        {
            u1Found = RPTE_TRUE;
            break;
        }
        pTnlArHopInfo =
            (tRsvpTeArHopInfo *)
            TMO_SLL_Next (&RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo),
                          (&(pTnlArHopInfo->NextHop)));
    }
    /* Check whether Headend + PLR is in use */
    if ((u1Found == RPTE_FALSE) &&
        ((RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) & LOCAL_PROT_IN_USE)
         == LOCAL_PROT_IN_USE))
    {
        u1Found = RPTE_TRUE;
    }
    if (u1Found == RPTE_FALSE)
    {
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_GBL_REVERT_TIMER (pCtTnlInfo));
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RpteFrrGblRevertiveOptPathTrigger : EXIT\n");
        return;
    }
    /* FRR global revertive handle in head-end */
    /* This compuation is for the protected tnl to get the optimal path */
    RSVPTE_TNL_GBL_REVERT_FLAG (pCtTnlInfo) = RPTE_TRUE;
    RPTE_FRR_CSPF_INFO (pCtTnlInfo) &= (UINT1) (~(RPTE_FRR_CSPF_COMPUTED));

    RpteCspfCompForBkpOrProtTnl (pCtTnlInfo);

    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_GBL_REVERT_TIMER (pCtTnlInfo));
    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteCspfCompForBkpOrProtTnl : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRandomDelay                                        */
/* Description     : This function returns a random number in the range     */
/*                   (0.5 uRange - 1.5 u4Range)                             */
/* Input (s)       : u4Range - Range value                                  */
/* Output (s)      : None                                                   */
/* Returns         : Returns a Random number                                */
/****************************************************************************/
UINT4
RpteRandomDelay (UINT4 u4Range)
{
    UINT4               u4Min = 0;
    UINT4               u4Max = 0;

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteRandomDelay : ENTRY \n");
    if (u4Range == RSVPTE_ZERO)
    {
        u4Range++;
    }

    u4Min = (u4Range / RPTE_TWO);
    u4Max = ((u4Range * RPTE_THREE) / RPTE_TWO);

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteRandomDelay : EXIT \n");
    return ((UINT4) OSIX_RAND (u4Min, u4Max));
}

/*****************************************************************************/
/* Function Name : RpteRhSendMplsMlibUpdate                                  */
/* Description   : This routine prepares the necessary structure/information */
/*                 and invokes the interface routine provided by MPLS-FM     */
/*                 module to update the MLIB maintained by the MPLS-FM.      */
/* Input(s)      : u2MlibOperation - This field indicates the Operation to be*/
/*                 performed in the MLIB. It takes on any one of the follwing*/
/*                 five values                                               */
/*                 RPTE_MPLS_MLIB_ILM_CREATE, RPTE_MPLS_MLIB_ILM_DELETE,     */
/*                 RPTE_MPLS_MLIB_TNL_CREATE, RPTE_MPLS_MLIB_TNL_DELETE,     */
/*                 RPTE_MPLS_MLIB_TNL_MODIFY                                 */
/*                 u1OperState - Indicates whether the data Tx is enabled    */
/*                 disabled on this Tnl.                                     */
/*                 pCtTnlInfo - Pointer to the RsvpTeTnlInfo for which the   */
/*                 Label related information is to be updated in the MLIB.   */
/*                 u1SegDirection - indicates the tunnel direcation          */
/* Output(s)     : None.                                                     */
/* Return(s)     : RPTE_SUCCESS /RPTE_FAILURE                                */
/*****************************************************************************/
UINT1
RpteRhSendMplsMlibUpdate (UINT2 u2MlibOperation, UINT1 u1OperState,
                          tRsvpTeTnlInfo * pCtTnlInfo, UINT1 u1SegDirection)
{
    tNHLFE              Nhlfe;
    tLspInfo            LspInfo;
    tFecParams          FecParams;
    tIfEntry           *pIfEntry = NULL;
    tRsvpTeArHopInfo   *pTnlArHopInfo = NULL;
    tRsvpTeTnlInfo     *pFATnlInfo = NULL;
    UINT4               u4TmpLbl;
    UINT1               u1Exp;
    UINT1               u1TnlNodeRel = RPTE_ZERO;
    BOOL1               bMlibOprFailure = RPTE_FALSE;
    BOOL1               bL3IfNeeded = RPTE_FALSE;
    tMplsDiffServParams DiffParams;
    tElspMapRow         ElspMapRow;
    tMplsDiffServElspInfo *pTempElspInfo = NULL;
    UINT4               u4L3If = 0;
    UINT4               u4RemoteIp = 0;
    UINT4               u4TeLinkIf = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    RSVPTE_DBG (RSVPTE_PORT_ETEXT, " RpteRhSendMplsMlibUpdate : ENTRY \n");
    MEMSET (&Nhlfe, RSVPTE_ZERO, sizeof (tNHLFE));
    MEMSET (&LspInfo, RSVPTE_ZERO, sizeof (tLspInfo));
    MEMSET (&FecParams, RSVPTE_ZERO, sizeof (tFecParams));

    if (RPTE_TE_TNL (pCtTnlInfo) == NULL)
    {
        return RPTE_FAILURE;
    }

    /* Do not remove the hw entry when the RSVP-TE component is made
     * shut with GR enabled
     * */
    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_SHUT_DOWN_IN_PROGRESS) &&
        (gi4MplsSimulateFailure != RPTE_SIM_FAILURE_NO_ILM))
    {
        return RPTE_SUCCESS;
    }

    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pCtTnlInfo->u1IsGrTnl == RPTE_YES))
    {
        return RPTE_SUCCESS;
    }
    /* 
     * If the node is Ingress, only possible operation is FTN_CREATE/FTN_DELETE.
     */
    u1TnlNodeRel = RSVPTE_TNL_NODE_RELATION (pCtTnlInfo);
    if ((RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL) &&
        (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_BACKUP_TNL) &&
        (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL (pCtTnlInfo)))) &&
        ((u2MlibOperation == RPTE_MPLS_MLIB_ILM_CREATE) ||
         (u2MlibOperation == RPTE_MPLS_MLIB_ILM_DELETE)))
    {
        u1TnlNodeRel = RPTE_INTERMEDIATE;
    }

    switch (u1TnlNodeRel)
    {
        case RPTE_INGRESS:
            if (u1SegDirection == GMPLS_SEGMENT_REVERSE)
            {
                if (!((u2MlibOperation == RPTE_MPLS_MLIB_ILM_CREATE) ||
                      (u2MlibOperation == RPTE_MPLS_MLIB_ILM_DELETE)))
                {
                    bMlibOprFailure = RPTE_TRUE;
                }
            }
            else
            {
                if (!((u2MlibOperation == RPTE_MPLS_MLIB_TNL_CREATE) ||
                      (u2MlibOperation == RPTE_MPLS_MLIB_TNL_DELETE) ||
                      (u2MlibOperation == RPTE_MPLS_MLIB_TNL_MODIFY)))

                {
                    bMlibOprFailure = RPTE_TRUE;
                }
            }
            break;
        case RPTE_EGRESS:
            if (u1SegDirection == GMPLS_SEGMENT_REVERSE)
            {
                if (!((u2MlibOperation == RPTE_MPLS_MLIB_TNL_CREATE) ||
                      (u2MlibOperation == RPTE_MPLS_MLIB_TNL_DELETE) ||
                      (u2MlibOperation == RPTE_MPLS_MLIB_TNL_MODIFY)))
                {
                    bMlibOprFailure = RPTE_TRUE;
                }
            }
            else
            {
                if (!((u2MlibOperation == RPTE_MPLS_MLIB_ILM_CREATE) ||
                      (u2MlibOperation == RPTE_MPLS_MLIB_ILM_DELETE)))
                {
                    bMlibOprFailure = RPTE_TRUE;
                }
            }
            break;
        case RPTE_INTERMEDIATE:
            if (!((u2MlibOperation == RPTE_MPLS_MLIB_ILM_CREATE) ||
                  (u2MlibOperation == RPTE_MPLS_MLIB_ILM_DELETE)))
            {
                bMlibOprFailure = RPTE_TRUE;
            }
            break;
        default:
            break;
    }
    if (bMlibOprFailure == RPTE_TRUE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "RESV: MplsMlibUpdate failed for ILM Operation..\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    " RpteRhSendMplsMlibUpdate : INTMD-EXIT\n");
        return RPTE_FAILURE;
    }
    /* This case is applcable for
       1. Ingress and forward direction
       2. Egress and reverse direation
       3. Intermediate and forward direction
       4. Intermediate and reverse direction */
    if (((u1SegDirection == GMPLS_SEGMENT_FORWARD) &&
         (u1TnlNodeRel != RPTE_INGRESS)) ||
        ((u1SegDirection == GMPLS_SEGMENT_REVERSE) &&
         (u1TnlNodeRel != RPTE_EGRESS)))
    {
        if (RSVPTE_TNL_PSB (pCtTnlInfo) != NULL)
        {
            if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
            {
                pIfEntry = PSB_IF_ENTRY (RSVPTE_TNL_PSB (pCtTnlInfo));
                RptePortTlmGetCompIfIndex (pCtTnlInfo->pTeTnlInfo->
                                           u4UpStrDataTeLinkIf, &u4L3If);
                /* This is for bundle case.
                 * For bundle case, the actual component link is stored 
                 * when the resource reservation done */
                if (u4L3If == RPTE_ZERO)
                {
                    u4L3If = pCtTnlInfo->pTeTnlInfo->u4FwdComponentIfIndex;
                }
            }
            else
            {
                pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pCtTnlInfo));
                RptePortTlmGetCompIfIndex (pCtTnlInfo->pTeTnlInfo->
                                           u4DnStrDataTeLinkIf, &u4L3If);
                /* This is for bundle case.
                 * For bundle case, the actual component link is stored 
                 * when the resource reservation done */
                if (u4L3If == RPTE_ZERO)
                {
                    u4L3If = pCtTnlInfo->pTeTnlInfo->u4RevComponentIfIndex;
                }
            }
        }
        else
        {
            RSVPTE_DBG (RSVPTE_IF_PRCS,
                        "RESV: MplsMlibUpdate failed for Psb not available\n");
            RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                        " RpteRhSendMplsMlibUpdate : INTMD-EXIT\n");

            return RPTE_FAILURE;
        }
        if ((pIfEntry == NULL) && (RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL) &&
            (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_BACKUP_TNL) &&
            (RPTE_IS_PLR
             (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL (pCtTnlInfo)))))
        {
            pIfEntry = PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                     (RPTE_FRR_BASE_TNL (pCtTnlInfo)));
        }
        if (pIfEntry == NULL)
        {
            return RPTE_FAILURE;
        }
        if (u4L3If == RPTE_ZERO)
        {
#ifdef CFA_WANTED
            if (CfaUtilGetL3IfFromMplsIf (IF_ENTRY_IF_INDEX (pIfEntry),
                                          &u4L3If, TRUE) == CFA_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_IF_PRCS,
                            "RESV: MplsMlibUpdate fail to Get L3IF from MPLS IF\n");
                return RPTE_FAILURE;
            }
#else
            LspInfo.u4IfIndex = IF_ENTRY_IF_INDEX (pIfEntry);
#endif
        }

        LspInfo.u4IfIndex = u4L3If;

        if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
        {
            /* For forward direction, downstream in-label should be
             * programmed for this case */
            if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM)
            {
                /* Assinging the VCI value */
                u4TmpLbl = pCtTnlInfo->DownstrInLbl.AtmLbl.u2Vci;
                u4TmpLbl = u4TmpLbl << 16;

                /* Assinging the VPI value */
                u4TmpLbl |= pCtTnlInfo->DownstrInLbl.AtmLbl.u2Vpi;

                MEMCPY (&LspInfo.u4InTopLabel, &u4TmpLbl, sizeof (UINT4));

            }
            else if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ETHERNET)
            {
                LspInfo.u4InTopLabel = pCtTnlInfo->DownstrInLbl.u4GenLbl;
            }
        }
        else
        {
            /* For reverse direction, upstream in-label should be programmed 
             * for this case */
            LspInfo.u4InTopLabel = pCtTnlInfo->UpStrInLbl.u4GenLbl;
        }
    }
    FecParams.u1FecType = RSVPTE_TNL_TYPE;
    FecParams.u4TnlId = RSVPTE_TNL_TNLINDX (pCtTnlInfo);
    FecParams.u4TnlInstance = RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (pCtTnlInfo));
    FecParams.u4IngressId = OSIX_NTOHL (RSVPTE_TNL_INGRESS (pCtTnlInfo));
    FecParams.u4EgressId = OSIX_NTOHL (RSVPTE_TNL_EGRESS (pCtTnlInfo));

    rpteTeGetTrafficParams (pCtTnlInfo->pTeTnlInfo,
                            &(FecParams.MplsTrfcParms.u4PeakDataRate),
                            &(FecParams.MplsTrfcParms.u4PeakBurstSize),
                            &(FecParams.MplsTrfcParms.u4CommittedDataRate),
                            &(FecParams.MplsTrfcParms.u4CommittedBurstSize),
                            &(FecParams.MplsTrfcParms.u4ExcessBurstSize));

    /* NOTE : initial implementation supports stack depth of 1 only */
    if (u2MlibOperation == RPTE_MPLS_MLIB_TNL_MODIFY)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "RESV: MplsMlibUpdate - Tnl Modify\n");
        if (u1OperState == RPTE_DATA_TX_UP)
        {
            RSVPTE_DBG (RSVPTE_IF_PRCS, "RESV: MplsMlibUpdate - Data Tx Up\n");
            Nhlfe.u1OperStatus = RPTE_OPER_UP;
        }
        else
        {
            RSVPTE_DBG (RSVPTE_IF_PRCS,
                        "RESV: MplsMlibUpdate - Data Tx Down\n");
            Nhlfe.u1OperStatus = RPTE_OPER_DOWN;
        }
    }
    else
    {
        switch (u2MlibOperation)
        {
            case RPTE_MPLS_MLIB_TNL_CREATE:
                RSVPTE_DBG (RSVPTE_IF_PRCS,
                            "RESV: MplsMlibUpdate - Tnl Create\n");
                break;

            case RPTE_MPLS_MLIB_TNL_DELETE:
                RSVPTE_DBG (RSVPTE_IF_PRCS,
                            "RESV: MplsMlibUpdate - Tnl Delete\n");
                break;

            case RPTE_MPLS_MLIB_ILM_CREATE:
                RSVPTE_DBG (RSVPTE_IF_PRCS,
                            "RESV: MplsMlibUpdate - ILM Create\n");
                break;

            case RPTE_MPLS_MLIB_ILM_DELETE:
                RSVPTE_DBG (RSVPTE_IF_PRCS,
                            "RESV: MplsMlibUpdate - ILM Delete\n");
                break;

            default:
                RSVPTE_DBG (RSVPTE_IF_PRCS, "RESV: MplsMlibUpdate - Invalid\n");
                return RPTE_FAILURE;
        }
        Nhlfe.u1OperStatus = RPTE_OPER_UP;
    }
    /* This case is applcable for
       1. Ingress and reverse direction
       2. Egress and forward direation
       3. Intermediate and forward direction
       4. Intermediate and reverse direction */
    if (((u1SegDirection == GMPLS_SEGMENT_FORWARD) &&
         (u1TnlNodeRel != RPTE_EGRESS)) ||
        ((u1SegDirection == GMPLS_SEGMENT_REVERSE) &&
         (u1TnlNodeRel != RPTE_INGRESS)))
    {
        if (((u1SegDirection == GMPLS_SEGMENT_FORWARD) &&
             (RSVPTE_TNL_RSB (pCtTnlInfo) == NULL)) ||
            ((u1SegDirection == GMPLS_SEGMENT_REVERSE) &&
             (RSVPTE_TNL_PSB (pCtTnlInfo) == NULL)))
        {
            RSVPTE_DBG (RSVPTE_IF_PRCS, "RESV: MplsMlibUpdate: Tunnel RSB"
                        "is NULL...\n");
            RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                        " RpteRhSendMplsMlibUpdate : INTMD-EXIT\n");
            return RPTE_FAILURE;
        }

        if ((RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_BACKUP_TNL) &&
            (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS))
        {
            Nhlfe.NextHopAddr.u4Addr = pCtTnlInfo->pRsb->NextRsvpHop.u4HopAddr;
        }
        else if ((RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL) &&
                 (RSVPTE_TNL_RSB (RPTE_FRR_BASE_TNL (pCtTnlInfo)) != NULL) &&
                 (RPTE_IS_MP (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL
                                                   (pCtTnlInfo)))))
        {
            Nhlfe.NextHopAddr.u4Addr =
                RPTE_FRR_BASE_TNL (pCtTnlInfo)->pRsb->NextRsvpHop.u4HopAddr;
        }
        else
        {
            Nhlfe.u1NHAddrType = GMPLS_IPV4;
            if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
            {
                RptePortCfaGetTeLinkIfFromL3If (pCtTnlInfo->pTeTnlInfo->
                                                u4FwdComponentIfIndex,
                                                &u4TeLinkIf);
                RptePortTlmGetRemoteIP (u4TeLinkIf, &u4RemoteIp);
                Nhlfe.NextHopAddr.u4Addr = u4RemoteIp;
                /* For Non TLM scenario */
                if (Nhlfe.NextHopAddr.u4Addr == RPTE_ZERO)
                {
                    /* In this case Egress won't have a next hop */
                    Nhlfe.NextHopAddr.u4Addr =
                        (pCtTnlInfo->pRsb->NextRsvpHop.u4HopAddr);
                }
                /* For unnumbered case,  Address type should be unknown */
                if (pCtTnlInfo->u4DnStrDataTeLinkIfId != RPTE_ZERO)
                {
                    Nhlfe.u1NHAddrType = GMPLS_UNKNOWN;
                }
            }
            else
            {
                RptePortCfaGetTeLinkIfFromL3If (pCtTnlInfo->pTeTnlInfo->
                                                u4RevComponentIfIndex,
                                                &u4TeLinkIf);
                RptePortTlmGetRemoteIP (u4TeLinkIf, &u4RemoteIp);
                Nhlfe.NextHopAddr.u4Addr = u4RemoteIp;
                /* For Non TLM scenario */
                if (Nhlfe.NextHopAddr.u4Addr == RPTE_ZERO)
                {
                    /* In this case Ingress  won't have a next hop */
                    Nhlfe.NextHopAddr.u4Addr =
                        (pCtTnlInfo->pPsb->RsvpHop.u4HopAddr);
                }
                /* For unnumbered case, Address type should be unknown */
                if (pCtTnlInfo->u4UpStrDataTeLinkIfId != RPTE_ZERO)
                {
                    Nhlfe.u1NHAddrType = GMPLS_UNKNOWN;
                }
            }
        }

        RSVPTE_DBG1 (RSVPTE_IF_PRCS, "Nexthop Addr: %x\n",
                     Nhlfe.NextHopAddr.u4Addr);

        if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
        {
            pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pCtTnlInfo));
            /* Since in RSVP TE scenario there will be only one outgoing interface
               the interface at zeroth index is obtained */
            if (pIfEntry == NULL)
            {
                RSVPTE_DBG (RSVPTE_IF_PRCS,
                            "RESV: MplsMlibUpdate:IfEntry is NULL!! \n");
                return RPTE_FAILURE;
            }
            if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM)
            {
                /* Assinging the VCI value */
                u4TmpLbl = pCtTnlInfo->DownstrOutLbl.AtmLbl.u2Vci;
                u4TmpLbl = u4TmpLbl << 16;

                /* Assinging the VPI value */
                u4TmpLbl |= pCtTnlInfo->DownstrOutLbl.AtmLbl.u2Vpi;

                MEMCPY (&Nhlfe.u4OutLabel, &u4TmpLbl, sizeof (UINT4));

            }
            else if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ETHERNET)
            {
                Nhlfe.u4OutLabel = pCtTnlInfo->DownstrOutLbl.u4GenLbl;
            }

            if (pCtTnlInfo->pTeTnlInfo->u4FwdComponentIfIndex != RPTE_ZERO)
            {
                Nhlfe.u4OutIfIndex =
                    pCtTnlInfo->pTeTnlInfo->u4FwdComponentIfIndex;
            }
            else
            {
                bL3IfNeeded = RPTE_TRUE;
                Nhlfe.u4OutIfIndex = pIfEntry->u4IfIndex;
            }
        }
        else
        {
            pIfEntry = PSB_IF_ENTRY (RSVPTE_TNL_PSB (pCtTnlInfo));
            MEMCPY (&Nhlfe.u4OutLabel, &pCtTnlInfo->UpStrOutLbl,
                    sizeof (UINT4));
            if (pIfEntry == NULL)
            {
                RSVPTE_DBG (RSVPTE_IF_PRCS,
                            "RESV: MplsMlibUpdate:IfEntry is NULL!! \n");
                return RPTE_FAILURE;
            }
            if (pCtTnlInfo->pTeTnlInfo->u4RevComponentIfIndex != RPTE_ZERO)
            {
                Nhlfe.u4OutIfIndex =
                    pCtTnlInfo->pTeTnlInfo->u4RevComponentIfIndex;
            }
            else
            {
                bL3IfNeeded = RPTE_TRUE;
                Nhlfe.u4OutIfIndex = pIfEntry->u4IfIndex;
            }
        }

        if (bL3IfNeeded == RPTE_TRUE)
        {
            if (CfaUtilGetL3IfFromMplsIf (Nhlfe.u4OutIfIndex,
                                          &(Nhlfe.u4OutIfIndex), TRUE)
                == CFA_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_IF_PRCS,
                            "RESV: MplsMlibUpdate fail to Get L3IF from MPLS IF\n");
                return RPTE_FAILURE;
            }
        }
    }
    /* 
     * If RRO is present in the TnlInfo, the RRO count is used for updating the
     * u1NumDecTtl. else default value of 1 is used 
     */

    if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
    {                            /* For forward direction, out-arhop list should be taken */
        if ((RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) != NULL) &&
            (TMO_SLL_Count (&RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo)) !=
             RSVPTE_ZERO))
        {
            Nhlfe.u1NumDecTtl =
                (UINT1) (TMO_SLL_Count (&RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo))
                         - 1);
        }
    }
    else
    {
        /* For reverse direction, in-arhop list should be taken */
        if ((RSVPTE_IN_ARHOP_LIST_INFO (pCtTnlInfo) != NULL) &&
            (TMO_SLL_Count (&RSVPTE_TNL_INARHOP_LIST (pCtTnlInfo)) !=
             RSVPTE_ZERO))
        {
            Nhlfe.u1NumDecTtl =
                (UINT1) (TMO_SLL_Count (&RSVPTE_TNL_INARHOP_LIST (pCtTnlInfo))
                         - RSVPTE_ONE);
        }
    }

    /* Possible only in the penultimate Hop */
    if (Nhlfe.u1NumDecTtl == RSVPTE_ZERO)
    {
        Nhlfe.u1NumDecTtl = RSVPTE_ONE;
    }

    switch (u1TnlNodeRel)
    {
        case RPTE_INGRESS:
            Nhlfe.u1Operation = RPTE_MPLS_OPR_PUSH;
            break;
        case RPTE_EGRESS:
            Nhlfe.u1Operation = RPTE_MPLS_OPR_POP;
            break;
        default:
            Nhlfe.u1Operation = RPTE_MPLS_OPR_POP_PUSH;
            break;
    }
    if (RPTE_TE_DS_TNL (pCtTnlInfo) != NULL)
    {
        DiffParams.u1ServiceType = RSVPTE_TE_DS_TNL_SERVICE_TYPE (pCtTnlInfo);
        DiffParams.u1Creator = MPLS_CTRL_PROTO;
        if (DiffParams.u1ServiceType == RPTE_DIFFSERV_LLSP)
        {
            DiffParams.unLspParams.u1LlspPscDscp =
                RSVPTE_TE_DS_TNL_LLSP_DSCP (pCtTnlInfo);
        }
        else
        {
            DiffParams.unLspParams.ElspParams.u1ElspType =
                RSVPTE_TE_DS_TNL_ELSP_TYPE (pCtTnlInfo);
            if (DiffParams.unLspParams.ElspParams.u1ElspType
                == MPLS_DIFFSERV_PRECONF_ELSP)
            {
                DiffParams.unLspParams.ElspParams.pElspSigMap = NULL;
            }
            else
            {
                ElspMapRow.u1Creator = MPLS_CTRL_PROTO;
                for (u1Exp = RSVPTE_ZERO; u1Exp < RSVPTE_EIGHT; u1Exp++)
                {
                    ElspMapRow.aElspExpPhbMapArray[u1Exp].u1IsExpValid =
                        MPLS_FALSE;
                }
                if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pCtTnlInfo) != RSVPTE_ZERO)
                {
                    TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST (pCtTnlInfo),
                                  pTempElspInfo, tMplsDiffServElspInfo *)
                    {
                        u1Exp = pTempElspInfo->u1ElspInfoIndex;
                        ElspMapRow.aElspExpPhbMapArray[u1Exp].u1IsExpValid =
                            MPLS_TRUE;
                        ElspMapRow.aElspExpPhbMapArray[u1Exp].u1ElspPhbDscp =
                            pTempElspInfo->u1PhbDscp;
                    }
                    DiffParams.unLspParams.ElspParams.pElspSigMap = &ElspMapRow;
                }
            }
        }
        Nhlfe.pDiffServParams = &DiffParams;
    }
    if ((RPTE_FRR_STACK_BIT (pCtTnlInfo) == TRUE) &&
        ((u2MlibOperation == RPTE_MPLS_MLIB_TNL_CREATE) ||
         (u2MlibOperation == RPTE_MPLS_MLIB_ILM_DELETE) ||
         (u2MlibOperation == RPTE_MPLS_MLIB_ILM_CREATE)))
    {
        if (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) != NULL)
        {
            Nhlfe.StackTnlInfo.u1StackTnlBit = TRUE;
            Nhlfe.StackTnlInfo.u4TnlId =
                RSVPTE_TNL_TNLINDX (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo));
            Nhlfe.StackTnlInfo.u4TnlInstance =
                RSVPTE_TNL_TNLINST (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo));
            Nhlfe.StackTnlInfo.u4IngressId =
                OSIX_NTOHL (RSVPTE_TNL_INGRESS
                            (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo)));
            Nhlfe.StackTnlInfo.u4EgressId =
                OSIX_NTOHL (RSVPTE_TNL_EGRESS
                            (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo)));
            if (RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) == NULL)
            {
                RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                            " RpteRhSendMplsMlibUpdate : INTMD-EXIT\n");
                return RPTE_FAILURE;
            }
            /* Stack with the top label of ARHOP list */
            pTnlArHopInfo = (tRsvpTeArHopInfo *)
                (TMO_SLL_First (&RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo)));
            if (pTnlArHopInfo == NULL)
            {
                RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                            " RpteRhSendMplsMlibUpdate : INTMD-EXIT\n");
                return RPTE_FAILURE;
            }

            if (RSVPTE_TE_TNL_FRR_PROT_TYPE
                (RPTE_TE_TNL (pCtTnlInfo)) == TE_TNL_FRR_PROT_NODE)
            {
                pTnlArHopInfo = (tRsvpTeArHopInfo *)
                    TMO_SLL_Next (&RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo),
                                  &pTnlArHopInfo->NextHop);
                if (pTnlArHopInfo == NULL)
                {
                    RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                                " RpteRhSendMplsMlibUpdate : INTMD-EXIT\n");
                    return RPTE_FAILURE;
                }
            }

            Nhlfe.u4OutLabel = pTnlArHopInfo->GmplsTnlArHopInfo.u4ForwardLbl;

            Nhlfe.u4OutIfIndex = TE_TNL_IFINDEX (RPTE_TE_TNL
                                                 (RPTE_FRR_OUT_TNL_INFO
                                                  (pCtTnlInfo)));
        }
        else if (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo) != NULL)
        {
            LspInfo.StackTnlInfo.u1StackTnlBit = TRUE;
            LspInfo.StackTnlInfo.u4TnlId =
                RSVPTE_TNL_TNLINDX (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo));
            LspInfo.StackTnlInfo.u4TnlInstance =
                RSVPTE_TNL_TNLINST (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo));
            LspInfo.StackTnlInfo.u4IngressId =
                OSIX_NTOHL (RSVPTE_TNL_INGRESS
                            (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)));
            LspInfo.StackTnlInfo.u4EgressId =
                OSIX_NTOHL (RSVPTE_TNL_EGRESS
                            (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)));
            pIfEntry =
                PSB_IF_ENTRY (RSVPTE_TNL_PSB
                              (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)));
#ifdef CFA_WANTED
            if (CfaUtilGetL3IfFromMplsIf
                (IF_ENTRY_IF_INDEX (pIfEntry), &u4L3If, TRUE) == CFA_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_IF_PRCS,
                            "RESV: MplsMlibUpdate fail to Get L3IF from MPLS IF\n");
                return RPTE_FAILURE;
            }
            LspInfo.u4IfIndex = u4L3If;
#endif
            LspInfo.StackTnlInfo.u2IsFrrMPByPassTnl = TRUE;
        }
        else
        {
            u4IngressId = RpteUtilConvertToInteger
                (pCtTnlInfo->pTeTnlInfo->TnlMapIngressLsrId);
            u4EgressId = RpteUtilConvertToInteger
                (pCtTnlInfo->pTeTnlInfo->TnlMapEgressLsrId);
            u4IngressId = OSIX_NTOHL (u4IngressId);
            u4EgressId = OSIX_NTOHL (u4EgressId);
            if ((RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                         pCtTnlInfo->pTeTnlInfo->u4TnlMapIndex,
                                         pCtTnlInfo->pTeTnlInfo->
                                         u4TnlMapInstance, u4IngressId,
                                         u4EgressId,
                                         &pFATnlInfo) == RPTE_SUCCESS)
                &&
                (((RSVPTE_TNL_NODE_RELATION (pFATnlInfo) == RPTE_INGRESS)
                  && (u1SegDirection == GMPLS_SEGMENT_FORWARD))
                 || ((RSVPTE_TNL_NODE_RELATION (pFATnlInfo) == RPTE_EGRESS)
                     && (u1SegDirection == GMPLS_SEGMENT_REVERSE))))
            {

                Nhlfe.StackTnlInfo.u1StackTnlBit = TRUE;
                Nhlfe.StackTnlInfo.u4TnlId = pCtTnlInfo->pTeTnlInfo->
                    u4TnlMapIndex;
                Nhlfe.StackTnlInfo.u4TnlInstance = pCtTnlInfo->pTeTnlInfo->
                    u4TnlMapInstance;
                Nhlfe.StackTnlInfo.u4IngressId = u4IngressId;
                Nhlfe.StackTnlInfo.u4EgressId = u4EgressId;
                if (RSVPTE_TNL_NODE_RELATION (pFATnlInfo) == RPTE_INGRESS)
                {
                    Nhlfe.u4OutIfIndex = pFATnlInfo->pTeTnlInfo->u4TnlIfIndex;
                }
                else
                {
                    Nhlfe.u4OutIfIndex =
                        pFATnlInfo->pTeTnlInfo->u4RevTnlIfIndex;
                }
            }
        }
    }
    MEMCPY (&LspInfo.FecParams, &FecParams, sizeof (tFecParams));
    LspInfo.pNhlfe = (tNHLFE *) & Nhlfe;
    LspInfo.u1Owner = MPLS_OWNER_RSVPTE;

    if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
    {
        LspInfo.Direction = MPLS_DIRECTION_FORWARD;
    }
    else if (u1SegDirection == GMPLS_SEGMENT_REVERSE)
    {
        LspInfo.Direction = MPLS_DIRECTION_REVERSE;
    }

    /*
     * MPLS_FM API invoked to update the MLIB - In case of third party
     * MPLS-FM this has to be appropriately ported
     */
    if (RPTE_MPLS_MLIB_UPDATE (u2MlibOperation, &LspInfo) != RPTE_MPLS_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "RESV: MplsMlibUpdate failed ..\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    " RpteRhSendMplsMlibUpdate : INTMD-EXIT\n");
        return RPTE_FAILURE;
    }

    if ((pCtTnlInfo->pTeTnlInfo->u1FwdArpResolveStatus == MPLS_ARP_RESOLVED) &&
        (pCtTnlInfo->pTeTnlInfo->u1RevArpResolveStatus == MPLS_ARP_RESOLVED))
    {
        if (pCtTnlInfo->pTeTnlInfo->bIsLabelChange == RPTE_ZERO)
        {
            rpteTeSetTnlOperStatus (pCtTnlInfo->pTeTnlInfo, RPTE_OPER_UP);
            RSVPTE_DBG (RSVPTE_IF_PRCS,
                        "RESV: MplsMlibUpdate Success.. Tunnel Made UP\n");
        }
        else
        {
            pCtTnlInfo->pTeTnlInfo->bIsLabelChange = RPTE_ZERO;
        }
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, " RpteRhSendMplsMlibUpdate : EXIT\n");
    }
    else
    {
        if (pCtTnlInfo->pTeTnlInfo->bIsLabelChange == RPTE_ZERO)
        {
            rpteTeSetTnlOperStatus (pCtTnlInfo->pTeTnlInfo, RPTE_OPER_DOWN);
            RSVPTE_DBG (RSVPTE_IF_PRCS,
                        "RESV: ARP resolution triggered, Tunnel Made DOWN\n");
        }
    }

    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : rptemplsDiffServGetPreConfMap                             */
/* Description   : This function interfaces with FM to get Preconfigured map */
/*                 from its Global structure into the Array passed           */
/* Input(s)      : u4Incarn  - Incarn Id                                     */
/* Output(s)     : aElspPhbMapEntry - A PreAllocated array of size           */
/*                 MPLS_DIFFSERV_MAX_EXP into which the Preconfigured map is */
/*                 copied                                                    */
/* Return(s)     : RPTE_SUCCESS or RPTE_FAILURE                              */
/*****************************************************************************/
UINT4
RpteMplsDiffServGetPreConfPhbMap (UINT4 u4Incarn,
                                  tElspPhbMapEntry aElspPhbMapEntry[])
{
    RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                " RpteMplsDiffServGetPreConfPhbMap: ENTRY\n");
#ifndef NPAPI_WANTED
    if (RPTE_MPLS_DIFF_PRECONF_MAP (u4Incarn, aElspPhbMapEntry) !=
        RPTE_MPLS_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    " RpteMplsDiffServGetPreConfPhbMap:INTMD - EXIT \n ");
        return RPTE_FAILURE;
    }
#else

/*TODO:The above is fm call. so For diffserv same functionality should be added */
    UNUSED_PARAM (u4Incarn);
    UNUSED_PARAM (aElspPhbMapEntry);
#endif

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, " RpteMplsDiffServGetPreConfPhbMap: EXIT\n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteTEEventHandler                                        */
/* Description   : This routine is called by TE module.                      */
/*                 This calls rpteProcessTeEvent, which handles the event    */
/*                 set in u4Event.                                           */
/* Input(s)      : u4Event - Specifies the event given by TE module.         */
/*                 u4TeParams - TE Parameter                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
UINT1
RpteTEEventHandler (UINT4 u4Event, tTeTnlInfo * pTeTnlInfo,
                    UINT1 *pu1IsRpteAdminDown)
{
    tRsvpTeTnlInfo     *pAdnlTnlInfo = NULL;
    UINT4               u4TunnelIndex = RSVPTE_ZERO;
    UINT4               u4TunnelInstance = RSVPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RSVPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RSVPTE_ZERO;

    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;

    MEMSET (&RpteEnqueueMsgInfo, 0, sizeof (tRpteEnqueueMsgInfo));

    /* u4Event will receive only tunnel related events - TunnelUp, 
     * TunnelDestroy, TunnelDown, TunnelDataTransmitEnable and 
     * TunnelDataTransmitDisable. 
     * This function calls the ProcessTe function directly. 
     * When the TE module is made a seperate task, this function should
     * post internal TE related events instead of the function call. */

    if (RSVP_INITIALISED == FALSE)
    {
        *pu1IsRpteAdminDown = RPTE_TRUE;
        return RPTE_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        *pu1IsRpteAdminDown = RPTE_TRUE;
        return RPTE_FAILURE;
    }
    /* Fix for coverity degrade */
    if (pTeTnlInfo == NULL)
    {
        return RPTE_FAILURE;
    }

    if ((pTeTnlInfo != NULL) &&
        (pTeTnlInfo->u1TnlRowStatus == NOT_IN_SERVICE) &&
        (pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_ENABLED))
    {
        return RPTE_SUCCESS;
    }

    if (TE_TNL_REOPTIMIZE_STATUS (pTeTnlInfo) == TE_REOPTIMIZE_MANUAL_TRIG)
    {

        u4TunnelIndex = RSVPTE_TNL_INDEX (pTeTnlInfo);
        u4TunnelInstance = pTeTnlInfo->u4TnlInstance;
        CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID (pTeTnlInfo),
                            u4TunnelIngressLSRId);
        u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
        CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID (pTeTnlInfo),
                            u4TunnelEgressLSRId);
        u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);

        if (RpteCheckAdnlTnlInTnlTable (gpRsvpTeGblInfo,
                                        u4TunnelIndex, u4TunnelInstance,
                                        u4TunnelIngressLSRId,
                                        u4TunnelEgressLSRId,
                                        &pAdnlTnlInfo) == RPTE_FAILURE)
        {
            if (rpteTeReleaseTnlInfo (pTeTnlInfo) == RPTE_TE_FAILURE)
            {
                return RPTE_FAILURE;
            }
            return RPTE_FAILURE;
        }

        if ((RSVPTE_TNL_ROLE (pAdnlTnlInfo->pTeTnlInfo) == RPTE_EGRESS) ||
            ((RSVPTE_TNL_ROLE (pAdnlTnlInfo->pTeTnlInfo) == RPTE_INTERMEDIATE)
             && (RPTE_REOPTIMIZE_NODE_STATE (pAdnlTnlInfo) !=
                 RPTE_MID_POINT_NODE)))
        {
            if (rpteTeReleaseTnlInfo (pTeTnlInfo) == RPTE_TE_FAILURE)
            {
                return RPTE_FAILURE;
            }
            return RPTE_FAILURE;
        }
    }

    RpteEnqueueMsgInfo.u4TeTnlEvent = RPTE_TNL_TE_EVENT;
    RpteEnqueueMsgInfo.u4RpteEvent = u4Event;
    RpteEnqueueMsgInfo.u.pTeTnlInfo = pTeTnlInfo;

    if (RpteEnqueueMsgToRsvpQ (&RpteEnqueueMsgInfo) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to EnQ TE event to RSVP-TE Task\n");
        return RPTE_FAILURE;
    }
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteL3VPNEventHandler                                     */
/* Description   : This routine is called by L3VPN module.                   */
/*                 This calls rpteProcessTeEvent, which handles the event    */
/*                 set in u4Event.                                           */
/* Input(s)      : u4Event - Specifies the event given by L3VPN module.      */
/*                 u4TeParams - TE Parameter                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
UINT1
RpteL3VPNEventHandler (UINT4 u4Event, tTeTnlInfo * pTeTnlInfo)
{
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;

    MEMSET (&RpteEnqueueMsgInfo, 0, sizeof (tRpteEnqueueMsgInfo));

    RpteEnqueueMsgInfo.u4TeTnlEvent = RPTE_TNL_L3VPN_EVENT;
    RpteEnqueueMsgInfo.u4RpteEvent = u4Event;
    RpteEnqueueMsgInfo.TeTnlL3VpnInfo.u4TnlIndex = pTeTnlInfo->u4TnlIndex;
    RpteEnqueueMsgInfo.TeTnlL3VpnInfo.u4TnlInstance = pTeTnlInfo->u4TnlInstance;
    RpteEnqueueMsgInfo.TeTnlL3VpnInfo.u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
    MEMCPY (&RpteEnqueueMsgInfo.TeTnlL3VpnInfo.TnlIngressLsrId,
            &pTeTnlInfo->TnlIngressLsrId, sizeof (UINT4));
    MEMCPY (&RpteEnqueueMsgInfo.TeTnlL3VpnInfo.TnlEgressLsrId,
            &pTeTnlInfo->TnlEgressLsrId, sizeof (UINT4));

    if (RPTE_PROC_EVT_FUNC[RpteEnqueueMsgInfo.u4TeTnlEvent]
        (&RpteEnqueueMsgInfo) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to EnQ TE event to RSVP-TE Task\n");
        return RPTE_FAILURE;
    }

    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteL2VPNEventHandler                                     */
/* Description   : This routine is called by L2VPN module.                   */
/*                 L2VPN module calls this event once it deletes older tunnel*/
/*                 If index in Facility backup method                        */
/*                 set in u4Event.                                           */
/* Input(s)      : u4Event - Specifies the event given by TE module.         */
/*                 u4TeParams - TE Parameter                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
RpteL2VPNEventHandler (tTeTnlInfo * pTeTnlInfo)
{
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;

    MEMSET (&RpteEnqueueMsgInfo, 0, sizeof (tRpteEnqueueMsgInfo));

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        return;
    }

    RpteEnqueueMsgInfo.u4TeTnlEvent = RPTE_TNL_L2VPN_EVENT;
    RpteEnqueueMsgInfo.u.pTeTnlInfo = pTeTnlInfo;

    if (RpteEnqueueMsgToRsvpQ (&RpteEnqueueMsgInfo) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to EnQ TE event to RSVP-TE Task\n");
        return;
    }
    return;
}

/*****************************************************************************/
/* Function Name : RpteTEProcessTeEvent                                      */
/* Description   : This routine is called by RpteTEEventHandler.             */
/*                 This calls the corresponding functions depending          */
/*                 on the event received.                                    */
/* Input(s)      : aTaskEnqMsgInfo[] - Array of Task Enq Message Info.       */
/*                 aTaskEnqMsgInfo[0] - contains the major event (TE_EVENT)  */
/*                 aTaskEnqMsgInfo[1] - contains the minor event (like,      */
/*                                      TunnelUp, TunnelDown, ...            */
/*                 aTaskEnqMsgInfo[2] - contains the TeParams.               */
/*                 aTaskEnqMsgInfo[3] - Dummy.                               */
/* Output(s)     : NONE.                                                     */
/* Return(s)     : RPTE_SUCCESS/RPTE_FAILURE.                                */
/*****************************************************************************/
UINT1
RpteTEProcessTeEvent (tRpteEnqueueMsgInfo * pRpteEnqueueMsgInfo)
{
    UINT1               u1RetVal = RPTE_FAILURE;
    UINT4               u4TunnelIndex = RSVPTE_ZERO;
    UINT4               u4TunnelInstance = RSVPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RSVPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RSVPTE_ZERO;
    UINT4               u4PrimaryTnlInstance = RSVPTE_ZERO;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeWorkTnlInfo = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pNewTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
#ifdef MPLS_L3VPN_WANTED
    tL3VpnRsvpTeLspEventInfo L3VpnRsvpTeLspEventInfo;

    MEMSET (&L3VpnRsvpTeLspEventInfo, 0, sizeof (L3VpnRsvpTeLspEventInfo));
#endif

    pTeTnlInfo = pRpteEnqueueMsgInfo->u.pTeTnlInfo;
    if (pTeTnlInfo != NULL)
    {
        u4TunnelIndex = RSVPTE_TNL_INDEX (pTeTnlInfo);
        u4TunnelInstance = pTeTnlInfo->u4TnlInstance;
        u4PrimaryTnlInstance = RSVPTE_TNL_PRIMARY_INSTANCE (pTeTnlInfo);
        CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID (pTeTnlInfo),
                            u4TunnelIngressLSRId);
        u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
        CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID (pTeTnlInfo),
                            u4TunnelEgressLSRId);
        u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);

    }
    else if (pRpteEnqueueMsgInfo->u4RpteEvent != RSVPTE_TNL_TE_GOING_DOWN)
    {
        return RPTE_FAILURE;
    }

    switch (pRpteEnqueueMsgInfo->u4RpteEvent)
    {
        case RSVPTE_TNL_UP:
            if ((pTeTnlInfo == NULL) ||
                (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN))
            {
                RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteTEProcessTeEvent: "
                            " RSVPTE Globally Down Or Tunnel Not exist in TE"
                            " : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            /* If the passed tunnel instance value is zero, a new entry should be
             * created in TE with owner as RSVP */
            if (u4TunnelInstance == RPTE_ZERO)
            {
                if (rpteTeCreateTunnel (pTeTnlInfo, &pNewTeTnlInfo)
                    == RPTE_TE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                                "RpteTEProcessTeEvent: Tunnel Entry creation "
                                "failed in TE module - INTMD-EXIT\n");
                    return RPTE_FAILURE;
                }

                /* Use the newly created tunnel to create equivalent entry in
                 * RSVP-TE and Tunnel Setup. */
                pTeTnlInfo = pNewTeTnlInfo;

                u4TunnelIndex = RSVPTE_TNL_INDEX (pTeTnlInfo);
                u4TunnelInstance = RSVPTE_TNL_PRIMARY_INSTANCE (pTeTnlInfo);
                CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID (pTeTnlInfo),
                                    u4TunnelIngressLSRId);
                u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
                CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID (pTeTnlInfo),
                                    u4TunnelEgressLSRId);
                u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);
            }
            else
            {
                u4TunnelInstance = u4PrimaryTnlInstance;
            }

            if (RpteCreateNewRsvpteTunnel (u4TunnelIndex,
                                           u4TunnelInstance,
                                           u4TunnelIngressLSRId,
                                           u4TunnelEgressLSRId,
                                           pTeTnlInfo, &pRsvpTeTnlInfo,
                                           RPTE_TRUE) != RPTE_SUCCESS)
            {
                RSVPTE_DBG4 (RSVPTE_MAIN_MEM,
                             "Tunnel creation %d %d %x %x failed in RSVP-TE\n",
                             u4TunnelIndex, u4TunnelInstance,
                             u4TunnelIngressLSRId, u4TunnelEgressLSRId);
                return RPTE_FAILURE;
            }

            RSVPTE_DBG1 (RSVPTE_REOPT_DBG, "RpteTEProcessTeEvent : "
                         "Reoptimization Trigger State(%d)\n",
                         RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo));
            if ((pTeTnlInfo->u1TnlPathType == RPTE_TNL_PROTECTION_PATH) ||
                (pTeTnlInfo->u1TnlRerouteFlag == TRUE) ||
                (pTeTnlInfo->bIsMbbRequired == TRUE) ||
                (RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo) == TRUE))
            {
                if (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                            u4TunnelIndex,
                                            u4PrimaryTnlInstance,
                                            u4TunnelIngressLSRId,
                                            u4TunnelEgressLSRId,
                                            &pRsvpTeWorkTnlInfo)
                    == RPTE_SUCCESS)
                {
                    pRsvpTeTnlInfo->pMapTnlInfo = pRsvpTeWorkTnlInfo;
                    pRsvpTeWorkTnlInfo->pMapTnlInfo = pRsvpTeTnlInfo;
                    pRsvpTeWorkTnlInfo->pTeTnlInfo->u4BkpTnlIndex =
                        u4TunnelIndex;
                    pRsvpTeWorkTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
                        u4TunnelInstance;
                    MEMCPY (pRsvpTeWorkTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                            &RSVPTE_TNL_INGRESS_RTR_ID (pTeTnlInfo),
                            ROUTER_ID_LENGTH);
                    MEMCPY (pRsvpTeWorkTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                            &RSVPTE_TNL_EGRESS_RTR_ID (pTeTnlInfo),
                            ROUTER_ID_LENGTH);

                    if ((pTeTnlInfo->u1TnlRole == RPTE_INTERMEDIATE) &&
                        (pTeTnlInfo->u1TnlPathType == RPTE_TNL_PROTECTION_PATH)
                        && (pRsvpTeWorkTnlInfo->pPsb != NULL)
                        && (pTeTnlInfo->pTeBackupPathInfo == NULL))
                    {
                        pRsvpTeWorkTnlInfo->pPsb->Association.u2AssocID =
                            (UINT2) u4TunnelInstance;
                    }

                    RSVPTE_DBG1 (RSVPTE_REOPT_DBG, "RpteTEProcessTeEvent : "
                                 "Map Tunnel Info is Populated(%x)\n",
                                 pRsvpTeTnlInfo->pMapTnlInfo);
                }
            }
            RSVPTE_DBG4 (RSVPTE_IF_PRCS,
                         "Tunnel %d %d %x %x created in RSVP-TE\n",
                         u4TunnelIndex, u4TunnelInstance, u4TunnelIngressLSRId,
                         u4TunnelEgressLSRId);

            RPTE_TE_TNL (pRsvpTeTnlInfo) = pTeTnlInfo;
            /*Indices are declared in teTnlInfo. Hence RB Tree addtion is done
               once teTnlInfo is assigned  */

            if (RBTreeAdd (gpRsvpTeGblInfo->RpteTnlTree,
                           (tRBElem *) pRsvpTeTnlInfo) != RB_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteTEProcessTeEvent: "
                            " Failed to Add :RsvpTeTnlInfo to RpteTnlTree\n");
                return RPTE_FAILURE;
            }

            RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) = RPTE_INGRESS;

            /* Stop the Retrigger Timer for the tunnel. */
            MplsStopRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                                     &(pTeTnlInfo->TnlRetrigTmr));

            pInstance0Tnl =
                TeGetTunnelInfo (u4TunnelIndex, RPTE_ZERO, u4TunnelIngressLSRId,
                                 u4TunnelEgressLSRId);

            if ((pInstance0Tnl != NULL) &&
                (pInstance0Tnl->GmplsTnlInfo.i4E2EProtectionType !=
                 MPLS_TE_DEDICATED_ONE2ONE))
            {
                MplsStopRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                                         &(pInstance0Tnl->TnlRetrigTmr));
            }

            u1RetVal = RpteTriggerInternalEvent (pRsvpTeTnlInfo,
                                                 RPTE_TNL_SETUP);
            break;

        case RPTE_PROTECTION_SWITCH_OVER:

        case RPTE_PROTECTION_SWITCH_BACK:

        case RSVPTE_TNL_DOWN:
            /* Intentional fall through */
        case RSVPTE_TNL_DESTROY:
            /* Intentional fall through */
        case RSVPTE_TNL_TX_PATH_MSG:
            /* Intentional fall through */
            if (u4TunnelInstance != RPTE_ZERO)
            {
                u4TunnelInstance = u4PrimaryTnlInstance;
            }

            RpteHandleTunnelEvents (u4TunnelIndex, u4TunnelInstance,
                                    u4TunnelIngressLSRId, u4TunnelEgressLSRId,
                                    pRpteEnqueueMsgInfo->u4RpteEvent,
                                    pTeTnlInfo);
            break;

        case RSVPTE_TNL_TE_GOING_DOWN:

            /* Call The TeGoingDown routine to remove all the tunnels and
             * release the resources associated */
            u1RetVal = RpteProcessTeGoingDownEvent ();
            break;
        case RSVPTE_TNL_MANUAL_REOPTIMIZATION:

            u1RetVal = RpteProcessTeTnlReoptManualEvent (pTeTnlInfo);

        default:
            return RPTE_FAILURE;
    }
    if (gu1TeAdminFlag == RPTE_FALSE)
    {
        gu1TeAdminFlag = RPTE_TRUE;
        rpteTeSigAdminStatus (RPTE_PROT, TE_ADMIN_UP);
    }
    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : RptePlrGenerateErHopsTowardEgress                         */
/* Description   : This routine is used to construct the ER-Hops             */
/*                 toward Egress for the backup path messages.               */
/* Input(s)      : ppRsvpTeOutTnlInfo - Pointer to Pointer to                */
/*                 backup tnl information                                    */
/*                 pCspfCompInfo - CSPF computed info.                       */
/*                 pCspfPath - CSPF computed path info.                      */
/* Output(s)     : None                                                      */
/* Return(s)     : RPTE_SUCCESS/RPTE_FAILURE                                 */
/*****************************************************************************/
UINT1
RptePlrGenerateErHopsTowardEgress (tRsvpTeTnlInfo ** ppRsvpTeOutTnlInfo,
                                   tCspfCompInfo * pCspfCompInfo,
                                   tOsTeAppPath * pCspfPath)
{
    tTMO_SLL           *pErHopList = NULL;
    tRsvpTePathListInfo *pOutPathListInfo = NULL;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeOutTnlInfo = *ppRsvpTeOutTnlInfo;
    UINT4               u4ErHopAddr = RPTE_ZERO;

    if (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeOutTnlInfo) != NULL)
    {
        /* Get Every ER-Hop in the ER-Hop List.
         * Increment count till we reach the Merge Point (MP).*/
        pErHopList = (&RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeOutTnlInfo));
        pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_First (pErHopList);
        while (pRsvpTeErHopInfo != NULL)
        {
            CONVERT_TO_INTEGER (pRsvpTeErHopInfo->IpAddr.au1Ipv4Addr,
                                u4ErHopAddr);

            /* Remove all the ER-Hops from the Current Hop till the
             * Merge Point in the Protected (primary path) tunnel. 
             * This is with reference to RFC 4090 Section 6.3 Point Number : 5. */
            if (pCspfCompInfo->u4DestIpAddr == OSIX_NTOHL (u4ErHopAddr))
            {
                /* If Protected tnl ER-Hop and CSPF computed ER-Hop are same, 
                 * then ignore that ER-HOP from constructing 
                 * for backup path tunnel */
                if (pCspfPath->aNextHops[(pCspfPath->u2HopCount) - 1].
                    u4NextHopIpAddr == OSIX_NTOHL (u4ErHopAddr))
                {
                    pCspfPath->u2HopCount--;
                }
                break;
            }
            rpteTeDeleteTnlHopInfo (RSVPTE_TNL_OUTERHOP_LIST_INFO
                                    (pRsvpTeOutTnlInfo),
                                    RSVPTE_TNL_PATH_OPTION
                                    (pRsvpTeOutTnlInfo), pRsvpTeErHopInfo);
            pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_First (pErHopList);
        }
        if (TMO_SLL_Count (pErHopList) == 0)
        {
            return RPTE_FAILURE;
        }
    }
    else
    {
        /* If Protected Tunnel does not have an ER-Hop List,
         * Construct one for the Backup Path. */
        if (rpteTeCreatePathListAndPathOption (&pOutPathListInfo) ==
            RPTE_TE_FAILURE)
        {
            return RPTE_FAILURE;
        }
        RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeOutTnlInfo) = pOutPathListInfo;
        RSVPTE_TNL_PATH_OPTION (pRsvpTeOutTnlInfo) =
            RSVPTE_TNL_FIRST_PATH_OPTION (pOutPathListInfo);
        RSVPTE_HOP_ROLE (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeOutTnlInfo)) =
            RSVPTE_TNL_NODE_RELATION (pRsvpTeOutTnlInfo);
    }

    /* Now, Insert all the ER-Hops computed through CSPF into 
     * Outer Hop List as Strict Hops. This is with reference to RFC 4090 
     * Section 6.3, Point Number : 5. */
    if (rpteTeCreateErHopsForCspfComputedHops ((&RSVPTE_TNL_OUTERHOP_LIST_INFO
                                                (pRsvpTeOutTnlInfo)),
                                               pCspfPath) == RPTE_TE_FAILURE)
    {
        return RPTE_FAILURE;
    }
    return RPTE_SUCCESS;
}

/************************************************************************
 *   Function Name   : RsvpProcessCspfMsgForFRRTnl
 *   Description     : Receive and Process the CSPF response packets
 *                     from OSPF-TE for FRR tunnel
 *   Input(s)        : pRpteCspfInfo - Pointer to CSPF info - Backup path
 *                     information.
 *   Output(s)       : None
 *   Returns         : None
 **************************************************************************/
VOID
RsvpProcessCspfMsgForFRRTnl (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                             tCspfCompInfo * pCspfCompInfo,
                             tOsTeAppPath * pCspfPath)
{
    tTeTnlInfo          TeTnlInfo;
    tRsvpTeTnlInfo     *pRsvpTeOutTnlInfo = NULL;
    tRsvpTePathListInfo *pOutPathListInfo = NULL;
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4OutGw = RPTE_ZERO;
    UINT4               u4TnlInst = RPTE_ZERO;
    UINT2               u2HopCnt = RPTE_ZERO;
    MEMSET (&TeTnlInfo, RSVPTE_ZERO, sizeof (tTeTnlInfo));
    if ((RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) & RPTE_FRR_CSPF_COMPUTED) ==
        RPTE_FRR_CSPF_COMPUTED)
    {
        RSVPTE_DBG (RSVPTE_PORT_DBG,
                    "Err : CSPF Computation is done already\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RsvpProcessCspfMsg : INTMD-EXIT \n");
        return;
    }

    if (RSVPTE_TNL_GBL_REVERT_FLAG (pRsvpTeTnlInfo) == RPTE_TRUE)
    {
        RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) |= RPTE_FRR_CSPF_COMPUTED;
        RptePhFrrSetupGblRevertOptTunnel (pRsvpTeTnlInfo, pCspfCompInfo,
                                          pCspfPath);
        return;
    }

    RSVPTE_DBG4 (RSVPTE_PH_DBG,
                 "CSPF Computation successful for the tunnel %d %d %x %x\n",
                 pCspfCompInfo->u4TnlIndex, pCspfCompInfo->u4TnlInst,
                 pCspfCompInfo->u4TnlSrcAddr, pCspfCompInfo->u4TnlDestAddr);

    /* Verifying whether hop limit constraint for the backup path has been
     * satisfied. */
    if ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS) &&
        (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo) != NULL))
    {
        if (TE_TNL_FRR_CONST_HOP_LIMIT
            (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)) < (pCspfPath->u2HopCount))
        {
            RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) = RPTE_FRR_CSPF_NO_START;
            RSVPTE_DBG (RSVPTE_PORT_DBG,
                        "Err : HOP Limit Constraint is not met to trigger a backup path\n");
            RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                        "RsvpProcessCspfMsg : INTMD-EXIT \n");
            return;
        }
    }
    else
    {
        pRsvpTeFastReroute =
            &(PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
        if ((pRsvpTeFastReroute->u1Flags != RPTE_ZERO) &&
            (pRsvpTeFastReroute->u1HopLmt < pCspfPath->u2HopCount))
        {
            RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) = RPTE_FRR_CSPF_NO_START;
            RSVPTE_DBG (RSVPTE_PORT_DBG,
                        "Err : HOP Limit Constraint is met to trigger a backup path\n");
            RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                        "RsvpProcessCspfMsg : INTMD-EXIT \n");
            return;
        }
    }

    RSVPTE_DBG (RSVPTE_PORT_DBG, "List of hops given by CSPF are\n");
    for (u2HopCnt = 0; u2HopCnt < pCspfPath->u2HopCount; u2HopCnt++)
    {
        RSVPTE_DBG2 (RSVPTE_PORT_DBG,
                     "Next Hop Addr: %x Router-Id: %x\n",
                     pCspfPath->aNextHops[u2HopCnt].u4NextHopIpAddr,
                     pCspfPath->aNextHops[u2HopCnt].u4RouterId);
    }
    if (RpteIpUcastRouteQuery (pCspfPath->aNextHops[0].u4NextHopIpAddr,
                               &pIfEntry, &u4OutGw) != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_PORT_DBG, "CSPF processing failed as neighbor "
                    "is not reachable\n");
        RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) = RPTE_FRR_CSPF_NO_START;
        return;
    }
    RSVPTE_DBG1 (RSVPTE_PORT_DBG,
                 "Required Strict Addr: %x\n", pCspfCompInfo->u4DestIpAddr);

    RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) |= RPTE_FRR_CSPF_COMPUTED;
    RPTE_FRR_CSPF_INTVL_COUNT (pRsvpTeTnlInfo) = RPTE_ZERO;
    u4TnlInst = RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo);
    RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo)++;
    /* Creating a new RSVP Tunnel for available backup path. */
    if ((RpteCreateNewRsvpteTunnel (pCspfCompInfo->u4TnlIndex,
                                    u4TnlInst,
                                    pCspfCompInfo->u4TnlSrcAddr,
                                    pCspfCompInfo->u4TnlDestAddr,
                                    &TeTnlInfo, &pRsvpTeOutTnlInfo,
                                    RPTE_FALSE)) != RPTE_SUCCESS)
    {
        RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) = RPTE_FRR_CSPF_NO_START;
        RSVPTE_DBG (RSVPTE_PORT_DBG,
                    "Err : Creating New Tunnel Entry in Hash Table Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RsvpProcessCspfMsg : INTMD-EXIT \n");
        return;
    }
    /* TE Row Status & Admin Status are made ACTIVE */
    TeTnlInfo.u1TnlRowStatus = RPTE_ACTIVE;
    TeTnlInfo.u1TnlAdminStatus = RPTE_ADMIN_UP;
    TeTnlInfo.u4TnlMode = GMPLS_FORWARD;
    /* Tunnel Entry Creation is indicated to TE Module */
    if (rpteTeCreateNewTnl (&pTeTnlInfo, &TeTnlInfo) == RPTE_TE_FAILURE)
    {
        RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) = RPTE_FRR_CSPF_NO_START;
        /* Tunnel info returned to the Tunnel memory Pool */
        MEMSET (pRsvpTeOutTnlInfo, 0, sizeof (tRsvpTeTnlInfo));
        MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeOutTnlInfo);
        RSVPTE_DBG (RSVPTE_PORT_DBG,
                    "Err : Tunnel Entry Addition in TE Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RsvpProcessCspfMsg : INTMD-EXIT \n");
        return;
    }
    RPTE_TE_TNL (pRsvpTeOutTnlInfo) = pTeTnlInfo;
    pRsvpTeOutTnlInfo->pTeTnlInfo->b1IsResourceReserved = RPTE_FALSE;
    RPTE_FRR_BASE_TNL (pRsvpTeOutTnlInfo) = pRsvpTeTnlInfo;
    RSVPTE_TNL_ROW_STATUS (&TeTnlInfo) = RPTE_ACTIVE;
    RSVPTE_TNL_ADMIN_STATUS (&TeTnlInfo) = RPTE_ADMIN_UP;
    RPTE_FRR_TNL_INFO_TYPE (pRsvpTeOutTnlInfo) = RPTE_FRR_BACKUP_TNL;
    RSVPTE_TNL_NODE_RELATION (pRsvpTeOutTnlInfo) = RPTE_INGRESS;
    RPTE_IS_FRR_CAPABLE (pRsvpTeOutTnlInfo) = RPTE_TRUE;

    RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) = pRsvpTeOutTnlInfo;

    /* RFC 4090 sec. 6.1 Signaling a Backup path */
    /*--------------------------------------------*/
    /* RFC 4090 sec. 6.3 Signaling Backups for one-to-one protection */
    /* Copy all the protected tnls's ER-Hop information to backup tunnel */
    if ((RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL) &&
        (TMO_SLL_Count (&RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo)) !=
         RSVPTE_ZERO))
    {
        if (rpteTeCreateHopListInfo (&pOutPathListInfo,
                                     &RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo))
            != RPTE_TE_SUCCESS)
        {
            RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) = RPTE_FRR_CSPF_NO_START;
            rpteTeDeleteTnlInfo (RPTE_TE_TNL (pRsvpTeOutTnlInfo),
                                 TE_TNL_CALL_FROM_SIG);
            RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) = NULL;
            MEMSET ((UINT1 *) pRsvpTeOutTnlInfo, RPTE_ZERO,
                    sizeof (tRsvpTeTnlInfo));
            MemReleaseMemBlock (RSVPTE_TNL_POOL_ID,
                                (UINT1 *) pRsvpTeOutTnlInfo);
            RSVPTE_DBG (RSVPTE_PORT_DBG,
                        "Err : Tunnel Hop Entry addition in TE Failed\n");
            RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                        "RsvpProcessCspfMsg : INTMD-EXIT \n");
            return;
        }
        RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeOutTnlInfo) = pOutPathListInfo;
        RSVPTE_TNL_PATH_OPTION (pRsvpTeOutTnlInfo) =
            RSVPTE_TNL_FIRST_PATH_OPTION (pOutPathListInfo);
        RSVPTE_HOP_ROLE (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeOutTnlInfo)) =
            RSVPTE_TNL_NODE_RELATION (pRsvpTeOutTnlInfo);
    }
    /* Copy CSPF computed HOPs information to ER-Hops of backup tunnel */
    if (RptePlrGenerateErHopsTowardEgress (&pRsvpTeOutTnlInfo,
                                           pCspfCompInfo,
                                           pCspfPath) == RPTE_FAILURE)
    {
        RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) = RPTE_FRR_CSPF_NO_START;
        rpteTeDeleteTnlInfo (RPTE_TE_TNL (pRsvpTeOutTnlInfo),
                             TE_TNL_CALL_FROM_SIG);
        RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) = NULL;
        MEMSET ((UINT1 *) pRsvpTeOutTnlInfo, RPTE_ZERO,
                sizeof (tRsvpTeTnlInfo));
        MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeOutTnlInfo);
        RSVPTE_DBG (RSVPTE_PORT_DBG,
                    "Err : ER-Hops Generation towards Egress Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RsvpProcessCspfMsg : INTMD-EXIT \n");
        return;
    }
    RPTE_TE_TNL (pRsvpTeOutTnlInfo)->u4TnlHopTableIndex =
        RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeOutTnlInfo)->u4HopListIndex;
    RPTE_TE_TNL (pRsvpTeOutTnlInfo)->u4TnlPathInUse =
        RSVPTE_TNL_PATH_OPTION (pRsvpTeOutTnlInfo)->u4PathOptionIndex;

    if (rpteTeUpdateTeTnl (RPTE_TE_TNL (pRsvpTeOutTnlInfo)) == RPTE_TE_FAILURE)
    {
        RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) = RPTE_FRR_CSPF_NO_START;
        rpteTeDeleteTnlInfo (RPTE_TE_TNL (pRsvpTeOutTnlInfo),
                             TE_TNL_CALL_FROM_SIG);
        RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) = NULL;
        MEMSET ((UINT1 *) pRsvpTeOutTnlInfo, RPTE_ZERO,
                sizeof (tRsvpTeTnlInfo));
        MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeOutTnlInfo);
        RSVPTE_DBG (RSVPTE_PORT_DBG, "Err : rpteTeUpdateTeTnl Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RsvpProcessCspfMsg : INTMD-EXIT \n");
        return;
    }
    RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) |= RPTE_FRR_NODE_STATE_PLRAWAIT;
    /* TODO Copying the Resource affinities flags incase of ERO 
     * is not completely strict is pending.*/
    if (RptePhSetupTunnel (pRsvpTeOutTnlInfo) != RPTE_SUCCESS)
    {
        RpteResetPlrStatus (pRsvpTeTnlInfo);
        rpteTeDeleteHopListInfo (pOutPathListInfo);
        rpteTeDeleteTnlInfo (RPTE_TE_TNL (pRsvpTeOutTnlInfo),
                             TE_TNL_CALL_FROM_SIG);
        MEMSET ((UINT1 *) pRsvpTeOutTnlInfo, RPTE_ZERO,
                sizeof (tRsvpTeTnlInfo));
        MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeOutTnlInfo);
        RSVPTE_DBG (RSVPTE_PORT_DBG,
                    "Err : Setup Tunnel and Sending PATH Message Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RsvpProcessCspfMsg : INTMD-EXIT \n");
        return;
    }

    RSVPTE_DBG (RSVPTE_PORT_DBG, "Backup Tunnel to be established\n");

    /* Copying the Backup Tunnel Indices to Protected Tunnel Table to 
     * display.  */
    RSVPTE_TE_TNL_FRR_BKP_TNL_INDEX (RPTE_TE_TNL (pRsvpTeTnlInfo))
        = RSVPTE_TNL_INDEX (RPTE_TE_TNL (pRsvpTeOutTnlInfo));
    RSVPTE_TE_TNL_FRR_BKP_TNL_INST (RPTE_TE_TNL (pRsvpTeTnlInfo))
        = RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (pRsvpTeOutTnlInfo));

    MEMCPY ((UINT1 *) &(RSVPTE_TE_TNL_FRR_BKP_TNL_INGRESS_ID
                        (RPTE_TE_TNL (pRsvpTeTnlInfo))),
            (UINT1 *) &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL
                                                   (pRsvpTeOutTnlInfo))),
            ROUTER_ID_LENGTH);
    MEMCPY ((UINT1 *) &(RSVPTE_TE_TNL_FRR_BKP_TNL_EGRESS_ID (RPTE_TE_TNL
                                                             (pRsvpTeTnlInfo))),
            (UINT1 *) &(RSVPTE_TNL_EGRESS_RTR_ID (RPTE_TE_TNL
                                                  (pRsvpTeOutTnlInfo))),
            ROUTER_ID_LENGTH);

    /* Copying Protected Interfaces */
    RSVPTE_TNL_PROT_IF_INDEX (RPTE_TE_TNL (pRsvpTeTnlInfo))
        = IF_ENTRY_IF_INDEX (PSB_OUT_IF_ENTRY
                             (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));

    /* Setting Protection Type Values... */
    if ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & FRR_NODE_PROT)
        == FRR_NODE_PROT)
    {
        RSVPTE_TE_TNL_FRR_PROT_TYPE (RPTE_TE_TNL (pRsvpTeTnlInfo))
            = TE_TNL_FRR_PROT_NODE;
    }
    else if ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_AVAIL)
             == LOCAL_PROT_AVAIL)
    {
        RSVPTE_TE_TNL_FRR_PROT_TYPE (RPTE_TE_TNL (pRsvpTeTnlInfo))
            = TE_TNL_FRR_PROT_LINK;
    }

    /* Setting One2One Plr ID, sender node address, type and
     * Avoid Node address, type. */
    RSVPTE_TE_TNL_FRR_ONE2ONE_PLR_ID (RPTE_TE_TNL (pRsvpTeOutTnlInfo))
        = PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB
                               (pRsvpTeOutTnlInfo)).au4PlrId[RPTE_ZERO];

    RSVPTE_TE_TNL_FRR_ONE2ONE_SENDER_ADDR_TYPE (RPTE_TE_TNL
                                                (pRsvpTeOutTnlInfo)) = IPV4;

    RSVPTE_TE_TNL_FRR_ONE2ONE_SENDER_ADDR (RPTE_TE_TNL (pRsvpTeOutTnlInfo))
        = PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB
                               (pRsvpTeOutTnlInfo)).au4PlrId[RPTE_ZERO];

    RSVPTE_TE_TNL_FRR_ONE2ONE_AVOID_ADDR_TYPE (RPTE_TE_TNL
                                               (pRsvpTeOutTnlInfo)) = IPV4;

    RSVPTE_TE_TNL_FRR_ONE2ONE_AVOID_ADDR (RPTE_TE_TNL (pRsvpTeOutTnlInfo))
        = PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB
                               (pRsvpTeOutTnlInfo)).au4AvoidNodeId[RPTE_ZERO];

    /* Setting the Detour Merging Values and its status */
    RSVPTE_TE_TNL_FRR_DETOUR_MERGING (RPTE_TE_TNL (pRsvpTeOutTnlInfo))
        = TE_TNL_FRR_DETOUR_MERGE_NONE;

    RpteUtlSetDetourStatus (pRsvpTeOutTnlInfo, RPTE_FALSE);

    RSVPTE_FRR_DETOUR_ORIG_NUM (gpRsvpTeGblInfo)++;

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RsvpProcessCspfMsg : EXIT \n");
    return;
}

/************************************************************************
 *   Function Name   : RsvpProcessCspfMsgForNormalTnl
 *   Description     : Receive and Process the CSPF response packets
 *                     from OSPF-TE for FRR tunnel
 *   Input(s)        : pRpteCspfInfo - Pointer to CSPF info - Backup path
 *                     information.
 *   Output(s)       : None
 *   Returns         : None
 **************************************************************************/
VOID
RsvpProcessCspfMsgForNormalTnl (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                tOsTeAppPath * pCspfPath)
{
    tTeCHopListInfo    *pCHopListInfo = NULL;
    tTeCHopListInfo    *pEroCacheListInfo = NULL;
    UINT4               u4IngressId = RPTE_ZERO;
    UINT4               u4EgressId = RPTE_ZERO;
    UINT2               u2HopCnt = RPTE_ZERO;
    UINT1               u1ExcludeHopCount = RPTE_ZERO;
    UINT2               u2HopCount = RPTE_ZERO;

    /* CSPF Timer is stopped */
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_CSPF_TIMER (pRsvpTeTnlInfo));

    /* This for holding the CSPF computation for some time
     *      * Test case mapping -> GMPLS_ST_PROTECT_FULL_REROUTE_FUNC_21*/

    CONVERT_TO_INTEGER ((pRsvpTeTnlInfo->pTeTnlInfo->TnlIngressLsrId),
                        u4IngressId);
    CONVERT_TO_INTEGER ((pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId),
                        u4EgressId);

    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    if (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_BW_RESV)
    {
        RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, RPTE_ROUTE_PROB,
                                     RPTE_NO_ROUTE_AVAIL);
        return;
    }

    if ((pCspfPath->u2HopCount == RPTE_ZERO) ||
        (pCspfPath->u2HopCount > OSPF_TE_MAX_NEXT_HOPS) ||
        (pCspfPath->u1PathReturnCode != RPTE_ZERO))
    {
        /* CSPF Computation has failed */
        RSVPTE_DBG5 (RSVPTE_MAIN_MISC,
                     "CSPF Computation failed for the tunnel %d %d %x %x - "
                     "Current Instance - %d\n",
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance,
                     u4IngressId, u4EgressId,
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlPrimaryInstance);

        if ((pRsvpTeTnlInfo->pMapTnlInfo != NULL) &&
            (RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER (pRsvpTeTnlInfo->pTeTnlInfo) ==
             TRUE))
        {
            rpteTeUpdatePrimaryInstance (pRsvpTeTnlInfo->pMapTnlInfo->
                                         pTeTnlInfo->u4TnlIndex,
                                         pRsvpTeTnlInfo->pMapTnlInfo->
                                         pTeTnlInfo->u4TnlInstance, u4IngressId,
                                         u4EgressId);
        }

        if ((RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) !=
             RPTE_REOPT_MANUAL_TRIGGER)
            && (RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) !=
                RPTE_REOPT_TIMER_EXPIRY)
            && (RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) != RPTE_REOPT_LINK_UP)
            && (pRsvpTeTnlInfo->u1ReoptMaintenanceType !=
                RPTE_REOPT_LINK_MAINTENANCE)
            && (pRsvpTeTnlInfo->u1ReoptMaintenanceType !=
                RPTE_REOPT_NODE_MAINTENANCE))
        {
            /*This function shall delete tunnel only at Ingress Node */
            RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, RPTE_ROUTE_PROB,
                                         RPTE_NO_ROUTE_AVAIL);
        }
        else
        {
            if (pRsvpTeTnlInfo->pMapTnlInfo != NULL)
            {
                RpteRhStartReoptimizeTmr (pRsvpTeTnlInfo->pMapTnlInfo);
            }
            RSVPTE_DBG (RSVPTE_REOPT_DBG, "RsvpProcessCspfMsgForNormalTnl : "
                        "Alternate Path is not available, Reoptimization is "
                        "not triggered\n");
        }

        return;
    }

    if (((RSVPTE_TE_TNL_REOPTIMIZE_STATUS (RPTE_TE_TNL (pRsvpTeTnlInfo)))
         == TE_REOPTIMIZE_ENABLE) &&
        ((RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) == RPTE_TRUE) ||
         (RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) & RPTE_REOPT_MANUAL_TRIGGER)
         || (RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) & RPTE_REOPT_TIMER_EXPIRY)
         || (RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) & RPTE_REOPT_LINK_UP)
         || (pRsvpTeTnlInfo->u1ReoptMaintenanceType ==
             RPTE_REOPT_LINK_MAINTENANCE)
         || (pRsvpTeTnlInfo->u1ReoptMaintenanceType ==
             RPTE_REOPT_NODE_MAINTENANCE)))
    {
        RSVPTE_DBG (RSVPTE_REOPT_DBG, "RsvpProcessCspfMsgForNormalTnl : "
                    "Re-evaluation request(%d) - Cspf Reply with a Path\n");

        RSVPTE_DBG5 (RSVPTE_REOPT_DBG,
                     "List of hops given by CSPF for the tunnel %d %d %x %x - "
                     "Current Instance - %d are\n",
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance,
                     u4IngressId, u4EgressId,
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlPrimaryInstance);

        for (u2HopCnt = RPTE_ZERO; u2HopCnt < pCspfPath->u2HopCount; u2HopCnt++)
        {
            RSVPTE_DBG2 (RSVPTE_REOPT_DBG,
                         "Next Hop Addr: %x Router-Id: %x\n",
                         pCspfPath->aNextHops[u2HopCnt].u4NextHopIpAddr,
                         pCspfPath->aNextHops[u2HopCnt].u4RouterId);
        }

        if (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) == RPTE_MID_POINT_NODE)
        {

            if (rpteTeCreateCHopListInfo (&pEroCacheListInfo) ==
                RPTE_TE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_REOPT_DBG,
                            "Ero Cache List Info Creation Failed\n");
                return;
            }

            if (rpteTeCreateCHopsForCspfPaths
                (pRsvpTeTnlInfo->pTeTnlInfo, pCspfPath,
                 &pEroCacheListInfo) == RPTE_TE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_REOPT_DBG, "Adding Ero Cache Failed\n");
                return;
            }

            pRsvpTeTnlInfo->pTeTnlInfo->pTeEroCacheListInfo = pEroCacheListInfo;
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlEroCacheTableIndex
                = pEroCacheListInfo->u4CHopListIndex;

            if (rpteTeCompareEroCacheAndCHopInfo
                (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlEroCacheTableIndex,
                 pRsvpTeTnlInfo->pTeTnlInfo->u4TnlCHopTableIndex) ==
                RPTE_TE_EQUAL)
            {
                RSVPTE_DBG (RSVPTE_REOPT_DBG,
                            "RsvpProcessCspfMsgForNormalTnl : "
                            "ERO Cache Info and CSPF are equal\n");

                rpteTeDeleteCHopListInfo (pRsvpTeTnlInfo->pTeTnlInfo->
                                          pTeEroCacheListInfo);
                pRsvpTeTnlInfo->pTeTnlInfo->pTeEroCacheListInfo = NULL;
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlEroCacheTableIndex = RPTE_ZERO;

                if (RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) ==
                    RPTE_TRUE)
                {
                    RptePhPathRefresh (pRsvpTeTnlInfo);
                    RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) =
                        RPTE_FALSE;
                }
                if (RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) ==
                    RPTE_REOPT_TIMER_EXPIRY)
                {
                    RpteRhStartReoptimizeTmr (pRsvpTeTnlInfo);
                }
                pRsvpTeTnlInfo->u1ReoptMaintenanceType = RPTE_ZERO;
            }
            else
            {
                RSVPTE_DBG (RSVPTE_REOPT_DBG,
                            "RsvpProcessCspfMsgForNormalTnl : "
                            "ERO Cache Info and CSPF are different\n");

                RptePhStartEroCacheTmr (pRsvpTeTnlInfo);
                RpteConstructPktMapAndSendPathErr (pRsvpTeTnlInfo,
                                                   RPTE_NOTIFY,
                                                   RPTE_PREFERABLE_PATH_EXIST);
                if (RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) ==
                    RPTE_TRUE)
                {
                    RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) =
                        RPTE_FALSE;
                }
            }
            RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) = RPTE_ZERO;
            return;

        }
        else if (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) ==
                 RPTE_HEAD_MID_POINT_NODE)
        {
            if (rpteTeCreateCHopListInfo (&pEroCacheListInfo) ==
                RPTE_TE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_REOPT_DBG,
                            "Ero Cache List Info Creation Failed\n");
                return;
            }

            if (rpteTeCreateCHopsForCspfPaths
                (pRsvpTeTnlInfo->pTeTnlInfo, pCspfPath,
                 &pEroCacheListInfo) == RPTE_TE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_REOPT_DBG, "Adding Ero Cache Failed\n");
                return;
            }

            pRsvpTeTnlInfo->pTeTnlInfo->pTeEroCacheListInfo = pEroCacheListInfo;
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlEroCacheTableIndex
                = pEroCacheListInfo->u4CHopListIndex;

            if (rpteTeCompareEroCacheAndCHopInfo
                (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlEroCacheTableIndex,
                 pRsvpTeTnlInfo->pTeTnlInfo->u4TnlCHopTableIndex) ==
                RPTE_TE_EQUAL)
            {
                RSVPTE_DBG (RSVPTE_REOPT_DBG,
                            "RsvpProcessCspfMsgForNormalTnl : "
                            "ERO Cache Info and CSPF are equal\n");

                rpteTeDeleteCHopListInfo (pRsvpTeTnlInfo->pTeTnlInfo->
                                          pTeEroCacheListInfo);
                pRsvpTeTnlInfo->pTeTnlInfo->pTeEroCacheListInfo = NULL;
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlEroCacheTableIndex = RPTE_ZERO;

                RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) = RPTE_TRUE;
                RptePhPathRefresh (pRsvpTeTnlInfo);
                RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) = RPTE_FALSE;

                if (RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) ==
                    RPTE_REOPT_TIMER_EXPIRY)
                {
                    RpteRhStartReoptimizeTmr (pRsvpTeTnlInfo);
                }
            }
            else
            {
                RSVPTE_DBG (RSVPTE_REOPT_DBG,
                            "RsvpProcessCspfMsgForNormalTnl : "
                            "ERO Cache Info and CSPF are different\n");

                pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo =
                    pRsvpTeTnlInfo->pTeTnlInfo->pTeEroCacheListInfo;
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlCHopTableIndex =
                    pRsvpTeTnlInfo->pTeTnlInfo->u4TnlEroCacheTableIndex;

                /* Call RptePhSetupTunnel to construct PSB and send Path Msg from Ingress */
                if (RptePhSetupTunnel (pRsvpTeTnlInfo) != RPTE_SUCCESS)
                {
                    /* No clean up required here since it is already handled in
                     * SetupTunnel itself */

                    RSVPTE_DBG (RSVPTE_MAIN_MEM, "Setup Tunnel Failed\n");
                    return;
                }
                pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->pTeEroCacheListInfo =
                    NULL;
                pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
                    u4TnlEroCacheTableIndex = RPTE_ZERO;
            }
            RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) = RPTE_ZERO;
            return;
        }
    }
    else
    {
        RSVPTE_DBG5 (RSVPTE_MAIN_MISC,
                     "List of hops given by CSPF for the tunnel %d %d %x %x - "
                     "Current Instance - %d are\n",
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance,
                     u4IngressId, u4EgressId,
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlPrimaryInstance);

        for (u2HopCnt = RPTE_ZERO; u2HopCnt < pCspfPath->u2HopCount; u2HopCnt++)
        {
            RSVPTE_DBG2 (RSVPTE_MAIN_MISC,
                         "Next Hop Addr: %x Router-Id: %x\n",
                         pCspfPath->aNextHops[u2HopCnt].u4NextHopIpAddr,
                         pCspfPath->aNextHops[u2HopCnt].u4RouterId);
        }

        /* Create Computed hop list */
        if (rpteTeCreateCHopListInfo (&pCHopListInfo) == RPTE_TE_FAILURE)
        {
            RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, RPTE_ROUTE_PROB,
                                         RPTE_NO_ROUTE_AVAIL);

            RSVPTE_DBG (RSVPTE_MAIN_MEM, "CHop List Info Creation Failed\n");
            return;
        }

        if (rpteTeCreateCHopsForCspfPaths
            (pRsvpTeTnlInfo->pTeTnlInfo, pCspfPath,
             &pCHopListInfo) == RPTE_TE_FAILURE)
        {
            RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, RPTE_ROUTE_PROB,
                                         RPTE_NO_ROUTE_AVAIL);

            RSVPTE_DBG (RSVPTE_MAIN_MEM, "Adding CHops Failed\n");
            return;
        }

        pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo = pCHopListInfo;
        pRsvpTeTnlInfo->pTeTnlInfo->u4TnlCHopTableIndex
            = pCHopListInfo->u4CHopListIndex;
        pRsvpTeTnlInfo->pTeTnlInfo->u4TnlPathMetric = pCspfPath->u4TeMetric;
    }

    RSVPTE_DBG1 (RSVPTE_REOPT_DBG, "RsvpProcessCspfMsgForNormalTnl : "
                 "Reoptimization Trigger State(%d)\n",
                 RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER (pRsvpTeTnlInfo->pTeTnlInfo));
    if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
        (RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER (pRsvpTeTnlInfo->pTeTnlInfo) == TRUE))
    {
        RSVPTE_DBG1 (RSVPTE_REOPT_DBG, "RsvpProcessCspfMsgForNormalTnl : "
                     "Map Tunnel Info(%x)\n", pRsvpTeTnlInfo->pMapTnlInfo);
        if (pRsvpTeTnlInfo->pMapTnlInfo != NULL)
        {
            if ((rpteTeCompareCHopInfo (pRsvpTeTnlInfo->pTeTnlInfo,
                                        pRsvpTeTnlInfo->pMapTnlInfo->
                                        pTeTnlInfo) == RPTE_TE_EQUAL)
                && (pRsvpTeTnlInfo->pMapTnlInfo->b1IsPreferablePathExist ==
                    FALSE))
            {
                RSVPTE_DBG (RSVPTE_REOPT_DBG,
                            "RsvpProcessCspfMsgForNormalTnl : "
                            "Old and New Path are equal\n");

                RpteRhStartReoptimizeTmr (pRsvpTeTnlInfo->pMapTnlInfo);

                u1ExcludeHopCount =
                    pRsvpTeTnlInfo->pMapTnlInfo->u1NoOfExcludeHop;

                for (u2HopCount = RPTE_ZERO; u2HopCount < u1ExcludeHopCount;
                     u2HopCount++)
                {
                    pRsvpTeTnlInfo->pMapTnlInfo->au4ExcludeHop[u2HopCount] =
                        RPTE_ZERO;
                }

                rpteTeUpdatePrimaryInstance (pRsvpTeTnlInfo->pMapTnlInfo->
                                             pTeTnlInfo->u4TnlIndex,
                                             pRsvpTeTnlInfo->pMapTnlInfo->
                                             pTeTnlInfo->u4TnlInstance,
                                             u4IngressId, u4EgressId);

                rpteTeUpdateOriginalInstance (pRsvpTeTnlInfo->pMapTnlInfo->
                                              pTeTnlInfo->u4TnlIndex,
                                              pRsvpTeTnlInfo->pMapTnlInfo->
                                              pTeTnlInfo->u4TnlInstance,
                                              u4IngressId, u4EgressId);

                rpteTeUpdateReoptTriggerState (pRsvpTeTnlInfo->pTeTnlInfo->
                                               u4TnlIndex, RPTE_ZERO,
                                               u4IngressId, u4EgressId);

                RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
                return;
            }
            RSVPTE_DBG (RSVPTE_REOPT_DBG, "RsvpProcessCspfMsgForNormalTnl : "
                        "Old and New Path are different\n");
            pRsvpTeTnlInfo->pMapTnlInfo->u1ReoptMaintenanceType = RPTE_ZERO;
            pRsvpTeTnlInfo->pMapTnlInfo->u4ErrReoptLinkAddress = RPTE_ZERO;
            pRsvpTeTnlInfo->pMapTnlInfo->b1IsPreferablePathExist = FALSE;
        }
    }

    /* Call RptePhSetupTunnel to construct PSB and send Path Msg from Ingress */
    if (RptePhSetupTunnel (pRsvpTeTnlInfo) != RPTE_SUCCESS)
    {
        /* No clean up required here since it is already handled in
         * SetupTunnel itself */

        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Setup Tunnel Failed\n");
        return;
    }

    return;
}

/****************************************************************************/
/* Function Name   : RptePortTlmGetCompIfIndex                              */
/* Description     : This function is used to component If index from TE    */
/*                   Link Index                                             */
/* Input (s)       : u4TeLinkIf - Te link index                             */
/* Output (s)      : pu4CompIfIndex - component if index                    */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePortTlmGetCompIfIndex (UINT4 u4TeLinkIf, UINT4 *pu4CompIfIndex)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;
    UINT4               u4Flags = RSVPTE_ZERO;

    u4Flags = TLM_TE_LINK_IF_INDEX;

    MEMSET (&InTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tOutTlmTeLink));

    InTlmTeLinkInfo.u4IfIndex = u4TeLinkIf;
    u4Flags = TLM_TE_LINK_IF_INDEX;

    TlmApiGetTeLinkParams (u4Flags, &InTlmTeLinkInfo, &OutTlmTeLinkInfo);

    *pu4CompIfIndex = OutTlmTeLinkInfo.u4CompIfIndex;
#else
    UNUSED_PARAM (u4TeLinkIf);
    UNUSED_PARAM (pu4CompIfIndex);
#endif

    return;
}

/****************************************************************************/
/* Function Name   : RptePortTlmUpdateTeLinkUnResvBw                        */
/* Description     : This function used to update the TE Link Unreserved BW */
/* Input (s)       : u1TnlHoldPrio - Holding priority                       */
/*                   reqResBw      - Requested bw                           */
/*                   u1ResvRsrc    - Resource reservation / deletion        */
/* Output (s)        *pu4CompIfIndex - Component If Index                   */
/* Returns         : RPTE_SUCCESS/RPTE_FAILURE                              */
/****************************************************************************/
INT4
RptePortTlmUpdateTeLinkUnResvBw (UINT4 u4TeIfIndex,
                                 UINT1 u1TnlHoldPrio, FLOAT reqResBw,
                                 UINT1 u1ResvRsrc, UINT4 *pu4CompIfIndex)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;

    MEMSET (&InTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tOutTlmTeLink));

    InTlmTeLinkInfo.u4TeClass = u1TnlHoldPrio;
    InTlmTeLinkInfo.reqResBw = reqResBw;
    InTlmTeLinkInfo.u4IfIndex = u4TeIfIndex;
    InTlmTeLinkInfo.u4CompIfIndex = *pu4CompIfIndex;

    if (TlmApiUpdateTeLinkUnResvBw (u1ResvRsrc, &InTlmTeLinkInfo,
                                    &OutTlmTeLinkInfo) == TLM_FAILURE)
    {
        return RPTE_FAILURE;
    }
    *pu4CompIfIndex = OutTlmTeLinkInfo.u4CompIfIndex;
#else
    UNUSED_PARAM (u1TnlHoldPrio);
    UNUSED_PARAM (reqResBw);
    UNUSED_PARAM (u1ResvRsrc);
    UNUSED_PARAM (u4TeIfIndex);
    UNUSED_PARAM (pu4CompIfIndex);
#endif

    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePortTlmGetTeLinkUnResvBw                           */
/* Description     : This function used to get the unreserved bandwidth     */
/*                   of a TE Link corresponding to setup priority           */
/* Input (s)       : u4TeIfIndex     - TE Link If Index                     */
/*                   u1TnlSetPrio    - Tunnel Setup priority                */
/* Output (s)      : *pAvailBw       - Available Bandwidth                  */
/*                   *pUnResvBw      - Unreserved Bandwidth                 */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePortTlmGetTeLinkUnResvBw (UINT4 u4TeIfIndex, UINT1 u1TnlSetPrio,
                              FLOAT * pUnResvBw, FLOAT * pAvailBw)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;

    MEMSET (&InTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tOutTlmTeLink));

    InTlmTeLinkInfo.u4TeClass = u1TnlSetPrio;
    InTlmTeLinkInfo.u4IfIndex = u4TeIfIndex;

    TlmApiGetTeLinkUnResvBw (&InTlmTeLinkInfo, &OutTlmTeLinkInfo);

    *pAvailBw = OutTlmTeLinkInfo.availBw;
    *pUnResvBw = OutTlmTeLinkInfo.unResBw;
#else
    UNUSED_PARAM (u4TeIfIndex);
    UNUSED_PARAM (u1TnlSetPrio);
    UNUSED_PARAM (pUnResvBw);
    UNUSED_PARAM (pAvailBw);
#endif

    return;
}

/****************************************************************************/
/* Function Name   : RptePortTlmGetRemoteRtrIp                              */
/* Description     : This function used to get Remote Router Id from        */
/*                   Remote TE Link IP address                              */
/* Input (s)       : u4RemoteLinkAddr - Remote TE Link IP address           */
/* Output (s)      : *pu4RemoteRouterIp - Remote rtr id                     */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePortTlmGetRemoteRtrIp (UINT4 u4RemoteLinkAddr, UINT4 *pu4RemoteRouterIp)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;
    UINT4               u4Flags = RSVPTE_ZERO;

    MEMSET (&InTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tOutTlmTeLink));

    InTlmTeLinkInfo.RemoteIpAddr.ip6_addr_u.u4WordAddr[0] = u4RemoteLinkAddr;
    u4Flags = TLM_TE_LINK_REMOTE_IP_ADD;

    TlmApiGetTeLinkParams (u4Flags, &InTlmTeLinkInfo, &OutTlmTeLinkInfo);

    *pu4RemoteRouterIp = OutTlmTeLinkInfo.u4RemoteRtrId;
#else
    UNUSED_PARAM (u4RemoteLinkAddr);
    UNUSED_PARAM (pu4RemoteRouterIp);
#endif

    return;
}

/****************************************************************************/
/* Function Name   : RptePortTlmGetLocalId                                  */
/* Description     : This function used to get TE link local Identifier     */
/*                   from TE Link Remote Identifier & Remote Router ID      */
/* Input (s)       : u4RemoteId  - Remote TE link Identifier                */
/*                 : u4RemoteRtrId  - Remote Router Identifier              */
/* Output (s)      : pu4LocalId  - Local TE link Identifier                 */
/*                   pu4IfIndex  - TE Link If Index                         */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePortTlmGetLocalId (UINT4 u4RemoteId, UINT4 u4RemoteRtrId,
                       UINT4 *pu4LocalId, UINT4 *pu4IfIndex)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;
    UINT4               u4Flags = RSVPTE_ZERO;

    MEMSET (&InTlmTeLinkInfo, RSVPTE_ZERO, sizeof (InTlmTeLinkInfo));
    MEMSET (&OutTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tOutTlmTeLink));

    u4Flags = TLM_TE_LINK_REMOTE_IDENTIFIER;
    InTlmTeLinkInfo.u4RemoteIdentifier = u4RemoteId;
    InTlmTeLinkInfo.u4RemoteRtrId = u4RemoteRtrId;

    TlmApiGetTeLinkParams (u4Flags, &InTlmTeLinkInfo, &OutTlmTeLinkInfo);

    *pu4LocalId = OutTlmTeLinkInfo.u4LocalIdentifier;
    *pu4IfIndex = OutTlmTeLinkInfo.u4IfIndex;
#else
    UNUSED_PARAM (u4RemoteId);
    UNUSED_PARAM (u4RemoteRtrId);
    UNUSED_PARAM (pu4LocalId);
    UNUSED_PARAM (pu4IfIndex);
#endif

    return;
}

/****************************************************************************/
/* Function Name   : RptePortTlmGetLocalIP                                  */
/* Description     : This function is used to get TE Link Local IP from     */
/*                   TE Link Remote IP                                      */
/* Input (s)       : u4RemoteIP - TE Link Remote IP                         */
/* Output (s)      : pu4LocalIP - TE Link Local IP                          */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePortTlmGetLocalIP (UINT4 u4RemoteAddr, UINT4 *pu4LocalIP, UINT4 *pu4IfIndex)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;
    UINT4               u4Flags = RSVPTE_ZERO;

    MEMSET (&InTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tOutTlmTeLink));

    u4Flags = TLM_TE_LINK_REMOTE_IP_ADD;
    InTlmTeLinkInfo.RemoteIpAddr.ip6_addr_u.u4WordAddr[0] = u4RemoteAddr;

    TlmApiGetTeLinkParams (u4Flags, &InTlmTeLinkInfo, &OutTlmTeLinkInfo);

    *pu4LocalIP = OutTlmTeLinkInfo.LocalIpAddr.ip6_addr_u.u4WordAddr[0];
    *pu4IfIndex = OutTlmTeLinkInfo.u4IfIndex;
#else
    UNUSED_PARAM (u4RemoteAddr);
    UNUSED_PARAM (pu4LocalIP);
    UNUSED_PARAM (pu4IfIndex);
#endif

    return;
}

/****************************************************************************/
/* Function Name   : RptePortTlmGetRemoteIP                                 */
/* Description     : This function is used to get TE Link Remote IP from    */
/*                   TE Link Local IP                                       */
/* Input (s)       : u4LocalIP   -  TE Link Local IP                         */
/* Output (s)      : pu4RemoteIP - TE Link Local IP                          */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePortTlmGetRemoteIP (UINT4 u4IfIndex, UINT4 *pu4RemoteAddr)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;
    UINT4               u4Flags = RSVPTE_ZERO;

    MEMSET (&InTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tOutTlmTeLink));

    u4Flags = TLM_TE_LINK_IF_INDEX;
    InTlmTeLinkInfo.u4IfIndex = u4IfIndex;

    TlmApiGetTeLinkParams (u4Flags, &InTlmTeLinkInfo, &OutTlmTeLinkInfo);

    *pu4RemoteAddr = OutTlmTeLinkInfo.RemoteIpAddr.ip6_addr_u.u4WordAddr[0];
#else
    UNUSED_PARAM (pu4RemoteAddr);
    UNUSED_PARAM (u4IfIndex);
#endif

    return;
}

/****************************************************************************/
/* Function Name   : RptePortTlmGetEncType                                  */
/* Description     : This function used to get encoding type from           */
/*                   TE Link If Index                                       */
/* Input (s)       : u4IfIndex - TE Link If Index                           */
/* Output (s)      : *pu1EncType - encoding type                            */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePortTlmGetEncType (UINT4 u4IfIndex, UINT1 *pu1EncType)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;
    UINT4               u4Flags = RSVPTE_ZERO;

    MEMSET (&InTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tOutTlmTeLink));

    InTlmTeLinkInfo.u4IfIndex = u4IfIndex;
    u4Flags = TLM_TE_LINK_IF_INDEX;

    TlmApiGetTeLinkParams (u4Flags, &InTlmTeLinkInfo, &OutTlmTeLinkInfo);

    *pu1EncType = (UINT1) OutTlmTeLinkInfo.i4EncodingType;

#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1EncType);
#endif

    return;
}

/****************************************************************************/
/* Function Name   : RptePortTlmGetSwCap                                    */
/* Description     : This function used to get switching capability from    */
/*                   TE link If Index                                       */
/* Input (s)       : u4IfIndex - TE Link If Index                           */
/* Output (s)      : *pu1SwCap - Switching capability                       */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePortTlmGetSwCap (UINT4 u4IfIndex, UINT1 *pu1SwCap)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;
    UINT4               u4Flags = RSVPTE_ZERO;

    MEMSET (&InTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, RSVPTE_ZERO, sizeof (tOutTlmTeLink));

    InTlmTeLinkInfo.u4IfIndex = u4IfIndex;
    u4Flags = TLM_TE_LINK_IF_INDEX;

    TlmApiGetTeLinkParams (u4Flags, &InTlmTeLinkInfo, &OutTlmTeLinkInfo);

    *pu1SwCap = (UINT1) OutTlmTeLinkInfo.i4SwitchingCap;
#else
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (pu1SwCap);
#endif

    return;
}

/****************************************************************************/
/* Function Name   : RptePortIsOspfTeEnabled                                */
/* Description     : This function is used to check whether OSPF-TE is      */
/*                   enabled or not                                         */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS / RPTE_FAILURE                            */
/****************************************************************************/
INT1
RptePortIsOspfTeEnabled (VOID)
{
#ifdef OSPFTE_WANTED
    if (OspfTeIsEnabled () == TRUE)
    {
        return RPTE_SUCCESS;
    }
#endif

    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RptePortCfaGetTeLinkIfFromMplsIf                       */
/* Description     : This function is used to get TE Link If from MPLS If   */
/*                   from Interface Manager                                 */
/* Input (s)       : u4MplsIfIndex     - MPLS Interface Index               */
/* Output (s)      : pu4TeLinkIf       - TE Link If                         */
/* Returns         : RPTE_SUCCESS / RPTE_FAILURE                            */
/****************************************************************************/
INT4
RptePortCfaGetTeLinkIfFromMplsIf (UINT4 u4MplsIfIndex, UINT4 *pu4TeLinkIf)
{
    *pu4TeLinkIf = RPTE_ZERO;
#ifdef CFA_WANTED
    if (CfaApiGetTeLinkIfFromMplsIf (u4MplsIfIndex,
                                     pu4TeLinkIf, TRUE) == CFA_FAILURE)
    {
        return RPTE_FAILURE;
    }
#else
    UNUSED_PARAM (u4MplsIfIndex);
#endif

    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePortCfaGetTeLinkIfFromL3If                         */
/* Description     : This function is used to get TE Link If from L3 If     */
/*                   from Interface Manager                                 */
/* Input (s)       : L3 Index          - MPLS Interface Index               */
/* Output (s)      : pu4TeLinkIf       - TE Link If                         */
/* Returns         : RPTE_SUCCESS / RPTE_FAILURE                            */
/****************************************************************************/
INT4
RptePortCfaGetTeLinkIfFromL3If (UINT4 u4L3IfIndex, UINT4 *pu4TeLinkIf)
{
    *pu4TeLinkIf = RPTE_ZERO;
#ifdef TLM_WANTED
#ifdef CFA_WANTED
    if (CfaApiGetTeLinkIfFromL3If (u4L3IfIndex, pu4TeLinkIf,
                                   RPTE_ONE, TRUE) == CFA_FAILURE)
    {
        return RPTE_FAILURE;
    }
#endif
#endif
    UNUSED_PARAM (u4L3IfIndex);
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePortCfaGetMplsIfIfFromTeLinkIf                     */
/* Description     : This function is used to get MPLS If from TE Link If   */
/*                   from Interface Manager                                 */
/* Input (s)       : u4MplsIfIndex     - MPLS Interface Index               */
/* Output (s)      : pu4TeLinkIf       - TE Link If                         */
/* Returns         : RPTE_SUCCESS / RPTE_FAILURE                            */
/****************************************************************************/
INT4
RptePortCfaGetMplsIfFromTeLinkIf (UINT4 u4TeLinkIf, UINT4 *pu4MplsIf)
{
    *pu4MplsIf = RPTE_ZERO;
#ifdef TLM_WANTED
    if (CfaApiGetMplsIfFromTeLinkIf (u4TeLinkIf, pu4MplsIf, TRUE)
        == CFA_FAILURE)
    {
        return RPTE_FAILURE;
    }
#else
    UNUSED_PARAM (u4TeLinkIf);
#endif

    return RPTE_SUCCESS;
}

/************************************************************************
 *  Function Name   : RpteDnStrLblProgForExpLblControl
 *  Description     : Function programs down stream labels for explicit
 *                    label control.
 *  Input           : pRsvpTeTnlInfo - pointer to tunnel info
 *                    u4FwdLbl       - Forward Label
 *                    u4RevLbl       - Reverse Label
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
RpteDnStrLblProgForExpLblControl (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                  UINT4 u4FwdLbl, UINT4 u4RevLbl)
{
    UINT2               u2MlibOperation = RPTE_ZERO;

    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pRsvpTeTnlInfo->u1IsGrTnl == RPTE_YES))
    {
        return;
    }

    /* AS per RFC 3473, section 5.1.1, The reverse label in ERO
     * object should be copied into Upstream Label object.
     * First the Label is copied into RSVP-TE tunnel. Then it will
     * be copied into Upstream Label object when the Label set object is
     * formed. */
    pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl = u4RevLbl;

    if ((u4FwdLbl == MPLS_INVALID_LABEL) ||
        (gpRsvpTeGblInfo->RsvpTeCfgParams.u1LabelSetEnabled == RPTE_DISABLED))
    {
        return;
    }

    /* AS per RFC 3473, section 5.1.1, The forward label in ERO
     * object should be copied into Label set object.
     * First the Label is copied into RSVP-TE tunnel. Then it will
     * be copied into Label set object when the Label set object is
     * formed. */
    pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl = u4FwdLbl;

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
    {
        u2MlibOperation = MPLS_MLIB_TNL_CREATE;
    }
    else
    {
        u2MlibOperation = MPLS_MLIB_ILM_CREATE;
    }

    /* Failure check is intentionally skipped */
    RpteRhSendMplsMlibUpdate (u2MlibOperation, RPTE_ZERO, pRsvpTeTnlInfo,
                              GMPLS_SEGMENT_FORWARD);

    return;
}

/****************************************************************************/
/* Function Name   : RpteUpStrLblProgramming
 * Description     : Function is used to program the upstream out-label
 * Input (s)       : *pRsvpTeTnlInfo - Pointer to RSVP-TE tunnel info
 * Output (s)      : None
 * Returns         : None
 *         */
/****************************************************************************/
INT1
RpteUpStrLblProgramming (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tIfEntry           *pIfEntry;
    uLabel              UpStrInLabel;

    MEMSET (&UpStrInLabel, RSVPTE_ZERO, sizeof (uLabel));

    pIfEntry = ((pRsvpTeTnlInfo)->pPsb)->pOutIfEntry;

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS)
    {
        if (RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE, RPTE_ZERO,
                                      pRsvpTeTnlInfo, GMPLS_SEGMENT_REVERSE)
            == RPTE_FAILURE)
        {
            return RPTE_FAILURE;
        }

        return RPTE_SUCCESS;
    }

    /* AS per RFC 3473, section 3.1, node must also allocate a label on the 
     * outgoing interface and establish internal data paths before filling in an
     * outgoing upstream label and propagating the Path message */
    if (pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl == MPLS_INVALID_LABEL)
    {
        if (RpteRhGetLabel (pIfEntry, &UpStrInLabel) == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PH_IF,
                        "PATH: Getting a free Upstrm Label failed\n");
            return RPTE_FAILURE;
        }

        MEMCPY (&pRsvpTeTnlInfo->UpStrInLbl, &UpStrInLabel, sizeof (uLabel));
    }
    else
    {
        /* If the reverse label is configured in Explicit Label Control
           case, then the label value should not be invalid */

        /* Check the label availability */
        if (LblMgrAvailableLblInLblGroup (RSVPTE_LBL_MODULE_ID,
                                          RSVPTE_ZERO,
                                          pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl)
            == RPTE_LBL_MNGR_FAILURE)
        {
            return RPTE_FAILURE;
        }

        if (LblMgrAssignLblToLblGroup (RSVPTE_LBL_MODULE_ID, RSVPTE_ZERO,
                                       pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl)
            == RPTE_LBL_MNGR_FAILURE)
        {
            return RPTE_FAILURE;
        }
    }

    if (RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, RPTE_ZERO,
                                  pRsvpTeTnlInfo, GMPLS_SEGMENT_REVERSE)
        == RPTE_FAILURE)
    {
        return RPTE_FAILURE;
    }

    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePortCfaCreateMplsTnlIf
 * Description     : Function is used to create MPLS Tunnel Interface at the
 *                   Ingress and Egress Nodes.
 * Input (s)       : *pRsvpTeTnlInfo - Pointer to RSVP-TE tunnel info
 *                   u4MsgType       - Message Type
 * Output (s)      : None
 * Returns         : None
 *                                                                          */
/****************************************************************************/
INT1
RptePortCfaCreateMplsTnlIf (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT4 u4MsgType)
{
    tIfEntry           *pIfEntry;
    UINT4               u4L3If = RSVPTE_ZERO;
    UINT4               u4MplsTnlIfIndex = RSVPTE_ZERO;
    UINT4               u4CurL3If = RSVPTE_ZERO;
    UINT4               u4ReRoutedTnlIfIndex = RSVPTE_ZERO;
    UINT4               u4TnlIfIndex = RSVPTE_ZERO;
    UINT4               u4RevTnlIfIndex = RSVPTE_ZERO;
    INT4                i4OrigStorageType = RSVPTE_ZERO;
    BOOL1               b1ReRouteFlag = FALSE;
    tRsvpTeTnlInfo     *pRsvpTeMapTnlInfo = NULL;

    /*Stacking is not needed for Gr tunnel  */
    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pRsvpTeTnlInfo->u1IsGrTnl == RPTE_YES))
    {
        return RPTE_SUCCESS;
    }

    pRsvpTeMapTnlInfo = pRsvpTeTnlInfo->pMapTnlInfo;

    /* Mpls Tunnel interface stacking */

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlIsIf == TE_FALSE)
    {
        return RPTE_SUCCESS;
    }

    if (u4MsgType == RESV_MSG)
    {
        u4MplsTnlIfIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIfIndex;
    }
    else if (u4MsgType == PATH_MSG)
    {
        u4MplsTnlIfIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4RevTnlIfIndex;
    }

    if (pRsvpTeMapTnlInfo != NULL)
    {
        u4TnlIfIndex = pRsvpTeMapTnlInfo->pTeTnlInfo->u4TnlIfIndex;
        u4RevTnlIfIndex = pRsvpTeMapTnlInfo->pTeTnlInfo->u4RevTnlIfIndex;
    }

    if (u4MsgType == RESV_MSG)
    {
        pIfEntry = pRsvpTeTnlInfo->pPsb->pOutIfEntry;
        u4L3If = pRsvpTeTnlInfo->pTeTnlInfo->u4FwdComponentIfIndex;
        u4ReRoutedTnlIfIndex = u4TnlIfIndex;
    }
    else if (u4MsgType == PATH_MSG)
    {
        pIfEntry = pRsvpTeTnlInfo->pPsb->pIncIfEntry;
        u4L3If = pRsvpTeTnlInfo->pTeTnlInfo->u4RevComponentIfIndex;
        u4ReRoutedTnlIfIndex = u4RevTnlIfIndex;
    }
    else
    {
        return RPTE_FAILURE;
    }

    if (((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRerouteFlag == TRUE) ||
         (pRsvpTeTnlInfo->pTeTnlInfo->bIsMbbRequired == TRUE) ||
         (TE_TNL_REOPTIMIZE_TRIGGER (pRsvpTeTnlInfo->pTeTnlInfo) == TRUE)) &&
        (u4ReRoutedTnlIfIndex != RPTE_ZERO))
    {
        b1ReRouteFlag = TRUE;
        u4MplsTnlIfIndex = u4ReRoutedTnlIfIndex;
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_PROTECTION_PATH)
    {
        u4MplsTnlIfIndex = RPTE_ZERO;
    }

    /* u4L3If filled from Forward or Reverse Component If 
     * is not equal to zero when,
     *   1. When TLM is enabled
     *   2. Bundle case
     */
    if (u4L3If == RPTE_ZERO)
    {
        u4L3If = pIfEntry->u4IfIndex;

#ifdef CFA_WANTED
        if (CfaUtilGetL3IfFromMplsIf (u4L3If, &u4L3If, TRUE) == CFA_FAILURE)
        {
            return RPTE_FAILURE;
        }
#endif
    }

    if (u4MplsTnlIfIndex != RPTE_ZERO)
    {
        if (CfaUtilGetIfIndexFromMplsTnlIf (u4MplsTnlIfIndex, &u4CurL3If, TRUE)
            == CFA_FAILURE)
        {
            return RPTE_FAILURE;
        }
        if ((u4CurL3If != RPTE_ZERO) && (u4L3If != RPTE_ZERO) &&
            (u4L3If == u4CurL3If))
        {
            RpteSetTnlIfIndex (pRsvpTeTnlInfo, pRsvpTeMapTnlInfo,
                               u4MplsTnlIfIndex, u4MsgType, b1ReRouteFlag);
            return RPTE_SUCCESS;
        }
        if (b1ReRouteFlag == TRUE)
        {
            CfaGetIfMainStorageType (u4MplsTnlIfIndex, &i4OrigStorageType);
            CfaSetIfMainStorageType (u4MplsTnlIfIndex,
                                     MPLS_STORAGE_NONVOLATILE);
            if (CfaIfmDeleteStackMplsTunnelInterface
                (u4CurL3If, u4MplsTnlIfIndex) == CFA_FAILURE)
            {
                CfaSetIfMainStorageType (u4MplsTnlIfIndex, i4OrigStorageType);
                return RPTE_FAILURE;
            }
            CfaSetIfMainStorageType (u4MplsTnlIfIndex, i4OrigStorageType);
        }
    }

#ifdef CFA_WANTED
    if (CfaIfmCreateStackMplsTunnelInterface (u4L3If, &u4MplsTnlIfIndex)
        == CFA_FAILURE)
    {
        return RPTE_FAILURE;
    }
#endif

    if (MplsSetIfInfo (RPTE_TE_TNL (pRsvpTeTnlInfo), u4MplsTnlIfIndex, IF_NAME)
        == RPTE_FAILURE)
    {
        return RPTE_FAILURE;
    }

    RpteSetTnlIfIndex (pRsvpTeTnlInfo, pRsvpTeMapTnlInfo, u4MplsTnlIfIndex,
                       u4MsgType, b1ReRouteFlag);

    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function     : RpteGetIfUnnumPeerMac                                      */
/* Description  : This function gets peer mac address of unnumbered I/F      */
/* Input        : u4IfIndex         - I/F Index                              */
/* Output       : pu1MacAddr        - Peer mac address                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
RpteGetIfUnnumPeerMac (UINT4 u4IfIndex, UINT1 *pu1MacAddr)
{
    UINT4               u4L3IntIf = RPTE_ZERO;
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, RPTE_ZERO, sizeof (tCfaIfInfo));
    if (MplsGetL3Intf (u4IfIndex, &u4L3IntIf) == MPLS_FAILURE)
    {
        return;
    }
    if (CfaGetIfInfo (u4L3IntIf, &IfInfo) == CFA_FAILURE)
    {
        return;
    }
    MEMCPY (pu1MacAddr, IfInfo.au1PeerMacAddr, CFA_ENET_ADDR_LEN);
    return;
}

/*****************************************************************************/
/* Function     : RpteHandleTunnelEvents                                     */
/* Description  : This function handles tunnel events from TE module.        */
/*                This scans through the RSVP-TE Tunnel Table and matches    */
/*                tunnel index, tunnel ingress and tunnel egress with the    */
/*                passed value. If these values match and passed tunnel      */
/*                instance is ZERO, act on all instances of the tunnel       */
/*                If these values match and passed tunnel instance is        */
/*                NON-ZERO, act on this tunnel alone.                        */
/* Input        : u4TunnelIndex     - Tunnel Index                           */
/*                u4TunnelInstance  - Tunnel Instance                        */
/*                u4IngressId       - Ingress ID                             */
/*                u4EgressId        - Egress ID                              */
/*                u4Event           - Event                                  */
/*                pTeTnlInfo        - Pointer to TE Tunnel Info              */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/

VOID
RpteHandleTunnelEvents (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                        UINT4 u4IngressId, UINT4 u4EgressId, UINT4 u4Event,
                        tTeTnlInfo * pTeTnlInfo)
{
    tRsvpTeTnlInfo      InRpteTnlInfo;
    MEMSET (&InRpteTnlInfo, RPTE_ZERO, sizeof (tRsvpTeTnlInfo));
    RpteGetTnlsAndHandleEvents (u4TunnelIndex, u4TunnelInstance,
                                u4IngressId, u4EgressId, u4Event,
                                pTeTnlInfo, &InRpteTnlInfo);
    return;
}

/*****************************************************************************/
/* Function     : RpteGetTnlsAndHandleEvents                                 */
/* Description  : This function handles tunnel events from TE module.        */
/*                This scans through the RSVP-TE Tunnel Table and matches    */
/*                tunnel index, tunnel ingress and tunnel egress with the    */
/*                passed value. If these values match and passed tunnel      */
/*                instance is ZERO, act on all instances of the tunnel       */
/*                If these values match and passed tunnel instance is        */
/*                NON-ZERO, act on this tunnel alone.                        */
/* Input        : u4TunnelIndex     - Tunnel Index                           */
/*                u4TunnelInstance  - Tunnel Instance                        */
/*                u4IngressId       - Ingress ID                             */
/*                u4EgressId        - Egress ID                              */
/*                u4Event           - Event                                  */
/*                pTeTnlInfo        - Pointeir to TE Tunnel Info             */
/*                pInRpteTnlInfo    - Pointer to RSVPTE tunnel Info          */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/

VOID
RpteGetTnlsAndHandleEvents (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                            UINT4 u4IngressId, UINT4 u4EgressId, UINT4 u4Event,
                            tTeTnlInfo * pTeTnlInfo,
                            tRsvpTeTnlInfo * pInRpteTnlInfo)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTempRsvpTeTnlInfo = NULL;
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;
    BOOL1               bEntryFound = FALSE;
    UINT4               u4ValIngressId = RPTE_ZERO;
    UINT4               u4ValEgressId = RPTE_ZERO;
    tTeTnlInfo          InTeTnlInfo;

    MEMSET (&RpteEnqueueMsgInfo, RPTE_ZERO, sizeof (tRpteEnqueueMsgInfo));
    MEMSET (&InTeTnlInfo, RPTE_ZERO, sizeof (tTeTnlInfo));

    u4IngressId = OSIX_HTONL (u4IngressId);
    u4EgressId = OSIX_HTONL (u4EgressId);

    InTeTnlInfo.u4TnlIndex = u4TunnelIndex;
    InTeTnlInfo.u4TnlPrimaryInstance = u4TunnelInstance;
    MEMCPY (&InTeTnlInfo.TnlIngressLsrId, &u4IngressId, IPV4_ADDR_LENGTH);
    MEMCPY (&InTeTnlInfo.TnlEgressLsrId, &u4EgressId, IPV4_ADDR_LENGTH);
    pInRpteTnlInfo->pTeTnlInfo = &InTeTnlInfo;

    /* If Instance 0 is not passed to this function, handle the event
     * only for that instance alone. */
    if (u4TunnelInstance != RSVPTE_ZERO)
    {
        pRsvpTeTnlInfo =
            (tRsvpTeTnlInfo *) RBTreeGet (gpRsvpTeGblInfo->RpteTnlTree,
                                          (tRBElem *) pInRpteTnlInfo);
        if (pRsvpTeTnlInfo != NULL)
        {
            bEntryFound = TRUE;
            RpteActOnTunnelEvent (pRsvpTeTnlInfo, u4Event);
        }
    }
    else
    {
        pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                        (tRBElem *) pInRpteTnlInfo, NULL);
        while (pRsvpTeTnlInfo != NULL)
        {
            if (RPTE_TE_TNL (pRsvpTeTnlInfo) == NULL)
            {
                return;
            }

            CONVERT_TO_INTEGER (pRsvpTeTnlInfo->pTeTnlInfo->TnlIngressLsrId,
                                u4ValIngressId);
            CONVERT_TO_INTEGER (pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId,
                                u4ValEgressId);

            if ((u4TunnelIndex != RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo)) ||
                (u4IngressId != u4ValIngressId)
                || (u4EgressId != u4ValEgressId))
            {
                break;
            }

            /* If Instance 0 is passed to this function, repeat the event
             * for all the instances. */
            bEntryFound = TRUE;
            /*Holding Priority should be zero for HLSP tunnel */
            if (pTeTnlInfo->u4TnlType & MPLS_TE_TNL_TYPE_HLSP)
            {
                pRsvpTeTnlInfo->pTeTnlInfo->u1TnlHoldPrio = RPTE_ZERO;
            }

            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType = pTeTnlInfo->u4TnlType;
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSsnAttr = pTeTnlInfo->u1TnlSsnAttr;
            pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus =
                pTeTnlInfo->GmplsTnlInfo.u4AdminStatus;

            if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                 MPLS_TE_DEDICATED_ONE2ONE) &&
                (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                 MPLS_TE_UNPROTECTED))
            {
                if ((pTeTnlInfo->u4TnlPrimaryInstance ==
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance) &&
                    (pTeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
                {
                    pTempRsvpTeTnlInfo = pRsvpTeTnlInfo;
                    pRsvpTeTnlInfo =
                        RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                       (tRBElem *) pRsvpTeTnlInfo, NULL);
                    RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pTempRsvpTeTnlInfo;
                    RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
                    continue;
                }
                else if ((pTeTnlInfo->u4BkpTnlInstance ==
                          pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance) &&
                         (pTeTnlInfo->u1TnlLocalProtectInUse !=
                          LOCAL_PROT_IN_USE))
                {
                    pTempRsvpTeTnlInfo = pRsvpTeTnlInfo;
                    pRsvpTeTnlInfo =
                        RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                       (tRBElem *) pRsvpTeTnlInfo, NULL);
                    RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pTempRsvpTeTnlInfo;
                    RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
                    continue;
                }

            }
            else if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                      i4E2EProtectionType == MPLS_TE_UNPROTECTED)
                     && (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                         MPLS_TE_DEDICATED_ONE2ONE))
            {
                RpteEnqueueMsgInfo.u.pTeTnlInfo = pTeTnlInfo;
                RpteEnqueueMsgInfo.u4RpteEvent = RSVPTE_TNL_UP;
                RpteTEProcessTeEvent (&RpteEnqueueMsgInfo);
            }
            pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType
                = pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType;

            pTempRsvpTeTnlInfo = pRsvpTeTnlInfo;
            pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                            (tRBElem *) pRsvpTeTnlInfo, NULL);
            RpteActOnTunnelEvent (pTempRsvpTeTnlInfo, u4Event);
        }
    }

    if ((bEntryFound == FALSE) && (u4Event == RSVPTE_TNL_DESTROY))
    {
        TeSigDeleteTnlInfo (pTeTnlInfo, TE_TNL_CALL_FROM_ADMIN);
    }

    return;
}

/*****************************************************************************/
/* Function     : RpteActOnTunnelEvent                                       */
/* Description  : This function handles tunnel event for a tunnel from TE    */
/*                module.                                                    */
/* Input        : pRsvpTeTnlInfo    - Pointer to RSVP-TE Tunnel Info         */
/*                u4Event           - Event                                  */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/

VOID
RpteActOnTunnelEvent (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT4 u4Event)
{

    switch (u4Event)
    {
        case RSVPTE_TNL_DOWN:
        case RSVPTE_TNL_DESTROY:
            if (RpteTriggerInternalEvent (pRsvpTeTnlInfo,
                                          RPTE_TNL_DESTROY) == RPTE_FAILURE)
            {
                return;
            }
            break;

        case RSVPTE_TNL_TX_PATH_MSG:
            /* if Admin status change indication is received from TE,
             * immediately path message has to be sent from ingress */
            RptePhPathRefresh (pRsvpTeTnlInfo);
            break;

        case RPTE_PROTECTION_SWITCH_OVER:
            RpteUtlSwitchTraffic (pRsvpTeTnlInfo, (UINT1) u4Event);
            break;

        case RPTE_PROTECTION_SWITCH_BACK:
            RpteUtlSwitchTraffic (pRsvpTeTnlInfo, (UINT1) u4Event);
            break;

        default:
            break;
    }

    return;
}

/****************************************************************************/
/* Function Name   : RptePortCfaDeleteMplsTnlIf
 * Description     : Function is used to delete MPLS Tunnel Interface at the
 *                   Ingress and Egress Nodes.
 * Input (s)       : *pRsvpTeTnlInfo - Pointer to RSVP-TE tunnel info
 *                   u4MsgType       - Message Type
 * Output (s)      : None
 * Returns         : None
 *                                                                          */
/****************************************************************************/
INT1
RptePortCfaDeleteMplsTnlIf (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT4 u4MsgType)
{
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4L3If = RSVPTE_ZERO;
    UINT4               u4MplsTnlIfIndex = RSVPTE_ZERO;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    /* Mpls Tunnel interface stacking */
    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlIsIf == TE_FALSE)
    {
        return RPTE_SUCCESS;
    }

    if (u4MsgType == RESV_MSG)
    {
        u4MplsTnlIfIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIfIndex;
    }
    else if (u4MsgType == PATH_MSG)
    {
        u4MplsTnlIfIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4RevTnlIfIndex;
    }

    if (MplsGetL3Intf (u4MplsTnlIfIndex, &u4L3If) == RPTE_FAILURE)
    {
        return RPTE_FAILURE;
    }

#ifdef CFA_WANTED
    if (CfaIfmDeleteStackMplsTunnelInterface (u4L3If, u4MplsTnlIfIndex)
        == CFA_FAILURE)
    {
        return RPTE_FAILURE;
    }
#endif

    if (u4MsgType == RESV_MSG)
    {
        if (CfaGetIfInfo (u4MplsTnlIfIndex, &CfaIfInfo) == CFA_FAILURE)
        {
            return RPTE_FAILURE;
        }

        if (CfaIfInfo.i4IfMainStorageType == MPLS_STORAGE_VOLATILE)
        {
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIfIndex = RPTE_ZERO;
        }
    }
    else if (u4MsgType == PATH_MSG)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u4RevTnlIfIndex = RPTE_ZERO;
    }

    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function     : RptePortCreateFATELink                             */
/* Description  : This function Calls the TLM API and creates or updates     */
/*                FA TE Link in TLM Database                                 */
/* Input        : pRsvpTeTnlInfo - Pointer to tRsvpTeTnlInfo                 */
/* Returns      : RPTE_SUCCESS / RPTE_FAILURE                                */
/*****************************************************************************/

INT4
RptePortCreateFATELink (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RptePortCreateFATELink : " "ENTRY \n");

    MEMSET (&InTlmTeLinkInfo, 0, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, 0, sizeof (tOutTlmTeLink));

    /*
     * Local Identifier of FA TE Link is always the Tunnel Interface Index
     * of the Router.
     * Remote Identifier of FA TE Link is always the Tunnel Interface Index
     * of the Peer Router.
     * Remote Information is Stored in RSB in case of Ingress and in PSB
     * in case of Egress
     */

    if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INTERMEDIATE) ||
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH))
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "TE Link creation not required at intermediate node\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RptePortCreateFATELink: EXIT\n");
        return RPTE_SUCCESS;
    }

    if (((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
         (pRsvpTeTnlInfo->pRsb == NULL)) ||
        ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS) &&
         (pRsvpTeTnlInfo->pPsb == NULL)))
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "Err : In Ingress, RSB cannot be NULL "
                    "And In Egress, PSB Cannot be NULL\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RptePortCreateFATELink: INTMD-EXIT\n");
        return RPTE_FAILURE;
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
    {
        InTlmTeLinkInfo.u4LocalIdentifier
            = pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIfIndex;
        InTlmTeLinkInfo.u4RemoteIdentifier
            = pRsvpTeTnlInfo->pRsb->LspTnlIfId.u4InterfaceId;
        InTlmTeLinkInfo.u4RemoteRtrId
            = pRsvpTeTnlInfo->pRsb->LspTnlIfId.u4RouterId;
    }
    else if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS)
    {
        InTlmTeLinkInfo.u4LocalIdentifier
            = pRsvpTeTnlInfo->pTeTnlInfo->u4RevTnlIfIndex;
        InTlmTeLinkInfo.u4RemoteIdentifier
            = pRsvpTeTnlInfo->pPsb->LspTnlIfId.u4InterfaceId;
        InTlmTeLinkInfo.u4RemoteRtrId
            = pRsvpTeTnlInfo->pPsb->LspTnlIfId.u4RouterId;
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->pTeTrfcParams != NULL &&
        pRsvpTeTnlInfo->pTeTnlInfo->pTeTrfcParams->pRSVPTrfcParams != NULL)
    {
        InTlmTeLinkInfo.reqResBw = (tBandWidth) pRsvpTeTnlInfo->pTeTnlInfo->
            pTeTrfcParams->pRSVPTrfcParams->u4PeakDataRate;
        RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (InTlmTeLinkInfo.reqResBw);
    }

    pRsvpTeTnlInfo->u4RsrcClassColor = RPTE_ZERO;
    InTlmTeLinkInfo.u4RsrcClassColor = pRsvpTeTnlInfo->u4RsrcClassColor;

    InTlmTeLinkInfo.i4SwitchingCap = (INT4) pRsvpTeTnlInfo->pTeTnlInfo->
        GmplsTnlInfo.u1SwitchingType;
    InTlmTeLinkInfo.i4EncodingType = (INT4) pRsvpTeTnlInfo->pTeTnlInfo->
        GmplsTnlInfo.u1EncodingType;
    InTlmTeLinkInfo.u4CompIfIndex = InTlmTeLinkInfo.u4LocalIdentifier;

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
    {
        /* RFC 4206 : By default the TE metric on the FA is set 
         * to max(1, (the TE metric of the FA-LSP path) - 1) */
        InTlmTeLinkInfo.u4TeMetric =
            RPTE_ONE >
            ((pRsvpTeTnlInfo->pTeTnlInfo->u4TnlPathMetric) -
             RPTE_ONE) ? RPTE_ONE : ((pRsvpTeTnlInfo->pTeTnlInfo->
                                      u4TnlPathMetric) - RPTE_ONE);
    }
    else
    {
        InTlmTeLinkInfo.u4TeMetric = RPTE_ONE;
    }

    if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == TE_TNL_MODE_UNIDIRECTIONAL))
    {
        InTlmTeLinkInfo.i4IsTeLinkAdvtReqd = FALSE;
    }
    else
    {
        InTlmTeLinkInfo.i4IsTeLinkAdvtReqd = TRUE;
    }

    if (TlmApiCreateFATeLink (&InTlmTeLinkInfo, &OutTlmTeLinkInfo) ==
        TLM_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RptePortCreateFATELink : " "EXIT \n");
        return RPTE_SUCCESS;
    }
#else
    UNUSED_PARAM (pRsvpTeTnlInfo);
#endif

    RSVPTE_DBG (RSVPTE_IF_PRCS, "Err : FA TE Link Creation/Updation Failed\n");
    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RptePortCreateFATELink : EXIT \n");
    return RPTE_FAILURE;
}

/*****************************************************************************/
/* Function     : RpteDeleteFATeLink                                         */
/* Description  : This function Calls the TLM API and Deletes the created    */
/*                FA TE Link in TLM Database                                 */
/* Input        : pRsvpTeTnlInfo - Pointer to tRsvpTeTnlInfo                 */
/* Output       : None                                                       */
/* Returns      : RPTE_SUCCESS / RPTE_FAILURE                                */
/*****************************************************************************/

INT4
RpteDeleteFATeLink (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tRsvpTeTnlInfo     *pRsvpFATnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpFARsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteDeleteFATeLink : ENTRY \n");
    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INTERMEDIATE)
    {
        return RPTE_SUCCESS;
    }

    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        InTlmTeLinkInfo.u4CompIfIndex =
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIfIndex;
    }
    else
    {
        InTlmTeLinkInfo.u4CompIfIndex =
            pRsvpTeTnlInfo->pTeTnlInfo->u4RevTnlIfIndex;
    }

    UTL_SLL_OFFSET_SCAN (&(pRsvpTeTnlInfo->StackTnlList),
                         pRsvpFATnlInfo, pTmpFARsvpTeTnlInfo, tRsvpTeTnlInfo *)
    {
        TeSigDeleteStackTnlIf (pRsvpFATnlInfo->pTeTnlInfo);
    }

    if (TlmApiDeleteFATeLink (&InTlmTeLinkInfo) == TLM_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteDeleteFATeLink : EXIT \n");
        return RPTE_SUCCESS;
    }
#else
    UNUSED_PARAM (pRsvpTeTnlInfo);
#endif
    RSVPTE_DBG (RSVPTE_IF_PRCS, "Err : FA TE Link Delete Failed\n");
    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteDeleteFATeLink : EXIT \n");
    return RPTE_FAILURE;
}

/*****************************************************************************/
/* Function     : RpteSetTnlIfIndex                                          */
/* Description  : This function sets the tunnel if index for the current     */
/*                and resets it from the previous tunnel                     */
/* Input        : pRsvpTeTnlInfo    - Pointer to tRsvpTeTnlInfo              */
/*                pRsvpTeMapTnlInfo - Pointer to Map RSVP-TE Tnl Info        */
/*                u4TnlIfIndex      - Tunnel If Index to be set              */
/*                u4MsgType         - Message Type in which tnl if index     */
/*                                    to be set                              */
/*                b1ReRouteFlag     - Re Route Flag                          */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RpteSetTnlIfIndex (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                   tRsvpTeTnlInfo * pRsvpTeMapTnlInfo, UINT4 u4TnlIfIndex,
                   UINT4 u4MsgType, BOOL1 b1ReRouteFlag)
{
    if (u4MsgType == RESV_MSG)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIfIndex = u4TnlIfIndex;
        if ((pRsvpTeMapTnlInfo != NULL) &&
            ((pRsvpTeMapTnlInfo->u1FrrCapable != RPTE_TRUE) ||
             (pRsvpTeMapTnlInfo->pTeTnlInfo->u1TnlReoptimizeStatus == TRUE)) &&
            (b1ReRouteFlag == TRUE))
        {
            pRsvpTeMapTnlInfo->pTeTnlInfo->u4TnlIfIndex = RPTE_ZERO;
        }
    }
    else if (u4MsgType == PATH_MSG)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u4RevTnlIfIndex = u4TnlIfIndex;
        if (pRsvpTeMapTnlInfo != NULL)
        {
            if ((pRsvpTeMapTnlInfo->pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP) &&
                (pRsvpTeMapTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS))
            {
                if (pRsvpTeMapTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                    i4E2EProtectionType != MPLS_TE_DEDICATED_ONE2ONE)
                {
                    TeSigDelFromTeIfIdxbasedTable (pRsvpTeMapTnlInfo->
                                                   pTeTnlInfo);
                    pRsvpTeMapTnlInfo->pTeTnlInfo->u4TnlIfIndex = RPTE_ZERO;
                }

                TeSigAddToTeIfIdxbasedTable (pRsvpTeTnlInfo->pTeTnlInfo);
            }
            if (((pRsvpTeMapTnlInfo->u1FrrCapable != RPTE_TRUE) ||
                 (pRsvpTeMapTnlInfo->pTeTnlInfo->u1TnlReoptimizeStatus == TRUE))
                && (b1ReRouteFlag == TRUE))
            {
                pRsvpTeMapTnlInfo->pTeTnlInfo->u4RevTnlIfIndex = RPTE_ZERO;
            }
        }
    }

    return;
}

/*****************************************************************************/
/* Function     : RpteApiIfStatusChgNotify                                   */
/* Description  : This function handles data te link down indication         */
/* Input        : i4CtrlIfIndex     - control telink index                   */
/*                i4DataIfIndex     - data telink index                      */
/*                u1OperStatus      - Oper status of the link                */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/

VOID
RpteApiIfStatusChgNotify (INT4 i4CtrlMplsIfIndex, INT4 i4DataTeIfIndex,
                          UINT1 u1OperStatus)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal = RPTE_FAILURE;

    u1RetVal = RpteGetIfEntry (i4CtrlMplsIfIndex, &pIfEntry);

    if (u1RetVal == RPTE_FAILURE)
    {
        return;
    }

    RsvpIfStChgEventHandler ((UINT4) i4CtrlMplsIfIndex,
                             IF_ENTRY_ADDR (pIfEntry), u1OperStatus,
                             i4DataTeIfIndex);

    return;
}

/*****************************************************************************/
/* Function     : RpteProcessL3VPNEvent                                      */
/* Description  : This function will notify the tunnel OPER state to L3VPN   */
/* Input        : pRpteEnqueueMsgInfo - Message Info                         */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
UINT1
RpteProcessL3VPNEvent (tRpteEnqueueMsgInfo * pRpteEnqueueMsgInfo)
{
    UINT4               u4TunnelIndex = RSVPTE_ZERO;
    UINT4               u4TunnelInstance = RSVPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RSVPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RSVPTE_ZERO;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tTeTnlL3VpnInfo    *pTeTnlL3VpnInfo = NULL;
#ifdef MPLS_L3VPN_WANTED
    tL3VpnRsvpTeLspEventInfo L3VpnRsvpTeLspEventInfo;

    MEMSET (&L3VpnRsvpTeLspEventInfo, 0, sizeof (L3VpnRsvpTeLspEventInfo));
#endif

    pTeTnlL3VpnInfo = &pRpteEnqueueMsgInfo->TeTnlL3VpnInfo;
    if (pTeTnlL3VpnInfo == NULL)
    {
        return RPTE_FAILURE;
    }
    u4TunnelIndex = RSVPTE_TNL_INDEX (pTeTnlL3VpnInfo);
    u4TunnelInstance = pTeTnlL3VpnInfo->u4TnlInstance;
    CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID (pTeTnlL3VpnInfo),
                        u4TunnelIngressLSRId);
    u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
    CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID (pTeTnlL3VpnInfo),
                        u4TunnelEgressLSRId);
    u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);

    if (((RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                  u4TunnelIndex,
                                  u4TunnelInstance,
                                  u4TunnelIngressLSRId,
                                  u4TunnelEgressLSRId,
                                  &pRsvpTeTnlInfo)) == RPTE_SUCCESS) &&
        (pRsvpTeTnlInfo->pTeTnlInfo != NULL))
    {
#ifdef MPLS_L3VPN_WANTED
        L3VpnRsvpTeLspEventInfo.u4TnlIndex =
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex;
        L3VpnRsvpTeLspEventInfo.u4TnlXcIndex =
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlXcIndex;
        L3VpnRsvpTeLspEventInfo.u1TnlOperStatus =
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlOperStatus;
        L3VpnRsvpTeLspEventInfo.u1TnlAdminStatus =
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlAdminStatus;
        L3VpnRsvpTeLspEventInfo.u1Protection =
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse;
        if (pRpteEnqueueMsgInfo->u4RpteEvent == TE_OPER_UP)
        {

            L3VpnRsvpTeLspEventInfo.u4EventType = L3VPN_RSVPTE_LSP_UP;
        }
        if (pRpteEnqueueMsgInfo->u4RpteEvent == TE_OPER_DOWN)
        {
            L3VpnRsvpTeLspEventInfo.u4EventType = L3VPN_RSVPTE_LSP_DOWN;
        }
        L3VpnRsvpTeLspStatusChangeEventHandler (L3VpnRsvpTeLspEventInfo);
#endif
    }
    else
    {
        if (pRpteEnqueueMsgInfo->u4RpteEvent == TE_OPER_DOWN)
        {
#ifdef MPLS_L3VPN_WANTED
            if (pTeTnlL3VpnInfo->u4TnlIndex != RSVPTE_ZERO)
            {
                L3VpnRsvpTeLspEventInfo.u4TnlIndex =
                    pTeTnlL3VpnInfo->u4TnlIndex;
                L3VpnRsvpTeLspEventInfo.u4TnlXcIndex =
                    pTeTnlL3VpnInfo->u4TnlXcIndex;
                L3VpnRsvpTeLspEventInfo.u4EventType = L3VPN_RSVPTE_LSP_DOWN;
                L3VpnRsvpTeLspStatusChangeEventHandler
                    (L3VpnRsvpTeLspEventInfo);
            }
#endif
        }
    }
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function     : RpteProcessL2VPNEvent                                      */
/* Description  : This function done the reprogramming of tunnels in facility*/
/*                backup method since the older tunnel if index is deleted by*/
/*                L2VPN module                                               */
/* Input        : pRpteEnqueueMsgInfo - Message Info                         */
/* Output       : None                                                       */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
RpteProcessL2VPNEvent (tRpteEnqueueMsgInfo * pRpteEnqueueMsgInfo)
{
    UINT4               u4TunnelIndex = RSVPTE_ZERO;
    UINT4               u4TunnelInstance = RSVPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RSVPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RSVPTE_ZERO;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = pRpteEnqueueMsgInfo->u.pTeTnlInfo;
    if (pTeTnlInfo == NULL)
    {
        return;
    }
    u4TunnelIndex = RSVPTE_TNL_INDEX (pTeTnlInfo);
    u4TunnelInstance = pTeTnlInfo->u4TnlInstance;
    CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID (pTeTnlInfo),
                        u4TunnelIngressLSRId);
    u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
    CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID (pTeTnlInfo),
                        u4TunnelEgressLSRId);
    u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);

    if (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                u4TunnelIndex,
                                u4TunnelInstance,
                                u4TunnelIngressLSRId,
                                u4TunnelEgressLSRId,
                                &pRsvpTeTnlInfo) == RPTE_SUCCESS)
    {
        MEMSET (pRsvpTeTnlInfo->pTeTnlInfo->au1NextHopMac, MPLS_ZERO,
                MAC_ADDR_LEN);
        RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE, RSVPTE_ZERO,
                                  pRsvpTeTnlInfo, GMPLS_SEGMENT_FORWARD);

        if (RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE)
        {
            rpteTeFrrTnlAppUp (RPTE_TE_TNL (pRsvpTeTnlInfo));
        }
        else
        {
            RSVPTE_DBG1 (RSVPTE_IF_PRCS,
                         "RpteProcessL2VPNEvent: Sending TE_OPER_UP for label: %u"
                         "\n", RSVPTE_TNL_OUTLBL (pRsvpTeTnlInfo).u4GenLbl);
            TeSigProcessL2VpnAssociation (RPTE_TE_TNL (pRsvpTeTnlInfo),
                                          TE_OPER_UP);
        }
        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlDelSyncFlag = MPLS_FALSE;
    }
    return;
}

/****************************************************************************/
/* Function Name   : RpteFillExpRoute                                       */
/* Description     : This function Fills the OsteExpRoute For CSPF PATH     */
/* Input (s)       : pEROList: ERO List Of ER Hops                          */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

INT1
RpteFillExpRoute (tTMO_SLL * pEROList, tCspfCompInfo * pCspfCompInfo,
                  tTeTnlInfo * pTeTnlInfo)
{

    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    tRsvpTeArHopInfo   *pRsvpTeArHopInfo = NULL;
    tTMO_SLL           *pRROList = NULL;
    UINT2               u2HopCount = RPTE_ZERO;
    UINT2               u2HopType = RPTE_ZERO;
    UINT4               u4HopIpAddr = RPTE_ZERO;
    UINT4               u4DestAddr = RPTE_ZERO;
    BOOLEAN             bCountEROList = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteFillExpRoute: ENTRY\n");
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4DestAddr);

    u4DestAddr = OSIX_NTOHL (u4DestAddr);

    TMO_SLL_Scan (pEROList, pRsvpTeErHopInfo, tRsvpTeHopInfo *)
    {
        if (u2HopCount >= OSPF_TE_MAX_NEXT_HOPS)
        {
            break;
        }

        if (bCountEROList == RPTE_ZERO)
        {
            if (pRsvpTeErHopInfo->u1LsrPartOfErHop == RPTE_TRUE)
            {
                continue;
            }
            bCountEROList = RPTE_ONE;
        }
        CONVERT_TO_INTEGER (pRsvpTeErHopInfo->IpAddr.au1Ipv4Addr, u4HopIpAddr);
        u4HopIpAddr = OSIX_NTOHL (u4HopIpAddr);
        if (pRsvpTeErHopInfo->i4TnlHopIncludeAny == TE_SNMP_TRUE)
        {
            u2HopType = OSPF_TE_INCLUDE_ANY;
        }
        else if (pRsvpTeErHopInfo->u1HopIncludeExclude == TE_SNMP_FALSE)
        {
            u2HopType = OSPF_TE_FAILED_LINK;
        }
        else if (pRsvpTeErHopInfo->u1HopType == RPTE_ERHOP_LOOSE)
        {
            if (u2HopType == OSPF_TE_STRICT_EXP_ROUTE)
            {
                /* If the Loose Hop is present after Strict Hop,
                 ** then set hop type as OSPF_TE_STRICT_LOOSE_EXP_ROUTE*/
                u2HopType = OSPF_TE_STRICT_EXP_ROUTE;
                pCspfCompInfo->u4DestIpAddr =
                    pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount -
                                                          1].u4RouterId;
                break;
            }
            else
            {
                /*If all the nodes are Loose Hops,
                 ** then set hop type as OSPF_TE_LOOSE_EXP_ROUTE*/
                u2HopType = OSPF_TE_LOOSE_EXP_ROUTE;
            }
            /* If loose hop is encountered, then set destination
             ** ip address as ip address of the loose node */
            pCspfCompInfo->u4DestIpAddr = u4HopIpAddr;
        }
        else if (pRsvpTeErHopInfo->u1HopType == RPTE_ERHOP_STRICT)
        {
            /*If all the nodes are Stric Hops,
             ** then set hop type as OSPF_TE_STRICT_EXP_ROUTE*/
            pCspfCompInfo->u4DestIpAddr = u4HopIpAddr;
            u2HopType = OSPF_TE_STRICT_EXP_ROUTE;
        }

        if ((pTeTnlInfo->u1TnlRerouteFlag == TRUE)
            || (pTeTnlInfo->bIsMbbRequired == TRUE)
            || (RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo) == TRUE))
        {
            pCspfCompInfo->osTeAlternateRoute.aNextHops[u2HopCount].
                u4RouterId = u4HopIpAddr;
            u2HopCount++;

            if (u2HopType == OSPF_TE_LOOSE_EXP_ROUTE)
            {
                break;
            }

        }
        else
        {
            /*Fill information in explicit route */
            if (pRsvpTeErHopInfo->u1HopType == RPTE_ERHOP_LOOSE)
            {
                break;
            }
            else
            {
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].u4RouterId
                    = u4HopIpAddr;
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].
                    u4RemoteIdentifier = pRsvpTeErHopInfo->u4UnnumIf;
                u2HopCount++;
            }

        }
    }

    /*If Complete Strict ERO is not given till Tunnel Destination */
    /*If ERO is strict ERO , Compare last Hop Address with tunnel destination,
     *       If Tunnel destination is not present in ERO,set u2HopType as
     *       OSPF_TE_STRICT_LOOSE_EXP_ROUTE.
     *       In Strict-Loose computation CSPF will compute path from last strict Ip to
     *       Tunnel destination address*/
#if 0
    if (u2HopType == OSPF_TE_STRICT_EXP_ROUTE)
    {
        if ((pTeTnlInfo->u1TnlRerouteFlag == TRUE)
            || (pTeTnlInfo->bIsMbbRequired == TRUE))
        {
            if (pCspfCompInfo->osTeAlternateRoute.aNextHops[u2HopCount - 1].
                u4RouterId != u4DestAddr)
            {
                u2HopType = OSPF_TE_STRICT_LOOSE_EXP_ROUTE;
            }
        }
        else if (pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount - 1].
                 u4RouterId != u4DestAddr)
        {
            u2HopType = OSPF_TE_STRICT_LOOSE_EXP_ROUTE;
        }

    }
#endif

    if ((pTeTnlInfo->u1TnlRerouteFlag == TRUE)
        || (pTeTnlInfo->bIsMbbRequired == TRUE)
        || (RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo) == TRUE))
    {
        pCspfCompInfo->osTeAlternateRoute.u2Info = u2HopType;
        pCspfCompInfo->osTeAlternateRoute.u2HopCount = u2HopCount;
    }
    else
    {
        if ((((pRsvpTeErHopInfo != NULL) &&
              (pRsvpTeErHopInfo->u1HopType == RPTE_ERHOP_LOOSE))
             || (pRsvpTeErHopInfo == NULL))
            && (u2HopType != OSPF_TE_STRICT_EXP_ROUTE))
        {
            if (u2HopType == OSPF_TE_INCLUDE_ANY)
            {
                pCspfCompInfo->osTeExpRoute.u2Info = OSPF_TE_INCLUDE_ANY;
            }
            else
            {
                pCspfCompInfo->osTeExpRoute.u2Info = OSPF_TE_FAILED_LINK;
            }

            if ((pCspfCompInfo->u4TnlDestAddr != pCspfCompInfo->u4DestIpAddr)
                && (pRsvpTeErHopInfo != NULL))
            {
                pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].u4RouterId
                    = pCspfCompInfo->u4TnlDestAddr;
                u2HopCount++;
            }
            if (TE_IN_AR_HOP_LIST_INFO (pTeTnlInfo) != NULL)
            {
                pRROList = &(TE_IN_ARHOP_LIST (pTeTnlInfo));
                if (pRROList != NULL)
                {
                    TMO_SLL_Scan (pRROList, pRsvpTeArHopInfo,
                                  tRsvpTeArHopInfo *)
                    {
                        CONVERT_TO_INTEGER (pRsvpTeArHopInfo->IpAddr.
                                            au1Ipv4Addr, u4HopIpAddr);
                        u4HopIpAddr = OSIX_NTOHL (u4HopIpAddr);
                        if ((u4HopIpAddr != pCspfCompInfo->u4TnlDestAddr)
                            || (pTeTnlInfo->u1TnlPathType !=
                                RPTE_TNL_PROTECTION_PATH))
                        {
                            pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].
                                u4RouterId = u4HopIpAddr;
                            pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].
                                u4RemoteIdentifier =
                                pRsvpTeArHopInfo->u4UnnumIf;
                            u2HopCount++;
                        }
                    }
                }
            }
            TMO_SLL_Scan (pEROList, pRsvpTeErHopInfo, tRsvpTeHopInfo *)
            {
                CONVERT_TO_INTEGER (pRsvpTeErHopInfo->IpAddr.au1Ipv4Addr,
                                    u4HopIpAddr);
                u4HopIpAddr = OSIX_NTOHL (u4HopIpAddr);
                if ((u4HopIpAddr == pCspfCompInfo->u4DestIpAddr) ||
                    (u4HopIpAddr == pCspfCompInfo->u4TnlDestAddr))
                {
                    continue;
                }
                else
                {
                    pCspfCompInfo->osTeExpRoute.aNextHops[u2HopCount].u4RouterId
                        = u4HopIpAddr;
                    u2HopCount++;
                }
            }
        }
        else
        {
            pCspfCompInfo->osTeExpRoute.u2Info = u2HopType;
        }
        pCspfCompInfo->osTeExpRoute.u2HopCount = u2HopCount;
    }

    u2HopCount = RPTE_ZERO;

    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteHhStartEventToL2vpnTmr                             */
/* Description     : This function starts the Refresh Timer for the Rsvpte  */
/*                   Tunnel                                                 */
/* Input (s)       : pRsvpTeGblInfo - Pointer to the RsvpTeGblInfo          */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
extern UINT4        gu4Stups;
UINT1
RpteStartEventToL2vpnTmr (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTimerParam        *pTmrParam = NULL;
    UINT4               u4Duration;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteStartEventToL2vpnTmr : ENTRY\n");

    pTmrParam = (tTimerParam *) & RPTE_CSPF_TIMER_PARAM (pRsvpTeTnlInfo);

    TIMER_PARAM_ID (pTmrParam) = RPTE_EVENT_TO_L2VPN;
    TIMER_PARAM_RPTE_TNL (pTmrParam) = pRsvpTeTnlInfo;
    RSVP_TIMER_NAME (&RPTE_EVT_TO_L2VPN_TIMER (pRsvpTeTnlInfo)) =
        (FS_ULONG) pTmrParam;
    u4Duration = ((gu4Stups * RPTE_EVENT_TO_L2VPN_TIMEOUT) / 1000);
    TmrStopTimer (RSVP_GBL_TIMER_LIST,
                  &RPTE_EVT_TO_L2VPN_TIMER (pRsvpTeTnlInfo));
    if (TmrStartTimer
        (RSVP_GBL_TIMER_LIST, &RPTE_EVT_TO_L2VPN_TIMER (pRsvpTeTnlInfo),
         u4Duration) != TMR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: Starting of TmrStartTimer failed ..\n");
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteStartEventToL2vpnTmr : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteProcessEventToL2vpnTimeOut                         */
/* Description     : This function computes takes for FRR tunnel when the   */
/*                  L2VPN processing is completed                           */
/* Input (s)       : pCtTnlInfo - RSVP Tunnel info.                         */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteProcessEventToL2vpnTimeOut (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteProcessEventToL2vpnTimeOut : ENTRY\n");

    if (TE_TNL_IN_USE_BY_VPN (pRsvpTeTnlInfo->pTeTnlInfo) &
        TE_TNL_INUSE_BY_L2VPN)
    {
        TmrStopTimer (RSVP_GBL_TIMER_LIST,
                      &RPTE_EVT_TO_L2VPN_TIMER (pRsvpTeTnlInfo));
        RpteStartEventToL2vpnTmr (pRsvpTeTnlInfo);
    }
    else
    {
        TmrStopTimer (RSVP_GBL_TIMER_LIST,
                      &RPTE_EVT_TO_L2VPN_TIMER (pRsvpTeTnlInfo));

        RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE, RSVPTE_ZERO,
                                  pRsvpTeTnlInfo, GMPLS_SEGMENT_FORWARD);

        if (RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) == RPTE_TRUE)
        {
            if (RpteIfChangeForProtTnlLinkUp (pRsvpTeTnlInfo) == RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_NBR_DBG,
                            "RpteIfChangeForProtTnlLinkUp : "
                            "FTN Programming FAILED, EXIT \n");
            }
            RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) = RPTE_FALSE;
        }
        else
        {
            if (RpteIfChangeForProtTnlLinkDown (pRsvpTeTnlInfo) == RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_NBR_DBG,
                            "RpteIfChangeForProtTnlLinkDown : FTN Programming"
                            "FAILED, EXIT \n");
            }
            RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) = RPTE_TRUE;
        }

        MEMSET (pRsvpTeTnlInfo->pTeTnlInfo->au1NextHopMac, MPLS_ZERO,
                MAC_ADDR_LEN);
        RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE, RSVPTE_ZERO,
                                  pRsvpTeTnlInfo, GMPLS_SEGMENT_FORWARD);
        rpteTeFrrTnlAppUp (RPTE_TE_TNL (pRsvpTeTnlInfo));
        RPTE_TE_TNL (pRsvpTeTnlInfo)->bFrrDnStrLblChg = RPTE_FALSE;

    }

    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RpteProcessEventToL2vpnTimeOut : EXIT\n");
    return;

}

/****************************************************************************/
/* Function Name   : RpteTEGetReoptTime                                    */
/* Description     : This function get the remaining reoptimize time        */
/* Input (s)       : pTeTnlInfo                                             */
/* Output (s)      : pu4RemainingTime                                       */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RpteTEGetReoptTime (tTeTnlInfo * pTeTnlInfo, UINT4 *pu4RemainingTime)
{
    UINT4               u4TunnelIndex = RSVPTE_ZERO;
    UINT4               u4TunnelInstance = RSVPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RSVPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RSVPTE_ZERO;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;

    if (pTeTnlInfo == NULL)
    {
        return;
    }
    u4TunnelIndex = RSVPTE_TNL_INDEX (pTeTnlInfo);
    u4TunnelInstance = pTeTnlInfo->u4TnlPrimaryInstance;
    CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID (pTeTnlInfo),
                        u4TunnelIngressLSRId);
    u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
    CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID (pTeTnlInfo),
                        u4TunnelEgressLSRId);
    u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);

    if (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                u4TunnelIndex,
                                u4TunnelInstance,
                                u4TunnelIngressLSRId,
                                u4TunnelEgressLSRId,
                                &pRsvpTeTnlInfo) == RPTE_SUCCESS)
    {
        TmrGetRemainingTime (RSVP_GBL_TIMER_LIST,
                             &RPTE_REOPTIMIZE_TIMER (pRsvpTeTnlInfo),
                             pu4RemainingTime);
    }

}

/*---------------------------------------------------------------------------*/
/*                        End of file rpteport.c                             */
/*---------------------------------------------------------------------------*/
