
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: rptedump.c,v 1.15 2014/03/09 13:19:00 siva Exp $
*---------------------------------------------------------------------------*/
/* 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : rptedump.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : RSVP-TE 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    AUTHOR                 : GopiNath
 *    DESCRIPTION            : This file contains routines to handle 
 *                             RSVPTE message dump.
 *---------------------------------------------------------------------------*/

#include "rpteincs.h"

const char          au1ObjHdrDelimiter[RPTE_MAX_DELIM_STR_LEN] =
    "- - - - - - - - - - - - - - - - - - - - - - - - \n";
const char          au1ObjDelimiter[RPTE_MAX_DELIM_STR_LEN] =
    "------------------------------------------------\n";
const char          au1MsgDelimiter[RPTE_MAX_DELIM_STR_LEN] =
    "********************************************************\n";

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpObjHdr
 * Description               : This routine dumps an RSVPTE Object Header 
 *                             contents.
 * Input(s)                  : pObjHdr - Pointer to the Object header.
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpObjHdr (tObjHdr * pObjHdr)
{
    UINT2               u2Val = RSVPTE_ZERO;
    UINT2              *pu2TempAddr = &u2Val;

    if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_HDR)
        == RSVPTE_DUMP_LVL_HDR)
    {
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjHdrDelimiter);
        pu2TempAddr = &(pObjHdr->u2ClassNumType);

        RSVPTE_DBG3 (RSVPTE_DBG_FLAG,
                     "ClassNum - CType - Length    : %d - %d - %d\n",
                     (*(UINT1 *) pu2TempAddr),
                     (*((UINT1 *) pu2TempAddr + RSVPTE_ONE)),
                     OSIX_NTOHS (pObjHdr->u2Length));
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjHdrDelimiter);
    }
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteMsgObjDump
 * Description               : This routine dumps RSVPTE Object contents.
 * Input(s)                  : pMsg     - Pointer to the Rsvp Packet.
 *                             u4MsgLen - Rsvpte message length.
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None  
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteMsgObjDump (UINT1 *pMsg, UINT4 u4MsgLen, UINT1 u1LblType)
{
    UINT1               u1Index = RSVPTE_ZERO;
    UINT1               u1Psc = RSVPTE_ZERO;
    UINT1               u1MsgIdFlag = RSVPTE_ZERO;
    UINT1              *pEndOfPkt = NULL;
    UINT1              *pPduPtr = NULL;
    UINT1              *pLblObjEnd = NULL;
    UINT2               u2Temp = RSVPTE_ZERO;
    UINT2               u2PhbId = RSVPTE_ZERO;
    tObjHdr            *pObjHdr = NULL;
    UINT4               u4Val = RSVPTE_ZERO;
    UINT4               u4Index = RSVPTE_ZERO;
    UINT4               u4MsgIdsLen = RSVPTE_ZERO;
    UINT4               u4MsgIdCont = RSVPTE_ZERO;
    UINT4               u4Epoch = RSVPTE_ZERO;
    UINT4               u4MsgId = RSVPTE_ZERO;
    UINT4               u4MsgLstEpoch = RSVPTE_ZERO;
    UINT4              *pTemp = &u4Val;
    UINT4              *pu4TempAddr = &u4Val;
    UINT1               u1Val = RSVPTE_ZERO;
    tMsgIdListObj      *pMsgIdListObj = NULL;
    tRASsnAttr          RASsnAttr;

    pObjHdr = (tObjHdr *) (VOID *) (((UINT1 *) pMsg) + sizeof (tRsvpHdr));

    pEndOfPkt = pMsg + u4MsgLen;
    while (pObjHdr < ((tObjHdr *) (VOID *) pEndOfPkt))
    {
        if (VALIDATE_OBJHDR (pObjHdr, pEndOfPkt))
        {
            RSVPTE_DBG (RSVPTE_PVM_PRCS, "Validate obj hdr failed \n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RpteMsgObjDump : INTMD-EXIT \n");
            return;
        }
        switch (OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)))
        {

            case IPV4_NULL_CLASS_NUM_TYPE:
                RSVPTE_DBG (RSVPTE_PVM_PRCS, "NULL Object\n");
                /* Ignore */
                break;

            case IPV4_SESSION_CLASS_NUM_TYPE:
                /* SESSION Object */
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "Rsvp Session Object recvd \n");
                break;

            case IPV4_RSVP_HOP_CLASS_NUM_TYPE:
                /* RSVP_HOP Object */
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "RSVP Hop Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) (VOID *) pObjHdr);
                    pu4TempAddr = &(RSVP_HOP_ADDR
                                    (&RSVP_HOP_OBJ
                                     ((tRsvpHopObj *) (VOID *) pObjHdr)));
                    RSVPTE_DBG4 (RSVPTE_DBG_FLAG,
                                 "Dest Addr                    : "
                                 "%d.%d.%d.%d \n", (*(UINT1 *) pu4TempAddr),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_ONE)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_TWO)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_THREE)));
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Lih                          : 0x%x\n",
                                 RSVP_HOP_LIH (&RSVP_HOP_OBJ
                                               ((tRsvpHopObj *) (VOID *)
                                                pObjHdr)));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case IPV4_TIME_VALUES_CLASS_NUM_TYPE:
                /* TIME_VALUES Object */
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Time Values Object Contents:\n");
                    RpteDumpObjHdr ((tObjHdr *) (VOID *) pObjHdr);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Refresh Period               : %d\n",
                                 OSIX_NTOHL (TIME_VALUES_PERIOD
                                             (&TIME_VALUES_OBJ
                                              ((tTimeValuesObj *) (VOID *)
                                               pObjHdr))));
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case IPV4_ERROR_SPEC_CLASS_NUM_TYPE:
                if (((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MIN)
                     == RSVPTE_DUMP_LVL_MIN) ||
                    ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                     == RSVPTE_DUMP_LVL_MAX))
                {
                    /* ERROR_SPEC Object */
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Error Spec Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    pu4TempAddr = &(ERROR_SPEC_ADDR
                                    (&ERROR_SPEC_OBJ
                                     ((tErrorSpecObj *) (VOID *) pObjHdr)));
                    RSVPTE_DBG4 (RSVPTE_DBG_FLAG,
                                 "Dest Addr                    : "
                                 "%d.%d.%d.%d \n",
                                 (*(UINT1 *) pu4TempAddr),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_ONE)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_TWO)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_THREE)));
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Error Spec Flags             : 0x%x\n",
                                 ERROR_SPEC_FLAGS (&ERROR_SPEC_OBJ
                                                   ((tErrorSpecObj *) (VOID *)
                                                    pObjHdr)));
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Error Spec Code              : %d\n",
                                 ERROR_SPEC_CODE (&ERROR_SPEC_OBJ
                                                  ((tErrorSpecObj *) (VOID *)
                                                   pObjHdr)));
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Error Spec Value             : %d\n",
                                 ERROR_SPEC_VALUE (&ERROR_SPEC_OBJ
                                                   ((tErrorSpecObj *) (VOID *)
                                                    pObjHdr)));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case IPV4_SCOPE_CLASS_NUM_TYPE:
                /* SCOPE Object */
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Scope Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    pu4TempAddr =
                        (UINT4 *) (VOID *) ((UINT1 *) pObjHdr +
                                            sizeof (tObjHdr));

                    RSVPTE_DBG4 (RSVPTE_DBG_FLAG,
                                 "Dest Addr                    : "
                                 "%d.%d.%d.%d \n",
                                 (*(UINT1 *) pu4TempAddr),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_ONE)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_TWO)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_THREE)));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case IPV4_STYLE_CLASS_NUM_TYPE:
                if (((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MIN)
                     == RSVPTE_DUMP_LVL_MIN) ||
                    ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                     == RSVPTE_DUMP_LVL_MAX))
                {
                    /* STYLE Object */
                    RpteDumpStyleObj ((tObjHdr *) pObjHdr);
                }
                break;

            case IPV4_FLOW_SPEC_CLASS_NUM_TYPE:
                /* FLOW_SPEC Object */
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RpteDumpFlowSpecObj ((tObjHdr *) pObjHdr);
                }
                break;

            case IPV4_SENDER_TEMPLATE_CLASS_NUM_TYPE:
                /* SENDER_TEMPLATE Object */
                RSVPTE_DBG (RSVPTE_DBG_FLAG,
                            "Rsvp Sender Template Object recvd \n  ");
                break;

            case IPV4_SENDER_TSPEC_CLASS_NUM_TYPE:
                /* SENDER_TSPEC Object */
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RpteDumpSenderTSpecObj ((tObjHdr *) pObjHdr);
                }
                break;

            case IPV4_ADSPEC_CLASS_NUM_TYPE:
                /* ASPEC Object */
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RpteDumpAdSpecObj ((tObjHdr *) pObjHdr);
                }
                break;

            case IPV4_POLICY_DATA_CLASS_NUM_TYPE:
                /* POLICY_DATA Object */
                /* Not Supported, Ignore */
                break;

            case IPV4_RESV_CONFIRM_CLASS_NUM_TYPE:
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    pPduPtr =
                        ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Resv Conf Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    RPTE_EXT_4_BYTES (pPduPtr, *((UINT4 *) pTemp));
                    RSVPTE_DBG4 (RSVPTE_DBG_FLAG,
                                 "Recev Addr                   : "
                                 "%d.%d.%d.%d \n",
                                 (*((UINT1 *) pTemp + RSVPTE_THREE)),
                                 (*((UINT1 *) pTemp + RSVPTE_TWO)),
                                 (*((UINT1 *) pTemp + RSVPTE_ONE)),
                                 (*((UINT1 *) pTemp)));
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case RPTE_GEN_LBL_CLASS_NUM_TYPE:
            case RPTE_LBL_CLASS_NUM_TYPE:
                if (((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MIN)
                     == RSVPTE_DUMP_LVL_MIN) ||
                    ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                     == RSVPTE_DUMP_LVL_MAX))
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Label Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    pLblObjEnd = (UINT1 *) ((UINT1 *) pObjHdr
                                            +
                                            OSIX_NTOHS (OBJ_HDR_LENGTH
                                                        (pObjHdr)));
                    pPduPtr =
                        ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));

                    while ((UINT1 *) pPduPtr < pLblObjEnd)
                    {
                        if (u1LblType == RPTE_ATM)
                        {
                            RPTE_EXT_2_BYTES (pPduPtr, u2Temp);
                            RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                         "VPI                          : %d\n",
                                         u2Temp);

                            RPTE_EXT_2_BYTES (pPduPtr, u2Temp);
                            RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                         "VCI                          : %d\n",
                                         u2Temp);

                        }
                        else if (u1LblType == RPTE_ETHERNET)
                        {
                            RPTE_EXT_2_BYTES (pPduPtr, u2Temp);
                            RPTE_EXT_2_BYTES (pPduPtr, u2Temp);

                            RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                         "Lbl Value                    : %d\n",
                                         u2Temp);
                        }

                    }
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case RPTE_LBL_REQ_CLASS_NUM_TYPE:
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {

                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Label Req Object Contents:, \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    switch (OSIX_NTOHS ((&((tGenLblReqObj *) pObjHdr)->
                                         GenLblReq)->u2GPid))
                    {
                        case RPTE_STD_IPV4_ETHERTYPE:
                            RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                        "L3Pid                        "
                                        ": Ipv4\n");
                            break;

                        default:
                            RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                         "L3Pid                        "
                                         ": %d\n",
                                         OSIX_NTOHS ((&((tGenLblReqObj *)
                                                        pObjHdr)->GenLblReq)->
                                                     u2GPid));
                            break;
                    }
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case RPTE_LBL_REQ_ATM_RNG_CLASS_NUM_TYPE:
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Lbl Req Atm Rng Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);

                    switch (OSIX_NTOHS ((&((tLblReqAtmRngObj *) pObjHdr)->
                                         GenLblReq)->u2GPid))
                    {
                        case RPTE_STD_IPV4_ETHERTYPE:
                            RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                        "L3Pid                        "
                                        ": Ipv4\n");
                            break;

                        default:
                            RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                         "L3Pid                        : %d\n",
                                         OSIX_NTOHS ((&((tLblReqAtmRngObj *)
                                                        pObjHdr)->GenLblReq)->
                                                     u2GPid));
                            break;
                    }
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "LB VPI                       : %d\n",
                                 OSIX_NTOHS (RPTE_LBL_REQ_ATM_LBL_LBVPI
                                             ((tLblReqAtmRngObj *) pObjHdr)));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "LB VCI                       : %d \n",
                                 (RPTE_LBL_REQ_ATM_LBL_LBVCI
                                  ((tLblReqAtmRngObj *) pObjHdr)));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "UB VPI                       : %d \n",
                                 OSIX_NTOHS (RPTE_LBL_REQ_ATM_LBL_UBVPI
                                             ((tLblReqAtmRngObj *) pObjHdr)));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "UB VCI                       : %d \n\n",
                                 OSIX_NTOHS (RPTE_LBL_REQ_ATM_LBL_UBVCI
                                             ((tLblReqAtmRngObj *) pObjHdr)));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;
            case RPTE_HELLO_REQ_CLASS_NUM_TYPE:
                if (((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MIN)
                     == RSVPTE_DUMP_LVL_MIN) ||
                    ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                     == RSVPTE_DUMP_LVL_MAX))
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Hello Req Object: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Src Inst                     : %d \n",
                                 OSIX_NTOHL (HELLO_SRC_INS
                                             (&HELLO_OBJ
                                              ((tHelloObj *) (VOID *)
                                               pObjHdr))));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Dst Inst                     : %d \n\n",
                                 OSIX_NTOHL (HELLO_DST_INS
                                             (&HELLO_OBJ
                                              ((tHelloObj *) (VOID *)
                                               pObjHdr))));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case RPTE_HELLO_ACK_CLASS_NUM_TYPE:
                if (((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MIN)
                     == RSVPTE_DUMP_LVL_MIN) ||
                    ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                     == RSVPTE_DUMP_LVL_MAX))
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Hello Ack Object: \n");
                    RpteDumpObjHdr ((tObjHdr *) (VOID *) pObjHdr);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Src Inst                     : %d \n",
                                 OSIX_NTOHL (HELLO_SRC_INS
                                             (&HELLO_OBJ
                                              ((tHelloObj *) (VOID *)
                                               pObjHdr))));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Dst Inst                     : %d \n\n",
                                 OSIX_NTOHL (HELLO_DST_INS
                                             (&HELLO_OBJ
                                              ((tHelloObj *) (VOID *)
                                               pObjHdr))));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case RPTE_ERO_CLASS_NUM_TYPE:
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RpteDumpEroObj ((tObjHdr *) pObjHdr);
                }
                break;

            case RPTE_RRO_CLASS_NUM_TYPE:
                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RpteDumpRroObj ((tObjHdr *) pObjHdr);
                }
                break;

            case IPV4_RPTE_SSN_CLASS_NUM_TYPE:
                if (((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MIN)
                     == RSVPTE_DUMP_LVL_MIN) ||
                    ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                     == RSVPTE_DUMP_LVL_MAX))
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "RsvpTe Session Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);

                    pu4TempAddr = (UINT4 *) (VOID *) &(RPTE_SSN_TNL_DEST_ADDR
                                                       (&(RPTE_SSN_OBJ
                                                          ((tRsvpTeSsnObj *)
                                                           pObjHdr))));

                    RSVPTE_DBG4 (RSVPTE_DBG_FLAG,
                                 "Dest Addr                    : "
                                 "%d.%d.%d.%d \n",
                                 (*(UINT1 *) pu4TempAddr),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_ONE)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_TWO)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_THREE)));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Tnl Id                       : %d \n",
                                 OSIX_NTOHS (RPTE_SSN_TNL_ID (&RPTE_SSN_OBJ
                                                              ((tRsvpTeSsnObj *)
                                                               pObjHdr))));

                    pu4TempAddr = (UINT4 *) (VOID *) &(RPTE_SSN_EXTN_TNL_ID
                                                       (&RPTE_SSN_OBJ
                                                        ((tRsvpTeSsnObj *)
                                                         pObjHdr)));
                    RSVPTE_DBG4 (RSVPTE_DBG_FLAG,
                                 "Extn TnlId                   : "
                                 "%d.%d.%d.%d \n", (*(UINT1 *) pu4TempAddr),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_ONE)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_TWO)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_THREE)));
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE:
                if (((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MIN)
                     == RSVPTE_DUMP_LVL_MIN) ||
                    ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                     == RSVPTE_DUMP_LVL_MAX))
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Rsvpte Sender Temp Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);

                    pu4TempAddr = (UINT4 *) (VOID *) &(RPTE_SNDR_TNL_SNDR_ADDR
                                                       (&RPTE_SNDR_TEMP_OBJ
                                                        ((tRsvpTeSndrTmpObj *)
                                                         pObjHdr)));
                    RSVPTE_DBG4 (RSVPTE_DBG_FLAG,
                                 "Sndr Addr                    : "
                                 "%d.%d.%d.%d \n",
                                 (*(UINT1 *) pu4TempAddr),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_ONE)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_TWO)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_THREE)));
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Lsp Id                       : %d \n",
                                 OSIX_NTOHS (RPTE_SNDR_TNL_LSPID
                                             (&RPTE_SNDR_TEMP_OBJ
                                              ((tRsvpTeSndrTmpObj *)
                                               pObjHdr))));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE:
                if (((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MIN)
                     == RSVPTE_DUMP_LVL_MIN) ||
                    ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                     == RSVPTE_DUMP_LVL_MAX))
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Rsvpte Filter Spec Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);

                    pu4TempAddr = (UINT4 *) (VOID *) &(RPTE_FILTER_TNL_SNDR_ADDR
                                                       (&RPTE_FILTER_SPEC_OBJ
                                                        ((tRsvpTeFilterSpecObj
                                                          *) pObjHdr)));
                    RSVPTE_DBG4 (RSVPTE_DBG_FLAG,
                                 "Sndr Addr                    : "
                                 "%d.%d.%d.%d \n", (*(UINT1 *) pu4TempAddr),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_ONE)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_TWO)),
                                 (*((UINT1 *) pu4TempAddr + RSVPTE_THREE)));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Lsp Id                       : %d \n",
                                 OSIX_NTOHS (RPTE_FILTER_TNL_LSPID
                                             (&RPTE_FILTER_SPEC_OBJ
                                              ((tRsvpTeFilterSpecObj *)
                                               pObjHdr))));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case RPTE_SSN_ATTR_CLASS_NUM_TYPE:
                if (((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MIN)
                     == RSVPTE_DUMP_LVL_MIN) ||
                    ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                     == RSVPTE_DUMP_LVL_MAX))
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Session Attr Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Set Prio                     : %d\n",
                                 ((tSsnAttrObj *) pObjHdr)->SsnAttr.u1SetPrio);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Hold Prio                    : %d\n",
                                 ((tSsnAttrObj *) pObjHdr)->SsnAttr.u1HoldPrio);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Flags                        : 0x%x\n",
                                 ((tSsnAttrObj *) pObjHdr)->SsnAttr.u1Flags);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Name Len                     : %d\n",
                                 ((tSsnAttrObj *) pObjHdr)->SsnAttr.u1NameLen);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Ssn Name                     : %s\n",
                                 ((tSsnAttrObj *) pObjHdr)->SsnAttr.au1SsnName);
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case RPTE_RA_SSN_ATTR_CLASS_NUM_TYPE:
                if (((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MIN)
                     == RSVPTE_DUMP_LVL_MIN) ||
                    ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                     == RSVPTE_DUMP_LVL_MAX))
                {

                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "RA Ssn Attr Object Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    RASsnAttr = ((tRASsnAttrObj *) (VOID *) pObjHdr)->RASsnAttr;
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Set Prio                     : %d\n",
                                 RASsnAttr.SsnAttr.u1SetPrio);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Hold Prio                    : %d\n",
                                 RASsnAttr.SsnAttr.u1HoldPrio);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Ssn Flags                    : 0x%x\n",
                                 RASsnAttr.SsnAttr.u1Flags);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Name Len                     : %d\n",
                                 RASsnAttr.SsnAttr.u1NameLen);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Ssn Name                     : %s\n",
                                 RASsnAttr.SsnAttr.au1SsnName);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "RA Param ExcAny              : 0x%x\n",
                                 OSIX_NTOHL (RASsnAttr.RAParam.u4ExAnyAttr));
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "RA Param IncAny              : 0x%x\n",
                                 OSIX_NTOHL (RASsnAttr.RAParam.u4IncAnyAttr));
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "RA Param IncAll              : 0x%x\n",
                                 OSIX_NTOHL (RASsnAttr.RAParam.u4IncAllAttr));
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case RPTE_CLASS_TYPE_OBJ_CLASS_NUM_TYPE:

                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    pPduPtr =
                        ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Class Type Obj Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    RPTE_EXT_1_BYTE (pPduPtr, *((UINT1 *) pTemp));
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Class Type                   : %x\n",
                                 (*(UINT1 *) pTemp));
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case RPTE_ELSP_TP_OBJ_CLASS_NUM_TYPE:

                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RpteDumpElspTpObj ((tObjHdr *) pObjHdr);
                }
                break;

            case RPTE_DS_LLSP_OBJ_CLASS_NUM_TYPE:

                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    pPduPtr =
                        ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Llsp Obj Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    RPTE_EXT_2_BYTES (pPduPtr, *(pTemp));
                    RPTE_EXT_2_BYTES (pPduPtr, *(pTemp));
                    u2PhbId = (UINT2) (*pTemp);
                    RPTE_DS_DSCP_FOR_PHBID (u2PhbId, u1Psc);
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "PSC value                    : %d\n", u1Psc);
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;

            case RPTE_DS_ELSP_OBJ_CLASS_NUM_TYPE:

                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    u1Index = RSVPTE_ZERO;
                    pPduPtr =
                        ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Elsp Obj Contents: \n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                    RPTE_EXT_2_BYTES (pPduPtr, *(pTemp));
                    RPTE_EXT_1_BYTE (pPduPtr, *((UINT1 *) pTemp));
                    RPTE_EXT_1_BYTE (pPduPtr, *((UINT1 *) pTemp));
                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "No of Map Entries            : %d\n",
                                 (*(UINT1 *) pTemp));
                    u1Val = *((UINT1 *) pTemp);
                    for (u1Index = RSVPTE_ZERO; u1Index < u1Val; u1Index++)
                    {
                        RPTE_EXT_1_BYTE (pPduPtr, *((UINT1 *) pTemp));
                        RPTE_EXT_1_BYTE (pPduPtr, *((UINT1 *) pTemp));
                        RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                     "Exp value                    : %d\n",
                                     (*(UINT1 *) pTemp));
                        RPTE_EXT_2_BYTES (pPduPtr, *(pTemp));
                        u2PhbId = (UINT2) (*pTemp);
                        RPTE_DS_DSCP_FOR_PHBID (u2PhbId, u1Psc);
                        RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                     "Phb Id                       : %d\n",
                                     u1Psc);
                    }
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;
            case RPTE_MESSAGE_ID_CLASS_NUM_TYPE:

                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Message Id Object Contents:\n");

                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);

                    pPduPtr = (UINT1 *) ((UINT1 *) pObjHdr + sizeof (tObjHdr));

                    RPTE_EXT_4_BYTES (pPduPtr, *((UINT4 *) pTemp));

                    u4MsgIdCont = *((UINT4 *) pTemp);

                    u1MsgIdFlag = (UINT1) ((u4MsgIdCont &
                                            RPTE_WORD_UPPER_1_BYTE_MASK)
                                           >> RPTE_3_BYTE_SHIFT);

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "MsgIdFlag                    : %d\n",
                                 u1MsgIdFlag);

                    u4Epoch = (u4MsgIdCont & RPTE_WORD_LOWER_3_BYTE_MASK);

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Epoch                        : %d\n",
                                 u4Epoch);

                    RPTE_EXT_4_BYTES (pPduPtr, *((UINT4 *) pTemp));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "MsgId                        : %d\n",
                                 *((UINT4 *) pTemp));

                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);

                }
                break;
            case RPTE_MESSAGE_ID_ACK_CLASS_NUM_TYPE:

                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Message Id Ack Object Contents:\n");

                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);

                    pPduPtr = (UINT1 *) ((UINT1 *) pObjHdr + sizeof (tObjHdr));

                    RPTE_EXT_4_BYTES (pPduPtr, *((UINT4 *) pTemp));

                    u4Epoch = *((UINT4 *) pTemp);
                    u4Epoch = (u4Epoch & RPTE_WORD_LOWER_3_BYTE_MASK);

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Epoch                        : %d\n",
                                 u4Epoch);
                    RPTE_EXT_4_BYTES (pPduPtr, *((UINT4 *) pTemp));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "MsgId                        : %d\n",
                                 *((UINT4 *) pTemp));
                }
                break;

            case RPTE_MESSAGE_ID_NACK_CLASS_NUM_TYPE:

                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Message Id Nack Object Contents:\n");

                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);

                    pPduPtr = (UINT1 *) ((UINT1 *) pObjHdr + sizeof (tObjHdr));

                    RPTE_EXT_4_BYTES (pPduPtr, *((UINT4 *) pTemp));

                    u4Epoch = *((UINT4 *) pTemp);
                    u4Epoch = (u4Epoch & RPTE_WORD_LOWER_3_BYTE_MASK);

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Epoch                        : %d\n",
                                 u4Epoch);
                    RPTE_EXT_4_BYTES (pPduPtr, *((UINT4 *) pTemp));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "MsgId                        : %d\n",
                                 *((UINT4 *) pTemp));
                }
                break;

            case RPTE_MESSAGE_ID_LIST_CLASS_NUM_TYPE:

                if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_MAX)
                    == RSVPTE_DUMP_LVL_MAX)
                {
                    RSVPTE_DBG (RSVPTE_DBG_FLAG,
                                "Message Id List Object Contents:\n");
                    RpteDumpObjHdr ((tObjHdr *) pObjHdr);

                    pMsgIdListObj =
                        (tMsgIdListObj *) (VOID *) ((UINT1 *) pObjHdr);
                    u4MsgIdsLen =
                        OSIX_NTOHS (OBJ_HDR_LENGTH
                                    (&RPTE_MSG_ID_LIST_OBJ_HDR
                                     (pMsgIdListObj)));
                    u4MsgIdsLen =
                        u4MsgIdsLen - RSVP_OBJ_HDR_LEN - RPTE_FLAG_EPOCH_LEN;
                    u4MsgLstEpoch =
                        OSIX_NTOHL (RPTE_MSG_ID_LIST_EPOCH (pMsgIdListObj));

                    RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                 "Epoch                        : %d\n",
                                 u4MsgLstEpoch);

                    for (u4Index = RPTE_ZERO; (u4Index <
                                               u4MsgIdsLen / RPTE_MSG_ID_LEN);
                         u4Index += RPTE_ONE)
                    {
                        u4MsgId = OSIX_NTOHL (RPTE_MSG_ID_LIST_MSG_ID
                                              (pMsgIdListObj, u4Index));

                        RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                                     "MsgId                        : %d\n",
                                     u4MsgId);
                    }
                    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                }
                break;
            default:
                RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                             "\nRcvd Unknown Obj of Class Num Type 0x%x\n",
                             OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)));
                RpteDumpObjHdr ((tObjHdr *) pObjHdr);
                break;
        }                        /*switch-case */
        pObjHdr = NEXT_OBJ (pObjHdr);
    }                            /* while */
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpIncomRpteMsg
 * Description               : This routine dumps incoming RSVPTE message. 
 * Input(s)                  : pPktMap - Pointer to the Packet map structure.
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpIncomRpteMsg (const tPktMap * pPktMap)
{
    UINT1               u1MsgDumpReq = RPTE_FALSE;
    UINT4               u4MsgDumpDir = RSVPTE_ONE;
    tRsvpHdr           *pRsvpHdr = NULL;

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);

    if (RSVP_HDR_MSG_TYPE (pRsvpHdr) == BUNDLE_MSG)
    {
        RpteDumpInRpteBundleMsg (pPktMap);
        return;
    }

    switch (RSVP_HDR_MSG_TYPE (pRsvpHdr))
    {
        case PATH_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_PATH) ==
                 RSVPTE_DUMP_PATH)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_PATH_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tPATH MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case RESV_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_RESV) ==
                 RSVPTE_DUMP_RESV)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_RESV_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tRESV MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case PATHERR_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_PERR) ==
                 RSVPTE_DUMP_PERR)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_PATHERR_MSG_BIT)))

            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tPATH-ERR MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case RESVERR_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_RERR) ==
                 RSVPTE_DUMP_RERR)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_RESVERR_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tRESV-ERR MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case PATHTEAR_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_PTEAR) ==
                 RSVPTE_DUMP_PTEAR)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_PATHTEAR_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tPATH-TEAR MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case RESVTEAR_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_RTEAR) ==
                 RSVPTE_DUMP_RTEAR)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_RESVTEAR_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tRESV-TEAR MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case RESVCONF_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_RCNFM) ==
                 RSVPTE_DUMP_RCNFM)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_RESVCONF_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tRESV CONF MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case HELLO_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_HELLO) ==
                 RSVPTE_DUMP_HELLO)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_HELLO_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tHELLO MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;
        case BUNDLE_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_BUNDLE) ==
                 RSVPTE_DUMP_BUNDLE)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_BUNDLE_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tBUNDLE_MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            return;

        case ACK_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_ACK) ==
                 RSVPTE_DUMP_ACK)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_ACK_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tACK MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case SREFRESH_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_SREFRESH) ==
                 RSVPTE_DUMP_SREFRESH)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_SREFRESH_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tSREFRESH MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case NOTIFY_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_NOTIFY) ==
                 RSVPTE_DUMP_NOTIFY)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_NOTIFY_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tNOTIFY MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case RECOVERY_PATH_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) &
                  RSVPTE_DUMP_RECOVERY_PATH) == RSVPTE_DUMP_RECOVERY_PATH)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_RECOVERY_PATH_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tRECOVERY PATH MSG RCVD\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        default:
            return;
    }                            /* End of Switch */
    if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_HDR)
        == RSVPTE_DUMP_LVL_HDR)
    {
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Msg Type                     : %d\n",
             RSVP_HDR_MSG_TYPE (pRsvpHdr));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "RR Bit                       : 0x%x\n",
             RSVP_IS_RR_BIT_SET (RSVP_HDR_VER_FLAG (pRsvpHdr)));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Version Flag                 : 0x%x\n",
             (RSVP_HDR_VER_FLAG (pRsvpHdr)));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Checksum Value               : 0x%x\n",
             OSIX_NTOHS (RSVP_HDR_CHECK_SUM (pRsvpHdr)));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Msg Len                      : %d\n",
             OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr)));
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
    }
    if (u1MsgDumpReq == RPTE_TRUE)
    {
        RpteMsgObjDump ((UINT1 *) (PKT_MAP_RSVP_PKT (pPktMap)),
                        OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr)),
                        RPTE_IF_LBL_TYPE (PKT_MAP_IF_ENTRY (pPktMap)));
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
        RSVPTE_DBG (RSVPTE_DBG_FLAG, "\n");
    }
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpOutRpteBundleMsg
 * Description               : This routine dumps an outgoing RSVPTE
 *                             Bundle Message contents.
 * Input(s)                  : pPktMap - Pointer to the PktMap that contains
 *                                       the message buf.
 * Output(s)                 : None
 * Global Variables Referred : None
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpOutRpteBundleMsg (const tPktMap * pPktMap)
{
    tPktMap             PktMap;
    tRsvpHdr           *pRsvpHdr = NULL;
    UINT1              *pu1EndOfPkt = NULL;

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);

    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
    RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tBUNDLE MSG SENT\n");
    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);

    if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_HDR)
        == RSVPTE_DUMP_LVL_HDR)
    {
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Msg Type                     : %d\n",
             RSVP_HDR_MSG_TYPE (pRsvpHdr));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "RR Bit                       : 0x%x\n",
             RSVP_IS_RR_BIT_SET (RSVP_HDR_VER_FLAG (pRsvpHdr)));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Version Flag                 : 0x%x\n",
             RSVP_HDR_VER_FLAG (pRsvpHdr));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Checksum Value               : 0x%x\n",
             RSVP_HDR_CHECK_SUM (pRsvpHdr));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Msg Len                      : %d\n",
             OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr)));
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
    }

    pu1EndOfPkt = (UINT1 *) pRsvpHdr + OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr));

    pRsvpHdr = (tRsvpHdr *) (VOID *) ((UINT1 *) pRsvpHdr + sizeof (tRsvpHdr));
    PKT_MAP_OUT_IF_ENTRY (&PktMap) = PKT_MAP_OUT_IF_ENTRY (pPktMap);

    while (pRsvpHdr < (tRsvpHdr *) (VOID *) pu1EndOfPkt)
    {
        PKT_MAP_RSVP_PKT (&PktMap) = (UINT1 *) pRsvpHdr;
        RpteDumpOutRpteMsg (&PktMap);
        pRsvpHdr = (tRsvpHdr *) (VOID *) ((UINT1 *) pRsvpHdr +
                                          OSIX_NTOHS (RSVP_HDR_LENGTH
                                                      (pRsvpHdr)));
    }
    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpInRpteBundleMsg
 * Description               : This routine dumps an incomming RSVPTE
 *                             Bundle Message contents.
 * Input(s)                  : pPktMap - Pointer to the PktMap that contains
 *                                       the message buf.
 * Output(s)                 : None
 * Global Variables Referred : None
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpInRpteBundleMsg (const tPktMap * pPktMap)
{
    tPktMap             PktMap;
    tRsvpHdr           *pRsvpHdr = NULL;
    UINT1              *pu1EndOfPkt = NULL;

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
    RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tBUNDLE MSG RCVD\n");
    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);

    if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_HDR) ==
        RSVPTE_DUMP_LVL_HDR)
    {
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Msg Type                     : %d\n",
             RSVP_HDR_MSG_TYPE (pRsvpHdr));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "RR Bit                       : 0x%x\n",
             RSVP_IS_RR_BIT_SET (RSVP_HDR_VER_FLAG (pRsvpHdr)));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Version Flag                 : 0x%x\n",
             RSVP_HDR_VER_FLAG (pRsvpHdr));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Checksum Value               : 0x%x\n",
             RSVP_HDR_CHECK_SUM (pRsvpHdr));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Msg Len                      : %d\n",
             OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr)));
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
    }

    pu1EndOfPkt = (UINT1 *) pRsvpHdr + OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr));

    pRsvpHdr = (tRsvpHdr *) (VOID *) ((UINT1 *) pRsvpHdr + sizeof (tRsvpHdr));
    PKT_MAP_IF_ENTRY (&PktMap) = PKT_MAP_IF_ENTRY (pPktMap);

    while (pRsvpHdr < (tRsvpHdr *) (VOID *) pu1EndOfPkt)
    {
        PKT_MAP_RSVP_PKT (&PktMap) = (UINT1 *) pRsvpHdr;
        RpteDumpIncomRpteMsg (&PktMap);
        pRsvpHdr = (tRsvpHdr *) (VOID *) ((UINT1 *) pRsvpHdr +
                                          OSIX_NTOHS (RSVP_HDR_LENGTH
                                                      (pRsvpHdr)));
    }
    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpOutRpteMsg
 * Description               : This routine dumps a outgoing RSVPTE Message 
 *                             contents.
 * Input(s)                  : pPktMap - Pointer to the MsgBuf. 
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpOutRpteMsg (const tPktMap * pPktMap)
{
    tRsvpHdr           *pRsvpHdr = NULL;
    UINT1               u1MsgDumpReq = RPTE_FALSE;
    UINT4               u4MsgDumpDir = RPTE_TWO;

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);

    if (RSVP_HDR_MSG_TYPE (pRsvpHdr) == BUNDLE_MSG)
    {
        RpteDumpOutRpteBundleMsg (pPktMap);
        return;
    }

    switch (RSVP_HDR_MSG_TYPE (pRsvpHdr))
    {
        case PATH_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_PATH) ==
                 RSVPTE_DUMP_PATH)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_PATH_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tPATH MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case RESV_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_RESV) ==
                 RSVPTE_DUMP_RESV)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_RESV_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tRESV MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case PATHERR_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_PERR) ==
                 RSVPTE_DUMP_PERR)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_PATHERR_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tPATH-ERR MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case RESVERR_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_RERR) ==
                 RSVPTE_DUMP_RERR)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_RESVERR_MSG_BIT)))

            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tRESV-ERR MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case PATHTEAR_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_PTEAR) ==
                 RSVPTE_DUMP_PTEAR)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_PATHERR_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tPATH-TEAR MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case RESVTEAR_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_RTEAR) ==
                 RSVPTE_DUMP_RTEAR)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_RESVTEAR_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tRESV-TEAR MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case RESVCONF_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_RCNFM) ==
                 RSVPTE_DUMP_RCNFM)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_RESVCONF_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tRESV-CONF MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case HELLO_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_HELLO) ==
                 RSVPTE_DUMP_HELLO)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_HELLO_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tHELLO MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case BUNDLE_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_BUNDLE) ==
                 RSVPTE_DUMP_BUNDLE)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_BUNDLE_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tBUNDLE MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case ACK_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_ACK) ==
                 RSVPTE_DUMP_ACK)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_ACK_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tACK MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case SREFRESH_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_SREFRESH) ==
                 RSVPTE_DUMP_SREFRESH)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_SREFRESH_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tSREFRESH MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case NOTIFY_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) & RSVPTE_DUMP_NOTIFY) ==
                 RSVPTE_DUMP_NOTIFY)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_NOTIFY_MSG_BIT)))

            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tPATH-ERR MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        case RECOVERY_PATH_MSG:
            if (((RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) &
                  RSVPTE_DUMP_RECOVERY_PATH) == RSVPTE_DUMP_RECOVERY_PATH)
                && 0 !=
                (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) &
                 (u4MsgDumpDir << DEBUG_RECOVERY_PATH_MSG_BIT)))
            {
                u1MsgDumpReq = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, "\t\tRECOVERY PATH MSG SENT\n");
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
            }
            break;

        default:
            return;
    }
    if ((RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) & RSVPTE_DUMP_LVL_HDR)
        == RSVPTE_DUMP_LVL_HDR)
    {
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Msg Type                     : %d\n",
             RSVP_HDR_MSG_TYPE (pRsvpHdr));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "RR Bit                       : 0x%x\n",
             RSVP_IS_RR_BIT_SET (RSVP_HDR_VER_FLAG (pRsvpHdr)));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Version Flag                 : 0x%x\n",
             RSVP_HDR_VER_FLAG (pRsvpHdr));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Checksum Value               : 0x%x\n",
             RSVP_HDR_CHECK_SUM (pRsvpHdr));
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Msg Len                      : %d\n",
             OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr)));
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
    }
    if (u1MsgDumpReq == RPTE_TRUE)
    {
        RpteMsgObjDump ((UINT1 *) (PKT_MAP_RSVP_PKT (pPktMap)),
                        OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr)),
                        RPTE_IF_LBL_TYPE (PKT_MAP_OUT_IF_ENTRY (pPktMap)));
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1MsgDelimiter);
        RSVPTE_DBG (RSVPTE_DBG_FLAG, "\n");
    }
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpEroObj
 * Description               : This routine dumps RSVPTE Ero Object contents.
 * Input(s)                  : pObjHdr  - Pointer to the object header.
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpEroObj (tObjHdr * pObjHdr)
{
    UINT1              *pPdu = NULL;
    UINT1              *pErObjEnd = NULL;
    UINT1               u1Count = RSVPTE_ZERO;
    UINT4               u4Val = RSVPTE_ZERO;
    UINT4              *pTemp = &u4Val;
    pErObjEnd
        = (UINT1 *) ((UINT1 *) pObjHdr + OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)));
    pPdu = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));
    RSVPTE_DBG (RSVPTE_DBG_FLAG, "ERO Contents: \n");
    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
    u1Count = RSVPTE_ONE;
    while ((UINT1 *) pPdu < pErObjEnd)
    {
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Ero SubObj - %d\n", u1Count);
        RPTE_EXT_1_BYTE (pPdu, *(UINT1 *) pTemp);
        if ((*(UINT1 *) pTemp & ERO_TYPE_MASK) == ERO_TYPE_STRICT_SET)
        {
            RSVPTE_DBG (RSVPTE_DBG_FLAG, "Strict Ero\n");
        }
        else if ((*(UINT1 *) pTemp & ERO_TYPE_MASK) == ERO_TYPE_LOOSE_SET)
        {
            RSVPTE_DBG (RSVPTE_DBG_FLAG, "Loose Ero\n");
        }
        switch ((*((UINT1 *) pTemp)) &= ERO_TYPE_SET_VALUE)
        {
            case ERO_TYPE_IPV4_ADDR:
                RPTE_EXT_1_BYTE (pPdu, *(UINT1 *) pTemp);
                RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                             "Length                       : %d\n",
                             (*((UINT1 *) pTemp)));
                RPTE_EXT_4_BYTES (pPdu, *((UINT4 *) pTemp));
                RSVPTE_DBG4 (RSVPTE_DBG_FLAG,
                             "Ipv4 Addr                    : "
                             "%d.%d.%d.%d \n",
                             (*((UINT1 *) pTemp + RSVPTE_THREE)),
                             (*((UINT1 *) pTemp + RSVPTE_TWO)),
                             (*((UINT1 *) pTemp + RSVPTE_ONE)),
                             (*((UINT1 *) pTemp)));
                break;

            default:
                break;
        }
        RPTE_EXT_1_BYTE (pPdu, *((UINT1 *) pTemp));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Prefix Len                   : %d\n",
                     (*(UINT1 *) pTemp));
        pPdu += sizeof (UINT1);
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
        u1Count++;
    }                            /*While end */
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpStyleObj
 * Description               : This routine dumps contents of RSVPTE 
 *                             Style Object.
 * Input(s)                  : pObjHdr  - Pointer to the ObjHdr.
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpStyleObj (tObjHdr * pObjHdr)
{
    tStyle             *pStyle = NULL;
    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Style Object Contents: \n");
    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
    pStyle = &(STYLE_OBJ ((tStyleObj *) pObjHdr));
    if (pStyle != NULL)
    {
        RSVPTE_DBG1
            (RSVPTE_DBG_FLAG,
             "Style Flags                  : 0x%x\n", STYLE_FLAGS (pStyle));
        switch (((STYLE_OPT_VECT (pStyle))[RSVPTE_TWO]) & STYLE_MASK)
        {
            case RPTE_FF:
                RSVPTE_DBG (RSVPTE_DBG_FLAG,
                            "Style Optical Vector         : FF\n");
                break;
            case RPTE_SE:
                RSVPTE_DBG (RSVPTE_DBG_FLAG,
                            "Style Optical Vector         : SE\n");
                break;
            case RPTE_WF:
                RSVPTE_DBG (RSVPTE_DBG_FLAG,
                            "Style Optical Vector         : WF\n");
                break;
            default:
                RSVPTE_DBG (RSVPTE_DBG_FLAG,
                            "Style Optical Vector         : Unknown ");
                break;
        }
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
    }
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpFlowSpecObj
 * Description               : This routine dumps contents of RSVPTE FlowSpec 
 *                             Object.
 * Input(s)                  : pObjHdr  - Pointer to the ObjHdr.
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpFlowSpecObj (tObjHdr * pObjHdr)
{
    tFlowSpec           FlowSpec;
    FLT4                f4TmpVar = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Flow Spec Object Contents: \n");
    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
    FlowSpec = ((tFlowSpecObj *) (VOID *) pObjHdr)->FlowSpec;
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MsgHdr Ver                   : %d \n",
                 FlowSpec.MsgHdr.u1Version);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MsgHdr HdrLen                : %d \n",
                 OSIX_NTOHS (FlowSpec.MsgHdr.u2HdrLen));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr SrvId                 : %d \n",
                 FlowSpec.SrvHdr.u1SrvID);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr Flag                  : 0x%x \n",
                 FlowSpec.SrvHdr.u1Flag);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr SrvLen                : %d \n",
                 OSIX_NTOHS (FlowSpec.SrvHdr.u2SrvLength));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "ParamHdr ParamNum            : %d \n",
                 FlowSpec.ParamHdr.u1ParamNum);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "ParamHdr Flag                : 0x%x \n",
                 FlowSpec.ParamHdr.u1ParamFlags);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "ParamHdr ParamLen            : %d \n",
                 OSIX_NTOHS (FlowSpec.ParamHdr.u2ParamLen));
    if (FlowSpec.SrvHdr.u1SrvID == FLOWSPEC_CLS_HDR_NUMBER)
    {
        f4TmpVar =
            OSIX_NTOHF (FlowSpec.ServiceParams.ClsService.TBParams.fBktRate);
        RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                     "Bucket Rate                  : %d kbps\n",
                     (UINT4) f4TmpVar);

        f4TmpVar =
            OSIX_NTOHF (FlowSpec.ServiceParams.ClsService.TBParams.fBktSize);
        RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                     "Bucket Size                  : %d kbps\n",
                     (UINT4) f4TmpVar);

        f4TmpVar =
            OSIX_NTOHF (FlowSpec.ServiceParams.ClsService.TBParams.fPeakRate);
        RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                     "Peak Data Rate               : %d kbps\n",
                     (UINT4) f4TmpVar);

        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Min Size                     : %d \n",
                     OSIX_NTOHL
                     (FlowSpec.ServiceParams.ClsService.TBParams.u4MinSize));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Max Size                     : %d \n",
                     OSIX_NTOHL
                     (FlowSpec.ServiceParams.ClsService.TBParams.u4MaxSize));
    }
    else
    {
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvId                        : %d \n",
                     FlowSpec.ServiceParams.GsService.GsRSpec.SrvHdr.u1SrvID);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Flag                         : 0x%x \n",
                     FlowSpec.ServiceParams.GsService.GsRSpec.SrvHdr.u1Flag);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvLen                       : %d \n",
                     OSIX_NTOHS
                     (FlowSpec.ServiceParams.GsService.GsRSpec.SrvHdr.
                      u2SrvLength));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "RSpecRate                    : %d \n",
                     (UINT4) OSIX_NTOHF (FlowSpec.ServiceParams.GsService.
                                         GsRSpec.fRSpecRate));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Slack                        : %d \n",
                     OSIX_NTOHL (FlowSpec.ServiceParams.GsService.GsRSpec.
                                 u4Slack));
    }
    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpAdSpecObj
 * Description               : This routine dumps contents of AdSpec Object.
 * Input(s)                  : pObjHdr  - Pointer to the ObjHdr.
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpAdSpecObj (tObjHdr * pObjHdr)
{
    tAdSpec             AdSpec;
    UINT2               u2AdSpecWithoutGsAndCls = RSVPTE_ZERO;

    u2AdSpecWithoutGsAndCls = sizeof (tAdSpecObj)
        - (sizeof (tGsAdSpec) + sizeof (tClsAdSpec));
    RSVPTE_DBG (RSVPTE_DBG_FLAG, "AdSpec Object Contents:\n");
    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
    AdSpec = ((tAdSpecObj *) (VOID *) pObjHdr)->AdSpec;
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MsgHdr Ver                   : %d\n",
                 AdSpec.MsgHdr.u1Version);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MsgHdr HdrLen                : %d \n",
                 OSIX_NTOHS (AdSpec.MsgHdr.u2HdrLen));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr SrvId                 : %d \n",
                 AdSpec.DfltParams.SrvHdr.u1SrvID);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr Flag                  : 0x%x \n",
                 AdSpec.DfltParams.SrvHdr.u1Flag);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr SrvLen                : %d \n",
                 OSIX_NTOHS (AdSpec.DfltParams.SrvHdr.u2SrvLength));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "HopCount ParamNum            : %d\n",
                 AdSpec.DfltParams.HopCount.ParamHdr.u1ParamNum);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "HopCount ParamFlags          : 0x%x\n",
                 AdSpec.DfltParams.HopCount.ParamHdr.u1ParamFlags);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "HopCount ParamLen            : %d\n",
                 OSIX_NTOHS (AdSpec.DfltParams.HopCount.ParamHdr.u2ParamLen));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "HopCount                     : %d\n",
                 OSIX_NTOHL (AdSpec.DfltParams.HopCount.u4ISHopCount));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "PathBwParam ParamNum         : %d\n",
                 AdSpec.DfltParams.PathBw.ParamHdr.u1ParamNum);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "PathBwParam ParamFlags       : 0x%x\n",
                 AdSpec.DfltParams.PathBw.ParamHdr.u1ParamFlags);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "PathBwParam ParamLen         : %d\n",
                 OSIX_NTOHS (AdSpec.DfltParams.PathBw.ParamHdr.u2ParamLen));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "PathBw                       : %d\n",
                 (UINT4) OSIX_NTOHF (AdSpec.DfltParams.PathBw.fPathBw));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MinPathLatency ParamNum      : %d\n",
                 AdSpec.DfltParams.MinPathLatency.ParamHdr.u1ParamNum);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MinPathLatency ParamFlags    : %d\n",
                 AdSpec.DfltParams.MinPathLatency.ParamHdr.u1ParamFlags);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MinPathLatency ParamLen      : %d\n",
                 OSIX_NTOHS
                 (AdSpec.DfltParams.MinPathLatency.ParamHdr.u2ParamLen));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MinPathLatency               : %d\n",
                 OSIX_NTOHL
                 (AdSpec.DfltParams.MinPathLatency.u4MinPathLatency));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MaxTransUnit ParamNum        : %d\n",
                 AdSpec.DfltParams.MaxTransUnit.ParamHdr.u1ParamNum);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MaxTransUnit ParamFlags      : %d\n",
                 AdSpec.DfltParams.MaxTransUnit.ParamHdr.u1ParamFlags);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MaxTransUnit ParamLen        : %d\n",
                 OSIX_NTOHS
                 (AdSpec.DfltParams.MaxTransUnit.ParamHdr.u2ParamLen));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MaxTransUnit                 : %d\n",
                 OSIX_NTOHL (AdSpec.DfltParams.MaxTransUnit.u4MTUSize));

    if (AdSpec.MsgHdr.u2HdrLen == u2AdSpecWithoutGsAndCls)
    {
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
        return;
    }
    else
    {
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr SrvId                 : %d \n",
                     AdSpec.GsAdSpec.SrvHdr.u1SrvID);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr Flag                  : 0x%x \n",
                     AdSpec.GsAdSpec.SrvHdr.u1Flag);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr SrvLen                : %d \n",
                     OSIX_NTOHS (AdSpec.GsAdSpec.SrvHdr.u2SrvLength));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "CTot ParamNum                : %d \n",
                     AdSpec.GsAdSpec.CTot.ParamHdr.u1ParamNum);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "CTot ParamFlags              : 0x%x\n",
                     AdSpec.GsAdSpec.CTot.ParamHdr.u1ParamFlags);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "CTot ParamLen                : %d\n",
                     OSIX_NTOHS (AdSpec.GsAdSpec.CTot.ParamHdr.u2ParamLen));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "CTot                         : %d\n",
                     OSIX_NTOHL (AdSpec.GsAdSpec.CTot.u4CTot));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "DTot ParamNum                : %d\n",
                     AdSpec.GsAdSpec.DTot.ParamHdr.u1ParamNum);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "DTot ParamFlags              : 0x%x\n",
                     AdSpec.GsAdSpec.DTot.ParamHdr.u1ParamFlags);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "DTot ParamLen                : %d\n",
                     OSIX_NTOHS (AdSpec.GsAdSpec.DTot.ParamHdr.u2ParamLen));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "DTot                         : %d\n",
                     OSIX_NTOHL (AdSpec.GsAdSpec.DTot.u4DTot));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "CSum ParamNum                : %d\n",
                     AdSpec.GsAdSpec.DTot.ParamHdr.u1ParamNum);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "CSum ParamFlags              : 0x%x\n",
                     AdSpec.GsAdSpec.CSum.ParamHdr.u1ParamFlags);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "CSum ParamLen                : %d\n",
                     OSIX_NTOHS (AdSpec.GsAdSpec.CSum.ParamHdr.u2ParamLen));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Csum                         : %d\n",
                     OSIX_NTOHL (AdSpec.GsAdSpec.CSum.u4CSum));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "DSum ParamNum                : %d\n",
                     AdSpec.GsAdSpec.DSum.ParamHdr.u1ParamNum);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "DSum ParamFlags              : 0x%x\n",
                     AdSpec.GsAdSpec.DSum.ParamHdr.u1ParamFlags);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "DSum ParamLen                : %d\n",
                     OSIX_NTOHS (AdSpec.GsAdSpec.DSum.ParamHdr.u2ParamLen));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Dsum                         : %d\n",
                     OSIX_NTOHL (AdSpec.GsAdSpec.DSum.u4DSum));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr SrvId                 : %d \n",
                     AdSpec.ClsAdSpec.SrvHdr.u1SrvID);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr Flag                  : 0x%x \n",
                     AdSpec.ClsAdSpec.SrvHdr.u1Flag);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr SrvLen                : %d \n",
                     OSIX_NTOHS (AdSpec.ClsAdSpec.SrvHdr.u2SrvLength));
        RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
        return;
    }
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpSenderTSpecObj
 * Description               : This routine dumps contents of SenderTSpec 
 *                             Object.
 * Input(s)                  : pObjHdr  - Pointer to the ObjHdr.
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpSenderTSpecObj (tObjHdr * pObjHdr)
{
    tSenderTspec        SenderTspec;
    FLT4                f4TmpVar = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Sender TSpec Contents: \n");
    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
    SenderTspec = ((tSenderTspecObj *) (VOID *) pObjHdr)->SenderTspec;
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MsgHdr Ver                   : %d \n",
                 SenderTspec.MsgHdr.u1Version);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "MsgHdr HdrLen                : %d \n",
                 OSIX_NTOHS (SenderTspec.MsgHdr.u2HdrLen));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr SrvId                 : %d \n",
                 SenderTspec.SrvHdr.u1SrvID);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr Flag                  : 0x%x \n",
                 SenderTspec.SrvHdr.u1Flag);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "SrvHdr SrvLen                : %d \n",
                 OSIX_NTOHS (SenderTspec.SrvHdr.u2SrvLength));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "ParamHdr ParamNum            : %d \n",
                 SenderTspec.ParamHdr.u1ParamNum);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "ParamHdr Flag                : 0x%x \n",
                 SenderTspec.ParamHdr.u1ParamFlags);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "ParamHdr ParamLen            : %d \n",
                 OSIX_NTOHS (SenderTspec.ParamHdr.u2ParamLen));

    f4TmpVar = OSIX_NTOHF (SenderTspec.TBParams.fBktRate);
    RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Bucket Rate                  : %d kbps\n",
                 (UINT4) f4TmpVar);

    f4TmpVar = OSIX_NTOHF (SenderTspec.TBParams.fBktSize);
    RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Bucket Size                  : %d kbps\n",
                 (UINT4) f4TmpVar);

    f4TmpVar = OSIX_NTOHF (SenderTspec.TBParams.fPeakRate);
    RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Peak Data Rate               : %d kbps\n",
                 (UINT4) f4TmpVar);

    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Min Size                     : %d \n",
                 OSIX_NTOHL (SenderTspec.TBParams.u4MinSize));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Max Size                     : %d \n",
                 OSIX_NTOHL (SenderTspec.TBParams.u4MaxSize));
    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpRroObj
 * Description               : This dumps contents of RRO Object.
 * Input(s)                  : pObjHdr - Pointer to the Object header.
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpRroObj (tObjHdr * pObjHdr)
{
    UINT1              *pPdu = NULL;
    UINT1               u1RroCount = RSVPTE_ONE;
    UINT1               u1LblCount = RSVPTE_ONE;
    UINT1              *pRroObjEnd = NULL;
    UINT4               u4Val = RSVPTE_ZERO;
    UINT4              *pTemp = &u4Val;
    RSVPTE_DBG (RSVPTE_DBG_FLAG, "RRO Contents: \n");
    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
    pRroObjEnd = (UINT1 *)
        ((UINT1 *) pObjHdr + OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)));
    pPdu = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));
    while ((UINT1 *) pPdu < pRroObjEnd)
    {
        RPTE_EXT_1_BYTE (pPdu, *((UINT1 *) pTemp));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Type                         : %d\n",
                     *((UINT1 *) pTemp));
        switch (*((UINT1 *) pTemp))
        {
            case RRO_TYPE_IPV4_ADDR:
                RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                             "Rro SubObj                   : %d\n", u1RroCount);
                RPTE_EXT_1_BYTE (pPdu, *((UINT1 *) pTemp));
                RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                             "Length                       : %d\n",
                             *((UINT1 *) pTemp));
                RPTE_EXT_4_BYTES (pPdu, *((UINT4 *) pTemp));
                RSVPTE_DBG4 (RSVPTE_DBG_FLAG,
                             "Ipv4 Addr                    : "
                             "%d.%d.%d.%d\n",
                             *((UINT1 *) ((UINT1 *) pTemp) + RSVPTE_THREE),
                             *((UINT1 *) ((UINT1 *) pTemp) + RSVPTE_TWO),
                             *((UINT1 *) ((UINT1 *) pTemp) + RSVPTE_ONE),
                             *(UINT1 *) pTemp);
                RPTE_EXT_1_BYTE (pPdu, *((UINT1 *) pTemp));
                RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                             "Prefix Len                   : %d\n",
                             *((UINT1 *) pTemp));
                RPTE_EXT_1_BYTE (pPdu, *((UINT1 *) pTemp));
                RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                             "Flags                        : 0x%x\n",
                             *(UINT1 *) pTemp);
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                u1RroCount++;
                break;

            case RRO_TYPE_LBL_OBJ:

                RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                             "Lbl SubObj                   : %d\n", u1LblCount);
                RPTE_EXT_1_BYTE (pPdu, *((UINT1 *) pTemp));
                RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                             "Length                       : %d\n",
                             *(UINT1 *) pTemp);
                RPTE_EXT_1_BYTE (pPdu, *((UINT1 *) pTemp));
                RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                             "Flags                        : 0x%x\n",
                             *(UINT1 *) pTemp);
                RPTE_EXT_1_BYTE (pPdu, *((UINT1 *) pTemp));
                RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                             "C Type                       : %d\n",
                             *(UINT1 *) pTemp);
                RPTE_EXT_4_BYTES (pPdu, *((UINT4 *) pTemp));
                RSVPTE_DBG1 (RSVPTE_DBG_FLAG,
                             "Lbl                          : %d\n",
                             *((UINT4 *) pTemp));
                RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
                u1LblCount++;
                break;

            default:
                break;
        }                        /*Switch End */
    }                            /*While end */
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name             : RpteDumpElspTpObj
 * Description               : This dumps contents of the ElspTp Object.
 * Input(s)                  : pObjHdr - Pointer to the Object header
 * Output(s)                 : None
 * Global Variables Referred : gpRsvpTeGblInfo.
 * Global Variables Modified : None
 * Exceptions   or   OS 
 * Error  Handling           : None
 * Use of Recursion          : None
 * Return(s)                 : None
 -------------------------------------------------------------------------- */
VOID
RpteDumpElspTpObj (tObjHdr * pObjHdr)
{
    UINT1               u1Index = RSVPTE_ZERO;
    UINT1               u1Val = RSVPTE_ZERO;
    UINT1               u1Psc = RSVPTE_ZERO;
    UINT2               u2PhbId = RSVPTE_ZERO;
    UINT1              *pPdu = NULL;
    UINT4               u4Val = RSVPTE_ZERO;
    UINT4              *pTemp = &u4Val;
    pPdu = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));
    RSVPTE_DBG (RSVPTE_DBG_FLAG, "Elsp TP Obj Contents: \n");
    RpteDumpObjHdr ((tObjHdr *) pObjHdr);
    RPTE_EXT_1_BYTE (pPdu, *((UINT1 *) pTemp));
    RPTE_EXT_2_BYTES (pPdu, *(pTemp));
    RPTE_EXT_1_BYTE (pPdu, *((UINT1 *) pTemp));
    RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "No of Map Entries            : %d\n",
                 (*(UINT1 *) pTemp));
    u1Val = *((UINT1 *) pTemp);
    for (u1Index = RSVPTE_ZERO; u1Index < u1Val; u1Index++)
    {

        RPTE_EXT_2_BYTES (pPdu, *(pTemp));
        RPTE_EXT_2_BYTES (pPdu, *(pTemp));
        u2PhbId = (UINT2) (*pTemp);
        RPTE_DS_DSCP_FOR_PHBID (u2PhbId, u1Psc);
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "PSC value                    : %d \n",
                     u1Psc);
        RPTE_EXT_4_BYTES (pPdu, *((UINT4 *) pTemp));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Token Bkt Rate               : %d \n",
                     (*(UINT4 *) pTemp));
        RPTE_EXT_4_BYTES (pPdu, *((UINT4 *) pTemp));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Token Bkt Size               : %d \n",
                     (*(UINT4 *) pTemp));
        RPTE_EXT_4_BYTES (pPdu, *((UINT4 *) pTemp));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Peak Data Rate               : %d \n",
                     (*(UINT4 *) pTemp));
        RPTE_EXT_4_BYTES (pPdu, *((UINT4 *) pTemp));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Min Police Unit              : %d \n",
                     (*(UINT4 *) pTemp));
        RPTE_EXT_4_BYTES (pPdu, *((UINT4 *) pTemp));
        RSVPTE_DBG1 (RSVPTE_DBG_FLAG, "Max Pkt Size                 : %d \n",
                     (*(UINT4 *) pTemp));
    }
    RSVPTE_DBG (RSVPTE_DBG_FLAG, au1ObjDelimiter);
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptedump.c                             */
/*---------------------------------------------------------------------------*/
