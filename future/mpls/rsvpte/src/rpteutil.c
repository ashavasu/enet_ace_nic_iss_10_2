/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rpteutil.c,v 1.90 2018/02/08 10:07:41 siva Exp $
 *
 * Description: This file contains routines for
 *              1) addition,deletion, checking of Tunnel
 *                 in the Tunnel RbTree.
 *              2) additon, deletion, checking of Tunnel Trafic
 *                 Param elements in the Tunnel Trfc Parm Table.
 ********************************************************************/

#include "rpteincs.h"
#include "../../mplsdb/mplsftn.h"
/*****************************************************************************/
/* Function Name : RpteCheckAllTnlInTnlTable                                */
/* Description   : This routine Checks the presence of a RSVP Tunnel, which  */
/*                 has same set of parameters like a new tunnel.             */
/* Input(s)      : pRsvpTeGblInfo. - Pointer to the RsvpTeGlobal information.*/
/*                 OutLabel        - Outgoing Label                          */
/*                 pOutIfEntry     - Pointer to Out Iface Entry              */
/* Output(s)     : None                                                      */
/* Return(s)     : RPTE_SUCCESS / RPTE FAILURE                               */
/*****************************************************************************/
UINT1
RpteCheckAllTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                           uLabel OutLabel, tIfEntry * pOutIfEntry)
{
    UINT4               u4ImpNull = IMPLICIT_NULL_LABEL;
    tIfEntry           *pIfEntry = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteCheckAllTnlInTnlTable : ENTRY \n");

    pTmpRsvpTeTnlInfo = RBTreeGetFirst (pRsvpTeInfo->RpteTnlTree);

    while (pTmpRsvpTeTnlInfo != NULL)

    {
        if (RPTE_TE_TNL (pTmpRsvpTeTnlInfo) == NULL)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (pRsvpTeInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }

        if (RSVPTE_TNL_ADMIN_STATUS
            (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) != RPTE_ADMIN_UP)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (pRsvpTeInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }
        if ((RSVPTE_TNL_NODE_RELATION (pTmpRsvpTeTnlInfo) == RPTE_EGRESS) ||
            (RSVPTE_TNL_PSB (pTmpRsvpTeTnlInfo) == NULL))
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (pRsvpTeInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }
        pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pTmpRsvpTeTnlInfo));
        if (((pIfEntry) == NULL) || (((pIfEntry) != NULL) &&
                                     (IF_ENTRY_ADDR (pIfEntry) !=
                                      IF_ENTRY_ADDR (pOutIfEntry))))
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (pRsvpTeInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }
        /* verifying whether the label is alloted from the requested
         * Label range  or not
         */
        if ((RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM) &&
            (((OutLabel.AtmLbl.u2Vpi) <
              pIfEntry->RpteIfInfo.u2MinVpi) ||
             ((OutLabel.AtmLbl.u2Vpi) >
              pIfEntry->RpteIfInfo.u2MaxVpi) ||
             ((OutLabel.AtmLbl.u2Vci) <
              pIfEntry->RpteIfInfo.u2MinVci) ||
             ((OutLabel.AtmLbl.u2Vci) > pIfEntry->RpteIfInfo.u2MaxVci)))
        {
            RSVPTE_DBG (RSVPTE_UTL_PRCS,
                        "UTIL: Label is allocated outside the Label "
                        "Range requested \n");
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteCheckAllTnlInTnlTable : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        if ((RPTE_IF_LBL_TYPE (pIfEntry) != RPTE_ATM) &&
            ((MEM_CMP (&OutLabel, &u4ImpNull, sizeof (UINT4))) == RSVPTE_ZERO))
        {
            RSVPTE_DBG (RSVPTE_UTL_PRCS, "UTIL: Implicit Null Label\n");
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteCheckAllTnlInTnlTable : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        /* check for allocation of same label for merge incapable ATM */
        if (RSVPTE_TNL_OPER_STATUS
            (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) != RPTE_OPER_UP)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (pRsvpTeInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }
        if ((RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM) &&
            (pRsvpTeInfo->u1AtmMergeSupport != ATM_MERGE_SUPPRT) &&
            ((MEM_CMP (&OutLabel, &pTmpRsvpTeTnlInfo->DownstrOutLbl,
                       sizeof (uLabel))) == RSVPTE_ZERO))
        {
            RSVPTE_DBG (RSVPTE_UTL_PRCS,
                        "UTIL: Same Atm Label is allocated for an "
                        "existing tunnel. \n");
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteCheckAllTnlInTnlTable : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        pTmpRsvpTeTnlInfo = RBTreeGetNext (pRsvpTeInfo->RpteTnlTree,
                                           (tRBElem *) pTmpRsvpTeTnlInfo, NULL);
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteCheckAllTnlInTnlTable : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteCheckTnlInTnlTable                                   */
/* Description   : This routine Check the presence of a RSVP Tunnel info     */
/*                 in the Tunnel RB Tree.                                    */
/* Input(s)      : pRsvpTeInfo.    - Pointer to the Rsvp Te Global           */
/*                 information.                                              */
/*                 u4TunnelIndex    - Tunnel Index                           */
/*                 u4TunnelInstance - Tunnel Instance value                  */
/*                 u4TnlIngressAddr - Tunnel Source Addr - IPv4              */
/*                 u4TnlEgressAddr  - Tunnel Destination address - IPv4      */
/* Output(s)     : ppRsvpTeTnlInfo  - Pointer to Pointer of the tunnel info  */
/*                 whose index matches u4TunnelIndex.                        */
/* Return(s)     : RPTE_SUCCESS in case of tunnel being present, otherwise   */
/*                 RPTE_FAILURE                                              */
/*****************************************************************************/
UINT1
RpteCheckTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                        UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                        UINT4 u4TnlIngressLsrId, UINT4 u4TnlEgressLsrId,
                        tRsvpTeTnlInfo ** ppRsvpTeTnlInfo)
{
    tRsvpTeTnlInfo      InRpteTnlInfo;

    MEMSET (&InRpteTnlInfo, RPTE_ZERO, sizeof (tRsvpTeTnlInfo));

    if (RpteUtlCheckTnlInTnlTable (pRsvpTeInfo, u4TunnelIndex, u4TunnelInstance,
                                   u4TnlIngressLsrId, u4TnlEgressLsrId,
                                   ppRsvpTeTnlInfo, &InRpteTnlInfo)
        == RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteCheckTnlInTnlTable : EXIT \n");
        return RPTE_SUCCESS;
    }

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteCheckTnlInTnlTable : INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/*****************************************************************************/
/* Function Name : RpteUtlCheckTnlInTnlTable                                 */
/* Description   : This routine Check the presence of a RSVP Tunnel info     */
/*                 in the Tunnel RB Tree.                                    */
/* Input(s)      : pRsvpTeInfo.    - Pointer to the Rsvp Te Global           */
/*                 information.                                              */
/*                 u4TunnelIndex    - Tunnel Index                           */
/*                 u4TunnelInstance - Tunnel Instance value                  */
/*                 u4TnlIngressAddr - Tunnel Source Addr - IPv4              */
/*                 u4TnlEgressAddr  - Tunnel Destination address - IPv4      */
/*                 pRpteInTnlInfo   - Pointer to RSVPTE tnl info             */
/* Output(s)     : ppRsvpTeTnlInfo  - Pointer to Pointer of the tunnel info  */
/*                 whose index matches u4TunnelIndex.                        */
/* Return(s)     : RPTE_SUCCESS in case of tunnel being present, otherwise   */
/*                 RPTE_FAILURE                                              */
/*****************************************************************************/
UINT1
RpteUtlCheckTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                           UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                           UINT4 u4TnlIngressLsrId, UINT4 u4TnlEgressLsrId,
                           tRsvpTeTnlInfo ** ppRsvpTeTnlInfo,
                           tRsvpTeTnlInfo * pRpteInTnlInfo)
{

    tTeTnlInfo          InTeTnlInfo;

    MEMSET (&InTeTnlInfo, RPTE_ZERO, sizeof (tTeTnlInfo));

    u4TnlIngressLsrId = OSIX_HTONL (u4TnlIngressLsrId);
    u4TnlEgressLsrId = OSIX_HTONL (u4TnlEgressLsrId);

    InTeTnlInfo.u4TnlIndex = u4TunnelIndex;
    InTeTnlInfo.u4TnlPrimaryInstance = u4TunnelInstance;
    MEMCPY (&InTeTnlInfo.TnlIngressLsrId, &u4TnlIngressLsrId, IPV4_ADDR_LENGTH);
    MEMCPY (&InTeTnlInfo.TnlEgressLsrId, &u4TnlEgressLsrId, IPV4_ADDR_LENGTH);
    pRpteInTnlInfo->pTeTnlInfo = &InTeTnlInfo;

    *ppRsvpTeTnlInfo =
        (tRsvpTeTnlInfo *) RBTreeGet (pRsvpTeInfo->RpteTnlTree,
                                      (tRBElem *) pRpteInTnlInfo);

    if (*ppRsvpTeTnlInfo != NULL)
    {
        return RPTE_SUCCESS;
    }
    return RPTE_FAILURE;
}

/*****************************************************************************/
/* Function Name : RpteCheckAdnlTnlInTnlTable                               */
/* Description   : This routine Check the presence of a RSVP Tunnel info     */
/*                 in the Tunnel RB Tree, whose Tunnel index and the         */
/*                 source address matches with the given inputs, but has a   */
/*                 different Tunnel instance (LSPID) value.                  */
/* Input(s)      : pRsvpTeGblInfo. - Pointer to the Rsvp Te Global           */
/*                 information.                                              */
/*                 u4TunnelIndex    - Tunnel Index                           */
/*                 u4TunnelInstance - Tunnel Instance value                  */
/*                 u4TnlIngressAddr - Tunnel Source Addr - IPv4              */
/*                 u4TnlEgressAddr  - Tunnel Egress Addr - IPv4              */
/* Output(s)     : ppRsvpTeTnlInfo  - Pointer to Pointer of the tunnel info  */
/*                 whose index matches u4TunnelIndex and u4TnlIngressAddr but*/
/*                 not the u4TunnelInstance         .                        */
/* Return(s)     : RPTE_SUCCESS in case of tunnel being present, otherwise   */
/*                 RPTE_FAILURE                                              */
/*****************************************************************************/
UINT1
RpteCheckAdnlTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                            UINT4 u4TunnelIndex,
                            UINT4 u4TunnelInstance,
                            UINT4 u4TnlIngressAddr,
                            UINT4 u4TnlEgressAddr,
                            tRsvpTeTnlInfo ** ppRsvpTeTnlInfo)
{
    tRsvpTeTnlInfo      InRpteTnlInfo;

    MEMSET (&InRpteTnlInfo, RPTE_ZERO, sizeof (tRsvpTeTnlInfo));

    if (RpteUtlCheckAdnlTnlInTnlTable
        (pRsvpTeInfo, u4TunnelIndex, u4TunnelInstance, u4TnlIngressAddr,
         u4TnlEgressAddr, ppRsvpTeTnlInfo, &InRpteTnlInfo) == RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteCheckAdnlTnlInTnlTable : EXIT \n");
        return RPTE_SUCCESS;
    }

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteCheckAdnlTnlInTnlTable : INTMD-EXIT \n");
    return RPTE_FAILURE;

}

/*****************************************************************************/
/* Function Name : RpteUtlCheckAdnlTnlInTnlTable                               */
/* Description   : This routine Check the presence of a RSVP Tunnel info     */
/*                 in the Tunnel RB Tree, whose Tunnel index and the         */
/*                 source address matches with the given inputs, but has a   */
/*                 different Tunnel instance (LSPID) value.                  */
/* Input(s)      : pRsvpTeGblInfo. - Pointer to the Rsvp Te Global           */
/*                 information.                                              */
/*                 u4TunnelIndex    - Tunnel Index                           */
/*                 u4TunnelInstance - Tunnel Instance value                  */
/*                 u4TnlIngressAddr - Tunnel Source Addr - IPv4              */
/*                 u4TnlEgressAddr  - Tunnel Egress Addr - IPv4              */
/*                 pRpteInTnlInfo   - Pointer to RSVPTE tnl info             */
/* Output(s)     : ppRsvpTeTnlInfo  - Pointer to Pointer of the tunnel info  */
/*                 whose index matches u4TunnelIndex and u4TnlIngressAddr but*/
/*                 not the u4TunnelInstance         .                        */
/* Return(s)     : RPTE_SUCCESS in case of tunnel being present, otherwise   */
/*                 RPTE_FAILURE                                              */
/*****************************************************************************/
UINT1
RpteUtlCheckAdnlTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                               UINT4 u4TunnelIndex,
                               UINT4 u4TunnelInstance,
                               UINT4 u4TnlIngressAddr,
                               UINT4 u4TnlEgressAddr,
                               tRsvpTeTnlInfo ** ppRsvpTeTnlInfo,
                               tRsvpTeTnlInfo * pRpteInTnlInfo)
{
    tTeTnlInfo          InTeTnlInfo;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;

    MEMSET (&InTeTnlInfo, RPTE_ZERO, sizeof (tTeTnlInfo));

    u4TnlIngressAddr = OSIX_HTONL (u4TnlIngressAddr);
    u4TnlEgressAddr = OSIX_HTONL (u4TnlEgressAddr);

    InTeTnlInfo.u4TnlIndex = u4TunnelIndex;
    MEMCPY (&InTeTnlInfo.TnlIngressLsrId, &u4TnlIngressAddr, IPV4_ADDR_LENGTH);
    MEMCPY (&InTeTnlInfo.TnlEgressLsrId, &u4TnlEgressAddr, IPV4_ADDR_LENGTH);
    pRpteInTnlInfo->pTeTnlInfo = &InTeTnlInfo;

    pTmpRsvpTeTnlInfo = RBTreeGetNext (pRsvpTeInfo->RpteTnlTree,
                                       (tRBElem *) pRpteInTnlInfo, NULL);
    while (pTmpRsvpTeTnlInfo != NULL)
    {
        if ((u4TunnelIndex != RSVPTE_TNL_TNLINDX (pTmpRsvpTeTnlInfo)) ||
            (u4TnlIngressAddr != RSVPTE_TNL_INGRESS
             (pTmpRsvpTeTnlInfo)) ||
            (u4TnlEgressAddr != RSVPTE_TNL_EGRESS (pTmpRsvpTeTnlInfo)))
        {
            break;
        }
        /* Do not consider Detour tunnel instances. */
        if ((RSVPTE_TNL_TNLINST (pTmpRsvpTeTnlInfo) <
             RPTE_DETOUR_TNL_INSTANCE) &&
            (RSVPTE_TNL_CFG_STATUS (pTmpRsvpTeTnlInfo) == RPTE_ACTIVE)
            && (RSVPTE_TNL_OPER_STATUS (RPTE_TE_TNL
                                        (pTmpRsvpTeTnlInfo))
                == RPTE_OPER_UP) &&
            (u4TunnelInstance != RSVPTE_TNL_TNLINST (pTmpRsvpTeTnlInfo)))
        {
            *ppRsvpTeTnlInfo = pTmpRsvpTeTnlInfo;
            return RPTE_SUCCESS;
        }
        pTmpRsvpTeTnlInfo = RBTreeGetNext (pRsvpTeInfo->RpteTnlTree,
                                           (tRBElem *) pTmpRsvpTeTnlInfo, NULL);
    }
    return RPTE_FAILURE;
}

/*****************************************************************************/
/* Function Name : RpteDelTnlFromTnlTable                                   */
/* Description   : This routine deletes an RSVP Tunnel information from the  */
/*                 Tunnel RBTree.                                        */
/* Input(s)      : pRsvpTeGblInfo. - Pointer to the Rsvp Te Global           */
/*                 information.                                              */
/*                 u4TunnelIndex    - Tunnel Index                           */
/*                 u4TunnelInstance - Tunnel Instance value                  */
/*                 u4TnlIngressAddr - Tunnel Source Addr - IPv4              */
/*                 u4TnlEgressAddr  - Tunnel Egress Addr - IPv4              */
/*                 u1RemoveFlag  - TRUE -> Tunnel to be removed from RBTree  */
/*                              FALSE -> Tunnel not to be removed from RBTree*/
/* Output(s)     : Tunnel RBTree deleted with the tunnel info.               */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
UINT1
RpteDelTnlFromTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                        tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                        UINT4 u4TunnelIndex,
                        UINT4 u4TunnelInstance,
                        UINT4 u4TnlIngressAddr,
                        UINT4 u4TnlEgressAddr, UINT1 u1RemoveFlag)
{
    UINT1               u1RetVal = RPTE_FAILURE;
    tRpteKey            RpteKey;
    tNbrEntry          *pNbrEntry = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteDelTnlFromTnlTable : ENTRY \n");
    if (u1RemoveFlag)
    {

        /* Check done for the presence of the tunnel in the tunnel RBTree. */
        u1RetVal = RpteCheckTnlInTnlTable (pRsvpTeInfo, u4TunnelIndex,
                                           u4TunnelInstance,
                                           u4TnlIngressAddr,
                                           u4TnlEgressAddr, &pRsvpTeTnlInfo);

        if (u1RetVal == RPTE_SUCCESS)
        {
            RSVPTE_TNL_CFG_STATUS (pRsvpTeTnlInfo) = RPTE_DESTROY;

            /* Tunnel present and hence deleted. */
            RBTreeRemove (pRsvpTeInfo->RpteTnlTree, pRsvpTeTnlInfo);
            RSVPTE_DBG4 (RSVPTE_UTL_ETEXT,
                         "Tunnel %d %d %x %x deleted from RSVP RBTree\n",
                         u4TunnelIndex, u4TunnelInstance, u4TnlIngressAddr,
                         u4TnlEgressAddr);
        }
        else
        {
            RSVPTE_DBG4 (RSVPTE_UTL_ETEXT,
                         "Tunnel %d %d %x %x NOT deleted from RSVP RBTree\n",
                         u4TunnelIndex, u4TunnelInstance, u4TnlIngressAddr,
                         u4TnlEgressAddr);

            RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RB Node not found : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
    }
    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) & RPTE_IN_PATH_MSG_ID_PRESENT) ==
        RPTE_IN_PATH_MSG_ID_PRESENT)
    {
        RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (RPTE_TNL_USTR_NBR
                                                    (pRsvpTeTnlInfo));
        RpteKey.u8_Key[RPTE_ONE] = RPTE_TNL_IN_PATH_MSG_ID (pRsvpTeTnlInfo);
        RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_NBR_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
    }

    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) & RPTE_IN_RESV_MSG_ID_PRESENT) ==
        RPTE_IN_RESV_MSG_ID_PRESENT)
    {
        RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (RPTE_TNL_DSTR_NBR
                                                    (pRsvpTeTnlInfo));
        RpteKey.u8_Key[RPTE_ONE] = RPTE_TNL_IN_RESV_MSG_ID (pRsvpTeTnlInfo);
        RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_NBR_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
    }

    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) & RPTE_OUT_PATH_MSG_ID_PRESENT)
        == RPTE_OUT_PATH_MSG_ID_PRESENT)
    {
        RpteKey.u4_Key = RPTE_TNL_OUT_PATH_MSG_ID (pRsvpTeTnlInfo);
        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_LCL_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);

        /* After removing out path message id from trie, teTnlInfo
         * pointer has to be removed from RBTree. RBTree removal has
         * to be done only for out path message id, since only out
         * path message id is used for SRefresh RecoveryPath message
         * support
         * */
        if (gpRsvpTeGblInfo->u1GrProgressState != RPTE_GR_SHUT_DOWN_IN_PROGRESS)
        {
            TeSigDelFromTeMsgIdBasedTable (pRsvpTeTnlInfo->pTeTnlInfo);
        }
    }

    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) & RPTE_OUT_RESV_MSG_ID_PRESENT)
        == RPTE_OUT_RESV_MSG_ID_PRESENT)
    {
        RpteKey.u4_Key = RPTE_TNL_OUT_RESV_MSG_ID (pRsvpTeTnlInfo);
        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_LCL_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
    }

    /* Decrement the updated tunnel count in the upstream interface.
     * tunnel count decrement in the downstream interface is done
     * when RSB is deleted (RpteUtlDeleteRsb)*/
    pNbrEntry = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);
    if (pNbrEntry != NULL)
    {
        if (NBR_ENTRY_IF_ENTRY (pNbrEntry) != NULL)
        {
            (RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS
             (NBR_ENTRY_IF_ENTRY (pNbrEntry)))--;
        }
        (NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry))--;
        TMO_DLL_Delete (&(pNbrEntry->UpStrTnlList),
                        &(pRsvpTeTnlInfo->UpStrNbrTnl));
        if (NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry) == RPTE_ZERO)
        {
            if (NBR_ENTRY_IF_ENTRY (pNbrEntry) != NULL)
            {
                RpteUtlDeleteNbrTableEntry (NBR_ENTRY_IF_ENTRY (pNbrEntry),
                                            pNbrEntry);
            }
        }
    }

    pNbrEntry = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
    if (pNbrEntry != NULL)
    {
        if (NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry) == RPTE_ZERO)
        {
            if (NBR_ENTRY_IF_ENTRY (pNbrEntry) != NULL)
            {
                RpteUtlDeleteNbrTableEntry (NBR_ENTRY_IF_ENTRY (pNbrEntry),
                                            pNbrEntry);
            }
        }
    }

    MEMSET (pRsvpTeTnlInfo, RPTE_ZERO, sizeof (tRsvpTeTnlInfo));
    /* Tunnel info returned to the Tunnel memory Pool */
    MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeTnlInfo);

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteDelTnlFromTnlTable : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteUtlDeleteTnlTbl                                    */
/* Description   : This routine Deletes an RSVP Tunnel Related parameter     */
/*                 from the Tunnel RbTree.                                   */
/* Input(s)      : pRsvpTeTnlTable - Pointer to the RsvpTe Tunnel RBTree     */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
RpteUtlDeleteTnlTbl ()
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpInRsvpTeTnlInfo = NULL;
    tTimerBlock        *pTimerBlock = NULL;
    tRpteKey            RpteKey;
    BOOL1               bFlag = RPTE_TRUE;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlDeleteTnlTbl : ENTRY \n");

    gu4RMDFlags = RPTE_ZERO;
    if (gpRsvpTeGblInfo->RpteTnlTree == NULL)
    {
        return;
    }
    pRsvpTeTnlInfo = RBTreeGetFirst (gpRsvpTeGblInfo->RpteTnlTree);

    while (pRsvpTeTnlInfo != NULL)
    {
        pTmpRsvpTeTnlInfo = pRsvpTeTnlInfo;
        pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                        (tRBElem *) pRsvpTeTnlInfo, NULL);
        pTmpInRsvpTeTnlInfo = RPTE_FRR_IN_TNL_INFO (pTmpRsvpTeTnlInfo);
        if ((pTmpInRsvpTeTnlInfo == pRsvpTeTnlInfo) && (pRsvpTeTnlInfo != NULL))
        {
            pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                            (tRBElem *) pRsvpTeTnlInfo, NULL);
        }
        do
        {
            if (RPTE_TE_TNL (pTmpRsvpTeTnlInfo) == NULL)
            {
                if (bFlag == RPTE_TRUE)
                {
                    /* Tunnel info returned to the Tunnel memory Pool */
                    RBTreeRem (gpRsvpTeGblInfo->RpteTnlTree, pTmpRsvpTeTnlInfo);
                }
                MEMSET (pTmpRsvpTeTnlInfo, RPTE_ZERO, sizeof (tRsvpTeTnlInfo));
                MemReleaseMemBlock (RSVPTE_TNL_POOL_ID,
                                    (UINT1 *) pTmpRsvpTeTnlInfo);

            }
            else
            {
                RptePmRemoveTnlFromPrioList (pTmpRsvpTeTnlInfo);
                if ((gpRsvpTeGblInfo->u1GrProgressState !=
                     RPTE_GR_SHUT_DOWN_IN_PROGRESS) &&
                    (RPTE_IS_FRR_CAPABLE (pTmpRsvpTeTnlInfo) != RPTE_TRUE))
                {
                    switch (RSVPTE_TNL_NODE_RELATION (pTmpRsvpTeTnlInfo))
                    {
                        case RPTE_INGRESS:
                            RptePhGeneratePathTear (pTmpRsvpTeTnlInfo);
                            break;

                        case RPTE_INTERMEDIATE:
                            if ((pTmpRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                                 i4E2EProtectionType !=
                                 MPLS_TE_DEDICATED_ONE2ONE))
                            {
                                RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                                            "RpteUtlDeleteTnlTb: In 1:1 Path Tear should not be generated\n");
                                RptePhGeneratePathTear (pTmpRsvpTeTnlInfo);
                            }
                            if ((pTmpRsvpTeTnlInfo->pRsb != NULL) &&
                                (((pTmpRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                                   i4E2EProtectionType ==
                                   MPLS_TE_DEDICATED_ONE2ONE)
                                  && (pTmpRsvpTeTnlInfo->pTeTnlInfo->
                                      u1TnlPathType == TE_TNL_PROTECTION_PATH))
                                 || (pTmpRsvpTeTnlInfo->pTeTnlInfo->
                                     GmplsTnlInfo.i4E2EProtectionType !=
                                     MPLS_TE_DEDICATED_ONE2ONE)))
                            {
                                RpteRthSendResvTear (pTmpRsvpTeTnlInfo->pRsb);
                            }
                            break;

                        case RPTE_EGRESS:
                            if (pTmpRsvpTeTnlInfo->pRsb != NULL)
                            {
                                RpteRthSendResvTear (pTmpRsvpTeTnlInfo->pRsb);
                            }
                            break;
                        default:
                            break;
                    }
                }

                if (RPTE_TNL_PT_TIMER_BLOCK (pTmpRsvpTeTnlInfo) != NULL)
                {
                    pTimerBlock = RPTE_TNL_PT_TIMER_BLOCK (pTmpRsvpTeTnlInfo);

                    /* Timer associated with the tear message is deleted */
                    TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                                  (pTimerBlock));

                    RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                    RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                         &RpteKey,
                                         RPTE_LCL_MSG_ID_DB_PREFIX,
                                         RpteMIHReleaseTrieInfo);

                    MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                        (UINT1 *) pTimerBlock);
                    RPTE_TNL_PT_TIMER_BLOCK (pTmpRsvpTeTnlInfo) = NULL;
                }

                if (RPTE_TNL_RT_TIMER_BLOCK (pTmpRsvpTeTnlInfo) != NULL)
                {
                    pTimerBlock = RPTE_TNL_RT_TIMER_BLOCK (pTmpRsvpTeTnlInfo);

                    /* Timer associated with the tear message is deleted */
                    TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                                  (pTimerBlock));

                    RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                    RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                         &RpteKey,
                                         RPTE_LCL_MSG_ID_DB_PREFIX,
                                         RpteMIHReleaseTrieInfo);

                    MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                        (UINT1 *) pTimerBlock);
                    RPTE_TNL_RT_TIMER_BLOCK (pTmpRsvpTeTnlInfo) = NULL;
                }

                /* Delete the existing Tnl Info from the Tnl Table */
                if (pTmpRsvpTeTnlInfo->pTeTnlInfo->u1TnlProtMethod ==
                    TE_TNL_FRR_FACILITY_METHOD)
                {
                    if (RPTE_TRUE == bFlag)
                    {
                        RPTE_TNL_DEL_TNL_FLAG (pTmpRsvpTeTnlInfo) = RPTE_TRUE;
                    }
                }
                else
                {
                    RPTE_TNL_DEL_TNL_FLAG (pTmpRsvpTeTnlInfo) = RPTE_TRUE;
                }
                RpteUtlFreeTnlNodeTnlTbl (pTmpRsvpTeTnlInfo, bFlag);
            }
            if (pTmpInRsvpTeTnlInfo != NULL)
            {
                bFlag = RPTE_FALSE;
                pTmpRsvpTeTnlInfo = pTmpInRsvpTeTnlInfo;
            }
        }
        while (pTmpInRsvpTeTnlInfo != NULL);
        bFlag = RPTE_TRUE;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlDeleteTnlTbl : EXIT \n");
    return;
}

/*****************************************************************************/
/* Function Name : RpteGetIfEntry                                            */
/* Description   : This routine gets the interface entry associated with the */
/*                 given IfIndex.                                            */
/* Input(s)      : i4IfIndex  - Interface index                              */
/* Output(s)     : ppIfEntry - Point to the Pointer of the interface entry,  */
/*                 In case of Interface existing for the given IF entry.     */
/* Return(s)     : RPTE_SUCCESS or RPTE_FAILURE.                             */
/*****************************************************************************/
UINT1
RpteGetIfEntry (INT4 i4IfIndex, tIfEntry ** ppIfEntry)
{
    tIfEntry            IfEntry;
    UINT1               u1RetVal = MPLS_FAILURE;
    UINT4               u4HashIndex;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetIfEntry : ENTRY \n");

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        return RPTE_FAILURE;
    }
    u4HashIndex = RSVPTE_COMPUTE_HASH_INDEX ((UINT4) i4IfIndex);
    IF_ENTRY_IF_INDEX (&IfEntry) = ((UINT4) i4IfIndex);

    u1RetVal = HASHSearch (GBL_IF_ENTRY_OFFSET_TBL, GBL_IF_ENTRY_LENGTH_TBL,
                           GBL_IF_ENTRY_BYTE_ORDER_TBL,
                           RPTE_IF_ENTRY_NUM_INDICES, RSVP_GBL_IF_HSH_TBL,
                           &IfEntry, u4HashIndex, GBL_IF_ENTRY_NODE_OFFSET,
                           (VOID **) ppIfEntry);

    if (u1RetVal == MPLS_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetIfEntry : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetIfEntry : EXIT \n");
    return RPTE_SUCCESS;
}

/*----------------------------------------------------------------------------
 * Function Name   : RpteGetNbrEntry
 * Description     : This function gets the neighbour entry.
 * Input (s)       : u4NbrAddr  - Hold Neighbour Address
 *                   pIfEntry   - Pointer to IfEntry
 * Output (s)      : ppNbrEntry - pointer to the Pointer of Neighbour Entry
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE 
 *---------------------------------------------------------------------------*/
UINT1
RpteGetNbrEntry (UINT4 u4NbrAddr, tIfEntry * pIfEntry, tNbrEntry ** ppNbrEntry)
{
    tTMO_SLL           *pNbrList = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetNbrEntry : ENTRY \n");
    pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));
    TMO_SLL_Scan (pNbrList, pNbrEntry, tNbrEntry *)
    {
        if (u4NbrAddr == NBR_ENTRY_ADDR (pNbrEntry))
        {
            *ppNbrEntry = pNbrEntry;
            RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetNbrEntry : EXIT \n");
            return RPTE_SUCCESS;
        }
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetNbrEntry : INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/* ***************************************************************************
 * Function Name   : RpteGetNbrEntryFromNbrAddr
 * Description     : This function gets the neighbour entry.
 * Input (s)       : u4NbrAddr  - Hold Neighbour Address
 * Output (s)      : ppNbrEntry - Corresponding neighbour entry 
 * Returns         : RPTE_SUCCESS / RPTE_FAILURE 
 * ************************************************************************* */
UINT1
RpteGetNbrEntryFromNbrAddr (UINT4 u4NbrAddr, tNbrEntry ** ppNbrEntry)
{
    tTMO_SLL           *pNbrList = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tIfEntry           *pIfEntry = NULL;
    INT4                i4IfIndex;
    INT4                i4Temp1;

    if (nmhGetFirstIndexFsMplsRsvpTeIfStatsTable (&i4IfIndex) == SNMP_FAILURE)
    {
        return RPTE_FAILURE;
    }

    do
    {
        i4Temp1 = i4IfIndex;
        if (RpteGetIfEntry (i4IfIndex, &pIfEntry) == RPTE_FAILURE)
        {
            return RPTE_FAILURE;
        }
        pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));

        TMO_SLL_Scan (pNbrList, pNbrEntry, tNbrEntry *)
        {
            if (u4NbrAddr == NBR_ENTRY_ADDR (pNbrEntry))
            {
                *ppNbrEntry = pNbrEntry;
                RSVPTE_DBG (RSVPTE_UTL_PRCS,
                            "RpteGetNbrEntryFromNbrAddr: FOUND");
                return RPTE_SUCCESS;
            }
        }
    }
    while (nmhGetNextIndexFsMplsRsvpTeIfStatsTable (i4Temp1, &i4IfIndex)
           != SNMP_FAILURE);

    RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteGetNbrEntryFromNbrAddr: NOT FOUND");
    return RPTE_FAILURE;
}

/*****************************************************************************/
/* Function Name : RpteCreateNewRsvpteTunnel                                 */
/* Description   : This routine allocates memory for new RSVPTE Tunnel       */
/* Input(s)      : u4TunnelIndex - Tunnel index                              */
/*                 u4TunnelInstance - Tunnel Instance                        */
/*                 u4TnlIngressAddr - Tunnel Src Address                     */
/*                 u4TnlEgressAddr  - Tunnel destination address             */
/*                 pTeTnlInfo - pointer to the TeTnlInfo structure           */
/* Output(s)     : ppRsvpTeTnlInfo - Pointer to the Pointer of the Tunnelinfo*/
/*                 in case of successful allocation.                         */
/*                 bIsRbReq - Add into RbTree to be done or not.             */
/* Return(s)     : RPTE_SUCCESS or RPTE_FAILURE.                             */
/*****************************************************************************/
UINT1
RpteCreateNewRsvpteTunnel (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                           UINT4 u4TnlIngressAddr, UINT4 u4TnlEgressAddr,
                           tTeTnlInfo * pTeTnlInfo,
                           tRsvpTeTnlInfo ** ppRsvpTeTnlInfo, BOOL1 bIsRbReq)
{
    UINT1               u1TempPscIndex = RPTE_ZERO;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    UINT4               u4IngressId = RPTE_ZERO;
    UINT4               u4EgressId = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteCreateNewRsvpteTunnel : ENTRY \n");

    if ((bIsRbReq == RPTE_TRUE) &&
        (RpteCheckTnlInTnlTable
         (gpRsvpTeGblInfo, u4TunnelIndex, u4TunnelInstance, u4TnlIngressAddr,
          u4TnlEgressAddr, &pTmpRsvpTeTnlInfo) == RPTE_SUCCESS))
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                    "RpteCreateNewRsvpteTunnel : Tunnel already exists... INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    pTmpRsvpTeTnlInfo = (tRsvpTeTnlInfo *)
        RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TNL_POOL_ID);

    if (pTmpRsvpTeTnlInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                    "RpteCreateNewRsvpteTunnel : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    INIT_RSVP_TNL_INFO (pTmpRsvpTeTnlInfo);
    UTL_SLL_Init (&(pTmpRsvpTeTnlInfo->StackTnlList),
                  RPTE_OFFSET (tRsvpTeTnlInfo, NextStackTnl));
    TMO_SLL_Init (&pTmpRsvpTeTnlInfo->NewSubObjList);
    TMO_DLL_Init ((tTMO_DLL
                   *) (&(RPTE_FRR_FAC_PROT_TNL_LIST (pTmpRsvpTeTnlInfo))));

    TMO_DLL_Init ((tTMO_DLL *) (&(pTmpRsvpTeTnlInfo->FacInFrrList)));

    /* Initialize NextHoldPrioDnStrTnl SLL node */
    TMO_SLL_Init_Node (&(pTmpRsvpTeTnlInfo->NextHoldPrioDnStrTnl));

    /* Initialize NextHoldPrioUpStrTnl SLL node */
    TMO_SLL_Init_Node (&(pTmpRsvpTeTnlInfo->NextHoldPrioUpStrTnl));

    /* Initialize NextPreemptTnl SLL node */
    TMO_SLL_Init_Node (&(pTmpRsvpTeTnlInfo->NextPreemptTnl));

    /* Initialize NextStackTnl SLL node */
    TMO_SLL_Init_Node (&(pTmpRsvpTeTnlInfo->NextStackTnl));

    /* Initialize The UpStream and Downstream Neighbour Entry DLL node */
    TMO_DLL_Init_Node (&(pTmpRsvpTeTnlInfo->UpStrNbrTnl));
    TMO_DLL_Init_Node (&(pTmpRsvpTeTnlInfo->DnStrNbrTnl));

    TMO_DLL_Init_Node (&(pTmpRsvpTeTnlInfo->FacFrrProtTnl));

    TMO_DLL_Init_Node (&(pTmpRsvpTeTnlInfo->FacInFrrProtTnl));

    RSVPTE_TNL_RRO_ERR_FLAG (pTmpRsvpTeTnlInfo) = RPTE_RRO_ERR_PATH_RESV_RESET;
    /* Assign L3Pid default value here */
    RSVPTE_TNL_L3PID (pTmpRsvpTeTnlInfo) = RPTE_STD_IPV4_ETHERTYPE;

    RPTE_TNL_DEL_TNL_FLAG (pTmpRsvpTeTnlInfo) = RPTE_FALSE;

    pTmpRsvpTeTnlInfo->DownstrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
    pTmpRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl = MPLS_INVALID_LABEL;
    pTmpRsvpTeTnlInfo->UpStrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
    pTmpRsvpTeTnlInfo->UpStrOutLbl.u4GenLbl = MPLS_INVALID_LABEL;

    for (u1TempPscIndex = RSVPTE_ONE;
         u1TempPscIndex <= RPTE_DS_MAX_NO_OF_PSCS; u1TempPscIndex++)
    {
        RPTE_PSC_HANDLE (pTmpRsvpTeTnlInfo, u1TempPscIndex) =
            RPTE_INVALID_HANDLE;
    }
    if (RSVPTE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP)
    {
        RSVPTE_TNL_INDEX (pTeTnlInfo) = u4TunnelIndex;
        RSVPTE_TNL_INSTANCE (pTeTnlInfo) = u4TunnelInstance;
        RSVPTE_TNL_PRIMARY_INSTANCE (pTeTnlInfo) = u4TunnelInstance;
        u4IngressId = OSIX_NTOHL (u4TnlIngressAddr);
        MEMCPY ((UINT1 *) &(RSVPTE_TNL_INGRESS_RTR_ID (pTeTnlInfo)),
                (UINT1 *) &(u4IngressId), RSVPTE_IPV4ADR_LEN);

        u4EgressId = OSIX_NTOHL (u4TnlEgressAddr);
        MEMCPY ((UINT1 *) &(RSVPTE_TNL_EGRESS_RTR_ID (pTeTnlInfo)),
                (UINT1 *) &(u4EgressId), RSVPTE_IPV4ADR_LEN);
        RSVPTE_TNL_ROLE (pTeTnlInfo) = RPTE_INTERMEDIATE;
        RSVPTE_TNL_OWNER (pTeTnlInfo) = RPTE_TNL_OWNER_RSVP;
        RSVPTE_TNL_SGNL_PRTCL (pTeTnlInfo) = RPTE_PROT;
    }

    if (pTeTnlInfo->GmplsTnlInfo.u1Direction == GMPLS_FORWARD)
    {
        pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVED;
    }
    RSVPTE_TNL_CFG_STATUS (pTmpRsvpTeTnlInfo) = RPTE_ACTIVE;

    RSVPTE_DBG4 (RSVPTE_FRR_DBG,
                 "Create: Tunnel %d %d %x %x added to RbTree\n",
                 u4TunnelIndex, u4TunnelInstance, u4TnlIngressAddr,
                 u4TnlEgressAddr);

    *ppRsvpTeTnlInfo = pTmpRsvpTeTnlInfo;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteCreateNewRsvpteTunnel : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteRelTnlEntryFunc                                       */
/* Description   : Function to release the Tunnel informtion from its links  */
/*                 etc and releasing it to the associated mem pools          */
/* Input(s)      : pRsvpTeGblInfo -  Pointer to the RSVP TE Global           */
/*                 information.                                              */
/*                 pRpteTnlInfo - Pointer to the Tunnel information to be    */
/*                 released.                                                 */
/*                 u1RemoveFlag - To be removed from RPTE table or not.      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
UINT1
RpteRelTnlEntryFunc (const tRsvpTeGblInfo * pRsvpTeGblInfo,
                     tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 u1CallerId,
                     UINT1 u1RemoveFlag)
{
    UINT1               u1TmpVar = 0;
    UINT1               u1ResvRsrc = RPTE_FALSE;
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4TunnelIngressLSRId = 0;
    UINT4               u4TunnelEgressLSRId = 0;
    UINT1               u1IsGrTnl = 0;
    tIfEntry           *pIfFacEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tTimerBlock        *pTimerBlock = NULL;
    tRpteKey            RpteKey;
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeBaseTnlInfo = NULL;
    tRsvpTeTnlInfo     *pOutRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pInRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeMapTnlInfo = NULL;
    tRsvpTeTnlInfo     *pFARsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpFATnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpFARsvpTeTnlInfo = NULL;
    tErrorSpec          ErrorSpec;
    tTMO_DLL           *pFacFrrList = NULL;
    tTMO_DLL_NODE      *pFacFrrProtTnl = NULL;
    tTMO_DLL           *pFacInFrrList = NULL;
    tTMO_DLL_NODE      *pFacInFrrProtTnl = NULL;
    UINT4               u4ProtIfIndex = RPTE_ZERO;
    UINT1               u1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;
    tSenderTspec       *pPathTe = NULL;
    FLOAT               reqResBw = RSVPTE_ZERO;
    UINT4               u4IngressId = RSVPTE_ZERO;
    UINT4               u4EgressId = RSVPTE_ZERO;
    tTMO_DLL           *pIfFacTnlsList = NULL;
    tTMO_DLL_NODE      *pIfFacTnlsNode = NULL;
    tFacilityRpteBkpTnl *pTmpFacilityRpteBkpTnl = NULL;
    tNotifyRecipient   *pNotifyRecipient = NULL;
    tNotifyMsgTnl      *pNotifyMsgTnl = NULL;
    tNotifyMsgTnl      *pTempNotifyMsgTnl = NULL;

    UNUSED_PARAM (pRsvpTeGblInfo);

    RSVPTE_DBG (RSVPTE_UTL_PRCS, "UTIL : Entering RpteRelTnlEntryFunc\n");
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteRelTnlEntryFunc : ENTRY \n");

    u4TunnelIndex = RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);
    u4TunnelInstance = RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);
    u4TunnelIngressLSRId = OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo));
    u4TunnelEgressLSRId = OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));
    u1IsGrTnl = pRsvpTeTnlInfo->u1IsGrTnl;

    MEMSET (&ErrorSpec, RPTE_ZERO, sizeof (tErrorSpec));

    pRsvpTeMapTnlInfo = pRsvpTeTnlInfo->pMapTnlInfo;

    if (pRsvpTeMapTnlInfo != NULL)
    {
        pRsvpTeMapTnlInfo->pMapTnlInfo = NULL;
        pRsvpTeTnlInfo->pMapTnlInfo = NULL;
        if ((pRsvpTeMapTnlInfo->pTeTnlInfo != NULL) &&
            (pRsvpTeMapTnlInfo->pTeTnlInfo->GmplsTnlInfo.
             i4E2EProtectionType == MPLS_TE_DEDICATED_ONE2ONE))
        {
            if (pRsvpTeMapTnlInfo->pTeTnlInfo->u1TnlPathType ==
                RPTE_TNL_WORKING_PATH)
            {
                RSVPTE_DBG1 (RSVPTE_UTL_PRCS, "u1TnlLocalProtectInUse as "
                             "LOCAL_PROT_NOT_AVAIL for path-type: %u\n",
                             pRsvpTeMapTnlInfo->pTeTnlInfo->u1TnlPathType);
                pRsvpTeMapTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse =
                    LOCAL_PROT_NOT_AVAIL;
            }

            pRsvpTeMapTnlInfo->pTeTnlInfo->u4BkpTnlIndex = RPTE_ZERO;
            pRsvpTeMapTnlInfo->pTeTnlInfo->u4BkpTnlInstance = RPTE_ZERO;
            MEMSET (&pRsvpTeMapTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                    RPTE_ZERO, ROUTER_ID_LENGTH);
            MEMSET (&pRsvpTeMapTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                    RPTE_ZERO, ROUTER_ID_LENGTH);
            if (pRsvpTeMapTnlInfo->pPsb != NULL)
            {
                pRsvpTeMapTnlInfo->pPsb->Association.u2AssocID = RPTE_ZERO;
            }
        }
        else if ((pRsvpTeMapTnlInfo->pTeTnlInfo != NULL)
                 && (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP)
                 &&
                 ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) !=
                   RPTE_INTERMEDIATE)))
        {
            UTL_SLL_OFFSET_SCAN (&(pRsvpTeTnlInfo->StackTnlList),
                                 pRsvpFATnlInfo, pTmpFARsvpTeTnlInfo,
                                 tRsvpTeTnlInfo *)
            {
                TeSigCopyOutLabel (pRsvpTeMapTnlInfo->pTeTnlInfo,
                                   pRsvpFATnlInfo->pTeTnlInfo);
            }
        }

        if (pRsvpTeMapTnlInfo->pTeTnlInfo != NULL)
        {
            pRsvpTeMapTnlInfo->pTeTnlInfo->u1TnlRerouteFlag = FALSE;
        }
    }
    /* Protected tnl deletion 
     * Delete the node in IfEntry*/
    if (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) != NULL)
    {
        pOutRsvpTeTnlInfo = RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo);
        pFacFrrList = &(RPTE_FRR_FAC_PROT_TNL_LIST (pOutRsvpTeTnlInfo));

        /* Delete this node from backup tnl's protected tunl list */
        if (TMO_DLL_Count (pFacFrrList) != RPTE_ZERO)
        {
            TMO_DLL_Delete (pFacFrrList,
                            (tTMO_DLL_NODE *)
                            & (RPTE_FRR_FAC_PROT_TNL_NODE (pRsvpTeTnlInfo)));
        }
    }

    if (RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo) != NULL)
    {
        pInRsvpTeTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);
        pFacInFrrList = &(pInRsvpTeTnlInfo->FacInFrrList);

        /* Delete this node from backup tnl's protected tunl list */
        if (TMO_DLL_Count (pFacInFrrList) != RPTE_ZERO)
        {
            TMO_DLL_Delete (pFacInFrrList,
                            (tTMO_DLL_NODE *)
                            & (pRsvpTeTnlInfo->FacInFrrProtTnl));
        }
    }

    /* Backup tunnel deletion *
     * 1) Delete the protected tnls for which it is acting as a backup tnl 
     * 2) Delete the backup tnl from the interface entry.*/
    pFacFrrList = &(RPTE_FRR_FAC_PROT_TNL_LIST (pRsvpTeTnlInfo));
    if (TMO_DLL_Count (pFacFrrList) != RPTE_ZERO)
    {
        pFacFrrProtTnl = TMO_DLL_First (pFacFrrList);
        while (pFacFrrProtTnl != NULL)
        {
            pTmpRsvpTeTnlInfo = (tRsvpTeTnlInfo *)
                (((FS_ULONG) pFacFrrProtTnl
                  - (RPTE_OFFSET (tRsvpTeTnlInfo, FacFrrProtTnl))));
            /* making the protected tnl's bypass tnl NULL */
            RPTE_FRR_OUT_TNL_INFO (pTmpRsvpTeTnlInfo) = NULL;
            TMO_DLL_Delete (pFacFrrList, pFacFrrProtTnl);
            pFacFrrProtTnl = TMO_DLL_First (pFacFrrList);

            RSVPTE_TNL_PROT_IF_INDEX
                (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
            RSVPTE_TE_TNL_FRR_BKP_TNL_INDEX
                (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
            RSVPTE_TE_TNL_FRR_BKP_TNL_INST
                (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
            MEMSET (RSVPTE_TE_TNL_FRR_BKP_TNL_INGRESS_ID
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)), RSVPTE_ZERO,
                    ROUTER_ID_LENGTH);
            MEMSET (RSVPTE_TE_TNL_FRR_BKP_TNL_EGRESS_ID
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)), RSVPTE_ZERO,
                    ROUTER_ID_LENGTH);
            RSVPTE_TE_TNL_FRR_ONE2ONE_PLR_ID
                (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
            RSVPTE_TE_TNL_FRR_ONE2ONE_SENDER_ADDR_TYPE
                (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
            RSVPTE_TE_TNL_FRR_ONE2ONE_SENDER_ADDR
                (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
            RSVPTE_TE_TNL_FRR_ONE2ONE_AVOID_ADDR_TYPE
                (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
            RSVPTE_TE_TNL_FRR_ONE2ONE_AVOID_ADDR
                (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
            RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS
                (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) =
                TE_TNL_FRR_PROT_STATUS_PARTIAL;
            RSVPTE_TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH (RPTE_TE_TNL
                                                      (pTmpRsvpTeTnlInfo)) =
                RSVPTE_ZERO;

            RpteUtlSetDetourStatus (pTmpRsvpTeTnlInfo, RPTE_FALSE);
            RSVPTE_TE_TNL_FRR_DETOUR_MERGING
                (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;

            RpteResetPlrStatus (pTmpRsvpTeTnlInfo);
        }
    }

    pFacInFrrList = &(pRsvpTeTnlInfo->FacInFrrList);

    if (TMO_DLL_Count (pFacInFrrList) != RPTE_ZERO)
    {
        pFacInFrrProtTnl = TMO_DLL_First (pFacInFrrList);
        while (pFacInFrrProtTnl != NULL)
        {
            pTmpRsvpTeTnlInfo = (tRsvpTeTnlInfo *)
                (((FS_ULONG) pFacInFrrProtTnl
                  - (RPTE_OFFSET (tRsvpTeTnlInfo, FacInFrrProtTnl))));
            RPTE_FRR_IN_TNL_INFO (pTmpRsvpTeTnlInfo) = NULL;
            TMO_DLL_Delete (pFacInFrrList, pFacInFrrProtTnl);
            pFacInFrrProtTnl = TMO_DLL_First (pFacInFrrList);
            RPTE_FRR_NODE_STATE (pTmpRsvpTeTnlInfo) &=
                (UINT1) (~(RPTE_FRR_NODE_STATE_MP));
        }
    }

    u4ProtIfIndex = RSVPTE_TNL_PROT_IF_INDEX (RPTE_TE_TNL (pRsvpTeTnlInfo));
    if ((RpteGetIfEntry ((INT4) u4ProtIfIndex, &pIfFacEntry) == RPTE_SUCCESS) &&
        (pIfFacEntry != NULL))
    {
        pIfFacTnlsList = (tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfFacEntry));
        TMO_DLL_Scan (pIfFacTnlsList, pIfFacTnlsNode, tTMO_DLL_NODE *)
        {
            pTmpFacilityRpteBkpTnl = (tFacilityRpteBkpTnl *) (pIfFacTnlsNode);

            if ((RPTE_FRR_PROT_IF_INDEX (pTmpFacilityRpteBkpTnl)
                 == u4ProtIfIndex)
                && (RPTE_FRR_FACILITY_BKP_TNL_INDEX (pTmpFacilityRpteBkpTnl)
                    == u4TunnelIndex)
                &&
                (RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID
                 (pTmpFacilityRpteBkpTnl) == u4TunnelIngressLSRId)
                &&
                (RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID
                 (pTmpFacilityRpteBkpTnl) == u4TunnelEgressLSRId))
            {
                pTmpFacilityRpteBkpTnl->pRsvpTeTnlInfo = NULL;
            }
        }
    }

    pNbrEntry = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);
    if (pNbrEntry != NULL)
    {
        if (NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) == RPTE_TRUE)
        {
            RpteRRSRefreshThreshold (pNbrEntry, RPTE_ZERO);
        }
    }

    pNbrEntry = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
    if (pNbrEntry != NULL)
    {
        if (NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) == RPTE_TRUE)
        {
            RpteRRSRefreshThreshold (pNbrEntry, RPTE_ZERO);
        }
    }

    /* Call Update TC to release the reserved resources */
    if (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL)
    {
        /* Update Mlib & associated Labels */
        if ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS)
            || (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INTERMEDIATE))
        {
            pRsvpTeBaseTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);

            /* Indicate the applications (l2VPN, LDP over RSVP etc..) 
             * to delete the MP ILM for the protected tunnel */
            if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) ==
                 RPTE_FRR_BACKUP_TNL) && (pRsvpTeBaseTnlInfo != NULL)
                && (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeBaseTnlInfo) ==
                    RPTE_FRR_PROTECTED_TNL)
                && (RSVPTE_TNL_NODE_RELATION (pRsvpTeBaseTnlInfo) ==
                    RPTE_EGRESS))
            {
                TeSigNotifyFrrTnlMpHwIlmAddOrDel
                    (RPTE_TE_TNL (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo)),
                     RPTE_TE_TNL (pRsvpTeTnlInfo), FALSE);
            }
            /* Do not remove the hw entry when the RSVP-TE component is made
             * shut with GR enabled
             * */
            if (gpRsvpTeGblInfo->u1GrProgressState !=
                RPTE_GR_SHUT_DOWN_IN_PROGRESS)
            {
                RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_DELETE,
                                          RSVPTE_ZERO, pRsvpTeTnlInfo,
                                          GMPLS_SEGMENT_FORWARD);
                RpteRhFreeLabel (PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                               (pRsvpTeTnlInfo)),
                                 &pRsvpTeTnlInfo->DownstrInLbl);
                pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
            }
        }
        else
        {
            pRsvpTeBaseTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);

            /* Indicate the applications (l2VPN, LDP over RSVP etc..) 
             * to delete the MP ILM for the protected tunnel */
            if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) ==
                 RPTE_FRR_BACKUP_TNL) && (pRsvpTeBaseTnlInfo != NULL)
                && (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeBaseTnlInfo) ==
                    RPTE_FRR_PROTECTED_TNL)
                &&
                (RSVPTE_TE_TNL_FRR_DETOUR_ACTIVE
                 (RPTE_TE_TNL (pRsvpTeBaseTnlInfo)) == RPTE_TRUE)
                && (RSVPTE_TNL_NODE_RELATION (pRsvpTeBaseTnlInfo) !=
                    RPTE_INGRESS))
            {
                RSVPTE_DBG1 (RSVPTE_UTL_PRCS,
                             "\n ILM delete called for label=%d\n",
                             pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl);

                RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_DELETE,
                                          RSVPTE_ZERO, pRsvpTeTnlInfo,
                                          GMPLS_SEGMENT_FORWARD);
                RpteRhFreeLabel (PSB_IF_ENTRY
                                 (RSVPTE_TNL_PSB (pRsvpTeBaseTnlInfo)),
                                 &pRsvpTeBaseTnlInfo->DownstrInLbl);
                pRsvpTeBaseTnlInfo->DownstrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
                pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
            }
            else
            {
                RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_TNL_DELETE,
                                          RSVPTE_ZERO, pRsvpTeTnlInfo,
                                          GMPLS_SEGMENT_FORWARD);
            }
        }

        if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_EGRESS)
        {
            RpteDSRelTrafficControl (pRsvpTeTnlInfo);
            RpteRhUpdateTrafficControl (pRsvpTeTnlInfo, u1ResvRsrc,
                                        &u1TmpVar, &ErrorSpec);
        }
        else if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
        {
            RpteRhUpdateTrafficControl (pRsvpTeTnlInfo, u1ResvRsrc,
                                        &u1TmpVar, &ErrorSpec);
        }
    }

    if (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL)
    {
        TmrStopTimer (RSVP_GBL_TIMER_LIST,
                      &RSB_TIMER (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)));
    }

    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL)
    {
        TmrStopTimer (RSVP_GBL_TIMER_LIST,
                      &PSB_TIMER (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));

        if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
        {
            if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS)
            {
                RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_TNL_DELETE,
                                          RSVPTE_ZERO, pRsvpTeTnlInfo,
                                          GMPLS_SEGMENT_REVERSE);
            }
            else if (pRsvpTeTnlInfo->pPsb->pOutIfEntry != NULL)
            {
                RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_DELETE,
                                          RSVPTE_ZERO, pRsvpTeTnlInfo,
                                          GMPLS_SEGMENT_REVERSE);
                RpteRhFreeLabel (PSB_OUT_IF_ENTRY
                                 (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                                 &pRsvpTeTnlInfo->UpStrInLbl);
                pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
            }

            if (pRsvpTeTnlInfo->pTeTnlInfo->b1IsResourceReserved == RPTE_TRUE)
            {
                pPathTe =
                    &SENDER_TSPEC_OBJ (&PSB_SENDER_TSPEC_OBJ
                                       (pRsvpTeTnlInfo->pPsb));
                reqResBw = OSIX_NTOHF (pPathTe->TBParams.fPeakRate);

                if (RpteTlmUpdateTrafficControl (pRsvpTeTnlInfo,
                                                 pRsvpTeTnlInfo->pTeTnlInfo->
                                                 u4UpStrDataTeLinkIf, reqResBw,
                                                 u1ResvRsrc,
                                                 &u1ResvRefreshNeeded,
                                                 GMPLS_SEGMENT_REVERSE) ==
                    RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteRelTnlEntryFunc : "
                                "RpteTlmUpdateTrafficControl failed\n");
                }
            }
        }

        /* Check if the DownStream InLabel is Valid, if it is valid,
         * its because label has been requested through label set feature but
         * tunnel is not able to setup. So, the label should be freed here. */
        if (pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl != MPLS_INVALID_LABEL)
        {
            RpteRhFreeLabel (PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                           (pRsvpTeTnlInfo)),
                             &pRsvpTeTnlInfo->DownstrInLbl);
            pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
        }
    }

    /*
     * Delete the RRO back off Timer from the Timer list, which is started
     * if the Node is of type Ingress/Egress.
     */
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_TNL_RRO_TIMER (pRsvpTeTnlInfo));

    /* FRR related timers stop */
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_CSPF_TIMER (pRsvpTeTnlInfo));
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_GBL_REVERT_TIMER (pRsvpTeTnlInfo));

    TmrStopTimer (RSVP_GBL_TIMER_LIST, &(pRsvpTeTnlInfo->AdminStatusTimer));

    TmrStopTimer (RSVP_GBL_TIMER_LIST, &(pRsvpTeTnlInfo->RecoveryPathTimer));

    /* Reoptimization Timer stop */
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_REOPTIMIZE_TIMER (pRsvpTeTnlInfo));
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_ERO_CACHE_TIMER (pRsvpTeTnlInfo));
    TmrStopTimer (RSVP_GBL_TIMER_LIST,
                  &RPTE_LINK_UP_WAIT_TIMER (pRsvpTeTnlInfo));

    TmrStopTimer (RSVP_GBL_TIMER_LIST,
                  &RPTE_EVT_TO_L2VPN_TIMER (pRsvpTeTnlInfo));

    if (RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo) != NULL)
    {
        pTimerBlock = RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo);
        /* Timer associated with the error message is stopped */
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER (pTimerBlock));

        /* Deleting the entry from the Trie Data base and releasing the
         * timer block */
        RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_LCL_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
        MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID, (UINT1 *) pTimerBlock);
        RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
    }

    if (RPTE_TNL_RE_TIMER_BLOCK (pRsvpTeTnlInfo) != NULL)
    {
        pTimerBlock = RPTE_TNL_RE_TIMER_BLOCK (pRsvpTeTnlInfo);
        /* Timer associated with the error message is stopped */
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER (pTimerBlock));

        /* Deleting the entry from the Trie Data base and releasing the
         * timer block */
        RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_LCL_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
        MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID, (UINT1 *) pTimerBlock);
        RPTE_TNL_RE_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
    }

    if (pRsvpTeTnlInfo->pRPTimerBlock != NULL)
    {
        pTimerBlock = pRsvpTeTnlInfo->pRPTimerBlock;
        /* Timer associated with the error message is stopped */
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER (pTimerBlock));

        /* Deleting the entry from the Trie Data base and releasing the
         * timer block */
        RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_LCL_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
        MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID, (UINT1 *) pTimerBlock);
        pRsvpTeTnlInfo->pRPTimerBlock = NULL;
    }

    if ((RPTE_TNL_PT_TIMER_BLOCK (pRsvpTeTnlInfo) != NULL) ||
        (RPTE_TNL_RT_TIMER_BLOCK (pRsvpTeTnlInfo) != NULL) ||
        (RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) == RPTE_FALSE))
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteRelTnlEntryFunc : PT/RT timer exist"
                    "or TNL not set to DEL - INTMD-EXIT\n");
        return RPTE_SUCCESS;
    }

    /* The TE module is called to delete the structure pointed by
     * pInTePathListInfo */
    if (RSVPTE_TNL_INERHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL)
    {
        rpteTeDeleteHopListInfo (RSVPTE_TNL_INERHOP_LIST_INFO (pRsvpTeTnlInfo));
    }

    if (TMO_SLL_Count (&pRsvpTeTnlInfo->NewSubObjList) != RSVPTE_ZERO)
    {
        RpteUtlFreeNewSubObjList (&pRsvpTeTnlInfo->NewSubObjList);
    }

    if (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL)
    {
        RpteUtlDeleteRsb (RSVPTE_TNL_RSB (pRsvpTeTnlInfo));
    }

    /* The No Of Tunnels Count is decremented */
    RpteUtlDecTnlCount (pRsvpTeTnlInfo);

    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL)
    {
        /* The Associated PSB are marked free  */
        RpteUtlDeletePsb (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    }
    RSVPTE_DBG (RSVPTE_UTL_PRCS, "UTIL : ROW STATUS NOTINSERVICE \n");

    /* Delete the TnlInfo structure from the TE module */
    pTmpTeTnlInfo = RPTE_TE_TNL (pRsvpTeTnlInfo);
    /* Only possibility is the Outgoing ArHopList. Hence Deleted */
    if ((RPTE_TNL_OWNER (pRsvpTeTnlInfo) != RPTE_TNL_OWNER_RSVP) &&
        (RSVPTE_OUT_ARHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL) &&
        (gpRsvpTeGblInfo->u1GrProgressState != RPTE_GR_SHUT_DOWN_IN_PROGRESS))
    {
        rpteTeDeleteArHopListInfo (RSVPTE_OUT_ARHOP_LIST_INFO (pRsvpTeTnlInfo));
        /* Trap Generation If possible */
        RSVPTE_DBG (RSVPTE_UTL_PRCS,
                    "UTIL : Tunnel is Created by TE Module - Deleted in "
                    "Sig Level\n");
    }

    if ((pRsvpTeTnlInfo->pFrrOutTePathListInfo != NULL) &&
        (gpRsvpTeGblInfo->u1GrProgressState != RPTE_GR_SHUT_DOWN_IN_PROGRESS))
    {
        rpteTeDeleteHopListInfo (pRsvpTeTnlInfo->pFrrOutTePathListInfo);
        pRsvpTeTnlInfo->pFrrOutTePathListInfo = NULL;
    }
    /* Below if condition is for E2E LSP over FA LSP */
    if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMapIndex != MPLS_ZERO)
    {
        u4IngressId = RpteUtilConvertToInteger
            (pRsvpTeTnlInfo->pTeTnlInfo->TnlMapIngressLsrId);
        u4EgressId = RpteUtilConvertToInteger
            (pRsvpTeTnlInfo->pTeTnlInfo->TnlMapEgressLsrId);
        u4IngressId = OSIX_NTOHL (u4IngressId);
        u4EgressId = OSIX_NTOHL (u4EgressId);
        if (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                    pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMapIndex,
                                    pRsvpTeTnlInfo->pTeTnlInfo->
                                    u4TnlMapInstance, u4IngressId, u4EgressId,
                                    &pFARsvpTeTnlInfo) == RPTE_SUCCESS)
        {
            TMO_SLL_Delete (&pFARsvpTeTnlInfo->StackTnlList,
                            &pRsvpTeTnlInfo->NextStackTnl);
        }
    }
    /*Below if condition is for FA LSP */
    if ((pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP) &&
        (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INTERMEDIATE))
    {
        UTL_SLL_OFFSET_SCAN (&(pRsvpTeTnlInfo->StackTnlList),
                             pRsvpFATnlInfo, pTmpFARsvpTeTnlInfo,
                             tRsvpTeTnlInfo *)
        {
            RpteNhHandlePsbOrRsbOrHelloTimeOut
                (pRsvpFATnlInfo, RPTE_LSP_LOCALLY_FAILED);
        }

        if ((pRsvpTeTnlInfo->pTeTnlInfo->u4BkpTnlIndex == RPTE_ZERO) ||
            (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
             MPLS_TE_DEDICATED_ONE2ONE))
        {
            RpteDeleteFATeLink (pRsvpTeTnlInfo);
        }

        TMO_SLL_Clear (&pRsvpTeTnlInfo->StackTnlList);

        if ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode ==
             TE_TNL_MODE_UNIDIRECTIONAL))
        {
            if (RptePortCfaDeleteMplsTnlIf (pRsvpTeTnlInfo, PATH_MSG) ==
                RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS,
                            "FA TE Link Deletion Failed in CFA..\n");
            }
        }

    }

    /* Delete Tnl from NotifyRecipientTree */
    if (gpRsvpTeGblInfo->NotifyRecipientTree != NULL)
    {
        pNotifyRecipient =
            RBTreeGetFirst (gpRsvpTeGblInfo->NotifyRecipientTree);

        while (pNotifyRecipient != NULL)
        {
            TMO_DYN_SLL_Scan (&pNotifyRecipient->NotifyMsgTnlList,
                              pNotifyMsgTnl, pTempNotifyMsgTnl, tNotifyMsgTnl *)
            {
                if (pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.u2TnlId ==
                    RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo))
                {
                    TMO_SLL_Delete (&pNotifyRecipient->NotifyMsgTnlList,
                                    &pNotifyMsgTnl->NextTnl);
                    MemReleaseMemBlock (RSVPTE_NOTIFY_TNL_POOL_ID,
                                        (UINT1 *) (pNotifyMsgTnl));
                    RSVPTE_DBG1 (RSVPTE_UTL_PRCS,
                                 "UTIL : Tunnel: %u removed from NotifyRecipientTree\n",
                                 RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo));
                    break;
                }
            }
            if (TMO_SLL_Count (&pNotifyRecipient->NotifyMsgTnlList) ==
                RPTE_ZERO)
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS,
                            "UTIL : All the Tunnels deleted from NotifyRecipientTree\n");
                RpteNhDelRBNode (pNotifyRecipient);
                pNotifyRecipient =
                    RBTreeGetFirst (gpRsvpTeGblInfo->NotifyRecipientTree);
            }
            else
            {
                pNotifyRecipient =
                    RBTreeGetNext (gpRsvpTeGblInfo->NotifyRecipientTree,
                                   (tRBElem *) pNotifyRecipient, NULL);
            }
        }

    }

    /* Delete Tnl from the RbTree */
    if (RpteDelTnlFromTnlTable
        (pRsvpTeGblInfo, pRsvpTeTnlInfo, u4TunnelIndex, u4TunnelInstance,
         u4TunnelIngressLSRId, u4TunnelEgressLSRId,
         u1RemoveFlag) != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_UTL_PRCS,
                    "UTIL : Tunnel is not removed from the RbTree\n");
    }

    MEMSET (pTmpTeTnlInfo->au1BkpNextHopMac, RSVPTE_ZERO, MAC_ADDR_LEN);

    MEMSET (pTmpTeTnlInfo->au1NextHopMac, RSVPTE_ZERO, MAC_ADDR_LEN);

    pTmpTeTnlInfo->u1TnlRelStatus = RPTE_TNLREL_DONE;

    /* Set the Tunnel status as not synchronized at TE level */
    if (gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_SHUT_DOWN_IN_PROGRESS)
    {
        TeSigSetGrSynchronizedStatus (pTmpTeTnlInfo, TNL_GR_NOT_SYNCHRONIZED);
    }
    /* Non recovered tunnel will not have RSB details. Hence ILM/TNL deletion will not be
     * happened in this flow. To delete those information TeSigGrDeleteNonSynchronizedTnls
     * function is called here...
     *  */
    else if ((gpRsvpTeGblInfo->u1GrProgressState ==
              RPTE_GR_RECOVERY_IN_PROGRESS) && (u1IsGrTnl == RPTE_YES))
    {
        TeSigGrDeleteNonSynchronizedTnls ();
    }
    else if (rpteTeDeleteTnlInfo (pTmpTeTnlInfo, u1CallerId) != RPTE_TE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_UTL_PRCS,
                    "UTIL : Tunnel is not deleted in the TE Module\n");
    }

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteRelTnlEntryFunc : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteUtlCompareRRList                                   */
/* Description     : This function Compares two RRList structures           */
/* Input (s)       : RrHopList - PktMap RrHopList structure                 */
/*                   OutArHopList - Tnl Info OutArHopList structure         */
/* Output (s)      : None                                                   */
/* Returns         : If both structures are same, then returns RPTE_EQUAL.  */
/*                   else returns RPTE_NOT_EQUAL                            */
/****************************************************************************/
INT1
RpteUtlCompareRRList (tTMO_SLL RrHopList, tTMO_SLL OutArHopList)
{

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareRRList : ENTRY \n");

    if ((TMO_SLL_Count (&RrHopList)) != (TMO_SLL_Count (&OutArHopList)))
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareRRList : INTMD-EXIT \n");
        return RPTE_NOT_EQUAL;
    }
    if (RpteCompRRO (&RrHopList, &OutArHopList) != RPTE_EQUAL)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareRRList : INTMD-EXIT \n");
        return RPTE_NOT_EQUAL;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareRRList : EXIT \n");
    return RPTE_EQUAL;

}

/****************************************************************************/
/* Function Name   : RpteUtlResvManObjHandler                               */
/* Description     : This function Fills the Mandatory entries For Reserve  */
/*                   Message Processing from the Packet Map                 */
/* Input (s)       : pPktMap            - Pointer to Packet Map             */
/*                   ppRsvpTeSession    - Pointer to pRsvpTeSession         */
/*                   ppRsvpHop          - Pointer to the pRsvpHop           */
/*                   ppFlowSpec         - Pointer to the pFlowSpec          */
/*                   ppRsvpTeFilterSpec - Pointer to the pRsvpTeFilterSpec  */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS  - If all Entries are Ok.                 */
/****************************************************************************/
INT1
RpteUtlResvManObjHandler (tPktMap * pPktMap,
                          tRsvpTeSession ** ppRsvpTeSession,
                          tRsvpHop ** ppRsvpHop,
                          tFlowSpec ** ppFlowSpec,
                          tRsvpTeFilterSpec ** ppRsvpTeFilterSpec)
{
    tFlowSpecObj       *pFlowSpecObj = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlResvManObjHandler : ENTRY \n");

    *ppRsvpTeSession = &RPTE_SSN_OBJ (PKT_MAP_RPTE_SSN_OBJ (pPktMap));
    if (pPktMap->pRsvpHopObj != NULL)
    {
        *ppRsvpHop = &RSVP_HOP_OBJ (PKT_MAP_RSVP_HOP_OBJ (pPktMap));
    }
    else if (pPktMap->pIfIdRsvpHopObj != NULL)
    {
        *ppRsvpHop = &pPktMap->pIfIdRsvpHopObj->RsvpHop;
    }
    else if (pPktMap->pIfIdRsvpNumHopObj != NULL)
    {
        *ppRsvpHop = &pPktMap->pIfIdRsvpNumHopObj->RsvpHop;
    }

    /*  Only one of the FlowSpec will be present in the PktMap. If both are
     * present the message will be dropped in the PvmModule itself.
     *
     */
    pFlowSpecObj = PKT_MAP_FLOW_SPEC_OBJ (pPktMap);
    if (pFlowSpecObj != NULL)
    {
        *ppFlowSpec = &FLOW_SPEC_OBJ (pFlowSpecObj);
    }
    else
    {
        return RPTE_FAILURE;
    }

    if (pPktMap->u1RROErrVal == RPTE_BAD_STRICT_NODE)
    {
        RptePvmSendResvErr (pPktMap, RPTE_ROUTE_PROB, pPktMap->u1RROErrVal);
        return RPTE_FAILURE;
    }
    *ppRsvpTeFilterSpec = &RPTE_FILTER_SPEC_OBJ (PKT_MAP_RPTE_FILTER_SPEC_OBJ
                                                 (pPktMap));
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlResvManObjHandler : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                                                                           */
/* Function Name   : RpteUtlRhPreparePktMapRROErr                            */
/* Description     : This function Prepares PktMap struct for the RRO err.   */
/* Input (s)       : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo Structure.    */
/* Output (s)      : None                                                    */
/* Returns         : &PktMap.                                                */
/*                                                                           */
/*---------------------------------------------------------------------------*/
VOID
RpteUtlRhPreparePktMapRROErr (tPktMap * pPktMap, tRsvpTeTnlInfo * pCtTnlInfo)
{
    UINT4               u4TmpAddr = 0;
    tObjHdr             ObjHdr;
    tRsb               *pRsb = NULL;
    tFlowSpec          *pFlowSpec = NULL;
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlRhPreparePktMapRROErr: ENTRY \n");
    pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
    /* Copy Inc If Id from Tnl Info Rsb */
    PKT_MAP_IF_ENTRY (pPktMap) = RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pCtTnlInfo));

    /* Copy Src Addr */
    IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) =
        IF_ENTRY_ADDR (RSB_OUT_IF_ENTRY (pRsb));

    /* Copy RsvpTeSession */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pPktMap->pRsvpTeSsnObj->ObjHdr = ObjHdr;
    u4TmpAddr = RSVPTE_TNL_EGRESS (pCtTnlInfo);
    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeSsnObj->RsvpTeSession.TnlDestAddr,
            (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPktMap->pRsvpTeSsnObj->RsvpTeSession.u2Rsvd = RSVPTE_ZERO;
    pPktMap->pRsvpTeSsnObj->RsvpTeSession.u2TnlId =
        (UINT2) OSIX_HTONS (pCtTnlInfo->pTeTnlInfo->u4TnlIndex);
    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pCtTnlInfo)), u4TmpAddr);
    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeSsnObj->RsvpTeSession.ExtnTnlId,
            (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    /* Copy RsvpHop info */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (sizeof (tRsvpHopObj));
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_RSVP_HOP_CLASS_NUM_TYPE);

    pPktMap->pRsvpHopObj->ObjHdr = ObjHdr;
    MEMCPY (&pPktMap->pRsvpHopObj->RsvpHop, &RSB_NEXT_RSVP_HOP
            (RSVPTE_TNL_RSB (pCtTnlInfo)), sizeof (tRsvpHop));

    /* Fill  Style Obj * */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (sizeof (tStyleObj));
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) = OSIX_HTONS (IPV4_STYLE_CLASS_NUM_TYPE);

    pPktMap->pStyleObj->ObjHdr = ObjHdr;

    MEMCPY (&(pPktMap->pStyleObj->Style), &(pRsb->Style), sizeof (tStyle));

    /* Copy FlowSpec  Obj */
    pFlowSpec = &RSB_FLOW_SPEC (pRsb);

    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (sizeof (tFlowSpecObj));
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_FLOW_SPEC_CLASS_NUM_TYPE);
    pPktMap->pFlowSpecObj->ObjHdr = ObjHdr;
    MEMCPY (&(pPktMap->pFlowSpecObj->FlowSpec), pFlowSpec, sizeof (tFlowSpec));

    /* Copy RsvpTeFilterSpecObj */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (RPTE_IPV4_FLTR_SPEC_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE);

    pPktMap->pRsvpTeFilterSpecObj->ObjHdr = ObjHdr;

    u4TmpAddr = RSVPTE_TNL_INGRESS (pCtTnlInfo);
    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.
            TnlSndrAddr, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPktMap->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.u2Rsvd = RSVPTE_ZERO;
    pPktMap->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.u2LspId =
        (UINT2) OSIX_HTONS (pCtTnlInfo->pTeTnlInfo->u4TnlInstance);

    PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlRhPreparePktMapRROErr: EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlFreeNewSubObjList
 * Description     : This function Frees the NewSubObject list.
 * Input (s)       : pNewSubObjList - Pointer to Rcvd NewSubObj list.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteUtlFreeNewSubObjList (tTMO_SLL * pNewSubObjList)
{
    tNewSubObjInfo     *pNewSubObjInfo = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlFreeNewSubObjList: ENTRY \n");
    pNewSubObjInfo = (tNewSubObjInfo *) TMO_SLL_First (pNewSubObjList);
    while (pNewSubObjInfo != NULL)
    {
        MemReleaseMemBlock (RSVPTE_AR_HOP_POOL_ID,
                            (UINT1 *) pNewSubObjInfo->pSubObj);
        TMO_SLL_Delete (pNewSubObjList,
                        (tTMO_SLL_NODE *) (&pNewSubObjInfo->NextSubNode));

        MemReleaseMemBlock (RSVPTE_NEWSUB_OBJ_POOL_ID,
                            (UINT1 *) pNewSubObjInfo);
        pNewSubObjInfo = (tNewSubObjInfo *) TMO_SLL_First (pNewSubObjList);
    }
    TMO_SLL_Init (pNewSubObjList);
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlFreeNewSubObjList: EXIT \n");
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlFreeRROList
 * Description     : This function Frees RRO list.
 * Input (s)       : pRROList - Pointer to Rcvd RRO list.
 *                   u4TnlArHopListIndex - index  to the ArHopListInfo 
 *                   structure. 
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteUtlFreeRROList (tTMO_SLL * pRROList)
{

    tRsvpTeArHopInfo   *pRsvpTeArHopInfo = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlFreeRROList: ENTRY \n");

    pRsvpTeArHopInfo = (tRsvpTeArHopInfo *) TMO_SLL_First (pRROList);
    while (pRsvpTeArHopInfo != NULL)
    {

        TMO_SLL_Delete (pRROList,
                        (tTMO_SLL_NODE *) (&pRsvpTeArHopInfo->NextHop));

        MemReleaseMemBlock (RSVPTE_RRHOP_POOL_ID, (UINT1 *) pRsvpTeArHopInfo);
        pRsvpTeArHopInfo = (tRsvpTeArHopInfo *) TMO_SLL_First (pRROList);
    }
    TMO_SLL_Init (pRROList);

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlFreeRROList: EXIT \n");
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlFreeEROList
 * Description     : This function Frees ERO list.
 * Input (s)       : pEROList - Pointer to Rcvd ERO list.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteUtlFreeEROList (tTMO_SLL * pEROList)
{
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlFreeEROList: ENTRY \n");
    pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_First (pEROList);
    while (pRsvpTeErHopInfo != NULL)
    {

        TMO_SLL_Delete (pEROList,
                        (tTMO_SLL_NODE *) (&pRsvpTeErHopInfo->NextHop));

        MemReleaseMemBlock (RSVPTE_ERHOP_POOL_ID, (UINT1 *) pRsvpTeErHopInfo);
        pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_First (pEROList);
    }
    TMO_SLL_Init (pEROList);

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlFreeEROList: EXIT \n");
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlPhPreparePktMapRROErr
 * Description     : This function Prepares PktMap struct for the RRO err.
 * Input (s)       : pPktMap - Pointer to PktMap structure.
 *                   pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo Structure.
 * Output (s)      : None
 * Returns         : None.
 */
/*---------------------------------------------------------------------------*/
VOID
RpteUtlPhPreparePktMapRROErr (tPktMap * pPktMap,
                              tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    UINT4               u4TmpAddr = 0;
    tObjHdr             ObjHdr;
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlPhPreparePktMapRROErr: ENTRY \n");
    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL)
    {
        /* Copy Inc If Id from Tnl Info PSB */
        PKT_MAP_IF_ENTRY (pPktMap) =
            PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));

        /* Copy Dest Addr */
        pPktMap->pRsvpHopObj->RsvpHop.u4HopAddr =
            OSIX_HTONL (RSVP_HOP_ADDR
                        (&PSB_RSVP_HOP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))));

        /* Copy Src Addr */
        IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) =
            IF_ENTRY_ADDR (PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
    }

    /* Copy Session */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pPktMap->pRsvpTeSsnObj->ObjHdr = ObjHdr;
    u4TmpAddr = RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo);

    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeSsnObj->RsvpTeSession.TnlDestAddr,
            (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPktMap->pRsvpTeSsnObj->RsvpTeSession.u2Rsvd = RSVPTE_ZERO;
    pPktMap->pRsvpTeSsnObj->RsvpTeSession.u2TnlId =
        (UINT2) OSIX_HTONS (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex);

    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo)), u4TmpAddr);

    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeSsnObj->RsvpTeSession.ExtnTnlId,
            (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    /* Copy SenderTemplate */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (RPTE_IPV4_SNDR_TMP_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE);

    pPktMap->pRsvpTeSndrTmpObj->ObjHdr = ObjHdr;

    u4TmpAddr = RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo);

    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.TnlSndrAddr,
            (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPktMap->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.u2Rsvd = RSVPTE_ZERO;

    pPktMap->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.u2LspId =
        (UINT2) OSIX_HTONS (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);

    /* 
     * Copy SenderTspec - If TnlRsrcEntryIndex is zero COS TSpec, 
     * else Int-serv TSpec 
     */

    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL)
    {
        MEMCPY (PKT_MAP_SENDER_TSPEC_OBJ (pPktMap),
                &PSB_SENDER_TSPEC_OBJ (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                sizeof (tSenderTspecObj));
    }

    PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlPhPreparePktMapRROErr: EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlCleanTeLists
 * Description     : This function releases ErHopLists, ArHopLists and Traffic
 *                   params associated 
 * Input (s)       : pPktMap - Pointer to Packet Map
 *                   pRsvpTeTnlInfo - Pointer to the RsvpTeTnlInfo 
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteUtlCleanTeLists (const tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    UINT1               u1RemoveFlag = RPTE_FALSE;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlCleanTeLists: ENTRY \n");

    UNUSED_PARAM (pPktMap);

    if (RPTE_TE_TNL (pRsvpTeTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlCleanTeLists: EXIT \n");
        return;
    }

    if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_FALSE) ||
        (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL) ||
        RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)) ||
        ((RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) == RPTE_FRR_NODE_STATE_UNKNOWN)
         && (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) == NULL)))
    {
        u1RemoveFlag = RPTE_TRUE;
    }

    RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                         TE_TNL_CALL_FROM_SIG, u1RemoveFlag);
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlCleanTeLists: EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlCleanPktMap
 * Description     : This function releases all the memory associoated
 *                   with pPktMap.
 * Input (s)       : pPktMap - Pointer to Packet Map
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteUtlCleanPktMap (tPktMap * pPktMap)
{
    tTMO_SLL           *pList = NULL;
    tMsgIdObjNode      *pMsgIdObjNode = NULL;
    tRecNotifyTnl      *pRecNotifyTnl = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlCleanPktMap: ENTRY \n");
    if (PKT_MAP_MSG_BUF (pPktMap) != NULL)
    {
        BUF_RELEASE (PKT_MAP_MSG_BUF (pPktMap));
        PKT_MAP_MSG_BUF (pPktMap) = NULL;
    }

    if (PKT_MAP_RSVP_PKT_ALLOC (pPktMap) == RPTE_PKTMAP_IP_ALLOC)
    {
        MemReleaseMemBlock (RSVPTE_IP_PKT_POOL_ID,
                            (UINT1 *) PKT_MAP_IP_PKT (pPktMap));
        PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_NO_ALLOC;
    }
    else if (PKT_MAP_RSVP_PKT_ALLOC (pPktMap) == RPTE_PKTMAP_RSVP_ALLOC)
    {
        MemReleaseMemBlock (RSVPTE_RSVP_PKT_POOL_ID,
                            (UINT1 *) PKT_MAP_RSVP_PKT (pPktMap));
        PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_NO_ALLOC;
    }

    if (TMO_SLL_Count (&pPktMap->ErObj.ErHopList) != RSVPTE_ZERO)
    {
        RpteUtlFreeEROList (&pPktMap->ErObj.ErHopList);
    }
    if (TMO_SLL_Count (&pPktMap->RrObj.RrHopList) != RSVPTE_ZERO)
    {
        RpteUtlFreeRROList (&pPktMap->RrObj.RrHopList);
    }
    if (TMO_SLL_Count (&pPktMap->RrObj.NewSubObjList) != RSVPTE_ZERO)
    {
        RpteUtlFreeNewSubObjList (&pPktMap->RrObj.NewSubObjList);
    }

    if (TMO_SLL_Count (&RPTE_PKT_MAP_MSG_ID_ACK_LIST (pPktMap)) != RPTE_ZERO)
    {
        pList = &RPTE_PKT_MAP_MSG_ID_ACK_LIST (pPktMap);
        pMsgIdObjNode = (tMsgIdObjNode *) TMO_SLL_First (pList);
        while (pMsgIdObjNode != NULL)
        {
            TMO_SLL_Delete (pList, &(pMsgIdObjNode->NextMsgIdObjNode));
            MemReleaseMemBlock (RSVPTE_MSG_ID_LIST_POOL_ID,
                                (UINT1 *) pMsgIdObjNode);
            pMsgIdObjNode = (tMsgIdObjNode *) TMO_SLL_First (pList);
        }
    }
    if (TMO_SLL_Count (&RPTE_PKT_MAP_MSG_ID_NACK_LIST (pPktMap)) != RPTE_ZERO)
    {
        pList = &RPTE_PKT_MAP_MSG_ID_NACK_LIST (pPktMap);
        pMsgIdObjNode = (tMsgIdObjNode *) TMO_SLL_First (pList);
        while (pMsgIdObjNode != NULL)
        {
            TMO_SLL_Delete (pList, &(pMsgIdObjNode->NextMsgIdObjNode));
            MemReleaseMemBlock (RSVPTE_MSG_ID_LIST_POOL_ID,
                                (UINT1 *) pMsgIdObjNode);
            pMsgIdObjNode = (tMsgIdObjNode *) TMO_SLL_First (pList);
        }
    }
    if (TMO_SLL_Count (&pPktMap->NotifyMsgTnlList) != RPTE_ZERO)
    {
        pList = &pPktMap->NotifyMsgTnlList;
        pRecNotifyTnl = (tRecNotifyTnl *) TMO_SLL_First (pList);
        while (pRecNotifyTnl != NULL)
        {
            TMO_SLL_Delete (pList, &(pRecNotifyTnl->NextRecNtfyTnl));
            MemReleaseMemBlock (RSVPTE_REC_NTFY_TNL_POOL_ID,
                                (UINT1 *) pRecNotifyTnl);
            pRecNotifyTnl = (tRecNotifyTnl *) TMO_SLL_First (pList);
        }
    }

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlCleanPktMap: EXIT \n");
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlFreeTnlNodeTnlTbl
 * Description     : This function frees the Tnl Node from Tnl RbTree.
 * Input (s)       : pTnlRbNode - Pointer to Tnl .
 *                   bFlag - RB node deletion to be done or not.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteUtlFreeTnlNodeTnlTbl (tRsvpTeTnlInfo * pTnlRbNode, BOOL1 bFlag)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlFreeTnlNodeTnlTbl: ENTRY \n");
    if (RPTE_TE_TNL (pTnlRbNode)->u1TnlRelStatus == TE_SIGMOD_TNLREL_AWAITED)
    {
        /* If Tnl release status is "waiting" meaning it is signalled 
         * from admin. So override the caller option.
         */
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pTnlRbNode,
                             TE_TNL_CALL_FROM_ADMIN, (UINT1) bFlag);
    }
    else
    {
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pTnlRbNode,
                             TE_TNL_CALL_FROM_SIG, (UINT1) bFlag);
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlFreeTnlNodeTnlTbl: EXIT \n");
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlDecTnlCount
 * Description     : This function Decrements the no of Tunnels Assoc Count from
 *                   Traffic Parameters and the Interface Entry.
 * Input (s)       : pRsvpTeTnlInfo - Pointer to TnlInfo structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteUtlDecTnlCount (const tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tIfEntry           *pIfEntry = NULL;
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlDecTnlCount: ENTRY \n");

    if ((RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL))
    {
        /* The Tunnels Count is decremented from the Interface Entry */
        if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS)
        {
            pIfEntry = PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
            if ((pIfEntry != NULL) &&
                (RPTE_IF_NUM_TNL_ASSOC (pIfEntry) > RSVPTE_ZERO))
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS,
                            "No of Tunnels Count Decremented at Inc IF\n");
                RPTE_IF_NUM_TNL_ASSOC (pIfEntry)--;
            }
        }
        if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_EGRESS)
        {
            pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
            if ((pIfEntry != NULL) &&
                (RPTE_IF_NUM_TNL_ASSOC (pIfEntry) > RSVPTE_ZERO))
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS,
                            "No of Tunnels Count Decremented at Out IF\n");
                RPTE_IF_NUM_TNL_ASSOC (pIfEntry)--;
            }
        }
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlDecTnlCount: EXIT \n");
    return;
}

/* NOTE: The following routines are taken from rsvputil.c of FutureRSVP */

/****************************************************************************/
/* Function Name   : RpteUtlBufChainedData                                  */
/* Description     : This function checks whether the buffer is chained or  */
/*                   not                                                    */
/* Input (s)       : pMsgBuf                                                */
/* Output (s)      : None                                                   */
/* Returns         : If Chained Buffer, then returns RPTE_YES. Else returns */
/*                   RPTE_NO                                                */
/****************************************************************************/
INT1
RpteUtlBufChainedData (const tMsgBuf * pMsgBuf)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlBufChainedData: ENTRY \n");
    if (BUF_FIRST_DATA_DESC (pMsgBuf) == BUF_LAST_DATA_DESC (pMsgBuf))
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlBufChainedData: INTMD-EXIT \n");
        return RPTE_NO;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlBufChainedData: EXIT \n");
    return RPTE_YES;
}

/****************************************************************************/
/* Function Name   : RpteUtlCheckSum                                        */
/* Description     : This function calculates the checksum for the Rsvp Pkt */
/* Input (s)       : pPkt - Ponter to Pkt                                   */
/*                   u2Size - Pkt Size                                      */
/* Output (s)      : None                                                   */
/* Returns         : Checksum of pPkt                                       */
/****************************************************************************/
UINT4
RpteUtlCheckSum (UINT1 *pPkt, UINT2 u2Size)
{
    UINT2              *pTmpPkt = NULL;
    UINT4               u4Sum;

    pTmpPkt = (UINT2 *) (VOID *) pPkt;
    u4Sum = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCheckSum: ENTRY \n");
    /* Sum half-words */
    while (u2Size > RSVPTE_ONE)
    {
        u4Sum = (UINT4) (u4Sum + (*pTmpPkt++));
        u2Size = (UINT2) (u2Size - RSVPTE_TWO);
    }

    /* Add left-over byte, if any */
    if (u2Size > RSVPTE_ZERO)
    {
        u4Sum = (UINT4) (u4Sum + (*(char *) pTmpPkt));
    }

    /* Fold 32-bit sum to 16 bits */
    u4Sum = (u4Sum & CHECK_SUM_MASK) + (u4Sum >> RSHIFT_16BITS);
    u4Sum = ~(u4Sum + (u4Sum >> RSHIFT_16BITS)) & CHECK_SUM_MASK;

    if (u4Sum == CHECK_SUM_MASK)
    {
        u4Sum = RSVPTE_ZERO;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCheckSum: EXIT \n");
    return ((UINT2) u4Sum);
}

/****************************************************************************/
/* Function Name   : RpteUtlInitPktMap                                      */
/* Description     : This function initlizes the PktMap structure           */
/* Input (s)       : tPktMap *pPktMap                                       */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteUtlInitPktMap (tPktMap * pPktMap)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlInitPktMap: ENTRY \n");

    MEMSET (pPktMap, RSVPTE_ZERO, sizeof (tPktMap));

    PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_NO_ALLOC;
    PKT_MAP_MSG_BUF (pPktMap) = NULL;
    PKT_MAP_RE_ROUTE_FLAG (pPktMap) = RPTE_NO;
    PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_NO;
    pPktMap->u1RROErrVal = RSVPTE_ZERO;
    PKT_MAP_RPTE_SSN_OBJ (pPktMap) = NULL;
    PKT_MAP_RPTE_LBL_OBJ (pPktMap) = NULL;
    PKT_MAP_RPTE_ADNL_LBL_OBJ (pPktMap) = NULL;
    pPktMap->pGenLblReqObj = NULL;
    pPktMap->pLblReqAtmRngObj = NULL;
    pPktMap->pLabelSetObj = NULL;
    PKT_MAP_RPTE_FILTER_SPEC_OBJ (pPktMap) = NULL;
    PKT_MAP_RPTE_SND_TMP_OBJ (pPktMap) = NULL;
    PKT_MAP_RPTE_ADNL_FILTER_SPEC_OBJ (pPktMap) = NULL;
    TMO_SLL_Init (&RPTE_PKT_MAP_MSG_ID_ACK_LIST (pPktMap));
    TMO_SLL_Init (&RPTE_PKT_MAP_MSG_ID_NACK_LIST (pPktMap));
    TMO_SLL_Init (&pPktMap->NotifyMsgTnlList);
    MEMSET (&RPTE_PKT_MAP_MSG_ID_OBJ (pPktMap), RPTE_ZERO, sizeof (tMsgIdObj));
    RPTE_PKT_MAP_MSG_ID_LIST (pPktMap) = NULL;
    RPTE_PKT_MAP_NBR_ENTRY (pPktMap) = NULL;
    RPTE_PKT_MAP_TNL_INFO (pPktMap) = NULL;
    RPTE_PKT_LSP_TNL_IF_OBJ (pPktMap) = NULL;

    RpteUtlDSInitPktMap (pPktMap);

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlInitPktMap: EXIT \n");
}

/****************************************************************************/
/* Function Name   : RpteUtlUdpEncapsRequired                               */
/* Description     : This function checks whether UDP encapsulation is      */
/*                   required for a pkt going out on interface pointed by   */
/*                   pIfEntry                                               */
/* Input (s)       : pIfEntry - Pointer to IfEntry                          */
/* Output (s)      : None                                                   */
/* Returns         : If UDP Encapsulation required, then returns RPTE_YES.  */
/*                   Else returns RPTE_NO                                   */
/****************************************************************************/
INT1
RpteUtlUdpEncapsRequired (const tIfEntry * pIfEntry)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlUdpEncapsRequired: ENTRY \n");
    if ((IF_ENTRY_UDP_REQUIRED (pIfEntry) == RPTE_SNMP_TRUE) ||
        (IF_ENTRY_UDP_NBRS (pIfEntry) != RSVPTE_ZERO))
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlUdpEncapsRequired: EXIT \n");
        return RPTE_YES;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlUdpEncapsRequired: INTMD-EXIT \n");
    return RPTE_NO;
}

/****************************************************************************/
/* Function Name   : RpteUtlMcastAddr                                       */
/* Description     : This function determines whether u4Addr is multicast   */
/*                   address or not                                         */
/* Input (s)       : u4Addr                                                 */
/* Output (s)      : None                                                   */
/* Returns         : If the address is multicast address, then returns      */
/*                   RPTE_YES. Else returns RPTE_NO                         */
/****************************************************************************/
INT1
RpteUtlMcastAddr (UINT4 u4Addr)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlMcastAddr: ENTRY \n");
    if ((u4Addr & RSVPTE_MCAST_ADDR_MASK) == RSVPTE_MCAST_IPV4ADDR)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlMcastAddr: EXIT \n");
        return RPTE_YES;
    }
    RSVPTE_DBG (RSVPTE_UTL_PRCS, "NOT a Multicast Address\n");
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlMcastAddr: INTMD-EXIT \n");
    return RPTE_NO;
}

/****************************************************************************/
/* Function Name   : RpteUtlCompareRsvpHop                                  */
/* Description     : This function Comapares two RsvpHop structures         */
/* Input (s)       : pRsvpHop1                                              */
/*                   pRsvpHop2                                              */
/* Output (s)      : None                                                   */
/* Returns         : If both structures are same, then returns RPTE_SUCCESS.*/
/*                   Else returns RPTE_FAILURE                              */
/****************************************************************************/
INT1
RpteUtlCompareRsvpHop (const tRsvpHop * pRsvpHop1, const tRsvpHop * pRsvpHop2)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareRsvpHop: ENTRY \n");
    if (MEM_CMP (pRsvpHop1, pRsvpHop2, sizeof (tRsvpHop)) == RSVPTE_ZERO)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareRsvpHop: EXIT \n");
        return RPTE_SUCCESS;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareRsvpHop: INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteUtlEncodeLih                                       */
/* Description     : This function encodes interface id into a Logical      */
/*                   Interface Handle                                       */
/* Input (s)       : pu4Lih - Pointer to Lih                                */
/*                   pIfEntry - Pointer to Interface Entry                  */
/* Output (s)      : The Lih Value is returned in pu4Lih                    */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteUtlEncodeLih (UINT4 *pu4Lih, tIfEntry * pIfEntry)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlEncodeLih: ENTRY \n");

    if ((pIfEntry != NULL) && (pu4Lih != NULL))
    {
        *pu4Lih = IF_ENTRY_IF_INDEX (pIfEntry);
        *pu4Lih = OSIX_HTONL (*pu4Lih);
    }

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlEncodeLih: EXIT \n");
}

/****************************************************************************/
/* Function Name   : RpteUtlDecodeLih                                       */
/* Description     : This function Decodes Lih into Interface Id            */
/* Input (s)       : u4Lih - Lih Value                                      */
/*                   pIfEntryId - Pointer to Interface Entry                */
/* Output (s)      : The decoded value is returned in pIfId                 */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteUtlDecodeLih (UINT4 u4Lih, tIfEntry * pIfEntry)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlDecodeLih: ENTRY \n");
    u4Lih = OSIX_NTOHL (u4Lih);
    MEMCPY ((UINT1 *) &IF_ENTRY_IF_INDEX (pIfEntry), (UINT1 *) &u4Lih,
            sizeof (UINT4));
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlDecodeLih: EXIT \n");
}

/****************************************************************************/
/* Function Name   : RpteUtlPsbRoutesToOutIf                                */
/* Description     : This function determines whether the Out interface list*/
/*                   of pPsb Contains OutIfId or not                        */
/* Input (s)       : pPsb  - Pointer to Psb                                 */
/*                   pIfEntry - Pointer to Interface Entry                  */
/*                   pRsvpHop - Pointer to Rsvp Hop                         */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_YES on success RPTE_NO on failure                 */
/****************************************************************************/
INT1
RpteUtlPsbRoutesToOutIf (const tPsb * pPsb, tIfEntry * pIfEntry,
                         tRsvpHop * pRsvpHop)
{
    UINT4               u4PsbLih = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlPsbRoutesToOutIf: ENTRY \n");

    if (PSB_OUT_IF_ENTRY (pPsb) != NULL)
    {
        RpteUtlEncodeLih (&u4PsbLih, PSB_OUT_IF_ENTRY (pPsb));
        if ((IF_ENTRY_ADDR (PSB_OUT_IF_ENTRY (pPsb)) ==
             IF_ENTRY_ADDR (pIfEntry)) && (u4PsbLih == RSVP_HOP_LIH (pRsvpHop)))
        {
            RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlPsbRoutesToOutIf: EXIT \n");
            return RPTE_YES;
        }
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlPsbRoutesToOutIf: INTMD-EXIT \n");
    return RPTE_NO;
}

/****************************************************************************/
/* Function Name   : RpteUtlPsbRoutesToOutIf                                */
/* Description     : This function determines whether the Out interface list*/
/*                   of pPsb Contains OutIfId or not                        */
/* Input (s)       : pPsb  - Pointer to Psb                                 */
/*                   pIfEntry - Pointer to Interface Entry                  */
/*                   pRsvpHop - Pointer to Rsvp Hop                         */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_YES on success RPTE_NO on failure                 */
/****************************************************************************/
INT1
RpteUtlPsbRoutesToInIf (const tPsb * pPsb, tIfEntry * pIfEntry,
                        tRsvpHop * pRsvpHop)
{
    UINT4               u4PsbLih = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlPsbRoutesToOutIf: ENTRY \n");

    RpteUtlEncodeLih (&u4PsbLih, PSB_IF_ENTRY (pPsb));
    if ((IF_ENTRY_ADDR (PSB_IF_ENTRY (pPsb)) == IF_ENTRY_ADDR (pIfEntry))
        && (u4PsbLih == RSVP_HOP_LIH (pRsvpHop)))
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlPsbRoutesToOutIf: EXIT \n");
        return RPTE_YES;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlPsbRoutesToOutIf: INTMD-EXIT \n");
    return RPTE_NO;
}

/****************************************************************************/
/* Function Name   : RpteUtlRsbRoutesToOutIf                                */
/* Description     : This function determines whether the Out interface list*/
/*                   of pPsb Contains OutIfId or not                        */
/* Input (s)       : pPsb  - Pointer to Psb                                 */
/*                   pIfEntry - Pointer to Interface Entry                  */
/*                   pRsvpHop - Pointer to Rsvp Hop                         */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_YES on success RPTE_NO on failure                 */
/****************************************************************************/
INT1
RpteUtlRsbRoutesToInIf (const tRsb * pRsb, tIfEntry * pIfEntry,
                        tRsvpHop * pRsvpHop)
{
    UINT4               u4PsbLih = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlPsbRoutesToOutIf: ENTRY \n");

    RpteUtlEncodeLih (&u4PsbLih, RSB_OUT_IF_ENTRY (pRsb));
    if ((IF_ENTRY_ADDR (RSB_OUT_IF_ENTRY (pRsb)) == IF_ENTRY_ADDR (pIfEntry))
        && (u4PsbLih == RSVP_HOP_LIH (pRsvpHop)))
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlPsbRoutesToOutIf: EXIT \n");
        return RPTE_YES;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlPsbRoutesToOutIf: INTMD-EXIT \n");
    return RPTE_NO;
}

/****************************************************************************/
/* Function Name   : RpteUtlComputeLifeTime                                 */
/* Description     : This function computes and returns the life time of a  */
/*                   Psb/Rsb by using the Period value in pTimeValues       */
/* Input (s)       : *pIfEntry - Pointer to an entry in IfTable             */
/*                   *pTimeValues - Pointer to TimeValues                   */
/* Output (s)      : None                                                   */
/* Returns         : Returns the Computed Life time                         */
/****************************************************************************/
UINT4
RpteUtlComputeLifeTime (const tIfEntry * pIfEntry, UINT4 u4RefreshTime)
{
    UINT4               u4Time;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlComputeLifeTime: ENTRY \n");
    OsixGetSysTime ((tOsixSysTime *) & (u4Time));

    /* NOTE : Current immplementation of RSVPTE assumes that
     * refresh time is done in terms of seconds, hence division by 1000
     * is removed */

    u4Time = u4Time + (UINT4) ((IF_ENTRY_REFRESH_MULTIPLE (pIfEntry) +
                                GRACE_VALUE) * GRACE_MULTIPLE *
                               ((u4RefreshTime / DEFAULT_CONV_TO_SECONDS) *
                                SYS_NUM_OF_TIME_UNITS_IN_A_SEC));

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlComputeLifeTime: EXIT \n");
    return u4Time;
}

/****************************************************************************/
/* Function Name   : RpteUtlValidateStyle                                   */
/* Description     : This function validates the style                      */
/* Input (s)       : pStyle                                                 */
/* Output (s)      : None                                                   */
/* Returns         : If valid style then returns RPTE_SUCCESS               */
/*                   Else returns RPTE_FAILURE                              */
/****************************************************************************/
INT1
RpteUtlValidateStyle (const tStyle * pStyle)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlValidateStyle: ENTRY \n");
    switch (((STYLE_OPT_VECT (pStyle))[RSVPTE_TWO]) & STYLE_MASK)
    {
        case RPTE_FF:
        case RPTE_WF:
        case RPTE_SE:
            RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlValidateStyle: EXIT \n");
            return RPTE_SUCCESS;
        default:
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteUtlValidateStyle: INTMD-EXIT \n");
            return RPTE_FAILURE;
    }
}

/****************************************************************************/
/* Function Name   : RpteUtlCompareIfId                                     */
/* Description     : This function compares two interface Id structures     */
/* Input (s)       : pIfId1                                                 */
/*                   pIfId2                                                 */
/* Output (s)      : None                                                   */
/* Returns         : If IfId's are matched, then returns RPTE_SUCCESS. Else */
/*                   returns RPTE_FAILURE                                   */
/****************************************************************************/
INT1
RpteUtlCompareIfId (const tCRU_INTERFACE * pIfId1, tCRU_INTERFACE * pIfId2)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareIfId: ENTRY \n");
    if ((IF_TYPE (*pIfId1) == IF_TYPE (*pIfId2)) &&
        (IF_NUM (*pIfId1) == IF_NUM (*pIfId2)) &&
        (IF_VCNUM (*pIfId1) == IF_VCNUM (*pIfId2)))
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareIfId: EXIT \n");
        return RPTE_SUCCESS;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareIfId: INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteUtlCompareStyle                                    */
/* Description     : This function Comapres two style structures            */
/* Input (s)       : pStyle1                                                */
/*                   pStyle2                                                */
/* Output (s)      : None                                                   */
/* Returns         : If both the structures are matching, then retuns       */
/*                   RPTE_SUCCESS. Else returns RPTE_FAILURE                */
/****************************************************************************/
INT1
RpteUtlCompareStyle (const tStyle * pStyle1, tStyle * pStyle2)
{
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareStyle: ENTRY \n");
    if (MEM_CMP (STYLE_OPT_VECT (pStyle1),
                 STYLE_OPT_VECT (pStyle2), RSVPTE_THREE) == RSVPTE_ZERO)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareStyle: EXIT \n");
        return RPTE_SUCCESS;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlCompareStyle: INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteUtlDeleteNbrTableEntry                             */
/* Description     : This function creates an entry in NbrTable.            */
/* Input (s)       : pIfEntry - Pointer to Interface Entry                  */
/*                 : pNbrEntry - Pointer to Neighbour Entry                 */
/* Output (s)      : RPTE_SUCCESS/RPTE_FAILURE                              */
/* Returns         : None.                                                  */
/****************************************************************************/
UINT1
RpteUtlDeleteNbrTableEntry (tIfEntry * pIfEntry, tNbrEntry * pDelNbrEntry)
{
    tNbrEntry          *pNbrEntry = NULL;
    tNbrEntry          *pTmpNbrEntry = NULL;
    tTMO_SLL           *pNbrList = NULL;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RpteUtlDeleteNbrTableEntry : ENTRY \n");

    pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));

    if (pNbrList == NULL)
    {
        return RPTE_FAILURE;
    }

    /* Reset the bIsHelloActive flag */

    RpteHhSetNbrHelloActive (pDelNbrEntry, pIfEntry, RPTE_FALSE);

    TMO_DYN_SLL_Scan (pNbrList, pNbrEntry, pTmpNbrEntry, tNbrEntry *)
    {
        if ((pNbrEntry->u4Addr) == (pDelNbrEntry->u4Addr))
        {
            TmrStopTimer (RSVP_GBL_TIMER_LIST, &pNbrEntry->GrTimer);
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "PVM : NbrEntry  is found and deleted ..\n");
            TMO_SLL_Delete (pNbrList, (tRSVP_SLL_NODE *) (&(pNbrEntry->pNext)));

            RSVP_RELEASE_MEM_BLOCK (RSVPTE_NBR_ENTRY_POOL_ID,
                                    (UINT1 *) pNbrEntry);
            /* Update IfEntry */
            --IF_ENTRY_NBRS (pIfEntry);
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RpteUtlDeleteNbrTableEntry : EXIT \n");
            return RPTE_SUCCESS;
        }
    }
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RpteUtlDeleteNbrTableEntry : INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteUtlCreatePsb                                       */
/* Description     : This function Creates a PSB.                           */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : If successfull returns a pointer to Psb else NULL      */
/****************************************************************************/
tPsb               *
RpteUtlCreatePsb (VOID)
{
    tPsb               *pPsb = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlCreatePsb : ENTRY \n");

    pPsb = (tPsb *) RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_PSB_POOL_ID);

    if (pPsb == NULL)
    {
        RSVPTE_DBG (RSVPTE_UTL_PRCS,
                    "Err - Memory allocation failed in creating PSB");
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "rptePvmUtlCreatePsb : INTMD-EXIT \n");
        return NULL;
    }

    MEMSET (pPsb, RSVPTE_ZERO, sizeof (tPsb));

    PSB_TIMER_TYPE (pPsb) = RPTE_TIMER_TYPE_UNKNOWN;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlCreatePsb : EXIT \n");
    return pPsb;
}

/****************************************************************************/
/* Function Name   : RpteUtlDeletePsb                                       */
/* Description     : This function Creates a PSB.                           */
/* Input (s)       : pPsb - Pointer to the PSB                              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteUtlDeletePsb (tPsb * pPsb)
{
    tNbrEntry          *pNbrEntry = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlDeletePsb : ENTRY \n");
    if (pPsb == NULL)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlDeletePsb : INTMD-EXIT \n");
        return;
    }
    pRsvpTeTnlInfo = pPsb->pRsvpTeTnlInfo;

    if ((pRsvpTeTnlInfo != NULL) &&
        (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS))
    {
        pNbrEntry = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);
        if (pNbrEntry != NULL)
        {
            TMO_DLL_Delete (&(pNbrEntry->UpStrTnlList),
                            &(pRsvpTeTnlInfo->UpStrNbrTnl));
            RSVPTE_DBG5 (RSVPTE_UTL_PRCS,
                         "Tunnel %d %d %x %x removed from upstream list "
                         "of Nbr %x\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                         NBR_ENTRY_ADDR (pNbrEntry));
        }
        else
        {
            RSVPTE_DBG (RSVPTE_UTL_PRCS,
                        "UTIL : Upstream NbrEntry does not exist\n");
        }
    }

    if (pRsvpTeTnlInfo != NULL)
    {
        RSVPTE_TNL_PSB (pRsvpTeTnlInfo) = NULL;
    }

    MemReleaseMemBlock (RSVPTE_PSB_POOL_ID, (UINT1 *) pPsb);

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlDeletePsb : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteUtlCreateRsb                                       */
/* Description     : This function Creates a RSB.                           */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : If successfull returns a pointer to Rsb else NULL      */
/****************************************************************************/
tRsb               *
RpteUtlCreateRsb (VOID)
{
    tRsb               *pRsb = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlCreateRsb : ENTRY \n");

    pRsb = (tRsb *) RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_RSB_POOL_ID);

    if (pRsb == NULL)
    {
        RSVPTE_DBG (RSVPTE_UTL_PRCS,
                    "Err - Memory allocation failed in creating Rsb");
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "rptePvmUtlCreateRsb : INTMD-EXIT \n");
        return NULL;
    }

    MEMSET (pRsb, RSVPTE_ZERO, sizeof (tRsb));

    RSB_TIMER_TYPE (pRsb) = RPTE_TIMER_TYPE_UNKNOWN;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlCreateRsb : EXIT \n");
    return pRsb;
}

/****************************************************************************/
/* Function Name   : RpteUtlDeleteRsb                                       */
/* Description     : This function Creates a Rsb.                           */
/* Input (s)       : pRsb - Pointer to the Rsb                              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteUtlDeleteRsb (tRsb * pRsb)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRpteKey            RpteKey;
    tNbrEntry          *pNbrEntry = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlDeleteRsb : ENTRY \n");

    if (RPTE_TNL_RT_TIMER_BLOCK (RSB_RPTE_TNL_INFO (pRsb)) != NULL)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlDeleteRsb : INTMD-EXIT \n");
        return;
    }

    pRsvpTeTnlInfo = RSB_RPTE_TNL_INFO (pRsb);

    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_EGRESS)
    {
        pNbrEntry = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
        RSVPTE_DBG1 (RSVPTE_UTL_PRCS,
                     "Neighbor Address: %x\n",
                     RSVP_HOP_ADDR (&RSB_NEXT_RSVP_HOP
                                    (RSVPTE_TNL_RSB (pRsvpTeTnlInfo))));

        if (pNbrEntry != NULL)
        {
            if (NBR_ENTRY_IF_ENTRY (pNbrEntry) != NULL)
            {
                (RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS
                 (NBR_ENTRY_IF_ENTRY (pNbrEntry)))--;
            }
            (NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry))--;
            TMO_DLL_Delete (&(pNbrEntry->DnStrTnlList),
                            &(pRsvpTeTnlInfo->DnStrNbrTnl));
            RSVPTE_DBG5 (RSVPTE_UTL_PRCS,
                         "Tunnel %d %d %x %x removed from downstream list "
                         "of Nbr %x\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                         NBR_ENTRY_ADDR (pNbrEntry));
        }
        else
        {
            RSVPTE_DBG4 (RSVPTE_UTL_PRCS,
                         "Tunnel %d %d %x %x not removed from downstream list\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));
        }

    }

    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) & RPTE_IN_RESV_MSG_ID_PRESENT) ==
        RPTE_IN_RESV_MSG_ID_PRESENT)
    {
        if (RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo) != NULL)
        {
            RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (RPTE_TNL_DSTR_NBR
                                                        (pRsvpTeTnlInfo));
        }
        RpteKey.u8_Key[RPTE_ONE] = RPTE_TNL_IN_RESV_MSG_ID (pRsvpTeTnlInfo);
        RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_NBR_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
    }

    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) & RPTE_OUT_RESV_MSG_ID_PRESENT)
        == RPTE_OUT_RESV_MSG_ID_PRESENT)
    {
        RpteKey.u4_Key = RPTE_TNL_OUT_RESV_MSG_ID (pRsvpTeTnlInfo);
        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_LCL_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
    }

    /* Remove the tunnel from Preemption List */
    RptePmRemoveTnlFromPrioList (pRsvpTeTnlInfo);

    RSVPTE_TNL_RSB (RSB_RPTE_TNL_INFO (pRsb)) = NULL;
    MemReleaseMemBlock (RSVPTE_RSB_POOL_ID, (UINT1 *) pRsb);
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, " RpteUtlDeleteRsb : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteUtlStartTimer
 * Description     : Function to start the timer. If the timer is already
 *                   running, the timer is first stopped before starting.
 * Input (s)       : Same as the input parameters for TmrStartTimer.
 * Output (s)      : None
 * Returns         : Value returned by TmrStartTimer
 */
/****************************************************************************/
UINT4
RpteUtlStartTimer (tTimerListId TimerListId, tTmrAppTimer * pReference,
                   UINT4 u4Duration)
{
    /* We are adding this as per function header description */
    TmrStopTimer (TimerListId, pReference);
    return (TmrStartTimer (TimerListId, pReference, u4Duration));
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlIsUpStrMsgRcvdOnBkpPath
 * Description     : This function verifies whether the UpStream Error Messages
 *                   (PATH Tear/RESV Error) is received on backup path or not. 
 *                   It also returns the corresponding RsvpTeTnlInfo, and 
 *                   Current State of the LSR. 
 * Input (s)       : pPktMap - pointer to the RSVP packet map
 *                                                       
 * Output (s)      : ppRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo   
 *                   ppRsvpTePrevTnlInfo - Pointer to RsvpTeTnlInfo
 *                   pbIsBkpTnl - Shows whether received on backup tunnel 
 *                                or not.
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
INT1
RpteUtlIsUpStrMsgRcvdOnBkpPath (tPktMap * pPktMap,
                                tRsvpTeTnlInfo ** ppRsvpTeTnlInfo,
                                tRsvpTeTnlInfo ** ppRsvpTePrevTnlInfo,
                                BOOL1 * bIsBkpTnl)
{
    tRsvpTeTnlInfo     *pRsvpTeInTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTmpTnlInfo = NULL;
    tPsb               *pPsb = NULL;
    BOOL1               bFound = RPTE_FALSE;

    pRsvpTeTmpTnlInfo = *ppRsvpTeTnlInfo;
    pRsvpTeInTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeTmpTnlInfo);

    while (pRsvpTeInTnlInfo != NULL)
    {
        pPsb = RSVPTE_TNL_PSB (pRsvpTeInTnlInfo);

        if (pPsb == NULL)
        {
            RSVPTE_DBG (RSVPTE_UTL_PRCS, "Err - PSB Not Found\n");
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteUtlIsUpStrMsgRcvdOnBkpPath : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        if (IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) ==
            IF_ENTRY_ADDR (PSB_IF_ENTRY (pPsb)))
        {
            *bIsBkpTnl = RPTE_YES;
            bFound = RPTE_TRUE;
            RSVPTE_DBG1 (RSVPTE_UTL_PRCS,
                         "Msg not received on wrong interface - bIsBkpTnl(%d)\n",
                         *bIsBkpTnl);
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteUtlIsUpStrMsgRcvdOnBkpPath : INTMD-EXIT \n");
            break;
        }
        pRsvpTeTmpTnlInfo = pRsvpTeInTnlInfo;
        pRsvpTeInTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeTmpTnlInfo);
    }
    if (bFound == RPTE_FALSE)
    {
        pPsb = RSVPTE_TNL_PSB (*ppRsvpTeTnlInfo);
        if ((pPsb == NULL) || (PSB_IF_ENTRY (pPsb) == NULL))
        {
            RSVPTE_DBG (RSVPTE_UTL_PRCS, "Err - PSB Not Found\n");
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteUtlIsUpStrMsgRcvdOnBkpPath : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        if (IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) ==
            IF_ENTRY_ADDR (PSB_IF_ENTRY (pPsb)))
        {
            *bIsBkpTnl = RPTE_NO;
            bFound = RPTE_TRUE;
            *ppRsvpTePrevTnlInfo = *ppRsvpTeTnlInfo;
            RSVPTE_DBG1 (RSVPTE_UTL_PRCS,
                         "Msg not received on wrong interface - bIsBkpTnl(%d)\n",
                         *bIsBkpTnl);
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteUtlIsUpStrMsgRcvdOnBkpPath : INTMD-EXIT \n");
            return RPTE_SUCCESS;
        }
    }
    if (bFound == RPTE_FALSE)
    {
        RSVPTE_DBG1 (RSVPTE_UTL_PRCS,
                     "Msg not received on wrong interface - bFound(%d)\n",
                     bFound);
        RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                    "RpteUtlIsUpStrMsgRcvdOnBkpPath : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    *ppRsvpTeTnlInfo = pRsvpTeInTnlInfo;
    *ppRsvpTePrevTnlInfo = pRsvpTeTmpTnlInfo;
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlIsUpStrMsgRcvdOnBkpPath : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlGetTnlInfoAndGrpId
 * Description     : This function gets the Correct RSVP Tunnel Information 
 *                   and the corresponding Detour Group ID of the Tunnel. 
 * Input (s)       : pPktMap - pointer to the RSVP packet map                                    
 *                   pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo   
 * Output (s)      : pu1DetourGrpId  - Detour Group ID of the Tunnel Fetched.
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
INT1
RpteUtlGetTnlInfoAndGrpId (tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                           UINT1 *pu1DetourGrpId)
{
    tRsvpTeTnlInfo     *pRsvpTeInTnlInfo = pRsvpTeTnlInfo;
    tPsb               *pPsb = NULL;

    do
    {
        pPsb = RSVPTE_TNL_PSB (pRsvpTeInTnlInfo);

        if ((pPsb == NULL) || (PSB_OUT_IF_ENTRY (pPsb) == NULL))
        {
            continue;
        }

        if (IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) ==
            IF_ENTRY_ADDR (PSB_OUT_IF_ENTRY (pPsb)))
        {
            *pu1DetourGrpId = RPTE_FRR_DETOUR_GRP_ID (pRsvpTeInTnlInfo);
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteUtlIsUpStrMsgRcvdOnBkpPath : INTMD-EXIT \n");
            return RPTE_SUCCESS;
        }
    }
    while ((pRsvpTeInTnlInfo =
            RPTE_FRR_IN_TNL_INFO (pRsvpTeInTnlInfo)) != NULL);
    return RPTE_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlGetGrpIdForDMP
 * Description     : This function gets the group id for detour merge point. 
 * Input (s)       : pRsvpTeTnlInfo - pointer to the RSVP tnl info.
 *                   pu1GrpId - pointer to Group ID
 *                                                       
 * Output (s)      : pu1GrpId - Group ID.
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
INT1
RpteUtlGetGrpIdForDMP (tRsvpTeTnlInfo * pRsvpTeTnlInfo, tIfEntry * pIfEntry,
                       UINT1 *pu1DetourGrpId)
{
    tRsvpTeTnlInfo     *pTempCtTnlInfo = NULL;
    tPsb               *pPsb = NULL;

    pTempCtTnlInfo = pRsvpTeTnlInfo;
    do
    {
        pPsb = RSVPTE_TNL_PSB (pTempCtTnlInfo);
        if (pPsb == NULL)
        {
            RSVPTE_DBG (RSVPTE_UTL_PRCS, "ERROR : PSB Not Found\n");
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteUtlGetGrpIdForDMP : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        if (IF_ENTRY_ADDR (PSB_OUT_IF_ENTRY (pPsb)) == IF_ENTRY_ADDR (pIfEntry))
        {
            *pu1DetourGrpId = RPTE_FRR_DETOUR_GRP_ID (pTempCtTnlInfo);
            break;
        }
    }
    while ((pTempCtTnlInfo = RPTE_FRR_IN_TNL_INFO (pTempCtTnlInfo)) != NULL);
    if (*pu1DetourGrpId == RPTE_ZERO)
    {
        RSVPTE_DBG (RSVPTE_UTL_PRCS, "ERROR : u1DetourGrpId Not Found\n");
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlGetGrpIdForDMP : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlGetGrpIdForDMP : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlIsNodeProtDesired
 * Description     : This function checks whether node protection
 *                   is desired. 
 * Input (s)       : pRsvpTeTnlInfo - pointer to the RSVP tnl info.
 * Output (s)      : None
 * Returns         : RPTE_NO/RPTE_YES
 */
/*---------------------------------------------------------------------------*/
INT1
RpteUtlIsNodeProtDesired (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        if ((RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo) != NULL) &&
            ((RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u1ProtType)
             == TE_TNL_FRR_PROT_NODE))
        {
            return RPTE_YES;
        }
    }
    else
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlProtType == TE_TNL_FRR_PROT_NODE)
        {
            return RPTE_YES;
        }
    }
    return RPTE_NO;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlIsBwProtDesired
 * Description     : This function checks whether bandwidth protection
 *                   is desired and returns desired bandwidth. 
 * Input (s)       : pRsvpTeTnlInfo - pointer to the RSVP tnl info.
 * Output (s)      : pu4Bandwidth - Bandwidth Desired
 * Returns         : RPTE_NO/RPTE_YES
 */
/*---------------------------------------------------------------------------*/
INT1
RpteUtlIsBwProtDesired (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT4 *pu4Bandwidth)
{
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    FLT4                f4TmpVar = RPTE_ZERO;

    pRsvpTeFastReroute =
        &(PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        if (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo) != NULL)
        {
            *pu4Bandwidth = TE_TNL_FRR_CONST_BANDWIDTH
                (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo));
            return RPTE_YES;
        }
    }
    else
    {
        if ((pRsvpTeFastReroute->u1Flags != RPTE_ZERO) &&
            (pRsvpTeFastReroute->u1Flags ==
             RSVPTE_TNL_FRR_ONE2ONE_METHOD) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->bIsBwProtDesired == TRUE))
        {
            f4TmpVar = OSIX_NTOHF (pRsvpTeFastReroute->fBandwth);
            RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
            *pu4Bandwidth = (UINT4) f4TmpVar;
            return RPTE_YES;
        }
    }
    return RPTE_NO;
}

/*****************************************************************************/
/* Function Name : RpteTnlScanForFrrMakeAfterBreakProt                       */
/* Description   : This routine Checks the presence of a RSVP Tunnel, which  */
/*                 has same set of parameters like a new tunnel.             */
/* Input(s)      : pRsvpTeGblInfo. - Pointer to the RsvpTeGlobal information.*/
/*                 OutLabel        - Outgoing Label                          */
/*                 pOutIfEntry     - Pointer to Out Iface Entry              */
/* Output(s)     : None                                                      */
/* Return(s)     : RPTE_SUCCESS / RPTE FAILURE                               */
/*****************************************************************************/
UINT1
RpteTnlScanForFrrMakeAfterBreakProt (tIfEntry * pOutIfEntry)
{
    tIfEntry           *pIfEntry = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                "RpteTnlScanForFrrMakeAfterBreakProt : ENTRY \n");

    pTmpRsvpTeTnlInfo = RBTreeGetFirst (gpRsvpTeGblInfo->RpteTnlTree);

    while (pTmpRsvpTeTnlInfo != NULL)
    {
        if (RPTE_TE_TNL (pTmpRsvpTeTnlInfo) == NULL)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }
        if (RPTE_IS_FRR_CAPABLE (pTmpRsvpTeTnlInfo) == RPTE_FALSE)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }
        if (RSVPTE_TNL_ADMIN_STATUS
            (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) != RPTE_ADMIN_UP)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }
        if ((RSVPTE_TNL_NODE_RELATION (pTmpRsvpTeTnlInfo) == RPTE_EGRESS) ||
            (RSVPTE_TNL_PSB (pTmpRsvpTeTnlInfo) == NULL))
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }
        pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pTmpRsvpTeTnlInfo));
        if (((pIfEntry) == NULL) || (((pIfEntry) != NULL) &&
                                     (IF_ENTRY_ADDR (pIfEntry) !=
                                      IF_ENTRY_ADDR (pOutIfEntry))))
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }
        RpteRhBackupPathTrigger (pTmpRsvpTeTnlInfo);
        if (RSVPTE_TNL_NODE_RELATION (pTmpRsvpTeTnlInfo) != RPTE_INGRESS)
        {
            RpteFrrConstructAndSendPathErr (pTmpRsvpTeTnlInfo);
        }
        /* PSB Time To Die updation */
        PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pTmpRsvpTeTnlInfo)) =
            RpteUtlComputeLifeTime (pIfEntry,
                                    (2 *
                                     (IF_ENTRY_REFRESH_INTERVAL (pIfEntry))));
        PSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_PSB (pTmpRsvpTeTnlInfo)) =
            (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) / DEFAULT_CONV_TO_SECONDS);

        if ((RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo) != NULL) &&
            (RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo)) != NULL))
        {
            /* RSB Time To Die updation */
            pIfEntry = RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo));
            RSB_TIME_TO_DIE (RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo)) =
                RpteUtlComputeLifeTime (pIfEntry,
                                        (2 *
                                         (IF_ENTRY_REFRESH_INTERVAL
                                          (pIfEntry))));
            RSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo)) =
                (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                 DEFAULT_CONV_TO_SECONDS);
        }
        pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                           (tRBElem *) pTmpRsvpTeTnlInfo, NULL);
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                "RpteTnlScanForFrrMakeAfterBreakProt : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteResetPlrStatus                                        */
/* Description   : This function resets the PLR status                       */
/* Input(s)      : pRsvpTeTnlInfo  - Pointer to the Rsvp Tnl information.    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
RpteResetPlrStatus (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) = NULL;
    RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) &= ~(RPTE_FRR_NODE_STATE_PLR);
    RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) &=
        (UINT1) (~(RPTE_FRR_NODE_STATE_PLRAWAIT));
    RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) &=
        (UINT1) (~(RPTE_FRR_NODE_STATE_DNLOST));
    RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) = RPTE_FRR_CSPF_NO_START;
    RPTE_FRR_CSPF_INTVL_COUNT (pRsvpTeTnlInfo) = RPTE_ZERO;
    pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_NOT_AVAIL;
}

/*****************************************************************************/
/* Function Name : RpteUpdateDetourStats                                     */
/* Description   : This function updates the Detour statistics               */
/* Input(s)      : u4AddOrSub - Notifies whether Addition or Subtraction to  */
/*                              be done.                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
RpteUpdateDetourStats (UINT4 u4AddOrSub)
{
    if (u4AddOrSub == RPTE_ONE)
    {
        RSVPTE_FRR_DETOUR_OUTGOING_NUM (gpRsvpTeGblInfo)++;
        RSVPTE_FRR_DETOUR_ORIG_NUM (gpRsvpTeGblInfo)++;
    }
    else
    {
        if (RSVPTE_FRR_DETOUR_OUTGOING_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
        {
            RSVPTE_FRR_DETOUR_OUTGOING_NUM (gpRsvpTeGblInfo)--;
        }

        if (RSVPTE_FRR_DETOUR_ORIG_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
        {
            RSVPTE_FRR_DETOUR_ORIG_NUM (gpRsvpTeGblInfo)--;
        }
    }
}

/*****************************************************************************/
/* Function Name : RpteUtilGetPrevTnlInfo                                    */
/* Description   : This function gets the previous tunnel information        */
/*                 to a passed tunnel information                            */
/* Input(s)      : pBaseTnlInfo - Base Tunnel Information                    */
/*                 pCurTnlInfo  - Current Tunnel Information                 */
/* Output(s)     : ppPrevTnlInfo - Previous Tunnel Information of Current    */
/*                                 Tunnel Information                        */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
RpteUtilGetPrevTnlInfo (tRsvpTeTnlInfo * pBaseTnlInfo,
                        tRsvpTeTnlInfo * pCurTnlInfo,
                        tRsvpTeTnlInfo ** ppPrevTnlInfo)
{
    tRsvpTeTnlInfo     *pTmpTnlInfo = NULL;
    tRsvpTeTnlInfo     *pPrevTnlInfo = NULL;

    pTmpTnlInfo = pBaseTnlInfo;

    do
    {
        if ((RSVPTE_TNL_TNLINDX (pTmpTnlInfo) ==
             RSVPTE_TNL_TNLINDX (pCurTnlInfo)) &&
            (RSVPTE_TNL_TNLINST (pTmpTnlInfo) ==
             RSVPTE_TNL_TNLINST (pCurTnlInfo)) &&
            (MEMCMP (&(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pTmpTnlInfo))),
                     &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pCurTnlInfo))),
                     IPV4_ADDR_LENGTH) == MPLS_ZERO) &&
            (MEMCMP (&(RSVPTE_TNL_EGRESS_RTR_ID (RPTE_TE_TNL (pTmpTnlInfo))),
                     &(RSVPTE_TNL_EGRESS_RTR_ID (RPTE_TE_TNL (pCurTnlInfo))),
                     IPV4_ADDR_LENGTH) == MPLS_ZERO))
        {
            break;
        }
        pPrevTnlInfo = pTmpTnlInfo;
    }
    while ((pTmpTnlInfo = RPTE_FRR_IN_TNL_INFO (pTmpTnlInfo)) != NULL);

    *ppPrevTnlInfo = pPrevTnlInfo;

    return;
}

/****************************************************************************/
/* Function Name   : RpteUtilConvertToInteger
 * Description     : 
 *                   
 * Input (s)       :  
 * Output (s)      : None
 * Returns         : 
 */
/****************************************************************************/
UINT4
RpteUtilConvertToInteger (tTeRouterId RouterId)
{
    UINT4               u4TempVal = 0;
    CONVERT_TO_INTEGER (RouterId, u4TempVal);
    return u4TempVal;

}

/*****************************************************************************/
/* Function Name : RpteUtlPathSizeCalculation                                */
/* Description   : This function is used to calculate path message size      */
/* Input(s)      : pRsvpTeTnlInfo - Base Tunnel Information                  */
/* Output(s)     : pu2SsnObjLen   - Session object length                    */
/*                 pu2EROCount    - Number of ERO count                      */
/*                 pu2RROSize     - RRO object length                        */
/*                 pu1EroNeeded   - flag used to check whether rro object    */
/*                                  is needed                                */
/*                 pu1RroNeeded   - flag used to check whether rro object    */
/*                                  is needed                                */
/* Return(s)     : None                                                      */
/*****************************************************************************/

UINT2
RpteUtlCalculatePathMsgSize (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                             UINT2 *pu2SsnObjLen, UINT2 *pu2RROSize,
                             UINT1 *pu1EroNeeded, UINT1 *pu1RroNeeded,
                             UINT1 u1MsgType)
{
    UINT2               u2Retval = RSVPTE_ZERO;
    UINT2               u2PathSize = RSVPTE_ZERO;
    BOOL1               b1Oob = RPTE_FALSE;
    UINT4               u4DataTeLinkIfId = RPTE_ZERO;
    UINT4               u4Addr = RPTE_ZERO;
    UINT1               u1LblType = RPTE_ZERO;
    tNbrEntry          *pNbrEntry = NULL;

    /* Calculte the size of RSVP hop, Times value, sender tspec and
     * sender template objects */
    u2PathSize = (sizeof (tRsvpHdr) + RPTE_IPV4_SSN_OBJ_LEN +
                  sizeof (tTimeValuesObj) +
                  sizeof (tSenderTspecObj) + RPTE_IPV4_SNDR_TMP_OBJ_LEN);

    if (u1MsgType == PATH_MSG)
    {
        b1Oob = pRsvpTeTnlInfo->b1DnStrOob;
        u4DataTeLinkIfId = pRsvpTeTnlInfo->u4DnStrDataTeLinkIfId;
        u4Addr = pRsvpTeTnlInfo->pPsb->pOutIfEntry->u4Addr;
        u1LblType = pRsvpTeTnlInfo->pPsb->pOutIfEntry->RpteIfInfo.u1LblType;
    }
    else
    {
        b1Oob = pRsvpTeTnlInfo->b1UpStrOob;
        u4DataTeLinkIfId = pRsvpTeTnlInfo->u4UpStrDataTeLinkIfId;
        u4Addr = pRsvpTeTnlInfo->pPsb->pIncIfEntry->u4Addr;
        u1LblType = pRsvpTeTnlInfo->pPsb->pIncIfEntry->RpteIfInfo.u1LblType;
    }
    u2Retval = RpteUtlCalculateRSVPHopSize (b1Oob, u4DataTeLinkIfId, u4Addr);
    u2PathSize = (UINT2) (u2PathSize + u2Retval);

    /* Calculate Ssn Attr Obj Len, if present */
    if ((RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pRsvpTeTnlInfo) == RSVPTE_ZERO)
        && (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo) ==
            RSVPTE_ZERO)
        && (RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pRsvpTeTnlInfo) == RSVPTE_ZERO))

    {
        *pu2SsnObjLen =
            (UINT2) ((sizeof (tObjHdr) + (sizeof (UINT1) * RSVPTE_FOUR) +
                      ((STRNLEN
                        ((INT1 *)
                         RSVPTE_TNL_NAME (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                         LSP_TNLNAME_LEN) <
                        SSN_NAME_MIN_LEN) ? SSN_NAME_MIN_LEN : STRNLEN ((INT1 *)
                                                                        RSVPTE_TNL_NAME
                                                                        (RPTE_TE_TNL
                                                                         (pRsvpTeTnlInfo)),
                                                                        LSP_TNLNAME_LEN))));
    }
    else
    {
        *pu2SsnObjLen =
            (UINT2) ((sizeof (tObjHdr) + (sizeof (UINT1) * RSVPTE_FOUR) +
                      sizeof (RSVPTE_EXC_ANY_AFFINITY (pRsvpTeTnlInfo)) +
                      sizeof (RSVPTE_INC_ALL_AFFINITY (pRsvpTeTnlInfo)) +
                      sizeof (RSVPTE_INC_ANY_AFFINITY (pRsvpTeTnlInfo)) +
                      ((STRNLEN
                        ((INT1 *)
                         RSVPTE_TNL_NAME (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                         LSP_TNLNAME_LEN) <
                        SSN_NAME_MIN_LEN) ? SSN_NAME_MIN_LEN : STRNLEN ((INT1 *)
                                                                        RSVPTE_TNL_NAME
                                                                        (RPTE_TE_TNL
                                                                         (pRsvpTeTnlInfo)),
                                                                        LSP_TNLNAME_LEN))));
    }
    if ((*pu2SsnObjLen % WORD_BNDRY) != RSVPTE_ZERO)
    {
        *pu2SsnObjLen = (UINT2) (*pu2SsnObjLen + (WORD_BNDRY -
                                                  (STRNLEN
                                                   ((INT1 *)
                                                    RSVPTE_TNL_NAME (RPTE_TE_TNL
                                                                     (pRsvpTeTnlInfo)),
                                                    LSP_TNLNAME_LEN) %
                                                   WORD_BNDRY)));
    }
    u2PathSize = (UINT2) (u2PathSize + *pu2SsnObjLen);

    if (pRsvpTeTnlInfo->pPsb->AdSpec.MsgHdr.u2HdrLen != RSVPTE_ZERO)
    {
        /*  Calculate and update AdSpec Obj Len */
        if ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS) ||
            (pRsvpTeTnlInfo->pPsb->AdSpec.MsgHdr.u2HdrLen ==
             IPV4_ADSPEC_WITH_GS_AND_CLS_MSG_HDR_LEN))
        {
            u2PathSize = (UINT2) (u2PathSize + sizeof (tAdSpecObj));
        }
        else
        {
            u2PathSize =
                (UINT2) (u2PathSize + sizeof (tAdSpecWithoutGsAndClsObj));
        }
    }

    /* Calculate Explicit Route Object size */
    if ((u1MsgType == PATH_MSG) &&
        ((pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo != NULL) ||
         (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL)))
    {
        u2PathSize = (UINT2) (u2PathSize + pRsvpTeTnlInfo->u2EroSize);
        *pu1EroNeeded = RPTE_TRUE;
    }
    else if ((u1MsgType == RECOVERY_PATH_MSG) &&
             (RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL))
    {
        RpteUtlCalEroSizeForRecoveryPathMsg (pRsvpTeTnlInfo);
        *pu1EroNeeded = RPTE_TRUE;
        u2PathSize =
            (UINT2) (u2PathSize + pRsvpTeTnlInfo->u2RecoveryPathEroSize);
    }

    /* Calculate Lbl Req Obj len */
    if (u1LblType == RPTE_ATM)
    {
        u2PathSize = (UINT2) (u2PathSize + sizeof (tLblReqAtmRngObj));
    }
    else if (gi4MplsSimulateFailure != RPTE_SIM_FAILURE_NO_LBL_REQ)
    {
        u2PathSize = (UINT2) (u2PathSize + sizeof (tGenLblReqObj));
    }

    /* Calculate RRO obj len */
    if (u1MsgType == RECOVERY_PATH_MSG)
    {
        *pu2RROSize =
            RpteUtlCalculateRROSize (pRsvpTeTnlInfo, RECOVERY_PATH_MSG);
    }
    else if ((((RSVPTE_TNL_OR_ATTRLIST_SSN_ATTR (pRsvpTeTnlInfo))
               & (RSVPTE_SSN_REC_ROUTE_BIT)) == (RSVPTE_SSN_REC_ROUTE_BIT)) ||
             ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
              (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) ==
               RPTE_FRR_PROTECTED_TNL))
             || (RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL))
    {
        *pu2RROSize = RpteUtlCalculateRROSize (pRsvpTeTnlInfo, PATH_MSG);
    }

    if (*pu2RROSize != RPTE_ZERO)
    {
        *pu2RROSize = (UINT2) (sizeof (tObjHdr) + *pu2RROSize);
        *pu1RroNeeded = RPTE_TRUE;
        u2PathSize = (UINT2) (u2PathSize + *pu2RROSize);
    }

    /* Label Set Object size updation */
    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1LabelSetEnabled == RPTE_ENABLED)
    {
        u2PathSize = (UINT2) (u2PathSize + sizeof (tLabelSetObj));
    }
    if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
    {
        u2PathSize = (UINT2) (u2PathSize + sizeof (tGenLblObj));
    }

    if ((pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlOperStatus == MPLS_OPER_UP))
    {
        if ((gi4MplsSimulateFailure == RPTE_SIM_FAILURE_LSP_TNL_IFID_CTYPE_1) ||
            (pRsvpTeTnlInfo->u2HlspCType ==
             RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE_1))
        {
            u2PathSize =
                (UINT2) (u2PathSize +
                         (sizeof (tLspTnlIfIdObj) - sizeof (UINT4)));
        }
        else
        {
            u2PathSize = (UINT2) (u2PathSize + sizeof (tLspTnlIfIdObj));
        }
    }
    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus !=
         GMPLS_ADMIN_UNKNOWN) &&
        (gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable == RPTE_ENABLED))
    {
        u2PathSize = (UINT2) (u2PathSize + sizeof (tAdminStatusObj));
    }
    if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4SendPathNotifyRecipient !=
        RPTE_ZERO)
    {
        u2PathSize = (UINT2) (u2PathSize + sizeof (tNotifyRequestObj));
    }
    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE) ||
        (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_FULL_REROUTE))
    {
        u2PathSize = (UINT2) (u2PathSize + sizeof (tProtectionObj));
        u2PathSize = (UINT2) (u2PathSize + sizeof (tAssociationObj));
    }
    if (u1MsgType == PATH_MSG)
    {
        pNbrEntry = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
    }
    else
    {
        pNbrEntry = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);
    }
    if ((pNbrEntry != NULL) &&
        (pNbrEntry->u1GrFaultType == RPTE_GR_NODAL_FAULT) &&
        (pNbrEntry->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
         TNL_GR_NOT_SYNCHRONIZED))
    {
        u2PathSize = (UINT2) (u2PathSize + sizeof (tRecoveryLblObj));
    }
    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
         TNL_GR_FULLY_SYNCHRONIZED)
        && (gi4MplsSimulateFailure != RPTE_SIM_FAILURE_NO_SUGGESTED_LBL_OBJ))
    {
        u2PathSize = (UINT2) (u2PathSize + sizeof (tSuggestedLblObj));
    }
    return u2PathSize;
}

/*****************************************************************************/
/* Function Name : RpteUtlCalculateEROSize                                   */
/* Description   : This function is used to calculate path message size      */
/* Input(s)      : pRsvpTeTnlInfo - Base Tunnel Information                  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
RpteUtlCalculateEROSize (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTMO_SLL           *pEROList = NULL;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    tTeCHopInfo        *pTeCHopInfo = NULL;

    /* Calculate Computed hop list size */
    if (pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo != NULL)
    {
        if ((RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) == RPTE_TRUE) &&
            ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_IN_USE) ==
             LOCAL_PROT_IN_USE))
        {
            RSVPTE_DBG (RSVPTE_PB_PRCS, "Handling for Facility backup.\n");
            pEROList = &pRsvpTeTnlInfo->pFrrOutTePathInfo->ErHopList;
            TMO_SLL_Scan (pEROList, pRsvpTeErHopInfo, tRsvpTeHopInfo *)
            {
                /* if the hop is part of the ER-Hop list and the node is ingress,
                 * do not consider that hop size */
                if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
                    (pRsvpTeErHopInfo->u1LsrPartOfErHop == RPTE_TRUE))
                {
                    continue;
                }

                /* unnumbered ERO sub object size */
                if (pRsvpTeErHopInfo->u1AddressType == ERO_TYPE_UNNUM_IF)
                {
                    pRsvpTeTnlInfo->u2EroSize =
                        (UINT2) (pRsvpTeTnlInfo->u2EroSize +
                                 ERO_TYPE_UNNUM_LEN);
                }
                /* numbered ERO sub object size */
                else
                {
                    pRsvpTeTnlInfo->u2EroSize =
                        (UINT2) (pRsvpTeTnlInfo->u2EroSize + ERO_TYPE_IPV4_LEN);
                }

                /* label sub object size -> forward direction */
                if (pRsvpTeErHopInfo->GmplsTnlHopInfo.u4ForwardLbl !=
                    MPLS_INVALID_LABEL)
                {
                    pRsvpTeTnlInfo->u2EroSize =
                        (UINT2) (pRsvpTeTnlInfo->u2EroSize + ERO_TYPE_LBL_LEN);
                }

                if (pRsvpTeErHopInfo->GmplsTnlHopInfo.u4ReverseLbl !=
                    MPLS_INVALID_LABEL)
                {
                    pRsvpTeTnlInfo->u2EroSize =
                        (UINT2) (pRsvpTeTnlInfo->u2EroSize + ERO_TYPE_LBL_LEN);
                }
            }
        }
        else
        {
            TMO_SLL_Scan (&
                          (pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo->
                           CHopList), pTeCHopInfo, tTeCHopInfo *)
            {
                /* if the hop is part of the C-Hop list and the node is ingress,
                 * do not consider that hop size */
                if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
                    (pTeCHopInfo->u1LsrPartOfCHop == RPTE_TRUE))
                {
                    continue;
                }

                /* unnumbered ERO sub object size  */
                if (pTeCHopInfo->u1AddressType == ERO_TYPE_UNNUM_IF)
                {
                    pRsvpTeTnlInfo->u2EroSize =
                        (UINT2) (pRsvpTeTnlInfo->u2EroSize +
                                 ERO_TYPE_UNNUM_LEN);
                }
                /* numbered ERO sub object size */
                else
                {
                    pRsvpTeTnlInfo->u2EroSize =
                        (UINT2) (pRsvpTeTnlInfo->u2EroSize + ERO_TYPE_IPV4_LEN);
                }

                /* label sub object size -> forward direction */
                if (pTeCHopInfo->GmplsTnlCHopInfo.u4ForwardLbl !=
                    MPLS_INVALID_LABEL)
                {
                    pRsvpTeTnlInfo->u2EroSize =
                        (UINT2) (pRsvpTeTnlInfo->u2EroSize + ERO_TYPE_LBL_LEN);
                }

                if (pTeCHopInfo->GmplsTnlCHopInfo.u4ReverseLbl !=
                    MPLS_INVALID_LABEL)
                {
                    pRsvpTeTnlInfo->u2EroSize =
                        (UINT2) (pRsvpTeTnlInfo->u2EroSize + ERO_TYPE_LBL_LEN);
                }
            }

        }
    }
    /* Calculate ER-HOP list size */
    else if ((RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL) &&
             (TMO_SLL_Count (&(RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo)))
              != RSVPTE_ZERO))
    {
        if ((RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) == RPTE_TRUE) &&
            ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_IN_USE) ==
             LOCAL_PROT_IN_USE))
        {
            RSVPTE_DBG (RSVPTE_PB_PRCS, "RPTE_FRR_STACK_BIT is true\n");
            pEROList = &pRsvpTeTnlInfo->pFrrOutTePathInfo->ErHopList;
        }
        else
        {
            RSVPTE_DBG (RSVPTE_PB_PRCS, "RPTE_FRR_STACK_BIT is not true\n");
            pEROList = &RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo);
        }
        TMO_SLL_Scan (pEROList, pRsvpTeErHopInfo, tRsvpTeHopInfo *)
        {
            /* if the hop is part of the ER-Hop list and the node is ingress,
             * do not consider that hop size */
            if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
                (pRsvpTeErHopInfo->u1LsrPartOfErHop == RPTE_TRUE))
            {
                continue;
            }

            /* unnumbered ERO sub object size */
            if (pRsvpTeErHopInfo->u1AddressType == ERO_TYPE_UNNUM_IF)
            {
                pRsvpTeTnlInfo->u2EroSize =
                    (UINT2) (pRsvpTeTnlInfo->u2EroSize + ERO_TYPE_UNNUM_LEN);
            }
            /* numbered ERO sub object size */
            else
            {
                pRsvpTeTnlInfo->u2EroSize =
                    (UINT2) (pRsvpTeTnlInfo->u2EroSize + ERO_TYPE_IPV4_LEN);
            }

            /* label sub object size -> forward direction */
            if (pRsvpTeErHopInfo->GmplsTnlHopInfo.u4ForwardLbl !=
                MPLS_INVALID_LABEL)
            {
                pRsvpTeTnlInfo->u2EroSize =
                    (UINT2) (pRsvpTeTnlInfo->u2EroSize + ERO_TYPE_LBL_LEN);
            }

            if (pRsvpTeErHopInfo->GmplsTnlHopInfo.u4ReverseLbl !=
                MPLS_INVALID_LABEL)
            {
                pRsvpTeTnlInfo->u2EroSize =
                    (UINT2) (pRsvpTeTnlInfo->u2EroSize + ERO_TYPE_LBL_LEN);
            }
        }
    }
    if (pRsvpTeTnlInfo->u2EroSize > RSVPTE_ZERO)
    {
        pRsvpTeTnlInfo->u2EroSize =
            (UINT2) (pRsvpTeTnlInfo->u2EroSize + sizeof (tObjHdr));
    }

    return;
}

VOID
RpteUtlCalEroSizeForRecoveryPathMsg (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTMO_SLL           *pEROList = NULL;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;

    if (RSVPTE_TNL_INERHOP_LIST_INFO (pRsvpTeTnlInfo) == NULL)
    {
        return;
    }
    pEROList = &pRsvpTeTnlInfo->pTePathInfo->ErHopList;
    TMO_SLL_Scan (pEROList, pRsvpTeErHopInfo, tRsvpTeHopInfo *)
    {
        /* unnumbered ERO sub object size */
        if (pRsvpTeErHopInfo->u1AddressType == ERO_TYPE_UNNUM_IF)
        {
            pRsvpTeTnlInfo->u2RecoveryPathEroSize =
                (UINT2) (pRsvpTeTnlInfo->u2RecoveryPathEroSize +
                         ERO_TYPE_UNNUM_LEN);
        }
        /* numbered ERO sub object size */
        else
        {
            pRsvpTeTnlInfo->u2RecoveryPathEroSize =
                (UINT2) (pRsvpTeTnlInfo->u2RecoveryPathEroSize +
                         ERO_TYPE_IPV4_LEN);
        }

        /* label sub object size -> forward direction */
        if (pRsvpTeErHopInfo->GmplsTnlHopInfo.u4ForwardLbl !=
            MPLS_INVALID_LABEL)
        {
            pRsvpTeTnlInfo->u2RecoveryPathEroSize =
                (UINT2) (pRsvpTeTnlInfo->u2RecoveryPathEroSize +
                         ERO_TYPE_LBL_LEN);
        }

        if (pRsvpTeErHopInfo->GmplsTnlHopInfo.u4ReverseLbl !=
            MPLS_INVALID_LABEL)
        {
            pRsvpTeTnlInfo->u2RecoveryPathEroSize =
                (UINT2) (pRsvpTeTnlInfo->u2RecoveryPathEroSize +
                         ERO_TYPE_LBL_LEN);
        }
    }

    if (pRsvpTeTnlInfo->u2RecoveryPathEroSize > RSVPTE_ZERO)
    {
        pRsvpTeTnlInfo->u2RecoveryPathEroSize =
            (UINT2) (pRsvpTeTnlInfo->u2RecoveryPathEroSize + sizeof (tObjHdr));
    }
    return;
}

/*****************************************************************************/
/* Function Name : RpteUtlCalculateRROSize                                   */
/* Description   : This function is used to calculate Record Route Object    */
/*                 size in PATH or RESV Message                              */
/* Input(s)      : pRsvpTeTnlInfo - RSVP Tunnel Information                  */
/*                 u4MsgType - Message type - PATH or RESV                   */
/* Output(s)     : None                                                      */
/* Return(s)     : u2RroSize - Rro Size                                      */
/*****************************************************************************/

UINT2
RpteUtlCalculateRROSize (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT4 u4MsgType)
{
    tTMO_SLL           *pRROList = NULL;
    tRsvpTeArHopInfo   *pRsvpTeArHopInfo = NULL;
    UINT2               u2RROLength = RSVPTE_ZERO;
    UINT4               u4DownstrLabel = MPLS_INVALID_LABEL;
    UINT4               u4UpstrLabel = MPLS_INVALID_LABEL;
    tIfEntry           *pIfEntry = NULL;

    if (u4MsgType == PATH_MSG)
    {
        /* For path message, If entry should be taken from PSB */
        pIfEntry = pRsvpTeTnlInfo->pPsb->pOutIfEntry;

        /* For path message, IN-ARHOP List should be used to create
         * RRO object */
        if ((RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL) &&
            (TMO_SLL_Count (&RSVPTE_TNL_INARHOP_LIST (pRsvpTeTnlInfo))
             != RSVPTE_ZERO))
        {
            pRROList = &(RSVPTE_TNL_INARHOP_LIST (pRsvpTeTnlInfo));
        }

        /* For path message, the downstream label should be out-label
         * And upstream label should be in-label */
        u4DownstrLabel = pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl;
        u4UpstrLabel = pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl;
    }
    else
    {
        /* For resv message, If entry should be taken from RSB */
        pIfEntry = pRsvpTeTnlInfo->pRsb->pIncIfEntry;

        /* For resv message, OUT-ARHOP List should be used to create
         * RRO object */
        if ((RSVPTE_OUT_ARHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL) &&
            (TMO_SLL_Count (&RSVPTE_TNL_OUTARHOP_LIST (pRsvpTeTnlInfo))
             != RSVPTE_ZERO))
        {
            pRROList = &(RSVPTE_TNL_OUTARHOP_LIST (pRsvpTeTnlInfo));
        }

        /* For resv message, the downstream label should be in-label
         * And upstream label should be out-label */
        u4DownstrLabel = pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl;
        u4UpstrLabel = pRsvpTeTnlInfo->UpStrOutLbl.u4GenLbl;
    }

    /* Size calculation for hops in the AR-HOP list */
    if (pRROList != NULL)
    {
        TMO_SLL_Scan (pRROList, pRsvpTeArHopInfo, tRsvpTeArHopInfo *)
        {
            /*RRO unnumbered subobject size */
            if (pRsvpTeArHopInfo->u1AddressType == ERO_TYPE_UNNUM_IF)
            {
                u2RROLength = (UINT2) (u2RROLength + RRO_TYPE_UNNUM_LEN);
            }
            else
            {                    /*RRO numbered subobject size */
                u2RROLength = (UINT2) (u2RROLength + RRO_TYPE_IPV4_LEN);
            }

            /*RRO unidirectional label subobject size */
            if (pRsvpTeArHopInfo->GmplsTnlArHopInfo.u4ForwardLbl !=
                MPLS_INVALID_LABEL)
            {
                u2RROLength = (UINT2) (u2RROLength + RRO_TYPE_LBL_LEN);
            }

            /*RRO bidirectional label subobject size */
            if (pRsvpTeArHopInfo->GmplsTnlArHopInfo.u4ReverseLbl !=
                MPLS_INVALID_LABEL)
            {
                u2RROLength = (UINT2) (u2RROLength + RRO_TYPE_LBL_LEN);
            }
        }
    }

    /* Size calculation for current hop */
    /* RRO numbered subobject size */
    if (pIfEntry->u4Addr != RSVPTE_ZERO)
    {
        u2RROLength = (UINT2) (u2RROLength + RRO_TYPE_IPV4_LEN);
    }
    else
    {
        /* RRO unnumbered subobject size */
        u2RROLength = (UINT2) (u2RROLength + RRO_TYPE_UNNUM_LEN);
    }
    if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1Attributes &
        RSVPTE_SSN_LBL_RECORD_BIT)
    {
        if (u4DownstrLabel != MPLS_INVALID_LABEL)
        {
            /* RRO unidirectional label subobject size */
            u2RROLength = (UINT2) (u2RROLength + RRO_TYPE_LBL_LEN);
        }
        if (u4UpstrLabel != MPLS_INVALID_LABEL)
        {
            /* RRO bidirectional label subobject size */
            u2RROLength = (UINT2) (u2RROLength + RRO_TYPE_LBL_LEN);
        }
    }

    return u2RROLength;
}

/************************************************************************
 *  Function Name   : RpteUtlIsCspfComputationReqd
 *  Description     : Function used to find whether CSPF is required or
 *                    not
 *  Input           : pRsvpTeTnlInfo - pointer to tunnel info
 *  Output          : None
 *  Returns         : bIsCspfReqd - TRUE: CSPF required
 ************************************************************************/
BOOL1
RpteUtlIsCspfComputationReqd (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    BOOL1               bIsCspfReqd = RPTE_FALSE;

    if ((pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo != NULL) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlPathInUse != RSVPTE_ZERO))
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo->u1PathCompType
            == TE_DYNAMIC)
        {
            bIsCspfReqd = RPTE_TRUE;
        }
    }
    else if ((pRsvpTeTnlInfo->pTeTnlInfo->pTeBackupPathInfo != NULL) &&
             (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlBackupPathInUse != RSVPTE_ZERO))
    {
        bIsCspfReqd = RPTE_TRUE;
    }
    else if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlPathInUse != RSVPTE_ZERO)
    {
        bIsCspfReqd = RPTE_TRUE;
    }
    if ((RptePortIsOspfTeEnabled () == RPTE_SUCCESS))
    {
        bIsCspfReqd = RPTE_TRUE;
    }
    else
    {
        bIsCspfReqd = RPTE_FALSE;
    }
    return bIsCspfReqd;
}

/************************************************************************
 *  Function Name   : RpteUtlCalculateRSVPHopSize
 *  Description     : Function used to calculate RSVP Hop Object Size
 *  Input           : b1OobFlag        - OOB Flag
 *                    u4InterfaceId    - Interface Identifier
 *                    u4InterfaceAddr  - Interface Address
 *  Output          : None
 *  Returns         : RSVP Hop Object Size
 ************************************************************************/
UINT2
RpteUtlCalculateRSVPHopSize (BOOL1 b1OobFlag, UINT4 u4InterfaceId,
                             UINT4 u4InterfaceAddr)
{
    UINT2               u2RsvpHopSize = RPTE_ZERO;
    if (b1OobFlag == TRUE)
    {
        if (u4InterfaceId != RSVPTE_ZERO)
        {
            u2RsvpHopSize = (UINT2) (u2RsvpHopSize + sizeof (tIfIdRsvpHopObj));
        }
        else
        {
            u2RsvpHopSize =
                (UINT2) (u2RsvpHopSize + sizeof (tIfIdRsvpNumHopObj));
        }
    }
    else
    {
        if (u4InterfaceAddr == RSVPTE_ZERO)
        {
            u2RsvpHopSize = (UINT2) (u2RsvpHopSize + sizeof (tIfIdRsvpHopObj));
        }
        else
        {
            u2RsvpHopSize = (UINT2) (u2RsvpHopSize + sizeof (tRsvpHopObj));
        }
    }
    return u2RsvpHopSize;
}

/************************************************************************
 *  Function Name   : RpteUtlCalculateErrorSpecSize
 *  Description     : Function used to calculate Error Spec Object Size
 *  Input           : b1OobFlag        - OOB Flag
 *                    u4InterfaceId    - Interface Identifier
 *                    u4InterfaceAddr  - Interface Address
 *  Output          : None
 *  Returns         : Error Spec Object Size
 ************************************************************************/
UINT2
RpteUtlCalculateErrorSpecSize (tPktMap * pPktMap)
{
    UINT2               u2ErrorSpecSize = RPTE_ZERO;

    if (pPktMap->pErrorSpecObj != NULL)
    {
        u2ErrorSpecSize = (UINT2) (u2ErrorSpecSize + sizeof (tErrorSpecObj));
    }
    else if (pPktMap->pIfIdRsvpNumErrObj != NULL)
    {
        u2ErrorSpecSize =
            (UINT2) (u2ErrorSpecSize + sizeof (tIfIdRsvpNumErrObj));
    }
    else if (pPktMap->pIfIdRsvpErrObj != NULL)
    {
        u2ErrorSpecSize = (UINT2) (u2ErrorSpecSize + sizeof (tIfIdRsvpErrObj));
    }

    return u2ErrorSpecSize;
}

/************************************************************************
 *  Function Name   : RpteUtlFillErrorTable
 *  Description     : Function used to fill the error table
 *  Input           : pErrorSpec        - Pointer to Error spec structure
 *                    pRsvpTeTnlInfo    - Pointer to RSVP-TE tnl structure
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
RpteUtlFillErrorTable (UINT1 u1ErrCode, UINT2 u2ErrValue,
                       UINT4 u4ErrNodeAddr, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTunnelErrInfo      TunnelErrInfo;
    eTunnelErrType      LastErrorType = TNL_PATH_COMPUTATION_ERROR;
    UINT4               u4Length = RSVPTE_ZERO;
    UINT1               au1HelpString[256];

    MEMSET (&TunnelErrInfo, RPTE_ZERO, sizeof (tTunnelErrInfo));
    MEMSET (au1HelpString, 0, sizeof (au1HelpString));
    if (u1ErrCode != RSVPTE_ZERO)
    {
        if ((u1ErrCode == ADMISSION_CONTROL_FAILURE) ||
            (u1ErrCode == POLICY_CONTROL_FAILURE))
        {
            LastErrorType = TNL_LOCAL_RESOURCES;
        }
        else
        {
            LastErrorType = TNL_PROTOCOL_ERROR;
        }
        RpteUtlGetHelpString (u1ErrCode, u2ErrValue, &au1HelpString[0],
                              &u4Length);
        MEMCPY (&(TunnelErrInfo.au1GmplsErrHelpString), &au1HelpString,
                u4Length);
        TunnelErrInfo.i4GmplsErrHelpStringLength = (INT4) u4Length;
    }
    else
    {
        LastErrorType = TNL_PATH_COMPUTATION_ERROR;
    }
    OsixGetSysTime ((tOsixSysTime *) & (TunnelErrInfo.u4LastErrTime));
    TunnelErrInfo.LastErrType = LastErrorType;
    TunnelErrInfo.ErrorReporterType = GMPLS_IPV4;
    TunnelErrInfo.u4ErrorCode = u1ErrCode;
    TunnelErrInfo.u4SubErrorCode = u2ErrValue;
    TunnelErrInfo.u4ErrorIpAddress = u4ErrNodeAddr;
    TunnelErrInfo.u1ErrorIpAddressLen = RSVPTE_IPV4ADR_LEN;

    rpteSigCreateTnlErrorTable (&TunnelErrInfo, pRsvpTeTnlInfo->pTeTnlInfo);
    return;
}

/************************************************************************
 *  Function Name   : RpteUtlGetHelpString
 *  Description     : Function used to get help string from error code and
 *                    error value
 *  Input           : u1ErrCode       - Error code
 *                    u2ErrValue      - Error value
 *  Output          : pau1HelpString  - Help string value
 *                    pu4Length       - Help string length
 *  Returns         : None
 ************************************************************************/

VOID
RpteUtlGetHelpString (UINT1 u1ErrCode, UINT2 u2ErrValue,
                      UINT1 *pau1HelpString, UINT4 *pu4Length)
{
    UINT4               u4Length = RPTE_ZERO;

    switch (u1ErrCode)
    {
        case ADMISSION_CONTROL_FAILURE:
        {
            MEMCPY (pau1HelpString, "AdmissionControlFailure-DefaultValue",
                    STRLEN ("AdmissionControlFailure-DefaultValue"));
            u4Length = STRLEN ("AdmissionControlFailure-DefaultValue");
            break;
        }

        case POLICY_CONTROL_FAILURE:
        {
            MEMCPY (pau1HelpString, "PolicyControlFailure-DefaultValue",
                    STRLEN ("PolicyControlFailure-DefaultValue"));
            u4Length = STRLEN ("PolicyControlFailure-DefaultValue");
            break;
        }

        case RPTE_ROUTE_PROB:
            switch (u2ErrValue)
            {
                case RPTE_BAD_EXPLICT_ROUTE_OBJ:
                {
                    MEMCPY (pau1HelpString, "RoutingProblem-BadExplicitRoute",
                            STRLEN ("RoutingProblem-BadExplicitRoute"));
                    u4Length = STRLEN ("RoutingProblem-BadExplicitRoute");
                    break;
                }

                case RPTE_BAD_STRICT_NODE:
                {
                    MEMCPY (pau1HelpString, "RoutingProblem-BadStrictNode",
                            STRLEN ("RoutingProblem-BadStrictNode"));
                    u4Length = STRLEN ("RoutingProblem-BadStrictNode");
                    break;
                }

                case RPTE_BAD_LOOSE_NODE:
                {
                    MEMCPY (pau1HelpString, "RoutingProblem-BadLooseNode",
                            STRLEN ("RoutingProblem-BadLooseNode"));
                    u4Length = STRLEN ("RoutingProblem-BadLooseNode");
                    break;
                }

                case RPTE_BAD_INIT_SUB_OBJ:
                {
                    MEMCPY (pau1HelpString, "RoutingProblem-BadInitialSubObj",
                            STRLEN ("RoutingProblem-BadInitialSubObj"));
                    u4Length = STRLEN ("RoutingProblem-BadInitialSubObj");
                    break;
                }

                case RPTE_NO_ROUTE_AVAIL:
                {
                    MEMCPY (pau1HelpString, "RoutingProblem-NoRouteAvailable",
                            STRLEN ("RoutingProblem-NoRouteAvailable"));
                    u4Length = STRLEN ("RoutingProblem-NoRouteAvailable");
                    break;
                }

                case RPTE_UNACTBL_LABEL_VALUE:
                {
                    MEMCPY (pau1HelpString, "RoutingProblem-UnAcceptableLabel",
                            STRLEN ("RoutingProblem-UnAcceptableLabel"));
                    u4Length = STRLEN ("RoutingProblem-UnAcceptableLabel");
                    break;
                }

                case RPTE_RRO_IND_ROUTE_LOOP:
                {
                    MEMCPY (pau1HelpString, "RoutingProblem-RRORouteLoop",
                            STRLEN ("RoutingProblem-RRORouteLoop"));
                    u4Length = STRLEN ("RoutingProblem-RRORouteLoop");
                    break;
                }

                case RPTE_NON_RSVP_RTR:
                {
                    MEMCPY (pau1HelpString, "RoutingProblem-NonRSVPRouter",
                            STRLEN ("RoutingProblem-NonRSVPRouter"));
                    u4Length = STRLEN ("RoutingProblem-NonRSVPRouter");
                    break;
                }

                case RPTE_LBL_ALLOC_FAIL:
                {
                    MEMCPY (pau1HelpString,
                            "RoutingProblem-LabelAllocationFailure",
                            STRLEN ("RoutingProblem-LabelAllocationFailure"));
                    u4Length = STRLEN ("RoutingProblem-LabelAllocationFailure");
                    break;
                }

                case RPTE_UNSUPPORTED_L3PID:
                {
                    MEMCPY (pau1HelpString, "RoutingProblem-UnSupportedL3PID",
                            STRLEN ("RoutingProblem-UnSupportedL3PID"));
                    u4Length = STRLEN ("RoutingProblem-UnSupportedL3PID");
                    break;
                }

                case RPTE_UNSUPPORTED_SWITCHING:
                {
                    MEMCPY (pau1HelpString,
                            "RoutingProblem-UnSupportedSwitching",
                            STRLEN ("RoutingProblem-UnSupportedSwitching"));
                    u4Length = STRLEN ("RoutingProblem-UnSupportedSwitching");
                    break;
                }

                case RPTE_UNSUPPORTED_ENCODING:
                {
                    MEMCPY (pau1HelpString,
                            "RoutingProblem-UnSupportedEncoding",
                            STRLEN ("RoutingProblem-UnSupportedEncoding"));
                    u4Length = STRLEN ("RoutingProblem-UnSupportedEncoding");
                    break;
                }

                default:
                    break;
            }
            break;

        case RPTE_NOTIFY:
            switch (u2ErrValue)
            {
                case RPTE_RRO_TOO_LARGE:
                {
                    MEMCPY (pau1HelpString, "NotifyError-RROTooLarge",
                            STRLEN ("NotifyError-RROTooLarge"));
                    u4Length = STRLEN ("NotifyError-RROTooLarge");
                    break;
                }

                case RPTE_RRO_NOTIFY:
                {
                    MEMCPY (pau1HelpString, "NotifyError-RRONotify",
                            STRLEN ("NotifyError-RRONotify"));
                    u4Length = STRLEN ("NotifyError-Notify");
                    break;
                }

                case RPTE_TNL_LOCAL_REPAIR:
                {
                    MEMCPY (pau1HelpString, "NotifyError-TunnelLocalRepair",
                            STRLEN ("NotifyError-TunnelLocalRepair"));
                    u4Length = STRLEN ("NotifyError-TunnelLocalRepair");
                    break;
                }

                default:
                    break;
            }
            break;

        case RPTE_DIFFSERV_ERROR:
            switch (u2ErrValue)
            {
                case RPTE_UNEXPECTED_DIFFSERV_OBJ:
                {
                    MEMCPY (pau1HelpString, "DiffervError-UnexpectedObject",
                            STRLEN ("DiffervError-UnexpectedObject"));
                    u4Length = STRLEN ("DiffervError-UnexpectedObject");
                    break;
                }

                case RPTE_UNSUPPORTED_PHB:
                {
                    MEMCPY (pau1HelpString, "DiffervError-UnSupportedPHB",
                            STRLEN ("DiffervError-UnSupportedPHB"));
                    u4Length = STRLEN ("DiffervError-UnSupportedPHB");
                    break;
                }

                case RPTE_INVALID_EXPPHB_MAPPING:
                {
                    MEMCPY (pau1HelpString, "DiffervError-InvalidExpPhbMapping",
                            STRLEN ("DiffervError-InvalidExpPhbMapping"));
                    u4Length = STRLEN ("DiffervError-InvalidExpPhbMapping");
                    break;
                }

                case RPTE_UNSUPPORTED_PSC:
                {
                    MEMCPY (pau1HelpString, "DiffervError-UnSupportedPSC",
                            STRLEN ("DiffervError-UnSupportedPSC"));
                    u4Length = STRLEN ("DiffervError-UnSupportedPSC");
                    break;
                }

                case RPTE_LSP_CONTEXT_ALLOC_FAIL:
                {
                    MEMCPY (pau1HelpString,
                            "DiffervError-LSPContextAllocationFailure",
                            STRLEN
                            ("DiffervError-LSPContextAllocationFailure"));
                    u4Length =
                        STRLEN ("DiffervError-LSPContextAllocationFailure");
                    break;
                }

                default:
                    break;
            }
            break;

        case RPTE_DIFFSERV_TE_ERROR:
            switch (u2ErrValue)
            {
                case RPTE_UNEXPECTED_CLASS_TYPE_OBJ:
                {
                    MEMCPY (pau1HelpString,
                            "DiffervTEError-UnexpectedClassType",
                            STRLEN ("DiffervTEError-UnexpectedClassType"));
                    u4Length = STRLEN ("DiffervTEError-UnexpectedClassType");
                    break;
                }

                case RPTE_UNSUPPORTED_CLASS_TYPE:
                {
                    MEMCPY (pau1HelpString,
                            "DiffervTEError-UnSupportedClassType",
                            STRLEN ("DiffervTEError-UnSupportedClassType"));
                    u4Length = STRLEN ("DiffervTEError-UnSupportedClassType");
                    break;
                }

                case RPTE_INVALID_CLASS_TYPE_VALUE:
                {
                    MEMCPY (pau1HelpString, "DiffervTEError-InvalidClassType",
                            STRLEN ("DiffervTEError-InvalidClassType"));
                    u4Length = STRLEN ("DiffervTEError-InvalidClassType");
                    break;
                }

                case RPTE_UNSUPPORTED_CLASS_TYPE_SET_PRIO:
                {
                    MEMCPY (pau1HelpString,
                            "DiffervTEError-UnSupportedClassType,SetupPriority",
                            STRLEN
                            ("DiffervTEError-UnSupportedClassType,SetupPriority"));
                    u4Length =
                        STRLEN
                        ("DiffervTEError-UnSupportedClassType,SetupPriority");
                    break;
                }

                case RPTE_UNSUPPORTED_CLASS_TYPE_HOLD_PRIO:
                {
                    MEMCPY (pau1HelpString,
                            "DiffervTEError-UnSupportedClassType,HoldingPriority",
                            STRLEN
                            ("DiffervTEError-UnSupportedClassType,HoldingPriority"));
                    u4Length =
                        STRLEN
                        ("DiffervTEError-UnSupportedClassType,HoldingPriority");
                    break;
                }

                case RPTE_UNSUPPORTED_CLASS_TYPE_HOLD_PRIO_SET_PRIO:
                {
                    MEMCPY (pau1HelpString,
                            "DiffervTEError-UnSupportedCT,HoldPrio,SetPrio",
                            STRLEN
                            ("DiffervTEError-UnSupportedCT,HoldPrio,SetPrio"));
                    u4Length =
                        STRLEN
                        ("DiffervTEError-UnSupportedCT,HoldPrio,SetPrio");
                    break;
                }

                case RPTE_CLASS_TYPE_AND_PSC_INCONSISTENT:
                {
                    MEMCPY (pau1HelpString,
                            "DiffervTEError-InconsistentClassTypeAndPSC",
                            STRLEN
                            ("DiffervTEError-InconsistentClassTypeAndPSC"));
                    u4Length =
                        STRLEN ("DiffervTEError-InconsistentClassTypeAndPSC");
                    break;
                }

                case RPTE_CLASS_TYPE_AND_PHB_INCONSISTENT:
                {
                    MEMCPY (pau1HelpString,
                            "DiffervTEError-InconsistentClassTypeAndPHB",
                            STRLEN
                            ("DiffervTEError-InconsistentClassTypeAndPHB"));
                    u4Length =
                        STRLEN ("DiffervTEError-InconsistentClassTypeAndPHB");
                    break;
                }

                default:
                    break;
            }
            break;

        default:
            break;
    }
    *pu4Length = u4Length;
    return;
}

/************************************************************************
 *  Function Name   : RpteUtlSetDetourStatus
 *  Description     : This function sets the detour active value for
 *                    the RSVP-TE tunnel.
 *  Input           : pRsvpTeTnlInfo   - Pointer to RSVP-TE Tunnel
 *                    u1DetourStatus   - Detour Status
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
RpteUtlSetDetourStatus (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 u1DetourStatus)
{
    rpteTeSetDetourStatus (RPTE_TE_TNL (pRsvpTeTnlInfo), u1DetourStatus);

    RSVPTE_TE_TNL_FRR_DETOUR_ACTIVE (RPTE_TE_TNL (pRsvpTeTnlInfo))
        = u1DetourStatus;

    return;
}

/*****************************************************************************/
/* Function Name : RpteGetTnlInTnlTable                                     */
/* Description   : This routine Check the presence of a RSVP Tunnel info     */
/*                 in the Tunnel RbTree ignoring tunnel instance.            */
/* Input(s)      : pRsvpTeInfo      - Pointer to the Rsvp Te Global          */
/*                                    information.                           */
/*                 u4TunnelInstance - Tunnel Instance value                  */
/*                 u4TnlIngressAddr - Tunnel Source Addr - IPv4              */
/*                 u4TnlEgressAddr  - Tunnel Destination address - IPv4      */
/* Output(s)     : ppRsvpTeTnlInfo  - Pointer to Pointer of the tunnel info  */
/*                 whose index matches u4TunnelIndex.                        */
/* Return(s)     : RPTE_SUCCESS in case of tunnel being present, otherwise   */
/*                 RPTE_FAILURE                                              */
/*****************************************************************************/
UINT1
RpteGetTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                      UINT4 u4TunnelIndex,
                      UINT4 u4TunnelInstance,
                      UINT4 u4TnlIngressAddr,
                      UINT4 u4TnlEgressAddr, tRsvpTeTnlInfo ** ppRsvpTeTnlInfo)
{
    tRsvpTeTnlInfo      InRpteTnlInfo;

    UNUSED_PARAM (u4TunnelInstance);

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetTnlInTnlTable : ENTRY \n");

    MEMSET (&InRpteTnlInfo, RPTE_ZERO, sizeof (tRsvpTeTnlInfo));

    if (RpteUtlGetTnlInTnlTable (pRsvpTeInfo, u4TunnelIndex, RPTE_ZERO,
                                 u4TnlIngressAddr, u4TnlEgressAddr,
                                 ppRsvpTeTnlInfo, &InRpteTnlInfo)
        == RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetTnlInTnlTable : EXIT \n");
        return RPTE_SUCCESS;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetTnlInTnlTable : INTMD-EXIT \n");
    return RPTE_FAILURE;

}

/*****************************************************************************/
/* Function Name : RpteUtlGetTnlInTnlTable                                     */
/* Description   : This routine Check the presence of a RSVP Tunnel info     */
/*                 in the Tunnel RbTree ignoring tunnel instance.            */
/* Input(s)      : pRsvpTeInfo      - Pointer to the Rsvp Te Global          */
/*                                    information.                           */
/*                 u4TunnelInstance - Tunnel Instance value                  */
/*                 u4TnlIngressAddr - Tunnel Source Addr - IPv4              */
/*                 u4TnlEgressAddr  - Tunnel Destination address - IPv4      */
/*                 pRpteInTnlInfo   - Pointer to RSVPTE tnl info             */
/* Output(s)     : ppRsvpTeTnlInfo  - Pointer to Pointer of the tunnel info  */
/*                 whose index matches u4TunnelIndex.                        */
/* Return(s)     : RPTE_SUCCESS in case of tunnel being present, otherwise   */
/*                 RPTE_FAILURE                                              */
/*****************************************************************************/
UINT1
RpteUtlGetTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                         UINT4 u4TunnelIndex,
                         UINT4 u4TunnelInstance,
                         UINT4 u4TnlIngressAddr,
                         UINT4 u4TnlEgressAddr,
                         tRsvpTeTnlInfo ** ppRsvpTeTnlInfo,
                         tRsvpTeTnlInfo * pRpteInTnlInfo)
{
    tTeTnlInfo          InTeTnlInfo;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;

    MEMSET (&InTeTnlInfo, RPTE_ZERO, sizeof (tTeTnlInfo));

    u4TnlIngressAddr = OSIX_HTONL (u4TnlIngressAddr);
    u4TnlEgressAddr = OSIX_HTONL (u4TnlEgressAddr);

    InTeTnlInfo.u4TnlIndex = u4TunnelIndex;
    InTeTnlInfo.u4TnlPrimaryInstance = u4TunnelInstance;
    MEMCPY (&InTeTnlInfo.TnlIngressLsrId, &u4TnlIngressAddr, IPV4_ADDR_LENGTH);
    MEMCPY (&InTeTnlInfo.TnlEgressLsrId, &u4TnlEgressAddr, IPV4_ADDR_LENGTH);
    pRpteInTnlInfo->pTeTnlInfo = &InTeTnlInfo;

    pTmpRsvpTeTnlInfo = RBTreeGetNext (pRsvpTeInfo->RpteTnlTree,
                                       (tRBElem *) pRpteInTnlInfo, NULL);
    if ((pTmpRsvpTeTnlInfo != NULL) &&
        (u4TunnelIndex == RSVPTE_TNL_TNLINDX (pTmpRsvpTeTnlInfo)) &&
        (u4TnlIngressAddr == (RSVPTE_TNL_INGRESS
                              (pTmpRsvpTeTnlInfo))) &&
        (u4TnlEgressAddr == (RSVPTE_TNL_EGRESS (pTmpRsvpTeTnlInfo))))
    {
        *ppRsvpTeTnlInfo = pTmpRsvpTeTnlInfo;
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetTnlInTnlTable : EXIT \n");
        return RPTE_SUCCESS;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteGetTnlInTnlTable : INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/************************************************************************
 *  Function Name   : RpteUtlNtfyMsgSizeCalculation
 *  Description     : Function used to calculate notify message Size
 *  Input           : pNotifyRecipient - Pointer to recipient structure
 *                    u4InterfaceId    - Interface Identifier
 *  Output          : None
 *  Returns         : Notify message Size                                                                          ************************************************************************/

UINT4
RpteUtlNtfyMsgSizeCalculation (tNotifyRecipient * pNotifyRecipient)
{
    UINT4               u4NotifyMsgSize = RPTE_ZERO;

    u4NotifyMsgSize = sizeof (tRsvpHdr) + sizeof (tErrorSpecObj) +
        pNotifyRecipient->u4MsgSize + RPTE_MSG_ID_OBJ_LEN;

    return u4NotifyMsgSize;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlSwitchTraffic
 * Description     : This function switches the traffic from working to protected
 *                   tunnels and vice versa .
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 *                   u1SwitchType   - switch back or switch over
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteUtlSwitchTraffic (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 u1SwitchType)
{
    tLspInfo            LspInfo;
    tFecParams          FecParams;
    tNHLFE              Nhlfe;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlSwitchTraffic : ENTRY \n");

    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlSwitchTraffic : ENTRY \n");

#ifdef MPLS_L3VPN_WANTED
    tL3VpnRsvpTeLspEventInfo L3VpnRsvpTeLspEventInfo;

    MEMSET (&L3VpnRsvpTeLspEventInfo, 0, sizeof (L3VpnRsvpTeLspEventInfo));
#endif

    MEMSET (&Nhlfe, RSVPTE_ZERO, sizeof (tNHLFE));
    MEMSET (&LspInfo, RSVPTE_ZERO, sizeof (tLspInfo));
    MEMSET (&FecParams, RSVPTE_ZERO, sizeof (tFecParams));

    if (pRsvpTeTnlInfo->pMapTnlInfo == NULL)
    {
        return;
    }

    if (((u1SwitchType == RPTE_PROTECTION_SWITCH_OVER) &&
         (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
          LOCAL_PROT_IN_USE)) || ((u1SwitchType == RPTE_PROTECTION_SWITCH_BACK)
                                  && (pRsvpTeTnlInfo->pTeTnlInfo->
                                      u1TnlLocalProtectInUse ==
                                      LOCAL_PROT_AVAIL)))
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlSwitchTraffic : EXIT1 \n");
        return;
    }
    RpteUtlSetAdminFlagsAndRefreshMsg (pRsvpTeTnlInfo, u1SwitchType);

    if (pRsvpTeTnlInfo->pMapTnlInfo == NULL)    /*COverity fix */
    {
        return;
    }

    if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u4RevTnlIfIndex == RPTE_ZERO))
    {
        if (u1SwitchType == RPTE_PROTECTION_SWITCH_OVER)
        {
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse =
                LOCAL_PROT_IN_USE;
        }
        else
        {
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse =
                LOCAL_PROT_AVAIL;
        }
        /*return; */
    }

    FecParams.u1FecType = RSVPTE_TNL_TYPE;
    FecParams.u4TnlId = RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);
    FecParams.u4TnlInstance =
        RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (pRsvpTeTnlInfo));
    FecParams.u4IngressId = OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo));
    FecParams.u4EgressId = OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));
    MEMCPY (&LspInfo.FecParams, &FecParams, sizeof (tFecParams));
    Nhlfe.u1OperStatus = RPTE_OPER_UP;
    LspInfo.pNhlfe = (tNHLFE *) & Nhlfe;

    switch (u1SwitchType)
    {
        case RPTE_PROTECTION_SWITCH_OVER:
        {

            /* Tunnel is now using  protected path */
            /* So update the mpls binding accordingly on protected path */
            if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
                (RpteUpdateOne2OneTunnelDelFtnEntry (pRsvpTeTnlInfo) ==
                 RPTE_FAILURE))
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUtlSwitchTraffic: "
                            "Fail to Delete FtnEntry in hardware in switch over  case \n");
            }
            else
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUtlSwitchTraffic: "
                            "Success to Delete FtnEntry in hardware in switch over  case \n");
            }
            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteUtlSwitchTraffic ---Switchover: ENTRY \n");

            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteUtlSwitchTraffic ---Switchover: ENTRY \n");
            if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
            {
                LspInfo.Direction = MPLS_DIRECTION_FORWARD;
                if (pRsvpTeTnlInfo->pMapTnlInfo != NULL)
                {
                    pRsvpTeTnlInfo->pTeTnlInfo->u4OrgTnlIfIndex =
                        pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->u4TnlIfIndex;
                    pRsvpTeTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
                        pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->u4TnlInstance;
                }
            }
            else if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS)
            {

                LspInfo.Direction = MPLS_DIRECTION_REVERSE;
                if (pRsvpTeTnlInfo->pMapTnlInfo != NULL)
                {
                    pRsvpTeTnlInfo->pTeTnlInfo->u4OrgTnlIfIndex =
                        pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
                        u4RevTnlIfIndex;
                }
            }
            if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlXcIndex !=
                pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->u4TnlXcIndex)
            {

                pRsvpTeTnlInfo->pTeTnlInfo->u4OrgTnlXcIndex =
                    pRsvpTeTnlInfo->pTeTnlInfo->u4TnlXcIndex;
            }
            MEMCPY (pRsvpTeTnlInfo->pTeTnlInfo->au1BkpNextHopMac,
                    pRsvpTeTnlInfo->pTeTnlInfo->au1NextHopMac, MAC_ADDR_LEN);

            if (pRsvpTeTnlInfo->pMapTnlInfo != NULL)
            {
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlXcIndex =
                    pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->u4TnlXcIndex;
                /*Update Backup Tunnel Instance in Tunnel Info structure */
                pRsvpTeTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
                    pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->u4TnlInstance;

                MEMCPY (pRsvpTeTnlInfo->pTeTnlInfo->au1NextHopMac,
                        pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->au1NextHopMac,
                        MAC_ADDR_LEN);
            }
            if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
                (RpteUpdateOne2OneTunnelAddFtnEntry (pRsvpTeTnlInfo) ==
                 RPTE_FAILURE))
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUtlSwitchTraffic: "
                            "Fail to Add FtnEntry in hardware in switch over  case \n");
            }
            else
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUtlSwitchTraffic: "
                            "Success to Add FtnEntry in hardware in switch over  case \n");
            }

            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse =
                LOCAL_PROT_IN_USE;

            /*Switching Type To Set For Use In L2Vpn */
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSwitchingType =
                RPTE_PROTECTION_SWITCH_OVER;
#ifdef MPLS_L3VPN_WANTED
            /* Notify L3VPN L3VPN_RSVPTE_LSP_UP. Since we will notify OPER_DOWN for working
               ahead of this notification */
            L3VpnRsvpTeLspEventInfo.u4TnlIndex =
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex;
            L3VpnRsvpTeLspEventInfo.u4TnlXcIndex =
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlXcIndex;
            L3VpnRsvpTeLspEventInfo.u1Protection = LOCAL_PROT_IN_USE;
            L3VpnRsvpTeLspEventInfo.u4EventType = L3VPN_RSVPTE_LSP_UP;
            L3VpnRsvpTeLspStatusChangeEventHandler (L3VpnRsvpTeLspEventInfo);
#endif
            break;
        }

        case RPTE_PROTECTION_SWITCH_BACK:
        {
            if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
                (RpteUpdateOne2OneTunnelDelFtnEntry (pRsvpTeTnlInfo) ==
                 RPTE_FAILURE))
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUtlSwitchTraffic: "
                            "Fail to Delete FtnEntry in hardware in switch back  case \n");
            }
            else
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUtlSwitchTraffic: "
                            "Success to Delete FtnEntry in hardware in switch back  case \n");
            }

            RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                        "RpteUtlSwitchTraffic ---SwitchBack: ENTRY \n");
            if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
            {
                LspInfo.Direction = MPLS_DIRECTION_FORWARD;
            }
            else if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS)
            {
                LspInfo.Direction = MPLS_DIRECTION_REVERSE;
            }

            if (pRsvpTeTnlInfo->pTeTnlInfo->u4OrgTnlXcIndex != RPTE_ZERO)
            {
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlXcIndex =
                    pRsvpTeTnlInfo->pTeTnlInfo->u4OrgTnlXcIndex;
                pRsvpTeTnlInfo->pTeTnlInfo->u4OrgTnlXcIndex = RPTE_ZERO;
                MEMCPY (pRsvpTeTnlInfo->pTeTnlInfo->au1NextHopMac,
                        pRsvpTeTnlInfo->pTeTnlInfo->au1BkpNextHopMac,
                        MAC_ADDR_LEN);
            }

            if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
                (RpteUpdateOne2OneTunnelAddFtnEntry (pRsvpTeTnlInfo) ==
                 RPTE_FAILURE))
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUtlSwitchTraffic: "
                            "Fail to Add FtnEntry in hardware in switch over  case \n");
            }
            else
            {
                RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUtlSwitchTraffic: "
                            "Success to Add FtnEntry in hardware in switch over  case \n");
            }

            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse =
                LOCAL_PROT_AVAIL;
            /*Switching Type To Set For Use In L2Vpn */
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSwitchingType =
                RPTE_PROTECTION_SWITCH_BACK;
#ifdef MPLS_L3VPN_WANTED
            /* Notify L3VPN for LSP UP */
            L3VpnRsvpTeLspEventInfo.u4TnlIndex =
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex;
            L3VpnRsvpTeLspEventInfo.u4TnlXcIndex =
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlXcIndex;
            L3VpnRsvpTeLspEventInfo.u1Protection = LOCAL_PROT_AVAIL;
            L3VpnRsvpTeLspEventInfo.u4EventType = L3VPN_RSVPTE_LSP_UP;
            L3VpnRsvpTeLspStatusChangeEventHandler (L3VpnRsvpTeLspEventInfo);
#endif
            RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUtlSwitchTraffic: "
                        "setting protection status as avaialable--switch back case\n");
            break;
        }
        default:
            break;
    }

    TeSigProcessL2VpnAssociation (pRsvpTeTnlInfo->pTeTnlInfo, TE_TNL_REESTB);
    if (MplsMlibUpdate (MPLS_MLIB_TNL_MODIFY, &LspInfo) == MPLS_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlSwitchTraffic : EXIT2 \n");
        return;
    }
    RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUtlSwitchTraffic : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlFillExcludeHopList
 * Description     : This function fills the Hops to be excluded  .
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 *                   u4ErrNodeAddr  - Hop address to be excluded
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteUtlFillExcludeHopList (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT4 u4ErrNodeAddr)
{
    UINT4               u1ExcludeHop = RPTE_ZERO;
    for (u1ExcludeHop = RPTE_ZERO; u1ExcludeHop < MPLS_TE_MAX_HOPS;
         u1ExcludeHop++)
    {
        if (pRsvpTeTnlInfo->au4ExcludeHop[u1ExcludeHop] == RPTE_ZERO)
        {
            pRsvpTeTnlInfo->au4ExcludeHop[u1ExcludeHop] = u4ErrNodeAddr;
            pRsvpTeTnlInfo->u1NoOfExcludeHop++;
            break;
        }
        else
        {
            if (pRsvpTeTnlInfo->au4ExcludeHop[u1ExcludeHop] == u4ErrNodeAddr)
            {
                break;
            }
        }
    }
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlSetAdminFlagsAndRefreshMsg
 * Description     : This function sets the Admin status flags and sends the 
 *                   refresh messages immediately for switch over/switch back
 *                   cases  .
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 *                   u1SwitchType   - switch back or switch over
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteUtlSetAdminFlagsAndRefreshMsg (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                   UINT1 u1SwitchType)
{
    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable != RPTE_ENABLED)
    {
        gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable = RPTE_ENABLED;
        gpRsvpTeGblInfo->RsvpTeCfgParams.u1IntAdminStatusCapable = RPTE_TRUE;
    }

    if (u1SwitchType == RPTE_PROTECTION_SWITCH_OVER)
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
        {
            pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus =
                GMPLS_ADMIN_ADMIN_DOWN;
        }
        else
        {
            pRsvpTeTnlInfo->pRsb->u4AdminStatus = GMPLS_ADMIN_ADMIN_DOWN;
        }
    }
    else
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
        {
            pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus =
                GMPLS_ADMIN_UNKNOWN;
        }
        else
        {
            pRsvpTeTnlInfo->pRsb->u4AdminStatus = GMPLS_ADMIN_UNKNOWN;
        }
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
    {
        RptePhPathRefresh (pRsvpTeTnlInfo);
    }
    else
    {
        RpteRhResvRefresh (pRsvpTeTnlInfo);
    }

    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUtlCalculateHelloMsgSize
 * Description     : This function calculates hello message size
 * Input (s)       : pNbrEntry - Pointer to neighbor entry
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

UINT2
RpteUtlCalculateHelloMsgSize (tNbrEntry * pNbrEntry, tIfEntry * pIfEntry)
{
    UINT2               u2HelloSize = RPTE_ZERO;

    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability !=
        RPTE_GR_CAPABILITY_NONE)
    {
        if (gi4MplsSimulateFailure != RPTE_SIM_FAILURE_NO_RESTART_CAP_OBJ)
        {
            u2HelloSize = (UINT2) (u2HelloSize + sizeof (tRestartCapObj));
            pIfEntry->IfStatsInfo.u4IfNumHelloSentWithRestartCap++;
        }
        if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1RecoveryPathCapability !=
            RPTE_ZERO)
        {
            u2HelloSize = (UINT2) (u2HelloSize + sizeof (tCapabilityObj));
            pIfEntry->IfStatsInfo.u4IfNumHelloSentWithCapability++;
        }
    }
    /* if S bit is set and R bit is not set in the received hello message,
     * helper should send hello message without capability object
     * */
    if ((pNbrEntry->u2RestartTime != RPTE_ZERO) &&
        (pNbrEntry->u1RecoveryPathCapability & RPTE_GR_RECOVERY_PATH_SREFRESH)
        && (!(pNbrEntry->u1RecoveryPathCapability & RPTE_GR_RECOVERY_PATH_RX)))
    {
        if (u2HelloSize >= sizeof (tCapabilityObj))
        {
            u2HelloSize = (UINT2) (u2HelloSize - sizeof (tCapabilityObj));
        }
        else
        {
            u2HelloSize = RPTE_ZERO;
        }

        pIfEntry->IfStatsInfo.u4IfNumHelloSentWithCapability--;
    }
    return u2HelloSize;
}

/****************************************************************************/
/* Function Name   : RpteStartLinkUpWaitTimer                               */
/* Description     : This function starts wait timer at link up event       */
/*                   At expiry of this LSP timer Reoptmization is triggered */
/* Input (s)       : pRsvpTeTnlInfo                                         */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteStartLinkUpWaitTimer (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTimerParam        *pTmrParam = NULL;

    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteStartLinkUpWaitTimer : ENTRY\n");

    pTmrParam =
        (tTimerParam *) & RPTE_LINK_UP_WAIT_TIMER_PARAM (pRsvpTeTnlInfo);

    TIMER_PARAM_ID (pTmrParam) = RPTE_LINK_UP_WAIT_INTERVAL;
    TIMER_PARAM_RPTE_TNL (pTmrParam) = pRsvpTeTnlInfo;
    RSVP_TIMER_NAME (&RPTE_LINK_UP_WAIT_TIMER (pRsvpTeTnlInfo)) =
        (FS_ULONG) pTmrParam;
    if (RpteUtlStartTimer
        (RSVP_GBL_TIMER_LIST, &RPTE_LINK_UP_WAIT_TIMER (pRsvpTeTnlInfo),
         (UINT4) (RPTE_LINK_UP_WAIT_TIME * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) !=
        TMR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_REOPT_DBG,
                    "RESV: Starting of RpteStartLinkUpWaitTimer failed ..\n");
    }
    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteStartLinkUpWaitTimer : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteHandleIfUpEvtForLspReoptimization            */
/* Description     : This function handling trigger of Lsp Reoptimization   */
/*                     When interface come up. It starts the wait timer for   */
/*                     each Reoptimization enabled Lsp.                       */
/*                   At expiry of this Wait timer Reoptmization is triggered*/
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RpteHandleIfUpEvtForLspReoptimization ()
{

    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_REOPT_DBG,
                "RpteHandleIfUpEvtForLspReoptimization : ENTRY\n");

    pTmpRsvpTeTnlInfo = RBTreeGetFirst (gpRsvpTeGblInfo->RpteTnlTree);

    while (pTmpRsvpTeTnlInfo != NULL)

    {
        if (RPTE_TE_TNL (pTmpRsvpTeTnlInfo) == NULL)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }

        if (RSVPTE_TNL_ADMIN_STATUS
            (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) != RPTE_ADMIN_UP)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }

        if ((RSVPTE_TNL_NODE_RELATION (pTmpRsvpTeTnlInfo) == RPTE_EGRESS) ||
            (RSVPTE_TNL_PSB (pTmpRsvpTeTnlInfo) == NULL))
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }

        if (RSVPTE_TNL_OPER_STATUS
            (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) != RPTE_OPER_UP)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }

        if ((RSVPTE_TE_TNL_REOPTIMIZE_STATUS
             (RPTE_TE_TNL (pTmpRsvpTeTnlInfo))) != TE_REOPTIMIZE_ENABLE)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }

        if ((RSVPTE_TNL_NODE_RELATION (pTmpRsvpTeTnlInfo) == RPTE_INTERMEDIATE)
            && (RPTE_REOPTIMIZE_NODE_STATE (pTmpRsvpTeTnlInfo) !=
                RPTE_MID_POINT_NODE))
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }

        RpteStartLinkUpWaitTimer (pTmpRsvpTeTnlInfo);

        pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                           (tRBElem *) pTmpRsvpTeTnlInfo, NULL);
    }
    RSVPTE_DBG (RSVPTE_REOPT_DBG,
                "RpteHandleIfUpEvtForLspReoptimization : EXIT\n");
}

/****************************************************************************/
/* Function Name   : RpteIsFirstHopAsLoose                                   */
/* Description     : This function checks whether the first hop is Loose in */
/*                   ERO or not                                             */
/* Input (s)       : pRsvpTeTnlInfo                                         */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS / RPTE_FAILURE                            */
/****************************************************************************/

INT1
RpteIsFirstHopAsLoose (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{

    tTMO_SLL           *pEROList = NULL;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;

    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteIsFirstHopAsLoose : ENTRY\n");

    if (pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_REOPT_DBG,
                    "RpteIsFirstHopAsLoose : Path Info is NULL\n");
        return RPTE_FAILURE;
    }

    pEROList = &pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo->ErHopList;

    if (pEROList == NULL)
    {
        RSVPTE_DBG (RSVPTE_REOPT_DBG,
                    "RpteIsFirstHopAsLoose : Ero List is empty\n");
        return RPTE_FAILURE;
    }

    pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_First (pEROList);

    if (pRsvpTeErHopInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_REOPT_DBG,
                    "RpteIsFirstHopAsLoose : Ero Hop info is NULL\n");
        return RPTE_FAILURE;
    }

    if (pRsvpTeErHopInfo->u1HopType == RPTE_ERHOP_LOOSE)
    {
        return RPTE_SUCCESS;
    }

    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteIsFirstHopAsLoose : EXIT\n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteTriggerReoptimization                              */
/* Description     : This function send an event to RSVP-TE for creation    */
/*                   of a new tunnel on more reoptimized path               */
/* Input (s)       : pRsvpTeTnlInfo - RSVP Tunnel info.                     */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteTriggerReoptimization (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                           UINT4 u4TriggerEvent)
{
    UINT4               u4TnlId = RPTE_ZERO;
    UINT4               u4IngressId = RPTE_ZERO;
    UINT4               u4EgressId = RPTE_ZERO;
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;

    MEMSET (&RpteEnqueueMsgInfo, 0, sizeof (tRpteEnqueueMsgInfo));

    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteTriggerReoptimization : ENTRY\n");
    RSVPTE_DBG1 (RSVPTE_REOPT_DBG,
                 "RpteTriggerReoptimization - Trigger Type(%d)\n",
                 u4TriggerEvent);

    if (pRsvpTeTnlInfo == NULL)
    {
        return;
    }

    u4TnlId = RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);
    u4IngressId = OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo));
    u4EgressId = OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));

    switch (u4TriggerEvent)
    {
        case RPTE_REOPT_MANUAL_TRIGGER:

            if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo) == RPTE_INGRESS)
            {
                if (pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo == NULL)
                {
                    if (rpteTeCreateReoptimizeTunnel
                        (u4TnlId, u4IngressId, u4EgressId) == RPTE_TE_FAILURE)
                    {
                        return;
                    }
                }
                else
                {
                    /* If Next Hop as Loose Hop */

                    if (RpteIsFirstHopAsLoose (pRsvpTeTnlInfo) == RPTE_SUCCESS)
                    {
                        RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) =
                            RPTE_HEAD_MID_POINT_NODE;
                        RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) =
                            RPTE_REOPT_MANUAL_TRIGGER;
                        if (rpteTeCreateReoptimizeTunnel
                            (u4TnlId, u4IngressId,
                             u4EgressId) == RPTE_TE_FAILURE)
                        {
                            return;
                        }
                    }
                    /* If Next Hop is strict but ERO may consist Loose Hop */
                    else
                    {
                        RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) =
                            RPTE_TRUE;
                        RptePhPathRefresh (pRsvpTeTnlInfo);
                        RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) =
                            RPTE_FALSE;

                    }
                }
            }
            /*Mid Node Behaviour - Whose next hop is loose */

            if (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) ==
                RPTE_MID_POINT_NODE)
            {
                if (((RSVPTE_TE_TNL_REOPTIMIZE_STATUS
                      (RPTE_TE_TNL (pRsvpTeTnlInfo))) == TE_REOPTIMIZE_ENABLE))
                {
                    if (RpteCspfCompForNormalTnl (pRsvpTeTnlInfo) ==
                        RPTE_FAILURE)
                    {
                        RSVPTE_DBG (RSVPTE_REOPT_DBG,
                                    "RpteTriggerReoptimization : Cspf Computation Request Failed\n");
                        return;
                    }

                    RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) =
                        RPTE_REOPT_MANUAL_TRIGGER;
                }
            }

            break;
        case RPTE_REOPT_TIMER_EXPIRY:
            /* Head Node Behaviour in case of CSPF and Next hop as Loose in ERO */

            if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo) == RPTE_INGRESS)
            {
                if (pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo == NULL)
                {
                    if (rpteTeCreateReoptimizeTunnel
                        (u4TnlId, u4IngressId, u4EgressId) == RPTE_TE_FAILURE)
                    {
                        RpteRhStartReoptimizeTmr (pRsvpTeTnlInfo);
                    }
                }
                else
                {
                    /* If Next Hop as Loose Hop */

                    if (RpteIsFirstHopAsLoose (pRsvpTeTnlInfo) == RPTE_SUCCESS)
                    {

                        /*Calculate the Path with First Loose Hop, if both path are Equal 
                         *then Send a path message with "Path Re-evaluation Request"
                         *Put this Logic in Handling of Process CSPF MEssage */

                        RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) =
                            RPTE_HEAD_MID_POINT_NODE;
                        RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) =
                            RPTE_REOPT_TIMER_EXPIRY;
                        if (rpteTeCreateReoptimizeTunnel
                            (u4TnlId, u4IngressId,
                             u4EgressId) == RPTE_TE_FAILURE)
                        {
                            RpteRhStartReoptimizeTmr (pRsvpTeTnlInfo);
                            return;
                        }
                    }
                    /* If Next Hop is strict but ERO may consist Loose Hop */
                    else
                    {
                        /*Send a path message with "Path Re-evaluation Request" */
                        RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) =
                            RPTE_TRUE;
                        RptePhPathRefresh (pRsvpTeTnlInfo);
                        RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) =
                            RPTE_FALSE;
                        RpteRhStartReoptimizeTmr (pRsvpTeTnlInfo);
                    }
                }
            }
            /*Mid Node Behaviour - Whose next hop is loose */

            if (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) ==
                RPTE_MID_POINT_NODE)
            {
                if (((RSVPTE_TE_TNL_REOPTIMIZE_STATUS
                      (RPTE_TE_TNL (pRsvpTeTnlInfo))) == TE_REOPTIMIZE_ENABLE))
                {
                    if (RpteCspfCompForNormalTnl (pRsvpTeTnlInfo) ==
                        RPTE_FAILURE)
                    {
                        RSVPTE_DBG (RSVPTE_REOPT_DBG,
                                    "RpteTriggerReoptimization : Cspf Computation Request Failed\n");
                        return;
                    }

                    RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) =
                        RPTE_REOPT_TIMER_EXPIRY;
                }

            }
            break;
        case RPTE_REOPT_LINK_UP:

            if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo) == RPTE_INGRESS)
            {
                if (pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo == NULL)
                {
                    if (rpteTeCreateReoptimizeTunnel
                        (u4TnlId, u4IngressId, u4EgressId) == RPTE_TE_FAILURE)
                    {
                        return;
                    }
                }
                else
                {
                    /* If Next Hop as Loose Hop */

                    if (RpteIsFirstHopAsLoose (pRsvpTeTnlInfo) == RPTE_SUCCESS)
                    {
                        RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) =
                            RPTE_HEAD_MID_POINT_NODE;
                        RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) =
                            RPTE_REOPT_LINK_UP;

                        if (rpteTeCreateReoptimizeTunnel
                            (u4TnlId, u4IngressId,
                             u4EgressId) == RPTE_TE_FAILURE)
                        {
                            return;
                        }
                    }
                    /* If Next Hop is strict but ERO may consist Loose Hop */
                    else
                    {
                        /*Send a path message with "Path Re-evaluation Request" */
                        RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) =
                            RPTE_TRUE;
                        RptePhPathRefresh (pRsvpTeTnlInfo);
                        RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) =
                            RPTE_FALSE;
                    }
                }
            }
            /*Mid Node Behaviour - Whose next hop is loose */

            if (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) ==
                RPTE_MID_POINT_NODE)
            {
                if (((RSVPTE_TE_TNL_REOPTIMIZE_STATUS
                      (RPTE_TE_TNL (pRsvpTeTnlInfo))) == TE_REOPTIMIZE_ENABLE))
                {
                    if (RpteCspfCompForNormalTnl (pRsvpTeTnlInfo) ==
                        RPTE_FAILURE)
                    {
                        RSVPTE_DBG (RSVPTE_REOPT_DBG,
                                    "RpteTriggerReoptimization : Cspf Computation Request Failed\n");
                        return;
                    }

                    RPTE_REOPT_TRIGGER_TYPE (pRsvpTeTnlInfo) =
                        RPTE_REOPT_LINK_UP;
                }
            }
            break;
        case RPTE_REOPT_LINK_MAINTENANCE:

            if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo) == RPTE_INGRESS)
            {
                if (pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo == NULL)
                {
                    if (rpteTeCreateReoptimizeTunnel
                        (u4TnlId, u4IngressId, u4EgressId) == RPTE_TE_FAILURE)
                    {
                        return;
                    }
                }
                else
                {
                    /* If Next Hop as Loose Hop */
                    if (RpteIsFirstHopAsLoose (pRsvpTeTnlInfo) == RPTE_SUCCESS)
                    {
                        if (rpteTeCreateReoptimizeTunnel
                            (u4TnlId, u4IngressId,
                             u4EgressId) == RPTE_TE_FAILURE)
                        {
                            return;
                        }
                    }
                    /* If Next Hop is strict but ERO may consist Loose Hop */
                    else
                    {
                        return;
                    }
                }
            }
            /*Mid Node Behaviour - Whose next hop is loose */
            if (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) ==
                RPTE_MID_POINT_NODE)
            {
                if (RpteCspfCompForReoptimizeTnlAtMidNode (pRsvpTeTnlInfo) ==
                    RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_REOPT_DBG,
                                "RpteTriggerReoptimization : Cspf Computation Request Failed\n");
                    return;
                }
            }
            break;

        case RPTE_REOPT_NODE_MAINTENANCE:

            if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo) == RPTE_INGRESS)
            {
                if (pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo == NULL)
                {
                    if (rpteTeCreateReoptimizeTunnel
                        (u4TnlId, u4IngressId, u4EgressId) == RPTE_TE_FAILURE)
                    {
                        return;
                    }
                }
                else
                {
                    /* If Next Hop as Loose Hop */
                    if (RpteIsFirstHopAsLoose (pRsvpTeTnlInfo) == RPTE_SUCCESS)
                    {
                        if (rpteTeCreateReoptimizeTunnel
                            (u4TnlId, u4IngressId,
                             u4EgressId) == RPTE_TE_FAILURE)
                        {
                            return;
                        }
                    }
                    /* If Next Hop is strict but ERO may consist Loose Hop */
                    else
                    {

                    }
                }
            }
            /*Mid Node Behaviour - Whose next hop is loose */

            if (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) ==
                RPTE_MID_POINT_NODE)
            {
                if (RpteCspfCompForReoptimizeTnlAtMidNode (pRsvpTeTnlInfo) ==
                    RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_REOPT_DBG,
                                "RpteTriggerReoptimization : Cspf Computation Request Failed\n");
                    return;
                }
            }

            break;
        case RPTE_REOPT_PREFER_PATH_EXIST:
            if (rpteTeCreateReoptimizeTunnel (u4TnlId, u4IngressId, u4EgressId)
                == RPTE_TE_FAILURE)
            {
                return;
            }
            break;
        default:
            break;
    }
    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteTriggerReoptimization : EXIT\n");
}

/****************************************************************************/
/* Function Name   : RpteReoptimizeTimeOut                                  */
/* Description     : This function trigger the LSP Reoptimization at        */
/*                      Reoptimize Time Out                                    */
/* Input (s)       : pRsvpTeTnlInfo - RSVP Tunnel info.                     */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteReoptimizeTimeOut (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteReoptimizeTimeOut : ENTRY\n");

    RpteTriggerReoptimization (pRsvpTeTnlInfo, RPTE_REOPT_TIMER_EXPIRY);

    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteReoptimizeTimeOut : EXIT\n");
}

/****************************************************************************/
/* Function Name   : RpteEroCacheTimeOut                                    */
/* Description     : This function flush the ERO Cache for tunnel at        */
/*                      Ero Cache Time Out                                     */
/* Input (s)       : pRsvpTeTnlInfo - RSVP Tunnel info.                     */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteEroCacheTimeOut (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteEroCacheTimeOut : ENTRY\n");

    rpteTeDeleteCHopListInfo (pRsvpTeTnlInfo->pTeTnlInfo->pTeEroCacheListInfo);
    pRsvpTeTnlInfo->pTeTnlInfo->pTeEroCacheListInfo = NULL;
    pRsvpTeTnlInfo->pTeTnlInfo->u4TnlEroCacheTableIndex = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteEroCacheTimeOut : EXIT\n");
}

/****************************************************************************/
/* Function Name   : RpteLinkUpWaitTimeOut                                  */
/* Description     : This function trigger Lsp Reoptimization for all       */
/*                      Reoptimize tunnel at Link Up Wait Time Out.            */
/* Input (s)       : pRsvpTeTnlInfo - RSVP Tunnel info.                     */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteLinkUpWaitTimeOut (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteLinkUpWaitTimeOut : ENTRY\n");

    RpteTriggerReoptimization (pRsvpTeTnlInfo, RPTE_REOPT_LINK_UP);

    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteLinkUpWaitTimeOut : EXIT\n");
}

 /* RSVP-TE MPLS-L3VPN-TE  */
 /*---------------------------------------------------------------------------*/
 /*
  * Function Name   : RpteUtlFetchTnlInfoFromTnlTable
  * Description     : This function will check the RSVP-TE RB Tree and fetch the
  *                   tunnel info based on tunnel index.
  * Input (s)       : UINT4 u4TunnelIndex - Fetch tunnel info based on this index
  * Output (s)      : tRsvpTeTnlInfo * pRpteInTnlInfo
  * Returns         : RPTE_SUCCESS/RPTE_FAILURE
  */
 /*---------------------------------------------------------------------------*/

UINT1
RpteUtlFetchTnlInfoFromTnlTable (UINT4 u4TunnelIndex,
                                 tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{

    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;

    if (gpRsvpTeGblInfo->RpteTnlTree == NULL)
    {
        return RPTE_FAILURE;
    }

    pTmpRsvpTeTnlInfo = RBTreeGetFirst (gpRsvpTeGblInfo->RpteTnlTree);

    while (pTmpRsvpTeTnlInfo != NULL)
    {
        if (pTmpRsvpTeTnlInfo->pTeTnlInfo != NULL)
        {
            if (u4TunnelIndex == pTmpRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex)
            {
                MEMCPY (pRsvpTeTnlInfo, pTmpRsvpTeTnlInfo,
                        sizeof (tRsvpTeTnlInfo));
                return RPTE_SUCCESS;
            }
        }
        pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                           (tRBElem *) pTmpRsvpTeTnlInfo, NULL);
    }
    return RPTE_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*
 *  Function Name   : RpteUpdateOne2OneTunnelDelFtnEntry
 *  Description     : This function updates the FtnEntry in hardware 
 *                    in one to one tunnel scenario.
 *  Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 *  Output (s)      : None
 *  Returns         : RPTE_SUCCESS
 *                    RPTE_FAILURE
 *  */
/*---------------------------------------------------------------------------*/
UINT1
RpteUpdateOne2OneTunnelDelFtnEntry (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTMO_DLL_NODE      *pFtnNodeInfo = NULL;
    tFtnEntry          *pFtnEntry = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    UINT4               u4SrcAddr = RPTE_ZERO;
    UINT4               u4DestAddr = RPTE_ZERO;
    UINT4               u4TunnelInstance0 = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                "RpteUpdateOne2OneTunnelDelFtnEntry : ENTRY \n");

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pRsvpTeTnlInfo->pTeTnlInfo)),
                        u4SrcAddr);
    u4SrcAddr = OSIX_NTOHL (u4SrcAddr);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pRsvpTeTnlInfo->pTeTnlInfo)),
                        u4DestAddr);
    u4DestAddr = OSIX_NTOHL (u4DestAddr);

    /* Get the tunnel with instance zero */
    if (!(pInstance0Tnl = TeGetTunnelInfo (RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                                           u4TunnelInstance0,
                                           u4SrcAddr, u4DestAddr)))
    {
        RSVPTE_DBG (RSVPTE_UTL_PRCS, "Fail to get Tunnel 0 instance \n");
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUpdateOne2OneTunnelDelFtnEntry "
                    ": INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* Scan FtnList to delete FtnEntry */
    TMO_DLL_Scan (&TE_TNL_FTN_LIST (pInstance0Tnl),
                  pFtnNodeInfo, tTMO_DLL_NODE *)
    {
        if (!(pFtnEntry = (tFtnEntry *) (((FS_ULONG) pFtnNodeInfo
                                          -
                                          (TE_OFFSET
                                           (tFtnEntry, FtnInfoNode))))))
        {
            RSVPTE_DBG (RSVPTE_UTL_PRCS,
                        "pFtnEntry is NULL in Ftn deletion \n");

            RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUpdateOne2OneTunnelDelFtnEntry "
                        ": INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        /* Delete Ftn entry in Hardware */
        if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
        {
            RSVPTE_DBG1 (RSVPTE_UTL_PRCS,
                         " RpteUpdateOne2OneTunnelDelFtnEntry : "
                         "FTN %x Deletion Failed \n",
                         OSIX_NTOHL (*(UINT4 *)
                                     &(pFtnEntry->DestAddrMin.u4_addr)));
            return RPTE_FAILURE;
        }
        RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUpdateOne2OneTunnelDelFtnEntry: "
                    "FTN deletion successfully done \n");
    }

    RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                "RpteUpdateOne2OneTunnelDelFtnEntry : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 *  Function Name   : RpteUpdateOne2OneTunnelAddFtnEntry
 *  Description     : This function updates the FtnEntry in hardware 
 *                    in one to one tunnel scenario.
 *  Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 *  Output (s)      : None
 *  Returns         : RPTE_SUCCESS
 *                    RPTE_FAILURE
 *  */
/*---------------------------------------------------------------------------*/
UINT1
RpteUpdateOne2OneTunnelAddFtnEntry (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTMO_DLL_NODE      *pFtnNodeInfo = NULL;
    tFtnEntry          *pFtnEntry = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    UINT4               u4SrcAddr = RPTE_ZERO;
    UINT4               u4DestAddr = RPTE_ZERO;
    UINT4               u4TunnelInstance0 = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                "RpteUpdateOne2OneTunnelAddFtnEntry : ENTRY \n");

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pRsvpTeTnlInfo->pTeTnlInfo)),
                        u4SrcAddr);
    u4SrcAddr = OSIX_NTOHL (u4SrcAddr);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pRsvpTeTnlInfo->pTeTnlInfo)),
                        u4DestAddr);
    u4DestAddr = OSIX_NTOHL (u4DestAddr);

    /* Get the tunnel with instance zero */
    if (!(pInstance0Tnl = TeGetTunnelInfo (RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                                           u4TunnelInstance0,
                                           u4SrcAddr, u4DestAddr)))
    {
        RSVPTE_DBG (RSVPTE_UTL_PRCS, "Fail to get Tunnel 0 instance \n");
        RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUpdateOne2OneTunnelAddFtnEntry "
                    ": INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG1 (RSVPTE_UTL_PRCS,
                 "RpteUpdateOne2OneTunnelAddFtnEntry: TnlPrimaryInstance = %d \n",
                 pInstance0Tnl->u4TnlPrimaryInstance);

    /* Scan FtnList to add FtnEntry */
    TMO_DLL_Scan (&TE_TNL_FTN_LIST (pInstance0Tnl),
                  pFtnNodeInfo, tTMO_DLL_NODE *)
    {
        if (!(pFtnEntry = (tFtnEntry *) (((FS_ULONG) pFtnNodeInfo
                                          -
                                          (TE_OFFSET
                                           (tFtnEntry, FtnInfoNode))))))
        {
            RSVPTE_DBG (RSVPTE_UTL_PRCS,
                        "pFtnEntry is NULL in Ftn Addition \n");

            RSVPTE_DBG (RSVPTE_UTL_ETEXT, "RpteUpdateOne2OneTunnelAddFtnEntry "
                        ": INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        /* Add Ftn entry in Hardware */
        if (MplsFTNAdd (pFtnEntry) == MPLS_FAILURE)
        {
            RSVPTE_DBG1 (RSVPTE_UTL_PRCS,
                         " RpteUpdateOne2OneTunnelAddFtnEntry : "
                         "FTN %x Addition Failed \n",
                         OSIX_NTOHL (*(UINT4 *)
                                     &(pFtnEntry->DestAddrMin.u4_addr)));
            return RPTE_FAILURE;
        }
        RSVPTE_DBG (RSVPTE_UTL_PRCS, "RpteUpdateOne2OneTunnelAddFtnEntry: "
                    "FTN Addition successfully done \n");
    }

    RSVPTE_DBG (RSVPTE_UTL_ETEXT,
                "RpteUpdateOne2OneTunnelAddFtnEntry : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteUtlStopTimer
 * Description     : Function to stop the timer. If the LSP_FAILURE event is
 *                   recieved, the timer is stopped before starting.
 * Input (s)       : Same as the input parameters for TmrStartTimer.
 * Output (s)      : None
 * Returns         : Value returned by TmrStartTimer
 */
/****************************************************************************/
UINT4
RpteUtlStopTimer (tTimerListId TimerListId, tTmrAppTimer * pReference)
{
    /* We are adding this as per function header description */
    TmrStopTimer (TimerListId, pReference);
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rpteutil.c                             */
/*---------------------------------------------------------------------------*/
