/********************************************************************
 *   Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *   
 *   $Id: rptepb.c,v 1.22 2014/12/12 11:56:45 siva Exp $
 *   
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptepb.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : RSVP-TE 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines to build the Rsvp 
 *                             pkt and calls appropriate routine to send it as
 *                             UDP encapsulated/Raw IP encaptulated pkt.
 *----------------------------------------------------------------------------*/

#include "rpteincs.h"

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePbSendMsg                                          
 * Description     : This function builds the Rsvp pkt to be send on the    
 *                   net and calls appropriate routine to send it           
 * Input (s)       : pPktMap - Pointer to PktMap                            
 * Output (s)      : None                                                   
 * Returns         : None                                                   
 */
/*---------------------------------------------------------------------------*/
VOID
RptePbSendMsg (tPktMap * pPktMap)
{
    tRsvpHdr           *pRsvpHdr = NULL;
    tMsgBuf            *pMsgBuf = NULL;
    tIfEntry           *pIfEntry = NULL;
    UINT2               u2PktSize = 0;
    UINT4               u4MsgSize = 0;
    UINT4               u4WriteOffSet = 0;
    UINT1               u1UdpSet = RPTE_NO;
    UINT1               u1IpHdrLen = RPTE_ZERO;
    UINT1               u1Flags = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PB_ETEXT, "RptePbSendMsg : ENTRY \n");

    if (MsrGetRestorationStatus () == ISS_TRUE)
    {
        RpteUtlCleanPktMap (pPktMap);
        return;
    }

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);

    if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) == NOTIFY_MSG) ||
        ((RSVP_HDR_MSG_TYPE (pRsvpHdr) == ACK_MSG) &&
         (PKT_MAP_IF_ENTRY (pPktMap) == NULL)))

    {
        RptePbSendNotifyOrAck (pPktMap);
        return;
    }

    pIfEntry = PKT_MAP_OUT_IF_ENTRY (pPktMap);
    if ((pIfEntry == NULL) || (IF_ENTRY_STATUS (pIfEntry) != ACTIVE) ||
        (IF_ENTRY_ENABLED (pIfEntry) != RPTE_ENABLED))
    {
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PB_ETEXT, "RptePbSendMsg : INTMD-EXIT \n");
        return;
    }
    /* RSVP Header Flag update */
    if (gu1RRCapable == RPTE_TRUE)
    {
        RSVP_HDR_VER_FLAG (pRsvpHdr) |= RSVP_HDR_FLAG_WITH_RR;
    }
    /* Update Check Sum */
    RSVP_HDR_CHECK_SUM (pRsvpHdr) = RSVPTE_ZERO;
    u2PktSize = OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr));

    RSVP_HDR_CHECK_SUM (pRsvpHdr) =
        (UINT2) RpteUtlCheckSum ((UINT1 *) pRsvpHdr, u2PktSize);
    u4MsgSize = u2PktSize;

    /* Calculate size */
    if (RpteUtlUdpEncapsRequired (pIfEntry) == RPTE_YES)
    {
        u4MsgSize += sizeof (tUdpHdr);
        u1UdpSet = RPTE_YES;
    }

    if (((RSVP_HDR_MSG_TYPE (pRsvpHdr) == HELLO_MSG) &&
         (pIfEntry->u4Addr == RPTE_ZERO)) && (u1UdpSet == RPTE_YES))
    {
        u4MsgSize -= sizeof (tUdpHdr);
        u1UdpSet = RPTE_NO;
    }

    if ((RPTE_PKT_MAP_TNL_INFO (pPktMap) != NULL) &&
        ((PKT_MAP_SEND_TO_BYPASS_TUNNEL (pPktMap) == RPTE_TRUE) ||
         (PKT_MAP_IS_BKP_PATH_MSG_TO_CFA (pPktMap) == RPTE_TRUE)))
    {
        if (PKT_MAP_RA_OPT_NEEDED (pPktMap) == RPTE_YES)
        {
            u1IpHdrLen = IP_HDR_LEN + RSVPTE_IP_RTR_ALERT_LEN;
        }
    }
    if (PKT_MAP_BUILD_PKT (pPktMap) == RPTE_YES)
    {
        /* Allocate Message Buffer */
        pMsgBuf = BUF_ALLOC ((u4MsgSize + u1IpHdrLen +
                              (sizeof (tRsvpIpHdr) + MAC_HEADER_LENGTH
                               + SHIM_HDR_LEN)), RPTE_ZERO);

        if (pMsgBuf == NULL)
        {
            RSVPTE_DBG (RSVPTE_PB_PRCS, "PB : Mem alloc failed\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PB_ETEXT, "RptePbSendMsg : INTMD-EXIT \n");
            return;
        }

        PKT_MAP_MSG_BUF (pPktMap) = pMsgBuf;
        /* Copy Rsvp Pkt */
        if (BUF_COPY_OVER_CHAIN (pMsgBuf, PKT_MAP_RSVP_PKT (pPktMap),
                                 u4WriteOffSet, u2PktSize) != CRU_SUCCESS)
        {
            return;
        }
    }
    else
    {
        pMsgBuf = PKT_MAP_MSG_BUF (pPktMap);
        if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) != PATHTEAR_MSG) ||
            (RSVP_HDR_MSG_TYPE (pRsvpHdr) != RESVCONF_MSG))
        {
            CRU_BUF_Move_ValidOffset (pMsgBuf, (u4MsgSize - u2PktSize));
        }
    }
    MEMCPY (&BUF_GET_INTERFACE_ID (PKT_MAP_MSG_BUF (pPktMap)),
            &IF_ENTRY_IF_ID (pIfEntry), sizeof (tCRU_INTERFACE));

    if (RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATHERR_MSG)
    {

        /* As per RFC 3473, section 4.4
         * If the node setting the Path State Removed flag is not the 
         * session end point, the node should generate the corresponding
         * Path tear message 
         * This case will not come for Ingress node*/
        if (pPktMap->pErrorSpecObj != NULL)
        {
            u1Flags = pPktMap->pErrorSpecObj->ErrorSpec.u1Flags;
        }
        else if (pPktMap->pIfIdRsvpNumErrObj != NULL)
        {
            u1Flags = pPktMap->pIfIdRsvpNumErrObj->ErrorSpec.u1Flags;
        }
        else if (pPktMap->pIfIdRsvpErrObj != NULL)
        {
            u1Flags = pPktMap->pIfIdRsvpErrObj->ErrorSpec.u1Flags;
        }
        if ((u1Flags == RPTE_PATH_STATE_REMOVED) &&
            (pPktMap->pRsvpTeTnlInfo != NULL))
        {
            if (pPktMap->pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole != RPTE_EGRESS)
            {
                RptePhGeneratePathTear (pPktMap->pRsvpTeTnlInfo);
            }
            RPTE_TNL_DEL_TNL_FLAG (pPktMap->pRsvpTeTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pPktMap->pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
        }
    }
    /* If pkt is sent as a UDP encaptulated pkt, send pkt to UDP */
    if (u1UdpSet == RPTE_YES)
    {
        if (RpteSendMsgToUdp (PKT_MAP_MSG_BUF (pPktMap), pPktMap) ==
            RPTE_FAILURE)
        {
            /* 
             * Send packet to UDP iface failed, release the buffer log err 
             * message. 
             */
            RSVPTE_DBG (RSVPTE_PB_PRCS,
                        "PB : Failed to send RSVP pkt to UDP\n");
            PKT_MAP_MSG_BUF (pPktMap) = NULL;
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PB_ETEXT, "RptePbSendMsg : INTMD-EXIT \n");
            return;
        }
        RSVPTE_DBG (RSVPTE_PB_PRCS, "PB : Send RSVP pkt to UDP - Success\n");
    }
    else
    {
        if (RPTE_IPIF_SEND_PKT (PKT_MAP_MSG_BUF (pPktMap), pPktMap) ==
            RPTE_FAILURE)
        {
            /* 
             * Send packet to IP iface failed, release the buffer log err 
             * message. 
             */
            RSVPTE_DBG (RSVPTE_PB_PRCS, "PB : Failed to send RSVP pkt to IP\n");
            PKT_MAP_MSG_BUF (pPktMap) = NULL;
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PB_ETEXT, "RptePbSendMsg : INTMD-EXIT \n");
            return;
        }
        RSVPTE_DBG (RSVPTE_PB_PRCS, "PB : Send RSVP pkt to IP - Success\n");
    }
    /*
     * Set Msgbuf pointer in packet map to  null so that clean pkt map
     * won't release the allocated memory for the packet as the packet will
     * be used by the task to which we enqueued
     */
    PKT_MAP_MSG_BUF (pPktMap) = NULL;

    /* Dump the rsvp message */
    if (RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) != RSVPTE_DUMP_DIR_NONE)
    {
        RpteDumpOutRpteMsg (pPktMap);
    }

    RPTE_IF_STAT_ENTRY_NUM_MSG_SENT (pIfEntry)++;

    switch (RSVP_HDR_MSG_TYPE (pRsvpHdr))
    {
        case HELLO_MSG:
            RPTE_IF_STAT_ENTRY_NUM_HELLO_SENT (pIfEntry)++;
            break;

        case PATHERR_MSG:
            RPTE_IF_STAT_ENTRY_PATH_ERR_SENT (pIfEntry)++;
            break;

        case PATHTEAR_MSG:
            RPTE_IF_STAT_ENTRY_PATH_TEAR_SENT (pIfEntry)++;
            break;

        case RESVERR_MSG:
            RPTE_IF_STAT_ENTRY_RESV_ERR_SENT (pIfEntry)++;
            break;

        case RESVTEAR_MSG:
            RPTE_IF_STAT_ENTRY_RESV_TEAR_SENT (pIfEntry)++;
            break;

        case RESVCONF_MSG:
            RPTE_IF_STAT_ENTRY_RESV_CONF_SENT (pIfEntry)++;
            break;

        case BUNDLE_MSG:
            RPTE_IF_STAT_ENTRY_BUNDLE_MSG_SENT (pIfEntry)++;
            /* Only SREFRESH message is bundled with BUNDLE message.
             * Hence, SREFRESH message sent counter is incremented
             * after sending the BUNDLE message out */
            pRsvpHdr =
                (tRsvpHdr *) (VOID *) ((UINT1 *) pRsvpHdr + sizeof (tRsvpHdr));
            if (RSVP_HDR_MSG_TYPE (pRsvpHdr) == SREFRESH_MSG)
            {
                RPTE_IF_STAT_ENTRY_SREFRESH_MSG_SENT (pIfEntry)++;
            }
            break;

        case SREFRESH_MSG:
            RPTE_IF_STAT_ENTRY_SREFRESH_MSG_SENT (pIfEntry)++;
            break;
        case PATH_MSG:
            RPTE_IF_STAT_ENTRY_PATH_SENT (pIfEntry)++;
            break;
        case RESV_MSG:
            RPTE_IF_STAT_ENTRY_RESV_SENT (pIfEntry)++;
            break;
        case NOTIFY_MSG:
            (IF_ENTRY_STATS_INFO (pIfEntry)).u4IfNumNotifyMsgSent++;
            break;
        case RECOVERY_PATH_MSG:
            (IF_ENTRY_STATS_INFO (pIfEntry)).u4IfNumRecoveryPathSent++;
            break;
	default :
	    break;

    }

    RpteUtlCleanPktMap (pPktMap);
    RSVPTE_DBG (RSVPTE_PB_ETEXT, "RptePbSendMsg : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePbSendNotifyOrAck
 * Description     : This function builds the Notify or Ack pkt to be send on the
 *                   net and calls appropriate routine to send it
 * Input (s)       : pPktMap - Pointer to PktMap
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RptePbSendNotifyOrAck (tPktMap * pPktMap)
{
    tRsvpHdr           *pRsvpHdr = NULL;
    tMsgBuf            *pMsgBuf = NULL;
    UINT2               u2PktSize = 0;
    UINT4               u4MsgSize = 0;
    UINT4               u4WriteOffSet = 0;
    UINT1               u1IpHdrLen = RPTE_ZERO;

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);

    /* Update Check Sum */
    RSVP_HDR_CHECK_SUM (pRsvpHdr) = RSVPTE_ZERO;
    u2PktSize = OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr));

    RSVP_HDR_CHECK_SUM (pRsvpHdr) =
        (UINT2) RpteUtlCheckSum ((UINT1 *) pRsvpHdr, u2PktSize);
    u4MsgSize = u2PktSize;
    if (PKT_MAP_BUILD_PKT (pPktMap) == RPTE_YES)
    {
        /* Allocate Message Buffer */
        pMsgBuf = BUF_ALLOC ((u4MsgSize + u1IpHdrLen +
                              (sizeof (tRsvpIpHdr) + MAC_HEADER_LENGTH
                               + SHIM_HDR_LEN)), RPTE_ZERO);

        if (pMsgBuf == NULL)
        {
            RSVPTE_DBG (RSVPTE_PB_PRCS, "PB : Mem alloc failed\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PB_ETEXT, "RptePbSendMsg : INTMD-EXIT \n");
            return;
        }

        PKT_MAP_MSG_BUF (pPktMap) = pMsgBuf;
        /* Copy Rsvp Pkt */
        if (BUF_COPY_OVER_CHAIN (pMsgBuf, PKT_MAP_RSVP_PKT (pPktMap),
                                 u4WriteOffSet, u2PktSize) != CRU_SUCCESS)
        {
            return;
        }
    }
    else
    {
        pMsgBuf = PKT_MAP_MSG_BUF (pPktMap);
        if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) != PATHTEAR_MSG) ||
            (RSVP_HDR_MSG_TYPE (pRsvpHdr) != RESVCONF_MSG))
        {
            CRU_BUF_Move_ValidOffset (pMsgBuf, (u4MsgSize - u2PktSize));
        }
    }
    if (RPTE_IPIF_SEND_PKT (PKT_MAP_MSG_BUF (pPktMap), pPktMap) == RPTE_FAILURE)
    {
        /*
         * Send packet to IP iface failed, release the buffer log err
         * message.
         */
        RSVPTE_DBG (RSVPTE_PB_PRCS, "PB : Failed to send RSVP pkt to IP\n");
        PKT_MAP_MSG_BUF (pPktMap) = NULL;
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PB_ETEXT, "RptePbSendMsg : INTMD-EXIT \n");
        return;
    }
    RSVPTE_DBG (RSVPTE_PB_PRCS, "PB : Send RSVP pkt to IP - Success\n");
    PKT_MAP_MSG_BUF (pPktMap) = NULL;
    RpteUtlCleanPktMap (pPktMap);
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptepb.c                               */
/*---------------------------------------------------------------------------*/
