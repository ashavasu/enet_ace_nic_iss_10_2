
/* $Id: rpteget2.c,v 1.8 2013/12/13 11:26:01 siva Exp $*/
/********************************************************************
 *
 * $RCSfile: rpteget2.c,v $
 *
 * $Date: 2013/12/13 11:26:01 $
 *
 * $Revision: 1.8 $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rpteget2.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  : 
 *    DESCRIPTION            : This file contains the low level GET routines
 *                             derived from FutureRSVP.
 *----------------------------------------------------------------------------*/

/* RSVP TE support related header file. */
#include "rpteincs.h"
#include "fsmpfrlw.h"

/****************************************************************************
 Function    :  nmhGetFirstIndexRsvpIfTable
 Input       :  The Indices
                ifIndex 
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexRsvpIfTable (INT4 *pi4_ifIndex)
{
    UINT1               u1IfEntryFound = RPTE_FALSE;
    UINT4               u4HashIndex;
    tIfEntry           *pIfEntry = NULL;

    /* Unless the Admin Status is made up, there will not
     * be any entries present in the Table, Hence Failure
     * is returned
     */
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        return SNMP_FAILURE;
    }

    TMO_HASH_Scan_Table (RSVP_GBL_IF_HSH_TBL, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (RSVP_GBL_IF_HSH_TBL, u4HashIndex, pIfEntry,
                              tIfEntry *)
        {
            if (u1IfEntryFound == RPTE_FALSE)
            {
                *pi4_ifIndex = (INT4) IF_ENTRY_IF_INDEX (pIfEntry);
                u1IfEntryFound = RPTE_TRUE;
            }
            else if (IF_ENTRY_IF_INDEX (pIfEntry) < (UINT4) *pi4_ifIndex)
            {
                *pi4_ifIndex = (INT4) IF_ENTRY_IF_INDEX (pIfEntry);
            }
        }
    }

    if (u1IfEntryFound == RPTE_FALSE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexRsvpIfTable
 Input       :  The Indices
                ifIndex 
                next_ifIndex 
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexRsvpIfTable (INT4 i4_ifIndex, INT4 *pi4_next_ifIndex)
{
    UINT1               u1IfEntryFound = RPTE_FALSE;
    UINT4               u4HashIndex;
    tIfEntry           *pIfEntry = NULL;

    /* Unless the Admin Status is made up, there will not 
     * be any entries present in the Table, Hence Failure 
     * is returned
     */
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        return SNMP_FAILURE;
    }

    TMO_HASH_Scan_Table (RSVP_GBL_IF_HSH_TBL, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (RSVP_GBL_IF_HSH_TBL, u4HashIndex, pIfEntry,
                              tIfEntry *)
        {
            if (IF_ENTRY_IF_INDEX (pIfEntry) > (UINT4) i4_ifIndex)
            {
                if (u1IfEntryFound == RPTE_FALSE)
                {
                    *pi4_next_ifIndex = (INT4) IF_ENTRY_IF_INDEX (pIfEntry);
                    u1IfEntryFound = RPTE_TRUE;
                    continue;
                }
                else if (IF_ENTRY_IF_INDEX (pIfEntry) <
                         (UINT4) *pi4_next_ifIndex)
                {
                    *pi4_next_ifIndex = (INT4) IF_ENTRY_IF_INDEX (pIfEntry);
                    continue;
                }
            }
        }
    }

    if (u1IfEntryFound == RPTE_FALSE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsBypassTunnelIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsBypassTunnelIfTable
 Input       :  The Indices
                FsMplsBypassTunnelIfIndex
                FsMplsBypassTunnelIndex
                FsMplsBypassTunnelInstance
                FsMplsBypassTunnelIngressLSRId
                FsMplsBypassTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsBypassTunnelIfTable (INT4
                                                   i4FsMplsBypassTunnelIfIndex,
                                                   UINT4
                                                   u4FsMplsBypassTunnelIndex,
                                                   UINT4
                                                   u4FsMplsBypassTunnelIngressLSRId,
                                                   UINT4
                                                   u4FsMplsBypassTunnelEgressLSRId)
{
    tIfEntry           *pIfEntry = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;

    /* To Validate this Table, Interface Index should exist priorly */
    if (RpteGetIfEntry (i4FsMplsBypassTunnelIfIndex, &pIfEntry) == RPTE_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* To Validate this Table, Tunnel should exist priorly */
    if (RpteGetTnlInTnlTable (gpRsvpTeGblInfo,
                              u4FsMplsBypassTunnelIndex,
                              RPTE_ZERO,
                              u4FsMplsBypassTunnelIngressLSRId,
                              u4FsMplsBypassTunnelEgressLSRId,
                              &pRsvpTeTnlInfo) == RPTE_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsBypassTunnelIfTable
 Input       :  The Indices
                FsMplsBypassTunnelIfIndex
                FsMplsBypassTunnelIndex
                FsMplsBypassTunnelInstance
                FsMplsBypassTunnelIngressLSRId
                FsMplsBypassTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsBypassTunnelIfTable (INT4 *pi4FsMplsBypassTunnelIfIndex,
                                           UINT4 *pu4FsMplsBypassTunnelIndex,
                                           UINT4
                                           *pu4FsMplsBypassTunnelIngressLSRId,
                                           UINT4
                                           *pu4FsMplsBypassTunnelEgressLSRId)
{
    tIfEntry           *pIfEntry = NULL;
    tTMO_DLL           *pIfFacTnlsList = NULL;
    tTMO_DLL_NODE      *pIfFacTnlsNode = NULL;
    tFacilityRpteBkpTnl *pTmpFacilityRpteBkpTnl = NULL;

    /* To Fetch the First Index of this Table, Interface should present */
    if (nmhGetFirstIndexRsvpIfTable (pi4FsMplsBypassTunnelIfIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    do
    {
        /* To Fetch the First Index of this Table, 
         * pIfEntry should exist for the Interface */
        if (RpteGetIfEntry (*pi4FsMplsBypassTunnelIfIndex, &pIfEntry)
            == RPTE_FAILURE)
        {
            continue;
        }

        /* Getting the Facility Tunnel List in the IfEntry */
        pIfFacTnlsList = (tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfEntry));

        /* Getting the First Tunnel Node in Facility Tunnel List */
        pIfFacTnlsNode = (tTMO_DLL_NODE *) TMO_DLL_First (pIfFacTnlsList);
        if (pIfFacTnlsNode == NULL)
        {
            continue;
        }

        pTmpFacilityRpteBkpTnl = (tFacilityRpteBkpTnl *) (pIfFacTnlsNode);

        /* Returning the Indices */

        *pu4FsMplsBypassTunnelIndex =
            RPTE_FRR_FACILITY_BKP_TNL_INDEX (pTmpFacilityRpteBkpTnl);
        *pu4FsMplsBypassTunnelIngressLSRId =
            RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID (pTmpFacilityRpteBkpTnl);
        *pu4FsMplsBypassTunnelEgressLSRId =
            RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID (pTmpFacilityRpteBkpTnl);

        return SNMP_SUCCESS;
    }
    while (nmhGetNextIndexRsvpIfTable (*pi4FsMplsBypassTunnelIfIndex,
                                       pi4FsMplsBypassTunnelIfIndex)
           == SNMP_SUCCESS);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsBypassTunnelIfTable
 Input       :  The Indices
                FsMplsBypassTunnelIfIndex
                nextFsMplsBypassTunnelIfIndex
                FsMplsBypassTunnelIndex
                nextFsMplsBypassTunnelIndex
                FsMplsBypassTunnelInstance
                nextFsMplsBypassTunnelInstance
                FsMplsBypassTunnelIngressLSRId
                nextFsMplsBypassTunnelIngressLSRId
                FsMplsBypassTunnelEgressLSRId
                nextFsMplsBypassTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsBypassTunnelIfTable (INT4 i4FsMplsBypassTunnelIfIndex,
                                          INT4
                                          *pi4NextFsMplsBypassTunnelIfIndex,
                                          UINT4 u4FsMplsBypassTunnelIndex,
                                          UINT4 *pu4NextFsMplsBypassTunnelIndex,
                                          UINT4
                                          u4FsMplsBypassTunnelIngressLSRId,
                                          UINT4
                                          *pu4NextFsMplsBypassTunnelIngressLSRId,
                                          UINT4 u4FsMplsBypassTunnelEgressLSRId,
                                          UINT4
                                          *pu4NextFsMplsBypassTunnelEgressLSRId)
{
    tIfEntry           *pIfEntry = NULL;
    tTMO_DLL           *pIfFacTnlsList = NULL;
    tTMO_DLL_NODE      *pIfFacTnlsNode = NULL;
    tFacilityRpteBkpTnl *pTmpFacilityRpteBkpTnl = NULL;
    BOOL1               bIsFound = RPTE_FALSE;

    /* To Get Next Index in this table, pIfEntry for 
     * previous should present */
    if (RpteGetIfEntry (i4FsMplsBypassTunnelIfIndex, &pIfEntry) == RPTE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Getting Facility Tunnel List in the IfEntry 
     * Scanning the Facility Tunnel Node in Facility Tunnel List to
     * identify whether Previous Tunnel Indices exist.*/
    pIfFacTnlsList = (tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfEntry));
    TMO_DLL_Scan (pIfFacTnlsList, pIfFacTnlsNode, tTMO_DLL_NODE *)
    {
        pTmpFacilityRpteBkpTnl = (tFacilityRpteBkpTnl *) (pIfFacTnlsNode);

        if ((RPTE_FRR_PROT_IF_INDEX (pTmpFacilityRpteBkpTnl)
             == (UINT4) i4FsMplsBypassTunnelIfIndex)
            && (RPTE_FRR_FACILITY_BKP_TNL_INDEX (pTmpFacilityRpteBkpTnl)
                == u4FsMplsBypassTunnelIndex)
            &&
            (RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID (pTmpFacilityRpteBkpTnl)
             == u4FsMplsBypassTunnelIngressLSRId)
            && (RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID (pTmpFacilityRpteBkpTnl)
                == u4FsMplsBypassTunnelEgressLSRId))
        {
            bIsFound = RPTE_TRUE;
            break;
        }
    }

    if (bIsFound == RPTE_FALSE)
    {
        return SNMP_FAILURE;
    }

    /* If the Previous Tunnel Indices exist, 
     * Checking whether the Next Pointer to this tunnel exist,
     * If so, just Getting the Pointer. */
    if (TMO_DLL_Next (pIfFacTnlsList, pIfFacTnlsNode) != NULL)
    {
        pIfFacTnlsNode = TMO_DLL_Next (pIfFacTnlsList, pIfFacTnlsNode);

        pTmpFacilityRpteBkpTnl = (tFacilityRpteBkpTnl *) (pIfFacTnlsNode);

        if (pTmpFacilityRpteBkpTnl)
        {
            *pu4NextFsMplsBypassTunnelIndex =
                RPTE_FRR_FACILITY_BKP_TNL_INDEX (pTmpFacilityRpteBkpTnl);
            *pu4NextFsMplsBypassTunnelIngressLSRId =
                RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID
                (pTmpFacilityRpteBkpTnl);
            *pu4NextFsMplsBypassTunnelEgressLSRId =
                RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID
                (pTmpFacilityRpteBkpTnl);
            return SNMP_SUCCESS;
        }
    }

    *pi4NextFsMplsBypassTunnelIfIndex = i4FsMplsBypassTunnelIfIndex;
    /* If the Next Pointer is NULL, trying to another IfEntry,
     * and same process is repeated. */
    while (nmhGetNextIndexRsvpIfTable (*pi4NextFsMplsBypassTunnelIfIndex,
                                       pi4NextFsMplsBypassTunnelIfIndex)
           == RPTE_SUCCESS)
    {
        if (RpteGetIfEntry (*pi4NextFsMplsBypassTunnelIfIndex, &pIfEntry)
            == RPTE_FAILURE)
        {
            continue;
        }

        pIfFacTnlsList = (tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfEntry));

        pIfFacTnlsNode = (tTMO_DLL_NODE *) TMO_DLL_First (pIfFacTnlsList);
        if (pIfFacTnlsNode == NULL)
        {
            continue;
        }
        pTmpFacilityRpteBkpTnl = (tFacilityRpteBkpTnl *) (pIfFacTnlsNode);

        if (pTmpFacilityRpteBkpTnl->pRsvpTeTnlInfo == NULL)
        {
            continue;
        }

        *pu4NextFsMplsBypassTunnelIndex =
            RPTE_FRR_FACILITY_BKP_TNL_INDEX (pTmpFacilityRpteBkpTnl);
        *pu4NextFsMplsBypassTunnelIngressLSRId =
            RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID (pTmpFacilityRpteBkpTnl);
        *pu4NextFsMplsBypassTunnelEgressLSRId =
            RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID (pTmpFacilityRpteBkpTnl);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsBypassTunnelRowStatus
 Input       :  The Indices
                FsMplsBypassTunnelIfIndex
                FsMplsBypassTunnelIndex
                FsMplsBypassTunnelInstance
                FsMplsBypassTunnelIngressLSRId
                FsMplsBypassTunnelEgressLSRId

                The Object 
                retValFsMplsBypassTunnelRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsBypassTunnelRowStatus (INT4 i4FsMplsBypassTunnelIfIndex,
                                   UINT4 u4FsMplsBypassTunnelIndex,
                                   UINT4 u4FsMplsBypassTunnelIngressLSRId,
                                   UINT4 u4FsMplsBypassTunnelEgressLSRId,
                                   INT4 *pi4RetValFsMplsBypassTunnelRowStatus)
{
    tIfEntry           *pIfEntry = NULL;
    tTMO_DLL           *pIfFacTnlsList = NULL;
    tTMO_DLL_NODE      *pIfFacTnlsNode = NULL;
    tFacilityRpteBkpTnl *pTmpFacilityRpteBkpTnl = NULL;

    MPLS_RSVPTE_LOCK ();
    /* Verifying whether the passed interface exists */
    if (RpteGetIfEntry (i4FsMplsBypassTunnelIfIndex, &pIfEntry) == RPTE_FAILURE)
    {
        MPLS_RSVPTE_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Verifying whether the tunnel with the indices passed is added to the 
     * interface list or not. If added, then CREATE AND GO is passed as 
     * output. If Not Added, DESTROY is passed. */
    pIfFacTnlsList = (tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfEntry));
    TMO_DLL_Scan (pIfFacTnlsList, pIfFacTnlsNode, tTMO_DLL_NODE *)
    {
        pTmpFacilityRpteBkpTnl = (tFacilityRpteBkpTnl *) (pIfFacTnlsNode);

        if ((RPTE_FRR_PROT_IF_INDEX (pTmpFacilityRpteBkpTnl)
             == (UINT4) i4FsMplsBypassTunnelIfIndex)
            && (RPTE_FRR_FACILITY_BKP_TNL_INDEX (pTmpFacilityRpteBkpTnl)
                == u4FsMplsBypassTunnelIndex)
            &&
            (RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID (pTmpFacilityRpteBkpTnl)
             == u4FsMplsBypassTunnelIngressLSRId)
            && (RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID (pTmpFacilityRpteBkpTnl)
                == u4FsMplsBypassTunnelEgressLSRId))
        {
            *pi4RetValFsMplsBypassTunnelRowStatus = RPTE_ACTIVE;
            MPLS_RSVPTE_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    MPLS_RSVPTE_UNLOCK ();
    return SNMP_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rpteget2.c                             */
/*---------------------------------------------------------------------------*/
