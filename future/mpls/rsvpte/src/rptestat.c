/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptestat.c,v 1.6 2012/08/02 09:58:30 siva Exp $
 *
 ********************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptestat.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains utility fuctions to display
 *                             the set of Active RSVP-TE Tunnels.
 *                             
 *---------------------------------------------------------------------------*/

#include "rpteincs.h"

#define RSVPTE_LCB_DUMP 0x08000001
/* ---------------------------------------------------------------------------
 * Function Name : RptePrintRsvpTeTnls                                        
 * Description   : This routine prints the main identifier info of a RSVP-TE
 *                 Tunnels.
 * Input(s)      : u4TnlNum, pRsvpTeTnlInfo
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
RptePrintRsvpTeTnls (UINT4 u4TnlNum, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    u4TnlNum = (UINT4) u4TnlNum;
    pRsvpTeTnlInfo = (tRsvpTeTnlInfo *) pRsvpTeTnlInfo;
    RSVPTE_DBG (RSVPTE_STAT_ETEXT, " RptePrintRsvpTeTnls : ENTRY \n");
    /* Printing the LSP Number */
    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "RSVPTE:%4d |", u4TnlNum);
    /* Printing the Tunnel Id */
    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5d  |",
                 pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex);

    /* Printing the Tunnel Instance */
    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%6d  |",
                 pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);

    /* Printing the Ingress Id */
    RSVPTE_DBG4 (RSVPTE_LCB_DUMP, " %#02x%02x%02x%02x |",
                 pRsvpTeTnlInfo->pTeTnlInfo->TnlIngressLsrId[RSVPTE_THREE],
                 pRsvpTeTnlInfo->pTeTnlInfo->TnlIngressLsrId[RSVPTE_TWO],
                 pRsvpTeTnlInfo->pTeTnlInfo->TnlIngressLsrId[RSVPTE_ONE],
                 pRsvpTeTnlInfo->pTeTnlInfo->TnlIngressLsrId[RSVPTE_ZERO]);

    /* Printing the Egress Id */

    RSVPTE_DBG4 (RSVPTE_LCB_DUMP, " %#02x%02x%02x%02x |",
                 pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId[RSVPTE_THREE],
                 pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId[RSVPTE_TWO],
                 pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId[RSVPTE_ONE],
                 pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId[RSVPTE_ZERO]);

    if (RSVPTE_TNL_OPER_STATUS (RPTE_TE_TNL (pRsvpTeTnlInfo)) == RPTE_OPER_DOWN)
    {
        /* Printing the Tnl State */
        RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%s|", "Dn");
        RSVPTE_DBG (RSVPTE_LCB_DUMP, "   -   |");
        RSVPTE_DBG (RSVPTE_LCB_DUMP, "   -  \n");
    }
    else if (RSVPTE_TNL_OPER_STATUS (RPTE_TE_TNL (pRsvpTeTnlInfo))
             == RPTE_OPER_UP)
    {
        /* Printing the Tnl State */
        RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%s|", "Up");
        switch (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo))
        {
            case RPTE_INGRESS:

                if (RPTE_IF_LBL_TYPE
                    (PSB_OUT_IF_ENTRY
                     (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))) == RPTE_ETHERNET)
                {
                    /* Printing the InLabel */
                    RSVPTE_DBG (RSVPTE_LCB_DUMP, "   -   |");
                    /* Printing the OutLabel */
                    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5x\n",
                                 (int) (pRsvpTeTnlInfo->DownstrOutLbl.
                                        u4GenLbl));
                }
                else
                {
                    /* Printing the VCI */
                    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5x\n",
                                 (int) (pRsvpTeTnlInfo->DownstrOutLbl.AtmLbl.
                                        u2Vci));
                    /* Printing the VPI */
                    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5x\n",
                                 (int) (pRsvpTeTnlInfo->DownstrOutLbl.AtmLbl.
                                        u2Vci));
                }
                break;
            case RPTE_EGRESS:

                if (RPTE_IF_LBL_TYPE
                    (PSB_IF_ENTRY
                     (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))) == RPTE_ETHERNET)
                {
                    /* Printing the InLabel */
                    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5x  |",
                                 (int) (pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl));
                    /* Printing the OutLabel */
                    RSVPTE_DBG (RSVPTE_LCB_DUMP, "   -  \n");
                }
                else
                {
                    /* Printing the VCI */
                    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5x\n",
                                 (int) (pRsvpTeTnlInfo->DownstrOutLbl.AtmLbl.
                                        u2Vci));
                    /* Printing the VPI */
                    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5x\n",
                                 (int) (pRsvpTeTnlInfo->DownstrOutLbl.AtmLbl.
                                        u2Vci));
                }
                break;
            default:

                if (RPTE_IF_LBL_TYPE
                    (PSB_IF_ENTRY
                     (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))) == RPTE_ETHERNET)
                {
                    /* Printing the InLabel */
                    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5x  |",
                                 (int) (pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl));
                    /* Printing the OutLabel */
                    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5x\n",
                                 (int) (pRsvpTeTnlInfo->DownstrOutLbl.
                                        u4GenLbl));
                }
                else
                {
                    /* Printing the VCI */
                    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5x\n",
                                 (int) (pRsvpTeTnlInfo->DownstrOutLbl.AtmLbl.
                                        u2Vci));
                    /* Printing the VPI */
                    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "%5x\n",
                                 (int) (pRsvpTeTnlInfo->DownstrOutLbl.AtmLbl.
                                        u2Vci));
                }

        }
    }
    RSVPTE_DBG (RSVPTE_STAT_ETEXT, " RptePrintRsvpTeTnls : EXIT \n");
}

/* ---------------------------------------------------------------------------
 * Function Name : RpteDumpAllRsvpTeTnls                                       
 * Description   : This routine prints the info on all active RSVP-TE Tnls 
 *                 when requested at that given time. 
 * Input(s)      : None 
 * Output(s)     : pNumLsps
 * Return(s)     : RPTE_SUCCESS/RPTE_FAILURE
 -------------------------------------------------------------------------- */
UINT1
RpteDumpAllRsvpTeTnls (INT4 *pNumLsps)
{
    UINT4               u4NumActiveLsp = 0;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    *pNumLsps = (INT4) u4NumActiveLsp;
    RSVPTE_DBG (RSVPTE_STAT_ETEXT, " RpteDumpAllRsvpTeTnls : ENTRY \n");

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        RSVPTE_DBG (RSVPTE_LCB_DUMP, "RSVP-TE Admin Down\n");
        RSVPTE_DBG (RSVPTE_STAT_ETEXT,
                    " RpteDumpAllRsvpTeTnls : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    pRsvpTeTnlInfo = RBTreeGetFirst (gpRsvpTeGblInfo->RpteTnlTree);

    RSVPTE_DBG (RSVPTE_LCB_DUMP,
                "-----------------------------------------"
                "------------------------------\n");
    RSVPTE_DBG (RSVPTE_LCB_DUMP,
                "LSP | TnlId | TnlIns |   IngrId  |   EgrId   | S | InLbl "
                "| OutLbl\n");
    RSVPTE_DBG (RSVPTE_LCB_DUMP,
                "-----------------------------------------"
                "------------------------------\n");

    while (pRsvpTeTnlInfo != NULL)
    {
        if ((RPTE_TE_TNL (pRsvpTeTnlInfo) != NULL) &&
            (RSVPTE_TNL_OPER_STATUS (RPTE_TE_TNL (pRsvpTeTnlInfo))
             == TE_OPER_UP))
        {
            u4NumActiveLsp = u4NumActiveLsp + RPTE_ONE;
            RptePrintRsvpTeTnls (u4NumActiveLsp, pRsvpTeTnlInfo);
        }
        pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                        (tRBElem *) pRsvpTeTnlInfo,
                                        NULL);
    }                    
    RSVPTE_DBG (RSVPTE_LCB_DUMP,
                "-----------------------------------------"
                "------------------------------\n");
    RSVPTE_DBG1 (RSVPTE_LCB_DUMP, "RSVP-TE Tnls currently at this node = %d\n",
                 u4NumActiveLsp);
    RSVPTE_DBG (RSVPTE_LCB_DUMP,
                "-----------------------------------------"
                "------------------------------\n");
    *pNumLsps = (INT4) u4NumActiveLsp;
    RSVPTE_DBG (RSVPTE_STAT_ETEXT, " RpteDumpAllRsvpTeTnls : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptestat.c                             */
/*---------------------------------------------------------------------------*/
