
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: rpteperr.c,v 1.29 2017/08/04 13:15:32 siva Exp $
 ********************************************************************
*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rpteperr.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : RSVP 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for 
 *                             Path Err module
 *----------------------------------------------------------------------------*/

#include "rpteincs.h"

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePehProcessPathErrMsg
 * Description     : This function processes the PathErr message with
 *                   u1ErrCode and u2ErrValue
 * Input (s)       : *pPktMap - Pointer to Packet Map
 * Output (s)      : None 
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePehProcessPathErrMsg (tPktMap * pPktMap)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTmpTeTnlInfo = NULL;
    tIfEntry           *pIfEntry = NULL;
    tErrorSpec         *pErrorSpec = NULL;
    UINT2               u2Tmp = RPTE_ZERO;
    UINT1               u1DetourGrpId = RPTE_ZERO;

    /* 
     * Check for Tnl info list, if Tnl present 
     * Check for Ingress, if ingress, process Path Err, if required send 
     * Path Tear msg, else send path err with  PHOP updated 
     */
    RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsg : ENTRY \n");

    if (RPTE_CHECK_PATH_ERR_MSG_TNL (pPktMap, pRsvpTeTnlInfo) == RPTE_FAILURE)
    {
        RSVPTE_DBG1 (RSVPTE_PE_PRCS,
                     "PATH ERR : No entry present in Tnl Table "
                     "with Tnl Id : %d\n",
                     OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID (pPktMap)));
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsg : EXIT \n");
        return;
    }
    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_WORKING_PATH))
    {
        if (((pPktMap->pIfIdRsvpErrObj != NULL) &&
             (pPktMap->pIfIdRsvpErrObj->ErrorSpec.u1ErrCode !=
              POLICY_CONTROL_FAILURE)) || ((pPktMap->pIfIdRsvpNumErrObj != NULL)
                                           && (pPktMap->pIfIdRsvpNumErrObj->
                                               ErrorSpec.u1ErrCode !=
                                               POLICY_CONTROL_FAILURE))
            || ((pPktMap->pIfIdRsvpErrObj == NULL)
                && (pPktMap->pIfIdRsvpNumErrObj == NULL)))
        {
            if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                 MPLS_TE_DEDICATED_ONE2ONE) &&
                (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType ==
                 RPTE_TNL_WORKING_PATH)
                &&
                ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                  LOCAL_PROT_IN_USE)
                 || (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                     LOCAL_PROT_AVAIL)))
            {
                RSVPTE_DBG1 (RSVPTE_PE_PRCS,
                             "PATH ERR should not sent further incase of Tunnel IN-USE case "
                             "with Tnl Id : %d\n",
                             OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID (pPktMap)));
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_PE_ETEXT,
                            "RptePehProcessPathErrMsg : EXIT \n");
                return;
            }
            if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                 MPLS_TE_DEDICATED_ONE2ONE) &&
                (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType ==
                 RPTE_TNL_WORKING_PATH)
                && (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse !=
                    LOCAL_PROT_NOT_AVAIL)
                && (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse !=
                    LOCAL_PROT_NOT_APPLICABLE))
            {
                RSVPTE_DBG1 (RSVPTE_PE_PRCS,
                             "PATH ERR should not sent further incase of Tunnel IN-USE case "
                             "with Tnl Id : %d\n",
                             OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID (pPktMap)));
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_PE_ETEXT,
                            "RptePehProcessPathErrMsg : EXIT \n");
                return;
            }
        }
    }
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) & RPTE_DSTR_MSG_ID_PRESENT)
            && ((INT4) RPTE_TNL_DSTR_MAX_MSG_ID (pRsvpTeTnlInfo) >=
                (INT4) RPTE_PKT_MAP_MSG_ID (pPktMap)))
        {
            RSVPTE_DBG (RSVPTE_PE_PRCS,
                        "Msg ID Out Of Order. Sliently Dropping sshhh....\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PE_ETEXT,
                        "RptePehProcessPathErrMsg : INTMD-EXIT \n");
            return;
        }
        RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) |= RPTE_DSTR_MSG_ID_PRESENT;
        RPTE_TNL_DSTR_MAX_MSG_ID (pRsvpTeTnlInfo) =
            RPTE_PKT_MAP_MSG_ID (pPktMap);
    }
    RPTE_PKT_MAP_TNL_INFO (pPktMap) = pRsvpTeTnlInfo;

    /* Scanning the local msg id data base */
    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
         RPTE_OUT_PATH_MSG_ID_PRESENT) &&
        (RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) == RPTE_EXP_BACK_OFF_TIMER))
    {
        /* Treating the Path Error as an implicit ack */
        pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
        RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) = RPTE_NORMAL_TIMER;
        RPTE_TNL_PSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
            RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                             DEFAULT_CONV_TO_SECONDS);
        RptePhStartPathRefreshTmr (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    }
    if (pPktMap->pErrorSpecObj != NULL)
    {
        pErrorSpec = &pPktMap->pErrorSpecObj->ErrorSpec;
    }
    else if (pPktMap->pIfIdRsvpErrObj != NULL)
    {
        pErrorSpec = &pPktMap->pIfIdRsvpErrObj->ErrorSpec;
    }
    else if (pPktMap->pIfIdRsvpNumErrObj != NULL)
    {
        pErrorSpec = &pPktMap->pIfIdRsvpNumErrObj->ErrorSpec;
    }
    else
    {
        return;
    }

    /* As per RFC 3473, section 4.4
     * A node receiving a path Error message containing an ERROR_SPEC
     * object with the Path State Removed flag MAY also set the Path
     * State Removed flag in the outgoing Path error message. */
    if (pErrorSpec->u1Flags == RPTE_PATH_STATE_REMOVED)
    {
        /* Fill the Errospecc flag value */
        if (pPktMap->pErrorSpecObj != NULL)
        {
            RptePvmFillErrorSpecFlags (&(ERROR_SPEC_FLAGS
                                         (&ERROR_SPEC_OBJ
                                          (PKT_MAP_ERROR_SPEC_OBJ (pPktMap)))));
        }
        else if (pPktMap->pIfIdRsvpErrObj != NULL)
        {
            RptePvmFillErrorSpecFlags (&(pPktMap->pIfIdRsvpErrObj->
                                         ErrorSpec.u1Flags));
        }
        else if (pPktMap->pIfIdRsvpNumErrObj != NULL)
        {
            RptePvmFillErrorSpecFlags (&(pPktMap->pIfIdRsvpNumErrObj->
                                         ErrorSpec.u1Flags));
        }
        else
        {
            return;
        }
    }

    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        RpteUtlFillErrorTable (pErrorSpec->u1ErrCode,
                               OSIX_NTOHS (pErrorSpec->u2ErrValue),
                               pErrorSpec->u4ErrNodeAddr, pRsvpTeTnlInfo);

        if ((pRsvpTeTnlInfo->pTeTnlInfo->bIsMbbRequired == TRUE) ||
            (OSIX_NTOHS (ERROR_SPEC_VALUE (pErrorSpec)) ==
             RPTE_LOCAL_NODE_MAINTENANCE)
            || (OSIX_NTOHS (ERROR_SPEC_VALUE (pErrorSpec)) ==
                RPTE_LOCAL_LINK_MAINTENANCE))
        {
            RpteUtlFillExcludeHopList (pRsvpTeTnlInfo,
                                       pErrorSpec->u4ErrNodeAddr);
        }
    }

    if (ERROR_SPEC_CODE (pErrorSpec) == UNKNOWN_OBJECT_CLASS)
    {
        if (OSIX_NTOHS (ERROR_SPEC_VALUE (pErrorSpec)) ==
            RPTE_MESSAGE_ID_CLASS_NUM_TYPE)
        {
            if (gu1MsgIdCapable == RPTE_ENABLED)
            {
                RptePehProcessPathErr (pPktMap, pRsvpTeTnlInfo, RPTE_TRUE);
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_PE_ETEXT,
                            "RptePehProcessPathErrMsg : EXIT \n");
                return;
            }
        }
    }

    /* If path error message is received for the recovered tunnel (due to
     * invalid suggested label), reset the synchronization status
     * */

    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pRsvpTeTnlInfo->u1IsGrTnl == RPTE_YES))
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus = TNL_GR_NOT_SYNCHRONIZED;
    }

    u2Tmp = OSIX_NTOHS (ERROR_SPEC_VALUE (pErrorSpec));

    if (((RSVPTE_TE_TNL_REOPTIMIZE_STATUS
          (RPTE_TE_TNL (pRsvpTeTnlInfo))) == TE_REOPTIMIZE_ENABLE) &&
        (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) == RPTE_MID_POINT_NODE))
    {

        if ((u2Tmp == RPTE_LOCAL_LINK_MAINTENANCE) ||
            (u2Tmp == RPTE_LOCAL_NODE_MAINTENANCE))
        {
            RpteUtlFillErrorTable (pErrorSpec->u1ErrCode,
                                   OSIX_NTOHS (pErrorSpec->u2ErrValue),
                                   pErrorSpec->u4ErrNodeAddr, pRsvpTeTnlInfo);

            RpteUtlFillExcludeHopList (pRsvpTeTnlInfo,
                                       pErrorSpec->u4ErrNodeAddr);

            RptePehProcessPathErr (pPktMap, pRsvpTeTnlInfo, RPTE_TRUE);
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsg : EXIT \n");
            return;
        }

    }

    if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) != RPTE_TRUE)
        || (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) ==
            RPTE_FRR_NODE_STATE_HEADEND)
        || ((ERROR_SPEC_CODE (pErrorSpec) == RPTE_NOTIFY) &&
            ((u2Tmp & ERROR_SPEC_MASK) == RSVPTE_ZERO) &&
            (u2Tmp == RPTE_TNL_LOCAL_REPAIR)))
    {
        /*
         * If Ingress, send Path Tear message and delete Tnl Info from the table,
         * else update PHOP info and forward the Path Err down stream.
         */
        if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
        {
            /*
             * Call Process path Err routine to handle the rcvd
             * Error codes and Error Values.
             */
            RptePehProcessPathErr (pPktMap, pRsvpTeTnlInfo, RPTE_TRUE);
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsg : EXIT \n");
            return;
        }
        PKT_MAP_OUT_IF_ENTRY (pPktMap) = PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                                       (pRsvpTeTnlInfo));
        PKT_MAP_SRC_ADDR (pPktMap) =
            IF_ENTRY_ADDR (PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
        PKT_MAP_DST_ADDR (pPktMap) =
            RSVP_HOP_ADDR (&PSB_RSVP_HOP (pRsvpTeTnlInfo->pPsb));

        PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

        RPTE_PKT_MAP_NBR_ENTRY (pPktMap) = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);

        RptePvmConstructAndSndPEMsg (pPktMap);
        RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsg : EXIT \n");
        return;
    }
    else if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
             (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) ==
              RPTE_FRR_NODE_STATE_UNKNOWN))
    {
        pRsvpTmpTeTnlInfo = pRsvpTeTnlInfo;
        RpteUtlGetTnlInfoAndGrpId (pPktMap, pRsvpTeTnlInfo, &u1DetourGrpId);
        do
        {
            if (RPTE_FRR_DETOUR_GRP_ID (pRsvpTmpTeTnlInfo) != u1DetourGrpId)
            {
                continue;
            }

            if ((RSVPTE_TNL_PSB (pRsvpTmpTeTnlInfo) == NULL) ||
                (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTmpTeTnlInfo)) == NULL))
            {
                continue;
            }
            PKT_MAP_OUT_IF_ENTRY (pPktMap) = PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                                           (pRsvpTmpTeTnlInfo));
            PKT_MAP_SRC_ADDR (pPktMap) =
                IF_ENTRY_ADDR (PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                             (pRsvpTmpTeTnlInfo)));
            PKT_MAP_DST_ADDR (pPktMap) =
                RSVP_HOP_ADDR (&PSB_RSVP_HOP (pRsvpTmpTeTnlInfo->pPsb));

            PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

            RPTE_PKT_MAP_NBR_ENTRY (pPktMap) =
                RPTE_TNL_USTR_NBR (pRsvpTmpTeTnlInfo);

            RptePvmConstructAndSndPEMsg (pPktMap);
            RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsg : EXIT \n");
        }
        while ((pRsvpTmpTeTnlInfo =
                RPTE_FRR_IN_TNL_INFO (pRsvpTmpTeTnlInfo)) != NULL);

        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RTH_PRCS,
                    "RESVTEAR: Label is freed and mesg is forwarded\n");
        RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthProcessResvTearMsg : EXIT \n");
        return;
    }
    else
    {
        RptePehProcessPathErrMsgForFrr (pPktMap, pRsvpTeTnlInfo);
    }
    RpteUtlCleanPktMap (pPktMap);

    RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsg : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePehProcessPathErr
 * Description     : This function processes the PathErr message if necessary
 *                   send's Path Tear Message.
 * Input (s)       : *pPktMap - Pointer to Packet Map
 *                 : *pRsvpTeTnlInfo - Pointer to ResvpTeTnlInfo 
 * Output (s)      : None 
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePehProcessPathErr (tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                       UINT1 u1RemoveFlag)
{
    tErrorSpec         *pErrorSpec = NULL;
    UINT2               u2Tmp = RPTE_ZERO;
    tRpteKey            RpteKey;

    RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErr : ENTRY \n");
    if (pPktMap->pErrorSpecObj != NULL)
    {
        pErrorSpec = &pPktMap->pErrorSpecObj->ErrorSpec;
    }
    else if (pPktMap->pIfIdRsvpErrObj != NULL)
    {
        pErrorSpec = &pPktMap->pIfIdRsvpErrObj->ErrorSpec;
    }
    else if (pPktMap->pIfIdRsvpNumErrObj != NULL)
    {
        pErrorSpec = &pPktMap->pIfIdRsvpNumErrObj->ErrorSpec;
    }
    else
    {
        return;
    }

    switch (ERROR_SPEC_CODE (pErrorSpec))
    {
            /* For Routing Problem Err code */
        case RPTE_ROUTE_PROB:
            switch (OSIX_NTOHS (ERROR_SPEC_VALUE (pErrorSpec)))
            {

                case RPTE_BAD_EXPLICT_ROUTE_OBJ:
                case RPTE_BAD_STRICT_NODE:
                case RPTE_BAD_LOOSE_NODE:
                case RPTE_BAD_INIT_SUB_OBJ:
                case RPTE_NO_ROUTE_AVAIL:
                case RPTE_UNACTBL_LABEL_VALUE:
                case RPTE_RRO_IND_ROUTE_LOOP:
                case RPTE_NON_RSVP_RTR:
                case RPTE_LBL_ALLOC_FAIL:
                case RPTE_UNSUPPORTED_L3PID:
                    /* For the above all Err Values, send Path Tear Message */
                    RptePhGeneratePathTear (pRsvpTeTnlInfo);
                    /* Delete the existing Tnl Info from the Tnl Table */
                    RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_SIG, u1RemoveFlag);
                    break;

                default:
                    RSVPTE_DBG1 (RSVPTE_PE_PRCS,
                                 "PATH ERR : Unknown Error value rcvd with "
                                 "Routing Problem Error code : %d\n",
                                 OSIX_NTOHS (ERROR_SPEC_VALUE (pErrorSpec)));
                    break;
            }
            break;

            /* For Notify Err code */
        case RPTE_NOTIFY:

            u2Tmp = OSIX_NTOHS (ERROR_SPEC_VALUE (pErrorSpec));
            if ((u2Tmp & ERROR_SPEC_MASK) == RSVPTE_ZERO)
            {
                if (u2Tmp == RPTE_RRO_TOO_LARGE)
                {
                    /*
                     * Set u1RROErrFlag, so that subsequent path message will
                     * not be sent with RRO's.
                     */
                    pRsvpTeTnlInfo->u1RROErrFlag |= RPTE_RRO_ERR_PATH_SET;
                    /* Since removal of RRO needs to notified to all
                     * other routers in the path, a new message id
                     * should be generated.
                     * So, if a message id exists it is removed
                     */
                    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
                         RPTE_OUT_PATH_MSG_ID_PRESENT) ==
                        RPTE_OUT_PATH_MSG_ID_PRESENT)
                    {
                        RpteKey.u4_Key = RPTE_TNL_OUT_PATH_MSG_ID
                            (pRsvpTeTnlInfo);
                        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                             &RpteKey,
                                             RPTE_LCL_MSG_ID_DB_PREFIX,
                                             RpteMIHReleaseTrieInfo);
                        /* After removing out path message id from trie, teTnlInfo
                         * pointer has to be removed from RBTree. RBTree removal has
                         * to be done only for out path message id, since only out
                         * path message id is used for SRefresh RecoveryPath message
                         * support
                         * */
                        TeSigDelFromTeMsgIdBasedTable (pRsvpTeTnlInfo->
                                                       pTeTnlInfo);

                    }
                    /* Call path refresh */
                    RptePhPathRefresh (pRsvpTeTnlInfo);
                }
                else if (u2Tmp == RPTE_RRO_NOTIFY)
                {
                    /*
                     * Set u1RROErrFlag, so that subsequent path message will
                     * not be sent with RRO's.
                     */
                    pRsvpTeTnlInfo->u1RROErrFlag |= RPTE_RRO_ERR_PATH_SET;
                    /* Since removal of RRo needs to notified to all
                     * other routers in the path, a new message id
                     * should be generated.
                     * So, if a message id exists it is removed
                     */
                    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
                         RPTE_OUT_PATH_MSG_ID_PRESENT) ==
                        RPTE_OUT_PATH_MSG_ID_PRESENT)
                    {
                        RpteKey.u4_Key = RPTE_TNL_OUT_PATH_MSG_ID
                            (pRsvpTeTnlInfo);
                        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                             &RpteKey,
                                             RPTE_LCL_MSG_ID_DB_PREFIX,
                                             RpteMIHReleaseTrieInfo);
                        /* After removing out path message id from trie, teTnlInfo
                         * pointer has to be removed from RBTree. RBTree removal has
                         * to be done only for out path message id, since only out
                         * path message id is used for SRefresh RecoveryPath message
                         * support
                         * */
                        TeSigDelFromTeMsgIdBasedTable (pRsvpTeTnlInfo->
                                                       pTeTnlInfo);

                    }

                    /* Call path refresh */
                    RptePhPathRefresh (pRsvpTeTnlInfo);
                }
                else if (u2Tmp == RPTE_TNL_LOCAL_REPAIR)
                {
                    RpteStartFrrGblRevertTimer (pRsvpTeTnlInfo);
                }
                else if (u2Tmp == RPTE_PREFERABLE_PATH_EXIST)
                {
                    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RptePehProcessPathErr : "
                                "ErrorValue - RPTE_PREFERABLE_PATH_EXIST\n");
                    pRsvpTeTnlInfo->b1IsPreferablePathExist = TRUE;
                    RpteTriggerReoptimization (pRsvpTeTnlInfo,
                                               RPTE_REOPT_PREFER_PATH_EXIST);
                }
                else if (u2Tmp == RPTE_LOCAL_LINK_MAINTENANCE)
                {
                    RSVPTE_DBG1 (RSVPTE_REOPT_DBG, "RptePehProcessPathErr : "
                                 "ErrorValue - RPTE_REOPT_LINK_MAINTENANCE, Error Node Address(%x)\n",
                                 OSIX_NTOHL (ERROR_SPEC_ADDR (pErrorSpec)));

                    if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo)
                        == RPTE_INGRESS)
                    {
                        RPTE_REOPTIMIZE_ERR_LINK_ADDR (pRsvpTeTnlInfo) =
                            OSIX_NTOHL (ERROR_SPEC_ADDR (pErrorSpec));
                        pRsvpTeTnlInfo->u1ReoptMaintenanceType =
                            RPTE_REOPT_LINK_MAINTENANCE;
                        RpteTriggerReoptimization (pRsvpTeTnlInfo,
                                                   RPTE_REOPT_LINK_MAINTENANCE);
                    }

                    if (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) ==
                        RPTE_MID_POINT_NODE)
                    {
                        RPTE_REOPTIMIZE_ERR_LINK_ADDR (pRsvpTeTnlInfo) =
                            OSIX_NTOHL (ERROR_SPEC_ADDR (pErrorSpec));
                        pRsvpTeTnlInfo->u1ReoptMaintenanceType =
                            RPTE_REOPT_LINK_MAINTENANCE;
                        RpteTriggerReoptimization (pRsvpTeTnlInfo,
                                                   RPTE_REOPT_LINK_MAINTENANCE);
                    }
                }
                else if (u2Tmp == RPTE_LOCAL_NODE_MAINTENANCE)
                {
                    RSVPTE_DBG1 (RSVPTE_REOPT_DBG, "RptePehProcessPathErr : "
                                 "ErrorValue - RPTE_REOPT_NODE_MAINTENANCE, Error Node Address(%x)\n",
                                 OSIX_NTOHL (ERROR_SPEC_ADDR (pErrorSpec)));

                    if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo)
                        == RPTE_INGRESS)
                    {
                        RPTE_REOPTIMIZE_ERR_LINK_ADDR (pRsvpTeTnlInfo) =
                            OSIX_NTOHL (ERROR_SPEC_ADDR (pErrorSpec));
                        pRsvpTeTnlInfo->u1ReoptMaintenanceType =
                            RPTE_REOPT_NODE_MAINTENANCE;
                        RpteTriggerReoptimization (pRsvpTeTnlInfo,
                                                   RPTE_REOPT_NODE_MAINTENANCE);
                    }

                    if (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) ==
                        RPTE_MID_POINT_NODE)
                    {
                        RPTE_REOPTIMIZE_ERR_LINK_ADDR (pRsvpTeTnlInfo) =
                            OSIX_NTOHL (ERROR_SPEC_ADDR (pErrorSpec));
                        pRsvpTeTnlInfo->u1ReoptMaintenanceType =
                            RPTE_REOPT_NODE_MAINTENANCE;
                        RpteTriggerReoptimization (pRsvpTeTnlInfo,
                                                   RPTE_REOPT_NODE_MAINTENANCE);
                    }
                }
            }
            break;

            /* 
             * NOTE : ADMISSION_CONTROL_FAILURE err is rcvd due to Lbl alloc 
             * failure or Traffic controller updation failure.
             */
        case ADMISSION_CONTROL_FAILURE:
        case POLICY_CONTROL_FAILURE:
        case RPTE_DIFFSERV_ERROR:
        case RPTE_DIFFSERV_TE_ERROR:
            /* 
             * NOTE : CONFLICTING_RESV_STYLE err will occur in
             *  Re-route condition, When we initiate LSP without Tnl
             *  may Re route flag, and later if we initiate LSP with Tnl
             *  may Re Route Flag, this err will occur.
             */
        case CONFLICTING_RESV_STYLE:
        case UNKNOWN_OBJECT_CTYPE:
            if (OSIX_NTOHS (ERROR_SPEC_VALUE (pErrorSpec)) ==
                RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE)
            {
                pRsvpTeTnlInfo->u2HlspCType =
                    RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE_1;
                RptePhPathRefresh (pRsvpTeTnlInfo);
                break;

            }

            /* For the above Err Codes, send Path Tear Message */
            RptePhGeneratePathTear (pRsvpTeTnlInfo);
            /* Delete the existing Tnl Info from the Tnl Table */
            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, u1RemoveFlag);
            break;

        case UNKNOWN_OBJECT_CLASS:

            if (OSIX_NTOHS (ERROR_SPEC_VALUE (pErrorSpec)) ==
                RPTE_MESSAGE_ID_CLASS_NUM_TYPE)
            {
                if (gu1MsgIdCapable == RPTE_ENABLED)
                {
                    NBR_ENTRY_RR_STATE (RPTE_PKT_MAP_NBR_ENTRY (pPktMap)) =
                        RPTE_DISABLED;
                    NBR_ENTRY_RMD_CAPABLE (RPTE_PKT_MAP_NBR_ENTRY (pPktMap)) =
                        RPTE_DISABLED;
                    RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo) =
                        RPTE_PKT_MAP_NBR_ENTRY (pPktMap);
                    RSVPTE_DBG (RSVPTE_PE_ETEXT,
                                "RptePehProcessPathErr : EXIT \n");
                    return;
                }
            }
            RptePhGeneratePathTear (pRsvpTeTnlInfo);
            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, u1RemoveFlag);
            break;
        default:
            RSVPTE_DBG1 (RSVPTE_PE_PRCS,
                         "PATH ERR : Unknown Error code in Path Err : %d \n",
                         ERROR_SPEC_CODE (pErrorSpec));
            break;
    }

    RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErr : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteFrrConstructAndSendPathErr
 * Description     : This function processes the PathErr message with
 *                   u1ErrCode and u2ErrValue
 * Input (s)       : *pPktMap - Pointer to Packet Map
 * Output (s)      : None 
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteFrrConstructAndSendPathErr (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tPktMap             PktMap;
    tRsvpTeSsnObj       SsnObj;
    tRsvpTeSndrTmpObj   RsvpTeSndrTmpObj;
    tSenderTspecObj     SenderTspecObj;
    tRsvpHopObj         RsvpHopObj;
    tIfEntry           *pIfEntry = NULL;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcRROBackoffTimeOut : ENTRY \n");

    if ((pIfEntry = MemAllocMemBlk (RSVPTE_IF_ENTRY_POOL_ID)) == NULL)
    {
        return;
    }
    MEMSET (&SsnObj, RSVPTE_ZERO, sizeof (tRsvpTeSsnObj));
    MEMSET (&RsvpTeSndrTmpObj, RSVPTE_ZERO, sizeof (tRsvpTeSndrTmpObj));
    MEMSET (&SenderTspecObj, RSVPTE_ZERO, sizeof (tSenderTspecObj));
    MEMSET (&RsvpHopObj, RSVPTE_ZERO, sizeof (tRsvpHopObj));
    MEMSET (pIfEntry, RSVPTE_ZERO, sizeof (tIfEntry));
    /* Initialise PktMap */
    RpteUtlInitPktMap (&PktMap);

    PKT_MAP_RSVP_HOP_OBJ (&PktMap) = &RsvpHopObj;
    PKT_MAP_IF_ENTRY (&PktMap) = pIfEntry;
    PKT_MAP_RPTE_SSN_OBJ (&PktMap) = &SsnObj;
    PKT_MAP_RPTE_SND_TMP_OBJ (&PktMap) = &RsvpTeSndrTmpObj;
    PKT_MAP_SENDER_TSPEC_OBJ (&PktMap) = &SenderTspecObj;

    RpteUtlPhPreparePktMapRROErr (&PktMap, pRsvpTeTnlInfo);
    RptePvmSendPathErr (&PktMap, RPTE_NOTIFY, RPTE_TNL_LOCAL_REPAIR);
    MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfEntry);
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePehProcessPathErrMsgForFrr
 * Description     : This function processes the PathErr message for
 *                   fast reroute
 * Input (s)       : pPktMap - Pointer to Packet Map
 *                   pRsvpTeTnlInfo - Rsvp tnl info
 * Output (s)      : None 
 * Returns         : None
 *---------------------------------------------------------------------------*/
VOID
RptePehProcessPathErrMsgForFrr (tPktMap * pPktMap,
                                tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tIfEntry           *pIfEntry = NULL;
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    BOOL1               bFlag = RPTE_FALSE;
    BOOL1               bIsBkpTnl = RPTE_NO;
    UINT1               u1DetourGrpId = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsgForFrr : ENTRY \n");

    pRsvpTeFastReroute =
        &PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));

    if (RPTE_IS_PLR_OR_PLRAWAIT (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
    {
        /* Classifying whether PATH Error is received on primary or 
         * backup path */
        if ((IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) ==
             IF_ENTRY_ADDR (PSB_OUT_IF_ENTRY (pRsvpTeTnlInfo->pPsb))) &&
            (MEMCMP (&PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB
                                         (pRsvpTeTnlInfo)).TnlSndrAddr,
                     (&PKT_MAP_RPTE_SNDR_TMP_OBJ_SNDR_ADDR (pPktMap)),
                     RSVPTE_IPV4ADR_LEN) == RPTE_ZERO))
        {
            bIsBkpTnl = RPTE_NO;
        }
        else if (((RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) != NULL) &&
                  (PKT_MAP_IF_ENTRY (pPktMap) == PSB_OUT_IF_ENTRY
                   (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo)->pPsb))) &&
                 (MEMCMP (&PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB
                                              (RPTE_FRR_OUT_TNL_INFO
                                               (pRsvpTeTnlInfo))).TnlSndrAddr,
                          (&PKT_MAP_RPTE_SNDR_TMP_OBJ_SNDR_ADDR (pPktMap)),
                          RSVPTE_IPV4ADR_LEN) == RPTE_ZERO))
        {
            bIsBkpTnl = RPTE_YES;
        }
        else
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "PERR: Message received on wrong interface.\n");
            RSVPTE_DBG (RSVPTE_PE_ETEXT,
                        "RptePehProcessPathErrMsgForFrr : EXIT \n");
            return;
        }

        /* Handling Facility Backup Here. */
        if (pRsvpTeFastReroute->u1Flags == TE_TNL_FRR_FACILITY_METHOD)
        {
            if ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_IN_USE)
                == LOCAL_PROT_IN_USE)
            {
                if (RPTE_IS_HEADEND (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
                {
                    RptePehProcessPathErr (pPktMap, pRsvpTeTnlInfo, RPTE_TRUE);
                    RSVPTE_DBG (RSVPTE_PE_PRCS,
                                "PERR: Msg is sent upstream.\n");
                    RSVPTE_DBG (RSVPTE_PE_ETEXT,
                                "RptePehProcessPathErrMsgForFrr : " "EXIT \n");
                    return;
                }
                PKT_MAP_OUT_IF_ENTRY (pPktMap) =
                    PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
                PKT_MAP_SRC_ADDR (pPktMap) =
                    IF_ENTRY_ADDR (PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                                 (pRsvpTeTnlInfo)));
                PKT_MAP_DST_ADDR (pPktMap) =
                    RSVP_HOP_ADDR (&PSB_RSVP_HOP (pRsvpTeTnlInfo->pPsb));

                PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

                RPTE_PKT_MAP_NBR_ENTRY (pPktMap) =
                    RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);

                RptePvmConstructAndSndPEMsg (pPktMap);
                RSVPTE_DBG (RSVPTE_PE_PRCS, "PERR: Msg is sent upstream.\n");
                RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsgForFrr : "
                            "EXIT \n");
            }
            else if (bIsBkpTnl == RPTE_YES)
            {
                RptePehProcessPathErr (pPktMap, pRsvpTeTnlInfo, RPTE_TRUE);
            }
            return;
        }

        /* PATH Err is received on the primary path. But No Backup path
         * exists. So, forwarding the PATH Err to the Upstream Node. */
        if ((bIsBkpTnl == RPTE_NO)
            && (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) == NULL))
        {
            PKT_MAP_OUT_IF_ENTRY (pPktMap) = PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                                           (pRsvpTeTnlInfo));
            PKT_MAP_SRC_ADDR (pPktMap) =
                IF_ENTRY_ADDR (PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
            PKT_MAP_DST_ADDR (pPktMap) =
                RSVP_HOP_ADDR (&PSB_RSVP_HOP (pRsvpTeTnlInfo->pPsb));

            PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

            RPTE_PKT_MAP_NBR_ENTRY (pPktMap) =
                RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);

            RptePvmConstructAndSndPEMsg (pPktMap);
            RSVPTE_DBG (RSVPTE_PE_PRCS, "PERR: Msg is sent upstream.\n");
            RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsgForFrr : "
                        "EXIT \n");
            return;
        }

        /* Path Error message is received on primary path. Backup path exists.
         * So, Just Updating PSB Timer of Protected Tunnel. */
        if ((bIsBkpTnl == RPTE_NO)
            && (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) != NULL))
        {
            /* PSB Time To Die updation */
            pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
            PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                RpteUtlComputeLifeTime (pIfEntry,
                                        IF_ENTRY_REFRESH_INTERVAL (pIfEntry));
            PSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                 DEFAULT_CONV_TO_SECONDS);

            /* RSB Time To Die updation */
            if (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL)
            {
                pIfEntry = RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pRsvpTeTnlInfo));
                RSB_TIME_TO_DIE (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)) =
                    RpteUtlComputeLifeTime (pIfEntry,
                                            IF_ENTRY_REFRESH_INTERVAL
                                            (pIfEntry));
                RSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)) =
                    (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                     DEFAULT_CONV_TO_SECONDS);
            }
            RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) |= RPTE_FRR_NODE_STATE_DNLOST;
            RSVPTE_DBG (RSVPTE_PE_PRCS,
                        "PERR: Backup Tunnel exists for this tunnel. "
                        "So, Updating the PSB Timer of the Primary path.\n");
            RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsgForFrr : "
                        "EXIT \n");
            return;
        }

        /* Path Err msg is received on backup path. So, Sending 
         * a PATH Tear on the backup path. Changing the state of PLR. */
        if (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) != NULL)
        {
            RptePehProcessPathErr (pPktMap, RPTE_FRR_OUT_TNL_INFO
                                   (pRsvpTeTnlInfo), RPTE_FALSE);

            RpteResetPlrStatus (pRsvpTeTnlInfo);

            RpteUpdateDetourStats (RPTE_TWO);
        }

        /* If the Node state is not MP or DMP and the node state is 
         * Downstream Lost, Sending the PATH Error received to the 
         * upstream node. */
        if ((!(RPTE_IS_MP (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo))) ||
             !(RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))) &&
            RPTE_IS_DNLOST (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
        {
            if (RPTE_IS_HEADEND (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
            {
                RptePehProcessPathErr (pPktMap, pRsvpTeTnlInfo, RPTE_TRUE);
                RSVPTE_DBG (RSVPTE_PE_PRCS,
                            "PERR: Msg has reached the headend.\n");
                return;
            }
            PKT_MAP_OUT_IF_ENTRY (pPktMap) = PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                                           (pRsvpTeTnlInfo));
            PKT_MAP_SRC_ADDR (pPktMap) =
                IF_ENTRY_ADDR (PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
            PKT_MAP_DST_ADDR (pPktMap) =
                RSVP_HOP_ADDR (&PSB_RSVP_HOP (pRsvpTeTnlInfo->pPsb));

            PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

            RPTE_PKT_MAP_NBR_ENTRY (pPktMap) =
                RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);

            RptePvmConstructAndSndPEMsg (pPktMap);
            RSVPTE_DBG (RSVPTE_PE_PRCS, "PERR: Msg is sent upstream.\n");
            RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsgForFrr : "
                        "EXIT \n");
            return;
        }
    }

    if (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
    {
        /* Getting the Group ID to duplicate the PATH Error to the path
         * having the same ID. */
        if (RpteUtlGetGrpIdForDMP (pRsvpTeTnlInfo, PKT_MAP_IF_ENTRY (pPktMap),
                                   &u1DetourGrpId) == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsgForFrr : "
                        "INTMD-EXIT \n");
            return;
        }
    }

    /* If the Node state is MP or DMP, the received PATH Error is 
     * duplicated on all the In Rsvp Tunnels. */
    if (RPTE_IS_MP (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)) ||
        RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
    {
        do
        {
            if (u1DetourGrpId != RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTnlInfo))
            {
                continue;
            }
            if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL)
            {
                continue;
            }
            PKT_MAP_OUT_IF_ENTRY (pPktMap) = PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                                           (pRsvpTeTnlInfo));
            PKT_MAP_SRC_ADDR (pPktMap) =
                IF_ENTRY_ADDR (PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
            PKT_MAP_DST_ADDR (pPktMap) =
                RSVP_HOP_ADDR (&PSB_RSVP_HOP (pRsvpTeTnlInfo->pPsb));

            PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

            RPTE_PKT_MAP_NBR_ENTRY (pPktMap) =
                RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);

            RptePvmConstructAndSndPEMsg (pPktMap);

            /* Updating the statistical values. */
            if ((bFlag == RPTE_TRUE) && (RSVPTE_FRR_DETOUR_INCOMING_NUM
                                         (gpRsvpTeGblInfo) != RPTE_ZERO))
            {
                RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
            }
            bFlag = RPTE_TRUE;
        }
        while ((pRsvpTeTnlInfo = RPTE_FRR_IN_TNL (pRsvpTeTnlInfo)) != NULL);
    }
    RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsgForFrr : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 *  *  * Function Name   : RpteConstructPktMapAndSendResvErr
 *   *   * Description     : This function processes the ResvErr message with
 *    *    *                   u1ErrCode and u2ErrValue
 *     *     * Input (s)       : pRsvpTeTnlInfo - Rsvp tnl info
 *      *      * Output (s)      : None
 *       *       * Returns         : None
 *        *        */
/*---------------------------------------------------------------------------*/
VOID
RpteConstructPktMapAndSendResvErr (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tPktMap             PktMap;
    tPktMap            *pPktMap;
    tStyleObj           StyleObj;
    tRsvpTeSsnObj       SsnObj;
    tRsvpTeFilterSpecObj RsvpTeFilterSpecObj;
    tRsvpHopObj         RsvpHopObj;
    tFlowSpecObj        FlowSpecObj;
    UINT4               u4TmpAddr = 0;
    tObjHdr             ObjHdr;
    tRsb               *pRsb = NULL;
    tFlowSpec          *pFlowSpec = NULL;
    tIfEntry           *pIfEntry = NULL;

    if ((pIfEntry = MemAllocMemBlk (RSVPTE_IF_ENTRY_POOL_ID)) == NULL)
    {
        return;
    }
    MEMSET (&SsnObj, RSVPTE_ZERO, sizeof (tRsvpTeSsnObj));
    MEMSET (&StyleObj, RSVPTE_ZERO, sizeof (tStyleObj));
    MEMSET (&RsvpTeFilterSpecObj, RSVPTE_ZERO, sizeof (tRsvpTeFilterSpecObj));
    MEMSET (&FlowSpecObj, RSVPTE_ZERO, sizeof (tFlowSpecObj));
    MEMSET (&RsvpHopObj, RSVPTE_ZERO, sizeof (tRsvpHopObj));
    MEMSET (pIfEntry, RSVPTE_ZERO, sizeof (tIfEntry));
    /* Initialise PktMap */
    RpteUtlInitPktMap (&PktMap);

    PKT_MAP_RSVP_HOP_OBJ (&PktMap) = &RsvpHopObj;
    PKT_MAP_IF_ENTRY (&PktMap) = pIfEntry;
    PKT_MAP_RPTE_SSN_OBJ (&PktMap) = &SsnObj;
    PKT_MAP_RPTE_FILTER_SPEC_OBJ (&PktMap) = &RsvpTeFilterSpecObj;
    PKT_MAP_FLOW_SPEC_OBJ (&PktMap) = &FlowSpecObj;
    PKT_MAP_STYLE_OBJ (&PktMap) = &StyleObj;

    pPktMap = &PktMap;
    pRsb = RSVPTE_TNL_RSB (pRsvpTeTnlInfo);
/* Copy Inc If Id from Tnl Info Rsb */
    PKT_MAP_IF_ENTRY (pPktMap) =
        RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pRsvpTeTnlInfo));

    /* Copy Src Addr */
    IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) =
        IF_ENTRY_ADDR (RSB_OUT_IF_ENTRY (pRsb));

    /* Copy RsvpTeSession */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pPktMap->pRsvpTeSsnObj->ObjHdr = ObjHdr;
    u4TmpAddr = RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo);
    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeSsnObj->RsvpTeSession.TnlDestAddr,
            (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPktMap->pRsvpTeSsnObj->RsvpTeSession.u2Rsvd = RSVPTE_ZERO;
    pPktMap->pRsvpTeSsnObj->RsvpTeSession.u2TnlId =
        (UINT2) OSIX_HTONS (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex);
    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo)), u4TmpAddr);

    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeSsnObj->RsvpTeSession.ExtnTnlId,
            (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    /* Copy RsvpHop info */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (sizeof (tRsvpHopObj));
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_RSVP_HOP_CLASS_NUM_TYPE);
    pPktMap->pRsvpHopObj->ObjHdr = ObjHdr;
    MEMCPY (&pPktMap->pRsvpHopObj->RsvpHop, &RSB_NEXT_RSVP_HOP
            (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)), sizeof (tRsvpHop));

    /* Copy Dest Addr */
    pPktMap->pRsvpHopObj->RsvpHop.u4HopAddr =
        OSIX_HTONL (RSVP_HOP_ADDR
                    (&RSB_NEXT_RSVP_HOP (RSVPTE_TNL_RSB (pRsvpTeTnlInfo))));

    /* Fill  Style Obj * */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (sizeof (tStyleObj));
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) = OSIX_HTONS (IPV4_STYLE_CLASS_NUM_TYPE);

    pPktMap->pStyleObj->ObjHdr = ObjHdr;

    MEMCPY (&(pPktMap->pStyleObj->Style), &(pRsb->Style), sizeof (tStyle));

    /* Copy FlowSpec  Obj */
    pFlowSpec = &RSB_FLOW_SPEC (pRsb);

    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (sizeof (tFlowSpecObj));
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_FLOW_SPEC_CLASS_NUM_TYPE);
    pPktMap->pFlowSpecObj->ObjHdr = ObjHdr;
    MEMCPY (&(pPktMap->pFlowSpecObj->FlowSpec), pFlowSpec, sizeof (tFlowSpec));

    /* Copy RsvpTeFilterSpecObj */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (RPTE_IPV4_FLTR_SPEC_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE);

    pPktMap->pRsvpTeFilterSpecObj->ObjHdr = ObjHdr;

    u4TmpAddr = RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo);

    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.
            TnlSndrAddr, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPktMap->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.u2Rsvd = RSVPTE_ZERO;
    pPktMap->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.u2LspId =
        (UINT2) OSIX_HTONS (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);

    PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;

    RptePvmSendResvErr (&PktMap, POLICY_CONTROL_FAILURE, DEFAULT_ERROR_VALUE);
    MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfEntry);
    return;
}

/*---------------------------------------------------------------------------*/
/*
 *  *  * Function Name   : RpteConstructPktMapAndSendPathErr
 *   *   * Description     : This function processes the PathErr message with
 *    *    *                   u1ErrCode and u2ErrValue
 *     *     * Input (s)       : pRsvpTeTnlInfo - Rsvp tnl info
 *      *      * Output (s)      : None
 *       *       * Returns         : None
 *        *        */
/*---------------------------------------------------------------------------*/
VOID
RpteConstructPktMapAndSendPathErr (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                   UINT2 u2ErrCode, UINT2 u2ErrVal)
{
    tPktMap             PktMap;
    tPktMap            *pPktMap;
    tRsvpTeSsnObj       SsnObj;
    tRsvpTeSndrTmpObj   RsvpTeSndrTmpObj;
    tSenderTspecObj     SenderTspecObj;
    tRsvpHopObj         RsvpHopObj;
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4TmpAddr = 0;
    tObjHdr             ObjHdr;

    if ((pIfEntry = MemAllocMemBlk (RSVPTE_IF_ENTRY_POOL_ID)) == NULL)
    {
        return;
    }
    MEMSET (&SsnObj, RSVPTE_ZERO, sizeof (tRsvpTeSsnObj));
    MEMSET (&RsvpTeSndrTmpObj, RSVPTE_ZERO, sizeof (tRsvpTeSndrTmpObj));
    MEMSET (&SenderTspecObj, RSVPTE_ZERO, sizeof (tSenderTspecObj));
    MEMSET (&RsvpHopObj, RSVPTE_ZERO, sizeof (tRsvpHopObj));
    MEMSET (pIfEntry, RSVPTE_ZERO, sizeof (tIfEntry));

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
    {
        MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfEntry);
        return;
    }

    if ((u2ErrCode == RPTE_ZERO) && (u2ErrVal == RPTE_ZERO))
    {
        MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfEntry);
        return;
    }

    /* Initialise PktMap */
    RpteUtlInitPktMap (&PktMap);

    PKT_MAP_RSVP_HOP_OBJ (&PktMap) = &RsvpHopObj;
    PKT_MAP_IF_ENTRY (&PktMap) = pIfEntry;
    PKT_MAP_RPTE_SSN_OBJ (&PktMap) = &SsnObj;
    PKT_MAP_RPTE_SND_TMP_OBJ (&PktMap) = &RsvpTeSndrTmpObj;
    PKT_MAP_SENDER_TSPEC_OBJ (&PktMap) = &SenderTspecObj;

    pPktMap = &PktMap;

    /* Copy Inc If Id from Tnl Info PSB */
    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL)
    {
        PKT_MAP_IF_ENTRY (pPktMap) =
            PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));

        /* Copy Dest Addr */
        pPktMap->pRsvpHopObj->RsvpHop.u4HopAddr =
            OSIX_HTONL (RSVP_HOP_ADDR
                        (&PSB_RSVP_HOP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))));

        /* Copy Src Addr */
        IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) =
            IF_ENTRY_ADDR (PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
    }

    /* Copy Session */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pPktMap->pRsvpTeSsnObj->ObjHdr = ObjHdr;
    u4TmpAddr = RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo);

    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeSsnObj->RsvpTeSession.TnlDestAddr,
            (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPktMap->pRsvpTeSsnObj->RsvpTeSession.u2Rsvd = RSVPTE_ZERO;
    pPktMap->pRsvpTeSsnObj->RsvpTeSession.u2TnlId =
        (UINT2) OSIX_HTONS (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex);

    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo)), u4TmpAddr);

    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeSsnObj->RsvpTeSession.ExtnTnlId,
            (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    /* Copy SenderTemplate */
    OBJ_HDR_LENGTH (&ObjHdr) = OSIX_HTONS (RPTE_IPV4_SNDR_TMP_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (&ObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE);

    pPktMap->pRsvpTeSndrTmpObj->ObjHdr = ObjHdr;
    u4TmpAddr = RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo);

    MEMCPY ((UINT1 *) &pPktMap->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.TnlSndrAddr,
            (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPktMap->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.u2Rsvd = RSVPTE_ZERO;

    pPktMap->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.u2LspId =
        (UINT2) OSIX_HTONS (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);
    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL)
    {
        MEMCPY (PKT_MAP_SENDER_TSPEC_OBJ (pPktMap),
                &PSB_SENDER_TSPEC_OBJ (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                sizeof (tSenderTspecObj));
    }

    pPktMap->pRsvpTeTnlInfo = pRsvpTeTnlInfo;
    PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;

    RptePvmSendPathErr (&PktMap, (UINT1) u2ErrCode, u2ErrVal);
    MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfEntry);
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rpteperr.c                             */
/*---------------------------------------------------------------------------*/
