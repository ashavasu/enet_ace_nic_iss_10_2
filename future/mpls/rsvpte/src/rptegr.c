/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptegr.c,v 1.11 2014/12/24 10:58:29 siva Exp $
 *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptegr.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for the GR functionalities.
 *
 *---------------------------------------------------------------------------*/
#include "rpteincs.h"
#include "mplscli.h"
#include "iss.h"

#define MAX_COLUMN_LENGTH 80

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteGrShutDownProcess
 * Description     : This function handles the GR-shoutdown process
 * Input (s)       : None
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

PUBLIC VOID
RpteGrShutDownProcess (VOID)
{
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrShutDownProcess : ENTRY \n");
    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability ==
        RPTE_GR_CAPABILITY_FULL)
    {
#ifdef CLI_WANTED
        IssCsrSaveCli ((UINT1 *) "RSVPTE");
#endif
        gpRsvpTeGblInfo->u1GrProgressState = RPTE_GR_SHUT_DOWN_IN_PROGRESS;
        RpteGrStoreDynamicInfo ();
        /*start Max-wait timer im mpls-rtr module */
        MplsStartRequestedTimers (MPLS_RPTE_MAX_WAIT_TMR_EVENT, NULL);
    }
    RpteShutDownProcess ();
    IssSetModuleSystemControl (RSVPTE_MODULE_ID, MODULE_SHUTDOWN);
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrShutDownProcess : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteGrStoreDynamicInfo
 * Description     : This function stores the dynamically populated info
 *                   [hello src instance, nbr addr and nbr if index] when the
 *                   RSVP-TE component is made as shutdown
 * Input (s)       : None
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteGrStoreDynamicInfo ()
{
    tNbrEntry          *pTempNbrEntry = NULL;
    tIfEntry           *pTempIfEntry = NULL;
    tTMO_SLL           *pTempNbrList = NULL;
    UINT4               u4HashIndex;
    CHR1                ai1Buf[MAX_COLUMN_LENGTH + RPTE_ONE];
    INT4                i4FileFd = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrStoreDynamicInfo : ENTRY \n");
    MEMSET (ai1Buf, RPTE_ZERO, MAX_COLUMN_LENGTH);

    i4FileFd = FileOpen (RSVPTE_GR_CONF, OSIX_FILE_CR | OSIX_FILE_WO);

    if (i4FileFd < RPTE_ZERO)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS, " File is not created \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrStoreDynamicInfo : INTMD-EXIT \n");
        /* File Creation failed */
        return;
    }
    SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "MY_HELLO_INSTANCE = %d\n",
              gu4MyHelloInstance);
    FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));

    TMO_HASH_Scan_Table (RSVP_GBL_IF_HSH_TBL, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (RSVP_GBL_IF_HSH_TBL, u4HashIndex, pTempIfEntry,
                              tIfEntry *)
        {
            pTempNbrList = &(IF_ENTRY_NBR_LIST (pTempIfEntry));

            TMO_SLL_Scan (pTempNbrList, pTempNbrEntry, tNbrEntry *)
            {
                if (pTempNbrEntry->bIsHelloActive == RPTE_FALSE)
                {
                    continue;
                }
                SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "NBR_ADDR = %d\n",
                          pTempNbrEntry->u4Addr);
                FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));
                SNPRINTF (ai1Buf, MAX_COLUMN_LENGTH, "NBR_IF_INDEX = %d\n",
                          pTempNbrEntry->pIfEntry->u4IfIndex);
                FileWrite (i4FileFd, ai1Buf, STRLEN (ai1Buf));
            }
        }
    }
    FileClose (i4FileFd);
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrStoreDynamicInfo : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteGrReStoreDynamicInfo
 * Description     : This function restores the dynamically populated info
 *                   [hello src instance, nbr addr and nbr if index] when the
 *                   RSVP-TE component is restarted
 * Input (s)       : None
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

PUBLIC VOID
RpteGrReStoreDynamicInfo ()
{
    tNbrEntry          *pNbrEntry = NULL;
    tIfEntry           *pIfEntry = NULL;
    tHelloNbrInfo      *pHelloNbrInfo = NULL;
    INT1                ai1Buf[MAX_COLUMN_LENGTH + RPTE_ONE];
    /* Stores each line in file */
    UINT1               au1NameStr[MAX_COLUMN_LENGTH + 1];
    /* Stores line identification */
    UINT1               au1EqualStr[MAX_COLUMN_LENGTH + 1];
    /* Stores the string "=" */
    UINT1               au1ValueStr[MAX_COLUMN_LENGTH + 1];
    /* Stores the value for each identification */
    INT4                i4FileFd = RPTE_ZERO;

    UINT4               u4NbrAddr = RPTE_ZERO;
    UINT4               u4NbrIfIndex = RPTE_ZERO;
    INT1               *pi1String = NULL;
    INT1                ai1Temp[MAX_COLUMN_LENGTH + 1];

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrReStoreDynamicInfo : ENTRY \n");

    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability !=
        RPTE_GR_CAPABILITY_FULL)
    {
        return;
    }

    gpRsvpTeGblInfo->u1GrProgressState = RPTE_GR_RESTART_IN_PROGRESS;

    MEMSET (ai1Buf, RPTE_ZERO, MAX_COLUMN_LENGTH + RPTE_ONE);
    i4FileFd = FileOpen (RSVPTE_GR_CONF, OSIX_FILE_RO);

    if (i4FileFd < RPTE_ZERO)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS, " File is not created \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteGrReStoreDynamicInfo : INTMD-EXIT \n");
        /* File Creation failed */
        return;
    }

    while (FsUtlReadLine (i4FileFd, ai1Buf) == OSIX_SUCCESS)
    {
        MEMSET (au1NameStr, 0, sizeof (au1NameStr));
        MEMSET (au1EqualStr, 0, sizeof (au1EqualStr));
        MEMSET (au1ValueStr, 0, sizeof (au1ValueStr));

        if ((STRCMP (ai1Buf, "")) != 0)
        {
            STRNCPY (ai1Temp, ai1Buf, MAX_COLUMN_LENGTH);
            ai1Temp[MAX_COLUMN_LENGTH] = '\0';
            pi1String = (INT1 *) STRTOK ((CHR1 *) ai1Temp, " ");

            if ((pi1String != NULL) && (STRLEN (pi1String) <= sizeof(au1NameStr)))
            {
                STRNCPY (au1NameStr, pi1String, STRLEN (pi1String));
                au1NameStr[MAX_COLUMN_LENGTH] = '\0';
            }

            pi1String = (INT1 *) STRTOK (NULL, " ");

            if ((pi1String != NULL)&& (STRLEN (pi1String) <= sizeof(au1EqualStr)))
            {
                STRNCPY (au1EqualStr, pi1String, STRLEN (pi1String));
                au1EqualStr[MAX_COLUMN_LENGTH] = '\0';
            }

            pi1String = (INT1 *) STRTOK (NULL, " ");

            if ((pi1String != NULL) && (STRLEN (pi1String) <= sizeof(au1ValueStr)))
            {
                STRNCPY (au1ValueStr, pi1String, STRLEN (pi1String));
                au1ValueStr[MAX_COLUMN_LENGTH] = '\0';
            }

            if (STRLEN (au1ValueStr) == 0)
            {
                continue;
            }

            if (STRCMP (au1NameStr, "MY_HELLO_INSTANCE") == 0)
            {
                gu4MyHelloInstance = (UINT4)ATOI (au1ValueStr);
                gu4MyHelloInstance++;
            }
            else if (STRCMP (au1NameStr, "NBR_ADDR") == 0)
            {
                u4NbrAddr = (UINT4)ATOI (au1ValueStr);
            }
            else if (STRCMP (au1NameStr, "NBR_IF_INDEX") == 0)
            {
                u4NbrIfIndex = (UINT4)ATOI (au1ValueStr);
                if (RpteGetIfEntry ((INT4)u4NbrIfIndex, &pIfEntry) == RPTE_SUCCESS)
                {
                    pNbrEntry =
                        RptePvmCreateNbrTableEntry (pIfEntry, u4NbrAddr,
                                                    RPTE_ZERO, RPTE_ZERO);

                    if (pNbrEntry != NULL)
                    {
                        pHelloNbrInfo = &pNbrEntry->HelloNbrInfo;
                        pHelloNbrInfo->u1HelloState = STATUS_UNKNOWN;
                        pNbrEntry->bIsHelloSynchronoziedAfterRestart =
                            RPTE_FALSE;
                        gpRsvpTeGblInfo->u1GrProgressState =
                            RPTE_GR_RECOVERY_IN_PROGRESS;
                        RpteHhGenerateHello (pNbrEntry, RPTE_HELLO_REQ_CTYPE,
                                             pIfEntry);
                    }
                }

            }
        }
        MEMSET (ai1Buf, 0, MAX_COLUMN_LENGTH);
    }
    /* After sending hello messages to the nbrs, start Recovery timer */
    RpteGrStartRecoveryTmr (gpRsvpTeGblInfo);
    FileClose (i4FileFd);
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrReStoreDynamicInfo : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteGrStoreNbrProperties
 * Description     : This function stores the nbr GR properties
 * Input (s)       : pNbrEntry    - Pointer to the neigbour entry
 *                   pPktMap      - Pointer to the packet map
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteGrStoreNbrProperties (tNbrEntry * pNbrEntry, tPktMap * pPktMap)
{
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4RecoveryPathCapable = RPTE_ZERO;

    pIfEntry = PKT_MAP_IF_ENTRY (pPktMap);

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrStoreNbrProperties : ENTRY \n");

    if (pPktMap->pRestartCapObj != NULL)
    {
        pNbrEntry->u2RestartTime =
            (UINT2)OSIX_NTOHL (pPktMap->pRestartCapObj->RestartCap.u4RestartTime);
        pNbrEntry->u2RecoveryTime =
            (UINT2)OSIX_NTOHL (pPktMap->pRestartCapObj->RestartCap.u4RecoveryTime);
        pIfEntry->IfStatsInfo.u4IfNumHelloRcvdWithRestartCap++;
    }
    else
    {
        pNbrEntry->u2RestartTime = RPTE_ZERO;
        pNbrEntry->u2RecoveryTime = RPTE_ZERO;
    }
    if (pPktMap->pCapabilityObj != NULL)
    {
        u4RecoveryPathCapable =
            OSIX_NTOHL (pPktMap->pCapabilityObj->Capability.
                        u4RecoveryPathCapable);
        if (u4RecoveryPathCapable & RPTE_GR_PKT_RECOVERY_PATH_RX)
        {
            pNbrEntry->u1RecoveryPathCapability |= RPTE_GR_RECOVERY_PATH_RX;
        }
        if (u4RecoveryPathCapable & RPTE_GR_PKT_RECOVERY_PATH_TX)
        {
            pNbrEntry->u1RecoveryPathCapability |= RPTE_GR_RECOVERY_PATH_TX;
        }
        if (u4RecoveryPathCapable & RPTE_GR_PKT_RECOVERY_PATH_SREFRESH)
        {
            pNbrEntry->u1RecoveryPathCapability |=
                RPTE_GR_RECOVERY_PATH_SREFRESH;
        }

        pIfEntry->IfStatsInfo.u4IfNumHelloRcvdWithCapability++;
    }
    else
    {
        pNbrEntry->u1RecoveryPathCapability = RPTE_ZERO;
    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrStoreNbrProperties : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteGrStartRecoveryTmr                                 */
/* Description     : This function starts the Recovery Timer for the        */
/*                   RsvpTeGblInfo                                          */
/* Input (s)       : pRsvpTeGblInfo - Pointer to the RsvpTeGblInfo          */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
UINT1
RpteGrStartRecoveryTmr (tRsvpTeGblInfo * pRsvpTeGblInfo)
{
    tTimerParam        *pTmrParam = NULL;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrStartRecoveryTmr : ENTRY \n");

    pTmrParam = (tTimerParam *) & (pRsvpTeGblInfo->GrTimerParam);

    TIMER_PARAM_ID (pTmrParam) = RPTE_GR_RECOVERY_TIMER;
    pTmrParam->u.pRsvpTeGblInfo = (tRsvpTeGblInfo *) pRsvpTeGblInfo;
    pRsvpTeGblInfo->GrTimer.u4Data = (FS_ULONG) pTmrParam;
    if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST, &(pRsvpTeGblInfo->GrTimer),
                           (pRsvpTeGblInfo->RsvpTeCfgParams.u2RecoveryTime *
                            SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS, " GR Recovery timer failed \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrStartRecoveryTmr : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrStartRecoveryTmr : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteGrRecoveryLabelProcedure                           */
/* Description     : This function starts the Recovery procedure after      */
/*                   receiving path message with recovery label obj         */
/* Input (s)       : pPktMap - Pointer to the packet map                    */
/*                   pRsvpTeTnlInfo - Pointer to the RSVPTE tnl info        */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

UINT1
RpteGrRecoveryLabelProcedure (tPktMap * pPktMap,
                              tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4RecoveryLabel = MPLS_INVALID_LABEL;
    UINT4               u4DnOutLbl = MPLS_INVALID_LABEL;
    UINT4               u4UpOutLbl = MPLS_INVALID_LABEL;
    UINT4               u4UpInLbl = MPLS_INVALID_LABEL;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrRecoveryLabelProcedure : ENTRY \n");

    if (pPktMap->pRecoveryLblObj == NULL)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS, " Recovery label is not available \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteGrRecoveryLabelProcedure : INTMD-EXIT \n");
        return RPTE_NO;
    }

    pPktMap->pIncIfEntry->IfStatsInfo.u4IfNumPathRcvdWithRecoveryLbl++;

    u4RecoveryLabel = OSIX_NTOHL (pPktMap->pRecoveryLblObj->Label.u4GenLbl);

    MplsGetDnStrInfoUsingRecoveryLbl (u4RecoveryLabel, &pTeTnlInfo,
                                      &u4DnOutLbl);

    if (pTeTnlInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS, " Te Tunnel info is not availble for "
                    "the given recovery label \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteGrRecoveryLabelProcedure : INTMD-EXIT \n");
        return RPTE_NO;
    }

    RPTE_TE_TNL (pRsvpTeTnlInfo) = pTeTnlInfo;

    if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
    {
        TeSigGetLabelInfoUsingTnl (pRsvpTeTnlInfo->pTeTnlInfo, &u4UpOutLbl,
                                   &u4UpInLbl, MPLS_DIRECTION_REVERSE);
    }
    if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus == TNL_GR_NOT_SYNCHRONIZED)
        || (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
            TNL_GR_DOWNSTREAM_SYNCHRONIZED))
    {
        /* For egress case, it can receive only path message with recovery label
         * object from upstream node. Once it receives the sync status can be
         * set as fully synchronized
         *
         * For Intermediate case, it can recive both path message with recovery label
         * object from upstrem node and RecoveryPath message from downstream 
         * node. Once it receives path message with recovery label object,
         * it has to check whether RecoveryPath capability is enabled or not
         * If its not enabled, then the sync status can be set as fully synchronized.
         * If its enabled, it has to check whether RecoveryPath message is already
         * received or not. If it received RecoveryPath message already, then 
         * the sync status can be set as fully synchronized. Otherwise it can be
         * set as upstream synchronized
         * */
        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS)
        {
            pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl = u4RecoveryLabel;
            if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
            {
                pRsvpTeTnlInfo->UpStrOutLbl.u4GenLbl = u4UpOutLbl;
            }
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus =
                TNL_GR_FULLY_SYNCHRONIZED;
        }
        else if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1RecoveryPathCapability &
                 RPTE_GR_RECOVERY_PATH_RX)
        {
            if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
                TNL_GR_DOWNSTREAM_SYNCHRONIZED)
            {
                /* The labels programmed after receiving RecoveryPath message
                 * should be matched with current labels
                 * */
                if ((pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl != u4UpInLbl) ||
                    (pRsvpTeTnlInfo->UpStrOutLbl.u4GenLbl != u4UpOutLbl) ||
                    (pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl != u4RecoveryLabel)
                    || (pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl != u4DnOutLbl))
                {
                    RSVPTE_DBG (RSVPTE_GR_PRCS,
                                " Labels already programmed (recovered by "
                                "recovery path message) are not matched with the "
                                "current labels (recovered by recovery label object) \n");
                    RSVPTE_DBG (RSVPTE_GR_ETEXT,
                                "RpteGrRecoveryLabelProcedure : INTMD-EXIT \n");
                    return RPTE_NO;
                }
                pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus =
                    TNL_GR_FULLY_SYNCHRONIZED;
            }
            else
            {
                pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl = u4RecoveryLabel;
                pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl = u4DnOutLbl;
                if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
                {
                    pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl = u4UpInLbl;
                    pRsvpTeTnlInfo->UpStrOutLbl.u4GenLbl = u4UpOutLbl;
                }
                pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus =
                    TNL_GR_UPSTREAM_SYNCHRONIZED;
            }
        }
        else
        {
            pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl = u4RecoveryLabel;
            pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl = u4DnOutLbl;
            if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
            {
                pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl = u4UpInLbl;
                pRsvpTeTnlInfo->UpStrOutLbl.u4GenLbl = u4UpOutLbl;
            }
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus =
                TNL_GR_FULLY_SYNCHRONIZED;
        }
        RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrRecoveryLabelProcedure : EXIT \n");
        return RPTE_YES;
    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT,
                "RpteGrRecoveryLabelProcedure : INTMD-EXIT \n");
    return RPTE_NO;
}

/****************************************************************************/
/* Function Name   : RpteGrProcessRecoveryTimeOut                           */
/* Description     : This function handles the recovery timer expiry        */
/* Input (s)       : pRsvpTeGblInfo - Pointer to the RSVPTE gbl info        */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteGrProcessRecoveryTimeOut (tRsvpTeGblInfo * pRsvpTeGblInfo)
{
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrProcessRecoveryTimeOut : ENTRY \n");
    pRsvpTeGblInfo->u1GrProgressState = RPTE_GR_COMPLETED;
    TeSigGrDeleteNonSynchronizedTnls ();
    /* Temporarly created RSVP-TE tnl, PSB information of non synchronized
     * tunnels should be removed
     * */
    pTmpRsvpTeTnlInfo = RBTreeGetFirst (pRsvpTeGblInfo->RpteTnlTree);
    while (pTmpRsvpTeTnlInfo != NULL)
    {
        if (pTmpRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
            TNL_GR_NOT_SYNCHRONIZED)
        {
            RpteRelTnlEntryFunc (pRsvpTeGblInfo, pTmpRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
        }
        pTmpRsvpTeTnlInfo = RBTreeGetNext (pRsvpTeGblInfo->RpteTnlTree,
                                           (tRBElem *) pTmpRsvpTeTnlInfo, NULL);
    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrProcessRecoveryTimeOut : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteGrProcessRecoveryPathMsg                           */
/* Description     : This function is used to process the RecoveryPath msg  */
/* Input (s)       : pPktMap - Pointer to the packet map                    */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RpteGrProcessRecoveryPathMsg (tPktMap * pPktMap)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4RecoveryLabel = RPTE_ZERO;
    UINT4               u4NextHopAddr = RPTE_ZERO;
    UINT4               u4DnInLbl = RPTE_ZERO;
    UINT4               u4UpOutLbl = MPLS_INVALID_LABEL;
    UINT4               u4UpInLbl = MPLS_INVALID_LABEL;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrProcessRecoveryPathMsg : ENTRY \n");

    if ((pPktMap->pRecoveryLblObj == NULL) ||
        (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_NO_ACK))
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS, " Recovery label is not available \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteGrProcessRecoveryPathMsg : INTMD-EXIT \n");
        RpteUtlCleanPktMap (pPktMap);
        return;
    }

    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pPktMap->pNbrEntry->bIsHelloSynchronoziedAfterRestart != RPTE_TRUE) &&
        (pPktMap->pRecoveryLblObj != NULL))
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS,
                    "Hello Resynchronization is not happened after restart \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteGrProcessRecoveryPathMsg : INTMD-EXIT \n");
        RpteUtlCleanPktMap (pPktMap);
        return;
    }

    u4RecoveryLabel = OSIX_NTOHL (pPktMap->pRecoveryLblObj->Label.u4GenLbl);

    if (pPktMap->pRsvpHopObj != NULL)
    {
        u4NextHopAddr = OSIX_NTOHL (pPktMap->pRsvpHopObj->RsvpHop.u4HopAddr);
    }
    else if (pPktMap->pIfIdRsvpHopObj != NULL)
    {
        u4NextHopAddr = OSIX_NTOHL (pPktMap->pIfIdRsvpHopObj->
                                    RsvpTlvObjHdr.u4HopAddr);
    }
    else if (pPktMap->pIfIdRsvpNumHopObj != NULL)
    {
        u4NextHopAddr = OSIX_NTOHL (pPktMap->pIfIdRsvpNumHopObj->
                                    RsvpTlvNumObjHdr.u4HopAddr);
    }
    MplsGetDnStrInfoUsingRecoveryPath (u4RecoveryLabel, u4NextHopAddr,
                                       &pTeTnlInfo, &u4DnInLbl);
    if (pTeTnlInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS, " Te tunnel info is not available for the "
                    "recovery path message \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteGrProcessRecoveryPathMsg : INTMD-EXIT \n");
        RpteUtlCleanPktMap (pPktMap);
        return;
    }

    /* If the node is already received RecoveryPath message ans recovered the
     * tunnel, then silently drop the packet */

    if ((pTeTnlInfo->u1TnlSyncStatus == TNL_GR_DOWNSTREAM_SYNCHRONIZED) ||
        (pTeTnlInfo->u1TnlSyncStatus == TNL_GR_FULLY_SYNCHRONIZED))
    {
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_GR_PRCS, " node is already received RecoveryPath "
                    "message. Simply drop the packet \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrProcessRecoveryPathMsg : EXIT \n");
        return;
    }

    if (pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
    {
        TeSigGetLabelInfoUsingTnl (pTeTnlInfo, &u4UpOutLbl, &u4UpInLbl,
                                   MPLS_DIRECTION_REVERSE);
    }
    RpteGrRecoveryPathMsgProcedure (pTeTnlInfo, u4DnInLbl, u4RecoveryLabel,
                                    u4UpOutLbl, u4UpInLbl);
    RpteUtlCleanPktMap (pPktMap);
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrProcessRecoveryPathMsg : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteGrRecoveryPathMsgProcedure                         */
/* Description     : This function starts recovery procedure after          */
/*                   receiving  RecoveryPath msg                            */
/* Input (s)       : pPktMap - Pointer to the packet map                    */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

UINT1
RpteGrRecoveryPathMsgProcedure (tTeTnlInfo * pTeTnlInfo, UINT4 u4DnInLbl,
                                UINT4 u4DnOutLbl, UINT4 u4UpOutLbl,
                                UINT4 u4UpInLbl)
{
    UINT4               u4TunnelIndex = RPTE_ZERO;
    UINT4               u4TunnelInstance = RPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RPTE_ZERO;
    UINT1               u1TempTnlRole = RPTE_ZERO;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;

    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrRecoveryPathMsgProcedure : ENTRY \n");

    if ((pTeTnlInfo->u1TnlSyncStatus == TNL_GR_NOT_SYNCHRONIZED) ||
        (pTeTnlInfo->u1TnlSyncStatus == TNL_GR_UPSTREAM_SYNCHRONIZED))
    {
        u1TempTnlRole = pTeTnlInfo->u1TnlRole;
        u4TunnelIndex = RSVPTE_TNL_INDEX (pTeTnlInfo);
        u4TunnelInstance = RSVPTE_TNL_INSTANCE (pTeTnlInfo);
        CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID (pTeTnlInfo),
                            u4TunnelIngressLSRId);
        u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
        CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID (pTeTnlInfo),
                            u4TunnelEgressLSRId);
        u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);
        if (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo, u4TunnelIndex,
                                    u4TunnelInstance, u4TunnelIngressLSRId,
                                    u4TunnelEgressLSRId, &pRsvpTeTnlInfo)
            == RPTE_FAILURE)
        {
            if (RpteCreateNewRsvpteTunnel (u4TunnelIndex,
                                           u4TunnelInstance,
                                           u4TunnelIngressLSRId,
                                           u4TunnelEgressLSRId,
                                           pTeTnlInfo, &pRsvpTeTnlInfo,
                                           RPTE_TRUE) != RPTE_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_GR_PRCS,
                            " RSVP-TE tunnel info is not created \n");
                RSVPTE_DBG (RSVPTE_GR_ETEXT,
                            "RpteGrRecoveryPathMsgProcedure : "
                            "INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            RPTE_TE_TNL (pRsvpTeTnlInfo) = pTeTnlInfo;
            if (RBTreeAdd (gpRsvpTeGblInfo->RpteTnlTree,
                           (tRBElem *) pRsvpTeTnlInfo) != RB_SUCCESS)
            {
                return RPTE_FAILURE;
            }
        }
        pRsvpTeTnlInfo->u1IsGrTnl = RPTE_YES;
        /* For ingress case, it can receive only RecoveryPath message
         * downstream node. Once it receives the sync status can be
         * set as fully synchronized
         *
         * For Intermediate case, it can recive both path message with recovery label
         * object from upstrem node and RecoveryPath message from downstream
         * node. Once it receives RecoveryPath message from downstream node,
         * path message with recovery label object is already received or not. 
         * If it received path message with recovery label object already, then
         * the sync status can be set as fully synchronized. Otherwise it can be
         * set as downstream synchronized
         * */

        if (u1TempTnlRole == RPTE_INGRESS)
        {
            pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl = u4DnOutLbl;
            if (pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
            {
                pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl = u4UpInLbl;
            }
            pTeTnlInfo->u1TnlSyncStatus = TNL_GR_FULLY_SYNCHRONIZED;
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole = RPTE_INGRESS;
            if (RptePhSetupTunnel (pRsvpTeTnlInfo) != RPTE_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_PORT_DBG, "Setup Tunnel Failed\n");

                return RPTE_FAILURE;
            }
        }
        else if (pTeTnlInfo->u1TnlSyncStatus == TNL_GR_UPSTREAM_SYNCHRONIZED)
        {
            /* The labels programmed after receiving Path message with 
             * recovery label object should be matched with current labels
             * */
            if ((pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl != u4UpInLbl) ||
                (pRsvpTeTnlInfo->UpStrOutLbl.u4GenLbl != u4UpOutLbl) ||
                (pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl != u4DnInLbl) ||
                (pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl != u4DnOutLbl))
            {
                RSVPTE_DBG (RSVPTE_GR_PRCS,
                            " Labels already programmed (recovered by "
                            "recovery label object) are not matched with the "
                            "current labels (recovered by recovery path message) \n");

                RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrRecoveryPathMsgProcedure :"
                            "INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            pTeTnlInfo->u1TnlSyncStatus = TNL_GR_FULLY_SYNCHRONIZED;

            /* Gr node gets help from both upstream and downstream node.
             * Hence it has to start refresh the path message to its
             * downstream node and resv message to its upstream node 
             * */
            RptePhPathRefresh (pRsvpTeTnlInfo);
            RptePhStartPathRefreshTmr (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
            RpteRhResvRefresh (pRsvpTeTnlInfo);
        }
        else
        {
            pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl = u4DnOutLbl;
            pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl = u4DnInLbl;
            if (pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
            {
                pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl = u4UpInLbl;
                pRsvpTeTnlInfo->UpStrOutLbl.u4GenLbl = u4UpOutLbl;
            }
            pTeTnlInfo->u1TnlSyncStatus = TNL_GR_DOWNSTREAM_SYNCHRONIZED;
        }
    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT, "RpteGrRecoveryPathMsgProcedure : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteGrProcessSRefreshRecoveryPathMsg                   */
/* Description     : This function is used to process SRefresh              */
/*                   RecoveryPath msg                                       */
/* Input (s)       : u4MsgId - Message Id                                   */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

UINT1
RpteGrProcessSRefreshRecoveryPathMsg (UINT4 u4MsgId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4DnOutLbl = MPLS_INVALID_LABEL;
    UINT4               u4DnInLbl = MPLS_INVALID_LABEL;
    UINT4               u4UpOutLbl = MPLS_INVALID_LABEL;
    UINT4               u4UpInLbl = MPLS_INVALID_LABEL;

    RSVPTE_DBG (RSVPTE_GR_ETEXT,
                "RpteGrProcessSRefreshRecoveryPathMsg : ENTRY \n");
    /*Get the te tunnel info from Msg Id based RBTree */
    TeSigGetTunnelInfoFromOutMsgId (&pTeTnlInfo, u4MsgId);

    if (pTeTnlInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS,
                    " Te Tunnel info is not available for the msg id "
                    "present in the SRefresh recovery path msg \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteGrProcessSRefreshRecoveryPathMsg : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    TeSigGetLabelInfoUsingTnl (pTeTnlInfo, &u4DnOutLbl, &u4DnInLbl,
                               MPLS_DIRECTION_FORWARD);

    if (pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
    {
        TeSigGetLabelInfoUsingTnl (pTeTnlInfo, &u4UpOutLbl, &u4UpInLbl,
                                   MPLS_DIRECTION_REVERSE);
    }

    if (RpteGrRecoveryPathMsgProcedure (pTeTnlInfo, u4DnInLbl, u4DnOutLbl,
                                        u4UpOutLbl, u4UpInLbl) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_GR_PRCS,
                    " Recovery Path message procedur failed \n");
        RSVPTE_DBG (RSVPTE_GR_ETEXT,
                    "RpteGrProcessSRefreshRecoveryPathMsg : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_GR_ETEXT,
                "RpteGrProcessSRefreshRecoveryPathMsg : EXIT \n");
    return RPTE_SUCCESS;
}
