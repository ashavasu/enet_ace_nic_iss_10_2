
/********************************************************************
 *                                                                  *
 * $RCSfile: rptesock.c,v $
 *                                                                  *
 * $Date: 2016/01/25 12:11:10 $
 *
 * $Id: rptesock.c,v 1.38 2016/01/25 12:11:10 siva Exp $
 *                                                                  *
 * $Revision: 1.38 $
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 * FILE NAME             : rptesock.c                                          
 * PRINCIPAL AUTHOR      : Aricent Inc.                      
 * SUBSYSTEM NAME        : MPLS                                                
 * MODULE NAME           : RSVP-TE
 * LANGUAGE              : ANSI-C                                               
 * TARGET ENVIRONMENT    : Linux (Portable)                                    
 * DATE OF FIRST RELEASE :                                                     
 * DESCRIPTION           : This file contains the Socket layer related         
 *                         functions used by the RSVP-TE module     
 *--------------------------------------------------------------------------*/

#include "rpteincs.h"
#include "fssocket.h"
#include "rtm.h"
#include "arp.h"

/****************************************************************************/
/* Function Name   : RpteOpenRawSocket                                      */
/* Description     : this function opens a raw socket for RSVP-TE           */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : i4SockFd or -1                                        */
/****************************************************************************/
INT4
RpteOpenRawSocket (VOID)
{
    INT4                i4RawSockFd = RSVPTE_ZERO;
    UINT4               u4OptnVal = RSVPTE_ONE;
    UINT1               u1OptnVal = RSVPTE_ONE;
    INT4                i4OptnVal = RSVPTE_ONE;

    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteOpenRawSocket : ENTRY \n ");

    i4RawSockFd = RSVPTE_SOCKET (AF_INET, SOCK_RAW, IPPROTO_RSVP);
    if (i4RawSockFd < RSVPTE_ZERO)
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "Raw Socket Creation Failed.\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteOpenRawSocket : INTMD-EXIT \n");
        return (ERR_SENT);
    }
    /*Used for Non-Blocking case. Timer needs to be re-started for this case */
    if (RSVPTE_FCNTL (i4RawSockFd, F_SETFL, O_NONBLOCK) < RSVPTE_ZERO)
    {
        RSVPTE_SOCK_CLOSE (i4RawSockFd);
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    "Failure in setting NON_BLOCK option.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteOpenRawSocket : INTMD-EXIT \n");
        return (ERR_SENT);
    }
    /* For Interface index receiving upon packet arrival, IP_PKTINFO is mandatory */
    if (setsockopt (i4RawSockFd, IPPROTO_IP, IP_PKTINFO, &u1OptnVal,
                    sizeof (UINT1)) < 0)
    {
        RSVPTE_SOCK_CLOSE (i4RawSockFd);
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "Failure in setsockopt (IP_PKTINFO).\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteOpenRawSocket : INTMD-EXIT \n");
        return (ERR_SENT);
    }
    /* This option is for receiving packet with Router alert option */
    if (setsockopt (i4RawSockFd, IPPROTO_IP, IP_ROUTER_ALERT, &u4OptnVal,
                    sizeof (UINT4)) < 0)
    {
        RSVPTE_SOCK_CLOSE (i4RawSockFd);
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "Failure in setsockopt (IP_PKTINFO).\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteOpenRawSocket : INTMD-EXIT \n");
        return (ERR_SENT);
    }
    /* IP Header is included in the packet construction */
    if (setsockopt (i4RawSockFd, IPPROTO_IP, IP_HDRINCL,
                    (&i4OptnVal), (sizeof (INT4))) < 0)
    {
        RSVPTE_SOCK_CLOSE (i4RawSockFd);
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "Failure in setsockopt (IP_HDRINCL).\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteOpenRawSocket : INTMD-EXIT \n");
        return (ERR_SENT);
    }
    if (SelAddFd (i4RawSockFd, RsvpRawPktInSocket) == OSIX_FAILURE)
    {

        RSVPTE_SOCK_CLOSE (i4RawSockFd);
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "Failure in SelAddFd.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteOpenRawSocket : INTMD-EXIT \n");
        return (ERR_SENT);
    }
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteOpenRawSocket : EXIT \n ");
    return i4RawSockFd;
}

/****************************************************************************/
/* Function Name   : RpteCloseRawSocket                                     */
/* Description     : this function closes the raw socket opened by RSVP-TE  */
/* Input (s)       : i4RawSockFd                                            */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS/RPTE_FAILURE                              */
/****************************************************************************/
UINT1
RpteCloseRawSocket (INT4 i4RawSockFd)
{
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteCloseRawSocket : ENTRY \n ");

    SelRemoveFd (i4RawSockFd);
    if (RSVPTE_SOCK_CLOSE (i4RawSockFd) != RSVPTE_ZERO)
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "Failure in Closing the Raw Socket.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteCloseRawSocket : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteCloseRawSocket : EXIT \n ");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteSendViaSocket                                      */
/* Description     : This function sends packet via raw socket              */
/* Input (s)       : pMsgBuf - pointer to message buffer                    */
/*                   u4SrcAddr - Source address of the packet               */
/*                   u4DestAddr - Destination address to be sent            */
/* Output (s)      : None.                                                  */
/* Returns         : RPTE_SUCCESS or RPTE_FAILURE                           */
/****************************************************************************/
UINT1
RpteSendViaSocket (tMsgBuf * pMsgBuf, tPktMap * pPktMap)
{
    struct sockaddr_in  DestAddr;
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo = NULL;
    struct cmsghdr     *pCmsgInfo = NULL;
    t_IP_HEADER        *pIpHdr = NULL;
    UINT4               u4SrcAddr = RPTE_ZERO;
    UINT4               u4DestAddr = RPTE_ZERO;
    UINT4               u4L3IfIndex = RPTE_ZERO;
    UINT4               u4PktMapAddr = RPTE_ZERO;
#ifndef BSDCOMP_SLI_WANTED
    UINT4               u4IfIndex = RPTE_ZERO;
#endif
    UINT4               u4IpPort = RPTE_ZERO;
    UINT4               u4BkpTnlXcIndex = RPTE_ZERO;
    UINT4               u4ActualNextHop = RPTE_ZERO;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT1              *pRawPkt = NULL;
    UINT1               u1MsgType = RPTE_ZERO;
    UINT1               u1IpHdrLen = IP_HDR_LEN;
    UINT1               aRtrAlert[RSVPTE_IP_RTR_ALERT_LEN + 1] =
        { 148, 04, 00, 00 };
    UINT1               au1Cmsg[24];
    BOOL1               bRtrAlert = FALSE;
#ifndef BSDCOMP_SLI_WANTED
    BOOL1               bIsExplicitPath = FALSE;
    UINT1               au1DstMac[CFA_ENET_ADDR_LEN], u1EncapType = 0;
    tArpQMsg            ArpQMsg;
#endif
    tRsvpHdr           *pRsvpHdr = NULL;
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
#endif

    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    u4SrcAddr = PKT_MAP_SRC_ADDR (pPktMap);
    u4DestAddr = PKT_MAP_DST_ADDR (pPktMap);

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);

    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : ENTRY \n");
    /* Get the message type for IP Header construction */
    if (CRU_BUF_Copy_FromBufChain (pMsgBuf, &u1MsgType, RPTE_ONE,
                                   RPTE_ONE) < RSVPTE_ZERO)
    {
        BUF_RELEASE (pMsgBuf);
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    "IPIF: Message type get failed in RpteSendViaSocket..\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD_EXIT \n");
        return RPTE_FAILURE;
    }
    if ((u1MsgType == PATH_MSG) || (u1MsgType == PATHTEAR_MSG)
        || (u1MsgType == RESVCONF_MSG))
    {
        /* Router Alert option needs to be added in PATH Message, 
         * PATH TEAR Message, RESVCONF Message as per RFC 2205 section : 3.3*/
        if (PKT_MAP_RA_OPT_NEEDED (pPktMap) == RPTE_YES)
        {
            bRtrAlert = TRUE;
            u1IpHdrLen =(UINT1)(u1IpHdrLen + RSVPTE_IP_RTR_ALERT_LEN);
        }
    }

    if ((pRawPkt = MemAllocMemBlk (RSVPTE_RAW_PKT_POOL_ID)) == NULL)
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    "IPIF: Memory allocation for linear memory failed in RpteSendViaSocket..\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD_EXIT \n");
        BUF_RELEASE (pMsgBuf);
        return RPTE_FAILURE;
    }

    pIpHdr = (t_IP_HEADER *) (VOID *) pRawPkt;

    pIpHdr->u1Ver_hdrlen = (bRtrAlert == TRUE) ?
        IP_VERS_AND_HLEN (IP_VERSION_4,
                          RSVPTE_IP_RTR_ALERT_LEN) :
        IP_VERS_AND_HLEN (IP_VERSION_4, RSVPTE_ZERO);

    pIpHdr->u1Tos = RPTE_ZERO;
    pIpHdr->u2Totlen =
        OSIX_HTONS ((CRU_BUF_Get_ChainValidByteCount (pMsgBuf) + u1IpHdrLen));
    pIpHdr->u2Id = RPTE_ZERO;
    pIpHdr->u2Fl_offs = RPTE_ZERO;
    pIpHdr->u1Ttl = RSVP_HDR_SEND_TTL (pRsvpHdr);
    pIpHdr->u1Proto = IPPROTO_RSVP;
    pIpHdr->u2Cksum = RPTE_ZERO;
    pIpHdr->u4Src = OSIX_HTONL (u4SrcAddr);
    pIpHdr->u4Dest = OSIX_HTONL (u4DestAddr);
    if (bRtrAlert == TRUE)
    {
        MEMCPY (pIpHdr->u1Options, aRtrAlert, RSVPTE_IP_RTR_ALERT_LEN);
    }
    pIpHdr->u2Cksum = (UINT2) RpteUtlCheckSum ((UINT1 *) pIpHdr, u1IpHdrLen);

    MEMSET ((char *) &DestAddr, RSVPTE_ZERO, sizeof (DestAddr));
    DestAddr.sin_family = AF_INET;
    DestAddr.sin_addr.s_addr = pIpHdr->u4Dest;

    if ((RPTE_PKT_MAP_TNL_INFO (pPktMap) != NULL) &&
        (PKT_MAP_SEND_TO_BYPASS_TUNNEL (pPktMap) == RPTE_TRUE) &&
        ((u1MsgType == PATH_MSG) || (u1MsgType == RESVERR_MSG) ||
         (u1MsgType == PATHTEAR_MSG) || (u1MsgType == RESVCONF_MSG)))
    {
        if (CRU_BUF_Prepend_BufChain
            (pMsgBuf, pRawPkt, u1IpHdrLen) == CRU_FAILURE)
        {
            MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
            BUF_RELEASE (pMsgBuf);
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                        "IPIF: CRU Buf copy failed in RpteSendViaSocket..\n");
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        /* Pass the backup tnl's XC index so that label stack info. will have the 
         * protected tnl's out label to stack (Bottom Of Stack (BOS) */
        u4BkpTnlXcIndex = (RPTE_TE_TNL (RPTE_PKT_MAP_TNL_INFO (pPktMap))->
                           u4TnlXcIndex);
        CRU_BUF_Set_U2Reserved (pMsgBuf, RPTE_TO_MPLSRTR);
        if (MplsHandlePktForByPassTnl (pMsgBuf, u4BkpTnlXcIndex) !=
            MPLS_SUCCESS)
        {
            /* The API is returning failure. It should have freed the 
             * memory. So, we can decrment the global variable. */
            gi4MsgAlloc--;
            MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
            RSVPTE_DBG (RSVPTE_PB_ETEXT, "RpteSendViaSocket : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        /* CRU Buf given to MPLS RTR Module which will take care of freeing
         * the memory. So, we can decrement the global variable. */
        gi4MsgAlloc--;
        MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : EXIT \n");
        return RPTE_SUCCESS;
    }

    if (PKT_MAP_OUT_IF_ENTRY (pPktMap) != NULL)
    {
        if (CfaUtilGetL3IfFromMplsIf
            ((PKT_MAP_OUT_IF_ENTRY (pPktMap)->u4IfIndex), &u4L3IfIndex,
             RPTE_TRUE) == CFA_FAILURE)
        {
            MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
            BUF_RELEASE (pMsgBuf);
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                        "IPIF: Getting L3 Interface Index from Mpls If failed\n");
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        if (NetIpv4GetPortFromIfIndex (u4L3IfIndex, &u4IpPort) ==
            NETIPV4_FAILURE)
        {
            MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
            BUF_RELEASE (pMsgBuf);
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                        "IPIF: No IP Interface available for Outgoing Packet "
                        "Interface\n");
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        u4PktMapAddr = PKT_MAP_OUT_IF_ENTRY (pPktMap)->u4Addr;
    }

    /* Backup path message sending */
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    RtQuery.u4DestinationIpAddress = OSIX_NTOHL (pIpHdr->u4Dest);
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    /* Failure check is not handled here.
     * If explicit route is configured, Actual Next Hop and Configured
     * next hop will differ. 
     * If explicit route is not configured, Actual Next Hop and configured
     * next hop will not differ. */
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "IPIF: NetIpv4GetRoute failed\n");
    }

    if (RPTE_PKT_MAP_TNL_INFO (pPktMap) != NULL)
    {
        u4ActualNextHop = RSVPTE_TNL_DN_NHOP (RPTE_PKT_MAP_TNL_INFO (pPktMap));
    }

    if ((u1MsgType == PATH_MSG) || (u1MsgType == PATHTEAR_MSG))
    {
        if ((u4ActualNextHop == RPTE_ZERO) &&
            (NetIpRtInfo.u4NextHop == RPTE_ZERO))
        {
            MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
            BUF_RELEASE (pMsgBuf);
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                        "IPIF: No Route to send the packet out\n");
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

/*#ifndef BSDCOMP_SLI_WANTED*/
        DestAddr.sin_addr.s_addr = OSIX_HTONL(u4ActualNextHop);
/*#endif*/
    }

    /* TODO The below portion should be removed once the FSIP supports
     * explicit routing. */
#ifndef BSDCOMP_SLI_WANTED
    if ((NetIpRtInfo.u4NextHop != RPTE_ZERO) && (u4ActualNextHop != RPTE_ZERO)
        && (NetIpRtInfo.u4NextHop != u4ActualNextHop))
    {
        bIsExplicitPath = TRUE;
    }
    else if ((NetIpRtInfo.u4NextHop == RPTE_ZERO) &&
             (u4ActualNextHop != RPTE_ZERO) &&
             (RPTE_PKT_MAP_TNL_INFO (pPktMap) != NULL) &&
             (RSVPTE_TNL_PATH_OPTION (RPTE_PKT_MAP_TNL_INFO (pPktMap)) != NULL))
    {
        if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                          &u4IfIndex) == NETIPV4_FAILURE)
        {
            MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        if (u4IfIndex != u4L3IfIndex)
        {
            bIsExplicitPath = TRUE;
        }
    }
    if ((((u1MsgType == PATH_MSG) || (u1MsgType == RESVERR_MSG) ||
          (u1MsgType == PATHTEAR_MSG) || (u1MsgType == RESVCONF_MSG)) &&
         ((bIsExplicitPath == TRUE) ||
          ((RPTE_PKT_MAP_TNL_INFO (pPktMap) != NULL) &&
           (PKT_MAP_IS_BKP_PATH_MSG_TO_CFA (pPktMap) == RPTE_TRUE)))) ||
        (((u1MsgType == HELLO_MSG) || (u1MsgType == RESV_MSG) ||
          (u1MsgType == PATHERR_MSG) || (u1MsgType == RESVTEAR_MSG)) &&
         (u4PktMapAddr == RPTE_ZERO)))

    {
        MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));

        /* TODO After link down handle */
        if (CRU_BUF_Prepend_BufChain
            (pMsgBuf, pRawPkt, u1IpHdrLen) == CRU_FAILURE)
        {
            MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
            BUF_RELEASE (pMsgBuf);
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                        "IPIF: CRU Buf copy failed in RpteSendViaSocket..\n");
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        if (u4PktMapAddr != RPTE_ZERO)
        {
            MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
            MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
            RtQuery.u4DestinationIpAddress = u4ActualNextHop;
            RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
            RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

            if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                            "IPIF: NetIpv4GetRoute failed\n");
            }
            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4IfIndex) == NETIPV4_FAILURE)
            {
                MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
                RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                            "RpteSendViaSocket : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            u4L3IfIndex = u4IfIndex;

            if (NetIpRtInfo.u4NextHop != RPTE_ZERO)
            {
                u4ActualNextHop = NetIpRtInfo.u4NextHop;
            }

            /* ARP Resolve */
            if (ArpResolveWithIndex (u4L3IfIndex, u4ActualNextHop,
                                     (INT1 *) au1DstMac,
                                     &u1EncapType) == ARP_FAILURE)
            {
                ArpQMsg.u4MsgType = ARP_APP_IF_MSG;
                ArpQMsg.u2Port = (UINT2) u4IpPort;
                ArpQMsg.u4IpAddr = u4ActualNextHop;
                ArpQMsg.pBuf = pMsgBuf;
                /* Trigger ARP module for Resolving NextHopAddress */
                if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                                "RpteSendViaSocket: ArpEnqueuePkt failed\n");
                }
                MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
                return RPTE_SUCCESS;
            }
        }
        else
        {
            RpteGetIfUnnumPeerMac (u4L3IfIndex, (UINT1 *) au1DstMac);
        }
        /* Send the labeled Packet to CFA Task for transmission */
        if (CfaHandlePktFromMpls (pMsgBuf, u4L3IfIndex, RPTE_TO_CFA,
                                  au1DstMac, CFA_LINK_UCAST) != CFA_SUCCESS)
        {
            MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
            BUF_RELEASE (pMsgBuf);
            return RPTE_FAILURE;
        }

        /* CRU Buf given to MPLS RTR Module which will take care of freeing
         * the memory. So, we can decrement the global variable. */
        gi4MsgAlloc--;
        MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
        return RPTE_SUCCESS;
    }
#else
    UNUSED_PARAM (u4PktMapAddr);
#endif

    if (CRU_BUF_Copy_FromBufChain (pMsgBuf, (pRawPkt + u1IpHdrLen), RPTE_ZERO,
                                   CRU_BUF_Get_ChainValidByteCount (pMsgBuf)) <
        RSVPTE_ZERO)
    {
        MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
        BUF_RELEASE (pMsgBuf);
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    "IPIF: CRU Buf copy failed in RpteSendViaSocket..\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

#ifdef BSDCOMP_SLI_WANTED
    Iov.iov_base = pRawPkt;
    Iov.iov_len = (CRU_BUF_Get_ChainValidByteCount (pMsgBuf) + u1IpHdrLen);
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#endif

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    PktInfo.msg_name = (VOID *) &DestAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_RAW;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo =
        (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

    pIpPktInfo->ipi_ifindex = (int) u4IpPort;
    pIpPktInfo->ipi_addr.s_addr = DestAddr.sin_addr.s_addr;
    pIpPktInfo->ipi_spec_dst.s_addr = DestAddr.sin_addr.s_addr;

    if (sendmsg (RSVP_GBL_SOCK_FD, &PktInfo, 0) < 0)
    {
        MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
        BUF_RELEASE (pMsgBuf);
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "IPIF: Message Sending Failed..\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD_EXIT \n");
        return RPTE_FAILURE;
    }
#ifndef BSDCOMP_SLI_WANTED
    if ((RSVPTE_SENDTO (RSVP_GBL_SOCK_FD, pRawPkt,
                        (CRU_BUF_Get_ChainValidByteCount (pMsgBuf) +
                         u1IpHdrLen), RSVPTE_ZERO,
                        (struct sockaddr *) &DestAddr,
                        sizeof (struct sockaddr_in))) < RSVPTE_ZERO)
    {
        /* pMsgBuf will be freed by caller on failure */
        MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
        BUF_RELEASE (pMsgBuf);
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    "IPIF: socket sendto failed in RpteSendViaSocket..\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : INTMD_EXIT \n");
        return RPTE_FAILURE;
    }
#endif
    BUF_RELEASE (pMsgBuf);
    MemReleaseMemBlock (RSVPTE_RAW_PKT_POOL_ID, (UINT1 *) pRawPkt);
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "RpteSendViaSocket : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteUdpOpen                                            */
/* Description     : This function opens UDP port for receving UDP Pkts     */
/* Input (s)       : i4Port - UDP Port Number                               */
/*                   i4IfIndex - Interface Number                           */
/* Output (s)      : None                                                   */
/* Returns         : If successful returns RPTE_SUCCESS.                    */
/*                   Else returns RPTE_FAILURE                              */
/****************************************************************************/
INT1
RpteUdpOpen (INT4 i4Port, INT4 i4IfIndex)
{

    struct sockaddr_in  RsvpUdpLocalAddr;
    UINT4               u4OptnVal = RSVPTE_ONE;

    UNUSED_PARAM (i4IfIndex);
    if ((RSVP_GBL_UDP_READ_SOCK_FD = socket (AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        return RPTE_FAILURE;
    }
    MEMSET ((INT1 *) &RsvpUdpLocalAddr, 0, sizeof (struct sockaddr_in));
    RsvpUdpLocalAddr.sin_family = AF_INET;
    RsvpUdpLocalAddr.sin_addr.s_addr = 0;
    RsvpUdpLocalAddr.sin_port = OSIX_HTONS (i4Port);

    /* Bind to Well-Known port */
    if (bind (RSVP_GBL_UDP_READ_SOCK_FD, (struct sockaddr *) &RsvpUdpLocalAddr,
              sizeof (struct sockaddr)) < 0)
    {

        RSVPTE_DBG (RSVPTE_IPIF_PRCS, "-E- : Udp Open Failed\n");
        RSVPTE_SOCK_CLOSE (RSVP_GBL_UDP_READ_SOCK_FD);
        return RPTE_FAILURE;
    }
    if (fcntl (RSVP_GBL_UDP_READ_SOCK_FD, F_SETFL, O_NONBLOCK) < 0)
    {
        RSVPTE_SOCK_CLOSE (RSVP_GBL_UDP_READ_SOCK_FD);
        RSVPTE_DBG (RSVPTE_IPIF_PRCS, "-E- :Setting NONBLOCK Failed\n");
        return RPTE_FAILURE;
    }

    /* This option is for receiving packet with Router alert option */
    /* Since, for FSIP SLI case IP Router alert option is not supported and
       will return failure, failure is not handled here. */
    if (setsockopt (RSVP_GBL_UDP_READ_SOCK_FD, IPPROTO_IP, IP_ROUTER_ALERT,
                    &u4OptnVal, sizeof (UINT4)) < 0)
    {
        RSVPTE_DBG (RSVPTE_IPIF_PRCS,
                    "setsockopt Failed for IP_ROUTER_ALERT\n");
    }

    if (SelAddFd (RSVP_GBL_UDP_READ_SOCK_FD, RsvpUdpPktInSocket) ==
        OSIX_FAILURE)
    {

        RSVPTE_DBG (RSVPTE_IPIF_PRCS, "-E- :SelAddFd Failed\n");
        return RPTE_FAILURE;
    }
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteUdpClose                                           */
/* Description     : This function closes a UDP port                        */
/* Input (s)       : i4Port - UDP Port Number                               */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteUdpClose (INT4 i4Port)
{

    UNUSED_PARAM (i4Port);
    SelRemoveFd (RSVP_GBL_UDP_READ_SOCK_FD);
    if (close (RSVP_GBL_UDP_READ_SOCK_FD) == RSVPTE_ZERO)
    {
        return;
    }

    RSVPTE_DBG (RSVPTE_IPIF_PRCS, "Failed in Closing Rsvp Port\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteSendMsgToUdp                                       */
/* Description     : This function fills the MsgParams and and enques the   */
/*                   message to UDP Task                                    */
/* Input (s)       : pMsgBuf - Pointer to buffer                            */
/*                   pPktMap - Pointer to PktMap                            */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS or RPTE_FAILURE                           */
/****************************************************************************/
UINT1
RpteSendMsgToUdp (tMsgBuf * pMsgBuf, tPktMap * pPktMap)
{
    struct sockaddr_in  DestInfo;
    UINT4               u4DestAddr = PKT_MAP_DST_ADDR (pPktMap);
    UINT1              *pu1Data = NULL;
    INT2                i2BufLen =
        (INT2)CRU_BUF_Get_ChainValidByteCount (pMsgBuf);
    MEMSET ((INT1 *) &DestInfo, 0, sizeof (struct sockaddr_in));
    DestInfo.sin_family = AF_INET;
    DestInfo.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    DestInfo.sin_port = OSIX_HTONS (UDP_READ_PORT);
    pu1Data = MemAllocMemBlk (RSVPTE_DATA_BUFFER_POOL_ID);
    if (pu1Data != NULL)
    {
        /*
         * A linear buffer is expected to be passed while writing to 
         *  socket. So copying the contents of pBuf to a linear buffer.
         */
        if ((BUF_COPY_FROM_CHAIN (pMsgBuf, pu1Data, 0,(UINT4)i2BufLen) == i2BufLen))
        {
            if (sendto (RSVP_GBL_UDP_READ_SOCK_FD, (INT1 *) pu1Data,
                        (UINT4)i2BufLen, 0, (struct sockaddr *) &DestInfo,
                        sizeof (struct sockaddr)) == i2BufLen)
            {
                MemReleaseMemBlock (RSVPTE_DATA_BUFFER_POOL_ID,
                                    (UINT1 *) pu1Data);
                BUF_RELEASE (pMsgBuf);
                RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteSendMsgToUdp : EXIT \n");
                return RPTE_SUCCESS;
            }
        }
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "IF: Failed to send UDP packet \n");
        MemReleaseMemBlock (RSVPTE_DATA_BUFFER_POOL_ID, (UINT1 *) pu1Data);
        BUF_RELEASE (pMsgBuf);
        return RPTE_FAILURE;
    }

    BUF_RELEASE (pMsgBuf);
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "IF: Failed to send UDP packet \n");
    return RPTE_FAILURE;
}

/*rsvp udp call back routine*/
/************************************************************************/
/*  Function Name   : RsvpUdpPktInSocket                               */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : RPTE_SUCCESS / RPTE_FAILURE                       */
/************************************************************************/

VOID
RsvpUdpPktInSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    if (RSVP_INITIALISED != TRUE)
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "IF: RSVP-TE module is in "
                    "shutdown state\n");
        return;
    }
    if (OsixEvtSend (RSVP_TSK_ID, UDP_PKT_RXED_EVENT) == OSIX_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "IF: Failed in RSVP UDP Call Back\n");
        return;
    }
}

/*rsvp Raw socket call back routine*/
/************************************************************************/
/*  Function Name   : RsvpRawPktInSocket                               */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : RPTE_SUCCESS / RPTE_FAILURE                       */
/************************************************************************/

VOID
RsvpRawPktInSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    if (RSVP_INITIALISED != TRUE)
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "IF: RSVP-TE module is in "
                    "shutdown state\n");
        return;
    }

    if (OsixEvtSend (RSVP_TSK_ID, IP_PKT_RXED_EVENT) == OSIX_FAILURE)

    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "IF: Failed in RSVP RAW Call Back\n");
        return;
    }
}

/************************************************************************
*  Function Name   : RsvpProcessRawIpMsg 
*  Description     : Receive and Process Raw Rsvp Packet
*  Input(s)        : None 
*  Output(s)       : None
*  Returns         : None
************************************************************************/
VOID
RsvpProcessRawIpMsg (VOID)
{
    struct sockaddr_in  PeerInfo;
    struct msghdr       PktInfo;
    tMsgBuf            *pMsgBuf = NULL;
    tCRU_INTERFACE      IfId;
    UINT4               u4IfIndex = 0;
    UINT4               u4MplsIfIndex = 0;
    INT4                i4BytesRcvd = 0;
    UINT1              *pu1Buf = NULL;
#ifdef BSDCOMP_SLI_WANTED
    struct cmsghdr     *pCmsgInfo;
    struct iovec        Iov;
    struct in_pktinfo  *pIpPktInfo;
    UINT1               au1Cmsg[24];
#else
    struct cmsghdr      CmsgInfo;
    tInPktinfo         *pIpPktInfo;
    UINT4               u4PeerLen = sizeof (PeerInfo);
#endif

    MEMSET ((INT1 *) &PeerInfo, 0, sizeof (struct sockaddr_in));
    MEMSET ((UINT1 *) &IfId, 0, sizeof (tCRU_INTERFACE));

    if (MsrGetRestorationStatus () == ISS_TRUE)
    {
        if (SelAddFd (RSVP_GBL_SOCK_FD, RsvpRawPktInSocket) == OSIX_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                        "RpteProcessTeGoingDownEvent : INTMD-EXIT \n");
            close (RSVP_GBL_SOCK_FD);
        }

        return;
    }

    pu1Buf = MemAllocMemBlk (RSVPTE_RAW_IP_DATA_BUFFER_POOL_ID);
    if (pu1Buf == NULL)
    {
        if (SelAddFd (RSVP_GBL_SOCK_FD, RsvpRawPktInSocket) == OSIX_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                        "RpteProcessTeGoingDownEvent : INTMD-EXIT \n");
            close (RSVP_GBL_SOCK_FD);
        }

        return;
    }
#ifdef BSDCOMP_SLI_WANTED
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    PktInfo.msg_name = (VOID *) &PeerInfo;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pu1Buf;
    Iov.iov_len = RPTE_DEF_PKT_SIZE;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_RAW;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
    while ((i4BytesRcvd = recvmsg (RSVP_GBL_SOCK_FD, &PktInfo, 0)) > 0)
    {
        pMsgBuf = BUF_ALLOC (RPTE_DEF_PKT_SIZE, 0);
        if (pMsgBuf != NULL)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsgBuf, pu1Buf, 0,(UINT4) i4BytesRcvd)
                == CRU_FAILURE)
            {
                BUF_RELEASE (pMsgBuf);
                continue;
            }
            pIpPktInfo = (struct in_pktinfo *) (VOID *)
                CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

            RPTE_SRC_ADDR (pMsgBuf) = OSIX_NTOHL (PeerInfo.sin_addr.s_addr);

            if (NetIpv4GetCfaIfIndexFromPort ((UINT4)pIpPktInfo->ipi_ifindex,
                                              &u4IfIndex) == NETIPV4_FAILURE)
            {
                BUF_RELEASE (pMsgBuf);
                continue;
            }
            /* Get the Mpls If index which is stacked over this L3 interface */
            if (CfaUtilGetMplsIfFromIfIndex (u4IfIndex,
                                             &u4MplsIfIndex,
                                             FALSE) == CFA_FAILURE)
            {
                BUF_RELEASE (pMsgBuf);
                continue;
            }
            IfId.u4IfIndex = u4MplsIfIndex;
            CRU_BUF_Set_InterfaceId (pMsgBuf, IfId);
            RpteRRProcessPkt (pMsgBuf, IP_ENCAP);
        }
        MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
        PktInfo.msg_name = (VOID *) &PeerInfo;
        PktInfo.msg_namelen = sizeof (struct sockaddr_in);
        Iov.iov_base = pu1Buf;
        Iov.iov_len = RPTE_DEF_PKT_SIZE;
        PktInfo.msg_iov = &Iov;
        PktInfo.msg_iovlen = 1;
        PktInfo.msg_control = (VOID *) au1Cmsg;
        PktInfo.msg_controllen = sizeof (au1Cmsg);

        pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
        pCmsgInfo->cmsg_level = SOL_RAW;
        pCmsgInfo->cmsg_type = IP_PKTINFO;
        pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
    }                            /* while end */
#else
    MEMSET ((INT1 *) &CmsgInfo, 0, sizeof (struct cmsghdr));
    PktInfo.msg_control = (VOID *) &CmsgInfo;
    while ((i4BytesRcvd = recvfrom (RSVP_GBL_SOCK_FD, pu1Buf,
                                    RPTE_DEF_PKT_SIZE, 0,
                                    (struct sockaddr *) &PeerInfo,
                                    &u4PeerLen)) > 0)
    {
        if (recvmsg (RSVP_GBL_SOCK_FD, &PktInfo, 0) < 0)
        {
            continue;
        }
        pIpPktInfo =
            (tInPktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
        pMsgBuf = BUF_ALLOC (i4BytesRcvd, RSVPTE_ZERO);
        if (pMsgBuf != NULL)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsgBuf, pu1Buf, RSVPTE_ZERO,
                                           i4BytesRcvd) == CRU_FAILURE)
            {
                BUF_RELEASE (pMsgBuf);
                continue;
            }
            RPTE_SRC_ADDR (pMsgBuf) = OSIX_NTOHL (PeerInfo.sin_addr.s_addr);

            if (NetIpv4GetCfaIfIndexFromPort (pIpPktInfo->ipi_ifindex,
                                              &u4IfIndex) == NETIPV4_FAILURE)
            {
                BUF_RELEASE (pMsgBuf);
                continue;
            }
            /* Get the Mpls If index which is stacked over this L3 interface */
            if (CfaUtilGetMplsIfFromIfIndex (u4IfIndex,
                                             &u4MplsIfIndex,
                                             TRUE) == CFA_FAILURE)
            {
                BUF_RELEASE (pMsgBuf);
                continue;
            }
            IfId.u4IfIndex = u4MplsIfIndex;
            CRU_BUF_Set_InterfaceId (pMsgBuf, IfId);
            RpteRRProcessPkt (pMsgBuf, IP_ENCAP);
        }
    }
#endif
    MemReleaseMemBlock (RSVPTE_RAW_IP_DATA_BUFFER_POOL_ID, (UINT1 *) pu1Buf);
    if (SelAddFd (RSVP_GBL_SOCK_FD, RsvpRawPktInSocket) == OSIX_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteProcessTeGoingDownEvent : EXIT \n");
        close (RSVP_GBL_SOCK_FD);
        return;
    }
}

/************************************************************************
*  Function Name   : RsvpProcessUdpIpMsg 
*  Description     : Receive and Process Udp Rsvp Packet
*  Input(s)        : None 
*  Output(s)       : None
*  Returns         : None
************************************************************************/
VOID
RsvpProcessUdpMsg (VOID)
{
    INT4                i4BytesRcvd = 0;
    UINT1              *pu1Buf = NULL;
    tIfEntry           *pIfEntry = NULL;
    struct sockaddr_in  PeerInfo;
    tMsgBuf            *pMsgBuf = NULL;
    UINT4               u4Addr;
    UINT4               u4OutGw;
    UINT4               u4SrcAddr = 0;
    tCRU_INTERFACE      IfId;
    UINT4               u4IfIndex = 0;
    UINT4               u4PeerLen = sizeof (PeerInfo);

    MEMSET ((INT1 *) &PeerInfo, 0, sizeof (struct sockaddr_in));
    MEMSET ((UINT1 *) &IfId, 0, sizeof (tCRU_INTERFACE));

    if (MsrGetRestorationStatus () == ISS_TRUE)
    {
        if (SelAddFd (RSVP_GBL_UDP_READ_SOCK_FD, RsvpUdpPktInSocket) ==
            OSIX_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                        "RpteProcessTeGoingDownEvent : INTMD-EXIT \n");
            close (RSVP_GBL_UDP_READ_SOCK_FD);
        }
        return;
    }

    pu1Buf = MemAllocMemBlk (RSVPTE_PRCS_UDP_DATA_POOL_ID);
    if (pu1Buf != NULL)
    {
        while ((i4BytesRcvd = recvfrom (RSVP_GBL_UDP_READ_SOCK_FD, pu1Buf,
                                        RPTE_DEF_PKT_SIZE, 0,
                                        (struct sockaddr *) &PeerInfo,
                                        &u4PeerLen)) > 0)
        {
            pMsgBuf = BUF_ALLOC ((UINT4)i4BytesRcvd, RSVPTE_ZERO);
            if (pMsgBuf != NULL)
            {
                if (CRU_BUF_Copy_OverBufChain (pMsgBuf, pu1Buf, RSVPTE_ZERO,
                                               (UINT4)i4BytesRcvd) != CRU_SUCCESS)
                {
                    BUF_RELEASE (pMsgBuf);
                    continue;
                }
                u4Addr = RPTE_SRC_ADDR (pMsgBuf) =
                    OSIX_NTOHL (PeerInfo.sin_addr.s_addr);
                if (RpteIpUcastRouteQuery (u4Addr, &pIfEntry, &u4OutGw) !=
                    RPTE_SUCCESS)
                {
                    BUF_RELEASE (pMsgBuf);
                    RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                                "Route Query failed in RsvpProcessUdpMsg : "
                                "INTMD-EXIT  \n");
                    continue;
                }
                if (NetIpv4GetSrcAddrToUseForDest (u4Addr, &u4SrcAddr) ==
                    NETIPV4_FAILURE)
                {
                    BUF_RELEASE (pMsgBuf);
                    RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                                "NetIpv4GetSrcAddrToUseForDest : "
                                "INTMD-EXIT  \n");
                    continue;
                }
                RPTE_DEST_ADDR (pMsgBuf) = u4SrcAddr;
                RpteIpGetIfId ((INT4)IF_ENTRY_IF_INDEX (pIfEntry), &IfId);
                u4IfIndex = IF_ENTRY_IF_INDEX (pIfEntry);
                IfId.u4IfIndex = u4IfIndex;
                CRU_BUF_Set_InterfaceId (pMsgBuf, IfId);
                RpteRRProcessPkt (pMsgBuf, UDP_ENCAP);
            }
        }
        MemReleaseMemBlock (RSVPTE_PRCS_UDP_DATA_POOL_ID, (UINT1 *) pu1Buf);
    }
    if (SelAddFd (RSVP_GBL_UDP_READ_SOCK_FD, RsvpUdpPktInSocket)
        == OSIX_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteProcessTeGoingDownEvent : EXIT \n");
        close (RSVP_GBL_UDP_READ_SOCK_FD);
        return;
    }
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptesock.c                             */
/*---------------------------------------------------------------------------*/
