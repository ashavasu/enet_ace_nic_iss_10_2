/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: rptepvm.c,v 1.60 2017/06/08 11:40:32 siva Exp $
 *
 * Description: This file contains functions for validating RSVP-TE packets
 *****************************************************************************/

#include "rpteincs.h"

PRIVATE INT1        RptePvmValidatePkt (tPktMap * pPktMap);
/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePvmSendPathErr
 * Description     : This function builds the PathErr message with
 *                   u1ErrCode and u2ErrValue
 * Input (s)       : *pPktMap - Pointer to Packet Map
 *                   u1ErrCode - Error Code 
 *                   u2ErrValue - Error Value
 * Output (s)      : None 
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePvmSendPathErr (tPktMap * pPktMap, UINT1 u1ErrCode, UINT2 u2ErrValue)
{
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmSendPathErr : ENTRY \n");

    if ((u1ErrCode == RPTE_ZERO) && (u2ErrValue == RPTE_ZERO))
    {
        return;
    }

    RptePvmFillSrcAndDestAddr (pPktMap);

    PKT_MAP_OUT_IF_ENTRY (pPktMap) = PKT_MAP_IF_ENTRY (pPktMap);

    PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

    /* Fill Error Spec Object and Send Path Error Message */
    RptePvmFormAndSendPEOrREMsg (pPktMap, u1ErrCode, u2ErrValue, PATHERR_MSG);

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmSendPathErr : EXIT \n");

    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePvmUtlSendPathErr
 * Description     : This function builds the PathErr message with
 *                   u1ErrCode and u2ErrValue. It doesn't allocate any timer
 *                   blocks and doesn't start any timers for path errors since
 *                   it is ADMISSION_CTRL_FAILURE
 * Input (s)       : *pPktMap - Pointer to Packet Map
 *                   u1ErrCode - Error Code 
 *                   u2ErrValue - Error Value
 * Output (s)      : None 
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RptePvmUtlSendPathErr (tPktMap * pPktMap, UINT1 u1ErrCode, UINT2 u2ErrValue)
{
    UINT1              *pu1Pdu = NULL;
    UINT1              *pOrgRsvpPkt = NULL;
    INT4                i4PathErrSize = 0;
    tObjHdr            *pObjHdr = NULL;
    tMsgBuf            *pOrgMsgBuf = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRpteKey            RpteKey;
    UINT4               u4PathErrMsgId;
    UINT4               u4Val;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmUtlSendPathErr : ENTRY \n");

    RptePvmFillSrcAndDestAddr (pPktMap);

    PKT_MAP_OUT_IF_ENTRY (pPktMap) = PKT_MAP_IF_ENTRY (pPktMap);
    PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

    /* No need to send the Message. Just Fill Error Spec */
    RptePvmFormAndSendPEOrREMsg (pPktMap, u1ErrCode, u2ErrValue, RPTE_ZERO);

    /* Build the Path Error Message */
    pRsvpTeTnlInfo = RPTE_PKT_MAP_TNL_INFO (pPktMap);

    if ((gu4RMDFlags & RPTE_RMD_PATH_ERR_MSG) &&
        (NBR_ENTRY_RMD_CAPABLE (RPTE_PKT_MAP_NBR_ENTRY (pPktMap))
         == RPTE_ENABLED))
    {
        /* Msg Id is generated for the outgoing path error
         * message */
        RpteMIHGenerateMsgId (&u4PathErrMsgId);

        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;
    }
    else
    {
        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
    }

    if (pRsvpTeTnlInfo != NULL)
    {
        /*  Need not maintain any timer(s) and their info */
        RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
    }

    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) = gu4Epoch;
        RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_MSG_ID_ACK_DESIRED;
        RPTE_PKT_MAP_MSG_ID (pPktMap) = u4PathErrMsgId;
        i4PathErrSize += RPTE_MSG_ID_OBJ_LEN;
    }

    /* Since a path error is sent to the upstream, the msg id
     * corresponding the previous path message is deleted from the
     * neighbour msg id data base. */
    if ((pRsvpTeTnlInfo != NULL) &&
        ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
          RPTE_IN_PATH_MSG_ID_PRESENT) == RPTE_IN_PATH_MSG_ID_PRESENT))
    {
        RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (RPTE_TNL_USTR_NBR
                                                    (pRsvpTeTnlInfo));
        RpteKey.u8_Key[RPTE_ONE] = RPTE_TNL_IN_PATH_MSG_ID (pRsvpTeTnlInfo);
        RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_NBR_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
    }

    if (pPktMap->pSenderTspecObj != NULL)
    {
        i4PathErrSize =
            (INT4) (i4PathErrSize + sizeof (tRsvpHdr) + RPTE_IPV4_SSN_OBJ_LEN +
                    RPTE_IPV4_SNDR_TMP_OBJ_LEN + sizeof (tSenderTspecObj));
    }
    else
    {
        i4PathErrSize =
            (INT4) (i4PathErrSize + sizeof (tRsvpHdr) + RPTE_IPV4_SSN_OBJ_LEN +
                    RPTE_IPV4_SNDR_TMP_OBJ_LEN);
    }

    i4PathErrSize += RpteUtlCalculateErrorSpecSize (pPktMap);

    pOrgMsgBuf = NULL;
    if (PKT_MAP_RSVP_PKT_ALLOC (pPktMap) == RPTE_PKTMAP_IP_ALLOC)
    {
        pOrgRsvpPkt = PKT_MAP_IP_PKT (pPktMap);
    }

    pOrgMsgBuf = PKT_MAP_MSG_BUF (pPktMap);
    PKT_MAP_MSG_BUF (pPktMap) = NULL;
    PKT_MAP_RA_OPT_NEEDED (pPktMap) = RPTE_NO;
    PKT_MAP_RSVP_PKT (pPktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);
    PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    if (PKT_MAP_RSVP_PKT (pPktMap) == NULL)
    {
        if (pOrgRsvpPkt != NULL)
        {
            MemReleaseMemBlock (RSVPTE_IP_PKT_POOL_ID, (UINT1 *) pOrgRsvpPkt);
            PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_NO_ALLOC;
        }
        if (pOrgMsgBuf != NULL)
        {
            BUF_RELEASE (pOrgMsgBuf);
        }
        PKT_MAP_ERROR_SPEC_OBJ (pPktMap) = NULL;
        pPktMap->pIfIdRsvpErrObj = NULL;
        pPktMap->pIfIdRsvpNumErrObj = NULL;
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmUtlSendPathErr : INTD EXIT \n");
        return;
    }

    /* Copy RsvpHdr */
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    MEMSET (pRsvpHdr, RSVPTE_ZERO, sizeof (tRsvpHdr));
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS ((UINT2) i4PathErrSize);
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = PATHERR_MSG;
    RSVP_HDR_SEND_TTL (pRsvpHdr) = IF_ENTRY_TTL (PKT_MAP_IF_ENTRY (pPktMap));
    pObjHdr = (tObjHdr *) (VOID *) ((UINT1 *) pRsvpHdr + sizeof (tRsvpHdr));

    /* Msg Id Obj update, if present */
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        /* Filling the Header */
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_MSG_ID_OBJ_LEN);
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_MESSAGE_ID_CLASS_NUM_TYPE);

        pu1Pdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

        /* Filling the Epoch and Flags fields */
        u4Val = (UINT4) (RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) |
                         (UINT4) (RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) <<
                                  RPTE_3_BYTE_SHIFT));
        u4Val = OSIX_HTONL (u4Val);
        MEMCPY ((UINT1 *) pu1Pdu, (UINT1 *) &u4Val, RPTE_FLAG_EPOCH_LEN);

        pu1Pdu += RPTE_FLAG_EPOCH_LEN;

        /* Filling the Message Identifier field */
        u4Val = OSIX_HTONL (RPTE_PKT_MAP_MSG_ID (pPktMap));
        MEMCPY ((UINT1 *) pu1Pdu, (UINT1 *) &u4Val, RPTE_MSG_ID_LEN);
        pu1Pdu += RPTE_MSG_ID_LEN;

        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* Copy Session */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pu1Pdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

    MEMCPY ((UINT1 *) pu1Pdu,
            (UINT1 *) &PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap),
            RSVPTE_IPV4ADR_LEN);

    pu1Pdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pu1Pdu, PKT_MAP_RPTE_SSN_RSVD (pPktMap));
    RPTE_PUT_2_BYTES (pu1Pdu, PKT_MAP_RPTE_SSN_TNL_ID (pPktMap));
    MEMCPY ((UINT1 *) pu1Pdu,
            (UINT1 *) &PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap),
            RSVPTE_IPV4ADR_LEN);

    pu1Pdu += RSVPTE_IPV4ADR_LEN;

    /* Copy ErrorSpec */
    RptePvmFillErrorSpecObj (&pu1Pdu, pPktMap);

    /* Copy SenderTemplate */
    RPTE_PUT_2_BYTES (pu1Pdu, OSIX_HTONS (RPTE_IPV4_SNDR_TMP_OBJ_LEN));
    RPTE_PUT_2_BYTES (pu1Pdu, OSIX_HTONS (IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE));
    MEMCPY ((UINT1 *) pu1Pdu,
            (UINT1 *) &PKT_MAP_RPTE_ST_TNL_SNDR_ADDR (pPktMap),
            RSVPTE_IPV4ADR_LEN);

    pu1Pdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pu1Pdu, PKT_MAP_RPTE_SNDR_TMP_RSVD (pPktMap));
    RPTE_PUT_2_BYTES (pu1Pdu, PKT_MAP_RPTE_SNDR_TMP_LSPID (pPktMap));

    /* Copy SenderTspec */

    if (pPktMap->pSenderTspecObj != NULL)
    {
        *((tSenderTspecObj *) (VOID *) pu1Pdu) =
            *PKT_MAP_SENDER_TSPEC_OBJ (pPktMap);
        pu1Pdu += sizeof (tSenderTspecObj);
    }

    PKT_MAP_OPT_LEN (pPktMap) = 0;

    RptePbSendMsg (pPktMap);
    PKT_MAP_ERROR_SPEC_OBJ (pPktMap) = NULL;
    pPktMap->pIfIdRsvpErrObj = NULL;
    pPktMap->pIfIdRsvpNumErrObj = NULL;

    /* Free the allocated memory */
    if (pOrgRsvpPkt != NULL)
    {
        MemReleaseMemBlock (RSVPTE_IP_PKT_POOL_ID, (UINT1 *) pOrgRsvpPkt);
    }

    if (pOrgMsgBuf != NULL)
    {
        BUF_RELEASE (pOrgMsgBuf);
    }

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmUtlSendPathErr : EXIT \n");

    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePvmConstructAndSndPEMsg
 * Description     : This function builds the PathErr message with
 *                   u1ErrCode and u2ErrValue
 * Input (s)       : *pPktMap - Pointer to Packet Map
 *                   u1ErrCode - Error Code 
 *                   u2ErrValue - Error Value
 * Output (s)      : None 
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePvmConstructAndSndPEMsg (tPktMap * pPktMap)
{
    UINT1              *pu1Pdu = NULL;
    UINT1              *pOrgRsvpPkt = NULL;
    INT4                i4PathErrSize = 0;
    tObjHdr            *pObjHdr = NULL;
    tMsgBuf            *pOrgMsgBuf = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tTimerBlock        *pTimerBlock = NULL;
    tRpteKey            RpteKey;
    tuTrieInfo         *pTrieInfo = NULL;
    UINT4               u4PathErrMsgId;
    UINT4               u4Val;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmConstructAndSndPEMsg : EXIT \n");

    pRsvpTeTnlInfo = RPTE_PKT_MAP_TNL_INFO (pPktMap);
    if ((pRsvpTeTnlInfo != NULL) && (pRsvpTeTnlInfo->pPsb != NULL))
    {
        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;

        if ((gu4RMDFlags & RPTE_RMD_PATH_ERR_MSG) &&
            (NBR_ENTRY_RMD_CAPABLE (RPTE_PKT_MAP_NBR_ENTRY (pPktMap))
             == RPTE_ENABLED))
        {
            if (RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo) == NULL)
            {
                /* Allocate memory if timer block is not present */
                pTimerBlock = (tTimerBlock *)
                    RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TIMER_BLK_POOL_ID);

                if (pTimerBlock == NULL)
                {
                    /* If the allocation of the Timer Block fails
                     * the message will be sent without Msg Id */
                    RSVPTE_DBG (RSVPTE_PVM_MEM,
                                "Failed to allocate timer block. "
                                "Sending Path Error msg without msg id.\n");
                    pTimerBlock = NULL;
                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                }
                else
                {
                    RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo) = pTimerBlock;
                    /* Back Off Timer value is set to the initial value */
                    TIMER_BLK_BACKOFF_INTERVAL (pTimerBlock) =
                        (IF_ENTRY_REFRESH_INTERVAL (PKT_MAP_OUT_IF_ENTRY
                                                    (pPktMap)) /
                         RPTE_BACK_OFF_FACTOR) / DEFAULT_CONV_TO_SECONDS;

                    /* Msg Id is generated for the outgoing path error
                     * message */
                    RpteMIHGenerateMsgId (&u4PathErrMsgId);

                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) =
                        RPTE_NONZERO;

                    /* Since RMD is enabled for Path Err, back off timer is
                     * started for the same. */
                    TIMER_PARAM_RPTE_TNL ((&TIMER_BLK_TIMER_PARAM
                                           (pTimerBlock))) = pRsvpTeTnlInfo;
                    TIMER_BLK_MSG_ID (pTimerBlock) = u4PathErrMsgId;
                    TIMER_PARAM_ID ((&TIMER_BLK_TIMER_PARAM (pTimerBlock)))
                        = RPTE_PATHERR;
                    if (pPktMap->pErrorSpecObj != NULL)
                    {
                        MEMCPY ((UINT1 *) &TIMER_BLK_ERROR_SPEC (pTimerBlock),
                                (UINT1 *)
                                &ERROR_SPEC_OBJ (PKT_MAP_ERROR_SPEC_OBJ
                                                 (pPktMap)),
                                sizeof (tErrorSpec));
                    }
                    else if (pPktMap->pIfIdRsvpErrObj != NULL)
                    {
                        MEMCPY ((UINT1 *) &TIMER_BLK_ERROR_SPEC (pTimerBlock),
                                (UINT1 *)
                                &pPktMap->pIfIdRsvpErrObj->ErrorSpec,
                                sizeof (tErrorSpec));
                    }
                    else if (pPktMap->pIfIdRsvpNumErrObj != NULL)
                    {
                        MEMCPY ((UINT1 *) &TIMER_BLK_ERROR_SPEC (pTimerBlock),
                                (UINT1 *)
                                &pPktMap->pIfIdRsvpNumErrObj->ErrorSpec,
                                sizeof (tErrorSpec));
                    }
                    RSVP_TIMER_NAME ((&TIMER_BLK_APP_TIMER (pTimerBlock))) =
                        (FS_ULONG) & TIMER_BLK_TIMER_PARAM (pTimerBlock);

                    pTrieInfo = (tuTrieInfo *)
                        RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TRIE_INFO_POOL_ID);

                    if (pTrieInfo == NULL)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_MEM,
                                    "Failed to allocate Trie Info "
                                    "Sending Path Err msg without msg id.\n");
                        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) =
                            RPTE_ZERO;
                        MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                            (UINT1 *) pTimerBlock);
                        pTimerBlock = NULL;
                        pTrieInfo = NULL;
                    }
                    else
                    {
                        MEMSET (pTrieInfo, RSVPTE_ZERO, sizeof (tuTrieInfo));
                        RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo) = RPTE_PATHERR;
                        RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = pTimerBlock;

                        RpteKey.u4_Key = u4PathErrMsgId;

                        /* RBTree Addtion is only required for out path message id for
                         * SRefresh RecoveryPath message support.
                         * Its not required for Path error message id
                         * */

                        if (RpteTrieAddEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                              &RpteKey,
                                              pTrieInfo) != RPTE_SUCCESS)
                        {
                            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                        "PVM : Trie Addition Failed. "
                                        "- in Path Err\n");
                            RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap)
                                = RPTE_ZERO;
                            MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                                (UINT1 *) pTrieInfo);
                            RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = NULL;
                            MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                                (UINT1 *) pTimerBlock);
                            pTimerBlock = NULL;
                            pTrieInfo = NULL;
                        }
                    }
                }
            }
            else
            {
                pTimerBlock = RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo);

                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;
                u4PathErrMsgId = TIMER_BLK_MSG_ID (pTimerBlock);

                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                if (RpteTrieLookupEntry
                    (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                     &pTrieInfo) != RPTE_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Look Up Failed - in Path Err\n"
                                "ATTENTION : TimerBlock Present, but msg id "
                                "not present in Trie\n");
                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                    MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                        (UINT1 *) pTimerBlock);
                }
            }

            if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
            {
                if (RpteUtlStartTimer
                    (RSVP_GBL_TIMER_LIST,
                     &TIMER_BLK_APP_TIMER (pTimerBlock),
                     TIMER_BLK_BACKOFF_INTERVAL (pTimerBlock) *
                     SYS_NUM_OF_TIME_UNITS_IN_A_SEC) != TMR_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "PVM : Start Timer failed. - in Path Err "
                                "Sending Path Err without msg id\n");
                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                    MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                        (UINT1 *) pTimerBlock);
                    RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = NULL;
                    MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                        (UINT1 *) pTrieInfo);
                    pTrieInfo = NULL;
                    pTimerBlock = NULL;
                }
            }

        }
        else
        {
            /* In the case of PE Timer Block already present,
             * Stop the timer, Delete the Msg Id already present in
             * the Data base and continue. */
            pTimerBlock = RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo);

            if (pTimerBlock != NULL)
            {
                TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                              (pTimerBlock));
                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);

                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                pTimerBlock = NULL;
                pTrieInfo = NULL;
            }
            RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
        }
    }
    else
    {
        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
    }

    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) == RPTE_ZERO)
    {
        if (pRsvpTeTnlInfo != NULL)
        {
            RPTE_TNL_PE_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
        }
    }
    else
    {
        RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) = gu4Epoch;
        RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_MSG_ID_ACK_DESIRED;
        RPTE_PKT_MAP_MSG_ID (pPktMap) = u4PathErrMsgId;
        i4PathErrSize += RPTE_MSG_ID_OBJ_LEN;
    }

    /* Since a path error is sent to the upstream, the msg id
     * corresponding the previous path message is deleted from the
     * neighbour msg id data base. */
    if (pRsvpTeTnlInfo != NULL)
    {
        if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
             RPTE_IN_PATH_MSG_ID_PRESENT) == RPTE_IN_PATH_MSG_ID_PRESENT)
        {
            RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (RPTE_TNL_USTR_NBR
                                                        (pRsvpTeTnlInfo));
            RpteKey.u8_Key[RPTE_ONE] = RPTE_TNL_IN_PATH_MSG_ID (pRsvpTeTnlInfo);
            RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                                 RPTE_NBR_MSG_ID_DB_PREFIX,
                                 RpteMIHReleaseTrieInfo);
        }
    }

    if (pPktMap->pSenderTspecObj != NULL)
    {
        i4PathErrSize =
            (INT4) (i4PathErrSize + sizeof (tRsvpHdr) + RPTE_IPV4_SSN_OBJ_LEN +
                    RPTE_IPV4_SNDR_TMP_OBJ_LEN + sizeof (tSenderTspecObj));
    }
    else
    {
        i4PathErrSize =
            (INT4) (i4PathErrSize + sizeof (tRsvpHdr) + RPTE_IPV4_SSN_OBJ_LEN +
                    RPTE_IPV4_SNDR_TMP_OBJ_LEN);
    }

    i4PathErrSize += RpteUtlCalculateErrorSpecSize (pPktMap);

    pOrgMsgBuf = NULL;
    if (PKT_MAP_RSVP_PKT_ALLOC (pPktMap) == RPTE_PKTMAP_IP_ALLOC)
    {
        pOrgRsvpPkt = PKT_MAP_IP_PKT (pPktMap);
    }

    pOrgMsgBuf = PKT_MAP_MSG_BUF (pPktMap);
    PKT_MAP_MSG_BUF (pPktMap) = NULL;
    PKT_MAP_RA_OPT_NEEDED (pPktMap) = RPTE_NO;
    PKT_MAP_RSVP_PKT (pPktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);
    PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    if (PKT_MAP_RSVP_PKT (pPktMap) == NULL)
    {
        if (pOrgRsvpPkt != NULL)
        {
            MemReleaseMemBlock (RSVPTE_IP_PKT_POOL_ID, (UINT1 *) pOrgRsvpPkt);
            PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_NO_ALLOC;
        }
        if (pOrgMsgBuf != NULL)
        {
            BUF_RELEASE (pOrgMsgBuf);
        }
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmConstructAndSndPEMsg : EXIT \n");
        return;
    }

    /* Copy RsvpHdr */
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    MEMSET (pRsvpHdr, RSVPTE_ZERO, sizeof (tRsvpHdr));
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS ((UINT2) i4PathErrSize);
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = PATHERR_MSG;
    RSVP_HDR_SEND_TTL (pRsvpHdr) = IF_ENTRY_TTL (PKT_MAP_IF_ENTRY (pPktMap));
    pObjHdr = (tObjHdr *) (VOID *) ((UINT1 *) pRsvpHdr + sizeof (tRsvpHdr));

    /* Msg Id Obj update, if present */
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        /* Filling the Header */
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_MSG_ID_OBJ_LEN);
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_MESSAGE_ID_CLASS_NUM_TYPE);

        pu1Pdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

        /* Filling the Epoch and Flags fields */
        u4Val = (UINT4) (RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) |
                         (UINT4) (RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) <<
                                  RPTE_3_BYTE_SHIFT));
        u4Val = OSIX_HTONL (u4Val);
        MEMCPY ((UINT1 *) pu1Pdu, (UINT1 *) &u4Val, RPTE_FLAG_EPOCH_LEN);

        pu1Pdu += RPTE_FLAG_EPOCH_LEN;

        /* Filling the Message Identifier field */
        u4Val = OSIX_HTONL (RPTE_PKT_MAP_MSG_ID (pPktMap));
        MEMCPY ((UINT1 *) pu1Pdu, (UINT1 *) &u4Val, RPTE_MSG_ID_LEN);
        pu1Pdu += RPTE_MSG_ID_LEN;

        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* Copy Session */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pu1Pdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

    MEMCPY ((UINT1 *) pu1Pdu,
            (UINT1 *) &PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap),
            RSVPTE_IPV4ADR_LEN);

    pu1Pdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pu1Pdu, PKT_MAP_RPTE_SSN_RSVD (pPktMap));

    RPTE_PUT_2_BYTES (pu1Pdu, PKT_MAP_RPTE_SSN_TNL_ID (pPktMap));

    MEMCPY ((UINT1 *) pu1Pdu,
            (UINT1 *) &PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap),
            RSVPTE_IPV4ADR_LEN);

    pu1Pdu += RSVPTE_IPV4ADR_LEN;

    /* Copy ErrorSpec */

    RptePvmFillErrorSpecObj (&pu1Pdu, pPktMap);

    /* Copy SenderTemplate */
    RPTE_PUT_2_BYTES (pu1Pdu, OSIX_HTONS (RPTE_IPV4_SNDR_TMP_OBJ_LEN));

    RPTE_PUT_2_BYTES (pu1Pdu, OSIX_HTONS (IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE));

    MEMCPY ((UINT1 *) pu1Pdu,
            (UINT1 *) &PKT_MAP_RPTE_ST_TNL_SNDR_ADDR (pPktMap),
            RSVPTE_IPV4ADR_LEN);

    pu1Pdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pu1Pdu, PKT_MAP_RPTE_SNDR_TMP_RSVD (pPktMap));

    RPTE_PUT_2_BYTES (pu1Pdu, PKT_MAP_RPTE_SNDR_TMP_LSPID (pPktMap));

    /* Copy SenderTspec */

    if (pPktMap->pSenderTspecObj != NULL)
    {
        *((tSenderTspecObj *) (VOID *) pu1Pdu) =
            *PKT_MAP_SENDER_TSPEC_OBJ (pPktMap);
        pu1Pdu += sizeof (tSenderTspecObj);
    }

    PKT_MAP_OPT_LEN (pPktMap) = 0;

    RptePbSendMsg (pPktMap);

    /* Free the allocated memory */
    if (pOrgRsvpPkt != NULL)
    {
        MemReleaseMemBlock (RSVPTE_IP_PKT_POOL_ID, (UINT1 *) pOrgRsvpPkt);
    }

    if (pOrgMsgBuf != NULL)
    {
        BUF_RELEASE (pOrgMsgBuf);
    }
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmConstructAndSndPEMsg : EXIT \n");

    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteValidatePathMsgObjs
 * Description     : This function validates Path Message objects.
 * Input (s)       : pPktMap - Pointer to PktMap
 * Output (s)      : None
 * Returns         : If successful returns RPTE_SUCCESS or else RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteValidatePathMsgObjs (tPktMap * pPktMap)
{
    UINT4               u4DestAddr = RSVPTE_ZERO;
    tIfEntry           *pIfEntry = NULL;
    tLblReqAtmRngObj   *pLblReqAtmRngObj = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RpteValidatePathMsgObjs : ENTRY \n");

    pIfEntry = PKT_MAP_IF_ENTRY (pPktMap);
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    CONVERT_TO_INTEGER ((PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap)), u4DestAddr);
    u4DestAddr = OSIX_NTOHL (u4DestAddr);

    /* Validate Objects present in the Path message, if the
     * object founds to be invalid drop the pkt.
     * */
    if ((pPktMap->u1EroErrType == RPTE_BAD_EXPLICT_ROUTE_OBJ) ||
        (pPktMap->u1EroErrType == RPTE_BAD_INIT_SUB_OBJ))
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH :Invalid ERO received in PATH message - Sending PATH ERROR\n");
        RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB, pPktMap->u1EroErrType);
        return RPTE_FAILURE;
    }

    /* Check for L3PID in case of ATM Iface, if fails send path err */
    if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM)
    {
        /* Check for LBL intersection, if fails send path err */
        if (pPktMap->pLblReqAtmRngObj != NULL)
        {
            pLblReqAtmRngObj = pPktMap->pLblReqAtmRngObj;
            /* NOTE: Assume L3Pid can be zero or STD_IPV4_ETHERTYPE value */
            switch (OSIX_NTOHS (pPktMap->pLblReqAtmRngObj->GenLblReq.u2GPid))
            {
                case RPTE_STD_IPV4_ETHERTYPE:
                    break;
                default:
                    RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                        RPTE_UNSUPPORTED_L3PID);
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "PVM : L3Pid is different - path err sent "
                                "- Route problem \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RpteValidatePathMsgObjs : INTMD-EXIT \n");
                    return RPTE_FAILURE;
            }
            /* Checking for perfect label Intersection */
            /* Since the merge support flag is placed in the reserved field
             * of VPI values, the comparision is done after masking the values
             */
            if (!((RPTE_IF_ENTRY_MIN_VPI (pIfEntry) ==
                   (OSIX_NTOHS (RPTE_LBL_REQ_ATM_LBL_LBVPI (pLblReqAtmRngObj))
                    & ATM_MERGE_SUPPRT_CMP_MASK)) &&
                  ((RPTE_IF_ENTRY_MIN_VCI (pIfEntry)) ==
                   OSIX_NTOHS (RPTE_LBL_REQ_ATM_LBL_LBVCI (pLblReqAtmRngObj)))
                  && ((RPTE_IF_ENTRY_MAX_VPI (pIfEntry)) ==
                      (OSIX_NTOHS
                       (RPTE_LBL_REQ_ATM_LBL_UBVPI (pLblReqAtmRngObj)) &
                       ATM_MERGE_SUPPRT_CMP_MASK))
                  && ((RPTE_IF_ENTRY_MAX_VCI (pIfEntry)) ==
                      OSIX_NTOHS (RPTE_LBL_REQ_ATM_LBL_UBVCI
                                  (pLblReqAtmRngObj)))))
            {
                RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                    RPTE_LBL_ALLOC_FAIL);
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "PVM : Lbl Intersection failed - path err sent - "
                            "Route problem \n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RpteValidatePathMsgObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
        }
    }
    else if (gi4MplsSimulateFailure != RPTE_SIM_FAILURE_NO_LBL_REQ)
    {
        if (pPktMap->pGenLblReqObj == NULL)
        {
            RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                RPTE_UNSUPPORTED_L3PID);
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "PVM : Label req obj is NULL - path err sent - "
                        "Route problem \n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RpteValidatePathMsgObjs : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        /* As per RFC 3473 section 2.1.1 , For GMPLS tunnel, 
         * GPID should be validated in egress node only.*/
        if (((pPktMap->pGenLblReqObj->GenLblReq.u1EncType ==
              RSVPTE_ZERO) || (RPTE_IS_DEST_OUR_ADDR
                               (u4DestAddr) == RPTE_SUCCESS)))
        {
            if ((OSIX_NTOHS (pPktMap->pGenLblReqObj->ObjHdr.u2ClassNumType)) ==
                RPTE_GLBL_REQ_CLASS_NUM_TYPE)
            {
                /*currently Ether type payload is only supported */
                /*TODO verify if RPTE_STD_IPV4_ETHERTYPE is valid for RPTE_GLBL_REQ_CLASS_NUM_TYPE */
                switch (OSIX_NTOHS (pPktMap->pGenLblReqObj->GenLblReq.u2GPid))
                {
                    case RSVPTE_ZERO:
                        break;
                    case RPTE_STD_IPV4_ETHERTYPE:
                        break;
                    case RPTE_GMPLS_ETHERNET_ETHERTYPE:
                        break;
                    default:
                        RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                            RPTE_UNSUPPORTED_L3PID);
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "PVM : L3Pid is different - path err sent - "
                                    "Route problem \n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RpteValidatePathMsgObjs : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                }
            }
        }
        /* As per RFC 3209, For MPLS tunnel, it should be 
         * validated in all nodes*/
        if ((OSIX_NTOHS (pPktMap->pGenLblReqObj->ObjHdr.u2ClassNumType)) ==
            RPTE_LBL_REQ_CLASS_NUM_TYPE)
        {
            switch (OSIX_NTOHS (pPktMap->pGenLblReqObj->GenLblReq.u2GPid))
            {
                case RPTE_STD_IPV4_ETHERTYPE:
                    break;
                case RPTE_GMPLS_ETHERNET_ETHERTYPE:
                    break;
                default:
                    RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                        RPTE_UNSUPPORTED_L3PID);
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "PVM : L3Pid is different - path err sent - "
                                "Route problem \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RpteValidatePathMsgObjs : INTMD-EXIT \n");
                    return RPTE_FAILURE;
            }
        }
    }

    if (RpteValidateDSPathMsgObjs (pPktMap) == RPTE_FAILURE)
    {
        return RPTE_FAILURE;
    }

    /* PATH Message should not have both FAST_REROUTE and DETOUR Object at
     * the same time */
    if ((PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap) != NULL) &&
        (FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap)).u2Length !=
         RPTE_ZERO))
    {
        /*Sending PATH Error with proper Error Code and
         * Error Value in Error Spec Object should be handled properly */
        RptePvmSendPathErr (pPktMap, UNKNOWN_OBJECT_CLASS, DEFAULT_ERROR_VALUE);
        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                    "PVM : Unsupported Format in PATH Message"
                    " - PATH ERR Sent");
        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                    "RpteValidatePathMsgObjs : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* When a MP receives a backup LSP's PATH Message through bypass tunnel,
     * the Send_TTL in the common header may not match the TTL of the IP Packet 
     * within the PATH Message was transported. This is expected behaviour and 
     * should be handled. */
    /* Non Rsvp Router Stands in the Path */
    if ((PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap) != NULL) &&
        (PSB_RPTE_FRR_FAST_REROUTE (PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap)).
         u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD))
    {
        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RpteValidatePathMsgObjs : EXIT \n");
        return RPTE_SUCCESS;
    }
    if (RSVP_HDR_SEND_TTL (pRsvpHdr) != PKT_MAP_RX_TTL (pPktMap))
    {
        RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB, RPTE_NON_RSVP_RTR);
        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                    "PVM : Non Rsvp Router - path err sent - Route "
                    "problem \n");
        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                    "RpteValidatePathMsgObjs : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pPktMap->pNbrEntry->bIsHelloSynchronoziedAfterRestart != RPTE_TRUE) &&
        (pPktMap->pRecoveryLblObj != NULL))
    {
        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                    "Hello Resynchronization is not happened after restart \n");
        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                    "RpteValidatePathMsgObjs : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RpteValidatePathMsgObjs : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePvmSendResvErr
 * Description     : This function builds the ResvErr message with
 *                   u1ErrCode and u2ErrValue
 * Input (s)       : *pPktMap - Pointer to Packet Map
 *                   u1ErrCode - Error Code
 *                   u2ErrValue - Error Value
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePvmSendResvErr (tPktMap * pPktMap, UINT1 u1ErrCode, UINT2 u2ErrValue)
{
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmSendResvErr : ENTRY \n");

    RptePvmFillSrcAndDestAddr (pPktMap);

    PKT_MAP_OUT_IF_ENTRY (pPktMap) = PKT_MAP_IF_ENTRY (pPktMap);

    PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

    /* Fill Error Spec Object and Send Resv Error Message */
    RptePvmFormAndSendPEOrREMsg (pPktMap, u1ErrCode, u2ErrValue, RESVERR_MSG);

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmSendResvErr : EXIT \n");

    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePvmConstructAndSndREMsg
 * Description     : This function builds the ResvErr message with
 *                   u1ErrCode and u2ErrValue
 * Input (s)       : *pPktMap - Pointer to Packet Map
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePvmConstructAndSndREMsg (tPktMap * pPktMap)
{
    UINT1              *pu1PduPtr = NULL;
    UINT1              *pOrgRsvpPkt = NULL;
    INT4                i4ResvErrSize = 0;
    UINT2               u2FSpecObjLength = 0;
    tObjHdr            *pObjHdr = NULL;
    tObjHdr            *pTmpObjHdr = NULL;
    tMsgBuf            *pOrgMsgBuf = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tRsvpHopObj        *pRsvpHopObj = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tTimerBlock        *pTimerBlock = NULL;
    tRpteKey            RpteKey;
    tuTrieInfo         *pTrieInfo = NULL;
    UINT4               u4ResvErrMsgId = 0;
    UINT4               u4Val;
    BOOL1               b1DnStrOob = FALSE;
    UINT4               u4DnStrDataTeLinkIfId = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmConstructAndSndREMsg : ENTRY \n");

    pRsvpTeTnlInfo = RPTE_PKT_MAP_TNL_INFO (pPktMap);
    if ((pRsvpTeTnlInfo != NULL)
        && (RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE))
    {
        if (((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_IN_USE) ==
             LOCAL_PROT_IN_USE)
            &&
            ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
              u1Flags) == RSVPTE_TNL_FRR_FACILITY_METHOD)
            && (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) ==
                RPTE_FRR_PROTECTED_TNL)
            && (RPTE_IS_PLR_OR_PLRAWAIT (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo))))
        {
            pRsvpTeTnlInfo->u1SendToByPassTnl = RPTE_TRUE;
        }
    }
    if ((pRsvpTeTnlInfo != NULL) && (pRsvpTeTnlInfo->pRsb != NULL))
    {
        if ((gu4RMDFlags & RPTE_RMD_RESV_ERR_MSG) &&
            (NBR_ENTRY_RMD_CAPABLE (RPTE_PKT_MAP_NBR_ENTRY (pPktMap))
             == RPTE_ENABLED))
        {
            if (RPTE_TNL_RE_TIMER_BLOCK (pRsvpTeTnlInfo) == NULL)
            {
                /* Allocate memory if timer block is not present */
                pTimerBlock = (tTimerBlock *)
                    RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TIMER_BLK_POOL_ID);
                if (pTimerBlock == NULL)
                {
                    /* If the allocation of the Timer Block fails
                     * the message will be sent without Msg Id */
                    RSVPTE_DBG (RSVPTE_PVM_MEM,
                                "Failed to allocate timer block. "
                                "Sending Resv Error msg without msg id.\n");
                    pTimerBlock = NULL;
                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                }
                else
                {
                    RPTE_TNL_RE_TIMER_BLOCK (pRsvpTeTnlInfo) = pTimerBlock;
                    /* Back Off Timer value is set to the initial value */
                    TIMER_BLK_BACKOFF_INTERVAL (pTimerBlock) =
                        (IF_ENTRY_REFRESH_INTERVAL (PKT_MAP_OUT_IF_ENTRY
                                                    (pPktMap)) /
                         RPTE_BACK_OFF_FACTOR) / DEFAULT_CONV_TO_SECONDS;

                    /* Msg Id is generated for the outgoing resv error
                     * message */
                    RpteMIHGenerateMsgId (&u4ResvErrMsgId);

                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) =
                        RPTE_NONZERO;

                    /* Since RMD is enabled for Resv Err, back off timer is
                     * started for the same. */
                    TIMER_PARAM_RPTE_TNL ((&TIMER_BLK_TIMER_PARAM
                                           (pTimerBlock))) = pRsvpTeTnlInfo;
                    TIMER_BLK_MSG_ID (pTimerBlock) = u4ResvErrMsgId;
                    TIMER_PARAM_ID ((&TIMER_BLK_TIMER_PARAM (pTimerBlock))) =
                        RPTE_RESVERR;

                    if (pPktMap->pErrorSpecObj != NULL)
                    {
                        MEMCPY ((UINT1 *) &TIMER_BLK_ERROR_SPEC (pTimerBlock),
                                (UINT1 *)
                                &ERROR_SPEC_OBJ (PKT_MAP_ERROR_SPEC_OBJ
                                                 (pPktMap)),
                                sizeof (tErrorSpec));
                    }
                    else if (pPktMap->pIfIdRsvpErrObj != NULL)
                    {
                        MEMCPY ((UINT1 *) &TIMER_BLK_ERROR_SPEC (pTimerBlock),
                                (UINT1 *)
                                &pPktMap->pIfIdRsvpErrObj->ErrorSpec,
                                sizeof (tErrorSpec));
                    }
                    else if (pPktMap->pIfIdRsvpNumErrObj != NULL)
                    {
                        MEMCPY ((UINT1 *) &TIMER_BLK_ERROR_SPEC (pTimerBlock),
                                (UINT1 *)
                                &pPktMap->pIfIdRsvpNumErrObj->ErrorSpec,
                                sizeof (tErrorSpec));
                    }

                    RSVP_TIMER_NAME ((&TIMER_BLK_APP_TIMER (pTimerBlock))) =
                        (FS_ULONG) & TIMER_BLK_TIMER_PARAM (pTimerBlock);

                    pTrieInfo = (tuTrieInfo *) RSVP_ALLOCATE_MEM_BLOCK
                        (RSVPTE_TRIE_INFO_POOL_ID);

                    if (pTrieInfo == NULL)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_MEM,
                                    "Failed to allocate Trie Info "
                                    "Sending Resv Err msg without msg id.\n");
                        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) =
                            RPTE_ZERO;
                        MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                            (UINT1 *) pTimerBlock);
                        pTimerBlock = NULL;
                        pTrieInfo = NULL;
                    }
                    else
                    {
                        MEMSET (pTrieInfo, RSVPTE_ZERO, sizeof (tuTrieInfo));
                        RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo) = RPTE_RESVERR;
                        RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = pTimerBlock;

                        RpteKey.u4_Key = u4ResvErrMsgId;

                        /* RBTree Addtion is only required for out path message id for
                         * SRefresh RecoveryPath message support.
                         * Its not required for resv error message id
                         * */

                        if (RpteTrieAddEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                              &RpteKey,
                                              pTrieInfo) != RPTE_SUCCESS)
                        {
                            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                        "PVM : Trie Addition Failed. "
                                        "- in Resv Err\n");
                            RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) =
                                RPTE_ZERO;
                            MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                                (UINT1 *) pTrieInfo);
                            RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = NULL;
                            MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                                (UINT1 *) pTimerBlock);
                            pTimerBlock = NULL;
                            pTrieInfo = NULL;
                        }
                    }
                }
            }
            else
            {
                pTimerBlock = RPTE_TNL_RE_TIMER_BLOCK (pRsvpTeTnlInfo);

                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;
                u4ResvErrMsgId = TIMER_BLK_MSG_ID (pTimerBlock);

                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
                if (RpteTrieLookupEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                         &pTrieInfo) != RPTE_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Look Up Failed - in Resv Err\n"
                                "ATTENTION : TimerBlock Present, but msg id "
                                "not present in Trie\n");
                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                    MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                        (UINT1 *) pTimerBlock);
                }
            }

            if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
            {
                if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                                       (pTimerBlock),
                                       TIMER_BLK_BACKOFF_INTERVAL (pTimerBlock)
                                       * SYS_NUM_OF_TIME_UNITS_IN_A_SEC) !=
                    TMR_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "PVM : Start Timer failed. - in Resv Err "
                                "Sending Resv Err without msg id\n");
                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                    MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                        (UINT1 *) pTimerBlock);
                    RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = NULL;
                    MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                        (UINT1 *) pTrieInfo);
                    pTrieInfo = NULL;
                    pTimerBlock = NULL;
                }
            }

        }
        else
        {
            /* In the case of RE Timer Block already present,
             * Stop the timer, Delete the Msg Id already present in
             * the Data base and continue. */
            pTimerBlock = RPTE_TNL_RE_TIMER_BLOCK (pRsvpTeTnlInfo);

            if (pTimerBlock != NULL)
            {
                TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                              (pTimerBlock));
                RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);

                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                pTimerBlock = NULL;
                pTrieInfo = NULL;
            }
            RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
        }
    }

    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) == RPTE_ZERO)
    {
        if (pRsvpTeTnlInfo != NULL)
        {
            RPTE_TNL_RE_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
        }
    }
    else
    {
        RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) = gu4Epoch;
        RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_MSG_ID_ACK_DESIRED;
        RPTE_PKT_MAP_MSG_ID (pPktMap) = u4ResvErrMsgId;
        i4ResvErrSize += RPTE_MSG_ID_OBJ_LEN;
    }

    /* Since a resv error is sent to the downstream, the msg id
     * corresponding the previous resv message is deleted from the
     * neighbour msg id data base. */
    if (pRsvpTeTnlInfo != NULL)
    {
        if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
             RPTE_IN_RESV_MSG_ID_PRESENT) == RPTE_IN_RESV_MSG_ID_PRESENT)
        {
            RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (RPTE_TNL_DSTR_NBR
                                                        (pRsvpTeTnlInfo));
            RpteKey.u8_Key[RPTE_ONE] = RPTE_TNL_IN_RESV_MSG_ID (pRsvpTeTnlInfo);
            RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                                 RPTE_NBR_MSG_ID_DB_PREFIX,
                                 RpteMIHReleaseTrieInfo);
        }
    }

    i4ResvErrSize = (INT4) (i4ResvErrSize + sizeof (tRsvpHdr) +
                            RPTE_IPV4_SSN_OBJ_LEN +
                            sizeof (tFlowSpecObj) +
                            sizeof (tStyleObj) + RPTE_IPV4_FLTR_SPEC_OBJ_LEN);
    if (pRsvpTeTnlInfo != NULL)
    {
        b1DnStrOob = pRsvpTeTnlInfo->b1DnStrOob;
        u4DnStrDataTeLinkIfId = pRsvpTeTnlInfo->u4DnStrDataTeLinkIfId;
    }

    i4ResvErrSize
        += RpteUtlCalculateRSVPHopSize (b1DnStrOob,
                                        u4DnStrDataTeLinkIfId,
                                        IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY
                                                       (pPktMap)));
    i4ResvErrSize += RpteUtlCalculateErrorSpecSize (pPktMap);

    if (((tFlowSpecObj *)
         PKT_MAP_FLOW_SPEC_OBJ (pPktMap))->FlowSpec.SrvHdr.u1SrvID
        == FLOWSPEC_CLS_HDR_NUMBER)
    {
        u2FSpecObjLength = sizeof (tFlowSpecObj) - sizeof (tGsRSpec);
        i4ResvErrSize = (INT4) (i4ResvErrSize - sizeof (tGsRSpec));
    }
    else
    {
        u2FSpecObjLength = sizeof (tFlowSpecObj);
    }

    if (PKT_MAP_RSVP_PKT_ALLOC (pPktMap) == RPTE_PKTMAP_IP_ALLOC)
    {
        pOrgRsvpPkt = PKT_MAP_IP_PKT (pPktMap);
    }

    pOrgMsgBuf = PKT_MAP_MSG_BUF (pPktMap);
    PKT_MAP_MSG_BUF (pPktMap) = NULL;
    PKT_MAP_RA_OPT_NEEDED (pPktMap) = RPTE_NO;
    PKT_MAP_RSVP_PKT (pPktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);
    PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    if (PKT_MAP_RSVP_PKT (pPktMap) == NULL)
    {
        if (pOrgRsvpPkt != NULL)
        {
            MemReleaseMemBlock (RSVPTE_IP_PKT_POOL_ID, (UINT1 *) pOrgRsvpPkt);
        }
        if (pOrgMsgBuf != NULL)
        {
            BUF_RELEASE (pOrgMsgBuf);
        }
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmConstructAndSndREMsg : EXIT \n");
        return;
    }

    /* Copy RsvpHdr */
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    MEMSET (pRsvpHdr, RSVPTE_ZERO, sizeof (tRsvpHdr));
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS ((UINT2) i4ResvErrSize);
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = RESVERR_MSG;
    RSVP_HDR_SEND_TTL (pRsvpHdr) = IF_ENTRY_TTL (PKT_MAP_IF_ENTRY (pPktMap));
    pObjHdr = (tObjHdr *) (VOID *) ((UINT1 *) pRsvpHdr + sizeof (tRsvpHdr));

    /* Msg Id Obj update, if present */
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        /* Filling the Header */
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_MSG_ID_OBJ_LEN);
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_MESSAGE_ID_CLASS_NUM_TYPE);

        pu1PduPtr = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

        /* Filling the Epoch and Flags fields */
        u4Val = (UINT4) (RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) |
                         (UINT4) (RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) <<
                                  RPTE_3_BYTE_SHIFT));
        u4Val = OSIX_HTONL (u4Val);
        MEMCPY ((UINT1 *) pu1PduPtr, (UINT1 *) &u4Val, RPTE_FLAG_EPOCH_LEN);

        pu1PduPtr += RPTE_FLAG_EPOCH_LEN;

        /* Filling the Message Identifier field */
        u4Val = OSIX_HTONL (RPTE_PKT_MAP_MSG_ID (pPktMap));
        MEMCPY ((UINT1 *) pu1PduPtr, (UINT1 *) &u4Val, RPTE_MSG_ID_LEN);
        pu1PduPtr += RPTE_MSG_ID_LEN;

        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* copy RsvpTeSessionObj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pTmpObjHdr = pObjHdr;
    pObjHdr = (tObjHdr *) (VOID *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    pu1PduPtr = (UINT1 *) pObjHdr;
    MEMCPY ((UINT1 *) pu1PduPtr,
            (UINT1 *) &PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap),
            RSVPTE_IPV4ADR_LEN);

    pu1PduPtr += RSVPTE_IPV4ADR_LEN;
    RPTE_PUT_2_BYTES (pu1PduPtr, PKT_MAP_RPTE_SSN_RSVD (pPktMap));
    RPTE_PUT_2_BYTES (pu1PduPtr, PKT_MAP_RPTE_SSN_TNL_ID (pPktMap));

    MEMCPY ((UINT1 *) pu1PduPtr,
            (UINT1 *) &PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap),
            RSVPTE_IPV4ADR_LEN);

    pu1PduPtr += RSVPTE_IPV4ADR_LEN;
    pObjHdr = NEXT_OBJ (pTmpObjHdr);

    /* Copy RsvpHop */
    if (pRsvpTeTnlInfo != NULL)
    {
        RptePvmFillRsvpHopObj (pRsvpTeTnlInfo, &pObjHdr, RESVERR_MSG);
        pObjHdr = NEXT_OBJ (pObjHdr);
    }
    else
    {
        pRsvpHopObj = (tRsvpHopObj *) (VOID *) pObjHdr;
        RSVP_HOP_ADDR (&RSVP_HOP_OBJ (pRsvpHopObj)) =
            OSIX_HTONL (IF_ENTRY_ADDR (PKT_MAP_OUT_IF_ENTRY (pPktMap)));
        RpteUtlEncodeLih (&RSVP_HOP_LIH (&RSVP_HOP_OBJ (pRsvpHopObj)),
                          PKT_MAP_OUT_IF_ENTRY (pPktMap));

        OBJ_HDR_LENGTH ((tObjHdr *) pRsvpHopObj) =
            OSIX_HTONS (sizeof (tRsvpHopObj));

        OBJ_HDR_CLASS_NUM_TYPE ((tObjHdr *) pRsvpHopObj) =
            OSIX_HTONS (IPV4_RSVP_HOP_CLASS_NUM_TYPE);

        pObjHdr = NEXT_OBJ (pObjHdr);
    }
    /* Copy ErrorSpec */
    pu1PduPtr = (UINT1 *) pObjHdr;
    RptePvmFillErrorSpecObj (&pu1PduPtr, pPktMap);
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* Fill Style Obj */
    *((tStyleObj *) pObjHdr) = *PKT_MAP_STYLE_OBJ (pPktMap);
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* Copy FlowSpec Obj */
    *((tFlowSpecObj *) (VOID *) pObjHdr) = *PKT_MAP_FLOW_SPEC_OBJ (pPktMap);
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (u2FSpecObjLength);
    pObjHdr = NEXT_OBJ (pObjHdr);

    /*  RsvpTeFilterSpecObj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_FLTR_SPEC_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS
        (IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE);

    pTmpObjHdr = pObjHdr;
    pObjHdr = (tObjHdr *) (VOID *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    pu1PduPtr = (UINT1 *) pObjHdr;

    MEMCPY ((UINT1 *) pu1PduPtr,
            (UINT1 *) &PKT_MAP_RPTE_FS_TNL_SNDR_ADDR (pPktMap),
            RSVPTE_IPV4ADR_LEN);

    pu1PduPtr += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pu1PduPtr, PKT_MAP_RPTE_FLTR_SPEC_RSVD (pPktMap));

    RPTE_PUT_2_BYTES (pu1PduPtr, PKT_MAP_RPTE_FLTR_SPEC_LSPID (pPktMap));
    pObjHdr = NEXT_OBJ (pTmpObjHdr);

    PKT_MAP_OPT_LEN (pPktMap) = 0;

    RptePbSendMsg (pPktMap);

    /* Free the allocated memory */
    if (pOrgRsvpPkt != NULL)
    {
        MemReleaseMemBlock (RSVPTE_IP_PKT_POOL_ID, (UINT1 *) pOrgRsvpPkt);
    }

    if (pOrgMsgBuf != NULL)
    {
        BUF_RELEASE (pOrgMsgBuf);
    }
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmConstructAndSndREMsg : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePvmSendResvPathErr
 * Description     : This function builds PathErr message from the ResvErr 
 *                   message with u1ErrCode and u2ErrValue
 * Input (s)       : pPktMap - Pointer to Packet Map
 *                 : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo
 *                   u1ErrCode - Error Code
 *                   u2ErrValue - Error Value
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePvmSendResvPathErr (tPktMap * pPktMap, UINT1 u1ErrCode, UINT2 u2ErrValue)
{
    tRsvpTeSndrTmpObj   RsvpTeSndrTmpObj;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmSendResvPathErr : ENTRY \n");

    RptePvmFillSrcAndDestAddr (pPktMap);

    PKT_MAP_OUT_IF_ENTRY (pPktMap) = PKT_MAP_IF_ENTRY (pPktMap);

    PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

    MEMSET (&RsvpTeSndrTmpObj, RPTE_ZERO, sizeof (tRsvpTeSndrTmpObj));
    pPktMap->pRsvpTeSndrTmpObj = &RsvpTeSndrTmpObj;

    PKT_MAP_RPTE_SNDR_TMP_LSPID (pPktMap) =
        (UINT2) RSVPTE_TNL_TNLINST (pPktMap->pRsvpTeTnlInfo);
    PKT_MAP_RPTE_SNDR_TMP_LSPID (pPktMap) =
        OSIX_HTONS (PKT_MAP_RPTE_SNDR_TMP_LSPID (pPktMap));
    MEMCPY ((UINT1 *) &PKT_MAP_RPTE_ST_TNL_SNDR_ADDR (pPktMap),
            (UINT1 *) &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL
                                                   (pPktMap->pRsvpTeTnlInfo))),
            RSVPTE_IPV4ADR_LEN);

    /* Fill Error Spec Object and Send Path Error */
    RptePvmFormAndSendPEOrREMsg (pPktMap, u1ErrCode, u2ErrValue, PATHERR_MSG);

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmSendResvPathErr : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmHandleUnknownObj                                */
/* Description     : This function handles the Unknown object. Depending on */
/*                   higher two bits of the onknown object class, it takes  */
/*                   decision either to send an error message or to ignore  */
/*                   message or ignore the object                           */
/* Input (s)       : *pPktMap - Pointer to Packet Map                       */
/*                   *pObjHdr - Pointer to Object Header                    */
/* Output (s)      : None                                                   */
/* Returns         : If the message with unknown Object has to be rejected, */
/*                   then RPTE_FAILURE is returned. Else if a Error message */
/*                   has to be sent, then ERR_SENT is retruned. If the      */
/*                   object has to be ignored, then RPTE_SUCCESS is returned*/
/****************************************************************************/
PRIVATE INT1
RptePvmHandleUnknownObj (tPktMap * pPktMap)
{
    tObjHdr            *pObjHdr = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tErrorSpecObj       ErrorSpecObj;
    UINT2               u2Tmp = RPTE_ZERO;
    UINT1               u1ClassNum = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmHandleUnknownObj : ENTRY \n");

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    pObjHdr = PKT_MAP_UNKNOWN_OBJ_HDR (pPktMap);
    u2Tmp = (OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)));
    u1ClassNum = (UINT1) ((u2Tmp & CLASS_NUM_MASK) >> RSHIFT_8BITS);

    switch (u1ClassNum)
    {
        case RPTE_CLASS_TYPE_OBJ_CLASS:
            /*If the Class Num and C-type is not supported send path Error */
            if (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_UNKNOWN_CNUM)
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Unknown Object Class - Reject\n");
                if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATH_MSG) ||
                    (RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATHTEAR_MSG))
                {
                    RptePvmSendPathErr (pPktMap, UNKNOWN_OBJECT_CLASS, u2Tmp);
                }
                else if (RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATHERR_MSG)
                {
                    break;
                }
                else
                {
                    RptePvmSendResvErr (pPktMap, UNKNOWN_OBJECT_CLASS, u2Tmp);
                }
                /* Error Message Sent */
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmHandleUnknownObj : INTMD-EXIT \n");
                return ERR_SENT;

            }
            break;
        case RPTE_LSP_TNL_IF_ID_OBJ_CLASS:
        case IPV4_NULL_CLASS:
        case IPV4_SESSION_CLASS:
        case IPV4_RSVP_HOP_CLASS:
        case RPTE_PROTEC_OBJ_CLASS:
        case RPTE_ASSOC_OBJ_CLASS:
        case RPTE_NOTIFY_REQUEST_CLASS:
        case IPV4_TIME_VALUES_CLASS:
        case IPV4_ERROR_SPEC_CLASS:
        case IPV4_SCOPE_CLASS:
        case IPV4_STYLE_CLASS:
        case IPV4_FLOW_SPEC_CLASS:
        case IPV4_FILTER_SPEC_CLASS:
        case IPV4_SENDER_TEMPLATE_CLASS:
        case IPV4_SENDER_TSPEC_CLASS:
        case IPV4_ADSPEC_CLASS:
        case IPV4_RESV_CONFIRM_CLASS:
        case RPTE_LBL_CLASS:
        case RPTE_GLBL_REQ_CLASS:
        case RPTE_HELLO_ACK_CLASS:
        case RPTE_ERO_CLASS:
        case RPTE_RRO_CLASS:
        case IPV4_RPTE_FRR_CLASS:
        case IPV4_RPTE_FRR_DETOUR_CLASS:
        case RPTE_SSN_ATTR_CLASS:
        case RPTE_DS_ELSP_OBJ_CLASS:
        case RPTE_ELSP_TP_OBJ_CLASS:
        case GMPLS_LABEL_SET_CLASS:
        case RPTE_ADMIN_STATUS_CLASS:
        case RPTE_UPSTR_GLBL_CLASS:
            break;
            /* As per RFC 2961, section 4.8 when the receiver of a MESSAGE_ID object does 
             * not support the class, Unknown Object Class error message will be generated */
        case RPTE_MESSAGE_ID_CLASS:
        case RPTE_MESSAGE_ID_ACK_CLASS:
        case RPTE_MESSAGE_ID_LIST_CLASS:
            if (gu1MsgIdCapable == RPTE_ENABLED)
            {
                break;
            }
        default:
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "-E- : Unknown Object Class - Reject\n");
            if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATH_MSG) ||
                (RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATHTEAR_MSG))
            {
                RptePvmSendPathErr (pPktMap, UNKNOWN_OBJECT_CLASS, u2Tmp);
            }
            else if (RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATHERR_MSG)
            {
                break;
            }
            else
            {
                RptePvmSendResvErr (pPktMap, UNKNOWN_OBJECT_CLASS, u2Tmp);
            }
            /* Error Message Sent */
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmHandleUnknownObj : INTMD-EXIT \n");
            return ERR_SENT;
    }                            /* switch */

    /* Unknown Class Type */

    if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATH_MSG) ||
        (RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATHTEAR_MSG))
    {
        RptePvmSendPathErr (pPktMap, UNKNOWN_OBJECT_CTYPE, u2Tmp);
    }
    else if (RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATHERR_MSG)
    {
        /* Copy ErrorSpec */
        PKT_MAP_ERROR_SPEC_OBJ (pPktMap) = &ErrorSpecObj;
        ERROR_SPEC_ADDR (&ERROR_SPEC_OBJ (&ErrorSpecObj)) =
            OSIX_HTONL (IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)));
        /* AS per RFC 3473, section 4.4
         * A Node which encounters an error MAY set Path State Removed flag */
        if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1PathStateRemovedFlag ==
            RPTE_ENABLED)
        {
            ERROR_SPEC_FLAGS (&ERROR_SPEC_OBJ (&ErrorSpecObj)) =
                RPTE_PATH_STATE_REMOVED;
        }
        else
        {
            ERROR_SPEC_FLAGS (&ERROR_SPEC_OBJ (&ErrorSpecObj)) = RSVPTE_ZERO;
        }
        ERROR_SPEC_CODE (&ERROR_SPEC_OBJ (&ErrorSpecObj)) =
            UNKNOWN_OBJECT_CTYPE;
        ERROR_SPEC_VALUE (&ERROR_SPEC_OBJ (&ErrorSpecObj)) = OSIX_HTONS (u2Tmp);

        if (RPTE_CHECK_PATH_ERR_MSG_TNL (pPktMap, pRsvpTeTnlInfo) ==
            RPTE_FAILURE)
        {
            PKT_MAP_ERROR_SPEC_OBJ (pPktMap) = NULL;
            pPktMap->pIfIdRsvpErrObj = NULL;
            pPktMap->pIfIdRsvpNumErrObj = NULL;
            RpteUtlCleanPktMap (pPktMap);
            return RPTE_FAILURE;
        }
        if ((pRsvpTeTnlInfo == NULL) || (pRsvpTeTnlInfo->pPsb == NULL))
        {
            PKT_MAP_ERROR_SPEC_OBJ (pPktMap) = NULL;
            pPktMap->pIfIdRsvpErrObj = NULL;
            pPktMap->pIfIdRsvpNumErrObj = NULL;
            RpteUtlCleanPktMap (pPktMap);
            return RPTE_FAILURE;
        }
        /*
         * If Ingress, send Path Tear message and delete Tnl Info from the table,
         * else update PHOP info and forward the Path Err down stream.
         */
        if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
        {
            /*
             * Call Process path Err routine to handle the rcvd
             * Error codes and Error Values.
             */
            RptePehProcessPathErr (pPktMap, pRsvpTeTnlInfo, RPTE_TRUE);
            PKT_MAP_ERROR_SPEC_OBJ (pPktMap) = NULL;
            pPktMap->pIfIdRsvpErrObj = NULL;
            pPktMap->pIfIdRsvpNumErrObj = NULL;
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PE_ETEXT, "RptePehProcessPathErrMsg : EXIT \n");
            return ERR_SENT;
        }

        PKT_MAP_OUT_IF_ENTRY (pPktMap) = PSB_IF_ENTRY (RSVPTE_TNL_PSB
                                                       (pRsvpTeTnlInfo));
        PKT_MAP_DST_ADDR (pPktMap) =
            RSVP_HOP_ADDR (&PSB_RSVP_HOP (pRsvpTeTnlInfo->pPsb));

        PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

        RPTE_PKT_MAP_NBR_ENTRY (pPktMap) = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);

        RptePvmConstructAndSndPEMsg (pPktMap);
    }
    else
    {
        RptePvmSendResvErr (pPktMap, UNKNOWN_OBJECT_CTYPE, u2Tmp);
    }
    /* Sent Error Message */
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmHandleUnknownObj : INTMD-EXIT \n");
    return ERR_SENT;
}

/****************************************************************************/
/* Function Name   : RptePvmValidateRORObj
 * Description     : This function validates all the ROR object in the
 *                   message
 * Input (s)       : pPktMap - Pointer to Packet Map
 *                   pObjHdr - Pointer to the Object
 * Output (s)      : None
 * Returns         : If successful, returns RPTE_SUCCESS
 *                   else returns RPTE_FAILURE
 */
/****************************************************************************/
PRIVATE INT1
RptePvmValidateRORObj (tPktMap * pPktMap, tObjHdr * pObjHdr)
{
    tMsgIdObjNode      *pMsgIdObjNode = NULL;
    UINT1              *pu1Pdu = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    UINT4               u4Epoch;

    switch (OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)))
    {
        case RPTE_MESSAGE_ID_CLASS_NUM_TYPE:

            pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
            switch (RSVP_HDR_MSG_TYPE (pRsvpHdr))
            {
                case ACK_MSG:
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Message Id Object present in Ack Message."
                                "Dropping it!\n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateRORObj : INTMD-EXIT\n");
                    return RPTE_FAILURE;

                case SREFRESH_MSG:
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Message Id Object present in SRefresh "
                                "Message. Dropping it!\n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateRORObj : INTMD-EXIT\n");
                    return RPTE_FAILURE;

                default:
                    break;
            }
            if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) == RSVPTE_ZERO)
            {
                pu1Pdu = (UINT1 *) ((UINT1 *) pObjHdr + sizeof (tObjHdr));

                /* Set the Msg-Id Present flag */
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;

                RPTE_EXT_4_BYTES (pu1Pdu, u4Epoch);
                RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap)
                    = (UINT1) ((u4Epoch & RPTE_WORD_UPPER_1_BYTE_MASK)
                               >> RPTE_3_BYTE_SHIFT);
                RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap)
                    = (u4Epoch & RPTE_WORD_LOWER_3_BYTE_MASK);
                RPTE_EXT_4_BYTES (pu1Pdu, RPTE_PKT_MAP_MSG_ID (pPktMap));
            }
            else
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "More than one Msg ID Object present in the "
                            "Message. Dropping it!\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateRORObj : INTMD-EXIT\n");
                return RPTE_FAILURE;
            }
            break;

        case RPTE_MESSAGE_ID_ACK_CLASS_NUM_TYPE:

            if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RSVPTE_ZERO)
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "Message Id Object comes before Ack Object. "
                            "Dropping it!\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateRORObj : INTMD-EXIT\n");
                return RPTE_FAILURE;
            }
            /* Since more than one MESSAGE_ID_ACK might be present
             * in the message, a list of these objects is maintained
             * in the Pkt Map structure. */

            pu1Pdu = (UINT1 *) ((UINT1 *) pObjHdr + sizeof (tObjHdr));

            RPTE_EXT_4_BYTES (pu1Pdu, u4Epoch);

            if ((u4Epoch & RPTE_WORD_LOWER_3_BYTE_MASK) != gu4Epoch)
            {
                /* Epoch value received from the neighbour in the
                 * Ack Object is different from the current Epcoh
                 * value of the node.
                 * Discard the object. */
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "Epoch rcvd in Ack Obj is different from the "
                            "node's Epoch.\n");
                break;
            }

            pMsgIdObjNode = (tMsgIdObjNode *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_MSG_ID_LIST_POOL_ID);

            if (pMsgIdObjNode == NULL)
            {
                RSVPTE_DBG (RSVPTE_PVM_MEM,
                            "Err - Memory allocation failed in "
                            "MSG_ID_ACK Extraction.\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateRORObj : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            TMO_SLL_Init_Node (&(pMsgIdObjNode->NextMsgIdObjNode));
            RPTE_MSG_ID_OBJ_NODE_FLAGS (pMsgIdObjNode)
                = (UINT1) ((u4Epoch & RPTE_WORD_UPPER_1_BYTE_MASK)
                           >> RPTE_3_BYTE_SHIFT);
            RPTE_MSG_ID_OBJ_NODE_EPOCH (pMsgIdObjNode)
                = (u4Epoch & RPTE_WORD_LOWER_3_BYTE_MASK);
            RPTE_EXT_4_BYTES (pu1Pdu, RPTE_MSG_ID_OBJ_NODE_MSG_ID
                              (pMsgIdObjNode));

            TMO_SLL_Add ((tTMO_SLL *) & (RPTE_PKT_MAP_MSG_ID_ACK_LIST
                                         (pPktMap)),
                         &(pMsgIdObjNode->NextMsgIdObjNode));
            break;

        case RPTE_MESSAGE_ID_NACK_CLASS_NUM_TYPE:

            if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RSVPTE_ZERO)
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "Message Id Object comes before Nack Object. "
                            "Dropping it!\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateRORObj : INTMD-EXIT\n");
                return RPTE_FAILURE;
            }
            /* Since more than one MESSAGE_ID_NACK might be present
             * in the message, a list of these objects is maintained
             * in the Pkt Map structure. */

            pu1Pdu = (UINT1 *) ((UINT1 *) pObjHdr + sizeof (tObjHdr));

            RPTE_EXT_4_BYTES (pu1Pdu, u4Epoch);

            if ((u4Epoch & RPTE_WORD_LOWER_3_BYTE_MASK) != gu4Epoch)
            {
                /* Epoch value received from the neighbour in the
                 * Nack Object is different from the current Epcoh
                 * value of the node.
                 * Discard the object. */
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "Epoch rcvd in Nack Obj is different from "
                            "the node's Epoch.\n");
                break;
            }

            pMsgIdObjNode = (tMsgIdObjNode *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_MSG_ID_LIST_POOL_ID);

            if (pMsgIdObjNode == NULL)
            {
                RSVPTE_DBG (RSVPTE_PVM_MEM,
                            "Err - Memory allocation failed in "
                            "MSG_ID_ACK Extraction.\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateRORObj : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            TMO_SLL_Init_Node (&(pMsgIdObjNode->NextMsgIdObjNode));
            RPTE_MSG_ID_OBJ_NODE_FLAGS (pMsgIdObjNode)
                = (UINT1) ((u4Epoch & RPTE_WORD_UPPER_1_BYTE_MASK)
                           >> RPTE_3_BYTE_SHIFT);
            RPTE_MSG_ID_OBJ_NODE_EPOCH (pMsgIdObjNode)
                = (u4Epoch & RPTE_WORD_LOWER_3_BYTE_MASK);
            RPTE_EXT_4_BYTES (pu1Pdu, RPTE_MSG_ID_OBJ_NODE_MSG_ID
                              (pMsgIdObjNode));

            TMO_SLL_Add ((tTMO_SLL *) & (RPTE_PKT_MAP_MSG_ID_NACK_LIST
                                         (pPktMap)),
                         &(pMsgIdObjNode->NextMsgIdObjNode));
            break;

        case RPTE_MESSAGE_ID_LIST_CLASS_NUM_TYPE:
            if (RPTE_PKT_MAP_MSG_ID_LIST (pPktMap) == NULL)
            {
                RPTE_PKT_MAP_MSG_ID_LIST (pPktMap) =
                    (tMsgIdListObj *) (VOID *) ((UINT1 *) pObjHdr);
            }
            break;
        default:
            break;
    }
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePvmValidateObj                                     */
/* Description     : This function validates all the objects in the message */
/* Input (s)       : *pPktMap - Pointer to Packet Map                       */
/* Output (s)      : None                                                   */
/* Returns         : If successful, returns RPTE_SUCCESS                    */
/*                   else returns RPTE_FAILURE                              */
/****************************************************************************/
PRIVATE INT1
RptePvmValidateObj (tPktMap * pPktMap)
{
    tRsvpHdr           *pRsvpHdr = NULL;
    tObjHdr            *pObjHdr = NULL;
    tIfEntry           *pIfEntry = NULL;
    UINT2               u2FSpecObjLength = RPTE_ZERO;
    UINT2               u2AdSpecWithoutGsAndCls = RPTE_ZERO;
    UINT1              *pEndOfPkt = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT1               u1SsnAttrFlag = RPTE_ZERO;
    UINT1               u1RROFlag = RPTE_ZERO;
    UINT1               u1EROFlag = RPTE_ZERO;
    UINT1               u1ReRtFltrFlag = RPTE_ZERO;
    UINT1               u1ReRtLblFlag = RPTE_ZERO;
    UINT2               u2Len = RPTE_ZERO;
    UINT2               u2Cnt = RPTE_ZERO;
    UINT4               u4SessionAttrLen = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "\nrptePvmValidateObj : ENTRY \n");

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    pEndOfPkt = (UINT1 *) pRsvpHdr + OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr));
    PKT_MAP_UNKNOWN_OBJ_HDR (pPktMap) = NULL;
    pObjHdr = (tObjHdr *) (VOID *) (((UINT1 *) pRsvpHdr) + sizeof (tRsvpHdr));
    pIfEntry = PKT_MAP_IF_ENTRY (pPktMap);

    u2FSpecObjLength = sizeof (tFlowSpecObj) - sizeof (tGsRSpec);
    u2AdSpecWithoutGsAndCls = sizeof (tAdSpecObj)
        - (sizeof (tGsAdSpec) + sizeof (tClsAdSpec));

    while (pObjHdr < (tObjHdr *) (VOID *) pEndOfPkt)
    {
        if (VALIDATE_OBJHDR (pObjHdr, pEndOfPkt))
        {
            RSVPTE_DBG (RSVPTE_PVM_PRCS, "Validate obj hdr failed \n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidateObj : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        switch (OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)))
        {

            case IPV4_NULL_CLASS_NUM_TYPE:
                RSVPTE_DBG (RSVPTE_PVM_PRCS, "NULL Object\n");
                /* Ignore */
                break;

            case IPV4_SESSION_CLASS_NUM_TYPE:
                /* NOT Supported */
                break;

            case IPV4_RSVP_HOP_CLASS_NUM_TYPE:
                /* RSVP_HOP Object */
                /* Check for Valid RSVP Hop obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    RPTE_IPV4_RSVP_HOP_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Rsvp Hop Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_RSVP_HOP_OBJ (pPktMap) =
                    (tRsvpHopObj *) (VOID *) pObjHdr;
                break;
            case IPV4_IF_ID_RSVP_HOP_CLASS_NUM_TYPE:

                /* IF_ID RSVP_HOP Object */
                /* Check for Valid RSVP IF_ID Hop obj, if not present return not ok */
                if ((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                     IPV4_IF_ID_SUB_OBJ_RSVP_LENGTH)
                    && (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                        IPV4_IF_ID_NUM_SUB_OBJ_RSVP_LENGTH))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid IF ID Rsvp Hop Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) ==
                    IPV4_IF_ID_NUM_SUB_OBJ_RSVP_LENGTH)
                {
                    pPktMap->pIfIdRsvpNumHopObj =
                        (tIfIdRsvpNumHopObj *) (VOID *) pObjHdr;
                }
                else
                {
                    pPktMap->pIfIdRsvpHopObj =
                        (tIfIdRsvpHopObj *) (VOID *) pObjHdr;
                }

                break;

            case RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE_1:
                pPktMap->pLspTnlIfIdObj = (tLspTnlIfIdObj *) (VOID *) pObjHdr;
                break;
            case RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE:

                if (gi4MplsSimulateFailure ==
                    RPTE_SIM_FAILURE_LSP_TNL_IFID_CTYPE_1)
                {
                    PKT_MAP_UNKNOWN_OBJ_HDR (pPktMap) = pObjHdr;
                }
                else
                {
                    if ((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                         RPTE_LSP_TNL_IF_ID_OBJ_LEN))
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "Err : Invalid LSP_TNL_IF_ID Obj \n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                    pPktMap->pLspTnlIfIdObj =
                        (tLspTnlIfIdObj *) (VOID *) pObjHdr;
                }
                break;

            case RPTE_PROTEC_OBJ_CLASS_NUM_TYPE:
                if ((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                     RPTE_PROTEC_OBJ_LEN))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Protection Object \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pProtectionObj = (tProtectionObj *) (VOID *) pObjHdr;
                break;

            case RPTE_ASSOC_OBJ_CLASS_NUM_TYPE:
                if ((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                     RPTE_ASSOC_OBJ_LEN))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Association Object \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pAssociationObj = (tAssociationObj *) (VOID *) pObjHdr;
                break;

            case RPTE_NOTIFY_REQUEST_CLASS_NUM_TYPE:
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    RPTE_NOTIFY_REQUEST_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid length of Notify Request Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pNotifyRequestObj =
                    (tNotifyRequestObj *) (VOID *) pObjHdr;
                break;

            case IPV4_INTEGRITY_CLASS_NUM_TYPE:
                /* INTEGRITY Object */
                /* For the Time being Ignore this */
                break;

            case IPV4_TIME_VALUES_CLASS_NUM_TYPE:
                /* TIME_VALUES Object */
                PKT_MAP_TIME_VALUES_OBJ (pPktMap) =
                    (tTimeValuesObj *) (VOID *) pObjHdr;
                break;

            case IPV4_ERROR_SPEC_CLASS_NUM_TYPE:
                /* ERROR_SPEC Object */
                /* Check for Valid Err Spec obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    RPTE_IPV4_ERR_SPEC_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Err Spec Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_ERROR_SPEC_OBJ (pPktMap) =
                    (tErrorSpecObj *) (VOID *) pObjHdr;
                break;
            case IPV4_IF_ID_ERROR_SPEC_CLASS_NUM_TYPE:
                /* IF_ID ERROR_SPEC Object */
                /* Check for Valid Err Spec obj, if not present return not ok */
                if ((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                     IPV4_IF_ID_ERROR_OBJ_LEN) &&
                    (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                     IPV4_IF_ID_ERROR_NUM_OBJ_LEN))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Err Spec Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) ==
                    IPV4_IF_ID_ERROR_NUM_OBJ_LEN)
                {
                    pPktMap->pIfIdRsvpNumErrObj =
                        (tIfIdRsvpNumErrObj *) (VOID *) pObjHdr;
                }
                else
                {
                    pPktMap->pIfIdRsvpErrObj =
                        (tIfIdRsvpErrObj *) (VOID *) pObjHdr;
                }

                break;
            case IPV4_SCOPE_CLASS_NUM_TYPE:
                /* SCOPE Object */
                PKT_MAP_SCOPE_OBJ (pPktMap) = (tScopeObj *) (VOID *) pObjHdr;
                break;

            case IPV4_STYLE_CLASS_NUM_TYPE:
                /* STYLE Object */
                /* Check for Valid Style obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) != sizeof (tStyleObj))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS, "Err : Invalid Style Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_STYLE_OBJ (pPktMap) = (tStyleObj *) pObjHdr;
                if (RpteUtlValidateStyle (&STYLE_OBJ (PKT_MAP_STYLE_OBJ
                                                      (pPktMap))) ==
                    RPTE_FAILURE)
                {
                    if (RSVP_HDR_MSG_TYPE (pRsvpHdr) != RESVERR_MSG)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                    else
                    {
                        /* 
                         * Drop the message since the resv error mesg has
                         * error.
                         * Dont generate Error messages for Error Mesges.
                         */
                        RSVPTE_DBG (RSVPTE_PVM_PRCS, "Error in  Error Msg\n");
                        RpteUtlCleanPktMap (pPktMap);
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                }
                break;

            case IPV4_FLOW_SPEC_CLASS_NUM_TYPE:
                /* FLOW_SPEC Object */
                /* Check for Valid Flow Spec obj, if not present
                 * return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) ==
                    sizeof (tFlowSpecObj)
                    || (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) ==
                        u2FSpecObjLength))
                {
                    if (PKT_MAP_STYLE_OBJ (pPktMap) == NULL)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "-E- : FlowSpec before Style\n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                    if (PKT_MAP_RPTE_MSG_FLAG (pPktMap) == RPTE_YES)
                    {
                        PKT_MAP_FLOW_SPEC_OBJ (pPktMap)
                            = (tFlowSpecObj *) (VOID *) pObjHdr;
                    }
                }
                else
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid  Flow Spec Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                break;

            case IPV4_FILTER_SPEC_CLASS_NUM_TYPE:
                /* NOT Supported */
                break;

            case IPV4_SENDER_TEMPLATE_CLASS_NUM_TYPE:
                /* NOT Supported */
                break;

            case IPV4_SENDER_TSPEC_CLASS_NUM_TYPE:
                /* SENDER_TSPEC Object */
                /* Check for Valid Sender Tspec obj, if not present 
                 * return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    sizeof (tSenderTspecObj))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Sender Tspec Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_SENDER_TSPEC_OBJ (pPktMap) =
                    (tSenderTspecObj *) (VOID *) pObjHdr;
                break;

            case IPV4_ADSPEC_CLASS_NUM_TYPE:
                /* ASPEC Object */
                /* Check for Valid AdSpec Obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) == sizeof (tAdSpecObj)
                    || (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) ==
                        u2AdSpecWithoutGsAndCls))
                {
                    PKT_MAP_ADSPEC_OBJ (pPktMap) =
                        (tAdSpecObj *) (VOID *) pObjHdr;
                }
                else
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS, "Err : Invalid AdSpec Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                break;

            case IPV4_POLICY_DATA_CLASS_NUM_TYPE:
                /* POLICY_DATA Object */
                /* Not Supported, Ignore */
                break;

            case IPV4_RESV_CONFIRM_CLASS_NUM_TYPE:
                /* RESV_CONF Object */
                /* Check for Valid ResvConf obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    sizeof (tResvConfObj))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid ResvConf Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_RESV_CONF_OBJ (pPktMap) =
                    (tResvConfObj *) (VOID *) pObjHdr;
                break;

                /* NOTE: In case of Re route, the order of objects expected 
                 * is as specified in the Draft lsp-tunnel-08.txt */

                /* For Generalized label request, only class type is different
                 * Format is same as like label object */
            case RPTE_GEN_LBL_CLASS_NUM_TYPE:
            case RPTE_LBL_CLASS_NUM_TYPE:
                PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                pu1Pdu = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));
                if (u1ReRtLblFlag == RSVPTE_ZERO)
                {
                    PKT_MAP_RPTE_LBL_OBJ (pPktMap) =
                        (tGenLblObj *) (VOID *) pObjHdr;
                    if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) <
                        RPTE_MIN_LBL_OBJ_LEN)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "Err : Invalid Label Obj \n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                    if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM)
                    {
                        RPTE_EXT_2_BYTES (pu1Pdu,
                                          PKT_MAP_LBL_OBJ_ATM_LBL_VPI
                                          (pPktMap));
                        RPTE_EXT_2_BYTES (pu1Pdu,
                                          PKT_MAP_LBL_OBJ_ATM_LBL_VCI
                                          (pPktMap));
                    }
                    else if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ETHERNET)
                    {
                        RPTE_EXT_4_BYTES (pu1Pdu,
                                          PKT_MAP_LBL_OBJ_GEN_LBL (pPktMap));
                    }
                    else
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS, "Err : Invalid Lbl Obj\n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                    u1ReRtLblFlag++;
                }

                else if (u1ReRtLblFlag == RSVPTE_ONE)
                {                /* Re route condition */
                    PKT_MAP_RPTE_ADNL_LBL_OBJ (pPktMap) =
                        (tGenLblObj *) (VOID *) pObjHdr;
                    if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) <
                        RPTE_MIN_LBL_OBJ_LEN)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "Err : Invalid Label Obj \n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                    if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM)
                    {
                        RPTE_EXT_2_BYTES (pu1Pdu,
                                          PKT_MAP_ADNL_LBL_OBJ_ATM_LBL_VPI
                                          (pPktMap));
                        RPTE_EXT_2_BYTES (pu1Pdu,
                                          PKT_MAP_ADNL_LBL_OBJ_ATM_LBL_VCI
                                          (pPktMap));
                    }
                    else if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ETHERNET)
                    {
                        RPTE_EXT_4_BYTES (pu1Pdu,
                                          PKT_MAP_ADNL_LBL_OBJ_GEN_LBL
                                          (pPktMap));
                    }
                    else
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "Err : Invalid Lbl Obj in reroute "
                                    "condition \n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                    u1ReRtLblFlag++;
                }
                break;

            case RPTE_GLBL_REQ_CLASS_NUM_TYPE:
            case RPTE_LBL_REQ_CLASS_NUM_TYPE:
                PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                /* Check for Valid LblReq obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    sizeof (tGenLblReqObj))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS, "Err : Invalid LblReq Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pGenLblReqObj = (tGenLblReqObj *) pObjHdr;
                break;

            case RPTE_LBL_REQ_ATM_RNG_CLASS_NUM_TYPE:
                PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                /* Check for Valid LblReqAtmRng obj, if not present 
                 * return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    sizeof (tLblReqAtmRngObj))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid LblReqAtmRng Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pLblReqAtmRngObj = (tLblReqAtmRngObj *) pObjHdr;
                break;

            case RPTE_HELLO_REQ_CLASS_NUM_TYPE:
            case RPTE_HELLO_ACK_CLASS_NUM_TYPE:
                PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                /* Check for Valid HelloReq/Ack obj, if not present 
                 * return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) != sizeof (tHelloObj))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid HelloReq/Ack Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_HELLO_OBJ (pPktMap) = (tHelloObj *) (VOID *) pObjHdr;
                break;

            case RPTE_ERO_CLASS_NUM_TYPE:
                if (u1EROFlag == RSVPTE_ZERO)
                {
                    /* Set to ignore subsequent ERO */
                    u1EROFlag = RSVPTE_ONE;
                    if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) <
                        RPTE_MIN_ERO_OBJ_LEN)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "Err : Invalid ERO Obj Len\n");
                        pPktMap->u1EroErrType = RPTE_BAD_EXPLICT_ROUTE_OBJ;
                        /* 
                         * NOTE: We are not returning RPTE_FAILURE here, 
                         * instead we are generating path err msg from RPTE 
                         * process path msg.
                         */
                    }

                    PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                    if (RptePvmExtractEROList (pPktMap, &pObjHdr, pEndOfPkt) ==
                        RPTE_FAILURE)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS, "Err : Invalid ERO \n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                }
                break;

            case RPTE_RRO_CLASS_NUM_TYPE:
                if (u1RROFlag == RSVPTE_ZERO)
                {
                    /* Set to ignore subsequent RRO */
                    u1RROFlag = RSVPTE_ONE;
                    if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) <
                        RPTE_MIN_RRO_OBJ_LEN)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "Err : Invalid RRO Obj \n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                    PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                    if (RptePvmExtractRROList (pPktMap, &pObjHdr, pEndOfPkt)
                        == RPTE_FAILURE)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS, "Err : Invalid RRO \n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                }
                break;

            case IPV4_RPTE_SSN_CLASS_NUM_TYPE:
                PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                /* Check for valid Ssn Obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    RPTE_IPV4_SSN_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS, "Err : Invalid Ssn Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_RPTE_SSN_OBJ (pPktMap) = (tRsvpTeSsnObj *) pObjHdr;
                break;

            case IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE:
                PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                /* Check for valid Sndr Tmp Obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    RPTE_IPV4_SNDR_TMP_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Sndr Tmp Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_RPTE_SND_TMP_OBJ (pPktMap) =
                    (tRsvpTeSndrTmpObj *) pObjHdr;
                break;

            case IPV4_RPTE_FRR_CLASS_NUM_TYPE:
                PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                /* Check for valid fast reroute Obj */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    RPTE_IPV4_FRR_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid FAST_REROUTE Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap) =
                    (tRsvpTeFastRerouteObj *) (VOID *) pObjHdr;
                break;

            case IPV4_RPTE_FRR_DETOUR_CLASS_NUM_TYPE:
                PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                if (RSVPTE_FRR_DETOUR_CAPABLE (gpRsvpTeGblInfo) != RPTE_TRUE)
                {
                    /* Cannot Support DETOUR. Quit */
                    PKT_MAP_UNKNOWN_OBJ_HDR (pPktMap) = pObjHdr;
                    RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                                 "\nRcvd Unknown Obj of Class Num Type 0x%x\n",
                                 OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)));
                    break;
                }

                /* Check for valid Detour Obj */
                if ((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) <
                     RPTE_IPV4_FRR_MIN_DETOUR_LEN) ||
                    (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) % WORD_BNDRY
                     != RSVPTE_ZERO))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS, "Err : Invalid Detour Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                u2Len = OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr));
                pu1Pdu = (UINT1 *) pObjHdr;
                RPTE_GET_4_BYTES (pu1Pdu,
                                  FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ
                                                      (pPktMap)));
                u2Len = (UINT2) (u2Len - sizeof (tObjHdr));
                /* No of PLRs and Avoid Node Id pairs */
                u2Len =
                    (UINT2) (u2Len / ((UINT2) (RPTE_TWO * RSVPTE_IPV4ADR_LEN)));
                for (u2Cnt = RPTE_ZERO; u2Cnt < u2Len; u2Cnt++)
                {
                    RPTE_GET_4_BYTES (pu1Pdu, PKT_MAP_RPTE_DETOUR_OBJ (pPktMap).
                                      RsvpTeDetour.au4PlrId[u2Cnt]);
                    RPTE_GET_4_BYTES (pu1Pdu, PKT_MAP_RPTE_DETOUR_OBJ (pPktMap).
                                      RsvpTeDetour.au4AvoidNodeId[u2Cnt]);
                }
                break;

            case IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE:
                PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                if (u1ReRtFltrFlag == RSVPTE_ZERO)
                {
                    /* Check for valid Fltr Spec Obj, 
                     * if not present return not ok */
                    if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr))
                        != RPTE_IPV4_FLTR_SPEC_OBJ_LEN)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "Err : Invalid Filter Spec Obj \n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                    PKT_MAP_RPTE_FILTER_SPEC_OBJ (pPktMap) =
                        (tRsvpTeFilterSpecObj *) pObjHdr;
                    u1ReRtFltrFlag++;
                }
                else if (u1ReRtFltrFlag == RSVPTE_ONE)
                {                /* Re route condition */

                    /* Check for valid Fltr Spec Obj, 
                     * if not present return not ok */
                    if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                        RPTE_IPV4_FLTR_SPEC_OBJ_LEN)
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "Err : Invalid Adnl Filter Spec Obj \n");
                        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                    "RptePvmValidateObj : INTMD-EXIT \n");
                        return RPTE_FAILURE;
                    }
                    PKT_MAP_RPTE_ADNL_FILTER_SPEC_OBJ (pPktMap) =
                        (tRsvpTeFilterSpecObj *) pObjHdr;
                    u1ReRtFltrFlag++;
                }
                break;

            case RPTE_SSN_ATTR_CLASS_NUM_TYPE:
                if (u1SsnAttrFlag == RSVPTE_ZERO)
                {
                    u1SsnAttrFlag = RSVPTE_ONE;
                    /* Flag used to ignore subsequent SsnAttr */
                    /* Check for valid Ssn Attr Obj, if not present 
                     * log err msg */
                    if ((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) <
                         RPTE_MIN_SSN_ATTR_OBJ_LEN) ||
                        (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) % WORD_BNDRY
                         != RSVPTE_ZERO))
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "Err : Invalid Ssn Attr Obj - "
                                    "due to Length\n");
                        return RPTE_FAILURE;
                    }

                    pu1Pdu = (UINT1 *) pObjHdr;

                    /* Extract Ssn Attr Obj header Len */
                    RPTE_GET_2_BYTES (pu1Pdu,
                                      PKT_MAP_RPTE_SSN_ATTR_HLEN (pPktMap));

                    /* Extract Ssn Attr Obj Class and Num Type */
                    RPTE_GET_2_BYTES (pu1Pdu,
                                      PKT_MAP_RPTE_SSN_ATTR_HCNTYPE (pPktMap));

                    /* Extract Ssn Attr Setup priority */
                    RPTE_GET_1_BYTE (pu1Pdu,
                                     PKT_MAP_RPTE_SSN_ATTR_SPRIO (pPktMap));

                    /* Extract Ssn Attr Hold priority */
                    RPTE_GET_1_BYTE (pu1Pdu,
                                     PKT_MAP_RPTE_SSN_ATTR_HPRIO (pPktMap));

                    /* Extract Ssn Attr Flags */
                    RPTE_GET_1_BYTE (pu1Pdu,
                                     PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap));

                    /* Extract Ssn Attr Name Len */
                    RPTE_GET_1_BYTE (pu1Pdu,
                                     PKT_MAP_RPTE_SSN_ATTR_NLEN (pPktMap));
                    u4SessionAttrLen =
                        (UINT4) PKT_MAP_RPTE_SSN_ATTR_NLEN (pPktMap);

                    /* Extract Ssn Attr Obj Ssn Name */
                    MEM_COPY ((UINT1 *) pu1Pdu,
                              (UINT1
                               *) (&PKT_MAP_RPTE_SSN_ATTR_SNAME (pPktMap)),
                              u4SessionAttrLen);

                    if ((PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap)
                         & RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG) ==
                        RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG)
                    {
                        PKT_MAP_RE_ROUTE_FLAG (pPktMap) = RPTE_YES;
                    }

                    PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                }
                break;

            case RPTE_RA_SSN_ATTR_CLASS_NUM_TYPE:
                if (u1SsnAttrFlag == RSVPTE_ZERO)
                {
                    /* Set to ignore subsequent RA SsnAttr */
                    u1SsnAttrFlag = RSVPTE_ONE;

                    /* Check for valid Resource Affinity Ssn Attr Obj,  */
                    /* if not present log err msg                       */
                    if ((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) <
                         RPTE_RA_MIN_SSN_ATTR_OBJ_LEN) ||
                        (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) % WORD_BNDRY
                         != RSVPTE_ZERO))
                    {
                        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                    "Err : Invalid RA Ssn Attr Obj - "
                                    "due to Length\n");
                        return RPTE_FAILURE;
                    }

                    pu1Pdu = (UINT1 *) pObjHdr;

                    /* Extract RA Ssn Attr Obj header Len */
                    RPTE_GET_2_BYTES (pu1Pdu,
                                      PKT_MAP_RPTE_RA_SSN_ATTR_HLEN (pPktMap));

                    /* Extract RA Ssn Attr Obj Class and Num Type */
                    RPTE_GET_2_BYTES (pu1Pdu,
                                      PKT_MAP_RPTE_RA_SSN_ATTR_HCNTYPE
                                      (pPktMap));

                    /* Extract RA Ssn Attr Obj Exclude Any Attribute  */
                    RPTE_EXT_4_BYTES (pu1Pdu,
                                      PKT_MAP_RPTE_RA_SSN_ATTR_EX_ANY_ATTR
                                      (pPktMap));

                    /* Extract RA Ssn Attr Obj Include Any Attribute  */
                    RPTE_EXT_4_BYTES (pu1Pdu,
                                      PKT_MAP_RPTE_RA_SSN_ATTR_INC_ANY_ATTR
                                      (pPktMap));

                    /* Extract RA Ssn Attr Obj Include All Attribute  */
                    RPTE_EXT_4_BYTES (pu1Pdu,
                                      PKT_MAP_RPTE_RA_SSN_ATTR_INC_ALL_ATTR
                                      (pPktMap));

                    /* Extract RA Ssn Attr Setup priority */
                    RPTE_GET_1_BYTE (pu1Pdu,
                                     PKT_MAP_RPTE_RA_SSN_ATTR_SPRIO (pPktMap));

                    /* Extract RA Ssn Attr Hold priority */
                    RPTE_GET_1_BYTE (pu1Pdu,
                                     PKT_MAP_RPTE_RA_SSN_ATTR_HPRIO (pPktMap));

                    /* Extract RA Ssn Attr Flags */
                    RPTE_GET_1_BYTE (pu1Pdu,
                                     PKT_MAP_RPTE_RA_SSN_ATTR_FLAGS (pPktMap));

                    /* Extract RA Ssn Attr Name Len */
                    RPTE_GET_1_BYTE (pu1Pdu,
                                     PKT_MAP_RPTE_RA_SSN_ATTR_NLEN (pPktMap));

                    u4SessionAttrLen =
                        (UINT4) PKT_MAP_RPTE_RA_SSN_ATTR_NLEN (pPktMap);

                    /* Extract RA Ssn Attr Obj Ssn Name */
                    MEM_COPY ((UINT1 *) pu1Pdu,
                              (UINT1
                               *) (&PKT_MAP_RPTE_RA_SSN_ATTR_SNAME (pPktMap)),
                              u4SessionAttrLen);

                    if ((PKT_MAP_RPTE_RA_SSN_ATTR_FLAGS (pPktMap)
                         & RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG) ==
                        RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG)
                    {
                        PKT_MAP_RE_ROUTE_FLAG (pPktMap) = RPTE_YES;
                    }
                    PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
                }
                break;

            case RPTE_MESSAGE_ID_CLASS_NUM_TYPE:
            case RPTE_MESSAGE_ID_ACK_CLASS_NUM_TYPE:
            case RPTE_MESSAGE_ID_NACK_CLASS_NUM_TYPE:
            case RPTE_MESSAGE_ID_LIST_CLASS_NUM_TYPE:

                if (gu1MsgIdCapable != RPTE_ENABLED)
                {
                    /* The Further processing depends on the Class Num and
                     * Class Type values */
                    PKT_MAP_UNKNOWN_OBJ_HDR (pPktMap) = pObjHdr;
                    RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                                 "\nRcvd Unknown Obj of Class Num Type 0x%x\n",
                                 OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)));
                    break;
                }
                if (RptePvmValidateRORObj (pPktMap, pObjHdr) == RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT\n");
                    return RPTE_FAILURE;
                }
                break;

            case RPTE_DS_ELSP_OBJ_CLASS_NUM_TYPE:
            case RPTE_DS_LLSP_OBJ_CLASS_NUM_TYPE:
            case RPTE_ELSP_TP_OBJ_CLASS_NUM_TYPE:
            case RPTE_CLASS_TYPE_OBJ_CLASS_NUM_TYPE:
                if (RptePvmValidateDSObj (pPktMap, pObjHdr) == RPTE_FAILURE)
                {
                    return RPTE_FAILURE;
                }
                break;
                /*Label Set Object validation */
            case GMPLS_LABEL_SET_CLASS_NUM_TYPE:

                pPktMap->pLabelSetObj = (tLabelSetObj *) (VOID *) pObjHdr;

                if (!(((pPktMap->pLabelSetObj->LabelSet.u1Action) ==
                       GMPLS_LBLSET_INCLUDE_LIST_RANGE) ||
                      ((pPktMap->pLabelSetObj->LabelSet.u1Action) ==
                       GMPLS_LBLSET_INCLUDE_LIST)))

                {
                    return RPTE_FAILURE;
                }

                if ((pPktMap->pLabelSetObj->LabelSet.u1Action) ==
                    GMPLS_LBLSET_INCLUDE_LIST_RANGE)
                {
                    if ((pPktMap->pLabelSetObj->LabelSet.u4SubChannel1
                         == RSVPTE_ZERO) ||
                        (pPktMap->pLabelSetObj->LabelSet.u4SubChannel1
                         == RSVPTE_ZERO))
                    {
                        return RPTE_FAILURE;
                    }
                }
                break;

            case RPTE_ADMIN_STATUS_CLASS_NUM_TYPE:
                /* Check the Valid Admin Status Object */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    RPTE_ADMIN_STATUS_OBJECT_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Admin Status Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pAdminStatusObj = (tAdminStatusObj *) (VOID *) pObjHdr;
                break;

                /* upstream label obj validation */
            case RPTE_UPSTR_GLBL_CLASS_NUM_TYPE:
                pu1Pdu = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));
                pPktMap->pUpStrLblObj = (tGenLblObj *) (VOID *) pObjHdr;
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) <
                    RPTE_MIN_LBL_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Upstream Label Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                RPTE_EXT_4_BYTES (pu1Pdu,
                                  pPktMap->pUpStrLblObj->Label.u4GenLbl);
                break;

            case RPTE_SUGGESTED_GLBL_CLASS_NUM_TYPE:
                pu1Pdu = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));
                pPktMap->pSuggestedLblObj =
                    (tSuggestedLblObj *) (VOID *) pObjHdr;
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) <
                    RPTE_MIN_LBL_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Suggested Label Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                break;

            case RPTE_RECOVERY_GLBL_CLASS_NUM_TYPE:
                pu1Pdu = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));
                pPktMap->pRecoveryLblObj = (tRecoveryLblObj *) (VOID *) pObjHdr;
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) <
                    RPTE_MIN_LBL_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Recovery Label Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                break;

            case RPTE_GR_RESTART_CAP_CLASS_NUM_TYPE:
                /* Check for Valid Restart_Cap obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    sizeof (tRestartCapObj))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Restart_Cap Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pRestartCapObj = (tRestartCapObj *) (VOID *) pObjHdr;
                break;

            case RPTE_GR_CAPABILITY_CLASS_NUM_TYPE:
                /* Check for Valid Capability obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    sizeof (tCapabilityObj))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Capability Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pCapabilityObj = (tCapabilityObj *) (VOID *) pObjHdr;
                break;

            default:
                /* 
                 *  The Further processing depends on the Class Num and 
                 *  Class Type values 
                 */
                PKT_MAP_UNKNOWN_OBJ_HDR (pPktMap) = pObjHdr;
                RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                             "\nRcvd Unknown Obj of Class Num Type 0x%x\n",
                             OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)));
                break;
        }
        pObjHdr = NEXT_OBJ (pObjHdr);

    }                            /* while */

    if (pObjHdr != (tObjHdr *) (VOID *) pEndOfPkt)
    {
        RSVPTE_DBG (RSVPTE_PVM_PRCS, "-E- : Invalid Total Length\n");
        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidateObj : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* Check for setting reroute flag in pPktMap */
    if ((u1ReRtFltrFlag == RSVPTE_TWO) && (u1ReRtLblFlag == RSVPTE_TWO))
    {
        pPktMap->u1ReRtFlag = RPTE_YES;
    }

    if ((u1ReRtLblFlag == RSVPTE_TWO) && (u1ReRtFltrFlag != RSVPTE_TWO))
    {
        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                    "-E- : Two Label Object is present in RESV MSG"
                    ",Invalid RESV Message - Dropping the packet\n");
        return RPTE_FAILURE;
    }

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidateObj : EXIT \n");

    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePvmLocalAddr                                       */
/* Description     : This function Matches the given address with the local */
/*                   addresses and returns RPTE_SUCCESS if it matches. Else */
/*                   returns RPTE_FAILURE                                   */
/* Input (s)       : u4Addr - IP Address                                    */
/* Output (s)      : None                                                   */
/* Returns         : If successful returns RPTE_SUCCESS,                    */
/*                   else returns RPTE_FAILURE                              */
/****************************************************************************/
INT1
RptePvmLocalAddr (UINT4 u4Addr)
{
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4HashIndex;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmLocalAddr : ENTRY \n");

    TMO_HASH_Scan_Table (RSVP_GBL_IF_HSH_TBL, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (RSVP_GBL_IF_HSH_TBL, u4HashIndex, pIfEntry,
                              tIfEntry *)
        {
            if (IF_ENTRY_ADDR (pIfEntry) == u4Addr)
            {
                /* Dst Addr matches the local interface address */
                RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmLocalAddr : EXIT \n");
                return RPTE_SUCCESS;
            }
        }
    }

    if (NetIpv4IsLoopbackAddress (u4Addr) == NETIPV4_SUCCESS)
    {
        RSVPTE_DBG1 (RSVPTE_PVM_ETEXT,
                     "RptePvmLocalAddr : %x is a Loopback Address - EXIT\n",
                     u4Addr);
        return RPTE_SUCCESS;
    }

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmLocalAddr Address Not found: "
                "INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RptePvmValidateMandatoryObjs                           */
/* Description     : This function checks for the mandatory object in a msg */
/*                   depending on the type. If all the objects are present  */
/*                   then it returns RPTE_SUCCESS.                          */
/*                   Else RPTE_FAILURE is returned                          */
/* Input (s)       : *pPktMap - Pointer to Packet Map                       */
/* Output (s)      : None                                                   */
/* Returns         : If successful returns RPTE_SUCCESS,                    */
/*                   else returns RPTE_FAILURE                              */
/****************************************************************************/
PRIVATE INT1
RptePvmValidateMandatoryObjs (const tPktMap * pPktMap)
{
    tRsvpHdr           *pRsvpHdr = NULL;
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidateMandatoryObjs : ENTRY \n");

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);

    switch (RSVP_HDR_MSG_TYPE (pRsvpHdr))
    {
        case PATH_MSG:
            if (((PKT_MAP_RSVP_HOP_OBJ (pPktMap) == NULL) &&
                 (pPktMap->pIfIdRsvpHopObj == NULL) &&
                 (pPktMap->pIfIdRsvpNumHopObj == NULL)) ||
                (PKT_MAP_TIME_VALUES_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_RPTE_SND_TMP_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_SENDER_TSPEC_OBJ (pPktMap) == NULL) ||
                ((pPktMap->pGenLblReqObj != NULL) &&
                 (pPktMap->pLblReqAtmRngObj != NULL)))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in Path Msg\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            if ((pPktMap->pGenLblReqObj == NULL) &&
                (pPktMap->pLblReqAtmRngObj == NULL) &&
                (gi4MplsSimulateFailure != RPTE_SIM_FAILURE_NO_LBL_REQ))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in Path Msg\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            if (PKT_MAP_RPTE_SSN_OBJ (pPktMap) == NULL)
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in Path Msg\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in the Path Msg\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;

        case PATHTEAR_MSG:
            if ((PKT_MAP_RPTE_SSN_OBJ (pPktMap) == NULL) ||
                ((PKT_MAP_RSVP_HOP_OBJ (pPktMap) == NULL) &&
                 (pPktMap->pIfIdRsvpHopObj == NULL) &&
                 (pPktMap->pIfIdRsvpNumHopObj == NULL)) ||
                (PKT_MAP_RPTE_SND_TMP_OBJ (pPktMap) == NULL))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in PathTear Msg\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in the PathTear "
                        "Msg\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;

        case PATHERR_MSG:
            if ((PKT_MAP_RPTE_SSN_OBJ (pPktMap) == NULL) ||
                ((PKT_MAP_ERROR_SPEC_OBJ (pPktMap) == NULL) &&
                 (pPktMap->pIfIdRsvpErrObj == NULL) &&
                 (pPktMap->pIfIdRsvpNumErrObj == NULL)) ||
                (PKT_MAP_RPTE_SND_TMP_OBJ (pPktMap) == NULL))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in PathErr Msg\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in the PathErr "
                        "Msg\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;

        case RESV_MSG:
            if ((PKT_MAP_RPTE_SSN_OBJ (pPktMap) == NULL) ||
                ((PKT_MAP_RSVP_HOP_OBJ (pPktMap) == NULL) &&
                 (pPktMap->pIfIdRsvpHopObj == NULL) &&
                 (pPktMap->pIfIdRsvpNumHopObj == NULL)) ||
                (PKT_MAP_TIME_VALUES_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_FLOW_SPEC_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_RPTE_FILTER_SPEC_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_STYLE_OBJ (pPktMap) == NULL))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in Resv Msg\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in the Resv Msg\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;

        case RESVTEAR_MSG:
            if ((PKT_MAP_RPTE_SSN_OBJ (pPktMap) == NULL) ||
                ((PKT_MAP_RSVP_HOP_OBJ (pPktMap) == NULL) &&
                 (pPktMap->pIfIdRsvpHopObj == NULL) &&
                 (pPktMap->pIfIdRsvpNumHopObj == NULL)) ||
                (PKT_MAP_FLOW_SPEC_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_RPTE_FILTER_SPEC_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_STYLE_OBJ (pPktMap) == NULL))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in ResvTear Msg\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in the ResvTear "
                        "Msg\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;

        case RESVERR_MSG:
            if ((PKT_MAP_RPTE_SSN_OBJ (pPktMap) == NULL) ||
                ((PKT_MAP_RSVP_HOP_OBJ (pPktMap) == NULL) &&
                 (pPktMap->pIfIdRsvpHopObj == NULL) &&
                 (pPktMap->pIfIdRsvpNumHopObj == NULL)) ||
                ((PKT_MAP_ERROR_SPEC_OBJ (pPktMap) == NULL) &&
                 (pPktMap->pIfIdRsvpErrObj == NULL) &&
                 (pPktMap->pIfIdRsvpNumErrObj == NULL)) ||
                (PKT_MAP_FLOW_SPEC_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_RPTE_FILTER_SPEC_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_STYLE_OBJ (pPktMap) == NULL))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in ResvError "
                            "Msg\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in the ResvErr "
                        "Msg\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;

        case RESVCONF_MSG:
            if ((PKT_MAP_RPTE_SSN_OBJ (pPktMap) == NULL) ||
                ((PKT_MAP_ERROR_SPEC_OBJ (pPktMap) == NULL) &&
                 (pPktMap->pIfIdRsvpErrObj == NULL) &&
                 (pPktMap->pIfIdRsvpNumErrObj == NULL)) ||
                (PKT_MAP_RESV_CONF_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_FLOW_SPEC_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_STYLE_OBJ (pPktMap) == NULL))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in ResvConf Msg\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in the ResvConf "
                        "Msg\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;

        case HELLO_MSG:
            if (PKT_MAP_HELLO_OBJ (pPktMap) == NULL)
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in Hello Msg...\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in the Hello "
                        "Msg...\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;

        case ACK_MSG:
            if ((TMO_SLL_Count (&RPTE_PKT_MAP_MSG_ID_ACK_LIST (pPktMap))
                 == RPTE_ZERO) &&
                (TMO_SLL_Count (&RPTE_PKT_MAP_MSG_ID_NACK_LIST (pPktMap))
                 == RPTE_ZERO))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in Ack Msg...\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in Ack Msg...\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;

        case SREFRESH_MSG:
            if (RPTE_PKT_MAP_MSG_ID_LIST (pPktMap) == NULL)
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in SRefresh "
                            "Msg...\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in the SRefresh "
                        "Msg...\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;

        case NOTIFY_MSG:
            if ((PKT_MAP_RPTE_SSN_OBJ (pPktMap) == NULL) &&
                (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) == RSVPTE_ZERO)
                && (PKT_MAP_ERROR_SPEC_OBJ (pPktMap) == NULL)
                && ((PKT_MAP_FLOW_SPEC_OBJ (pPktMap) == NULL)
                    || (PKT_MAP_RPTE_SND_TMP_OBJ (pPktMap) == NULL)))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in Notify Message \n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "All the mandatory objects present in the Notify Msg\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmValidateMandatoryObjs : EXIT \n");
            return RPTE_SUCCESS;
        case RECOVERY_PATH_MSG:
            if (((PKT_MAP_RSVP_HOP_OBJ (pPktMap) == NULL) &&
                 (pPktMap->pIfIdRsvpHopObj == NULL) &&
                 (pPktMap->pIfIdRsvpNumHopObj == NULL)) ||
                (PKT_MAP_TIME_VALUES_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_RPTE_SND_TMP_OBJ (pPktMap) == NULL) ||
                (PKT_MAP_SENDER_TSPEC_OBJ (pPktMap) == NULL) ||
                ((pPktMap->pGenLblReqObj != NULL) &&
                 (pPktMap->pLblReqAtmRngObj != NULL)))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "-E- : Mandatory Object missing in Path Msg\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            return RPTE_SUCCESS;
        default:
            break;
    }
    RSVPTE_DBG (RSVPTE_PVM_PRCS, "-E- : Unknown Msg Type\n");
    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                "RptePvmValidateMandatoryObjs : INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RptePvmValidatePkt                                     */
/* Description     : This function validates the packet in pMsgBuf and fills*/
/*                   the appropriate fields in the pPktMap. If the packet is*/
/*                   is of not type Path or PathTear or ResvConf and the    */
/*                   destination address doesn't match any of the local     */
/*                   addresses, then the packet is forwarded to the         */
/*                   destination                                            */
/* Input (s)       : *pMsgBuf - Pointer to Buffer                           */
/*                   *pPktMap - Pointer to Packet Map                       */
/* Output (s)      : None                                                   */
/* Returns         : If the message is forwarded, then ERR_SENT is          */
/*                   returned. If the packet is invalid then RPTE_FAILURE is*/
/*                   returned. If the packet is valid, then RPTE_SUCCESS is */
/*                   returned.                                              */
/****************************************************************************/
PRIVATE INT1
RptePvmValidatePkt (tPktMap * pPktMap)
{
    tRsvpHdr           *pRsvpHdr = NULL;
    UINT2               u2CheckSum;
    UINT2               u2CalCheckSum;
    INT1                i1RetVal;
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidatePkt : ENTRY \n");

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);

    if (RSVP_HDR_CHECK_SUM (pRsvpHdr) != RSVPTE_ZERO)
    {
        u2CheckSum = RSVP_HDR_CHECK_SUM (pRsvpHdr);

        RSVP_HDR_CHECK_SUM (pRsvpHdr) = RSVPTE_ZERO;

        u2CalCheckSum = (UINT2) RpteUtlCheckSum ((UINT1 *) pRsvpHdr,
                                                 (UINT2)
                                                 OSIX_NTOHS (RSVP_HDR_LENGTH
                                                             (pRsvpHdr)));
        if (u2CheckSum != u2CalCheckSum)
        {
            RSVPTE_DBG2 (RSVPTE_PVM_PRCS,
                         " Invalid Checksum RxCSum = %x CalCsum = %x\n",
                         OSIX_HTONS (u2CheckSum), OSIX_HTONS (u2CalCheckSum));
            RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidatePkt : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
    }

    if (((RSVP_HDR_VER_FLAG (pRsvpHdr) & RSVP_HDR_VER_MASK)
         >> RSHIFT_4BITS) != RSVP_VERSION)
    {
        RSVPTE_DBG (RSVPTE_PVM_PRCS, "-E- : Invalid RSVP Protocol Version\n");
        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidatePkt : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    switch (RSVP_HDR_MSG_TYPE (pRsvpHdr))
    {
        case PATH_MSG:
        case RESV_MSG:
        case PATHERR_MSG:
        case RESVERR_MSG:
        case PATHTEAR_MSG:
        case RESVTEAR_MSG:
        case RESVCONF_MSG:
        case HELLO_MSG:
        case NOTIFY_MSG:
        case RECOVERY_PATH_MSG:
            break;

        case SREFRESH_MSG:
            if (gu1RRCapable != RPTE_ENABLED)
            {
                RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                             "-E- : Invalid Msg Type - Msg Type = %d\n",
                             RSVP_HDR_MSG_TYPE (pRsvpHdr));
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidatePkt : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            break;

        case ACK_MSG:
            if (gu1MsgIdCapable != RPTE_ENABLED)
            {
                RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                             "-E- : Invalid Msg Type - Msg Type = %d\n",
                             RSVP_HDR_MSG_TYPE (pRsvpHdr));
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmValidatePkt : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            break;

        default:
            RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                         "-E- : Invalid Msg Type - Msg Type = %d\n",
                         RSVP_HDR_MSG_TYPE (pRsvpHdr));
            RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidatePkt : INTMD-EXIT \n");
            return RPTE_FAILURE;
    }

    if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) != PATH_MSG) &&
        (RSVP_HDR_MSG_TYPE (pRsvpHdr) != PATHTEAR_MSG) &&
        (RSVP_HDR_MSG_TYPE (pRsvpHdr) != RESVCONF_MSG))
    {
        /* Msg Type is NOT Path/PathTear/ResvConf */
        if (RptePvmLocalAddr (PKT_MAP_DST_ADDR (pPktMap)) != RPTE_SUCCESS)
        {
            /* Destination Address doesn't match local address */
            if (RpteUtlMcastAddr (PKT_MAP_DST_ADDR (pPktMap)) == RPTE_YES)
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS, "-E- : Multicast Dst Addr\n");
            }
            else
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS, "-E- : Unicast Dst Addr\n");
            }
            RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidatePkt : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
    }

    i1RetVal = RptePvmValidateObj (pPktMap);
    if ((i1RetVal == RPTE_FAILURE))
    {
        RSVPTE_DBG (RSVPTE_PVM_PRCS, "-E- : Objects Validation Failed\n");
        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidatePkt : INTMD-EXIT \n");
        return i1RetVal;
    }

    if (RptePvmValidateMandatoryObjs (pPktMap) != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                    "-E- : Mandatory Objects Validation Failed\n");
        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidatePkt : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (PKT_MAP_UNKNOWN_OBJ_HDR (pPktMap) != NULL)
    {
        /* Processing the Unknown object in the msg */
        i1RetVal = RptePvmHandleUnknownObj (pPktMap);
        if ((i1RetVal == RPTE_FAILURE) || (i1RetVal == ERR_SENT))
        {
            RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidatePkt : INTMD-EXIT \n");
            return i1RetVal;
        }
    }
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmValidatePkt : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePvmProcessPkt                                      */
/* Description     : This function processes the received RSVP packet. This */
/*                   function takes in PktMap as the input. The PktMap      */
/*                   contains the RSVP message in a linear buffer.          */
/* Input (s)       : *pPktMap - Pointer to the Input PktMap                 */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
INT1
RptePvmProcessPkt (tPktMap * pPktMap)
{
    tRsvpHdr           *pRsvpHdr = NULL;
    tObjHdr            *pObjHdr = NULL;
    INT1                i1RetVal;
    tNbrEntry          *pNbrEntry = NULL;
    tRsvpHop           *pRsvpHop = NULL;
    UINT1               u1VersionFlag;
    UINT1               u1Encaps;
    UINT4               u4RetVal;
    UINT4               u4HopAddr = RSVPTE_ZERO;
    tuTrieInfo         *pTrieInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tMsgBuf            *pMsgBuf = NULL;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmProcessPkt : ENTRY \n");

    pMsgBuf = PKT_MAP_MSG_BUF (pPktMap);
    u1Encaps = PKT_MAP_ENCAPS_TYPE (pPktMap);
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    if (RSVP_HDR_MSG_TYPE (pRsvpHdr) == NOTIFY_MSG)
    {
        RpteNhProcessNotifyMsg (pPktMap);
        return RPTE_SUCCESS;
    }

    if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) == ACK_MSG) &&
        (PKT_MAP_IF_ENTRY (pPktMap) == NULL))
    {
        pObjHdr =
            (tObjHdr *) (VOID *) (((UINT1 *) pRsvpHdr) + sizeof (tRsvpHdr));
        RptePvmValidateRORObj (pPktMap, pObjHdr);
        /* Msg Id Acks are processed for Notify msg */
        RpteMIHProcessMsgIdAcks (NULL,
                                 &RPTE_PKT_MAP_MSG_ID_ACK_LIST (pPktMap),
                                 PKT_MAP_SRC_ADDR (pPktMap));
        return RPTE_SUCCESS;
    }
    i1RetVal = RptePvmValidatePkt (pPktMap);
    if (i1RetVal == RPTE_FAILURE)
    {
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                    "Packet Validation Failed, Clean PktMap\n");
        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmProcessPkt : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (i1RetVal == ERR_SENT)
    {
        RSVPTE_DBG (RSVPTE_PVM_PRCS, "Packet Validation Failed, Err Sent\n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmProcessPkt : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    u1VersionFlag =
        RSVP_HDR_VER_FLAG ((tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap));

    /* If the RSVPTE Is not enabled in this interface/interface is down,
     * don't process path message, let the path gets tore down subsequently.
     */
    if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATH_MSG) &&
        ((IF_ENTRY_ENABLED (PKT_MAP_IF_ENTRY (pPktMap))) == RPTE_DISABLED))
    {
        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                    "RptePvmProcessPkt : RSVPTE is Disabled in this interface INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATH_MSG) ||
        (RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATHTEAR_MSG) ||
        (RSVP_HDR_MSG_TYPE (pRsvpHdr) == RESV_MSG) ||
        (RSVP_HDR_MSG_TYPE (pRsvpHdr) == RESVTEAR_MSG) ||
        (RSVP_HDR_MSG_TYPE (pRsvpHdr) == RESVERR_MSG) ||
        (RSVP_HDR_MSG_TYPE (pRsvpHdr) == RECOVERY_PATH_MSG))
    {
        if (pPktMap->pRsvpHopObj != NULL)
        {
            pRsvpHop = &RSVP_HOP_OBJ (PKT_MAP_RSVP_HOP_OBJ (pPktMap));
            u4HopAddr = OSIX_NTOHL (RSVP_HOP_ADDR (pRsvpHop));
        }
        else if (pPktMap->pIfIdRsvpHopObj != NULL)
        {
            pRsvpHop = &pPktMap->pIfIdRsvpHopObj->RsvpHop;
            u4HopAddr = OSIX_NTOHL (RSVP_HOP_ADDR (pRsvpHop));
        }
        else if (pPktMap->pIfIdRsvpNumHopObj != NULL)
        {
            pRsvpHop = &pPktMap->pIfIdRsvpNumHopObj->RsvpHop;
            u4HopAddr = OSIX_NTOHL (RSVP_HOP_ADDR (pRsvpHop));
        }

        pNbrEntry = RptePvmMatchNbrAddr (PKT_MAP_IF_ENTRY (pPktMap), u4HopAddr);

        if (pNbrEntry == NULL)
        {
            pRsvpHop = &RSVP_HOP_OBJ (PKT_MAP_RSVP_HOP_OBJ (pPktMap));
            pNbrEntry = RptePvmCreateNbrTableEntry (PKT_MAP_IF_ENTRY (pPktMap),
                                                    u4HopAddr,
                                                    u1Encaps, u1VersionFlag);
            if (pNbrEntry == NULL)
            {
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmProcessPkt : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
        }
        else
        {
            /* Neighbour is already present. Should update the neighbour
             * entry according to its current behaviour. */
            RpteRRUpdateNbrEntry (pNbrEntry, u1VersionFlag);
        }
    }
    else if ((RSVP_HDR_MSG_TYPE (pRsvpHdr) == PATHERR_MSG) ||
             (RSVP_HDR_MSG_TYPE (pRsvpHdr) == SREFRESH_MSG) ||
             (RSVP_HDR_MSG_TYPE (pRsvpHdr) == ACK_MSG))
    {
        pNbrEntry = RptePvmMatchNbrAddr (PKT_MAP_IF_ENTRY (pPktMap),
                                         PKT_MAP_SRC_ADDR (pPktMap));
        if (pNbrEntry == NULL)
        {
            pNbrEntry = RptePvmCreateNbrTableEntry (PKT_MAP_IF_ENTRY (pPktMap),
                                                    PKT_MAP_SRC_ADDR (pPktMap),
                                                    u1Encaps, u1VersionFlag);
            if (pNbrEntry == NULL)
            {
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmProcessPkt : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
        }
        else
        {
            /* Neighbour is already present. Should update the neighbour
             * entry according to its current behaviour. */
            RpteRRUpdateNbrEntry (pNbrEntry, u1VersionFlag);
        }
    }

    RPTE_PKT_MAP_NBR_ENTRY (pPktMap) = pNbrEntry;

    if (TMO_SLL_Count (&RPTE_PKT_MAP_MSG_ID_ACK_LIST (pPktMap)) != RSVPTE_ZERO)
    {
        /* Msg Id Acks are processed */
        RpteMIHProcessMsgIdAcks (pNbrEntry,
                                 &RPTE_PKT_MAP_MSG_ID_ACK_LIST (pPktMap),
                                 PKT_MAP_SRC_ADDR (pPktMap));
    }

    if (TMO_SLL_Count (&RPTE_PKT_MAP_MSG_ID_NACK_LIST (pPktMap)) != RSVPTE_ZERO)
    {
        /* Msg Id Nacks are processed */
        RpteMIHProcessMsgIdNacks (pNbrEntry,
                                  &RPTE_PKT_MAP_MSG_ID_NACK_LIST (pPktMap));
    }

    if ((RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RSVPTE_ZERO) &&
        (RSVP_HDR_MSG_TYPE (pRsvpHdr) != NOTIFY_MSG))
    {
        /* If Ack Desired Flag is set, Ack Message is sent
         * immediately */
        if (((RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) & RPTE_MSG_ID_ACK_DESIRED) ==
             RPTE_MSG_ID_ACK_DESIRED) ||
            ((RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) & RPTE_MSG_ID_RECOVERY_PATH)
             == RPTE_MSG_ID_RECOVERY_PATH))
        {
            RpteMIHSendAckMsg (pNbrEntry, pPktMap);
        }

        /* Since Msg Id and RSVP_HOP Info are retrieved,
         * Message Id Handler is called.
         * If the message is a Trigger Message or the Epoc Value has
         * changed, the neighbour is updated accordingly in Process MsgId
         * function.
         * If it is a refresh message, then the message related refresh
         * processing is called in Process MsgId function. */
        u4RetVal = RpteMIHProcessMsgId (pNbrEntry, RPTE_PKT_MAP_MSG_ID_EPOCH
                                        (pPktMap),
                                        RPTE_PKT_MAP_MSG_ID (pPktMap),
                                        &pTrieInfo);

        if ((u4RetVal == RPTE_TRIG_MSG) ||
            (u4RetVal == RPTE_EPOCH_CHANGE) ||
            (u4RetVal == RPTE_MSG_ID_NOT_IN_DB))
        {
            /* Since this is a new message id, the processing should
             * continue. */
        }
        else
        {
            switch (RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo))
            {
                case RPTE_PATH_INCOMING:

                    /* Extract the RsvpTe Tnl Info from the TRIE Info */
                    pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);
                    PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                        RpteUtlComputeLifeTime (PKT_MAP_IF_ENTRY (pPktMap),
                                                PSB_IN_REFRESH_INTERVAL
                                                (RSVPTE_TNL_PSB
                                                 (pRsvpTeTnlInfo)));
                    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) ==
                        RPTE_EGRESS)
                    {
                        RpteRhPhStartMaxWaitTmr (pRsvpTeTnlInfo,
                                                 PSB_TIME_TO_DIE
                                                 (RSVPTE_TNL_PSB
                                                  (pRsvpTeTnlInfo)));
                    }
                    PSB_RX_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                        PKT_MAP_RX_TTL (pPktMap);
                    PSB_REMAINING_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                        RSVP_HDR_SEND_TTL (pRsvpHdr);
                    break;

                case RPTE_RESV_INCOMING:

                    /* Extract the RsvpTe Tnl Info from the TRIE Info */
                    pRsvpTeTnlInfo = RPTE_TRIE_INFO_TNL_INFO (pTrieInfo);
                    RSB_TIME_TO_DIE (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)) =
                        RpteUtlComputeLifeTime (PKT_MAP_IF_ENTRY (pPktMap),
                                                RSB_IN_REFRESH_INTERVAL
                                                (RSVPTE_TNL_RSB
                                                 (pRsvpTeTnlInfo)));
                    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) ==
                        RPTE_INGRESS)
                    {
                        RpteRhPhStartMaxWaitTmr (pRsvpTeTnlInfo,
                                                 RSB_TIME_TO_DIE
                                                 (RSVPTE_TNL_RSB
                                                  (pRsvpTeTnlInfo)));
                    }
                    break;

                default:
                    break;
            }
            RpteUtlCleanPktMap (pPktMap);
            return RPTE_SUCCESS;
        }
    }

    switch (RSVP_HDR_MSG_TYPE (pRsvpHdr))
    {
        case PATH_MSG:
            RptePhProcessPathMsg (pPktMap);
            break;

        case PATHTEAR_MSG:
            RptePthProcessPathTearMsg (pPktMap);
            break;

        case PATHERR_MSG:
            RptePehProcessPathErrMsg (pPktMap);
            break;

        case RESV_MSG:
            RpteRhProcessResvMsg (pPktMap);
            break;

        case RESVTEAR_MSG:
            RpteRthProcessResvTearMsg (pPktMap);
            break;

        case RESVERR_MSG:
            RpteRehProcessResvErrMsg (pPktMap);
            break;

        case RESVCONF_MSG:
            if (PKT_MAP_ENCAPS_TYPE (pPktMap) == IP_ENCAP)
            {
                CRU_BUF_Move_ValidOffset (pMsgBuf,
                                          IP_HDR_LENGTH
                                          ((tRsvpIpHdr *) (VOID *)
                                           PKT_MAP_IP_PKT (pPktMap)));
            }

            RpteRchProcessResvConfMsg (pPktMap);
            break;

        case HELLO_MSG:
            RpteHhProcessHelloMsg (pPktMap);
            break;

        case SREFRESH_MSG:
            RpteMIHProcessSRefreshMsg (pPktMap);
            break;

        case ACK_MSG:
            RpteUtlCleanPktMap (pPktMap);
            break;

        case NOTIFY_MSG:
            RpteNhProcessNotifyMsg (pPktMap);
            break;

        case RECOVERY_PATH_MSG:
            RpteGrProcessRecoveryPathMsg (pPktMap);
            break;

        default:
            RpteUtlCleanPktMap (pPktMap);
            break;
    }                            /* end of switch */
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmProcessPkt : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePvmExtractEROList
 * Description     : This function extracts the Explicit route object from the
 *                   message buffer.
 * Input (s)       : pPktMap - Pointer to Packet map 
 *                   ppObjHdr - Pointer to data buffer
 *                   pEndOfPkt - Pointer to End of packet
 * Output (s)      : None                                                   
 * Returns         : RPTE_SUCCESS on Success, RPTE_FAILURE on Failure. 
 */
/*---------------------------------------------------------------------------*/
INT1
RptePvmExtractEROList (tPktMap * pPktMap, tObjHdr ** ppObjHdr, UINT1 *pEndOfPkt)
{
    UINT1               u1EROType = RSVPTE_ZERO;
    UINT1               u1Type = RSVPTE_ZERO;
    UINT1              *pu1Pdu = NULL;
    UINT1              *pEROEnd = NULL;
    UINT4               u4Label = RSVPTE_ZERO;
    UINT1               u1UBitAndRsvd = RSVPTE_ZERO;
    UINT4               u4TempAddr = RSVPTE_ZERO;
    tObjHdr            *pObjHdr = NULL;
    UINT4               u4TempTeHopIndx = RSVPTE_ONE;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    tRsvpTeHopInfo     *pTempRsvpTeErHopInfo = NULL;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmExtractEROList : ENTRY \n");

    pObjHdr = *ppObjHdr;
    pEndOfPkt = (UINT1 *) pEndOfPkt;

    pEROEnd =
        (UINT1 *) ((UINT1 *) pObjHdr + OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)));
    TMO_SLL_Init (&PKT_MAP_RPTE_ERO_LIST (pPktMap));

    MEM_COPY ((tObjHdr *) pObjHdr, &PKT_MAP_RPTE_ERO_HDR (pPktMap),
              sizeof (tObjHdr));

    pu1Pdu = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));

    while (pu1Pdu < pEROEnd)
    {
        /* Validate Sub object length */
        if (VALIDATE_SUBOBJ_LEN (pu1Pdu))
        {
            return RPTE_FAILURE;
        }
        u1EROType = *(UINT1 *) pu1Pdu;
        u1EROType &= ERO_TYPE_SET_VALUE;

        /* Validation for ERO IPV4 numbered object */
        if ((u1EROType == ERO_TYPE_IPV4_ADDR) ||
            (u1EROType == ERO_TYPE_UNNUM_IF))
        {
            pRsvpTeErHopInfo = (tRsvpTeHopInfo *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_ERHOP_POOL_ID);

            if (pRsvpTeErHopInfo == NULL)
            {
                RSVPTE_DBG (RSVPTE_MAIN_MEM,
                            "Err - Memory allocation failed in ERO processing\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmExtractEROList : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            MEMSET (pRsvpTeErHopInfo, RSVPTE_ZERO, sizeof (tRsvpTeHopInfo));
            pRsvpTeErHopInfo->GmplsTnlHopInfo.u4ForwardLbl = MPLS_INVALID_LABEL;
            pRsvpTeErHopInfo->GmplsTnlHopInfo.u4ReverseLbl = MPLS_INVALID_LABEL;
            pRsvpTeErHopInfo->u1RowStatus = RPTE_ACTIVE;

            TMO_SLL_Init_Node (&(pRsvpTeErHopInfo->NextHop));
            TMO_SLL_Add (&pPktMap->ErObj.ErHopList, &pRsvpTeErHopInfo->NextHop);

            /* Extract Type Field */
            RPTE_EXT_1_BYTE (pu1Pdu, u1Type);

            /* Extract Hop Type - Strict = 0 or loose = 1 */
            if ((u1Type & ERO_TYPE_MASK) == ERO_TYPE_STRICT_SET)
            {
                pRsvpTeErHopInfo->u1HopType = ERO_TYPE_STRICT;
            }
            else if ((u1Type & ERO_TYPE_MASK) == ERO_TYPE_LOOSE_SET)
            {
                pRsvpTeErHopInfo->u1HopType = ERO_TYPE_LOOSE;
            }
            else
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "Err - Bad ErHop Type in ERO processing\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmExtractEROList : INTMD-EXIT \n");

                MemReleaseMemBlock (RSVPTE_ERHOP_POOL_ID,
                                    (UINT1 *) pRsvpTeErHopInfo);
                pPktMap->u1EroErrType = RPTE_BAD_INIT_SUB_OBJ;
                break;
            }

            pRsvpTeErHopInfo->u1AddressType = (u1Type & ERO_TYPE_SET_VALUE);

            /* Extract ERO Length */
            pu1Pdu += sizeof (UINT1);

            if (u1EROType == ERO_TYPE_IPV4_ADDR)
            {
                /* Extract ERO Ip Addr */
                RPTE_EXT_4_BYTES (pu1Pdu, u4TempAddr);
                u4TempAddr = OSIX_HTONL (u4TempAddr);
                MEMCPY (&pRsvpTeErHopInfo->IpAddr, &u4TempAddr, sizeof (UINT4));

                /* Extract Prefix Len */
                RPTE_EXT_1_BYTE (pu1Pdu, pRsvpTeErHopInfo->u1AddrPrefixLen);

                /* Extract Reserved Field */
                pu1Pdu += sizeof (UINT1);
            }
            else
            {
                /* Extract Reserved Field */
                pu1Pdu += sizeof (UINT2);

                /* Extract ERO Ip Addr */
                RPTE_EXT_4_BYTES (pu1Pdu, u4TempAddr);
                u4TempAddr = OSIX_HTONL (u4TempAddr);
                MEMCPY (&pRsvpTeErHopInfo->IpAddr, &u4TempAddr, sizeof (UINT4));

                /* Extract Interface Identifier. */
                RPTE_EXT_4_BYTES (pu1Pdu, pRsvpTeErHopInfo->u4UnnumIf);
            }
            pTempRsvpTeErHopInfo = pRsvpTeErHopInfo;
            RSVPTE_ERHOP_INDEX (pRsvpTeErHopInfo) = u4TempTeHopIndx++;
        }
        /* Validation for ERO Label sub object */
        else if (u1EROType == ERO_TYPE_LBL_OBJ)
        {
            /* Extract Type Field */
            RPTE_EXT_1_BYTE (pu1Pdu, u1Type);

            /* Extract Length Field */
            pu1Pdu += sizeof (UINT1);

            /* Extract Reserved and U-Bit Field */
            RPTE_EXT_1_BYTE (pu1Pdu, u1UBitAndRsvd);

            /* Extract C-Type Field */
            pu1Pdu += sizeof (UINT1);

            /* Extract Label Field */
            RPTE_EXT_4_BYTES (pu1Pdu, u4Label);

            /* AS per RFC 3473, section 5.1.1,
             * The following SHOULD result in "Bad EXPLICIT_ROUTE Object" Error.
             *
             * 1. If the first label subobject is not preceded by a subobject
             *    containing an IP address, or an interface identifier
             *    associated with an output link.
             * 2. For a label subobject to follow a subobject that has the
             *    L-bit set.
             * 3. On unidirectional LSP setup, for there to be a label subobject
             *    with the U-bit set
             * 4. For there to be two label subobjects with the same U-bit
             *    values */
            if ((pTempRsvpTeErHopInfo == NULL) ||
                ((u1Type & ERO_TYPE_MASK) == ERO_TYPE_LOOSE_SET) ||
                ((u1UBitAndRsvd & ERO_TYPE_MASK) &&
                 (pRsvpTeErHopInfo->GmplsTnlHopInfo.u1LblStatus &
                  ERO_TYPE_LBL_REV)) ||
                (((u1UBitAndRsvd & ERO_TYPE_MASK) == RSVPTE_ZERO) &&
                 (pRsvpTeErHopInfo->GmplsTnlHopInfo.u1LblStatus &
                  ERO_TYPE_LBL_FWD)))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "Err - Bad ErHop in ERO processing\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmExtractEROList : INTMD-EXIT \n");
                pPktMap->u1EroErrType = RPTE_BAD_EXPLICT_ROUTE_OBJ;
                break;
            }
            pRsvpTeErHopInfo = pTempRsvpTeErHopInfo;

            u1UBitAndRsvd &= ERO_TYPE_MASK;

            /* if "U" bit is set, the label sub object contains the label
             * for reverse direction */
            if (u1UBitAndRsvd)
            {
                pRsvpTeErHopInfo->GmplsTnlHopInfo.u1LblStatus
                    |= ERO_TYPE_LBL_REV;
                pRsvpTeErHopInfo->GmplsTnlHopInfo.u4ReverseLbl = u4Label;
            }
            /* if "U" bit is not set, the label sub object contains the label
             * for forward direction */
            else
            {
                pRsvpTeErHopInfo->GmplsTnlHopInfo.u1LblStatus
                    |= ERO_TYPE_LBL_FWD;
                pRsvpTeErHopInfo->GmplsTnlHopInfo.u4ForwardLbl = u4Label;
            }
        }
        else
        {
            /* Note: Current implementation only supports IPv4 Type subobjects,
             * any other Type of subobjects treated as erroneous type. */
            RSVPTE_DBG (RSVPTE_PVM_PRCS,
                        "Err : UnSupported ERO SubObject Type\n");
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmExtractEROList : INTMD-EXIT \n");
            pPktMap->u1EroErrType = RPTE_BAD_INIT_SUB_OBJ;
            break;
        }
    }

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmExtractEROList : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePvmExtractRROList
 * Description     : This function extracts the Record route object from the
 *                   message buffer.
 * Input (s)       : pPktMap - pointer to the packet map. 
 *                   ppObjHdr - pointer to the data buffer. 
 *                   pEndOfPkt - pointer to the end of packet.
 * Output (s)      : None                                                   
 * Returns         : RPTE_SUCCESS on Success, RPTE_FAILURE on Failure. 
 */
/*---------------------------------------------------------------------------*/
INT1
RptePvmExtractRROList (tPktMap * pPktMap, tObjHdr ** ppObjHdr, UINT1 *pEndOfPkt)
{

    UINT1              *pRROEnd = NULL;
    UINT1              *pu1Pdu = NULL;
    UINT1              *pTmpPtr = NULL;
    UINT4               u4Label = MPLS_INVALID_LABEL;
    UINT4               u4TempAddr = RSVPTE_ZERO;
    UINT1               u1SubObjSize = (UINT1) RSVPTE_ZERO;
    UINT1               u1Flags = RSVPTE_ZERO;
    UINT1               u1UBit = (UINT1) RSVPTE_ZERO;
    UINT1               u1GblFlag = (UINT1) RSVPTE_ZERO;
    tObjHdr            *pObjHdr = NULL;
    UINT4               u4TempArHopIndx = RSVPTE_ONE;
    tRsvpTeArHopInfo   *pRsvpTeArHopInfo = NULL;
    tNewSubObjInfo     *pNewSubObjInfo = NULL;
    tRsvpTeArHopInfo   *pTempRsvpTeArHopInfo = NULL;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmExtractRROList : ENTRY \n");

    pObjHdr = *ppObjHdr;
    pEndOfPkt = (UINT1 *) pEndOfPkt;
    pRROEnd =
        (UINT1 *) ((UINT1 *) pObjHdr + OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)));

    TMO_SLL_Init (&pPktMap->RrObj.RrHopList);
    TMO_SLL_Init (&pPktMap->RrObj.NewSubObjList);

    MEM_COPY ((tObjHdr *) pObjHdr, &PKT_MAP_RPTE_RRO_HDR (pPktMap),
              sizeof (tObjHdr));

    pu1Pdu = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));
    while (pu1Pdu < pRROEnd)
    {

        /* Validate Sub object length */
        if (VALIDATE_SUBOBJ_LEN (pu1Pdu))
        {
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "RptePvmExtractRROList : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        if ((*pu1Pdu == RRO_TYPE_IPV4_ADDR) || (*pu1Pdu == RRO_TYPE_UNNUM_IF))
        {
            pRsvpTeArHopInfo = (tRsvpTeArHopInfo *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_RRHOP_POOL_ID);

            if (pRsvpTeArHopInfo == NULL)
            {
                RSVPTE_DBG (RSVPTE_MAIN_MEM,
                            "Err - Memory allocation failed in RRO processing");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmExtractRROList : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            MEMSET (pRsvpTeArHopInfo, RSVPTE_ZERO, sizeof (tRsvpTeArHopInfo));
            pRsvpTeArHopInfo->GmplsTnlArHopInfo.u4ForwardLbl =
                MPLS_INVALID_LABEL;
            pRsvpTeArHopInfo->GmplsTnlArHopInfo.u4ReverseLbl =
                MPLS_INVALID_LABEL;
            TMO_SLL_Init_Node (&(pRsvpTeArHopInfo->NextHop));
            TMO_SLL_Add (&pPktMap->RrObj.RrHopList, &pRsvpTeArHopInfo->NextHop);

            /* Extract RRO Type */
            RPTE_EXT_1_BYTE (pu1Pdu, pRsvpTeArHopInfo->u1AddressType);

            /* Extract RRO Length */
            pu1Pdu += sizeof (UINT1);

            if (pRsvpTeArHopInfo->u1AddressType == RRO_TYPE_IPV4_ADDR)
            {
                /* Extract RRO Ip Addr */
                RPTE_EXT_4_BYTES (pu1Pdu, u4TempAddr);
                u4TempAddr = OSIX_HTONL (u4TempAddr);
                MEMCPY (&pRsvpTeArHopInfo->IpAddr, &u4TempAddr, sizeof (UINT4));

                /* Extract RRO Ip Addr Prefix Len */
                RPTE_EXT_1_BYTE (pu1Pdu, pRsvpTeArHopInfo->u1AddrPrefixLen);

                /* Extract RRO Flags */
                RPTE_EXT_1_BYTE (pu1Pdu, u1Flags);
            }
            else
            {
                /* Extract RRO Flags */
                RPTE_EXT_1_BYTE (pu1Pdu, u1Flags);

                /* Extract Reserved Bits */
                pu1Pdu += sizeof (UINT1);

                /* Extract RRO Router Ip Addr */
                RPTE_EXT_4_BYTES (pu1Pdu, u4TempAddr);
                u4TempAddr = OSIX_HTONL (u4TempAddr);
                MEMCPY (&pRsvpTeArHopInfo->IpAddr, &u4TempAddr, sizeof (UINT4));

                /* Extract RRO Interface Id */
                RPTE_EXT_4_BYTES (pu1Pdu, pRsvpTeArHopInfo->u4UnnumIf);
            }

            if (u1Flags & LOCAL_PROT_AVAIL)
            {
                pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1Protection
                    |= (LOCAL_PROT_AVAIL << 7);
                pRsvpTeArHopInfo->u1ProtType |= TE_TNL_FRR_PROTECTION_LINK;
            }

            if (u1Flags & FRR_NODE_PROT)
            {
                pRsvpTeArHopInfo->u1ProtType |= TE_TNL_FRR_PROTECTION_NODE;
            }

            if (u1Flags & LOCAL_PROT_IN_USE)
            {
                pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1Protection
                    |= (LOCAL_PROT_IN_USE << 5);
                pRsvpTeArHopInfo->u1ProtTypeInUse
                    = pRsvpTeArHopInfo->u1ProtType;
            }
            pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1ProtLen = sizeof (UINT1);

            if (u1Flags & FRR_BW_PROT)
            {
                pRsvpTeArHopInfo->u1BwProtAvailable = TRUE;
            }
            pRsvpTeArHopInfo->u1Flags = u1Flags;
            pRsvpTeArHopInfo->u4TnlArHopIndex = u4TempArHopIndx++;
            pTempRsvpTeArHopInfo = pRsvpTeArHopInfo;
        }
        else if (*pu1Pdu == RRO_TYPE_LBL_OBJ)
        {
            if ((pTempRsvpTeArHopInfo == NULL) ||
                ((u1Flags & RRO_TYPE_FLAG_MASK) &&
                 (pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1LblStatus ==
                  RRO_TYPE_LBL_REV)) ||
                (((u1Flags & RRO_TYPE_FLAG_MASK) == RSVPTE_ZERO) &&
                 (pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1LblStatus ==
                  RRO_TYPE_LBL_FWD)))
            {
                pPktMap->u1RROErrVal = RPTE_BAD_STRICT_NODE;

                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "Err - No Hop Info for this Label Object\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmExtractRROList : INTMD-EXIT \n");
                break;
            }
            pRsvpTeArHopInfo = pTempRsvpTeArHopInfo;

            /* Extract LblSubObj Type */
            pu1Pdu += sizeof (UINT1);

            /* Extract LblSubObj Length */
            pu1Pdu += sizeof (UINT1);

            /* Extract LblSubObj Flags */
            RPTE_EXT_1_BYTE (pu1Pdu, u1Flags);
            u1UBit = (u1Flags & RRO_TYPE_MASK);
            u1GblFlag = (u1Flags & RRO_TYPE_FLAG_MASK);

            /* Extract C-Type */
            RPTE_EXT_1_BYTE (pu1Pdu, pRsvpTeArHopInfo->u1LblCType);

            /* Extract Label */
            RPTE_EXT_4_BYTES (pu1Pdu, u4Label);
            if (u1UBit)
            {
                pRsvpTeArHopInfo->GmplsTnlArHopInfo.u4ReverseLbl = u4Label;
                pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1LblStatus
                    |= RRO_TYPE_LBL_REV;

                if (u1GblFlag == RPTE_TRUE)
                {
                    pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1LblStatus
                        |= RRO_TYPE_LBL_REV_GBL;
                }
            }
            else
            {
                pRsvpTeArHopInfo->GmplsTnlArHopInfo.u4ForwardLbl = u4Label;
                pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1LblStatus
                    |= RRO_TYPE_LBL_FWD;

                if (u1GblFlag == RPTE_TRUE)
                {
                    pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1LblStatus
                        |= RRO_TYPE_LBL_FWD_GBL;
                }
            }
        }
        else
        {
            pNewSubObjInfo = (tNewSubObjInfo *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_NEWSUB_OBJ_POOL_ID);

            if (pNewSubObjInfo == NULL)
            {
                RSVPTE_DBG (RSVPTE_MAIN_MEM,
                            "Err - Memory allocation failed in "
                            "NEW_SUB_OBJ processing");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmExtractRROList : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            MEMSET (pNewSubObjInfo, RSVPTE_ZERO, sizeof (tNewSubObjInfo));
            TMO_SLL_Init_Node (&pNewSubObjInfo->NextSubNode);
            TMO_SLL_Add (&pPktMap->RrObj.NewSubObjList,
                         &pNewSubObjInfo->NextSubNode);
            u1SubObjSize = *(pu1Pdu + RSVPTE_ONE);
            pTmpPtr = MemAllocMemBlk (RSVPTE_AR_HOP_POOL_ID);
            if (pTmpPtr == NULL)
            {
                RSVPTE_DBG (RSVPTE_MAIN_MEM,
                            "Err - Mem Alloc failed in NewSubObj "
                            "Processing ..\n");
                RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                            "RptePvmExtractRROList : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            MEMCPY (pTmpPtr, pu1Pdu, u1SubObjSize);
            pu1Pdu += u1SubObjSize;
            pNewSubObjInfo->u1SubObjLen = u1SubObjSize;
            pNewSubObjInfo->pSubObj = pTmpPtr;
            pNewSubObjInfo->u4NodePos = u4TempArHopIndx;
        }
    }

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmExtractRROList : EXIT \n");
    return RPTE_SUCCESS;

}

/****************************************************************************/
/* Function Name   : RptePvmMatchNbrAddr                                    */
/* Description     : This function searches for a matching entry for u4Addr */
/*                   in the Neighbour table                                 */
/* Input (s)       : *pIfEntry - Pointer to an IfTable entry                */
/*                   u4Addr - IP Address                                    */
/* Output (s)      : None                                                   */
/* Returns         : If successful returns the Address of the matched Nbr   */
/*                   entry, Else returns NULL                               */
/****************************************************************************/
tNbrEntry          *
RptePvmMatchNbrAddr (tIfEntry * pIfEntry, UINT4 u4NeighAddr)
{
    tNbrEntry          *pNbrEntry = NULL;
    tTMO_SLL           *pNbrList = NULL;
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmMatchNbrAddr : ENTRY \n");

    if (pIfEntry == NULL)
    {
        return NULL;
    }

    pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));

    TMO_SLL_Scan (pNbrList, pNbrEntry, tNbrEntry *)
    {
        if (NBR_ENTRY_STATUS (pNbrEntry) == ACTIVE)
        {
            if (u4NeighAddr == NBR_ENTRY_ADDR (pNbrEntry))
            {
                RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmMatchNbrAddr : EXIT \n");
                return pNbrEntry;
            }
        }
    }                            /* RsvpSllScan for pNbrEntry */

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmMatchNbrAddr : INTMD-EXIT \n");
    return NULL;
}

/****************************************************************************/
/* Function Name   : RptePvmCreateNbrTableEntry                             */
/* Description     : This function creates an entry in NbrTable.            */
/* Input (s)       : *pPktMap - Pointer to Packet Map                       */
/* Output (s)      : None                                                   */
/* Returns         : If successful returns the address of the newly created */
/*                   entry. Else returns NULL                               */
/****************************************************************************/
tNbrEntry          *
RptePvmCreateNbrTableEntry (tIfEntry * pIfEntry, UINT4 u4Addr, UINT1 u1Encaps,
                            UINT1 u1RsvpVersionFlag)
{
    tNbrEntry          *pNbrEntry = NULL;
    tTMO_SLL           *pNbrList = NULL;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmCreateNbrTableEntry : ENTRY \n");

    pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));

    pNbrEntry = (tNbrEntry *)
        RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_NBR_ENTRY_POOL_ID);

    if (pNbrEntry == NULL)
    {
        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                    "Failed : Nbr Entry Creation Fail - Mem Alloc Fail.\n");
        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                    "RptePvmCreateNbrTableEntry : INTMD-EXIT \n");
        return NULL;
    }
    else
    {
        MEMSET (pNbrEntry, RSVPTE_ZERO, sizeof (tNbrEntry));
        NBR_ENTRY_ADDR (pNbrEntry) = u4Addr;
        NBR_ENTRY_ENCAP (pNbrEntry) = u1Encaps;
        NBR_ENTRY_STATUS (pNbrEntry) = ACTIVE;
        NBR_ENTRY_EPOCH_VALUE (pNbrEntry) = RPTE_UN_INIT_EPOCH;
        NBR_ENTRY_RR_CAPABLE (pNbrEntry) = gu1RRCapable;
        NBR_ENTRY_RR_STATE (pNbrEntry) = gu1RRCapable;
        NBR_ENTRY_RMD_CAPABLE (pNbrEntry) = gu1MsgIdCapable;
        NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) = RPTE_FALSE;
        NBR_ENTRY_IF_ENTRY (pNbrEntry) = pIfEntry;
        NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry) = RPTE_ZERO;
        pNbrEntry->u1RecoveryPathCapability = RPTE_GR_RECOVERY_PATH_DEFAULT;
        pNbrEntry->u2RestartTime = RPTE_ZERO;
        pNbrEntry->u2RecoveryTime = RPTE_ZERO;
        pNbrEntry->bIsHelloActive = RPTE_TRUE;
        UTL_DLL_INIT (&(pNbrEntry->UpStrTnlList),
                      RPTE_OFFSET (tRsvpTeTnlInfo, UpStrNbrTnl));
        UTL_DLL_INIT (&(pNbrEntry->DnStrTnlList),
                      RPTE_OFFSET (tRsvpTeTnlInfo, DnStrNbrTnl));
        TMO_SLL_Init (&NBR_ENTRY_SREFRESH_LIST (pNbrEntry));

        /* Set the Neighbor hello active flag */
        RpteHhSetNbrHelloActive (pNbrEntry, pIfEntry, RPTE_TRUE);

        TMO_SLL_Add (pNbrList, (tRSVP_SLL_NODE *) (&(pNbrEntry->pNext)));

        /* Update IfEntry */
        ++IF_ENTRY_NBRS (pIfEntry);
        if (u1Encaps == UDP_ENCAP)
        {
            ++IF_ENTRY_UDP_NBRS (pIfEntry);
        }
        else
        {
            ++IF_ENTRY_IP_NBRS (pIfEntry);
        }

        /* In the case of this node being RR capable the Rsvp Hdr
         * Flag is examined to determine the neighbour RR Capability */
        RpteRRNbrAutoDiscovery (pNbrEntry, u1RsvpVersionFlag);

        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmCreateNbrTableEntry : EXIT \n");
        return pNbrEntry;
    }
}

/****************************************************************************/
/* Function Name   : RpteTeGetTnlInfo                                       */
/* Description     : This function used to get te tunnel informtion         */
/* Input (s)       : *pRsvpTeTnlInfo - RSVP-TE tunnel information           */
/* Output (s)      : *pRsvpTeGetTnlInfo - Temporary structure used to get   */
/*                                         the te tunnel information        */
/* Returns         : None                                                   */
/****************************************************************************/
INT1
RpteTeGetTnlInfo (tRsvpTeGetTnlInfo * pRsvpTeGetTnlInfo,
                  tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    /* proper fuunction should be added for getting te tunnel information */
    pRsvpTeGetTnlInfo->u1EncodingType
        = pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1EncodingType;
    pRsvpTeGetTnlInfo->u1SwitchingType
        = pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1SwitchingType;
    pRsvpTeGetTnlInfo->u2Gpid = pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u2Gpid;

    /* Other te tunnel information have to be added */
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePvmValidateSwAndEncType                            */
/* Description     : This function used to validate                         */
/*                   Encoding type and Switching Type received on the PATH  */
/*                   message over the interface information passed          */
/*                   If validation fails this function sends PATH ERROR to  */
/*                   the source with proper error code and cleans up the    */
/*                   the packet map.                                        */
/* Input (s)       : pIfEntry       - Pointer to If Entry                   */
/*                   pRsvpTeTnlInfo - Pointer to RSVP-TE Tnl Info           */
/*                   bIsDnStr       - Indicates whether DownStream or       */
/*                                    UpStream                              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
INT1
RptePvmValidateSwAndEncType (tIfEntry * pIfEntry,
                             tRsvpTeTnlInfo * pRsvpTeTnlInfo, BOOL1 bIsDnStr)
{
    UINT2               u2SwTypeVal = RPTE_ZERO;
    UINT2               u2EncTypeVal = RPTE_ZERO;
    UINT1               u1EncType = RPTE_ZERO;
    UINT1               u1SwitchingType = RPTE_ZERO;
    UINT1               u1CspfPath = RPTE_FALSE;
    UINT4               u4TeIfIndex = RPTE_ZERO;

    u1EncType = pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1EncodingType;
    u1SwitchingType = pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1SwitchingType;
    u1CspfPath = pRsvpTeTnlInfo->u1CSPFPathRequested;

    if ((u1EncType == RSVPTE_ZERO) || (u1CspfPath != RPTE_FALSE))
    {
        return RPTE_SUCCESS;
    }

    if (bIsDnStr == TRUE)
    {
        if (pRsvpTeTnlInfo->b1DnStrOob == TRUE)
        {
            u4TeIfIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf;
        }
    }
    else
    {
        if (pRsvpTeTnlInfo->b1UpStrOob == TRUE)
        {
            u4TeIfIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf;
        }
    }

    u2EncTypeVal =
        RptePvmValidateEncodingType (u1EncType, pIfEntry, u4TeIfIndex);

    u2SwTypeVal =
        RptePvmValidateSwitchingCap (u1SwitchingType, pIfEntry, u4TeIfIndex);

    if (u2EncTypeVal != RPTE_ZERO)
    {
        RpteConstructPktMapAndSendPathErr (pRsvpTeTnlInfo,
                                           RPTE_ROUTE_PROB, u2EncTypeVal);
        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                    "Encoding type does not match - Path Err Sent\n");
        return RPTE_FAILURE;
    }

    if (u2SwTypeVal != RPTE_ZERO)
    {
        RpteConstructPktMapAndSendPathErr (pRsvpTeTnlInfo,
                                           RPTE_ROUTE_PROB, u2SwTypeVal);
        RSVPTE_DBG (RSVPTE_PVM_PRCS,
                    "Switching type does not match - Path Err Sent\n");
        return RPTE_FAILURE;
    }

    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePvmValidateEncodingType                            */
/* Description     : This function used to validate Encoding type           */
/* Input (s)       : u1EncType   - Encoding Type                            */
/*                   pIfEntry    - Interface Information                    */
/*                   u4TeLinkIf  - TE Link If Index                         */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_FAILURE / RPTE_SUCCESS                            */
/****************************************************************************/
UINT2
RptePvmValidateEncodingType (UINT1 u1EncType, tIfEntry * pIfEntry,
                             UINT4 u4TeLinkIf)
{
    UINT4               u4TeIfIndex = RSVPTE_ZERO;
    UINT1               u1TeLinkEncType = RSVPTE_ZERO;
    UINT2               u2ErrVal = RPTE_UNSUPPORTED_ENCODING;

    if (u4TeLinkIf != RPTE_ZERO)
    {
        u4TeIfIndex = u4TeLinkIf;
    }
    else
    {
        if (RptePortCfaGetTeLinkIfFromMplsIf (pIfEntry->u4IfIndex,
                                              &u4TeIfIndex) == RPTE_FAILURE)
        {
            return u2ErrVal;
        }
    }

    if (u4TeIfIndex != RPTE_ZERO)
    {
        RptePortTlmGetEncType (u4TeIfIndex, &u1TeLinkEncType);

        if (u1EncType != u1TeLinkEncType)
        {
            return u2ErrVal;
        }
    }

    u2ErrVal = RPTE_ZERO;

    return u2ErrVal;
}

/****************************************************************************/
/* Function Name   : RptePvmValidateSwitchingCap                            */
/* Description     : This function used to validate switching type          */
/* Input (s)       : u1SwCap     - Switching capability                     */
/*                   pIfEntry    - Interface Info                           */
/*                   u4TeLinkIf  - TE Link If Index                         */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS / RPTE_FAILURE                            */
/****************************************************************************/
UINT2
RptePvmValidateSwitchingCap (UINT1 u1SwCap, tIfEntry * pIfEntry,
                             UINT4 u4TeLinkIf)
{
    UINT4               u4TeIfIndex = RSVPTE_ZERO;
    UINT1               u1TeLinkSwCap = RSVPTE_ZERO;
    UINT2               u2ErrVal = RPTE_UNSUPPORTED_SWITCHING;

    if (u4TeLinkIf != RPTE_ZERO)
    {
        u4TeIfIndex = u4TeLinkIf;
    }
    else
    {
        if (RptePortCfaGetTeLinkIfFromMplsIf (pIfEntry->u4IfIndex,
                                              &u4TeIfIndex) == RPTE_FAILURE)
        {
            return u2ErrVal;
        }
    }

    if (u4TeIfIndex != RPTE_ZERO)
    {
        RptePortTlmGetSwCap (u4TeIfIndex, &u1TeLinkSwCap);

        if (u1SwCap != u1TeLinkSwCap)
        {
            return u2ErrVal;
        }
    }

    u2ErrVal = RPTE_ZERO;

    return u2ErrVal;
}

/****************************************************************************/
/* Function Name   : RptePvmFillErrorSpecFlags                              */
/* Description     : This function is uded to fill the error spec flag      */
/* Input (s)       : *u1Flag                                                */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillErrorSpecFlags (UINT1 *u1Flag)
{
    /* AS per RFC 3473, section 4.4
     * A Node which encounters an error MAY set Path State Removed flag */
    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1PathStateRemovedFlag == RPTE_ENABLED)
    {
        *u1Flag = RPTE_PATH_STATE_REMOVED;
    }
    else
    {
        *u1Flag = RSVPTE_ZERO;
    }
}

/****************************************************************************/
/* function Name   : RptePvmFillGenericLabelReqObj
 * Description     : Function to update the Generic label request object in
 *                   Path message
 * Input (s)       : tRsvpTeTnlInfo - RSVP-TE tunnel informations.
 * Output (s)      : ppObjHdr - Updated PATH message.
 * Returns         : None
 *         */
/****************************************************************************/

VOID
RptePvmFillGenericLabelReqObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                               tObjHdr ** ppObjHdr, UINT1 u1MsgType)
{
    tRsvpTeGetTnlInfo   RsvpTeGetTnlInfo;
    tGenLblReqObj      *pGenLblReqObj = NULL;
    tLblReqAtmRngObj   *pLblReqAtmRngObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT2               u2TmpVpi = RSVPTE_ZERO;
    UINT1               u1EncodingType = RSVPTE_ZERO;
    UINT1               u1SwitchingType = RSVPTE_ZERO;
    UINT2               u2GPid = RSVPTE_ZERO;
    UINT2               u2ClassNumType = RPTE_LBL_REQ_CLASS_NUM_TYPE;
    UINT1               u1LblType = RPTE_ZERO;
    UINT2               u2MinVpi = RPTE_ZERO;
    UINT2               u2MinVci = RPTE_ZERO;
    UINT2               u2MaxVpi = RPTE_ZERO;
    UINT2               u2MaxVci = RPTE_ZERO;

    if (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_NO_LBL_REQ)
    {
        return;
    }

    if (u1MsgType == PATH_MSG)
    {
        u1LblType = pRsvpTeTnlInfo->pPsb->pOutIfEntry->RpteIfInfo.u1LblType;
        u2MinVpi = pRsvpTeTnlInfo->pPsb->pOutIfEntry->RpteIfInfo.u2MinVpi;
        u2MinVci = pRsvpTeTnlInfo->pPsb->pOutIfEntry->RpteIfInfo.u2MinVci;
        u2MaxVpi = pRsvpTeTnlInfo->pPsb->pOutIfEntry->RpteIfInfo.u2MaxVpi;
        u2MaxVci = pRsvpTeTnlInfo->pPsb->pOutIfEntry->RpteIfInfo.u2MaxVci;
    }
    else
    {
        u1LblType = pRsvpTeTnlInfo->pPsb->pIncIfEntry->RpteIfInfo.u1LblType;
        u2MinVpi = pRsvpTeTnlInfo->pPsb->pIncIfEntry->RpteIfInfo.u2MinVpi;
        u2MinVci = pRsvpTeTnlInfo->pPsb->pIncIfEntry->RpteIfInfo.u2MinVci;
        u2MaxVpi = pRsvpTeTnlInfo->pPsb->pIncIfEntry->RpteIfInfo.u2MaxVpi;
        u2MaxVci = pRsvpTeTnlInfo->pPsb->pIncIfEntry->RpteIfInfo.u2MaxVci;
    }

    /*Get the required tunnel information */
    MEMSET (&RsvpTeGetTnlInfo, RSVPTE_ZERO, sizeof (tRsvpTeGetTnlInfo));

    RpteTeGetTnlInfo (&RsvpTeGetTnlInfo, pRsvpTeTnlInfo);

    u1EncodingType = RsvpTeGetTnlInfo.u1EncodingType;
    u1SwitchingType = RsvpTeGetTnlInfo.u1SwitchingType;

    if (u1EncodingType != RSVPTE_ZERO)
    {
        u2GPid = RsvpTeGetTnlInfo.u2Gpid;

        /* As per RFC 3473, section 2.1
         * For GMPLS tunnel, Label request object class - 19, ctype -4 */
        u2ClassNumType = RPTE_GLBL_REQ_CLASS_NUM_TYPE;
    }
    else
    {
        u2GPid = pRsvpTeTnlInfo->u2L3pid;

        /* AS per RFC 3209,
         * For MPLS tunnel, Label request object class - 16, ctype - 4 */
        u2ClassNumType = RPTE_LBL_REQ_CLASS_NUM_TYPE;
    }

    /* Label request object updation for MPLS tunnel with label type-ATM */
    if (u1LblType == RPTE_ATM)
    {
        pObjHdr->u2Length = OSIX_HTONS (sizeof (tLblReqAtmRngObj));
        pObjHdr->u2ClassNumType =
            OSIX_HTONS (RPTE_LBL_REQ_ATM_RNG_CLASS_NUM_TYPE);

        pLblReqAtmRngObj = (tLblReqAtmRngObj *) (pObjHdr);
        pObjHdr = NEXT_OBJ (pObjHdr);

        pLblReqAtmRngObj->GenLblReq.u1EncType = u1EncodingType;
        pLblReqAtmRngObj->GenLblReq.u1SwitchingType = u1SwitchingType;
        pLblReqAtmRngObj->GenLblReq.u2GPid = OSIX_HTONS (u2GPid);
        u2TmpVpi = OSIX_HTONS (u2MinVpi);
        if (gpRsvpTeGblInfo->u1AtmMergeSupport == ATM_MERGE_SUPPRT)
        {
            u2TmpVpi |= ATM_MERGE_SUPPRT_ADD_MASK;
        }
        pLblReqAtmRngObj->AtmLblRngEntry.u2LBVpi = u2TmpVpi;
        pLblReqAtmRngObj->AtmLblRngEntry.u2LBVci = OSIX_HTONS (u2MinVci);
        pLblReqAtmRngObj->AtmLblRngEntry.u2UBVpi = OSIX_HTONS (u2MaxVpi);
        pLblReqAtmRngObj->AtmLblRngEntry.u2UBVci = OSIX_HTONS (u2MaxVci);
    }
    /*Genric label request updation for GMPLS and MPLS tunnel with label 
     * type - Ethernet */
    else
    {
        pObjHdr->u2Length = OSIX_HTONS (sizeof (tGenLblReqObj));

        pObjHdr->u2ClassNumType = OSIX_HTONS (u2ClassNumType);

        pGenLblReqObj = (tGenLblReqObj *) (pObjHdr);
        pObjHdr = NEXT_OBJ (pObjHdr);

        pGenLblReqObj->GenLblReq.u1EncType = u1EncodingType;
        pGenLblReqObj->GenLblReq.u1SwitchingType = u1SwitchingType;
        if (u2GPid == GMPLS_GPID_ETHERNET)
        {
            pGenLblReqObj->GenLblReq.u2GPid = OSIX_HTONS
                (RPTE_GMPLS_ETHERNET_ETHERTYPE);
        }
        else
        {
            pGenLblReqObj->GenLblReq.u2GPid = OSIX_HTONS (u2GPid);
        }
    }

    *ppObjHdr = pObjHdr;

    return;
}

/****************************************************************************/
/* function Name   : RptePvmFillUpstreamGenericLblObj
 * Description     : Function to update the Upstream Generic label object in
 *                   Path message
 * Input (s)       : tRsvpTeTnlInfo - RSVP-TE tunnel informations.
 * Output (s)      : ppObjHdr - Updated PATH message.
 * Returns         : None
 *         */
/****************************************************************************/
VOID
RptePvmFillUpstreamGenericLblObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                  tObjHdr ** ppObjHdr, UINT1 u1MsgType)
{
    tGenLblObj         *pUpStrLblObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;

    if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode != GMPLS_BIDIRECTION)
    {
        return;
    }

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tGenLblObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (RPTE_UPSTR_GLBL_CLASS_NUM_TYPE);

    pUpStrLblObj = (tGenLblObj *) (VOID *) pObjHdr;
    pObjHdr = NEXT_OBJ (pObjHdr);

    if (u1MsgType == PATH_MSG)
    {
        pUpStrLblObj->Label.u4GenLbl =
            OSIX_HTONL (pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl);
    }
    else
    {
        pUpStrLblObj->Label.u4GenLbl =
            OSIX_HTONL (pRsvpTeTnlInfo->UpStrOutLbl.u4GenLbl);
    }

    *ppObjHdr = pObjHdr;

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillExplicitRouteObj
 * Description     : Function to update the ERO object in Path message
 * Input (s)       : pRsvpTeTnlInfo - RSVP-TE tunnel informations.
 *                   u2EroSize      - Size of ERO object
 * Output (s)      : ppObjHdr       - Pointer to Object Header.
 * Returns         : None
 *         */
/****************************************************************************/
VOID
RptePvmFillExplicitRouteObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT2 u2EroSize,
                             tObjHdr ** ppObjHdr)
{
    UINT1              *pPdu = NULL;
    UINT1               u1LabelType = RPTE_LBL_CTYPE;
    tTMO_SLL           *pEROList = NULL;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    tTeCHopInfo        *pTeCHopInfo = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    tRsvpTeGetTnlInfo   RsvpTeGetTnlInfo;
    UINT1               u1FrrSkipHop = RPTE_FALSE;

    /*Get the required tunnel information */
    MEMSET (&RsvpTeGetTnlInfo, RSVPTE_ZERO, sizeof (tRsvpTeGetTnlInfo));

    RpteTeGetTnlInfo (&RsvpTeGetTnlInfo, pRsvpTeTnlInfo);
    if (RsvpTeGetTnlInfo.u1EncodingType != RSVPTE_ZERO)
    {
        u1LabelType = RPTE_GLBL_CTYPE;
    }

    /* Object Header Updations */
    pObjHdr->u2Length = OSIX_HTONS (u2EroSize);
    pObjHdr->u2ClassNumType = OSIX_HTONS (RPTE_ERO_CLASS_NUM_TYPE);

    pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

    /* ERO object updation using the HOP list given CSPF */
    /* if pTeCHopListInfo is not present then fill the EroList from pFrrOutTePathInfo */

    if (pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo != NULL)
    {
        TMO_SLL_Scan (&(pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo->CHopList),
                      pTeCHopInfo, tTeCHopInfo *)
        {
            if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
                (pTeCHopInfo->u1LsrPartOfCHop == RPTE_TRUE))
            {
                continue;
            }

            if ((RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) == RPTE_TRUE) &&
                ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_IN_USE) ==
                 LOCAL_PROT_IN_USE) &&
                (RPTE_YES == RpteUtlIsNodeProtDesired (pRsvpTeTnlInfo)) &&
                (u1FrrSkipHop == RPTE_FALSE))
            {
                /* Incase of Node-protection inuse one hop need to stripped */
                RSVPTE_DBG4 (RSVPTE_PVM_ETEXT,
                             "Node-protection inuse Skipping hop: %u.%u.%u.%u\n",
                             pTeCHopInfo->IpAddr.au1Ipv4Addr[0],
                             pTeCHopInfo->IpAddr.au1Ipv4Addr[1],
                             pTeCHopInfo->IpAddr.au1Ipv4Addr[2],
                             pTeCHopInfo->IpAddr.au1Ipv4Addr[3]);
                u1FrrSkipHop = RPTE_TRUE;
                continue;
            }
            RSVPTE_DBG4 (RSVPTE_PVM_ETEXT, "Adding hop: %u.%u.%u.%u\n",
                         pTeCHopInfo->IpAddr.au1Ipv4Addr[0],
                         pTeCHopInfo->IpAddr.au1Ipv4Addr[1],
                         pTeCHopInfo->IpAddr.au1Ipv4Addr[2],
                         pTeCHopInfo->IpAddr.au1Ipv4Addr[3]);

            RptePvmFillEROFromCHop (&pPdu, pTeCHopInfo, u1LabelType);
        }

        RSVPTE_DBG (RSVPTE_PVM_ETEXT, "pTeCHopListInfo is not NULL\n");

        /* ERO filled in PATH message using CHOP List. So, return here. */
        pObjHdr = NEXT_OBJ (pObjHdr);
        *ppObjHdr = pObjHdr;

        return;
    }
    else if ((RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) == RPTE_TRUE) &&
             ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_IN_USE) ==
              LOCAL_PROT_IN_USE))
    {

        RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                    "Updating pEROList for Facility backup\n");
        pEROList = &pRsvpTeTnlInfo->pFrrOutTePathInfo->ErHopList;

    }
    else
    {
        pEROList = &RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo);
    }

    /* ERO object updation using the explicit Hop list */
    TMO_SLL_Scan (pEROList, pRsvpTeErHopInfo, tRsvpTeHopInfo *)
    {
        if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
            (pRsvpTeErHopInfo->u1LsrPartOfErHop == RPTE_TRUE))
        {
            RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                        "Tnl role is ingress and LSR is part of ErHop so skipping it\n");
            continue;
        }

        RptePvmFillEROFromERHop (&pPdu, pRsvpTeErHopInfo, u1LabelType);
    }

    pObjHdr = NEXT_OBJ (pObjHdr);
    *ppObjHdr = pObjHdr;

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillLabelSetObj
 * Description     : Function to update the Label Set Object in Path message
 * Input (s)       : pRsvpTeTnlInfo - RSVP-TE tunnel informations.
 * Output (s)      : ppObjHdr       - Pointer to Object Header.
 * Returns         : None
 *
 ****************************************************************************/
VOID
RptePvmFillLabelSetObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo, tObjHdr ** ppObjHdr,
                        UINT1 u1MsgType)
{
    tLabelSetObj       *pLabelSetObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT4               u4Label = RSVPTE_ZERO;

    pObjHdr->u2Length = OSIX_HTONS (sizeof (tLabelSetObj));
    pObjHdr->u2ClassNumType = OSIX_HTONS (GMPLS_LABEL_SET_CLASS_NUM_TYPE);
    pLabelSetObj = (tLabelSetObj *) (VOID *) (pObjHdr);

    pObjHdr = NEXT_OBJ (pObjHdr);

    if (u1MsgType == PATH_MSG)
    {
        u4Label = pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl;
    }
    else if (u1MsgType == RECOVERY_PATH_MSG)
    {
        u4Label = pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl;
    }

    /* AS per RFC 3473, section 5.1.1,
     * The forward label in ERO object should be copied into Label
     * set object */
    if (u4Label != MPLS_INVALID_LABEL)
    {
        pLabelSetObj->LabelSet.u1Action = GMPLS_LBLSET_INCLUDE_LIST;
        pLabelSetObj->LabelSet.u1Resvd = RSVPTE_ZERO;
        pLabelSetObj->LabelSet.u2LblType = RPTE_LBL_GENERIC;
        pLabelSetObj->LabelSet.u4SubChannel1 = OSIX_HTONL (u4Label);
        pLabelSetObj->LabelSet.u4SubChannel2 = RSVPTE_ZERO;
    }
    else
    {
        pLabelSetObj->LabelSet.u1Action = GMPLS_LBLSET_INCLUDE_LIST_RANGE;
        pLabelSetObj->LabelSet.u1Resvd = RSVPTE_ZERO;
        pLabelSetObj->LabelSet.u2LblType = RPTE_LBL_GENERIC;
        pLabelSetObj->LabelSet.u4SubChannel1
            = OSIX_HTONL (RSVPTE_GENLBL_MINLBL (gpRsvpTeGblInfo));
        pLabelSetObj->LabelSet.u4SubChannel2
            = OSIX_HTONL (RSVPTE_GENLBL_MAXLBL (gpRsvpTeGblInfo));
    }

    *ppObjHdr = pObjHdr;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillRecordRouteObj
 * Description     : Function to update the Record Route object in Path message
 * Input (s)       : pRsvpTeTnlInfo - RSVP-TE tunnel information
 *                   u2RroSize - Size of RRO object
 *                   ppObjHdr - Pointer to Object Header.
 * Output (s)      : None
 * Returns         : None
 ****************************************************************************/
VOID
RptePvmFillRecordRouteObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT2 u2RroSize,
                           tObjHdr ** ppObjHdr)
{
    UINT1              *pPdu = NULL;
    UINT4               u4FwdLbl = MPLS_INVALID_LABEL;
    UINT4               u4RevLbl = MPLS_INVALID_LABEL;
    tObjHdr            *pObjHdr = *ppObjHdr;

    pObjHdr->u2Length = OSIX_HTONS (u2RroSize);
    pObjHdr->u2ClassNumType = OSIX_HTONS (RPTE_RRO_CLASS_NUM_TYPE);

    pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

    u4FwdLbl = pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl;
    u4RevLbl = pRsvpTeTnlInfo->UpStrInLbl.u4GenLbl;

    RptePvmFillTopRRO (&pPdu, pRsvpTeTnlInfo, pRsvpTeTnlInfo->pPsb->pOutIfEntry,
                       u4FwdLbl, u4RevLbl);
    if (RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL)
    {
        if (TMO_SLL_Count (&RSVPTE_TNL_INARHOP_LIST (pRsvpTeTnlInfo))
            != RSVPTE_ZERO)
        {
            RptePvmFillDnStrOrUpStrRRO (&pPdu, pRsvpTeTnlInfo,
                                        &RSVPTE_TNL_INARHOP_LIST
                                        (pRsvpTeTnlInfo));
        }
    }
    pObjHdr = NEXT_OBJ (pObjHdr);
    *ppObjHdr = pObjHdr;

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillEROFromERHop
 * Description     : This function is used to fill the ERHop information
 * Input (s)       : pRsvpTeTnlInfo - RSVP-TE tunnel information
 *                   **ppPdu - pdu information
 *                   u1LblCType - Label type.
 * Output (s)      : None
 * Returns         : None
 ****************************************************************************/
VOID
RptePvmFillEROFromERHop (UINT1 **ppPdu, tRsvpTeHopInfo * pRsvpTeHopInfo,
                         UINT1 u1LblCType)
{
    UINT1               u1LBit = (UINT1) ERO_TYPE_LOOSE_SET;
    UINT1               u1UBit = (UINT1) RSVPTE_ZERO;
    UINT4               u4IpAddr = RSVPTE_ZERO;
    UINT1              *pPdu = *ppPdu;

    if (pRsvpTeHopInfo->u1HopType == ERO_TYPE_STRICT)
    {
        u1LBit = (UINT1) ERO_TYPE_STRICT_SET;
    }

    CONVERT_TO_INTEGER (pRsvpTeHopInfo->IpAddr.au1Ipv4Addr, u4IpAddr);
    u4IpAddr = OSIX_NTOHL (u4IpAddr);

    /* As per RFC 3477 Section 4, IPV4 unnumbered ERO obj updation */
    if (pRsvpTeHopInfo->u1AddressType == ERO_TYPE_IPV4_ADDR)
    {
        RptePvmFillIpv4SubObjInERO (&pPdu, u1LBit, u4IpAddr,
                                    pRsvpTeHopInfo->u1AddrPrefixLen);
    }
    /* As per RFC 3209, IPV4 numbered ERO object updation */
    else
    {
        RptePvmFillUnnumIfSubObjInERO (&pPdu, u1LBit, u4IpAddr,
                                       pRsvpTeHopInfo->u4UnnumIf);
    }

    /* As per RFC 3473 section 5 (Explicit Label control, 
       ERO label sub object updation */
    RptePvmFillLblSubObjInERO (&pPdu, u1LBit, u1UBit, u1LblCType,
                               pRsvpTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl);

    u1UBit = (UINT1) RRO_TYPE_MASK;
    RptePvmFillLblSubObjInERO (&pPdu, u1LBit, u1UBit, u1LblCType,
                               pRsvpTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl);

    *ppPdu = pPdu;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillEROFromCHop
 * Description     : This function is used to fill the CHop information
 * Input (s)       : pRsvpTeTnlInfo - RSVP-TE tunnel information
 *                   **ppPdu - pdu information
 *                   u1LblCType - Label type.
 * Output (s)      : None
 * Returns         : None
 ****************************************************************************/

VOID
RptePvmFillEROFromCHop (UINT1 **ppPdu, tTeCHopInfo * pTeCHopInfo,
                        UINT1 u1LblCType)
{
    UINT1               u1LBit = (UINT1) ERO_TYPE_LOOSE_SET;
    UINT1               u1UBit = (UINT1) RSVPTE_ZERO;
    UINT4               u4IpAddr = RSVPTE_ZERO;
    UINT1              *pPdu = *ppPdu;

    if (pTeCHopInfo->u1CHopType == ERO_TYPE_STRICT)
    {
        u1LBit = (UINT1) ERO_TYPE_STRICT_SET;
    }

    CONVERT_TO_INTEGER (pTeCHopInfo->IpAddr.au1Ipv4Addr, u4IpAddr);
    u4IpAddr = OSIX_NTOHL (u4IpAddr);

    /* As per RFC 3477 Section 4, IPV4 unnumbered ERO obj updation */
    if (pTeCHopInfo->u1AddressType == ERO_TYPE_IPV4_ADDR)
    {
        RptePvmFillIpv4SubObjInERO (&pPdu, u1LBit, u4IpAddr,
                                    pTeCHopInfo->u1AddrPrefixLen);
    }
    /* As per RFC 3209, IPV4 numbered ERO object updation */
    else
    {
        RptePvmFillUnnumIfSubObjInERO (&pPdu, u1LBit, u4IpAddr,
                                       pTeCHopInfo->u4UnnumIf);
    }

    /* As per RFC 3473 section 5 (Explicit Label control, 
       ERO label sub object updation */
    RptePvmFillLblSubObjInERO (&pPdu, u1LBit, u1UBit, u1LblCType,
                               pTeCHopInfo->GmplsTnlCHopInfo.u4ForwardLbl);

    u1UBit = (UINT1) RRO_TYPE_MASK;
    RptePvmFillLblSubObjInERO (&pPdu, u1LBit, u1UBit, u1LblCType,
                               pTeCHopInfo->GmplsTnlCHopInfo.u4ReverseLbl);
    *ppPdu = pPdu;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillTopRRO                                      */
/* Description     : This function fills the Top RRO info                   */
/* Input (s)       : pPdu       - Pointer to PDU                            */
/*                   pIfEntry   - Pointer to If Entry                       */
/*                   u4FwdLbl   - Forward Label                             */
/*                   u4RevLbl   - Reverse Label                             */
/*                   u1LblCType - Lbl C-Type                                */
/*                   u1RroFlag  - RRO Flags                                 */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillTopRRO (UINT1 **ppPdu, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                   tIfEntry * pIfEntry, UINT4 u4FwdLbl, UINT4 u4RevLbl)
{
    UINT4               u4TmpAddr = RSVPTE_ZERO;
    UINT1               u1LblCType = (UINT1) RPTE_LBL_CTYPE;
    UINT1               u1UBit = (UINT1) RSVPTE_ZERO;
    UINT1              *pPdu = *ppPdu;
    tRsvpTeGetTnlInfo   RsvpTeGetTnlInfo;

    /*Get the required tunnel information */
    MEMSET (&RsvpTeGetTnlInfo, RSVPTE_ZERO, sizeof (tRsvpTeGetTnlInfo));

    RpteTeGetTnlInfo (&RsvpTeGetTnlInfo, pRsvpTeTnlInfo);
    if (RsvpTeGetTnlInfo.u1EncodingType != RSVPTE_ZERO)
    {
        u1LblCType = (UINT1) RPTE_GLBL_CTYPE;
    }
    CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                        u4TmpAddr);
    u4TmpAddr = OSIX_NTOHL (u4TmpAddr);

    /* AS per RFC 3209, RRO numbered sub object updation */
    if (pIfEntry->u4Addr != RSVPTE_ZERO)
    {
        RptePvmFillIpv4SubObjInRRO (&pPdu, u4TmpAddr,
                                    (UINT1) RRO_TYPE_IPV4_PRELEN,
                                    RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo));
    }
    else
    {
        /* As per RFC 3473 Section 5, RRO unnumbered sub object updation */
        RptePvmFillUnnumIfSubObjInRRO (&pPdu,
                                       RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo),
                                       u4TmpAddr, pRsvpTeTnlInfo->
                                       u4DnStrDataTeLinkIfId);
    }

    if (!(pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1Attributes
          & RSVPTE_SSN_LBL_RECORD_BIT))
    {
        *ppPdu = pPdu;
        return;
    }

    /* As per RFC 3209, Fill Down Stream Label Info */
    RptePvmFillLblSubObjInRRO (&pPdu, u1UBit, (UINT1) RRO_TYPE_FLAG_MASK,
                               u1LblCType, u4FwdLbl);

    /* As per RFC 3473 Section 5.2, Fill Upstream Label Info */
    u1UBit = (UINT1) RRO_TYPE_MASK;
    RptePvmFillLblSubObjInRRO (&pPdu, u1UBit,
                               (UINT1) (RRO_TYPE_MASK | RRO_TYPE_FLAG_MASK),
                               u1LblCType, u4RevLbl);
    *ppPdu = pPdu;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillDnStrOrUpStrRRO                             */
/* Description     : This function fills the hop info known                 */
/* Input (s)       : pPdu       - Pointer to PDU                            */
/*                   pRroList   - Pointer to RRO List                       */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillDnStrOrUpStrRRO (UINT1 **ppPdu, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                            tTMO_SLL * pRroList)
{
    tRsvpTeArHopInfo   *pRsvpTeArHopInfo = NULL;
    tNewSubObjInfo     *pNewSubObj = NULL;
    UINT4               u4TmpAddr = RSVPTE_ZERO;
    UINT4               u4FwdLbl = MPLS_INVALID_LABEL;
    UINT4               u4RevLbl = MPLS_INVALID_LABEL;
    UINT1               u1Flag = (UINT1) RSVPTE_ZERO;
    UINT1               u1UBit = (UINT1) RSVPTE_ZERO;
    UINT1               u1NewSubNodeNum = (UINT1) RPTE_ONE;
    UINT1              *pPdu = *ppPdu;

    TMO_SLL_Scan (pRroList, pRsvpTeArHopInfo, tRsvpTeArHopInfo *)
    {
        CONVERT_TO_INTEGER (pRsvpTeArHopInfo->IpAddr.au1Ipv4Addr, u4TmpAddr);
        u4TmpAddr = OSIX_NTOHL (u4TmpAddr);
        u4FwdLbl = pRsvpTeArHopInfo->GmplsTnlArHopInfo.u4ForwardLbl;
        u4RevLbl = pRsvpTeArHopInfo->GmplsTnlArHopInfo.u4ReverseLbl;

        /* As per RFC 3209, RRO-numbered sub object updation */
        if (pRsvpTeArHopInfo->u1AddressType == RRO_TYPE_IPV4_ADDR)
        {
            RptePvmFillIpv4SubObjInRRO (&pPdu, u4TmpAddr,
                                        pRsvpTeArHopInfo->u1AddrPrefixLen,
                                        pRsvpTeArHopInfo->u1Flags);
        }
        else
        {
            /* As per RFC 3477 Section 5, RRO-unnumbered sub object updation */
            RptePvmFillUnnumIfSubObjInRRO (&pPdu, pRsvpTeArHopInfo->u1Flags,
                                           u4TmpAddr,
                                           pRsvpTeArHopInfo->u4UnnumIf);
        }

        u1Flag = (UINT1) RSVPTE_ZERO;
        if (pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1LblStatus &
            RRO_TYPE_LBL_FWD_GBL)
        {
            u1Flag = (UINT1) RRO_TYPE_FLAG_MASK;
        }

        /* As per RFC 3473 Section 5.2, Fill Label RRO */
        u1UBit = (UINT1) RSVPTE_ZERO;
        RptePvmFillLblSubObjInRRO (&pPdu, u1UBit, u1Flag,
                                   pRsvpTeArHopInfo->u1LblCType, u4FwdLbl);

        u1Flag = (UINT1) RSVPTE_ZERO;
        if (pRsvpTeArHopInfo->GmplsTnlArHopInfo.u1LblStatus &
            RRO_TYPE_LBL_REV_GBL)
        {
            u1Flag = (UINT1) RRO_TYPE_FLAG_MASK;
        }

        u1UBit = (UINT1) RRO_TYPE_MASK;
        RptePvmFillLblSubObjInRRO (&pPdu, u1UBit, u1Flag,
                                   pRsvpTeArHopInfo->u1LblCType, u4RevLbl);

        /* As per RFC 3209, Fill the unknown RR objects */
        pNewSubObj =
            (tNewSubObjInfo *) TMO_SLL_Nth (&pRsvpTeTnlInfo->NewSubObjList,
                                            u1NewSubNodeNum);
        if ((pNewSubObj != NULL) &&
            (pNewSubObj->u4NodePos == pRsvpTeArHopInfo->u4TnlArHopIndex))
        {
            MEMCPY (pPdu, pNewSubObj->pSubObj, pNewSubObj->u1SubObjLen);
            pPdu += pNewSubObj->u1SubObjLen;
            u1NewSubNodeNum++;
        }
    }
    *ppPdu = pPdu;
}

/****************************************************************************/
/* Function Name   : RptePvmFillIpv4SubObjInERO                             */
/* Description     : This function fills IPV4 sub obj info                  */
/* Input (s)       : pPdu       - Pointer to PDU                            */
/*                   u1LBit     - L Bit                                     */
/*                   u4IpAddr   - IP Address                                */
/*                   u1PrefixLen- prefix length                             */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillIpv4SubObjInERO (UINT1 **ppPdu, UINT1 u1LBit, UINT4 u4IpAddr,
                            UINT1 u1PrefixLen)
{
    UINT1              *pPdu = *ppPdu;
    RPTE_PUT_1_BYTE (pPdu, (u1LBit | ERO_TYPE_IPV4_ADDR));
    RPTE_PUT_1_BYTE (pPdu, ERO_TYPE_IPV4_LEN);
    RPTE_PUT_4_BYTES (pPdu, OSIX_HTONL (u4IpAddr));
    RPTE_PUT_1_BYTE (pPdu, u1PrefixLen);
    RPTE_PUT_1_BYTE (pPdu, RSVPTE_ZERO);
    *ppPdu = pPdu;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillUnnumIfSubObjInERO                          */
/* Description     : This function fills unnum sub obj info                 */
/* Input (s)       : pPdu       - Pointer to PDU                            */
/*                   u1LBit     - L Bit                                     */
/*                   u4IpAddr   - Router Id                                 */
/*                   u4UnnumIf  - Interface Id                              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillUnnumIfSubObjInERO (UINT1 **ppPdu, UINT1 u1LBit, UINT4 u4IpAddr,
                               UINT4 u4UnnumIf)
{
    UINT1              *pPdu = *ppPdu;
    RPTE_PUT_1_BYTE (pPdu, (u1LBit | ERO_TYPE_UNNUM_IF));
    RPTE_PUT_1_BYTE (pPdu, ERO_TYPE_UNNUM_LEN);
    RPTE_PUT_2_BYTES (pPdu, ERO_TYPE_UNNUM_RSVD);
    RPTE_PUT_4_BYTES (pPdu, OSIX_HTONL (u4IpAddr));
    RPTE_PUT_4_BYTES (pPdu, OSIX_HTONL (u4UnnumIf));
    *ppPdu = pPdu;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillLblSubObjInERO                              */
/* Description     : This function fills label sub obj info                 */
/* Input (s)       : pPdu       - Pointer to PDU                            */
/*                   u1LBit     - L Bit                                     */
/*                   u1UBit     - U Bit                                     */
/*                   u1LblCType - label type                                */
/*                   u4Label    - label                                     */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillLblSubObjInERO (UINT1 **ppPdu, UINT1 u1LBit, UINT1 u1UBit,
                           UINT1 u1LblCType, UINT4 u4Label)
{
    UINT1              *pPdu = *ppPdu;
    if (u4Label == MPLS_INVALID_LABEL)
    {
        return;
    }

    RPTE_PUT_1_BYTE (pPdu, (u1LBit | ERO_TYPE_LBL_OBJ));
    RPTE_PUT_1_BYTE (pPdu, ERO_TYPE_LBL_LEN);
    RPTE_PUT_1_BYTE (pPdu, (u1UBit | ERO_TYPE_LBL_RSVD));
    RPTE_PUT_1_BYTE (pPdu, u1LblCType);
    RPTE_PUT_4_BYTES (pPdu, OSIX_HTONL (u4Label));
    *ppPdu = pPdu;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillIpv4SubObjInRRO                             */
/* Description     : This function fills IPV4 sub obj info                  */
/* Input (s)       : pPdu       - Pointer to PDU                            */
/*                   u1LBit     - L Bit                                     */
/*                   u4IpAddr   - IP Address                                */
/*                   u1Flags    - flag values                               */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillIpv4SubObjInRRO (UINT1 **ppPdu, UINT4 u4IpAddr,
                            UINT1 u1PrefixLen, UINT1 u1Flags)
{
    UINT1              *pPdu = *ppPdu;
    RPTE_PUT_1_BYTE (pPdu, RRO_TYPE_IPV4_ADDR);
    RPTE_PUT_1_BYTE (pPdu, RRO_TYPE_IPV4_LEN);
    RPTE_PUT_4_BYTES (pPdu, OSIX_HTONL (u4IpAddr));
    RPTE_PUT_1_BYTE (pPdu, u1PrefixLen);
    RPTE_PUT_1_BYTE (pPdu, u1Flags);
    *ppPdu = pPdu;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillUnnumIfSubObjInRRO                          */
/* Description     : This function fills unnum sub obj info                 */
/* Input (s)       : pPdu       - Pointer to PDU                            */
/*                   u1Flags    - flag values                               */
/*                   u4IpAddr   - Router Id                                 */
/*                   u4UnnumIf  - Interface Id                              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillUnnumIfSubObjInRRO (UINT1 **ppPdu, UINT1 u1Flags,
                               UINT4 u4IpAddr, UINT4 u4UnnumIf)
{
    UINT1              *pPdu = *ppPdu;
    RPTE_PUT_1_BYTE (pPdu, RRO_TYPE_UNNUM_IF);
    RPTE_PUT_1_BYTE (pPdu, RRO_TYPE_UNNUM_LEN);
    RPTE_PUT_1_BYTE (pPdu, u1Flags);
    RPTE_PUT_1_BYTE (pPdu, RRO_TYPE_UNNUM_RSVD);
    RPTE_PUT_4_BYTES (pPdu, OSIX_HTONL (u4IpAddr));
    RPTE_PUT_4_BYTES (pPdu, OSIX_HTONL (u4UnnumIf));
    *ppPdu = pPdu;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillLblSubObjInRRO                              */
/* Description     : This function fills label sub obj info                 */
/* Input (s)       : pPdu       - Pointer to PDU                            */
/*                   u1LBit     - L Bit                                     */
/*                   u1UBit     - U Bit                                     */
/*                   u1LblCType - label type                                */
/*                   u4Label    - label                                     */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillLblSubObjInRRO (UINT1 **ppPdu, UINT1 u1UBit, UINT1 u1Flags,
                           UINT1 u1LblCType, UINT4 u4Label)
{
    UINT1              *pPdu = *ppPdu;
    if (u4Label == MPLS_INVALID_LABEL)
    {
        return;
    }

    RPTE_PUT_1_BYTE (pPdu, RRO_TYPE_LBL_OBJ);
    RPTE_PUT_1_BYTE (pPdu, RRO_TYPE_LBL_LEN);
    RPTE_PUT_1_BYTE (pPdu, (u1UBit | u1Flags));
    RPTE_PUT_1_BYTE (pPdu, u1LblCType);
    RPTE_PUT_4_BYTES (pPdu, OSIX_HTONL (u4Label));
    *ppPdu = pPdu;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillErrorSpecObj                                */
/* Description     : This function fills the hop info known                 */
/* Input (s)       : pPdu       - Pointer to PDU                            */
/*                   pPktMap    - packet map                                */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillErrorSpecObj (UINT1 **ppu1Pdu, tPktMap * pPktMap)
{
    UINT1              *pu1Pdu = *ppu1Pdu;

    if (pPktMap->pErrorSpecObj != NULL)
    {
        RPTE_PUT_2_BYTES (pu1Pdu, OSIX_HTONS (sizeof (tErrorSpecObj)));
        RPTE_PUT_2_BYTES (pu1Pdu, OSIX_HTONS (IPV4_ERROR_SPEC_CLASS_NUM_TYPE));
        MEMCPY ((UINT1 *) pu1Pdu,
                (UINT1 *) &ERROR_SPEC_OBJ (PKT_MAP_ERROR_SPEC_OBJ (pPktMap)),
                sizeof (tErrorSpec));
        pu1Pdu += sizeof (tErrorSpec);
    }
    else if (pPktMap->pIfIdRsvpNumErrObj != NULL)
    {
        RPTE_PUT_2_BYTES (pu1Pdu, OSIX_HTONS (sizeof (tIfIdRsvpNumErrObj)));
        RPTE_PUT_2_BYTES (pu1Pdu,
                          OSIX_HTONS (IPV4_IF_ID_ERROR_SPEC_CLASS_NUM_TYPE));
        MEMCPY ((UINT1 *) pu1Pdu,
                (UINT1 *) &pPktMap->pIfIdRsvpNumErrObj->ErrorSpec,
                sizeof (tErrorSpec));
        pu1Pdu += sizeof (tErrorSpec);
        MEMCPY ((UINT1 *) pu1Pdu,
                (UINT1 *) &pPktMap->pIfIdRsvpNumErrObj->ErrorRsvpNumTlvObj,
                sizeof (tIfIdRsvpNumTlvObj));
        pu1Pdu += sizeof (tIfIdRsvpNumTlvObj);
    }
    else if (pPktMap->pIfIdRsvpErrObj != NULL)
    {
        RPTE_PUT_2_BYTES (pu1Pdu, OSIX_HTONS (sizeof (tIfIdRsvpErrObj)));
        RPTE_PUT_2_BYTES (pu1Pdu,
                          OSIX_HTONS (IPV4_IF_ID_ERROR_SPEC_CLASS_NUM_TYPE));
        MEMCPY ((UINT1 *) pu1Pdu,
                (UINT1 *) &pPktMap->pIfIdRsvpErrObj->ErrorSpec,
                sizeof (tErrorSpec));
        pu1Pdu += sizeof (tErrorSpec);
        MEMCPY ((UINT1 *) pu1Pdu,
                (UINT1 *) &pPktMap->pIfIdRsvpErrObj->ErrorRsvpTlvObj,
                sizeof (tIfIdRsvpTlvObj));
        pu1Pdu += sizeof (tIfIdRsvpTlvObj);
    }

    *ppu1Pdu = pu1Pdu;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillRsvpHopObj                                  */
/* Description     : This function fills the RSVP Hop Object                */
/* Input (s)       : pRsvpTeTnlInfo  - Pointer to RSVP-TE tnl info          */
/*                   ppObjHdr        - Pointe to object header              */
/*                   u4MsgType       - Message Type                         */
/* Output (s)      : ppObjHdr        - Pointe to object header              */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillRsvpHopObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo, tObjHdr ** ppObjHdr,
                       UINT4 u4MsgType)
{
    UINT4               u4Lih = RPTE_ZERO;
    tIfEntry           *pIfEntry = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    BOOL1               b1OobFlag = FALSE;
    UINT4               u4DataInterfaceId = RPTE_ZERO;
    UINT4               u4DataInterfaceIp = RPTE_ZERO;
    UINT4               u4TmpAddr = RSVPTE_ZERO;

    CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                        u4TmpAddr);
    u4TmpAddr = OSIX_NTOHL (u4TmpAddr);

    if ((u4MsgType == PATH_MSG) || (u4MsgType == RESVERR_MSG) ||
        (u4MsgType == PATHTEAR_MSG))
    {
        pIfEntry = pRsvpTeTnlInfo->pPsb->pOutIfEntry;

        if (pIfEntry == NULL)
        {
            return;
        }

        RpteUtlEncodeLih (&u4Lih, pIfEntry);
        b1OobFlag = pRsvpTeTnlInfo->b1DnStrOob;
        u4DataInterfaceId = pRsvpTeTnlInfo->u4DnStrDataTeLinkIfId;
        u4DataInterfaceIp = pRsvpTeTnlInfo->u4DnStrDataTeLinkIfAddr;
    }
    else
    {
        pIfEntry = PSB_IF_ENTRY (pRsvpTeTnlInfo->pPsb);

        if (pIfEntry == NULL)
        {
            return;
        }

        u4Lih = RSVP_HOP_LIH (&RSB_FWD_RSVP_HOP (pRsvpTeTnlInfo->pRsb));

        b1OobFlag = pRsvpTeTnlInfo->b1UpStrOob;
        u4DataInterfaceId = pRsvpTeTnlInfo->u4UpStrDataTeLinkIfId;
        u4DataInterfaceIp = pRsvpTeTnlInfo->u4UpStrDataTeLinkIfAddr;
    }

    if (b1OobFlag == FALSE)
    {
        if (IF_ENTRY_ADDR (pIfEntry) != RPTE_ZERO)
        {
            RptePvmFillRsvpNumHopObj (pObjHdr, IF_ENTRY_ADDR (pIfEntry), u4Lih);
        }
        else
        {
            RptePvmFillIfIdRsvpHopObj (pObjHdr, u4TmpAddr, u4DataInterfaceId,
                                       u4Lih);
        }
    }
    else
    {
        if (u4DataInterfaceId != RPTE_ZERO)
        {
            RptePvmFillIfIdRsvpHopObj (pObjHdr, u4TmpAddr, u4DataInterfaceId,
                                       u4Lih);
        }
        else
        {
            if (IF_ENTRY_ADDR (pIfEntry) != RPTE_ZERO)
            {
                u4TmpAddr = IF_ENTRY_ADDR (pIfEntry);
            }
            RptePvmFillIfIdNumRsvpHOpObj (pObjHdr, u4TmpAddr,
                                          u4DataInterfaceIp, u4Lih);
        }
    }

    **ppObjHdr = *pObjHdr;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillRsvpNumHopObj                               */
/* Description     : This function fills the rsvp hop info                  */
/* Input (s)       : pObjHdr        -  object header                        */
/*                   u4Addr         - Hop Address                           */
/*                   u4Lih          - LIH value                             */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillRsvpNumHopObj (tObjHdr * pObjHdr, UINT4 u4Addr, UINT4 u4Lih)
{
    tRsvpHopObj        *pRsvpHopObjHdr = NULL;

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tRsvpHopObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RSVP_HOP_CLASS_NUM_TYPE);

    pRsvpHopObjHdr = (tRsvpHopObj *) (VOID *) pObjHdr;
    RSVP_HOP_ADDR (&RSVP_HOP_OBJ (pRsvpHopObjHdr)) = OSIX_HTONL (u4Addr);

    RSVP_HOP_LIH (&RSVP_HOP_OBJ ((tRsvpHopObj *) (VOID *) pObjHdr)) = u4Lih;

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillIfIdRsvpHopObj                              */
/* Description     : This function fills the IF ID RSVP Hop Object          */
/* Input (s)       : pObjHdr        -  object header                        */
/*                   u4RouterId     - Router Id                             */
/*                   u4InterfaceId  - Interface Id                          */
/*                   u4Lih          - LIH value                             */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RptePvmFillIfIdRsvpHopObj (tObjHdr * pObjHdr, UINT4 u4RouterId,
                           UINT4 u4InterfaceId, UINT4 u4Lih)
{
    tIfIdRsvpHopObj    *pIfIdRsvpHopObjHdr = NULL;

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tIfIdRsvpHopObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_IF_ID_RSVP_HOP_CLASS_NUM_TYPE);
    pIfIdRsvpHopObjHdr = (tIfIdRsvpHopObj *) (VOID *) pObjHdr;

    RSVP_HOP_ADDR (&RSVP_HOP_OBJ (pIfIdRsvpHopObjHdr)) =
        OSIX_HTONL (u4RouterId);
    RSVP_HOP_LIH (&RSVP_HOP_OBJ ((tIfIdRsvpHopObj *) (VOID *) pObjHdr)) = u4Lih;

    pIfIdRsvpHopObjHdr->RsvpTlvObjHdr.u2Type =
        OSIX_HTONS (IPV4_IF_ID_SUB_OBJ_RSVP_TYPE);
    pIfIdRsvpHopObjHdr->RsvpTlvObjHdr.u2LengthValue =
        OSIX_HTONS (IPV4_IF_ID_SUB_OBJ_RSVP_LENGTH);
    pIfIdRsvpHopObjHdr->RsvpTlvObjHdr.u4HopAddr = OSIX_HTONL (u4RouterId);
    pIfIdRsvpHopObjHdr->RsvpTlvObjHdr.u4InterfaceId =
        OSIX_HTONL (u4InterfaceId);

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillIfIdNumRsvpHOpObj                           */
/* Description     : This function fills numbered IF ID RSVP Object         */
/* Input (s)       : pObjHdr        - object header                         */
/*                   u4ControlIfIp  - Control interface IP                  */
/*                   u4DateIfIP     - Data interface IP                     */
/*                   u4Lih          - LIH value                             */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillIfIdNumRsvpHOpObj (tObjHdr * pObjHdr, UINT4 u4ControlIfIp,
                              UINT4 u4DataIfIp, UINT4 u4Lih)
{
    tIfIdRsvpNumHopObj *pIfIdRsvpNumHopObjHdr = NULL;

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tIfIdRsvpNumHopObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_IF_ID_RSVP_HOP_CLASS_NUM_TYPE);

    pIfIdRsvpNumHopObjHdr = (tIfIdRsvpNumHopObj *) (VOID *) pObjHdr;

    RSVP_HOP_ADDR (&RSVP_HOP_OBJ (pIfIdRsvpNumHopObjHdr)) = OSIX_HTONL
        (u4ControlIfIp);
    RSVP_HOP_LIH (&RSVP_HOP_OBJ
                  ((tIfIdRsvpNumHopObj *) (VOID *) pObjHdr)) = u4Lih;

    pIfIdRsvpNumHopObjHdr->RsvpTlvNumObjHdr.u2Type =
        OSIX_HTONS (IPV4_IF_ID_NUM_SUB_OBJ_RSVP_TYPE);

    pIfIdRsvpNumHopObjHdr->RsvpTlvNumObjHdr.u2LengthValue =
        OSIX_HTONS (IPV4_IF_ID_NUM_SUB_OBJ_RSVP_LENGTH);
    pIfIdRsvpNumHopObjHdr->RsvpTlvNumObjHdr.u4HopAddr = OSIX_HTONL (u4DataIfIp);

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillSrcAndDestAddr                              */
/* Description     : This function fills Source and Destination address     */
/* Input (s)       : pPktMap - Packet map                                   */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillSrcAndDestAddr (tPktMap * pPktMap)
{
    UINT4               u4TmpAddr = RPTE_ZERO;

    /* If RSVP Hop Object is present, it means data channel and control
     * channel are not separated. Destination address should be filled
     * from received RSVP Hop Object. */
    if (pPktMap->pRsvpHopObj != NULL)
    {
        PKT_MAP_DST_ADDR (pPktMap) =
            OSIX_NTOHL (RSVP_HOP_ADDR
                        (&RSVP_HOP_OBJ (PKT_MAP_RSVP_HOP_OBJ (pPktMap))));
    }
    /* If IfId RSVP Hop Object is present, it means data channel and control
     * channel may be separated or not separated. Destination address should be
     * filled from received IFId RSVP Hop Object */
    else if (pPktMap->pIfIdRsvpHopObj != NULL)
    {
        PKT_MAP_DST_ADDR (pPktMap) =
            OSIX_NTOHL (pPktMap->pIfIdRsvpHopObj->RsvpHop.u4HopAddr);
    }
    /* If IfId Num RSVP Hop Object is present, it means data channel and control
     * channel is separated. Destination address should be filled from received
     * IfId RSVP Numbered Hop Object. */
    else if (pPktMap->pIfIdRsvpNumHopObj != NULL)
    {
        PKT_MAP_DST_ADDR (pPktMap) =
            OSIX_NTOHL (pPktMap->pIfIdRsvpNumHopObj->RsvpHop.u4HopAddr);
    }

    if (IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) != RSVPTE_ZERO)
    {
        PKT_MAP_SRC_ADDR (pPktMap) = IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap));
    }
    else
    {
        CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                            u4TmpAddr);
        u4TmpAddr = OSIX_NTOHL (u4TmpAddr);
        PKT_MAP_SRC_ADDR (pPktMap) = u4TmpAddr;
    }

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFormAndSendPEOrREMsg                            */
/* Description     : This function fills appropriate error spec object      */
/*                   and sends Path Error or Resv Error Message             */
/* Input (s)       : pPktMap        - Packet map                            */
/*                   u1ErrCode      - Error code                            */
/*                   u2ErrValue     - Error value                           */
/*                   u1MsgType      - Msg type                              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFormAndSendPEOrREMsg (tPktMap * pPktMap, UINT1 u1ErrCode,
                             UINT2 u2ErrValue, UINT1 u1MsgType)
{
    PRIVATE tErrorSpecObj ErrorSpecObj;
    PRIVATE tIfIdRsvpErrObj IfIdErrorSpecObj;
    PRIVATE tIfIdRsvpNumErrObj IfIdNumErrorSpecObj;
    UINT4               u4LocalIp = RSVPTE_ZERO;
    UINT4               u4LocalId = RSVPTE_ZERO;
    BOOL1               bOobFlag = RPTE_FALSE;

    if (pPktMap->pRsvpTeTnlInfo != NULL)
    {
        if (u1MsgType == RESVERR_MSG)
        {
            u4LocalIp = pPktMap->pRsvpTeTnlInfo->u4DnStrDataTeLinkIfAddr;
            u4LocalId = pPktMap->pRsvpTeTnlInfo->u4DnStrDataTeLinkIfId;
            bOobFlag = pPktMap->pRsvpTeTnlInfo->b1DnStrOob;
        }
        /* Here, intentionally check for PATHERR_MSG is skipped.
         * expecting intentional fall through here */
        else
        {
            u4LocalIp = pPktMap->pRsvpTeTnlInfo->u4UpStrDataTeLinkIfAddr;
            u4LocalId = pPktMap->pRsvpTeTnlInfo->u4UpStrDataTeLinkIfId;
            bOobFlag = pPktMap->pRsvpTeTnlInfo->b1UpStrOob;
        }
    }

    /* Tunnel Information is stored in Packet Map if receieved 
     * message processing is successfull and PSB is created.
     *
     * Tunnel Information is not stored in Packet Map if received
     * message processing fails.
     *
     * Error Message can be sent on both the above occassions. Here,
     * control-data channel separation and numbered-unnumbered identification
     * has to be done and appropriate Error Spec objects to be filled.
     */

    /* For Oob case, RSVP-TE Tunnel Info will not be NULL. Data Interface Index
     * TLV will be filled in that case for both numbered and unnumbered. */
    if (bOobFlag == RPTE_TRUE)
    {
        if (u4LocalId != RSVPTE_ZERO)
        {
            pPktMap->pIfIdRsvpErrObj = &IfIdErrorSpecObj;
            RptePvmFormIfIdErrorSpecObj (pPktMap, &IfIdErrorSpecObj, u1ErrCode,
                                         u2ErrValue, u4LocalId);
        }
        else
        {
            pPktMap->pIfIdRsvpNumErrObj = &IfIdNumErrorSpecObj;
            RptePvmFormIfIdNumErrorSpecObj (pPktMap, &IfIdNumErrorSpecObj,
                                            u1ErrCode, u2ErrValue, u4LocalIp);
        }
    }
    /* In Band Numbered case - Normal Path Error is sent here */
    else if (IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) != RSVPTE_ZERO)
    {
        PKT_MAP_ERROR_SPEC_OBJ (pPktMap) = &ErrorSpecObj;
        RptePvmFormNormalErrorSpecObj (pPktMap, &ErrorSpecObj, u1ErrCode,
                                       u2ErrValue);
    }
    /* In Band unnumbered case, IfId RSVP Error Spec to be sent. 
     * Data Interface Index will be filled correctly if RSVP-TE Tunnel
     * Info is created.
     * Data Interface Index will be filled as ZERO if RSVP-TE Tunnel Info
     * is not created.
     */
    else
    {
        pPktMap->pIfIdRsvpErrObj = &IfIdErrorSpecObj;
        RptePvmFormIfIdErrorSpecObj (pPktMap, &IfIdErrorSpecObj, u1ErrCode,
                                     u2ErrValue, u4LocalId);
    }

    if (u1MsgType == PATHERR_MSG)
    {
        if ((pPktMap->pRsvpTeTnlInfo != NULL) &&
            (pPktMap->pRsvpTeTnlInfo->pPsb != NULL) &&
            (pPktMap->pRsvpTeTnlInfo->pPsb->u4NotifyAddr != RPTE_ZERO))
        {
            RpteNhAddTnlToNotifyRecipient (pPktMap->pRsvpTeTnlInfo,
                                           pPktMap->pRsvpTeTnlInfo->pPsb->
                                           u4NotifyAddr, DOWNSTREAM, RPTE_FALSE,
                                           u1ErrCode, u2ErrValue);
        }
        RptePvmConstructAndSndPEMsg (pPktMap);
    }
    else if (u1MsgType == RESVERR_MSG)
    {
        if ((pPktMap->pRsvpTeTnlInfo != NULL) &&
            (pPktMap->pRsvpTeTnlInfo->pRsb != NULL) &&
            (pPktMap->pRsvpTeTnlInfo->pRsb->u4NotifyAddr != RPTE_ZERO))
        {
            RpteNhAddTnlToNotifyRecipient (pPktMap->pRsvpTeTnlInfo,
                                           pPktMap->pRsvpTeTnlInfo->pRsb->
                                           u4NotifyAddr, UPSTREAM, RPTE_FALSE,
                                           u1ErrCode, u2ErrValue);
        }
        RptePvmConstructAndSndREMsg (pPktMap);
    }
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFormNormalErrorSpecObj                          */
/* Description     : This function fills Numbered Error Spec Object         */
/* Input (s)       : pPktMap        - Packet map                            */
/*                   pErrorSpecObj  - Error spec obj                        */
/*                   u1ErrCode      - Error code                            */
/*                   u2ErrValue     - Error value                           */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFormNormalErrorSpecObj (tPktMap * pPktMap,
                               tErrorSpecObj * pErrorSpecObj, UINT1 u1ErrCode,
                               UINT2 u2ErrValue)
{

    UINT4               u4TempAddr = RSVPTE_ZERO;

    if ((u2ErrValue == RPTE_LOCAL_LINK_MAINTENANCE)
        && (pPktMap->pRsvpTeTnlInfo != NULL))
    {
        ERROR_SPEC_ADDR (&ERROR_SPEC_OBJ (pErrorSpecObj)) =
            OSIX_HTONL (RPTE_REOPT_LINK_MAINTENANCE_ADDR
                        (pPktMap->pRsvpTeTnlInfo));

    }
    else if (u2ErrValue == RPTE_LOCAL_NODE_MAINTENANCE)
    {
        CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                            u4TempAddr);
        ERROR_SPEC_ADDR (&ERROR_SPEC_OBJ (pErrorSpecObj)) = u4TempAddr;

    }
    else
    {
        ERROR_SPEC_ADDR (&ERROR_SPEC_OBJ (pErrorSpecObj)) =
            OSIX_HTONL (IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)));
    }

    /* Fill the Errospecc flag value */
    RptePvmFillErrorSpecFlags (&(ERROR_SPEC_FLAGS (&ERROR_SPEC_OBJ
                                                   (pErrorSpecObj))));

    ERROR_SPEC_CODE (&ERROR_SPEC_OBJ (pErrorSpecObj)) = u1ErrCode;
    ERROR_SPEC_VALUE (&ERROR_SPEC_OBJ (pErrorSpecObj)) =
        OSIX_HTONS (u2ErrValue);

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFormIfIdNumErrorSpecObj                         */
/* Description     : This function fills the numbered If Id Errorspec obj   */
/* Input (s)       : pPktMap            - Pointer to the Packet Map         */
/*                   pIfIdRsvpNumErrObj - If Id Rsvp Num Err Object         */
/*                   u1ErrCode      - Error code                            */
/*                   u2ErrValue     - Error value                           */
/*                   u4LocalIp      - Outgoing Data Channel IP              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFormIfIdNumErrorSpecObj (tPktMap * pPktMap,
                                tIfIdRsvpNumErrObj * pIfIdNumErrorSpecObj,
                                UINT1 u1ErrCode, UINT2 u2ErrValue,
                                UINT4 u4DataOutIp)
{
    UINT4               u4TempAddr = RSVPTE_ZERO;

    if ((u2ErrValue == RPTE_LOCAL_LINK_MAINTENANCE)
        && (pPktMap->pRsvpTeTnlInfo != NULL))
    {
        pIfIdNumErrorSpecObj->ErrorSpec.u4ErrNodeAddr =
            OSIX_HTONL (RPTE_REOPT_LINK_MAINTENANCE_ADDR
                        (pPktMap->pRsvpTeTnlInfo));

    }
    else if (u2ErrValue == RPTE_LOCAL_NODE_MAINTENANCE)
    {
        CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                            u4TempAddr);
        pIfIdNumErrorSpecObj->ErrorSpec.u4ErrNodeAddr = u4TempAddr;

    }
    else
    {
        pIfIdNumErrorSpecObj->ErrorSpec.u4ErrNodeAddr =
            OSIX_HTONL (IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)));
    }
    /* Fill the Error Spec Flag value */
    RptePvmFillErrorSpecFlags (&(pIfIdNumErrorSpecObj->ErrorSpec.u1Flags));

    pIfIdNumErrorSpecObj->ErrorSpec.u1ErrCode = u1ErrCode;
    pIfIdNumErrorSpecObj->ErrorSpec.u2ErrValue = OSIX_HTONS (u2ErrValue);

    pIfIdNumErrorSpecObj->ErrorRsvpNumTlvObj.u2Type
        = OSIX_HTONS (IPV4_IF_ID_NUM_SUB_OBJ_RSVP_TYPE);
    pIfIdNumErrorSpecObj->ErrorRsvpNumTlvObj.u2LengthValue
        = OSIX_HTONS (IPV4_IF_ID_NUM_SUB_OBJ_RSVP_LENGTH);

    pIfIdNumErrorSpecObj->ErrorRsvpNumTlvObj.u4HopAddr
        = OSIX_HTONL (u4DataOutIp);

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFormIfIdErrorSpecObj                            */
/* Description     : This function fills the unnumbered If Id Errorspec obj */
/* Input (s)       : pPktMap        - Packet map                            */
/*                   pIfIdNumErrorSpecObj  - Unmbered If Id Error spec obj  */
/*                   u1ErrCode      - Error code                            */
/*                   u2ErrValue     - Error value                           */
/*                   u4LocalId      - Local Identifier                      */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFormIfIdErrorSpecObj (tPktMap * pPktMap,
                             tIfIdRsvpErrObj * pIfIdErrorSpecObj,
                             UINT1 u1ErrCode, UINT2 u2ErrValue, UINT4 u4LocalId)
{
    UINT4               u4TempAddr = RSVPTE_ZERO;

    UNUSED_PARAM (pPktMap);

    CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                        u4TempAddr);
    pIfIdErrorSpecObj->ErrorSpec.u4ErrNodeAddr = u4TempAddr;

    /* Fill the Error Spec flag value */
    RptePvmFillErrorSpecFlags (&(pIfIdErrorSpecObj->ErrorSpec.u1Flags));

    pIfIdErrorSpecObj->ErrorSpec.u1ErrCode = u1ErrCode;
    pIfIdErrorSpecObj->ErrorSpec.u2ErrValue = OSIX_HTONS (u2ErrValue);

    /* Fill IF ID TLV */
    pIfIdErrorSpecObj->ErrorRsvpTlvObj.u2Type
        = OSIX_HTONS (IPV4_IF_ID_SUB_OBJ_RSVP_TYPE);
    pIfIdErrorSpecObj->ErrorRsvpTlvObj.u2LengthValue
        = OSIX_HTONS (IPV4_IF_ID_SUB_OBJ_RSVP_LENGTH);

    /* u4TempAddr is already in Network order */
    pIfIdErrorSpecObj->ErrorRsvpTlvObj.u4HopAddr = u4TempAddr;
    pIfIdErrorSpecObj->ErrorRsvpTlvObj.u4InterfaceId = OSIX_HTONL (u4LocalId);

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillAdminStatusObj
 * Description     : Function to update the Admin Status Object in Path message
 * Input (s)       : pRsvpTeTnlInfo - RSVP-TE tunnel informations.
 * Output (s)      : ppObjHdr       - Pointer to Object Header.
 * Returns         : None
 *
 ****************************************************************************/
VOID
RptePvmFillAdminStatusObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo, tObjHdr ** ppObjHdr,
                           UINT4 u4MsgType)
{
    tAdminStatusObj    *pAdminStatusObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT4               u4PrevAdminStatus = RPTE_ZERO;
    UINT4               u4CurAdminStatus = RPTE_ZERO;

    if (u4MsgType == PATH_MSG)
    {
        u4PrevAdminStatus = pRsvpTeTnlInfo->pPsb->AdminStatusObj.
            AdminStatus.u4AdminStatus;
        u4CurAdminStatus =
            pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus;
        pRsvpTeTnlInfo->pPsb->AdminStatusObj.AdminStatus.u4AdminStatus =
            u4CurAdminStatus;
    }
    else
    {
        u4PrevAdminStatus = pRsvpTeTnlInfo->pRsb->AdminStatusObj.
            AdminStatus.u4AdminStatus;
        u4CurAdminStatus = pRsvpTeTnlInfo->pRsb->u4AdminStatus;
        pRsvpTeTnlInfo->pRsb->AdminStatusObj.AdminStatus.u4AdminStatus
            = u4CurAdminStatus;
    }

    /* do the action item needed for particular flag */
    if (u4PrevAdminStatus != u4CurAdminStatus)
    {
        RptePhAdminStatusActionItem (pRsvpTeTnlInfo);
    }

    if ((u4CurAdminStatus == GMPLS_ADMIN_UNKNOWN) ||
        (gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable != RPTE_ENABLED))
    {
        return;
    }

    pObjHdr->u2Length = OSIX_HTONS (sizeof (tAdminStatusObj));
    pObjHdr->u2ClassNumType = OSIX_HTONS (RPTE_ADMIN_STATUS_CLASS_NUM_TYPE);
    pAdminStatusObj = (tAdminStatusObj *) (VOID *) (pObjHdr);

    pObjHdr = NEXT_OBJ (pObjHdr);
    pAdminStatusObj->AdminStatus.u4AdminStatus = OSIX_HTONL (u4CurAdminStatus);
    *ppObjHdr = pObjHdr;
    *ppObjHdr = pObjHdr;
    return;

}

/****************************************************************************/
/* Function Name   : RptePvmFillProtectionObj                                */
/* Description     : This function fills the Protection Object              */
/* Input (s)       : pObjHdr        -  object header                        */
/*                   u4RouterId     - Router Id                             */
/*                   u4InterfaceId  - Interface Id                          */
/*                   u4Lih          - LIH value                             */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RptePvmFillProtectionObj (tObjHdr ** ppObjHdr, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tProtectionObj     *pProtectionObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RptePvmFillProtectionObj : ENTRY \n");

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tProtectionObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (RPTE_PROTEC_OBJ_CLASS_NUM_TYPE);
    pProtectionObj = (tProtectionObj *) (VOID *) pObjHdr;

    if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1Secondary == RPTE_TRUE)
    {
        pProtectionObj->Protection.u1ProtectionFlag |=
            RPTE_PROTECTION_SECONDARY;
    }
    else if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType ==
              RPTE_TNL_PROTECTION_PATH) &&
             (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INTERMEDIATE))
    {
        pProtectionObj->Protection.u1ProtectionFlag |=
            RPTE_PROTECTION_PROTECTING;
        if ((pRsvpTeTnlInfo->pMapTnlInfo != NULL)
            && (pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
                u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
        {
            pProtectionObj->Protection.u1ProtectionFlag |=
                RPTE_PROTECTION_OPERATIONAL;
        }

    }
    else if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INTERMEDIATE)
    {
        if ((RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) != NULL)
        {
            pProtectionObj->Protection.u1ProtectionFlag =
                pRsvpTeTnlInfo->pPsb->Protection.u1ProtectionFlag;
        }
    }

    pProtectionObj->Protection.u1LspFlag = (UINT1) pRsvpTeTnlInfo->pTeTnlInfo->
        GmplsTnlInfo.i4E2EProtectionType;
    pProtectionObj->Protection.u1Resv1 = RPTE_ZERO;
    pProtectionObj->Protection.u1LinkFlag = pRsvpTeTnlInfo->pTeTnlInfo->
        GmplsTnlInfo.u1LinkProtection;
    pProtectionObj->Protection.u4Resv2 = RPTE_ZERO;
    pObjHdr = NEXT_OBJ (pObjHdr);
    *ppObjHdr = pObjHdr;

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillAssociationObj                              */
/* Description     : This function fills the IF ID RSVP Hop Object          */
/* Input (s)       : pObjHdr        -  object header                        */
/*                   u4RouterId     - Router Id                             */
/*                   u4InterfaceId  - Interface Id                          */
/*                   u4Lih          - LIH value                             */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RptePvmFillAssociationObj (tObjHdr ** ppObjHdr, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tAssociationObj    *pAssociationObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT4               u4IngressId = RPTE_ZERO;

    CONVERT_TO_INTEGER ((RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL
                                                    (pRsvpTeTnlInfo))),
                        u4IngressId);

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tAssociationObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (RPTE_ASSOC_OBJ_CLASS_NUM_TYPE);
    pAssociationObj = (tAssociationObj *) (VOID *) pObjHdr;

    pAssociationObj->Association.u2AssocType = RPTE_ONE;

    if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
        MPLS_TE_DEDICATED_ONE2ONE)
    {
        pAssociationObj->Association.u2AssocID =
            OSIX_HTONS (pRsvpTeTnlInfo->pPsb->Association.u2AssocID);
    }
    else if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
             MPLS_TE_FULL_REROUTE)
    {
        pAssociationObj->Association.u2AssocID =
            OSIX_HTONS (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);
    }
    pAssociationObj->Association.u4AssocSource = OSIX_HTONL (u4IngressId);
    pObjHdr = NEXT_OBJ (pObjHdr);
    *ppObjHdr = pObjHdr;

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillNotifyReqObj                                */
/* Description     : This function fills the Notify request object          */
/* Input (s)       : pObjHdr        -  object header                        */
/*                   pRsvpTeTnlInfo -  Pointer to RSVPTE-tnl info           */
/*                   u4MsgType      -  Message type                         */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RptePvmFillNotifyReqObj (tObjHdr ** ppObjHdr, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                         UINT4 u4MsgType)
{
    tNotifyRequestObj  *pNotifyRequestObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT4               u4NotifyAddr = RPTE_ZERO;

    if (u4MsgType == RESV_MSG)
    {
        u4NotifyAddr = pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
            u4SendResvNotifyRecipient;
    }
    if (u4MsgType == PATH_MSG)
    {
        u4NotifyAddr = pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
            u4SendPathNotifyRecipient;
    }

    if (u4NotifyAddr == RPTE_ZERO)
    {
        return;
    }

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tNotifyRequestObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (RPTE_NOTIFY_REQUEST_CLASS_NUM_TYPE);
    pNotifyRequestObj = (tNotifyRequestObj *) (VOID *) pObjHdr;
    pNotifyRequestObj->u4NotifyNodeAddr = OSIX_HTONL (u4NotifyAddr);
    pObjHdr = NEXT_OBJ (pObjHdr);
    *ppObjHdr = pObjHdr;

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillMsgIdObj                                    */
/* Description     : This function fills the Message Id Object              */
/* Input (s)       : pObjHdr        -  object header                        */
/*                   u4MsgId        - Message Id                            */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RptePvmFillMsgIdObj (tObjHdr ** ppObjHdr, UINT4 u4MsgId)
{
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT1              *pPdu = NULL;
    UINT4               u4Val = RPTE_ZERO;

    /* Filling the Header */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_MSG_ID_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (RPTE_MESSAGE_ID_CLASS_NUM_TYPE);

    pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

    /* Filling the Epoch and Flags fields */
    u4Val = (gu4Epoch | (RPTE_MSG_ID_ACK_DESIRED << RPTE_3_BYTE_SHIFT));
    u4Val = OSIX_HTONL (u4Val);
    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4Val, RPTE_FLAG_EPOCH_LEN);

    pPdu += RPTE_FLAG_EPOCH_LEN;

    /* Filling the Message Identifier field */
    u4Val = OSIX_HTONL (u4MsgId);
    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4Val, RPTE_MSG_ID_LEN);
    pPdu += RPTE_MSG_ID_LEN;
    *ppObjHdr = pObjHdr;

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillSsnObj                                      */
/* Description     : This function fills the session Object                 */
/* Input (s)       : pObjHdr        -  object header                        */
/*                   TnlDest        - tunnel destination                    */
/*                   u4TnlIndex     - tunnel index                          */
/*                   ExtnTnlId      - extented tunnel id                    */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RptePvmFillSsnObj (tObjHdr ** ppObjHdr, tIpv4Addr TnlDest, UINT4 u4TnlIndex,
                   tIpv4Addr ExtnTnlId)
{
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT1              *pPdu = NULL;
    UINT4               u4TmpAddr = RPTE_ZERO;

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pObjHdr = (tObjHdr *) (VOID *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

    pPdu = (UINT1 *) pObjHdr;

    CONVERT_TO_INTEGER ((TnlDest), u4TmpAddr);
    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);    /* reserved field */
    RPTE_PUT_2_BYTES (pPdu, (UINT2) OSIX_HTONS (u4TnlIndex));

    CONVERT_TO_INTEGER ((ExtnTnlId), u4TmpAddr);

    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;
    pObjHdr = (tObjHdr *) (VOID *) pPdu;
    *ppObjHdr = pObjHdr;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillSndrTempObj                                 */
/* Description     : This function fills the sender template Object         */
/* Input (s)       : pObjHdr        -  object header                        */
/*                   TnlSndrAddr    - tunnel source                         */
/*                   u2LspId        - LSP Id                                */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePvmFillSndrTempObj (tObjHdr ** ppObjHdr, tIpv4Addr TnlSndrAddr,
                        UINT2 u2LspId)
{
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT1              *pPdu = NULL;
    UINT4               u4TmpAddr = RPTE_ZERO;
    UINT2               u2TmpVar = RPTE_ZERO;

    pPdu = (UINT1 *) pObjHdr;
    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (RPTE_IPV4_SNDR_TMP_OBJ_LEN));

    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE));
    CONVERT_TO_INTEGER (TnlSndrAddr, u4TmpAddr);
    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);    /* reserved field */
    u2TmpVar = OSIX_HTONS (u2LspId);
    MEMCPY (pPdu, (UINT1 *) &u2TmpVar, sizeof (UINT2));
    pPdu += sizeof (UINT2);
    pObjHdr = (tObjHdr *) (VOID *) pPdu;
    *ppObjHdr = pObjHdr;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillFilterSpecObj                               */
/* Description     : This function fills the Filter spect Object            */
/* Input (s)       : pObjHdr        -  object header                        */
/*                   TnlSndrAddr    - tunnel source                         */
/*                   u2LspId        - LSP Id                                */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RptePvmFillFilterSpecObj (tObjHdr ** ppObjHdr, tIpv4Addr TnlSndrAddr,
                          UINT2 u2LspId)
{
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT1              *pPdu = NULL;
    UINT4               u4TmpAddr = RPTE_ZERO;
    UINT2               u2TmpVar = RPTE_ZERO;

    pPdu = (UINT1 *) pObjHdr;
    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (RPTE_IPV4_FLTR_SPEC_OBJ_LEN));

    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE));
    CONVERT_TO_INTEGER (TnlSndrAddr, u4TmpAddr);
    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);    /* reserved field */
    u2TmpVar = OSIX_HTONS (u2LspId);
    MEMCPY (pPdu, (UINT1 *) &u2TmpVar, sizeof (UINT2));
    pPdu += sizeof (UINT2);
    pObjHdr = (tObjHdr *) (VOID *) pPdu;
    *ppObjHdr = pObjHdr;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillLSPTnlIfIdObj                               */
/* Description     : This function fills the LSP Tnl If Id object           */
/* Input (s)       : pObjHdr        -  object header                        */
/*                   u4InterfaceId  - Interface Id                          */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/

VOID
RptePvmFillLSPTnlIfIdObj (tObjHdr ** ppObjHdr, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                          UINT4 u4MsgType)
{
    tLspTnlIfIdObj     *pLspTnlIfIdObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT4               u4TmpAddr = RPTE_ZERO;

    /* LSP_TUNNEL_INTERFACE_ID object needs to be filled only when the
     * tunnel type is hlsp and the tunnel is operationlly up. i.e., after
     * it has received First RESV Message */
    if ((!(pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP)) ||
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlOperStatus != MPLS_OPER_UP))
    {
        return;
    }

    pLspTnlIfIdObj = (tLspTnlIfIdObj *) (VOID *) pObjHdr;
    if ((gi4MplsSimulateFailure == RPTE_SIM_FAILURE_LSP_TNL_IFID_CTYPE_1) ||
        (pRsvpTeTnlInfo->u2HlspCType ==
         RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE_1))
    {
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE_1);
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS ((sizeof (tLspTnlIfIdObj) -
                                                sizeof (UINT4)));
    }
    else
    {
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE);
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tLspTnlIfIdObj));
    }

    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INTERMEDIATE)
    {
        CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                            u4TmpAddr);
        u4TmpAddr = OSIX_NTOHL (u4TmpAddr);

        pLspTnlIfIdObj->LspTnlIfId.u4RouterId = OSIX_HTONL (u4TmpAddr);
    }
    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        pLspTnlIfIdObj->LspTnlIfId.u4InterfaceId =
            OSIX_HTONL (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIfIndex);
    }
    else if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS)
    {
        pLspTnlIfIdObj->LspTnlIfId.u4InterfaceId =
            OSIX_HTONL (pRsvpTeTnlInfo->pTeTnlInfo->u4RevTnlIfIndex);
    }
    else
    {
        if (u4MsgType == PATH_MSG)
        {
            pLspTnlIfIdObj->LspTnlIfId.u4InterfaceId =
                OSIX_HTONL (PSB_LSP_TNL_IF_ID (pRsvpTeTnlInfo->pPsb).
                            u4InterfaceId);
            pLspTnlIfIdObj->LspTnlIfId.u4RouterId =
                OSIX_HTONL (PSB_LSP_TNL_IF_ID (pRsvpTeTnlInfo->pPsb).
                            u4RouterId);
        }
        else
        {
            pLspTnlIfIdObj->LspTnlIfId.u4InterfaceId =
                OSIX_HTONL (RSB_LSP_TNL_IF_ID (pRsvpTeTnlInfo->pRsb).
                            u4InterfaceId);
            pLspTnlIfIdObj->LspTnlIfId.u4RouterId =
                OSIX_HTONL (RSB_LSP_TNL_IF_ID (pRsvpTeTnlInfo->pRsb).
                            u4RouterId);
        }
    }

    pLspTnlIfIdObj->LspTnlIfId.u1Actions = RPTE_ZERO;
    pObjHdr = NEXT_OBJ (pObjHdr);
    *ppObjHdr = pObjHdr;

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillRestartCapObj
 * Description     : Function to update the Restart_Cap Object in Hell0 message
 * Input (s)       : u2RestartTime  - Restart time value.
 *                   u2RecoveryTime - Recovery time value.
 * Output (s)      : ppObjHdr       - Pointer to Object Header.
 * Returns         : None
 *
 ****************************************************************************/
VOID
RptePvmFillRestartCapObj (UINT2 u2RestartTime, UINT2 u2RecoveryTime,
                          tObjHdr ** ppObjHdr)
{
    tRestartCapObj     *pRestartCapObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    UINT4               u4RestartTime = RPTE_ZERO;
    UINT4               u4RecoveryTime = RPTE_ZERO;
    if (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_NO_RESTART_CAP_OBJ)
    {
        return;
    }
    pObjHdr->u2Length = OSIX_HTONS (sizeof (tRestartCapObj));
    pObjHdr->u2ClassNumType = OSIX_HTONS (RPTE_GR_RESTART_CAP_CLASS_NUM_TYPE);
    pRestartCapObj = (tRestartCapObj *) (VOID *) (pObjHdr);
    pObjHdr = NEXT_OBJ (pObjHdr);
    u4RestartTime = (UINT4) u2RestartTime;
    u4RecoveryTime = (UINT4) u2RecoveryTime;
    pRestartCapObj->RestartCap.u4RestartTime = OSIX_HTONL (u4RestartTime);
    pRestartCapObj->RestartCap.u4RecoveryTime = OSIX_HTONL (u4RecoveryTime);

    *ppObjHdr = pObjHdr;
    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillCapabilityObj
 * Description     : Function to update the Capability Object in Hello message
 * Input (s)       : pNbrEntry      - Pointer to neighbour entry.
 *                   u1RecoveryPathCapability - Recovery path capabiluty value
 * Output (s)      : ppObjHdr       - Pointer to Object Header.
 * Returns         : None
 *
 ****************************************************************************/
VOID
RptePvmFillCapabilityObj (tObjHdr ** ppObjHdr, tNbrEntry * pNbrEntry,
                          UINT1 u1RecoveryPathCapability)
{
    UINT1               u1PktRecoveryPath = RPTE_ZERO;
    UINT4               u4RecoveryPath = RPTE_ZERO;
    tCapabilityObj     *pCapabilityObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;

    /* if S bit is set and R bit is not set in the received hello message,
     * helper should send hello message without capability object
     * */
    if ((pNbrEntry->u2RestartTime != RPTE_ZERO) &&
        (pNbrEntry->u1RecoveryPathCapability & RPTE_GR_RECOVERY_PATH_SREFRESH)
        && (!(pNbrEntry->u1RecoveryPathCapability & RPTE_GR_RECOVERY_PATH_RX)))
    {
        return;
    }
    if (gi4MplsSimulateFailure == RPTE_SIM_FIALURE_NO_RBIT)
    {
        u1RecoveryPathCapability &= (UINT1) (~RPTE_GR_RECOVERY_PATH_RX);
    }

    if (u1RecoveryPathCapability & RPTE_GR_RECOVERY_PATH_RX)
    {
        u1PktRecoveryPath |= RPTE_GR_PKT_RECOVERY_PATH_RX;
    }
    if (u1RecoveryPathCapability & RPTE_GR_RECOVERY_PATH_TX)
    {
        u1PktRecoveryPath |= RPTE_GR_PKT_RECOVERY_PATH_TX;
    }
    if (u1RecoveryPathCapability & RPTE_GR_RECOVERY_PATH_SREFRESH)
    {
        u1PktRecoveryPath |= RPTE_GR_PKT_RECOVERY_PATH_SREFRESH;
    }
    pObjHdr->u2Length = OSIX_HTONS (sizeof (tCapabilityObj));
    pObjHdr->u2ClassNumType = OSIX_HTONS (RPTE_GR_CAPABILITY_CLASS_NUM_TYPE);
    pCapabilityObj = (tCapabilityObj *) (VOID *) (pObjHdr);
    u4RecoveryPath = (UINT4) u1PktRecoveryPath;
    pCapabilityObj->Capability.u4RecoveryPathCapable =
        OSIX_HTONL (u4RecoveryPath);

    *ppObjHdr = pObjHdr;
    return;
}

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        /****************************************************************************//* function Name   : RptePvmFillRecoveryLblObj
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         * Description     : Function to update the Recovery label object in
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         *                   Path message
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         * Input (s)       : tRsvpTeTnlInfo - RSVP-TE tunnel informations.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         * Output (s)      : ppObjHdr - Updated PATH message.
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         * Returns         : None
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         *         */
/****************************************************************************/
VOID
RptePvmFillRecoveryLblObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                           tObjHdr ** ppObjHdr, UINT1 u1MsgType)
{
    tRecoveryLblObj    *pRecoveryLblObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    tNbrEntry          *pNbrEntry = NULL;
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4Label = RPTE_ZERO;

    if (u1MsgType == PATH_MSG)
    {
        pNbrEntry = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
        pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
        u4Label = pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl;
    }
    else
    {
        pNbrEntry = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);
        u4Label = pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl;
        pIfEntry = PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    }

    if (pNbrEntry == NULL)
    {
        return;
    }
    /* For Nodal faults only, Recovery label object is required */
    if (pNbrEntry->u1GrFaultType != RPTE_GR_NODAL_FAULT)
    {
        return;
    }

    if ((pNbrEntry->u1GrProgressState != RPTE_GR_RECOVERY_IN_PROGRESS) ||
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus !=
         TNL_GR_NOT_SYNCHRONIZED))
    {
        return;
    }
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tRecoveryLblObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (RPTE_RECOVERY_GLBL_CLASS_NUM_TYPE);

    pRecoveryLblObj = (tRecoveryLblObj *) (VOID *) pObjHdr;
    pObjHdr = NEXT_OBJ (pObjHdr);

    pRecoveryLblObj->Label.u4GenLbl = OSIX_HTONL (u4Label);

    *ppObjHdr = pObjHdr;

    pIfEntry->IfStatsInfo.u4IfNumPathSentWithRecoveryLbl++;

    return;
}

                                                                                                                              /****************************************************************************/
/* function Name   : RptePvmFillSuggestedLblObj
 * Description     : Function to update the suggested label object in
 *                   Path message
 * Input (s)       : tRsvpTeTnlInfo - RSVP-TE tunnel informations.
 * Output (s)      : ppObjHdr - Updated PATH message.
 * Returns         : None
 */
/****************************************************************************/
VOID
RptePvmFillSuggestedLblObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                            tObjHdr ** ppObjHdr)
{
    tSuggestedLblObj   *pSuggestedLblObj = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    tIfEntry           *pIfEntry = NULL;

    if ((gpRsvpTeGblInfo->u1GrProgressState != RPTE_GR_RECOVERY_IN_PROGRESS) ||
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus !=
         TNL_GR_FULLY_SYNCHRONIZED)
        || (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_NO_SUGGESTED_LBL_OBJ))
    {
        return;
    }
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tSuggestedLblObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (RPTE_SUGGESTED_GLBL_CLASS_NUM_TYPE);

    pSuggestedLblObj = (tSuggestedLblObj *) (VOID *) pObjHdr;
    pObjHdr = NEXT_OBJ (pObjHdr);

    if (gi4MplsSimulateFailure != RPTE_SIM_FAILURE_NO_SUGGESTED_LBL)
    {
        pSuggestedLblObj->Label.u4GenLbl =
            OSIX_HTONL (pRsvpTeTnlInfo->DownstrOutLbl.u4GenLbl);
    }
    else
    {
        pSuggestedLblObj->Label.u4GenLbl = MPLS_INVALID_LABEL;
    }
    *ppObjHdr = pObjHdr;

    pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    pIfEntry->IfStatsInfo.u4IfNumPathSentWithSuggestedLbl++;

    return;
}

/****************************************************************************/
/* Function Name   : RptePvmFillEROForRecoveryPathMsg
 * Description     : Function to update the ERO object in RecoveryPath message
 * Input (s)       : pRsvpTeTnlInfo - RSVP-TE tunnel informations.
 *                   u2EroSize      - Size of ERO object
 * Output (s)      : ppObjHdr       - Pointer to Object Header.
 * Returns         : None
 *         */
/****************************************************************************/
VOID
RptePvmFillEROForRecoveryPathMsg (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                  UINT2 u2EroSize, tObjHdr ** ppObjHdr)
{
    UINT1              *pPdu = NULL;
    UINT1               u1LabelType = RPTE_LBL_CTYPE;
    tTMO_SLL           *pEROList = NULL;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    tObjHdr            *pObjHdr = *ppObjHdr;
    tRsvpTeGetTnlInfo   RsvpTeGetTnlInfo;

    /*Get the required tunnel information */
    MEMSET (&RsvpTeGetTnlInfo, RSVPTE_ZERO, sizeof (tRsvpTeGetTnlInfo));

    RpteTeGetTnlInfo (&RsvpTeGetTnlInfo, pRsvpTeTnlInfo);
    if (RsvpTeGetTnlInfo.u1EncodingType != RSVPTE_ZERO)
    {
        u1LabelType = RPTE_GLBL_CTYPE;
    }

    /* Object Header Updations */
    pObjHdr->u2Length = OSIX_HTONS (u2EroSize);
    pObjHdr->u2ClassNumType = OSIX_HTONS (RPTE_ERO_CLASS_NUM_TYPE);

    pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

    pEROList = &pRsvpTeTnlInfo->pTePathInfo->ErHopList;

    /* ERO object updation using the explicit Hop list */
    TMO_SLL_Scan (pEROList, pRsvpTeErHopInfo, tRsvpTeHopInfo *)
    {
        RptePvmFillEROFromERHop (&pPdu, pRsvpTeErHopInfo, u1LabelType);
    }

    pObjHdr = NEXT_OBJ (pObjHdr);
    *ppObjHdr = pObjHdr;
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptepvm.c                             */
/*---------------------------------------------------------------------------*/
