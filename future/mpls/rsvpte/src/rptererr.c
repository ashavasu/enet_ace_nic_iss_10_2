/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: rptererr.c,v 1.10 2017/06/08 11:40:32 siva Exp $
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptererr.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for the ResvErr
 *                             Module.                                  
 *                                             
 *---------------------------------------------------------------------------*/

#include "rpteincs.h"

/****************************************************************************/
/* Function Name   : RpteRehProcessResvErrMsg                               */
/* Description     : This function receives  Resv Error message, process it.*/
/*                   If the node is Egress node then the mesg, if it needs  */
/*                   to be forwarded to the Ingress it is done,else dropped.*/
/*                   In all other nodes it is Forwarded                     */
/* Input (s)       : *pPktMap - Pointer to Packet Map                       */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRehProcessResvErrMsg (tPktMap * pPktMap)
{
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTePrevInTnlInfo = NULL;
    tRsvpTeSession     *pRsvpTeSession = NULL;
    tPsb               *pPsb = NULL;
    tFlowSpec          *pBFlowSpec = NULL;
    tRsb               *pRsb = NULL;
    tStyle             *pStyle = NULL;
    tErrorSpec         *pErrorSpec = NULL;
    tRsvpHop           *pRsvpHop = NULL;
    tRsvpTeFilterSpec  *pRsvpTeFilterSpec = NULL;
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1NodeType;
    tRpteKey            RpteKey;
    BOOL1               bIsBkpTnl = RPTE_NO;
    UINT1               u1State = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_REH_ETEXT, " RpteRehProcessResvErrMsg : ENTRY \n");

    /* Check for mandatory objects */
    if ((RpteUtlResvManObjHandler (pPktMap, &pRsvpTeSession, &pRsvpHop,
                                   &pBFlowSpec,
                                   &pRsvpTeFilterSpec)) != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_REH_PRCS,
                    "RESVERR: Received message has mandatory objects "
                    "missing\n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_REH_ETEXT,
                    "RpteRehProcessResvErrMsg : INTMD-EXIT \n");
        return;
    }

    pStyle = &STYLE_OBJ (PKT_MAP_STYLE_OBJ (pPktMap));

    if (pPktMap->pErrorSpecObj != NULL)
    {
        pErrorSpec = &pPktMap->pErrorSpecObj->ErrorSpec;
    }
    else if (pPktMap->pIfIdRsvpErrObj != NULL)
    {
        pErrorSpec = &pPktMap->pIfIdRsvpErrObj->ErrorSpec;
    }
    else if (pPktMap->pIfIdRsvpNumErrObj != NULL)
    {
        pErrorSpec = &pPktMap->pIfIdRsvpNumErrObj->ErrorSpec;
    }
    else
    {
        return;
    }

    /* Check whether TnlInfo exists for the ResvErr mesg */
    if (RPTE_CHECK_TNL_RESV_ERR_MSG (pPktMap, pCtTnlInfo) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_REH_PRCS,
                    "RESVERR: Tnldoesn't exist for the rcvd Resv Err Mesg\n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_REH_ETEXT,
                    " RpteRehProcessResvErrMsg : INTMD-EXIT \n");
        return;
    }
    if ((pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE) &&
        (pCtTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_WORKING_PATH) &&
        (pCtTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse != LOCAL_PROT_NOT_AVAIL)
        && (pCtTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse !=
            LOCAL_PROT_NOT_APPLICABLE))
    {
        RSVPTE_DBG1 (RSVPTE_REH_ETEXT,
                     "RESV ERR should not sent further incase of Tunnel IN-USE case "
                     "with Tnl Id : %d\n",
                     OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID (pPktMap)));
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_REH_ETEXT, "RpteRehProcessResvErrMsg : EXIT \n");
        return;
    }
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) & RPTE_USTR_MSG_ID_PRESENT) &&
            ((INT4) RPTE_TNL_USTR_MAX_MSG_ID (pCtTnlInfo) >=
             (INT4) RPTE_PKT_MAP_MSG_ID (pPktMap)))
        {
            RSVPTE_DBG (RSVPTE_REH_PRCS,
                        "Msg ID Out Of Order. Sliently Dropping sshhh....\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_REH_ETEXT,
                        "RpteRehProcessResvErrMsg : INTMD-EXIT \n");
            return;
        }
        RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) |= RPTE_USTR_MSG_ID_PRESENT;
        RPTE_TNL_USTR_MAX_MSG_ID (pCtTnlInfo) = RPTE_PKT_MAP_MSG_ID (pPktMap);
    }

    RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;

    /* Scanning the local msg id data base */
    if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) &
         RPTE_OUT_RESV_MSG_ID_PRESENT) &&
        (RPTE_TNL_RSB_TIMER_TYPE (pCtTnlInfo) == RPTE_EXP_BACK_OFF_TIMER))
    {
        /* Treating the Resv Error as an implicit ack */
        pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
        pIfEntry = RSB_OUT_IF_ENTRY (pRsb);
        RPTE_TNL_RSB_TIMER_TYPE (pCtTnlInfo) = RPTE_NORMAL_TIMER;
        RPTE_TNL_RSB_REFRESH_INTERVAL (pCtTnlInfo) =
            RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                             DEFAULT_CONV_TO_SECONDS);
        RpteRhStartResvRefreshTmr (RSVPTE_TNL_RSB (pCtTnlInfo));
    }
    /* Since a resv error is sent by the downstream, the msg id
     * corresponding the previous resv message is deleted from the
     * local msg id data base. */
    if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) & RPTE_OUT_RESV_MSG_ID_PRESENT)
        == RPTE_OUT_RESV_MSG_ID_PRESENT)
    {
        RpteKey.u4_Key = RPTE_TNL_OUT_RESV_MSG_ID (pCtTnlInfo);
        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_LCL_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
    }

    pPsb = RSVPTE_TNL_PSB (pCtTnlInfo);
    /* Check whether Psb exists for the ResvTear mesg from which the PHop 
     * info is available.
     */
    if (pPsb == NULL)
    {
        RSVPTE_DBG (RSVPTE_REH_PRCS,
                    "RESVERR: Psb doesnot exist for the received message "
                    "- Resv Err Mesg Dropped \n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_REH_ETEXT,
                    " RpteRehProcessResvErrMsg : INTMD-EXIT \n");
        return;
    }
    pRsvpHop->u4HopAddr = OSIX_NTOHL (pRsvpHop->u4HopAddr);
    /* Check whether the ResvErr mesg is received from the proper PHop */
    if (RpteUtlCompareRsvpHop (pRsvpHop, &PSB_RSVP_HOP (pPsb)) != RPTE_SUCCESS)
    {
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_REH_ETEXT,
                    " RpteRehProcessResvErrMsg : INTMD-EXIT \n");
        return;
    }
    pRsvpHop->u4HopAddr = OSIX_HTONL (pRsvpHop->u4HopAddr);
    if (RSVPTE_TNL_RSB (pCtTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_REH_PRCS,
                    "RESVERR: Rsb doesnot exist for the received message "
                    "- Resv Err Mesg Dropped \n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_REH_ETEXT,
                    " RpteRehProcessResvErrMsg : INTMD-EXIT \n");
        return;
    }
    else
    {
        pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
    }

    /* Compare the Style of ResvErr mesg with that of the existing Style prsent
     * in Rsb */
    if ((RpteUtlCompareStyle (&RSB_STYLE (pRsb), pStyle)) != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_REH_PRCS,
                    "RESVERR: Incoming Style contradicts with prev Style - "
                    "Resv Err Mesg Dropped\n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_REH_ETEXT,
                    " RpteRehProcessResvErrMsg : INTMD-EXIT \n");
        return;
    }

    if (ERROR_SPEC_CODE (pErrorSpec) == UNKNOWN_OBJECT_CLASS)
    {
        if (OSIX_NTOHS (ERROR_SPEC_VALUE (pErrorSpec)) ==
            RPTE_MESSAGE_ID_CLASS_NUM_TYPE)
        {
            if (gu1MsgIdCapable == RPTE_ENABLED)
            {
                NBR_ENTRY_RR_STATE (RPTE_PKT_MAP_NBR_ENTRY (pPktMap)) =
                    RPTE_DISABLED;
                NBR_ENTRY_RMD_CAPABLE (RPTE_PKT_MAP_NBR_ENTRY (pPktMap)) =
                    RPTE_DISABLED;
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_PE_ETEXT,
                            "RpteRehProcessResvErrMsg : EXIT \n");
                return;
            }
        }
    }

    u1NodeType = RSVPTE_TNL_NODE_RELATION (pCtTnlInfo);
    /* If The node is Egress node, for ensuring the Tunnel gets deleted because
     * of Admisson control failure, Label Allocation failure, MlibUpdation
     * failue etc, the ResvErr is sent to the Ingress of the Tunnel. Since the
     * Ingress has the control of initiating the PathTear mesg to remove the
     * TnlInfo, the mesg is forwarded 
     */
    if (((ERROR_SPEC_CODE (pErrorSpec) == CONFLICTING_RESV_STYLE) ||
         (ERROR_SPEC_CODE (pErrorSpec) == UNKNOWN_OBJECT_CLASS) ||
         (ERROR_SPEC_CODE (pErrorSpec) == RPTE_ROUTE_PROB) ||
         (ERROR_SPEC_CODE (pErrorSpec) == POLICY_CONTROL_FAILURE) ||
         (ERROR_SPEC_CODE (pErrorSpec) == ADMISSION_CONTROL_FAILURE)) &&
        (u1NodeType == RPTE_EGRESS))
    {
        RSVPTE_DBG (RSVPTE_REH_PRCS,
                    "RESVERR: Incoming Resv Err Mesg has errors to be "
                    "forwarded - Path Err Sent\n");
        RptePvmSendResvPathErr (pPktMap, pErrorSpec->u1ErrCode,
                                pErrorSpec->u2ErrValue);
        RSVPTE_DBG (RSVPTE_REH_ETEXT,
                    " RpteRehProcessResvErrMsg" " : INTMD-EXIT \n");
        return;
    }
    /* If The node is Egress node, for ensuring the RRO gets deleted because
     * of RRO Notify form the next ResvMesg the ResvErr is sent to the Ingress
     * of the Tunnel. As per Rsvp-Lsp-Tunnel-05.txt, RRO should be included in
     * the ResvErr mesg, hence currently if any node sends RRO Notify ResvErr
     * without RRO it is not forwarded.
     */
    if (ERROR_SPEC_CODE (pErrorSpec) == RPTE_NOTIFY)
    {
        if (u1NodeType == RPTE_EGRESS)
        {
            RSVPTE_DBG (RSVPTE_REH_PRCS,
                        "RESVERR: Incoming Resv Err Mesg has RRO errors to "
                        "be forwarded - Path Err Sent\n");
            RptePvmSendResvPathErr (pPktMap, pErrorSpec->u1ErrCode,
                                    RPTE_RRO_NOTIFY);
            RSVPTE_DBG (RSVPTE_REH_ETEXT,
                        " RpteRehProcessResvErrMsg" " : INTMD-EXIT \n");
            return;
        }
    }

    /* Assumption is these Errors can occur only in the case of general RSVP
     * messages. Hence it can be dropped by the Egress of the Tunnel */
    if (u1NodeType == RPTE_EGRESS)
    {
        RSVPTE_DBG (RSVPTE_REH_PRCS,
                    "RESVERR: Incoming Resv Err Mesg has errors not to be "
                    "forwarded - Resv Err Mesg Dropped \n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_REH_ETEXT,
                    " RpteRehProcessResvErrMsg : INTMD-EXIT \n");
        return;
    }
    if (RPTE_IS_FRR_CAPABLE (pCtTnlInfo) == RPTE_TRUE)
    {
        u1State = RPTE_FRR_NODE_STATE (pCtTnlInfo);
        if ((RpteUtlIsUpStrMsgRcvdOnBkpPath (pPktMap, &pCtTnlInfo,
                                             &pRsvpTePrevInTnlInfo,
                                             &bIsBkpTnl)) == RPTE_FAILURE)
        {
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_REH_ETEXT,
                        " RpteRehProcessResvErrMsg : INTMD-EXIT \n");
            return;
        }
        /* The following condition occurs when a RESV ERR is received on 
         * backup path. */
        if (RPTE_IS_MP (u1State) && (bIsBkpTnl == RPTE_YES))
        {
            RptePvmSendResvPathErr (pPktMap, pErrorSpec->u1ErrCode,
                                    pErrorSpec->u2ErrValue);
            RpteUtlCleanPktMap (pPktMap);

            if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
            {
                RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
            }

            RSVPTE_DBG (RSVPTE_REH_ETEXT,
                        " RpteRehProcessResvErrMsg : INTMD-EXIT \n");
            return;
        }

        /* The following code is verified for DMP case */
        if (RPTE_IS_DMP (u1State))
        {
            RptePvmSendResvPathErr (pPktMap, pErrorSpec->u1ErrCode,
                                    pErrorSpec->u2ErrValue);
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_REH_ETEXT,
                        " RpteRehProcessResvErrMsg : INTMD-EXIT \n");

            if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
            {
                RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
            }
            return;
        }
    }
    /* ResvErr mesg is forwarded to the NextHop */
    PKT_MAP_OUT_IF_ENTRY (pPktMap) = RSB_OUT_IF_ENTRY (pRsb);
    PKT_MAP_DST_ADDR (pPktMap) = RSVP_HOP_ADDR (&RSB_NEXT_RSVP_HOP (pRsb));

    RPTE_PKT_MAP_NBR_ENTRY (pPktMap) = RPTE_TNL_DSTR_NBR (pCtTnlInfo);
    PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

    RptePvmConstructAndSndREMsg (pPktMap);
    RSVPTE_DBG (RSVPTE_REH_PRCS,
                "RESVERR: Incoming Resv Err Mesg has errors to be forwarded "
                "- Resv Err Mesg is forwarded\n");

    RpteUtlCleanPktMap (pPktMap);
    RSVPTE_DBG (RSVPTE_REH_ETEXT, " RpteRehProcessResvErrMsg : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptererr.c                             */
/*---------------------------------------------------------------------------*/
