/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptepath.c,v 1.102.2.1 2018/05/11 11:30:59 siva Exp $
 *
 * Description: This file contains routines to handle RSVP TE
 *              Tunnels path message processing.
 ********************************************************************/

#include "rpteincs.h"

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhProcessPathMsg
 * Description     : This function processes RSVPTE Path Message received.
 *                   It takes appropriate action based on the node relation of 
 *                   LSR. If node relation is intermediate, it forwards the 
 *                   PATH Message towards the egress.
 *                   If FRR is enabled, it also takes into consideration the 
 *                   direction of the PATH Message for further FRR processing.                   
 * Input (s)       : pPktMap - Pointer to PktMap
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhProcessPathMsg (tPktMap * pPktMap)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpBaseTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeBkpTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTmpTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTePrevInTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeMapTnlInfo = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo          TeTnlInfo;
    tNbrEntry          *pNbrEntry = NULL;
    tuTrieInfo         *pTrieInfo = NULL;
    tRpteKey            RpteKey;
    UINT4               u4OutGw = RPTE_ZERO;
    UINT4               u4TnlDestAddr = RSVPTE_ZERO;
    UINT1               u1NewTnl = RPTE_NO;
    UINT1               u1NewBkpTnl = RPTE_NO;
    UINT1               u1PathRefreshNeeded = RPTE_REFRESH_MSG_NO;
    UINT1               u1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;
    UINT1               u1RecoveyPathFlag = RPTE_NO;
    UINT4               u4RetVal = RPTE_ZERO;
    UINT4               u4TnlIndex = RPTE_ZERO;
    UINT4               u4TnlInstance = RPTE_ZERO;
    UINT4               u4SenderAddr = RPTE_ZERO;
    UINT4               u4DestAddr = RPTE_ZERO;
    UINT4               u4ExtTnlId = RPTE_ZERO;
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1IsCspfReqd = RPTE_FALSE;
    UINT1               u1ErrorCode = RPTE_ZERO;
    UINT2               u2ErrorValue = RPTE_ZERO;
    BOOL1               bIsBkpTnl = RPTE_NO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessPathMsg : ENTRY \n");

    pNbrEntry = RPTE_PKT_MAP_NBR_ENTRY (pPktMap);
    MEMSET (&TeTnlInfo, RSVPTE_ZERO, sizeof (tTeTnlInfo));

    if (RpteValidatePathMsgObjs (pPktMap) != RPTE_SUCCESS)
    {
        RpteUtlCleanPktMap (pPktMap);    /* Clean pPktMap */
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Validate Path Msg Objs failed - Pkt dropped..\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessPathMsg : INTMD-EXIT \n");
        return;
    }

    u4TnlIndex = OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID (pPktMap));
    u4TnlInstance = OSIX_NTOHS (PKT_MAP_SNDR_TMP_OBJ_LSP_ID (pPktMap));
    CONVERT_TO_INTEGER ((PKT_MAP_RPTE_ST_TNL_SNDR_ADDR (pPktMap)),
                        u4SenderAddr);
    u4SenderAddr = OSIX_NTOHL (u4SenderAddr);
    CONVERT_TO_INTEGER ((PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap)), u4ExtTnlId);
    u4ExtTnlId = OSIX_NTOHL (u4ExtTnlId);
    CONVERT_TO_INTEGER ((PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap)), u4DestAddr);
    u4DestAddr = OSIX_NTOHL (u4DestAddr);

    if ((RPTE_CHECK_PATH_MSG_TNL (pPktMap, pRsvpTeTnlInfo)) == RPTE_FAILURE)
    {
        RSVPTE_DBG2 (RSVPTE_PH_PRCS,
                     "PATH : New Tnl entry created in Tnl Tbl: Tnl Id : "
                     "%d : Tnl Instance : %d\n", u4TnlIndex, u4TnlInstance);
        u1NewTnl = RPTE_YES;
        u1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
        /* 
         * Create Tnl Table - allocate memory and add this into the Tnl 
         * RB Tree. 
         */

        if (RpteCreateNewRsvpteTunnel (u4TnlIndex, u4TnlInstance, u4ExtTnlId,
                                       u4DestAddr, &TeTnlInfo, &pRsvpTeTnlInfo,
                                       RPTE_TRUE) == RPTE_FAILURE)
        {
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Create Tnl Tbl - failed - Pkt dropped..\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhProcessPathMsg : INTMD-EXIT \n");
            return;
        }
        /* FRR Handle */
        if (PKT_MAP_RE_ROUTE_FLAG (pPktMap) == RPTE_YES)
        {
            RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) = RPTE_TRUE;
            RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) = RPTE_FRR_PROTECTED_TNL;
            RPTE_PKT_MAP_PKT_PATH (pPktMap) = RPTE_FRR_PROTECTED_PATH;
        }
        else if ((FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap)).
                  u2Length != RPTE_ZERO) || (u4SenderAddr != u4ExtTnlId))
        {
            /* Path specific method or sender template specific method 
             * with/without DETOUR object */
            RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) = RPTE_TRUE;
            RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) = RPTE_FRR_BACKUP_TNL;
            RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTnlInfo) = RPTE_TRUE;

            RSVPTE_FRR_GBL_DETOUR_GRP_ID (gpRsvpTeGblInfo)++;
            RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTnlInfo)
                = RSVPTE_FRR_GBL_DETOUR_GRP_ID (gpRsvpTeGblInfo);
        }

        RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo) = pNbrEntry;
        if (NBR_ENTRY_IF_ENTRY (pNbrEntry) != NULL)
        {
            (RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS
             (NBR_ENTRY_IF_ENTRY (pNbrEntry)))++;
        }
        (NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry))++;
    }
    else if ((FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap)).u2Length !=
              RPTE_ZERO) || (u4SenderAddr != u4ExtTnlId))
    {
        /* Could be 
         * 1. PATH Refresh Message for Backup Tunnel,
         * 2. A New PATH Message on the backup Path with 3
         *    different approaches.
         *      1. Sender Template Approach with change of 
         *         sender address alone.
         *      2. Sender Template Approach with change of 
         *         sender address and DETOUR Object
         *      3. Path Specific Approach */
        if (RptePhProcessBkpPathMsg
            (pPktMap, pRsvpTeTnlInfo, &TeTnlInfo, &pRsvpTeBkpTnlInfo,
             &u1NewBkpTnl) == RPTE_FAILURE)
        {
            RpteUtlCleanPktMap (pPktMap);    /* Clean pPktMap */
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "PATH : PATH Message received on backup path is invalid"
                        " - Packet Dropped.\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhProcessPathMsg : INTMD-EXIT \n");
            return;
        }
        pRsvpTeTnlInfo = pRsvpTeBkpTnlInfo;
        if (u1NewBkpTnl == RPTE_YES)
        {
            RPTE_PKT_MAP_PKT_PATH (pPktMap) = RPTE_FRR_BACKUP_PATH;
            u1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
            RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)++;
            RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo) = pNbrEntry;
            if (NBR_ENTRY_IF_ENTRY (pNbrEntry) != NULL)
            {
                (RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS
                 (NBR_ENTRY_IF_ENTRY (pNbrEntry)))++;
            }
            (NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry))++;
        }
    }
    /* This condition is satisfied when the node received RecoveryPath message
     * first (downstream synchronized). In that situation, RSVP-TE tunnel info 
     * is created in Recovery  Path message processing flow itself. 
     * And PSB related info will be created when it receives path message 
     * with recovery label object. For that purpose, u1NewTnl variable is set here
     * 
     * As Per RFC 5063, using  RecoveryPath message alone, intermediate node cant
     * recover its state. Thatswhy, psb related info and all is built in 
     * Path message with recovery label object process*/
    else if ((pRsvpTeTnlInfo->pTeTnlInfo != NULL) &&
             (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
              TNL_GR_DOWNSTREAM_SYNCHRONIZED))
    {
        u1NewTnl = RPTE_YES;
        u1RecoveyPathFlag = RPTE_YES;
        u1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
        RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo) = pNbrEntry;
        if (NBR_ENTRY_IF_ENTRY (pNbrEntry) != NULL)
        {
            (RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS
             (NBR_ENTRY_IF_ENTRY (pNbrEntry)))++;
        }
        (NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry))++;
    }

    if (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL)
    {
        if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL)
        {
            if (PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD)
            {
                if (RPTE_IS_MP (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo))
                    && RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo) != NULL)
                {
                    pRsvpTeTmpTnlInfo = pRsvpTeTnlInfo;
                    if ((RpteUtlIsUpStrMsgRcvdOnBkpPath
                         (pPktMap, &pRsvpTeTmpTnlInfo, &pRsvpTePrevInTnlInfo,
                          &bIsBkpTnl)) == RPTE_SUCCESS)
                    {
                        if (bIsBkpTnl == RPTE_YES)
                        {
                            pRsvpTeTnlInfo->b1IsPathMsgRecvOnBkpPath =
                                RPTE_TRUE;
                        }
                        else
                        {
                            pRsvpTeTnlInfo->b1IsPathMsgRecvOnBkpPath =
                                RPTE_FALSE;
                        }
                        RSVPTE_DBG1 (RSVPTE_FRR_DBG,
                                     "Is Path Msg of Protected Tunnel is received on backup Path(%d) ?\n",
                                     pRsvpTeTnlInfo->b1IsPathMsgRecvOnBkpPath);
                    }
                }
            }
        }
    }

    if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL) &&
        (PKT_MAP_RE_ROUTE_FLAG (pPktMap) == RPTE_YES))
    {
        /* This may happen when PATH Message for protected tunnel is 
         * sent on backup path after protected interface is down. So,
         * do not delete the tunnel here. Only send path error. */
        RptePvmUtlSendPathErr (pPktMap, RPTE_ROUTE_PROB, RPTE_BAD_STRICT_NODE);

        if ((u1NewTnl == RPTE_YES) || (u1NewBkpTnl == RPTE_YES))
        {
            RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
        }

        RpteUtlCleanPktMap (pPktMap);
        return;
    }

    if ((u1NewTnl == RPTE_NO) && (u1NewBkpTnl == RPTE_NO))
    {
        RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) &=
            (UINT1) (~(RPTE_FRR_NODE_STATE_UPLOST));

        RSVPTE_DBG4 (RSVPTE_PH_PRCS,
                     "Old Path msg recvd upstream lost bit reset for Tnl "
                     "%d %d %x %x\n",
                     u4TnlIndex, u4TnlInstance, u4ExtTnlId, u4DestAddr);
    }

    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
             RPTE_USTR_MSG_ID_PRESENT) &&
            ((INT4) RPTE_TNL_USTR_MAX_MSG_ID (pRsvpTeTnlInfo) >=
             (INT4) RPTE_PKT_MAP_MSG_ID (pPktMap)) &&
            (RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo) == pNbrEntry))
        {
            if ((u1NewTnl == RPTE_YES) || (u1NewBkpTnl == RPTE_YES))
            {
                RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
            }

            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "Msg ID Out Of Order. Sliently Dropping sshhh....\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhProcessPathMsg : INTMD-EXIT \n");
            return;
        }

        /* Msg Id is present and the tunnel already exists.
         * This means that the incoming msg id differs from the
         * existing msg id or there was no previously existing
         * msg id for this tunnel. */
        if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
             RPTE_IN_PATH_MSG_ID_PRESENT) == RPTE_IN_PATH_MSG_ID_PRESENT)
        {
            RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (pNbrEntry);
            RpteKey.u8_Key[RPTE_ONE] = RPTE_TNL_IN_PATH_MSG_ID (pRsvpTeTnlInfo);
            RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                                 RPTE_NBR_MSG_ID_DB_PREFIX,
                                 RpteMIHReleaseTrieInfo);

            /* NOTE : Since Incoming path msg id is different, there is
             * a change in the path state. This change needs to be propogated
             * to the downstream. So a different msg id is generated for the
             * next outgoing path msg of the tunnel. */
            if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
                 RPTE_OUT_PATH_MSG_ID_PRESENT) == RPTE_OUT_PATH_MSG_ID_PRESENT)
            {
                RpteKey.u4_Key = RPTE_TNL_OUT_PATH_MSG_ID (pRsvpTeTnlInfo);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                /* After removing out path message id from trie, teTnlInfo
                 * pointer has to be removed from RBTree. RBTree removal has
                 * to be done only for out path message id, since only out
                 * path message id is used for SRefresh RecoveryPath message
                 * support
                 * */
                TeSigDelFromTeMsgIdBasedTable (pRsvpTeTnlInfo->pTeTnlInfo);
            }
        }
    }

    RPTE_PKT_MAP_TNL_INFO (pPktMap) = pRsvpTeTnlInfo;

    /* Updating the Neighbour Entry in case of facilty fast rerouting 
     * Node protection */
    if ((RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo) != NULL)
        && (RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo) != pNbrEntry)
        && (pRsvpTeTnlInfo->b1IsPathMsgRecvOnBkpPath != RPTE_TRUE))
    {
        (RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS (NBR_ENTRY_IF_ENTRY
                                            (RPTE_TNL_USTR_NBR
                                             (pRsvpTeTnlInfo))))--;
        (NBR_ENTRY_NBR_NUM_TNLS (RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo)))--;
        /* Upstream Neighbor is changed, so remove the tnl from old neighbor */
        TMO_DLL_Delete (&(RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo)->UpStrTnlList),
                        &(pRsvpTeTnlInfo->UpStrNbrTnl));
        RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo) = pNbrEntry;
        TMO_DLL_Add (&(RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo)->UpStrTnlList),
                     &(pRsvpTeTnlInfo->UpStrNbrTnl));
        if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL)
        {
            if (NBR_ENTRY_IF_ENTRY (pNbrEntry) != NULL)
            {
                PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                    NBR_ENTRY_IF_ENTRY (pNbrEntry);
                (RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS
                 (NBR_ENTRY_IF_ENTRY (pNbrEntry)))++;
            }
        }
        (NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry))++;

        /* Since Change of Upstream neighbour is to be notified to all
         * other routers in the path, a new message id should be generated.
         * So, if a message id exists it is removed*/

        if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
             RPTE_OUT_PATH_MSG_ID_PRESENT) == RPTE_OUT_PATH_MSG_ID_PRESENT)
        {
            RpteKey.u4_Key = RPTE_TNL_OUT_PATH_MSG_ID (pRsvpTeTnlInfo);
            RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                 &RpteKey,
                                 RPTE_LCL_MSG_ID_DB_PREFIX,
                                 RpteMIHReleaseTrieInfo);

            /* After removing out path message id from trie, teTnlInfo
             * pointer has to be removed from RBTree. RBTree removal has
             * to be done only for out path message id, since only out
             * path message id is used for SRefresh RecoveryPath message
             * support
             * */
            TeSigDelFromTeMsgIdBasedTable (pRsvpTeTnlInfo->pTeTnlInfo);
        }

        u1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
    }

    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (pNbrEntry);
        RpteKey.u8_Key[RPTE_ONE] = RPTE_PKT_MAP_MSG_ID (pPktMap);

        pTrieInfo = (tuTrieInfo *)
            RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TRIE_INFO_POOL_ID);

        if (pTrieInfo == NULL)
        {
            /* Since the memory allocation is a failure,
             * the msg id received in the path msg is not
             * stored in the neighbour msg id data base. */
            RSVPTE_DBG (RSVPTE_PH_MEM,
                        "Failed to allocate Trie Info "
                        "Not Storing the msg id received in path msg.\n");
            RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &=
                RPTE_IN_PATH_MSG_ID_NOT_PRESENT;
        }
        else
        {
            MEMSET (pTrieInfo, RSVPTE_ZERO, sizeof (tuTrieInfo));
            RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo) = RPTE_PATH_INCOMING;
            RPTE_TRIE_INFO_TNL_INFO (pTrieInfo) = pRsvpTeTnlInfo;

            /* RBTree Addtion is only required for out path message id for
             * SRefresh RecoveryPath message support.
             * Its not required for in Path message id
             * */

            if (RpteTrieAddEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                                  pTrieInfo) != RPTE_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_PH_MEM,
                            "Addition to Trie Failed. "
                            "Not Storing the msg id received in path msg.\n");
                MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                    (UINT1 *) pTrieInfo);
                RPTE_TRIE_INFO_TNL_INFO (pTrieInfo) = NULL;
                RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &=
                    RPTE_IN_PATH_MSG_ID_NOT_PRESENT;
            }
            else
            {
                /* Store the new message id into the tunnel info */
                RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) |=
                    (RPTE_IN_PATH_MSG_ID_PRESENT | RPTE_USTR_MSG_ID_PRESENT);
                RPTE_TNL_IN_PATH_MSG_ID (pRsvpTeTnlInfo) =
                    RPTE_PKT_MAP_MSG_ID (pPktMap);
                RPTE_TNL_USTR_MAX_MSG_ID (pRsvpTeTnlInfo) =
                    RPTE_PKT_MAP_MSG_ID (pPktMap);
            }
        }
    }

    PKT_MAP_TNL_ALLOC_FLAG (pPktMap) = RPTE_YES;
    if ((u1NewTnl == RPTE_YES) || (u1NewBkpTnl == RPTE_YES))
    {
        if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS)
            || (gpRsvpTeGblInfo->u1GrProgressState ==
                RPTE_GR_RESTART_IN_PROGRESS))
        {
            pRsvpTeTnlInfo->u1IsGrTnl = RpteGrRecoveryLabelProcedure
                (pPktMap, pRsvpTeTnlInfo);
        }
        /* TE Row Status & Admin Status are made ACTIVE */
        TeTnlInfo.u1TnlRowStatus = RPTE_ACTIVE;
        TeTnlInfo.u1TnlAdminStatus = RPTE_ADMIN_UP;
        if (pRsvpTeTnlInfo->u1IsGrTnl == RPTE_NO)
        {
            /* The Tunnel Entry Creation is made in the TE Module */
            if (rpteTeCreateNewTnl (&pTeTnlInfo, &TeTnlInfo) == RPTE_TE_FAILURE)
            {
                RptePvmUtlSendPathErr (pPktMap, ADMISSION_CONTROL_FAILURE,
                                       DEFAULT_ERROR_VALUE);
                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "PATH : TE resources un-available - Path Err sent\n");

                /*  Delete tunnel from the tunnel hash table   */
                if (RpteDelTnlFromTnlTable
                    (gpRsvpTeGblInfo, pRsvpTeTnlInfo, u4TnlIndex,
                     TeTnlInfo.u4TnlInstance, u4ExtTnlId, u4DestAddr,
                     RPTE_FALSE) != RPTE_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_UTL_PRCS,
                                "UTIL : Tunnel is not removed from the RbTree\n");
                }
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RptePhProcessPathMsg : INTMD-EXIT \n");
                return;
            }
            RPTE_TE_TNL (pRsvpTeTnlInfo) = pTeTnlInfo;
            pRsvpTeTnlInfo->pTeTnlInfo->b1IsResourceReserved = RPTE_FALSE;
        }

        /*Indices are declared in teTnlInfo. Hence RB Tree addtion is done
           once teTnlInfo is assigned  */

        if ((u1NewBkpTnl != RPTE_YES) && (u1RecoveyPathFlag != RPTE_YES))
        {
            if (RBTreeAdd (gpRsvpTeGblInfo->RpteTnlTree,
                           (tRBElem *) pRsvpTeTnlInfo) != RB_SUCCESS)
            {
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RptePhProcessPathMsg : Failed to Add pRsvpTeTnlInfo to RpteTnlTree \n");

                RpteUtlCleanPktMap (pPktMap);
                return;
            }
        }

        if ((u1NewTnl == RPTE_YES) &&
            (PKT_MAP_RE_ROUTE_FLAG (pPktMap) == RPTE_YES) &&
            ((PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap) == NULL) ||
             ((FRR_FAST_REROUTE (PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap))).
              u1Flags == TE_TNL_FRR_ONE2ONE_METHOD)))
        {
            RSVPTE_TE_TNL_FRR_PROT_METHOD (RPTE_TE_TNL (pRsvpTeTnlInfo)) =
                TE_TNL_FRR_ONE2ONE_METHOD;
        }
        else if ((u1NewTnl == RPTE_YES) &&
                 (PKT_MAP_RE_ROUTE_FLAG (pPktMap) == RPTE_YES) &&
                 (PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap) != NULL))
        {
            RSVPTE_TE_TNL_FRR_PROT_METHOD (RPTE_TE_TNL (pRsvpTeTnlInfo)) =
                TE_TNL_FRR_FACILITY_METHOD;
        }

        if ((u1NewTnl == RPTE_YES)
            &&
            ((FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap)).u2Length !=
              RPTE_ZERO) || (u4SenderAddr != u4ExtTnlId)))
        {
            RSVPTE_TE_TNL_FRR_DETOUR_MERGING
                (RPTE_TE_TNL (pRsvpTeTnlInfo)) = TE_TNL_FRR_DETOUR_MERGE_NONE;
        }
        if (u1NewBkpTnl == RPTE_YES)
        {
            /* Just for Show Purpose */
            if (RPTE_IS_MP (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL
                                                 (pRsvpTeTnlInfo))))
            {
                RSVPTE_TE_TNL_FRR_DETOUR_MERGING
                    (RPTE_TE_TNL (pRsvpTeTnlInfo))
                    = TE_TNL_FRR_DETOUR_MERGE_PROT;
            }
            else if (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL
                                                       (pRsvpTeTnlInfo))))
            {
                RSVPTE_TE_TNL_FRR_DETOUR_MERGING
                    (RPTE_TE_TNL (pRsvpTeTnlInfo)) =
                    TE_TNL_FRR_DETOUR_MERGE_DETOUR;
            }
        }
        /* Create Tnl Table PSB using pPktMap */
        if (RpteCreateTnlTblPsb (pRsvpTeTnlInfo, pPktMap) == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Update Tnl Tbl (PSB) Failed - pkt dropped\n");
            /*  Delete tunnel from the tunnel Rbtree   */
            RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhProcessPathMsg : INTMD-EXIT \n");
            return;
        }

        /* Create Tnl Table info using pPktMap */
        if (RpteCreateTnlTblInfo (pRsvpTeTnlInfo, pPktMap) == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Create Tnl Tbl (Copy into OutErHopList) "
                        "Failed - pkt dropped\n");
            /* The Traffic Parameters associated with the Tunnel are deleted,
             * The ErHopList's and ArHopList's are not deleted here since they
             * are removed while returning failure */
            RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhProcessPathMsg : INTMD-EXIT \n");
            return;
        }

        if (!TMO_DLL_Is_Node_In_List (&(pRsvpTeTnlInfo->UpStrNbrTnl)))
        {
            TMO_DLL_Init_Node (&(pRsvpTeTnlInfo->UpStrNbrTnl));
            TMO_DLL_Add (&(pNbrEntry->UpStrTnlList),
                         &(pRsvpTeTnlInfo->UpStrNbrTnl));

            RSVPTE_DBG5 (RSVPTE_PH_PRCS,
                         "Tunnel %d %d %x %x added in the Nbr %x List\n",
                         u4TnlIndex, u4TnlInstance, u4ExtTnlId, u4DestAddr,
                         NBR_ENTRY_ADDR (pNbrEntry));
        }
    }

    /* During the neighbor restart period, node may send path error message
       with error value - control channel degrade to its upstream node
       Also it may send path error message with error value - control channel
       active to its upstream node in recovery period
     */

    if ((pRsvpTeTnlInfo->pDStrNbr != NULL) &&
        (pRsvpTeTnlInfo->pTeTnlInfo != NULL) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
         TNL_GR_NOT_SYNCHRONIZED))
    {
        if (pRsvpTeTnlInfo->pDStrNbr->u1GrProgressState ==
            RPTE_GR_RESTART_IN_PROGRESS)
        {
            RptePvmUtlSendPathErr (pPktMap, RPTE_NOTIFY,
                                   RPTE_CONTROL_CHANNEL_DEGRADE);
        }
        else if (pRsvpTeTnlInfo->pDStrNbr->u1GrProgressState ==
                 RPTE_GR_RECOVERY_IN_PROGRESS)
        {
            RptePvmUtlSendPathErr (pPktMap, RPTE_NOTIFY,
                                   RPTE_CONTROL_CHANNEL_ACTIVE);
        }
    }

    /* if RRO Support is enabled, check for loop */
    if ((TMO_SLL_Count (&pPktMap->RrObj.RrHopList) != RSVPTE_ZERO))
    {
        u4RetVal = RpteIsLoopFound (pPktMap);
        if (u4RetVal == RPTE_TRUE)
        {
            /* Send Path Err - Route Loop found */
            if ((u1NewTnl == RPTE_YES) || (u1NewBkpTnl == RPTE_YES))
            {
                RptePvmUtlSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                       RPTE_RRO_IND_ROUTE_LOOP);
                /* Delete tunnel from the tunnel RbTree   */
                RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
            }
            else
            {
                RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                    RPTE_RRO_IND_ROUTE_LOOP);
            }
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Forwarding Loop Found - Path err sent \n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhProcessPathMsg : INTMD-EXIT \n");
            return;
        }
    }
    if ((pRsvpTeTnlInfo->pTeTnlInfo != NULL) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus == TNL_GR_NOT_SYNCHRONIZED)
        && (pNbrEntry->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS))
    {
        if (pPktMap->pSuggestedLblObj == NULL)
        {
            u1ErrorCode = RPTE_ROUTE_PROB;
            u2ErrorValue = RPTE_LBL_ALLOC_FAIL;
        }
        else if ((OSIX_NTOHL (pPktMap->pSuggestedLblObj->Label.u4GenLbl)) !=
                 pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl)
        {
            u1ErrorCode = RPTE_ROUTE_PROB;
            u2ErrorValue = RPTE_UNACTBL_LABEL_VALUE;
        }
        else
        {
            pPktMap->pIncIfEntry->IfStatsInfo.u4IfNumPathRcvdWithSuggestedLbl++;
        }

        if (u1ErrorCode != RPTE_ZERO)
        {
            RptePvmSendPathErr (pPktMap, u1ErrorCode, u2ErrorValue);
            RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
            RpteUtlCleanPktMap (pPktMap);
            return;
        }

        u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus = TNL_GR_FULLY_SYNCHRONIZED;
    }

    if ((pRsvpTeTnlInfo->pTeTnlInfo != NULL) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_WORKING_PATH) &&
        (pRsvpTeTnlInfo->pPsb != NULL) &&
        (pRsvpTeTnlInfo->pPsb->Association.u2AssocID != RPTE_ZERO))
    {
        if (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                    u4TnlIndex,
                                    pRsvpTeTnlInfo->pPsb->
                                    Association.u2AssocID,
                                    u4SenderAddr,
                                    u4DestAddr,
                                    &pRsvpTeMapTnlInfo) == RPTE_SUCCESS)
        {
            pRsvpTeTnlInfo->pMapTnlInfo = pRsvpTeMapTnlInfo;
            pRsvpTeMapTnlInfo->pMapTnlInfo = pRsvpTeTnlInfo;
            pRsvpTeMapTnlInfo->pTeTnlInfo->u4BkpTnlIndex = u4TnlIndex;
            pRsvpTeMapTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance;
            MEMCPY (pRsvpTeMapTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                    &RSVPTE_TNL_INGRESS_RTR_ID (pRsvpTeTnlInfo->pTeTnlInfo),
                    ROUTER_ID_LENGTH);
            MEMCPY (pRsvpTeMapTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                    &RSVPTE_TNL_EGRESS_RTR_ID (pRsvpTeTnlInfo->pTeTnlInfo),
                    ROUTER_ID_LENGTH);

            pRsvpTeTnlInfo->pTeTnlInfo->u4BkpTnlIndex
                = pRsvpTeMapTnlInfo->pTeTnlInfo->u4TnlIndex;

            pRsvpTeTnlInfo->pTeTnlInfo->u4BkpTnlInstance
                = pRsvpTeMapTnlInfo->pTeTnlInfo->u4TnlInstance;

            MEMCPY (pRsvpTeTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                    &RSVPTE_TNL_INGRESS_RTR_ID (pRsvpTeMapTnlInfo->pTeTnlInfo),
                    ROUTER_ID_LENGTH);
            MEMCPY (pRsvpTeTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                    &RSVPTE_TNL_EGRESS_RTR_ID (pRsvpTeMapTnlInfo->pTeTnlInfo),
                    ROUTER_ID_LENGTH);
        }
    }

    /* 
     * Update Tnl Table info using pPktMap, Compare the Tnl Table info 
     * with the pPktMap, if necessary set Refresh flags.
     */
    if (RpteUpdateTnlTblInfo (pRsvpTeTnlInfo, pPktMap,
                              &u1PathRefreshNeeded,
                              &u1ResvRefreshNeeded) == RPTE_FAILURE)
    {
        /* 
         * Update Tnl Info Tbl Failed, generate Log msg, dropping the pkt.
         */
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Update Tnl Info Failed, droping the pkt..!\n");
        if (u1NewTnl == RPTE_YES)
        {
            RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
        }
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessPathMsg : INTMD-EXIT \n");
        return;
    }

    if ((RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo)
         != RPTE_INGRESS) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType !=
         MPLS_TE_DEDICATED_ONE2ONE))
    {
        rpteTeUpdateReoptimizeStatus (pRsvpTeTnlInfo->pTeTnlInfo);
    }

    if ((u1NewTnl == RPTE_YES) || (u1NewBkpTnl == RPTE_YES))
    {
        if (RpteCopyDiffServObjsToTnlInfo (pRsvpTeTnlInfo, pPktMap) ==
            RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Copying of DiffServ Objs to Tnl Failed\n");
            RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhProcessPathMsg : INTMD-EXIT \n");
            return;
        }
        if ((pRsvpTeTnlInfo->u1IsGrTnl == RPTE_NO) &&
            (rpteTeUpdateTeTnl (RPTE_TE_TNL (pRsvpTeTnlInfo)) ==
             RPTE_TE_FAILURE))
        {
            RptePvmUtlSendPathErr (pPktMap, ADMISSION_CONTROL_FAILURE,
                                   DEFAULT_ERROR_VALUE);
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Updation of TE Tnl Info is Failed\n");
            RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhProcessPathMsg : INTMD-EXIT \n");
            return;
        }
        /* For GR Tunnel, Hop info is not required for finding the next hop,
         * since if entry is retrieved from the out-index in recovery=procedure
         * */
        if (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) == NULL)
        {
            /* 
             * If the destination address in the RSVP-TE Path message is 
             * equal to one of the local addresses, then we are the egress of
             * the tunnel and we have to send the Resv message. 
             */
            CONVERT_TO_INTEGER ((PKT_MAP_SSN_EGRESS_ADDR (pPktMap)),
                                u4TnlDestAddr);
            u4TnlDestAddr = OSIX_NTOHL (u4TnlDestAddr);
            RSVPTE_DBG (RSVPTE_PH_IF,
                        " PH : IF RPTE_IS_DEST_OUR_ADDR: CALLED \n ");
            if (RPTE_IS_DEST_OUR_ADDR (u4TnlDestAddr) == RPTE_SUCCESS)
            {
                RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) = RPTE_EGRESS;
                /* 
                 * If the destination address in the RSVP-TE Path message is 
                 * equal to one of the local addresses, then we are the egress
                 * of the tunnel and we have to send the Resv message. 
                 * Call Egress processing routine to trigger reservation 
                 * at Egress 
                 */
                RPTE_PKT_MAP_TNL_INFO (pPktMap) = pRsvpTeTnlInfo;
                if (RpteEgrsPrcs (pPktMap) == RPTE_FAILURE)
                {
                    RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
                    RpteUtlCleanPktMap (pPktMap);
                    return;
                }

                RpteDeleteTrfcParamsAtEgress (pRsvpTeTnlInfo);
            }
            else
            {

                RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) = RPTE_INTERMEDIATE;
                /* Check for the Reachability. */
                if (RpteIpUcastRouteQuery (u4TnlDestAddr, &pIfEntry, &u4OutGw)
                    != RPTE_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_PH_PRCS,
                                "PATH : Route Query Failed - Send Path Err\n");
                    RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                        RPTE_NO_ROUTE_AVAIL);
                    RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
                    RpteUtlCleanPktMap (pPktMap);
                    RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                "RptePhProcessPathMsg : INTMD-EXIT \n");
                    return;
                }
                else            /* Reachability is there, call intermediate processing */
                {
                    RptePhCheckAndUpdateOobInfo (pRsvpTeTnlInfo, &pIfEntry,
                                                 RPTE_ZERO, RPTE_ZERO,
                                                 RPTE_ZERO, TRUE, NULL);
                    /*  Updatation of Diffserv Objects Related Informat-
                     *  ion in TunnelInfo is Done by, Copying the Informa-
                     *  tion Received in the Path Message In PacketMap to
                     *  TunnelInfo 
                     */

                    RpteIntermediatePrcs (u4OutGw, pIfEntry,
                                          pRsvpTeTnlInfo,
                                          pPktMap, u1PathRefreshNeeded,
                                          u1ResvRefreshNeeded, u1NewTnl,
                                          u1NewBkpTnl);

                    RpteUtlCleanPktMap (pPktMap);
                    RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                "RptePhProcessPathMsg : EXIT \n");
                    return;
                }
            }
        }
        else
        {                        /* ErHopList != 0 */
            /*
             * ErHop objs present in the Path msg, call the ErHop
             * Objs processing.
             */
            if (RptePrcsAndUpdateErHopList (pRsvpTeTnlInfo, pPktMap,
                                            u1PathRefreshNeeded,
                                            u1ResvRefreshNeeded, u1NewTnl,
                                            u1NewBkpTnl, &u1IsCspfReqd) ==
                RPTE_FAILURE)
            {
                RpteUtlCleanPktMap (pPktMap);    /* Clean pPktMap */
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RptePhProcessPathMsg : INTMD-EXIT \n");
                return;
            }

            /* If the Next Er-Hop is loose hop, then CSPF computation 
             * should be done and return from the function */
            if (u1IsCspfReqd == RPTE_TRUE)
            {
                RpteUtlCleanPktMap (pPktMap);    /* Clean pPktMap */
                return;
            }
        }
    }                            /* if NewTnl */
    else if ((u1PathRefreshNeeded == RPTE_REFRESH_MSG_YES) &&
             (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole != RPTE_EGRESS))
    {
        if ((PSB_REMAINING_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) - RSVPTE_ONE)
            == RSVPTE_ZERO)
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : ZERO TTL for Outgoing msg - Send Path Err\n");
            RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB, RPTE_NO_ROUTE_AVAIL);
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhProcessPathMsg : INTMD-EXIT \n");
            RpteUtlCleanPktMap (pPktMap);
            return;
        }
        /* NOTE : Since PathRefreshNeeded flag is true, there is a change in
         * the path state. This change needs to be propogated to the
         * downstream. So a different msg id is generated for the next
         * outgoing path msg of the tunnel. */
        if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
             RPTE_OUT_PATH_MSG_ID_PRESENT) == RPTE_OUT_PATH_MSG_ID_PRESENT)
        {
            RpteKey.u4_Key = RPTE_TNL_OUT_PATH_MSG_ID (pRsvpTeTnlInfo);
            RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                 RPTE_LCL_MSG_ID_DB_PREFIX,
                                 RpteMIHReleaseTrieInfo);
            /* After removing out path message id from trie, teTnlInfo
             * pointer has to be removed from RBTree. RBTree removal has
             * to be done only for out path message id, since only out
             * path message id is used for SRefresh RecoveryPath message
             * support
             * */
            TeSigDelFromTeMsgIdBasedTable (pRsvpTeTnlInfo->pTeTnlInfo);
        }
        if (RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE)
        {
            if (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) != NULL)
            {
                pRsvpBaseTeTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);
            }
            else
            {
                pRsvpBaseTeTnlInfo = pRsvpTeTnlInfo;
            }
            /* It is not actually upstream lost but upstream parameters changed 
             * After the least ER-Hops to reach destination computation, resets the state*/
            if (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo)))
            {
                RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo) |=
                    RPTE_FRR_NODE_STATE_UPLOST;
            }
        }
        /* If the synchronization status is upstream synchronized, then only upstream node
         * helped the node to recover its state and down stream node has to help. Hence 
         * path message should not be sent out until it gets help from down stream node
         * */
        if ((pRsvpTeTnlInfo->u1IsGrTnl == RPTE_YES) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
             TNL_GR_UPSTREAM_SYNCHRONIZED))
        {
            RpteUtlCleanPktMap (pPktMap);
            return;
        }
        RptePhPathRefresh (pRsvpTeTnlInfo);
    }

    if (PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap) & RPTE_SSN_ATTR_PATH_REEVAL_REQ)
    {
        if (((RSVPTE_TE_TNL_REOPTIMIZE_STATUS
              (RPTE_TE_TNL (pRsvpTeTnlInfo))) == TE_REOPTIMIZE_ENABLE) &&
            (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo)
             == RPTE_MID_POINT_NODE))
        {
            RSVPTE_DBG (RSVPTE_REOPT_DBG, "RptePhProcessPathMsg : "
                        "Path Re-evaluation Request is received\n");
            if (RpteCspfCompForNormalTnl (pRsvpTeTnlInfo) == RPTE_FAILURE)
            {
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RptePhProcessPathMsg : Cspf Computation Request Failed\n");
                return;
            }
            RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) = RPTE_TRUE;
        }
    }

    /* If Admin status is changed, resv message has to be sent immediately at egress node */
    if (u1ResvRefreshNeeded == RPTE_REFRESH_MSG_YES)
    {
        RpteRhResvRefresh (pRsvpTeTnlInfo);
    }

    if (((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) != RPTE_TRUE) ||
         (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL)) &&
        (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS))
    {
        RpteRhPhStartMaxWaitTmr (pRsvpTeTnlInfo,
                                 PSB_TIME_TO_DIE (RSVPTE_TNL_PSB
                                                  (pRsvpTeTnlInfo)));
    }

    /* FRR Merge point (Refreshing the protected tnl's PSB) */
    if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL) &&
        (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) != NULL) &&
        (RPTE_FRR_TNL_INFO_TYPE (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo))
         == RPTE_FRR_PROTECTED_TNL))
    {
        RSVPTE_DBG (RSVPTE_FRR_DBG, "Backup path refreshing the "
                    "protected tnl at MP\n");
        RpteRhPhStartMaxWaitTmr (pRsvpTeTnlInfo,
                                 PSB_TIME_TO_DIE (RSVPTE_TNL_PSB
                                                  (pRsvpTeTnlInfo)));
        RpteRhPhStartMaxWaitTmr (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo),
                                 PSB_TIME_TO_DIE (RSVPTE_TNL_PSB
                                                  (pRsvpTeTnlInfo)));
    }

    RpteUtlCleanPktMap (pPktMap);
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessPathMsg : EXIT \n");
    return;
}

/*----------------------------------------------------------------------------*/
/*                                                                            
 * Function Name   : RptePhSetupTunnel                                        
 * Description     : This function triggers the PATH Message from the Ingress.
 *                   It creates the PSB for the tunnel from the RSVP Tunnel
 *                   information structure passed.
 * Input (s)       : pRsvpTeTnlInfo - pointer to Tnl Info struct.             
 * Output (s)      : None.                   
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE                            
 */
/*---------------------------------------------------------------------------*/
UINT1
RptePhSetupTunnel (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tRsvpHop            RsvpHop;
    tSenderTspecObj     SenderTspecObj;
    tRsvpTeTrfcParams   TeTrfcParms;
    tRsvpTeRSVPTrfcParams RsvpTeTrfcParms;
    tIfEntry           *pIfEntry = NULL;
    tPsb               *pPsb = NULL;
    UINT4               u4DestAddr = RSVPTE_ZERO;
    UINT4               u4OutGw = RSVPTE_ZERO;
    UINT4               u4UnnumIf = RSVPTE_ZERO;
    UINT4               u4NextHopAddr = RSVPTE_ZERO;
    UINT1               u1ErrCode = RPTE_ZERO;
    UINT2               u2ErrValue = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RptePhSetupTunnel : ENTRY \n");
    MEMSET (&SenderTspecObj, RSVPTE_ZERO, sizeof (tSenderTspecObj));
    MEMSET (&RsvpHop, RSVPTE_ZERO, sizeof (tRsvpHop));
    MEMSET (&RsvpTeTrfcParms, RSVPTE_ZERO, sizeof (tRsvpTeRSVPTrfcParams));
    MEMSET (&TeTrfcParms, RSVPTE_ZERO, sizeof (tRsvpTeTrfcParams));

    /* If FRR Support is Required for the tunnel, then Explcit Route
     * Hop Should Be there */
    if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
        (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) == NULL))
    {
        RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, u1ErrCode, u2ErrValue);

        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RptePhSetupTunnel : " "INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* Get memory for this PSB */
    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL)
    {
        pPsb = RpteUtlCreatePsb ();
        if (pPsb == NULL)
        {
            RSVPTE_TNL_PSB (pRsvpTeTnlInfo) = NULL;
            RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, u1ErrCode, u2ErrValue);

            RSVPTE_DBG (RSVPTE_MAIN_MISC, " Psb creation failed \n");
            RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                        " RptePhSetupTunnel : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        RSVPTE_TNL_PSB (pRsvpTeTnlInfo) = pPsb;
        pPsb->pRsvpTeTnlInfo = pRsvpTeTnlInfo;
    }

    RptePhGetNextHopInfo (pRsvpTeTnlInfo, &u4DestAddr, &u4UnnumIf,
                          &u4NextHopAddr);

    if (RpteIpUcastRouteQuery (u4DestAddr, &pIfEntry, &u4OutGw) == RPTE_FAILURE)
    {
        /* It is wiser to remove the Tunnel from the RSVP RbTree when
         *   1. Route do not exist to send the path message out.
         *   2. RSVP Interface to carry the path message is not active.
         *
         * 1 will be resolved when Route change event is received.
         * 2 will be resolved when RSVP interface becomes active. 
         *
         * When resolving 1 and 2, Tunnel Instance would be incremented. */
        RSVPTE_DBG5 (RSVPTE_PH_IF,
                     "Route not found for u4DestAddr: %u or RSVP Interface not active "
                     "for the Tunnel %d %d %x %x - Deleted from Rbtree Table\n",
                     u4DestAddr,
                     RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                     RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                     OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                     OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));

        u1ErrCode = RPTE_ROUTE_PROB;
        u2ErrValue = RPTE_NO_ROUTE_AVAIL;

        RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, u1ErrCode, u2ErrValue);

        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RptePhSetupTunnel : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RptePhCheckAndUpdateOobInfo (pRsvpTeTnlInfo, &pIfEntry, u4UnnumIf,
                                 u4DestAddr, u4NextHopAddr, TRUE, &u4OutGw);
    /* The next hop address to forward the packet is
       stored in the Tnl's NextHop Address to forward
       the path message downstream. */
    RSVPTE_TNL_DN_NHOP (pRsvpTeTnlInfo) = u4OutGw;

    MEMCPY ((UINT1 *) &(RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo)),
            (UINT1 *) &(RSVPTE_TNL_INGRESS_RTR_ID
                        (RPTE_TE_TNL (pRsvpTeTnlInfo))), RSVPTE_IPV4ADR_LEN);

    PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = pIfEntry;

    RSVPTE_DBG6 (RSVPTE_PH_PRCS,
                 "Tunnel %d %d %x %x with Dest addr: %x has DN STR NH %x\n",
                 RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                 RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                 OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                 OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                 u4DestAddr, RSVPTE_TNL_DN_NHOP (pRsvpTeTnlInfo));

    /* As per RFC 3209, Section 4.7.4, Resource Affinity procedure has to be
     * applied to validate a link
     */

    if (RptePhRsrcAffVerProc (pRsvpTeTnlInfo) != RPTE_SUCCESS)
    {
        u1ErrCode = RPTE_ROUTE_PROB;
        u2ErrValue = RPTE_NO_ROUTE_AVAIL;

        RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, u1ErrCode, u2ErrValue);
        return RPTE_FAILURE;
    }

    if (RptePvmValidateSwAndEncType (pIfEntry, pRsvpTeTnlInfo, TRUE)
        == RPTE_FAILURE)
    {
        RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, u1ErrCode, u2ErrValue);
        return RPTE_FAILURE;
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
    {
        /* Copy Sender Template Obj */
        MEMCPY (&PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                TnlSndrAddr,
                &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                RSVPTE_IPV4ADR_LEN);

        PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).u2LspId =
            (UINT2) (RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo));
    }

    /* Copying the constraint informations from protected tunnel to backup tunnel.
     * Will be useful in the intermediate nodes. */
    if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL)
        && (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) != NULL))
    {
        /* Path specific method
         * PLR id - outgoing interface address
         * Avoid Node address - link protection - resv msg hop addr of protected tnl 
         * Avoid Node address - node protection - out ARHOP top hop address 
         * 
         * Sender template specific method
         * Sender template address is the outgoing interface address 
         *
         *Check whether avoid node is crossed in the packet traversal, if so drop the packet.
         * 
         * */
        /* If both the protected and backup tunnel travel through the 
         * same path then ignore it */

        if (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo)->pPsb->pOutIfEntry ==
            pRsvpTeTnlInfo->pPsb->pOutIfEntry)
        {
            RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, u1ErrCode, u2ErrValue);
            RSVPTE_DBG (RSVPTE_PH_IF,
                        "Both Prot & bkp tnl pass thru same path - ignored\n");
            return RPTE_FAILURE;
        }

        if (RptePhConstructBkpPathConsts (&pRsvpTeTnlInfo) == RPTE_FAILURE)
        {
            RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, u1ErrCode, u2ErrValue);

            return RPTE_FAILURE;
        }
        pRsvpTeTnlInfo->u1IsBkpPathMsgToCfa = RPTE_TRUE;
        PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).u2LspId =
            (UINT2) RSVPTE_TNL_TNLINST (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo));
        MEMCPY (&RSVPTE_TNL_NAME (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                &RSVPTE_TNL_NAME (RPTE_TE_TNL
                                  (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo))),
                STRLEN ((INT1 *)
                        RSVPTE_TNL_NAME (RPTE_TE_TNL
                                         (RPTE_FRR_BASE_TNL
                                          (pRsvpTeTnlInfo)))));
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
    {
        /* Update PSB TTL value using IfEntry */
        PSB_REMAINING_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
            IF_ENTRY_TTL (pIfEntry);
        pRsvpTeTnlInfo->pPsb->RsvpTeSndrTmp.u2Rsvd = RSVPTE_ZERO;

        /* Copy Int-serv Sender TSpec Obj */
        OBJ_HDR_LENGTH (&SenderTspecObj.ObjHdr) =
            OSIX_HTONS (sizeof (tSenderTspecObj));
        OBJ_HDR_CLASS_NUM_TYPE (&SenderTspecObj.ObjHdr) =
            OSIX_HTONS (IPV4_SENDER_TSPEC_CLASS_NUM_TYPE);

        SenderTspecObj.SenderTspec.MsgHdr.u1Version = RSVPTE_ZERO;
        SenderTspecObj.SenderTspec.MsgHdr.u1Reserved = RSVPTE_ZERO;
        SenderTspecObj.SenderTspec.MsgHdr.u2HdrLen = TSPEC_MSGHDR_LEN;

        SenderTspecObj.SenderTspec.SrvHdr.u1SrvID = TSPEC_SRVHDR_SRV_ID;
        SenderTspecObj.SenderTspec.SrvHdr.u1Flag = RSVPTE_ZERO;
        SenderTspecObj.SenderTspec.SrvHdr.u2SrvLength = TSPEC_SRVHDR_LEN;

        SenderTspecObj.SenderTspec.ParamHdr.u1ParamNum = TSPEC_PARAMHDR_ID;
        SenderTspecObj.SenderTspec.ParamHdr.u1ParamFlags = TSPEC_PARAMHDR_FLG;
        SenderTspecObj.SenderTspec.ParamHdr.u2ParamLen = TSPEC_PARAMHDR_LEN;

        if (TE_RSVPTE_TRFC_PARAMS
            (RSVPTE_TNL_TRFC_PARAM (RPTE_TE_TNL (pRsvpTeTnlInfo))) != NULL)
        {
            SenderTspecObj.SenderTspec.TBParams.fBktRate =
                (FLT4) (RSVPTE_TNLRSRC_TKN_BKT_RATE (pRsvpTeTnlInfo));
            SenderTspecObj.SenderTspec.TBParams.fBktSize =
                (FLT4) (RSVPTE_TNLRSRC_TKN_BKT_SIZE (pRsvpTeTnlInfo));
            SenderTspecObj.SenderTspec.TBParams.fPeakRate =
                (FLT4) (RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfo));
            SenderTspecObj.SenderTspec.TBParams.u4MinSize =
                (RSVPTE_TNLRSRC_MIN_POLICED_UNIT (pRsvpTeTnlInfo));
            SenderTspecObj.SenderTspec.TBParams.u4MaxSize =
                (RSVPTE_TNLRSRC_MAX_PKT_SIZE (pRsvpTeTnlInfo));
        }

        MEMCPY (&pRsvpTeTnlInfo->pPsb->SenderTspecObj,
                &SenderTspecObj, sizeof (tSenderTspecObj));

        RpteTcGenerateAdSpec (pIfEntry, &(PSB_ADSPEC (RSVPTE_TNL_PSB
                                                      (pRsvpTeTnlInfo))));

        /* Copy RSVP Hop Obj */
        RSVP_HOP_ADDR (&RsvpHop) = IF_ENTRY_ADDR (pIfEntry);

        RpteUtlEncodeLih (&RSVP_HOP_LIH (&RsvpHop),
                          PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));

        MEMCPY (&PSB_RSVP_HOP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                &RsvpHop, sizeof (tRsvpHop));

        OsixGetSysTime ((tOsixSysTime *) &
                        (PSB_LAST_CHANGE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))));
    }
    else
    {
        RpteTcCalcAdSpec (pIfEntry,
                          &(PSB_ADSPEC (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))),
                          &(pRsvpTeTnlInfo->pPsb->SenderTspecObj.SenderTspec),
                          &(PSB_ADSPEC (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))));
    }

    if (RpteValidateDSObjs (pRsvpTeTnlInfo) == RPTE_FAILURE)
    {
        RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, u1ErrCode, u2ErrValue);

        RSVPTE_DBG (RSVPTE_MAIN_MISC, "Validate DiffServ Objs Failed \n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RptePhSetupTunnel :  INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    TE_TNL_SIG_STATE (RPTE_TE_TNL (pRsvpTeTnlInfo)) = TE_SIG_DONE;

    RpteUtlCalculateEROSize (pRsvpTeTnlInfo);

    if (RptePhUpStrResvBwAndLblProg (pRsvpTeTnlInfo, &u1ErrCode, &u2ErrValue)
        == RPTE_FAILURE)
    {
        RptePhActOnPathSetupFailure (pRsvpTeTnlInfo, u1ErrCode, u2ErrValue);

        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RptePhSetupTunnel :  INTMD-EXIT \n");

        return RPTE_FAILURE;
    }

    if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_PROTECTION_PATH)
        && (pRsvpTeTnlInfo->pMapTnlInfo != NULL))
    {
        pRsvpTeTnlInfo->pPsb->Association.u2AssocID =
            (UINT2) pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
            u4TnlPrimaryInstance;
    }

    /* Call Path Refresh to construct and send path msg from Ingress */
    RptePhPathRefresh (pRsvpTeTnlInfo);

    /* Start Refresh Tmr at the Ingress */
    RptePhStartPathRefreshTmr (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));

    /* No ofTunnels Associated with this interface is incremented */
    RPTE_IF_NUM_TNL_ASSOC (pIfEntry)++;

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RptePhSetupTunnel : EXIT \n");

    return RPTE_SUCCESS;
}

/* --------------------------------------------------------------------------
 * Function Name   : RpteFacilityStripErHopsTillMP
 * Description     : This function strips the list of ER-HOPS arrived in PATH
 *                   Message from the PLR till the MP. This function is 
 *                   utilised by facility backup method.
 * Input (s)       : ppRsvpTeTnlInfo - Pointer to RSVPTE Tunnel Information
 * Output (s)      : ppRsvpTeTnlInfo - Pointer to RSVPTE Tunnel Information
 * Returns         : None
 * -------------------------------------------------------------------------  */
VOID
RpteFacilityStripErHopsTillMP (tRsvpTeTnlInfo ** ppRsvpTeTnlInfo)
{
    tTMO_SLL           *pErHopList = NULL;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = *ppRsvpTeTnlInfo;
    UINT4               u4ErHopAddr = 0;
    UINT4               u4MergePointAddr = 0;
    UINT1               u1ErHopCnt = 0;
    UINT1               u1Cnt = 0;
    BOOL1               bFound = RPTE_FALSE;

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteFacilityStripErHopsTillMP : ENTRY \n");

    if ((RpteUtlIsNodeProtDesired (pRsvpTeTnlInfo) == RPTE_YES) &&
        (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) != NULL))
    {
        u4MergePointAddr =
            OSIX_NTOHL (RSVPTE_TNL_EGRESS
                        (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo)));

        /* If protected tunnel has the complete ER-HOP information then incase of 
           NNHOP bypass tunnel intermediate hops information must be removed */
        pErHopList = &pRsvpTeTnlInfo->pFrrOutTePathInfo->ErHopList;
        pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_First (pErHopList);
        if (RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) == RPTE_TRUE)
        {

            u1ErHopCnt = 1;        /* In case of Facility backup node protection only one node will be skipped. */

        }
        else
        {
            while (pRsvpTeErHopInfo != NULL)
            {
                MEMCPY ((UINT1 *) (&(u4ErHopAddr)), &pRsvpTeErHopInfo->IpAddr,
                        RSVPTE_IPV4ADR_LEN);
                if (u4MergePointAddr == OSIX_NTOHL (u4ErHopAddr))
                {
                    /* we are replacing IP Address of MP */
                    bFound = RPTE_TRUE;
                    break;
                }
                pRsvpTeErHopInfo = (tRsvpTeHopInfo *)
                    TMO_SLL_Next (pErHopList, &(pRsvpTeErHopInfo->NextHop));
                u1ErHopCnt++;
            }
            if (bFound != RPTE_TRUE)
            {
                RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                            " RpteFacilityStripErHopsTillMP : "
                            "INTMD - EXIT \n");
                return;
            }
        }
        /* Preceding sub-objects to MP must be removed */
        pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_First (pErHopList);
        for (u1Cnt = 0; u1Cnt < u1ErHopCnt; u1Cnt++)
        {
            rpteTeDeleteTnlHopInfo (pRsvpTeTnlInfo->pFrrOutTePathListInfo,
                                    pRsvpTeTnlInfo->pFrrOutTePathInfo,
                                    pRsvpTeErHopInfo);
            /* Since unnumbered/label set support is not available for 
             * FRR, only 8 byte value is decreased from ERO size
             * */
            pRsvpTeTnlInfo->u2EroSize = (UINT2) (pRsvpTeTnlInfo->u2EroSize -
                                                 ERO_TYPE_IPV4_LEN);
            pRsvpTeErHopInfo = (tRsvpTeHopInfo *)
                TMO_SLL_Next (pErHopList, &(pRsvpTeErHopInfo->NextHop));
        }
    }
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteFacilityStripErHopsTillMP : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name : RpteChkLsrPresInAbsNodeIpv4
 * Description   : This routine is called when to check whether the LSR is
 *                 present in an abstract node, that is received as a ER Hop
 *                 in a Path message.
 * Input(s)      : pLsrId - Pointer to the value of the Lsr's LSR ID
 *                 (LSR ID is (UINT[4]) 4 byte IPv4 address).
 *                 pRsvpTeErHopInfo  - Pointer to the Next hop information
 *                 received in the label request message.
 * Output(s)     : None
 * Return(s)     : RPTE_SUCCESS in case the LSR ID is present in the abstract
 *                 node RPTE_FAILURE otherwise.
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteChkLsrPresInAbsNodeIpv4 (const UINT1 *pLsrId,
                             tRsvpTeHopInfo * pRsvpTeErHopInfo)
{
    UINT4               u4LsrIpAddr;
    UINT4               u4AbsNodeIpAddr;
    UINT4               u4HashIndex;
    tIfEntry           *pIfEntry = NULL;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteChkLsrPresInAbsNodeIpv4 : ENTRY \n");

    MEMCPY ((UINT1 *) (&(u4LsrIpAddr)), pLsrId, RSVPTE_IPV4ADR_LEN);
    MEMCPY ((UINT1 *) (&(u4AbsNodeIpAddr)),
            (UINT1 *) (&(RSVPTE_ERHOP_IPV4_ADDR (pRsvpTeErHopInfo))),
            RSVPTE_IPV4ADR_LEN);
    /* 
     * ErHopInfo contains Host order Addr, convert LsrIpAddr from
     * n/w to host order before comparing.
     */
    u4LsrIpAddr = ((OSIX_NTOHL (u4LsrIpAddr)) &
                   gaRsvpTeIpv4AddrMask[(pRsvpTeErHopInfo->u1AddrPrefixLen)]);

    u4AbsNodeIpAddr = u4AbsNodeIpAddr &
        gaRsvpTeIpv4AddrMask[(pRsvpTeErHopInfo->u1AddrPrefixLen)];

    if (u4LsrIpAddr == u4AbsNodeIpAddr)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteChkLsrPresInAbsNodeIpv4 : EXIT \n");
        return RPTE_SUCCESS;
    }

    /* 
     * If the ErHop doesn't match with the LSR/LER Id, we check whether
     * one of the interface adderess matches with the ErHop Address.
     */
    TMO_HASH_Scan_Table (RSVP_GBL_IF_HSH_TBL, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (RSVP_GBL_IF_HSH_TBL, u4HashIndex, pIfEntry,
                              tIfEntry *)
        {
            if (IF_ENTRY_STATUS (pIfEntry) == ACTIVE)
            {
                /* 
                 * ErHopInfo contains Host order Addr, convert LsrIpAddr from
                 * n/w to host order before comparing.
                 */
                u4LsrIpAddr =
                    (OSIX_NTOHL
                     (IF_ENTRY_ADDR (pIfEntry)) &
                     gaRsvpTeIpv4AddrMask[(pRsvpTeErHopInfo->u1AddrPrefixLen)]);
                if (u4LsrIpAddr == u4AbsNodeIpAddr)
                {
                    RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                "RpteChkLsrPresInAbsNodeIpv4 : EXIT \n");
                    return RPTE_SUCCESS;
                }
            }
        }
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteChkLsrPresInAbsNodeIpv4 : EXIT \n");
    return RPTE_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhPathRefresh
 * Description     : This function sends a PathRefresh message on all the
 *                   outgoing interfaces for which the pPsb routes are 
 *                   specified or available. If EXPLICIT_ROUTE Object is 
 *                   configured PATH Refresh message is sent on the outgoing
 *                   interface corresponding to the gateway specified in ER
 *                   that is not part of this hop.
 * Input (s)       : pRsvpTeTnlInfo - Pointer to Tnl Info struture.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhPathRefresh (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tIfEntry           *pIfEntry = NULL;
    tObjHdr            *pObjHdr = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tTimeValuesObj     *pTimeValuesObjHdr = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTmpTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpBaseTeTnlInfo = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tAdSpecObj         *pAdSpecObjHdr = NULL;
    tAdSpecWithoutGsAndClsObj *pAdSpecWithoutGsAndClsObjHdr = NULL;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    tSenderTspec       *pSenderTspec = NULL;
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    tRsvpTePathListInfo *pOutPathListInfo = NULL;
    UINT2               u2PathSize = RSVPTE_ZERO;
    tPktMap            *pPktMap = NULL;
    tPktMap             RROPktMap;
    tRpteKey            RpteKey;
    tTMO_SLL           *pErHopList = NULL;
    UINT4               u4OutGw = RPTE_ZERO;
    UINT1              *pPdu = NULL;
    UINT1               au1TmpSsnName[LSP_TNLNAME_LEN] = { RSVPTE_ZERO };
    UINT4               u4TmpAddr = RSVPTE_ZERO;
    FLT4                f4TmpVar = RSVPTE_ZERO;
    UINT4               u4TmpVar = RSVPTE_ZERO;
    UINT2               u2Retval = RSVPTE_ZERO;
    UINT2               u2MsgSize = RSVPTE_ZERO;
    UINT2               u2OffSet = RSVPTE_ZERO;
    UINT2               u2RROSize = RSVPTE_ZERO;
    UINT2               u2ErHopCnt = RSVPTE_ZERO;
    UINT2               u2InErHopCnt = RSVPTE_ZERO;
    UINT2               u2SsnObjLen = RSVPTE_ZERO;
    UINT2               u2DetourSize = RSVPTE_ZERO;
    UINT2               u2DetourLen = RSVPTE_ZERO;
    UINT2               u2TmpVar = RSVPTE_ZERO;
    UINT1               u1TmpVar = RSVPTE_ZERO;
    UINT1               u1RroNeeded = RPTE_FALSE;
    UINT1               u1EroNeeded = RPTE_FALSE;
    UINT1               u1FrrNeeded = RPTE_FALSE;
    tRsvpTeSsnObj       SsnObj;
    tRsvpTeSndrTmpObj   RsvpTeSndrTmpObj;
    tRsvpHopObj         RsvpHopObj;
    tIfIdRsvpHopObj     IfIdRsvpHopObj;
    tIfIdRsvpNumHopObj  IfIdRsvpNumHopObj;
    tIfEntry           *pIfMapEntry = NULL;
    UINT4               u4PathOutMsgId = RSVPTE_ZERO;
    UINT4               u4Val = RSVPTE_ZERO;
    UINT4               u4DestAddr = RSVPTE_ZERO;
    tuTrieInfo         *pTrieInfo = NULL;
    UINT1               u1MsgIdCreated = RPTE_FALSE;
    UINT1               u1DetourNeeded = RPTE_FALSE;
    UINT1               u1Cnt = RSVPTE_ZERO;
    UINT1               u1DetourGrpId = RPTE_ZERO;

    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL)
    {
        return;
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS)
    {
        return;
    }
    if ((pPktMap = MemAllocMemBlk (RSVPTE_PKT_MAP_POOL_ID)) == NULL)
    {
        return;
    }
    if ((pIfMapEntry = MemAllocMemBlk (RSVPTE_IF_ENTRY_POOL_ID)) == NULL)
    {
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        return;
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhPathRefresh : ENTRY \n");

    if (pRsvpTeTnlInfo->b1IsPsbTimeToDieIncreasedForProtTunnel == RPTE_TRUE)
    {
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "Path Message is not sending to MP, "
                    "Path-Tear is hold for some time\n");
        return;
    }

    if ((RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo)
         == RPTE_INGRESS) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType !=
         MPLS_TE_DEDICATED_ONE2ONE))
    {
        rpteTeUpdateReoptimizeStatus (pRsvpTeTnlInfo->pTeTnlInfo);
    }

    pNbrEntry = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
    if (((pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo == NULL) &&
         (RSVPTE_TNL_PATH_OPTION (pRsvpTeTnlInfo) != NULL)) &&
        /*In case of FRR-Facility backup No check required for DN STR NH */
        (RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) == RPTE_FALSE))
    {
        pErHopList = (&RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo));

        TMO_SLL_Scan (pErHopList, pRsvpTeErHopInfo, tRsvpTeHopInfo *)
        {
            if (pRsvpTeErHopInfo->u1LsrPartOfErHop != RPTE_TRUE)
            {
                break;
            }
        }

        if (pRsvpTeErHopInfo != NULL)
        {
            MEMCPY ((UINT1 *) &u4DestAddr,
                    (UINT1 *) (&(RSVPTE_ERHOP_IPV4_ADDR (pRsvpTeErHopInfo))),
                    RSVPTE_IPV4ADR_LEN);

            if (RpteIpUcastRouteQuery
                (OSIX_NTOHL (u4DestAddr), &pIfEntry, &u4OutGw) == RPTE_SUCCESS)
            {

                RSVPTE_DBG6 (RSVPTE_PH_PRCS,
                             "Tnl %d %d %x %x with Dest addr: %x "
                             "has OutGw %x\n",
                             RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                             RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                             OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                             OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                             OSIX_NTOHL (u4DestAddr), u4OutGw);

                RSVPTE_DBG6 (RSVPTE_PH_PRCS,
                             "Tnl %d %d %x %x with Dest addr: %x "
                             "has DN STR NH %x\n",
                             RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                             RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                             OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                             OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                             OSIX_NTOHL (u4DestAddr),
                             RSVPTE_TNL_DN_NHOP (pRsvpTeTnlInfo));
                if ((RSVPTE_TNL_DN_NHOP (pRsvpTeTnlInfo) != u4OutGw) &&
                    (pRsvpTeTnlInfo->b1DnStrOob != TRUE))
                {
                    MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID,
                                        (UINT1 *) pPktMap);
                    MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID,
                                        (UINT1 *) pIfMapEntry);
                    RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                "Path message not sent as DN STR NH is"
                                "diffent from u4OutGw: INTMD - EXIT\n");
                    return;
                }
            }
        }
    }
    if (pNbrEntry == NULL)
    {
        if ((gu1RRCapable == RPTE_ENABLED) ||
            ((gu1MsgIdCapable == RPTE_ENABLED) &&
             (gu4RMDFlags & RPTE_RMD_PATH_MSG)))
        {
            /* If Global Msg Id Capable flag is enabled or Global Msg ID
             * flag is enabled and RMD flag is set for Path Msg then
             * Msg ID is generated, provided message id is not generated yet.
             */
            if (!((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
                   RPTE_OUT_PATH_MSG_ID_PRESENT) ==
                  RPTE_OUT_PATH_MSG_ID_PRESENT))
            {
                RpteMIHGenerateMsgId (&u4PathOutMsgId);

                RPTE_TNL_OUT_PATH_MSG_ID (pRsvpTeTnlInfo) = u4PathOutMsgId;
                RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) |=
                    RPTE_OUT_PATH_MSG_ID_PRESENT;
                u1MsgIdCreated = RPTE_TRUE;
            }
        }
    }
    else if ((NBR_ENTRY_RR_STATE (pNbrEntry) == RPTE_DISABLED) &&
             (NBR_ENTRY_RMD_CAPABLE (pNbrEntry) == RPTE_DISABLED) &&
             (RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
              RPTE_OUT_PATH_MSG_ID_PRESENT))
    {
        /* When Nbr Entry is present and it is RMD not capable, msg id
         * associated with that tunnel is released */

        RpteKey.u4_Key = RPTE_TNL_OUT_PATH_MSG_ID (pRsvpTeTnlInfo);
        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_LCL_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);

        /* After removing out path message id from trie, teTnlInfo
         * pointer has to be removed from RBTree. RBTree removal has
         * to be done only for out path message id, since only out
         * path message id is used for SRefresh RecoveryPath message
         * support
         * */
        TeSigDelFromTeMsgIdBasedTable (pRsvpTeTnlInfo->pTeTnlInfo);
    }
    else if (((NBR_ENTRY_RR_STATE (pNbrEntry) == RPTE_ENABLED) ||
              ((NBR_ENTRY_RMD_CAPABLE (pNbrEntry) == RPTE_ENABLED) &&
               (gu4RMDFlags & RPTE_RMD_PATH_MSG))) &&
             (!(RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
                RPTE_OUT_PATH_MSG_ID_PRESENT)))
    {
        /* When Nbr Entry is present and it is RR and Msg Id capable
         * and if msg id is not associated with that message earlier,
         * then generate a message id and associate it with the tunnel.
         * And treat it as a trigger message */
        RpteMIHGenerateMsgId (&u4PathOutMsgId);
        RPTE_TNL_OUT_PATH_MSG_ID (pRsvpTeTnlInfo) = u4PathOutMsgId;
        RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) |= RPTE_OUT_PATH_MSG_ID_PRESENT;
        u1MsgIdCreated = RPTE_TRUE;
    }
    else if (NBR_ENTRY_RR_STATE (pNbrEntry) == RPTE_ENABLED)
    {
        if (RpteRRSendSRefreshMsg (pRsvpTeTnlInfo, PATH_MSG) == RPTE_SUCCESS)
        {
            MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
            MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
            return;
        }
    }

    MEMSET (&SsnObj, RSVPTE_ZERO, sizeof (tRsvpTeSsnObj));
    MEMSET (&RsvpHopObj, RSVPTE_ZERO, sizeof (tRsvpHopObj));
    MEMSET (&IfIdRsvpNumHopObj, RSVPTE_ZERO, sizeof (tIfIdRsvpNumHopObj));
    MEMSET (&IfIdRsvpHopObj, RSVPTE_ZERO, sizeof (tIfIdRsvpHopObj));
    MEMSET (pIfMapEntry, RSVPTE_ZERO, sizeof (tIfEntry));

    /* Initialise RROPktMap */
    RpteUtlInitPktMap (&RROPktMap);
    RPTE_PKT_MAP_TNL_INFO (&RROPktMap) = pRsvpTeTnlInfo;

    PKT_MAP_RSVP_HOP_OBJ (&RROPktMap) = &RsvpHopObj;
    RROPktMap.pIfIdRsvpHopObj = &IfIdRsvpHopObj;
    RROPktMap.pIfIdRsvpNumHopObj = &IfIdRsvpNumHopObj;
    PKT_MAP_IF_ENTRY (&RROPktMap) = pIfMapEntry;
    PKT_MAP_RPTE_SSN_OBJ (&RROPktMap) = &SsnObj;
    PKT_MAP_RPTE_SND_TMP_OBJ (&RROPktMap) = &RsvpTeSndrTmpObj;

    MEMSET (au1TmpSsnName, RSVPTE_ZERO, LSP_TNLNAME_LEN);

    /************************************************************************
      For FRR backup path message handling always expects base tnl information.
     ************************************************************************/

    if (RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE)
    {
        /* Sending the prot tnl control msgs on bypass tnl if 
           downstream interface and node down */
        if ((RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) == RPTE_TRUE) &&
            ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_IN_USE) ==
             LOCAL_PROT_IN_USE))
        {
            if (pRsvpTeTnlInfo->pFrrOutTePathListInfo == NULL)
            {
                /* It strips the ER-Hops till MP if protected has the complete HOP list */
                if (rpteTeCreateHopListInfo (&pOutPathListInfo,
                                             &RSVPTE_TNL_OUTERHOP_LIST
                                             (pRsvpTeTnlInfo)) !=
                    RPTE_TE_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_PH_PRCS, "PATH : Facility backup path "
                                "construction failed\n");
                }
                pRsvpTeTnlInfo->pFrrOutTePathListInfo = pOutPathListInfo;
                pRsvpTeTnlInfo->pFrrOutTePathInfo =
                    RSVPTE_TNL_FIRST_PATH_OPTION (pOutPathListInfo);
                RSVPTE_HOP_ROLE (pRsvpTeTnlInfo->pFrrOutTePathListInfo) =
                    RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo);

                RpteFacilityStripErHopsTillMP (&pRsvpTeTnlInfo);
                pRsvpTeTnlInfo->u2EroSize = RPTE_ZERO;
                RpteUtlCalculateEROSize (pRsvpTeTnlInfo);
            }
            if ((RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) != NULL) &&
                (RSVPTE_TNL_PSB (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo)) !=
                 NULL))
            {
                PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                    PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (RPTE_FRR_OUT_TNL_INFO
                                                      (pRsvpTeTnlInfo)));

                u4TmpAddr = OSIX_NTOHL (IF_ENTRY_ADDR
                                        (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB
                                                           (pRsvpTeTnlInfo))));
                MEMCPY (&PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                        TnlSndrAddr, &u4TmpAddr, RSVPTE_IPV4ADR_LEN);
            }
            pRsvpTeTnlInfo->u1SendToByPassTnl = RPTE_TRUE;
        }
        else
        {
            pRsvpTeTnlInfo->u1SendToByPassTnl = RPTE_FALSE;
        }

        /* Get the Group ID of the Detour. */
        u1DetourGrpId = RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTnlInfo);
        if (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) != NULL)
        {
            pRsvpBaseTeTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);
        }
        else
        {
            pRsvpBaseTeTnlInfo = pRsvpTeTnlInfo;
        }
        if (RPTE_IS_UPLOST (RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo)) &&
            RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo)) &&
            (RSVPTE_FRR_DETOUR_MERGING_CAPABLE (gpRsvpTeGblInfo) == RPTE_TRUE))
        {
            pRsvpTeTmpTnlInfo = pRsvpBaseTeTnlInfo;
            do
            {
                if (RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTmpTnlInfo) != u1DetourGrpId)
                {
                    continue;
                }
                if (RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTmpTnlInfo) ==
                    RPTE_TRUE)
                {
                    RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTmpTnlInfo) =
                        RPTE_FALSE;
                    break;
                }
            }
            while ((pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL_INFO
                    (pRsvpTeTmpTnlInfo)) != NULL);

            pRsvpTeTnlInfo = pRsvpTeTmpTnlInfo = pRsvpBaseTeTnlInfo;
            while (RPTE_FRR_IN_TNL (pRsvpTeTmpTnlInfo) != NULL)
            {
                if (RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTmpTnlInfo) != u1DetourGrpId)
                {
                    pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL (pRsvpTeTmpTnlInfo);
                    if (pRsvpTeTmpTnlInfo == NULL)
                    {
                        break;
                    }
                    continue;
                }
                u2ErHopCnt =
                    (UINT2)
                    TMO_SLL_Count (&RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo));
                u2InErHopCnt =
                    (UINT2)
                    TMO_SLL_Count (&RSVPTE_TNL_OUTERHOP_LIST
                                   (RPTE_FRR_IN_TNL (pRsvpTeTmpTnlInfo)));
                if (u2ErHopCnt < u2InErHopCnt)
                {
                    pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL (pRsvpTeTmpTnlInfo);
                    if (pRsvpTeTmpTnlInfo == NULL)
                    {
                        break;
                    }
                    continue;
                }
                pRsvpTeTnlInfo = pRsvpTeTmpTnlInfo;
                pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL (pRsvpTeTnlInfo);
                if (pRsvpTeTmpTnlInfo == NULL)
                {
                    break;
                }
            }
            RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTnlInfo) = RPTE_TRUE;
            RptePhStartPathRefreshTmr (pRsvpTeTnlInfo->pPsb);
            RSVPTE_DBG5 (RSVPTE_FRR_DBG,
                         "Tunnel %d %d %x %x state: %x\n choosen as downstream "
                         "tunnel to refresh the group of detours",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                         RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo));

            RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo) &=
                (UINT1) (~(RPTE_FRR_NODE_STATE_UPLOST));
        }
        if (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL)
        {
            if (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo)) &&
                (RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTnlInfo) !=
                 RPTE_TRUE))
            {
                RSVPTE_DBG5 (RSVPTE_FRR_DBG,
                             "Tunnel %d %d %x %x state: %x\n not choosen as downstream "
                             "tunnel to refresh the group of detours",
                             RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                             RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                             OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                             OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                             RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo));
                RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhPathRefresh : "
                            "INTMD - EXIT \n");
                MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
                MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID,
                                    (UINT1 *) pIfMapEntry);
                return;
            }
        }
    }
    /* Remaining TTL - PSB_REMAINING_TTL - pPsb */
    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS)
    {
        if ((PSB_REMAINING_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) - RSVPTE_ONE)
            == RSVPTE_ZERO)
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS, "PATH : ZERO TTL for Outgoing msg\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhPathRefresh : INTMD-EXIT \n");
            MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
            MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
            return;
        }
    }

    /* Initialize packet map */
    RpteUtlInitPktMap (pPktMap);
    RPTE_PKT_MAP_TNL_INFO (pPktMap) = pRsvpTeTnlInfo;

    pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));

    if ((pIfEntry == NULL) || (pIfEntry->u1Status != ACTIVE) ||
        (pIfEntry->u1Enabled != RPTE_ENABLED))
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS, "PATH : No Outgoing Interace or "
                    "Outgoing Interface is Not Active\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhPathRefresh : INTMD-EXIT \n");
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
        return;
    }

    /*This function is used to calculate the path message size */
    u2Retval = RpteUtlCalculatePathMsgSize (pRsvpTeTnlInfo, &u2SsnObjLen,
                                            &u2RROSize, &u1EroNeeded,
                                            &u1RroNeeded, PATH_MSG);
    u2PathSize = (UINT2) (u2PathSize + u2Retval);
    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
         RPTE_OUT_PATH_MSG_ID_PRESENT) == RPTE_OUT_PATH_MSG_ID_PRESENT)
    {
        if (u1MsgIdCreated == RPTE_TRUE)
        {
            RpteKey.u4_Key = u4PathOutMsgId;

            pTrieInfo = (tuTrieInfo *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TRIE_INFO_POOL_ID);

            if (pTrieInfo == NULL)
            {
                /* Since the memory allocation is a failure,
                 * the msg id received in the path msg is not
                 * stored in the neighbour msg id data base. */
                RSVPTE_DBG (RSVPTE_PH_MEM,
                            "Failed to allocate Trie Info "
                            "Not generating msg id for path msg.\n");
                RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &=
                    RPTE_OUT_PATH_MSG_ID_NOT_PRESENT;
            }
            else
            {
                MEMSET (pTrieInfo, RSVPTE_ZERO, sizeof (tuTrieInfo));
                RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo) = RPTE_PATH_REFRESH;
                RPTE_TRIE_INFO_TNL_INFO (pTrieInfo) = pRsvpTeTnlInfo;

                /* After Adding "out path message id" in the trie data structure
                 * sucessfully, tTeTnlInfo pointer has to be added in a RBTree based on
                 * the key - out path message id. This is used to recover the state
                 * when the node receives SRefresh RecoveryPath message with
                 * message id lists from its neigbor.
                 * RBTree addition is only required for out path message id
                 * */

                if (RpteTrieAddEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                      pTrieInfo) != RPTE_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_PH_MEM,
                                "Addition to Trie Failed. Not generating "
                                "msg id for path msg.\n");
                    MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                        (UINT1 *) pTrieInfo);
                    RPTE_TRIE_INFO_TNL_INFO (pTrieInfo) = NULL;
                    RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &=
                        RPTE_OUT_PATH_MSG_ID_NOT_PRESENT;
                }
                else
                {
                    RPTE_TNL_OUT_PATH_MSG_ID (pRsvpTeTnlInfo) = u4PathOutMsgId;
                    RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) |=
                        RPTE_OUT_PATH_MSG_ID_PRESENT;
                    TeSigAddToTeMsgIdBasedTable (pRsvpTeTnlInfo->pTeTnlInfo,
                                                 u4PathOutMsgId);
                }
            }
        }

        if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
             RPTE_OUT_PATH_MSG_ID_PRESENT) == RPTE_OUT_PATH_MSG_ID_PRESENT)
        {
            /* Msg Id is generated for the outgoing path message */
            u2PathSize = (UINT2) (u2PathSize + RPTE_MSG_ID_OBJ_LEN);
            RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;

            if ((gu4RMDFlags & RPTE_RMD_PATH_MSG) &&
                (RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) != RPTE_NORMAL_TIMER))
            {
                /* If the global RMD flag is set for Path Message Ack Desired
                 * bit is set */
                RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_MSG_ID_ACK_DESIRED;

                /* Since the message is a trigger message and the ack
                 * desired flag is set, BackOff Timer flag is set in
                 * Psb structure. */
                if (RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) ==
                    RPTE_TIMER_TYPE_UNKNOWN)
                {
                    RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) =
                        RPTE_EXP_BACK_OFF_TIMER;
                    RPTE_TNL_PSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                        (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                         RPTE_BACK_OFF_FACTOR) / DEFAULT_CONV_TO_SECONDS;
                }
                else            /* Timer Type is Exp Back Off */
                {
                    /* If the back off timer value has increased
                     * a certain threshold, the timer is converted
                     * to just a refresh timer and the refresh
                     * goes as a srefresh message */
                    if (RpteRRCalculateBackOffDelay
                        (pIfEntry, RPTE_TNL_PSB_REFRESH_INTERVAL
                         (pRsvpTeTnlInfo),
                         &(RPTE_TNL_PSB_REFRESH_INTERVAL (pRsvpTeTnlInfo)))
                        != RPTE_SUCCESS)
                    {
                        /* Setting the timer type to normal */
                        RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) =
                            RPTE_NORMAL_TIMER;
                        RPTE_TNL_PSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                            RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL
                                             (pIfEntry) /
                                             DEFAULT_CONV_TO_SECONDS);
                        /* Reset the Ack Desired Flag */
                        RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_ZERO;
                    }
                }
            }
            else
            {
                RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_ZERO;
                RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) = RPTE_NORMAL_TIMER;
                RPTE_TNL_PSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                    RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                                     DEFAULT_CONV_TO_SECONDS);
            }
            RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) = gu4Epoch;
            RPTE_PKT_MAP_MSG_ID (pPktMap) =
                RPTE_TNL_OUT_PATH_MSG_ID (pRsvpTeTnlInfo);
        }
        else
        {
            RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) = RPTE_NORMAL_TIMER;
            RPTE_TNL_PSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                                 DEFAULT_CONV_TO_SECONDS);
        }
    }
    else
    {
        if (RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) != RPTE_NORMAL_TIMER)
        {
            RPTE_TNL_PSB_TIMER_TYPE (pRsvpTeTnlInfo) = RPTE_NORMAL_TIMER;
            RPTE_TNL_PSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                                 DEFAULT_CONV_TO_SECONDS);
        }
    }
    RpteUpdateDSPathObjsSize (pRsvpTeTnlInfo, &u2PathSize);

    if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM)
    {
        u2OffSet = sizeof (tRsvpIpHdr) + SHIM_HDR_LEN;
    }
    else if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ETHERNET)
    {
        u2OffSet = sizeof (tRsvpIpHdr) + MAC_HEADER_LENGTH + SHIM_HDR_LEN;
    }

    /* Since, FRR Support is required and PATH Message is to be sent
     * on protected tunnel, sizeof FAST_REROUTE Object should be updated.
     * This is done here. */
    if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
        (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL))
    {
        if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS)
        {
            /* If the PATH Message sent on the protected tunnel is not from
             * Ingress, Intermediate LSR might or might not have received 
             * FAST_REROUTE Object in the PATH Message. So, Update the size 
             * accordingly. */
            pRsvpTeFastReroute =
                &(PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
            if (pRsvpTeFastReroute->u1Flags != RPTE_ZERO)
            {
                u2PathSize =
                    (UINT2) (u2PathSize + sizeof (tRsvpTeFastRerouteObj));
                u1FrrNeeded = RPTE_TRUE;
            }
        }
        else
        {
            /* Since the PATH Message sent out is from Ingress, we send
             * FAST_REROUTE Object in the PATH Message, if constraints are configured. 
             * So, size is updated. */
            if (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo) != NULL)
            {
                u2PathSize =
                    (UINT2) (u2PathSize + sizeof (tRsvpTeFastRerouteObj));
                u1FrrNeeded = RPTE_TRUE;
            }
        }
    }

    /* Combining all the detour objects to one detour object */
    if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
        (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL))
    {
        /* One2One path specific method will only contain 
         * the DETOUR object */
        pRsvpTeTmpTnlInfo = pRsvpTeTnlInfo;
        /* Get the Group ID of the Detour. */
        u1DetourGrpId = RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTnlInfo);
        if ((RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) != NULL) &&
            (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo))))
        {
            pRsvpTeTmpTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);

        }
        if ((PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTmpTnlInfo)).
             au4PlrId[RPTE_ZERO]) != RPTE_ZERO)
        {
            /* Find the no. of valid PLR objects present */
            for (u1Cnt = RPTE_ZERO; PSB_RPTE_FRR_DETOUR
                 (RSVPTE_TNL_PSB (pRsvpTeTmpTnlInfo)).au4PlrId[u1Cnt]
                 != RPTE_ZERO; u1Cnt++)
            {
                u2DetourSize = (UINT2) (u2DetourSize + (RSVPTE_IPV4ADR_LEN    /* PLR addr len */
                                                        + RSVPTE_IPV4ADR_LEN
                                                        /* Avoid Node addr len */
                                        ));
            }
            if (RSVPTE_FRR_DETOUR_MERGING_CAPABLE (gpRsvpTeGblInfo) ==
                RPTE_TRUE)
            {
                while ((pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL
                        (pRsvpTeTmpTnlInfo)) != NULL)
                {
                    if (u1DetourGrpId != RPTE_FRR_DETOUR_GRP_ID
                        (pRsvpTeTmpTnlInfo))
                    {
                        continue;
                    }
                    /* Find the no. of valid PLR objects present */
                    for (u1Cnt = RPTE_ZERO; PSB_RPTE_FRR_DETOUR
                         (RSVPTE_TNL_PSB (pRsvpTeTmpTnlInfo)).au4PlrId[u1Cnt]
                         != RPTE_ZERO; u1Cnt++)
                    {
                        u2DetourSize = (UINT2) (u2DetourSize + (RSVPTE_IPV4ADR_LEN    /* PLR addr len */
                                                                +
                                                                RSVPTE_IPV4ADR_LEN
                                                                /* Avoid Node addr len */
                                                ));
                    }
                }
            }
            u2DetourLen = (UINT2) (u2DetourSize + sizeof (tObjHdr));
            u2PathSize = (UINT2) (u2PathSize + u2DetourLen);
            u1DetourNeeded = (UINT1) RPTE_TRUE;
        }
    }

    /* Assuming by default Path Msg will be sent with Router Alert option */
    /*
     * NOTE: Currently RSVPTE assumes there won't be any IP options other
     * than the Router Alert present in the IpHdr while calculating Msg Size
     */
    /* Path Msg for E2E LSP should not have Router Alert Option
     * when it uses FA LSP 
     */
    if (pRsvpTeTnlInfo->u4FALspSrcOrDest == RPTE_ZERO)
    {
        u2MsgSize = (UINT2) (u2PathSize + sizeof (tRaOpt));
    }
    if (RpteUtlUdpEncapsRequired (pIfEntry) == RPTE_YES)
    {
        u2MsgSize =
            (UINT2) (((u2MsgSize + sizeof (tUdpHdr)) - sizeof (tRaOpt)));
    }

    /* Check for MTU size if doesn't fit send err message */
    if ((UINT2) (u2OffSet + u2MsgSize) > pIfEntry->RpteIfInfo.u4MaxMTUSize)
    {
        /* Set RRO Err Flag */
        pRsvpTeTnlInfo->u1RROErrFlag |= RPTE_RRO_ERR_PATH_SET;

        /* Construct PktMap and call RptePvmSendPathErr */
        if (pRsvpTeTnlInfo->u2BTime == RSVPTE_ZERO)
        {
            RpteUtlPhPreparePktMapRROErr (&RROPktMap, pRsvpTeTnlInfo);
            RptePhStartRROBackOffTimer (pRsvpTeTnlInfo);
            RptePvmSendPathErr (&RROPktMap, RPTE_NOTIFY, RPTE_RRO_TOO_LARGE);
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : RRO MTU size err - Path Err sent - "
                        "RRO Notification \n");
        }
    }
    /* Check if RRO Err has occured, allocate memory according to that and
     * send Path msg with out RRO to downstream.  */

    if ((pRsvpTeTnlInfo->u1RROErrFlag & RPTE_RRO_ERR_PATH_SET) ==
        RPTE_RRO_ERR_PATH_SET)
    {
        u2PathSize = (UINT2) (u2PathSize - u2RROSize);    /* subtracting RRO size */
        u2MsgSize = (UINT2) (u2MsgSize - u2RROSize);
        u1RroNeeded = RPTE_FALSE;
    }

    PKT_MAP_RSVP_PKT (pPktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);

    if (PKT_MAP_RSVP_PKT (pPktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Failed to allocate memory for the packet \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhPathRefresh : INTMD-EXIT \n");
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
        return;
    }

    MEMSET (PKT_MAP_RSVP_PKT (pPktMap), RSVPTE_ZERO, u2PathSize);
    PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    PKT_MAP_SRC_ADDR (pPktMap) =
        OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo));

    /* If the Path Msg is to be sent over FA TE Link, then it should
     * not have any RAO
     */
    if (pRsvpTeTnlInfo->u4FALspSrcOrDest == RPTE_ZERO)
    {
        /* Enable RA option needed */
        PKT_MAP_RA_OPT_NEEDED (pPktMap) = RPTE_YES;
        PKT_MAP_OPT_LEN (pPktMap) = sizeof (tRaOpt);
#ifndef LNXIP4_WANTED

        PKT_MAP_DST_ADDR (pPktMap) =
            OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));
#else
        PKT_MAP_DST_ADDR (pPktMap) = RSVPTE_TNL_DN_NHOP (pRsvpTeTnlInfo);
#endif
    }
    else
    {
        /* For E2E LSP at ingress of FA LSP, Destination of IP Header should be 
         * Other End of FA LSP */
        PKT_MAP_DST_ADDR (pPktMap) = pRsvpTeTnlInfo->u4FALspSrcOrDest;
    }

    PKT_MAP_OUT_IF_ENTRY (pPktMap) = pIfEntry;

    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        PKT_MAP_TX_TTL (pPktMap) =
            PSB_REMAINING_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    }
    else
    {
        PKT_MAP_TX_TTL (pPktMap) = (UINT1)
            (PSB_RX_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) - RSVPTE_ONE);
    }

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    MEMSET (pRsvpHdr, RSVPTE_ZERO, sizeof (tRsvpHdr));

    /* Rsvp Msg Header */
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = PATH_MSG;
    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        RSVP_HDR_SEND_TTL (pRsvpHdr) =
            PSB_REMAINING_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    }
    else
    {
        RSVP_HDR_SEND_TTL (pRsvpHdr) = (UINT1)
            ((PSB_REMAINING_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))) - 1);
    }
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (u2PathSize);

    pObjHdr =
        (tObjHdr *) (VOID *) (PKT_MAP_RSVP_PKT (pPktMap) + sizeof (tRsvpHdr));

    /* Msg Id Obj updation, if present */
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        /* Filling the Header */
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_MSG_ID_OBJ_LEN);
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_MESSAGE_ID_CLASS_NUM_TYPE);

        pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

        /* Filling the Epoch and Flags fields */
        u4Val = (UINT4) (RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) |
                         ((UINT4)
                          (RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) <<
                           RPTE_3_BYTE_SHIFT)));
        u4Val = (UINT4) (OSIX_HTONL (u4Val));
        MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4Val, RPTE_FLAG_EPOCH_LEN);

        pPdu += RPTE_FLAG_EPOCH_LEN;

        /* Filling the Message Identifier field */
        u4Val = OSIX_HTONL (RPTE_PKT_MAP_MSG_ID (pPktMap));
        MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4Val, RPTE_MSG_ID_LEN);
        pPdu += RPTE_MSG_ID_LEN;

        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* Ssn Obj updation */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    u4TmpAddr = RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo);

    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);    /* reserved field */
    RPTE_PUT_2_BYTES (pPdu,
                      (UINT2) OSIX_HTONS (RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo)));
    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo)), u4TmpAddr);
    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;

    pObjHdr = (tObjHdr *) (VOID *) pPdu;

    /* Rsvp Hop Obj updation */
    RptePvmFillRsvpHopObj (pRsvpTeTnlInfo, &pObjHdr, PATH_MSG);
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* Time values Obj updation */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tTimeValuesObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_TIME_VALUES_CLASS_NUM_TYPE);
    pTimeValuesObjHdr = (tTimeValuesObj *) (VOID *) pObjHdr;
    TIME_VALUES_PERIOD (&TIME_VALUES_OBJ (pTimeValuesObjHdr)) =
        OSIX_HTONL (IF_ENTRY_REFRESH_INTERVAL (pIfEntry));
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* ERO Obj IPv4 Type updation */
    if (u1EroNeeded == RPTE_TRUE)
    {
        RptePvmFillExplicitRouteObj (pRsvpTeTnlInfo,
                                     pRsvpTeTnlInfo->u2EroSize, &pObjHdr);
    }

    /* Generic Label Request Object updation */
    RptePvmFillGenericLabelReqObj (pRsvpTeTnlInfo, &pObjHdr, PATH_MSG);

    /* Label set obj updation */
    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1LabelSetEnabled == RPTE_ENABLED)
    {
        RptePvmFillLabelSetObj (pRsvpTeTnlInfo, &pObjHdr, PATH_MSG);
    }
    /* Ssn Attr Obj */

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (u2SsnObjLen);

    /* As per section 4.7 in RFC 3209, PATH message should be sent 
     * with all three affinity values, if atleast one of the affinity is 
     * set as non-zero.Otherwise, it should be sent without any affinity.
     */
    ((RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pRsvpTeTnlInfo) == RSVPTE_ZERO) &&
     (RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pRsvpTeTnlInfo) == RSVPTE_ZERO) &&
     (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo) == RSVPTE_ZERO)) ?
(OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
OSIX_HTONS (RPTE_SSN_ATTR_CLASS_NUM_TYPE)) : (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS (RPTE_RA_SSN_ATTR_CLASS_NUM_TYPE));

    pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    if ((RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pRsvpTeTnlInfo) != RSVPTE_ZERO) ||
        (RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pRsvpTeTnlInfo) != RSVPTE_ZERO) ||
        (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo) != RSVPTE_ZERO))
    {
        RPTE_PUT_4_BYTES (pPdu,
                          OSIX_HTONL (RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF
                                      (pRsvpTeTnlInfo)));
        RPTE_PUT_4_BYTES (pPdu,
                          OSIX_HTONL (RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF
                                      (pRsvpTeTnlInfo)));
        RPTE_PUT_4_BYTES (pPdu,
                          OSIX_HTONL (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF
                                      (pRsvpTeTnlInfo)));
    }
    RPTE_PUT_1_BYTE (pPdu, RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pRsvpTeTnlInfo));
    RPTE_PUT_1_BYTE (pPdu, RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pRsvpTeTnlInfo));

    /* As per RFC 4090 sec. 6.3 */
    if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
        (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL))
    {
        u1TmpVar = RSVPTE_ZERO;
        if ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS) &&
            (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo) != NULL))
        {
            if ((RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u1ProtType)
                == TE_TNL_FRR_PROT_NODE)
            {
                u1TmpVar |= RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG;
            }
            if (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u4Bandwidth)
            {
                u1TmpVar |= RPTE_FRR_SSN_ATTR_BANDWIDTH_FLAG;
            }
            if (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u1SEStyle == RPTE_TRUE)
            {
                u1TmpVar |= RPTE_FRR_SSN_ATTR_SE_STYLE_FLAG;
            }
            if ((RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u1ProtMethod) ==
                TE_TNL_FRR_FACILITY_METHOD)
            {
                u1TmpVar |= RPTE_FRR_SSN_ATTR_LBL_RECORD_FLAG;
            }
        }
        else if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS)
        {
            if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlProtType
                == TE_TNL_FRR_PROT_NODE)
            {
                u1TmpVar |= RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG;
            }

            if (pRsvpTeTnlInfo->pTeTnlInfo->bIsBwProtDesired == TRUE)
            {
                u1TmpVar |= RPTE_FRR_SSN_ATTR_BANDWIDTH_FLAG;
            }

            if (pRsvpTeTnlInfo->pTeTnlInfo->bIsSEStyleDesired == TRUE)
            {
                u1TmpVar |= RPTE_FRR_SSN_ATTR_SE_STYLE_FLAG;
            }

            if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1Attributes
                & RSVPTE_SSN_LBL_RECORD_BIT)
            {
                u1TmpVar |= RPTE_FRR_SSN_ATTR_LBL_RECORD_FLAG;
            }
        }
        u1TmpVar |= RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG;
    }
    else if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
             (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL))
    {
        /* Clearing all the flags with respect to Fast reroute except
         * SE Style Desired Flag. */
        if (pRsvpTeTnlInfo->pBaseFrrProtTnlInfo != NULL)
        {
            if (pRsvpTeTnlInfo->pBaseFrrProtTnlInfo->
                pTeTnlInfo->bIsSEStyleDesired == TRUE)
            {
                u1TmpVar |= RPTE_FRR_SSN_ATTR_SE_STYLE_FLAG;
            }
        }
        else
        {
            if (pRsvpTeTnlInfo->pTeTnlInfo->bIsSEStyleDesired == TRUE)
            {
                u1TmpVar |= RPTE_FRR_SSN_ATTR_SE_STYLE_FLAG;
            }
        }
    }
    else
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->bIsSEStyleDesired == TRUE)
        {
            u1TmpVar |= RPTE_FRR_SSN_ATTR_SE_STYLE_FLAG;
        }

        if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1Attributes
            & RSVPTE_SSN_LBL_RECORD_BIT)
        {
            u1TmpVar |= RPTE_FRR_SSN_ATTR_LBL_RECORD_FLAG;
        }

        if (RPTE_REOP_PATH_REEVALUATION_REQ (pRsvpTeTnlInfo) == TRUE)
        {
            u1TmpVar |= RPTE_SSN_ATTR_PATH_REEVAL_REQ;
        }
    }

    RPTE_PUT_1_BYTE (pPdu, u1TmpVar);
    RPTE_PUT_1_BYTE (pPdu, (UINT1) STRLEN ((INT1 *) RSVPTE_TNL_NAME
                                           (RPTE_TE_TNL (pRsvpTeTnlInfo))));

    /* 
     * Copy Ssn Name based on its length, if required add NULL Padded string
     * at the end.
     */
    if ((STRLEN ((INT1 *) RSVPTE_TNL_NAME
                 (RPTE_TE_TNL (pRsvpTeTnlInfo))) < SSN_NAME_MIN_LEN))
    {
        MEMCPY (&au1TmpSsnName, &RSVPTE_TNL_NAME
                (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                STRLEN ((INT1 *) RSVPTE_TNL_NAME
                        (RPTE_TE_TNL (pRsvpTeTnlInfo))));
        MEMCPY (pPdu, &au1TmpSsnName, LSP_TNLNAME_LEN);
    }
    else if ((STRLEN ((INT1 *) RSVPTE_TNL_NAME
                      (RPTE_TE_TNL (pRsvpTeTnlInfo))) % WORD_BNDRY) !=
             RSVPTE_ZERO)
    {
        MEMCPY (&au1TmpSsnName, &RSVPTE_TNL_NAME
                (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                STRLEN ((INT1 *) RSVPTE_TNL_NAME
                        (RPTE_TE_TNL (pRsvpTeTnlInfo))));

        MEMCPY (pPdu, &au1TmpSsnName, LSP_TNLNAME_LEN);
    }
    else
    {
        MEMCPY (pPdu, &RSVPTE_TNL_NAME (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                STRLEN ((INT1 *) RSVPTE_TNL_NAME
                        (RPTE_TE_TNL (pRsvpTeTnlInfo))));
    }
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* Adminstatus object updation */
    RptePvmFillAdminStatusObj (pRsvpTeTnlInfo, &pObjHdr, PATH_MSG);

    RptePvmFillNotifyReqObj (&pObjHdr, pRsvpTeTnlInfo, PATH_MSG);

    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE) ||
        (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_FULL_REROUTE))
    {
        RptePvmFillProtectionObj (&pObjHdr, pRsvpTeTnlInfo);
        RptePvmFillAssociationObj (&pObjHdr, pRsvpTeTnlInfo);
    }
    RpteUpdateDSPathObjs (pRsvpTeTnlInfo, &pObjHdr);

    pPdu = (UINT1 *) pObjHdr;

    /* Copy the Fast ReRoute Object */
    if (u1FrrNeeded == RPTE_TRUE)
    {
        RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (RPTE_IPV4_FRR_OBJ_LEN));
        RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (IPV4_RPTE_FRR_CLASS_NUM_TYPE));
        if ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS) &&
            (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo) != NULL))
        {
            /* Copy FAST_REROUTE obj */
            RPTE_PUT_1_BYTE (pPdu,
                             RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u1SetPrio);
            RPTE_PUT_1_BYTE (pPdu,
                             RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u1HoldPrio);
            RPTE_PUT_1_BYTE (pPdu,
                             RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u1HopLimit);
            RPTE_PUT_1_BYTE (pPdu,
                             RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->
                             u1ProtMethod);
            /* Bandwidth */
            f4TmpVar =
                (FLT4) RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u4Bandwidth;
            RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (f4TmpVar);
            f4TmpVar = OSIX_HTONF (f4TmpVar);
            MEMCPY (pPdu, &f4TmpVar, sizeof (FLT4));
            pPdu += sizeof (FLT4);

            u4TmpVar =
                OSIX_HTONL (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->
                            u4IncAnyAttr);
            MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpVar, RSVPTE_FOUR);
            pPdu += RSVPTE_FOUR;
            u4TmpVar =
                OSIX_HTONL (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->u4ExAnyAttr);
            MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpVar, RSVPTE_FOUR);
            pPdu += RSVPTE_FOUR;
            u4TmpVar =
                OSIX_HTONL (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo)->
                            u4IncAllAttr);
            MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpVar, RSVPTE_FOUR);
            pPdu += RSVPTE_FOUR;
        }
        else if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS)
        {
            pRsvpTeFastReroute =
                &(PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
            /* Copy FAST_REROUTE obj. The check u1FrrNeeded == RPTE_TRUE 
             * if succeeds in the intermediate nodes, 
             * means Fast ReRoute Object will always be there. */
            pRsvpTeFastReroute = (tRsvpTeFastReroute *) (VOID *) pPdu;
            MEMCPY (pRsvpTeFastReroute,
                    &(PSB_RPTE_FRR_FAST_REROUTE
                      (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))),
                    sizeof (tRsvpTeFastReroute));
            pPdu = (pPdu + sizeof (tRsvpTeFastReroute));
        }
    }
    if (u1DetourNeeded == RPTE_TRUE)
    {
        /* Copy DETOUR obj */
        RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (u2DetourLen));
        RPTE_PUT_2_BYTES (pPdu,
                          OSIX_HTONS (IPV4_RPTE_FRR_DETOUR_CLASS_NUM_TYPE));
        /* One2One path specific method will only contain 
         * the DETOUR object */
        pRsvpTeTmpTnlInfo = pRsvpTeTnlInfo;
        if ((RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) != NULL) &&
            (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo))))
        {
            pRsvpTeTmpTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);
        }
        for (u1Cnt = RPTE_ZERO; PSB_RPTE_FRR_DETOUR
             (RSVPTE_TNL_PSB
              (pRsvpTeTmpTnlInfo)).au4PlrId[u1Cnt] != RPTE_ZERO; u1Cnt++)
        {
            /* PLR id */
            u4TmpVar =
                OSIX_HTONL (PSB_RPTE_FRR_DETOUR
                            (RSVPTE_TNL_PSB (pRsvpTeTmpTnlInfo)).
                            au4PlrId[u1Cnt]);
            MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpVar, RSVPTE_FOUR);
            pPdu += RSVPTE_FOUR;
            /* Avoid Node id */
            u4TmpVar =
                OSIX_HTONL (PSB_RPTE_FRR_DETOUR
                            (RSVPTE_TNL_PSB (pRsvpTeTmpTnlInfo)).
                            au4AvoidNodeId[u1Cnt]);
            MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpVar, RSVPTE_FOUR);
            pPdu += RSVPTE_FOUR;
        }

        if (RSVPTE_FRR_DETOUR_MERGING_CAPABLE (gpRsvpTeGblInfo) == RPTE_TRUE)
        {
            while ((pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL
                    (pRsvpTeTmpTnlInfo)) != NULL)
            {
                if (u1DetourGrpId != RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTmpTnlInfo))
                {
                    continue;
                }
                for (u1Cnt = RPTE_ZERO; PSB_RPTE_FRR_DETOUR
                     (RSVPTE_TNL_PSB
                      (pRsvpTeTmpTnlInfo)).au4PlrId[u1Cnt] != RPTE_ZERO;
                     u1Cnt++)
                {
                    /* PLR id */
                    u4TmpVar =
                        OSIX_HTONL (PSB_RPTE_FRR_DETOUR
                                    (RSVPTE_TNL_PSB (pRsvpTeTmpTnlInfo)).
                                    au4PlrId[u1Cnt]);
                    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpVar, RSVPTE_FOUR);
                    pPdu += RSVPTE_FOUR;
                    /* Avoid Node id */
                    u4TmpVar =
                        OSIX_HTONL (PSB_RPTE_FRR_DETOUR
                                    (RSVPTE_TNL_PSB (pRsvpTeTmpTnlInfo)).
                                    au4AvoidNodeId[u1Cnt]);
                    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpVar, RSVPTE_FOUR);
                    pPdu += RSVPTE_FOUR;
                }
            }
        }
    }
    /* Copy Sndr Tmp obj */

    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (RPTE_IPV4_SNDR_TMP_OBJ_LEN));
    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE));

    CONVERT_TO_INTEGER (PSB_RPTE_SNDR_TMP
                        (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).TnlSndrAddr,
                        u4TmpAddr);
    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);
    pPdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);    /* reserved field */
    u2TmpVar =
        OSIX_HTONS (PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                    u2LspId);
    MEMCPY (pPdu, (UINT1 *) &u2TmpVar, sizeof (UINT2));
    pPdu += sizeof (UINT2);
    pObjHdr = (tObjHdr *) (VOID *) pPdu;

    /* Copy Sender Tspec */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tSenderTspecObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_SENDER_TSPEC_CLASS_NUM_TYPE);

    pPdu = ((UINT1 *) pObjHdr + sizeof (tObjHdr));

    /* In Ingress, PSB - SnderTspec is in Host order */
    if ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS) ||
        ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
         (RPTE_FRR_TNL_INFO_TYPE (pRsvpBaseTeTnlInfo) ==
          RPTE_FRR_BACKUP_TNL) &&
         (RPTE_IS_PLR_OR_PLRAWAIT (RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo)))))
    {
        pSenderTspec = &pRsvpTeTnlInfo->pPsb->SenderTspecObj.SenderTspec;

        RPTE_PUT_1_BYTE (pPdu, pSenderTspec->MsgHdr.u1Version);
        RPTE_PUT_1_BYTE (pPdu, RSVPTE_ZERO);    /* reserved field */

        RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (pSenderTspec->MsgHdr.u2HdrLen));

        RPTE_PUT_1_BYTE (pPdu, pSenderTspec->SrvHdr.u1SrvID);
        RPTE_PUT_1_BYTE (pPdu, pSenderTspec->SrvHdr.u1Flag);
        RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (pSenderTspec->SrvHdr.u2SrvLength));

        RPTE_PUT_1_BYTE (pPdu, pSenderTspec->ParamHdr.u1ParamNum);
        RPTE_PUT_1_BYTE (pPdu, pSenderTspec->ParamHdr.u1ParamFlags);
        RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (pSenderTspec->ParamHdr.u2ParamLen));

        f4TmpVar = pSenderTspec->TBParams.fBktRate;
        RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (f4TmpVar);
        f4TmpVar = OSIX_HTONF (f4TmpVar);
        MEMCPY (pPdu, &f4TmpVar, sizeof (FLT4));
        pPdu += sizeof (FLT4);

        f4TmpVar = pSenderTspec->TBParams.fBktSize;
        RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (f4TmpVar);
        f4TmpVar = OSIX_HTONF (f4TmpVar);
        MEMCPY (pPdu, &f4TmpVar, sizeof (FLT4));
        pPdu += sizeof (FLT4);

        f4TmpVar = pSenderTspec->TBParams.fPeakRate;
        RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (f4TmpVar);
        f4TmpVar = OSIX_HTONF (f4TmpVar);
        MEMCPY (pPdu, &f4TmpVar, sizeof (FLT4));
        pPdu += sizeof (FLT4);

        u4TmpVar = OSIX_HTONL (pSenderTspec->TBParams.u4MinSize);
        MEMCPY (pPdu, &u4TmpVar, sizeof (UINT4));
        pPdu += sizeof (UINT4);

        u4TmpVar = OSIX_HTONL (pSenderTspec->TBParams.u4MaxSize);
        MEMCPY (pPdu, &u4TmpVar, sizeof (UINT4));
        pPdu += sizeof (UINT4);
    }
    else
    {
        pSenderTspec = (tSenderTspec *) (VOID *) pPdu;
        MEMCPY (pSenderTspec,
                &pRsvpTeTnlInfo->pPsb->SenderTspecObj.SenderTspec,
                sizeof (tSenderTspec));
        pPdu = (pPdu + sizeof (tSenderTspec));
    }

    pObjHdr = (tObjHdr *) (VOID *) pPdu;

    /* Fill LSP_TUNNEL_INTERFACE_ID Object */
    RptePvmFillLSPTnlIfIdObj (&pObjHdr, pRsvpTeTnlInfo, PATH_MSG);

    /* If we use Int-Serv Sndr TSpec, we are using default AdSpec. */
    if (pRsvpTeTnlInfo->pPsb->AdSpec.MsgHdr.u2HdrLen != RSVPTE_ZERO)
    {
        if ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS) ||
            (pRsvpTeTnlInfo->pPsb->AdSpec.MsgHdr.u2HdrLen ==
             IPV4_ADSPEC_WITH_GS_AND_CLS_MSG_HDR_LEN))
        {

            pAdSpecObjHdr = (tAdSpecObj *) (VOID *) pObjHdr;

            OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tAdSpecObj));
            OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
                OSIX_HTONS (IPV4_ADSPEC_CLASS_NUM_TYPE);
            MEMCPY (&ADSPEC_OBJ (pAdSpecObjHdr),
                    &PSB_ADSPEC (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                    sizeof (tAdSpec));
        }
        else
        {
            pAdSpecWithoutGsAndClsObjHdr =
                (tAdSpecWithoutGsAndClsObj *) (VOID *) pObjHdr;

            OBJ_HDR_LENGTH (pObjHdr) =
                OSIX_HTONS (sizeof (tAdSpecWithoutGsAndClsObj));
            OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
                OSIX_HTONS (IPV4_ADSPEC_CLASS_NUM_TYPE);
            MEMCPY (&ADSPEC_OBJ (pAdSpecWithoutGsAndClsObjHdr),
                    &PSB_ADSPEC (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                    sizeof (tAdSpecWithoutGsAndCls));
            pAdSpecWithoutGsAndClsObjHdr->CtrlLoadSrvSpec =
                OSIX_HTONL (IPV4_ADSPEC_CLS_SRV_SPEC);
        }

        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* RRO Obj IPv4 Type updation */
    if (u1RroNeeded == RPTE_TRUE)
    {
        RptePvmFillRecordRouteObj (pRsvpTeTnlInfo, u2RROSize, &pObjHdr);
    }

    /* Upstream label obj updation */
    RptePvmFillUpstreamGenericLblObj (pRsvpTeTnlInfo, &pObjHdr, PATH_MSG);

    RptePvmFillRecoveryLblObj (pRsvpTeTnlInfo, &pObjHdr, PATH_MSG);

    RptePvmFillSuggestedLblObj (pRsvpTeTnlInfo, &pObjHdr);

    /* Upstream label obj updation */
    RpteUpdateDSElspTpObj (pRsvpTeTnlInfo, &pObjHdr);

    PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

    /* Path Refresh Sent on IfIndex */
    RptePbSendMsg (pPktMap);
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhPathRefresh : EXIT \n");
    MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
    MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhGeneratePathTear
 * Description     : This function generates the PATH Tear Messages to be sent
 *                   on the outgoing interfaces. It decides whether FRR is 
 *                   enabled. If so and if some backup tunnels also exists,
 *                   PATH Tear Message is sent on the backup tunnels also.
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo structure
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhGeneratePathTear (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tPktMap             PktMap;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    BOOL1               bBkpTnlExist = RPTE_FALSE;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhGeneratePathTear : ENTRY \n");

    /* if the path tear msg is sent for protection path and
     * protection in use bit of working path is set,
     * send path tear for working path also */

    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE) &&
        (pRsvpTeTnlInfo->pMapTnlInfo != NULL) &&
        (pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo != NULL) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_PROTECTION_PATH)
        && (pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
            LOCAL_PROT_IN_USE))
    {
        pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
            u1TnlLocalProtectInUse = LOCAL_PROT_NOT_AVAIL;
        RptePhGeneratePathTear (pRsvpTeTnlInfo->pMapTnlInfo);
        RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo->pMapTnlInfo) = RPTE_TRUE;
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo->pMapTnlInfo,
                             TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
    }

    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_WORKING_PATH)
        && (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
            LOCAL_PROT_AVAIL))
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse =
            LOCAL_PROT_NOT_AVAIL;
    }

    /* Initialize RSVP PktMap */
    RpteUtlInitPktMap (&PktMap);
    RPTE_PKT_MAP_TNL_INFO (&PktMap) = pRsvpTeTnlInfo;

    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH TEAR: Egress Node cannot generate PATH TEAR\n");
        return;
    }

    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH TEAR : PSB does not exist pathtear not sent\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhGeneratePathTear : "
                    "INTMD - EXIT \n");
        return;
    }

    if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
        (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL))
    {
        if ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS) &&
            (RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo) != NULL) &&
            ((TE_TNL_FRR_CONST_PROT_METHOD (RPTE_TE_TNL_FRR_INFO
                                            (pRsvpTeTnlInfo))
              == RSVPTE_TNL_FRR_ONE2ONE_METHOD) &&
             ((RSVPTE_TNL_SSN_ATTR (RPTE_TE_TNL (pRsvpTeTnlInfo))
               & TE_SSN_FAST_REROUTE_BIT) == TE_SSN_FAST_REROUTE_BIT)))
        {
            if (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) != NULL)
            {
                bBkpTnlExist = RPTE_TRUE;
            }
        }
        else
        {
            pRsvpTeFastReroute =
                &(PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
            if (RPTE_FAST_REROUTE_FLAGS (pRsvpTeFastReroute) ==
                RSVPTE_TNL_FRR_ONE2ONE_METHOD)
            {
                if (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) != NULL)
                {
                    bBkpTnlExist = RPTE_TRUE;
                }
            }
        }
    }
    pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));

    if (pIfEntry == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Outgoing Interface is not Active \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhGeneratePathTear : INTMD-EXIT \n");
        return;
    }

    PKT_MAP_OUT_IF_ENTRY (&PktMap) = pIfEntry;
    RPTE_PKT_MAP_NBR_ENTRY (&PktMap) = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
    PKT_MAP_TX_TTL (&PktMap) = IF_ENTRY_TTL (pIfEntry);

    pNbrEntry = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
    if (pNbrEntry != NULL)
    {
        if (NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) == RPTE_TRUE)
        {
            RpteRRSRefreshThreshold (pNbrEntry, RPTE_ZERO);
        }
    }

    if (!(RPTE_IS_DNLOST (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo))))
    {
        RptePhConstructAndSndPTMsg (&PktMap, pRsvpTeTnlInfo);
    }
    /* FRR is enabled. Some backup tunnels exists, so sending PATH Tear
     * for those tunnels also. */
    if (bBkpTnlExist)
    {
        RptePhConstructAndSndPTMsgOnBkpPath (&PktMap,
                                             RPTE_FRR_OUT_TNL_INFO
                                             (pRsvpTeTnlInfo));
        RpteResetPlrStatus (pRsvpTeTnlInfo);
    }
    RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePhGeneratePathTear : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhConstructAndSndPTMsg 
 * Description     : This function Constructs the PATH Tear Message with the 
 *                   format required and sends it on the outgoing interface.
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo structure
 *                   pPktMap        - Pointer to PktMap Structure which is 
 *                                    used to construct the PATH Tear Message.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhConstructAndSndPTMsg (tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    INT4                i4PathTearSize = RPTE_ZERO;
    INT4                i4Retval = RPTE_ZERO;
    UINT4               u4PathTearMsgId;
    UINT4               u4Val = RPTE_ZERO;
    UINT4               u4TmpAddr = RPTE_ZERO;
    tRpteKey            RpteKey;
    UINT1              *pPdu = NULL;
    tTimerBlock        *pTimerBlock = NULL;
    tuTrieInfo         *pTrieInfo = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tObjHdr            *pObjHdr = NULL;
    tSenderTspec       *pSenderTspec = NULL;
    UINT4               u4TmpVar = RPTE_ZERO;
    FLT4                f4TmpVar = RPTE_ZERO;
    UINT2               u2TmpVar = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePhConstructAndSndPTMsg : ENTRY \n");
    if ((pRsvpTeTnlInfo->u1SendToByPassTnl == RPTE_FALSE) &&
        (RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE))
    {
        if (((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_IN_USE) ==
             LOCAL_PROT_IN_USE) &&
            ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
              u1Flags) == RSVPTE_TNL_FRR_FACILITY_METHOD) &&
            (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL)
            && (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo))))
        {
            pRsvpTeTnlInfo->u1SendToByPassTnl = RPTE_TRUE;
        }
    }

    if (((RPTE_PKT_MAP_NBR_ENTRY (pPktMap) == NULL) ||
         (NBR_ENTRY_RMD_CAPABLE (RPTE_PKT_MAP_NBR_ENTRY (pPktMap)) ==
          RPTE_TRUE)) && (gu4RMDFlags & RPTE_RMD_PATH_TEAR_MSG))
    {
        if (RPTE_TNL_PT_TIMER_BLOCK (pRsvpTeTnlInfo) == NULL)
        {
            /* Allocate memory if timer block is not present */
            pTimerBlock = (tTimerBlock *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TIMER_BLK_POOL_ID);

            if (pTimerBlock == NULL)
            {
                /* If the allocation of the Timer Block fails
                 * the message will be sent without Msg Id */
                RSVPTE_DBG (RSVPTE_PH_MEM,
                            "Failed to allocate timer block. "
                            "Sending Path Tear msg without msg id.\n");
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
            }
            else
            {
                RPTE_TNL_PT_TIMER_BLOCK (pRsvpTeTnlInfo) = pTimerBlock;
                /* Back Off Timer value is set to the initial value */
                TIMER_BLK_BACKOFF_INTERVAL (pTimerBlock) =
                    (IF_ENTRY_REFRESH_INTERVAL (PKT_MAP_OUT_IF_ENTRY
                                                (pPktMap)) /
                     RPTE_BACK_OFF_FACTOR) / DEFAULT_CONV_TO_SECONDS;

                /* Msg Id is generated for the outgoing path error
                 * message */
                RpteMIHGenerateMsgId (&u4PathTearMsgId);

                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;

                /* Since RMD is enabled for Path Tear, back off timer is
                 * started for the same. */
                TIMER_PARAM_RPTE_TNL ((&TIMER_BLK_TIMER_PARAM
                                       (pTimerBlock))) = pRsvpTeTnlInfo;
                TIMER_BLK_MSG_ID (pTimerBlock) = u4PathTearMsgId;
                TIMER_PARAM_ID ((&TIMER_BLK_TIMER_PARAM (pTimerBlock))) =
                    RPTE_PATHTEAR;

                RSVP_TIMER_NAME ((&TIMER_BLK_APP_TIMER (pTimerBlock))) =
                    (FS_ULONG) & TIMER_BLK_TIMER_PARAM (pTimerBlock);

                pTrieInfo = (tuTrieInfo *)
                    RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TRIE_INFO_POOL_ID);

                if (pTrieInfo == NULL)
                {
                    RSVPTE_DBG (RSVPTE_PH_MEM,
                                "Failed to allocate Trie Info "
                                "Sending Path Tear msg without msg id.\n");
                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                    MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                        (UINT1 *) pTimerBlock);
                    pTimerBlock = NULL;
                }
                else
                {
                    MEMSET (pTrieInfo, RSVPTE_ZERO, sizeof (tuTrieInfo));
                    RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo) = RPTE_PATHTEAR;
                    RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = pTimerBlock;

                    RpteKey.u4_Key = u4PathTearMsgId;

                    /* RBTree Addtion is only required for out path message id for 
                     * SRefresh RecoveryPath message support.
                     * Its not required for Path tear message id
                     * */

                    if (RpteTrieAddEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                          &RpteKey, pTrieInfo) != RPTE_SUCCESS)
                    {
                        RSVPTE_DBG (RSVPTE_PH_PRCS,
                                    "PVM : Trie Addition Failed. "
                                    "- in Path Tear\n");
                        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) =
                            RPTE_ZERO;
                        MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                            (UINT1 *) pTrieInfo);
                        RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = NULL;
                        MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                            (UINT1 *) pTimerBlock);
                        pTimerBlock = NULL;
                        pTrieInfo = NULL;
                    }
                }
            }
        }
        else
        {
            pTimerBlock = RPTE_TNL_PT_TIMER_BLOCK (pRsvpTeTnlInfo);

            RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;
            u4PathTearMsgId = TIMER_BLK_MSG_ID (pTimerBlock);

            RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
            if (RpteTrieLookupEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     &pTrieInfo) != RPTE_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "Look Up Failed - in Path Tear\n"
                            "ATTENTION : TimerBlock Present, but msg id "
                            "not present in Trie\n");
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
            }
        }

        if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
        {
            if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                                   (pTimerBlock),
                                   TIMER_BLK_BACKOFF_INTERVAL (pTimerBlock) *
                                   SYS_NUM_OF_TIME_UNITS_IN_A_SEC) !=
                TMR_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "PVM : Start Timer failed. - in Path Tear "
                            "Sending Path Tear without msg id\n");
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = NULL;
                MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                    (UINT1 *) pTrieInfo);
                pTrieInfo = NULL;
                pTimerBlock = NULL;
            }
        }
    }
    else
    {
        /* In the case of PT Timer Block already present,
         * Stop the timer, Delete the Msg Id already present in
         * the Data base and continue. */
        pTimerBlock = RPTE_TNL_PT_TIMER_BLOCK (pRsvpTeTnlInfo);

        if (pTimerBlock != NULL)
        {
            TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                          (pTimerBlock));
            RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);

            RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                 RPTE_LCL_MSG_ID_DB_PREFIX,
                                 RpteMIHReleaseTrieInfo);

            MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                (UINT1 *) pTimerBlock);
            pTimerBlock = NULL;
            pTrieInfo = NULL;
        }
        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_ZERO;
    }

    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) == RPTE_ZERO)
    {
        RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
        RPTE_TNL_PT_TIMER_BLOCK (pRsvpTeTnlInfo) = NULL;
    }
    else
    {
        RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) = gu4Epoch;
        RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_MSG_ID_ACK_DESIRED;
        RPTE_PKT_MAP_MSG_ID (pPktMap) = u4PathTearMsgId;
        TIMER_BLK_TX_TTL_VALUE (pTimerBlock) = PKT_MAP_TX_TTL (pPktMap);
        i4PathTearSize = (INT4) (i4PathTearSize + RPTE_MSG_ID_OBJ_LEN);
    }

    i4PathTearSize =
        (INT4) (i4PathTearSize +
                (sizeof (tRsvpHdr) + RPTE_IPV4_SSN_OBJ_LEN +
                 RPTE_IPV4_SNDR_TMP_OBJ_LEN + sizeof (tSenderTspecObj)));

    i4Retval
        = RpteUtlCalculateRSVPHopSize (pRsvpTeTnlInfo->b1DnStrOob,
                                       pRsvpTeTnlInfo->u4DnStrDataTeLinkIfId,
                                       pRsvpTeTnlInfo->pPsb->pOutIfEntry->
                                       u4Addr);
    i4PathTearSize = (INT4) (i4PathTearSize + i4Retval);

    PKT_MAP_RSVP_PKT (pPktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);

    if (PKT_MAP_RSVP_PKT (pPktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS, "PATH TEAR : Memory Allocation failed \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RptePhConstructAndSndPTMsg : INTMD-EXIT \n");
        return;
    }

    MEMSET (PKT_MAP_RSVP_PKT (pPktMap), RSVPTE_ZERO, i4PathTearSize);
    PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    /* Update Src Addr from Tnl Info */
    PKT_MAP_SRC_ADDR (pPktMap) =
        OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo));

    /* Assuming by default PathTear Msg will be sent with Router Alert option */
    /* 
     * NOTE: Currently RSVPTE assumes there won't be any IP options other
     * than the Router Alert present in the IpHdr while calculating Msg Size 
     */
    /* Enable RA option needed */
    if (pRsvpTeTnlInfo->u4FALspSrcOrDest == RPTE_ZERO)
    {
        PKT_MAP_RA_OPT_NEEDED (pPktMap) = RPTE_YES;
        PKT_MAP_OPT_LEN (pPktMap) = sizeof (tRaOpt);
        /* Update Dest Addr from Tnl Info */
        PKT_MAP_DST_ADDR (pPktMap) =
            OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));
    }
    else
    {
        /* For E2E LSP at ingress of FA LSP, Destination of IP Header should be 
         * Egress of FA LSP */
        PKT_MAP_DST_ADDR (pPktMap) = pRsvpTeTnlInfo->u4FALspSrcOrDest;
    }
    /* Construct Rsvp Common Header */
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    MEMSET (pRsvpHdr, RSVPTE_ZERO, sizeof (tRsvpHdr));
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = PATHTEAR_MSG;
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS ((UINT2) i4PathTearSize);

    pObjHdr =
        (tObjHdr *) (VOID *) (PKT_MAP_RSVP_PKT (pPktMap) + sizeof (tRsvpHdr));

    /* Msg Id Obj update, if present */
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        /* Filling the Header */
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_MSG_ID_OBJ_LEN);
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_MESSAGE_ID_CLASS_NUM_TYPE);

        pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

        /* Filling the Epoch and Flags fields */
        u4Val = (RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) |
                 ((UINT4)
                  (RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) << RPTE_3_BYTE_SHIFT)));
        u4Val = OSIX_HTONL (u4Val);
        MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4Val, RPTE_FLAG_EPOCH_LEN);

        pPdu += RPTE_FLAG_EPOCH_LEN;

        /* Filling the Message Identifier field */
        u4Val = OSIX_HTONL (RPTE_PKT_MAP_MSG_ID (pPktMap));
        MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4Val, RPTE_MSG_ID_LEN);
        pPdu += RPTE_MSG_ID_LEN;

        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* Update RsvpTe Ssn Obj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);

    pObjHdr = (tObjHdr *) (VOID *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

    pPdu = (UINT1 *) pObjHdr;

    u4TmpAddr = RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo);

    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);    /* reserved field */
    RPTE_PUT_2_BYTES (pPdu,
                      (UINT2) OSIX_HTONS (RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo)));

    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo)), u4TmpAddr);

    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;

    pObjHdr = (tObjHdr *) (VOID *) pPdu;

    RptePvmFillRsvpHopObj (pRsvpTeTnlInfo, &pObjHdr, PATHTEAR_MSG);

    pObjHdr = NEXT_OBJ (pObjHdr);

    pPdu = (UINT1 *) pObjHdr;

    /* Update Sndr Tmp obj */
    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (RPTE_IPV4_SNDR_TMP_OBJ_LEN));

    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE));
    CONVERT_TO_INTEGER (PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                        TnlSndrAddr, u4TmpAddr);
    MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPdu += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);    /* reserved field */
    u2TmpVar =
        OSIX_HTONS (PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                    u2LspId);
    MEMCPY (pPdu, (UINT1 *) &u2TmpVar, sizeof (UINT2));
    pPdu += sizeof (UINT2);
    pObjHdr = (tObjHdr *) (VOID *) pPdu;

    /* Update Sender Tspec */

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tSenderTspecObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_SENDER_TSPEC_CLASS_NUM_TYPE);

    pPdu = ((UINT1 *) pObjHdr + sizeof (tObjHdr));

    /* In Ingress, PSB - SnderTspec is in Host order */
    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        pSenderTspec = &pRsvpTeTnlInfo->pPsb->SenderTspecObj.SenderTspec;

        RPTE_PUT_1_BYTE (pPdu, pSenderTspec->MsgHdr.u1Version);
        RPTE_PUT_1_BYTE (pPdu, RSVPTE_ZERO);    /* reserved field */

        RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (pSenderTspec->MsgHdr.u2HdrLen));

        RPTE_PUT_1_BYTE (pPdu, pSenderTspec->SrvHdr.u1SrvID);
        RPTE_PUT_1_BYTE (pPdu, pSenderTspec->SrvHdr.u1Flag);
        RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (pSenderTspec->SrvHdr.u2SrvLength));

        RPTE_PUT_1_BYTE (pPdu, pSenderTspec->ParamHdr.u1ParamNum);
        RPTE_PUT_1_BYTE (pPdu, pSenderTspec->ParamHdr.u1ParamFlags);
        RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (pSenderTspec->ParamHdr.u2ParamLen));

        f4TmpVar = pSenderTspec->TBParams.fBktRate;
        RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (f4TmpVar);
        f4TmpVar = OSIX_HTONF (f4TmpVar);
        MEMCPY (pPdu, &f4TmpVar, sizeof (FLT4));
        pPdu += sizeof (FLT4);

        f4TmpVar = pSenderTspec->TBParams.fBktSize;
        RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (f4TmpVar);
        f4TmpVar = OSIX_HTONF (f4TmpVar);
        MEMCPY (pPdu, &f4TmpVar, sizeof (FLT4));
        pPdu += sizeof (FLT4);

        f4TmpVar = pSenderTspec->TBParams.fPeakRate;
        RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC (f4TmpVar);
        f4TmpVar = OSIX_HTONF (f4TmpVar);
        MEMCPY (pPdu, &f4TmpVar, sizeof (FLT4));
        pPdu += sizeof (FLT4);

        u4TmpVar = OSIX_HTONL (pSenderTspec->TBParams.u4MinSize);
        MEMCPY (pPdu, &u4TmpVar, sizeof (UINT4));
        pPdu += sizeof (UINT4);

        u4TmpVar = OSIX_HTONL (pSenderTspec->TBParams.u4MaxSize);
        MEMCPY (pPdu, &u4TmpVar, sizeof (UINT4));
        pPdu += sizeof (UINT4);

    }
    else
    {
        pSenderTspec = (tSenderTspec *) (VOID *) pPdu;
        MEMCPY (pSenderTspec,
                &pRsvpTeTnlInfo->pPsb->SenderTspecObj.SenderTspec,
                sizeof (tSenderTspec));
        pPdu = (pPdu + sizeof (tSenderTspec));
    }
    pObjHdr = (tObjHdr *) (VOID *) pPdu;
    RSVP_HDR_SEND_TTL (pRsvpHdr) = PKT_MAP_TX_TTL (pPktMap);
    PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

    /* Path Tear Sent on Iface */
    RptePbSendMsg (pPktMap);
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhConstructAndSndPTMsg : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhConstructAndSndPTMsgOnBkpPath 
 * Description     : This function determines the outgoing the interface on 
 *                   which PATH Tear needs to be sent for the backup tunnels
 *                   and removes the RSVP tunnel related information 
 *                   maintained.
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo structure.
 *                   pPktMap        - Pointer to PktMap.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhConstructAndSndPTMsgOnBkpPath (tPktMap * pPktMap,
                                     tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tIfEntry           *pIfEntry = NULL;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhConstructAndSndPTMsgOnBkpPath - "
                "ENTRY \n");

    RPTE_PKT_MAP_TNL_INFO (pPktMap) = pRsvpTeTnlInfo;
    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH TEAR : PSB does not exist pathtear not sent\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhConstructAndSndPTMsgOnBkpPath "
                    ":INTMD - EXIT \n");
        return;
    }

    pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));

    if (pIfEntry == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS, "PATH : Outgoing Interface is NULL\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhConstructAndSndPTMsgOnBkpPath "
                    ":INTMD - EXIT \n");
        return;
    }

    PKT_MAP_OUT_IF_ENTRY (pPktMap) = pIfEntry;
    RPTE_PKT_MAP_NBR_ENTRY (pPktMap) = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
    PKT_MAP_TX_TTL (pPktMap) = IF_ENTRY_TTL (pIfEntry);

    pRsvpTeTnlInfo->u1IsBkpPathMsgToCfa = RPTE_TRUE;
    RptePhConstructAndSndPTMsg (pPktMap, pRsvpTeTnlInfo);
    RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
    if (RPTE_TE_TNL (pRsvpTeTnlInfo)->u1TnlRelStatus ==
        TE_SIGMOD_TNLREL_AWAITED)
    {
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                             TE_TNL_CALL_FROM_ADMIN, RPTE_FALSE);
    }
    else
    {
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                             TE_TNL_CALL_FROM_SIG, RPTE_FALSE);
    }
    RSVPTE_DBG (RSVPTE_PT_ETEXT,
                "RptePhConstructAndSndPTMsgOnBkpPath : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteUpdateTnlTblInfo
 * Description     : This function updates pRsvpTeTnlInfo. If necessary it 
 *                   turns on u1PathRefreshNeeded and u1ResvRefreshNeeded flags
 * Input (s)       : pRsvpTeTnlInfo - Pinter to pRsvpTeTnlInfo
 *                   pPktMap - Pointer to PktMap
 *                   pu1PathRefreshNeeded - Pointer to PathRefresh Flag
 *                   pu1ResvRefreshNeeded - Pointer to ResvRefresh Flag
 * Output (s)      : pRsvpTeTnlInfo
 *                   pu1PathRefreshNeeded
 *                   pu1ResvRefreshNeeded
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE 
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteUpdateTnlTblInfo (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                      tPktMap * pPktMap,
                      UINT1 *pu1PathRefreshNeeded, UINT1 *pu1ResvRefreshNeeded)
{
    tRsvpHop            RsvpTmpHop;
    tRsvpHop           *pTempRsvpHop = NULL;
    tTimeValues        *pTimeValues = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tRsvpTeArHopListInfo *pArHopListInfo = NULL;
    tObjHdr            *pObjHdr = NULL;
    UINT4               u4SenderAddr = RPTE_ZERO;
    UINT4               u4ExtTnlId = RPTE_ZERO;
    UINT4               u4PlrId = RPTE_ZERO;
    UINT4               u4AdminStatus = RPTE_ZERO;
    UINT2               u2Len = RPTE_ZERO;
    UINT2               u2Cnt = RPTE_ZERO;
    BOOL1               bUpdateFlag = RPTE_FALSE;
    UINT1               u1HlspFlag = FALSE;

    RsvpTmpHop.u4HopAddr =
        OSIX_NTOHL (PSB_RSVP_HOP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).u4HopAddr);
    RsvpTmpHop.u4Lih = PSB_RSVP_HOP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).u4Lih;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteUpdateTnlTblInfo : ENTRY \n");
    /* NOTE: pu1PathRefreshNeeded is set to RPTE_REFRESH_MSG_YES, */
    /*       if this is a New Tnl */
    if (*pu1PathRefreshNeeded != RPTE_REFRESH_MSG_YES)
    {
        /* Compare RsvpHop */
        if (pPktMap->pRsvpHopObj != NULL)
        {
            pTempRsvpHop = &RSVP_HOP_OBJ (PKT_MAP_RSVP_HOP_OBJ (pPktMap));
        }
        else if (pPktMap->pIfIdRsvpHopObj != NULL)
        {
            pTempRsvpHop = &(pPktMap->pIfIdRsvpHopObj->RsvpHop);
        }
        else if (pPktMap->pIfIdRsvpNumHopObj != NULL)
        {
            pTempRsvpHop = &(pPktMap->pIfIdRsvpNumHopObj->RsvpHop);
        }
        else
        {
            return RPTE_FAILURE;
        }

        /* We need to revisit this code again for IF ID RSVP Hop Object */
        if (RpteUtlCompareRsvpHop (&RsvpTmpHop, pTempRsvpHop) == RPTE_FAILURE)
        {
            PSB_RSVP_HOP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).u4HopAddr
                = OSIX_NTOHL (pTempRsvpHop->u4HopAddr);
            PSB_RSVP_HOP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).u4Lih
                = pTempRsvpHop->u4Lih;
        }

        /* Compare Previous Hop for FRR facility backup case handle */
        if (PKT_MAP_IF_ENTRY (pPktMap) !=
            PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)))
        {
            if (PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD)
            {
                PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                    PKT_MAP_IF_ENTRY (pPktMap);
                RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) |= RPTE_FRR_NODE_STATE_MP;
            }
        }
        if (FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap)).u2Length !=
            RPTE_ZERO)
        {
            pObjHdr = &FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap));
            u2Len = OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr));
            /* No of PLRs and Avoid Node Id pairs */
            u2Len = (UINT2) (u2Len - sizeof (tObjHdr));
            u2Len = (UINT2) (u2Len / ((UINT2) (RPTE_TWO * RSVPTE_IPV4ADR_LEN)));

            if (u2Len > RPTE_MAX_DETOUR_IDS)
            {
                return RPTE_FAILURE;
            }

            for (u2Cnt = RPTE_ZERO;
                 ((u2Cnt < RPTE_MAX_DETOUR_IDS) &&
                  (PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                   au4PlrId[u2Cnt] != RPTE_ZERO)); u2Cnt++);
            if (u2Len != u2Cnt)
            {
                bUpdateFlag = RPTE_TRUE;

                MEMSET (&((PSB_RPTE_FRR_DETOUR
                           (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))).au4PlrId),
                        RSVPTE_ZERO, ((sizeof (UINT4)) * RPTE_MAX_DETOUR_IDS));
                MEMSET (&((PSB_RPTE_FRR_DETOUR
                           (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))).au4AvoidNodeId),
                        RSVPTE_ZERO, ((sizeof (UINT4)) * RPTE_MAX_DETOUR_IDS));

                for (u2Cnt = RPTE_ZERO; u2Cnt < u2Len; u2Cnt++)
                {
                    PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                        au4PlrId[u2Cnt] =
                        OSIX_NTOHL (PKT_MAP_RPTE_DETOUR_OBJ
                                    (pPktMap).RsvpTeDetour.au4PlrId[u2Cnt]);
                    PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                        au4AvoidNodeId[u2Cnt] =
                        OSIX_NTOHL (PKT_MAP_RPTE_DETOUR_OBJ
                                    (pPktMap).RsvpTeDetour.
                                    au4AvoidNodeId[u2Cnt]);
                }
            }
        }
        CONVERT_TO_INTEGER ((PKT_MAP_RPTE_ST_TNL_SNDR_ADDR (pPktMap)),
                            u4SenderAddr);
        u4SenderAddr = OSIX_NTOHL (u4SenderAddr);
        CONVERT_TO_INTEGER ((PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap)),
                            u4ExtTnlId);
        u4ExtTnlId = OSIX_NTOHL (u4ExtTnlId);

        /* Comparing the Sender address received and the 
         * sender address of the tunnel that refreshes the downstream. 
         * If it is different copying the sender address received to the PSB. */
        if (u4SenderAddr != u4ExtTnlId)
        {
            if (MEMCMP
                (&PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                 TnlSndrAddr, (&PKT_MAP_RPTE_SNDR_TMP_OBJ_SNDR_ADDR (pPktMap)),
                 RSVPTE_IPV4ADR_LEN) != RPTE_ZERO)
            {
                /* Copy Sender Template Obj */
                MEMCPY (&PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                        TnlSndrAddr,
                        (&PKT_MAP_RPTE_SNDR_TMP_OBJ_SNDR_ADDR (pPktMap)),
                        RSVPTE_IPV4ADR_LEN);
                /* Update the filter sender address also */
                if (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL)
                {
                    MEMCPY (&RSB_RPTE_FILTER_SPEC
                            (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)).TnlSndrAddr,
                            (&PKT_MAP_RPTE_SNDR_TMP_OBJ_SNDR_ADDR (pPktMap)),
                            RSVPTE_IPV4ADR_LEN);
                }
            }
        }
        else
        {
            if ((bUpdateFlag == RPTE_TRUE) &&
                (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL))
            {
                u4PlrId = PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB
                                               (pRsvpTeTnlInfo)).
                    au4PlrId[RPTE_ZERO];
                u4PlrId = OSIX_HTONL (u4PlrId);

                MEMCPY (&RSB_RPTE_FILTER_SPEC (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)).
                        TnlSndrAddr, &u4PlrId, RSVPTE_IPV4ADR_LEN);
            }

            if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE) &&
                (PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                 u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD)
                && (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL))
            {
                MEMCPY (&RSB_RPTE_FILTER_SPEC (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)).
                        TnlSndrAddr, &u4SenderAddr, RSVPTE_IPV4ADR_LEN);
            }

        }

        if (PKT_MAP_SENDER_TSPEC_OBJ (pPktMap) != NULL)
        {
            if (MEMCMP
                (&SENDER_TSPEC_OBJ
                 (&PSB_SENDER_TSPEC_OBJ (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))),
                 &SENDER_TSPEC_OBJ (PKT_MAP_SENDER_TSPEC_OBJ (pPktMap)),
                 sizeof (tSenderTspec)) != RSVPTE_ZERO)
            {
                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "PATH : Rcvd Sndr Tspec doesn't match with "
                            "stored Sndr Tspec - pkt dropped\n");
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RpteUpdateTnlTblInfo : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
        }
        /* 
         * NOTE: The InErHopList is compared with the Pkt map ErHopList 
         * if there is difference, current implementation ignores the pkt.
         */
        if ((TMO_SLL_Count (&pPktMap->ErObj.ErHopList) != RSVPTE_ZERO) &&
            (RSVPTE_TNL_IN_PATH_OPTION (pRsvpTeTnlInfo) != NULL))
        {
            if ((TMO_SLL_Count (&pPktMap->ErObj.ErHopList) !=
                 TMO_SLL_Count (&pRsvpTeTnlInfo->pTePathInfo->ErHopList) ||
                 (RpteCompERO (&pPktMap->ErObj.ErHopList,
                               &pRsvpTeTnlInfo->pTePathInfo->ErHopList))
                 != RPTE_EQUAL))
            {

                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "PATH : Err - Rcvd ERO doesn't match with "
                            "stored ERO \n");
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RpteUpdateTnlTblInfo : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
        }

        if (TMO_SLL_Count (&pPktMap->RrObj.RrHopList) != RSVPTE_ZERO)
        {
            if (RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL)
            {
                if ((TMO_SLL_Count (&pPktMap->RrObj.RrHopList) !=
                     TMO_SLL_Count (&RSVPTE_TNL_INARHOP_LIST (pRsvpTeTnlInfo)))
                    ||
                    (RpteCompRRO (&pPktMap->RrObj.RrHopList,
                                  &RSVPTE_TNL_INARHOP_LIST (pRsvpTeTnlInfo))
                     != RPTE_EQUAL))
                {
                    /* 
                     * Rxed RRO Doesn't Match with Stored RRO, 
                     * Remove the stored RRO list free its memory, add new RRO 
                     * list into that, set path refresh flag 
                     */

                    /* Free the allocated RrHop List memory */
                    rpteTeDeleteArHopListInfo (RSVPTE_IN_ARHOP_LIST_INFO
                                               (pRsvpTeTnlInfo));
                    /* Copy the New Incoming RRO list into Tnl Info */
                    if (rpteTeCreateArHopListInfo (&pArHopListInfo,
                                                   &pPktMap->RrObj.RrHopList)
                        == RPTE_SUCCESS)
                    {
                        RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo) =
                            pArHopListInfo;
                    }

                    *pu1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
                }
            }
            else
            {
                if (rpteTeCreateArHopListInfo (&pArHopListInfo,
                                               &pPktMap->RrObj.RrHopList)
                    == RPTE_SUCCESS)
                {
                    RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo) = pArHopListInfo;
                }
            }
        }
        else if ((TMO_SLL_Count (&pPktMap->RrObj.RrHopList) == RSVPTE_ZERO) &&
                 (RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL))
        {
            /* 
             * Delete the RRO back off Timer from the Timer list, 
             * if I am the one who started RRO back off Timer
             */
            if (pRsvpTeTnlInfo->u2BTime > RSVPTE_ZERO)
            {
                TmrStopTimer (RSVP_GBL_TIMER_LIST,
                              (tTmrAppTimer *) & pRsvpTeTnlInfo->RROTnlTmr);
            }
            rpteTeDeleteArHopListInfo (RSVPTE_IN_ARHOP_LIST_INFO
                                       (pRsvpTeTnlInfo));
            RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo) = NULL;
            /*reset the record route bit */
            RSVPTE_TNL_SSN_ATTR (RPTE_TE_TNL (pRsvpTeTnlInfo))
                &= (UINT1) (~RSVPTE_SSN_REC_ROUTE_BIT);

            *pu1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
        }
    }
    pTimeValues = &TIME_VALUES_OBJ (PKT_MAP_TIME_VALUES_OBJ (pPktMap));
    PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
        RpteUtlComputeLifeTime (PKT_MAP_IF_ENTRY (pPktMap),
                                OSIX_NTOHL (TIME_VALUES_PERIOD (pTimeValues)));
    PSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
        OSIX_NTOHL (TIME_VALUES_PERIOD (pTimeValues));

    if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL) &&
        (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) != NULL) &&
        (RPTE_FRR_TNL_INFO_TYPE (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo))
         == RPTE_FRR_PROTECTED_TNL))
    {
        PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo))) =
            PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
        PSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_PSB
                                 (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo))) =
            OSIX_NTOHL (TIME_VALUES_PERIOD (pTimeValues));
    }
    PSB_RX_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = PKT_MAP_RX_TTL (pPktMap);
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    PSB_REMAINING_TTL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
        RSVP_HDR_SEND_TTL (pRsvpHdr);

    /* Incase of facility backup, refreshing thro. bypass tnl TTL value of RSVP header 
     * will be different and it is a expected behavior as per RFC 4090 */
    if (RSVP_HDR_SEND_TTL (pRsvpHdr) != PKT_MAP_RX_TTL (pPktMap))
    {
        /* Non-Rsvp Cloud Detected */
        PSB_NON_RSVP_FLAG (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = RPTE_SNMP_TRUE;
    }
    else
    {
        PSB_NON_RSVP_FLAG (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = RPTE_SNMP_FALSE;
    }

    /* If the received Admin status bit is not equal with the current admin status
     * bit, path message has to be sent immediately from intermediate nodes
     * 
     *  If the received Admin status bit is not equal with the current admin status
     *  bit, path message has to be sent immediately from egress node*/

    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable == RPTE_ENABLED)
    {
        if ((pPktMap->pAdminStatusObj != NULL) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus !=
             OSIX_NTOHL (pPktMap->pAdminStatusObj->AdminStatus.u4AdminStatus)))
        {
            u4AdminStatus
                =
                OSIX_NTOHL (pPktMap->pAdminStatusObj->AdminStatus.
                            u4AdminStatus);
            pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus =
                u4AdminStatus;

            if (pPktMap->pProtectionObj != NULL)
            {
                pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType =
                    pPktMap->pProtectionObj->Protection.u1LspFlag;
            }

            if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INTERMEDIATE)
            {
                RptePhAdminStatusActionItem (pRsvpTeTnlInfo);
                *pu1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
            }
            else if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS) &&
                     (pRsvpTeTnlInfo->pRsb != NULL))
            {
                pRsvpTeTnlInfo->pRsb->u4AdminStatus = u4AdminStatus;

                RptePhAdminStatusActionItem (pRsvpTeTnlInfo);

                /* If "R" bit is set, remove that bit */
                pRsvpTeTnlInfo->pRsb->u4AdminStatus &= (~GMPLS_ADMIN_REFLECT);

                *pu1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
            }
        }
        else if (pPktMap->pAdminStatusObj == NULL)
        {
            if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus !=
                GMPLS_ADMIN_UNKNOWN)
            {
                pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus
                    = GMPLS_ADMIN_UNKNOWN;

                RptePhAdminStatusActionItem (pRsvpTeTnlInfo);

                *pu1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
            }
        }
    }
    if (pPktMap->pLspTnlIfIdObj != NULL)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType |= TE_TNL_TYPE_HLSP;
    }
    else
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType &= (UINT4) ~TE_TNL_TYPE_HLSP;
    }

    if (((pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP)
         == TE_TNL_TYPE_HLSP) &&
        (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS))
    {
        /* For Creation of FA TE-Link, MPLS_TUNNEL Creation is mandatory in 
         * Egress even for Uni directional tunnel. For Bi Directional tunnel, 
         * the tunnel is created in the ProcessPath Msg.
         */
        if ((pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode ==
             TE_TNL_MODE_UNIDIRECTIONAL) &&
            ((pRsvpTeTnlInfo->pTeTnlInfo->u4RevTnlIfIndex == RPTE_ZERO) &&
             (RptePortCfaCreateMplsTnlIf (pRsvpTeTnlInfo, PATH_MSG) ==
              RPTE_FAILURE)))
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH: Mpls Tunnel Interface Creation failed\n");
            RptePvmSendPathErr (pPktMap,
                                ADMISSION_CONTROL_FAILURE, DEFAULT_ERROR_VALUE);
            RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                        "RptePhRhProcessPathMsg : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
    }

    if (pPktMap->pLspTnlIfIdObj != NULL)
    {
        pRsvpTeTnlInfo->u2HlspCType
            = OSIX_NTOHS (pPktMap->pLspTnlIfIdObj->ObjHdr.u2ClassNumType);
        if (pRsvpTeTnlInfo->pPsb->LspTnlIfId.u4RouterId == RPTE_ZERO)
        {
            pRsvpTeTnlInfo->pPsb->LspTnlIfId.u4RouterId
                = OSIX_NTOHL (pPktMap->pLspTnlIfIdObj->LspTnlIfId.u4RouterId);

            pRsvpTeTnlInfo->pPsb->LspTnlIfId.u4InterfaceId
                =
                OSIX_NTOHL (pPktMap->pLspTnlIfIdObj->LspTnlIfId.u4InterfaceId);

            if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType !=
                TE_TNL_PROTECTION_PATH)
            {
                if (RptePortCreateFATELink (pRsvpTeTnlInfo) == RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_PH_PRCS,
                                "PATH : Err - Create FA TE Link Failed\n");
                    RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                "RpteUpdateTnlTblInfo : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
            }
            if (TeSigAddToTeIfIdxbasedTable (pRsvpTeTnlInfo->pTeTnlInfo) ==
                RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "PATH : Err - Create IfIndexbased RBTree Failed\n");
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RpteUpdateTnlTblInfo : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            u1HlspFlag = TRUE;
        }
    }
    else
    {
        if (pRsvpTeTnlInfo->pPsb->LspTnlIfId.u4RouterId != RPTE_ZERO)
        {
            RpteDeleteFATeLink (pRsvpTeTnlInfo);

            pRsvpTeTnlInfo->pPsb->LspTnlIfId.u4RouterId = RPTE_ZERO;
            pRsvpTeTnlInfo->pPsb->LspTnlIfId.u4InterfaceId = RPTE_ZERO;

            if ((pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode ==
                 TE_TNL_MODE_UNIDIRECTIONAL) &&
                (RptePortCfaDeleteMplsTnlIf (pRsvpTeTnlInfo, PATH_MSG) ==
                 RPTE_FAILURE))
            {
                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "RESV: Mpls Tunnel Interface Deletion failed\n");
                RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                            "RptePhRhProcessPathMsg : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            u1HlspFlag = TRUE;
        }
    }

    if (u1HlspFlag == TRUE)
    {
        if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS)
        {
            *pu1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
        }
        else
        {
            *pu1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
        }
    }

    RptePhMapWorkAndProtTnls (pPktMap, pRsvpTeTnlInfo, pu1PathRefreshNeeded);
    OsixGetSysTime ((tOsixSysTime *) &
                    (PSB_LAST_CHANGE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))));
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteUpdateTnlTblInfo : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteCreateTnlTblPsb
 * Description     : This function creates PSB in pRsvpTeTnlInfo. 
 * Input (s)       : pRsvpTeTnlInfo - Pointer to pRsvpTeTnlInfo
 *                   pPktMap - Pointer to PktMap
 * Output (s)      : None
 * Returns         : RPTE_SUCCESS on success or RPTE_FAILURE on failure
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteCreateTnlTblPsb (tRsvpTeTnlInfo * pRsvpTeTnlInfo, tPktMap * pPktMap)
{
    /* Copy/Update the pRsvpTeTnlInfo from pPktMap structure */
    tIfEntry           *pIfEntry = NULL;
    tObjHdr            *pObjHdr = NULL;
    tRsvpTeRSVPTrfcParams RsvpTeTrfcParms;
    tRsvpTeTrfcParams   TeTrfcParms;
    tRsvpTeTrfcParams  *pTeTrfcParms = NULL;
    tPsb               *pPsb = NULL;
    UINT4               u4HopAddr = RPTE_ZERO;
    UINT4               u4RemoteId = RPTE_ZERO;
    FLT4                f4TmpVar = RPTE_ZERO;
    UINT2               u2Cnt = RPTE_ZERO;
    UINT2               u2Len = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCreateTnlTblPsb : ENTRY \n");
    MEMSET (&RsvpTeTrfcParms, RSVPTE_ZERO, sizeof (tRsvpTeRSVPTrfcParams));
    MEMSET (&TeTrfcParms, RSVPTE_ZERO, sizeof (tRsvpTeTrfcParams));

    pIfEntry = PKT_MAP_IF_ENTRY (pPktMap);

    /* Get memory for this PSB */
    pPsb = RpteUtlCreatePsb ();

    if (pPsb == NULL)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MISC, " Psb does not exist \n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateTnlTblPsb : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    RSVPTE_TNL_PSB (pRsvpTeTnlInfo) = pPsb;
    pPsb->pRsvpTeTnlInfo = pRsvpTeTnlInfo;

    if (pPktMap->pRsvpHopObj != NULL)
    {
        pRsvpTeTnlInfo->pPsb->RsvpHop.u4HopAddr = u4HopAddr =
            OSIX_NTOHL (pPktMap->pRsvpHopObj->RsvpHop.u4HopAddr);
        pRsvpTeTnlInfo->pPsb->RsvpHop.u4Lih
            = pPktMap->pRsvpHopObj->RsvpHop.u4Lih;
    }
    else if (pPktMap->pIfIdRsvpHopObj != NULL)
    {
        pRsvpTeTnlInfo->pPsb->RsvpHop.u4HopAddr =
            OSIX_NTOHL (pPktMap->pIfIdRsvpHopObj->RsvpHop.u4HopAddr);
        pRsvpTeTnlInfo->pPsb->RsvpHop.u4Lih
            = pPktMap->pIfIdRsvpHopObj->RsvpHop.u4Lih;

        pRsvpTeTnlInfo->pPsb->RsvpTlvObj.u4HopAddr = u4HopAddr =
            OSIX_NTOHL (pPktMap->pIfIdRsvpHopObj->RsvpTlvObjHdr.u4HopAddr);
        pRsvpTeTnlInfo->pPsb->RsvpTlvObj.u4InterfaceId = u4RemoteId =
            OSIX_NTOHL (pPktMap->pIfIdRsvpHopObj->RsvpTlvObjHdr.u4InterfaceId);
    }
    else if (pPktMap->pIfIdRsvpNumHopObj != NULL)
    {
        pRsvpTeTnlInfo->pPsb->RsvpHop.u4HopAddr
            = OSIX_NTOHL (pPktMap->pIfIdRsvpNumHopObj->RsvpHop.u4HopAddr);
        pRsvpTeTnlInfo->pPsb->RsvpHop.u4Lih
            = pPktMap->pIfIdRsvpNumHopObj->RsvpHop.u4Lih;

        pRsvpTeTnlInfo->pPsb->RsvpNumTlvObj.u4HopAddr = u4HopAddr =
            OSIX_NTOHL (pPktMap->pIfIdRsvpNumHopObj->RsvpTlvNumObjHdr.
                        u4HopAddr);
    }

    RptePhCheckAndUpdateOobInfo (pRsvpTeTnlInfo, &pIfEntry, u4RemoteId,
                                 RPTE_ZERO, u4HopAddr, FALSE, NULL);
    PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = (tIfEntry *) pIfEntry;

    /* Copy Sender Template Obj */
    MEMCPY (&PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).TnlSndrAddr,
            (&PKT_MAP_RPTE_SNDR_TMP_OBJ_SNDR_ADDR (pPktMap)),
            RSVPTE_IPV4ADR_LEN);
    PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).u2Rsvd =
        OSIX_NTOHS (PKT_MAP_RPTE_SNDR_TMP_RSVD (pPktMap));
    PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).u2LspId =
        OSIX_NTOHS (PKT_MAP_SNDR_TMP_LSP_ID (pPktMap));

    if (PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap) != NULL)
    {
        MEMCPY (&PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                &PSB_RPTE_FRR_FAST_REROUTE (PKT_MAP_RPTE_FAST_REROUTE_OBJ
                                            (pPktMap)),
                sizeof (tRsvpTeFastReroute));
        PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).RAParam.
            u4IncAnyAttr =
            OSIX_NTOHL (PSB_RPTE_FRR_FAST_REROUTE
                        (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).RAParam.u4IncAnyAttr);
        PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).RAParam.
            u4ExAnyAttr =
            OSIX_NTOHL (PSB_RPTE_FRR_FAST_REROUTE
                        (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).RAParam.u4ExAnyAttr);
        PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).RAParam.
            u4IncAllAttr =
            OSIX_NTOHL (PSB_RPTE_FRR_FAST_REROUTE
                        (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).RAParam.u4IncAllAttr);

    }
    if (FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap)).u2Length !=
        RPTE_ZERO)
    {
        pObjHdr = &FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap));
        u2Len = OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr));
        /* No of PLRs and Avoid Node Id pairs */
        u2Len = (UINT2) (u2Len - sizeof (tObjHdr));
        u2Len = (UINT2) (u2Len / ((UINT2) (RPTE_TWO * RSVPTE_IPV4ADR_LEN)));
        if (u2Len > RPTE_MAX_DETOUR_IDS)
        {
            return RPTE_FAILURE;
        }
        for (u2Cnt = RPTE_ZERO; u2Cnt < u2Len; u2Cnt++)
        {
            PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                au4PlrId[u2Cnt] =
                OSIX_NTOHL (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap).RsvpTeDetour.
                            au4PlrId[u2Cnt]);
            PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                au4AvoidNodeId[u2Cnt] =
                OSIX_NTOHL (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap).RsvpTeDetour.
                            au4AvoidNodeId[u2Cnt]);
        }
    }

    /* Copy COS Sender TSpec Obj */
    if (PKT_MAP_SENDER_TSPEC_OBJ (pPktMap) != NULL)
    {
        pObjHdr = &SENDER_TSPEC_OBJ_HDR (PKT_MAP_SENDER_TSPEC_OBJ (pPktMap));

        /* Update Psb and Tnl Info SndrTspec with same value */
        MEMCPY (&PSB_SENDER_TSPEC_OBJ (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                PKT_MAP_SENDER_TSPEC_OBJ (pPktMap),
                OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)));

        if (pRsvpTeTnlInfo->u1IsGrTnl != RPTE_YES)
        {
            /*Copy the traffic parameters to the TeTnlInfo Structure */

            RPTE_RSVPTE_TRFC_PARAMS ((&TeTrfcParms)) = &RsvpTeTrfcParms;

            f4TmpVar = OSIX_NTOHF (PKT_MAP_TSPEC_BKT_RATE (pPktMap));
            RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
            RPTE_TPARAM_TBR ((&TeTrfcParms)) = (UINT4) f4TmpVar;

            f4TmpVar = OSIX_NTOHF (PKT_MAP_TSPEC_BKT_SIZE (pPktMap));
            RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
            RPTE_TPARAM_TBS ((&TeTrfcParms)) = (UINT4) f4TmpVar;

            f4TmpVar = OSIX_NTOHF (PKT_MAP_TSPEC_PEAK_RATE (pPktMap));
            RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
            RPTE_TPARAM_PDR ((&TeTrfcParms)) = (UINT4) f4TmpVar;

            RPTE_TPARAM_MPU ((&TeTrfcParms)) =
                OSIX_NTOHL ((UINT4) PKT_MAP_TSPEC_MIN_SIZE (pPktMap));
            RPTE_TPARAM_MPS ((&TeTrfcParms)) =
                OSIX_NTOHL ((UINT4) PKT_MAP_TSPEC_MAX_SIZE (pPktMap));

            RPTE_TRFC_PARAM_ROLE ((&TeTrfcParms)) = RPTE_INTERMEDIATE;
            RPTE_TRFC_PARAM_NUM_TNLS ((&TeTrfcParms)) = RSVPTE_ONE;
            RPTE_TNLRSRC_ROW_STATUS ((&TeTrfcParms)) = RPTE_ACTIVE;

            if (rpteTeCreateTrfcParams (RPTE_PROT, &pTeTrfcParms,
                                        &TeTrfcParms) == RPTE_TE_FAILURE)
            {
                RpteUtlDeletePsb (pPsb);
                return RPTE_FAILURE;
            }
            RPTE_TNL_TRFC_PARMS (pRsvpTeTnlInfo) = pTeTrfcParms;
            RPTE_TNL_TRFC_PARM_INDEX (pRsvpTeTnlInfo) =
                RPTE_TRFC_PARAM_INDEX (pTeTrfcParms);
        }
    }
    else
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "No TSPEC is present in the Path message - msg dropped "
                    ": INTMD-EXIT \n");
        RpteUtlDeletePsb (pPsb);

        return RPTE_FAILURE;
    }

    /* Copy Adspec Obj into the Tnl PSB, if present */
    if (PKT_MAP_ADSPEC_OBJ (pPktMap) != NULL)
    {
        pObjHdr = &pPktMap->pAdSpecObj->ObjHdr;
        /* Copy only the ADSPEC information, 
         * Exclude message header (4 bytes) and 
         * control load service spec. (4 bytes) */
        MEMCPY (&(PSB_ADSPEC (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))),
                &(pPktMap->pAdSpecObj->AdSpec),
                ((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr))) - RSVPTE_EIGHT));
    }

    if (pPktMap->pNotifyRequestObj != NULL)
    {
        pRsvpTeTnlInfo->pPsb->u4NotifyAddr =
            OSIX_NTOHL (pPktMap->pNotifyRequestObj->u4NotifyNodeAddr);
        pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4SendPathNotifyRecipient =
            pRsvpTeTnlInfo->pPsb->u4NotifyAddr;
    }

    OsixGetSysTime ((tOsixSysTime *) &
                    (PSB_LAST_CHANGE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))));

    PSB_RPTE_TNL_INFO (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = pRsvpTeTnlInfo;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCreateTnlTblPsb : EXIT \n");

    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteCreateTnlTblInfo
 * Description     : This function creates pRsvpTeTnlInfo. 
 * Input (s)       : pRsvpTeTnlInfo - Pointer to pRsvpTeTnlInfo
 *                   pPktMap - Pointer to PktMap
 * Output (s)      : None
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE.
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteCreateTnlTblInfo (tRsvpTeTnlInfo * pRsvpTeTnlInfo, tPktMap * pPktMap)
{
    tRsvpTeArHopListInfo *pRsvpTeArHopListInfo = NULL;
    tRsvpTePathListInfo *pInPathListInfo = NULL;
    tRsvpTePathListInfo *pOutPathListInfo = NULL;
    UINT4               u4TunnelIndex = RPTE_ZERO;
    tRsvpTeTnlInfo     *pRsvpTeWorkTnlInfo = NULL;
    UINT4               u4TunnelInstance = RPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCreateTnlTblInfo : ENTRY \n");

    /* For Gr tunnels, updation of teTnlInfo is not needed. only RsvpTnlInfo
     * related updation is required */

    if (pRsvpTeTnlInfo->u1IsGrTnl == RPTE_YES)
    {
        if ((pPktMap->pGenLblReqObj != NULL) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1EncodingType ==
             RPTE_ZERO))
        {
            RSVPTE_TNL_L3PID (pRsvpTeTnlInfo) =
                OSIX_NTOHS (pPktMap->pGenLblReqObj->GenLblReq.u2GPid);
        }
        else if (pPktMap->pLblReqAtmRngObj != NULL)
        {
            RSVPTE_TNL_L3PID (pRsvpTeTnlInfo) =
                OSIX_NTOHS (pPktMap->pLblReqAtmRngObj->GenLblReq.u2GPid);
        }
        MEMCPY ((UINT1 *) &(RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo)),
                (UINT1 *) (&PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap)),
                RSVPTE_IPV4ADR_LEN);
        if (pPktMap->pGenLblReqObj != NULL)
        {
            MEMCPY (&RSVPTE_TNL_LBL_REQ_OBJ (pRsvpTeTnlInfo),
                    &PKT_MAP_LBL_REQ_OBJ (pPktMap), sizeof (tGenLblReq));
        }
        else if (pPktMap->pLblReqAtmRngObj != NULL)
        {
            MEMCPY (&RSVPTE_TNL_LBL_REQ_OBJ (pRsvpTeTnlInfo),
                    &PKT_MAP_LBL_REQ_ATM_RNG (pPktMap), sizeof (tGenLblReq));
            MEMCPY (&RSVPTE_TNL_LBL_REQ_ATM_RNG (pRsvpTeTnlInfo),
                    &PKT_MAP_LBL_REQ_ATM_RNG_ENTRY (pPktMap),
                    sizeof (tAtmRsvpLblRngEntry));
        }
        return RPTE_TE_SUCCESS;
    }

    /* Copy/Update the pRsvpTeTnlInfo from pPktMap structure */
    /* update L3Pid based on ATM/Ethernet */

    if (pPktMap->pGenLblReqObj != NULL)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1EncodingType =
            pPktMap->pGenLblReqObj->GenLblReq.u1EncType;
        pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1SwitchingType =
            pPktMap->pGenLblReqObj->GenLblReq.u1SwitchingType;
        if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1EncodingType
            != RPTE_ZERO)
        {
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType = TE_TNL_TYPE_GMPLS;
            pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u2Gpid =
                OSIX_NTOHS (pPktMap->pGenLblReqObj->GenLblReq.u2GPid);
            if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u2Gpid
                == RPTE_GMPLS_ETHERNET_ETHERTYPE)
            {
                pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u2Gpid
                    = GMPLS_GPID_ETHERNET;
            }
        }
        else
        {
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType = TE_TNL_TYPE_MPLS;
            RSVPTE_TNL_L3PID (pRsvpTeTnlInfo) =
                OSIX_NTOHS (pPktMap->pGenLblReqObj->GenLblReq.u2GPid);
        }

    }
    else if (pPktMap->pLblReqAtmRngObj != NULL)
    {
        RSVPTE_TNL_L3PID (pRsvpTeTnlInfo) =
            OSIX_NTOHS (pPktMap->pLblReqAtmRngObj->GenLblReq.u2GPid);
    }

    if (pPktMap->pUpStrLblObj != NULL)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode = GMPLS_BIDIRECTION;

        /* Copy the received upstream label to the outgoing upstream label */
        MEMCPY (&pRsvpTeTnlInfo->UpStrOutLbl, &pPktMap->pUpStrLblObj->Label,
                sizeof (uLabel));
    }
    else
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode = GMPLS_FORWARD;
        pRsvpTeTnlInfo->pTeTnlInfo->u1RevArpResolveStatus = MPLS_ARP_RESOLVED;
    }

    if (OSIX_NTOHS (PKT_MAP_RPTE_SSN_ATTR_HCNTYPE (pPktMap)) ==
        RPTE_SSN_ATTR_CLASS_NUM_TYPE)
    {
        RSVPTE_TNL_SETUP_PRIO (pRsvpTeTnlInfo) =
            PKT_MAP_RPTE_SSN_ATTR_SPRIO (pPktMap);
        RSVPTE_TNL_HLDNG_PRIO (pRsvpTeTnlInfo) =
            PKT_MAP_RPTE_SSN_ATTR_HPRIO (pPktMap);

        MEMCPY ((UINT1 *) &RSVPTE_TNL_NAME (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                (UINT1 *) &PKT_MAP_RPTE_SSN_ATTR_SNAME (pPktMap),
                STRLEN ((INT1 *) PKT_MAP_RPTE_SSN_ATTR_SNAME (pPktMap)));

        /* if Setup priority is > Holding priority, generate log msg */
        if (RSVPTE_TNL_SETUP_PRIO (pRsvpTeTnlInfo) >
            RSVPTE_TNL_HLDNG_PRIO (pRsvpTeTnlInfo))
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Session Setup priority higher than the "
                        "Holding priority..!\n");
        }
    }
    else if (OSIX_NTOHS (PKT_MAP_RPTE_RA_SSN_ATTR_HCNTYPE (pPktMap)) ==
             RPTE_RA_SSN_ATTR_CLASS_NUM_TYPE)
    {
        RSVPTE_EXC_ANY_AFFINITY (pRsvpTeTnlInfo) =
            PKT_MAP_RPTE_RA_SSN_ATTR_EX_ANY_ATTR (pPktMap);
        RSVPTE_INC_ANY_AFFINITY (pRsvpTeTnlInfo) =
            PKT_MAP_RPTE_RA_SSN_ATTR_INC_ANY_ATTR (pPktMap);
        RSVPTE_INC_ALL_AFFINITY (pRsvpTeTnlInfo) =
            PKT_MAP_RPTE_RA_SSN_ATTR_INC_ALL_ATTR (pPktMap);
        RSVPTE_TNL_SETUP_PRIO (pRsvpTeTnlInfo) =
            PKT_MAP_RPTE_RA_SSN_ATTR_SPRIO (pPktMap);
        RSVPTE_TNL_HLDNG_PRIO (pRsvpTeTnlInfo) =
            PKT_MAP_RPTE_RA_SSN_ATTR_HPRIO (pPktMap);
        MEMCPY ((UINT1 *) &RSVPTE_TNL_NAME (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                (UINT1 *) &PKT_MAP_RPTE_RA_SSN_ATTR_SNAME (pPktMap),
                STRLEN ((INT1 *) PKT_MAP_RPTE_RA_SSN_ATTR_SNAME (pPktMap)));
        /* if Setup priority is > Holding priority, generate log msg */
        if (RSVPTE_TNL_SETUP_PRIO (pRsvpTeTnlInfo) >
            RSVPTE_TNL_HLDNG_PRIO (pRsvpTeTnlInfo))
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Session Setup priority higher than the "
                        "Holding priority..!\n");
        }
    }
    MEMCPY ((UINT1 *) &(RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo)),
            (UINT1 *) (&PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap)),
            RSVPTE_IPV4ADR_LEN);
    if (pPktMap->pGenLblReqObj != NULL)
    {
        MEMCPY (&RSVPTE_TNL_LBL_REQ_OBJ (pRsvpTeTnlInfo),
                &PKT_MAP_LBL_REQ_OBJ (pPktMap), sizeof (tGenLblReq));
    }
    else if (pPktMap->pLblReqAtmRngObj != NULL)
    {
        MEMCPY (&RSVPTE_TNL_LBL_REQ_OBJ (pRsvpTeTnlInfo),
                &PKT_MAP_LBL_REQ_ATM_RNG (pPktMap), sizeof (tGenLblReq));
        MEMCPY (&RSVPTE_TNL_LBL_REQ_ATM_RNG (pRsvpTeTnlInfo),
                &PKT_MAP_LBL_REQ_ATM_RNG_ENTRY (pPktMap),
                sizeof (tAtmRsvpLblRngEntry));
    }
    if (pPktMap->pLabelSetObj != NULL)
    {
        if (LblMgrAvailableLblInLblGroup (RSVPTE_LBL_MODULE_ID,
                                          PER_PLATFORM_INTERFACE_INDEX,
                                          OSIX_NTOHL (pPktMap->
                                                      pLabelSetObj->
                                                      LabelSet.
                                                      u4SubChannel1)) !=
            RPTE_LBL_MNGR_SUCCESS)
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "Invalid Label Range in Label Set Object"
                        " - Sending PATH ERROR\n");
            RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB, RPTE_LABEL_SET);
            return RPTE_FAILURE;
        }
        pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl =
            OSIX_NTOHL (pPktMap->pLabelSetObj->LabelSet.u4SubChannel1);
        if (LblMgrAssignLblToLblGroup (RSVPTE_LBL_MODULE_ID, RSVPTE_ZERO,
                                       pRsvpTeTnlInfo->DownstrInLbl.u4GenLbl)
            == RPTE_LBL_MNGR_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "PATH : Assigning label to label group Failed\n");
            return RPTE_FAILURE;
        }
    }
    /* Copy the admin status flag value */
    if ((pPktMap->pAdminStatusObj != NULL) &&
        (gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable != RPTE_ENABLED))
    {
        pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus
            = OSIX_NTOHL (pPktMap->pAdminStatusObj->AdminStatus.u4AdminStatus);
    }

    /* If ERO list exists copy SLL Structure */
    if (TMO_SLL_Count (&pPktMap->ErObj.ErHopList) != RSVPTE_ZERO)
    {
        /* Copy Incoming Pkt Map ErHopList into Tnl Info OutErHopList */
        if (rpteTeCreateHopListInfo (&pInPathListInfo,
                                     &pPktMap->ErObj.ErHopList) ==
            RPTE_TE_SUCCESS)
        {
            RSVPTE_TNL_INERHOP_LIST_INFO (pRsvpTeTnlInfo) = pInPathListInfo;
            RSVPTE_TNL_IN_PATH_OPTION (pRsvpTeTnlInfo) =
                RSVPTE_TNL_FIRST_PATH_OPTION (pInPathListInfo);
        }
        else
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCreateTnlTblInfo : INTMD - "
                        "EXIT \n");
            return RPTE_FAILURE;
        }
        if (rpteTeCreateHopListInfo (&pOutPathListInfo,
                                     &pPktMap->ErObj.ErHopList) ==
            RPTE_TE_SUCCESS)
        {
            RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) = pOutPathListInfo;
            RSVPTE_TNL_PATH_OPTION (pRsvpTeTnlInfo) =
                RSVPTE_TNL_FIRST_PATH_OPTION (pOutPathListInfo);
            RSVPTE_HOP_ROLE (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo)) =
                RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo);
            RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlHopTableIndex =
                pOutPathListInfo->u4HopListIndex;
            if (RSVPTE_TNL_PATH_OPTION (pRsvpTeTnlInfo) != NULL)
            {
                RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlPathInUse =
                    RSVPTE_TNL_PATH_OPTION (pRsvpTeTnlInfo)->u4PathOptionIndex;
            }
        }
        else
        {
            rpteTeDeleteHopListInfo (pInPathListInfo);
            RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCreateTnlTblInfo : INTMD - "
                        "EXIT \n");
            return RPTE_FAILURE;
        }
    }
    /* If RRO list exists copy SLL Structure */
    if (TMO_SLL_Count (&pPktMap->RrObj.RrHopList) != RSVPTE_ZERO)
    {
        if (rpteTeCreateArHopListInfo (&pRsvpTeArHopListInfo,
                                       &pPktMap->RrObj.RrHopList)
            == RPTE_TE_SUCCESS)
        {
            RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo) = pRsvpTeArHopListInfo;
        }
        else
        {
            rpteTeDeleteHopListInfo (pInPathListInfo);
            rpteTeDeleteHopListInfo (pOutPathListInfo);
            RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCreateTnlTblInfo : INTMD - "
                        "EXIT \n");
            return RPTE_FAILURE;
        }

        RSVPTE_TNL_SSN_ATTR (RPTE_TE_TNL (pRsvpTeTnlInfo))
            |= RSVPTE_SSN_REC_ROUTE_BIT;
    }

    if ((pPktMap->pRsvpTeFastRerouteObj != NULL) ||
        (PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap) &
         RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG) ||
        (PKT_MAP_RPTE_RA_SSN_ATTR_FLAGS (pPktMap) &
         RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG))
    {
        RSVPTE_TNL_SSN_ATTR (RPTE_TE_TNL (pRsvpTeTnlInfo))
            |= RSVPTE_SSN_FAST_REROUT_BIT;
    }

    if ((PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap)
         & RPTE_FRR_SSN_ATTR_LBL_RECORD_FLAG) ||
        (PKT_MAP_RPTE_RA_SSN_ATTR_FLAGS (pPktMap)
         & RPTE_FRR_SSN_ATTR_LBL_RECORD_FLAG))
    {
        pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1Attributes
            |= RSVPTE_SSN_LBL_RECORD_BIT;
        pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1AttrLen = sizeof (UINT1);
    }

    if ((PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap)
         & RPTE_FRR_SSN_ATTR_SE_STYLE_FLAG) ||
        (PKT_MAP_RPTE_RA_SSN_ATTR_FLAGS (pPktMap)
         & RPTE_FRR_SSN_ATTR_SE_STYLE_FLAG))
    {
        pRsvpTeTnlInfo->pTeTnlInfo->bIsSEStyleDesired = TRUE;
    }

    if ((PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap)
         & RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG) ||
        (PKT_MAP_RPTE_RA_SSN_ATTR_FLAGS (pPktMap)
         & RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG))
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlProtType = TE_TNL_FRR_PROT_NODE;
    }

    if ((PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap)
         & RPTE_FRR_SSN_ATTR_BANDWIDTH_FLAG) ||
        (PKT_MAP_RPTE_RA_SSN_ATTR_FLAGS (pPktMap)
         & RPTE_FRR_SSN_ATTR_BANDWIDTH_FLAG))
    {
        pRsvpTeTnlInfo->pTeTnlInfo->bIsBwProtDesired = TRUE;
    }
    if ((pRsvpTeTnlInfo->pTeTnlInfo->bIsSEStyleDesired == RPTE_TRUE) &&
        (RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) != RPTE_TRUE))
    {
        u4TunnelIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex;
        CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID
                            (pRsvpTeTnlInfo->pTeTnlInfo), u4TunnelIngressLSRId);
        u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
        CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID
                            (pRsvpTeTnlInfo->pTeTnlInfo), u4TunnelEgressLSRId);
        u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);

        if ((RpteGetTnlInTnlTable (gpRsvpTeGblInfo, u4TunnelIndex,
                                   u4TunnelInstance, u4TunnelIngressLSRId,
                                   u4TunnelEgressLSRId, &pRsvpTeWorkTnlInfo)
             == RPTE_SUCCESS) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance ==
             pRsvpTeWorkTnlInfo->pTeTnlInfo->u4TnlInstance))
        {
            pRsvpTeWorkTnlInfo = NULL;
        }
    }

    if (pRsvpTeWorkTnlInfo != NULL)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRerouteFlag = TRUE;
        pRsvpTeTnlInfo->pMapTnlInfo = pRsvpTeWorkTnlInfo;
        pRsvpTeWorkTnlInfo->pMapTnlInfo = pRsvpTeTnlInfo;
        pRsvpTeWorkTnlInfo->pTeTnlInfo->u4BkpTnlIndex = u4TunnelIndex;
        pRsvpTeWorkTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance;
        MEMCPY (pRsvpTeWorkTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                &RSVPTE_TNL_INGRESS_RTR_ID (pRsvpTeTnlInfo->pTeTnlInfo),
                ROUTER_ID_LENGTH);
        MEMCPY (pRsvpTeWorkTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                &RSVPTE_TNL_EGRESS_RTR_ID (pRsvpTeTnlInfo->pTeTnlInfo),
                ROUTER_ID_LENGTH);
    }

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCreateTnlTblInfo : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteIsLoopFound
 * Description     : This function processes RRO for Loop Detection.
 * Input (s)       : pPktMap - Pointer to pPktMap
 * Output (s)      : None
 * Returns         : If successful returns RPTE_TRUE or else RPTE_FALSE
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteIsLoopFound (tPktMap * pPktMap)
{
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4TmpIpAddr = RSVPTE_ZERO;
    UINT4               u4TempIpAddr = RPTE_LOOP_BACK_ADDR;
    UINT4               u4HashIndex = 0;
    tRsvpTeArHopInfo   *pRsvpTeArHopInfo = NULL;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLoopFound : ENTRY \n");

    /* Getting the Local Lsr Id */
    MEMCPY (&u4TmpIpAddr,
            (UINT1 *) (&(gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId)),
            RPTE_LSR_ID_LEN);

    /* Sll scan for RRO List */
    TMO_SLL_Scan (&pPktMap->RrObj.RrHopList,
                  pRsvpTeArHopInfo, tRsvpTeArHopInfo *)
    {
        if (MEM_CMP (&u4TmpIpAddr,
                     (UINT1 *) &(pRsvpTeArHopInfo->IpAddr),
                     RPTE_LSR_ID_LEN) == RSVPTE_ZERO)
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RpteIsLoopFound : (Yes) INTMD-EXIT \n");
            return RPTE_TRUE;
        }

        TMO_HASH_Scan_Table (RSVP_GBL_IF_HSH_TBL, u4HashIndex)
        {
            TMO_HASH_Scan_Bucket (RSVP_GBL_IF_HSH_TBL, u4HashIndex, pIfEntry,
                                  tIfEntry *)
            {
                if (MEM_CMP (&IF_ENTRY_ADDR (pIfEntry),
                             (UINT1 *) &(pRsvpTeArHopInfo->IpAddr),
                             RPTE_LSR_ID_LEN) == RSVPTE_ZERO)
                {
                    RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                "RpteIsLoopFound : (Yes) INTMD-EXIT \n");
                    return RPTE_TRUE;
                }
            }
        }

        if (MEM_CMP (&u4TempIpAddr,
                     (UINT1 *) &(pRsvpTeArHopInfo->IpAddr),
                     RPTE_LSR_ID_LEN) == RSVPTE_ZERO)
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RpteIsLoopFound : (Yes) INTMD-EXIT \n");
            return RPTE_ILLEGAL;
        }
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLoopFound : EXIT \n");
    return RPTE_FALSE;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteCompERO
 * Description     : This function Compares EROs.
 * Input (s)       : pPktMapEROList - Pointer to Rcvd ERO list.
 *                 : pTnlEROList - Pointer to stored ERO list in Tnl.
 * Output (s)      : None
 * Returns         : If successful returns RPTE_EQUAL or else RPTE_NOT_EQUAL
 */
/*---------------------------------------------------------------------------*/
INT1
RpteCompERO (const tTMO_SLL * pPktMapEROList, tTMO_SLL * pTnlEROList)
{
    tRsvpTeHopInfo     *pPktMapErHopInfo = NULL;
    tRsvpTeHopInfo     *pTnlErHopInfo = NULL;
    UINT1               u1Count = (UINT1) TMO_SLL_Count (pPktMapEROList);

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCompERO : ENTRY \n");
    /* Sll scan for ERO List */
    for (pPktMapErHopInfo =
         (tRsvpTeHopInfo *) (TMO_SLL_First ((pPktMapEROList))),
         pTnlErHopInfo = (tRsvpTeHopInfo *) (TMO_SLL_First ((pTnlEROList)));
         (((pPktMapErHopInfo != NULL) && (pTnlErHopInfo != NULL))
          && (u1Count != RSVPTE_ZERO));
         --u1Count, pPktMapErHopInfo =
         (tRsvpTeHopInfo *) TMO_SLL_Next ((pPktMapEROList),
                                          (&(pPktMapErHopInfo->NextHop))),
         pTnlErHopInfo =
         (tRsvpTeHopInfo *) TMO_SLL_Next ((pTnlEROList),
                                          (&(pTnlErHopInfo->NextHop))))
    {
        /* 
         * Compare the EROs, if not matches return NOT_EQUAL else return EQUAL. 
         */
        if ((MEM_CMP ((UINT1 *) &pPktMapErHopInfo->IpAddr,
                      (UINT1 *) &pTnlErHopInfo->IpAddr,
                      RSVPTE_IPV4ADR_LEN) == RSVPTE_ZERO)
            && (pPktMapErHopInfo->u1HopType == pTnlErHopInfo->u1HopType)
            && (RSVPTE_ERHOP_ADDR_PRFX_LEN (pPktMapErHopInfo) ==
                RSVPTE_ERHOP_ADDR_PRFX_LEN (pTnlErHopInfo)))
        {
            continue;
        }
        else
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Rx ERO doesn't matches with stored ERO\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCompERO : EXIT \n");
            return RPTE_NOT_EQUAL;
        }
    }                            /* for */
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCompERO : EXIT \n");
    return RPTE_EQUAL;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteCompRRO
 * Description     : This function Compares RROs.
 * Input (s)       : pPktMapRROList - Pointer to Rcvd RRO list.
 *                 : pTnlRROList - Pointer to stored RRO list in Tnl.
 * Output (s)      : None
 * Returns         : If successful returns RPTE_EQUAL or else RPTE_NOT_EQUAL
 */
/*---------------------------------------------------------------------------*/
INT1
RpteCompRRO (const tTMO_SLL * pPktMapRROList, tTMO_SLL * pTnlRROList)
{
    tRsvpTeArHopInfo   *pPktMapArHopInfo = NULL;
    tRsvpTeArHopInfo   *pTnlArHopInfo = NULL;
    UINT1               u1Count = (UINT1) TMO_SLL_Count (pPktMapRROList);

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCompRRO : ENTRY \n");

    /* Sll scan for RRO List */
    for (pPktMapArHopInfo =
         (tRsvpTeArHopInfo *) (TMO_SLL_First ((pPktMapRROList))),
         pTnlArHopInfo = (tRsvpTeArHopInfo *) (TMO_SLL_First ((pTnlRROList)));
         (((pPktMapArHopInfo != NULL) && (pTnlArHopInfo != NULL))
          && (u1Count != RSVPTE_ZERO));
         --u1Count, pPktMapArHopInfo =
         (tRsvpTeArHopInfo *) TMO_SLL_Next ((pPktMapRROList),
                                            (&(pPktMapArHopInfo->NextHop))),
         pTnlArHopInfo =
         (tRsvpTeArHopInfo *) TMO_SLL_Next ((pTnlRROList),
                                            (&(pTnlArHopInfo->NextHop))))
    {
        /* 
         * Compare the RRO Ip Addr, if not matches return NOT_EQUAL 
         * else return EQUAL. 
         */
        if ((MEM_CMP ((UINT1 *) &RSVPTE_TE_ARHOP_IP_ADDR (pPktMapArHopInfo),
                      (UINT1 *) &RSVPTE_TE_ARHOP_IP_ADDR (pTnlArHopInfo),
                      RSVPTE_IPV4ADR_LEN) == RSVPTE_ZERO)
            && (pPktMapArHopInfo->u1Flags == pTnlArHopInfo->u1Flags))
        {
            continue;
        }
        else
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Rx RRO doesn't matches with stored RRO\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCompRRO : EXIT \n");
            return RPTE_NOT_EQUAL;
        }
    }                            /* for */
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteCompRRO : EXIT \n");
    return RPTE_EQUAL;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhStartRROBackOffTimer
 * Description     : This function starts the RRO Back off Timer
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhStartRROBackOffTimer (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTimerParam        *pTmrParam = NULL;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartRROBackOffTimer : ENTRY \n");
    /*
     * Starting a timer with some back-off time.
     * The Timer is started to ensure that the Sender responds to the
     * PathErr Msg Sent, within certain time.
     * If no response from Sender, then PathErr is again Tx. This msg
     * ReTx happens till, either When Sender responds with an PathMsg
     * or When back-off time crosses threshold value.
     */
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartRROBackOffTimer : ENTRY \n");

    pTmrParam = (tTimerParam *) & RPTE_TNL_RRO_TIMER (pRsvpTeTnlInfo);

    /* 
     * Note : We are calling this TmrStop to stop the timer when
     * normal Path refresh invokes this routine
     */
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_TNL_RRO_TIMER (pRsvpTeTnlInfo));

    /* 
     * NOTE: Compare PSB Refresh Interval with the default back off timer 
     * value, if PSB Refresh Interval is lesser than back off time update 
     * back off Time with default back off time, if not update with PSB
     * Refresh Interval time value.
     */

    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartRROBackOffTimer: "
                    "INTMD - EXIT  \n");
        return;
    }
    if (pRsvpTeTnlInfo->u2BTime == RSVPTE_ZERO)
    {

        if (((pRsvpTeTnlInfo->pPsb->u4RefreshInterval) /
             DEFAULT_CONV_TO_SECONDS) < RPTE_DEF_BACK_OFF_TIME)
        {
            pRsvpTeTnlInfo->u2BTime = RPTE_DEF_BACK_OFF_TIME;
        }
        else
        {
            pRsvpTeTnlInfo->u2BTime =
                (UINT2) ((pRsvpTeTnlInfo->pPsb->u4RefreshInterval) /
                         DEFAULT_CONV_TO_SECONDS);
        }
        /* Update Max Retrasmit Time out limit */
        pRsvpTeTnlInfo->u4MRTime =
            (UINT4) (RPTE_MAX_RETRY * pRsvpTeTnlInfo->u2BTime);
    }
    else
    {
        /* Calculate subsequent back off time value */
        pRsvpTeTnlInfo->u2BTime = (UINT2) (pRsvpTeTnlInfo->u2BTime +
                                           (pRsvpTeTnlInfo->u4MRTime /
                                            RPTE_MAX_RETRY));
    }
    if (pRsvpTeTnlInfo->u2BTime > pRsvpTeTnlInfo->u4MRTime)
    {

        /* 
         * PathErr Msg ReTx, crossed the threshold value, so deleting the
         * ArHop List from TnlInfo, delete the expired timer from the list
         * and set the RRO Flag to prevent subsequent RRO Object reception
         * for this Tnl. 
         */

        pRsvpTeTnlInfo->u1RROErrFlag |= RPTE_RRO_ERR_PATH_SET;
        rpteTeDeleteArHopListInfo (RSVPTE_IN_ARHOP_LIST_INFO (pRsvpTeTnlInfo));
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartRROBackOffTimer : EXIT \n");
        return;
    }
    else
    {
        TIMER_PARAM_ID (pTmrParam) = RPTE_PH_RRO_BOFF_TIMER;
        TIMER_PARAM_RPTE_TNL (pTmrParam) = pRsvpTeTnlInfo;
        RSVP_TIMER_NAME (&RPTE_TNL_RRO_TIMER (pRsvpTeTnlInfo)) =
            (FS_ULONG) pTmrParam;

        if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST,
                               &RPTE_TNL_RRO_TIMER (pRsvpTeTnlInfo),
                               ((pRsvpTeTnlInfo->u2BTime) *
                                SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
        {
            /* Start Timer failed. */
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Tmr Start Timer failed in "
                        "RRO Back off Timer\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhStartRROBackOffTimer : INTMD-EXIT \n");
            return;
        }
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartRROBackOffTimer : EXIT \n");
        return;
    }
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhProcRROBackoffTimeOut
 * Description     : This function processes the expired RRO Back off Timer
 * Input (s)       : pExpiredTmr - Pointer to Expired Timer
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhProcRROBackoffTimeOut (const tTmrAppTimer * pExpiredTmr)
{
    tPktMap             RROPktMap;
    tRsvpTeSsnObj       SsnObj;
    tRsvpTeSndrTmpObj   RsvpTeSndrTmpObj;
    tSenderTspecObj     SenderTspecObj;
    tRsvpHopObj         RsvpHopObj;
    tIfEntry           *pIfEntry = NULL;
    tTimerParam        *pTmrParam = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;

    if ((pIfEntry = MemAllocMemBlk (RSVPTE_IF_ENTRY_POOL_ID)) == NULL)
    {
        return;
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcRROBackoffTimeOut : ENTRY \n");

    MEMSET (&SsnObj, RSVPTE_ZERO, sizeof (tRsvpTeSsnObj));
    MEMSET (&RsvpTeSndrTmpObj, RSVPTE_ZERO, sizeof (tRsvpTeSndrTmpObj));
    MEMSET (&SenderTspecObj, RSVPTE_ZERO, sizeof (tSenderTspecObj));
    MEMSET (&RsvpHopObj, RSVPTE_ZERO, sizeof (tRsvpHopObj));
    MEMSET (pIfEntry, RSVPTE_ZERO, sizeof (tIfEntry));
    /* Initialise RROPktMap */
    RpteUtlInitPktMap (&RROPktMap);

    PKT_MAP_RSVP_HOP_OBJ (&RROPktMap) = &RsvpHopObj;
    PKT_MAP_IF_ENTRY (&RROPktMap) = pIfEntry;
    PKT_MAP_RPTE_SSN_OBJ (&RROPktMap) = &SsnObj;
    PKT_MAP_RPTE_SND_TMP_OBJ (&RROPktMap) = &RsvpTeSndrTmpObj;
    PKT_MAP_SENDER_TSPEC_OBJ (&RROPktMap) = &SenderTspecObj;

    pTmrParam = (tTimerParam *) RSVP_TIMER_NAME (pExpiredTmr);

    pRsvpTeTnlInfo = TIMER_PARAM_RPTE_TNL (pTmrParam);

    RpteUtlPhPreparePktMapRROErr (&RROPktMap, pRsvpTeTnlInfo);
    RptePhStartRROBackOffTimer (pRsvpTeTnlInfo);
    RptePvmSendPathErr (&RROPktMap, RPTE_NOTIFY, RPTE_RRO_TOO_LARGE);
    RSVPTE_DBG (RSVPTE_PH_PRCS,
                "PATH : RRO MTU size err - Path Err sent - "
                "RRO Notification \n");
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcRROBackoffTimeOut : EXIT \n");
    MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfEntry);
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhStartPathRefreshTmr
 * Description     : This function processes the Expired Psb Refresh Timer
 * Input (s)       : pPsb - Pointer to Path State Block structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhStartPathRefreshTmr (tPsb * pPsb)
{
    tTimerParam        *pTmrParam = NULL;
    UINT4               u4Time = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartPathRefreshTmr : ENTRY \n");

    if (pPsb == NULL)
    {
        return;
    }

    pTmrParam = (tTimerParam *) & PSB_TIMER_PARAM (pPsb);

    u4Time = pPsb->u4RefreshInterval;

    if (pPsb->pRsvpTeTnlInfo->pTeTnlInfo->u1TnlOperStatus != TE_OPER_UP)
    {
        u4Time = (u4Time / 10);
    }

    TIMER_PARAM_ID (pTmrParam) = RPTE_PATH_REFRESH;
    TIMER_PARAM_PSB (pTmrParam) = pPsb;
    RSVP_TIMER_NAME (&PSB_TIMER (pPsb)) = (FS_ULONG) pTmrParam;

    if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST, &PSB_TIMER (pPsb),
                           (UINT4) (u4Time *
                                    SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) !=
        TMR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Start Timer failed. - in Refresh Timer \n");
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartPathRefreshTmr : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteStartFrrGblRevertTimer                             */
/* Description     : This function starts the Global revertive timer        */
/* Input (s)       : pRsvpTeTnlInfo -  Pointer to the RSVP TE Tnl info.     */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteStartFrrGblRevertTimer (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    /* tTimerParam        *pTmrParam = NULL; */

    /* TO-DO Global revertive is not fully tested now, 
     * we need to uncomment the below 'return' if we test all the 
     * scenarios with Global revertive enabled */
    UNUSED_PARAM (pRsvpTeTnlInfo);
    return;

    /*
       RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteStartFrrGblRevertTimer : ENTRY\n");

       if (RPTE_TE_TNL (pRsvpTeTnlInfo) == NULL)
       {
       RSVPTE_DBG (RSVPTE_RH_ETEXT,
       "RpteStartFrrGblRevertTimer : INTMD EXIT\n");
       return;
       }
       pTmrParam = (tTimerParam *) & RPTE_GBL_REVERT_TIMER_PARAM (pRsvpTeTnlInfo);

       TIMER_PARAM_ID (pTmrParam) = RPTE_MAX_GBL_REVERT_TIMER;
       TIMER_PARAM_RPTE_TNL (pTmrParam) = pRsvpTeTnlInfo;
       RSVP_TIMER_NAME (&RPTE_GBL_REVERT_TIMER (pRsvpTeTnlInfo)) =
       (UINT4) pTmrParam;
       if (RpteUtlStartTimer
       (RSVP_GBL_TIMER_LIST, &RPTE_GBL_REVERT_TIMER (pRsvpTeTnlInfo),
       (UINT4) (((RPTE_MAX_GBL_REVERT_TIME (pRsvpTeTnlInfo)) /
       DEFAULT_CONV_TO_SECONDS) *
       SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
       {
       RSVPTE_DBG (RSVPTE_RH_PRCS,
       "RESV: Starting of RpteStartFrrGblRevertTimer failed ..\n");
       }
       RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteStartFrrGblRevertTimer : EXIT\n");
       return; */
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhProcessRouteDelayTimeOut
 * Description     : This function processes the Expired Psb Refresh Timer
 * Input (s)       : pExpiredTmr - Pointer to the Expired Timer
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhProcessRouteDelayTimeOut (const tTmrAppTimer * pExpiredTmr)
{
    tTimerParam        *pTmrParam = NULL;
    tPsb               *pPsb = NULL;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessRouteDelayTimeOut : ENTRY \n");

    pTmrParam = (tTimerParam *) RSVP_TIMER_NAME (pExpiredTmr);
    pPsb = TIMER_PARAM_PSB (pTmrParam);

    RptePhPathRefresh (pPsb->pRsvpTeTnlInfo);
    /* start path refresh timer, refresh existing Path */
    RptePhStartPathRefreshTmr (pPsb);
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessRouteDelayTimeOut : EXIT \n");
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhRsrcAffVerProc            
 * Description     : This function processes the received the Resource Affinity
 *                 : Attributes with that of the Link Attributes.              
 * Input (s)       : pRsvpTeTnlInfo - Pointer to the RsvpTeTnlInfo
 * Output (s)      : None
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
INT1
RptePhRsrcAffVerProc (const tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{

    tIfEntry           *pIfEntry = NULL;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhRsrcAffVerProc : ENTRY \n");

    if (pRsvpTeTnlInfo->u1CSPFPathRequested != RPTE_FALSE)
    {
        return RPTE_SUCCESS;
    }
/* Get the Outgoing Interface */
    pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    if (pIfEntry == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhRsrcAffVerProc : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* As per RFC 3209, Section 4.7.4, There are three Resource Affnity tests 
     * used to validate a link. 
     * Exclude Any test
     * Include Any test
     * Include All test  
     */

    /* Exclude Any test */
    if ((((IF_ENTRY_LINK_ATTR (pIfEntry) &
           (RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pRsvpTeTnlInfo))) ==
          RSVPTE_ZERO)
         ||
         ((IF_ENTRY_LINK_ATTR (pIfEntry) &
           (RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pRsvpTeTnlInfo))) !=
          RSVPTE_ZERO)) &&
        /* Include Any test */
        ((RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pRsvpTeTnlInfo) == RSVPTE_ZERO) ||
         ((IF_ENTRY_LINK_ATTR (pIfEntry) &
           (RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pRsvpTeTnlInfo))) !=
          RSVPTE_ZERO)) &&
        /* Include All test */
        ((RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo) == RSVPTE_ZERO) ||
         (((IF_ENTRY_LINK_ATTR (pIfEntry) &
            (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo))) ^
           ((RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pRsvpTeTnlInfo)))) ==
          RSVPTE_ZERO)))
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhRsrcAffVerProc : EXIT \n");
        return RPTE_SUCCESS;    /* All the three tests passes */
    }
    else
    {                            /* Any one of the test fails */
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhRsrcAffVerProc : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhStartLocalRepair
 * Description     : This function starts the Local Repair(Refresh) timer 
 * Input (s)       : pPsb - Pointer to Path state block Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhStartLocalRepair (tPsb * pPsb)
{
    tTimerParam        *pTmrParam = NULL;
    UINT4               u4RouteDelay;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartLocalRepair : ENTRY \n");

    TmrStopTimer (RSVP_GBL_TIMER_LIST, &PSB_TIMER (pPsb));

    pTmrParam = (tTimerParam *) & PSB_TIMER_PARAM (pPsb);

    u4RouteDelay = IF_ENTRY_ROUTE_DELAY (PSB_OUT_IF_ENTRY (pPsb));

    TIMER_PARAM_ID (pTmrParam) = RPTE_ROUTE_DELAY;
    TIMER_PARAM_PSB (pTmrParam) = pPsb;
    RSVP_TIMER_NAME (&PSB_TIMER (pPsb)) = (FS_ULONG) pTmrParam;

    if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST, &PSB_TIMER (pPsb),
                           ((UINT4) u4RouteDelay) *
                           SYS_NUM_OF_TIME_UNITS_IN_A_SEC) != TMR_SUCCESS)
    {

        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Start Timer failed. - in Local Repair \n");
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartLocalRepair : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhProcessPsbRefreshTimeOut
 * Description     : This function processes the Expired Psb Refresh Timer
 * Input (s)       : pExpiredTmr - Pointer to the Expired Timer
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhProcessPsbRefreshTimeOut (const tTmrAppTimer * pExpiredTmr)
{
    tTimerParam        *pTmrParam = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tPsb               *pPsb = NULL;
    tNbrEntry          *pUpNbrEntry = NULL;
    tNbrEntry          *pDnNbrEntry = NULL;
    UINT4               u4Time;
    UINT1               u1PathRefreshNeeded = RPTE_REFRESH_MSG_NO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessPsbRefreshTimeOut : ENTRY \n");

    pTmrParam = (tTimerParam *) RSVP_TIMER_NAME (pExpiredTmr);
    pPsb = TIMER_PARAM_PSB (pTmrParam);

    /* Only when the timer type is either Normal or Exp-BackOff timer,
     * Path Message needs to be sent. */
    OsixGetSysTime ((tOsixSysTime *) & (u4Time));

    RSVPTE_DBG2 (RSVPTE_PH_PRCS,
                 "PATH : PsbRefreshTimeOut : CurrTime = %d secs,"
                 "PsbTimeToDie = %d secs\n", u4Time, PSB_TIME_TO_DIE (pPsb));

    /* 
     * If it is Ingress, do not check with time to die field, instead start
     * sending path refresh message and start a refresh timer
     */

    pRsvpTeTnlInfo = PSB_RPTE_TNL_INFO (pPsb);

    pUpNbrEntry = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);
    pDnNbrEntry = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);

    if ((pDnNbrEntry != NULL) &&
        (pDnNbrEntry->u1GrProgressState == RPTE_GR_RESTART_IN_PROGRESS))
    {
        RptePhStartPathRefreshTmr (pPsb);
        return;
    }

    if ((RSVPTE_TNL_NODE_RELATION (pPsb->pRsvpTeTnlInfo) == RPTE_INGRESS) ||
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
         LOCAL_PROT_IN_USE))
    {
        u1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
    }
    else if ((u4Time >= PSB_TIME_TO_DIE (pPsb)) &&
             (((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                MPLS_TE_DEDICATED_ONE2ONE) &&
               (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                LOCAL_PROT_AVAIL)) ||
              (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
               MPLS_TE_FULL_REROUTE)))
    {
        if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
             MPLS_TE_DEDICATED_ONE2ONE))
        {
            RpteNhProcessOneToOneTnls (pRsvpTeTnlInfo, RPTE_ZERO,
                                       RPTE_LSP_LOCALLY_FAILED);
        }
        else
        {
            RpteNhHandlePsbOrRsbOrHelloTimeOut (pRsvpTeTnlInfo,
                                                RPTE_LSP_LOCALLY_FAILED);
            u1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
        }
    }
    else if ((u4Time >= PSB_TIME_TO_DIE (pPsb)) &&
             (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
              TNL_GR_NOT_SYNCHRONIZED)
             && ((pUpNbrEntry != NULL) &&
                 ((pUpNbrEntry->u1GrProgressState ==
                   RPTE_GR_RESTART_IN_PROGRESS)
                  || (pUpNbrEntry->u1GrProgressState ==
                      RPTE_GR_RECOVERY_IN_PROGRESS))))
    {
        u1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
    }
    /* If a refresh timeout occurs, at a node, for a LSP, while expecting a
     * Path Refresh message from a neighbor and that neighbor is GR Capable,
     * then action for the refresh failure is taken upon next successful Hello communication.
     * */
    else if ((u4Time >= PSB_TIME_TO_DIE (pPsb)) &&
             (pUpNbrEntry != NULL) &&
             (pUpNbrEntry->u2RestartTime != RPTE_ZERO) &&
             (pUpNbrEntry->bIsHelloSynchronoziedAfterRestart != RPTE_TRUE))
    {
        u1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
    }
    else if (u4Time >= PSB_TIME_TO_DIE (pPsb))
    {
        /* Deleting Psb */
        if (RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) != RPTE_TRUE)
        {
            RptePhGeneratePathTear (pRsvpTeTnlInfo);
            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            if (RPTE_TE_TNL (pRsvpTeTnlInfo)->u1TnlRelStatus ==
                TE_SIGMOD_TNLREL_AWAITED)
            {
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
            }
            else
            {
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
            }
            return;
        }
        else
        {
            RpteHandlePsbOrHelloTimeOutForFrr (pRsvpTeTnlInfo);
        }
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RptePhProcessPsbRefreshTimeOut : EXIT \n");
        return;
    }
    else
    {
        u1PathRefreshNeeded = RPTE_REFRESH_MSG_YES;
    }

    if (u1PathRefreshNeeded == RPTE_REFRESH_MSG_YES)
    {
        /* Refreshing Psb */
        RptePhPathRefresh (pRsvpTeTnlInfo);
        RptePhStartPathRefreshTmr (pPsb);
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessPsbRefreshTimeOut : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhProcessSnmpTimeOut
 * Description     : This function processes the Snmp TimeOut
 * Input (s)       : pExpiredTmr - Pointer to the Expired Timer
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhProcessSnmpTimeOut (const tTmrAppTimer * pExpiredTmr)
{
    tTimerParam        *pTmrParam = NULL;
    tPsb               *pPsb = NULL;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessSnmpTimeOut : ENTRY \n");

    pTmrParam = (tTimerParam *) RSVP_TIMER_NAME (pExpiredTmr);
    pPsb = TIMER_PARAM_PSB (pTmrParam);

    /* Call Tnl delete function to delete Tnl */
    RPTE_TNL_DEL_TNL_FLAG (pPsb->pRsvpTeTnlInfo) = RPTE_TRUE;
    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pPsb->pRsvpTeTnlInfo,
                         TE_TNL_CALL_FROM_SIG, RPTE_TRUE);

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessSnmpTimeOut : EXIT \n");
}

/****************************************************************************/
/* Function Name   : RptePhHandleRouteChange                                */
/* Description     : This function handles the route changes. If the route  */
/*                   change notification id from routing protocol, then     */
/*                   u4Addr will be a valid value. If the route change is   */
/*                   due to the change in the rsvpIfEnabled object in the   */
/*                   IfTable, then u4Addr will be 0                         */
/* Input (s)       : u4Addr - The destination address for which the route   */
/*                            changed.                                      */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePhHandleRouteChange (tRsvpteRtEntryInfo * pRouteInfo)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    UINT4               u4TnlDestNetAddr = 0;
    UINT4               u4DestAddr = 0;
    UINT1               u1AddrLen = 0;
    UINT1               u1IsEgrOfHop = 0;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhHandleRouteChange : ENTRY \n");

    /* Currently the route notification has to be handled for
     * the respective tunnels.
     */
    u4TnlDestNetAddr =
        (MPLS_IPV4_U4_ADDR (pRouteInfo->DestAddr)) &
        (MPLS_IPV4_U4_ADDR (pRouteInfo->DestMask));

    while (pRsvpTeTnlInfo != NULL)
    {
        if ((RPTE_TE_TNL (pRsvpTeTnlInfo) == NULL) ||
            (pRsvpTeTnlInfo->u1CSPFPathRequested == RPTE_FALSE))
        {
            pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                            (tRBElem *) pRsvpTeTnlInfo, NULL);
            continue;
        }
        if (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) == NULL)
        {
            u1AddrLen = RSVPTE_IPV4ADR_LEN;
            MEMCPY ((UINT1 *) (&(u4DestAddr)),
                    ((UINT1 *)
                     &(RSVPTE_TNL_EGRESS_RTR_ID
                       (RPTE_TE_TNL (pRsvpTeTnlInfo)))), u1AddrLen);
            u4DestAddr = OSIX_NTOHL (u4DestAddr);
        }
        else
        {
            /* 
             * Get the first ERHop from the ERHop list associated with 
             * the tunnel. 
             */
            pRsvpTeErHopInfo = (tRsvpTeHopInfo *)
                TMO_SLL_First (&RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo));
            if (pRsvpTeErHopInfo == NULL)
            {
                u1AddrLen = RSVPTE_IPV4ADR_LEN;
                MEMCPY ((UINT1 *) (&(u4DestAddr)),
                        ((UINT1 *)
                         &(RSVPTE_TNL_EGRESS_RTR_ID
                           (RPTE_TE_TNL (pRsvpTeTnlInfo)))), u1AddrLen);
                u4DestAddr = OSIX_NTOHL (u4DestAddr);
            }
            else
            {
                do
                {
                    /* 
                     * Check for the Lsr being Part of ErHop, if it is a part 
                     * of ErHop, continue until we get valid ErHop which is
                     * not part of it.
                     */
                    if (RpteIsLsrPartOfErHop
                        (pRsvpTeErHopInfo, &u1IsEgrOfHop) == RPTE_TRUE)
                    {
                        pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_Next
                            (&(RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo)),
                             &(pRsvpTeErHopInfo->NextHop));
                        continue;
                    }
                    else
                    {
                        break;
                    }
                }
                while (pRsvpTeErHopInfo != NULL);

                if (pRsvpTeErHopInfo == NULL)
                {
                    /* 
                     * There no ErHop, So, Process bassed on the 
                     * TnlRemote Address 
                     */
                    u1AddrLen = RSVPTE_IPV4ADR_LEN;
                    MEMCPY ((UINT1 *) (&(u4DestAddr)),
                            ((UINT1 *)
                             &(RSVPTE_TNL_EGRESS_RTR_ID
                               (RPTE_TE_TNL (pRsvpTeTnlInfo)))), u1AddrLen);
                    u4DestAddr = OSIX_NTOHL (u4DestAddr);
                }
                else
                {

                    /* The ER Hop value is obtained from the ER Hop Structure */
                    if (pRsvpTeErHopInfo->u1AddressType == ERO_TYPE_IPV4_ADDR)
                    {

                        u1AddrLen = RSVPTE_IPV4ADR_LEN;
                        MEMCPY ((UINT1 *) (&(u4DestAddr)),
                                (UINT1
                                 *) (&(RSVPTE_ERHOP_IPV4_ADDR
                                       (pRsvpTeErHopInfo))), u1AddrLen);
                        u4DestAddr = OSIX_NTOHL (u4DestAddr);

                    }
                }
            }
        }
        if (u4TnlDestNetAddr !=
            ((u4DestAddr) & (MPLS_IPV4_U4_ADDR (pRouteInfo->DestMask))))
        {
            pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                            (tRBElem *) pRsvpTeTnlInfo, NULL);
            continue;
        }
        if (pRouteInfo->u1CmdType == NETIPV4_ADD_ROUTE)
        {
            /* New Route is Added. So, Trying to trigger the 
             * backup path for the tunnel since FRR is required for the
             * tunnel. */
            if ((RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) & RPTE_FRR_CSPF_FAILED)
                == RPTE_FRR_CSPF_FAILED)
            {
                RPTE_FRR_CSPF_INFO (pRsvpTeTnlInfo) = RPTE_FRR_CSPF_NO_START;
                RPTE_FRR_CSPF_INTVL_COUNT (pRsvpTeTnlInfo) = RPTE_ZERO;
                RpteRhBackupPathTrigger (pRsvpTeTnlInfo);
            }
        }
        pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                        (tRBElem *) pRsvpTeTnlInfo, NULL);
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhHandleRouteChange : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RptePhHandleIfChange                                   */
/* Description     : This function handles the Interfac status changes.     */
/*                   If the route.                                          */
/* Input (s)       : pRsvpIf - The IfChange info                            */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePhHandleIfChange (tRsvpIfMsg * pRsvpIfMsg)
{
    tIfEntry           *pRpteIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tTMO_SLL_NODE      *pTmpNbrNode = NULL;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhHandleIfChange : ENTRY \n");

    if (pRsvpIfMsg->u4IfIndex == 0)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhHandleIfChange : INTMD - EXIT\n");
        return;
    }
    /* update the ifEntry Status in Hash Table,
     * let the processing of Path msg receiving/Resv message sending act on
     * this change.
     */
    if (RpteGetIfEntry ((INT4) pRsvpIfMsg->u4IfIndex, &pRpteIfEntry) ==
        RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhHandleIfChange : INTMD - EXIT\n");
        return;
    }
    if (IF_ENTRY_ENABLED (pRpteIfEntry) == (UINT1) pRsvpIfMsg->u4Status)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhHandleIfChange : INTMD - EXIT\n");
        return;
    }
    if (RSVPTE_FRR_MAKE_AFTER_BREAK_ENABLED (gpRsvpTeGblInfo) == RPTE_SNMP_TRUE)
    {
        RpteTnlScanForFrrMakeAfterBreakProt (pRpteIfEntry);
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhHandleIfChange : EXIT\n");
        return;
    }

    if (pRsvpIfMsg->u4DataTeIfIndex == RPTE_ZERO)
    {
        IF_ENTRY_ENABLED (pRpteIfEntry) = (UINT1) pRsvpIfMsg->u4Status;
    }

    RSVPTE_DBG1 (RSVPTE_PH_DBG,
                 "Handling Operational status change of the Interface %d\n",
                 IF_ENTRY_IF_INDEX (pRpteIfEntry));

    TMO_DYN_SLL_Scan (&(IF_ENTRY_NBR_LIST (pRpteIfEntry)), pNbrNode,
                      pTmpNbrNode, tTMO_SLL_NODE *)
    {
        pNbrEntry = (tNbrEntry *) pNbrNode;

        RpteHandleNbrStatusChange (pNbrEntry, pRsvpIfMsg->u4Status,
                                   pRsvpIfMsg->u4DataTeIfIndex, RPTE_TRUE);
    }

    if (pRsvpIfMsg->u4Status == RPTE_ENABLED)
    {
        IF_ENTRY_ADDR (pRpteIfEntry) = pRsvpIfMsg->u4Addr;

        RSVPTE_DBG3 (RSVPTE_PH_DBG,
                     "IP address for the L3 interface associated with "
                     "MPLS Interface %d is %x %x\n",
                     pRsvpIfMsg->u4IfIndex, pRpteIfEntry->u4Addr,
                     pRpteIfEntry->u4Mask);

        RpteHandleIfUpEvtForLspReoptimization ();
    }

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhHandleIfChange : EXIT\n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name : RpteIsLsrPartOfErHop                                        
 * Description   : This routine is called to check whether the given Node      
 *                 is part of the Er-Hop or not.                               
 *                                                                             
 * Input(s)      : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo structure.        
 *                 pu1IsEgrOfHop  - Pointer to IsEgrOfHop flag.                
 *                                                                             
 * Output(s)     : pu1IsEgrOfHop - Indicates whether the Lsr is Egress of the  
 *                 Er-Hop in context.                                          
 *                                                                             
 * Return(s)     : RPTE_FALSE when NOT part of given Er-Hop.                   
 *                 RPTE_TRUE when part of given Er-Hop.                        
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteIsLsrPartOfErHop (const tRsvpTeHopInfo * pRsvpTeErHopInfo,
                      UINT1 *pu1IsEgrOfHop)
{
    UINT4               u4ErHopAddr = 0;
    UINT4               u4ErHopMask = RPTE_HOST_NETMASK;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLsrPartOfErHop : ENTRY \n");
    switch (pRsvpTeErHopInfo->u1AddressType)
    {
        case ERO_TYPE_IPV4_ADDR:

            RPTE_GET_MASK_FROM_PRFX_LEN (pRsvpTeErHopInfo->u1AddrPrefixLen,
                                         u4ErHopMask);
            MEMCPY ((UINT1 *) (&(u4ErHopAddr)), &pRsvpTeErHopInfo->IpAddr,
                    RSVPTE_IPV4ADR_LEN);
            if (RpteIsLsrPartOfIpv4ErHop (OSIX_NTOHL (u4ErHopAddr),
                                          u4ErHopMask) == RPTE_TRUE)
            {
                *pu1IsEgrOfHop = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLsrPartOfErHop : EXIT \n");
                return RPTE_TRUE;
            }

            /* NOT Part of Er-Hop(Ipv4Addr Type) */
            break;

        default:
            RSVPTE_DBG (RSVPTE_PH_PRCS, "ADVT: Bad Er-Hop Type \n");
            break;
    }
    *pu1IsEgrOfHop = RPTE_FALSE;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLsrPartOfErHop : EXIT \n");
    return RPTE_FALSE;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name : RpteIsLsrPartOfCHop
 * Description   : This routine is called to check whether the given Node
 *                 is part of the C-Hop or not.
 *
 * Input(s)      : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo structure.
 *                 pu1IsEgrOfHop  - Pointer to IsEgrOfHop flag.
 *
 * Output(s)     : pu1IsEgrOfHop - Indicates whether the Lsr is Egress of the
 *                 C-Hop in context.
 *
 * Return(s)     : RPTE_FALSE when NOT part of given C-Hop.
 *                 RPTE_TRUE when part of given C-Hop.
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteIsLsrPartOfCHop (const tTeCHopInfo * pRsvpTeCHopInfo, UINT1 *pu1IsEgrOfHop)
{
    UINT4               u4ErHopAddr = 0;
    UINT4               u4ErHopMask = RPTE_HOST_NETMASK;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLsrPartOfCHop : ENTRY \n");
    switch (pRsvpTeCHopInfo->u1AddressType)
    {
        case ERO_TYPE_IPV4_ADDR:

            RPTE_GET_MASK_FROM_PRFX_LEN (pRsvpTeCHopInfo->u1AddrPrefixLen,
                                         u4ErHopMask);
            MEMCPY ((UINT1 *) (&(u4ErHopAddr)), &pRsvpTeCHopInfo->IpAddr,
                    RSVPTE_IPV4ADR_LEN);
            if (RpteIsLsrPartOfIpv4ErHop (OSIX_NTOHL (u4ErHopAddr),
                                          u4ErHopMask) == RPTE_TRUE)
            {
                *pu1IsEgrOfHop = RPTE_TRUE;
                RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLsrPartOfCHop : EXIT \n");
                return RPTE_TRUE;
            }

            /* NOT Part of C-Hop(Ipv4Addr Type) */
            break;

        default:
            RSVPTE_DBG (RSVPTE_PH_PRCS, "ADVT: Bad C-Hop Type \n");
            break;
    }
    *pu1IsEgrOfHop = RPTE_FALSE;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLsrPartOfCHop : EXIT \n");
    return RPTE_FALSE;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name : RpteEgrsPrcs                                          
 * Description   : This routines handles the processing at the Egress LSR.
 * Input(s)      : pPktMap - Points to the PktMap structure.  
 * Output(s)     : None                                                        
 * Return(s)     : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
INT1
RpteEgrsPrcs (tPktMap * pPktMap)
{
    UINT1               u1ErrCode = RPTE_ZERO;
    UINT2               u2ErrValue = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteEgrsPrcs : ENTRY \n");

    /* AS per RFC 3473 section 2.1.1, 
     * For GMPLS tunnel, Switching type and Encoding type validation
     * should be done in incoming interface for all nodes */
    if (RptePvmValidateSwAndEncType (pPktMap->pIncIfEntry,
                                     pPktMap->pRsvpTeTnlInfo, FALSE)
        == RPTE_FAILURE)
    {
        RptePvmSendPathErr (pPktMap, u1ErrCode, u2ErrValue);
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteEgrsPrcs : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (RptePhUpStrResvBwAndLblProg (pPktMap->pRsvpTeTnlInfo, &u1ErrCode,
                                     &u2ErrValue) == RPTE_FAILURE)
    {
        RptePvmSendPathErr (pPktMap, u1ErrCode, u2ErrValue);
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteEgrsPrcs : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (RptePhRhProcessResvMsg (pPktMap) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteEgrsPrcs : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* No ofTunnels Associated with this interface is incremented */
    RPTE_IF_NUM_TNL_ASSOC (PKT_MAP_IF_ENTRY (pPktMap))++;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteEgrsPrcs : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name : RpteIntermediatePrcs
 * Description   : This routine handles the intermediate LSR processing.
 * Input(s)      : u4OutGw - Address of the gateway                           
 *                 pIfEntry - Pointer to the Interface Entry.
 *                 pRsvpTeTnlInfo - Points to the RsvpTeTnlInfo structure.
 *                 pPktMap - Points to the PktMap structure.
 *                 u1PathRefreshNeeded - PathRefresh Flag.
 *                 u1ResvRefreshNeeded - ResvRefresh Flag.
 *                 u1NewTnl - NewTnl indication Flag.
 *                 u1NewBkpTnl - New Backup Tnl indication Flag.
 * Output(s)     : None                                                        
 * Return(s)     : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
INT1
RpteIntermediatePrcs (UINT4 u4OutGw,
                      tIfEntry * pIfEntry,
                      tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                      tPktMap * pPktMap,
                      UINT1 u1PathRefreshNeeded,
                      UINT1 u1ResvRefreshNeeded, UINT1 u1NewTnl,
                      UINT1 u1NewBkpTnl)
{
    tErrorSpec          ErrorSpec;
    UINT1               u1ResvRsrc = RPTE_FALSE;
    tAdSpec             NewAdSpec;
    tRsvpTeTnlInfo     *pRsvpTeTmpTnlInfo = NULL;
    UINT2               u2InErHopCnt = RSVPTE_ZERO;
    UINT2               u2ErHopCnt = RSVPTE_ZERO;
    UINT1               u1DetourGrpId = RSVPTE_ZERO;
    UINT4               u4TnlDestAddr = RSVPTE_ZERO;
    UINT1               u1ErrCode = RPTE_ZERO;
    UINT2               u2ErrValue = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : ENTRY \n");
    MEMSET (&NewAdSpec, RPTE_ZERO, sizeof (tAdSpec));

    /* 
     * The next hop address to forward the packet is
     * stored in the Tnl's NextHop Address to forward
     * the path message downstream.
     */
    RSVPTE_TNL_DN_NHOP (pRsvpTeTnlInfo) = u4OutGw;
    /* 
     * If this is a new TnlInfo then start a timer for the psb,
     * fill the outif list with the routes obtained from 
     * route query. Avoid having incifid of this message
     * as an element in the outifidlist of the TnlInfo to avoid
     * looping.
     */
    CONVERT_TO_INTEGER ((PKT_MAP_SSN_EGRESS_ADDR (pPktMap)), u4TnlDestAddr);
    u4TnlDestAddr = OSIX_NTOHL (u4TnlDestAddr);

    if ((RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) != RPTE_TRUE) &&
        (RPTE_IS_DEST_OUR_ADDR (u4TnlDestAddr) == RPTE_SUCCESS) &&
        (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) != NULL) &&
        (TMO_SLL_Count (&(RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo)))))
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : "
                    "Tnl take different path failed EXIT \n");
        RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB, RPTE_BAD_STRICT_NODE);
        RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
        return RPTE_FAILURE;
    }
    if ((u1NewTnl == RPTE_YES) || (u1NewBkpTnl == RPTE_YES))
    {
        if (pIfEntry == NULL)
        {
            return RPTE_FAILURE;
        }

        if (pIfEntry != PKT_MAP_IF_ENTRY (pPktMap))
        {
            PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = pIfEntry;
        }
        /*else
           {
           RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
           RpteUtlCleanPktMap (pPktMap);
           RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : EXIT \n");
           return RPTE_FAILURE;
           } */

        if (RptePhUpStrResvBwAndLblProg
            (pRsvpTeTnlInfo, &u1ErrCode, &u2ErrValue) == RPTE_FAILURE)
        {
            RptePvmSendPathErr (pPktMap, u1ErrCode, u2ErrValue);

            RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);

            RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : EXIT \n");

            return RPTE_FAILURE;
        }

        /* AS per RFC 3473 section 2.1.1, 
         * For GMPLS tunnel, Switching type and Encoding type validation
         * should be done in incoming interface for all nodes */
        if (RptePvmValidateSwAndEncType
            (pPktMap->pIncIfEntry, pRsvpTeTnlInfo, FALSE) == RPTE_FAILURE)
        {
            RpteUtlCleanPktMap (pPktMap);
            return RPTE_FAILURE;
        }

        /* As per RFC 3473, section 2.1.1,
         * For GMPLS tunnel, switching type and encoding type
         * validation should be done in outgoing interface also */
        if (RptePvmValidateSwAndEncType (pIfEntry, pRsvpTeTnlInfo, TRUE)
            == RPTE_FAILURE)
        {
            RpteUtlCleanPktMap (pPktMap);
            return RPTE_FAILURE;
        }

        if (RpteValidateSupprtdDSObjs (pRsvpTeTnlInfo, pIfEntry, pPktMap)
            == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : EXIT \n");
            RpteUtlCleanPktMap (pPktMap);
            return RPTE_SUCCESS;
        }

    }
    else
    {
        /* 
         * If the PSB is not new update the outifid list 
         * of the psb  and start local repair 
         */
        if (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) != pIfEntry)
        {
            PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = pIfEntry;
            RptePhStartLocalRepair (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
        }
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : EXIT \n");
        return RPTE_SUCCESS;
    }
    /* As per RFC 3209, Section 4.7.4, Resource Affinity procedure has to be 
     * applied to validate a link  
     */
    if (RptePhRsrcAffVerProc (pRsvpTeTnlInfo) != RPTE_SUCCESS)
    {
        /* Send Path Err - Routing Problem occured */
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : RSVP-TE Routing Problem -Send Path Err\n");
        RptePvmUtlSendPathErr (pPktMap, RPTE_ROUTE_PROB, RPTE_NO_ROUTE_AVAIL);
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : EXIT \n");
        RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);

        return RPTE_FAILURE;
    }

    if ((RPTE_PKT_MAP_PKT_PATH (pPktMap) == RPTE_FRR_BACKUP_PATH)
        && (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) != NULL) &&
        (RPTE_IS_MP (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo)))))
    {
        /* Since PATH Message is received on the Backup path and
         * node state is MP, Path Refresh NOT Needed */
        RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) =
            RSVPTE_TNL_NODE_RELATION (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo));
        RPTE_PKT_MAP_TNL_INFO (pPktMap) = pRsvpTeTnlInfo;
        if (RpteEgrsPrcs (pPktMap) == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS, "Rpte Egress Processing failed\n");
        }
        rpteTeDeleteHopListInfo (RSVPTE_TNL_OUTERHOP_LIST_INFO
                                 (pRsvpTeTnlInfo));
        RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) = NULL;

        RpteDeleteTrfcParamsAtEgress (pRsvpTeTnlInfo);

        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : EXIT \n");
        return RPTE_SUCCESS;
    }

    if (u1PathRefreshNeeded == RPTE_REFRESH_MSG_NO)
    {
        /* Path Refresh NOT Needed */
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : EXIT \n");
        return RPTE_SUCCESS;
    }
    /* Path Refresh Needed */
    if (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) != NULL)
    {
        if ((RpteTcCalcAdSpec
             (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
              &pRsvpTeTnlInfo->pPsb->AdSpec,
              &pRsvpTeTnlInfo->pPsb->SenderTspecObj.SenderTspec,
              &NewAdSpec)) == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_MAIN_MEM, "AdSpec Calculation Failed.\n");
            MEMSET (&(pRsvpTeTnlInfo->pPsb->AdSpec), RSVPTE_ZERO,
                    sizeof (tAdSpec));
        }
        else
        {
            MEMCPY (&pRsvpTeTnlInfo->pPsb->AdSpec, &NewAdSpec,
                    sizeof (tAdSpec));
        }
        /* No ofTunnels Associated with this Outgoing Interface is 
         * incremented */
        RPTE_IF_NUM_TNL_ASSOC (pIfEntry)++;
        /* No ofTunnels Associated with the Incoming Interface is incremented */
        pIfEntry = PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
        RPTE_IF_NUM_TNL_ASSOC (pIfEntry)++;

        /* FRR handle, If the Node's Position is DMP,
         * Select the least ERHOPs to reach the destination.
         * This Selection is done based on the Group ID Assigned. */

        if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL)
            && ((RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) != NULL) &&
                (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL
                                                   (pRsvpTeTnlInfo)))))
            && (RSVPTE_FRR_DETOUR_MERGING_CAPABLE (gpRsvpTeGblInfo) ==
                RPTE_TRUE))
        {
            pRsvpTeTmpTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);
            u1DetourGrpId = RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTnlInfo);
            /* Find the tnl which is used for downstream refreshment */
            while (RPTE_FRR_IN_TNL (pRsvpTeTmpTnlInfo) != NULL)
            {
                if (!((RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTmpTnlInfo)
                       == RPTE_TRUE)
                      && (u1DetourGrpId ==
                          RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTmpTnlInfo))))
                {
                    pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL (pRsvpTeTmpTnlInfo);
                    if (pRsvpTeTmpTnlInfo == NULL)
                    {
                        break;
                    }
                    continue;
                }
                break;
            }
            if (pRsvpTeTmpTnlInfo != NULL)
            {
                u2ErHopCnt =
                    (UINT2) TMO_SLL_Count (&RSVPTE_TNL_OUTERHOP_LIST
                                           (pRsvpTeTmpTnlInfo));

                u2InErHopCnt =
                    (UINT2) TMO_SLL_Count (&RSVPTE_TNL_OUTERHOP_LIST
                                           (pRsvpTeTnlInfo));
                if (u2ErHopCnt > u2InErHopCnt)
                {
                    RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTmpTnlInfo) =
                        RPTE_FALSE;
                    RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTnlInfo) =
                        RPTE_TRUE;
                }
                else if (u2ErHopCnt == u2InErHopCnt)
                {
                    RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTmpTnlInfo) =
                        RPTE_TRUE;
                    RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTnlInfo) =
                        RPTE_FALSE;
                }
            }
        }
        RSVPTE_DBG5 (RSVPTE_FRR_DBG,
                     "Tunnel %d %d %x %x state: %x\n choosen as downstream "
                     "tunnel to refresh the group of detours",
                     RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                     RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                     OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                     OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                     RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo));
        /* Destination is Reachable, Hence refreshing */
        RSVPTE_FRR_DETOUR_OUTGOING_NUM (gpRsvpTeGblInfo)++;
        /* If the synchronization status is upstream synchronized, then only upstream node
         * helped the node to recover its state and down stream node has to help. Hence
         * path message should not be sent out until it gets help from down stream node
         * */
        if ((pRsvpTeTnlInfo->u1IsGrTnl == RPTE_YES) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
             TNL_GR_UPSTREAM_SYNCHRONIZED))
        {
            return RPTE_SUCCESS;
        }

        RptePhPathRefresh (pRsvpTeTnlInfo);
        RptePhStartPathRefreshTmr (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    }
    /*
     * Search for an Rsb. This is to determine whether Traffic 
     * Control updation and ResvRefresh is Needed or not. 
     */

    if (u1NewTnl != RPTE_YES)
    {
        u1ResvRsrc = RPTE_TRUE;
        /* Update Traffic Control */
        if (RpteRhUpdateTrafficControl (pRsvpTeTnlInfo, u1ResvRsrc,
                                        &u1ResvRefreshNeeded, &ErrorSpec)
            == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Update Traffic control failed\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RpteIntermediatePrcs : INTMD-EXIT \n");
            return RPTE_SUCCESS;
        }
        /* 
         * We also do trigger for Resv refresh, 
         * when the PHOP changes
         */
        if (u1ResvRefreshNeeded == RPTE_REFRESH_MSG_YES)
        {
            /* Resv Refresh Needed */
            RpteRhResvRefresh (pRsvpTeTnlInfo);
        }
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name : RptePrcsAndUpdateErHopList                                   
 * Description   : This routine Processes and updates ErHopList.
 * Input(s)      : pRsvpTeTnlInfo - Points to the RsvpTeTnlInfo structure.
 *                 pPktMap - Points to the packt map structure.
 *                 u1PathRefreshNeeded - PathRefresh Flag.
 *                 u1ResvRefreshNeeded - ResvRefresh Flag.
 *                 u1NewTnl - NewTnl indication Flag.
 *                 u1NewBkpTnl - New backup Tnl indication Flag.
 * Output(s)     : None                                                        
 * Return(s)     : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
INT1
RptePrcsAndUpdateErHopList (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                            tPktMap * pPktMap,
                            UINT1 u1PathRefreshNeeded,
                            UINT1 u1ResvRefreshNeeded, UINT1 u1NewTnl,
                            UINT1 u1NewBkpTnl, UINT1 *pu1IsCspfReqd)
{
    /* If the First Er-Hop of current Er-Hop List is processed, then the
     * variable u1FirstHopPrcsd is set to RPTE_TRUE */
    UINT1               u1FirstHopPrcsd = RPTE_FALSE;
    UINT4               u4DestAddr = 0;
    tTMO_SLL           *pErHopList = NULL;

    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    tRsvpTeHopInfo     *pRsvpTeErNextHopInfo = NULL;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePrcsAndUpdateErHopList : ENTRY \n");

    /*   -------               ---------         */
    /*  | Hop N |------------>| Hop N+1 |        */
    /*   -------               ---------         */
    /*  pRsvpTeErHopInfo   pRsvpTeErNextHopInfo  */

    *pu1IsCspfReqd = RPTE_FALSE;

    if (RSVPTE_TNL_PATH_OPTION (pRsvpTeTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RptePrcsAndUpdateErHopList : INTMD-EXIT \n");
        return RPTE_SUCCESS;
    }

    pErHopList = (&RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo));

    pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_First (pErHopList);
    while (pRsvpTeErHopInfo != NULL)
    {
        if (pRsvpTeTnlInfo->u1IsGrTnl == RPTE_YES)
        {
            pRsvpTeErNextHopInfo =
                (tRsvpTeHopInfo *) TMO_SLL_First (pErHopList);
        }
        else
        {
            pRsvpTeErNextHopInfo = (tRsvpTeHopInfo *)
                TMO_SLL_Next (pErHopList, &(pRsvpTeErHopInfo->NextHop));
        }

        if ((pRsvpTeErHopInfo->u1LsrPartOfErHop == RPTE_TRUE) ||
            (pRsvpTeTnlInfo->u1IsGrTnl == RPTE_YES))
        {
            if (u1FirstHopPrcsd == RPTE_FALSE)
            {
                u1FirstHopPrcsd = RPTE_TRUE;
            }
            /*
             * Normally when an Lsr belongs to a given Er-Hop N then, we look 
             * at how to forward to Er-Hop N+1.
             */
            /* Look at second Er-hop */
            if (pRsvpTeErNextHopInfo != NULL)
            {
                if (pRsvpTeErNextHopInfo->u1LsrPartOfErHop == RPTE_TRUE)
                {
                    /*
                     * Part of the Next Er-Hop.
                     * if ( LSR is part of the Next Er-Hop or Abstract Node )
                     * then delete the previous Er-Hop or Abstract Node;
                     * switch to Next To Next Er-Hop and process;
                     */
                    rpteTeDeleteTnlHopInfo (RSVPTE_TNL_OUTERHOP_LIST_INFO
                                            (pRsvpTeTnlInfo),
                                            RSVPTE_TNL_PATH_OPTION
                                            (pRsvpTeTnlInfo), pRsvpTeErHopInfo);
                    if (TMO_SLL_Count (pErHopList) == RPTE_ZERO)
                    {
                        RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlHopTableIndex =
                            RPTE_ZERO;
                        RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlPathInUse =
                            RPTE_ZERO;
                    }
                    pRsvpTeErHopInfo = (tRsvpTeHopInfo *)
                        TMO_SLL_First (pErHopList);
                    continue;
                }
                else
                {
                    if (pRsvpTeTnlInfo->u1IsGrTnl == RPTE_NO)
                    {
                        rpteTeDeleteTnlHopInfo (RSVPTE_TNL_OUTERHOP_LIST_INFO
                                                (pRsvpTeTnlInfo),
                                                RSVPTE_TNL_PATH_OPTION
                                                (pRsvpTeTnlInfo),
                                                pRsvpTeErHopInfo);
                    }
                    if (TMO_SLL_Count (pErHopList) == RPTE_ZERO)
                    {
                        RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlHopTableIndex =
                            RPTE_ZERO;
                        RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlPathInUse =
                            RPTE_ZERO;
                    }

                    if (RptePhFindRouteAndFwdPathMsg (pRsvpTeErNextHopInfo,
                                                      pRsvpTeTnlInfo,
                                                      pPktMap,
                                                      u1PathRefreshNeeded,
                                                      u1ResvRefreshNeeded,
                                                      u1NewTnl,
                                                      u1NewBkpTnl,
                                                      (UINT1) RPTE_ONE,
                                                      pu1IsCspfReqd)
                        == RPTE_FAILURE)
                    {
                        RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
                        return RPTE_FAILURE;
                    }

                    RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                "RptePrcsAndUpdateErHopList : EXIT \n");
                    return RPTE_SUCCESS;
                }
            }
            else
            {                    /* Next Hop NOT present */
                /*
                 * Lsr belongs to the Last Er-Hop ie Egress.
                 * Update the Er-Hop List, by deleting the last Er-Hop.
                 */
                rpteTeDeleteTnlHopInfo (RSVPTE_TNL_OUTERHOP_LIST_INFO
                                        (pRsvpTeTnlInfo),
                                        RSVPTE_TNL_PATH_OPTION (pRsvpTeTnlInfo),
                                        pRsvpTeErHopInfo);
                if (TMO_SLL_Count (pErHopList) == RPTE_ZERO)
                {
                    RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlHopTableIndex =
                        RPTE_ZERO;
                    RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlPathInUse = RPTE_ZERO;
                }
                /* 
                 * If the destination address in the RSVP-TE Path message 
                 * is equal to one of the local addresses, then we are 
                 * the egress of the tunnel
                 * and we have to send the Resv message. 
                 */
                CONVERT_TO_INTEGER ((PKT_MAP_SSN_EGRESS_ADDR (pPktMap)),
                                    u4DestAddr);
                u4DestAddr = OSIX_NTOHL (u4DestAddr);
                RSVPTE_DBG (RSVPTE_PH_IF,
                            " PH : IF ipif_is_our_address: CALLED \n ");
                if (RPTE_IS_DEST_OUR_ADDR (u4DestAddr) == TRUE)
                {
                    /* 
                     * Call Egress processing routine to trigger reservation 
                     * at Egress 
                     */
                    RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) = RPTE_EGRESS;
                    RPTE_PKT_MAP_TNL_INFO (pPktMap) = pRsvpTeTnlInfo;

                    if (RpteEgrsPrcs (pPktMap) == RPTE_FAILURE)
                    {
                        RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
                        return RPTE_FAILURE;
                    }

                    /* The Outgoing ErHopLists are deleted as well as the
                     * Traffic params associated */
                    rpteTeDeleteHopListInfo (RSVPTE_TNL_OUTERHOP_LIST_INFO
                                             (pRsvpTeTnlInfo));
                    RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) = NULL;

                    RpteDeleteTrfcParamsAtEgress (pRsvpTeTnlInfo);

                    RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                "RptePrcsAndUpdateErHopList : EXIT \n");
                    return RPTE_SUCCESS;
                }
                else
                {
                    if (RptePhFindRouteAndFwdPathMsg (NULL,
                                                      pRsvpTeTnlInfo,
                                                      pPktMap,
                                                      u1PathRefreshNeeded,
                                                      u1ResvRefreshNeeded,
                                                      u1NewTnl,
                                                      u1NewBkpTnl,
                                                      (UINT1) RPTE_TWO,
                                                      pu1IsCspfReqd)
                        == RPTE_FAILURE)
                    {
                        RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
                        return RPTE_FAILURE;
                    }

                    RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                "RptePrcsAndUpdateErHopList : EXIT \n");
                    return RPTE_SUCCESS;
                }
            }
        }
        else if (pRsvpTeErHopInfo->u1HopType == ERO_TYPE_STRICT)
        {
            /* Error : Node does not belong to the Initial Strict Er-Hop. */
            if (u1FirstHopPrcsd == RPTE_FALSE)
            {
                /* First Er-Hop received from UStream in Er-Hop List */
                RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                    RPTE_BAD_INIT_SUB_OBJ);
                /* Removal of Erhops in TE */
                RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "PATH : -E- : Bad Initial Subobject - "
                            "Path err sent\n");
            }
            else
            {
                RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                    RPTE_BAD_STRICT_NODE);
                /* For all subsequent Strict Er-Hops */
                /* Removal of Erhops in TE */
                RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "PATH : -E- : Bad Strict Node - Path err sent\n");

            }
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePrcsAndUpdateErHopList : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        else
        {
            /*
             * Node does not belong to this Loose Er-Hop.
             *
             * Check the Reachability to the Er-Hop via MPLS Domain.
             * If Er-Hop is reachable via MPLS Domain then, update the Tnl with
             * essential forwarding information.
             */
            if (RptePhFindRouteAndFwdPathMsg (pRsvpTeErHopInfo, pRsvpTeTnlInfo,
                                              pPktMap, u1PathRefreshNeeded,
                                              u1ResvRefreshNeeded, u1NewTnl,
                                              u1NewBkpTnl, (UINT1) RPTE_TWO,
                                              pu1IsCspfReqd) == RPTE_FAILURE)
            {
                RpteUtlCleanTeLists (pPktMap, pRsvpTeTnlInfo);
                return RPTE_FAILURE;
            }

            return RPTE_SUCCESS;
        }
    }                            /* while */
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePrcsAndUpdateErHopList : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name : RpteFrrValidateBkpPathMsg                                   
 * Description   : This routine Validates the PATH Message on the backup path.
 *                 This function is called only if FRR is enabled.
 * Input(s)      : pPktMap - Points to the packet map structure.
 *                 pRsvpTeTnlInfo - Pointer to RSVP Tnl info.
 * Output(s)     : None                                                        
 * Return(s)     : RPTE_FAILURE/RPTE_SUCCESS
 */
/*---------------------------------------------------------------------------*/
INT1
RpteFrrValidateBkpPathMsg (tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    UINT4               u4AvoidNodeAddr = RPTE_ZERO;
    UINT4               u4LsrAddr = RPTE_ZERO;
    UINT1               u1Cnt = RPTE_ZERO;

    /* Backup PATH Message is received on the primary path interface. So,
     * returning Failure. */
    if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL) &&
        (PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))
         == PKT_MAP_IF_ENTRY (pPktMap)))
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Bkp PATH Message received on Primary interface "
                    " - Returning Failure \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RpteFrrValidateBkpPathMsg : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* RFC 4090 sec.6.3, clearing the backup path session attribute flags */
    /* In the Backup PATH Message, FAST_REROUTE Object 
     * should not present */
    if (PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap) != NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Bkp PATH Message contain Fast ReRoute Object "
                    " - Returning Failure \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RpteFrrValidateBkpPathMsg : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap)).u2Length !=
        RPTE_ZERO)
    {
        CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                            u4LsrAddr);
        u4LsrAddr = OSIX_NTOHL (u4LsrAddr);
        for (u1Cnt = RPTE_ZERO;
             ((u1Cnt < RPTE_MAX_DETOUR_IDS) &&
              ((u4AvoidNodeAddr =
                OSIX_NTOHL ((PKT_MAP_RPTE_DETOUR_OBJ
                             (pPktMap)).RsvpTeDetour.au4AvoidNodeId[u1Cnt])))
              != RPTE_ZERO); u1Cnt++)
        {
            if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) ==
                 RPTE_FRR_BACKUP_TNL) && (u4AvoidNodeAddr == u4LsrAddr))
            {
                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "PATH : Bkp PATH Message received by the node that "
                            " should be avoided  - Returning Failure \n");
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RpteFrrValidateBkpPathMsg : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            if (u4AvoidNodeAddr == IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)))
            {
                RSVPTE_DBG (RSVPTE_PH_PRCS,
                            "PATH : Bkp PATH Message received by the link that "
                            " should be avoided  - Returning Failure \n");
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RpteFrrValidateBkpPathMsg : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
        }
    }
    /* In the Backup PATH Message, Local Protection Desired flag in
     * Session Attribute should be cleared */
    if ((PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap) &
         RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG)
        == RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Bkp PATH Message received contains local"
                    " protection flag - Returning Failure \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RpteFrrValidateBkpPathMsg : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* In the Backup PATH Message, Node Protection Desired flag in
     * Session Attribute should be cleared */
    if ((PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap) &
         RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG) == RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Bkp PATH Message received contains node protection "
                    "flag - Returning Failure \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RpteFrrValidateBkpPathMsg : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* In the Backup PATH Message, Label recording Desired flag in
     * Session Attribute should be cleared */
    if ((PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap) &
         RPTE_FRR_SSN_ATTR_LBL_RECORD_FLAG)
        == RPTE_FRR_SSN_ATTR_LBL_RECORD_FLAG)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Bkp PATH Message received contains label recording"
                    " flag - Returning Failure \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RpteFrrValidateBkpPathMsg : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* In the Backup PATH Message, Bandwidth Protection Desired flag in
     * Session Attribute should be cleared */
    if ((PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap) &
         RPTE_FRR_SSN_ATTR_BANDWIDTH_FLAG) == RPTE_FRR_SSN_ATTR_BANDWIDTH_FLAG)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Bkp PATH Message received contains bandwidth "
                    "protection flag - Returning Failure \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RpteFrrValidateBkpPathMsg : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteFrrValidateBkpPathMsg : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*
 * Function Name : RptePhConstructBkpPathConsts                                   
 * Description   : This routine constructs the PATH Message to be sent on the
 *                 backup path. This function is called only if FRR is Enabled.
 * Input(s)      : ppRsvpTeTnlInfo - Pointer to Pointer to the RsvpTeTnlInfo
 * Output(s)     : None.
 * Return(s)     : RPTE_FAILURE/RPTE_SUCCESS
*---------------------------------------------------------------------------*/
UINT1
RptePhConstructBkpPathConsts (tRsvpTeTnlInfo ** ppRsvpTeTnlInfo)
{
    tRsvpTeTrfcParams   TeTrfcParms;
    tRsvpTeRSVPTrfcParams RsvpTeTrfcParms;
    tRsvpTeTnlInfo     *pBaseRsvpTeTnlInfo = NULL;
    tSenderTspec       *pSenderTspec = NULL;
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    tRsvpTeTrfcParams  *pTeTrfcParms = NULL;
    tRsvpTeArHopInfo   *pTnlArHopInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = *ppRsvpTeTnlInfo;
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4TmpAddr = RPTE_ZERO;
    FLT4                f4TmpVar = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhConstructBkpPathConsts : ENTRY \n");

    MEMSET (&RsvpTeTrfcParms, RSVPTE_ZERO, sizeof (tRsvpTeRSVPTrfcParams));
    MEMSET (&TeTrfcParms, RSVPTE_ZERO, sizeof (tRsvpTeTrfcParams));

    /* PATH Message Construction for backup path requires the base tunnel
     * information */
    pBaseRsvpTeTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);
    pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    if (RSVPTE_TNL_NODE_RELATION (pBaseRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        if (((RPTE_TE_TNL_FRR_INFO (pBaseRsvpTeTnlInfo)) == NULL) ||
            (((RPTE_TE_TNL_FRR_INFO (pBaseRsvpTeTnlInfo)) != NULL) &&
             (RSVPTE_TE_TNL_FRR_CONST_SE_STYLE (RPTE_TE_TNL_FRR_INFO
                                                (pBaseRsvpTeTnlInfo)) ==
              RPTE_TRUE)))
        {
            /* One2One sender template specific method */
            u4TmpAddr = OSIX_HTONL (IF_ENTRY_ADDR (pIfEntry));
            MEMCPY (&PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                    TnlSndrAddr, &u4TmpAddr, RSVPTE_IPV4ADR_LEN);
        }
    }
    else
    {
        if (pBaseRsvpTeTnlInfo->pTeTnlInfo->bIsSEStyleDesired == TRUE)
        {
            /* One2One sender template specific method */
            u4TmpAddr = OSIX_HTONL (IF_ENTRY_ADDR (pIfEntry));
            MEMCPY (&PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                    TnlSndrAddr, &u4TmpAddr, RSVPTE_IPV4ADR_LEN);
        }
    }

    /* Copy the protected tnls traffic params to backup tnls path msg */
    pSenderTspec = &(pBaseRsvpTeTnlInfo->pPsb->SenderTspecObj).SenderTspec;
    RPTE_RSVPTE_TRFC_PARAMS ((&TeTrfcParms)) = &RsvpTeTrfcParms;

    if (RSVPTE_TNL_NODE_RELATION (pBaseRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        RPTE_TPARAM_TBR ((&TeTrfcParms)) =
            (UINT4) pSenderTspec->TBParams.fBktRate;
        RPTE_TPARAM_TBS ((&TeTrfcParms)) =
            (UINT4) pSenderTspec->TBParams.fBktSize;
        RPTE_TPARAM_PDR ((&TeTrfcParms)) =
            (UINT4) pSenderTspec->TBParams.fPeakRate;
        RPTE_TPARAM_MPU ((&TeTrfcParms)) = pSenderTspec->TBParams.u4MinSize;
        RPTE_TPARAM_MPS ((&TeTrfcParms)) = pSenderTspec->TBParams.u4MaxSize;
    }
    else
    {
        RPTE_TPARAM_TBR ((&TeTrfcParms))
            = (UINT4) OSIX_NTOHF (pSenderTspec->TBParams.fBktRate);
        RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (RPTE_TPARAM_TBR ((&TeTrfcParms)));
        RPTE_TPARAM_TBS ((&TeTrfcParms))
            = (UINT4) OSIX_NTOHF (pSenderTspec->TBParams.fBktSize);
        RPTE_TPARAM_PDR ((&TeTrfcParms))
            = (UINT4) OSIX_NTOHF (pSenderTspec->TBParams.fPeakRate);
        RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (RPTE_TPARAM_PDR ((&TeTrfcParms)));
        RPTE_TPARAM_MPU ((&TeTrfcParms))
            = OSIX_NTOHL (pSenderTspec->TBParams.u4MinSize);
        RPTE_TPARAM_MPS ((&TeTrfcParms))
            = OSIX_NTOHL (pSenderTspec->TBParams.u4MaxSize);
    }
    /* HeadEnd + PLR case handle */
    if ((RSVPTE_TNL_NODE_RELATION (pBaseRsvpTeTnlInfo) == RPTE_INGRESS) &&
        (RPTE_TE_TNL_FRR_INFO (pBaseRsvpTeTnlInfo) != NULL))
    {
        /* One-to-One bandwidth is desired */
        RPTE_TPARAM_TBR ((&TeTrfcParms)) =
            RPTE_TE_TNL_FRR_INFO (pBaseRsvpTeTnlInfo)->u4Bandwidth;
        RPTE_TPARAM_PDR ((&TeTrfcParms)) =
            RPTE_TE_TNL_FRR_INFO (pBaseRsvpTeTnlInfo)->u4Bandwidth;

        RSVPTE_TNL_SETUP_PRIO (pRsvpTeTnlInfo) =
            RPTE_TE_TNL_FRR_INFO (pBaseRsvpTeTnlInfo)->u1SetPrio;
        RSVPTE_TNL_HLDNG_PRIO (pRsvpTeTnlInfo) =
            RPTE_TE_TNL_FRR_INFO (pBaseRsvpTeTnlInfo)->u1HoldPrio;
    }
    else if (RSVPTE_TNL_NODE_RELATION (pBaseRsvpTeTnlInfo) != RPTE_INGRESS)
    {
        /* PLR case handle */
        pRsvpTeFastReroute = &(PSB_RPTE_FRR_FAST_REROUTE
                               (RSVPTE_TNL_PSB (pBaseRsvpTeTnlInfo)));

        if ((pRsvpTeFastReroute->u1Flags != RPTE_ZERO) &&
            (pBaseRsvpTeTnlInfo->pTeTnlInfo->bIsBwProtDesired == TRUE))
        {
            f4TmpVar = OSIX_NTOHF (pRsvpTeFastReroute->fBandwth);
            RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
            RPTE_TPARAM_TBR ((&TeTrfcParms)) = (UINT4) f4TmpVar;
            RPTE_TPARAM_PDR ((&TeTrfcParms)) = (UINT4) f4TmpVar;
        }
        RSVPTE_TNL_SETUP_PRIO (pRsvpTeTnlInfo) = pRsvpTeFastReroute->u1SetPrio;
        RSVPTE_TNL_HLDNG_PRIO (pRsvpTeTnlInfo) = pRsvpTeFastReroute->u1HoldPrio;
    }

    RPTE_TRFC_PARAM_ROLE ((&TeTrfcParms)) = RPTE_INGRESS;
    RPTE_TRFC_PARAM_NUM_TNLS ((&TeTrfcParms)) = RSVPTE_ONE;
    RPTE_TNLRSRC_ROW_STATUS ((&TeTrfcParms)) = RPTE_ACTIVE;

    if (rpteTeCreateTrfcParams (RPTE_PROT, &pTeTrfcParms,
                                &TeTrfcParms) == RPTE_TE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Unable to Create Traffic Params for Bkp Path\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RptePhConstructBkpPathConsts : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RPTE_TNL_TRFC_PARMS (pRsvpTeTnlInfo) = pTeTrfcParms;
    RPTE_TNL_TRFC_PARM_INDEX (pRsvpTeTnlInfo) =
        RPTE_TRFC_PARAM_INDEX (pTeTrfcParms);
    if (RSVPTE_TNL_RSB (pBaseRsvpTeTnlInfo) == NULL)
    {
        rpteTeDeleteTrfcParams (RPTE_TNL_TRFC_PARMS (pRsvpTeTnlInfo));
        RPTE_TNL_TRFC_PARMS (pRsvpTeTnlInfo) = NULL;
        RSVPTE_DBG (RSVPTE_PH_PRCS, "PATH : RSB Null - Returning Failure \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RptePhConstructBkpPathConsts : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    /* Path specific method. Construct Detour object */
    PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
        au4PlrId[RPTE_ZERO] = IF_ENTRY_ADDR (PSB_OUT_IF_ENTRY
                                             (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));
    /* Link protection or node protection but failed case handle */
    if ((RpteUtlIsNodeProtDesired (pBaseRsvpTeTnlInfo) == RPTE_NO) ||
        ((RpteUtlIsNodeProtDesired (pBaseRsvpTeTnlInfo) == RPTE_YES) &&
         ((RPTE_FRR_CSPF_INFO (pBaseRsvpTeTnlInfo) & RPTE_FRR_CSPF_NODE_FAILED)
          == RPTE_FRR_CSPF_NODE_FAILED)))
    {
        PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
            au4AvoidNodeId[RPTE_ZERO] =
            RSB_NEXT_RSVP_HOP (RSVPTE_TNL_RSB (pBaseRsvpTeTnlInfo)).u4HopAddr;
    }
    else if (RpteUtlIsNodeProtDesired (pBaseRsvpTeTnlInfo) == RPTE_YES)
    {
        /* Node protection handle */
        pTnlArHopInfo = (tRsvpTeArHopInfo *)
            (TMO_SLL_First (&RSVPTE_TNL_OUTARHOP_LIST (pBaseRsvpTeTnlInfo)));
        if (pTnlArHopInfo == NULL)
        {
            rpteTeDeleteTrfcParams (RPTE_TNL_TRFC_PARMS (pRsvpTeTnlInfo));
            RPTE_TNL_TRFC_PARMS (pRsvpTeTnlInfo) = NULL;
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : ARHop Null - Returning Failure \n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhConstructBkpPathConsts : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        CONVERT_TO_INTEGER (pTnlArHopInfo->IpAddr.au1Ipv4Addr, u4TmpAddr);
        PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
            au4AvoidNodeId[RPTE_ZERO] = OSIX_NTOHL (u4TmpAddr);
    }
    if (IF_ENTRY_PLR_ID (pIfEntry) != RPTE_ZERO)
    {
        PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
            au4PlrId[RPTE_ZERO] = IF_ENTRY_PLR_ID (pIfEntry);
    }
    if (IF_ENTRY_AVOID_NODE_ID (pIfEntry) != RPTE_ZERO)
    {
        PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
            au4AvoidNodeId[RPTE_ZERO] = IF_ENTRY_AVOID_NODE_ID (pIfEntry);
    }
    PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
        au4PlrId[RPTE_ONE] = RPTE_ZERO;
    PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
        au4AvoidNodeId[RPTE_ONE] = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhConstructBkpPathConsts : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*
 * Function Name : RptePhProcessBkpPathMsg                                   
 * Description   : This routine processes PATH Message received on the backup
 *                 path. This function is called only if FRR is enabled.
 * Input(s)      : pPktMap - Points to the packet map structure.
 *                 pRsvpTeTnlInfo     - Pointer to the RsvpTeTnlInfo
 *                 ppRsvpTeBkpTnlInfo - Pointer to Pointer the Backup tunnel
 *                                      info
 *                 pu1NewBkpTnl       - New bkp tnl info.
 * Output(s)     : None                                                        
 * Return(s)     : RPTE_FAILURE/RPTE_SUCCESS
 *---------------------------------------------------------------------------*/
UINT1
RptePhProcessBkpPathMsg (tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                         tTeTnlInfo * pTeTnlInfo,
                         tRsvpTeTnlInfo ** ppRsvpTeBkpTnlInfo,
                         UINT1 *pu1NewBkpTnl)
{
    tRsvpTeTnlInfo     *pRsvpTeOrigTnlInfo = NULL;
    tRsvpTeTnlInfo     *pInRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pBkpRsvpTeTnlInfo = NULL;
    tTimeValues        *pTimeValues = NULL;
    tRsvpTeHopInfo     *pRsvpTeErHopInfo = NULL;
    tRsvpTeHopInfo     *pNewErHopInfo = NULL;
    UINT4               u4SenderAddr = RPTE_ZERO;
    UINT4               u4ExtTnlId = RPTE_ZERO;
    UINT4               u4InSenderAddr = RPTE_ZERO;
    UINT4               u4DestAddr = RPTE_ZERO;
    UINT4               u4InErHopAddr = RPTE_ZERO;
    UINT4               u4NewErHopAddr = RPTE_ZERO;
    UINT4               u4PlrAddr = RPTE_ZERO;
    UINT2               u2Len = RPTE_ZERO;
    UINT2               u2Cnt = RPTE_ZERO;
    BOOL1               bIsMerged = RPTE_FALSE;
    BOOL1               bOuterLoop = RPTE_FALSE;
    UINT1               u1DetourGrpId = RPTE_ZERO;
    UINT1               u1IsEgrOfHop = RPTE_FALSE;

    CONVERT_TO_INTEGER (PKT_MAP_RPTE_ST_TNL_SNDR_ADDR (pPktMap), u4SenderAddr);
    u4SenderAddr = OSIX_NTOHL (u4SenderAddr);

    CONVERT_TO_INTEGER (PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap), u4ExtTnlId);
    u4ExtTnlId = OSIX_NTOHL (u4ExtTnlId);

    CONVERT_TO_INTEGER (PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap), u4DestAddr);
    u4DestAddr = OSIX_NTOHL (u4DestAddr);

    /* Following contd. will occur when facility uses 
     * bypass tnl for the refreshment */
    if (((PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap) != NULL) &&
         ((FRR_FAST_REROUTE (PKT_MAP_RPTE_FAST_REROUTE_OBJ (pPktMap))).u1Flags
          == TE_TNL_FRR_FACILITY_METHOD)))
    {
        *ppRsvpTeBkpTnlInfo = pRsvpTeTnlInfo;
        RSVPTE_DBG (RSVPTE_FRR_DBG,
                    "PATH : Validate Path Msg Objs On Bkp Path  "
                    "- Pkt dropped..\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessBkpPathMsg : INTMD-EXIT \n");
        return RPTE_SUCCESS;
    }
    /* Validating the Backup PATH Message */
    if (RpteFrrValidateBkpPathMsg (pPktMap, pRsvpTeTnlInfo) == RPTE_FAILURE)
    {
        /* Sending PATH Error Message */
        RptePvmSendPathErr (pPktMap, UNKNOWN_OBJECT_CLASS, DEFAULT_ERROR_VALUE);
        RSVPTE_DBG (RSVPTE_FRR_DBG,
                    "PATH : Validate Path Msg Objs On Bkp Path  "
                    "- Pkt dropped..\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessBkpPathMsg : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    pInRsvpTeTnlInfo = pRsvpTeOrigTnlInfo = pRsvpTeTnlInfo;

    /* Only the PATH Message that has the same outgoing interface and
     * the next hop are eligible for merging in case of both MP and DMP. 
     * This is done in the following loop. */
    pNewErHopInfo = (tRsvpTeHopInfo *)
        (TMO_SLL_First (&pPktMap->ErObj.ErHopList));

    if (pNewErHopInfo != NULL)
    {
        do
        {
            if (RpteIsLsrPartOfErHop (pNewErHopInfo, &u1IsEgrOfHop) ==
                RPTE_FALSE)
            {
                break;
            }
        }
        while ((pNewErHopInfo = (tRsvpTeHopInfo *)
                (TMO_SLL_Next (&pPktMap->ErObj.ErHopList,
                               &(pNewErHopInfo->NextHop)))) != NULL);
    }

    if (pNewErHopInfo != NULL)
    {
        MEMCPY ((UINT1 *) (&u4NewErHopAddr), &(pNewErHopInfo->IpAddr),
                RSVPTE_IPV4ADR_LEN);
        u4NewErHopAddr = OSIX_NTOHL (u4NewErHopAddr);
    }

    if (RPTE_IS_HEADEND (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
    {
        RSVPTE_DBG (RSVPTE_FRR_DBG,
                    "Ingress: Bkp tnl tries to travel via prot tnl"
                    " - ignored\n");
        return RPTE_FAILURE;
    }
    else if (RPTE_IS_TAILEND (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
    {
        /* We have reached the tail-end of the tunnel, 
         * if any backup path tries to cross the protected tunnel's tail-end, 
         * please ignore.*/
        if (pNewErHopInfo != NULL)
        {
            RptePvmUtlSendPathErr (pPktMap, RPTE_ROUTE_PROB,
                                   RPTE_BAD_STRICT_NODE);
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "Egress: Bkp tnl tries to travel via prot tnl"
                        " - ignored\n");
            return RPTE_FAILURE;
        }
    }
    do
    {
        if (pNewErHopInfo == NULL)
        {
            if (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeOrigTnlInfo) ==
                RPTE_FRR_PROTECTED_TNL)
            {
                /* Complete Er-Hops are not provided from Ingress so, 
                 * taking protected tnls outgoing interface as a backup tnls 
                 * outgoing interface. */
                bIsMerged = RPTE_TRUE;
            }
            break;
        }
        pRsvpTeTnlInfo = pInRsvpTeTnlInfo;
        if (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeTnlInfo) == NULL)
        {
            continue;
        }
        pRsvpTeErHopInfo = (tRsvpTeHopInfo *) TMO_SLL_First
            (&RSVPTE_TNL_OUTERHOP_LIST (pRsvpTeTnlInfo));

        if (pRsvpTeErHopInfo == NULL)
        {
            continue;
        }

        MEMCPY ((UINT1 *) (&u4InErHopAddr), &(pRsvpTeErHopInfo->IpAddr),
                RSVPTE_IPV4ADR_LEN);

        u4InErHopAddr = OSIX_NTOHL (u4InErHopAddr);

        if (u4InErHopAddr != u4NewErHopAddr)
        {
            continue;
        }
        /* Group ID of the Merged Detour is taken. This will be assigned to 
         * the newly Merged Detour. 
         * This will be used to duplicate the RESV Messages received to only 
         * the Group having same ID. */
        u1DetourGrpId = RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTnlInfo);
        bIsMerged = RPTE_TRUE;
        break;
    }
    while ((pInRsvpTeTnlInfo = (RPTE_FRR_IN_TNL (pInRsvpTeTnlInfo))) != NULL);

    /* Scanning the Inner Pointers of pRsvpTeTnlInfo to classify whether
     * the PATH Message is refresh or not */
    /* If the PATH Message is a new, 
     * The Following Scanning is done */
    pInRsvpTeTnlInfo = pRsvpTeTnlInfo = pRsvpTeOrigTnlInfo;
    *pu1NewBkpTnl = RPTE_YES;
    do
    {
        if (RSVPTE_TNL_PSB (pInRsvpTeTnlInfo) == NULL)
        {
            continue;
        }
        CONVERT_TO_INTEGER (((PSB_RPTE_SNDR_TMP
                              (RSVPTE_TNL_PSB (pInRsvpTeTnlInfo))).
                             TnlSndrAddr), u4InSenderAddr);
        u4InSenderAddr = OSIX_NTOHL (u4InSenderAddr);
        pRsvpTeTnlInfo = pInRsvpTeTnlInfo;

        if (FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap)).u2Length !=
            RPTE_ZERO)
        {
            /* Sender Template specific method with DETOUR object */
            /* Getting the count of PLR ID - AVOID NODE ID Pairs from
             * DETOUR Object stored in PSB */
            if ((PSB_RPTE_FRR_DETOUR
                 (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).au4PlrId[RPTE_ZERO])
                == RPTE_ZERO)
            {
                continue;
            }
            for (u2Len = RPTE_ZERO;
                 ((u2Len < RPTE_MAX_DETOUR_IDS) &&
                  (PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                   au4PlrId[u2Len] != RPTE_ZERO)); u2Len++);
            u4PlrAddr =
                OSIX_NTOHL (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap).RsvpTeDetour.
                            au4PlrId[RPTE_ZERO]);
            /* Assumption: */
            /* First PLR id in DETOUR object and sender address in 
             * SENDER_TEMPLATE both are same
             * i.e u4PlrAddr = u4SenderAddr */
            /* If the Sender Address in SENDER_TEMPLATE object
             * and One of PLR - ID matches then we have 
             * received PATH Message for existing tunnel. */

            for (u2Cnt = RPTE_ZERO; u2Cnt < u2Len; u2Cnt++)
            {
                u4InSenderAddr =
                    PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                    au4PlrId[u2Cnt];
                if (u4SenderAddr == u4ExtTnlId)
                {
                    /* Path Specific Method */
                    if ((u4InSenderAddr == RPTE_ZERO) ||
                        (u4PlrAddr != u4InSenderAddr))
                    {
                        bOuterLoop = RPTE_TRUE;
                        continue;
                    }
                    else
                    {
                        bOuterLoop = RPTE_FALSE;
                        break;
                    }
                }
                else
                {
                    /* Sender template specific method handle */
                    if ((u4InSenderAddr == RPTE_ZERO) ||
                        (u4SenderAddr != u4InSenderAddr))
                    {
                        bOuterLoop = RPTE_TRUE;
                        continue;
                    }
                    else
                    {
                        bOuterLoop = RPTE_FALSE;
                        break;
                    }
                }
            }
        }
        else if ((u4SenderAddr != u4ExtTnlId)
                 && (u4SenderAddr != u4InSenderAddr))
        {
            bOuterLoop = RPTE_FALSE;
            /* Sender template specific method / 
             * Sender template with detour object */
            continue;
        }
        if (bOuterLoop == RPTE_TRUE)
        {
            continue;
        }
        *pu1NewBkpTnl = RPTE_NO;
        break;
    }
    while ((pInRsvpTeTnlInfo = (RPTE_FRR_IN_TNL (pInRsvpTeTnlInfo))) != NULL);

    /* PATH Message is newly received on the backup path,
     * So, creating a tunnel entry but it is not added to RB Tree */
    if (*pu1NewBkpTnl == RPTE_YES)
    {
        /* RFC 4090 sec 7. Merge Node Behavior */

        if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeOrigTnlInfo) ==
             RPTE_FRR_PROTECTED_TNL) &&
            (!(RPTE_IS_MP (RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo)))) &&
            (bIsMerged == RPTE_TRUE))
        {
            /* The Previous state of the node is UNKNOWN or PLR. We have 
             * received the PATH Message on the backup path. So, Tunnel 
             * Instance created will be 65535. */
            RSVPTE_TNL_INSTANCE (pTeTnlInfo) =
                RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo);
            RSVPTE_TNL_PRIMARY_INSTANCE (pTeTnlInfo) =
                RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo);
            RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo)++;
            RSVPTE_DBG (RSVPTE_FRR_DBG, "Node state is MP \n");
            RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo) |= RPTE_FRR_NODE_STATE_MP;
        }
        else if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeOrigTnlInfo) ==
                  RPTE_FRR_BACKUP_TNL) &&
                 (!(RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo)))) &&
                 (bIsMerged == RPTE_TRUE))
        {
            /* The Previous state of the node is UNKNOWN. We have 
             * received the PATH Message on the secondary backup path. 
             * So, Tunnel Instance created will be 
             * Instance of Primary path (65535) + 1. */
            RSVPTE_TNL_INSTANCE (pTeTnlInfo) =
                RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo);
            RSVPTE_TNL_PRIMARY_INSTANCE (pTeTnlInfo) =
                RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo);
            RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo)++;
            if (RSVPTE_FRR_DETOUR_MERGING_CAPABLE (gpRsvpTeGblInfo) ==
                RPTE_TRUE)
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG, "Node state is DMP \n");
                RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo) |=
                    RPTE_FRR_NODE_STATE_DMP;
            }
        }
        else
        {
            /* The Previous state of the node is MP or DMP. We have 
             * received the PATH Message on the secondary backup path. 
             * So, Tunnel Instance created will be 
             * Previous Tunnel Instance + 1.*/
            if ((RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (pRsvpTeTnlInfo))) <
                RPTE_DETOUR_TNL_INSTANCE)
            {
                RSVPTE_TNL_INSTANCE (pTeTnlInfo) =
                    RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo);
                RSVPTE_TNL_PRIMARY_INSTANCE (pTeTnlInfo) =
                    RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo);
                RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo)++;
            }
            else
            {
                RSVPTE_TNL_INSTANCE (pTeTnlInfo) = ((RSVPTE_TNL_INSTANCE
                                                     (RPTE_TE_TNL
                                                      (pRsvpTeTnlInfo))) +
                                                    RPTE_ONE);
                RSVPTE_TNL_PRIMARY_INSTANCE (pTeTnlInfo) =
                    ((RSVPTE_TNL_PRIMARY_INSTANCE
                      (RPTE_TE_TNL (pRsvpTeTnlInfo))) + RPTE_ONE);
            }
        }
        if (RpteCreateNewRsvpteTunnel
            (OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID (pPktMap)),
             RSVPTE_TNL_INSTANCE (pTeTnlInfo), u4ExtTnlId,
             u4DestAddr, pTeTnlInfo, &pBkpRsvpTeTnlInfo,
             RPTE_FALSE) == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "PATH : Bkp Path Create Tnl Tbl - failed "
                        "- Pkt dropped..\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhProcessBkpPathMsg : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
        if (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo)))
        {
            RPTE_FRR_DETOUR_GRP_ID (pBkpRsvpTeTnlInfo) = u1DetourGrpId;
        }
        /* Merging the New backup path messages 
         * as per RFC 4090 sec 7.1.1 and 7.1.2 */
        RPTE_FRR_IN_TNL (pRsvpTeTnlInfo) = pBkpRsvpTeTnlInfo;
        RPTE_FRR_BASE_TNL (pBkpRsvpTeTnlInfo) = pRsvpTeOrigTnlInfo;
        RPTE_FRR_TNL_INFO_TYPE (pBkpRsvpTeTnlInfo) = RPTE_FRR_BACKUP_TNL;
        RPTE_IS_FRR_CAPABLE (pBkpRsvpTeTnlInfo) = RPTE_TRUE;
        pRsvpTeTnlInfo = pBkpRsvpTeTnlInfo;

        if (bIsMerged == RPTE_FALSE)
        {
            RSVPTE_FRR_GBL_DETOUR_GRP_ID (gpRsvpTeGblInfo)++;
            RPTE_FRR_DETOUR_GRP_ID (pRsvpTeTnlInfo)
                = RSVPTE_FRR_GBL_DETOUR_GRP_ID (gpRsvpTeGblInfo);
            RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTnlInfo) = RPTE_TRUE;
        }
    }
    /* PATH Refresh received on the backup path */
    if (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeOrigTnlInfo) == RPTE_FRR_PROTECTED_TNL)
    {
        /* Refreshing the protected tunnel always as we don't know when the 
         * protected link goes down.*/
        pTimeValues = &TIME_VALUES_OBJ (PKT_MAP_TIME_VALUES_OBJ (pPktMap));
        PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pRsvpTeOrigTnlInfo)) =
            RpteUtlComputeLifeTime (PKT_MAP_IF_ENTRY (pPktMap),
                                    OSIX_NTOHL (TIME_VALUES_PERIOD
                                                (pTimeValues)));
        PSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_PSB (pRsvpTeOrigTnlInfo)) =
            OSIX_NTOHL (TIME_VALUES_PERIOD (pTimeValues));
        RSVPTE_DBG (RSVPTE_FRR_DBG,
                    "Protected tnl refeshed upon backup path refresh \n");
    }
    *ppRsvpTeBkpTnlInfo = pRsvpTeTnlInfo;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhProcessBkpPathMsg : EXIT \n");
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*
 * Function Name : RptePhHandleIfChangeForFrr                                   
 * Description   : This routine processes the Interface change event for 
 *                 RSVP fast reroute. This function is called only if FRR is 
 *                 enabled.
 * Input(s)      : pRsvpTeTnlInfo - Points to the rsvp tnl info.
 *                 u1Status - Interface status
 * Output(s)     : None                                                        
 * Return(s)     : None
*---------------------------------------------------------------------------*/
VOID
RptePhHandleIfChangeForFrr (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 u1Status)
{
    tIfEntry           *pIfEntry = NULL;

    RSVPTE_DBG (RSVPTE_NBR_DBG, "RptePhHandleIfChangeForFrr : ENTRY \n");

    if (u1Status == RPTE_DISABLED)
    {
        if ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_IN_USE)
            == LOCAL_PROT_IN_USE)
        {
            return;
        }

        RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) |= LOCAL_PROT_IN_USE;

        if ((RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL) &&
            (PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
             u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD))
        {
            /* Facility case handling */
            /* Double stacking ILM Create operation */
            /*----------------
             *                | 
             *ByPass Tnl Label|
             *                |
             *----------------
             *                | 
             *Prot Tnl Label  |
             *                |
             *----------------*/

            if (RPTE_IS_HEADEND_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
            {
                RSVPTE_DBG (RSVPTE_NBR_DBG, "RptePhHandleIfChangeForFrr : "
                            "Facility - Headend+PLR link/node down handling \n");

                RPTE_TE_TNL (pRsvpTeTnlInfo)->bFrrDnStrLblChg = RPTE_TRUE;
                rpteTeFrrTnlAppDown (RPTE_TE_TNL (pRsvpTeTnlInfo));

                /* Since the L2VPN PW is created over the tunnel,
                 * Tunnel reprogramming needs to be done once L2VPN
                 * module deletes the older tunnel
                 */
                if (TE_TNL_IN_USE_BY_VPN (pRsvpTeTnlInfo->pTeTnlInfo) &
                    TE_TNL_INUSE_BY_L2VPN)
                {
                    RpteStartEventToL2vpnTmr (pRsvpTeTnlInfo);
                }
                else
                {
                    RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE, RSVPTE_ZERO,
                                              pRsvpTeTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);
                    if (RpteIfChangeForProtTnlLinkDown (pRsvpTeTnlInfo) ==
                        RPTE_FAILURE)
                    {
                        RSVPTE_DBG (RSVPTE_NBR_DBG,
                                    "RpteIfChangeForProtTnlLinkDown : FTN Programming"
                                    "FAILED, EXIT \n");
                    }
                    RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) = RPTE_TRUE;
                    MEMSET (pRsvpTeTnlInfo->pTeTnlInfo->au1NextHopMac,
                            MPLS_ZERO, MAC_ADDR_LEN);
                    RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE, RSVPTE_ZERO,
                                              pRsvpTeTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);
                    rpteTeFrrTnlAppUp (RPTE_TE_TNL (pRsvpTeTnlInfo));
                    RPTE_TE_TNL (pRsvpTeTnlInfo)->bFrrDnStrLblChg = RPTE_FALSE;
                }
            }
            else if (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
            {
                RSVPTE_DBG (RSVPTE_NBR_DBG, "RptePhHandleIfChangeForFrr : "
                            "Facility - PLR link/node down handling \n");
                RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                          pRsvpTeTnlInfo,
                                          GMPLS_SEGMENT_FORWARD);
                if (RpteIfChangeForProtTnlLinkDown (pRsvpTeTnlInfo) ==
                    RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_NBR_DBG,
                                "RpteIfChangeForProtTnlLinkDown : "
                                "ILM Programming FAILED, EXIT \n");
                }

                RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) = RPTE_TRUE;
                RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_CREATE,
                                          RSVPTE_ZERO, pRsvpTeTnlInfo,
                                          GMPLS_SEGMENT_FORWARD);
            }

            /* As per RFC 4090 Section 7.2 ,we MUST not clear the PSB and RSB immediately */

            RSVPTE_FRR_ACT_PROT_IF_NUM (gpRsvpTeGblInfo)++;
            RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS
                (RPTE_TE_TNL (pRsvpTeTnlInfo)) = TE_TNL_FRR_PROT_STATUS_ACTIVE;
        }
        else
        {
            /* One-To-One case handling */
            if (RPTE_IS_HEADEND_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
            {
                RSVPTE_DBG (RSVPTE_NBR_DBG, "RptePhHandleIfChangeForFrr : "
                            "OneToOne - Headend+PLR link/node down handling \n");
                RpteFrrHandleHeadEndPlrFailure (pRsvpTeTnlInfo);

            }
            else if (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
            {
                RSVPTE_DBG (RSVPTE_NBR_DBG, "RptePhHandleIfChangeForFrr : "
                            "OneToOne - PLR link/node down handling \n");

                RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                          pRsvpTeTnlInfo,
                                          GMPLS_SEGMENT_FORWARD);

                MEMCPY (&
                        (RSVPTE_TNL_INLBL
                         (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo))),
                        &RSVPTE_TNL_INLBL (pRsvpTeTnlInfo), sizeof (uLabel));
                RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_CREATE,
                                          RSVPTE_ZERO,
                                          RPTE_FRR_OUT_TNL_INFO
                                          (pRsvpTeTnlInfo),
                                          GMPLS_SEGMENT_FORWARD);
            }

            RpteUtlSetDetourStatus (pRsvpTeTnlInfo, RPTE_TRUE);

            RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) |= RPTE_FRR_NODE_STATE_DNLOST;
        }

        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_IN_USE;

        RSVPTE_DBG (RSVPTE_NBR_DBG,
                    "RptePhHandleIfChangeForFrr : LOCAL_PROT_IN_USE Set \n");

        if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS)
        {
            RpteFrrConstructAndSendPathErr (pRsvpTeTnlInfo);
        }
        if (RPTE_IS_HEADEND_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
        {
            RpteStartFrrGblRevertTimer (pRsvpTeTnlInfo);
        }
        if ((RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL) &&
            (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) != NULL))
        {
            /* PSB Time To Die updation */
            pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
            PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                RpteUtlComputeLifeTime (pIfEntry,
                                        (2 *
                                         (IF_ENTRY_REFRESH_INTERVAL
                                          (pIfEntry))));
            PSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                 DEFAULT_CONV_TO_SECONDS);
        }

        if ((RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL) &&
            (RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)) != NULL))
        {
            /* RSB Time To Die updation */
            pIfEntry = RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pRsvpTeTnlInfo));
            RSB_TIME_TO_DIE (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)) =
                RpteUtlComputeLifeTime (pIfEntry,
                                        (2 *
                                         (IF_ENTRY_REFRESH_INTERVAL
                                          (pIfEntry))));
            RSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)) =
                (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                 DEFAULT_CONV_TO_SECONDS);
        }
    }
    else if (RSVPTE_FRR_REVERTIVE_MODE (gpRsvpTeGblInfo) ==
             RPTE_FRR_REVERTIVE_LOCAL)
    {
        if ((~(RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo)) &
             LOCAL_PROT_IN_USE) == LOCAL_PROT_IN_USE)
        {
            return;
        }

        /* Link/Node protection local revertive is not taken care here, 
         * instead upon receiving the RESV message on protected path this 
         * local revertive will be taken care */

        if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL)
            && (PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                u1Flags != RSVPTE_TNL_FRR_FACILITY_METHOD))
        {
            return;
        }

        /*RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) = RPTE_FALSE; */
        RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) &= (UINT1) (~(LOCAL_PROT_IN_USE));
        pRsvpTeTnlInfo->u2EroSize = RPTE_ZERO;
        RpteUtlCalculateEROSize (pRsvpTeTnlInfo);

        RSVPTE_DBG (RSVPTE_NBR_DBG, "Updating SNDR TMP object\n");
        /* Updating Sender Template Obj as tunnel's src addr */
        MEMCPY (&PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                TnlSndrAddr,
                &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo))),
                RSVPTE_IPV4ADR_LEN);

        if (RpteGetIfEntry ((INT4) PSB_FRR_FAC_LOC_REVERT_PROT_RSVP_IF
                            (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                            &pIfEntry) == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhHandleIfChangeForFrr : RpteGetIfEntry failed\n");
        }
        PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = pIfEntry;

        pRsvpTeTnlInfo->b1IsReversionTriggered = RPTE_TRUE;

        /*Sending path refresh for early recovery */
        RptePhPathRefresh (pRsvpTeTnlInfo);

    }

    RSVPTE_DBG (RSVPTE_NBR_DBG, "RptePhHandleIfChangeForFrr : EXIT \n");
    return;
}

/*----------------------------------------------------------------------------*/
/*                                                                            
 * Function Name   : RpteFrrHandleHeadEndPlrFailure                                        
 * Description     : Handles the PLR+HEADEND failure, 
 *                   just switches with the backup tnl path 
 * Input (s)       : pRsvpTeTnlInfo - RSVPTE tunnle info
 * Output (s)      : None                   
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteFrrHandleHeadEndPlrFailure (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteFrrHandleHeadEndPlrFailure : ENTRY \n");

    rpteTeFrrTnlAppDown (RPTE_TE_TNL (pRsvpTeTnlInfo));

    /* Keep the original tnl interface index */
    RPTE_TE_TNL (pRsvpTeTnlInfo)->u4OrgTnlIfIndex =
        RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlIfIndex;

    /* Use the backup path tnl interface for tnl's traffic */
    RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlIfIndex =
        RPTE_TE_TNL (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo))->u4TnlIfIndex;

    RSVPTE_DBG2 (RSVPTE_NBR_DBG,
                 "Tnl if index changed from %d to %d\n",
                 RPTE_TE_TNL (pRsvpTeTnlInfo)->u4OrgTnlIfIndex,
                 RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlIfIndex);

    /* Keep the original tnl XC index index */
    RPTE_TE_TNL (pRsvpTeTnlInfo)->u4OrgTnlXcIndex =
        RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlXcIndex;

    /* Use the backup path tnl XC index for tnl's traffic */
    MplsDbUpdateTunnelXCIndex
        (RPTE_TE_TNL (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo)),
         NULL,
         RPTE_TE_TNL (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo))->u4TnlXcIndex);

    RSVPTE_DBG2 (RSVPTE_NBR_DBG,
                 "Tnl XC changed from %d to %d\n",
                 RPTE_TE_TNL (pRsvpTeTnlInfo)->u4OrgTnlXcIndex,
                 RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlXcIndex);

    MEMCPY (RPTE_TE_TNL (pRsvpTeTnlInfo)->au1BkpNextHopMac,
            RPTE_TE_TNL (pRsvpTeTnlInfo)->au1NextHopMac, MAC_ADDR_LEN);

    MEMCPY (RPTE_TE_TNL (pRsvpTeTnlInfo)->au1NextHopMac,
            RPTE_TE_TNL (RPTE_FRR_OUT_TNL_INFO
                         (pRsvpTeTnlInfo))->au1NextHopMac, MAC_ADDR_LEN);
    MplsGetOutNextHopFrmXcIndex (RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlXcIndex,
                                 &RPTE_TE_TNL (pRsvpTeTnlInfo)->
                                 u4BkpNextHopAddr);
    /* Set the In use by L2VPN status to Backup tunnel also */
    pRsvpTeTnlInfo->pOutFrrProtTnlInfo->pTeTnlInfo->u1TnlInUseByVPN
        = pRsvpTeTnlInfo->pTeTnlInfo->u1TnlInUseByVPN;

    /* Indicate the applications to use new tnl interface 
     * for their traffic */
    rpteTeFrrTnlAppUp (RPTE_TE_TNL (pRsvpTeTnlInfo));
    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteFrrHandleHeadEndPlrFailure : EXIT \n");
    return RPTE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
/*                                                                            
 * Function Name   : RpteFrrHandlePlrLocRev                                        
 * Description     : Handles the local revertive failure 
 *
 * Input (s)       : pRsvpTeTnlInfo - RSVPTE tunnle info
 * Output (s)      : None                   
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteFrrHandlePlrLocRev (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteFrrHandlePlrLocRev : ENTRY \n");

    /* Local revertive behavior is configured in addition to Global
     * revertive behavior. Data traffic is going to be reverted back to
     * the original path. So, stop the global revertive timer that might
     * have been started. */
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_GBL_REVERT_TIMER (pRsvpTeTnlInfo));

    if ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
         u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD))
    {
        /* Facility case handling */
        if (RPTE_IS_HEADEND_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
        {
            RSVPTE_DBG (RSVPTE_NBR_DBG, "RptePhHandleIfChangeForFrr : "
                        "Facility-Local revertive - "
                        "Headend+PLR link/node up handling \n");

            RPTE_TE_TNL (pRsvpTeTnlInfo)->bFrrDnStrLblChg = RPTE_TRUE;

            rpteTeFrrTnlAppDown (RPTE_TE_TNL (pRsvpTeTnlInfo));

            /* Since the L2VPN PW is created over the tunnel,
             * Tunnel reprogramming needs to be done once L2VPN
             * module deletes the older tunnel
             * */
            if (TE_TNL_IN_USE_BY_VPN (pRsvpTeTnlInfo->pTeTnlInfo) &
                TE_TNL_INUSE_BY_L2VPN)
            {
                RpteStartEventToL2vpnTmr (pRsvpTeTnlInfo);
            }
            else
            {
                RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE, RSVPTE_ZERO,
                                          pRsvpTeTnlInfo,
                                          GMPLS_SEGMENT_FORWARD);
                if (RpteIfChangeForProtTnlLinkUp (pRsvpTeTnlInfo) ==
                    RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_NBR_DBG,
                                "RpteIfChangeForProtTnlLinkUp : "
                                "FTN Programming FAILED, EXIT \n");
                }
                RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) = RPTE_FALSE;
                MEMSET (pRsvpTeTnlInfo->pTeTnlInfo->au1NextHopMac, MPLS_ZERO,
                        MAC_ADDR_LEN);
                RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE, RSVPTE_ZERO,
                                          pRsvpTeTnlInfo,
                                          GMPLS_SEGMENT_FORWARD);

                if (TE_TNL_OPER_STATUS (RPTE_TE_TNL (pRsvpTeTnlInfo)) ==
                    TE_OPER_UP)
                {
                    rpteTeFrrTnlAppUp (RPTE_TE_TNL (pRsvpTeTnlInfo));
                }
            }
        }
        else if (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
        {
            RSVPTE_DBG (RSVPTE_NBR_DBG, "RptePhHandleIfChangeForFrr : "
                        "Facility-Local revertive - "
                        "PLR link/node up handling \n");

            RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                      pRsvpTeTnlInfo, GMPLS_SEGMENT_FORWARD);
            if (RpteIfChangeForProtTnlLinkUp (pRsvpTeTnlInfo) == RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_NBR_DBG,
                            "RpteIfChangeForProtTnlLinkUp : "
                            "FTN Programming FAILED, EXIT \n");
            }

            RPTE_FRR_STACK_BIT (pRsvpTeTnlInfo) = RPTE_FALSE;
            RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_CREATE,
                                      RSVPTE_ZERO, pRsvpTeTnlInfo,
                                      GMPLS_SEGMENT_FORWARD);
        }

        RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS
            (RPTE_TE_TNL (pRsvpTeTnlInfo)) = TE_TNL_FRR_PROT_STATUS_READY;
        if (pRsvpTeTnlInfo->pFrrOutTePathListInfo != NULL)
        {
            rpteTeDeleteHopListInfo (pRsvpTeTnlInfo->pFrrOutTePathListInfo);
            pRsvpTeTnlInfo->pFrrOutTePathListInfo = NULL;
            /* Assumed that only one hop is removed since it is node 
             * protection 
             * */
            if (RSVPTE_TE_TNL_FRR_PROT_TYPE (RPTE_TE_TNL (pRsvpTeTnlInfo)) ==
                RPTE_TE_TNL_FRR_PROTECTION_NODE)
            {
                pRsvpTeTnlInfo->u2EroSize = (UINT2) (pRsvpTeTnlInfo->u2EroSize +
                                                     ERO_TYPE_IPV4_LEN);
            }
        }
    }
    else
    {
        /* One-To-One case handling */
        if (RPTE_IS_HEADEND_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
        {
            RSVPTE_DBG (RSVPTE_NBR_DBG, "RptePhHandleIfChangeForFrr : "
                        "OneToOne-Local revertive - "
                        "Headend+PLR link/node up handling \n");
            RpteFrrHandleHeadEndPlrLocRev (pRsvpTeTnlInfo);

        }
        else if (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
        {
            RSVPTE_DBG (RSVPTE_NBR_DBG, "RptePhHandleIfChangeForFrr : "
                        "OneToOne-Local revertive - "
                        "PLR link/node up handling \n");
            RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                      RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo),
                                      GMPLS_SEGMENT_FORWARD);
            RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_CREATE,
                                      RSVPTE_ZERO, pRsvpTeTnlInfo,
                                      GMPLS_SEGMENT_FORWARD);
        }

        RpteUtlSetDetourStatus (pRsvpTeTnlInfo, RPTE_FALSE);

        RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) &=
            (UINT1) ~(RPTE_FRR_NODE_STATE_DNLOST);
    }

    RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) &= (UINT1) (~(LOCAL_PROT_IN_USE));
    RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) |= (UINT1) LOCAL_PROT_AVAIL;
    pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_AVAIL;

    /* Updating the Statistical Values */
    if (RSVPTE_FRR_SWITCH_OVER_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
    {
        RSVPTE_FRR_SWITCH_OVER_NUM (gpRsvpTeGblInfo)--;
    }

    if (RSVPTE_FRR_ACT_PROT_LSP_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
    {
        RSVPTE_FRR_ACT_PROT_LSP_NUM (gpRsvpTeGblInfo)--;
    }

    if (RSVPTE_FRR_ACT_PROT_TUN_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
    {
        RSVPTE_FRR_ACT_PROT_TUN_NUM (gpRsvpTeGblInfo)--;
    }

    if (RSVPTE_FRR_ACT_PROT_IF_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
    {
        RSVPTE_FRR_ACT_PROT_IF_NUM (gpRsvpTeGblInfo)--;
    }
    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteFrrHandlePlrLocRev : EXIT \n");
}

/*----------------------------------------------------------------------------*/
/*                                                                            
 * Function Name   : RpteFrrHandleHeadEndPlrLocRev                                        
 * Description     : Handles HeadEnd+PLR the local revertive failure 
 *
 * Input (s)       : pRsvpTeTnlInfo - RSVPTE tunnle info
 * Output (s)      : None                   
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteFrrHandleHeadEndPlrLocRev (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    rpteTeFrrTnlAppDown (RPTE_TE_TNL (pRsvpTeTnlInfo));

    RSVPTE_DBG2 (RSVPTE_NBR_DBG,
                 "PLR local revertive: Tnl if index to be changed from %d to %d\n",
                 RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlIfIndex,
                 RPTE_TE_TNL (pRsvpTeTnlInfo)->u4OrgTnlIfIndex);

    if ((pRsvpTeTnlInfo->pOutFrrProtTnlInfo != NULL) &&
        (pRsvpTeTnlInfo->pOutFrrProtTnlInfo->pTeTnlInfo != NULL))
    {
        pRsvpTeTnlInfo->pOutFrrProtTnlInfo->pTeTnlInfo->u1TnlInUseByVPN
            &= (UINT1) ~(TE_TNL_INUSE_BY_L2VPN);
    }

    /* Keep the original tnl interface index */
    RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlIfIndex =
        RPTE_TE_TNL (pRsvpTeTnlInfo)->u4OrgTnlIfIndex;

    RPTE_TE_TNL (pRsvpTeTnlInfo)->u4OrgTnlIfIndex = RPTE_ZERO;

    RSVPTE_DBG2 (RSVPTE_NBR_DBG,
                 "PLR local revertive: Tnl XC to be changed from %d to %d\n",
                 RPTE_TE_TNL (pRsvpTeTnlInfo)->u4TnlXcIndex,
                 RPTE_TE_TNL (pRsvpTeTnlInfo)->u4OrgTnlXcIndex);

    /* Keep the original tnl XC index index */
    MplsDbUpdateTunnelXCIndex (RPTE_TE_TNL (pRsvpTeTnlInfo), NULL,
                               RPTE_TE_TNL (pRsvpTeTnlInfo)->u4OrgTnlXcIndex);

    RPTE_TE_TNL (pRsvpTeTnlInfo)->u4OrgTnlXcIndex = RPTE_ZERO;
    RPTE_TE_TNL (pRsvpTeTnlInfo)->u4BkpNextHopAddr = RPTE_ZERO;

    MEMCPY (RPTE_TE_TNL (pRsvpTeTnlInfo)->au1NextHopMac,
            RPTE_TE_TNL (pRsvpTeTnlInfo)->au1BkpNextHopMac, MAC_ADDR_LEN);

    MEMSET (RPTE_TE_TNL (pRsvpTeTnlInfo)->au1BkpNextHopMac,
            RSVPTE_ZERO, MAC_ADDR_LEN);

    rpteTeFrrTnlAppUp (RPTE_TE_TNL (pRsvpTeTnlInfo));
    return RPTE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
/*                                                                            
 * Function Name   : RpteIfChangeForProtTnlLinkDown                                        
 * Description     : This function creates the MPLS tunnel interface 
 *                   based on the bypass tunnel path
 * Input (s)       : pRsvpTeTnlInfo - RSVPTE tunnle info
 * Output (s)      : None                   
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteIfChangeForProtTnlLinkDown (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4L3If = RSVPTE_ZERO;
    UINT4               u4MplsTnlIfIndex = RSVPTE_ZERO;

    if ((RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL) ||
        (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) == NULL))
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "RSVP if entry not exist \n");
        return RPTE_FAILURE;
    }
    PSB_FRR_FAC_LOC_REVERT_PROT_RSVP_IF (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))
        = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))->u4IfIndex;
    PSB_FRR_FAC_LOC_REVERT_PROT_TNL_IF (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))
        = TE_TNL_IFINDEX (RPTE_TE_TNL (pRsvpTeTnlInfo));

    if ((RSVPTE_TNL_PSB (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo)) == NULL) ||
        (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB
                           (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo))) == NULL))
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "\n Out tunnel RSVP if entry not exist \n");
        return RPTE_FAILURE;
    }

    pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (RPTE_FRR_OUT_TNL_INFO
                                                 (pRsvpTeTnlInfo)));
    if (CfaUtilGetL3IfFromMplsIf (IF_ENTRY_IF_INDEX (pIfEntry),
                                  &u4L3If, TRUE) == CFA_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "RESV: MplsMlibUpdate: L3 Interface"
                    "from MPLS Interface get failed...\n");
        return RPTE_FAILURE;
    }
    if (CfaIfmCreateStackMplsTunnelInterface (u4L3If,
                                              &u4MplsTnlIfIndex) == CFA_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "RESV: MplsMlibUpdate: MPLS tnl"
                    "interface creation error...\n");
        return RPTE_FAILURE;
    }
    if (MplsSetIfInfo (RPTE_TE_TNL (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo)),
                       u4MplsTnlIfIndex, IF_NAME) == RPTE_FAILURE)
    {
        return RPTE_FAILURE;
    }

    RSVPTE_DBG2 (RSVPTE_IF_PRCS, " RpteIfChangeForProtTnlLinkDown: "
                 "LINK DOWN L3 interface=%d, MPLS tnl if=%d\n",
                 u4L3If, u4MplsTnlIfIndex);
    /* If the tunnel is used by L2VPN module, L2VPN module has to delete the
     * tunnel. Hence Older tunnel if index has to be stored in u4OrgTnlIfIndex
     * and u4OrgTnlIfIndex if index  will be deleted by L2VPN module
     * */
    if (TE_TNL_IN_USE_BY_VPN (pRsvpTeTnlInfo->pTeTnlInfo) &
        TE_TNL_INUSE_BY_L2VPN)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u1DetourActive = MPLS_TRUE;
        pRsvpTeTnlInfo->pTeTnlInfo->u4OrgTnlIfIndex
            = TE_TNL_IFINDEX (RPTE_TE_TNL (pRsvpTeTnlInfo));
        pRsvpTeTnlInfo->pTeTnlInfo->u1FacFrrFlag = MPLS_TRUE;
    }

    PSB_FRR_FAC_LOC_REVERT_BYPASS_TNL_IF (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))
        = u4MplsTnlIfIndex;
    PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = pIfEntry;
    TE_TNL_IFINDEX (RPTE_TE_TNL (pRsvpTeTnlInfo)) = u4MplsTnlIfIndex;

    return RPTE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
/*                                                                            
 * Function Name   : RpteIfChangeForProtTnlLinkUp                                        
 * Description     : This function creates the MPLS tunnel interface 
 *                   based on the protected tunnel path
 * Input (s)       : pRsvpTeTnlInfo - RSVPTE tunnle info
 * Output (s)      : None                   
 * Returns         : RPTE_SUCCESS/RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteIfChangeForProtTnlLinkUp (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4L3If = RSVPTE_ZERO;

    if ((RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL) ||
        (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) == NULL))
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "RSVP if entry not exist \n");
        return RPTE_FAILURE;
    }

    pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    if (CfaUtilGetL3IfFromMplsIf (IF_ENTRY_IF_INDEX (pIfEntry),
                                  &u4L3If, TRUE) == CFA_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "RESV: MplsMlibUpdate: L3 Interface"
                    "from MPLS Interface get failed...\n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG2 (RSVPTE_IF_PRCS, "\n RpteIfChangeForProtTnlLinkUp: "
                 "LINK UP L3 interface=%d, MPLS tnl if=%d\n",
                 u4L3If, TE_TNL_IFINDEX (RPTE_TE_TNL (pRsvpTeTnlInfo)));
    if (CfaIfmDeleteStackMplsTunnelInterface (u4L3If,
                                              TE_TNL_IFINDEX
                                              (RPTE_TE_TNL (pRsvpTeTnlInfo))) ==
        CFA_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "CfaIfmDeleteStackMplsTunnelInterface failed \n");
        return RPTE_FAILURE;
    }
    if (RpteGetIfEntry ((INT4) PSB_FRR_FAC_LOC_REVERT_PROT_RSVP_IF
                        (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                        &pIfEntry) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RpteIfChangeForProtTnlLinkUp : INTMD - EXIT\n");
        return RPTE_FAILURE;
    }
    /* If the tunnel is used by L2VPN module, L2VPN module has to delete the
     * tunnel. Hence Newer tunnel if index has to be stored in u4OrgTnlIfIndex
     * and u4OrgTnlIfIndex if index  will be deleted by L2VPN module
     * */
    if (TE_TNL_IN_USE_BY_VPN (pRsvpTeTnlInfo->pTeTnlInfo) &
        TE_TNL_INUSE_BY_L2VPN)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u1DetourActive = MPLS_TRUE;
        pRsvpTeTnlInfo->pTeTnlInfo->u4OrgTnlIfIndex
            = TE_TNL_IFINDEX (RPTE_TE_TNL (pRsvpTeTnlInfo));
        pRsvpTeTnlInfo->pTeTnlInfo->u1FacFrrFlag = MPLS_TRUE;
    }
    PSB_FRR_FAC_LOC_REVERT_PROT_RSVP_IF (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))
        = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo))->u4IfIndex;
    PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) = pIfEntry;
    TE_TNL_IFINDEX (RPTE_TE_TNL (pRsvpTeTnlInfo)) =
        PSB_FRR_FAC_LOC_REVERT_PROT_TNL_IF (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
    return RPTE_SUCCESS;
}

/*----------------------------------------------------------------------------*/
/*                                                                            
 * Function Name   : RptePhFrrSetupGblRevertOptTunnel                                        
 * Description     : This function creates optimized tunnel if Global Revertive
 *                   Behavior is enabled. This function is called only if FRR
 *                   is enabled.
 * Input (s)       : pCspfCompInfo - CSPF Computation info.
 *                   pCspfPath - Path Computed info.
 * Output (s)      : None                   
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhFrrSetupGblRevertOptTunnel (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                  tCspfCompInfo * pCspfCompInfo,
                                  tOsTeAppPath * pCspfPath)
{
    tTeTnlInfo          TeTnlInfo;
    tRsvpTeTnlInfo     *pRsvpTeOutTnlInfo = NULL;
    tRsvpTePathListInfo *pOutPathListInfo = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4TnlInst = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhFrrSetupGblRevertOptTunnel : ENTRY \n");

    MEMSET (&TeTnlInfo, RSVPTE_ZERO, sizeof (tTeTnlInfo));
    u4TnlInst = (pCspfCompInfo->u4TnlInst) + RPTE_ONE;
    /* Creating a new RSVP Tunnel for available backup path. */
    if ((RpteCreateNewRsvpteTunnel (pCspfCompInfo->u4TnlIndex,
                                    u4TnlInst,
                                    pCspfCompInfo->u4TnlSrcAddr,
                                    pCspfCompInfo->u4TnlDestAddr,
                                    &TeTnlInfo, &pRsvpTeOutTnlInfo,
                                    RPTE_TRUE)) != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Creating New Tunnel Entry in RbTree Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RptePhFrrSetupGblRevertOptTunnel : INTMD-EXIT \n");
        return;
    }
    /* TE Row Status & Admin Status are made ACTIVE */
    TeTnlInfo.u1TnlRowStatus = RPTE_ACTIVE;
    TeTnlInfo.u1TnlAdminStatus = RPTE_ADMIN_UP;
    TeTnlInfo.u4TnlMode = GMPLS_FORWARD;
    /* Tunnel Entry Creation is indicated to TE Module */
    if (rpteTeCreateNewTnl (&pTeTnlInfo, &TeTnlInfo) == RPTE_TE_FAILURE)
    {
        /* Tunnel info returned to the Tunnel memory Pool */
        MEMSET (pRsvpTeOutTnlInfo, 0, sizeof (tRsvpTeTnlInfo));
        MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeOutTnlInfo);
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Tunnel Entry Addition in TE Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RptePhFrrSetupGblRevertOptTunnel : INTMD-EXIT \n");
        return;
    }
    RPTE_TE_TNL (pRsvpTeOutTnlInfo) = pTeTnlInfo;

    pRsvpTeOutTnlInfo->pTeTnlInfo->b1IsResourceReserved = RPTE_FALSE;

    /*Indices are declared in teTnlInfo. Hence RB Tree addtion is done
       once teTnlInfo is assigned  */

    if (RBTreeAdd (gpRsvpTeGblInfo->RpteTnlTree,
                   (tRBElem *) pRsvpTeOutTnlInfo) != RB_SUCCESS)
    {
        /* Tunnel info returned to the Tunnel memory Pool */
        MEMSET (pRsvpTeOutTnlInfo, 0, sizeof (tRsvpTeTnlInfo));
        MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeOutTnlInfo);
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to Add :EXIT RsvpTeOutTnlInfo to RpteTnlTree\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RptePhFrrSetupGblRevertOptTunnel : INTMD-EXIT \n");
        return;
    }

    SNPRINTF ((CHR1 *) pTeTnlInfo->au1TnlName, LSP_TNLNAME_LEN, "%s%d",
              "mplsTunnel", pCspfCompInfo->u4TnlIndex);

    RSVPTE_DBG1 (RSVPTE_PH_ETEXT,
                 "Global revertive base tnl instance=%d\n",
                 pCspfCompInfo->u4TnlInst);
    pTeTnlInfo->u4OrgTnlInstance = pCspfCompInfo->u4TnlInst;
    RSVPTE_TNL_ROW_STATUS (&TeTnlInfo) = RPTE_ACTIVE;
    RSVPTE_TNL_ADMIN_STATUS (&TeTnlInfo) = RPTE_ADMIN_UP;
    RSVPTE_TNL_NODE_RELATION (pRsvpTeOutTnlInfo) = RPTE_INGRESS;

    if (rpteTeCreatePathListAndPathOption (&pOutPathListInfo) ==
        RPTE_TE_FAILURE)
    {
        rpteTeDeleteTnlInfo (RPTE_TE_TNL (pRsvpTeOutTnlInfo),
                             TE_TNL_CALL_FROM_SIG);
        MEMSET (pRsvpTeOutTnlInfo, 0, sizeof (tRsvpTeTnlInfo));
        MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeOutTnlInfo);
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : ER-Hops Generation towards Egress Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RptePhFrrSetupGblRevertOptTunnel : INTMD-EXIT \n");
        return;
    }
    RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeOutTnlInfo) = pOutPathListInfo;
    RSVPTE_TNL_PATH_OPTION (pRsvpTeOutTnlInfo) =
        RSVPTE_TNL_FIRST_PATH_OPTION (pOutPathListInfo);
    RSVPTE_HOP_ROLE (RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeOutTnlInfo)) =
        RSVPTE_TNL_NODE_RELATION (pRsvpTeOutTnlInfo);

    /* Now, Insert all the ER-Hops computed through CSPF into
     * Outer Hop List as Strict Hops. This is with reference to RFC 4090
     * Section 6.3, Point Number : 5. */
    if (rpteTeCreateErHopsForCspfComputedHops ((&RSVPTE_TNL_OUTERHOP_LIST_INFO
                                                (pRsvpTeOutTnlInfo)),
                                               pCspfPath) == RPTE_TE_FAILURE)
    {
        rpteTeDeleteTnlInfo (RPTE_TE_TNL (pRsvpTeOutTnlInfo),
                             TE_TNL_CALL_FROM_SIG);
        MEMSET (pRsvpTeOutTnlInfo, 0, sizeof (tRsvpTeTnlInfo));
        MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeOutTnlInfo);
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : ER-Hops Generation towards Egress Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RptePhFrrSetupGblRevertOptTunnel : INTMD-EXIT \n");
        return;
    }

    RPTE_TE_TNL (pRsvpTeOutTnlInfo)->u4TnlHopTableIndex =
        RSVPTE_TNL_OUTERHOP_LIST_INFO (pRsvpTeOutTnlInfo)->u4HopListIndex;
    if (RSVPTE_TNL_PATH_OPTION (pRsvpTeOutTnlInfo) != NULL)
    {
        RPTE_TE_TNL (pRsvpTeOutTnlInfo)->u4TnlPathInUse =
            RSVPTE_TNL_PATH_OPTION (pRsvpTeOutTnlInfo)->u4PathOptionIndex;
    }
    if (rpteTeUpdateTeTnl (RPTE_TE_TNL (pRsvpTeOutTnlInfo)) == RPTE_TE_FAILURE)
    {
        rpteTeDeleteTnlInfo (RPTE_TE_TNL (pRsvpTeOutTnlInfo),
                             TE_TNL_CALL_FROM_SIG);
        MEMSET (pRsvpTeOutTnlInfo, 0, sizeof (tRsvpTeTnlInfo));
        MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeOutTnlInfo);
        RSVPTE_DBG (RSVPTE_IF_PRCS, "Err : rpteTeUpdateTeTnl Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RptePhFrrSetupGblRevertOptTunnel : INTMD-EXIT \n");
        return;
    }
    RSVPTE_TNL_TRFC_PARAM (RPTE_TE_TNL (pRsvpTeOutTnlInfo)) =
        RSVPTE_TNL_TRFC_PARAM (RPTE_TE_TNL (pRsvpTeTnlInfo));
    if (RptePhSetupTunnel (pRsvpTeOutTnlInfo) != RPTE_SUCCESS)
    {
        rpteTeDeleteHopListInfo (pOutPathListInfo);
        rpteTeDeleteTnlInfo (RPTE_TE_TNL (pRsvpTeOutTnlInfo),
                             TE_TNL_CALL_FROM_SIG);
        MEMSET (pRsvpTeOutTnlInfo, 0, sizeof (tRsvpTeTnlInfo));
        MemReleaseMemBlock (RSVPTE_TNL_POOL_ID, (UINT1 *) pRsvpTeOutTnlInfo);
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Setup Tunnel and Sending PATH Message Failed\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RptePhFrrSetupGblRevertOptTunnel : INTMD-EXIT \n");
        return;
    }
    RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                "RptePhFrrSetupGblRevertOptTunnel : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteHandlePsbOrHelloTimeOutForFrr
 * Description     : This function processes the PSB Timeout for 
 *                   Fast ReRoute Feature. This function is called only if FRR
 *                   is enabled.
 * Input (s)       : pRsvpTeTnlInfo - Rsvp tnl info
 * Output (s)      : None
 * Returns         : None
 *---------------------------------------------------------------------------*/
VOID
RpteHandlePsbOrHelloTimeOutForFrr (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tRsvpTeTnlInfo     *pRsvpBaseTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeInTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTmpTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTePrevTnlInfo = NULL;
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4TnlInstance = RPTE_ZERO;
    UINT1               u1IsDownStrTnl = RPTE_NO;
    BOOL1               bFlag = RPTE_FALSE;

    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteHandlePsbOrHelloTimeOutForFrr : ENTRY \n");

    if (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL)
    {
        RSVPTE_DBG4 (RSVPTE_NBR_DBG,
                     "PSB Timed out for Tunnel %d %d %x %x\n",
                     RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                     RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                     OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                     OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));

        /* This case handles facility backup also */
        /* Detour merging not capable and only one backup 
         * path message received handle */

        if (RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo) != NULL)
        {
            if (PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) != NULL)
            {
                pIfEntry = PSB_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
            }
            else
            {
                pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));
            }
            PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)) =
                RpteUtlComputeLifeTime (pIfEntry,
                                        PSB_IN_REFRESH_INTERVAL
                                        (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));

            RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) |= RPTE_FRR_NODE_STATE_UPLOST;

            /* Path - Refresh timer to be started for downstream of working tunnel
             * */
            RptePhPathRefresh (pRsvpTeTnlInfo);
            RptePhStartPathRefreshTmr (pRsvpTeTnlInfo->pPsb);

            RSVPTE_DBG (RSVPTE_NBR_DBG,
                        "RpteHandlePsbOrHelloTimeOutForFrr : EXIT \n");
            return;
        }

        /* PLR Behavior */
        /* Protected Tunnel Timer Expires, So Sending PATH Tear
         * to that tunnel as well as backup out tunnel. */
        RptePhGeneratePathTear (pRsvpTeTnlInfo);

        RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
        if (RPTE_TE_TNL (pRsvpTeTnlInfo)->u1TnlRelStatus ==
            TE_SIGMOD_TNLREL_AWAITED)
        {
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
        }
        else
        {
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
        }

        RSVPTE_DBG (RSVPTE_NBR_DBG,
                    "RpteHandlePsbOrHelloTimeOutForFrr : EXIT \n");
        return;
    }

    RSVPTE_DBG4 (RSVPTE_NBR_DBG,
                 "Hello Timed out for Tunnel %d %d %x %x\n",
                 RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                 RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                 OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                 OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));

    if (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) != NULL)
    {
        pRsvpBaseTeTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);
    }
    else
    {
        pRsvpBaseTeTnlInfo = pRsvpTeTnlInfo;
    }
    /* DMP Case Handle */
    if (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo)))
    {
        RSVPTE_DBG (RSVPTE_NBR_DBG, "DMP tunnel handling \n");
        /* Here Node State is DMP, Timer could expire for
         * 1. Base Tunnel,
         * 2. Any Inner Tunnel */
        if (RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTnlInfo) == RPTE_TRUE)
        {
            /* If timer has expired for the tunnel which was 
             * acting as downstream tunnel information, update it. */
            u1IsDownStrTnl = RPTE_YES;
        }
        RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo) |= RPTE_FRR_NODE_STATE_UPLOST;
        if (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) == NULL)
        {
            RSVPTE_DBG (RSVPTE_NBR_DBG, "DMP base tunnel to be deleted\n");
            /* DMP case: Tunnel expired is base tunnel which is added 
             * to RB Tree. So, delete this tunnel add the next 
             * tunnel to the RB Tree and update the base tnl info next 
             * bkp tunnels. */
            pRsvpTeInTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);
            if (pRsvpTeInTnlInfo == NULL)
            {
                RptePhGeneratePathTear (pRsvpTeTnlInfo);
                RpteRthSendResvTear (pRsvpTeTnlInfo->pRsb);
                RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                if (RPTE_TE_TNL (pRsvpTeTnlInfo)->
                    u1TnlRelStatus == TE_SIGMOD_TNLREL_AWAITED)
                {
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                         pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
                }
                else
                {
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                         pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
                }
                RSVPTE_DBG (RSVPTE_NBR_DBG,
                            "RpteHandlePsbOrHelloTimeOutForFrr - EXIT \n");
                return;
            }
            RPTE_FRR_BASE_TNL (pRsvpTeInTnlInfo) = NULL;
            RPTE_FRR_NODE_STATE (pRsvpTeInTnlInfo) =
                (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) |
                 RPTE_FRR_NODE_STATE_UPLOST);
            u4TnlInstance = RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);

            pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeInTnlInfo);
            if (pRsvpTeTmpTnlInfo == NULL)
            {
                RPTE_FRR_NODE_STATE (pRsvpTeInTnlInfo) &=
                    (UINT1) (~(RPTE_FRR_NODE_STATE_DMP));
                RPTE_FRR_NODE_STATE (pRsvpTeInTnlInfo) &=
                    (UINT1) (~(RPTE_FRR_NODE_STATE_UPLOST));
                RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeInTnlInfo)
                    = RPTE_TRUE;
            }
            while (pRsvpTeTmpTnlInfo != NULL)
            {
                /* Base tunnel (Newly selected tnl) updation for the 
                 * backup tunnels */
                RPTE_FRR_BASE_TNL (pRsvpTeTmpTnlInfo) = pRsvpTeInTnlInfo;
                pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeTmpTnlInfo);
            }
            RpteRthSendResvTear (pRsvpTeTnlInfo->pRsb);
            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            if (RPTE_TE_TNL (pRsvpTeTnlInfo)->u1TnlRelStatus ==
                TE_SIGMOD_TNLREL_AWAITED)
            {
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                     pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
            }
            else
            {
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                     pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
            }

            RSVPTE_DBG4 (RSVPTE_NBR_DBG,
                         "PSB TIME: DMP: Tunnel %d %d %x %x added to RB tree\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeInTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeInTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeInTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeInTnlInfo)));
            RSVPTE_TNL_TNLINST (pRsvpTeInTnlInfo) = u4TnlInstance;
            RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (pRsvpTeInTnlInfo))
                = u4TnlInstance;

            if (RBTreeAdd (gpRsvpTeGblInfo->RpteTnlTree,
                           (tRBElem *) pRsvpTeInTnlInfo) != RB_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_NBR_DBG,
                            "Failed to Add RsvpTeInTnlInfo to RpteTnlTree - EXIT \n");
                return;
            }

            if (u1IsDownStrTnl == RPTE_YES)
            {
                RptePhPathRefresh (pRsvpTeInTnlInfo);
                RptePhStartPathRefreshTmr (pRsvpTeInTnlInfo->pPsb);
            }
        }
        else
        {
            RSVPTE_DBG4 (RSVPTE_NBR_DBG,
                         "DMP: In Tunnel %d %d %x %x in detour merging node\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));

            /* Tunnel expired is not the base tunnel (one that is added
             * in the RB Tree). So, remove only that tunnel
             * and reassign the pointers. */
            pRsvpTeTmpTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);
            do
            {
                if (pRsvpTeTnlInfo == pRsvpTeTmpTnlInfo)
                {
                    break;
                }
                bFlag = RPTE_TRUE;
                pRsvpTePrevTnlInfo = pRsvpTeTmpTnlInfo;
            }
            while ((pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL_INFO
                    (pRsvpTeTmpTnlInfo)) != NULL);

            if (bFlag == RPTE_TRUE)
            {
                RPTE_FRR_IN_TNL_INFO (pRsvpTePrevTnlInfo) =
                    RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);
            }
            RpteRthSendResvTear (pRsvpTeTnlInfo->pRsb);
            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            if (RPTE_TE_TNL (pRsvpTeTnlInfo)->u1TnlRelStatus ==
                TE_SIGMOD_TNLREL_AWAITED)
            {
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                     pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_ADMIN, RPTE_FALSE);
            }
            else
            {
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                     pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_FALSE);
            }
            if (u1IsDownStrTnl == RPTE_YES)
            {
                RptePhPathRefresh (pRsvpBaseTeTnlInfo);
                RptePhStartPathRefreshTmr (pRsvpBaseTeTnlInfo->pPsb);
            }
            if (RPTE_FRR_IN_TNL_INFO (pRsvpBaseTeTnlInfo) == NULL)
            {
                RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo)
                    &= (UINT1) (~(RPTE_FRR_NODE_STATE_DMP));
                RPTE_FRR_NODE_STATE (pRsvpBaseTeTnlInfo)
                    &= (UINT1) (~(RPTE_FRR_NODE_STATE_UPLOST));
            }
        }
        return;
    }

    RpteFrrUnknownPsbOrHelloTimeOut (pRsvpTeTnlInfo);

    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteHandlePsbOrHelloTimeOutForFrr - EXIT \n");
    return;
}

/*---------------------------------------------------------------------------
 * Function Name   : RpteFrrUnknownPsbOrHelloTimeOut
 * Description     : This function processes the PSB or Hello Timeout for 
 *                   Tunnels whose Frr Node state is Unknown.
 * Input (s)       : pRsvpTeTnlInfo - Rsvp Tnl info
 * Output (s)      : None
 * Returns         : None
 *---------------------------------------------------------------------------*/

VOID
RpteFrrUnknownPsbOrHelloTimeOut (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tRsvpTeTnlInfo     *pInTnlInfo = NULL;
    tRsvpTeTnlInfo     *pBaseTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpTnlInfo = NULL;
    tRsvpTeTnlInfo     *pPrevTnlInfo = NULL;
    UINT4               u4TnlInstance = RPTE_ZERO;
    UINT1               u1FrrNodeState = RPTE_ZERO;
    BOOL1               bIsFrrInTnl = RPTE_FALSE;
    BOOL1               bIsOperDone = RPTE_FALSE;

    if (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) != RPTE_FRR_NODE_STATE_UNKNOWN)
    {
        RSVPTE_DBG (RSVPTE_NBR_DBG, "FRR tunnel node state is NOT unknown\n");
        return;
    }

    RSVPTE_DBG (RSVPTE_NBR_DBG, "FRR tunnel node state is unknown or Dnlost\n");

    if (RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo) == NULL)
    {
        /* Timeout happened for Frr Base Tnl on Backup path nodes. */
        pInTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);

        if (pInTnlInfo == NULL)
        {
            /* No more InTunnels present. So, tear down this tunnel. */
            RSVPTE_DBG (RSVPTE_NBR_DBG,
                        "Base tunnel NULL and IN Tunnel is also NULL\n");

            RptePhGeneratePathTear (pRsvpTeTnlInfo);
            RpteRthSendResvTear (pRsvpTeTnlInfo->pRsb);

            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);

            if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
            {
                RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
            }

            RSVPTE_DBG (RSVPTE_NBR_DBG,
                        "No Backup Tunnel Exist. Tunnel teared down.\n");
        }
        else
        {
            /* Some InTunnel present. Choose a new base tunnel among the 
             * in tunnels. Add it to RB Tree and tear down this tunnel. */

            RPTE_FRR_BASE_TNL (pInTnlInfo) = NULL;

            RSVPTE_DBG (RSVPTE_NBR_DBG,
                        "Base tunnel NULL but IN Tunnel present\n");

            u1FrrNodeState = RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo);
            u4TnlInstance = RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);

            pTmpTnlInfo = RPTE_FRR_IN_TNL_INFO (pInTnlInfo);
            while (pTmpTnlInfo != NULL)
            {
                RPTE_FRR_BASE_TNL (pTmpTnlInfo) = pInTnlInfo;
                pTmpTnlInfo = RPTE_FRR_IN_TNL_INFO (pTmpTnlInfo);
            }

            RptePhGeneratePathTear (pRsvpTeTnlInfo);
            RpteRthSendResvTear (pRsvpTeTnlInfo->pRsb);

            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
            if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
            {
                RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
            }

            RPTE_FRR_NODE_STATE (pInTnlInfo) = u1FrrNodeState;

            RSVPTE_DBG2 (RSVPTE_NBR_DBG,
                         "Value of Tnl Instance Old: %d New: %d\n",
                         RSVPTE_TNL_TNLINST (pInTnlInfo), u4TnlInstance);
            RSVPTE_TNL_TNLINST (pInTnlInfo) = u4TnlInstance;
            RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (pInTnlInfo)) = u4TnlInstance;

            RSVPTE_DBG4 (RSVPTE_NBR_DBG,
                         "PSB TIME: UNKN: Tunnel %d %d %x %x added to RB tree\n",
                         RSVPTE_TNL_TNLINDX (pInTnlInfo),
                         RSVPTE_TNL_TNLINST (pInTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pInTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pInTnlInfo)));

            if (RBTreeAdd (gpRsvpTeGblInfo->RpteTnlTree, (tRBElem *) pInTnlInfo)
                != RB_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_NBR_DBG,
                            "Failed to Add InTnlInfo to RpteTnlTree\n");
                return;
            }
        }
    }
    else
    {
        /* Timeout happened for Frr backup tunnel on backup path nodes or 
         * for backup tunnel on protected path nodes. */

        pBaseTnlInfo = RPTE_FRR_BASE_TNL (pRsvpTeTnlInfo);

        /* Get the previous tunnel information */
        RpteUtilGetPrevTnlInfo (pBaseTnlInfo, pRsvpTeTnlInfo, &pPrevTnlInfo);

        if ((pPrevTnlInfo != NULL) &&
            (RPTE_FRR_IN_TNL_INFO (pPrevTnlInfo) == pRsvpTeTnlInfo))
        {
            RSVPTE_DBG (RSVPTE_NBR_DBG, "Incoming Backup tunnel is down \n");
            bIsFrrInTnl = RPTE_TRUE;
            RPTE_FRR_IN_TNL_INFO (pPrevTnlInfo)
                = RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);
        }
        else
        {
            RSVPTE_DBG (RSVPTE_NBR_DBG, "Outgoing Backup tunnel is down \n");
            pPrevTnlInfo = pBaseTnlInfo;
        }

        /* In-tunnel received, UP_LOST, send PATH TEAR on both the upstream paths 
         * */
        if (RPTE_FRR_TNL_INFO_TYPE (pBaseTnlInfo) == RPTE_FRR_PROTECTED_TNL)
        {
            /* Check whether previous tnl is base tnl */
            RSVPTE_DBG (RSVPTE_NBR_DBG, "Base tunnel is NULL \n");
            /* Out tnl is down */
            if (bIsFrrInTnl == RPTE_FALSE)
            {
                RSVPTE_DBG4 (RSVPTE_NBR_DBG,
                             "FRR OUT Tunnel %d %d %x %x is down\n",
                             RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                             RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                             OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                             OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));

                if (RPTE_IS_DNLOST (RPTE_FRR_NODE_STATE (pBaseTnlInfo)))
                {
                    RSVPTE_DBG (RSVPTE_NBR_DBG,
                                "Base tunnel is NULL and DN lost\n");
                    if (!(RPTE_IS_HEADEND (RPTE_FRR_NODE_STATE (pBaseTnlInfo))))
                    {
                        RSVPTE_DBG (RSVPTE_NBR_DBG,
                                    "Base tunnel is NULL and DN lost "
                                    "at intermediate node\n");
                        if (RPTE_IS_MP (RPTE_FRR_NODE_STATE (pBaseTnlInfo)))
                        {
                            RpteRthSendResvTear
                                (RPTE_FRR_IN_TNL_INFO (pBaseTnlInfo)->pRsb);
                        }
                        RpteRthSendResvTear (pBaseTnlInfo->pRsb);

                        RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                             TE_TNL_CALL_FROM_SIG, RPTE_FALSE);
                        RpteResetPlrStatus (pBaseTnlInfo);
                    }
                    else
                    {
                        RSVPTE_DBG (RSVPTE_NBR_DBG, "Base tunnel is NULL and "
                                    "DN lost at Headend");

                        RPTE_TNL_DEL_TNL_FLAG (pBaseTnlInfo) = RPTE_TRUE;
                        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pBaseTnlInfo,
                                             TE_TNL_CALL_FROM_SIG, RPTE_TRUE);

                        RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                             TE_TNL_CALL_FROM_SIG, RPTE_FALSE);
                    }
                }
                else
                {
                    RSVPTE_DBG (RSVPTE_NBR_DBG, "Base tunnel is NULL and "
                                "DN is not lost\n");
                    RpteResetPlrStatus (pBaseTnlInfo);
                    RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                         TE_TNL_CALL_FROM_SIG, RPTE_FALSE);

                    RpteUpdateDetourStats (RPTE_TWO);
                }
                return;
            }

            RSVPTE_DBG4 (RSVPTE_NBR_DBG,
                         "FRR IN Tunnel %d %d %x %x is down\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));

            /* In tunnel is down */
            RptePrcsBkpPathTearOrTimeOut (pBaseTnlInfo, pPrevTnlInfo,
                                          pRsvpTeTnlInfo, &bIsOperDone);
            if (bIsOperDone == RPTE_YES)
            {
                RSVPTE_DBG (RSVPTE_NBR_DBG, "Base tunnel is NULL and Bkp Path "
                            "down operation completed\n");
                return;
            }

            /* FRR PLR processing */
            /* If FAST_REROUTE is not present, default method followed is
             * One-to-One. */
            u1FrrNodeState = RPTE_FRR_NODE_STATE (pBaseTnlInfo);
            if (RPTE_IS_UPLOST (u1FrrNodeState) &&
                RPTE_IS_PLR_OR_PLRAWAIT (u1FrrNodeState))
            {
                /* If node state is PLR or PLR_AWAIT and if local protection is
                 * not in use, then PATH Tear is duplicated on both path */
                RpteHandlePlrDown (pBaseTnlInfo);
            }
        }
        else
        {
            RSVPTE_DBG (RSVPTE_NBR_DBG, "Backup tunnel is down \n");

            RptePhGeneratePathTear (pRsvpTeTnlInfo);
            RpteRthSendResvTear (pRsvpTeTnlInfo->pRsb);

            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_FALSE);
        }

        if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
        {
            RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
        }
    }

    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteFrrUnknownPsbOrHelloTimeOut - EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteHandlePlrDown
 * Description     : This function deletes both the protected tunnel and
 *                   backup tunnel in case of PLR Going down.
 * Input (s)       : pRsvpTeOrigTnlInfo - RSVP tnl info.
 * Output (s)      : None
 * Returns         : None
 */
/****************************************************************************/
VOID
RpteHandlePlrDown (tRsvpTeTnlInfo * pRsvpTeOrigTnlInfo)
{
    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteUtlHdlPlrDown: Entry \n");
    /* If node state is PLR or PLR_AWAIT and if local protection is
     * not in use, then PATH Tear is duplicated on both path */

    RptePhGeneratePathTear (pRsvpTeOrigTnlInfo);
    RpteUpdateDetourStats (RPTE_TWO);

    RPTE_TNL_DEL_TNL_FLAG (pRsvpTeOrigTnlInfo) = RPTE_TRUE;
    RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeOrigTnlInfo,
                         TE_TNL_CALL_FROM_SIG, RPTE_TRUE);

    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteUtlHdlPlrDown: Exit \n");
    return;
}

/****************************************************************************/
/* Function Name   : RptePrcsBkpPathTearOrTimeOut
 * Description     : Function to handle the backup path path tear or 
 *                   neighbor down
 * Input (s)       : pRsvpTeOrigTnlInfo - Base RSVP tnl info.
 *                   pRsvpTePrevInTnlInfo - Previous tnl to the current tnl
 *                   pRsvpTeTnlInfo - Current RSVP tnl info.
 * Output (s)      : pbIsOperDone - For Operation completeness check flag
 * Returns         : None
 */
/****************************************************************************/
VOID
RptePrcsBkpPathTearOrTimeOut (tRsvpTeTnlInfo * pRsvpTeOrigTnlInfo,
                              tRsvpTeTnlInfo * pRsvpTePrevInTnlInfo,
                              tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                              BOOL1 * pbIsOperDone)
{
    tRsvpTeTnlInfo     *pRsvpTeInTnlInfo = NULL;

    pRsvpTeInTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);

    if (pRsvpTeInTnlInfo == NULL)
    {
        RSVPTE_DBG4 (RSVPTE_NBR_DBG,
                     "No further backup tunnel exist. "
                     "Tunnel %d %d %x %x to be deleted\n",
                     RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                     RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                     OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                     OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));

        /* No Further backup tunnel exist. Backup Tunnel Entry is removed and
         * Node state is reset from MP */
        RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                             pRsvpTeTnlInfo, TE_TNL_CALL_FROM_SIG, RPTE_FALSE);
        RPTE_FRR_IN_TNL_INFO (pRsvpTePrevInTnlInfo) = NULL;

        if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
        {
            RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
        }

        if (pRsvpTePrevInTnlInfo == pRsvpTeOrigTnlInfo)
        {
            RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo) &=
                (UINT1) (~(RPTE_FRR_NODE_STATE_MP));
            if (RPTE_IS_UPLOST (RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo)))
            {
                if (!(RPTE_IS_PLR_OR_PLRAWAIT (RPTE_FRR_NODE_STATE
                                               (pRsvpTeOrigTnlInfo))))
                {
                    RptePhGeneratePathTear (pRsvpTeOrigTnlInfo);

                    RPTE_TNL_DEL_TNL_FLAG (pRsvpTeOrigTnlInfo) = RPTE_TRUE;
                    RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                         pRsvpTeOrigTnlInfo,
                                         TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
                    RSVPTE_DBG (RSVPTE_NBR_DBG,
                                "Not PLR. Send path tear only for prot tnl "
                                "and remove it.\n");
                    *pbIsOperDone = RPTE_YES;
                    return;
                }
            }
            else
            {
                *pbIsOperDone = RPTE_YES;
                return;
            }
        }
        else
        {
            RSVPTE_DBG (RSVPTE_NBR_DBG,
                        "MP - Some Bkp Tnl Exist. PATH Tear Not sent.\n");
            *pbIsOperDone = RPTE_YES;
            return;
        }
    }
    else
    {
        /* Some backup tunnel exist. So, removing the pointer info 
         * from RsvpTeTnlInfo */

        RPTE_FRR_IN_TNL_INFO (pRsvpTePrevInTnlInfo) =
            RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);
        RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                             TE_TNL_CALL_FROM_SIG, RPTE_FALSE);

        if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
        {
            RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
        }
        RSVPTE_DBG (RSVPTE_NBR_DBG,
                    "MP - Some Bkp Tnl Exist. PATH Tear Not sent.\n");
        *pbIsOperDone = RPTE_YES;
        return;
    }
    *pbIsOperDone = RPTE_NO;
    return;
}

/****************************************************************************/
/* Function Name   : RptePhGetNextHopInfoFromCHop
 * Description     : Function is used to get the next hop information from
 *                   CHop List.
 * Input (s)       : pTeCHopListInfo   - CHopList
 *                   u4EgressId        - Egress of the tunnel
 * Output (s)      : pu4RemoteRtrAddr  - Next Hop Router ID
 *                   pu4RemoteLinkId   - Next Hop Identifier
 *                   pu4RemoteLinkAddr - Next Hop Address
 * Returns         : None
 *         */
/****************************************************************************/
VOID
RptePhGetNextHopInfoFromCHop (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                              tTeCHopListInfo * pTeCHopListInfo,
                              UINT4 u4EgressId, UINT4 *pu4NHRtrAddr,
                              UINT4 *pu4NHId, UINT4 *pu4NHIpAddr)
{
    tTeCHopInfo        *pTeCHopInfo = NULL;
    UINT4               u4NHRtrAddr = RPTE_ZERO;
    UINT4               u4NHId = RPTE_ZERO;
    UINT4               u4NHIpAddr = RPTE_ZERO;
    UINT4               u4FwdLbl = MPLS_INVALID_LABEL;
    UINT4               u4RevLbl = MPLS_INVALID_LABEL;

    TMO_SLL_Scan (&pTeCHopListInfo->CHopList, pTeCHopInfo, tTeCHopInfo *)
    {
        if (pTeCHopInfo->u1LsrPartOfCHop == RPTE_TRUE)
        {
            continue;
        }
        else
        {
            break;
        }
    }

    if (pTeCHopInfo == NULL)
    {
        u4NHRtrAddr = u4EgressId;
    }
    else
    {
        CONVERT_TO_INTEGER (pTeCHopInfo->RouterId.au1Ipv4Addr, u4NHRtrAddr);
        u4NHRtrAddr = OSIX_NTOHL (u4NHRtrAddr);

        CONVERT_TO_INTEGER (pTeCHopInfo->IpAddr.au1Ipv4Addr, u4NHIpAddr);
        u4NHIpAddr = OSIX_NTOHL (u4NHIpAddr);
        u4NHId = pTeCHopInfo->u4UnnumIf;
        u4FwdLbl = pTeCHopInfo->GmplsTnlCHopInfo.u4ForwardLbl;
        u4RevLbl = pTeCHopInfo->GmplsTnlCHopInfo.u4ReverseLbl;
    }

    *pu4NHRtrAddr = u4NHRtrAddr;
    *pu4NHId = u4NHId;
    *pu4NHIpAddr = u4NHIpAddr;

    RpteDnStrLblProgForExpLblControl (pRsvpTeTnlInfo, u4FwdLbl, u4RevLbl);

    return;
}

/****************************************************************************/
/* Function Name   : RptePhGetNextHopInfoFromERHop
 * Description     : Function is used to get the next hop information from
 *                   ERHop List.
 * Input (s)       : pTeErHopListInfo   - ERHopList
 *                   u4EgressId        - Egress of the tunnel
 * Output (s)      : pu4RemoteRtrAddr  - Next Hop Router ID
 *                   pu4RemoteLinkId   - Next Hop Identifier
 *                   pu4RemoteLinkAddr - Next Hop Address
 * Returns         : None
 *         */
/****************************************************************************/
VOID
RptePhGetNextHopInfoFromERHop (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                               tTePathInfo * pTePathInfo,
                               UINT4 u4EgressId, UINT4 *pu4NHRtrAddr,
                               UINT4 *pu4NHId, UINT4 *pu4NHIpAddr)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    UINT4               u4NHRtrAddr = RPTE_ZERO;
    UINT4               u4NHId = RPTE_ZERO;
    UINT4               u4NHIpAddr = RPTE_ZERO;
    UINT4               u4FwdLbl = MPLS_INVALID_LABEL;
    UINT4               u4RevLbl = MPLS_INVALID_LABEL;

    TMO_SLL_Scan (&pTePathInfo->ErHopList, pTeHopInfo, tTeHopInfo *)
    {
        if (pTeHopInfo->u1LsrPartOfErHop == RPTE_TRUE)
        {
            continue;
        }
        else
        {
            break;
        }
    }

    if (pTeHopInfo == NULL)
    {
        u4NHRtrAddr = u4EgressId;
    }
    else
    {
        CONVERT_TO_INTEGER (pTeHopInfo->IpAddr.au1Ipv4Addr, u4NHIpAddr);
        u4NHIpAddr = OSIX_NTOHL (u4NHIpAddr);

        RptePortTlmGetRemoteRtrIp (u4NHIpAddr, &u4NHRtrAddr);

        if (u4NHRtrAddr == RPTE_ZERO)
        {
            u4NHRtrAddr = u4NHIpAddr;
        }

        u4NHId = pTeHopInfo->u4UnnumIf;
        u4FwdLbl = pTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl;
        u4RevLbl = pTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl;
    }

    *pu4NHRtrAddr = u4NHRtrAddr;
    *pu4NHId = u4NHId;
    *pu4NHIpAddr = u4NHIpAddr;

    RpteDnStrLblProgForExpLblControl (pRsvpTeTnlInfo, u4FwdLbl, u4RevLbl);

    return;
}

/****************************************************************************/
/* Function Name   : RptePhGetNextHopInfo
 * Description     : Function is used to get the next hop information
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE Tunnel
 * Output (s)      : pu4NHRtrAddr   - Next Hop Router ID
 *                   pu4NHId        - Next Hop Identifier
 *                   pu4NHIpAddr    - Next Hop Address
 * Returns         : None
 *         */
/****************************************************************************/
VOID
RptePhGetNextHopInfo (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT4 *pu4NHRtrAddr,
                      UINT4 *pu4NHId, UINT4 *pu4NHIpAddr)
{
    tTeCHopListInfo    *pTeCHopListInfo = NULL;
    tTePathInfo        *pTePathInfo = NULL;
    UINT4               u4EgressId = RPTE_ZERO;

    CONVERT_TO_INTEGER (pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId, u4EgressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTeCHopListInfo = pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo;
    pTePathInfo = pRsvpTeTnlInfo->pTeTnlInfo->pTePathInfo;

    if (pTeCHopListInfo != NULL)
    {
        RptePhGetNextHopInfoFromCHop (pRsvpTeTnlInfo, pTeCHopListInfo,
                                      u4EgressId, pu4NHRtrAddr, pu4NHId,
                                      pu4NHIpAddr);
    }
    else if (pTePathInfo != NULL)
    {
        RptePhGetNextHopInfoFromERHop (pRsvpTeTnlInfo, pTePathInfo, u4EgressId,
                                       pu4NHRtrAddr, pu4NHId, pu4NHIpAddr);
    }
    else
    {
        *pu4NHRtrAddr = u4EgressId;
    }

    return;
}

/****************************************************************************/
/* Function Name   : RptePhCheckAndUpdateOobInfo
 * Description     : Function is used to check and update the data channel
 *                   information
 * Input (s)       : pRsvpTeTnlInfo - Tunnel Info
 *                   pIfEntry       - If Entry Info
 *                   u4NHId         - Next Hop Identifier
 *                   u4NHIpAddr     - Next Hop Ip Address
 *                   u4UnnumIf - Remote link id
 *                   u4NextHopAddr - Remote link ip
 *                   u1IsDnStr     - TRUE - If DN Stream Info to be filled
 *                                   FALSE - If UP Stream Info to be filled
 * Output (s)      : u4DestAddr - Destination Address
 * Returns         : None
 * 
****************************************************************************/
VOID
RptePhCheckAndUpdateOobInfo (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                             tIfEntry ** ppIfEntry,
                             UINT4 u4NHId, UINT4 u4RtrAddr, UINT4 u4NHIpAddr,
                             UINT1 u1IsDnStr, UINT4 *pu4OutGw)
{
    tRsvpTeTnlInfo     *pFARsvpTeTnlInfo = NULL;
    tTeRouterId         EgressLsrId;
    tTeRouterId         IngressLsrId;
    UINT4               u4LocalIP = RSVPTE_ZERO;
    UINT4               u4LocalId = RSVPTE_ZERO;
    UINT4               u4MplsInterface = RSVPTE_ZERO;
    UINT4               u4IngressId = RSVPTE_ZERO;
    UINT4               u4EgressId = RSVPTE_ZERO;
    UINT4               u4IfIndex = RSVPTE_ZERO;
    UINT4               u4TnlIndex = RSVPTE_ZERO;
    UINT4               u4TnlInstance = RSVPTE_ZERO;
    UINT4               u4NHRtrAddrShortest = RPTE_ZERO;
    BOOL1               bOobFlag = FALSE;
    UINT1               u1IfType = RSVPTE_ZERO;
    UINT1               u1TnlRole = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                " RSVPTE : RptePhCheckAndUpdateOobInfo : ENTRY \n");
    MEMSET (&EgressLsrId, RSVPTE_ZERO, sizeof (tTeRouterId));
    MEMSET (&IngressLsrId, RSVPTE_ZERO, sizeof (tTeRouterId));

    if (u4NHId != RSVPTE_ZERO)
    {
        RptePortTlmGetLocalId (u4NHId, u4NHIpAddr, &u4LocalId, &u4IfIndex);
    }
    else if (u4NHIpAddr != RSVPTE_ZERO)
    {
        RptePortTlmGetLocalIP (u4NHIpAddr, &u4LocalIP, &u4IfIndex);
    }

    /* If u4IfIndex is ZERO, it means either
     * 1. TLM is not enabled or
     * 2. u4NHId and u4NHIpAddr are ZERO.
     *
     * This means control and data channels are not separated. */
    if (u4IfIndex != RSVPTE_ZERO)
    {
        RptePortCfaGetMplsIfFromTeLinkIf (u4IfIndex, &u4MplsInterface);
        if (u4MplsInterface != (*ppIfEntry)->u4IfIndex)
        {
            if ((u4NHId == RSVPTE_ZERO) && (pu4OutGw != NULL)
                && (u4RtrAddr != RSVPTE_ZERO))
            {
                RptePortTlmGetRemoteRtrIp (*pu4OutGw, &u4NHRtrAddrShortest);
                if ((u4RtrAddr != u4NHRtrAddrShortest) &&
                    (RpteIpUcastRouteQuery (u4NHIpAddr, ppIfEntry, pu4OutGw) ==
                     RPTE_SUCCESS))
                {
                    RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                                " RSVPTE : Able get new IfEntry for u4NHIpAddr \n");
                }
                else
                {
#ifdef TLM_WANTED
                    /*Associte control Mpls interface with data telinks */
                    TlmApiAssociateCtrlIfAndDataIf ((*ppIfEntry)->u4IfIndex,
                                                    u4IfIndex);
#endif
                    bOobFlag = TRUE;
                }
            }
            else
            {
#ifdef TLM_WANTED
                /*Associte control Mpls interface with data telinks */
                TlmApiAssociateCtrlIfAndDataIf ((*ppIfEntry)->u4IfIndex,
                                                u4IfIndex);
#endif
                bOobFlag = TRUE;
            }
        }
    }
    else
    {
        /* Failure check is intentionally skipped here because TLM may not
         * be availble. */
        if (RptePortCfaGetTeLinkIfFromMplsIf
            ((*ppIfEntry)->u4IfIndex, &u4IfIndex) == RPTE_FAILURE)
        {
            RSVPTE_DBG1 (RSVPTE_PH_PRCS,
                         "Getting TE Link from MPLS Interface %d failed\n",
                         (*ppIfEntry)->u4IfIndex);
            /* Failure check is not needed here */
        }
    }

    /* For FA TE Link, Local Identifier and Component interface is 
     * mplsTunnelInterface */
    if ((CfaGetIfaceType (u4LocalId, &u1IfType) == CFA_SUCCESS) &&
        (u1IfType == CFA_MPLS_TUNNEL))
    {
        TeSigUpdateMapTnlIndex (u4LocalId, &u4TnlIndex, &u4TnlInstance,
                                &IngressLsrId, &EgressLsrId, &u1TnlRole);

        pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMapIndex = u4TnlIndex;
        pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMapInstance = u4TnlInstance;
        MEMCPY (&pRsvpTeTnlInfo->pTeTnlInfo->TnlMapIngressLsrId,
                &IngressLsrId, IPV4_ADDR_LENGTH);
        MEMCPY (&pRsvpTeTnlInfo->pTeTnlInfo->TnlMapEgressLsrId,
                &EgressLsrId, IPV4_ADDR_LENGTH);
        u4IngressId = RpteUtilConvertToInteger (IngressLsrId);
        u4EgressId = RpteUtilConvertToInteger (EgressLsrId);
        u4IngressId = OSIX_NTOHL (u4IngressId);
        u4EgressId = OSIX_NTOHL (u4EgressId);

        if (u1IsDnStr == TRUE)
        {
            if (u1TnlRole == RPTE_INGRESS)
            {
                pRsvpTeTnlInfo->u4FALspSrcOrDest = u4EgressId;
            }
            else
            {
                pRsvpTeTnlInfo->u4FALspSrcOrDest = u4IngressId;
            }
        }

        if (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo, u4TnlIndex,
                                    u4TnlInstance, u4IngressId, u4EgressId,
                                    &pFARsvpTeTnlInfo) != RPTE_SUCCESS)
        {
            return;
        }

        if (pFARsvpTeTnlInfo != NULL)
        {
            TMO_SLL_Add (&pFARsvpTeTnlInfo->StackTnlList,
                         &pRsvpTeTnlInfo->NextStackTnl);
            pFARsvpTeTnlInfo->pTeTnlInfo->u4NoOfStackedTunnels++;
            if ((pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION) ||
                (u1IsDnStr == TRUE))
            {
                RPTE_HLSP_STACK_BIT (pRsvpTeTnlInfo) = TRUE;
            }
        }
    }

    if (u1IsDnStr == TRUE)
    {
        pRsvpTeTnlInfo->u4DnStrDataTeLinkIfId = u4LocalId;
        pRsvpTeTnlInfo->u4DnStrDataTeLinkIfAddr = u4LocalIP;
        pRsvpTeTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf = u4IfIndex;
        pRsvpTeTnlInfo->b1DnStrOob = bOobFlag;
        RSVPTE_DBG1 (RSVPTE_PH_PRCS,
                     "setting b1DnStrOob: %d\n", pRsvpTeTnlInfo->b1DnStrOob);
    }
    else
    {
        pRsvpTeTnlInfo->u4UpStrDataTeLinkIfId = u4LocalId;
        pRsvpTeTnlInfo->u4UpStrDataTeLinkIfAddr = u4LocalIP;
        pRsvpTeTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf = u4IfIndex;
        pRsvpTeTnlInfo->b1UpStrOob = bOobFlag;
        RSVPTE_DBG1 (RSVPTE_PH_PRCS,
                     "setting b1UpStrOob: %d\n", pRsvpTeTnlInfo->b1UpStrOob);
    }

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                " RSVPTE : RptePhCheckAndUpdateOobInfo : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RptePhFindRouteAndFwdPathMsg
 * Description     : This function finds the route and forwards the path
 *                   message based on CSPF Computation or IGP Path.
 * Input (s)       : pRsvpTeErHopInfo      - Pointer to ER Hop Info
 *                   pRsvpTeTnlInfo        - Pointer to RSVP-TE Tnl Info
 *                   pPktMap               - Packet Map
 *                   u1PathRefreshNeeded   - Path Refresh Needed Flag
 *                   u1ResvRefreshNeeded   - Resv Refresh Needed Flag
 *                   u1NewTnl              - New Tnl Flag
 *                   u1NewBkpTnl           - New Backup Tnl Flag
 *                   u1ErrFlag             - Err Flag
 * Output (s)      : pu1IsCspfReqd         - CSPF Required Flag
 * Returns         : None
 * 
****************************************************************************/
INT4
RptePhFindRouteAndFwdPathMsg (tTeHopInfo * pRsvpTeErHopInfo,
                              tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                              tPktMap * pPktMap, UINT1 u1PathRefreshNeeded,
                              UINT1 u1ResvRefreshNeeded, UINT1 u1NewTnl,
                              UINT1 u1NewBkpTnl, UINT1 u1ErrFlag,
                              UINT1 *pu1IsCspfReqd)
{
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4OutGw = RPTE_ZERO;
    UINT4               u4NHIpAddr = RPTE_ZERO;
    UINT4               u4NHRtrAddr = RPTE_ZERO;
    UINT4               u4NHId = RPTE_ZERO;
    UINT4               u4DestAddr = RPTE_ZERO;
    UINT4               u4TnlDestAddr = RPTE_ZERO;
    UINT2               u2ErrValue = (UINT2) RPTE_NO_ROUTE_AVAIL;
    UINT1               u1HopType = RPTE_ZERO;
    BOOL1               bIsEntryPresentInEroCache = FALSE;
    UINT4               u4TunnelIndex = RPTE_ZERO;
    UINT4               u4TunnelInstance = RPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RPTE_ZERO;
    tRsvpTeTnlInfo     *pRsvpTeWorkTnlInfo = NULL;

    CONVERT_TO_INTEGER (pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId,
                        u4TnlDestAddr);
    u4TnlDestAddr = OSIX_NTOHL (u4TnlDestAddr);

    if (pRsvpTeErHopInfo != NULL)
    {
        CONVERT_TO_INTEGER (pRsvpTeErHopInfo->IpAddr.au1Ipv4Addr, u4NHIpAddr);
        u4NHIpAddr = OSIX_NTOHL (u4NHIpAddr);

        RptePortTlmGetRemoteRtrIp (u4NHIpAddr, &u4NHRtrAddr);

        if (u4NHRtrAddr == RPTE_ZERO)
        {
            u4NHRtrAddr = u4NHIpAddr;
        }

        u4DestAddr = u4NHRtrAddr;
        u1HopType = pRsvpTeErHopInfo->u1HopType;
    }
    else
    {
        CONVERT_TO_INTEGER (pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId,
                            u4DestAddr);
        u4DestAddr = OSIX_NTOHL (u4DestAddr);
    }
    if ((u1HopType != RPTE_ERHOP_STRICT) &&
        (RptePortIsOspfTeEnabled () != RPTE_FAILURE))
    {
        if (((RSVPTE_TE_TNL_REOPTIMIZE_STATUS
              (RPTE_TE_TNL (pRsvpTeTnlInfo))) == TE_REOPTIMIZE_ENABLE))
        {
            u4TunnelIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex;
            CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID
                                (pRsvpTeTnlInfo->pTeTnlInfo),
                                u4TunnelIngressLSRId);
            u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
            CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID
                                (pRsvpTeTnlInfo->pTeTnlInfo),
                                u4TunnelEgressLSRId);
            u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);

            u4TunnelInstance = pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance;

            if ((RpteCheckAdnlTnlInTnlTable (gpRsvpTeGblInfo, u4TunnelIndex,
                                             u4TunnelInstance,
                                             u4TunnelIngressLSRId,
                                             u4TunnelEgressLSRId,
                                             &pRsvpTeWorkTnlInfo) ==
                 RPTE_SUCCESS)
                && (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance ==
                    pRsvpTeWorkTnlInfo->pTeTnlInfo->u4TnlInstance))
            {
                RSVPTE_DBG (RSVPTE_REOPT_DBG, " RptePhFindRouteAndFwdPathMsg: "
                            "Working Tnl Info is NULL\n");
                pRsvpTeWorkTnlInfo = NULL;
            }

            if (pRsvpTeWorkTnlInfo != NULL)
            {
                pRsvpTeTnlInfo->pMapTnlInfo = pRsvpTeWorkTnlInfo;
                pRsvpTeWorkTnlInfo->pMapTnlInfo = pRsvpTeTnlInfo;
                pRsvpTeWorkTnlInfo->pTeTnlInfo->u4BkpTnlIndex = u4TunnelIndex;
                pRsvpTeWorkTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
                    u4TunnelInstance;
                MEMCPY (pRsvpTeWorkTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                        &RSVPTE_TNL_INGRESS_RTR_ID (pRsvpTeWorkTnlInfo->
                                                    pTeTnlInfo),
                        ROUTER_ID_LENGTH);
                MEMCPY (pRsvpTeWorkTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                        &RSVPTE_TNL_EGRESS_RTR_ID (pRsvpTeWorkTnlInfo->
                                                   pTeTnlInfo),
                        ROUTER_ID_LENGTH);

                pRsvpTeTnlInfo->u4RsrcClassColor =
                    pRsvpTeWorkTnlInfo->u4RsrcClassColor;

                if ((pRsvpTeWorkTnlInfo->pTeTnlInfo->pTeEroCacheListInfo !=
                     NULL)
                    && (pRsvpTeWorkTnlInfo->pTeTnlInfo->
                        u4TnlEroCacheTableIndex != RPTE_ZERO))
                {
                    RSVPTE_DBG (RSVPTE_REOPT_DBG,
                                " RptePhFindRouteAndFwdPathMsg: "
                                "Path is present in Ero Cache\n");
                    bIsEntryPresentInEroCache = TRUE;
                }
            }
        }

        if (bIsEntryPresentInEroCache == FALSE)
        {
            *pu1IsCspfReqd = RPTE_TRUE;
            if (RpteCspfCompForNormalTnl (pRsvpTeTnlInfo) == RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RptePhFindRouteAndFwdPathMsg : INTMD EXIT \n");
                return RPTE_FAILURE;
            }
            if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo)
                == RPTE_INTERMEDIATE)
            {
                if (((RSVPTE_TE_TNL_REOPTIMIZE_STATUS
                      (RPTE_TE_TNL (pRsvpTeTnlInfo))) == TE_REOPTIMIZE_ENABLE))
                {
                    RSVPTE_DBG (RSVPTE_REOPT_DBG,
                                "RptePhFindRouteAndFwdPathMsg : "
                                "Update Reoptimization Node State - Mid Point Node\n");
                    RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) =
                        RPTE_MID_POINT_NODE;
                }
            }
            return RPTE_SUCCESS;
        }
        else
        {
            *pu1IsCspfReqd = RPTE_TRUE;
            RSVPTE_DBG (RSVPTE_REOPT_DBG, " RptePhFindRouteAndFwdPathMsg: "
                        "CSPF Calculation Not Required, CHop is populated"
                        " from ERO Cache Hop Info\n");
            RSVPTE_DBG (RSVPTE_REOPT_DBG, "RptePhFindRouteAndFwdPathMsg : "
                        "Update Reoptimization Node State - Mid Point Node\n");
            RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) = RPTE_MID_POINT_NODE;

            pRsvpTeTnlInfo->pTeTnlInfo->pTeCHopListInfo =
                pRsvpTeWorkTnlInfo->pTeTnlInfo->pTeEroCacheListInfo;
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlCHopTableIndex =
                pRsvpTeWorkTnlInfo->pTeTnlInfo->u4TnlEroCacheTableIndex;
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlPathMetric =
                pRsvpTeWorkTnlInfo->pTeTnlInfo->u4TnlPathMetric;

            if (RptePhSetupTunnel (pRsvpTeTnlInfo) != RPTE_SUCCESS)
            {
                /* No clean up required here since it is already handled in
                 ** SetupTunnel itself */

                RSVPTE_DBG (RSVPTE_REOPT_DBG, " RptePhFindRouteAndFwdPathMsg: "
                            "Setup Tunnel Failed\n");
                return RPTE_FAILURE;
            }

            /*rpteTeDeleteCHopListInfo(pRsvpTeWorkTnlInfo->pTeTnlInfo->pTeEroCacheListInfo); */
            pRsvpTeWorkTnlInfo->pTeTnlInfo->pTeEroCacheListInfo = NULL;
            pRsvpTeWorkTnlInfo->pTeTnlInfo->u4TnlEroCacheTableIndex = RPTE_ZERO;
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &RPTE_ERO_CACHE_TIMER (pRsvpTeWorkTnlInfo));
            return RPTE_SUCCESS;
        }
    }
    else if (RptePortIsOspfTeEnabled () != RPTE_FAILURE)
    {
        if (RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE)
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        " FRR enabled cspf computation not needed  \n");
        }
        else
        {
            *pu1IsCspfReqd = RPTE_TRUE;
            if (RpteIpDirectlyConnected (u4NHRtrAddr) != RPTE_SUCCESS)
            {
                if (RpteCspfCompForNormalTnl (pRsvpTeTnlInfo) == RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                "RptePhFindRouteAndFwdPathMsg : INTMD EXIT1 \n");
                    return RPTE_FAILURE;
                }
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RptePhFindRouteAndFwdPathMsg : INTMD EXIT2-- Return success \n");
                return RPTE_SUCCESS;
            }
        }
    }

    if ((RPTE_IS_DEST_OUR_ADDR (u4TnlDestAddr) == RPTE_SUCCESS) ||
        (RpteIpUcastRouteQuery (u4DestAddr, &pIfEntry, &u4OutGw)
         != RPTE_SUCCESS))
    {
        if ((u1ErrFlag == RPTE_ONE) && (pRsvpTeErHopInfo != NULL))
        {
            if (pRsvpTeErHopInfo->u1HopType == ERO_TYPE_STRICT)
            {
                u2ErrValue = (UINT2) RPTE_BAD_STRICT_NODE;
            }
            else
            {
                u2ErrValue = (UINT2) RPTE_BAD_LOOSE_NODE;
            }
        }

        RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB, u2ErrValue);

        RSVPTE_DBG1 (RSVPTE_PH_PRCS,
                     "No Route to reach destination %x - Path err sent\n",
                     u4DestAddr);
        return RPTE_FAILURE;
    }

    if (pRsvpTeErHopInfo != NULL)
    {
        CONVERT_TO_INTEGER (pRsvpTeErHopInfo->IpAddr.au1Ipv4Addr, u4NHIpAddr);
        u4NHIpAddr = OSIX_NTOHL (u4NHIpAddr);

        u4NHId = pRsvpTeErHopInfo->u4UnnumIf;

        RSVPTE_DBG1 (RSVPTE_PH_PRCS,
                     "RptePhFindRouteAndFwdPathMsg : u4NHIpAddr: 0x%x \n",
                     u4NHIpAddr);
        RpteDnStrLblProgForExpLblControl (pRsvpTeTnlInfo,
                                          pRsvpTeErHopInfo->GmplsTnlHopInfo.
                                          u4ForwardLbl,
                                          pRsvpTeErHopInfo->GmplsTnlHopInfo.
                                          u4ReverseLbl);
        RptePhCheckAndUpdateOobInfo (pRsvpTeTnlInfo, &pIfEntry, u4NHId,
                                     u4DestAddr, u4NHIpAddr, TRUE, &u4OutGw);
        RpteUtlCalculateEROSize (pRsvpTeTnlInfo);
    }

    if (RpteIntermediatePrcs (u4OutGw, pIfEntry, pRsvpTeTnlInfo, pPktMap,
                              u1PathRefreshNeeded, u1ResvRefreshNeeded,
                              u1NewTnl, u1NewBkpTnl) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RptePhFindRouteAndFwdPathMsg : INTMD EXIT \n");
        return RPTE_FAILURE;
    }

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhFindRouteAndFwdPathMsg : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePhActOnPathSetupFailure
 * Description     : This function determines the action that should be
 *                   done when PATH setup fails due to CSPF computation failure
 *                   or IGP Path Failure
 *                   message based on CSPF Computation or IGP Path.
 * Input (s)       : pRsvpTeTnlInfo        - Pointer to RSVP-TE Tnl Info
 *                   u1ErrCode             - Error Code
 *                   u2ErrValue            - Error Value
 * Output (s)      : None
 * Returns         : None
 * 
****************************************************************************/
VOID
RptePhActOnPathSetupFailure (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 u1ErrCode,
                             UINT2 u2ErrValue)
{
    UINT4               u4IngressId = RPTE_ZERO;
    UINT4               u4EgressId = RPTE_ZERO;
    UINT4               u4TempVal = RPTE_ZERO;

    /* Primary Instance tunnel should be removed */

    CONVERT_TO_INTEGER (pRsvpTeTnlInfo->pTeTnlInfo->TnlIngressLsrId,
                        u4IngressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);

    CONVERT_TO_INTEGER (pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId, u4EgressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    if (pRsvpTeTnlInfo->u1CSPFPathRequested == RPTE_FALSE)
    {
        RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                             TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
    }
    else if (pRsvpTeTnlInfo->u1CSPFPathRequested == RPTE_TRUE)
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
        {
            RpteUtlFillErrorTable (u1ErrCode, u2ErrValue, u4IngressId,
                                   pRsvpTeTnlInfo);
            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
        }
        else
        {
            RpteConstructPktMapAndSendPathErr (pRsvpTeTnlInfo, u1ErrCode,
                                               u2ErrValue);
            CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                                u4TempVal);
            if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INTERMEDIATE)
            {
                RpteUtlFillErrorTable (u1ErrCode, u2ErrValue,
                                       OSIX_NTOHL (u4TempVal), pRsvpTeTnlInfo);
                RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
            }
        }
    }

    return;
}

/****************************************************************************/
/* Function Name   : RpteDeleteTrfcParamsAtEgress
 * Description     : This function deletes the traffic params at Egress node
 * Input (s)       : pRsvpTeTnlInfo   - Pointer to RSVP-TE Tnl Info
 * Output (s)      : None
 * Returns         : None
 *
****************************************************************************/
VOID
RpteDeleteTrfcParamsAtEgress (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode != GMPLS_BIDIRECTION)
    {
        /* The Traffic params associated are deleted */
        rpteTeDeleteTrfcParams (RPTE_TNL_TRFC_PARMS (pRsvpTeTnlInfo));
        RPTE_TNL_TRFC_PARMS (pRsvpTeTnlInfo) = NULL;
        RPTE_TNL_TRFC_PARM_INDEX (pRsvpTeTnlInfo) = RPTE_ZERO;
    }
    return;
}

/****************************************************************************/
/* Function Name   : RptePhUpStrResvBwAndLblProg
 * Description     : This function reserves the bandwidth and programmes the
 *                   label for the reverse direction of the tunnel.
 * Input (s)       : pRsvpTeTnlInfo   - Pointer to RSVP-TE Tnl Info
 * Output (s)      : *pu1ErrCode      - Error Code
 *                   *pu2ErrValue     - Error Value
 * Returns         : RPTE_FAILURE or RPTE_SUCCESS
 *
****************************************************************************/
INT4
RptePhUpStrResvBwAndLblProg (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 *pu1ErrCode,
                             UINT2 *pu2ErrValue)
{
    tSenderTspec       *pPathTe = NULL;
    FLOAT               reqResBw = RSVPTE_ZERO;
    UINT1               u1PathRefreshNeeded = RPTE_REFRESH_MSG_NO;

    *pu1ErrCode = RPTE_ZERO;
    *pu2ErrValue = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhUpStrResvBwAndLblProg : ENTRY\n");

    if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode != GMPLS_BIDIRECTION)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS, "PATH: Not a bidirectional tunnel\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhUpStrResvBwAndLblProg : EXIT\n");
        return RPTE_SUCCESS;
    }

    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pRsvpTeTnlInfo->u1IsGrTnl == RPTE_YES))
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "Programming is not needed for Gr tunnel\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhUpStrResvBwAndLblProg : EXIT\n");
        return RPTE_SUCCESS;
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole != RPTE_INGRESS)
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf == RPTE_ZERO)
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH: Upstream Data Link If is ZERO\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhUpStrResvBwAndLblProg : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }

        RpteRhRelReroutedTnlBw (pRsvpTeTnlInfo, GMPLS_SEGMENT_REVERSE);

        pPathTe =
            &SENDER_TSPEC_OBJ (&PSB_SENDER_TSPEC_OBJ (pRsvpTeTnlInfo->pPsb));
        reqResBw = OSIX_NTOHF (pPathTe->TBParams.fPeakRate);

        if (RpteTlmUpdateTrafficControl (pRsvpTeTnlInfo,
                                         pRsvpTeTnlInfo->pTeTnlInfo->
                                         u4UpStrDataTeLinkIf,
                                         reqResBw, TRUE, &u1PathRefreshNeeded,
                                         GMPLS_SEGMENT_REVERSE) == RPTE_FAILURE)
        {
            *pu1ErrCode = ADMISSION_CONTROL_FAILURE;
            *pu2ErrValue = DEFAULT_ERROR_VALUE;

            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Upstream bandwidth reservation failed\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhUpStrResvBwAndLblProg : INTMD-EXIT \n");

            return RPTE_FAILURE;
        }

        if ((u1PathRefreshNeeded == RPTE_REFRESH_MSG_NO) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole != RPTE_EGRESS))
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS, "PATH : PATH Refresh Not Needed\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhUpStrResvBwAndLblProg : INTMD-EXIT \n");

            return RPTE_FAILURE;
        }

        if (RptePortCfaCreateMplsTnlIf (pRsvpTeTnlInfo, PATH_MSG) ==
            RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_PH_PRCS,
                        "PATH : Mpls Tunnel Interface Creation failed\n");
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RptePhUpStrResvBwAndLblProg : INTMD-EXIT \n");
            return RPTE_FAILURE;
        }
    }

    if (RpteUpStrLblProgramming (pRsvpTeTnlInfo) == RPTE_FAILURE)
    {
        /* AS per RFC 3473, section 3.1, if an intermediate node is unable to 
         * allocate a label or internal resources, then it MUST issue a PathErr
         * message with a "Routing problem/MPLS label allocation failure"
         * indication */
        *pu1ErrCode = RPTE_NOTIFY;
        *pu2ErrValue = RPTE_LBL_ALLOC_FAIL;

        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH: Upstream Label Prog Failed - PATH Error Sent\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    RSVPTE_DBG (RSVPTE_PH_PRCS,
                "PATH: Upstream Bandwidth Reservation and Label Programming "
                "succeeded\n");
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIntermediatePrcs : EXIT\n");

    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePhAdminStatusActionItem
 * Description     : This function handles the action items to be done
 *                   before sending the admin status objece
 * Input (s)       : pRsvpTeTnlInfo   - Pointer to RSVP-TE Tnl Info
 * Output (s)      : None
 * Returns         : None
 *
****************************************************************************/
VOID
RptePhAdminStatusActionItem (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    UINT2               u2FwdMlibOperation = RPTE_ZERO;
    UINT2               u2RevMlibOperation = RPTE_ZERO;
    UINT2               u2FwdDeleteOper = RPTE_ZERO;
    UINT2               u2FwdCreateOper = RPTE_ZERO;
    UINT2               u2RevDeleteOper = RPTE_ZERO;
    UINT2               u2RevCreateOper = RPTE_ZERO;
    UINT1               u1TnlLocalProtectInUse = RPTE_ZERO;

    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE))
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus
            == GMPLS_ADMIN_ADMIN_DOWN)
        {
            u1TnlLocalProtectInUse = LOCAL_PROT_IN_USE;
        }
        else if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus
                 == GMPLS_ADMIN_UNKNOWN)
        {
            u1TnlLocalProtectInUse = LOCAL_PROT_AVAIL;
        }

        if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INTERMEDIATE)
        {
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse =
                u1TnlLocalProtectInUse;
        }
        return;
    }

    if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS) ||
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INTERMEDIATE))
    {
        u2FwdDeleteOper = MPLS_MLIB_ILM_DELETE;
        u2FwdCreateOper = MPLS_MLIB_ILM_CREATE;
    }
    else if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
    {
        u2FwdDeleteOper = MPLS_MLIB_TNL_DELETE;
        u2FwdCreateOper = MPLS_MLIB_TNL_CREATE;
    }
    if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
    {
        if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) ||
            (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INTERMEDIATE))
        {
            u2RevDeleteOper = MPLS_MLIB_ILM_DELETE;
            u2RevCreateOper = MPLS_MLIB_ILM_CREATE;
        }
        else if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_EGRESS)
        {
            u2RevDeleteOper = MPLS_MLIB_TNL_DELETE;
            u2RevCreateOper = MPLS_MLIB_TNL_CREATE;
        }
    }

    /* Action item for "A" and "D" bits:
     *   -Removing H/W programming to shut the traffic
     * */

    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus &
         GMPLS_ADMIN_ADMIN_DOWN) ||
        (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus &
         GMPLS_ADMIN_DELETE_IN_PROGRESS))
    {
        u2FwdMlibOperation = u2FwdDeleteOper;
        u2RevMlibOperation = u2RevDeleteOper;

    }

    /* Action item for resetting "A" bit:
     *   -Adding H/W programming
     * */

    if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus ==
        GMPLS_ADMIN_UNKNOWN)
    {
        u2FwdMlibOperation = u2FwdCreateOper;
        u2RevMlibOperation = u2RevCreateOper;

        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlAdminStatus = TE_ADMIN_UP;
    }
    if (u2FwdMlibOperation != RPTE_ZERO)
    {
        RpteRhSendMplsMlibUpdate (u2FwdMlibOperation, RSVPTE_ZERO,
                                  pRsvpTeTnlInfo, GMPLS_SEGMENT_FORWARD);
    }
    if (u2RevMlibOperation != RPTE_ZERO)
    {
        RpteRhSendMplsMlibUpdate (u2RevMlibOperation, RSVPTE_ZERO,
                                  pRsvpTeTnlInfo, GMPLS_SEGMENT_REVERSE);
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus &
        GMPLS_ADMIN_ADMIN_DOWN)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlAdminStatus = TE_ADMIN_DOWN;
    }

    /*calling Admin Status Trap & Syslog function */
    rpteSendAdminStatusTrapAndSyslog (RPTE_TE_TNL (pRsvpTeTnlInfo));

    /* Start graceful deletion timer if the admin status flag is deletion
     * in progress */
    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus &
         GMPLS_ADMIN_DELETE_IN_PROGRESS) &&
        (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS))
    {
        RptePhStartGracefulDelTmr (pRsvpTeTnlInfo);
    }
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhStartGracefulDelTmr
 * Description     : This function starts the GracefulDeletion Timer
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhStartGracefulDelTmr (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTimerParam        *pTmrParam = NULL;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartGracefulDelTmr: ENTRY \n");
    pTmrParam = (tTimerParam *) & (pRsvpTeTnlInfo->AdminStatusTmrParam);
    TIMER_PARAM_ID (pTmrParam) = RPTE_GRACEFUL_DEL_EXPIRE;
    pTmrParam->u.pRsvpTeTnlInfo = pRsvpTeTnlInfo;
    pRsvpTeTnlInfo->AdminStatusTimer.u4Data = (FS_ULONG) pTmrParam;

    if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST,
                           &(pRsvpTeTnlInfo->AdminStatusTimer),
                           (gpRsvpTeGblInfo->RsvpTeCfgParams.
                            u4AdminStatusTimerValue *
                            SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        /* Start Timer failed. */
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "Graceful Del Timer failed\n");
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RptePhStartGracefulDelTmr: EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePhMapWorkAndProtTnls
 * Description     : This function maps the working tunnels with protected
 *                   tunnels and vice versa .
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 *                   pPktMap        - Pointer to packet map
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePhMapWorkAndProtTnls (tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                          UINT1 *pu1PathRefreshNeeded)
{
    tRsvpTeTnlInfo     *pRsvpTeMapTnlInfo = NULL;
    tProtectionObj     *pProtectionObj = NULL;
    tAssociationObj    *pAssociationObj = NULL;
    UINT4               u4TunnelIndex = RPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RPTE_ZERO;

    if ((pPktMap->pProtectionObj == NULL) || (pPktMap->pAssociationObj == NULL))
    {
        if (pRsvpTeTnlInfo->pPsb->Protection.u1ProtectionFlag != RPTE_ZERO)
        {
            *pu1PathRefreshNeeded = RPTE_YES;
        }
        pRsvpTeTnlInfo->pPsb->Protection.u1ProtectionFlag = RPTE_ZERO;
        pRsvpTeTnlInfo->pPsb->Protection.u1LspFlag = RPTE_ZERO;
        pRsvpTeTnlInfo->pPsb->Association.u2AssocID = RPTE_ZERO;
        return;
    }

    pProtectionObj = pPktMap->pProtectionObj;
    pAssociationObj = pPktMap->pAssociationObj;

    if ((pRsvpTeTnlInfo->pPsb->Protection.u1ProtectionFlag ==
         pProtectionObj->Protection.u1ProtectionFlag) &&
        (pRsvpTeTnlInfo->pPsb->Protection.u1LspFlag ==
         pProtectionObj->Protection.u1LspFlag) &&
        (pRsvpTeTnlInfo->pPsb->Association.u2AssocID ==
         pAssociationObj->Association.u2AssocID))
    {
        return;
    }
    *pu1PathRefreshNeeded = RPTE_YES;
    if (pProtectionObj->Protection.u1ProtectionFlag & RPTE_PROTECTION_SECONDARY)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1Secondary = RPTE_TRUE;
    }

    if (pProtectionObj->Protection.
        u1ProtectionFlag & RPTE_PROTECTION_PROTECTING)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType = RPTE_TNL_PROTECTION_PATH;
    }
    else
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType = RPTE_TNL_WORKING_PATH;
    }

    pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType =
        pProtectionObj->Protection.u1LspFlag;
    pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u1LinkProtection =
        pProtectionObj->Protection.u1LinkFlag;

    pRsvpTeTnlInfo->pPsb->Protection.u1ProtectionFlag =
        pProtectionObj->Protection.u1ProtectionFlag;

    pRsvpTeTnlInfo->pPsb->Protection.u1LspFlag =
        pProtectionObj->Protection.u1LspFlag;

    pRsvpTeTnlInfo->pPsb->Association.u2AssocID =
        OSIX_NTOHS (pAssociationObj->Association.u2AssocID);

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE)
    {
        return;
    }

    u4TunnelIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex;
    CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID (pRsvpTeTnlInfo->pTeTnlInfo),
                        u4TunnelIngressLSRId);
    u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
    CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID (pRsvpTeTnlInfo->pTeTnlInfo),
                        u4TunnelEgressLSRId);
    u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_PROTECTION_PATH)
    {
        if (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                    u4TunnelIndex,
                                    pRsvpTeTnlInfo->pPsb->
                                    Association.u2AssocID,
                                    u4TunnelIngressLSRId,
                                    u4TunnelEgressLSRId,
                                    &pRsvpTeMapTnlInfo) == RPTE_SUCCESS)
        {
            pRsvpTeTnlInfo->pMapTnlInfo = pRsvpTeMapTnlInfo;
            pRsvpTeMapTnlInfo->pMapTnlInfo = pRsvpTeTnlInfo;
            pRsvpTeMapTnlInfo->pTeTnlInfo->u4BkpTnlIndex = u4TunnelIndex;
            pRsvpTeMapTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
                pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance;

            pRsvpTeTnlInfo->pTeTnlInfo->u4BkpTnlIndex =
                pRsvpTeMapTnlInfo->pTeTnlInfo->u4TnlIndex;
            pRsvpTeTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
                pRsvpTeMapTnlInfo->pTeTnlInfo->u4TnlInstance;
            MEMCPY (pRsvpTeMapTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                    &RSVPTE_TNL_INGRESS_RTR_ID (pRsvpTeTnlInfo->pTeTnlInfo),
                    ROUTER_ID_LENGTH);
            MEMCPY (pRsvpTeMapTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                    &RSVPTE_TNL_EGRESS_RTR_ID (pRsvpTeTnlInfo->pTeTnlInfo),
                    ROUTER_ID_LENGTH);

            MEMCPY (pRsvpTeTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                    &RSVPTE_TNL_INGRESS_RTR_ID (pRsvpTeMapTnlInfo->pTeTnlInfo),
                    ROUTER_ID_LENGTH);
            MEMCPY (pRsvpTeTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                    &RSVPTE_TNL_EGRESS_RTR_ID (pRsvpTeMapTnlInfo->pTeTnlInfo),
                    ROUTER_ID_LENGTH);
        }
    }

    /* If protection is available fill u4SendResvNotifyRecipient as
     * the egress id of the tunnel
     * */
    pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
        u4SendResvNotifyRecipient = u4TunnelEgressLSRId;

    /* Admin status capability should be enabled in the node to refresh the
     * messages for working tunnels with Admin status object, 
     * when the protection path is in use
     * */

    if (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_WORKING_PATH)
    {

        if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable !=
            RPTE_ENABLED)
        {
            gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable =
                RPTE_ENABLED;
            gpRsvpTeGblInfo->RsvpTeCfgParams.u1IntAdminStatusCapable =
                RPTE_TRUE;
        }
        if (pRsvpTeTnlInfo->pPsb->Association.u2AssocID != RPTE_ZERO)
        {
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse =
                LOCAL_PROT_AVAIL;
        }
        else
        {
            pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse =
                LOCAL_PROT_NOT_AVAIL;
            if (pRsvpTeMapTnlInfo != NULL)
            {
                pRsvpTeMapTnlInfo->pTeTnlInfo->u4BkpTnlIndex = RPTE_ZERO;
                pRsvpTeMapTnlInfo->pTeTnlInfo->u4BkpTnlInstance = RPTE_ZERO;
                MEMSET (&pRsvpTeMapTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                        RPTE_ZERO, ROUTER_ID_LENGTH);
                MEMSET (&pRsvpTeMapTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                        RPTE_ZERO, ROUTER_ID_LENGTH);
            }
        }
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
        MPLS_TE_DEDICATED_ONE2ONE)
    {
        pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRerouteFlag = FALSE;
    }
    return;
}

/****************************************************************************/
/* Function Name   : RptePhStartEroCacheTmr                              */
/* Description     : This function starts the Refresh Timer for the Rsb     */
/* Input (s)       : *pRsb -  Pointer to the Rsb                            */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePhStartEroCacheTmr (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTimerParam        *pTmrParam = NULL;

    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RptePhStartEroCacheTmr : ENTRY\n");

    pTmrParam = (tTimerParam *) & RPTE_ERO_CACHE_TIMER_PARAM (pRsvpTeTnlInfo);

    TIMER_PARAM_ID (pTmrParam) = RPTE_ERO_CACHE_INTERVAL;
    TIMER_PARAM_RPTE_TNL (pTmrParam) = pRsvpTeTnlInfo;
    RSVP_TIMER_NAME (&RPTE_ERO_CACHE_TIMER (pRsvpTeTnlInfo)) =
        (FS_ULONG) pTmrParam;
    if (RpteUtlStartTimer
        (RSVP_GBL_TIMER_LIST, &RPTE_ERO_CACHE_TIMER (pRsvpTeTnlInfo),
         (UINT4) (RSVPTE_ERO_CACHE_INTERVAL (gpRsvpTeGblInfo) *
                  SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_REOPT_DBG,
                    "RESV: Starting of RptePhStartEroCacheTmr failed ..\n");
    }
    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RptePhStartEroCacheTmr : EXIT\n");
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptepath.c                             */
/*---------------------------------------------------------------------------*/
