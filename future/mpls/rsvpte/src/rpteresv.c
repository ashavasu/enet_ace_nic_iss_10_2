/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rpteresv.c,v 1.82 2017/06/08 11:40:32 siva Exp $
 *
 * Description: This file contains routines for the Resv Handler Module.
 ********************************************************************/

#include "rpteincs.h"

/****************************************************************************/
/* Function Name   : RpteRhCreateRsb                                        */
/* Description     : This function searches for a free entry in RsbArray. If*/
/*                   found, marks the enrry as ACTIVE, initializes the entry*/
/*                   and returns a pointer to that entry.                   */
/* Input (s)       : pRsvpTeFilterSpec - Pointer to RsvpTeFilterSpec        */
/*                   pPktMap           - Pointer to PktMap                  */
/*                   pIfEntry          - Pointer to IfEntry                 */
/* Output (s)      : ppRsb             - pointer to pointer of IfEntry      */
/* Returns         : If successful, returns RPTE_SUCCESS. Else returns      */
/*                   RPTE_FAILURE                                           */
/****************************************************************************/
UINT1
RpteRhCreateRsb (const tRsvpTeFilterSpec * pRsvpTeFilterSpec, tPktMap * pPktMap,
                 tIfEntry * pOutIfEntry, tRsb ** ppRsb)
{
    tRsb               *pRsb = NULL;
    tTimeValues        *pTimeValues = NULL;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhCreateRsb : ENTRY \n");

    pRsb = RpteUtlCreateRsb ();

    if (pRsb == NULL)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MISC, " Rsb creation failed \n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteRhCreateRsb : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    RSB_RPTE_FILTER_SPEC (pRsb) = *pRsvpTeFilterSpec;

    if (pPktMap->pRsvpHopObj != NULL)
    {
        (RSB_NEXT_RSVP_HOP (pRsb)).u4HopAddr =
            OSIX_NTOHL ((RSVP_HOP_OBJ (PKT_MAP_RSVP_HOP_OBJ (pPktMap))).
                        u4HopAddr);
        (RSB_NEXT_RSVP_HOP (pRsb)).u4Lih =
            (RSVP_HOP_OBJ (PKT_MAP_RSVP_HOP_OBJ (pPktMap))).u4Lih;
    }
    else if (pPktMap->pIfIdRsvpHopObj != NULL)
    {
        (RSB_NEXT_RSVP_HOP (pRsb)).u4HopAddr =
            OSIX_NTOHL (pPktMap->pIfIdRsvpHopObj->RsvpHop.u4HopAddr);
        (RSB_NEXT_RSVP_HOP (pRsb)).u4Lih =
            pPktMap->pIfIdRsvpHopObj->RsvpHop.u4Lih;
    }
    else if (pPktMap->pIfIdRsvpNumHopObj != NULL)
    {
        (RSB_NEXT_RSVP_HOP (pRsb)).u4HopAddr =
            OSIX_NTOHL (pPktMap->pIfIdRsvpNumHopObj->RsvpHop.u4HopAddr);
        (RSB_NEXT_RSVP_HOP (pRsb)).u4Lih =
            pPktMap->pIfIdRsvpNumHopObj->RsvpHop.u4Lih;
    }

    RSB_STYLE (pRsb) = STYLE_OBJ (PKT_MAP_STYLE_OBJ (pPktMap));
    RSB_FLOW_SPEC (pRsb) = FLOW_SPEC_OBJ (PKT_MAP_FLOW_SPEC_OBJ (pPktMap));
    RSB_OUT_IF_ENTRY (pRsb) = pOutIfEntry;
    pTimeValues = &TIME_VALUES_OBJ (PKT_MAP_TIME_VALUES_OBJ (pPktMap));
    RSB_REFRESH_INTERVAL (pRsb) = OSIX_NTOHL (TIME_VALUES_PERIOD (pTimeValues));
    RSB_TIME_TO_DIE (pRsb) = RpteUtlComputeLifeTime (PKT_MAP_IF_ENTRY
                                                     (pPktMap),
                                                     OSIX_NTOHL
                                                     (TIME_VALUES_PERIOD
                                                      (pTimeValues)));
    RSB_IN_REFRESH_INTERVAL (pRsb) = OSIX_NTOHL (TIME_VALUES_PERIOD
                                                 (pTimeValues));
    /* If ResvConf obj is present in the ResvMesg it is copied */
    if (PKT_MAP_RESV_CONF_OBJ (pPktMap) != NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV : ResvConf object is added to the Rsb..\n");
        RSB_RESV_CONF (pRsb) = RESV_CONF_OBJ (PKT_MAP_RESV_CONF_OBJ (pPktMap));
    }
    OsixGetSysTime ((tOsixSysTime *) & (RSB_LAST_CHANGE (pRsb)));

    if (pPktMap->pNotifyRequestObj != NULL)
    {
        pRsb->u4NotifyAddr = OSIX_NTOHL
            (pPktMap->pNotifyRequestObj->u4NotifyNodeAddr);
    }

    OsixGetSysTime ((tOsixSysTime *) & (RSB_LAST_CHANGE (pRsb)));

    *ppRsb = pRsb;
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhCreateRsb : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteRhGetLabel                                            */
/* Description   : This routine allocates the next label to be used.         */
/* Input(s)      : pGetLblIfEntry - Pointer to the Interface Entry for       */
/*                 which the label is to be obtained from its label space.   */
/*               : pLabel - Pointer to the label in which the label value    */
/*                 to be filled in case of successful label allocation.      */
/* Output(s)     : None                                                      */
/* Return(s)     : RPTE_SUCESS or RPTE_FAILURE                               */
/*****************************************************************************/
UINT1
RpteRhGetLabel (const tIfEntry * pGetLblIfEntry, uLabel * pLabel)
{
    UINT4               u4Label1 = RSVPTE_ZERO;
    UINT4               u4Label2 = RSVPTE_ZERO;
    UINT2               u2LabelRangeId = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhGetLabel : ENTRY \n");

    if (pGetLblIfEntry == NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV : Label If Entry must not be NULL..\n");
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhGetLabel : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    /* If the Interface type is ATM or FR or ... */
    if (RPTE_IF_LBL_TYPE (pGetLblIfEntry) == RPTE_ATM)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS, "RESV : Interface is of type ATM..\n");
        u2LabelRangeId = pGetLblIfEntry->RpteIfInfo.u2LblSpaceId;
    }

    /* If the Interface type is Ethernet */
    if (RPTE_IF_LBL_TYPE (pGetLblIfEntry) == RPTE_ETHERNET)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS, "RESV : Interface is of type Ethernet..\n");
        u2LabelRangeId = gpRsvpTeGblInfo->u2GenbLblSpaceGrpId;
    }
    /* 
     * Currently u2IncarnId is given a value RSVPTE_ZERO since RSVPTE Tnl does
     * not have incarn id & the function expects u2InCarnId as a parameter 
     */
    if (RPTE_MPLS_GET_LBL_FROM_LBLGROUP (u2LabelRangeId,
                                         &u4Label1,
                                         &u4Label2) == RPTE_LBL_MNGR_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV : Getting Label from LabelGroup failed..\n");
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhGetLabel : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /*
     * The label range type is checked as whether it is per platform based
     * or is it per interface based. In case of per interface based, the label
     * is currently assumed to be of ATM label type. The VPI value would have
     * been obtained in u4label1 and the VCI value would have been obtained
     * in the u4label2. The VPI and VCI values are to be combined and given
     * back as a single label.
     */
    if (RPTE_IF_LBL_TYPE (pGetLblIfEntry) == RPTE_ETHERNET)
    {
        pLabel->u4GenLbl = u4Label2;
    }
    else if (RPTE_IF_LBL_TYPE (pGetLblIfEntry) == RPTE_ATM)
    {
        pLabel->AtmLbl.u2Vpi = (UINT2) u4Label1;
        pLabel->AtmLbl.u2Vci = (UINT2) u4Label2;
    }
    RSVPTE_DBG (RSVPTE_RH_PRCS,
                "RESV : Getting Label from LabelGroup succeeded..\n");
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhGetLabel : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteRhFreeLabel                                           */
/* Description   : This routine frees the label for reuse.                   */
/* Input(s)      : pRetLblIfEntry - Pointer to the Interface Entry for       */
/*                 which the label is to be released.                        */
/*                 pLabel - pointer to the label that is to be released.     */
/* Output(s)     : None                                                      */
/* Return(s)     : RPTE_SUCCESS or RPTE_FAILURE                              */
/*****************************************************************************/
UINT1
RpteRhFreeLabel (const tIfEntry * pRetLblIfEntry, uLabel * pLabel)
{
    UINT4               u4Label1 = RSVPTE_ZERO;
    UINT4               u4Label2 = RSVPTE_ZERO;
    UINT2               u2LabelRangeId = RSVPTE_ZERO;
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhFreeLabel : ENTRY\n");
    /* Do not remove the label in label manager, when the RSVP-TE component is made
     * shut with GR enabled
     * */
    if (gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_SHUT_DOWN_IN_PROGRESS)
    {
        return RPTE_SUCCESS;
    }

    if (RPTE_IF_LBL_TYPE (pRetLblIfEntry) == RPTE_ATM)
    {
        u2LabelRangeId = pRetLblIfEntry->RpteIfInfo.u2LblSpaceId;
        u4Label1 = (UINT2) pLabel->AtmLbl.u2Vpi;
        u4Label2 = (UINT2) pLabel->AtmLbl.u2Vci;
    }
    if (RPTE_IF_LBL_TYPE (pRetLblIfEntry) == RPTE_ETHERNET)
    {
        u2LabelRangeId = gpRsvpTeGblInfo->u2GenbLblSpaceGrpId;
        u4Label2 = pLabel->u4GenLbl;
    }

    if ((RPTE_MPLS_REL_LBL_TO_LBLGROUP (u2LabelRangeId,
                                        u4Label1,
                                        u4Label2)) != RPTE_LBL_MNGR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV : Returning Label to LabelGroup failed..\n");
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhFreeLabel : INTMD-EXIT\n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_RH_PRCS,
                "RESV : Returning Label to LabelGroup succeeded..\n");
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhFreeLabel : EXIT\n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name   : RpteRhStartRROBackOffTimer                              */
/* Description     : This function starts the RRO Back off Timer             */
/* Input (s)       : pCtTnlInfo - Pointer to RsvpTeTnlInfo Structure.        */
/* Output (s)      : If the timer value has exceeded the Maximum Retry time  */
/*                   u4MRTime, frees the RRO List                            */
/* Returns         : None                                                    */
/*****************************************************************************/
VOID
RpteRhStartRROBackOffTimer (tRsvpTeTnlInfo * pCtTnlInfo)
{
    tTimerParam        *pTmrParam = NULL;
    UINT4               u4Time = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhStartRROBackOffTimer : ENTRY\n");
    pTmrParam = (tTimerParam *) & pCtTnlInfo->RROTmrParam;
    /* If the timer is present stop the timer */
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_TNL_RRO_TIMER (pCtTnlInfo));

    OsixGetSysTime ((tOsixSysTime *) & (u4Time));
    if (pCtTnlInfo->u2BTime == RSVPTE_ZERO)
    {
        if ((((RSVPTE_TNL_RSB (pCtTnlInfo))->u4RefreshInterval) /
             DEFAULT_CONV_TO_SECONDS) < RPTE_DEF_BACK_OFF_TIME)
        {
            pCtTnlInfo->u2BTime =
                (UINT2) (pCtTnlInfo->u2BTime + RPTE_DEF_BACK_OFF_TIME);
            RSVPTE_DBG1 (RSVPTE_RH_PRCS,
                         "RESV : RRO BackOff timer is set with "
                         "default value: %d\n", pCtTnlInfo->u2BTime);
        }
        else
        {
            pCtTnlInfo->u2BTime =
                (UINT2) (pCtTnlInfo->u2BTime +
                         ((RSVPTE_TNL_RSB (pCtTnlInfo)->u4RefreshInterval) /
                          DEFAULT_CONV_TO_SECONDS));
            RSVPTE_DBG1 (RSVPTE_RH_PRCS,
                         "RESV : RRO BackOff timer Started with Refresh "
                         "interval: %d\n", pCtTnlInfo->u2BTime);
        }
        pCtTnlInfo->u4MRTime =
            u4Time + (RPTE_MAX_RETRY * (pCtTnlInfo->u2BTime));
    }
    if (u4Time >= pCtTnlInfo->u4MRTime)
    {
        RSVPTE_DBG2 (RSVPTE_RH_PRCS,
                     "RESV : Current time:%d exceeded the maximum RRO "
                     "BackOff timer:%d\n", u4Time, pCtTnlInfo->u4MRTime);
        rpteTeDeleteArHopListInfo (RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo));
        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                    "RpteRhStartRROBackOffTimer : INTMD-EXIT\n");
        return;
    }
    else
    {
        TIMER_PARAM_ID (pTmrParam) = RPTE_RH_RRO_BOFF_TIMER;
        TIMER_PARAM_RPTE_TNL (pTmrParam) = pCtTnlInfo;
        RSVP_TIMER_NAME (&RPTE_TNL_RRO_TIMER (pCtTnlInfo)) =
            (FS_ULONG) pTmrParam;
        if (RpteUtlStartTimer
            (RSVP_GBL_TIMER_LIST, &RPTE_TNL_RRO_TIMER (pCtTnlInfo),
             ((pCtTnlInfo->u2BTime) * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) !=
            TMR_SUCCESS)
        {
            /* Start Timer failed. */
            RSVPTE_DBG (RSVPTE_RH_PRCS,
                        "RESV : Starting of RRO BackOff timer failed ..\n");
            RSVPTE_DBG (RSVPTE_RH_ETEXT,
                        "RpteRhStartRROBackOffTimer : INTMD-EXIT\n");
            return;
        }
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhStartRROBackOffTimer : EXIT\n");
        return;
    }
}

/*****************************************************************************/
/* Function Name   : RpteRhProcRROBackoffTimeOut                             */
/* Description     : This function processes the expired RRO Back off Timer  */
/* Input (s)       : pExpiredTmr - Pointer to Expired Timer                  */
/* Output (s)      : None                                                    */
/* Returns         : None                                                    */
/*****************************************************************************/

VOID
RpteRhProcRROBackoffTimeOut (const tTmrAppTimer * pExpiredTmr)
{
    tPktMap             RROPktMap;
    tStyleObj           StyleObj;
    tRsvpTeSsnObj       SsnObj;
    tRsvpTeFilterSpecObj RsvpTeFilterSpecObj;
    tRsvpHopObj         RsvpHopObj;
    tFlowSpecObj        FlowSpecObj;
    tIfEntry           *pIfEntry = NULL;
    tTimerParam        *pTmrParam = NULL;
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;

    if ((pIfEntry = MemAllocMemBlk (RSVPTE_IF_ENTRY_POOL_ID)) == NULL)
    {
        return;
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "rpteRhProcRROBackOffTimeOut : ENTRY\n");
    RpteUtlInitPktMap (&RROPktMap);

    MEMSET (&SsnObj, RSVPTE_ZERO, sizeof (tRsvpTeSsnObj));
    MEMSET (&StyleObj, RSVPTE_ZERO, sizeof (tStyleObj));
    MEMSET (&RsvpTeFilterSpecObj, RSVPTE_ZERO, sizeof (tRsvpTeFilterSpecObj));
    MEMSET (&FlowSpecObj, RSVPTE_ZERO, sizeof (tFlowSpecObj));
    MEMSET (&RsvpHopObj, RSVPTE_ZERO, sizeof (tRsvpHopObj));
    MEMSET (pIfEntry, RSVPTE_ZERO, sizeof (tIfEntry));

    PKT_MAP_RSVP_HOP_OBJ (&RROPktMap) = &RsvpHopObj;
    PKT_MAP_IF_ENTRY (&RROPktMap) = pIfEntry;
    PKT_MAP_RPTE_SSN_OBJ (&RROPktMap) = &SsnObj;
    PKT_MAP_RPTE_FILTER_SPEC_OBJ (&RROPktMap) = &RsvpTeFilterSpecObj;
    PKT_MAP_FLOW_SPEC_OBJ (&RROPktMap) = &FlowSpecObj;
    PKT_MAP_STYLE_OBJ (&RROPktMap) = &StyleObj;

    pTmrParam = (tTimerParam *) RSVP_TIMER_NAME (pExpiredTmr);
    pCtTnlInfo = TIMER_PARAM_RPTE_TNL (pTmrParam);
    RpteUtlRhPreparePktMapRROErr (&RROPktMap, pCtTnlInfo);

    RPTE_PKT_MAP_NBR_ENTRY (&RROPktMap) = RPTE_TNL_DSTR_NBR (pCtTnlInfo);

    RptePvmSendResvErr (&RROPktMap, RPTE_NOTIFY, RPTE_RRO_TOO_LARGE);
    RSVPTE_DBG (RSVPTE_RH_PRCS,
                "RESV : RRO MTU size err - Resv Err sent - "
                "RRO Notification \n");
    RpteRhStartRROBackOffTimer (pCtTnlInfo);
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "rpteRhProcRROBackOffTimeOut : EXIT\n");
    MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfEntry);
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhProcessRsbRefreshTimeOut                         */
/* Description     : This function processes the Expired Rsb Refresh Timer  */
/* Input (s)       : *pExpiredTimer - Pointer to the Expired Timer List     */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhProcessRsbRefreshTimeOut (const tTmrAppTimer * pExpiredTmr)
{
    tTimerParam        *pTmrParam = NULL;
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;
    tRsb               *pRsb = NULL;
    tErrorSpec          ErrorSpec;
    tNbrEntry          *pUpNbrEntry = NULL;
    tNbrEntry          *pDnNbrEntry = NULL;
    UINT4               u4Time = RSVPTE_ZERO;
    UINT1               u1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;
    UINT1               u1ResvRsrc = RPTE_FALSE;
    UINT1               u1DetourGrpId = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessRsbRefreshTimeOut : ENTRY\n");

    MEMSET (&ErrorSpec, RSVPTE_ZERO, sizeof (tErrorSpec));
    pTmrParam = (tTimerParam *) RSVP_TIMER_NAME (pExpiredTmr);
    pRsb = (tRsb *) TIMER_PARAM_RSB (pTmrParam);

    pCtTnlInfo = RSB_RPTE_TNL_INFO (pRsb);

    /* If the function is invoked from Egress node irrespective of the
     * RSB_TIME_TO_DIE, it should construct ResvMesg & send it to the
     * PrevHop */
    if ((pCtTnlInfo == NULL) || (RSVPTE_TNL_RSB (pCtTnlInfo) == NULL))
    {
        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                    "RpteRhProcessRsbRefreshTimeOut : INTMD - EXIT\n");
        return;
    }

    pUpNbrEntry = RPTE_TNL_USTR_NBR (pCtTnlInfo);
    pDnNbrEntry = RPTE_TNL_DSTR_NBR (pCtTnlInfo);

    if ((pUpNbrEntry != NULL) &&
        (pUpNbrEntry->u1GrProgressState == RPTE_GR_RESTART_IN_PROGRESS))
    {
        RpteRhStartResvRefreshTmr (RSVPTE_TNL_RSB (pCtTnlInfo));
        return;
    }

    OsixGetSysTime ((tOsixSysTime *) & (u4Time));
    if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_EGRESS)

    {
        u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
        RSVPTE_DBG2 (RSVPTE_RH_PRCS,
                     "RESV : RefreshTimeOut  CurrTime = %d secs,"
                     "RsbTimeToDie = %d secs \n",
                     u4Time, RSB_TIME_TO_DIE (pRsb) + u4Time);

    }

    else if (pCtTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
             LOCAL_PROT_IN_USE)
    {
        u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
    }

    else if ((u4Time >= RSB_TIME_TO_DIE (pRsb)) &&
             (((pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                MPLS_TE_DEDICATED_ONE2ONE) &&
               (pCtTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                LOCAL_PROT_AVAIL)) ||
              (pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
               MPLS_TE_FULL_REROUTE)))

    {
        if (pCtTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INTERMEDIATE)
        {
            RpteNhHandlePsbOrRsbOrHelloTimeOut (pCtTnlInfo,
                                                RPTE_LSP_LOCALLY_FAILED);
            u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
        }
        else if (pCtTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS)
        {
            if (pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                MPLS_TE_DEDICATED_ONE2ONE)
            {
                RpteNhProcessOneToOneTnls (pCtTnlInfo, RPTE_ZERO,
                                           RPTE_LSP_LOCALLY_FAILED);
            }
            else
            {
                RpteNhProcessFullyReRouteTnls (pCtTnlInfo,
                                               RSVPTE_TNL_DN_NHOP (pCtTnlInfo));
            }
        }
    }
    /*Block the Resv message expiry, if the tunnel is in recovery state */
    else if ((u4Time >= RSB_TIME_TO_DIE (pRsb)) &&
             (pCtTnlInfo->pTeTnlInfo->u1TnlSyncStatus ==
              TNL_GR_NOT_SYNCHRONIZED) &&
             ((pDnNbrEntry != NULL) &&
              ((pDnNbrEntry->u1GrProgressState == RPTE_GR_RESTART_IN_PROGRESS)
               || (pDnNbrEntry->u1GrProgressState ==
                   RPTE_GR_RECOVERY_IN_PROGRESS))))
    {
        u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
    }
    /* If a refresh timeout occurs, at a node, for a LSP, while expecting a 
     * Resv Refresh message from a neighbor and that neighbor is GR Capable, 
     * then action for the refresh failure is taken upon next successful Hello communication.
     * */
    else if ((u4Time >= RSB_TIME_TO_DIE (pRsb)) &&
             (pDnNbrEntry != NULL) &&
             (pDnNbrEntry->u2RestartTime != RPTE_ZERO) &&
             (pDnNbrEntry->bIsHelloSynchronoziedAfterRestart != RPTE_TRUE))
    {
        u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
    }
    /* If the timer has expired & no valid ResvMesg is received from the
     * NextHop the Rsb is cleaned up & the resources are released 
     */
    else if (u4Time >= RSB_TIME_TO_DIE (pRsb))
    {
        RSVPTE_DBG2 (RSVPTE_RH_PRCS,
                     "RESV : RefreshTimeOut  CurrTime = %d secs,"
                     "RsbTimeToDie = %d secs \n",
                     u4Time, RSB_TIME_TO_DIE (pRsb));

        if (RPTE_IS_FRR_CAPABLE (pCtTnlInfo) != RPTE_TRUE)
        {
            RpteRhRsbRefreshTimeOutDestroy (pCtTnlInfo);
            RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                        &u1ResvRefreshNeeded, &ErrorSpec);

            TmrStopTimer (RSVP_GBL_TIMER_LIST, &RSB_TIMER (pRsb));
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &RPTE_TNL_RRO_TIMER (pCtTnlInfo));

            RpteUtlDeleteRsb (pRsb);

            RSVPTE_DBG (RSVPTE_RH_ETEXT,
                        "RpteRhProcessRsbRefreshTimeOut : EXIT\n");
            return;
        }
        u1DetourGrpId = RPTE_FRR_DETOUR_GRP_ID (pCtTnlInfo);
        if (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_PROTECTED_TNL)
        {
            do
            {
                if (RSVPTE_TNL_RSB (pCtTnlInfo) == NULL)
                {
                    continue;
                }

                pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);

                RpteRhRsbRefreshTimeOutDestroy (pCtTnlInfo);
                RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                            &u1ResvRefreshNeeded, &ErrorSpec);

                TmrStopTimer (RSVP_GBL_TIMER_LIST, &RSB_TIMER (pRsb));
                TmrStopTimer (RSVP_GBL_TIMER_LIST,
                              &RPTE_TNL_RRO_TIMER (pCtTnlInfo));

                RpteUtlDeleteRsb (pRsb);
            }
            while ((pCtTnlInfo = RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)) != NULL);
            RSVPTE_DBG (RSVPTE_RH_ETEXT,
                        "RpteRhProcessRsbRefreshTimeOut : EXIT\n");
            return;
        }
        if (RSVPTE_FRR_DETOUR_MERGING_CAPABLE (gpRsvpTeGblInfo) == RPTE_TRUE)
        {
            if (RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL)
            {
                pCtTnlInfo = RPTE_FRR_BASE_TNL (pCtTnlInfo);
            }
            do
            {
                if (RSVPTE_TNL_RSB (pCtTnlInfo) == NULL)
                {
                    continue;
                }

                if (RPTE_FRR_DETOUR_GRP_ID (pCtTnlInfo) != u1DetourGrpId)
                {
                    continue;
                }
                pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
                RpteRhRsbRefreshTimeOutDestroy (pCtTnlInfo);
                RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                            &u1ResvRefreshNeeded, &ErrorSpec);

                TmrStopTimer (RSVP_GBL_TIMER_LIST, &RSB_TIMER (pRsb));
                TmrStopTimer (RSVP_GBL_TIMER_LIST,
                              &RPTE_TNL_RRO_TIMER (pCtTnlInfo));

                RpteUtlDeleteRsb (pRsb);
            }
            while ((pCtTnlInfo = RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)) != NULL);
            RSVPTE_DBG (RSVPTE_RH_ETEXT,
                        "RpteRhProcessRsbRefreshTimeOut : EXIT\n");
            return;
        }
        else
        {
            RpteRhRsbRefreshTimeOutDestroy (pCtTnlInfo);
            RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                        &u1ResvRefreshNeeded, &ErrorSpec);

            TmrStopTimer (RSVP_GBL_TIMER_LIST, &RSB_TIMER (pRsb));
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &RPTE_TNL_RRO_TIMER (pCtTnlInfo));

            RpteUtlDeleteRsb (pRsb);
        }
    }
    else
    {
        RSVPTE_DBG2 (RSVPTE_RH_PRCS,
                     "RESV : RefreshTimeOut CurrTime = %ld secs,"
                     "RsbCmnHdrTimeToDie = %ld secs \n",
                     u4Time, RSB_TIME_TO_DIE (pRsb));
        u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
    }
    if (u1ResvRefreshNeeded == RPTE_REFRESH_MSG_YES)
    {
        RpteRhResvRefresh (pCtTnlInfo);
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessRsbRefreshTimeOut : EXIT\n");
}

/****************************************************************************/
/* Function Name   : RptePhRhProcessWaitTimeOut                             */
/* Description     : This function processes the Expired Rsb Wait Timer     */
/*                   Psb Wait Timer                                         */
/* Input (s)       : *pExpiredTimer - Pointer to the Expired Timer List     */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePhRhProcessWaitTimeOut (const tTmrAppTimer * pExpiredTmr)
{
    tTimerParam        *pTmrParam = NULL;
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pOrgCtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTmpTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTePrevTnlInfo = NULL;
    tRsb               *pRsb = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tErrorSpec          ErrorSpec;
    UINT4               u4Time = RSVPTE_ZERO;
    UINT1               u1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;
    UINT1               u1ResvRsrc = RPTE_FALSE;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RptePhRhProcessWaitTimeOut : ENTRY\n");

    MEMSET (&ErrorSpec, RSVPTE_ZERO, sizeof (tErrorSpec));

    OsixGetSysTime ((tOsixSysTime *) & (u4Time));
    pTmrParam = (tTimerParam *) RSVP_TIMER_NAME (pExpiredTmr);
    pCtTnlInfo = TIMER_PARAM_RPTE_TNL (pTmrParam);

    if (pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
        MPLS_TE_DEDICATED_ONE2ONE)
    {
        if ((pCtTnlInfo->pTeTnlInfo->u1TnlPathType ==
             RPTE_TNL_WORKING_PATH) &&
            (pCtTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse !=
             LOCAL_PROT_NOT_AVAIL))
        {
            RSVPTE_DBG (RSVPTE_RH_ETEXT, "RptePhRhProcessWaitTimeOut Calling "
                        " RpteNhProcessOneToOneTnls For Locally Failed\n");
            RpteNhProcessOneToOneTnls (pCtTnlInfo, RPTE_ZERO,
                                       RPTE_LSP_LOCALLY_FAILED);
            return;
        }
        else if ((pCtTnlInfo->pTeTnlInfo->u1TnlPathType ==
                  RPTE_TNL_PROTECTION_PATH) &&
                 (pCtTnlInfo->pMapTnlInfo != NULL))
        {
            pCtTnlInfo->pMapTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse =
                LOCAL_PROT_NOT_AVAIL;
        }
    }

    if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
    {
        /*
         * If the timer has expired & no valid ResvMesg is received from the
         * NextHop the Rsb is cleaned up & the resources are released
         */

        pNbrEntry = RPTE_TNL_DSTR_NBR (pCtTnlInfo);
        /*Block the path message expiry, if the tunnel is in recovery state */
        if ((pCtTnlInfo->pTeTnlInfo->u1TnlSyncStatus == TNL_GR_NOT_SYNCHRONIZED)
            && ((pNbrEntry != NULL)
                &&
                ((pNbrEntry->u1GrProgressState == RPTE_GR_RESTART_IN_PROGRESS)
                 || (pNbrEntry->u1GrProgressState ==
                     RPTE_GR_RECOVERY_IN_PROGRESS))))
        {
            return;
        }
        /* If a refresh timeout occurs, at a node, for a LSP, while expecting a    
         * Path Refresh message from a neighbor and that neighbor is GR Capable,
         * then action for the refresh failure is taken upon next successful Hello communication.
         * */
        else if ((pNbrEntry != NULL) &&
                 (pNbrEntry->u2RestartTime != RPTE_ZERO) &&
                 (pNbrEntry->bIsHelloSynchronoziedAfterRestart != RPTE_TRUE))
        {
            return;
        }
        if (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_PROTECTED_TNL)
        {
            if ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pCtTnlInfo)).u1Flags
                 == RSVPTE_TNL_FRR_FACILITY_METHOD)
                && RPTE_IS_HEADEND_PLR (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
            {
                if (pCtTnlInfo->b1IsProcessWaitTimeOutForProtTunnel ==
                    RPTE_FALSE)
                {
                    pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
                    if (pRsb == NULL)
                    {
                        return;
                    }
                    RSVPTE_DBG (RSVPTE_RH_PRCS,
                                "RESV : WaitTimeOut Occurs for first time "
                                "after fault at protected tunnel in case of Facility backup method\n");
                    RSVPTE_DBG2 (RSVPTE_RH_PRCS,
                                 "RESV : WaitTimeOut  CurrTime = %d secs,"
                                 "RsbCmnHdrTimeToDie = %d secs\n", u4Time,
                                 RSB_TIME_TO_DIE (pRsb));

                    RSB_TIME_TO_DIE (pRsb) =
                        RpteUtlComputeLifeTime (RSB_OUT_IF_ENTRY (pRsb),
                                                RSB_REFRESH_INTERVAL (pRsb));

                    RpteRhPhStartMaxWaitTmr (pCtTnlInfo,
                                             RSB_TIME_TO_DIE (pRsb));
                    pCtTnlInfo->b1IsProcessWaitTimeOutForProtTunnel = RPTE_TRUE;
                    return;
                }
            }
        }
        pOrgCtTnlInfo = pCtTnlInfo;
        do
        {
            pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
            if (pRsb == NULL)
            {
                continue;
            }
            RSVPTE_DBG2 (RSVPTE_RH_PRCS,
                         "RESV : WaitTimeOut  CurrTime = %d secs,"
                         "RsbCmnHdrTimeToDie = %d secs\n",
                         u4Time, RSB_TIME_TO_DIE (pRsb));
            RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE, RSVPTE_ZERO,
                                      pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
            if (pCtTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
            {
                RpteRhSendMplsMlibUpdate
                    (RPTE_MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO, pCtTnlInfo,
                     GMPLS_SEGMENT_REVERSE);
                RpteRhFreeLabel (PSB_OUT_IF_ENTRY
                                 (RSVPTE_TNL_PSB (pCtTnlInfo)),
                                 &pCtTnlInfo->UpStrInLbl);
                pCtTnlInfo->UpStrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
            }
            RpteDSRelTrafficControl (pCtTnlInfo);

            RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                        &u1ResvRefreshNeeded, &ErrorSpec);
            RpteUtlDeleteRsb (pRsb);
        }
        while ((pCtTnlInfo = RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo)) != NULL);

        pCtTnlInfo = pOrgCtTnlInfo;

        if ((RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL) &&
            (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_BACKUP_TNL))
        {
            /* Backup tunnel's max wait timer is expired so,
             * removing the PLR state */
            RpteResetPlrStatus (RPTE_FRR_BASE_TNL (pCtTnlInfo));
        }
        RptePhGeneratePathTear (pCtTnlInfo);
        RPTE_TNL_DEL_TNL_FLAG (pCtTnlInfo) = RPTE_TRUE;
        if (RPTE_TE_TNL (pCtTnlInfo)->u1TnlRelStatus ==
            TE_SIGMOD_TNLREL_AWAITED)
        {
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtTnlInfo,
                                 TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
        }
        else
        {
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
        }
    }
    else if ((RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_EGRESS) ||
             ((RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_BACKUP_TNL) &&
              (RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL) &&
              (RPTE_IS_MP (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL
                                                (pCtTnlInfo))))))
    {
        pNbrEntry = RPTE_TNL_USTR_NBR (pCtTnlInfo);

        /*Block the Path message expiry, if the tunnel is in recovery state */
        if ((pCtTnlInfo->pTeTnlInfo->u1TnlSyncStatus == TNL_GR_NOT_SYNCHRONIZED)
            && ((pNbrEntry != NULL)
                &&
                ((pNbrEntry->u1GrProgressState == RPTE_GR_RESTART_IN_PROGRESS)
                 || (pNbrEntry->u1GrProgressState ==
                     RPTE_GR_RECOVERY_IN_PROGRESS))))
        {
            return;
        }

        /* If a refresh timeout occurs, at a node, for a LSP, while expecting a     
         * Path Refresh message from a neighbor and that neighbor is GR Capable,    
         * then action for the refresh failure is taken upon next successful Hello communication.
         * */
        else if ((pNbrEntry != NULL) &&
                 (pNbrEntry->u2RestartTime != RPTE_ZERO) &&
                 (pNbrEntry->bIsHelloSynchronoziedAfterRestart != RPTE_TRUE))
        {
            return;
        }

        RSVPTE_DBG2 (RSVPTE_RH_PRCS,
                     "RESV : WaitTimeOut  CurrTime = %d secs, PsbTimeToDie "
                     "= %d secs\n", u4Time,
                     PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pCtTnlInfo)));
        pRsvpTeTmpTnlInfo = RPTE_FRR_BASE_TNL (pCtTnlInfo);
        if (pRsvpTeTmpTnlInfo != NULL)
        {
            /* Get the previous tunnel information */
            do
            {
                pRsvpTePrevTnlInfo = pRsvpTeTmpTnlInfo;
                if ((RSVPTE_TNL_TNLINDX (pCtTnlInfo) ==
                     RSVPTE_TNL_TNLINDX (pRsvpTeTmpTnlInfo)) &&
                    (RSVPTE_TNL_TNLINST (pCtTnlInfo) ==
                     RSVPTE_TNL_TNLINST (pRsvpTeTmpTnlInfo)) &&
                    (MEMCMP
                     (&(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pCtTnlInfo))),
                      &(RSVPTE_TNL_INGRESS_RTR_ID
                        (RPTE_TE_TNL (pRsvpTeTmpTnlInfo))),
                      IPV4_ADDR_LENGTH) == MPLS_ZERO)
                    &&
                    (MEMCMP
                     (&(RSVPTE_TNL_EGRESS_RTR_ID (RPTE_TE_TNL (pCtTnlInfo))),
                      &(RSVPTE_TNL_EGRESS_RTR_ID
                        (RPTE_TE_TNL (pRsvpTeTmpTnlInfo))),
                      IPV4_ADDR_LENGTH) == MPLS_ZERO))
                {
                    break;
                }
            }
            while ((pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL_INFO
                    (pRsvpTeTmpTnlInfo)) != NULL);
            if (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo) != NULL)
            {
                RPTE_FRR_IN_TNL_INFO (pRsvpTePrevTnlInfo) =
                    RPTE_FRR_IN_TNL_INFO (pCtTnlInfo);
            }
            else
            {
                RPTE_FRR_IN_TNL_INFO (pRsvpTePrevTnlInfo) = NULL;
            }
        }

        if ((RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL) &&
            (RPTE_IS_UPLOST (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL
                                                  (pCtTnlInfo)))))
        {
            RptePhGeneratePathTear (RPTE_FRR_BASE_TNL (pCtTnlInfo));
            RPTE_TNL_DEL_TNL_FLAG (RPTE_FRR_BASE_TNL (pCtTnlInfo)) = RPTE_TRUE;
            if (RPTE_TE_TNL (RPTE_FRR_BASE_TNL (pCtTnlInfo))->u1TnlRelStatus ==
                TE_SIGMOD_TNLREL_AWAITED)
            {
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                     RPTE_FRR_BASE_TNL (pCtTnlInfo),
                                     TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
            }
            else
            {
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                     RPTE_FRR_BASE_TNL (pCtTnlInfo),
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
            }
        }

        RPTE_TNL_DEL_TNL_FLAG (pCtTnlInfo) = RPTE_TRUE;
        if (RPTE_TE_TNL (pCtTnlInfo)->u1TnlRelStatus ==
            TE_SIGMOD_TNLREL_AWAITED)
        {
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtTnlInfo,
                                 TE_TNL_CALL_FROM_ADMIN, RPTE_TRUE);
        }
        else
        {
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
        }
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RptePhRhProcessWaitTimeOut : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhStartResvRefreshTmr                              */
/* Description     : This function starts the Refresh Timer for the Rsb     */
/* Input (s)       : *pRsb -  Pointer to the Rsb                            */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhStartResvRefreshTmr (tRsb * pRsb)
{
    tTimerParam        *pTmrParam = NULL;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhStartResvRefreshTmr : ENTRY\n");

    pTmrParam = (tTimerParam *) & RSB_TIMER_PARAM (pRsb);

    TIMER_PARAM_ID (pTmrParam) = RPTE_RESV_REFRESH;
    TIMER_PARAM_RSB (pTmrParam) = (tRsb *) pRsb;
    RSVP_TIMER_NAME (&RSB_TIMER (pRsb)) = (FS_ULONG) pTmrParam;

    if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST, &RSB_TIMER (pRsb),
                           (UINT4) (RSB_REFRESH_INTERVAL (pRsb) *
                                    SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) !=
        TMR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: Sterting of ResvRefreshTimer failed ..\n");
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhStartResvRefreshTmr : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhStartResvRefreshTmr                              */
/* Description     : This function starts the Refresh Timer for the Rsb     */
/* Input (s)       : *pRsb -  Pointer to the Rsb                            */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteStartCspfIntervalTimer (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTimerParam        *pTmrParam = NULL;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteStartCspfIntervalTimer : ENTRY\n");

    pTmrParam = (tTimerParam *) & RPTE_CSPF_TIMER_PARAM (pRsvpTeTnlInfo);

    TIMER_PARAM_ID (pTmrParam) = RPTE_CSPF_INTERVAL;
    TIMER_PARAM_RPTE_TNL (pTmrParam) = pRsvpTeTnlInfo;
    RSVP_TIMER_NAME (&RPTE_CSPF_TIMER (pRsvpTeTnlInfo)) = (FS_ULONG) pTmrParam;
    if (RpteUtlStartTimer
        (RSVP_GBL_TIMER_LIST, &RPTE_CSPF_TIMER (pRsvpTeTnlInfo),
         (UINT4) (((RSVPTE_FRR_CSPF_RETRY_INTERVAL (gpRsvpTeGblInfo))
                   / DEFAULT_CONV_TO_SECONDS) *
                  SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: Starting of RpteStartCspfIntervalTimer failed ..\n");
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteStartCspfIntervalTimer : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhPhStartMaxWaitTmr                                */
/* Description     : This function starts the Timer for the Maximum waiting */
/*                   Time Specified by Psb or Rsb                           */
/* Input (s)       : *pCtTnlInfo - Pointer to the RsvpTeTnlInfo             */
/*                 : u4MaxWaitTime - The Maximum time for receipt of mesg.  */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhPhStartMaxWaitTmr (tRsvpTeTnlInfo * pCtTnlInfo, UINT4 u4MaxWaitTime)
{
    tTimerParam        *pTmrParam = NULL;
    UINT4               u4Time;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhPhStartMaxWaitTmr : ENTRY\n");

    OsixGetSysTime ((tOsixSysTime *) & (u4Time));
    u4MaxWaitTime -= u4Time;
    pTmrParam = (tTimerParam *) & pCtTnlInfo->RROTmrParam;
    /* If the timer is present stop the timer */
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_TNL_RRO_TIMER (pCtTnlInfo));
    TIMER_PARAM_ID (pTmrParam) = RPTE_MAX_WAIT_TIMER;
    TIMER_PARAM_RPTE_TNL (pTmrParam) = pCtTnlInfo;
    RSVP_TIMER_NAME (&RPTE_TNL_RRO_TIMER (pCtTnlInfo)) = (FS_ULONG) pTmrParam;
    if (RpteUtlStartTimer
        (RSVP_GBL_TIMER_LIST, &RPTE_TNL_RRO_TIMER (pCtTnlInfo),
         u4MaxWaitTime) != TMR_SUCCESS)
    {
        /* Start Timer failed. */
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: Starting of MaxWaitTimer failed ..\n");
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhPhStartMaxWaitTmr : INTMD-EXIT\n");
        return;
    }
    RSVPTE_DBG2 (RSVPTE_RH_PRCS,
                 "RESV: MaxWaitTimer Started for %d secs and expires at "
                 "%d secs..\n", u4MaxWaitTime, u4MaxWaitTime + u4Time);
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhPhStartMaxWaitTmr : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhDelTcParams                                      */
/* Description     : This function interfaces with TC to delete a           */
/*                   FlowSpec and a Filterspec                              */
/* Input (s)       : pRsvpTeTnlInfo - Pointer to RsvpTeTnlInfo              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhDelTcParams (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "rpteRhDelTcFlowSpec : ENTRY\n");

    if (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL)
    {
        RpteTcDissociateFilterInfo
            (RSB_RHANDLE (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)), pRsvpTeTnlInfo);

        RpteTcFreeResources (RSB_RHANDLE (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)));

        RSVPTE_DBG (RSVPTE_RH_ETEXT, " rpteRhDelTcFlowSpec : EXIT\n");
    }
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhPreemptTunnel                                    */
/* Description     : This function preempts the list of tunnels.            */
/* Input (s)       : pPreEmpTnlList - Pointer to the list of tunnels        */
/*                    to be preempted.                                      */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
UINT1
RpteRhPreemptTunnel (tTMO_SLL * pPreEmpTnlList)
{
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pHeadStackTnlInfo = NULL;
    tTMO_SLL_NODE      *pPreemptNode = NULL;
    UINT1               u1TmpVar = RPTE_ZERO;
    tErrorSpec          ErrorSpec;
    UINT1               u1ResvRsrc = RPTE_FALSE;

    MEMSET (&ErrorSpec, RPTE_ZERO, sizeof (tErrorSpec));
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhPreemptTunnel : ENTRY\n");
    pPreemptNode = TMO_SLL_Get (pPreEmpTnlList);
    while (pPreemptNode != NULL)
    {
        pCtTnlInfo = ((tRsvpTeTnlInfo *) (VOID *) ((UINT1 *) pPreemptNode -
                                                   RPTE_OFFSET (tRsvpTeTnlInfo,
                                                                NextPreemptTnl)));
        if (pCtTnlInfo != NULL)
        {
            switch (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo))
            {
                case RPTE_INGRESS:
                    /* Simple Policy decision */
                    if (TMO_SLL_Count (&pCtTnlInfo->StackTnlList) ==
                        RSVPTE_ZERO)
                    {
                        pHeadStackTnlInfo =
                            (tRsvpTeTnlInfo *) (VOID *) pCtTnlInfo->
                            pStackTnlHead;
                        if (pHeadStackTnlInfo != NULL)
                        {
                            TMO_SLL_Delete (&pHeadStackTnlInfo->StackTnlList,
                                            &pCtTnlInfo->NextStackTnl);
                        }
                        RptePmRemTnlFromPrioList (RSB_OUT_IF_ENTRY
                                                  (RSVPTE_TNL_RSB
                                                   (pCtTnlInfo)),
                                                  RSVPTE_TNL_HOLD_PRIO
                                                  (RPTE_TE_TNL
                                                   (pCtTnlInfo)),
                                                  GMPLS_SEGMENT_FORWARD,
                                                  pCtTnlInfo);
                        RptePhGeneratePathTear (pCtTnlInfo);
                        RPTE_TNL_DEL_TNL_FLAG (pCtTnlInfo) = RPTE_TRUE;
                        if (RPTE_TE_TNL (pCtTnlInfo)->u1TnlRelStatus ==
                            TE_SIGMOD_TNLREL_AWAITED)
                        {
                            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtTnlInfo,
                                                 TE_TNL_CALL_FROM_ADMIN,
                                                 RPTE_TRUE);
                        }
                        else
                        {
                            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtTnlInfo,
                                                 TE_TNL_CALL_FROM_SIG,
                                                 RPTE_TRUE);
                        }
                        break;
                    }
                    else
                    {
                        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                                    "RpteRhPreemptTunnel : INTMD-EXIT\n");
                        return RPTE_FAILURE;
                    }

                default:
                    RptePmRemoveTnlFromPrioList (pCtTnlInfo);

                    /*Release the resource of preempted tunnel */

                    RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                                &u1TmpVar, &ErrorSpec);

                    RpteConstructPktMapAndSendPathErr (pCtTnlInfo,
                                                       POLICY_CONTROL_FAILURE,
                                                       DEFAULT_ERROR_VALUE);
                    if (RSVPTE_TNL_RSB (pCtTnlInfo) != NULL)
                    {
                        RpteConstructPktMapAndSendResvErr (pCtTnlInfo);
                    }
                    break;
            }
        }
        pPreemptNode = (tTMO_SLL_NODE *) TMO_SLL_Get (pPreEmpTnlList);
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhPreemptTunnel : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteRhBookTcParams                                     */
/* Description     : This function Adds new FlowSpec and FilterSpecs in TC  */
/* Input (s)       : pRsvpTeSession - Pointer to RsvpTeSession              */
/*                 : ppErrorSpec - Pointer to ErrorSpec                     */
/*                 : u1ResPoolType - Holds ResPoolType Value                */
/* Output (s)      : None                                                   */
/* Returns         : If successful, returns RPTE_SUCCESS. Else returns      */
/*                   RPTE_FAILURE.                                          */
/****************************************************************************/
INT1
RpteRhBookTcParams (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                    UINT1 *pu1ErrorCode, UINT1 u1ResPoolType)
{
    tFlowSpec           FlowSpec;

    MEMSET (&FlowSpec, RSVPTE_ZERO, sizeof (tFlowSpec));

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "rpteRhSetTcParams : ENTRY\n");
    if ((RpteTcCalculateFlowSpec
         (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
          u1ResPoolType,
          &PSB_ADSPEC (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
          &pRsvpTeTnlInfo->pPsb->SenderTspecObj.SenderTspec,
          &pRsvpTeTnlInfo->pRsb->FlowSpec,
          &FlowSpec, pu1ErrorCode)) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Calculate FlowSpec Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteRhBookTcParams : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    if (RpteTcResvResources
        (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
         &FlowSpec,
         u1ResPoolType,
         &RSB_RHANDLE (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)),
         pu1ErrorCode) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Reserving of Resources Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteRhBookTcParams : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (RpteTcAssociateFilterInfo
        (RSB_RHANDLE (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)),
         pRsvpTeTnlInfo, &FlowSpec, pu1ErrorCode) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Association of Filter Info Failed.\n");
        RpteTcFreeResources (RSB_RHANDLE (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)));
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteRhBookTcParams : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhBookTcParams : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteRhRroProc                                          */
/* Description     : This function process the RRO Subobjects present in the*/
/*                   PktMap. If all the Subobjects are valid then it updates*/
/*                   the info in the RsvpTeTnlInfo.                         */
/* Input (s)       : pPktMap    - Pointer to PktMap                         */
/*                   pCtTnlInfo        - Pointer to RsvpTeTnlInfo.          */
/* Output (s)      : ResvRefreshNeeded set to either RPTE_REFRESH_MSG_YES   */
/*                   or RPTE_REFRESH_MSG_NO                                 */
/* Returns         : If successful, returns RPTE_SUCCESS else returns       */
/*                   RPTE_FAILURE                                           */
/****************************************************************************/

UINT1
RpteRhRroProc (tPktMap * pPktMap,
               tRsvpTeTnlInfo * pCtTnlInfo, UINT1 *u1ResvRefreshNeeded)
{
    tRsvpTeArHopListInfo *pTeArHopListInfo = NULL;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhRroProc : ENTRY\n");
    /* check whether RRO is present in the received mesg */
    if (TMO_SLL_Count (&pPktMap->RrObj.RrHopList) != RSVPTE_ZERO)
    {
        if (RpteIsLoopFound (pPktMap) != RPTE_FALSE)
        {
            RSVPTE_DBG (RSVPTE_RH_PRCS,
                        "RESV : Forwarding Loop Found in RRO or Illegal "
                        "Address present - Mesg Dropped \n");
            RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhRroProc : INTMD-EXIT\n");
            return RPTE_FAILURE;
        }
        /* 
         * Compare the RRO list of the PktMap with the RRO list present
         * in the RsvpTeTnlInfo structure   
         */
        if (RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) != NULL)
        {
            if (RpteUtlCompareRRList (pPktMap->RrObj.RrHopList,
                                      RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo))
                != RPTE_EQUAL)
            {
                if (rpteTeCreateArHopListInfo (&pTeArHopListInfo,
                                               &pPktMap->RrObj.RrHopList)
                    == RPTE_TE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_RH_PRCS,
                                "RESV : The new RRO List cannot be added - "
                                "Mesg Dropped \n");
                    RSVPTE_DBG (RSVPTE_RH_ETEXT,
                                "RpteRhRroProc : INTMD-EXIT\n");
                    return RPTE_FAILURE;

                }
                rpteTeDeleteArHopListInfo (RSVPTE_OUT_ARHOP_LIST_INFO
                                           (pCtTnlInfo));
                RSVPTE_DBG3 (RSVPTE_RH_ETEXT,
                             "RpteRhRroProc1: updating u4ArHopListIndex: %u for tunnel: %u %u\n",
                             pTeArHopListInfo->u4ArHopListIndex,
                             RPTE_TE_TNL (pCtTnlInfo)->u4TnlIndex,
                             RPTE_TE_TNL (pCtTnlInfo)->u4TnlInstance);

                rpteTeUpdateArHopListInfo (RPTE_TE_TNL (pCtTnlInfo),
                                           pTeArHopListInfo,
                                           RPTE_TE_TNL_DIR_OUT);
                RSVPTE_DBG (RSVPTE_RH_PRCS,
                            "RESV : Incoming RRO varies from the prev RRO - "
                            "new RRO is copied to the TnlInfo\n");

                if (TMO_SLL_Count (&pPktMap->RrObj.NewSubObjList) !=
                    RSVPTE_ZERO)
                {
                    RpteUtlFreeNewSubObjList (&pCtTnlInfo->NewSubObjList);
                    MEMCPY (&pCtTnlInfo->NewSubObjList,
                            &pPktMap->RrObj.NewSubObjList, sizeof (tTMO_SLL));
                    RSVPTE_DBG (RSVPTE_RH_PRCS,
                                "RESV :  NewSubObjList is copied to the "
                                "TnlInfo\n");
                    /* Make Tail end of the Tnl NewSubObjList point to 
                     * its Head */
                    pCtTnlInfo->NewSubObjList.Tail->pNext =
                        &pCtTnlInfo->NewSubObjList.Head;
                    TMO_SLL_Init (&pPktMap->RrObj.NewSubObjList);
                }
                else if (TMO_SLL_Count (&pCtTnlInfo->NewSubObjList)
                         != RSVPTE_ZERO)
                {
                    RpteUtlFreeNewSubObjList (&pCtTnlInfo->NewSubObjList);
                }
                if ((RSVPTE_TNL_NODE_RELATION (pCtTnlInfo)) != RPTE_INGRESS)
                {
                    *u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
                }
            }
        }
        else
        {
            if (rpteTeCreateArHopListInfo (&pTeArHopListInfo,
                                           &pPktMap->RrObj.RrHopList)
                == RPTE_TE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_RH_PRCS,
                            "RESV : The new RRO List cannot be added - "
                            "Mesg Dropped \n");
                RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhRroProc : INTMD-EXIT\n");
                return RPTE_FAILURE;
            }

            RSVPTE_DBG3 (RSVPTE_RH_ETEXT,
                         "RpteRhRroProc2: updating u4ArHopListIndex: %u for tunnel: %u %u\n",
                         pTeArHopListInfo->u4ArHopListIndex,
                         RPTE_TE_TNL (pCtTnlInfo)->u4TnlIndex,
                         RPTE_TE_TNL (pCtTnlInfo)->u4TnlInstance);

            if (rpteTeUpdateArHopListInfo (RPTE_TE_TNL (pCtTnlInfo),
                                           pTeArHopListInfo,
                                           RPTE_TE_TNL_DIR_OUT) ==
                RPTE_TE_FAILURE)
            {
                RSVPTE_DBG1 (RSVPTE_RH_PRCS,
                             "\rRESV : The new RRO List cannot be updated "
                             "for tunnel %d\n",
                             RSVPTE_TNL_INDEX (RPTE_TE_TNL (pCtTnlInfo)));
                return RPTE_FAILURE;
            }
            if (TMO_SLL_Count (&pPktMap->RrObj.NewSubObjList) != RSVPTE_ZERO)
            {
                MEMCPY (&pCtTnlInfo->NewSubObjList,
                        &pPktMap->RrObj.NewSubObjList, sizeof (tTMO_SLL));
                RSVPTE_DBG (RSVPTE_RH_PRCS,
                            "RESV :  NewSubObjList is copied to the TnlInfo\n");
                /* Make Tail end of the Tnl NewSubObjList point to its Head */
                pCtTnlInfo->NewSubObjList.Tail->pNext =
                    &pCtTnlInfo->NewSubObjList.Head;
                TMO_SLL_Init (&pPktMap->RrObj.NewSubObjList);
            }
            if ((RSVPTE_TNL_NODE_RELATION (pCtTnlInfo)) != RPTE_INGRESS)
            {
                *u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
            }
        }
    }
    else if (RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) != NULL)
    {
        rpteTeDeleteArHopListInfo (RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo));
        RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) = NULL;
        RpteUtlFreeNewSubObjList (&pCtTnlInfo->NewSubObjList);
        if ((RSVPTE_TNL_NODE_RELATION (pCtTnlInfo)) != RPTE_INGRESS)
        {
            *u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
        }
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhRroProc : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteRhResvRefresh                                      */
/* Description     : This function refreshes the Reservation state for the  */
/*                   the Previous hop adrress given by pPhopAddr            */
/* Input (s)       : pCtTnlInfo - Pointer to RsvpTeTnlInfo                  */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhResvRefresh (tRsvpTeTnlInfo * pCtTnlInfo)
{
    tRsb               *pRsb = NULL;
    tPsb               *pPsb = NULL;
    tRsvpHop           *pFwdPhop = NULL;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhResvRefresh : ENTRY\n");

    pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
    if (pRsb == NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhResvRefresh : INTMD EXIT\n");
        return;
    }
    /* checking for Psb to get the PrevHop info */
    pPsb = RSVPTE_TNL_PSB (pCtTnlInfo);
    pFwdPhop = &PSB_RSVP_HOP (pPsb);
    RSB_FWD_RSVP_HOP (pRsb) = *pFwdPhop;
    RSB_IN_IF_ENTRY (pRsb) = PSB_IF_ENTRY (pPsb);
    RSB_NON_RSVP_FLAG (pRsb) = PSB_NON_RSVP_FLAG (pPsb);
    RSB_RPTE_TNL_INFO (pRsb) = pCtTnlInfo;
    if (IF_ENTRY_ENABLED (RSB_IN_IF_ENTRY (pRsb)) == RPTE_ENABLED)
    {
        RpteRhSendResv (pRsb);
    }

    /* This function is used to Fill the Facility List
     * information */
    if ((RPTE_TE_TNL (pCtTnlInfo) != NULL) &&
        ((RSVPTE_TNL_ROLE (RPTE_TE_TNL (pCtTnlInfo))) == RPTE_EGRESS) &&
        (RpteFrrFillFacilityListEntry (pCtTnlInfo) == RPTE_FAILURE))

    {
        RSVPTE_DBG (RSVPTE_RH_PRCS, "RpteRhResvRefresh :"
                    "Fail to fill the backup information in frr Facility list\n");
    }

    if ((RPTE_IS_FRR_CAPABLE (pCtTnlInfo) == RPTE_TRUE) &&
        ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pCtTnlInfo)).
          u1Flags) == RSVPTE_TNL_FRR_FACILITY_METHOD) &&
        (!(RPTE_IS_MP (RPTE_FRR_NODE_STATE (pCtTnlInfo)))))
    {
        /* If Facility backup method is desired, the backup tunnel
         * satisfying the necessary constraints to be identified to act as MP. */
        RpteFrrMPBypassTnlIdentification (&pCtTnlInfo);
    }

    /* Restart the timer for ResvRefresh */
    RpteRhStartResvRefreshTmr (RSVPTE_TNL_RSB (pCtTnlInfo));
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhResvRefresh : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhSendResv                                         */
/* Description     : This procedures frames and sends resv message for the  */
/*                   nexthop address found in the ResvFwdCmnHdr.            */
/* Input (s)       : pRsb           - Pointer to Rsb                        */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhSendResv (tRsb * pRsb)
{
    UINT2               u2TmpVar = RSVPTE_ZERO;
    UINT2               u2Offset = RSVPTE_ZERO;
    UINT4               u4TmpLabel = RSVPTE_ZERO;
    UINT4               u4TmpAddr = RSVPTE_ZERO;
    tObjHdr            *pObjHdr = NULL;
    UINT1              *pPduPtr = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tTimeValues        *pTimeValues = NULL;
    tIfEntry           *pIfEntry = NULL;
    tPktMap            *pPktMap = NULL;
    tPktMap             RROPktMap;
    UINT2               u2Retval = RSVPTE_ZERO;
    UINT2               u2ResvSize = RSVPTE_ZERO;
    UINT2               u2RrSizeCt = RSVPTE_ZERO;
    UINT2               u2TmpResvSize = RSVPTE_ZERO;
    UINT2               u2FSpecObjLength = RSVPTE_ZERO;
    UINT1               u1RroNeeded = RPTE_FALSE;
    tResvConf          *pResvConf = NULL;
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;
    tFlowSpec          *pFwdFlowSpec = NULL;
    tNewSubObjInfo     *pNewSubObjInfo = NULL;
    tStyleObj           StyleObj;
    tRsvpTeSsnObj       SsnObj;
    tRsvpTeFilterSpecObj RsvpTeFilterSpecObj;
    tRsvpHopObj         RsvpHopObj;
    tIfIdRsvpHopObj     IfIdRsvpHopObj;
    tIfIdRsvpNumHopObj  IfIdRsvpNumHopObj;
    tFlowSpecObj        FlowSpecObj;
    tIfEntry           *pIfMapEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRpteKey            RpteKey;
    tRsvpTeGetTnlInfo   RsvpTeGetTnlInfo;
    UINT4               u4ResvOutMsgId;
    UINT4               u4Val;
    tuTrieInfo         *pTrieInfo = NULL;
    UINT1               u1MsgIdCreated = RPTE_FALSE;
    UINT1               u1EncodingType = RSVPTE_ZERO;

    if ((pPktMap = MemAllocMemBlk (RSVPTE_PKT_MAP_POOL_ID)) == NULL)
    {
        return;
    }
    if ((pIfMapEntry = MemAllocMemBlk (RSVPTE_IF_ENTRY_POOL_ID)) == NULL)
    {
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        return;
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhSendResv : ENTRY\n");

    pRsvpTeTnlInfo = RSB_RPTE_TNL_INFO (pRsb);
    pNbrEntry = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);

    /*Get the required tunnel information */
    MEMSET (&RsvpTeGetTnlInfo, RSVPTE_ZERO, sizeof (tRsvpTeGetTnlInfo));
    RpteTeGetTnlInfo (&RsvpTeGetTnlInfo, pRsvpTeTnlInfo);
    u1EncodingType = RsvpTeGetTnlInfo.u1EncodingType;

    if (RPTE_IS_UPLOST (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
    {
        RSVPTE_DBG4 (RSVPTE_RH_PRCS,
                     "Upstream lost for the tunnel %d %d %x %x "
                     "- Cannot send Resv\n",
                     RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                     RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                     OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                     OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
        return;
    }

    if (RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) == RPTE_TIMER_TYPE_UNKNOWN)
    {
        if ((NBR_ENTRY_RR_STATE (pNbrEntry) == RPTE_ENABLED) ||
            ((NBR_ENTRY_RMD_CAPABLE (pNbrEntry) == RPTE_ENABLED) &&
             (gu4RMDFlags & RPTE_RMD_RESV_MSG)))
        {
            /* If Nbr RR Capable flag is enabled or Nbr RMD
             * flag is enabled and RMD flag is set for Resv Msg then
             * Msg ID is generated.
             */
            RpteMIHGenerateMsgId (&u4ResvOutMsgId);

            RPTE_TNL_OUT_RESV_MSG_ID (pRsvpTeTnlInfo) = u4ResvOutMsgId;
            RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) |=
                RPTE_OUT_RESV_MSG_ID_PRESENT;
            u1MsgIdCreated = RPTE_TRUE;
        }
    }
    else if ((NBR_ENTRY_RR_STATE (pNbrEntry) == RPTE_DISABLED) &&
             (NBR_ENTRY_RMD_CAPABLE (pNbrEntry) == RPTE_DISABLED) &&
             (RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
              RPTE_OUT_RESV_MSG_ID_PRESENT))
    {
        /* When Nbr Entry is present and it is RMD not capable, msg id
         * associated with that tunnel is released */
        RpteKey.u4_Key = RPTE_TNL_OUT_RESV_MSG_ID (pRsvpTeTnlInfo);
        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_LCL_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
    }
    else if (((NBR_ENTRY_RR_STATE (pNbrEntry) == RPTE_ENABLED) ||
              ((NBR_ENTRY_RMD_CAPABLE (pNbrEntry) == RPTE_ENABLED) &&
               (gu4RMDFlags & RPTE_RMD_RESV_MSG))) &&
             (!(RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
                RPTE_OUT_RESV_MSG_ID_PRESENT)))
    {
        /* When Nbr Entry is present and it is RR and Msg Id capable
         * and if msg id is not associated with that message earlier,
         * then generate a message id and associate it with the tunnel.
         * And treat it as a trigger message */
        RpteMIHGenerateMsgId (&u4ResvOutMsgId);
        RPTE_TNL_OUT_RESV_MSG_ID (pRsvpTeTnlInfo) = u4ResvOutMsgId;
        RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) |= RPTE_OUT_RESV_MSG_ID_PRESENT;
        u1MsgIdCreated = RPTE_TRUE;
    }
    else if ((NBR_ENTRY_RR_STATE (pNbrEntry) == RPTE_ENABLED) &&
             (RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) == RPTE_NORMAL_TIMER))
    {
        if (RpteRRSendSRefreshMsg (pRsvpTeTnlInfo, RESV_MSG) == RPTE_SUCCESS)
        {
            MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
            MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
            return;
        }
    }

    RpteUtlInitPktMap (pPktMap);
    RPTE_PKT_MAP_TNL_INFO (pPktMap) = pRsvpTeTnlInfo;
    RpteUtlInitPktMap (&RROPktMap);
    RPTE_PKT_MAP_TNL_INFO (&RROPktMap) = pRsvpTeTnlInfo;

    MEMSET (&SsnObj, RSVPTE_ZERO, sizeof (tRsvpTeSsnObj));
    MEMSET (&StyleObj, RSVPTE_ZERO, sizeof (tStyleObj));
    MEMSET (&RsvpTeFilterSpecObj, RSVPTE_ZERO, sizeof (tRsvpTeFilterSpecObj));
    MEMSET (&FlowSpecObj, RSVPTE_ZERO, sizeof (tFlowSpecObj));
    MEMSET (&RsvpHopObj, RSVPTE_ZERO, sizeof (tRsvpHopObj));
    MEMSET (&IfIdRsvpHopObj, RSVPTE_ZERO, sizeof (tIfIdRsvpHopObj));
    MEMSET (&IfIdRsvpNumHopObj, RSVPTE_ZERO, sizeof (tIfIdRsvpNumHopObj));
    MEMSET (pIfMapEntry, RSVPTE_ZERO, sizeof (tIfEntry));

    PKT_MAP_RSVP_HOP_OBJ (&RROPktMap) = &RsvpHopObj;
    (&RROPktMap)->pIfIdRsvpHopObj = &IfIdRsvpHopObj;
    (&RROPktMap)->pIfIdRsvpNumHopObj = &IfIdRsvpNumHopObj;
    PKT_MAP_IF_ENTRY (&RROPktMap) = pIfMapEntry;
    PKT_MAP_RPTE_SSN_OBJ (&RROPktMap) = &SsnObj;
    PKT_MAP_RPTE_FILTER_SPEC_OBJ (&RROPktMap) = &RsvpTeFilterSpecObj;
    PKT_MAP_FLOW_SPEC_OBJ (&RROPktMap) = &FlowSpecObj;
    PKT_MAP_STYLE_OBJ (&RROPktMap) = &StyleObj;
    pIfEntry = (tIfEntry *) RSB_IN_IF_ENTRY (pRsb);

    /* if the RSVPTE is disabled in the out going interface don't send 
     * RESV Msg
     */
    if ((IF_ENTRY_STATUS (pIfEntry) != ACTIVE) ||
        (IF_ENTRY_ENABLED (pIfEntry) == RPTE_DISABLED))
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV : Outgoing Interface is not Active or the RPTE \r\n"
                    "is disabled in this interface \r\n");
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhSendResv : INTMD-EXIT \n");
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
        return;
    }

    /* Calulate the ResvMesg size */
    u2ResvSize = sizeof (tRsvpHdr) + RPTE_IPV4_SSN_OBJ_LEN +
        sizeof (tTimeValuesObj) +
        sizeof (tStyleObj) + RPTE_IPV4_FLTR_SPEC_OBJ_LEN +
        sizeof (tGenLblObj) + sizeof (tFlowSpecObj);

    u2Retval
        = RpteUtlCalculateRSVPHopSize (pRsvpTeTnlInfo->b1UpStrOob,
                                       pRsvpTeTnlInfo->u4UpStrDataTeLinkIfId,
                                       pRsvpTeTnlInfo->pPsb->pIncIfEntry->
                                       u4Addr);
    u2ResvSize = (UINT2) (u2ResvSize + u2Retval);

    if ((pRsvpTeTnlInfo->pRsb->u4AdminStatus != GMPLS_ADMIN_UNKNOWN) &&
        (gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable == RPTE_ENABLED))
    {
        u2ResvSize = (UINT2) (u2ResvSize + sizeof (tAdminStatusObj));
    }

    if (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4SendResvNotifyRecipient !=
        RPTE_ZERO)
    {
        u2ResvSize = (UINT2) (u2ResvSize + sizeof (tNotifyRequestObj));
    }
    if (pRsvpTeTnlInfo->pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP)
    {
        if ((gi4MplsSimulateFailure == RPTE_SIM_FAILURE_LSP_TNL_IFID_CTYPE_1) ||
            (pRsvpTeTnlInfo->u2HlspCType ==
             RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE_1))
        {
            u2ResvSize =
                (UINT2) (u2ResvSize + sizeof (tLspTnlIfIdObj) - sizeof (UINT4));
        }
        else
        {
            u2ResvSize = (UINT2) (u2ResvSize + sizeof (tLspTnlIfIdObj));
        }

    }
    /* If ResvConf object is present in ResvFwdCmnhdr add its size */
    pResvConf = &RSB_RESV_CONF (pRsb);
    if ((pResvConf->u4RecevAddr) != RSVPTE_ZERO)
    {
        u2ResvSize = (UINT2) (u2ResvSize + sizeof (tResvConfObj));
    }

    pFwdFlowSpec = &RSB_FLOW_SPEC (pRsb);
    pCtTnlInfo = RSB_RPTE_TNL_INFO (pRsb);

    u2FSpecObjLength = sizeof (tFlowSpecObj) - sizeof (tGsRSpec);
    if (((tFlowSpec *) pFwdFlowSpec)->SrvHdr.u1SrvID == FLOWSPEC_CLS_HDR_NUMBER)
    {
        u2ResvSize = (UINT2) (u2ResvSize - sizeof (tGsRSpec));
    }

    if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
         RPTE_OUT_RESV_MSG_ID_PRESENT) == RPTE_OUT_RESV_MSG_ID_PRESENT)
    {
        if (u1MsgIdCreated == RPTE_TRUE)
        {
            RpteKey.u4_Key = u4ResvOutMsgId;

            pTrieInfo = (tuTrieInfo *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TRIE_INFO_POOL_ID);

            if (pTrieInfo == NULL)
            {
                /* Since the memory allocation is a failure,
                 * the msg id received in the path msg is not
                 * stored in the neighbour msg id data base. */
                RSVPTE_DBG (RSVPTE_PH_MEM,
                            "Failed to allocate Trie Info "
                            "Not generating msg id for resv msg.\n");
                RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &=
                    RPTE_OUT_RESV_MSG_ID_NOT_PRESENT;
            }
            else
            {
                MEMSET (pTrieInfo, RSVPTE_ZERO, sizeof (tuTrieInfo));
                RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo) = RPTE_RESV_REFRESH;
                RPTE_TRIE_INFO_TNL_INFO (pTrieInfo) = pRsvpTeTnlInfo;

                /* RBTree Addtion is only required for out path message id for
                 * SRefresh RecoveryPath message support.
                 * Its not required for out resv message id
                 * */

                if (RpteTrieAddEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                      pTrieInfo) != RPTE_SUCCESS)
                {
                    MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                        (UINT1 *) pTrieInfo);
                    RPTE_TRIE_INFO_TNL_INFO (pTrieInfo) = NULL;
                    RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &=
                        RPTE_OUT_RESV_MSG_ID_NOT_PRESENT;
                }
                else
                {
                    RPTE_TNL_OUT_RESV_MSG_ID (pRsvpTeTnlInfo) = u4ResvOutMsgId;
                    RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) |=
                        RPTE_OUT_RESV_MSG_ID_PRESENT;
                }
            }
        }

        if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) &
             RPTE_OUT_RESV_MSG_ID_PRESENT) == RPTE_OUT_RESV_MSG_ID_PRESENT)
        {
            /* Msg Id is generated for the outgoing path message */
            u2ResvSize = (UINT2) (u2ResvSize + RPTE_MSG_ID_OBJ_LEN);

            RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;

            if ((gu4RMDFlags & RPTE_RMD_RESV_MSG) &&
                (RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) != RPTE_NORMAL_TIMER))
            {
                /* If the global RMD flag is set for Resv Message Ack Desired
                 * bit is set */
                RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_MSG_ID_ACK_DESIRED;

                /* Since the message is a trigger message and the ack
                 * desired flag is set, BackOff Timer flag is set in
                 * Rsb structure. */
                if (RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) ==
                    RPTE_TIMER_TYPE_UNKNOWN)
                {
                    RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) =
                        RPTE_EXP_BACK_OFF_TIMER;
                    RPTE_TNL_RSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                        (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                         RPTE_BACK_OFF_FACTOR) / DEFAULT_CONV_TO_SECONDS;
                }
                else
                {
                    /* If the back off timer value has increased
                     * a certain threshold, the timer is converted
                     * to just a refresh timer and the refresh
                     * goes as a srefresh message */
                    if (RpteRRCalculateBackOffDelay
                        (pIfEntry, RPTE_TNL_RSB_REFRESH_INTERVAL
                         (pRsvpTeTnlInfo),
                         &(RPTE_TNL_RSB_REFRESH_INTERVAL (pRsvpTeTnlInfo)))
                        != RPTE_SUCCESS)
                    {
                        /* Setting the timer type to normal */
                        RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) =
                            RPTE_NORMAL_TIMER;
                        RPTE_TNL_RSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                            RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL
                                             (pIfEntry) /
                                             DEFAULT_CONV_TO_SECONDS);
                        RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_ZERO;
                    }
                }
            }
            else
            {
                RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap) = RPTE_ZERO;
                RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) = RPTE_NORMAL_TIMER;
                RPTE_TNL_RSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                    RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                                     DEFAULT_CONV_TO_SECONDS);
            }
            RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap) = gu4Epoch;
            RPTE_PKT_MAP_MSG_ID (pPktMap) =
                RPTE_TNL_OUT_RESV_MSG_ID (pRsvpTeTnlInfo);
        }
        else
        {
            RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) = RPTE_NORMAL_TIMER;
            RPTE_TNL_RSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                                 DEFAULT_CONV_TO_SECONDS);
        }
    }
    else
    {
        if (RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) != RPTE_NORMAL_TIMER)
        {
            RPTE_TNL_RSB_TIMER_TYPE (pRsvpTeTnlInfo) = RPTE_NORMAL_TIMER;
            RPTE_TNL_RSB_REFRESH_INTERVAL (pRsvpTeTnlInfo) =
                RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                                 DEFAULT_CONV_TO_SECONDS);
        }
    }

    RpteUpdateDSResvObjsSize (pCtTnlInfo, &u2ResvSize);

    /* 
     * calculate the size of RRO if present in the TnlInfo, if the size of
     * RRO exceeds maximum MTU then proceed with processing of forwarding
     * ResvMessage, at the same time send ResvErr periodically to the 
     * NextHop to get it removed from the subsequent ResvMesg 
     */
    u2TmpResvSize = u2ResvSize;
    if (((RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) != NULL) &&
         ((TMO_SLL_Count (&RSVPTE_TNL_OUTARHOP_LIST
                          (pCtTnlInfo)) != RSVPTE_ZERO) &&
          (!(pCtTnlInfo->u1RROErrFlag & RPTE_RRO_ERR_RESV_SET)))) ||
        ((RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_EGRESS) &&
         (RSVPTE_IN_ARHOP_LIST_INFO (pCtTnlInfo) != NULL)))

    {
        /* RRO object size calculation */
        u2Retval = RpteUtlCalculateRROSize (pRsvpTeTnlInfo, RESV_MSG);
        u2ResvSize = (UINT2) (u2ResvSize + u2Retval);
        u2ResvSize = (UINT2) (u2ResvSize + sizeof (tObjHdr));
        u1RroNeeded = RPTE_TRUE;
    }
    if (u1RroNeeded == RPTE_TRUE)
    {
        if (TMO_SLL_Count (&pCtTnlInfo->NewSubObjList) != RSVPTE_ZERO)
        {
            TMO_SLL_Scan (&pCtTnlInfo->NewSubObjList,
                          pNewSubObjInfo, tNewSubObjInfo *)
            {
                u2ResvSize = (UINT2) (u2ResvSize + pNewSubObjInfo->u1SubObjLen);
            }
        }

        u2RrSizeCt = (UINT2) (u2ResvSize - u2TmpResvSize);

        if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM)
        {
            u2Offset = sizeof (tRsvpIpHdr) + SHIM_HDR_LEN;
        }
        else if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ETHERNET)
        {
            u2Offset = sizeof (tRsvpIpHdr) + MAC_HEADER_LENGTH + SHIM_HDR_LEN;
        }

        u2Offset = (UINT2) (u2Offset + sizeof (tUdpHdr));
        /* Check for MTU size if doesn't fit send err message */
        if ((UINT2) (u2ResvSize + u2Offset) > pIfEntry->RpteIfInfo.u4MaxMTUSize)
        {
            RSVPTE_DBG (RSVPTE_RH_PRCS,
                        "RESV: ResvMesgSize exceeded the MTU size ..\n");
            /* Set RRO Err Flag */
            pCtTnlInfo->u1RROErrFlag |= RPTE_RRO_ERR_RESV_SET;
        }
    }
    if ((pCtTnlInfo->u1RROErrFlag & RPTE_RRO_ERR_RESV_SET) ==
        RPTE_RRO_ERR_RESV_SET)
    {
        RSVPTE_DBG2 (RSVPTE_RH_PRCS,
                     "RESV: ResvMesgSize %ld restored to MesgSize "
                     "without RRO %ld\n", u2ResvSize, u2TmpResvSize);
        u2ResvSize = u2TmpResvSize;
        u1RroNeeded = RPTE_FALSE;

        if (pCtTnlInfo->u2BTime == RSVPTE_ZERO)
        {
            RpteUtlRhPreparePktMapRROErr (&RROPktMap, pCtTnlInfo);
            RptePvmSendResvErr (&RROPktMap, RPTE_NOTIFY, RPTE_RRO_TOO_LARGE);
            RpteRhStartRROBackOffTimer (pCtTnlInfo);
        }
    }
    /* Allocate memory for the RSVP packet */
    PKT_MAP_RSVP_PKT (pPktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);
    if (PKT_MAP_RSVP_PKT (pPktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: Mem Alloc failed in RpteRhSendResv..\n");
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhSendResv : INTMD-EXIT\n");
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
        return;
    }
    MEMSET (PKT_MAP_RSVP_PKT (pPktMap), RSVPTE_ZERO, u2ResvSize);
    PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_RSVP_ALLOC;
    /* Copy Source address i.e this node's address */
    if (IF_ENTRY_ADDR (pIfEntry) != RPTE_ZERO)
    {
        PKT_MAP_SRC_ADDR (pPktMap) = IF_ENTRY_ADDR (pIfEntry);
    }
    else
    {
        CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                            u4TmpAddr);
        u4TmpAddr = OSIX_NTOHL (u4TmpAddr);

        PKT_MAP_SRC_ADDR (pPktMap) = u4TmpAddr;
    }
    /* Copy Destination address i.e Prev node's address */
    PKT_MAP_DST_ADDR (pPktMap) = RSVP_HOP_ADDR (&RSB_FWD_RSVP_HOP (pRsb));
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    MEMSET (pRsvpHdr, RSVPTE_ZERO, sizeof (tRsvpHdr));

    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = RESV_MSG;
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (u2ResvSize);

    pObjHdr =
        (tObjHdr *) (VOID *) (PKT_MAP_RSVP_PKT (pPktMap) + sizeof (tRsvpHdr));

    /* Msg Id Obj update, if present */
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        /* Filling the Header */
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_MSG_ID_OBJ_LEN);
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_MESSAGE_ID_CLASS_NUM_TYPE);

        pPduPtr = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

        /* Filling the Epoch field */
        u4Val = OSIX_HTONL (RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap));
        MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) &u4Val, RPTE_FLAG_EPOCH_LEN);

        /* Filling the flags field */
        MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) (&(RPTE_PKT_MAP_MSG_ID_FLAGS
                                                (pPktMap))),
                RPTE_MSG_ID_FLAG_LEN);
        pPduPtr += RPTE_FLAG_EPOCH_LEN;

        /* Filling the Message Identifier field */
        u4Val = OSIX_HTONL (RPTE_PKT_MAP_MSG_ID (pPktMap));
        MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) &u4Val, RPTE_MSG_ID_LEN);
        pPduPtr += RPTE_MSG_ID_LEN;

        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* copy RsvpTeSessionObj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);
    pPduPtr = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    u4TmpAddr = RSVPTE_TNL_EGRESS (pCtTnlInfo);
    MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPduPtr += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPduPtr, RSVPTE_ZERO);    /* reserved field */
    RPTE_PUT_2_BYTES (pPduPtr,
                      (UINT2) OSIX_HTONS (RSVPTE_TNL_TNLINDX (pCtTnlInfo)));

    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pCtTnlInfo)), u4TmpAddr);
    MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPduPtr += RSVPTE_IPV4ADR_LEN;

    pObjHdr = (tObjHdr *) (VOID *) pPduPtr;

    /* copy RsvpHopObj */
    RptePvmFillRsvpHopObj (pRsvpTeTnlInfo, &pObjHdr, RESV_MSG);
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* Fill TimeValues Obj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tTimeValuesObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_TIME_VALUES_CLASS_NUM_TYPE);
    pTimeValues = &TIME_VALUES_OBJ ((tTimeValuesObj *) (VOID *) pObjHdr);
    TIME_VALUES_PERIOD (pTimeValues) = OSIX_HTONL (IF_ENTRY_REFRESH_INTERVAL
                                                   ((tIfEntry *)
                                                    RSB_IN_IF_ENTRY (pRsb)));
    pObjHdr = NEXT_OBJ (pObjHdr);

    if ((pResvConf->u4RecevAddr) != RSVPTE_ZERO)
    {
        /* Fill ResvConf Obj, If Any */
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tResvConfObj));
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (IPV4_RESV_CONFIRM_CLASS_NUM_TYPE);
        RESV_CONF_OBJ ((tResvConfObj *) (VOID *) pObjHdr) =
            RSB_RESV_CONF (pRsb);
        pObjHdr = NEXT_OBJ (pObjHdr);
    }
    /* Fill  Style Obj * */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tStyleObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS (IPV4_STYLE_CLASS_NUM_TYPE);
    STYLE_OBJ ((tStyleObj *) pObjHdr) = RSB_STYLE (pRsb);
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* Copy FlowSpec Obj */

    if (((tFlowSpec *) pFwdFlowSpec)->SrvHdr.u1SrvID == FLOWSPEC_CLS_HDR_NUMBER)
    {
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (u2FSpecObjLength);
    }
    else
    {
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tFlowSpecObj));
    }

    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_FLOW_SPEC_CLASS_NUM_TYPE);
    FLOW_SPEC_OBJ ((tFlowSpecObj *) (VOID *) pObjHdr) = *pFwdFlowSpec;
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* copy RsvpTeFilterSpecObj */

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_FLTR_SPEC_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS
        (IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE);
    RPTE_FILTER_SPEC_OBJ ((tRsvpTeFilterSpecObj *) pObjHdr) =
        pCtTnlInfo->pRsb->RsvpTeFilterSpec;
    pObjHdr = NEXT_OBJ (pObjHdr);

    RptePvmFillLSPTnlIfIdObj (&pObjHdr, pRsvpTeTnlInfo, RESV_MSG);

    /* Label Obj updation */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tGenLblObj));
    if (u1EncodingType != RSVPTE_ZERO)
    {
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)
            = OSIX_HTONS (RPTE_GEN_LBL_CLASS_NUM_TYPE);
    }
    else
    {
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS (RPTE_LBL_CLASS_NUM_TYPE);
    }
    pPduPtr = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));

    if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM)
    {
        u2TmpVar = OSIX_HTONS (pCtTnlInfo->DownstrInLbl.AtmLbl.u2Vpi);
        MEMCPY (pPduPtr, &u2TmpVar, sizeof (UINT2));
        pPduPtr += sizeof (UINT2);
        u2TmpVar = OSIX_HTONS (pCtTnlInfo->DownstrInLbl.AtmLbl.u2Vci);
        MEMCPY (pPduPtr, &u2TmpVar, sizeof (UINT2));
        pPduPtr += sizeof (UINT2);
    }
    else if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ETHERNET)
    {
        u4TmpLabel = pCtTnlInfo->DownstrInLbl.u4GenLbl;
        u4TmpLabel = OSIX_HTONL (u4TmpLabel);
        MEMCPY (pPduPtr, &u4TmpLabel, sizeof (UINT4));
        pPduPtr += sizeof (UINT4);
    }

    pObjHdr = (tObjHdr *) (VOID *) pPduPtr;
    /* RRO Obj IPv4 Type updation */
    if (u1RroNeeded == RPTE_TRUE)
    {
        RpteRhUpdateRroPktMap (pCtTnlInfo, pObjHdr, u2RrSizeCt);
        pObjHdr = NEXT_OBJ (pObjHdr);
    }

    /* Adminstatus object updation */
    RptePvmFillAdminStatusObj (pCtTnlInfo, &pObjHdr, RESV_MSG);

    RptePvmFillNotifyReqObj (&pObjHdr, pRsvpTeTnlInfo, RESV_MSG);

    /* copy ElspTpObj */
    RpteUpdateDSElspTpObj (pCtTnlInfo, &pObjHdr);

    PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;
    PKT_MAP_OUT_IF_ENTRY (pPktMap) = RSB_IN_IF_ENTRY (pRsb);
    PKT_MAP_TX_TTL (pPktMap) = IF_ENTRY_TTL ((tIfEntry *) RSB_IN_IF_ENTRY
                                             (pRsb));
    RSVP_HDR_SEND_TTL (pRsvpHdr) = IF_ENTRY_TTL ((tIfEntry *)
                                                 RSB_IN_IF_ENTRY (pRsb));
    RptePbSendMsg (pPktMap);

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhSendResv : EXIT\n");
    MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
    MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID, (UINT1 *) pIfMapEntry);
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhUpdateRroPktMap                                  */
/* Description     : This function updates the RRO Subobjects to the Packet */
/*                   map                                                    */
/* Input (s)       : pCtTnlInfo - Pointer to the RsvpTeTnlInfo              */
/*                   pObjHdr    - Pointer to ObjHdr                         */
/*                   u2ObjSize  - Size of the Object                        */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhUpdateRroPktMap (tRsvpTeTnlInfo * pCtTnlInfo,
                       tObjHdr * pObjHdr, UINT2 u2ObjSize)
{
    UINT1              *pPdu = NULL;
    UINT4               u4FwdLbl = MPLS_INVALID_LABEL;
    UINT4               u4RevLbl = MPLS_INVALID_LABEL;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhUpdateRroPktMap : ENTRY\n");

    /* Object Header Updation */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (u2ObjSize);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS (RPTE_RRO_CLASS_NUM_TYPE);

    pPdu = ((UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr)));

    if (RPTE_IF_LBL_TYPE (pCtTnlInfo->pRsb->pIncIfEntry) == RPTE_ATM)
    {
        u4FwdLbl = pCtTnlInfo->DownstrInLbl.AtmLbl.u2Vci;
        u4FwdLbl = u4FwdLbl << 16;
        u4FwdLbl |= pCtTnlInfo->DownstrInLbl.AtmLbl.u2Vpi;
    }
    else
    {
        u4FwdLbl = pCtTnlInfo->DownstrInLbl.u4GenLbl;
        u4RevLbl = pCtTnlInfo->UpStrOutLbl.u4GenLbl;
    }

    RptePvmFillTopRRO (&pPdu, pCtTnlInfo, pCtTnlInfo->pRsb->pIncIfEntry,
                       u4FwdLbl, u4RevLbl);

    /* RRO object updation from Actual Route Hop list */
    if ((RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) != NULL) &&
        (TMO_SLL_Count (&RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo)) != RSVPTE_ZERO))
    {
        RptePvmFillDnStrOrUpStrRRO (&pPdu, pCtTnlInfo,
                                    &RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo));
    }

    pObjHdr = (tObjHdr *) (VOID *) pPdu;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhUpdateRroPktMap : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhUpdateTrafficControl                             */
/* Description     : This function sets the TC parameters to install the    */
/*                   reservation requested by the receivers                 */
/* Input (s)       : pCtTnlInfo - Pointer to the RsvpTeTnlInfo              */
/*                   pRsvpTeSession - Pointer to RsvpTe Session             */
/*                   u1ResvRsrc     - Flag to check wether Resources have   */
/*                                    to be reserved or released.           */
/*                   pu1ResvRefreshNeeded - Pointer to                      */
/*                                          ResvRefreshNeeded Flag          */
/*                 : pErrorSpec - Pointer to the ErrorSpec                  */
/* Output (s)      : None                                                   */
/* Returns         : If sucsessful returns RPTE_SUCCESS.                    */
/*                   Else returns RPTE_FAILURE                              */
/****************************************************************************/
INT1
RpteRhUpdateTrafficControl (tRsvpTeTnlInfo * pCtTnlInfo,
                            UINT1 u1ResvRsrc,
                            UINT1 *pu1ResvRefreshNeeded,
                            tErrorSpec * pErrorSpec)
{
    tFlowSpec          *pTcFlowSpec = NULL;
    tTMO_SLL            PreemptTnlList;
    tSenderTspec       *pPathTe = NULL;
    FLOAT               reqResBw = RSVPTE_ZERO;

    /* Do not release bandwidth when the RSVP-TE component is made
     * shut with GR enabled
     * */
    if (gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_SHUT_DOWN_IN_PROGRESS)
    {
        return RPTE_SUCCESS;
    }
    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pCtTnlInfo->u1IsGrTnl == RPTE_YES) &&
        (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) != RPTE_INGRESS))
    {
        *pu1ResvRefreshNeeded = RPTE_YES;
        return RPTE_SUCCESS;
    }

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhUpdateTrafficControl : ENTRY\n");
    if ((pCtTnlInfo->pRsb == NULL) || (pCtTnlInfo->pPsb == NULL))
    {
        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                    "RpteRhUpdateTrafficControl : INTMD-EXIT\n");
        return RPTE_FAILURE;
    }

    if ((u1ResvRsrc == RPTE_FALSE) &&
        (pCtTnlInfo->pTeTnlInfo->b1IsResourceReserved == RPTE_FALSE))
    {
        return RPTE_SUCCESS;
    }
    /*if u4UpStrDataTeLinkIf != 0, then TLM is enabled */

    if ((pCtTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf != RSVPTE_ZERO) ||
        (pCtTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf != RSVPTE_ZERO))
    {
        reqResBw
            =
            OSIX_NTOHF ((RSVPTE_TNL_RSB (pCtTnlInfo))->FlowSpec.ServiceParams.
                        ClsService.TBParams.fPeakRate);

        RpteRhRelReroutedTnlBw (pCtTnlInfo, GMPLS_SEGMENT_FORWARD);

        if (RpteTlmUpdateTrafficControl
            (pCtTnlInfo, pCtTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf, reqResBw,
             u1ResvRsrc, pu1ResvRefreshNeeded, GMPLS_SEGMENT_FORWARD)
            == RPTE_FAILURE)
        {
            if (u1ResvRsrc != RPTE_FALSE)
            {
                pErrorSpec->u1ErrCode = ADMISSION_CONTROL_FAILURE;
            }
            return RPTE_FAILURE;
        }
        if ((u1ResvRsrc == RPTE_FALSE) &&
            (pCtTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf != RSVPTE_ZERO) &&
            (pCtTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION) &&
            (RpteTlmUpdateTrafficControl (pCtTnlInfo,
                                          pCtTnlInfo->pTeTnlInfo->
                                          u4UpStrDataTeLinkIf,
                                          reqResBw, u1ResvRsrc,
                                          pu1ResvRefreshNeeded,
                                          GMPLS_SEGMENT_REVERSE)) ==
            RPTE_FAILURE)
        {
            return RPTE_FAILURE;
        }

        return RPTE_SUCCESS;
    }
    if (u1ResvRsrc == RPTE_TRUE)
    {
        pTcFlowSpec = &RSB_FLOW_SPEC (pCtTnlInfo->pRsb);
    }
    else
    {
        /* Used only in Tearing Down the Reserved Path */
        pTcFlowSpec = &RSB_FLOW_SPEC (pCtTnlInfo->pRsb);
        if ((OSIX_NTOHS (pTcFlowSpec->MsgHdr.u2HdrLen)) != RSVPTE_ZERO)
        {
            RpteRhDelTcParams (pCtTnlInfo);
            RptePmRemTnlFromPrioList (RSB_OUT_IF_ENTRY (pCtTnlInfo->pRsb),
                                      RSVPTE_TNL_HOLD_PRIO (RPTE_TE_TNL
                                                            (pCtTnlInfo)),
                                      GMPLS_SEGMENT_FORWARD, pCtTnlInfo);

        }
        *pu1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;
        pCtTnlInfo->pTeTnlInfo->b1IsResourceReserved = RPTE_FALSE;

        RSVPTE_DBG (RSVPTE_RH_PRCS, "RESV: Reserved Resources are freed ..\n");
        /* Release the Reserved Resources */
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhUpdateTrafficControl : EXIT\n");
        return RPTE_SUCCESS;
    }

    pPathTe = &SENDER_TSPEC_OBJ (&PSB_SENDER_TSPEC_OBJ (pCtTnlInfo->pPsb));
    RSB_TC_TSPEC (pCtTnlInfo->pRsb) = *pPathTe;

    /* 
     * Register the RsvpTeSession with the Style info which will be 
     * useful during Reroute condition 
     */
    RSB_RPTE_TNL_INFO (pCtTnlInfo->pRsb) = pCtTnlInfo;

    if (RpteRhBookTcParams (pCtTnlInfo, &pErrorSpec->u1ErrCode,
                            RPTE_TC_INTSERV_TYPE) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: Traffic Controller updation Fails . "
                    "Pre-emption is tried\n");
        /* for pre-emption support */
        if ((RptePmGetTnlFromPrioList (RSB_OUT_IF_ENTRY (pCtTnlInfo->pRsb),
                                       RSVPTE_TNL_HOLD_PRIO (RPTE_TE_TNL
                                                             (pCtTnlInfo)),
                                       pCtTnlInfo, &PreemptTnlList,
                                       GMPLS_SEGMENT_FORWARD) != RPTE_SUCCESS)
            || (RpteRhPreemptTunnel (&PreemptTnlList) != RPTE_SUCCESS))
        {
            RSVPTE_DBG (RSVPTE_RH_ETEXT,
                        "RpteRhUpdateTrafficControl : INTMD-EXIT\n");
            return RPTE_FAILURE;
        }

        if (RpteRhBookTcParams
            (pCtTnlInfo, &pErrorSpec->u1ErrCode,
             RPTE_TC_INTSERV_TYPE) == RPTE_FAILURE)
        {
            RptePmRemTnlFromPrioList (RSB_OUT_IF_ENTRY
                                      (RSVPTE_TNL_RSB (pCtTnlInfo)),
                                      RSVPTE_TNL_HOLD_PRIO
                                      (RPTE_TE_TNL (pCtTnlInfo)),
                                      GMPLS_SEGMENT_FORWARD, pCtTnlInfo);
            RSVPTE_DBG (RSVPTE_RH_PRCS,
                        "RESV: Traffic Controller updation Fails.\n");
            RSVPTE_DBG (RSVPTE_RH_ETEXT,
                        "RpteRhUpdateTrafficControl : INTMD-EXIT\n");
            return RPTE_FAILURE;
        }
    }

    RptePmAddTnlToPrioList (RSB_OUT_IF_ENTRY (pCtTnlInfo->pRsb),
                            RSVPTE_TNL_HOLD_PRIO (RPTE_TE_TNL (pCtTnlInfo)),
                            GMPLS_SEGMENT_FORWARD, pCtTnlInfo);
    pCtTnlInfo->pTeTnlInfo->b1IsResourceReserved = RPTE_TRUE;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhUpdateTrafficControl : EXIT\n");

    if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) != RPTE_INGRESS)
    {
        *pu1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
    }
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RptePhRhProcessResvMsg                                 */
/* Description     : This function Generates the Resv Msg at the egress end.*/
/*                   It is called whenever a new tunnel is created at the   */
/*                   egress end.                                            */
/* Input (s)       : *pPktMap - Pointer to PktMap                           */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS/RPTE_FAILURE                              */
/****************************************************************************/
INT1
RptePhRhProcessResvMsg (tPktMap * pPktMap)
{
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pAdnlTnlInfo = NULL;
    tRsvpTeFilterSpec   RsvpTeFilterSpec;
    uLabel              InLabel;
    tFlowSpecObj        FlowSpecObj;
    tFlowSpec           FlowSpec;
    tStyleObj           StyleObj;
    tStyle              Style;
    tErrorSpec          ErrorSpec;
    tRsb               *pRsb = NULL;
    tIfEntry           *pIfEntry = NULL;
    UINT4               u4TmpAddr = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RptePhRhProcessResvMsg : ENTRY\n");

    MEMSET (&Style, RSVPTE_ZERO, sizeof (tStyle));
    MEMSET (&StyleObj, RSVPTE_ZERO, sizeof (tStyleObj));
    MEMSET (&InLabel, RSVPTE_ZERO, sizeof (uLabel));
    MEMSET (&FlowSpec, RSVPTE_ZERO, sizeof (tFlowSpec));
    MEMSET (&ErrorSpec, RSVPTE_ZERO, sizeof (tErrorSpec));
    MEMSET (&FlowSpecObj, RSVPTE_ZERO, sizeof (tFlowSpecObj));
    MEMSET (&RsvpTeFilterSpec, RSVPTE_ZERO, sizeof (tRsvpTeFilterSpec));

    pIfEntry = PKT_MAP_IF_ENTRY (pPktMap);
    /* SenderTemplate to RsvpTeFilterSpec */
    RsvpTeFilterSpec = RPTE_SNDR_TEMP_OBJ (PKT_MAP_RPTE_SND_TMP_OBJ (pPktMap));
    /*  for IntServ */
    if (RpteTcGenerateFlowSpec
        (&ADSPEC_OBJ (PKT_MAP_ADSPEC_OBJ (pPktMap)),
         &SENDER_TSPEC_OBJ (PKT_MAP_SENDER_TSPEC_OBJ (pPktMap)),
         &FlowSpec, &ErrorSpec.u1ErrCode) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Generating of FlowSpec Failed.\n");
        RptePvmSendPathErr (pPktMap,
                            ADMISSION_CONTROL_FAILURE, DEFAULT_ERROR_VALUE);
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                    "RptePhRhProcessResvMsg : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    FlowSpecObj.FlowSpec = FlowSpec;
    pPktMap->pFlowSpecObj = &FlowSpecObj;
    /* 
     * If session attribute flag is set to Ingress may reroute then style 
     * is made as SE else it is set to FF style 
     */
    if ((PKT_MAP_RPTE_SSN_ATTR_FLAGS (pPktMap) & RPTE_SE_STYLE_DESIRED)
        == RPTE_SE_STYLE_DESIRED)
    {
        RSVPTE_DBG1 (RSVPTE_RH_PRCS,
                     "RESV: Received message has Session attribute value "
                     "to be set to SE \n", Style.au1OptVect[RSVPTE_TWO]);
        Style.au1OptVect[RSVPTE_TWO] = RPTE_SE;
    }
    else
    {
        RSVPTE_DBG1 (RSVPTE_RH_PRCS,
                     "RESV: Received message has Session attribute value to "
                     "be set to FF \n", Style.au1OptVect[RSVPTE_TWO]);
        Style.au1OptVect[RSVPTE_TWO] = RPTE_FF;
    }
    StyleObj.Style = Style;
    pPktMap->pStyleObj = &StyleObj;

    /* To check whether TnlInfo is created by the Path message */
    pCtTnlInfo = RPTE_PKT_MAP_TNL_INFO (pPktMap);
    if (pCtTnlInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: TnlInfo does not exist for the received message "
                    "- Path Err Sent \n");
        RptePvmSendPathErr (pPktMap,
                            NO_PATH_INFO_FOR_RESV_MSG, DEFAULT_ERROR_VALUE);
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RptePhRhProcessResvMsg : INTMD-EXIT\n");
        return RPTE_FAILURE;
    }

    /* If bandwidth needs to be shared among two tunnels, the two tunnels
     * should have the Shared-Explicit reservation style. If the two tunnels
     * have different reservation style, bandwidth cannot be shared. */
    if (RPTE_CHECK_ADNL_TNL_PATH_MSG (pPktMap, pAdnlTnlInfo) != RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: Received message leads to additional TnlInfo\n");
        if (Style.au1OptVect[RSVPTE_TWO] == RPTE_SE)
        {
            if ((((STYLE_OPT_VECT (&RPTE_TNL_STYLE (pAdnlTnlInfo)))[RSVPTE_TWO])
                 & STYLE_MASK) != RPTE_SE)
            {
                RSVPTE_DBG (RSVPTE_RH_PRCS,
                            "RESV: Previous tunnel is established without SE "
                            "filter - Path Err Sent\n");
                RptePvmSendPathErr (pPktMap, ADMISSION_CONTROL_FAILURE,
                                    DEFAULT_ERROR_VALUE);
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RptePhRhProcessResvMsg : INTMD-EXIT\n");
                return RPTE_FAILURE;
            }
        }
    }

    /* 
     * Since always this function is called whenever a new tunnel is created at
     * the Egress end, everytime a new Rsb, Rsbcmnhdr is created 
     */
    if ((RPTE_PKT_MAP_PKT_PATH (pPktMap) == RPTE_FRR_BACKUP_PATH) &&
        (RPTE_IS_MP (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL (pCtTnlInfo)))) &&
        (RPTE_FRR_IN_TNL_INFO (RPTE_FRR_BASE_TNL (pCtTnlInfo)) != NULL))
    {
        if ((pCtTnlInfo->pBaseFrrProtTnlInfo->pTeTnlInfo->bIsSEStyleDesired
             == FALSE) &&
            (FRR_DETOUR_OBJ_HDR (PKT_MAP_RPTE_DETOUR_OBJ (pPktMap)).
             u2Length != RPTE_ZERO))
        {
            /* One to One case Path specific handle */
            MEMCPY (&RsvpTeFilterSpec.TnlSndrAddr,
                    &((PKT_MAP_RPTE_DETOUR_OBJ (pPktMap)).RsvpTeDetour.
                      au4PlrId[RPTE_ZERO]), IPV4_ADDR_LENGTH);
        }
    }

    if (RpteRhCreateRsb (&RsvpTeFilterSpec, pPktMap, pIfEntry, &pRsb)
        == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS, "RESV: Creating a Rsb failed..\n");
        RptePvmSendPathErr (pPktMap, ADMISSION_CONTROL_FAILURE,
                            DEFAULT_ERROR_VALUE);
        RSVPTE_DBG (RSVPTE_RH_ETEXT, " RptePhRhProcessResvMsg : INTMD-EXIT\n");
        return RPTE_FAILURE;
    }
    else
    {
        RSB_RPTE_TNL_INFO (pRsb) = pCtTnlInfo;
        RSVPTE_TNL_RSB (pCtTnlInfo) = pRsb;
    }

    if (RSVP_HDR_SEND_TTL ((tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap)) !=
        PKT_MAP_RX_TTL (pPktMap))
    {
        RSB_NON_RSVP_FLAG (pRsb) = RPTE_YES;
    }
    /* 
     * If the destination address in the RSVP-TE Path message is 
     * equal to
     * one of the local addresses, then we are the egress of the tunnel
     * and Tunnel role is set to egress
     */
    /* 
     * Get a new Label from the LblManager that is used for forwarding
     * the message to the PrevHop 
     */
    if ((RPTE_PKT_MAP_PKT_PATH (pPktMap) == RPTE_FRR_BACKUP_PATH) &&
        (RPTE_IS_MP (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL (pCtTnlInfo)))) &&
        (RPTE_FRR_IN_TNL_INFO (RPTE_FRR_BASE_TNL (pCtTnlInfo)) != NULL))
    {
        if (RSVPTE_TNL_RSB (RPTE_FRR_BASE_TNL (pCtTnlInfo)) != NULL)
        {
            MEMCPY (&RSVPTE_TNL_OUTLBL (pCtTnlInfo),
                    &RSVPTE_TNL_OUTLBL (RPTE_FRR_BASE_TNL (pCtTnlInfo)),
                    sizeof (uLabel));
            RSVPTE_TNL_RSB (pCtTnlInfo)->u1FrrMlibUpdateReq =
                RPTE_FRR_MLIB_UPDATE_TRUE;
            TE_TNL_IFINDEX (RPTE_TE_TNL (pCtTnlInfo)) =
                TE_TNL_IFINDEX (RPTE_TE_TNL (RPTE_FRR_BASE_TNL (pCtTnlInfo)));
            RSVPTE_DBG1 (RSVPTE_FRR_DBG, "RESV: ILM create, "
                         "Protected tnl out label %d..\n",
                         RSVPTE_TNL_OUTLBL (pCtTnlInfo));
        }
        else
        {
            RSVPTE_TNL_RSB (pCtTnlInfo)->u1FrrMlibUpdateReq =
                RPTE_FRR_MLIB_UPDATE_FALSE;
        }
    }
    if ((pCtTnlInfo)->DownstrInLbl.u4GenLbl == MPLS_INVALID_LABEL)
    {
        if (RpteRhGetLabel (PKT_MAP_IF_ENTRY (pPktMap), &InLabel) ==
            RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_RH_PRCS,
                        "RESV: Getting a free Label failed - Path Err Sent\n");
            RpteUtlDeleteRsb (pRsb);
            RptePvmSendPathErr (pPktMap, RPTE_ROUTE_PROB, RPTE_LBL_ALLOC_FAIL);
            RSVPTE_DBG (RSVPTE_RH_ETEXT,
                        "RptePhRhProcessResvMsg : INTMD-EXIT\n");
            return RPTE_FAILURE;
        }
        else
        {
            MEMCPY (&RSVPTE_TNL_INLBL (pCtTnlInfo), &InLabel, sizeof (uLabel));
        }
    }
    /* MplsMlibUpdate is called with ILM_CREATE to update the MPLS-FM */
    if ((RSVPTE_TNL_RSB (pCtTnlInfo)->u1FrrMlibUpdateReq !=
         RPTE_FRR_MLIB_UPDATE_FALSE)
        &&
        (RpteRhSendMplsMlibUpdate
         (RPTE_MPLS_MLIB_ILM_CREATE, RSVPTE_ZERO, pCtTnlInfo,
          GMPLS_SEGMENT_FORWARD) != RPTE_SUCCESS))
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS, "RESV: MplsMlibUpdate failed..\n");
        RpteUtlDeleteRsb (pRsb);
        RptePvmSendPathErr (pPktMap, ADMISSION_CONTROL_FAILURE,
                            DEFAULT_ERROR_VALUE);
        RpteRhFreeLabel (PKT_MAP_IF_ENTRY (pPktMap), &InLabel);
        MEMSET (&RSVPTE_TNL_INLBL (pCtTnlInfo), RSVPTE_ZERO, sizeof (uLabel));
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RptePhRhProcessResvMsg : INTMD-EXIT\n");
        return RPTE_FAILURE;
    }

    TE_TNL_IFINDEX (RPTE_TE_TNL (pCtTnlInfo)) = RPTE_ZERO;

    if ((RPTE_PKT_MAP_PKT_PATH (pPktMap) == RPTE_FRR_BACKUP_PATH) &&
        (RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL) &&
        (RSVPTE_TNL_NODE_RELATION
         (RPTE_FRR_BASE_TNL (pCtTnlInfo)) == RPTE_EGRESS) &&
        (RSVPTE_TNL_RSB (pCtTnlInfo)->u1FrrMlibUpdateReq))
    {
        /* Indicate the Tunnel application to do the MP programming */
        TeSigNotifyFrrTnlMpHwIlmAddOrDel
            (RPTE_TE_TNL (RPTE_FRR_BASE_TNL (pCtTnlInfo)),
             RPTE_TE_TNL (pCtTnlInfo), TRUE);
        RSVPTE_DBG (RSVPTE_FRR_DBG,
                    " Egress Send Resv : TeSigNotifyFrrTnlMpHwIlmAddOrDel called\n");
    }

    u4TmpAddr = RSVPTE_TNL_INGRESS (pCtTnlInfo);
    MEMCPY (&(pRsb->ResvConf.u4RecevAddr), &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    if ((RPTE_IS_FRR_CAPABLE (pCtTnlInfo) == RPTE_TRUE) &&
        (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_PROTECTED_TNL))
    {
        RPTE_FRR_NODE_STATE (pCtTnlInfo) |= RPTE_FRR_NODE_STATE_TAILEND;
    }

    RpteRhResvRefresh (pCtTnlInfo);

    /* After sending resv msg for newer tunnel, send resv message with 
     * admin status object (Deletion in progress) for older tunnel in
     * case of FULL_REROUTE scenario. And also start grace full deletion
     * timer. If the node doesn't receive path tear for older tunnel,
     * it will delete the older tunnel in the timer expiry*/

    if ((pCtTnlInfo->pMapTnlInfo != NULL) &&
        (pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_FULL_REROUTE))
    {
        pCtTnlInfo->pMapTnlInfo->pRsb->u4AdminStatus
            = GMPLS_ADMIN_DELETE_IN_PROGRESS;
        pCtTnlInfo->pMapTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus =
            GMPLS_ADMIN_DELETE_IN_PROGRESS;
        RpteRhResvRefresh (pCtTnlInfo->pMapTnlInfo);
        RptePhStartGracefulDelTmr (pCtTnlInfo->pMapTnlInfo);
    }
    /* Once the GR (egress) receives path message from the helper, reset the GrTnl flag */

    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pCtTnlInfo->u1IsGrTnl == RPTE_YES))
    {
        pCtTnlInfo->u1IsGrTnl = RPTE_NO;
    }
    RpteUtlCleanPktMap (pPktMap);
    pPktMap->pFlowSpecObj = NULL;
    pPktMap->pStyleObj = NULL;
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RptePhRhProcessResvMsg : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteRhProcessResvMsg                                   */
/* Description     : This function processes the Pkt Map supplied by the    */
/*                   Resv Msg.                                              */
/* Input (s)       : *pPktMap - Pointer to PktMap                           */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhProcessResvMsg (tPktMap * pPktMap)
{
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pOrgCtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRpteBaseTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRpteTmpTnlInfo = NULL;
    tRsvpTeTnlInfo     *pCtProtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeBackTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tRsvpTeSession     *pRsvpTeSession = NULL;
    tErrorSpec          ErrorSpec;
    uLabel              OutLabel;
    uLabel              InLabel;
    tRsvpTeFilterSpec  *pRsvpTeFilterSpec = NULL;
    tFlowSpec          *pFlowSpec = NULL;
    tRsvpHop           *pNhop = NULL;
    tStyle             *pStyle = NULL;
    tPsb               *pPsb = NULL;
    tRsb               *pRsb = NULL;
    tRpteKey            RpteKey;
    tTimeValues        *pTimeValues = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tuTrieInfo         *pTrieInfo = NULL;
    tIfEntry           *pIfEntry = NULL;
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;
    FLT4                f4TmpVar = RSVPTE_ZERO;
    UINT4               u4FilterSpecAddr = RSVPTE_ZERO;
    UINT4               u4SenderNodeAddr = RSVPTE_ZERO;
    UINT4               u4Bandwth = RSVPTE_ZERO;
    UINT4               u4TmpVar = RSVPTE_ZERO;
    UINT4               u4RsbTimeToDie = RSVPTE_ZERO;
    UINT4               u4ExtnTnlId = RSVPTE_ZERO;
    UINT4               u4DestAddrId = RSVPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RPTE_ZERO;
    UINT2               u2FSpecObjLength = RSVPTE_ZERO;
    UINT1               u1New = RPTE_NO;
    UINT1               u1IsMsgFromBkpTnl = RPTE_NO;
    UINT1               u1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;
    UINT1               u1ResvDsRefreshNeeded = RPTE_REFRESH_MSG_NO;
    UINT1               u1ReRouteFlag = RPTE_NO;
    UINT1               u1ReCount = RSVPTE_ZERO;
    UINT1               u1ResvRsrc = RPTE_FALSE;
    UINT1               u1DetourGrpId = RPTE_ZERO;
    UINT1               u1FrrNodeState = RPTE_ZERO;
    INT4                i4StorageType = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : ENTRY\n");
    MEMSET (&InLabel, RSVPTE_ZERO, sizeof (uLabel));
    MEMSET (&OutLabel, RSVPTE_ZERO, sizeof (uLabel));
    MEMSET (&ErrorSpec, RSVPTE_ZERO, sizeof (tErrorSpec));

    /* 
     * This Routine is called to get the mandatory objects for the Resv
     * message. 
     * flag is set.
     */
    if ((RpteUtlResvManObjHandler (pPktMap, &pRsvpTeSession, &pNhop,
                                   &pFlowSpec, &pRsvpTeFilterSpec))
        != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: Received message has mandatory objects missing\n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : INTMD-EXIT\n");
        return;
    }
    pIfEntry = PKT_MAP_IF_ENTRY (pPktMap);
    pNbrEntry = RPTE_PKT_MAP_NBR_ENTRY (pPktMap);
    /* NBR_ENTRY_ADDR (pNbrEntry) = OSIX_NTOHL (NBR_ENTRY_ADDR (pNbrEntry)); */
    pStyle = &STYLE_OBJ (PKT_MAP_STYLE_OBJ (pPktMap));
    /* 
     * If the PktMap is received with the format specified in the reroute
     * condition of Rsvp-Lsp-tunnel-05.txt
     */
    u1ReRouteFlag = PKT_MAP_RE_ROUTE_FLAG (pPktMap);
    if (!(((((STYLE_OPT_VECT (pStyle))[RSVPTE_TWO]) & STYLE_MASK) == RPTE_FF) ||
          ((((STYLE_OPT_VECT (pStyle))[RSVPTE_TWO]) & STYLE_MASK) == RPTE_SE)))
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: Currently WF FilterStyle is not supported..\n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : INTMD-EXIT\n");
        return;
    }

    /* To check whether TnlInfo is created by the Path message */
    if (RPTE_CHECK_TNL_RESV_MSG (pPktMap, pCtTnlInfo) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESV: TnlInfo doesnot exist for the received"
                    " message - Resv Err Sent\n");
        RptePvmSendResvErr (pPktMap, NO_PATH_INFO_FOR_RESV_MSG,
                            DEFAULT_ERROR_VALUE);
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : INTMD-EXIT\n");
        return;
    }

    if ((pCtTnlInfo->pTeTnlInfo->u1TnlSyncStatus == TNL_GR_NOT_SYNCHRONIZED) &&
        (pNbrEntry->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS))
    {
        pCtTnlInfo->pTeTnlInfo->u1TnlSyncStatus = TNL_GR_FULLY_SYNCHRONIZED;
    }

    /* During the neighbor restart period, node may send notification message 
       with error value - control channel degrade to its downstream node
       Also it may send notification message with error value - control channel
       active to its downstream node in recovery period
     */

    if ((pCtTnlInfo->pUStrNbr != NULL) &&
        (pCtTnlInfo->pTeTnlInfo->u1TnlSyncStatus == TNL_GR_NOT_SYNCHRONIZED) &&
        (pCtTnlInfo->pRsb != NULL)
        && (pCtTnlInfo->pRsb->u4NotifyAddr != RPTE_ZERO))
    {
        if (pCtTnlInfo->pUStrNbr->u1GrProgressState ==
            RPTE_GR_RESTART_IN_PROGRESS)
        {
            RpteNhAddTnlToNotifyRecipient (pCtTnlInfo,
                                           pCtTnlInfo->pRsb->u4NotifyAddr,
                                           UPSTREAM, RPTE_FALSE,
                                           RPTE_NOTIFY,
                                           RPTE_CONTROL_CHANNEL_DEGRADE);

        }
        else if (pCtTnlInfo->pUStrNbr->u1GrProgressState ==
                 RPTE_GR_RECOVERY_IN_PROGRESS)
        {
            RpteNhAddTnlToNotifyRecipient (pCtTnlInfo,
                                           pCtTnlInfo->pRsb->u4NotifyAddr,
                                           UPSTREAM, RPTE_FALSE,
                                           RPTE_NOTIFY,
                                           RPTE_CONTROL_CHANNEL_ACTIVE);
        }
    }

    /* Check whether ACK Timer for Path Tear is running or not.
     * If the timer is running we should not accept the RESV Message and
     * RESV ERROR should be sent. */
    if (RPTE_TNL_PT_TIMER_BLOCK (pCtTnlInfo) != NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS, "PT TIMER Running - Resv Err Sent\n");
        RptePvmSendResvErr (pPktMap, NO_PATH_INFO_FOR_RESV_MSG,
                            DEFAULT_ERROR_VALUE);
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : INTMD-EXIT\n");
        return;
    }

    pTimeValues = &TIME_VALUES_OBJ (PKT_MAP_TIME_VALUES_OBJ (pPktMap));
    u4RsbTimeToDie =
        RpteUtlComputeLifeTime (PKT_MAP_IF_ENTRY (pPktMap),
                                OSIX_NTOHL (TIME_VALUES_PERIOD (pTimeValues)));
    MEMCPY (&OutLabel, &PKT_MAP_LBL_OBJ_LBL (pPktMap), sizeof (uLabel));

    if ((RPTE_REOPTIMIZE_TIMER_STATE (pCtTnlInfo) ==
         RPTE_REOPTIMIZE_TIMER_NOT_STARTED) &&
        (RSVPTE_TE_TNL_REOPTIMIZE_STATUS (pCtTnlInfo->pTeTnlInfo)
         == RPTE_TE_REOPTIMIZE_ENABLE) &&
        ((RSVPTE_TNL_ROLE (pCtTnlInfo->pTeTnlInfo) == RPTE_INGRESS) ||
         (RPTE_REOPTIMIZE_NODE_STATE (pCtTnlInfo) == RPTE_MID_POINT_NODE)))

    {
        RpteRhStartReoptimizeTmr (pCtTnlInfo);
        RPTE_REOPTIMIZE_TIMER_STATE (pCtTnlInfo) =
            RPTE_REOPTIMIZE_TIMER_STARTED;
    }

    /* If the Node State is PLR, it means we will receive RESV Refresh 
     * Message for both the protected tunnel and backup tunnel.
     * If the Node State is PLR_AWAIT, it means RESV Message for the 
     * protected tunnel has already been received for which we have 
     * triggered the PATH Message signalling on the backup path and 
     * expecting RESV Message on the backup path 
     */
    pRpteBaseTnlInfo = pCtTnlInfo;

    u1FrrNodeState = RPTE_FRR_NODE_STATE (pRpteBaseTnlInfo);
    if (RPTE_IS_FRR_CAPABLE (pCtTnlInfo) != RPTE_TRUE)
    {
        pPsb = RSVPTE_TNL_PSB (pCtTnlInfo);
        if (pPsb == NULL)
        {
            RSVPTE_DBG (RSVPTE_RH_PRCS,
                        "RESV: Psb doesnot exist for the received "
                        "message - Resv Err Sent\n");
            RptePvmSendResvErr (pPktMap, NO_SENDER_INFO_FOR_RESV_MSG,
                                DEFAULT_ERROR_VALUE);
            RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : INTMD-EXIT\n");
            return;
        }
        if (pCtTnlInfo->pTeTnlInfo->u4TnlMapIndex == RPTE_ZERO)
        {
            if ((RpteUtlPsbRoutesToOutIf (pPsb, pIfEntry, pNhop) != RPTE_YES))
            {
                RSVPTE_DBG (RSVPTE_RH_PRCS,
                            "RESV: Out Tunnel Info NULL - Resv Err Sent\n");
                RptePvmSendResvErr (pPktMap, NO_SENDER_INFO_FOR_RESV_MSG,
                                    DEFAULT_ERROR_VALUE);
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RpteRhProcessResvMsg : INTMD-EXIT\n");
                return;
            }
        }
    }
    else if (u1FrrNodeState == RPTE_FRR_NODE_STATE_UNKNOWN)
    {
        MEMCPY (&u4FilterSpecAddr, &(pRsvpTeFilterSpec->TnlSndrAddr),
                RSVPTE_IPV4ADR_LEN);
        u4FilterSpecAddr = OSIX_NTOHL (u4FilterSpecAddr);
        do
        {
            pPsb = RSVPTE_TNL_PSB (pCtTnlInfo);
            if (pPsb == NULL)
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "RESV: Psb doesnot exist for the received "
                            "message - Resv Err Sent\n");
                RptePvmSendResvErr (pPktMap, NO_SENDER_INFO_FOR_RESV_MSG,
                                    DEFAULT_ERROR_VALUE);
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RpteRhProcessResvMsg : INTMD-EXIT\n");
                return;
            }

            MEMCPY (&u4SenderNodeAddr,
                    &PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB
                                        (pCtTnlInfo)).TnlSndrAddr,
                    RSVPTE_IPV4ADR_LEN);

            RSVPTE_DBG5 (RSVPTE_FRR_DBG,
                         "Sender Address of Tunnel %d %d %x %x is %x\n",
                         RSVPTE_TNL_TNLINDX (pCtTnlInfo),
                         RSVPTE_TNL_TNLINST (pCtTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pCtTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pCtTnlInfo)),
                         OSIX_NTOHL (u4SenderNodeAddr));

            u4SenderNodeAddr = OSIX_NTOHL (u4SenderNodeAddr);
            /* First contd. matches for sender template specific method and second 
             * one matches for path specific method backup path resv message handle */
            if ((u4FilterSpecAddr == u4SenderNodeAddr) ||
                (((PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB
                                        (pCtTnlInfo)).au4PlrId[RPTE_ZERO])
                  != RPTE_ZERO)
                 && ((PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB
                                           (pCtTnlInfo)).au4PlrId[RPTE_ZERO])
                     == u4FilterSpecAddr)))
            {
                break;
            }

        }
        while ((pCtTnlInfo = RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)) != NULL);
        if (pCtTnlInfo == NULL)
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESV: Psb doesnot exist for the received "
                        "message - Resv Err Sent\n");
            RptePvmSendResvErr (pPktMap, NO_SENDER_INFO_FOR_RESV_MSG,
                                DEFAULT_ERROR_VALUE);
            RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : INTMD-EXIT\n");
            return;
        }

        RSVPTE_DBG4 (RSVPTE_FRR_DBG,
                     "Resv Message rcvd for the tunnel %d %d %x %x\n",
                     RSVPTE_TNL_TNLINDX (pCtTnlInfo),
                     RSVPTE_TNL_TNLINST (pCtTnlInfo),
                     OSIX_NTOHL (RSVPTE_TNL_INGRESS (pCtTnlInfo)),
                     OSIX_NTOHL (RSVPTE_TNL_EGRESS (pCtTnlInfo)));
    }
    if (RPTE_IS_PLR_OR_PLRAWAIT (u1FrrNodeState))
    {
        /* Reserve Message can come for protected or backup tunnel 
         * So, verifying whether it has been for the protected tunnel. */
        pPsb = RSVPTE_TNL_PSB (pCtTnlInfo);
        if (pPsb == NULL)
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESV: Psb doesnot exist for the received "
                        "message - Resv Err Sent\n");
            RptePvmSendResvErr (pPktMap, NO_SENDER_INFO_FOR_RESV_MSG,
                                DEFAULT_ERROR_VALUE);
            RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : INTMD-EXIT\n");
            return;
        }

        if ((RpteUtlPsbRoutesToOutIf (pPsb, pIfEntry, pNhop) != RPTE_YES) ||
            ((RSVPTE_TNL_RSB (pCtTnlInfo) != NULL) &&
             (RpteUtlRsbRoutesToInIf (RSVPTE_TNL_RSB (pCtTnlInfo),
                                      pIfEntry, pNhop)) != RPTE_YES))
        {
            /* We Should send a RESV ERROR if RESV Message is received on
             * Wrong Outgoing Interface. This has to be done here, if FRR
             * is not supported. */
            /* FRR handle */
            if (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) == NULL)
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "RESV: Out Tunnel Info NULL - Resv Err Sent\n");
                RptePvmSendResvErr (pPktMap, NO_SENDER_INFO_FOR_RESV_MSG,
                                    DEFAULT_ERROR_VALUE);
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RpteRhProcessResvMsg : INTMD-EXIT\n");
                return;
            }
            RPTE_PKT_MAP_TNL_INFO (pPktMap) =
                RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo);
            pPsb = RSVPTE_TNL_PSB (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo));
            if (pPsb == NULL)
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "RESV: Psb doesnot exist for the received "
                            "message - Resv Err Sent\n");
                RptePvmSendResvErr (pPktMap, NO_SENDER_INFO_FOR_RESV_MSG,
                                    DEFAULT_ERROR_VALUE);
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RpteRhProcessResvMsg : INTMD-EXIT\n");
                return;
            }
            /* Reserve Message has not received on protected tunnel. 
             * So, verifying whether it has received on the backup tunnel. */
            if ((RpteUtlPsbRoutesToOutIf (pPsb, pIfEntry, pNhop) != RPTE_YES))
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "RESV: Resv message is received in wrong "
                            "interface - Resv Err Sent\n");
                RptePvmSendResvErr (pPktMap,
                                    NO_SENDER_INFO_FOR_RESV_MSG,
                                    DEFAULT_ERROR_VALUE);
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RpteRhProcessResvMsg : INTMD-EXIT\n");
                return;
            }
            /* Resv Message is received on the backup path */
            u1IsMsgFromBkpTnl = (UINT1) RPTE_YES;
            if (RPTE_IS_PLRAWAIT (u1FrrNodeState))
            {
                RPTE_FRR_NODE_STATE (pRpteBaseTnlInfo) &=
                    ~(RPTE_FRR_NODE_STATE_PLRAWAIT);
                RPTE_FRR_NODE_STATE (pRpteBaseTnlInfo) |=
                    RPTE_FRR_NODE_STATE_PLR;
                u1FrrNodeState = RPTE_FRR_NODE_STATE (pRpteBaseTnlInfo);
                pRpteBaseTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse
                    = LOCAL_PROT_AVAIL;

                RSVPTE_DBG (RSVPTE_FRR_DBG, "The router has become PLR \n");
                if (RSVPTE_FRR_MAKE_AFTER_BREAK_ENABLED (gpRsvpTeGblInfo) ==
                    RPTE_SNMP_TRUE)
                {
                    if (RPTE_IS_HEADEND (RPTE_FRR_NODE_STATE
                                         (pRpteBaseTnlInfo)))
                    {
                        RpteStartFrrGblRevertTimer (pRpteBaseTnlInfo);
                    }
                }
                if ((RPTE_FRR_CSPF_INFO (pRpteBaseTnlInfo) &
                     RPTE_FRR_CSPF_NODE_FAILED) == RPTE_FRR_CSPF_NODE_FAILED)
                {
                    RSVPTE_TNL_RRO_FLAGS (pRpteBaseTnlInfo) |= LOCAL_PROT_AVAIL;
                }
                else if (RpteUtlIsNodeProtDesired (pRpteBaseTnlInfo)
                         == RPTE_YES)
                {
                    RSVPTE_TNL_RRO_FLAGS (pRpteBaseTnlInfo) |= LOCAL_PROT_AVAIL;
                    RSVPTE_TNL_RRO_FLAGS (pRpteBaseTnlInfo) |= FRR_NODE_PROT;
                }
                else
                {
                    RSVPTE_TNL_RRO_FLAGS (pRpteBaseTnlInfo) |= LOCAL_PROT_AVAIL;
                }
                if (RpteUtlIsBwProtDesired (pRpteBaseTnlInfo, &u4Bandwth)
                    == RPTE_YES)
                {
                    f4TmpVar = OSIX_NTOHF (RESV_TSPEC_BKT_RATE
                                           (FLOW_SPEC_OBJ
                                            (PKT_MAP_FLOW_SPEC_OBJ (pPktMap))));
                    RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);

                    if (u4Bandwth == (UINT4) f4TmpVar)
                    {
                        RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) |= FRR_BW_PROT;
                    }
                }
            }
            pCtTnlInfo = RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo);
        }
        pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
        /* Only First time Resv messages need to be checked for the
         * presence of same label been allocated to some other tunnel
         * wherein for Resv-Refreshes it shld be ignored
         */
    }

    if (RPTE_IS_DMP (u1FrrNodeState))
    {
        if (RpteUtlGetGrpIdForDMP (pCtTnlInfo, pIfEntry, &u1DetourGrpId) ==
            RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESV: Psb doesnot exist for the received "
                        "message - Resv Err Sent\n");
            RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
            RptePvmSendResvErr (pPktMap, NO_SENDER_INFO_FOR_RESV_MSG,
                                DEFAULT_ERROR_VALUE);
            RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : INTMD-EXIT\n");
            return;
        }
    }

    /* This function is used to Fill the Frr Facility List
     * Entry */
    if ((RPTE_TE_TNL (pCtTnlInfo) != NULL) &&
        ((RSVPTE_TNL_ROLE (RPTE_TE_TNL (pCtTnlInfo))) == RPTE_INGRESS) &&
        (RpteFrrFillFacilityListEntry (pCtTnlInfo) == RPTE_FAILURE))
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS, "RpteRhResvRefresh :"
                    "Fail to fill the backup information in frr Facility list\n");
    }

    /* Only First time Resv messages need to be checked for the
     * presence of same label been allocated to some other tunnel
     * wherein for Resv-Refreshes it shld be ignored
     */
    pOrgCtTnlInfo = pCtTnlInfo;

    /* For facility backup method, base tunnel's labels to be checked
       since it may receive different label for base tunnel in case
       of node protection
       * */

    if ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRpteBaseTnlInfo)).
         u1Flags) == RSVPTE_TNL_FRR_FACILITY_METHOD)
    {
        pCtTnlInfo = pRpteBaseTnlInfo;
    }

    do
    {
        u1New = RPTE_NO;
        if (RPTE_IS_DMP (u1FrrNodeState))
        {
            if (u1DetourGrpId != RPTE_FRR_DETOUR_GRP_ID (pCtTnlInfo))
            {
                continue;
            }
        }
        pPsb = RSVPTE_TNL_PSB (pCtTnlInfo);
        if (pPsb == NULL)
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESV: Psb doesnot exist for the received "
                        "message - Resv Err Sent\n");
            RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
            RptePvmSendResvErr (pPktMap, NO_SENDER_INFO_FOR_RESV_MSG,
                                DEFAULT_ERROR_VALUE);
            RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : INTMD-EXIT\n");
            return;
        }

        pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
        if ((pRsb == NULL) &&
            (RpteCheckAllTnlInTnlTable
             (gpRsvpTeGblInfo, OutLabel, pIfEntry) != RPTE_SUCCESS))

        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESV: Resv message has Same Atm label / "
                        "Implicit Null Label.Resv Err Sent\n");
            RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
            RptePvmSendResvErr (pPktMap, RPTE_ROUTE_PROB,
                                RPTE_UNACTBL_LABEL_VALUE);
            RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : INTMD-EXIT\n");
            return;
        }
        /* check for TnlRsb */
        if (pRsb != NULL)
        {
            /* ResvRefresh mesg received from the peer */
            /* compare the style received in prev mesg with this mesg */
            if ((RpteUtlCompareStyle (&RSB_STYLE (pRsb),
                                      pStyle)) != RPTE_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "RESV: Incoming Style contradicts with prev "
                            "Style - Resv Err Sent\n");
                RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
                RptePvmSendResvErr (pPktMap, CONFLICTING_RESV_STYLE,
                                    DEFAULT_ERROR_VALUE);
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RpteRhProcessResvMsg : INTMD-EXIT\n");
                return;
            }
            if ((RPTE_IS_FRR_CAPABLE (pCtTnlInfo) != RPTE_TRUE) ||
                (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_PROTECTED_TNL))
            {
                /* compare the FlowSpec received in prev mesg with this mesg */
                if ((u1ReCount == RSVPTE_ZERO)
                    || ((u1ReRouteFlag == RPTE_YES)
                        && (u1ReCount != RSVPTE_ZERO)))
                {
                    if (((tFlowSpec *) pFlowSpec)->SrvHdr.u1SrvID
                        == FLOWSPEC_CLS_HDR_NUMBER)
                    {
                        u2FSpecObjLength = sizeof (tFlowSpec)
                            - sizeof (tGsRSpec);
                    }
                    else
                    {
                        u2FSpecObjLength = sizeof (tFlowSpec);
                    }

                    if ((MEM_CMP (pFlowSpec, &RSB_FLOW_SPEC (pRsb),
                                  u2FSpecObjLength)) != RSVPTE_ZERO)
                    {
                        RSVPTE_DBG1 (RSVPTE_FRR_DBG,
                                     "RESV: Incoming FlowSpec contradicts "
                                     "with prev FlowSpec with u1ReRouteFlag: %u\n",
                                     u1ReRouteFlag);
                        RpteUtlCleanPktMap (pPktMap);
                        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                                    "RpteRhProcessResvMsg : INTMD-EXIT\n");
                        return;
                    }
                }
            }
            /* 
             * comparision done to check for the label validation, 
             * when we receive resv refresh msg.
             */
            if (MEMCMP (&RSVPTE_TNL_OUTLBL (pCtTnlInfo), &OutLabel,
                        sizeof (uLabel)) != RSVPTE_ZERO)
            {
                /* There is a possibility of peer giving the different label 
                 * incase of 1:1 node protection */
                if (((RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo)) &
                     LOCAL_PROT_IN_USE) == LOCAL_PROT_IN_USE)
                {
                    RSVPTE_DBG (RSVPTE_FRR_DBG,
                                "RESV: Incoming Label contradicts with "
                                "prev Label in detour Active case\n");

                    if ((RPTE_IS_FRR_CAPABLE (pCtTnlInfo) == RPTE_TRUE)
                        &&
                        ((RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) & FRR_NODE_PROT) ==
                         FRR_NODE_PROT))
                    {
                        RSVPTE_DBG (RSVPTE_FRR_DBG,
                                    "RESV: No need to take action when label mismatch happens "
                                    "with detour active in FRR Node-protection\n");
                        MEMCPY (&RSVPTE_TNL_OUTLBL (pCtTnlInfo), &OutLabel,
                                sizeof (uLabel));
                    }
                    else if (RPTE_IS_HEADEND (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
                    {
                        /* Perform the normal tunnel delete and 
                         * create for protected tnl */
                        RPTE_TE_TNL (pCtTnlInfo)->bFrrDnStrLblChg = RPTE_TRUE;
                        RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE,
                                                  RSVPTE_ZERO, pCtTnlInfo,
                                                  GMPLS_SEGMENT_FORWARD);
                        MEMCPY (&RSVPTE_TNL_OUTLBL (pCtTnlInfo), &OutLabel,
                                sizeof (uLabel));
                        RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE,
                                                  RSVPTE_ZERO, pCtTnlInfo,
                                                  GMPLS_SEGMENT_FORWARD);
                        RPTE_TE_TNL (pCtTnlInfo)->bFrrDnStrLblChg = RPTE_FALSE;
                    }
                    else
                    {
                        MEMCPY (&RSVPTE_TNL_OUTLBL (pCtTnlInfo), &OutLabel,
                                sizeof (uLabel));
                    }
                }
                else
                {
                    RSVPTE_DBG (RSVPTE_FRR_DBG,
                                "RESV: Incoming Label contradicts with "
                                "prev Label\n");
                    if (((~(RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo)) &
                          LOCAL_PROT_IN_USE) == LOCAL_PROT_IN_USE)
                        &&
                        (((RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo)) &
                          FRR_NODE_PROT) == FRR_NODE_PROT)
                        && (RPTE_TRUE == pCtTnlInfo->b1IsReversionTriggered))
                    {
                        RSVPTE_DBG (RSVPTE_FRR_DBG,
                                    "RESV: Reversion triggered for FRR capable"
                                    " tunnel - Facility backup node protection"
                                    " and reserve message received on restored"
                                    " resource ignore label mismatch\n");
                        MEMCPY (&RSVPTE_TNL_OUTLBL (pCtTnlInfo), &OutLabel,
                                sizeof (uLabel));

                    }
                    else
                    {
                        if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) ==
                            RPTE_INGRESS)
                        {
                            CfaGetIfMainStorageType
                                (pCtTnlInfo->pTeTnlInfo->u4TnlIfIndex,
                                 &i4StorageType);

                            CfaSetIfMainStorageType
                                (pCtTnlInfo->pTeTnlInfo->u4TnlIfIndex,
                                 MPLS_STORAGE_NONVOLATILE);

                            if (RPTE_IS_FRR_CAPABLE (pCtTnlInfo) == RPTE_TRUE)
                            {
                                rpteTeFrrTnlAppDown (RPTE_TE_TNL (pCtTnlInfo));
                            }
                            else
                            {
                                RSVPTE_DBG1 (RSVPTE_RH_ETEXT,
                                             "RESV: Sending TE_OPER_DOWN for label: %u"
                                             "\n",
                                             RSVPTE_TNL_OUTLBL (pCtTnlInfo).
                                             u4GenLbl);
                                TeSigProcessL2VpnAssociation (RPTE_TE_TNL
                                                              (pCtTnlInfo),
                                                              TE_OPER_DOWN);
                            }

                            if ((pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                                 i4E2EProtectionType ==
                                 MPLS_TE_DEDICATED_ONE2ONE)
                                && (pCtTnlInfo->pTeTnlInfo->u1TnlPathType ==
                                    RPTE_TNL_WORKING_PATH)
                                &&
                                ((pCtTnlInfo->pTeTnlInfo->
                                  u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE)
                                 || (pCtTnlInfo->pTeTnlInfo->
                                     u1TnlLocalProtectInUse ==
                                     LOCAL_PROT_AVAIL)))
                            {
                                pCtTnlInfo->pTeTnlInfo->bIsLabelChange =
                                    RPTE_ONE;
                            }
                            RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE,
                                                      RSVPTE_ZERO, pCtTnlInfo,
                                                      GMPLS_SEGMENT_FORWARD);

                            MEMCPY (&RSVPTE_TNL_OUTLBL (pCtTnlInfo), &OutLabel,
                                    sizeof (uLabel));

                            if (i4StorageType == MPLS_STORAGE_VOLATILE)
                            {
                                CfaSetIfMainStorageType
                                    (pCtTnlInfo->pTeTnlInfo->u4TnlIfIndex,
                                     i4StorageType);
                            }

                            /* Since the L2VPN PW is created over the tunnel,
                             * Tunnel reprogramming needs to be done once L2VPN
                             * module deletes the older tunnel
                             * */
                            if (TE_TNL_IN_USE_BY_VPN (pCtTnlInfo->pTeTnlInfo) &
                                TE_TNL_INUSE_BY_L2VPN)
                            {
                                pCtTnlInfo->pTeTnlInfo->u1TnlDelSyncFlag =
                                    MPLS_TRUE;
                            }
                            else
                            {
                                RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE,
                                                          RSVPTE_ZERO,
                                                          pCtTnlInfo,
                                                          GMPLS_SEGMENT_FORWARD);

                                if (RPTE_IS_FRR_CAPABLE (pCtTnlInfo) ==
                                    RPTE_TRUE)
                                {
                                    rpteTeFrrTnlAppUp (RPTE_TE_TNL
                                                       (pCtTnlInfo));
                                }
                                else
                                {
                                    if ((pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                                         i4E2EProtectionType ==
                                         MPLS_TE_DEDICATED_ONE2ONE)
                                        && (pCtTnlInfo->pTeTnlInfo->
                                            u1TnlPathType ==
                                            RPTE_TNL_WORKING_PATH)
                                        && (pCtTnlInfo->pTeTnlInfo->
                                            u1TnlLocalProtectInUse ==
                                            LOCAL_PROT_IN_USE))
                                    {

                                        RSVPTE_DBG1 (RSVPTE_RH_ETEXT,
                                                     "RESV: Not Sending TE_OPER_UP for label: %u"
                                                     "\n",
                                                     RSVPTE_TNL_OUTLBL
                                                     (pCtTnlInfo).u4GenLbl);
                                    }
                                    else
                                    {

                                        RSVPTE_DBG1 (RSVPTE_RH_ETEXT,
                                                     "RESV: Sending TE_OPER_UP for label: %u"
                                                     "\n",
                                                     RSVPTE_TNL_OUTLBL
                                                     (pCtTnlInfo).u4GenLbl);
                                        TeSigProcessL2VpnAssociation
                                            (RPTE_TE_TNL (pCtTnlInfo),
                                             TE_OPER_UP);
                                    }
                                }

                            }
                        }
                        else
                        {
                            RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                                      RSVPTE_ZERO, pCtTnlInfo,
                                                      GMPLS_SEGMENT_FORWARD);

                            MEMCPY (&RSVPTE_TNL_OUTLBL (pCtTnlInfo), &OutLabel,
                                    sizeof (uLabel));

                            RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE,
                                                      RSVPTE_ZERO, pCtTnlInfo,
                                                      GMPLS_SEGMENT_FORWARD);
                        }
                    }
                }
            }

            RSB_TIME_TO_DIE (pRsb) = u4RsbTimeToDie;
            RSB_IN_REFRESH_INTERVAL (pRsb) = OSIX_NTOHL
                (TIME_VALUES_PERIOD (pTimeValues));

            if (RSVPTE_TNL_RSB (pCtTnlInfo)->u1FrrMlibUpdateReq
                == RPTE_FRR_MLIB_UPDATE_FALSE)
            {
                if ((RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL) &&
                    (RSVPTE_TNL_RSB (RPTE_FRR_BASE_TNL (pCtTnlInfo)) != NULL))
                {
                    MEMCPY (&RSVPTE_TNL_OUTLBL (pCtTnlInfo),
                            &RSVPTE_TNL_OUTLBL (RPTE_FRR_BASE_TNL (pCtTnlInfo)),
                            sizeof (uLabel));
                    TE_TNL_IFINDEX (RPTE_TE_TNL (pCtTnlInfo)) =
                        TE_TNL_IFINDEX (RPTE_TE_TNL
                                        (RPTE_FRR_BASE_TNL (pCtTnlInfo)));
                    /* MplsMlibUpdate is called with ILM_CREATE 
                     * to update the MPLS-FM */
                    if (RpteRhSendMplsMlibUpdate
                        (RPTE_MPLS_MLIB_ILM_CREATE, RSVPTE_ZERO,
                         pCtTnlInfo, GMPLS_SEGMENT_FORWARD) != RPTE_SUCCESS)
                    {
                        RSVPTE_DBG (RSVPTE_FRR_DBG,
                                    "RESV: Mlib update is failed"
                                    "for the backup tnl\n");
                        continue;
                    }
                    TE_TNL_IFINDEX (RPTE_TE_TNL (pCtTnlInfo)) = RPTE_ZERO;
                    if ((RPTE_PKT_MAP_PKT_PATH (pPktMap) ==
                         RPTE_FRR_BACKUP_PATH)
                        &&
                        (RSVPTE_TNL_NODE_RELATION
                         (RPTE_FRR_BASE_TNL (pCtTnlInfo)) == RPTE_EGRESS))
                    {
                        /* Indicate the Tunnel application 
                         * to do the MP programming */

                        RSVPTE_DBG (RSVPTE_FRR_DBG,
                                    " Process Resv : TeSigNotifyFrrTnlMpHwIlmAddOrDel "
                                    "called\n");
                        TeSigNotifyFrrTnlMpHwIlmAddOrDel
                            (RPTE_TE_TNL (RPTE_FRR_BASE_TNL (pCtTnlInfo)),
                             RPTE_TE_TNL (pCtTnlInfo), TRUE);
                    }

                    RSVPTE_TNL_RSB (pCtTnlInfo)->u1FrrMlibUpdateReq
                        = RPTE_FRR_MLIB_UPDATE_TRUE;
                }
            }

            if (pPktMap->pLspTnlIfIdObj != NULL)
            {
                if (RSB_LSP_TNL_IF_ID (pRsb).u4RouterId == RPTE_ZERO)
                {
                    (RSB_LSP_TNL_IF_ID (pRsb)).u4RouterId =
                        OSIX_NTOHL (pPktMap->pLspTnlIfIdObj->LspTnlIfId.
                                    u4RouterId);
                    (RSB_LSP_TNL_IF_ID (pRsb)).u4InterfaceId =
                        OSIX_NTOHL (pPktMap->pLspTnlIfIdObj->LspTnlIfId.
                                    u4InterfaceId);
                    /* Intermediate Routers need to send ResvRefresh message when it
                     * receives the LSP_TNL_INTERFACE_ID object for the first time */
                    u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
                    pCtTnlInfo->u2HlspCType =
                        OSIX_NTOHS (pPktMap->pLspTnlIfIdObj->ObjHdr.
                                    u2ClassNumType);
                    if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
                    {
                        u1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;
                    }
                    if (((RSB_LSP_TNL_IF_ID (pRsb)).u4InterfaceId != RPTE_ZERO)
                        && ((RSB_LSP_TNL_IF_ID (pRsb)).u4RouterId != RPTE_ZERO))
                    {
                        if (RptePortCreateFATELink (pCtTnlInfo) == RPTE_FAILURE)
                        {
                            RSVPTE_DBG (RSVPTE_PH_PRCS,
                                        "RESV : Err - Create FA TE Link "
                                        "Failed\n");
                            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                        "RpteRhProcessResvMsg : "
                                        "INTMD-EXIT \n");
                        }
                    }
                }
            }
            else
            {
                if (RSB_LSP_TNL_IF_ID (pRsb).u4RouterId != RPTE_ZERO)
                {
                    u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
                    pCtTnlInfo->u2HlspCType = RPTE_ZERO;
                    if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
                    {
                        RpteDeleteFATeLink (pCtTnlInfo);
                        u1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;
                    }
                    RSB_LSP_TNL_IF_ID (pRsb).u4RouterId = RPTE_ZERO;
                    RSB_LSP_TNL_IF_ID (pRsb).u4InterfaceId = RPTE_ZERO;
                }
            }
        }
        else
        {
            /* New Resv mesg is received, create Rsb, RsbCmnHdr */
            if (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_BACKUP_TNL)
            {
                if ((PSB_RPTE_FRR_DETOUR (RSVPTE_TNL_PSB (pCtTnlInfo)).
                     au4PlrId[RPTE_ZERO]) != RPTE_ZERO)
                {
                    /* One to One case Path specific handle */
                    u4TmpVar =
                        OSIX_NTOHL (PSB_RPTE_FRR_DETOUR
                                    (RSVPTE_TNL_PSB (pCtTnlInfo)).
                                    au4PlrId[RPTE_ZERO]);
                    MEMCPY (&(pRsvpTeFilterSpec->TnlSndrAddr),
                            &u4TmpVar, RSVPTE_IPV4ADR_LEN);
                }
                else
                {
                    MEMCPY (&(pRsvpTeFilterSpec->TnlSndrAddr),
                            &(PSB_RPTE_SNDR_TMP (RSVPTE_TNL_PSB (pCtTnlInfo)).
                              TnlSndrAddr), RSVPTE_IPV4ADR_LEN);
                }
            }

            if (RpteRhCreateRsb
                (pRsvpTeFilterSpec, pPktMap, pIfEntry, &pRsb) == RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_RH_PRCS, "RESV: Creating a Rsb failed..\n");
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            " RpteRhProcessResvMsg : INTMD-EXIT\n");
                return;
            }
            RPTE_TNL_DSTR_NBR (pCtTnlInfo) = pNbrEntry;
            if (NBR_ENTRY_IF_ENTRY (pNbrEntry) != NULL)
            {
                (RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS (NBR_ENTRY_IF_ENTRY
                                                    (pNbrEntry)))++;
            }
            (NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry))++;
            RSVPTE_TNL_RSB (pCtTnlInfo) = pRsb;
            RSB_RPTE_TNL_INFO (pRsb) = pCtTnlInfo;
            u1New = RPTE_YES;
            /* 
             * Traffic controller is called with Flowspec values for updation.
             * If the Traffic controller updation succeeds the Resv mesg is 
             * forwarded towards the ingress node, else the error is sent 
             * to the Egress of the tunnel 
             */
            u1ResvRsrc = RPTE_TRUE;
            if (RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                            &u1ResvRefreshNeeded,
                                            &ErrorSpec) != RPTE_SUCCESS)
            {
                /* 
                 * If the TC updation fails then the Rsb created is removed
                 * & error is sent to the NextHop 
                 */
                RSVPTE_DBG (RSVPTE_RH_PRCS,
                            "RESV: Traffic Controller Updation failed - "
                            "Resv Err Sent\n");
                RpteUtlDeleteRsb (pRsb);
                RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
                RptePvmSendResvErr (pPktMap, ErrorSpec.u1ErrCode,
                                    ErrorSpec.u2ErrValue);
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RpteRhProcessResvMsg : INTMD-EXIT\n");
                return;

            }
            if (RpteRhDSUpdateTrafficControl (pCtTnlInfo,
                                              &u1ResvRefreshNeeded,
                                              &ErrorSpec) != RPTE_SUCCESS)
            {
                RpteRhDelTcParams (pCtTnlInfo);
                RptePmRemTnlFromPrioList (RSB_OUT_IF_ENTRY (pRsb),
                                          RSVPTE_TNL_HOLD_PRIO
                                          (RPTE_TE_TNL (pCtTnlInfo)),
                                          GMPLS_SEGMENT_FORWARD, pCtTnlInfo);

                RpteUtlDeleteRsb (pRsb);
                RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
                RptePvmSendResvErr (pPktMap, ADMISSION_CONTROL_FAILURE,
                                    DEFAULT_ERROR_VALUE);
                RSVPTE_DBG (RSVPTE_RH_PRCS,
                            "RESV: Getting a free Label from LblGroup failed "
                            "- Resv Err Sent\n");

                if ((RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) != NULL) &&
                    (TMO_SLL_Count (&RSVPTE_TNL_OUTARHOP_LIST
                                    (pCtTnlInfo)) != RSVPTE_ZERO))
                {
                    rpteTeDeleteArHopListInfo (RSVPTE_OUT_ARHOP_LIST_INFO
                                               (pCtTnlInfo));

                }
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RpteRhProcessResvMsg : INTMD-EXIT\n");
                return;

            }

            if (RptePortCfaCreateMplsTnlIf (pCtTnlInfo, RESV_MSG)
                == RPTE_FAILURE)
            {
                RpteUtlCleanPktMap (pPktMap);
                return;
            }
        }

        if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
        {
            if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) &
                 RPTE_DSTR_MSG_ID_PRESENT) &&
                ((INT4) RPTE_TNL_DSTR_MAX_MSG_ID (pCtTnlInfo) >=
                 (INT4) RPTE_PKT_MAP_MSG_ID (pPktMap)) &&
                (RPTE_TNL_DSTR_NBR (pCtTnlInfo) == pNbrEntry))
            {
                RSVPTE_DBG (RSVPTE_RH_PRCS,
                            "Msg ID Out Of Order. Sliently Dropping sshhh....\n");
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RptePhProcessPathMsg : INTMD-EXIT \n");
                return;
            }

            if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) &
                 RPTE_IN_RESV_MSG_ID_PRESENT) == RPTE_IN_RESV_MSG_ID_PRESENT)
            {
                /* Msg Id is present before. Releasing them from the Nbr
                 * msg id data base */
                RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (pNbrEntry);
                RpteKey.u8_Key[RPTE_ONE] = RPTE_TNL_IN_RESV_MSG_ID (pCtTnlInfo);
                RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     RPTE_NBR_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);

                /* NOTE : Since Incoming resv msg id is different, there is
                 * a change in the resv state. This change needs to be propogated
                 * to the upstream. So a different msg id is generated for the
                 * next outgoing resv msg of the tunnel. */
                if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) &
                     RPTE_OUT_RESV_MSG_ID_PRESENT) ==
                    RPTE_OUT_RESV_MSG_ID_PRESENT)
                {
                    RpteKey.u4_Key = RPTE_TNL_OUT_RESV_MSG_ID (pCtTnlInfo);
                    RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                         RPTE_LCL_MSG_ID_DB_PREFIX,
                                         RpteMIHReleaseTrieInfo);
                }
            }

            RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (pNbrEntry);
            RpteKey.u8_Key[RPTE_ONE] = RPTE_PKT_MAP_MSG_ID (pPktMap);

            pTrieInfo = (tuTrieInfo *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TRIE_INFO_POOL_ID);

            if (pTrieInfo == NULL)
            {
                /* Since the memory allocation is a failure,
                 * the msg id received in the resv msg is not
                 * stored in the neighbour msg id data base. */
                RSVPTE_DBG (RSVPTE_PH_MEM,
                            "Failed to allocate Trie Info "
                            "Not Storing the msg id received in resv msg.\n");
                RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) &=
                    RPTE_IN_RESV_MSG_ID_NOT_PRESENT;
            }
            else
            {
                MEMSET (pTrieInfo, RSVPTE_ZERO, sizeof (tuTrieInfo));
                RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo) = RPTE_RESV_INCOMING;
                RPTE_TRIE_INFO_TNL_INFO (pTrieInfo) = pCtTnlInfo;

                /* RBTree Addtion is only required for out path message id for
                 * SRefresh RecoveryPath message support.
                 * Its not required for in resv message id
                 * */

                if (RpteTrieAddEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                                      pTrieInfo) != RPTE_SUCCESS)
                {
                    MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                        (UINT1 *) pTrieInfo);
                    RPTE_TRIE_INFO_TNL_INFO (pTrieInfo) = NULL;
                    RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) &=
                        RPTE_IN_RESV_MSG_ID_NOT_PRESENT;
                }
                else
                {
                    /* Store the new message id into the tunnel info */
                    RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) |=
                        (RPTE_IN_RESV_MSG_ID_PRESENT |
                         RPTE_DSTR_MSG_ID_PRESENT);
                    RPTE_TNL_IN_RESV_MSG_ID (pCtTnlInfo) =
                        RPTE_PKT_MAP_MSG_ID (pPktMap);
                    RPTE_TNL_DSTR_MAX_MSG_ID (pCtTnlInfo) =
                        RPTE_PKT_MAP_MSG_ID (pPktMap);
                }
            }
        }

        /* Updating the Neighbour Entry in case of rerouting */
        if ((RPTE_TNL_DSTR_NBR (pCtTnlInfo) != NULL) &&
            (RPTE_TNL_DSTR_NBR (pCtTnlInfo) != pNbrEntry) &&
            (RSVPTE_TNL_FRR_FACILITY_METHOD !=
             RSVPTE_TE_TNL_FRR_PROT_METHOD (RPTE_TE_TNL (pCtTnlInfo)))
            && (u1IsMsgFromBkpTnl != RSVPTE_ONE))
        {
            (RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS (NBR_ENTRY_IF_ENTRY
                                                (RPTE_TNL_DSTR_NBR
                                                 (pCtTnlInfo))))--;
            (NBR_ENTRY_NBR_NUM_TNLS (RPTE_TNL_DSTR_NBR (pCtTnlInfo)))--;
            /* Downstream Neighbor is changed, so remove the tnl from old neighbor */
            TMO_DLL_Delete (&(RPTE_TNL_DSTR_NBR (pCtTnlInfo)->DnStrTnlList),
                            &(pCtTnlInfo->DnStrNbrTnl));
            RPTE_TNL_DSTR_NBR (pCtTnlInfo) = pNbrEntry;
            TMO_DLL_Add (&(RPTE_TNL_DSTR_NBR (pCtTnlInfo)->DnStrTnlList),
                         &(pCtTnlInfo->DnStrNbrTnl));

            RSB_OUT_IF_ENTRY (pRsb) = NBR_ENTRY_IF_ENTRY (pNbrEntry);
            if (NBR_ENTRY_IF_ENTRY (pNbrEntry) != NULL)
            {
                (RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS
                 (NBR_ENTRY_IF_ENTRY (pNbrEntry)))++;
            }
            (NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry))++;

            /* Since Change of Downstream neighbour is to be notified to all
             * other routers in the path, a new message id
             * should be generated.
             * So, if a message id exists it is removed
             */
            if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) &
                 RPTE_OUT_RESV_MSG_ID_PRESENT) == RPTE_OUT_RESV_MSG_ID_PRESENT)
            {
                RpteKey.u4_Key = RPTE_TNL_OUT_RESV_MSG_ID (pCtTnlInfo);
                RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                     &RpteKey,
                                     RPTE_LCL_MSG_ID_DB_PREFIX,
                                     RpteMIHReleaseTrieInfo);
            }

            if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) != RPTE_INGRESS)
            {
                u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
            }
        }
        if ((RPTE_IS_FRR_CAPABLE (pCtTnlInfo) != RPTE_TRUE) ||
            ((RPTE_IS_FRR_CAPABLE (pCtTnlInfo) == RPTE_TRUE) &&
             (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_PROTECTED_TNL)))
        {

            if ((u1ReCount == RSVPTE_ZERO)
                || ((u1ReRouteFlag == RPTE_YES) && (u1ReCount != RSVPTE_ZERO)))
            {
                if (RpteRhRroProc (pPktMap, pCtTnlInfo, &u1ResvRefreshNeeded) ==
                    RPTE_FAILURE)
                {
                    RpteUtlCleanPktMap (pPktMap);
                    RSVPTE_DBG (RSVPTE_RH_ETEXT,
                                "RpteRhProcessResvMsg : INTMD-EXIT\n");
                    return;
                }
            }
        }
        /* CONFIRM Object is mandatory */
        if (PKT_MAP_RESV_CONF_OBJ (pPktMap) == NULL)
        {
            RSVPTE_DBG (RSVPTE_RH_PRCS,
                        "RESV : Resv Mesg does not contain ResvConf Obj \n");
            MEMSET (&RSB_RESV_CONF (pRsb), RSVPTE_ZERO, sizeof (tResvConf));
            OsixGetSysTime ((tOsixSysTime *) & (RSB_LAST_CHANGE (pRsb)));
        }
        if (RSVP_HDR_SEND_TTL ((tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap))
            != PKT_MAP_RX_TTL (pPktMap))
        {
            RSB_NON_RSVP_FLAG (pRsb) = RPTE_YES;
        }

        if (RPTE_TNL_PSB_TIMER_TYPE (pCtTnlInfo) == RPTE_EXP_BACK_OFF_TIMER)
        {
            /* Resv received for an ack desired path msg.
             * Stopping the back off timer for the path msg */
            RPTE_TNL_PSB_TIMER_TYPE (pCtTnlInfo) = RPTE_NORMAL_TIMER;
            RPTE_TNL_PSB_REFRESH_INTERVAL (pCtTnlInfo) =
                RpteRandomDelay (IF_ENTRY_REFRESH_INTERVAL (PSB_OUT_IF_ENTRY
                                                            (RSVPTE_TNL_PSB
                                                             (pCtTnlInfo))) /
                                 DEFAULT_CONV_TO_SECONDS);
            RptePhStartPathRefreshTmr (RSVPTE_TNL_PSB (pCtTnlInfo));
        }

        RSVPTE_DBG5 (RSVPTE_FRR_DBG,
                     "Node state of Tunnel %d %d %x %x is: %x\n",
                     RSVPTE_TNL_TNLINDX (pCtTnlInfo),
                     RSVPTE_TNL_TNLINST (pCtTnlInfo),
                     OSIX_NTOHL (RSVPTE_TNL_INGRESS (pCtTnlInfo)),
                     OSIX_NTOHL (RSVPTE_TNL_EGRESS (pCtTnlInfo)),
                     u1FrrNodeState);

        /* FRR Backup Path Identification and Triggering Starts here...... */
        if ((RSVPTE_FRR_MAKE_AFTER_BREAK_ENABLED (gpRsvpTeGblInfo)
             == RPTE_SNMP_FALSE) &&
            (!(RPTE_IS_PLR (u1FrrNodeState))) &&
            (!(RPTE_IS_PLRAWAIT (u1FrrNodeState))) &&
            (RPTE_FRR_TNL_INFO_TYPE (pRpteBaseTnlInfo) ==
             RPTE_FRR_PROTECTED_TNL) &&
            (RPTE_IS_FRR_CAPABLE (pCtTnlInfo) == RPTE_TRUE) &&
            (RPTE_TE_TNL (pCtTnlInfo)->u1TnlLocalProtectInUse !=
             LOCAL_PROT_IN_USE))
        {
            RpteRhBackupPathTrigger (pCtTnlInfo);
        }

        /*
         * If new Rsb is created then call TC , LabelManager, FM to register the
         * tunnel parameters
         */
        if (u1New == RPTE_YES)
        {
            if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) != RPTE_INGRESS)
            {
                /* This is used to delay the setup of high priority tunnel
                 * which preempts the low priority tunnel at intermediate nodes */

                if ((u1ResvDsRefreshNeeded == RPTE_REFRESH_MSG_NO) &&
                    (u1ResvRefreshNeeded == RPTE_REFRESH_MSG_NO))
                {
                    RpteUtlDeleteRsb (pRsb);
                    return;
                }

                /* 
                 * Get a new Label from the LblManager that is used for 
                 * forwarding the message to the PrevHop 
                 */
                if ((pCtTnlInfo)->DownstrInLbl.u4GenLbl == MPLS_INVALID_LABEL)
                {
                    if (RpteRhGetLabel (PSB_IF_ENTRY (pPsb), &InLabel) ==
                        RPTE_FAILURE)
                    {
                        RpteDSRelTrafficControl (pCtTnlInfo);
                        RpteRhDelTcParams (pCtTnlInfo);
                        RptePmRemTnlFromPrioList (RSB_OUT_IF_ENTRY
                                                  (RSVPTE_TNL_RSB
                                                   (pCtTnlInfo)),
                                                  RSVPTE_TNL_HOLD_PRIO
                                                  (RPTE_TE_TNL
                                                   (pCtTnlInfo)),
                                                  GMPLS_SEGMENT_FORWARD,
                                                  pCtTnlInfo);

                        RpteUtlDeleteRsb (pRsb);
                        RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
                        RptePvmSendResvErr (pPktMap,
                                            ADMISSION_CONTROL_FAILURE,
                                            DEFAULT_ERROR_VALUE);
                        RSVPTE_DBG (RSVPTE_RH_PRCS,
                                    "RESV: Getting a free Label from LblGroup "
                                    "failed - Resv Err Sent\n");

                        if ((RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) !=
                             NULL)
                            &&
                            (TMO_SLL_Count
                             (&RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo)) !=
                             RSVPTE_ZERO))
                        {
                            rpteTeDeleteArHopListInfo
                                (RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo));

                        }
                        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                                    "RpteRhProcessResvMsg : INTMD-EXIT\n");
                        return;
                    }
                    MEMCPY (&RSVPTE_TNL_INLBL (pCtTnlInfo), &InLabel,
                            sizeof (uLabel));
                }
            }
            MEMCPY (&RSVPTE_TNL_OUTLBL (pCtTnlInfo), &OutLabel,
                    sizeof (uLabel));
            RSB_RPTE_TNL_INFO (pRsb) = pCtTnlInfo;
            /* 
             * MplsMlibUpdate is called with ILM_CREATE in all the
             * intermediate nodes as well as Egress of the tunnel
             * In the Ingress end of the tunnel it is called with
             * TNL_CREATE 
             */
            if ((RSVPTE_FRR_MAKE_AFTER_BREAK_ENABLED (gpRsvpTeGblInfo)
                 == RPTE_SNMP_TRUE) &&
                (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_BACKUP_TNL) &&
                (RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL) &&
                (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (RPTE_FRR_BASE_TNL
                                                   (pCtTnlInfo)))))
            {
                pRpteTmpTnlInfo = RPTE_FRR_BASE_TNL (pCtTnlInfo);
                /* One-to-One backup make after break handle */
                if (RPTE_IS_HEADEND (RPTE_FRR_NODE_STATE (pRpteTmpTnlInfo)))
                {
                    RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE, RSVPTE_ZERO,
                                              pRpteTmpTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);
                    RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE, RSVPTE_ZERO,
                                              pCtTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);
                    RpteCheckTnlFrrEntryInFTN (RPTE_TE_TNL (pRpteTmpTnlInfo),
                                               RSVPTE_TNL_INSTANCE (RPTE_TE_TNL
                                                                    (pCtTnlInfo)),
                                               MPLS_FALSE);
                }
                else
                {
                    RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                              pRpteTmpTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);
                    MEMCPY (&(RSVPTE_TNL_INLBL (pCtTnlInfo)),
                            &RSVPTE_TNL_INLBL (pRpteTmpTnlInfo),
                            sizeof (uLabel));
                    RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_CREATE,
                                              RSVPTE_ZERO, pCtTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);
                }
                RSVPTE_TNL_RRO_FLAGS (pRpteTmpTnlInfo) |= LOCAL_PROT_IN_USE;

                RpteUtlSetDetourStatus (pRpteTmpTnlInfo, RPTE_TRUE);
            }

            if ((u1IsMsgFromBkpTnl == RPTE_YES) &&
                (RPTE_IS_HEADEND_PLR (u1FrrNodeState)))
            {
                RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_TNL_CREATE,
                                          RSVPTE_ZERO, pCtTnlInfo,
                                          GMPLS_SEGMENT_FORWARD);
                RSVPTE_DBG1 (RSVPTE_FRR_DBG,
                             "\n Headend + PLR backup tnl interface created XC Index=%d\n",
                             RPTE_TE_TNL (pCtTnlInfo)->u4TnlXcIndex);
            }
            else if ((u1IsMsgFromBkpTnl == RPTE_YES) &&
                     (RPTE_IS_PLR (u1FrrNodeState)))
            {
                /* Label programming for backup tunnel is not done when node
                 * state is PLR and protected tunnel is still up. But operational
                 * status of the backup tunnel should be made UP. */
                rpteTeSetTnlOperStatus (pCtTnlInfo->pTeTnlInfo, TE_OPER_UP);
            }

            if ((u1IsMsgFromBkpTnl == RPTE_NO) &&
                (((RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
                  && (RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_TNL_CREATE,
                                                RSVPTE_ZERO, pCtTnlInfo,
                                                GMPLS_SEGMENT_FORWARD)
                      != RPTE_SUCCESS)) ||
                 ((RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) != RPTE_INGRESS)
                  && (RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_CREATE,
                                                RSVPTE_ZERO, pCtTnlInfo,
                                                GMPLS_SEGMENT_FORWARD)
                      != RPTE_SUCCESS))))
            {
                RSVPTE_DBG (RSVPTE_RH_PRCS,
                            "RESV: MplsMlibUpdate of MLIB_ILM/TNL_CREATE"
                            " failed..\n");
                /* 
                 * MlibUpdate failed, the associated Rsb,Tcsb are deleted,
                 * Release the Reserved resources & send a ResvErr 
                 */
                RpteDSRelTrafficControl (pCtTnlInfo);
                RpteRhDelTcParams (pCtTnlInfo);
                RptePmRemTnlFromPrioList (RSB_OUT_IF_ENTRY (pRsb),
                                          RSVPTE_TNL_HOLD_PRIO
                                          (RPTE_TE_TNL (pCtTnlInfo)),
                                          GMPLS_SEGMENT_FORWARD, pCtTnlInfo);
                RpteUtlDeleteRsb (pRsb);

                RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
                RptePvmSendResvErr (pPktMap, ADMISSION_CONTROL_FAILURE,
                                    DEFAULT_ERROR_VALUE);

                if (RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) != NULL)
                {
                    if (TMO_SLL_Count
                        (&RSVPTE_TNL_OUTARHOP_LIST (pCtTnlInfo)) != RSVPTE_ZERO)
                    {
                        rpteTeDeleteArHopListInfo
                            (RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo));
                    }
                }
                if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) != RPTE_INGRESS)
                {
                    RpteRhFreeLabel (PSB_IF_ENTRY (pPsb), &InLabel);
                }
                MEMSET (&RSVPTE_TNL_INLBL (pCtTnlInfo), RSVPTE_ZERO,
                        sizeof (uLabel));
                RSVPTE_DBG (RSVPTE_RH_ETEXT,
                            "RpteRhProcessResvMsg : INTMD-EXIT\n");
                return;
            }

            if ((pCtTnlInfo->pTeTnlInfo->u1TnlPathType ==
                 RPTE_TNL_PROTECTION_PATH)
                && (pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                    MPLS_TE_DEDICATED_ONE2ONE)
                && (pCtTnlInfo->pTeTnlInfo->pTeBackupPathInfo != NULL)
                && (pCtTnlInfo->pMapTnlInfo != NULL))
            {
                if (pCtTnlInfo->pMapTnlInfo->pPsb != NULL)
                {
                    pCtTnlInfo->pMapTnlInfo->pPsb->Association.u2AssocID =
                        (UINT2) pCtTnlInfo->pTeTnlInfo->u4TnlInstance;
                    RptePhPathRefresh (pCtTnlInfo->pMapTnlInfo);
                }
                pCtTnlInfo->u4RsrcClassColor =
                    pCtTnlInfo->pMapTnlInfo->u4RsrcClassColor;
            }

            CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID
                                (pCtTnlInfo->pTeTnlInfo), u4TunnelIngressLSRId);
            u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
            CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID
                                (pCtTnlInfo->pTeTnlInfo), u4TunnelEgressLSRId);
            u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);

            pInstance0Tnl =
                rpteTeGetTunnelInfo (TE_TNL_TNL_INDEX (pCtTnlInfo->pTeTnlInfo),
                                     RPTE_ZERO, u4TunnelIngressLSRId,
                                     u4TunnelEgressLSRId);
            if ((pCtTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_WORKING_PATH)
                && (pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                    MPLS_TE_DEDICATED_ONE2ONE)
                && (pCtTnlInfo->pTeTnlInfo->pTeBackupPathInfo != NULL)
                && (pCtTnlInfo->pMapTnlInfo == NULL) && (pInstance0Tnl != NULL)
                && (pInstance0Tnl->u1BackupUp == TRUE)
                && (pInstance0Tnl->u1WorkingUp == TRUE))
            {
                if (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                            TE_TNL_TNL_INDEX (pCtTnlInfo->
                                                              pTeTnlInfo),
                                            pInstance0Tnl->u4BkpTnlInstance,
                                            u4TunnelIngressLSRId,
                                            u4TunnelEgressLSRId,
                                            &pRsvpTeBackTnlInfo) ==
                    RPTE_SUCCESS)
                {
                    pRsvpTeBackTnlInfo->pMapTnlInfo = pCtTnlInfo;
                    pCtTnlInfo->pMapTnlInfo = pRsvpTeBackTnlInfo;
                    pCtTnlInfo->pTeTnlInfo->u4BkpTnlIndex =
                        TE_TNL_TNL_INDEX (pRsvpTeBackTnlInfo->pTeTnlInfo);
                    pCtTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
                        pRsvpTeBackTnlInfo->pTeTnlInfo->u4TnlInstance;

                    pRsvpTeBackTnlInfo->pTeTnlInfo->u4BkpTnlIndex =
                        TE_TNL_TNL_INDEX (pCtTnlInfo->pTeTnlInfo);
                    pRsvpTeBackTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
                        pCtTnlInfo->pTeTnlInfo->u4TnlInstance;

                    pRsvpTeBackTnlInfo->pTeTnlInfo->u4BkpTnlIndex =
                        TE_TNL_TNL_INDEX (pCtTnlInfo->pTeTnlInfo);
                    pRsvpTeBackTnlInfo->pTeTnlInfo->u4BkpTnlInstance =
                        pCtTnlInfo->pTeTnlInfo->u4TnlInstance;

                    MEMCPY (pCtTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                            &RSVPTE_TNL_INGRESS_RTR_ID (pRsvpTeBackTnlInfo->
                                                        pTeTnlInfo),
                            ROUTER_ID_LENGTH);
                    MEMCPY (pCtTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                            &RSVPTE_TNL_EGRESS_RTR_ID (pRsvpTeBackTnlInfo->
                                                       pTeTnlInfo),
                            ROUTER_ID_LENGTH);

                    MEMCPY (pRsvpTeBackTnlInfo->pTeTnlInfo->BkpTnlIngressLsrId,
                            &RSVPTE_TNL_INGRESS_RTR_ID (pCtTnlInfo->pTeTnlInfo),
                            ROUTER_ID_LENGTH);
                    MEMCPY (pRsvpTeBackTnlInfo->pTeTnlInfo->BkpTnlEgressLsrId,
                            &RSVPTE_TNL_EGRESS_RTR_ID (pCtTnlInfo->pTeTnlInfo),
                            ROUTER_ID_LENGTH);

                    if (pCtTnlInfo->pPsb != NULL)
                    {
                        pCtTnlInfo->pPsb->Association.u2AssocID =
                            (UINT2) pRsvpTeBackTnlInfo->pTeTnlInfo->
                            u4TnlInstance;
                        RptePhPathRefresh (pCtTnlInfo);
                    }
                    pRsvpTeBackTnlInfo->u4RsrcClassColor =
                        pCtTnlInfo->u4RsrcClassColor;

                    /*RpteUtlSwitchTraffic(pCtTnlInfo, (UINT1)RPTE_PROTECTION_SWITCH_BACK); */
                }
            }
            if (!TMO_DLL_Is_Node_In_List (&(pCtTnlInfo->DnStrNbrTnl)))
            {
                TMO_DLL_Init_Node (&(pCtTnlInfo->DnStrNbrTnl));
                TMO_DLL_Add (&(pNbrEntry->DnStrTnlList),
                             &(pCtTnlInfo->DnStrNbrTnl));

                RSVPTE_DBG5 (RSVPTE_RH_PRCS,
                             "Tunnel %d %d %x %x added in Nbr %x list\n",
                             RSVPTE_TNL_TNLINDX (pCtTnlInfo),
                             RSVPTE_TNL_TNLINST (pCtTnlInfo),
                             OSIX_NTOHL (RSVPTE_TNL_INGRESS (pCtTnlInfo)),
                             OSIX_NTOHL (RSVPTE_TNL_EGRESS (pCtTnlInfo)),
                             NBR_ENTRY_ADDR (pNbrEntry));
            }

            if ((pCtTnlInfo->pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP) &&
                (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS))
            {
                RptePhPathRefresh (pCtTnlInfo);
            }

        }

        u1ReCount++;
    }
    while (((pCtTnlInfo = RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)) != NULL) &&
           ((RPTE_IS_MP (u1FrrNodeState) || RPTE_IS_DMP (u1FrrNodeState))));

    pCtTnlInfo = pRpteBaseTnlInfo;

    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable == RPTE_ENABLED)
    {
        if ((pPktMap->pAdminStatusObj != NULL) &&
            (pCtTnlInfo->pRsb->u4AdminStatus !=
             OSIX_NTOHL (pPktMap->pAdminStatusObj->AdminStatus.u4AdminStatus)))
        {
            pCtTnlInfo->pRsb->u4AdminStatus =
                OSIX_NTOHL (pPktMap->pAdminStatusObj->AdminStatus.
                            u4AdminStatus);
            pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus =
                pCtTnlInfo->pRsb->u4AdminStatus;

            /* If it is ingress node and D bit is set, stop the graceful deletion 
             * timer and send path tear message */

            if ((pCtTnlInfo->pTeTnlInfo->u1TnlRole == RPTE_INGRESS) &&
                (pCtTnlInfo->pRsb->
                 u4AdminStatus & GMPLS_ADMIN_DELETE_IN_PROGRESS))
            {
                pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.u4AdminStatus &=
                    (~GMPLS_ADMIN_DELETE_IN_PROGRESS);
                TmrStopTimer (RSVP_GBL_TIMER_LIST,
                              &pCtTnlInfo->AdminStatusTimer);
                RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pCtTnlInfo;
                RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
                return;
            }
            else if (pCtTnlInfo->pTeTnlInfo->u1TnlRole != RPTE_INGRESS)
            {
                /* If Admin status is changed, resv message has to be sent 
                 * immediately at intermediate nodes */
                u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
            }
        }
        else if (pPktMap->pAdminStatusObj == NULL)
        {
            if (pCtTnlInfo->pRsb->u4AdminStatus != GMPLS_ADMIN_UNKNOWN)
            {
                pCtTnlInfo->pRsb->u4AdminStatus = GMPLS_ADMIN_UNKNOWN;

                if (pCtTnlInfo->pTeTnlInfo->u1TnlRole != RPTE_INGRESS)
                {
                    u1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
                }
            }
        }
    }

    /* If u1ResvRefreshNeeded flag is set by the TC then the mesg 
     * is forwarded */
    if (RPTE_IS_DNLOST (u1FrrNodeState) ||
        ((RSVPTE_TNL_RRO_FLAGS (pRpteBaseTnlInfo) &
          LOCAL_PROT_IN_USE) == LOCAL_PROT_IN_USE))
    {
        do
        {
            /* Backup Tunnel's reserve message */
            if (RPTE_IS_DMP (u1FrrNodeState))
            {
                if (u1DetourGrpId != RPTE_FRR_DETOUR_GRP_ID (pCtTnlInfo))
                {
                    continue;
                }
            }
            pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
            if (pRsb == NULL)
            {
                continue;
            }
            pTimeValues = &TIME_VALUES_OBJ (PKT_MAP_TIME_VALUES_OBJ (pPktMap));
            u4RsbTimeToDie =
                RpteUtlComputeLifeTime (PKT_MAP_IF_ENTRY (pPktMap),
                                        OSIX_NTOHL (TIME_VALUES_PERIOD
                                                    (pTimeValues)));
            RSB_TIME_TO_DIE (pRsb) = u4RsbTimeToDie;
            RSB_IN_REFRESH_INTERVAL (pRsb) = OSIX_NTOHL
                (TIME_VALUES_PERIOD (pTimeValues));
        }
        while ((pCtTnlInfo = RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)) != NULL);
    }
    pCtTnlInfo = pRpteBaseTnlInfo;
    if (u1ResvRefreshNeeded == RPTE_REFRESH_MSG_YES)
    {
        /* NOTE : Since ResvRefreshNeeded flag is true, there is a change in
         * the resv state. This change needs to be propogated to the
         * downstream. So a different msg id is generated for the next
         * outgoing resv msg of the tunnel. */
        if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) &
             RPTE_OUT_RESV_MSG_ID_PRESENT) == RPTE_OUT_RESV_MSG_ID_PRESENT)
        {
            RpteKey.u4_Key = RPTE_TNL_OUT_RESV_MSG_ID (pCtTnlInfo);
            RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                 RPTE_LCL_MSG_ID_DB_PREFIX,
                                 RpteMIHReleaseTrieInfo);
        }
        /* Protected tunnel reserve message */
        if (u1IsMsgFromBkpTnl != RPTE_YES)
        {
            RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
            RpteRhResvRefresh (pCtTnlInfo);
        }
        while ((pCtTnlInfo = RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)) != NULL)
        {
            /* Backup Tunnel's reserve message */
            if (RPTE_IS_DMP (u1FrrNodeState))
            {
                if (u1DetourGrpId != RPTE_FRR_DETOUR_GRP_ID (pCtTnlInfo))
                {
                    continue;
                }
            }
            RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
            RpteRhResvRefresh (pCtTnlInfo);
        }
    }
    pCtTnlInfo = pOrgCtTnlInfo;

    /* FRR Headend behavior */
    if ((RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) == RPTE_FRR_BACKUP_TNL) &&
        (RPTE_FRR_BASE_TNL (pCtTnlInfo) != NULL) &&
        (RPTE_FRR_TNL_INFO_TYPE (RPTE_FRR_BASE_TNL (pCtTnlInfo))
         == RPTE_FRR_PROTECTED_TNL))
    {
        if ((((RSVPTE_TNL_RRO_FLAGS (RPTE_FRR_BASE_TNL (pCtTnlInfo))) &
              LOCAL_PROT_IN_USE) == LOCAL_PROT_IN_USE) &&
            (RSVPTE_TNL_NODE_RELATION (RPTE_FRR_BASE_TNL (pCtTnlInfo)) ==
             RPTE_INGRESS))
        {
            RpteRhPhStartMaxWaitTmr (RPTE_FRR_BASE_TNL (pCtTnlInfo),
                                     u4RsbTimeToDie);
        }
    }

    if (((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRpteBaseTnlInfo)).
          u1Flags) == RSVPTE_TNL_FRR_ONE2ONE_METHOD) &&
        (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) ==
         RPTE_FRR_PROTECTED_TNL) &&
        (RSVPTE_FRR_REVERTIVE_MODE (gpRsvpTeGblInfo) ==
         RPTE_FRR_REVERTIVE_LOCAL) &&
        (((RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo)) &
          LOCAL_PROT_IN_USE) == LOCAL_PROT_IN_USE))
    {
        /* FRR 1:1 link/Node protection local revertive handle */
        RpteFrrHandlePlrLocRev (pCtTnlInfo);
    }

    RSVPTE_DBG4 (RSVPTE_RH_PRCS, "RESV: PSB_RPTE_FRR_FAST_REROUTE "
                 "(RSVPTE_TNL_PSB (pRpteBaseTnlInfo)).u1Flags: %u,"
                 " RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo): %d,"
                 " RSVPTE_FRR_REVERTIVE_MODE (gpRsvpTeGblInfo): %d,"
                 " RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pCtTnlInfo)): %d\n",
                 PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRpteBaseTnlInfo)).
                 u1Flags, RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo),
                 RSVPTE_FRR_REVERTIVE_MODE (gpRsvpTeGblInfo),
                 RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pCtTnlInfo)));

    RSVPTE_DBG2 (RSVPTE_RH_PRCS, "SVPTE_TNL_RRO_FLAGS (pCtTnlInfo)): %d, "
                 "pCtTnlInfo->b1IsReversionTriggered: %d\n",
                 RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo),
                 pCtTnlInfo->b1IsReversionTriggered);

    if (((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRpteBaseTnlInfo)).
          u1Flags) == TE_TNL_FRR_FACILITY_METHOD) &&
        (RPTE_FRR_TNL_INFO_TYPE (pCtTnlInfo) ==
         RPTE_FRR_PROTECTED_TNL) &&
        (RSVPTE_FRR_REVERTIVE_MODE (gpRsvpTeGblInfo) ==
         RPTE_FRR_REVERTIVE_LOCAL) &&
        (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pCtTnlInfo))))
    {
        if (((~(RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo)) &
              LOCAL_PROT_IN_USE) == LOCAL_PROT_IN_USE)
            && (RPTE_TRUE == pCtTnlInfo->b1IsReversionTriggered))
        {
            /* FRR Facility Method link/Node protection local revertive handle */
            RpteFrrHandlePlrLocRev (pCtTnlInfo);
            pCtTnlInfo->b1IsReversionTriggered = RPTE_FALSE;
        }

    }

    if ((u1IsMsgFromBkpTnl == RPTE_YES) ||
        (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS))
    {
        /*Resv message for working tunnel is received in backup
         * tunnel for facility backup method. 
         * Hence working tunnel's timer needs to be refreshed here
         * */
        if ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRpteBaseTnlInfo)).
             u1Flags) == RSVPTE_TNL_FRR_FACILITY_METHOD)
        {
            pCtTnlInfo = pRpteBaseTnlInfo;
        }

        RpteRhPhStartMaxWaitTmr (pCtTnlInfo, u4RsbTimeToDie);
        if (PKT_MAP_RESV_CONF_OBJ (pPktMap) != NULL)
        {
            if (RPTE_IS_FRR_CAPABLE (pCtTnlInfo) == RPTE_TRUE)
            {
                if (((RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) & LOCAL_PROT_IN_USE) ==
                     LOCAL_PROT_IN_USE)
                    &&
                    ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pCtTnlInfo)).
                      u1Flags) == RSVPTE_TNL_FRR_FACILITY_METHOD))
                {
                    pCtTnlInfo->u1SendToByPassTnl = RPTE_TRUE;
                }
            }
            RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
            RpteRchSendResvConf (pCtTnlInfo);
        }
    }

    if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
    {
        /* Check for the global revertive behavior created tunnel */
        /* Subtrct the instance by one and check that rsvp te tnlinfo is protected tnl 
         * then tear down the protected tnl make this tunnel up */
        if (RPTE_TE_TNL (pCtTnlInfo)->u4OrgTnlInstance != RPTE_ZERO)
        {
            CONVERT_TO_INTEGER (PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap),
                                u4ExtnTnlId);
            CONVERT_TO_INTEGER (PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap),
                                u4DestAddrId);
            (void) RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                           OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID
                                                       (pPktMap)),
                                           RPTE_TE_TNL (pCtTnlInfo)->
                                           u4OrgTnlInstance,
                                           OSIX_NTOHL (u4ExtnTnlId),
                                           OSIX_NTOHL (u4DestAddrId),
                                           &pCtProtTnlInfo);
        }
        if ((pCtProtTnlInfo != NULL)
            && (RPTE_IS_FRR_CAPABLE (pCtProtTnlInfo) == RPTE_TRUE)
            && (RSVPTE_TNL_GBL_REVERT_FLAG (pCtProtTnlInfo) == RPTE_TRUE))
        {
            TeSigHandleGblRevTnlUp (RPTE_TE_TNL (pCtProtTnlInfo),
                                    RPTE_TE_TNL (pCtTnlInfo));
            RptePhGeneratePathTear (pCtProtTnlInfo);
            /* Delete the existing Tnl Info from the Tnl Table */
            RPTE_TNL_DEL_TNL_FLAG (pCtProtTnlInfo) = RPTE_TRUE;
            RPTE_TE_TNL (pCtProtTnlInfo)->u1FrrGblRevertTnlFlag = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtProtTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
        }
    }

    /* Once the GR receives resv message from the helper, reset the GrTnl flag */

    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pCtTnlInfo->u1IsGrTnl == RPTE_YES))
    {
        pCtTnlInfo->u1IsGrTnl = RPTE_NO;
    }
    RpteUtlCleanPktMap (pPktMap);
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhProcessResvMsg : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteFrrMPBypassTnlIdentification                       */
/* Description     : This function processes Searches for best backup       */
/*                   tunnel and associates it with that of the protected    */
/*                   tunnel. Used in case of facility backup method.        */
/* Input (s)       : ppCtTnlInfo - Pointer to RsvpTeTnlInfo                 */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteFrrMPBypassTnlIdentification (tRsvpTeTnlInfo ** ppCtTnlInfo)
{
    tRsvpTeArHopInfo   *pTnlArHopInfo = NULL;
    tRsvpTeTnlInfo     *pCtTnlInfo = *ppCtTnlInfo;
    UINT4               u4StrictLinkAddr = RPTE_ZERO;
    UINT4               u4StrictNodeAddr = RPTE_ZERO;
    UINT1               u1ProtFlag = RPTE_ZERO;
    BOOL1               bIsNodeProtFailed = RPTE_NO;

    if (RSVPTE_IN_ARHOP_LIST_INFO (pCtTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                    "RpteFrrBypassTnlIdentification :INTMD EXIT\n");

        return;
    }
    if (RpteUtlIsNodeProtDesired (pCtTnlInfo) == RPTE_YES)
    {
        u1ProtFlag = RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG;
    }
    else
    {
        u1ProtFlag = RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG;
    }

    /* Link protection */
    pTnlArHopInfo = (tRsvpTeArHopInfo *)
        (TMO_SLL_First (&RSVPTE_IN_ARHOP_LIST (RPTE_TE_TNL (pCtTnlInfo))));
    if (pTnlArHopInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                    "RpteFrrBypassTnlIdentification :INTMD EXIT\n");

        return;
    }
    CONVERT_TO_INTEGER (pTnlArHopInfo->IpAddr.au1Ipv4Addr, u4StrictLinkAddr);
    u4StrictLinkAddr = OSIX_NTOHL (u4StrictLinkAddr);

    if (u1ProtFlag == RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG)
    {
        /* Node protection */
        pTnlArHopInfo =
            (tRsvpTeArHopInfo *) TMO_SLL_Next (&RSVPTE_IN_ARHOP_LIST
                                               (RPTE_TE_TNL
                                                (pCtTnlInfo)),
                                               (&(pTnlArHopInfo->NextHop)));

        /* Because this node is the penultimate hop,
         * node protection cannot be provided... */
        if (pTnlArHopInfo == NULL)
        {
            /* Atleast providing the link protection */
            bIsNodeProtFailed = RPTE_YES;
        }
        else
        {
            CONVERT_TO_INTEGER (pTnlArHopInfo->IpAddr.au1Ipv4Addr,
                                u4StrictNodeAddr);
            u4StrictNodeAddr = OSIX_NTOHL (u4StrictNodeAddr);
            if (RpteFrrMPBypassTnlSearch (u4StrictNodeAddr, &pCtTnlInfo) ==
                RPTE_FAILURE)
            {
                bIsNodeProtFailed = RPTE_YES;
            }
        }
    }
    if ((bIsNodeProtFailed == RPTE_YES)
        || (u1ProtFlag == RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG))
    {
        if (RpteFrrMPBypassTnlSearch (u4StrictLinkAddr, &pCtTnlInfo) ==
            RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_RH_ETEXT,
                        "RpteFrrMPBypassTnlIdentification :"
                        "RpteFrrMPBypassTnlSearch failed INTMD EXIT\n");
            return;
        }
    }
    if (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo) != NULL)
    {
        RPTE_FRR_STACK_BIT (pCtTnlInfo) = RPTE_TRUE;
        RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_CREATE,
                                  RSVPTE_ZERO, pCtTnlInfo,
                                  GMPLS_SEGMENT_FORWARD);
        RPTE_FRR_STACK_BIT (pCtTnlInfo) = RPTE_FALSE;
        /* Facility backup handling TODO */
        /* Indicate the Tunnel application to do the MP programming */
        TeSigNotifyFrrTnlMpHwIlmAddOrDel
            (RPTE_TE_TNL (pCtTnlInfo),
             RPTE_TE_TNL (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)), TRUE);
        RPTE_FRR_NODE_STATE (pCtTnlInfo) |= RPTE_FRR_NODE_STATE_MP;

        if (TMO_DLL_Is_Node_In_List
            ((tTMO_DLL_NODE *) & (pCtTnlInfo->FacInFrrProtTnl)) == RPTE_ZERO)
        {
            TMO_DLL_Init_Node ((tTMO_DLL_NODE *) &
                               (pCtTnlInfo->FacInFrrProtTnl));
            TMO_DLL_Add ((tTMO_DLL *) &
                         (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo)->FacInFrrList),
                         (tTMO_DLL_NODE *) & (pCtTnlInfo->FacInFrrProtTnl));
        }

    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteFrrBypassTnlIdentification : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteFrrBypassTnlIdentification                         */
/* Description     : This function processes Searches for best backup       */
/*                   tunnel and associates it with that of the protected    */
/*                   tunnel. Used in case of facility backup method.        */
/* Input (s)       : ppCtTnlInfo - Pointer to RsvpTeTnlInfo                 */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteFrrBypassTnlIdentification (tRsvpTeTnlInfo ** ppCtTnlInfo)
{
    tRsvpTeArHopInfo   *pTnlArHopInfo = NULL;
    tRsvpTeTnlInfo     *pCtTnlInfo = *ppCtTnlInfo;
    UINT4               u4StrictNodeAddr = RPTE_ZERO;
    UINT4               u4StrictLinkAddr = RPTE_ZERO;
    BOOL1               bIsNodeProtFailed = RPTE_NO;
    UINT1               u1ProtFlag = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteFrrBypassTnlIdentification : ENTRY\n");

    if (RSVPTE_OUT_ARHOP_LIST_INFO (pCtTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_FRR_DBG,
                    "RpteFrrBypassTnlIdentification :INTMD EXIT\n");
        return;
    }

    if (RpteUtlIsNodeProtDesired (pCtTnlInfo) == RPTE_YES)
    {
        u1ProtFlag = RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG;
    }
    else
    {
        u1ProtFlag = RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG;
    }

    /* Link protection */
    pTnlArHopInfo = (tRsvpTeArHopInfo *)
        (TMO_SLL_First (&RSVPTE_OUT_ARHOP_LIST (RPTE_TE_TNL (pCtTnlInfo))));
    if (pTnlArHopInfo == NULL)
    {
        RSVPTE_DBG (RSVPTE_FRR_DBG,
                    "RpteFrrBypassTnlIdentification :INTMD EXIT\n");
        return;
    }
    CONVERT_TO_INTEGER (pTnlArHopInfo->IpAddr.au1Ipv4Addr, u4StrictLinkAddr);
    u4StrictLinkAddr = OSIX_NTOHL (u4StrictLinkAddr);

    if (u1ProtFlag == RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG)
    {
        /* Node protection */
        pTnlArHopInfo =
            (tRsvpTeArHopInfo *) TMO_SLL_Next (&RSVPTE_OUT_ARHOP_LIST
                                               (RPTE_TE_TNL
                                                (pCtTnlInfo)),
                                               (&(pTnlArHopInfo->NextHop)));

        /* Because this node is the penultimate hop,
         * node protection cannot be provided... */
        if (pTnlArHopInfo == NULL)
        {
            /* Atleast providing the link protection */
            bIsNodeProtFailed = RPTE_YES;
        }
        else
        {
            CONVERT_TO_INTEGER (pTnlArHopInfo->IpAddr.au1Ipv4Addr,
                                u4StrictNodeAddr);
            u4StrictNodeAddr = OSIX_NTOHL (u4StrictNodeAddr);
            if (RpteFrrBypassTnlSearch (u4StrictNodeAddr, &pCtTnlInfo) ==
                RPTE_FAILURE)
            {
                bIsNodeProtFailed = RPTE_YES;
            }
            else
            {
                RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) |= LOCAL_PROT_AVAIL;
                RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) |= FRR_NODE_PROT;
            }
        }
    }
    if ((bIsNodeProtFailed == RPTE_YES)
        || (u1ProtFlag == RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG))
    {
        if (RpteFrrBypassTnlSearch (u4StrictLinkAddr, &pCtTnlInfo) ==
            RPTE_SUCCESS)
        {
            RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) |= LOCAL_PROT_AVAIL;
        }
        else
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RpteFrrBypassTnlIdentification :INTMD EXIT\n");
            return;
        }
    }
    RPTE_FRR_NODE_STATE (pCtTnlInfo) |= RPTE_FRR_NODE_STATE_PLR;
    pCtTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_AVAIL;

    if (RSVPTE_FRR_MAKE_AFTER_BREAK_ENABLED (gpRsvpTeGblInfo) == RPTE_SNMP_TRUE)
    {
        if (RPTE_IS_HEADEND (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
        {
            RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE, RSVPTE_ZERO,
                                      pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
            RPTE_FRR_STACK_BIT (pCtTnlInfo) = RPTE_TRUE;
            RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE, RSVPTE_ZERO,
                                      pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
            RpteStartFrrGblRevertTimer (pCtTnlInfo);
        }
        else
        {
            RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                      pCtTnlInfo, GMPLS_SEGMENT_FORWARD);

            RPTE_FRR_STACK_BIT (pCtTnlInfo) = RPTE_TRUE;
            RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_CREATE,
                                      RSVPTE_ZERO, pCtTnlInfo,
                                      GMPLS_SEGMENT_FORWARD);
        }
        RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) |= LOCAL_PROT_IN_USE;
    }

    /* Add the protected tnl info in the backup DLL so that when this backup tnl
     * goes down we need to delete the protected tnl also, if the protected tnl actively
     * using the backup tnl for the control and data traffic */
    if (TMO_DLL_Is_Node_In_List
        ((tTMO_DLL_NODE *) & (pCtTnlInfo->FacFrrProtTnl)) == RPTE_ZERO)
    {
        TMO_DLL_Init_Node ((tTMO_DLL_NODE *) & (pCtTnlInfo->FacFrrProtTnl));
        TMO_DLL_Add ((tTMO_DLL *) &
                     (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo)->FacFrrList),
                     (tTMO_DLL_NODE *) & (pCtTnlInfo->FacFrrProtTnl));
    }
    /* Copying the Backup Tunnel Indices to Protected Tunnel Table to
     * display.  */
    RSVPTE_TE_TNL_FRR_BKP_TNL_INDEX (RPTE_TE_TNL (pCtTnlInfo))
        = RSVPTE_TNL_INDEX (RPTE_TE_TNL (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo)));
    RSVPTE_TE_TNL_FRR_BKP_TNL_INST (RPTE_TE_TNL (pCtTnlInfo))
        = RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (RPTE_FRR_OUT_TNL_INFO
                                            (pCtTnlInfo)));

    MEMCPY ((UINT1 *) &(RSVPTE_TE_TNL_FRR_BKP_TNL_INGRESS_ID
                        (RPTE_TE_TNL (pCtTnlInfo))),
            (UINT1 *) &(RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL
                                                   (RPTE_FRR_OUT_TNL_INFO
                                                    (pCtTnlInfo)))),
            ROUTER_ID_LENGTH);
    MEMCPY ((UINT1 *) &(RSVPTE_TE_TNL_FRR_BKP_TNL_EGRESS_ID (RPTE_TE_TNL
                                                             (pCtTnlInfo))),
            (UINT1 *) &(RSVPTE_TNL_EGRESS_RTR_ID (RPTE_TE_TNL
                                                  (RPTE_FRR_OUT_TNL_INFO
                                                   (pCtTnlInfo)))),
            ROUTER_ID_LENGTH);

    /* Copying Protected Interfaces */
    RSVPTE_TNL_PROT_IF_INDEX (RPTE_TE_TNL (pCtTnlInfo))
        = IF_ENTRY_IF_INDEX (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pCtTnlInfo)));

    /* Setting Protection Type Values... */
    if ((RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) & FRR_NODE_PROT) == FRR_NODE_PROT)
    {
        RSVPTE_TE_TNL_FRR_PROT_TYPE (RPTE_TE_TNL (pCtTnlInfo))
            = TE_TNL_FRR_PROT_NODE;
    }
    else
    {
        RSVPTE_TE_TNL_FRR_PROT_TYPE (RPTE_TE_TNL (pCtTnlInfo))
            = TE_TNL_FRR_PROT_LINK;
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteFrrBypassTnlIdentification : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhBackupPathTrigger                                */
/* Description     : This function determines which backup method to be     */
/*                   used to create backup tunnel and calls CSPF            */
/*                   Computation in case of One-to-One Backup method or     */
/*                   calls Bypass Tunnel Identification in case of Faciltiy */
/*                   Backup Method.                                         */
/* Input (s)       : pCtTnlInfo - Pointer to RsvpTeTnlInfo                  */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhBackupPathTrigger (tRsvpTeTnlInfo * pCtTnlInfo)
{
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhBackupPathTrigger : ENTRY\n");

    /* Because the CSPF computation has already started, 
     * skipping silently. */
    if (RPTE_FRR_CSPF_INFO (pCtTnlInfo) != RPTE_FRR_CSPF_NO_START)
    {
        RSVPTE_DBG (RSVPTE_FRR_DBG,
                    "RESV: Resv refresh is received on protected tnl and "
                    "CSPF computation has initiated......... "
                    "Skipping silently.\n");
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhBackupPathTrigger : EXIT\n");
        return;
    }

    /* RFC 4090 sec. 6.2 Procedures for backup Path Computation */
    pRsvpTeFastReroute = &PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB
                                                     (pCtTnlInfo));
    if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
    {
        if (RPTE_TE_TNL_FRR_INFO (pCtTnlInfo) != NULL)
        {
            pTeFrrConstInfo = RPTE_TE_TNL_FRR_INFO (pCtTnlInfo);
            RPTE_FAST_REROUTE_SETUP_PRIORITY (pRsvpTeFastReroute)
                = TE_TNL_FRR_CONST_SETUP_PRIO (pTeFrrConstInfo);
            RPTE_FAST_REROUTE_HOLD_PRIORITY (pRsvpTeFastReroute)
                = TE_TNL_FRR_CONST_HOLD_PRIO (pTeFrrConstInfo);
            RPTE_FAST_REROUTE_HOP_LIMIT (pRsvpTeFastReroute)
                = TE_TNL_FRR_CONST_HOP_LIMIT (pTeFrrConstInfo);
            RPTE_FAST_REROUTE_FLAGS (pRsvpTeFastReroute)
                = TE_TNL_FRR_CONST_PROT_METHOD (pTeFrrConstInfo);
            RPTE_FAST_REROUTE_BANDWIDTH (pRsvpTeFastReroute)
                = (FLT4) TE_TNL_FRR_CONST_BANDWIDTH (pTeFrrConstInfo);
            RPTE_FAST_REROUTE_EXANY_ATTR (pRsvpTeFastReroute)
                = TE_TNL_FRR_CONST_EXANY_AFFINITY (pTeFrrConstInfo);
            RPTE_FAST_REROUTE_INANY_ATTR (pRsvpTeFastReroute)
                = TE_TNL_FRR_CONST_INCANY_AFFINITY (pTeFrrConstInfo);
            RPTE_FAST_REROUTE_INALL_ATTR (pRsvpTeFastReroute)
                = TE_TNL_FRR_CONST_INCALL_AFFINITY (pTeFrrConstInfo);
        }
    }
    /* One-To-One case handling */
    if ((pRsvpTeFastReroute->u1Flags == RPTE_ZERO) &&
        (RPTE_FRR_CSPF_INFO (pCtTnlInfo) == RPTE_FRR_CSPF_NO_START))
    {
        /* By default one-to-one backup method is selected */
        RSVPTE_TNL_GBL_REVERT_FLAG (pCtTnlInfo) = RPTE_FALSE;
        RpteCspfCompForBkpOrProtTnl (pCtTnlInfo);
    }
    else if ((pRsvpTeFastReroute->u1Flags == RSVPTE_TNL_FRR_ONE2ONE_METHOD) &&
             (RPTE_FRR_CSPF_INFO (pCtTnlInfo) == RPTE_FRR_CSPF_NO_START))
    {
        /* If One-to-One Backup method is desired, A call to CSPF Module
         * is done, to compute backup method. */
        RSVPTE_TNL_GBL_REVERT_FLAG (pCtTnlInfo) = RPTE_FALSE;
        RpteCspfCompForBkpOrProtTnl (pCtTnlInfo);
    }
    else if (pRsvpTeFastReroute->u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD)
    {
        /* If Facility backup method is desired, the backup tunnel 
         * satisfying the necessary constraints is identified. */
        RpteFrrBypassTnlIdentification (&pCtTnlInfo);
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhBackupPathTrigger : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRhRsbRefreshTimeOutDestroy                         */
/* Description     : This function deletes the ILM Information from the     */
/*                   hardware, frees the label and sends the RESV Tear to   */
/*                   the upstream node on receiving the RESV Refresh Time   */
/*                   out.                                                    */
/* Input (s)       : pCtTnlInfo - Pointer to RsvpTeTnlInfo                  */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhRsbRefreshTimeOutDestroy (tRsvpTeTnlInfo * pCtTnlInfo)
{
    RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO, pCtTnlInfo,
                              GMPLS_SEGMENT_FORWARD);
    RpteRhFreeLabel (RSB_IN_IF_ENTRY ((RSVPTE_TNL_RSB (pCtTnlInfo))),
                     &(pCtTnlInfo->DownstrInLbl));
    pCtTnlInfo->DownstrInLbl.u4GenLbl = MPLS_INVALID_LABEL;

    if (pCtTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
    {
        RpteRhSendMplsMlibUpdate
            (RPTE_MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO, pCtTnlInfo,
             GMPLS_SEGMENT_REVERSE);
        RpteRhFreeLabel (PSB_OUT_IF_ENTRY
                         (RSVPTE_TNL_PSB (pCtTnlInfo)),
                         &pCtTnlInfo->UpStrInLbl);
        pCtTnlInfo->UpStrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
    }
    RpteRthSendResvTear (pCtTnlInfo->pRsb);
    RpteDSRelTrafficControl (pCtTnlInfo);
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteRhRsbRefreshTimeOutDestroy : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteFrrMPBypassTnlSearch                               */
/* Description     : This function searches for the bypass tnls             */
/*                   for best backup                                        */
/* Input (s)       : RpteRhSendMplsMlibUpdate                          */
/*                   ppCtTnlInfo - Pointer to Pointer to RsvpTeTnlInfo      */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
UINT1
RpteFrrMPBypassTnlSearch (UINT4 u4StrictAddr, tRsvpTeTnlInfo ** ppCtTnlInfo)
{
    tIfEntry           *pPsbIfEntry = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pCtTnlInfo = *ppCtTnlInfo;
    tRsvpTeTnlInfo     *pDefProtRsvpTeTnlInfo = NULL;
    tTMO_DLL_NODE      *pFacilityTnlNode = NULL;
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    FLT4                f4TmpVar = RPTE_ZERO;
    UINT4               u4LsrAddr = RPTE_ZERO;
    UINT4               u4Bandwth = RPTE_ZERO;

    CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                        u4LsrAddr);
    u4LsrAddr = OSIX_NTOHL (u4LsrAddr);

    pRsvpTeFastReroute =
        &PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pCtTnlInfo));
    if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
    {
        if ((RPTE_TE_TNL_FRR_INFO (pCtTnlInfo)->u4Bandwidth) != RPTE_ZERO)
        {
            u4Bandwth =
                TE_TNL_FRR_CONST_BANDWIDTH (RPTE_TE_TNL_FRR_INFO (pCtTnlInfo));
        }
    }
    else if (pCtTnlInfo->pTeTnlInfo->bIsBwProtDesired == TRUE)
    {
        f4TmpVar = OSIX_NTOHF (pRsvpTeFastReroute->fBandwth);
        RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
        u4Bandwth = (UINT4) f4TmpVar;
    }

    pPsbIfEntry = PSB_IF_ENTRY (RSVPTE_TNL_PSB (pCtTnlInfo));
    TMO_DLL_Scan (&IF_ENTRY_FAC_TNL_LIST (pPsbIfEntry),
                  pFacilityTnlNode, tTMO_DLL_NODE *)
    {
        if ((((tFacilityRpteBkpTnl *) (pFacilityTnlNode))->pRsvpTeTnlInfo) ==
            NULL)
        {
            continue;
        }

        pTmpRsvpTeTnlInfo =
            ((tFacilityRpteBkpTnl *) (pFacilityTnlNode))->pRsvpTeTnlInfo;

        if (RPTE_TE_TNL (pTmpRsvpTeTnlInfo) != NULL)
        {
            if (!
                ((OSIX_NTOHL (RSVPTE_TNL_EGRESS (pTmpRsvpTeTnlInfo)) ==
                  u4LsrAddr)
                 && (OSIX_NTOHL (RSVPTE_TNL_INGRESS (pTmpRsvpTeTnlInfo)) ==
                     u4StrictAddr)))
            {
                continue;
            }
            if (RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo) == NULL)
            {
                continue;
            }
            /* Tunnel is matched and checking for the bandwidth constraint */
            if ((pCtTnlInfo->pTeTnlInfo->bIsBwProtDesired != TRUE) ||
                (RESV_TSPEC_BKT_RATE (FLOW_SPEC_OBJ
                                      (RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo)))
                 >=
                 RESV_TSPEC_BKT_RATE (FLOW_SPEC_OBJ
                                      (RSVPTE_TNL_RSB (pCtTnlInfo)))))
            {
                /* TODO Conflict here needs to be solved */
                if ((RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4IncAnyAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4ExAnyAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4IncAllAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->u1SetPrio) &&
                    (RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->u1HoldPrio))
                {
                    /* Backup bypass tunnel is matched */
                    pDefProtRsvpTeTnlInfo = pTmpRsvpTeTnlInfo;
                }
            }
            if ((pDefProtRsvpTeTnlInfo != NULL) && (u4Bandwth == RPTE_ZERO))
            {
                /* Backup bypass tunnel is matched */
                RPTE_FRR_IN_TNL_INFO (pCtTnlInfo) = pDefProtRsvpTeTnlInfo;
                RSVPTE_TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH
                    (RPTE_TE_TNL (pCtTnlInfo))
                    = (UINT4) RESV_TSPEC_BKT_RATE (FLOW_SPEC_OBJ
                                                   (RSVPTE_TNL_RSB
                                                    (pDefProtRsvpTeTnlInfo)));
                RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS (RPTE_TE_TNL (pCtTnlInfo))
                    = TE_TNL_FRR_PROT_STATUS_READY;
                break;
            }
            f4TmpVar = OSIX_NTOHF (RESV_TSPEC_BKT_RATE
                                   (FLOW_SPEC_OBJ
                                    (RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo))));
            RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);

            if ((u4Bandwth != RPTE_ZERO) && ((UINT4) f4TmpVar == u4Bandwth))
            {
                /* TODO Conflict here needs to be solved */
                if ((RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4IncAnyAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4ExAnyAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4IncAllAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->u1SetPrio) &&
                    (RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->u1HoldPrio))
                {
                    /* Backup bypass tunnel is matched */
                    RPTE_FRR_IN_TNL_INFO (pCtTnlInfo) = pTmpRsvpTeTnlInfo;
                    RSVPTE_TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH
                        (RPTE_TE_TNL (pCtTnlInfo))
                        = (UINT4) RESV_TSPEC_BKT_RATE (FLOW_SPEC_OBJ
                                                       (RSVPTE_TNL_RSB
                                                        (pTmpRsvpTeTnlInfo)));
                    RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS (RPTE_TE_TNL (pCtTnlInfo))
                        = TE_TNL_FRR_PROT_STATUS_READY;
                    RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) |= FRR_BW_PROT;
                    break;
                }
            }
        }
    }
    /* Desired bandwidth satisfying bypass is not macthed so,
     * taking the bypass tunnel which matches the protected tunnel's bandwidth */
    if ((u4Bandwth != RPTE_ZERO) &&
        (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo) == NULL)
        && (pDefProtRsvpTeTnlInfo != NULL))
    {
        RPTE_FRR_IN_TNL_INFO (pCtTnlInfo) = pDefProtRsvpTeTnlInfo;
        RSVPTE_TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH
            (RPTE_TE_TNL (pCtTnlInfo))
            = (UINT4) RESV_TSPEC_BKT_RATE (FLOW_SPEC_OBJ
                                           (RSVPTE_TNL_RSB
                                            (pDefProtRsvpTeTnlInfo)));
        RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS (RPTE_TE_TNL (pCtTnlInfo))
            = TE_TNL_FRR_PROT_STATUS_READY;
    }
    if (RPTE_FRR_IN_TNL_INFO (pCtTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteFrrMPBypassTnlSearch: "
                    "No bypass tnl exist: INTMD EXIT\n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteFrrMPBypassTnlSearch "
                "bypass tnl exist: EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteFrrBypassTnlSearch                                 */
/* Description     : This function searches for the bypass tnls             */
/*                   for best backup                                        */
/* Input (s)       : u4StrictAddr - Strict address                          */
/*                   ppCtTnlInfo - Pointer to Pointer to RsvpTeTnlInfo      */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
UINT1
RpteFrrBypassTnlSearch (UINT4 u4StrictAddr, tRsvpTeTnlInfo ** ppCtTnlInfo)
{
    tIfEntry           *pPsbIfEntry = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pCtTnlInfo = *ppCtTnlInfo;
    tRsvpTeTnlInfo     *pDefProtRsvpTeTnlInfo = NULL;
    tTMO_DLL_NODE      *pFacilityTnlNode = NULL;
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    FLT4                f4TmpVar = RSVPTE_ZERO;
    UINT4               u4LsrAddr = RPTE_ZERO;
    UINT4               u4Bandwth = RPTE_ZERO;

    CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                        u4LsrAddr);
    pRsvpTeFastReroute =
        &PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pCtTnlInfo));
    if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
    {
        if ((RPTE_TE_TNL_FRR_INFO (pCtTnlInfo)->u4Bandwidth) != RPTE_ZERO)
        {
            u4Bandwth =
                TE_TNL_FRR_CONST_BANDWIDTH (RPTE_TE_TNL_FRR_INFO (pCtTnlInfo));
        }
    }
    else if (pCtTnlInfo->pTeTnlInfo->bIsBwProtDesired == TRUE)
    {
        f4TmpVar = OSIX_NTOHF (pRsvpTeFastReroute->fBandwth);
        RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);
        u4Bandwth = (UINT4) f4TmpVar;
    }

    pPsbIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pCtTnlInfo));
    TMO_DLL_Scan (&IF_ENTRY_FAC_TNL_LIST (pPsbIfEntry),
                  pFacilityTnlNode, tTMO_DLL_NODE *)
    {
        if ((((tFacilityRpteBkpTnl *) (pFacilityTnlNode))->pRsvpTeTnlInfo) ==
            NULL)
        {
            continue;
        }

        pTmpRsvpTeTnlInfo =
            ((tFacilityRpteBkpTnl *) (pFacilityTnlNode))->pRsvpTeTnlInfo;
        if (RPTE_TE_TNL (pTmpRsvpTeTnlInfo) != NULL)
        {
            if (!
                ((RpteIpLocalAddress
                  (OSIX_NTOHL (RSVPTE_TNL_INGRESS (pTmpRsvpTeTnlInfo))) ==
                  RPTE_SUCCESS)
                 && (OSIX_NTOHL (RSVPTE_TNL_EGRESS (pTmpRsvpTeTnlInfo)) ==
                     u4StrictAddr)))
            {
                continue;
            }
            if (RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo) == NULL)
            {
                continue;
            }

            /* Tunnel is matched and checking for the bandwidth constraint */
            if ((pCtTnlInfo->pTeTnlInfo->bIsBwProtDesired != TRUE) ||
                (RESV_TSPEC_BKT_RATE (FLOW_SPEC_OBJ
                                      (RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo)))
                 >=
                 RESV_TSPEC_BKT_RATE (FLOW_SPEC_OBJ
                                      (RSVPTE_TNL_RSB (pCtTnlInfo)))))
            {
                /* TODO Conflict here needs to be solved */
                if ((RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4IncAnyAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4ExAnyAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4IncAllAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->u1SetPrio) &&
                    (RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->u1HoldPrio))
                {
                    /* Backup bypass tunnel is matched */
                    pDefProtRsvpTeTnlInfo = pTmpRsvpTeTnlInfo;
                }
            }

            if ((pDefProtRsvpTeTnlInfo != NULL) && (u4Bandwth == RPTE_ZERO))
            {
                /* Backup bypass tunnel is matched */
                RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) = pDefProtRsvpTeTnlInfo;
                RSVPTE_TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH
                    (RPTE_TE_TNL (pCtTnlInfo))
                    = (UINT4) RESV_TSPEC_BKT_RATE (FLOW_SPEC_OBJ
                                                   (RSVPTE_TNL_RSB
                                                    (pDefProtRsvpTeTnlInfo)));
                RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS (RPTE_TE_TNL (pCtTnlInfo))
                    = TE_TNL_FRR_PROT_STATUS_READY;
                break;
            }
            f4TmpVar = OSIX_NTOHF (RESV_TSPEC_BKT_RATE
                                   (FLOW_SPEC_OBJ
                                    (RSVPTE_TNL_RSB (pTmpRsvpTeTnlInfo))));
            RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS (f4TmpVar);

            if ((u4Bandwth != RPTE_ZERO) && ((UINT4) f4TmpVar == u4Bandwth))
            {
                /* TODO Conflict here needs to be solved */
                if ((RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4IncAnyAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4ExAnyAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->RAParam.u4IncAllAttr) &&
                    (RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->u1SetPrio) &&
                    (RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pTmpRsvpTeTnlInfo) ==
                     pRsvpTeFastReroute->u1HoldPrio))
                {
                    /* Backup bypass tunnel is matched */
                    RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) = pTmpRsvpTeTnlInfo;
                    RSVPTE_TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH
                        (RPTE_TE_TNL (pCtTnlInfo))
                        = (UINT4) RESV_TSPEC_BKT_RATE (FLOW_SPEC_OBJ
                                                       (RSVPTE_TNL_RSB
                                                        (pTmpRsvpTeTnlInfo)));
                    RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS (RPTE_TE_TNL (pCtTnlInfo))
                        = TE_TNL_FRR_PROT_STATUS_READY;
                    RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) |= FRR_BW_PROT;
                    break;
                }
            }
        }
    }
    /* Desired bandwidth satisfying bypass is not macthed so,
     * taking the bypass tunnel which matches the protected tunnel's bandwidth */
    if ((u4Bandwth != RPTE_ZERO) &&
        (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) == NULL)
        && (pDefProtRsvpTeTnlInfo != NULL))
    {
        RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) = pDefProtRsvpTeTnlInfo;
        RSVPTE_TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH
            (RPTE_TE_TNL (pCtTnlInfo))
            = (UINT4) RESV_TSPEC_BKT_RATE (FLOW_SPEC_OBJ
                                           (RSVPTE_TNL_RSB
                                            (pDefProtRsvpTeTnlInfo)));
        RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS (RPTE_TE_TNL (pCtTnlInfo))
            = TE_TNL_FRR_PROT_STATUS_READY;
    }
    if (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteFrrBypassTnlSearch: "
                    "No bypass tnl exist: INTMD EXIT\n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteFrrBypassTnlSearch "
                "bypass tnl exist: EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteTlmUpdateTrafficControl                            */
/* Description     : This function reserves or frees bandwidth for the      */
/*                   tunnel in in TLM module.                               */
/*                                                                          */
/*                   When bandwidth reservation is requested, unreserved    */
/*                   bandwidth is obtained for the tunnel setup priority    */
/*                   from TLM. If bandwidth is available it is reserved.    */
/*                   If bandwidth is not available, preemption of tunnels   */
/*                   are done.                                              */
/* Input (s)       : pRsvpTeTnlInfo   - Pointer to Tunnel Info              */
/*                   u4TeIfIndex      - Te link index                       */
/*                   reqResBw         - requested bw                        */
/*                   u1ResvRsrc       - resource reservation/deletion       */
/* Output (s)      : pu1RefreshNeeded - Refresh Needed or Not               */
/* Returns         : RPTE_FAILURE / RPTE_SUCCESS                            */
/****************************************************************************/
INT1
RpteTlmUpdateTrafficControl (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT4 u4TeIfIndex,
                             FLOAT reqResBw, UINT1 u1ResvRsrc,
                             UINT1 *pu1RefreshNeeded, UINT1 u1SegDirection)
{
    tTMO_SLL            PreemptTnlList;
    tIfEntry           *pIfEntry = NULL;
    FLOAT               availBw = RSVPTE_ZERO;
    FLOAT               unResvBw = RSVPTE_ZERO;
    UINT1               u1SetupPriority = RSVPTE_ZERO;
    UINT1               u1HoldingPriority = RSVPTE_ZERO;
    UINT4               u4CompIfIndex = RSVPTE_ZERO;
    /* Do not release bandwidth when the RSVP-TE component is made
     * shut with GR enabled
     * */
    if (gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_SHUT_DOWN_IN_PROGRESS)
    {
        return RPTE_SUCCESS;
    }

    if ((gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_RECOVERY_IN_PROGRESS) &&
        (pRsvpTeTnlInfo->u1IsGrTnl == RPTE_YES))
    {
        return RPTE_SUCCESS;
    }

    /* This is for simulating bandwidth reservation failure scenario
     * test case mapping -> GMPLS_ST_PROTECT_FULL_REROUTE_FUNC_20 */
    if (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_BW_RESV)
    {
        return RPTE_FAILURE;
    }
    if ((u4TeIfIndex == RPTE_ZERO) || (reqResBw == RPTE_ZERO))
    {
        if ((u1ResvRsrc == RPTE_TRUE) &&
            (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS))
        {
            *pu1RefreshNeeded = RPTE_REFRESH_MSG_YES;
        }
        else
        {
            *pu1RefreshNeeded = RPTE_REFRESH_MSG_NO;
        }
        return RPTE_SUCCESS;
    }

    if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
    {
        pIfEntry = pRsvpTeTnlInfo->pRsb->pOutIfEntry;
    }
    else
    {
        pIfEntry = pRsvpTeTnlInfo->pPsb->pIncIfEntry;
    }

    u1SetupPriority = RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pRsvpTeTnlInfo);
    u1HoldingPriority = RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pRsvpTeTnlInfo);

    if (MplsApiValDiffservTeParams (RPTE_ZERO, RPTE_ZERO, RPTE_ZERO, RPTE_ZERO,
                                    MPLS_DSTE_ENABLED_FLAG,
                                    RPTE_ZERO) == RPTE_SUCCESS)
    {
        if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
        {
            if (MplsApiValDiffservTeParams (RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE
                                            (pRsvpTeTnlInfo), u1SetupPriority,
                                            RPTE_ZERO, RPTE_ZERO,
                                            MPLS_DSTE_TE_CLASS_FLAG,
                                            &u1SetupPriority) == RPTE_FAILURE)
            {
                return RPTE_FAILURE;
            }
            if (MplsApiValDiffservTeParams (RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE
                                            (pRsvpTeTnlInfo), u1HoldingPriority,
                                            RPTE_ZERO, RPTE_ZERO,
                                            MPLS_DSTE_TE_CLASS_FLAG,
                                            &u1HoldingPriority) == RPTE_FAILURE)
            {
                return RPTE_FAILURE;
            }
        }
    }

    if (u1ResvRsrc == RPTE_FALSE)
    {
        if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
        {
            u4CompIfIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4FwdComponentIfIndex;
        }
        else
        {
            u4CompIfIndex = pRsvpTeTnlInfo->pTeTnlInfo->u4RevComponentIfIndex;
        }

        if (RptePortTlmUpdateTeLinkUnResvBw (u4TeIfIndex,
                                             u1HoldingPriority, reqResBw,
                                             u1ResvRsrc, &u4CompIfIndex)
            == RPTE_FAILURE)
        {
            return RPTE_FAILURE;
        }

        RptePmRemTnlFromPrioList (pIfEntry,
                                  RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO
                                  (pRsvpTeTnlInfo), u1SegDirection,
                                  pRsvpTeTnlInfo);
        *pu1RefreshNeeded = RPTE_REFRESH_MSG_NO;
        pRsvpTeTnlInfo->pTeTnlInfo->b1IsResourceReserved = RPTE_FALSE;

        return RPTE_SUCCESS;
    }
    else
    {
        RptePortTlmGetTeLinkUnResvBw (u4TeIfIndex, u1SetupPriority,
                                      &unResvBw, &availBw);
        if (availBw < reqResBw)
        {
            if (unResvBw < reqResBw)
            {
                return RPTE_FAILURE;
            }

            /* Preempt */

            if ((RptePmGetTnlFromPrioList
                 (pIfEntry, RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pRsvpTeTnlInfo),
                  pRsvpTeTnlInfo, &PreemptTnlList,
                  u1SegDirection) != RPTE_SUCCESS)
                || (RpteRhPreemptTunnel (&PreemptTnlList) != RPTE_SUCCESS))
            {
                return RPTE_FAILURE;
            }
        }
    }

    if (RptePortTlmUpdateTeLinkUnResvBw (u4TeIfIndex,
                                         u1HoldingPriority, reqResBw,
                                         u1ResvRsrc, &u4CompIfIndex)
        == RPTE_FAILURE)
    {
        return RPTE_FAILURE;
    }

    if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
    {
        if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS)
        {
            *pu1RefreshNeeded = RPTE_REFRESH_MSG_YES;
        }
        pRsvpTeTnlInfo->pTeTnlInfo->u4FwdComponentIfIndex = u4CompIfIndex;
    }
    else
    {
        if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_EGRESS)
        {
            *pu1RefreshNeeded = RPTE_REFRESH_MSG_YES;
        }
        pRsvpTeTnlInfo->pTeTnlInfo->u4RevComponentIfIndex = u4CompIfIndex;
    }
    RptePmAddTnlToPrioList (pIfEntry,
                            RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pRsvpTeTnlInfo),
                            u1SegDirection, pRsvpTeTnlInfo);
    pRsvpTeTnlInfo->pTeTnlInfo->b1IsResourceReserved = RPTE_TRUE;

    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteRhRelReroutedTnlBw
 * Description     : This function maps the working tunnels with protected
 *                   tunnels and vice versa .
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 *                   u1SegDirection - segment direction
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteRhRelReroutedTnlBw (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 u1SegDirection)
{
    tSenderTspec       *pPathTe = NULL;
    tRsvpTeTnlInfo     *pRsvpTeWorkTnlInfo;
    UINT4               u4TeLinkIf = RPTE_ZERO;
    FLOAT               reqResBw = RPTE_ZERO;
    UINT1               u1ResvRefreshNeeded = RPTE_ZERO;

    /* Working tunnel's bandwidth should be released only for
     * 1. Mbb and Map tunnel is not equal to NULL
     * 2. FULL-LSP-REROUTE and Map tunnel is not equal to NULL*/

    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE) || (pRsvpTeTnlInfo->pMapTnlInfo == NULL))
    {
        return;
    }

    pRsvpTeWorkTnlInfo = pRsvpTeTnlInfo->pMapTnlInfo;

    if ((u1SegDirection == GMPLS_SEGMENT_FORWARD)
        && ((pRsvpTeWorkTnlInfo->pTeTnlInfo) != NULL)
        && (pRsvpTeTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf ==
            pRsvpTeWorkTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf)
        && (RSVPTE_TNL_RSB (pRsvpTeWorkTnlInfo)) != NULL)

    {
        reqResBw = OSIX_NTOHF ((RSVPTE_TNL_RSB (pRsvpTeWorkTnlInfo))->
                               FlowSpec.ServiceParams.ClsService.
                               TBParams.fPeakRate);

        u4TeLinkIf = pRsvpTeWorkTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf;
    }
    else if ((u1SegDirection == GMPLS_SEGMENT_REVERSE)
             && ((pRsvpTeWorkTnlInfo->pTeTnlInfo) != NULL)
             && (pRsvpTeTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf ==
                 pRsvpTeWorkTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf)
             && ((pRsvpTeWorkTnlInfo->pPsb) != NULL))
    {
        pPathTe =
            &SENDER_TSPEC_OBJ (&PSB_SENDER_TSPEC_OBJ
                               (pRsvpTeWorkTnlInfo->pPsb));
        reqResBw = OSIX_NTOHF (pPathTe->TBParams.fPeakRate);
        u4TeLinkIf = pRsvpTeWorkTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf;
    }

    if (RpteTlmUpdateTrafficControl
        (pRsvpTeWorkTnlInfo, u4TeLinkIf, reqResBw,
         RPTE_FALSE, &u1ResvRefreshNeeded, u1SegDirection) == RPTE_FAILURE)
    {
        return;
    }
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteFrrFillFacilityListEntry
 * Description     : This function fill the rpte information of backup tunnel
 *                   in frr facility list.
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 * Output (s)      : None
 * Returns         : If successful, returns RPTE_SUCCESS. Else returns
 *                   RPTE_FAILURE.
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteFrrFillFacilityListEntry (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{

    tRsvpTeFrrTnlPortMapping *pRsvpTeFrrTnlPortMapping = NULL;
    tRsvpTeFrrTnlPortMapping RsvpTeTmpFrrTnlPortMapping;
    INT4                i4ArrayIndex = RPTE_ZERO;
    UINT4               u4BitPosition = RPTE_ZERO;
    UINT4               u4L3Intf = RPTE_ONE;
    UINT4               u4LocL3Intf = RPTE_ZERO;
    UINT4               u4MplsIfIndex = RPTE_ZERO;
    UINT4               u4IngressId = RPTE_ZERO;
    UINT4               u4EgressId = RPTE_ZERO;
    tIfEntry           *pIfEntry = NULL;
    tTMO_DLL           *pIfFacTnlsList = NULL;
    tTMO_DLL_NODE      *pIfFacTnlsNode = NULL;
    tFacilityRpteBkpTnl *pFacilityRpteBkpTnl = NULL;

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteFrrFillFacilityListInfo : ENTRY\n");

    MEMSET (&RsvpTeTmpFrrTnlPortMapping, RPTE_ZERO,
            sizeof (tRsvpTeFrrTnlPortMapping));

    /* Check the presence of backup tunnel in Rbtree */
    RsvpTeTmpFrrTnlPortMapping.u2TnlId = (UINT2)
        pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex;

    pRsvpTeFrrTnlPortMapping = (tRsvpTeFrrTnlPortMapping *)
        RBTreeGet (gpRsvpTeGblInfo->RpteFrrTnlPortMappingTree,
                   (tRBElem *) & RsvpTeTmpFrrTnlPortMapping);

    if (pRsvpTeFrrTnlPortMapping == NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteFrrFillFacilityListInfo :"
                    "No Frr tunnel port maping exists in Rbtree : INTMD-EXIT\n");
        return RPTE_SUCCESS;
    }

    /* Get the L3 interface and fill the Facility list information */
    for (i4ArrayIndex = (MAX_RSVPTE_L3_INF_ARRAY_INDEX - 1);
         i4ArrayIndex >= RPTE_ZERO; i4ArrayIndex--)
    {

        /* If interface is present in selected au4L3Intf 
         * Search the correct L3 interface and fill the frr 
         * facility list information */
        if (pRsvpTeFrrTnlPortMapping->au4L3Intf[i4ArrayIndex] != RPTE_ZERO)
        {
            /* find the enable bit in au4L3Intf */
            for (u4BitPosition = RPTE_ZERO; u4BitPosition <
                 RPTE_TOTAL_BITS_IN_UINT4; u4BitPosition++)
            {
                u4L3Intf <<= u4BitPosition;

                /* Interface is present in Rbtree */
                if ((u4L3Intf &=
                     pRsvpTeFrrTnlPortMapping->au4L3Intf[i4ArrayIndex]) !=
                    RPTE_ZERO)
                {
                    /* Calculate the actual l3 interface */
                    u4L3Intf = (u4BitPosition + RPTE_ONE) + u4LocL3Intf;

                    /* Get MPLS index from L3 index */
                    if ((CfaUtilGetMplsIfFromIfIndex (u4L3Intf, &u4MplsIfIndex,
                                                      FALSE) == CFA_FAILURE) ||
                        (u4MplsIfIndex == RPTE_ZERO))
                    {
                        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                                    "RpteFrrFillFacilityListInfo :"
                                    "Fails to Get Mpls Index : INTMD-EXIT\n");
                        return RPTE_FAILURE;
                    }

                    /* Associated pIfEntry for the Mpls interface should be there */
                    if ((RpteGetIfEntry ((INT4) u4MplsIfIndex, &pIfEntry)
                         == RPTE_FAILURE) || (pIfEntry == NULL))
                    {
                        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                                    "RpteFrrFillFacilityListInfo :"
                                    "IfEntry not present for selected mpls interface : INTMD-EXIT\n");
                        return RPTE_FAILURE;
                    }

                    /* Get the facility list from If entry */
                    if ((pIfFacTnlsList =
                         (tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfEntry))) ==
                        NULL)
                    {
                        RSVPTE_DBG (RSVPTE_RH_ETEXT,
                                    "RpteFrrFillFacilityListInfo :"
                                    "IfEntry not present for selected mpls interface : INTMD-EXIT\n");
                        return RPTE_FAILURE;
                    }

                    /* Scan the Frr facility list */
                    TMO_DLL_Scan (pIfFacTnlsList, pIfFacTnlsNode,
                                  tTMO_DLL_NODE *)
                    {
                        pFacilityRpteBkpTnl = (tFacilityRpteBkpTnl *)
                            (pIfFacTnlsNode);

                        /* Fill the facility list information 
                         * if rpte pointer is NULL */
                        if ((RPTE_FRR_FACILITY_BKP_TNL_INDEX
                             (pFacilityRpteBkpTnl) ==
                             (UINT4) pRsvpTeFrrTnlPortMapping->u2TnlId)
                            && (RPTE_FRR_PROT_IF_INDEX (pFacilityRpteBkpTnl) ==
                                u4MplsIfIndex)
                            && (RPTE_FRR_FACILITY_BKP_TNL (pFacilityRpteBkpTnl)
                                == NULL))
                        {
                            /* fill the entries in frr facility list */
                            if (RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID
                                (pFacilityRpteBkpTnl) == RPTE_ZERO)
                            {
                                CONVERT_TO_INTEGER (TE_TNL_INGRESS_LSRID
                                                    (pRsvpTeTnlInfo->
                                                     pTeTnlInfo), u4IngressId);
                                RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID
                                    (pFacilityRpteBkpTnl) =
                                    OSIX_NTOHL (u4IngressId);

                            }

                            if (RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID
                                (pFacilityRpteBkpTnl) == RPTE_ZERO)
                            {
                                CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID
                                                     (pRsvpTeTnlInfo->
                                                      pTeTnlInfo)), u4EgressId);
                                RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID
                                    (pFacilityRpteBkpTnl) =
                                    OSIX_NTOHL (u4EgressId);
                            }

                            pFacilityRpteBkpTnl->pRsvpTeTnlInfo =
                                pRsvpTeTnlInfo;
                            RSVPTE_TNL_PROT_IF_INDEX (RPTE_TE_TNL
                                                      (pRsvpTeTnlInfo)) =
                                RPTE_FRR_PROT_IF_INDEX (pFacilityRpteBkpTnl);

                            RSVPTE_FRR_CONF_IF_NUM (gpRsvpTeGblInfo)++;
                            RSVPTE_FRR_CONF_PROT_TUN_NUM (gpRsvpTeGblInfo)++;

                            RSVPTE_DBG (RSVPTE_RR_PRCS,
                                        "RpteFrrFillFacilityListInfo :"
                                        "Succesfully fill the information in Frr facility list"
                                        "fill the facility list for other l3 interfaces \n");
                        }

                    }            /* End of DLL Scan */

                }
                /* reset L3 interface */
                u4L3Intf = RPTE_ONE;
            }
        }

        /* interface not present in first array index */
        u4LocL3Intf += RPTE_TOTAL_BITS_IN_UINT4;
    }                            /* End of for loop */

    RSVPTE_DBG (RSVPTE_RH_ETEXT, "RpteFrrFillFacilityListInfo : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteRhStartReoptimizeTmr                              */
/* Description     : This function starts the Refresh Timer for the Rsb     */
/* Input (s)       : *pRsb -  Pointer to the Rsb                            */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRhStartReoptimizeTmr (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTimerParam        *pTmrParam = NULL;

    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteRhStartReoptimizeTmr : ENTRY\n");

    pTmrParam = (tTimerParam *) & RPTE_REOPTIMIZE_TIMER_PARAM (pRsvpTeTnlInfo);

    TIMER_PARAM_ID (pTmrParam) = RPTE_REOPTIMIZE_INTERVAL;
    TIMER_PARAM_RPTE_TNL (pTmrParam) = pRsvpTeTnlInfo;
    RSVP_TIMER_NAME (&RPTE_REOPTIMIZE_TIMER (pRsvpTeTnlInfo)) =
        (FS_ULONG) pTmrParam;
    if (RpteUtlStartTimer
        (RSVP_GBL_TIMER_LIST, &RPTE_REOPTIMIZE_TIMER (pRsvpTeTnlInfo),
         (UINT4) (RSVPTE_REOPTIMIZE_INTERVAL (gpRsvpTeGblInfo) *
                  SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_REOPT_DBG,
                    "RESV: Starting of RpteRhStartReoptimizeTmr failed ..\n");
    }
    RSVPTE_DBG (RSVPTE_REOPT_DBG, "RpteRhStartReoptimizeTmr : EXIT\n");
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rpteresv.c                             */
/*---------------------------------------------------------------------------*/
