#ifndef __RPTEECCLI_C__
#define __RPTEECCLI_C__

/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptecli.c,v 1.82 2017/12/22 09:42:23 siva Exp $
 *
 * Description: Contains Action routines for CLI RSVP commands.
 *
 *******************************************************************/

#include "rpteincs.h"
#include "mplscli.h"
#include "rpteclip.h"
#include "fsmpfrlw.h"
#include "fsmpfrwr.h"
#include "fsrsvpwr.h"
#include "fsrsvpcli.h"
INT4
cli_process_rsvp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *pu4Args[MPLS_CLI_RSVP_MAX_ARGS + 1];
    INT1                i1Argno = RPTE_ZERO;
    INT4                i4Var1 = RPTE_ZERO;
    INT4                i4Var2 = RPTE_ZERO;
    UINT4               u4Var1 = RPTE_ZERO;
    UINT4               u4Var2 = RPTE_ZERO;
    UINT4               u4ErrorCode = RPTE_ZERO;
    INT4                i4RetStatus = CLI_SUCCESS;
    INT4                i4RpteStatus = RPTE_ADMIN_DOWN;
    UINT4               u4ErrCode = 0;

    if (RSVP_INITIALISED != TRUE)
    {
        CliPrintf (CliHandle, "\r %%RSVP-TE module is shutdown\n");
        return CLI_FAILURE;
    }
    va_start (ap, u4Command);
    for (i1Argno = 0; i1Argno <= MPLS_CLI_RSVP_MAX_ARGS; i1Argno++)
    {
        pu4Args[i1Argno] = va_arg (ap, UINT4 *);
    }
    va_end (ap);

    CliRegisterLock (CliHandle, RsvpTeLock, RsvpTeUnLock);
    RsvpTeLock ();
    switch (u4Command)
    {
        case MPLS_CLI_RPTE_GLBL_STATUS:
            i4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_ADMIN_UP : RPTE_ADMIN_DOWN;
            i4RetStatus = RpteCliSetRsvpTeOperStatus (CliHandle, i4Var1);
            break;

        case MPLS_CLI_RPTE_ADMIN_STATUS_CAP:
            i4Var1 =
                (CLI_ENABLE == CLI_PTR_TO_I4 (pu4Args[0])) ?
                RPTE_ADMIN_STATUS_CAPABLE : RPTE_ADMIN_STATUS_INCAPABLE;
            i4RetStatus =
                RpteCliSetRsvpAdminStatusCapability (CliHandle, i4Var1);
            break;

        case MPLS_CLI_RPTE_PATH_STATE_REMOVE:
            i4Var1 =
                (CLI_ENABLE == CLI_PTR_TO_I4 (pu4Args[0])) ?
                PATH_STATE_REMOVE_ENABLE : PATH_STATE_REMOVE_DISABLE;
            i4RetStatus = RpteCliSetRsvpPathStateRemove (CliHandle, i4Var1);
            break;

        case MPLS_CLI_RPTE_ADMIN_GRACEFUL_TIMER:
            u4Var1 = *((UINT4 *) pu4Args[0]);
            i4RetStatus =
                RpteCliSetGracefulDeletionTimerValue (CliHandle, u4Var1);
            break;

        case MPLS_CLI_RPTE_INTF_CREATE:
            u4Var1 = CLI_PTR_TO_U4 (pu4Args[0]);
            i4RetStatus = RpteCliCreateRsvpInterface (CliHandle, u4Var1);
            break;

        case MPLS_CLI_RPTE_INTF_DELETE:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            if (nmhTestv2FsMplsRsvpTeIfStatus (&u4ErrorCode, i4Var1, DESTROY) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% No Entry Present\r\n");
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (nmhSetFsMplsRsvpTeIfStatus (i4Var1, DESTROY) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% RSVP interface can't be deleted,"
                           " some tunnels are associated.\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_RR_STATUS:
            i4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_RR_ENABLED :
                RPTE_RR_DISABLED;
            if (nmhSetFsMplsRsvpTeRRCapable (i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% RSVP must be disabled before "
                           "enabling/disabling RR\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_HELLO_INTVL:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? HELLO_REFRESH_INTERVAL
                : *((INT4 *) pu4Args[0]);
            if (nmhSetFsMplsRsvpTeHelloIntervalTime (i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% RSVP must be disbled before "
                           "setting hello interval\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_INTF_RFRSH_INTVL:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? DEFAULT_REFRESH_INTERVAL
                : *((INT4 *) pu4Args[0]);
            if (nmhTestv2FsMplsRsvpTeIfRefreshInterval
                (&u4ErrorCode, CLI_GET_IFINDEX (), i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Can not set it on active interface or ensure "
                           "that RSVP interface exist\r\n");
                RsvpTeUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsRsvpTeIfRefreshInterval (CLI_GET_IFINDEX (),
                                                     i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Can not set it on active interface\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_INTF_RFRSH_NMISS:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? DEFAULT_REFRESH_MULTIPLE
                : *((INT4 *) pu4Args[0]);
            if (nmhTestv2FsMplsRsvpTeIfRefreshMultiple
                (&u4ErrorCode, CLI_GET_IFINDEX (), i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Can not set it on active interface or "
                           "ensure that RSVP interface exist\r\n");
                RsvpTeUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsRsvpTeIfRefreshMultiple (CLI_GET_IFINDEX (),
                                                     i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Can not set it on active interface\r\n");
                i4RetStatus = CLI_FAILURE;
            }

            break;

        case MPLS_CLI_RPTE_SET_RTRID:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            if (i4Var1 == CFA_NONE)
            {
                u4Var1 = *((pu4Args[1]));    /* IpAddr */
            }
            else
            {
                u4Var1 = CLI_PTR_TO_U4 (pu4Args[1]);    /* Interface index */
            }
            i4RetStatus = RpteCliSetRsvpTeRouterId (CliHandle, i4Var1, u4Var1);
            break;

        case MPLS_CLI_RPTE_SET_GEN_LBL_SPACE:
            u4Var1 = *(pu4Args[0]);
            i4Var1 = *((INT4 *) pu4Args[1]);
            if ((INT4) u4Var1 > i4Var1)
            {
                CliPrintf (CliHandle, "\r%% Max label must be greater than "
                           "or equal to the min label\r\n");
                RsvpTeUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhGetFsMplsRsvpTeOperStatus (&i4RpteStatus) != SNMP_FAILURE)
            {
                if (i4RpteStatus == RPTE_ADMIN_UP)
                {
                    CliPrintf (CliHandle, "\r%% Disable RSVP to configure the "
                               "labels \r\n");
                    RsvpTeUnLock ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }
            }

            if (nmhTestv2FsMplsRsvpTeGenLblSpaceMaxLbl (&u4ErrorCode, i4Var1)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid MAX label, "
                           "range must be between %d and %d \r\n",
                           gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange,
                           gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange);
                RsvpTeUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhTestv2FsMplsRsvpTeGenLblSpaceMinLbl
                (&u4ErrorCode, CLI_PTR_TO_I4 (u4Var1)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Invalid MIN label, "
                           "range must be between %d and %d \r\n",
                           gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange,
                           gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange);
                RsvpTeUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsRsvpTeGenLblSpaceMaxLbl (i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to set RSVP-TE Maximum Label Range\r\n");
                i4RetStatus = CLI_FAILURE;
            }

            if ((i4RetStatus == CLI_SUCCESS) &&
                (nmhSetFsMplsRsvpTeGenLblSpaceMinLbl (CLI_PTR_TO_I4 (u4Var1)) ==
                 SNMP_FAILURE))
            {
                CliPrintf (CliHandle, "\r%% RSVP must be disbled before "
                           "setting the label space\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_RESET_GEN_LBL_SPACE:
            if (nmhSetFsMplsRsvpTeGenLblSpaceMaxLbl
                (gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Maximum Label Range\r\n");
                i4RetStatus = CLI_FAILURE;
            }

            if ((i4RetStatus == CLI_SUCCESS) &&
                (nmhSetFsMplsRsvpTeGenLblSpaceMinLbl
                 (gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange)
                 == SNMP_FAILURE))
            {
                CliPrintf (CliHandle, "\r%% RSVP must be disbled to "
                           "reset the label space values\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_SOCK_SUPPORT:
            i4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_SNMP_TRUE : RPTE_SNMP_FALSE;
            if (nmhSetFsMplsRsvpTeSockSupprtd (i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% RSVP must be disbled before to "
                           "enable/disable the sock support\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_HELLO_SUPPORT:
            i4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_SNMP_TRUE : RPTE_SNMP_FALSE;
            if (nmhSetFsMplsRsvpTeHelloSupprtd (i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% RSVP must be disbled before to "
                           "enable/disable the hello support\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_MSGID_SUPPORT:
            i4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_ENABLED : RPTE_DISABLED;
            if (nmhSetFsMplsRsvpTeMsgIdCapable (i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% RSVP must be disbled before to "
                           "enable/disable the message id support\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_LABEL_SET_SUPPORT:
            i4Var1 = (CLI_ENABLE == CLI_PTR_TO_I4 (pu4Args[0])) ?
                RPTE_ENABLED : RPTE_DISABLED;
            i4RetStatus = RpteCliSetLabelSetSupport (CliHandle, i4Var1);
            break;
        case MPLS_CLI_RPTE_SET_MAX_TNLS:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                RSVPTE_MAX_RSVPTETNL (gpRsvpTeGblInfo) : *((INT4 *) pu4Args[0]);
            if (nmhSetFsMplsRsvpTeMaxTnls (i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% RSVP must be disbled before "
                           "to set/reset maximum rsvp tunnels\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_MAX_TNL_ERHOPS:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RSVPTE_TNLERHOP_DEF_VAL
                : *((INT4 *) pu4Args[0]);
            if (nmhTestv2FsMplsRsvpTeMaxErhopsPerTnl (&u4ErrorCode, i4Var1) ==
                SNMP_FAILURE)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            if (nmhSetFsMplsRsvpTeMaxErhopsPerTnl (i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% RSVP must be disbled before "
                           "set/reset maximum erhops per tunnel.\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_MAX_RSVP_INTFS:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RSVPTE_MAX_IFACES_DEF_VAL
                : *((INT4 *) pu4Args[0]);
            if (nmhSetFsMplsRsvpTeMaxIfaces (i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% RSVP must be disbled before "
                           "to set/reset maximum RSVP interfaces\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_MAX_RSVP_NBRS:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                RSVPTE_MAX_NEIGHBOURS (gpRsvpTeGblInfo)
                : *((INT4 *) pu4Args[0]);
            if (nmhSetFsMplsRsvpTeMaxNbrs (i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% RSVP must be disbled before "
                           "to set/reset maximum RSVP neighbours\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_RMDPOLICY:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            u4Var1 = CLI_PTR_TO_U4 (pu4Args[1]);
            i4RetStatus = RpteCliSetRsvpTeRMDPolicy (CliHandle, u4Var1, i4Var1);
            break;

        case MPLS_CLI_RPTE_SET_ROUTE_DELAY:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            if (i4Var1 == CLI_FALSE)
            {
                i4Var1 = DEFAULT_ROUTE_DELAY;
            }
            else
            {
                i4Var1 = *((INT4 *) pu4Args[1]);
                if (nmhTestv2FsMplsRsvpTeIfRouteDelay (&u4Var1,
                                                       CLI_GET_IFINDEX (),
                                                       i4Var1) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Interface must be down to "
                               "set/reset the route delay or ensure that RSVP interface exist\r\n");
                    RsvpTeUnLock ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }
            }

            if (nmhSetFsMplsRsvpTeIfRouteDelay (CLI_GET_IFINDEX (), i4Var1)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Interface must be down to "
                           "set/reset the route delay\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_UDP_ENCAP:
            i4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_SNMP_TRUE : RPTE_SNMP_FALSE;
            if (nmhTestv2FsMplsRsvpTeIfUdpRequired
                (&u4ErrorCode, CLI_GET_IFINDEX (), i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Interface must be down to "
                           "enable/disable UDP encapsulation or ensure that RSVP"
                           " interface exist\r\n");
                RsvpTeUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsRsvpTeIfUdpRequired (CLI_GET_IFINDEX (), i4Var1)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Interface must be down to "
                           "enable/disable UDP encapsulation\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_HELLO_ONIF:
            i4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_SNMP_TRUE : RPTE_SNMP_FALSE;
            if (nmhTestv2FsMplsRsvpTeIfHelloSupported
                (&u4ErrorCode, CLI_GET_IFINDEX (), i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Interface must be down to "
                           "enable/disable hello support or ensure that RSVP interface exist\r\n");
                RsvpTeUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }
            if (nmhSetFsMplsRsvpTeIfHelloSupported (CLI_GET_IFINDEX (), i4Var1)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Interface must be down to "
                           "enable/disable hello support\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_LINK_ATTR:
            u4Var1 = CLI_PTR_TO_U4 (pu4Args[0]);
            if (u4Var1 == CLI_FALSE)
            {
                u4Var2 = RPTE_IF_LINK_ATTR_DEF;
            }
            else
            {
                u4Var2 = *(pu4Args[1]);
            }
            if (nmhTestv2FsMplsRsvpTeIfLinkAttr (&u4Var1,
                                                 CLI_GET_IFINDEX (),
                                                 u4Var2) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Interface must be down to "
                           "enable/disable Link Attributes or ensure that RSVP interface exist\r\n");

                RsvpTeUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsRsvpTeIfLinkAttr (CLI_GET_IFINDEX (), u4Var2) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Interface must be down to set\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_INTF_MTU:
            u4Var1 = CLI_PTR_TO_U4 (pu4Args[0]);
            if (u4Var1 == CLI_FALSE)
            {
                i4Var1 = RPTE_IF_MIN_MTU_SIZE;
            }
            else
            {
                i4Var1 = *((INT4 *) pu4Args[1]);
            }
            if (nmhTestv2FsMplsRsvpTeIfMtu
                (&u4ErrorCode, CLI_GET_IFINDEX (), i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Interface must be down to set If MTU or ensure that "
                           "RSVP interface exist\r\n");
                RsvpTeUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsRsvpTeIfMtu (CLI_GET_IFINDEX (), i4Var1) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Interface must be down to set\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_INTF_TTL:
            u4Var1 = CLI_PTR_TO_U4 (pu4Args[0]);
            if (u4Var1 == CLI_FALSE)
            {
                i4Var1 = DEFAULT_TTL;
            }
            else
            {
                i4Var1 = *((INT4 *) pu4Args[1]);
            }
            if (nmhTestv2FsMplsRsvpTeIfTTL (&u4Var1,
                                            CLI_GET_IFINDEX (),
                                            i4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Interface must be down to "
                           "enable/disable If TTL or ensure that RSVP interface exist\r\n");

                RsvpTeUnLock ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsRsvpTeIfTTL (CLI_GET_IFINDEX (), i4Var1) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Interface must be down to set\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_SET_INTF_STATUS:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            /* msg *//* lvl *//* dir */
            if (i4Var1 == CLI_ENABLE)
            {
                if (nmhTestv2FsMplsRsvpTeIfStatus
                    (&u4ErrorCode, CLI_GET_IFINDEX (), ACTIVE) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Can not make the interface "
                               "up or ensure that RSVP interface exist\r\n");
                    RsvpTeUnLock ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }

                if (nmhSetFsMplsRsvpTeIfStatus (CLI_GET_IFINDEX (), ACTIVE) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Can not make the interface "
                               "up\r\n");
                    i4RetStatus = CLI_FAILURE;
                }
            }
            else
            {
                if (nmhTestv2FsMplsRsvpTeIfStatus
                    (&u4ErrorCode, CLI_GET_IFINDEX (),
                     NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Can not make the interface "
                               "down or ensure that RSVP interface exist\r\n");
                    RsvpTeUnLock ();
                    CliUnRegisterLock (CliHandle);
                    return CLI_FAILURE;
                }

                if (nmhSetFsMplsRsvpTeIfStatus
                    (CLI_GET_IFINDEX (), NOT_IN_SERVICE) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%% Can not make the interface "
                               "down\r\n");
                    i4RetStatus = CLI_FAILURE;
                }
            }
            break;
        case MPLS_CLI_RPTE_SET_DS_OVERRIDE:

            if (CLI_PTR_TO_I4 (pu4Args[0]) == CLI_ENABLE)
            {
                i4RetStatus = RpteCliSetRsvpTeOverRideOption
                    (CliHandle, TE_DS_OVERRIDE_SET);
            }
            else
            {
                i4RetStatus = RpteCliSetRsvpTeOverRideOption
                    (CliHandle, TE_DS_OVERRIDE_NOT_SET);
            }
            break;

        case MPLS_CLI_RPTE_DBG_DUMP_MSGS:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[3]);

            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            /* msg *//* lvl *//* dir */
            if (i4Var1 == CLI_ENABLE)
            {
                i4RetStatus = RpteCliDbgSetMsgDumpAttr (CliHandle,
                                                        CLI_PTR_TO_I4
                                                        (pu4Args[1]),
                                                        CLI_PTR_TO_I4
                                                        (pu4Args[2]));
            }
            else
            {
                i4RetStatus = RpteCliDbgReSetMsgDumpAttr (CliHandle,
                                                          CLI_PTR_TO_I4
                                                          (pu4Args[1]),
                                                          CLI_PTR_TO_I4
                                                          (pu4Args[2]));
            }
            break;

        case MPLS_CLI_RPTE_DBG_DUMP_MSG_LVL:

            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            /* msg *//* lvl */
            if (i4Var1 == CLI_ENABLE)
            {
                i4RetStatus =
                    RpteCliDbgSetDumpMsgLvl (CliHandle,
                                             CLI_PTR_TO_I4 (pu4Args[1]));
            }
            else
            {
                i4RetStatus =
                    RpteCliDbgReSetDumpMsgLvl (CliHandle,
                                               CLI_PTR_TO_I4 (pu4Args[1]));
            }

            break;

        case MPLS_CLI_RPTE_DBG_HELLO:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            u4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                (RSVPTE_DBG_FLAG | RSVPTE_HH_PRCS |
                 RSVPTE_DBG_ETEXT)
                : (RSVPTE_DBG_FLAG &
                   ((UINT4) (~(RSVPTE_HH_DBG | RSVPTE_DBG_ETEXT))));
            if (nmhSetFsMplsRsvpTeGenDebugFlag (u4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Debug Flag\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_DBG_INTF:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            u4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                (RSVPTE_DBG_FLAG | RSVPTE_IF_PRCS |
                 RSVPTE_DBG_ETEXT)
                : (RSVPTE_DBG_FLAG &
                   ((UINT4) (~(RSVPTE_IF_DBG | RSVPTE_DBG_ETEXT))));
            if (nmhSetFsMplsRsvpTeGenDebugFlag (u4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Debug Flag\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_DBG_PACKET:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            u4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                (RSVPTE_DBG_FLAG | RSVPTE_PB_PRCS |
                 RSVPTE_PVM_PRCS | RSVPTE_PVM_MEM |
                 RSVPTE_DBG_ETEXT)
                : (RSVPTE_DBG_FLAG &
                   ((UINT4)
                    (~
                     (RSVPTE_PB_DBG | RSVPTE_PVM_DBG | RSVPTE_PVM_MEM |
                      RSVPTE_DBG_ETEXT))));
            if (nmhSetFsMplsRsvpTeGenDebugFlag (u4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Debug Flag\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_DBG_PATH:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            u4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                (RSVPTE_DBG_FLAG | RSVPTE_PE_PRCS |
                 RSVPTE_PT_PRCS | RSVPTE_PH_PRCS |
                 RSVPTE_PH_MEM | RSVPTE_DBG_ETEXT)
                : (RSVPTE_DBG_FLAG & ((UINT4) (~(RSVPTE_PE_DBG | RSVPTE_PT_DBG |
                                                 RSVPTE_PH_DBG | RSVPTE_PH_MEM |
                                                 RSVPTE_DBG_ETEXT))));
            if (nmhSetFsMplsRsvpTeGenDebugFlag (u4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Debug Flag\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_DBG_PROCESS:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            u4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                (RSVPTE_DBG_FLAG | RSVPTE_MAIN_ALL)
                : (RSVPTE_DBG_FLAG & (~RSVPTE_MAIN_ALL));
            if (nmhSetFsMplsRsvpTeGenDebugFlag (u4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Debug Flag\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_DBG_RESV:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            u4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                (RSVPTE_DBG_FLAG | RSVPTE_REH_PRCS |
                 RSVPTE_RTH_PRCS | RSVPTE_RH_PRCS |
                 RSVPTE_RTH_MEM | RSVPTE_RCH_PRCS |
                 RSVPTE_DBG_ETEXT)
                : (RSVPTE_DBG_FLAG &
                   ((UINT4)
                    (~
                     (RSVPTE_REH_DBG | RSVPTE_RTH_DBG | RSVPTE_RH_DBG |
                      RSVPTE_RTH_MEM | RSVPTE_RCH_DBG | RSVPTE_DBG_ETEXT))));
            if (nmhSetFsMplsRsvpTeGenDebugFlag (u4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Debug Flag\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;
        case MPLS_CLI_RPTE_DBG_NEIGHBOUR:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            u4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                (RSVPTE_DBG_FLAG | RSVPTE_NBR_DBG)
                : (RSVPTE_DBG_FLAG & ((UINT4) (~RSVPTE_NBR_DBG)));
            if (nmhSetFsMplsRsvpTeGenDebugFlag (u4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Debug Flag\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_DBG_FRR:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            u4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                (RSVPTE_DBG_FLAG | RSVPTE_FRR_DBG)
                : (RSVPTE_DBG_FLAG & ((UINT4) (~RSVPTE_FRR_DBG)));
            if (nmhSetFsMplsRsvpTeGenDebugFlag (u4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Debug Flag\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;

        case MPLS_CLI_RPTE_DBG_GR:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            u4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                (RSVPTE_DBG_FLAG | RSVPTE_GR_DBG)
                : (RSVPTE_DBG_FLAG & ((UINT4) (~RSVPTE_GR_DBG)));
            if (nmhSetFsMplsRsvpTeGenDebugFlag (u4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Debug Flag\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;
        case MPLS_CLI_RPTE_SHOW_COUNTERS:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            if (i4Var1 == CLI_TRUE)
            {
                i4Var1 = CLI_PTR_TO_I4 (pu4Args[3]);
                if (CfaUtilGetMplsIfFromIfIndex ((UINT4) i4Var1, &u4Var1, TRUE)
                    == CFA_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%%MPLS is not enabled on the "
                               "interface\r\n");
                }
                else
                {
                    i4RetStatus =
                        RpteCliShowRsvpMsgCounters (CliHandle, u4Var1);
                }
                break;
            }

            if (nmhGetFirstIndexFsMplsRsvpTeIfStatsTable (&i4Var1) ==
                SNMP_FAILURE)
            {
                /* Nothing to be displayed */
                break;
            }

            do
            {
                i4RetStatus = RpteCliShowRsvpMsgCounters (CliHandle,
                                                          (UINT4) i4Var1);
                u4Var1 = (UINT4) i4Var1;
            }
            while (nmhGetNextIndexFsMplsRsvpTeIfStatsTable ((INT4) u4Var1,
                                                            &i4Var1)
                   != SNMP_FAILURE);
            break;
        case MPLS_CLI_RPTE_CLEAR_COUNTERS:

            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            if (i4Var1 == CLI_TRUE)
            {
                i4Var1 = CLI_PTR_TO_I4 (pu4Args[3]);
                if (CfaUtilGetMplsIfFromIfIndex ((UINT4) i4Var1, &u4Var1, TRUE)
                    == CFA_FAILURE)
                {
                    CliPrintf (CliHandle, "\r%%MPLS is not enabled on the "
                               "interface\r\n");
                }
                else
                {
                    if (CLI_FAILURE ==
                        RpteCliClearRsvpMsgCounters (CliHandle, u4Var1))
                    {
                        CliPrintf (CliHandle,
                                   "\r%%Unable to clear counters !\n");
                        i4RetStatus = CLI_FAILURE;
                        break;
                    }
                    CliPrintf (CliHandle,
                               "\rCounters cleared successfully !\n");

                }
                break;
            }

            if (nmhGetFirstIndexFsMplsRsvpTeIfStatsTable (&i4Var1) ==
                SNMP_FAILURE)
            {
                /* Nothing to be cleared */
                break;
            }

            do
            {
                i4RetStatus = RpteCliClearRsvpMsgCounters (CliHandle,
                                                           (UINT4) i4Var1);
                u4Var1 = (UINT4) i4Var1;
            }
            while (nmhGetNextIndexFsMplsRsvpTeIfStatsTable ((INT4) u4Var1,
                                                            &i4Var1)
                   != SNMP_FAILURE);
            if (CLI_SUCCESS == i4RetStatus)
            {
                CliPrintf (CliHandle, "\rCounters cleared successfully !\n");
            }
            break;
        case MPLS_CLI_RPTE_SHOW_HELLO_INFO:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            if (i4Var1 == CLI_TRUE)
            {
                u4Var1 = *(pu4Args[1]);
            }
            else
            {
                u4Var1 = 0;
            }
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[2]);
            i4RetStatus = RpteCliShowHelloInstInfo (CliHandle, u4Var1, i4Var1);
            break;

        case MPLS_CLI_RPTE_SHOW_NBR_INFO:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            if (i4Var1 == CLI_TRUE)
            {
                u4Var1 = *(pu4Args[1]);
            }
            else
            {
                u4Var1 = 0;
            }
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[2]);
            i4RetStatus = RpteCliShowNeighborInfo (CliHandle, u4Var1, i4Var1);
            break;

        case MPLS_CLI_RPTE_SHOW_PATH_REQ:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);    /* brief / detail */
            u4Var1 = CLI_PTR_TO_U4 (pu4Args[1]);    /* src/ dest */

            if (u4Var1 == MPLS_CLI_IN)
            {
                u4Var1 = *(pu4Args[2]);
                i4RetStatus = RpteCliShowSrcPathReq (CliHandle, i4Var1, u4Var1);
            }
            else if (u4Var1 == MPLS_CLI_OUT)
            {
                u4Var1 = *(pu4Args[2]);
                i4RetStatus = RpteCliShowDestPathReq (CliHandle, i4Var1,
                                                      u4Var1);
            }
            else                /* display all requests */
            {
                i4RetStatus = RpteCliShowPathReq (CliHandle, i4Var1);
            }
            break;
         /*FRR*/ case MPLS_CLI_RPTE_FRR_DETOUR_MERG:
            i4RetStatus = TeCliFrrDetourMerging (CliHandle, CLI_PTR_TO_I4
                                                 (pu4Args[0]));
            break;
        case MPLS_CLI_RPTE_FRR_DETOUR:
            i4RetStatus = TeCliFrrDetour (CliHandle, CLI_PTR_TO_I4
                                          (pu4Args[0]));
            break;
        case MPLS_CLI_RPTE_FRR_REVERTIVE_LOCAL:
            i4RetStatus = TeCliFrrLocalRevertive (CliHandle);
            break;
        case MPLS_CLI_RPTE_FRR_REVERTIVE_GLOBAL:
            i4RetStatus = TeCliFrrGlobalRevertive (CliHandle);
            break;
        case MPLS_CLI_RPTE_FRR_CSPF:
            i4RetStatus = TeCliFrrCspf (CliHandle,
                                        *((INT4 *) (pu4Args[0])),
                                        *((pu4Args[1])));
            break;
        case MPLS_CLI_RPTE_FRR_NO_CSPF:
            i4RetStatus = TeCliFrrCspf (CliHandle,
                                        RPTE_FRR_CSPF_RETRY_INTERVAL_DEF,
                                        RPTE_FRR_CSPF_RETRY_COUNT_DEF);
            break;
        case MPLS_CLI_RPTE_FRR_BKPUP_TUNNEL:
            i4RetStatus = TeCliFrrBackupTunnel (CliHandle,
                                                *((INT4 *) pu4Args[0]));
            break;
        case MPLS_CLI_RPTE_FRR_NO_BKPUP_TUNNEL:
            i4RetStatus = TeCliFrrNoBackupTunnel (CliHandle,
                                                  *((INT4 *) pu4Args[0]));
            break;
        case MPLS_CLI_RPTE_PLR_AND_AVOID_NODE_ID:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            i4Var2 = CLI_PTR_TO_I4 (pu4Args[1]);
            if (i4Var1 == CLI_TRUE)
            {
                u4Var1 = *((pu4Args[2]));
            }
            if (i4Var2 == CLI_TRUE)
            {
                u4Var2 = *((pu4Args[3]));
            }
            i4RetStatus = TeCliFrrPlrAndAvoidNodeId (CliHandle, u4Var1, u4Var2);
            break;
        case MPLS_CLI_RPTE_FRR_MAKE_AFTER_BREAK:
            i4RetStatus = TeCliFrrMakeAfterBreak (CliHandle, CLI_PTR_TO_I4
                                                  (pu4Args[0]));
            break;
            /*End of FRR */
        case MPLS_CLI_RPTE_GR_MODE:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_GR_CAPABILITY_NONE
                : CLI_PTR_TO_I4 (pu4Args[0]);
            u4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[1])) ? RPTE_GR_RECOVERY_PATH_DEFAULT
                : CLI_PTR_TO_U4 (pu4Args[1]);

            i4RetStatus = RpteCliSetGRCapability (CliHandle, i4Var1, u4Var1);
            break;
        case MPLS_CLI_RPTE_GR_RESTART_TIMER:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_GR_RESTART_DEFAULT_VAL
                : *((INT4 *) pu4Args[0]);
            i4RetStatus = RpteCliSetGrRestartTimerValue (CliHandle, i4Var1);
            break;
        case MPLS_CLI_RPTE_GR_RECOVERY_TIMER:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_GR_RECOVERY_DEFAULT_VAL
                : *((INT4 *) pu4Args[0]);
            i4RetStatus = RpteCliSetGrRecoveryTimerValue (CliHandle, i4Var1);
            break;
        case MPLS_CLI_RPTE_SHOW_GR_INFO:
            RpteCliDisplayGrConfigurations (CliHandle);
            break;
        case MPLS_CLI_RPTE_SET_NBRID:
            u4Var1 = *((pu4Args[0]));
            i4RetStatus =
                RpteCliAddNeighbor (CliHandle, CLI_GET_IFINDEX (), u4Var1);
            break;
        case MPLS_CLI_RPTE_DEL_NBRID:
            u4Var1 = *((pu4Args[0]));
            i4RetStatus =
                RpteCliDelNeighbor (CliHandle, CLI_GET_IFINDEX (), u4Var1);
            break;
        case MPLS_CLI_RPTE_REOPTIMIZE_TIMER:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_REOPT_TIME_DEFAULT_VAL
                : *((INT4 *) pu4Args[0]);
            i4RetStatus = RpteCliSetReoptimizeTimerValue (CliHandle, i4Var1);
            break;
        case MPLS_CLI_RPTE_ERO_CACHE_TIMER:
            i4Var1 =
                (-1 ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ? RPTE_ERO_CACHE_TIME_DEFAULT_VAL
                : *((INT4 *) pu4Args[0]);
            i4RetStatus = RpteCliSetEroCacheTimerValue (CliHandle, i4Var1);
            break;
        case MPLS_CLI_RPTE_REOPT_LINK_MAINTENANCE:
            i4Var1 = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus = RpteCliLspReoptLinkMaintenance (CliHandle, i4Var1);
            break;
        case MPLS_CLI_RPTE_REOPT_NODE_MAINTENANCE:
            i4RetStatus = RpteCliLspReoptNodeMaintenance (CliHandle);
            break;
        case MPLS_CLI_RPTE_SHOW_REOPT_INFO:
            RpteCliShowLspReoptParameters (CliHandle);
            break;
        case MPLS_CLI_RPTE_DBG_REOPT:

            RpteCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            u4Var1 =
                (CLI_ENABLE ==
                 CLI_PTR_TO_I4 (pu4Args[0])) ?
                (RSVPTE_DBG_FLAG | RSVPTE_REOPT_DBG)
                : (RSVPTE_DBG_FLAG & ((UINT4) (~RSVPTE_REOPT_DBG)));

            if (nmhSetFsMplsRsvpTeGenDebugFlag (u4Var1) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% unable to set RSVP-TE Debug Flag\r\n");
                i4RetStatus = CLI_FAILURE;
            }
            break;
        default:
            break;
    }

    if ((i4RetStatus == CLI_FAILURE) &&
        (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > RPTE_CLI_NO_ERROR)
            && (u4ErrCode < RPTE_CLI_MAX_ERROR_MSGS))
        {
            CliPrintf (CliHandle, "%s", RPTE_CLI_ERROR_MSGS[u4ErrCode]);
            CLI_SET_CMD_STATUS (CLI_FAILURE);
            u4ErrCode = 0;
        }
        CLI_SET_ERR (0);
    }

    RsvpTeUnLock ();
    CliUnRegisterLock (CliHandle);
    return i4RetStatus;
}

/****************************************************************************
* Function    :  RpteCliGetDebugLevelVal
* Description :
* Input       :  pu4ArgsOne
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RpteCliGetDebugLevelVal (tCliHandle CliHandle, UINT4 *pu4ArgsOne)
{
    INT4                i4Args = 0;

    if (pu4ArgsOne != NULL)
    {
        i4Args = CLI_PTR_TO_I4 (pu4ArgsOne);
        if (i4Args < DEBUG_DEF_LVL_FLAG)
        {
            RpteCliSetDebugLevel (CliHandle, i4Args);
        }
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  RpteCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
RpteCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    RSVPTE_DBG_LVL = 0x0;

    UNUSED_PARAM (CliHandle);
    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        RSVPTE_DBG_LVL = RSVPTE_ALL_DBG | RSVPTE_DEBUG_ALL;
    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        RSVPTE_DBG_LVL =
            RSVPTE_MAIN_ETEXT | RSVPTE_PVM_ETEXT | RSVPTE_PH_ETEXT |
            RSVPTE_PB_ETEXT | RSVPTE_PE_ETEXT | RSVPTE_IF_ETEXT |
            RSVPTE_PT_ETEXT | RSVPTE_UTL_ETEXT | RSVPTE_RH_ETEXT |
            RSVPTE_REH_ETEXT | RSVPTE_RTH_ETEXT | RSVPTE_IPIF_ETEXT |
            RSVPTE_TCIF_ETEXT | RSVPTE_HH_ETEXT | RSVPTE_RCH_ETEXT |
            RSVPTE_PM_ETEXT | RSVPTE_PORT_ETEXT | RSVPTE_STAT_ETEXT |
            RSVPTE_RR_ETEXT | RSVPTE_MIH_ETEXT | RSVPTE_DIFF_ETEXT |
            RSVPTE_TRIE_ETEXT | RSVPTE_GR_ETEXT | RSVPTE_REOPT_ETEXT;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        RSVPTE_DBG_LVL = RSVPTE_REOPT_DBG | RSVPTE_GR_DBG | RSVPTE_MAIN_DBG |
            RSVPTE_IF_DBG | RSVPTE_IPIF_DBG |
            RSVPTE_PM_TCIF_DBG | RSVPTE_PVM_DBG |
            RSVPTE_PB_DBG | RSVPTE_PH_DBG | RSVPTE_RH_DBG | RSVPTE_PE_DBG |
            RSVPTE_PT_DBG | RSVPTE_REH_DBG |
            RSVPTE_RTH_DBG | RSVPTE_RCH_DBG | RSVPTE_HH_DBG |
            RSVPTE_LLVL_DBG | RSVPTE_UTL_STAT_DBG | RSVPTE_PORT_DBG |
            RSVPTE_DIFF_DBG | RSVPTE_RR_DBG | RSVPTE_MIH_TRIE_DBG |
            RSVPTE_NBR_DBG | RSVPTE_FRR_DBG |
            RSVPTE_DBG_MEM | RSVPTE_DBG_PRCS | RSVPTE_DBG_MISC |
            RSVPTE_DBG_ETEXT | RSVPTE_DBG_SCSS | RSVPTE_DBG_FAIL;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        RSVPTE_DBG_LVL = RSVPTE_REOPT_DBG | RSVPTE_GR_DBG | RSVPTE_MAIN_DBG |
            RSVPTE_IF_DBG | RSVPTE_IPIF_DBG |
            RSVPTE_PM_TCIF_DBG | RSVPTE_PVM_DBG |
            RSVPTE_PB_DBG | RSVPTE_PH_DBG | RSVPTE_RH_DBG | RSVPTE_PE_DBG |
            RSVPTE_PT_DBG | RSVPTE_REH_DBG |
            RSVPTE_RTH_DBG | RSVPTE_RCH_DBG | RSVPTE_HH_DBG |
            RSVPTE_LLVL_DBG | RSVPTE_UTL_STAT_DBG | RSVPTE_PORT_DBG |
            RSVPTE_DIFF_DBG | RSVPTE_RR_DBG | RSVPTE_MIH_TRIE_DBG |
            RSVPTE_NBR_DBG | RSVPTE_FRR_DBG |
            RSVPTE_DBG_MEM | RSVPTE_DBG_PRCS | RSVPTE_DBG_MISC |
            RSVPTE_DBG_ETEXT | RSVPTE_DBG_SCSS | RSVPTE_DBG_FAIL;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        RSVPTE_DBG_LVL = RSVPTE_DBG_FAIL;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        RSVPTE_DBG_LVL = RSVPTE_REOPT_DBG | RSVPTE_GR_DBG | RSVPTE_MAIN_DBG |
            RSVPTE_IF_DBG | RSVPTE_IPIF_DBG |
            RSVPTE_PM_TCIF_DBG | RSVPTE_PVM_DBG |
            RSVPTE_PB_DBG | RSVPTE_PH_DBG | RSVPTE_RH_DBG | RSVPTE_PE_DBG |
            RSVPTE_PT_DBG | RSVPTE_REH_DBG |
            RSVPTE_RTH_DBG | RSVPTE_RCH_DBG | RSVPTE_HH_DBG |
            RSVPTE_LLVL_DBG | RSVPTE_UTL_STAT_DBG | RSVPTE_PORT_DBG |
            RSVPTE_DIFF_DBG | RSVPTE_RR_DBG | RSVPTE_MIH_TRIE_DBG |
            RSVPTE_NBR_DBG | RSVPTE_FRR_DBG |
            RSVPTE_DBG_MEM | RSVPTE_DBG_PRCS | RSVPTE_DBG_MISC |
            RSVPTE_DBG_ETEXT | RSVPTE_DBG_SCSS | RSVPTE_DBG_FAIL;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliConfigMode
 * Description   : This routine changes the mode to RSVP configuration mode
 * Input(s)      : CliHandle - Index of current CLI context
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
RpteCliConfigMode (tCliHandle CliHandle)
{
    if (CliChangePath (MPLS_RSVP_MODE) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "/r%% Unable to enter into RSVP configuration mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliGetRsvpCfgPrompt
 * Description   : This routine returns the RSVP mode prompt 
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
RpteCliGetRsvpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_RSVP_MODE);

    /* Check proper mode name is set or not b4 setting the prompt str */
    if (STRNCMP (pi1ModeName, MPLS_RSVP_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    STRNCPY (pi1DispStr, "(config-rsvp)#", (MAX_PROMPT_LEN - 1));
    pi1DispStr[(MAX_PROMPT_LEN - 1)] = '\0';

    return TRUE;
}

/*****************************************************************************
 * Function Name : RpteCliCreateRsvpInterface
 * Description   : This routine sets the current interface index to the mode
 *                 information and returns the prompt to be displayed. 
 * Input(s)      : pi1ModeName - New mode that to be set
 * Output(s)     : None
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
PRIVATE INT4
RpteCliCreateRsvpInterface (tCliHandle CliHandle, UINT4 u4IfIndex)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4ErrorCode;

    if (nmhTestv2FsMplsRsvpTeIfStatus (&u4ErrorCode, (INT4) u4IfIndex,
                                       CREATE_AND_WAIT) == SNMP_SUCCESS)
    {
        if (nmhSetFsMplsRsvpTeIfStatus (u4IfIndex, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to create RSVP interface "
                       "(Make sure RSVP is enabled globally)\r\n");
            return CLI_FAILURE;
        }
    }

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);
    SNPRINTF ((CHR1 *) au1Cmd, MAX_PROMPT_LEN, "%s%u", MPLS_RSVP_IF_MODE,
              u4IfIndex);

    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "/r%% Unable to enter into RSVP "
                   "interface configuration mode\r\n");
        if (nmhSetFsMplsRsvpTeIfStatus (u4IfIndex, DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled here */
        }
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliGetRsvpIfCfgPrompt
 * Description   : This routine sets the current interface index to the mode
 *                 information and returns the prompt to be displayed. 
 * Input(s)      : pi1ModeName - New mode that to be set
 * Output(s)     : pi1DispStr  - Prompt for that new mode
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
RpteCliGetRsvpIfCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_RSVP_IF_MODE);

    /* check the mode passed is relavent to us and move
     * the name pointer to point to the interface index in it
     */
    if (STRNCMP (pi1ModeName, MPLS_RSVP_IF_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    CLI_SET_IFINDEX (CLI_ATOI (pi1ModeName));
    STRNCPY (pi1DispStr, "(config-rsvp-if)#", (MAX_PROMPT_LEN - 1));
    pi1DispStr[(MAX_PROMPT_LEN - 1)] = '\0';

    return TRUE;
}

/*****************************************************************************
 * Function Name : RpteCliSetRsvpTeOperStatus
 * Description   : This routine is used to enable/disable the RSVP module
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4Status - Rsvp admin status value
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
PRIVATE INT4
RpteCliSetRsvpTeOperStatus (tCliHandle CliHandle, INT4 i4Status)
{
    if (nmhSetFsMplsRsvpTeOperStatus (i4Status) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% RSVP router-id is not set before / "
                   "Failed to change the state\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliSetRsvpAdminStatusCapability 
 * Description   : This routine is used to enable/disable the Admin status
                   handling capability by the RSVP module
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4Status - Rsvp admin status value
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
PRIVATE INT4
RpteCliSetRsvpAdminStatusCapability (tCliHandle CliHandle, INT4 i4AdminStatus)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsMplsRsvpTeAdminStatusCapability (&u4Error,
                                                    i4AdminStatus) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% RSVP Admin Status Capability cannot be set\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsRsvpTeAdminStatusCapability (i4AdminStatus) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set RsvpTeAdminStatusCapability\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliSetRsvpPathStateRemove 
 * Description   : This routine is used to enable/disable the Path State Removal
                   Support by the RSVP module
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4PathStateRemove - Rsvppath state removal support
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
PRIVATE INT4
RpteCliSetRsvpPathStateRemove (tCliHandle CliHandle, INT4 i4PathStateRemove)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsMplsRsvpTePathStateRemovedSupport (&u4Error,
                                                      i4PathStateRemove) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% RSVP capability cannot be set\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsRsvpTePathStateRemovedSupport (i4PathStateRemove)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% unable to set RsvpTePathStateRemovedSupport\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliSetGracefulDeletionTimerValue
 * Description   : This routine is used to set the Graceful Deletion Timer 
                   value of RSVP module.
 * Input(s)      : CliHandle - Index of current CLI context
 *                 u4TimerValue - Rsvp graceful detion timer value 
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
PRIVATE INT4
RpteCliSetGracefulDeletionTimerValue (tCliHandle CliHandle, UINT4 u4TimerValue)
{

    UINT4               u4Error = 0;
    INT4                i4AdminStatus = 0;

    if (nmhGetFsMplsRsvpTeAdminStatusCapability (&i4AdminStatus) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%%Unable to get Admin Status Capability - Setting Graceful "
                   "Deletion Timer failed\r\n");
        return CLI_FAILURE;
    }

    if (i4AdminStatus == RPTE_ADMIN_STATUS_CAPABLE)
    {
        if (nmhTestv2FsMplsRsvpTeAdminStatusTimeIntvl (&u4Error, u4TimerValue)
            != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%%Incorrect Graceful Deletion Timer Value\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsRsvpTeAdminStatusTimeIntvl (u4TimerValue) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%%Unable to set graceful deletion timer\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r%%Admin Status Capability is not enabled - Setting Graceful "
                   "Deletion Timer failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name :  RpteCliSetLabelSetSupport
 * Description   : This routine is used to set the support foor Labelset
                   Object 
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4LabelSet - Rsvp LabelSet Enabled/Disabled
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
PRIVATE INT4
RpteCliSetLabelSetSupport (tCliHandle CliHandle, INT4 i4LabelSet)
{

    UINT4               u4Error = 0;

    if (nmhTestv2FsMplsRsvpTeLabelSetEnabled (&u4Error, i4LabelSet) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%Invalid Label Set support\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsRsvpTeLabelSetEnabled (i4LabelSet) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%Unable to set LabelSet Support\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliSetRsvpTeRouterId
 * Description   : This routine is used to enable/disable the RSVP module
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4Type - Interface type (CFA_L3IPVLAN/CFA_ENET/CFA_NONE)
 *                 u4Value - Ip addr if i4Type is NONE, intf index otherwise
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
PRIVATE INT4
RpteCliSetRsvpTeRouterId (tCliHandle CliHandle, INT4 i4Type, UINT4 u4Value)
{
    UINT4               u4ErrorCode;
    tIpConfigInfo       IpIfInfo;
    tSNMP_OCTET_STRING_TYPE LsrId;
    static UINT1        au1LsrId[RPTE_FOUR] = { RPTE_ZERO };
    LsrId.pu1_OctetList = au1LsrId;

    if (i4Type != CFA_NONE)
    {
        if (CfaIpIfGetIfInfo (u4Value, &IpIfInfo) != CFA_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Unable to get the ip address "
                       "of the interface\r\n");
            return CLI_FAILURE;
        }

        u4Value = IpIfInfo.u4Addr;
    }

    MPLS_INTEGER_TO_OCTETSTRING (u4Value, (&LsrId));
    if (nmhTestv2FsMplsRsvpTeLsrID (&u4ErrorCode, &LsrId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Disable RSVP to configure the router-id\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsRsvpTeLsrID (&LsrId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set RSVP LSR ID "
                   "(Make sure RSVP is enabled globally)\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliSetRsvpTeRMDPolicy
 * Description   : This routine is used to set RsvpTe RMDPolicyObject 
 * Input(s)      : CliHandle - Index of current CLI context
 *                 u4RMDBitMask - RMD Policy for various messages
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliSetRsvpTeRMDPolicy (tCliHandle CliHandle, UINT4 u4RMDBitMask,
                           INT4 i4Status)
{
    UINT4               u4ErrorCode = 0;
    tSNMP_OCTET_STRING_TYPE RmdPolicyObj;
    static UINT1        au1LsrId[RPTE_FOUR] = { RPTE_ZERO };

    RmdPolicyObj.pu1_OctetList = au1LsrId;

    if (nmhGetFsMplsRsvpTeRMDPolicyObject (&RmdPolicyObj) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set RMD policy\r\n");
        return CLI_FAILURE;
    }

    /* Set/Reset the bits for the specified messages */
    if (i4Status == CLI_DISABLE)
    {
        u4RMDBitMask = ~u4RMDBitMask;
        u4RMDBitMask = RmdPolicyObj.pu1_OctetList[0] & u4RMDBitMask;
    }
    else
    {
        u4RMDBitMask |= RmdPolicyObj.pu1_OctetList[0];
    }

    RmdPolicyObj.pu1_OctetList[0] = (UINT1) u4RMDBitMask;
    RmdPolicyObj.i4_Length = 1;
    if (nmhTestv2FsMplsRsvpTeRMDPolicyObject (&u4ErrorCode,
                                              &RmdPolicyObj) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Make sure RSVP is down and atleast one "
                   "option must be set always\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsRsvpTeRMDPolicyObject (&RmdPolicyObj) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Unable to set RSVP RMP Policy object "
                   "(Make sure RSVP is enabled globally)\r\n");
        return CLI_FAILURE;
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliDbgSetMsgDumpAttr
 * Description   : Configures the rsvp messages dumping attributes for
 *                 debugging
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4MsgType - Rsvp message type (path, resv, hello, etc.)
 *                 i4dumpDir - dump i/p msgs or o/p msgs or all
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliDbgSetMsgDumpAttr (tCliHandle CliHandle, INT4 i4MsgType, INT4 i4dumpDir)
{
    INT4                i4OldMsgType = RPTE_ZERO;
    INT4                i4OldDumpDir = RPTE_ZERO;
    INT4                i4NewDumpDir = RPTE_ZERO;
    INT4                i4MsgCount = RSVPTE_TWO;

    UNUSED_PARAM (CliHandle);

    /* Get the old dump message direction type */
    if (SNMP_FAILURE == nmhGetFsMplsRsvpTeGenPduDumpDirection (&i4OldDumpDir))
    {
        CliPrintf (CliHandle, "\r%% Unable to Get Debug Dump Direction\r\n");
        return CLI_FAILURE;
    }

    /* For dump message direction type */
    switch (i4MsgType)
    {
        case MPLS_CLI_RPTE_MSG_PATH:
            i4dumpDir <<= DEBUG_PATH_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_PTEAR:
            i4dumpDir <<= DEBUG_PATHTEAR_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_PERR:
            i4dumpDir <<= DEBUG_PATHERR_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_RESV:
            i4dumpDir <<= DEBUG_RESV_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_RTEAR:
            i4dumpDir <<= DEBUG_RESVTEAR_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_RERR:
            i4dumpDir <<= DEBUG_RESVERR_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_CNFRM:
            i4dumpDir <<= DEBUG_RESVCONF_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_HELLO:
            i4dumpDir <<= DEBUG_HELLO_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_BUNDLE:
            i4dumpDir <<= DEBUG_BUNDLE_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_ACK:
            i4dumpDir <<= DEBUG_ACK_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_SRFRSH:
            i4dumpDir <<= DEBUG_SREFRESH_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_NOTIFY:
            i4dumpDir <<= DEBUG_NOTIFY_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_RECOVERY_PATH:
            i4dumpDir <<= DEBUG_RECOVERY_PATH_MSG_BIT;
            break;
        default:
            break;

    }

    /* Change the other dump messages direction 
     * if message direction is set for all   */
    if (MPLS_CLI_RPTE_MSG_ALL == i4MsgType)
    {
        for (i4MsgCount = RSVPTE_TWO; i4MsgCount <= DEBUG_RSVP_ALL_MSG_BIT;
             i4MsgCount += RSVPTE_TWO)
        {
            i4NewDumpDir |= (i4dumpDir << i4MsgCount);
        }
        i4dumpDir = i4NewDumpDir;
    }

    /* Set the New dump message direction type */
    i4NewDumpDir = i4OldDumpDir | i4dumpDir;

    /* Set the dump message direction type */
    if (nmhSetFsMplsRsvpTeGenPduDumpDirection (i4NewDumpDir) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to Set Debug Dump Direction\r\n");
        return CLI_FAILURE;
    }

    switch (i4MsgType)            /* default type is all */
    {
        case MPLS_CLI_RPTE_MSG_ACK:
            i4MsgType = RSVPTE_DUMP_ACK;
            break;

        case MPLS_CLI_RPTE_MSG_BUNDLE:
            i4MsgType = RSVPTE_DUMP_BUNDLE;
            break;

        case MPLS_CLI_RPTE_MSG_CNFRM:
            i4MsgType = RSVPTE_DUMP_RCNFM;
            break;

        case MPLS_CLI_RPTE_MSG_HELLO:
            i4MsgType = RSVPTE_DUMP_HELLO;
            break;

        case MPLS_CLI_RPTE_MSG_PATH:
            i4MsgType = RSVPTE_DUMP_PATH;
            break;

        case MPLS_CLI_RPTE_MSG_PERR:
            i4MsgType = RSVPTE_DUMP_PERR;
            break;

        case MPLS_CLI_RPTE_MSG_PTEAR:
            i4MsgType = RSVPTE_DUMP_PTEAR;
            break;

        case MPLS_CLI_RPTE_MSG_RESV:
            i4MsgType = RSVPTE_DUMP_RESV;
            break;

        case MPLS_CLI_RPTE_MSG_RERR:
            i4MsgType = RSVPTE_DUMP_RERR;
            break;

        case MPLS_CLI_RPTE_MSG_RTEAR:
            i4MsgType = RSVPTE_DUMP_RTEAR;
            break;

        case MPLS_CLI_RPTE_MSG_SRFRSH:
            i4MsgType = RSVPTE_DUMP_SREFRESH;
            break;

        case MPLS_CLI_RPTE_MSG_NOTIFY:
            i4MsgType = RSVPTE_DUMP_NOTIFY;
            break;

        case MPLS_CLI_RPTE_MSG_RECOVERY_PATH:
            i4MsgType = RSVPTE_DUMP_RECOVERY_PATH;
            break;
        default:
            i4MsgType = RSVPTE_DUMP_ALL;
            break;
    }
    if (i4MsgType != RSVPTE_DUMP_NONE)
    {
        if (nmhGetFsMplsRsvpTeGenPduDumpMsgType (&i4OldMsgType) != SNMP_FAILURE)
        {
            i4MsgType |= i4OldMsgType;
        }
    }

    if (nmhSetFsMplsRsvpTeGenPduDumpMsgType (i4MsgType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set Debug Dump Message Type\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliDbgReSetMsgDumpAttr
 * Description   : Stop the dumping of selected rsvp messages
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4MsgType - Other than 'NONE' if the option is used
 *                 i4dumpDir - 'NONE' if the option is selected
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliDbgReSetMsgDumpAttr (tCliHandle CliHandle, INT4 i4MsgType,
                            INT4 i4dumpDir)
{
    INT4                i4OldMsgType;
    INT4                i4OldDumpDir = RPTE_ZERO;
    INT4                i4NewDumpDir = RPTE_ZERO;
    INT4                i4MsgCount = RSVPTE_TWO;
    INT4                i4DumpDir = MPLS_CLI_ALL;
    INT4                i4NewMsgType = RPTE_ONE;
    INT4                i4dumpDir1 = MPLS_CLI_ALL;

    UNUSED_PARAM (CliHandle);

    /* Get the old dump message direction type */
    if (SNMP_FAILURE == nmhGetFsMplsRsvpTeGenPduDumpDirection (&i4OldDumpDir))
    {
        CliPrintf (CliHandle, "\r%% Unable to Get Debug Dump Direction\r\n");
        return CLI_FAILURE;
    }

    /* For dump message direction type */
    switch (i4MsgType)
    {
        case MPLS_CLI_RPTE_MSG_PATH:
            i4dumpDir <<= DEBUG_PATH_MSG_BIT;
            i4dumpDir1 <<= DEBUG_PATH_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_PTEAR:
            i4dumpDir <<= DEBUG_PATHTEAR_MSG_BIT;
            i4dumpDir1 <<= DEBUG_PATHTEAR_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_PERR:
            i4dumpDir <<= DEBUG_PATHERR_MSG_BIT;
            i4dumpDir1 <<= DEBUG_PATHERR_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_RESV:
            i4dumpDir <<= DEBUG_RESV_MSG_BIT;
            i4dumpDir1 <<= DEBUG_RESV_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_RTEAR:
            i4dumpDir <<= DEBUG_RESVTEAR_MSG_BIT;
            i4dumpDir1 <<= DEBUG_RESVTEAR_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_RERR:
            i4dumpDir <<= DEBUG_RESVERR_MSG_BIT;
            i4dumpDir1 <<= DEBUG_RESVERR_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_CNFRM:
            i4dumpDir <<= DEBUG_RESVCONF_MSG_BIT;
            i4dumpDir1 <<= DEBUG_RESVCONF_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_HELLO:
            i4dumpDir <<= DEBUG_HELLO_MSG_BIT;
            i4dumpDir1 <<= DEBUG_HELLO_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_BUNDLE:
            i4dumpDir <<= DEBUG_BUNDLE_MSG_BIT;
            i4dumpDir1 <<= DEBUG_BUNDLE_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_ACK:
            i4dumpDir <<= DEBUG_ACK_MSG_BIT;
            i4dumpDir1 <<= DEBUG_ACK_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_SRFRSH:
            i4dumpDir <<= DEBUG_SREFRESH_MSG_BIT;
            i4dumpDir1 <<= DEBUG_SREFRESH_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_NOTIFY:
            i4dumpDir <<= DEBUG_NOTIFY_MSG_BIT;
            i4dumpDir1 <<= DEBUG_NOTIFY_MSG_BIT;
            break;

        case MPLS_CLI_RPTE_MSG_RECOVERY_PATH:
            i4dumpDir <<= DEBUG_RECOVERY_PATH_MSG_BIT;
            i4dumpDir1 <<= DEBUG_RECOVERY_PATH_MSG_BIT;
            break;
        default:
            break;
    }

    /* change the other dump messages direction
     * if message direction is set for all   */
    if (MPLS_CLI_RPTE_MSG_ALL == i4MsgType)
    {
        for (i4MsgCount = RSVPTE_TWO; i4MsgCount <= DEBUG_RSVP_ALL_MSG_BIT;
             i4MsgCount += RSVPTE_TWO)
        {
            i4NewDumpDir |= (i4dumpDir << i4MsgCount);
        }
        i4dumpDir = i4NewDumpDir;
    }

    /* Set the new dump message direction type */
    i4NewDumpDir = i4OldDumpDir & (~i4dumpDir);

    /* Set the dump message direction type */
    if (SNMP_FAILURE == nmhSetFsMplsRsvpTeGenPduDumpDirection (i4NewDumpDir))
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to reset Debug Message Dump Direction\r\n");
        return CLI_FAILURE;
    }

    switch (i4MsgType)            /* default type is all */
    {
        case MPLS_CLI_RPTE_MSG_ACK:
            i4MsgType = RSVPTE_DUMP_ACK;
            break;

        case MPLS_CLI_RPTE_MSG_BUNDLE:
            i4MsgType = RSVPTE_DUMP_BUNDLE;
            break;

        case MPLS_CLI_RPTE_MSG_CNFRM:
            i4MsgType = RSVPTE_DUMP_RCNFM;
            break;

        case MPLS_CLI_RPTE_MSG_HELLO:
            i4MsgType = RSVPTE_DUMP_HELLO;
            break;

        case MPLS_CLI_RPTE_MSG_PATH:
            i4MsgType = RSVPTE_DUMP_PATH;
            break;

        case MPLS_CLI_RPTE_MSG_PERR:
            i4MsgType = RSVPTE_DUMP_PERR;
            break;

        case MPLS_CLI_RPTE_MSG_PTEAR:
            i4MsgType = RSVPTE_DUMP_PTEAR;
            break;

        case MPLS_CLI_RPTE_MSG_RESV:
            i4MsgType = RSVPTE_DUMP_RESV;
            break;

        case MPLS_CLI_RPTE_MSG_RERR:
            i4MsgType = RSVPTE_DUMP_RERR;
            break;

        case MPLS_CLI_RPTE_MSG_RTEAR:
            i4MsgType = RSVPTE_DUMP_RTEAR;
            break;

        case MPLS_CLI_RPTE_MSG_SRFRSH:
            i4MsgType = RSVPTE_DUMP_SREFRESH;
            break;

        case MPLS_CLI_RPTE_MSG_ALL:
            i4MsgType = RSVPTE_DUMP_ALL;
            break;

        default:
            i4MsgType = RSVPTE_DUMP_NONE;
            break;
    }

    if (i4MsgType != RSVPTE_DUMP_NONE)
    {
        /* Disable the option selected only */
        if (nmhGetFsMplsRsvpTeGenPduDumpMsgType (&i4OldMsgType) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to get Debug Message Dump Type\r\n");
            return CLI_FAILURE;
        }

        /* reset the other dump messages type
         * if message type is receive for all RSVP messages */

        /* reset the all message type if rsvp message type is all selected  
         * and direction receive is both*/
        if ((RSVPTE_DUMP_ALL == i4MsgType) && (RPTE_ZERO == i4NewDumpDir))
        {
            i4MsgType = i4OldMsgType & (~i4MsgType);
        }
        /* reset the all message type if rsvp message type is all selected
         * and direction receive is not both*/
        else if (RSVPTE_DUMP_ALL == i4MsgType)
        {
            for (i4MsgCount = RSVPTE_TWO; i4MsgCount <= DEBUG_RSVP_ALL_MSG_BIT;
                 i4MsgCount += RSVPTE_TWO)
            {
                /* if message type is path msg */
                if (i4MsgCount != RSVPTE_TWO)
                {
                    i4NewMsgType = i4NewMsgType * RSVPTE_TWO;
                }
                /* reset the msg type if direction is set none */
                if (0 == (i4NewDumpDir & (i4DumpDir << i4MsgCount)))
                {
                    i4MsgType = i4OldMsgType & (~i4NewMsgType);
                }
            }

        }
        /* reset the message type if direction is none */
        else if ((RSVPTE_DUMP_ALL != i4MsgType)
                 && (0 == (i4NewDumpDir & (i4dumpDir1))))
        {
            i4MsgType = i4OldMsgType & (~i4MsgType);
        }
        else
        {
            i4MsgType = i4OldMsgType;
        }

        if (nmhSetFsMplsRsvpTeGenPduDumpMsgType (i4MsgType) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to reset Debug Message Dump Type\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliDbgSetDumpMsgLvl
 * Description   : Configures the rsvp messages dump level for
 *                 debugging
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4DumpLvl - O/p volume i.e min / max in other words br/de
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliDbgSetDumpMsgLvl (tCliHandle CliHandle, INT4 i4DumpLvl)
{

    UNUSED_PARAM (CliHandle);

    if (i4DumpLvl == MPLS_CLI_BRIEF)
    {

        if (nmhSetFsMplsRsvpTeGenPduDumpLevel (i4DumpLvl) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to Set Debug Dump Message Brief level\r\n");
            return CLI_FAILURE;
        }
    }
    else if (i4DumpLvl == MPLS_CLI_DETAIL)
    {

        if (nmhSetFsMplsRsvpTeGenPduDumpLevel (i4DumpLvl) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to Set Debug Dump Message Detail level\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliDbgReSetDumpMsgLvl
 * Description   : Disable the rsvp messages dump level for
 *                 debugging
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4DumpLvl - O/p volume i.e min / max in other words br/de
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliDbgReSetDumpMsgLvl (tCliHandle CliHandle, INT4 i4DumpLvl)
{
    INT4                i4Level = RPTE_ZERO;

    UNUSED_PARAM (CliHandle);

    if (i4DumpLvl == MPLS_CLI_DETAIL)
    {
        i4Level = MPLS_CLI_BRIEF;
        if (nmhSetFsMplsRsvpTeGenPduDumpLevel (i4Level) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to Reset Debug Dump Message Detail level\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliShowRsvpMsgCounters
 * Description   : Displays the interface wise various RSVP messages counters
 * Input(s)      : CliHandle - Index of current CLI context
 *                 u4IfIndex - RSVP interface index
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowRsvpMsgCounters (tCliHandle CliHandle, UINT4 u4ifIndex)
{
    tIfEntry           *pIfEntry = NULL;
    CHR1               *pc1Str;
    CHR1                ac1Str[CLI_MAX_COLUMN_WIDTH];

    pc1Str = &ac1Str[0];

    /* Check Whether the interface specified by u4IfIndex is rsvp enabled */
    if (RpteGetIfEntry ((INT4) u4ifIndex, &pIfEntry) == RPTE_FAILURE)
    {
        return CLI_FAILURE;
    }

    CfaCliGetIfName (u4ifIndex, (INT1 *) pc1Str);

    CliPrintf (CliHandle, "\r\n%-10s %-12s %-12s %-12s %-12s %-12s\r\n",
               pc1Str, "Recv", "Xmit", "", "Recv", "Xmit");

    CliPrintf (CliHandle, "%-10s %-12u %-12u %-12s %-12u %-12u\r\n",
               "Path",
               RPTE_IF_STAT_ENTRY_PATH_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_PATH_SENT (pIfEntry),
               "Resv",
               RPTE_IF_STAT_ENTRY_RESV_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_RESV_SENT (pIfEntry));

    CliPrintf (CliHandle, "%-10s %-12u %-12u %-12s %-12u %-12u\r\n",
               "PathError",
               RPTE_IF_STAT_ENTRY_PATH_ERR_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_PATH_ERR_SENT (pIfEntry),
               "ResvError",
               RPTE_IF_STAT_ENTRY_RESV_ERR_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_RESV_ERR_SENT (pIfEntry));

    CliPrintf (CliHandle, "%-10s %-12u %-12u %-12s %-12u %-12u\r\n",
               "PathTear",
               RPTE_IF_STAT_ENTRY_PATH_TEAR_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_PATH_TEAR_SENT (pIfEntry),
               "ResvTear",
               RPTE_IF_STAT_ENTRY_RESV_TEAR_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_RESV_TEAR_SENT (pIfEntry));

    CliPrintf (CliHandle, "%-10s %-12u %-12u %-12s %-12u %-12u\r\n",
               "Hello",
               RPTE_IF_STAT_ENTRY_NUM_HELLO_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_NUM_HELLO_SENT (pIfEntry),
               "ResvConfirm",
               RPTE_IF_STAT_ENTRY_RESV_CONF_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_RESV_CONF_SENT (pIfEntry));

    CliPrintf (CliHandle, "%-10s %-12u %-12u %-12s %-12u %-12u\r\n",
               "Bundle",
               RPTE_IF_STAT_ENTRY_BUNDLE_MSG_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_BUNDLE_MSG_SENT (pIfEntry),
               "SRefresh",
               RPTE_IF_STAT_ENTRY_SREFRESH_MSG_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_SREFRESH_MSG_SENT (pIfEntry));

    CliPrintf (CliHandle, "%-10s %-12u \r\n",
               "Discard", RPTE_IF_STAT_ENTRY_PKT_DISCRDED (pIfEntry));

    CliPrintf (CliHandle, "%-10s %-12u %-12u\r\n",
               "TotalMsgs",
               RPTE_IF_STAT_ENTRY_NUM_MSG_RCVD (pIfEntry),
               RPTE_IF_STAT_ENTRY_NUM_MSG_SENT (pIfEntry));

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliClearRsvpMsgCounters
 * Description   : Clear the interface wise various RSVP messages counters
 * Input(s)      : CliHandle - Index of current CLI context
 *                 u4IfIndex - RSVP interface index
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
RpteCliClearRsvpMsgCounters (tCliHandle CliHandle, UINT4 u4ifIndex)
{
    tIfEntry           *pIfEntry = NULL;
    UNUSED_PARAM (CliHandle);

    /* Check Whether the interface specified by u4IfIndex is rsvp enabled */
    if (RpteGetIfEntry ((INT4) u4ifIndex, &pIfEntry) == RPTE_FAILURE)
    {
        return CLI_FAILURE;
    }
    RPTE_IF_STAT_ENTRY_PATH_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_PATH_SENT (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_RESV_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_RESV_SENT (pIfEntry) = 0;

    RPTE_IF_STAT_ENTRY_PATH_ERR_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_PATH_ERR_SENT (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_RESV_ERR_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_RESV_ERR_SENT (pIfEntry) = 0;

    RPTE_IF_STAT_ENTRY_PATH_TEAR_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_PATH_TEAR_SENT (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_RESV_TEAR_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_RESV_TEAR_SENT (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_NUM_HELLO_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_NUM_HELLO_SENT (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_RESV_CONF_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_RESV_CONF_SENT (pIfEntry) = 0;

    RPTE_IF_STAT_ENTRY_BUNDLE_MSG_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_BUNDLE_MSG_SENT (pIfEntry) = 0;

    RPTE_IF_STAT_ENTRY_SREFRESH_MSG_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_SREFRESH_MSG_SENT (pIfEntry) = 0;

    RPTE_IF_STAT_ENTRY_NUM_MSG_RCVD (pIfEntry) = 0;
    RPTE_IF_STAT_ENTRY_NUM_MSG_SENT (pIfEntry) = 0;

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliShowHelloInstInfo
 * Description   : Calls appropriate function based on display type to show
 *                 the hello instance information
 * Input(s)      : CliHandle - Index of current CLI context
 *                 u4NbrAddr - = 0 implies display all neighbors
 *                             > 0 implies display particular
 *                 i4DispType - Brief or detail
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowHelloInstInfo (tCliHandle CliHandle, UINT4 u4NbrAddr,
                          INT4 i4DispType)
{
    tIfEntry           *pIfEntry = NULL;
    tTMO_SLL           *pNbrList = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    INT4                i4IfIdx1;
    INT4                i4IfIdx2;
    BOOL1               b1TitleFlag = TRUE;

    if (RSVPTE_HELLO_SPRTD (gpRsvpTeGblInfo) != RPTE_TRUE)
    {
        return CLI_SUCCESS;
    }

    if (u4NbrAddr != 0)
    {
        if (RpteGetNbrEntryFromNbrAddr (u4NbrAddr, &pNbrEntry) == RPTE_FAILURE)
        {
            return CLI_SUCCESS;
        }

        if (i4DispType == MPLS_CLI_BRIEF)
        {
            CliPrintf (CliHandle, "\r\nNeighbor \t Type \t\t State \t\t "
                       "Interface\r\n");
            CliPrintf (CliHandle, "------------ \t --------- \t ---------- \t "
                       "---------\r\n");
            return RpteCliShowHelloInstInfoBrief (CliHandle, pNbrEntry);
        }
        else
        {
            return RpteCliShowHelloInstInfoDetail (CliHandle, pNbrEntry);
        }
    }

    if (nmhGetFirstIndexFsMplsRsvpTeIfStatsTable (&i4IfIdx1) == SNMP_FAILURE)
    {
        /* Nothing to be displayed */
        return CLI_SUCCESS;
    }

    do
    {
        i4IfIdx2 = i4IfIdx1;
        if (RpteGetIfEntry (i4IfIdx1, &pIfEntry) == RPTE_FAILURE)
        {
            return CLI_SUCCESS;
        }
        pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));

        TMO_SLL_Scan (pNbrList, pNbrEntry, tNbrEntry *)
        {
            if (i4DispType == MPLS_CLI_BRIEF)
            {
                if (b1TitleFlag)
                {
                    CliPrintf (CliHandle,
                               "\r\nNeighbor \t Type \t\t State \t\t "
                               "Interface\r\n");
                    CliPrintf (CliHandle,
                               "------------ \t --------- \t ---------- \t "
                               "---------\r\n");
                    b1TitleFlag = FALSE;
                }
                RpteCliShowHelloInstInfoBrief (CliHandle, pNbrEntry);
            }
            else
            {
                RpteCliShowHelloInstInfoDetail (CliHandle, pNbrEntry);
                CliPrintf (CliHandle, "\r\n");    /* Paragraphs Seperator */
            }
        }
    }
    while (nmhGetNextIndexFsMplsRsvpTeIfStatsTable (i4IfIdx2, &i4IfIdx1)
           != SNMP_FAILURE);

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliShowHelloInstInfoBrief
 * Description   : Displays the RSVP neighbors hello information in tabular
 *                 format
 * Input(s)      : CliHandle - Index of current CLI context
 *                 pNbrEntry - Pointer to a row in neighbor table
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowHelloInstInfoBrief (tCliHandle CliHandle, tNbrEntry * pNbrEntry)
{
    CHR1               *pc1Str;
    CHR1                ac1Str[CLI_MAX_COLUMN_WIDTH];
    tHelloNbrInfo      *pHelloNbrInfo;

    pHelloNbrInfo = &(pNbrEntry->HelloNbrInfo);

    CLI_CONVERT_IPADDR_TO_STR (pc1Str, NBR_ENTRY_ADDR (pNbrEntry));
    CliPrintf (CliHandle, "%s \t ", pc1Str);

    CliPrintf (CliHandle, "%s \t ",
               MPLS_CLI_HELLO_REL (pHelloNbrInfo->u1HelloRel));

    switch (pHelloNbrInfo->u1HelloState)
    {
        case STATUS_UNKNOWN:
            CliPrintf (CliHandle, "Unknown \t ");
            break;

        case HELLO_NOT_SUPPRT:
            CliPrintf (CliHandle, "Not Supported \r\n");
            break;

        case HELLO_SUPPRT:
            CliPrintf (CliHandle, "Supported \t ");
            break;

        case HELLO_SUPPRT_RESET:
            CliPrintf (CliHandle, "Reset \t ");
            break;

        default:
            CliPrintf (CliHandle, "\t - \t ");
    }

    pc1Str = &ac1Str[0];
    CfaCliGetIfName (IF_ENTRY_IF_INDEX (NBR_ENTRY_IF_ENTRY (pNbrEntry)),
                     (INT1 *) pc1Str);

    CliPrintf (CliHandle, "%s \r\n", pc1Str);

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliShowHelloInstInfoDetail
 * Description   : Displays the detailed RSVP neighbors hello information in
 *                 paragraph style
 * Input(s)      : CliHandle - Index of current CLI context
 *                 pNbrEntry - Pointer to a row in neighbor table
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowHelloInstInfoDetail (tCliHandle CliHandle, tNbrEntry * pNbrEntry)
{
    CHR1               *pc1Str = NULL;
    CHR1                ac1Str[CLI_MAX_COLUMN_WIDTH];
    tIpConfigInfo       IpIfInfo;
    tHelloNbrInfo      *pHelloNbrInfo;
    INT4                i4Var1;
    UINT4               u4Var1 = RPTE_ZERO;

    pHelloNbrInfo = &(pNbrEntry->HelloNbrInfo);

    i4Var1 = (INT4) IF_ENTRY_IF_INDEX (NBR_ENTRY_IF_ENTRY (pNbrEntry));
#ifdef CFA_WANTED
    if (CfaUtilGetL3IfFromMplsIf ((UINT4) i4Var1, &u4Var1, TRUE) == CFA_FAILURE)
    {
        return CLI_SUCCESS;
    }
#endif

    CLI_CONVERT_IPADDR_TO_STR (pc1Str, NBR_ENTRY_ADDR (pNbrEntry));
    CliPrintf (CliHandle, "\r\nNeighbour: %s \t ", pc1Str);

    if (CfaIpIfGetIfInfo (u4Var1, &IpIfInfo) != CFA_SUCCESS)
    {
        CliPrintf (CliHandle, "Source: 0.0.0.0 (Unknown)\r\n");
    }
    else
    {
        CLI_CONVERT_IPADDR_TO_STR (pc1Str, IpIfInfo.u4Addr);
        CliPrintf (CliHandle, "Source: %s (MPLS)\r\n", pc1Str);
    }

    CliPrintf (CliHandle, "State: ");
    switch (pHelloNbrInfo->u1HelloState)
    {
        case STATUS_UNKNOWN:
            CliPrintf (CliHandle, "Unknown \r\n");
            break;

        case HELLO_NOT_SUPPRT:
            CliPrintf (CliHandle, "Not Supported \r\n");
            break;

        case HELLO_SUPPRT:
            CliPrintf (CliHandle, "Supported \r\n");
            break;

        case HELLO_SUPPRT_RESET:
            CliPrintf (CliHandle, "Reset \r\n");
            break;

        default:
            CliPrintf (CliHandle, "\t - \r\n");
    }

    CliPrintf (CliHandle, "Type: ");
    if (pHelloNbrInfo->u1HelloRel == RPTE_NBR_HELLO_REL_ACTIVE)
    {
        CliPrintf (CliHandle, "Active (Sending requests)\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Passive (Waiting for requests)\r\n");
    }

    pc1Str = &ac1Str[0];
    CfaCliGetIfName (IF_ENTRY_IF_INDEX (NBR_ENTRY_IF_ENTRY (pNbrEntry)),
                     (INT1 *) pc1Str);
    CliPrintf (CliHandle, "I/F: %s \r\n", pc1Str);

    CliPrintf (CliHandle, "Hello interval (seconds) (used when ACTIVE)\r\n");
    if (nmhGetFsMplsRsvpTeHelloIntervalTime (&i4Var1) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to get RsvpTeHelloIntervalTime\r\n");
    }
    CliPrintf (CliHandle, "Configured: %d \r\n", i4Var1);

    CliPrintf (CliHandle, "Src_Instance 0x%x, Dst_Instance 0x%x \r\n",
               pHelloNbrInfo->u4SrcIns, pHelloNbrInfo->u4DstIns);

    CliPrintf (CliHandle, "Counters:\r\n");
    CliPrintf (CliHandle, "Msgs Received: %d \r\nSent: %d \r\n",
               RPTE_IF_STAT_ENTRY_NUM_HELLO_RCVD (NBR_ENTRY_IF_ENTRY
                                                  (pNbrEntry)),
               RPTE_IF_STAT_ENTRY_NUM_HELLO_SENT (NBR_ENTRY_IF_ENTRY
                                                  (pNbrEntry)));

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliShowNeighborInfo
 * Description   : Branches to the appropriate to function to print the
 *                 neighbor details
 * Input(s)      : CliHandle - Index of current CLI context
 *                 u4NbrAddr - = 0 implies display all neighbors
 *                             > 0 implies display particular
 *                 i4DispType - Brief or detail
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowNeighborInfo (tCliHandle CliHandle, UINT4 u4NbrAddr, INT4 i4DispType)
{
    tIfEntry           *pIfEntry = NULL;
    tTMO_SLL           *pNbrList = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    INT4                i4IfIdx1;
    INT4                i4IfIdx2;
    BOOL1               b1TitleFlag = TRUE;

    if (u4NbrAddr != 0)
    {
        if (RpteGetNbrEntryFromNbrAddr (u4NbrAddr, &pNbrEntry) == RPTE_FAILURE)
        {
            return CLI_SUCCESS;
        }

        if (i4DispType == MPLS_CLI_BRIEF)
        {
            CliPrintf (CliHandle, "\r\nNeighbor \t RR State \t "
                       "RMD Status \t Hello State \t No.of Tnls");
            CliPrintf (CliHandle, "\r\n---------------  ------------- \t "
                       "---------- \t ------------- \t ----------\r\n");
            return RpteCliShowNeighborInfoBrief (CliHandle, pNbrEntry);
        }
        else
        {
            return RpteCliShowNeighborInfoDetail (CliHandle, pNbrEntry);
        }
    }

    if (nmhGetFirstIndexFsMplsRsvpTeIfStatsTable (&i4IfIdx1) == SNMP_FAILURE)
    {
        /* Nothing to be displayed */
        return CLI_SUCCESS;
    }

    do
    {
        i4IfIdx2 = i4IfIdx1;
        if (RpteGetIfEntry (i4IfIdx1, &pIfEntry) == RPTE_FAILURE)
        {
            return CLI_SUCCESS;
        }
        pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));

        TMO_SLL_Scan (pNbrList, pNbrEntry, tNbrEntry *)
        {
            if (i4DispType == MPLS_CLI_BRIEF)
            {
                if (b1TitleFlag)
                {
                    CliPrintf (CliHandle, "\r\nNeighbor \t RR State \t "
                               "RMD Status \t Hello State \t No.of Tnls");
                    CliPrintf (CliHandle,
                               "\r\n---------------  ------------- \t "
                               "---------- \t ------------- \t ----------\r\n");
                    b1TitleFlag = FALSE;
                }
                RpteCliShowNeighborInfoBrief (CliHandle, pNbrEntry);
            }
            else
            {
                RpteCliShowNeighborInfoDetail (CliHandle, pNbrEntry);
            }
        }
    }
    while (nmhGetNextIndexFsMplsRsvpTeIfStatsTable (i4IfIdx2, &i4IfIdx1)
           != SNMP_FAILURE);

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliShowNeighborInfoBrief
 * Description   : Displays the RSVP neighbors information in tabular format
 * Input(s)      : CliHandle - Index of current CLI context
 *                 pNbrEntry - Pointer to a row in neighbor table
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowNeighborInfoBrief (tCliHandle CliHandle, tNbrEntry * pNbrEntry)
{
    CHR1               *pc1Str = NULL;
    CHR1                ac1Str[CLI_MAX_COLUMN_WIDTH];

    pc1Str = &ac1Str[0];

    CLI_CONVERT_IPADDR_TO_STR (pc1Str, NBR_ENTRY_ADDR (pNbrEntry));
    CliPrintf (CliHandle, "%s \t ", pc1Str);

    if (NBR_ENTRY_RR_CAPABLE (pNbrEntry) == RPTE_RR_CAP_ENA)
    {
        if (NBR_ENTRY_RR_STATE (pNbrEntry) == RPTE_RR_ENABLED)
        {
            CliPrintf (CliHandle, "Enabled \t ");
        }
        else
        {
            CliPrintf (CliHandle, "Disabled \t ");
        }
    }
    else
    {
        CliPrintf (CliHandle, "Not Supported \t ");
    }

    if (NBR_ENTRY_RMD_CAPABLE (pNbrEntry) == RPTE_RMD_CAP_ENA)
    {
        CliPrintf (CliHandle, "Enabled \t ");
    }
    else
    {
        CliPrintf (CliHandle, "Disabled \t ");
    }

    if (NBR_ENTRY_HELLO_SPRT (pNbrEntry) == RPTE_NBR_HELLO_SPRT_ENA)
    {
        switch (NBR_ENTRY_HELLO_STATE (pNbrEntry))
        {
            case STATUS_UNKNOWN:
                CliPrintf (CliHandle, "Unknown \t ");
                break;

            case HELLO_NOT_SUPPRT:
                CliPrintf (CliHandle, "Disabled \t ");
                break;

            case HELLO_SUPPRT_RESET:
                CliPrintf (CliHandle, "Reset \t ");
                break;

            case HELLO_SUPPRT:
                CliPrintf (CliHandle, "Enabled \t ");
                break;

            default:
                CliPrintf (CliHandle, "\t - \t ");
                break;
        }
    }
    else
    {
        CliPrintf (CliHandle, "Not Supported \t ");
    }

    CliPrintf (CliHandle, "%d \r\n", NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry));

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliShowNeighborInfoDetail
 * Description   : Displays the detailed RSVP neighbors information in paras
 * Input(s)      : CliHandle - Index of current CLI context
 *                 pNbrEntry - Pointer to a row in neighbor table
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowNeighborInfoDetail (tCliHandle CliHandle, tNbrEntry * pNbrEntry)
{
    CHR1               *pc1Str = NULL;
    CHR1                ac1Str[CLI_MAX_COLUMN_WIDTH];
    tIpConfigInfo       IpIfInfo;
    INT4                i4Var1;
    UINT4               u4Var1 = RPTE_ZERO;

    i4Var1 = (INT4) IF_ENTRY_IF_INDEX (NBR_ENTRY_IF_ENTRY (pNbrEntry));
#ifdef CFA_WANTED
    if (CfaUtilGetL3IfFromMplsIf ((UINT4) i4Var1, &u4Var1, TRUE) == CFA_FAILURE)
    {
        return CLI_SUCCESS;
    }
#endif

    CLI_CONVERT_IPADDR_TO_STR (pc1Str, NBR_ENTRY_ADDR (pNbrEntry));
    CliPrintf (CliHandle, "\r\nNeighbour: %s \r\n", pc1Str);

    if (CfaIpIfGetIfInfo (u4Var1, &IpIfInfo) != CFA_SUCCESS)
    {
        CliPrintf (CliHandle, "Interface Neighbor: 0.0.0.0 (Unknown)\r\n");
    }
    else
    {
        CLI_CONVERT_IPADDR_TO_STR (pc1Str, IpIfInfo.u4Addr);
        CliPrintf (CliHandle, "Interface Neighbor: %s \r\n", pc1Str);
    }

    pc1Str = &ac1Str[0];
    CfaCliGetIfName (IF_ENTRY_IF_INDEX (NBR_ENTRY_IF_ENTRY (pNbrEntry)),
                     (INT1 *) pc1Str);
    CliPrintf (CliHandle, "Interface: %s \r\n", pc1Str);

    CliPrintf (CliHandle, "Refresh Reduction: ");
    if (NBR_ENTRY_RR_CAPABLE (pNbrEntry) == RPTE_RR_CAP_ENA)
    {
        if (NBR_ENTRY_RR_STATE (pNbrEntry) == RPTE_RR_ENABLED)
        {
            CliPrintf (CliHandle, "Enabled\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Disabled\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "Not Supported\r\n");
    }

    CliPrintf (CliHandle, "Remote epoch: 0x%x \r\n",
               NBR_ENTRY_EPOCH_VALUE (pNbrEntry));

    RpteCliDisplayNbrGrConfigurations (CliHandle, pNbrEntry);

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliShowPathReq
 * Description   : Displays requests or reservations states for the
 *                 reservation messages sent upstream.
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4DispType - Brief or detail
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowPathReq (tCliHandle CliHandle, INT4 i4DispType)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    BOOL1               b1TitleFlag = TRUE;

    pRsvpTeTnlInfo = RBTreeGetFirst (gpRsvpTeGblInfo->RpteTnlTree);
    if (i4DispType == MPLS_CLI_BRIEF)
    {
        while (pRsvpTeTnlInfo != NULL)
        {
            if (RPTE_TE_TNL (pRsvpTeTnlInfo) == NULL)
            {
                pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                                (tRBElem *) pRsvpTeTnlInfo,
                                                NULL);
                continue;
            }
            if (b1TitleFlag)
            {
                CliPrintf (CliHandle,
                           "\r\nDest Addr        Source Addr      Pro  "
                           "O/p Intf       Sty  Serv   Rate");
                CliPrintf (CliHandle,
                           "\r\n---------------  ---------------  ---  "
                           "-------------  ---  ----   ----\r\n");
                b1TitleFlag = FALSE;
            }
            RpteCliShowPathReqBrief (CliHandle, pRsvpTeTnlInfo);
            pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                            (tRBElem *) pRsvpTeTnlInfo, NULL);
        }
    }
    else                        /* Detailed display */
    {
        while (pRsvpTeTnlInfo != NULL)
        {
            if ((RPTE_TE_TNL (pRsvpTeTnlInfo) == NULL) ||
                (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL))
            {
                pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                                (tRBElem *) pRsvpTeTnlInfo,
                                                NULL);
                continue;
            }
            RpteCliShowPathReqDetail (CliHandle, pRsvpTeTnlInfo);
            pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                            (tRBElem *) pRsvpTeTnlInfo, NULL);
        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliShowPathReqBrief
 * Description   : Displays requests or reservations states for the
 *                 reservation messages sent upstream.
 * Input(s)      : CliHandle - Index of current CLI context
 *                 pRsvpTeTnlInfo - RSVP tunnel information
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowPathReqBrief (tCliHandle CliHandle, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    CHR1               *pc1Str = NULL;
    CHR1                ac1Str[CLI_MAX_COLUMN_WIDTH];
    UINT4               u4Var1;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MEMSET (ac1Str, 0, CLI_MAX_COLUMN_WIDTH);
    pTeTnlInfo = RPTE_TE_TNL (pRsvpTeTnlInfo);

    u4Var1 = OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));
    CLI_CONVERT_IPADDR_TO_STR (pc1Str, u4Var1);
    CliPrintf (CliHandle, "%-15s  ", pc1Str);

    u4Var1 = OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo));
    CLI_CONVERT_IPADDR_TO_STR (pc1Str, u4Var1);
    CliPrintf (CliHandle, "%-15s  ", pc1Str);

    CliPrintf (CliHandle, "%s  ",
               ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_AVAIL) ==
                LOCAL_PROT_AVAIL) ? "Ena" : "Dis");

    if (TE_TNL_IFINDEX (pTeTnlInfo) != 0)
    {
        pc1Str = &ac1Str[0];
        CfaCliGetIfName (TE_TNL_IFINDEX (pTeTnlInfo), (INT1 *) pc1Str);
        CliPrintf (CliHandle, "%-13s  ", pc1Str);
    }
    else
    {
        CliPrintf (CliHandle, "%-13s  ", "-");
    }

    if (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL)
    {
        if ((STYLE_OPT_VECT (&RPTE_TNL_STYLE (pRsvpTeTnlInfo))
             [RSVPTE_TWO] & STYLE_MASK) == RPTE_SE)
        {
            CliPrintf (CliHandle, "SE   ");
        }
        else if ((STYLE_OPT_VECT (&RPTE_TNL_STYLE (pRsvpTeTnlInfo))
                  [RSVPTE_TWO] & STYLE_MASK) == RPTE_FF)
        {
            CliPrintf (CliHandle, "FF   ");
        }
        else
        {
            CliPrintf (CliHandle, "WF   ");
        }
        if (FLOW_SPEC_OBJ (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)).SrvHdr.u1SrvID ==
            FLOWSPEC_CLS_HDR_NUMBER)
        {
            CliPrintf (CliHandle, "LOAD   ");
        }
        else
        {
            CliPrintf (CliHandle, "RATE   ");
        }
    }
    else
    {
        CliPrintf (CliHandle, "Unknown ");
        CliPrintf (CliHandle, "Unknown ");
    }
    pTeTrfcParams = TE_TNL_TRFC_PARAM (pTeTnlInfo);

    if (pTeTrfcParams != NULL)
    {
        if (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            u4Var1 = TE_RSVPTE_TPARAM_TBR (pTeTrfcParams);
        }
        else
        {
            u4Var1 = 0;
        }

        CliPrintf (CliHandle, "%4u\r\n", u4Var1);
    }
    else
    {
        CliPrintf (CliHandle, "%4s\r\n", "-");
    }
    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliShowPathReqDetail
 * Description   : Displays requests or reservations states for the
 *                 reservation messages sent upstream.
 * Input(s)      : CliHandle - Index of current CLI context
 *                 pRsvpTeTnlInfo - RSVP tunnel information
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowPathReqDetail (tCliHandle CliHandle, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    CHR1               *pc1Str = NULL;
    CHR1                ac1Str[CLI_MAX_COLUMN_WIDTH];
    UINT4               u4Var1;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    MEMSET (ac1Str, 0, CLI_MAX_COLUMN_WIDTH);
    pTeTnlInfo = RPTE_TE_TNL (pRsvpTeTnlInfo);

    u4Var1 = OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));
    CLI_CONVERT_IPADDR_TO_STR (pc1Str, u4Var1);
    /* TODO Update to be done when IPv6-LSP support is added */
    CliPrintf (CliHandle, "\r\nREQ: IPv4-LSP Session addr: %s.  ", pc1Str);
    CliPrintf (CliHandle, "TunID: %d.  ", TE_TNL_TNL_INDEX (pTeTnlInfo));
    CliPrintf (CliHandle, "LSPId: %d. \r\n", TE_TNL_TNL_INSTANCE (pTeTnlInfo));

    u4Var1 = OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo));
    CLI_CONVERT_IPADDR_TO_STR (pc1Str, u4Var1);
    CliPrintf (CliHandle, "Source addr: %s.  ", pc1Str);
    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo)), u4Var1);
    u4Var1 = OSIX_NTOHL (u4Var1);
    CLI_CONVERT_IPADDR_TO_STR (pc1Str, u4Var1);
    CliPrintf (CliHandle, "ExtID: %s. \r\n", pc1Str);

    if (TE_TNL_IFINDEX (pTeTnlInfo) != 0)
    {
        pc1Str = &ac1Str[0];
        CfaCliGetIfName (TE_TNL_IFINDEX (pTeTnlInfo), (INT1 *) pc1Str);
        CliPrintf (CliHandle, "Output interface: %s.  ", pc1Str);
    }

    u4Var1 = RSVPTE_TNL_DN_NHOP (pRsvpTeTnlInfo);
    if (u4Var1 != 0)
    {
        CLI_CONVERT_IPADDR_TO_STR (pc1Str, u4Var1);
        CliPrintf (CliHandle, "Next hop: %s (lih: 0x%x).\r\n", pc1Str,
                   OSIX_NTOHL (RSVP_HOP_LIH (&(PSB_RSVP_HOP
                                               (RSVPTE_TNL_PSB
                                                (pRsvpTeTnlInfo))))));
    }

    CliPrintf (CliHandle, "Local Protection: %s. \r\n",
               ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_AVAIL) ==
                LOCAL_PROT_AVAIL) ? "Enabled" : "Not in use");

    if (RSVPTE_TNL_RSB (pRsvpTeTnlInfo) != NULL)
    {
        CliPrintf (CliHandle, "Reservation Style: ");
        if ((STYLE_OPT_VECT (&RPTE_TNL_STYLE (pRsvpTeTnlInfo))
             [RSVPTE_TWO] & STYLE_MASK) == RPTE_SE)
        {
            CliPrintf (CliHandle, "Shared-Explicit.  ");
        }
        else if ((STYLE_OPT_VECT (&RPTE_TNL_STYLE (pRsvpTeTnlInfo))
                  [RSVPTE_TWO] & STYLE_MASK) == RPTE_FF)
        {
            CliPrintf (CliHandle, "Fixed-Filter.  ");
        }
        else
        {
            CliPrintf (CliHandle, "Wildcard-Filter.  ");
        }
        if (FLOW_SPEC_OBJ (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)).SrvHdr.u1SrvID ==
            FLOWSPEC_CLS_HDR_NUMBER)
        {
            CliPrintf (CliHandle, "Service: Controlled-Load.\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Service: Guaranteed.\r\n");
        }
    }
    else
    {
        CliPrintf (CliHandle, "Reservation Style: Not yet reserved.\r\n");
    }

    pTeTrfcParams = TE_TNL_TRFC_PARAM (pTeTnlInfo);

    if (pTeTrfcParams != NULL)
    {
        if (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            u4Var1 = TE_RSVPTE_TPARAM_TBR (pTeTrfcParams);
        }
        else
        {
            u4Var1 = 0;
        }
        CliPrintf (CliHandle, "Token bucket rate: %u bits/sec.  ", u4Var1);

        if (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            u4Var1 = TE_RSVPTE_TPARAM_PDR (pTeTrfcParams);
        }
        else
        {
            u4Var1 = 0;
        }
        CliPrintf (CliHandle, "Peak data rate: %u bits/sec.\r\n", u4Var1);
    }
    pNbrEntry = RPTE_TNL_DSTR_NBR (pRsvpTeTnlInfo);
    if (pNbrEntry == NULL)
    {
        pNbrEntry = RPTE_TNL_USTR_NBR (pRsvpTeTnlInfo);
    }
    if (pNbrEntry != NULL)
    {
        CliPrintf (CliHandle, "MTU min: %d, max: %d bytes.\r\n",
                   RPTE_IF_MIN_MTU_SIZE, NBR_ENTRY_IF_MTU (pNbrEntry));
    }
    CliPrintf (CliHandle, "Number of supporting PSBs: 1\r\n");
    CliPrintf (CliHandle, "Number of supporting RSBs: 1\r\n");

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliShowSrcPathReq
 * Description   : Displays requests or reservations states for the
 *                 reservation messages sent upstream.
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4DispType - Brief or detail
 *                 u4Addr - Tunnel destination for which info is requested
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowSrcPathReq (tCliHandle CliHandle, INT4 i4DispType, UINT4 u4Addr)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    BOOL1               b1TitleFlag = TRUE;

    pRsvpTeTnlInfo = RBTreeGetFirst (gpRsvpTeGblInfo->RpteTnlTree);
    u4Addr = OSIX_HTONL (u4Addr);

    if (i4DispType == MPLS_CLI_BRIEF)
    {
        while (pRsvpTeTnlInfo != NULL)
        {
            if (RPTE_TE_TNL (pRsvpTeTnlInfo) == NULL)
            {
                pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                                (tRBElem *) pRsvpTeTnlInfo,
                                                NULL);
                continue;
            }
            if ((UINT4) RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo) != u4Addr)
            {
                pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                                (tRBElem *) pRsvpTeTnlInfo,
                                                NULL);
                continue;
            }
            if (b1TitleFlag)
            {
                CliPrintf (CliHandle,
                           "\r\nDest Addr        Source Addr      Pro  "
                           "O/p Intf       Sty  Serv  Rate  Burst");
                CliPrintf (CliHandle,
                           "\r\n---------------  ---------------  ---  "
                           "-------------  ---  ----  ----  -----\r\n");
                b1TitleFlag = FALSE;
            }
            RpteCliShowPathReqBrief (CliHandle, pRsvpTeTnlInfo);
            pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                            (tRBElem *) pRsvpTeTnlInfo, NULL);
        }
    }
    else                        /* Detailed display */
    {
        while (pRsvpTeTnlInfo != NULL)
        {
            if ((RPTE_TE_TNL (pRsvpTeTnlInfo) == NULL) ||
                (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL))
            {
                pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                                (tRBElem *) pRsvpTeTnlInfo,
                                                NULL);
                continue;
            }
            if ((UINT4) RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo) != u4Addr)
            {
                pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                                (tRBElem *) pRsvpTeTnlInfo,
                                                NULL);
                continue;
            }
            RpteCliShowPathReqDetail (CliHandle, pRsvpTeTnlInfo);
            pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                            (tRBElem *) pRsvpTeTnlInfo, NULL);

        }
    }

    return (CLI_SUCCESS);
}

/*****************************************************************************
 * Function Name : RpteCliShowDestPathReq
 * Description   : Displays requests or reservations states for the
 *                 reservation messages sent upstream.
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4DispType - Brief or detail
 *                 u4Addr - Tunnel destination for which info is requested
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
RpteCliShowDestPathReq (tCliHandle CliHandle, INT4 i4DispType, UINT4 u4Addr)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    BOOL1               b1TitleFlag = TRUE;

    pRsvpTeTnlInfo = RBTreeGetFirst (gpRsvpTeGblInfo->RpteTnlTree);
    u4Addr = OSIX_HTONL (u4Addr);

    if (i4DispType == MPLS_CLI_BRIEF)
    {
        while (pRsvpTeTnlInfo != NULL)
        {
            if (RPTE_TE_TNL (pRsvpTeTnlInfo) == NULL)
            {
                pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                                (tRBElem *) pRsvpTeTnlInfo,
                                                NULL);
                continue;
            }
            if ((UINT4) RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo) != u4Addr)
            {
                pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                                (tRBElem *) pRsvpTeTnlInfo,
                                                NULL);
                continue;
            }
            if (b1TitleFlag)
            {
                CliPrintf (CliHandle,
                           "\r\nDest Addr        Source Addr      Pro  "
                           "O/p Intf       Sty  Serv  Rate  Burst");
                CliPrintf (CliHandle,
                           "\r\n---------------  ---------------  ---  "
                           "-------------  ---  ----  ----  -----\r\n");
                b1TitleFlag = FALSE;
            }
            RpteCliShowPathReqBrief (CliHandle, pRsvpTeTnlInfo);
            pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                            (tRBElem *) pRsvpTeTnlInfo, NULL);

        }
    }
    else                        /* Detailed display */
    {
        while (pRsvpTeTnlInfo != NULL)
        {
            if ((RPTE_TE_TNL (pRsvpTeTnlInfo) == NULL) ||
                (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL))
            {
                pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                                (tRBElem *) pRsvpTeTnlInfo,
                                                NULL);

                continue;
            }
            if ((UINT4) RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo) != u4Addr)

            {
                pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                                (tRBElem *) pRsvpTeTnlInfo,
                                                NULL);

                continue;
            }
            RpteCliShowPathReqDetail (CliHandle, pRsvpTeTnlInfo);
            pRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                            (tRBElem *) pRsvpTeTnlInfo, NULL);

        }
    }

    return (CLI_SUCCESS);
}

/******************************************************************************
* Function Name : TeCliFrrDetourMerging  
* Description   : This routine is used to enable or disable detour merging
* Input(s)      : CliHandle  - Cli Context Handle
*                 i4DeMerg   - Detour merging enable/disable. 
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/

PRIVATE INT4
TeCliFrrDetourMerging (tCliHandle CliHandle, INT4 i4DeMerg)
{
    UINT4               u4ErrorCode = RPTE_ZERO;
    /*Checking whether Detour merging is enabled or not */
    if (i4DeMerg == CLI_MPLS_FRR_DE_MER_ENABLE)
    {
        i4DeMerg = RPTE_SNMP_TRUE;
    }
    else
    {
        i4DeMerg = RPTE_SNMP_FALSE;
    }
    /*Checking whether detour-merge value is valid or not */
    if ((nmhTestv2FsMplsFrrDetourMergingEnabled (&u4ErrorCode, i4DeMerg))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Detour Merging cannot be configured\r\n");
        return CLI_FAILURE;
    }
    /*To set detour-merging value */
    if ((nmhSetFsMplsFrrDetourMergingEnabled (i4DeMerg)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Detour Merging cannot be configured \r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

 /******************************************************************************
* Function Name : TeCliFrrDetour  
* Description   : This routine is used to enable or disable Detour object 
*                 support.
* Input(s)      : CliHandle  - Cli Context Handle
*                 i4Detour   - Detour object support enable/disable. 
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
TeCliFrrDetour (tCliHandle CliHandle, INT4 i4Detour)
{
    UINT4               u4ErrorCode = RPTE_ZERO;
    /*Checkinh whether Detour support is enabled or not */
    if (i4Detour == CLI_MPLS_FRR_DE_ENABLE)
    {
        i4Detour = RPTE_SNMP_TRUE;
    }
    else
    {
        i4Detour = RPTE_SNMP_FALSE;
    }
    /*Checking whether detour value is valid or not */
    if ((nmhTestv2FsMplsFrrDetourEnabled (&u4ErrorCode, i4Detour))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Detour Support\r\n");
        return CLI_FAILURE;
    }
    /*To set detour value */
    if ((nmhSetFsMplsFrrDetourEnabled (i4Detour)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Detour Support\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrLocalRevertive  
* Description   : This routine is used to configure local reverive mode.
* Input(s)      : CliHandle  - Cli Context Handle.      
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
TeCliFrrLocalRevertive (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = RPTE_ZERO;
    tSNMP_OCTET_STRING_TYPE MplsFrrLocalRevertive;
    static UINT1        au1Length[RPTE_ONE] = { 0 };
    MplsFrrLocalRevertive.pu1_OctetList = au1Length;
    MplsFrrLocalRevertive.i4_Length = RPTE_ONE;
    MplsFrrLocalRevertive.pu1_OctetList[RPTE_ZERO] = RPTE_FRR_REVERTIVE_LOCAL;
    /*Checking whether local revertive value is valid or not */
    if ((nmhTestv2FsMplsFrrRevertiveMode (&u4ErrorCode,
                                          &MplsFrrLocalRevertive))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Local Revertive"
                   "behavior\r\n");
        if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)
        {
            CliPrintf (CliHandle, "\r%%Disable RSVP to configure the Local"
                       " Revertive behavior\r\n");
        }
        return CLI_FAILURE;
    }
    /*To set Local Revertive Support */
    if ((nmhSetFsMplsFrrRevertiveMode (&MplsFrrLocalRevertive)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Local Revertive"
                   "behavior\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrGlobalRevertive
* Description   : This routine is used to configure global revertive mode.
* Input(s)      : CliHandle  - Cli Context Handle. 
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
TeCliFrrGlobalRevertive (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = RPTE_ZERO;
    tSNMP_OCTET_STRING_TYPE MplsFrrGlobalRevertive;
    static UINT1        au1Length[RPTE_ONE] = { 0 };
    MplsFrrGlobalRevertive.pu1_OctetList = au1Length;
    MplsFrrGlobalRevertive.i4_Length = RPTE_ONE;
    MplsFrrGlobalRevertive.pu1_OctetList[RPTE_ZERO] = RPTE_FRR_REVERTIVE_GLOBAL;
    /*Checking whether global revertive value is valid or not */
    if ((nmhTestv2FsMplsFrrRevertiveMode (&u4ErrorCode,
                                          &MplsFrrGlobalRevertive))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Unable to configure Global Revertiv behavior\r\n");
        return CLI_FAILURE;
    }
    /*To set Global Revertive Support */
    if ((nmhSetFsMplsFrrRevertiveMode (&MplsFrrGlobalRevertive))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Global Revertive"
                   "behavior\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrCspf  
* Description   : This routine is used to configure cspf time interval
*                 and retry time interval for calculation of backup path.
* Input(s)      : CliHandle      - Cli Context Handle.
*                 i4TimeInterval - CSPF time interval.
*                 i4RetryCount   - CSPF retry count.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
TeCliFrrCspf (tCliHandle CliHandle, INT4 i4RetryTimeInterval,
              UINT4 u4RetryCount)
{
    UINT4               u4ErrorCode = RPTE_ZERO;
    /*Checking whether Retry Time Interval is within the 
     * specified range*/
    if ((nmhTestv2FsMplsFrrCspfRetryInterval (&u4ErrorCode,
                                              i4RetryTimeInterval))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Unable to configure Retry Time Interval\r\n");
        return CLI_FAILURE;
    }
    /*To Set the Retry Time Interval */
    if ((nmhSetFsMplsFrrCspfRetryInterval (i4RetryTimeInterval))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Retry Time"
                   "Interval\r\n");
        return CLI_FAILURE;
    }
    /*Checking whether Retry Count is within specified range */
    if ((nmhTestv2FsMplsFrrCspfRetryCount (&u4ErrorCode,
                                           u4RetryCount)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Retry Count\r\n");
        return CLI_FAILURE;
    }
    /*To Set the Retry Count */
    if ((nmhSetFsMplsFrrCspfRetryCount (u4RetryCount) == SNMP_FAILURE))
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Retry Count \r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrBackupTunnel  
* Description   : This routine is used to configure the backup path.
* Input(s)      : CliHandle  - Cli Context Handle
*                 i4TunnelNo - Tunnel id. 
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
TeCliFrrBackupTunnel (tCliHandle CliHandle, INT4 i4TunnelNo)
{
    UINT4               u4TunnelIndex = RPTE_ZERO;
    UINT4               u4IngressId = RPTE_ZERO;
    UINT4               u4EgressId = RPTE_ZERO;
    UINT4               u4ErrorCode = RPTE_ZERO;
    UINT4               u4IfIndex = RPTE_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    if (CfaUtilGetMplsIfFromIfIndex
        ((UINT4) CLI_GET_IFINDEX (), &u4IfIndex, TRUE) == CFA_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%MPLS is not enabled on this interface\r\n");
        return CLI_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTnlInfoByOwnerAndRole ((UINT4) i4TunnelNo, 0, 0, 0,
                                             RPTE_ZERO, RPTE_ZERO);

    if (pTeTnlInfo != NULL)
    {
        u4TunnelIndex = TE_TNL_TNL_INDEX (pTeTnlInfo);
        MEMCPY ((UINT1 *) (&u4IngressId),
                pTeTnlInfo->TnlIngressLsrId, ROUTER_ID_LENGTH);
        MEMCPY ((UINT1 *) (&u4EgressId),
                TE_TNL_EGRESS_LSRID (pTeTnlInfo), ROUTER_ID_LENGTH);
    }
    /* Tunnel does not present in TE */
    else
    {
        u4TunnelIndex = (UINT4) i4TunnelNo;
    }

    MPLS_CMN_UNLOCK ();

    if (pTeTnlInfo != NULL)
    {
        u4IngressId = OSIX_HTONL (u4IngressId);
        u4EgressId = OSIX_HTONL (u4EgressId);
    }

    MPLS_RSVPTE_UNLOCK ();
    /*Checking whether Protected Interface Index is valid or not */
    if ((nmhTestv2FsMplsBypassTunnelRowStatus (&u4ErrorCode,
                                               (INT4) u4IfIndex,
                                               u4TunnelIndex,
                                               u4IngressId, u4EgressId,
                                               CREATE_AND_GO)) == SNMP_FAILURE)
    {
        MPLS_RSVPTE_LOCK ();
        CliPrintf (CliHandle, "\r\n%%Unable to make bypass status up\r\n");
        return CLI_FAILURE;
    }
    /*To Set Protected Interface Index */
    if ((nmhSetFsMplsBypassTunnelRowStatus ((INT4) u4IfIndex,
                                            u4TunnelIndex,
                                            u4IngressId, u4EgressId,
                                            CREATE_AND_GO)) == SNMP_FAILURE)
    {
        MPLS_RSVPTE_LOCK ();
        CliPrintf (CliHandle, "\r\n%%Unable to make bypass status up\r\n");
        return CLI_FAILURE;
    }
    MPLS_RSVPTE_LOCK ();
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrNoBackupTunnel  
* Description   : This routine is used to configure the backup path.
* Input(s)      : CliHandle  - Cli Context Handle
*                 i4TunnelNo - Tunnel id. 
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
TeCliFrrNoBackupTunnel (tCliHandle CliHandle, INT4 i4TunnelNo)
{
    UINT4               u4TunnelIndex = RPTE_ZERO;
    UINT4               u4TunnelInstance = RPTE_ZERO;
    UINT4               u4IngressId = RPTE_ZERO;
    UINT4               u4EgressId = RPTE_ZERO;
    UINT4               u4ErrorCode = RPTE_ZERO;
    UINT4               u4IfIndex = RPTE_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    /* To get Mpls IfIndex */
    if (CfaUtilGetMplsIfFromIfIndex (CLI_GET_IFINDEX (), &u4IfIndex, TRUE)
        == CFA_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%MPLS is not enabled on this interface\r\n");
        return CLI_FAILURE;
    }

    MPLS_CMN_LOCK ();

    /* To Get TeTunnel Info */
    pTeTnlInfo = TeGetTnlInfoByOwnerAndRole ((UINT4) i4TunnelNo, 0, 0, 0,
                                             RPTE_ZERO, RPTE_ZERO);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return CLI_SUCCESS;
    }

    u4TunnelIndex = TE_TNL_TNL_INDEX (pTeTnlInfo);
    u4TunnelInstance = TE_TNL_TNL_INSTANCE (pTeTnlInfo);

    if (u4TunnelInstance == RPTE_ZERO)
    {
        u4TunnelInstance = TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo);
    }

    MEMCPY ((UINT1 *) (&u4IngressId),
            pTeTnlInfo->TnlIngressLsrId, ROUTER_ID_LENGTH);
    MEMCPY ((UINT1 *) (&u4EgressId),
            TE_TNL_EGRESS_LSRID (pTeTnlInfo), ROUTER_ID_LENGTH);

    MPLS_CMN_UNLOCK ();

    u4IngressId = OSIX_HTONL (u4IngressId);
    u4EgressId = OSIX_HTONL (u4EgressId);

    MPLS_RSVPTE_UNLOCK ();
    /*Checking whether Protected Interface Index is valid or not */
    if ((nmhTestv2FsMplsBypassTunnelRowStatus (&u4ErrorCode,
                                               (INT4) u4IfIndex,
                                               u4TunnelIndex,
                                               u4IngressId, u4EgressId,
                                               DESTROY)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%unable to destroy bypass tunnel \r\n");
        MPLS_RSVPTE_LOCK ();
        return CLI_FAILURE;
    }
    /*To Set Protected Interface Index */
    if ((nmhSetFsMplsBypassTunnelRowStatus ((INT4) u4IfIndex,
                                            u4TunnelIndex,
                                            u4IngressId, u4EgressId,
                                            DESTROY)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to destroy bypass tunnel\r\n");
        MPLS_RSVPTE_LOCK ();
        return CLI_FAILURE;
    }
    MPLS_RSVPTE_LOCK ();
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrPlrAndAvoidNodeId  
* Description   : This routine is used to configure PLR and Avoid address
* Input(s)      : CliHandle     - Cli Context Handle
*                 u4PlrId       - PLR address
*                 u4AvoidNodeId - Avoid address
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
TeCliFrrPlrAndAvoidNodeId (tCliHandle CliHandle, UINT4 u4PlrId,
                           UINT4 u4AvoidNodeId)
{
    static UINT1        au1PlrLength[RSVPTE_IPV4ADR_LEN] = { 0 };
    static UINT1        au1AvdLength[RSVPTE_IPV4ADR_LEN] = { 0 };
    tSNMP_OCTET_STRING_TYPE MplsRsvpTeIfPlrId;
    tSNMP_OCTET_STRING_TYPE MplsRsvpTeIfAvoidNodeId;
    UINT4               u4ErrorCode = RPTE_ZERO;

    if (u4PlrId != RPTE_ZERO)
    {
        /*Convert the peer address from integer to octet string */
        MplsRsvpTeIfPlrId.pu1_OctetList = &au1PlrLength[0];
        MPLS_INTEGER_TO_OCTETSTRING (u4PlrId, (&MplsRsvpTeIfPlrId));

        /*To configure PLR address */
        if (nmhTestv2FsMplsRsvpTeIfPlrId (&u4ErrorCode,
                                          CLI_GET_IFINDEX (),
                                          &MplsRsvpTeIfPlrId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to configure PLR-Id\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsMplsRsvpTeIfPlrId (CLI_GET_IFINDEX (),
                                       &MplsRsvpTeIfPlrId) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to configure PLR-Id\r\n");
            return CLI_FAILURE;
        }
    }
    if (u4AvoidNodeId != RPTE_ZERO)
    {
        /*Convert the peer address from integer to octet string */
        MplsRsvpTeIfAvoidNodeId.pu1_OctetList = &au1AvdLength[0];
        MPLS_INTEGER_TO_OCTETSTRING (u4AvoidNodeId, (&MplsRsvpTeIfAvoidNodeId));

        /*To configure Avoid Node address */
        if (nmhTestv2FsMplsRsvpTeIfAvoidNodeId (&u4ErrorCode,
                                                CLI_GET_IFINDEX (),
                                                &MplsRsvpTeIfAvoidNodeId) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure Avoid Node Id\r\n");
            return CLI_FAILURE;
        }
        if (nmhSetFsMplsRsvpTeIfAvoidNodeId (CLI_GET_IFINDEX (),
                                             &MplsRsvpTeIfAvoidNodeId) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure Avoid Node Id\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrMakeAfterBreak 
* Description   : This routine is used to configure Make-After-Break to protect 
*                 the tunnel.
* Input(s)      : CliHandle  - Cli Context Handle
*                 i4MakeAfterBreak - Enable or Disable make-after-break. . 
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
PRIVATE INT4
TeCliFrrMakeAfterBreak (tCliHandle CliHandle, INT4 i4MakeAfterBreak)
{
    UINT4               u4ErrorCode = RPTE_ZERO;
    INT4                i4Value = RPTE_ZERO;
    if (i4MakeAfterBreak == CLI_MPLS_FRR_MAKE_AFTER_BREAK_ENABLE)
    {
        i4Value = RPTE_SNMP_TRUE;
    }
    else
    {
        i4Value = RPTE_SNMP_FALSE;
    }
    if (nmhTestv2FsMplsFrrMakeAfterBreakEnabled (&u4ErrorCode,
                                                 i4Value) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure Make-After-Break\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsMplsFrrMakeAfterBreakEnabled (i4Value) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure Make-After-Break\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliSetRsvpTeOverRideOption
 * Description   : This routine is used to Set/Reset the RSVPTE OverRide
 *                 Option
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4Value - RsvpTe Overide Option Value
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
PRIVATE INT4
RpteCliSetRsvpTeOverRideOption (tCliHandle CliHandle, INT4 i4Value)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2FsMplsRsvpTeOverRideOption (&u4ErrorCode, i4Value)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure override option\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsRsvpTeOverRideOption (i4Value) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure override option\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliShowRunningConfig
 * Description   : This routine displays config commands for RSVPTE configuratio *                 ns
 * Input(s)      : CliHandle - Index of current CLI context
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 ******************************************************************************/
INT4
RpteCliRsvpTeShowRunnningConfig (tCliHandle CliHandle)
{
    RpteCliRsvpTeDebugShowRunningConfig (CliHandle);

    RpteCliRsvpTeGenShowRunningConfig (CliHandle);

    RpteCliRsvpTeIfShowRunningConfig (CliHandle);

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliRsvpTeGenShowRunningConfig
 * Description   : This routine displays config commands for scalar objects
 *                 under fsMplsRsvpTeGenObjects.
 * Input(s)      : CliHandle - Index of current CLI context
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 ******************************************************************************/

INT4
RpteCliRsvpTeGenShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4Operstatus = RPTE_OPER_DOWN;
    INT4                i4RefreshReduction = RPTE_RR_DISABLED;
    INT4                i4SockSupport = RPTE_TRUE;
    INT4                i4HelloSupport = RPTE_SNMP_FALSE;
    INT4                i4MessageIdSupport = RPTE_SNMP_FALSE;
    INT4                i4LabelSetSupport = RPTE_SNMP_FALSE;
    INT4                i4HelloIntervalTime = HELLO_REFRESH_INTERVAL;
    INT4                i4MaxTunnels = MAX_RSVPTE_TUNNEL;
    INT4                i4MaxErhops = MAX_RSVPTE_AR_HOP_INFO;
    INT4                i4MaxInterfaces = MAX_RSVPTE_IF_ENTRY;
    INT4                i4MaxNeighbours = MAX_RSVPTE_NBR_ENTRY;
    INT4                i4DiffServOption = TE_DS_OVERRIDE_NOT_SET;
    INT4                i4PathState = RPTE_ZERO;
    INT4                i4LabelSet = RPTE_ZERO;
    INT4                i4AdminStatus = RPTE_ZERO;
    UINT4               u4RmdData = RPTE_ZERO;
    INT4                i4GrCapability = RPTE_ZERO;
    INT4                i4RecoveryPath = RPTE_ZERO;
    INT4                i4RestartTime = RPTE_ZERO;
    INT4                i4RecoveryTime = RPTE_ZERO;
    UINT4               u4Port = RPTE_ZERO;
    UINT4               u4L3IfIndex = RPTE_ZERO;
    UINT4               u4RouterId = RPTE_ZERO;
    UINT4               u4AdminTime = RPTE_ZERO;
    INT4                i4CspfRetryInterval = RPTE_FRR_CSPF_RETRY_INTERVAL_DEF;
    UINT4               u4CspfRetryCount = RPTE_FRR_CSPF_RETRY_COUNT_DEF;
    UINT4               u4MaxLabelRange =
        gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange;
    UINT4               u4MinLabelRange =
        gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange;
    CHR1               *pc1String = NULL;
    INT1               *pi1IfName = NULL;
    UINT1               u1IfType = RPTE_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { RPTE_ZERO };

    tSNMP_OCTET_STRING_TYPE RmdPolicyObj;
    UINT1               au1Temp[RPTE_FOUR] = { RPTE_ZERO };

    tSNMP_OCTET_STRING_TYPE LsrId;
    UINT1               au1LsrId[RPTE_FOUR] = { RPTE_ZERO };

    tSNMP_OCTET_STRING_TYPE RecoveryPath;
    UINT1               au1RecoveryPath[RPTE_FOUR] = { RPTE_ZERO };
    UINT1               u1RsvpFlag = RPTE_FALSE;
    INT4                i4ReoptimizeTime = RPTE_ZERO;
    INT4                i4EroCacheTime = RPTE_ZERO;

    RmdPolicyObj.pu1_OctetList = au1Temp;
    LsrId.pu1_OctetList = au1LsrId;

    LsrId.i4_Length = RPTE_ZERO;
    RecoveryPath.pu1_OctetList = au1RecoveryPath;

    pc1String = (CHR1 *) au1LsrId;
    pi1IfName = (INT1 *) &au1IfName[0];

    CliPrintf (CliHandle, "\r\n!");
    FilePrintf (CliHandle, "\r\n!");

    /* Default Detour capabilty intialise as enable */
    if (RSVPTE_FRR_DETOUR_CAPABLE (gpRsvpTeGblInfo) != RPTE_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\n mpls traffic-eng detour disable");
        FilePrintf (CliHandle, "\r\n mpls traffic-eng detour disable");
    }

    /* Default Detour-merging  capabilty intialise as enable */
    if (RSVPTE_FRR_DETOUR_MERGING_CAPABLE (gpRsvpTeGblInfo) != RPTE_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\n mpls traffic-eng detour-merging disable");
        FilePrintf (CliHandle, "\r\n mpls traffic-eng detour-merging disable");
    }

    /* By default FRR revertive set as Global */
    if (RSVPTE_FRR_REVERTIVE_MODE (gpRsvpTeGblInfo) != RPTE_FRR_REVERTIVE_LOCAL)
    {
        CliPrintf (CliHandle,
                   "\r\n mpls traffic-eng fast-reroute revertive global");
        FilePrintf (CliHandle,
                    "\r\n mpls traffic-eng fast-reroute revertive global");
    }
    /* By default CSPF retrive count is Default */
    if ((nmhGetFsMplsFrrCspfRetryInterval (&i4CspfRetryInterval) ==
         SNMP_SUCCESS)
        && (nmhGetFsMplsFrrCspfRetryCount (&u4CspfRetryCount) == SNMP_SUCCESS))
    {
        if ((RPTE_FRR_CSPF_RETRY_INTERVAL_DEF != i4CspfRetryInterval)
            || (RPTE_FRR_CSPF_RETRY_COUNT_DEF != u4CspfRetryCount))
        {

            CliPrintf (CliHandle,
                       "\r\n mpls traffic-eng cspf time-interval %d  retry-count %d",
                       i4CspfRetryInterval, u4CspfRetryCount);
            FilePrintf (CliHandle,
                        "\r\n mpls traffic-eng cspf time-interval %d retry-count %d",
                        i4CspfRetryInterval, u4CspfRetryCount);
        }
    }

    /* By default make after break for Fast Reroute set as disable */
    if (RSVPTE_FRR_MAKE_AFTER_BREAK_ENABLED (gpRsvpTeGblInfo) !=
        RPTE_SNMP_FALSE)
    {
        CliPrintf (CliHandle,
                   "\r\n mpls traffic-eng fast-reroute protection make-after-break enable");
        FilePrintf (CliHandle,
                    "\r\n mpls traffic-eng fast-reroute protection make-after-break enable");
    }

    /* Get the Operational status */
    nmhGetFsMplsRsvpTeOperStatus (&i4Operstatus);

    CliPrintf (CliHandle, "\r\n!");
    FilePrintf (CliHandle, "\r\n!");

    /*to execute all the commands in rsvp mode */

    if ((nmhGetFsMplsRsvpTeLsrID (&LsrId) != SNMP_FAILURE) &&
        (RPTE_ZERO != LsrId.i4_Length))
    {
        MPLS_OCTETSTRING_TO_INTEGER ((&LsrId), u4RouterId);

        /* chech if router-id is configured */
        if (RPTE_ZERO != u4RouterId)
        {
            CliPrintf (CliHandle, "\r\nrsvp");
            CliPrintf (CliHandle, "\r\nrouter-id ");
            FilePrintf (CliHandle, "\r\nrsvp");
            FilePrintf (CliHandle, "\r\nrouter-id ");
            u1RsvpFlag = RPTE_TRUE;

            if ((NETIPV4_SUCCESS ==
                 NetIpv4GetIfIndexFromAddr (u4RouterId, &u4Port)
                 && (u4RouterId != 0)))
            {
                if (NETIPV4_SUCCESS ==
                    NetIpv4GetCfaIfIndexFromPort (u4Port, &u4L3IfIndex))
                {
                    if (CFA_SUCCESS == CfaGetIfaceType (u4L3IfIndex, &u1IfType))
                    {
                        if ((CFA_NONE == u1IfType)
                            || (CFA_LOOPBACK == u1IfType))
                        {
                            CLI_CONVERT_IPADDR_TO_STR (pc1String, u4RouterId);
                            CliPrintf (CliHandle, "%s", pc1String);
                            FilePrintf (CliHandle, "%s", pc1String);
                        }
                        else
                        {
                            if (CFA_SUCCESS ==
                                CfaCliConfGetIfName (u4L3IfIndex, pi1IfName))
                            {
                                CliPrintf (CliHandle, "%s ", pi1IfName);
                                FilePrintf (CliHandle, "%s ", pi1IfName);
                            }
                        }
                    }
                }
            }
            else
            {
                /* print the ip if it is not equal to an interfaces's ip 
                 * and also ip not equal to 0*/
                if (u4RouterId != 0)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pc1String, u4RouterId);
                    CliPrintf (CliHandle, "%s", pc1String);
                    FilePrintf (CliHandle, "%s", pc1String);
                }
            }
        }
    }

    /* Get the RR capable bit and display the command if 
     * RR is enabled */

    if ((nmhGetFsMplsRsvpTeRRCapable (&i4RefreshReduction) == SNMP_SUCCESS) &&
        (RPTE_RR_ENABLED == i4RefreshReduction))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nno signalling refresh reduction disable");
        FilePrintf (CliHandle, "\r\nno signalling refresh reduction disable");
    }

    /* Get the max and min range value of labels */
    /* check for default values */
    if ((nmhGetFsMplsRsvpTeGenLblSpaceMaxLbl ((INT4 *) &u4MaxLabelRange)
         == SNMP_SUCCESS) &&
        (nmhGetFsMplsRsvpTeGenLblSpaceMinLbl ((INT4 *) &u4MinLabelRange)
         == SNMP_SUCCESS) &&
        ((u4MinLabelRange != gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange) ||
         (u4MaxLabelRange != gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange)))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nsignalling label range min %d max %d",
                   u4MinLabelRange, u4MaxLabelRange);
        FilePrintf (CliHandle, "\r\nsignalling label range min %d max %d",
                    u4MinLabelRange, u4MaxLabelRange);
    }

    /* Get sock/hello/msg-id supports */

    if ((nmhGetFsMplsRsvpTeSockSupprtd (&i4SockSupport) == SNMP_SUCCESS) &&
        (RPTE_SNMP_FALSE == i4SockSupport))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nno signalling sock supported");
        FilePrintf (CliHandle, "\r\nno signalling sock supported");
    }

    if ((nmhGetFsMplsRsvpTeHelloSupprtd (&i4HelloSupport) == SNMP_SUCCESS) &&
        (i4HelloSupport == RPTE_TRUE))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\n signalling hello supported");
        FilePrintf (CliHandle, "\r\n signalling hello supported");
    }

    if ((nmhGetFsMplsRsvpTeMsgIdCapable (&i4MessageIdSupport) == SNMP_SUCCESS)
        && (i4MessageIdSupport == RPTE_TRUE))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\n signalling msg-id supported");
        FilePrintf (CliHandle, "\r\n signalling msg-id supported");
    }

    if ((nmhGetFsMplsRsvpTeLabelSetEnabled (&i4LabelSetSupport) == SNMP_SUCCESS)
        && (i4LabelSetSupport == RPTE_TRUE))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\n signalling label-set supported");
        FilePrintf (CliHandle, "\r\n signalling label-set supported");
    }

    /* Get the hello refresh interval time */

    if ((nmhGetFsMplsRsvpTeHelloIntervalTime (&i4HelloIntervalTime) ==
         SNMP_SUCCESS) && (HELLO_REFRESH_INTERVAL != i4HelloIntervalTime))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nsignalling hello refresh interval %d",
                   i4HelloIntervalTime);
        FilePrintf (CliHandle, "\r\nsignalling hello refresh interval %d",
                    i4HelloIntervalTime);
    }

    /* Get the maximum number of tunnels/erhops/interfaces/neighbours */
    if ((nmhGetFsMplsRsvpTeMaxTnls (&i4MaxTunnels) == SNMP_SUCCESS) &&
        (MAX_RSVPTE_TUNNEL != i4MaxTunnels))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nsignalling max tunnels %d ", i4MaxTunnels);
        FilePrintf (CliHandle, "\r\nsignalling max tunnels %d ", i4MaxTunnels);
    }

    if ((nmhGetFsMplsRsvpTeMaxErhopsPerTnl (&i4MaxErhops) == SNMP_SUCCESS) &&
        ((RSVPTE_TNLERHOP_DEF_VAL != i4MaxErhops)
         && (RPTE_ZERO != i4MaxErhops)))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nsignalling max erhops%d ", i4MaxErhops);
        FilePrintf (CliHandle, "\r\nsignalling max erhops %d ", i4MaxErhops);
    }

    if ((nmhGetFsMplsRsvpTeMaxIfaces (&i4MaxInterfaces) == SNMP_SUCCESS) &&
        (MAX_RSVPTE_IF_ENTRY != i4MaxInterfaces))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nsignalling max interfaces %d ",
                   i4MaxInterfaces);
        FilePrintf (CliHandle, "\r\nsignalling max interfaces %d ",
                    i4MaxInterfaces);
    }

    if ((nmhGetFsMplsRsvpTeMaxNbrs (&i4MaxNeighbours) == SNMP_FAILURE) &&
        (MAX_RSVPTE_NBR_ENTRY != i4MaxNeighbours))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nsignalling max neighbours %d ",
                   i4MaxNeighbours);
        FilePrintf (CliHandle, "\r\nsignalling max neighbours %d ",
                    i4MaxNeighbours);
    }

    /* Get the RMD policy option */

    if (nmhGetFsMplsRsvpTeRMDPolicyObject (&RmdPolicyObj) == SNMP_SUCCESS)
    {
        /*    MPLS_OCTETSTRING_TO_INTEGER((&RmdPolicyObj),u4RmdData); */
        u4RmdData = RmdPolicyObj.pu1_OctetList[0];

        if (RPTE_ZERO != u4RmdData)
        {
            RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
            CliPrintf (CliHandle, "\r\nsignalling rmd policy ");
            FilePrintf (CliHandle, "\r\nsignalling rmd policy ");
            if (RPTE_RMD_ALL_MSGS == (u4RmdData & RPTE_RMD_ALL_MSGS))
            {
                CliPrintf (CliHandle, "all");
                FilePrintf (CliHandle, "all");
            }
            else
            {
                if (RPTE_RMD_PATH_MSG == (u4RmdData & RPTE_RMD_PATH_MSG))
                {
                    CliPrintf (CliHandle, "pathonly ");
                    FilePrintf (CliHandle, "pathonly ");
                }

                if (RPTE_RMD_RESV_MSG == (u4RmdData & RPTE_RMD_RESV_MSG))
                {
                    CliPrintf (CliHandle, "resvonly ");
                    FilePrintf (CliHandle, "resvonly ");
                }

                if (RPTE_RMD_PATH_ERR_MSG ==
                    (u4RmdData & RPTE_RMD_PATH_ERR_MSG))
                {
                    CliPrintf (CliHandle, "patherr ");
                    FilePrintf (CliHandle, "patherr ");
                }

                if (RPTE_RMD_RESV_ERR_MSG ==
                    (u4RmdData & RPTE_RMD_RESV_ERR_MSG))
                {
                    CliPrintf (CliHandle, "resverr ");
                    FilePrintf (CliHandle, "resverr ");
                }

                if (RPTE_RMD_PATH_TEAR_MSG ==
                    (u4RmdData & RPTE_RMD_PATH_TEAR_MSG))
                {
                    CliPrintf (CliHandle, "pathtear ");
                    FilePrintf (CliHandle, "pathtear ");
                }

                if (RPTE_RMD_RESV_TEAR_MSG ==
                    (u4RmdData & RPTE_RMD_RESV_TEAR_MSG))
                {
                    CliPrintf (CliHandle, "resvtear ");
                    FilePrintf (CliHandle, "resvtear ");
                }
                if (RPTE_RMD_RECOVERY_PATH_MSG ==
                    (u4RmdData & RPTE_RMD_RECOVERY_PATH_MSG))
                {
                    CliPrintf (CliHandle, "recoverypath ");
                    FilePrintf (CliHandle, "recoverypath ");
                }

            }
        }
    }

    /* Get the differentiated services option */
    if ((nmhGetFsMplsRsvpTeOverRideOption (&i4DiffServOption) == SNMP_SUCCESS)
        && (TE_DS_OVERRIDE_NOT_SET == i4DiffServOption))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\n signalling override option disable");
        FilePrintf (CliHandle, "\r\n signalling override option disable");
    }

    if ((nmhGetFsMplsRsvpTeAdminStatusTimeIntvl (&u4AdminTime) == SNMP_SUCCESS)
        && (RPTE_ADMIN_GRACEFUL_DEL_TIME != u4AdminTime))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nset graceful deletion time interval %d",
                   u4AdminTime);
        FilePrintf (CliHandle, "\r\nset graceful deletion time interval %d",
                    u4AdminTime);
    }

    if ((nmhGetFsMplsRsvpTePathStateRemovedSupport (&i4PathState)
         == SNMP_SUCCESS) && (RPTE_ENABLED == i4PathState))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nset path-state-remove enable");
        FilePrintf (CliHandle, "\r\nset path-state-remove enable");
    }

    if ((nmhGetFsMplsRsvpTeLabelSetEnabled (&i4LabelSet) == SNMP_SUCCESS) &&
        (RPTE_ENABLED == i4LabelSet))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nsignalling label-set supported");
        FilePrintf (CliHandle, "\r\nsignalling label-set supported");
    }

    if ((nmhGetFsMplsRsvpTeAdminStatusCapability (&i4AdminStatus)
         == SNMP_SUCCESS) && (RPTE_ENABLED == i4AdminStatus))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nset admin-status-capability enable");
        FilePrintf (CliHandle, "\r\nset admin-status-capability enable");
    }

    if ((nmhGetFsMplsRsvpTeGrCapability (&i4GrCapability) == SNMP_SUCCESS) &&
        (RPTE_GR_CAPABILITY_NONE != i4GrCapability))
    {
        if (RPTE_GR_CAPABILITY_FULL == i4GrCapability)
        {
            RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
            CliPrintf (CliHandle, "\r\nsignalling hello graceful-restart full");
            FilePrintf (CliHandle,
                        "\r\nsignalling hello graceful-restart full");
        }
        else if (RPTE_GR_CAPABILITY_HELPER == i4GrCapability)
        {
            RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
            CliPrintf (CliHandle,
                       "\r\nsignalling hello graceful-restart help-neighbor");
            FilePrintf (CliHandle,
                        "\r\nsignalling hello graceful-restart help-neighbor");
        }
        if (nmhGetFsMplsRsvpTeGrRecoveryPathCapability (&RecoveryPath) ==
            SNMP_SUCCESS)
        {

            i4RecoveryPath = RecoveryPath.pu1_OctetList[0];
            if (!(RPTE_GR_RECOVERY_PATH_RX & i4RecoveryPath))
            {
                CliPrintf (CliHandle, " no-recoverypath-receive");
                FilePrintf (CliHandle, " no-recoverypath-receive");
            }
            if (!(RPTE_GR_RECOVERY_PATH_TX & i4RecoveryPath))
            {
                CliPrintf (CliHandle, " no-recoverypath-transmit");
                FilePrintf (CliHandle, " no-recoverypath-transmit");
            }
            if (!(RPTE_GR_RECOVERY_PATH_SREFRESH & i4RecoveryPath))
            {
                CliPrintf (CliHandle, " no-recoverypath-srefresh");
                FilePrintf (CliHandle, " no-recoverypath-srefresh");
            }
        }
        if (nmhGetFsMplsRsvpTeGrRestartTime (&i4RestartTime) == SNMP_SUCCESS)
        {
            RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
            CliPrintf (CliHandle,
                       "\r\nsignalling hello graceful-restart restart-time %d",
                       i4RestartTime);
            FilePrintf (CliHandle,
                        "\r\nsignalling hello graceful-restart restart-time %d",
                        i4RestartTime);
        }
        if (nmhGetFsMplsRsvpTeGrRecoveryTime (&i4RecoveryTime) == SNMP_SUCCESS)
        {

            RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
            CliPrintf (CliHandle,
                       "\r\nsignalling hello graceful-restart recovery-time %d",
                       i4RecoveryTime);
            FilePrintf (CliHandle,
                        "\r\nsignalling hello graceful-restart recovery-time %d",
                        i4RecoveryTime);
        }
    }

    if ((nmhGetFsMplsRsvpTeReoptimizeTime (&i4ReoptimizeTime) == SNMP_SUCCESS)
        && (RPTE_REOPT_TIME_DEFAULT_VAL != i4ReoptimizeTime))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nsignalling reoptimize-time %d ",
                   i4ReoptimizeTime);
        FilePrintf (CliHandle, "\r\nsignalling reoptimize-time %d ",
                    i4ReoptimizeTime);

    }
    if ((nmhGetFsMplsRsvpTeEroCacheTime (&i4EroCacheTime) == SNMP_SUCCESS)
        && (RPTE_ERO_CACHE_TIME_DEFAULT_VAL != i4EroCacheTime))
    {
        RpteCliCheckRsvpCommand (CliHandle, &u1RsvpFlag);
        CliPrintf (CliHandle, "\r\nsignalling ero-cache-time %d ",
                   i4EroCacheTime);
        FilePrintf (CliHandle, "\r\nsignalling ero-cache-time %d ",
                    i4EroCacheTime);
    }

    /* display the command is operstatus is enabled */
    if (RPTE_OPER_UP == i4Operstatus)
    {
        CliPrintf (CliHandle, "\r\nset rsvp enable");
        FilePrintf (CliHandle, "\r\nset rsvp enable");
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliRsvpTeDebugShowRunningConfig
 * Description   : This routine displays config commands for scalar objects
 *                 that set debug flags under fsMplsRsvpTeGenObjects.
 * Input(s)      : CliHandle - Index of current CLI context
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 ******************************************************************************/

INT4
RpteCliRsvpTeDebugShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4DumpMsgType = RSVPTE_DUMP_NONE;
    UINT4               u4DebugFlag = RPTE_ZERO;

    if ((nmhGetFsMplsRsvpTeGenDebugFlag (&u4DebugFlag) == SNMP_SUCCESS) &&
        (RSVPTE_MAIN_DBG == (RSVPTE_MAIN_DBG & u4DebugFlag)))
    {
        CliPrintf (CliHandle, "\r\ndebug rsvp all");
        FilePrintf (CliHandle, "\r\ndebug rsvp all");
    }
    else
    {
        if (RSVPTE_HH_DBG == (RSVPTE_HH_DBG & u4DebugFlag))
        {
            CliPrintf (CliHandle, "\r\ndebug rsvp hello");
            FilePrintf (CliHandle, "\r\ndebug rsvp hello");
        }

        if (RSVPTE_IF_DBG == (RSVPTE_IF_DBG & u4DebugFlag))
        {
            CliPrintf (CliHandle, "\r\ndebug rsvp interface");
            FilePrintf (CliHandle, "\r\ndebug rsvp interface");
        }

        if (RSVPTE_PB_DBG == (RSVPTE_PB_DBG & u4DebugFlag))
        {
            CliPrintf (CliHandle, "\r\ndebug rsvp packet");
            FilePrintf (CliHandle, "\r\ndebug rsvp packet");
        }

        if (RSVPTE_PH_DBG == (RSVPTE_PH_DBG & u4DebugFlag))
        {
            CliPrintf (CliHandle, "\r\ndebug rsvp path");
            FilePrintf (CliHandle, "\r\ndebug rsvp path");
        }

        if (RSVPTE_RH_DBG == (RSVPTE_RH_DBG & u4DebugFlag))
        {
            CliPrintf (CliHandle, "\r\ndebug rsvp resv");
            FilePrintf (CliHandle, "\r\ndebug rsvp resv");
        }

        if (RSVPTE_NBR_DBG == (RSVPTE_NBR_DBG & u4DebugFlag))
        {
            CliPrintf (CliHandle, "\r\ndebug rsvp neighbour");
            FilePrintf (CliHandle, "\r\ndebug rsvp neighbour");
        }

        if (RSVPTE_FRR_DBG == (RSVPTE_FRR_DBG & u4DebugFlag))
        {
            CliPrintf (CliHandle, "\r\ndebug rsvp frr");
            FilePrintf (CliHandle, "\r\ndebug rsvp frr");
        }
    }

    if ((nmhGetFsMplsRsvpTeGenPduDumpMsgType (&i4DumpMsgType)
         == SNMP_SUCCESS) && (RSVPTE_DUMP_NONE != i4DumpMsgType))
    {
        if (RSVPTE_DUMP_ALL == (i4DumpMsgType & RSVPTE_DUMP_ALL))
        {
            CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages all ");
            FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages all ");
            RpteCliRsvpTeGenSubDebugSRC (CliHandle);
        }

        else
        {
            if (RSVPTE_DUMP_ACK == (i4DumpMsgType & RSVPTE_DUMP_ACK))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages ack ");
                FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages ack ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }

            if (RSVPTE_DUMP_BUNDLE == (i4DumpMsgType & RSVPTE_DUMP_BUNDLE))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages bundle ");
                FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages bundle ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }

            if (RSVPTE_DUMP_RCNFM == (i4DumpMsgType & RSVPTE_DUMP_RCNFM))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages confirm ");
                FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages confirm ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }

            if (RSVPTE_DUMP_HELLO == (i4DumpMsgType & RSVPTE_DUMP_HELLO))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages hello ");
                FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages hello ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }

            if (RSVPTE_DUMP_PATH == (i4DumpMsgType & RSVPTE_DUMP_PATH))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages path ");
                FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages path ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }

            if (RSVPTE_DUMP_PERR == (i4DumpMsgType & RSVPTE_DUMP_PERR))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages perr ");
                FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages perr ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }

            if (RSVPTE_DUMP_PTEAR == (i4DumpMsgType & RSVPTE_DUMP_PTEAR))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages ptear ");
                FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages ptear ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }

            if (RSVPTE_DUMP_RERR == (i4DumpMsgType & RSVPTE_DUMP_RERR))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages rerr ");
                FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages rerr ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }

            if (RSVPTE_DUMP_RESV == (i4DumpMsgType & RSVPTE_DUMP_RESV))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages resv ");
                FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages resv ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }

            if (RSVPTE_DUMP_RTEAR == (i4DumpMsgType & RSVPTE_DUMP_RTEAR))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages rtear ");
                FilePrintf (CliHandle, "\r\ndebug rsvp dump-messages rtear ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }

            if (RSVPTE_DUMP_SREFRESH == (i4DumpMsgType & RSVPTE_DUMP_SREFRESH))
            {
                CliPrintf (CliHandle, "\r\ndebug rsvp dump-messages srefresh ");
                FilePrintf (CliHandle,
                            "\r\ndebug rsvp dump-messages srefresh ");
                RpteCliRsvpTeGenSubDebugSRC (CliHandle);
            }
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliRsvpTeGenSubDebugSRC
 * Description   : This routine displays config commands for debug options
 * Input(s)      : CliHandle - Index of current CLI context
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 ******************************************************************************/

INT4
RpteCliRsvpTeGenSubDebugSRC (tCliHandle CliHandle)
{
    INT4                i4DumpLevel = RPTE_ZERO;
    INT4                i4DumpDirection = RPTE_ZERO;

    if ((nmhGetFsMplsRsvpTeGenPduDumpLevel (&i4DumpLevel) == SNMP_SUCCESS) &&
        (RSVPTE_DUMP_LVL_MAX == (i4DumpLevel & RSVPTE_DUMP_LVL_MAX)))
    {
        CliPrintf (CliHandle, "detail ");
        FilePrintf (CliHandle, "detail ");
    }
    else
    {
        CliPrintf (CliHandle, "brief ");
        FilePrintf (CliHandle, "brief ");
    }

    if ((nmhGetFsMplsRsvpTeGenPduDumpDirection (&i4DumpDirection)
         == SNMP_SUCCESS) &&
        (RSVPTE_DUMP_DIR_NONE != i4DumpDirection) &&
        (RSVPTE_DUMP_DIR_OUT != i4DumpDirection))
    {
        CliPrintf (CliHandle, "direction ");
        FilePrintf (CliHandle, "direction ");

        switch (i4DumpDirection)
        {
            case RSVPTE_DUMP_DIR_IN:
                CliPrintf (CliHandle, "in ");
                FilePrintf (CliHandle, "in ");
                break;

            case RSVPTE_DUMP_DIR_INOUT:
                CliPrintf (CliHandle, "both ");
                FilePrintf (CliHandle, "both ");
                break;
            default:
                break;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliRsvpTeIfShowRunningConfig
 * Description   : This routine displays config commands for 
 *                 table fsMplsRsvpTeIfEntry
 * Input(s)      : CliHandle - Index of current CLI context
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 ******************************************************************************/

INT4
RpteCliRsvpTeIfShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4RsvpTeIfIndex = RPTE_ZERO;
    INT4                i4IfStatus = DESTROY;
    INT4                i4RefInterval = DEFAULT_REFRESH_INTERVAL;
    INT4                i4RefMultiple = DEFAULT_REFRESH_MULTIPLE;
    INT4                i4RouteDelay = DEFAULT_ROUTE_DELAY;
    INT4                i4UdpRequired = RPTE_SNMP_FALSE;
    INT4                i4HelloSupport = HELLO_NOT_SUPPRT;
    UINT4               u4LinkAttr = RPTE_IF_LINK_ATTR_DEF;
    INT4                i4IfMtu = RPTE_IF_MIN_MTU_SIZE;
    INT4                i4IfTtl = DEFAULT_TTL;
    UINT4               u4L3IfIndex = RPTE_ZERO;
    UINT4               u4PlrId = RPTE_ZERO;
    UINT4               u4AvoidNodeId = RPTE_ZERO;
    BOOL1               bloop = FALSE;
    BOOL1               bLockRequest = FALSE;

    INT1               *pi1IfName = NULL;
    CHR1               *pc1TempPlr = NULL;
    CHR1               *pc1TempAvd = NULL;
    UINT1               au1PlrLength[RSVPTE_IPV4ADR_LEN] = { RPTE_ZERO };
    UINT1               au1AvdLength[RSVPTE_IPV4ADR_LEN] = { RPTE_ZERO };
    tSNMP_OCTET_STRING_TYPE MplsRsvpTeIfPlrId;
    tSNMP_OCTET_STRING_TYPE MplsRsvpTeIfAvoidNodeId;

    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { RPTE_ZERO };

    pi1IfName = (INT1 *) &au1IfName[0];
    /* pc1TempPlr   = (CHR1 *)au1PlrLength;
       pc1TempAvd   = (CHR1 *)au1AvdLength; */

    MplsRsvpTeIfPlrId.pu1_OctetList = au1PlrLength;
    MplsRsvpTeIfAvoidNodeId.pu1_OctetList = au1AvdLength;

    while (1)
    {
        /* Loop prevails till the last entry of the table is reached */

        if ((TRUE == bloop) &&
            SNMP_FAILURE == nmhGetNextIndexRsvpIfTable (i4RsvpTeIfIndex,
                                                        &i4RsvpTeIfIndex))
        {
            break;
        }

        if ((FALSE == bloop) &&
            SNMP_FAILURE == nmhGetFirstIndexRsvpIfTable (&i4RsvpTeIfIndex))
        {
            break;
        }
        bloop = TRUE;

        if (CLI_SUCCESS == CfaUtilGetL3IfFromMplsIf ((UINT4) i4RsvpTeIfIndex,
                                                     &u4L3IfIndex,
                                                     bLockRequest))
        {
            if (CLI_FAILURE != CfaCliConfGetIfName (u4L3IfIndex, pi1IfName))
            {
                CliPrintf (CliHandle, "\r\ninterface %s", pi1IfName);
                FilePrintf (CliHandle, "\r\ninterface %s", pi1IfName);
            }
        }

        if (SNMP_SUCCESS ==
            nmhGetFsMplsRsvpTeIfRefreshInterval (i4RsvpTeIfIndex,
                                                 &i4RefInterval))
        {
            if (DEFAULT_REFRESH_INTERVAL != i4RefInterval)
            {
                CliPrintf (CliHandle, "\r\n signalling refresh interval %d",
                           i4RefInterval);
                FilePrintf (CliHandle, "\r\n signalling refresh interval %d",
                            i4RefInterval);
            }
        }

        if (SNMP_SUCCESS ==
            nmhGetFsMplsRsvpTeIfRefreshMultiple (i4RsvpTeIfIndex,
                                                 &i4RefMultiple))
        {
            if (DEFAULT_REFRESH_MULTIPLE != i4RefMultiple)
            {
                CliPrintf (CliHandle, "\r\n signalling refresh missed %d",
                           i4RefMultiple);
                FilePrintf (CliHandle, "\r\n signalling refresh missed %d",
                            i4RefMultiple);
            }
        }

        if (SNMP_SUCCESS == nmhGetFsMplsRsvpTeIfRouteDelay (i4RsvpTeIfIndex,
                                                            &i4RouteDelay))
        {
            if (DEFAULT_ROUTE_DELAY != i4RouteDelay)
            {
                CliPrintf (CliHandle, "\r\n signalling route change delay %d",
                           i4RouteDelay);
                FilePrintf (CliHandle, "\r\n signalling route change delay %d",
                            i4RouteDelay);
            }
        }

        if (SNMP_SUCCESS == nmhGetFsMplsRsvpTeIfUdpRequired (i4RsvpTeIfIndex,
                                                             &i4UdpRequired))
        {
            if (RPTE_SNMP_TRUE == i4UdpRequired)
            {
                CliPrintf (CliHandle, "\r\n signalling encapsulation udp");
                FilePrintf (CliHandle, "\r\n signalling encapsulation udp");
            }
        }

        if (SNMP_SUCCESS == nmhGetFsMplsRsvpTeIfHelloSupported (i4RsvpTeIfIndex,
                                                                &i4HelloSupport))
        {
            if (HELLO_SUPPRT == i4HelloSupport)
            {
                CliPrintf (CliHandle, "\r\n signalling hello supported");
                FilePrintf (CliHandle, "\r\n signalling hello supported");
            }
        }

        if (SNMP_SUCCESS == nmhGetFsMplsRsvpTeIfLinkAttr (i4RsvpTeIfIndex,
                                                          &u4LinkAttr))
        {
            if (RPTE_IF_LINK_ATTR_DEF != u4LinkAttr)
            {
                CliPrintf (CliHandle, "\r\n signalling link attributes %u",
                           u4LinkAttr);
                FilePrintf (CliHandle, "\r\n signalling link attributes %u",
                            u4LinkAttr);
            }
        }

        if ((SNMP_SUCCESS ==
             nmhGetFsMplsRsvpTeIfMtu (i4RsvpTeIfIndex, &i4IfMtu))
            && (SNMP_SUCCESS ==
                nmhGetFsMplsRsvpTeIfTTL (i4RsvpTeIfIndex, &i4IfTtl)))
        {
            if ((RPTE_IF_MIN_MTU_SIZE != i4IfMtu) || (DEFAULT_TTL != i4IfTtl))
            {
                CliPrintf (CliHandle, "\r\n signalling ");
                FilePrintf (CliHandle, "\r\n signalling ");
            }
            if (RPTE_IF_MIN_MTU_SIZE != i4IfMtu)
            {
                CliPrintf (CliHandle, "mtu %d ", i4IfMtu);
                FilePrintf (CliHandle, "mtu %d ", i4IfMtu);
            }
            if (DEFAULT_TTL != i4IfTtl)
            {
                CliPrintf (CliHandle, "ttl %d", i4IfTtl);
                FilePrintf (CliHandle, "ttl %d", i4IfTtl);
            }
        }

        if ((SNMP_FAILURE != nmhGetFsMplsRsvpTeIfPlrId (i4RsvpTeIfIndex,
                                                        &MplsRsvpTeIfPlrId)) &&
            (SNMP_FAILURE != nmhGetFsMplsRsvpTeIfAvoidNodeId (i4RsvpTeIfIndex,
                                                              &MplsRsvpTeIfAvoidNodeId)))
        {
            /* conversion of octet string to integer */

            MPLS_OCTETSTRING_TO_INTEGER ((&MplsRsvpTeIfPlrId), u4PlrId);
            MPLS_OCTETSTRING_TO_INTEGER ((&MplsRsvpTeIfAvoidNodeId),
                                         u4AvoidNodeId);

            if ((RPTE_ZERO != u4PlrId) || (RPTE_ZERO != u4AvoidNodeId))
            {
                CliPrintf (CliHandle, "\r\n mpls traffic-eng fast-reroute ");
                FilePrintf (CliHandle, "\r\n mpls traffic-eng fast-reroute ");

                if (RPTE_ZERO != u4PlrId)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pc1TempPlr, u4PlrId);
                    CliPrintf (CliHandle, "plr-id %s ", pc1TempPlr);
                    FilePrintf (CliHandle, "plr-id %s ", pc1TempPlr);
                }
                if (RPTE_ZERO != u4AvoidNodeId)
                {
                    CLI_CONVERT_IPADDR_TO_STR (pc1TempAvd, u4AvoidNodeId);
                    CliPrintf (CliHandle, "avoid-node-id %s", pc1TempAvd);
                    FilePrintf (CliHandle, "avoid-node-id %s", pc1TempAvd);
                }
            }
        }

        if (SNMP_SUCCESS == nmhGetFsMplsRsvpTeIfStatus (i4RsvpTeIfIndex,
                                                        &i4IfStatus))
        {
            if (ACTIVE == i4IfStatus)
            {
                CliPrintf (CliHandle, "\r\n no shutdown");
                FilePrintf (CliHandle, "\r\n no shutdown");
                CliPrintf (CliHandle, "\r\n !");
                FilePrintf (CliHandle, "\r\n exit");
            }
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliSetGRCapability
 * Description   : This routine is used to set the GR capability
 * Input(s)      : CliHandle - Index of current CLI context
 *                 i4GrCapability - GR capabililty mode
 *                 u4RecoveryPathCapability - RecoveryPath Capability mode
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
PRIVATE INT4
RpteCliSetGRCapability (tCliHandle CliHandle, INT4 i4GrCapability,
                        UINT4 u4RecoveryPathCapability)
{
    UINT1               au1RecoveryPath[RPTE_FOUR] = { 0 };
    tSNMP_OCTET_STRING_TYPE RecoveryPathCapability;
    UINT4               u4Error = RPTE_ZERO;

    if (nmhTestv2FsMplsRsvpTeGrCapability (&u4Error,
                                           i4GrCapability) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% RSVP GR Capability cannot be set\r\n");
        return CLI_FAILURE;

    }

    if (nmhSetFsMplsRsvpTeGrCapability (i4GrCapability) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% RSVP GR Capability cannot be set\r\n");
        return CLI_FAILURE;
    }

    RecoveryPathCapability.pu1_OctetList = au1RecoveryPath;
    RecoveryPathCapability.pu1_OctetList[0] = (UINT1) u4RecoveryPathCapability;
    RecoveryPathCapability.i4_Length = 1;

    if (nmhTestv2FsMplsRsvpTeGrRecoveryPathCapability (&u4Error,
                                                       &RecoveryPathCapability)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% RSVP PathRecovery Capability cannot be set\r\n");
        return CLI_FAILURE;

    }

    if (nmhSetFsMplsRsvpTeGrRecoveryPathCapability (&RecoveryPathCapability) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r%% RSVP GR RecoveryPath Capability cannot be set\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliSetGrRestartTimerValue
 * Description   : This routine is used to set the GR restart timer
                   value of RSVP module.
 * Input(s)      : CliHandle - Index of current CLI context
 *                 u4TimerValue - Rsvp-TE GR restart timer value
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
PRIVATE INT4
RpteCliSetGrRestartTimerValue (tCliHandle CliHandle, INT4 i4TimerValue)
{

    UINT4               u4Error = 0;

    if (nmhTestv2FsMplsRsvpTeGrRestartTime (&u4Error, i4TimerValue)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%Invalid RSVP capability\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsRsvpTeGrRestartTime (i4TimerValue) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% RSVP GR RestartTime cannot be set\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : RpteCliSetGrRecoveryTimerValue
 * Description   : This routine is used to set the GR recovery timer
                   value of RSVP module.
 * Input(s)      : CliHandle - Index of current CLI context
 *                 u4TimerValue - Rsvp-TE GR recovery time value
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
PRIVATE INT4
RpteCliSetGrRecoveryTimerValue (tCliHandle CliHandle, INT4 i4TimerValue)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsMplsRsvpTeGrRecoveryTime (&u4Error, i4TimerValue)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%Invalid RSVP capability\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsRsvpTeGrRecoveryTime (i4TimerValue) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% RSVP GR RecoveryTime cannot be set\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IssRsvpShowDebugging                                */
/*                                                                         */
/*     Description   : This function is used to display debug level for    */
/*                     RSVP  module                                        */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : None                                                */
/*                                                                         */
/***************************************************************************/

VOID
IssRsvpShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DumpMsgType = RSVPTE_DUMP_NONE;
    UINT4               u4DebugFlag = RPTE_ZERO;
    INT4                i4DumpLvl = 0;
    INT4                i4Direction = 0;
    INT4                i4DumpDir = MPLS_CLI_ALL;
    INT4                i4DumpDir1 = RPTE_ZERO;

    CliRegisterLock (CliHandle, RsvpTeLock, RsvpTeUnLock);
    RsvpTeLock ();

    /* to get debug flag */
    if (nmhGetFsMplsRsvpTeGenDebugFlag (&u4DebugFlag) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% failed to get debug flag\r\n");
    }

    /* to get dump message type */
    if (nmhGetFsMplsRsvpTeGenPduDumpMsgType (&i4DumpMsgType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% failed to get dump message type\r\n");
    }

    /* to get dump level */
    if (nmhGetFsMplsRsvpTeGenPduDumpLevel (&i4DumpLvl) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% failed to get dump level\r\n");
    }

    /* to get dump direction */
    if (nmhGetFsMplsRsvpTeGenPduDumpDirection (&i4Direction) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% failed to get dump direction\r\n");
    }

    RsvpTeUnLock ();
    CliUnRegisterLock (CliHandle);

    if ((u4DebugFlag == 0) && (i4DumpMsgType == 0))
    {
        return;
    }

    CliPrintf (CliHandle, "\rRSVP :");
    if (RSVPTE_HH_DBG == (RSVPTE_HH_DBG & u4DebugFlag))
    {
        CliPrintf (CliHandle, "\r\n  RSVP hello debugging is on");
    }
    if (RSVPTE_IF_DBG == (RSVPTE_IF_DBG & u4DebugFlag))
    {
        CliPrintf (CliHandle, "\r\n  RSVP interface debugging is on");
    }
    if (RSVPTE_PB_DBG == (RSVPTE_PB_DBG & u4DebugFlag))
    {
        CliPrintf (CliHandle, "\r\n  RSVP packet debugging is on");
    }
    if (RSVPTE_PH_DBG == (RSVPTE_PH_DBG & u4DebugFlag))
    {
        CliPrintf (CliHandle, "\r\n  RSVP path debugging is on");
    }
    if (RSVPTE_RH_DBG == (RSVPTE_RH_DBG & u4DebugFlag))
    {
        CliPrintf (CliHandle, "\r\n  RSVP resv debugging is on");
    }
    if (RSVPTE_NBR_DBG == (RSVPTE_NBR_DBG & u4DebugFlag))
    {
        CliPrintf (CliHandle, "\r\n  RSVP neighbour debugging is on");
    }
    if (RSVPTE_FRR_DBG == (RSVPTE_FRR_DBG & u4DebugFlag))
    {
        CliPrintf (CliHandle, "\r\n  RSVP frr debugging is on");
    }

    if (i4DumpLvl == RSVPTE_DUMP_LVL_MAX)
    {
        CliPrintf (CliHandle, "\r\n  RSVP dump-messages level detail");
    }

    if (RSVPTE_DUMP_NONE != i4DumpMsgType)
    {
        if (RSVPTE_DUMP_ACK == (i4DumpMsgType & RSVPTE_DUMP_ACK))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages ack ");
            i4DumpDir1 = i4DumpDir << DEBUG_ACK_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_ACK_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_BUNDLE == (i4DumpMsgType & RSVPTE_DUMP_BUNDLE))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages bundle ");
            i4DumpDir1 = i4DumpDir << DEBUG_BUNDLE_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_BUNDLE_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_RCNFM == (i4DumpMsgType & RSVPTE_DUMP_RCNFM))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages confirm ");
            i4DumpDir1 = i4DumpDir << DEBUG_RESVCONF_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_RESVCONF_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_HELLO == (i4DumpMsgType & RSVPTE_DUMP_HELLO))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages hello ");
            i4DumpDir1 = i4DumpDir << DEBUG_HELLO_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_HELLO_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_PATH == (i4DumpMsgType & RSVPTE_DUMP_PATH))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages path ");
            i4DumpDir1 = i4DumpDir << DEBUG_PATH_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_PATH_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_PERR == (i4DumpMsgType & RSVPTE_DUMP_PERR))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages perr ");
            i4DumpDir1 = i4DumpDir << DEBUG_PATHERR_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_PATHERR_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_PTEAR == (i4DumpMsgType & RSVPTE_DUMP_PTEAR))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages ptear ");
            i4DumpDir1 = i4DumpDir << DEBUG_PATHTEAR_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_PATHTEAR_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_RERR == (i4DumpMsgType & RSVPTE_DUMP_RERR))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages rerr ");
            i4DumpDir1 = i4DumpDir << DEBUG_RESVERR_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_RESVERR_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_RESV == (i4DumpMsgType & RSVPTE_DUMP_RESV))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages resv ");
            i4DumpDir1 = i4DumpDir << DEBUG_RESV_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_RESV_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_RTEAR == (i4DumpMsgType & RSVPTE_DUMP_RTEAR))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages rtear ");
            i4DumpDir1 = i4DumpDir << DEBUG_RESVTEAR_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_RESVTEAR_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_SREFRESH == (i4DumpMsgType & RSVPTE_DUMP_SREFRESH))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages srefresh ");
            i4DumpDir1 = i4DumpDir << DEBUG_SREFRESH_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_SREFRESH_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }
        if (RSVPTE_DUMP_NOTIFY == (i4DumpMsgType & RSVPTE_DUMP_NOTIFY))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages notify ");
            i4DumpDir1 = i4DumpDir << DEBUG_NOTIFY_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_NOTIFY_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }

        if (RSVPTE_DUMP_RECOVERY_PATH ==
            (i4DumpMsgType & RSVPTE_DUMP_RECOVERY_PATH))
        {
            CliPrintf (CliHandle, "\r\n  RSVP dump-messages recovery path ");
            i4DumpDir1 = i4DumpDir << DEBUG_RECOVERY_PATH_MSG_BIT;
            i4DumpDir1 &= i4Direction;
            i4DumpDir1 >>= DEBUG_RECOVERY_PATH_MSG_BIT;
            RpteCliRsvpTeDumpDirection (CliHandle, i4DumpDir1);
        }
    }
    CliPrintf (CliHandle, "\r\n");

    return;
}

/*****************************************************************************
 * Function Name : RpteCliRsvpTeDumpDirection
 * Description   : This routine displays message direction 
 * Input(s)      : CliHandle - Index of current CLI context
 * Return(s)     : NONE
 ******************************************************************************/

VOID
RpteCliRsvpTeDumpDirection (tCliHandle CliHandle, INT4 i4DumpDirection)
{

    if (RSVPTE_DUMP_DIR_NONE != i4DumpDirection)
    {
        switch (i4DumpDirection)
        {
            case RSVPTE_DUMP_DIR_IN:
                CliPrintf (CliHandle, "incoming messages debugging is on");
                break;

            case RSVPTE_DUMP_DIR_INOUT:
                CliPrintf (CliHandle, "both direction debugging is on");
                break;

            case RSVPTE_DUMP_DIR_OUT:
                CliPrintf (CliHandle, "outgoing messages debugging is on");
                break;
            default:
                return;
        }
    }
}

/******************************************************************************
 * * Function Name : RpteCliDisplayGrConfigurations
 * * Description   : This routine Displays the Configurations of GR parameters
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : NONE
 * *****************************************************************************/

VOID
RpteCliDisplayGrConfigurations (tCliHandle CliHandle)
{
    INT4                i4RestartTime = RPTE_ZERO;
    INT4                i4RecoveryTime = RPTE_ZERO;
    INT4                i4RecoveryPath = RPTE_ZERO;
    INT4                i4GrCapability = RPTE_ZERO;
    INT4                i4GrProgressStatus = RPTE_ZERO;
    tSNMP_OCTET_STRING_TYPE RecoveryPath;
    UINT1               au1RecoveryPath[RPTE_FOUR] = { RPTE_ZERO };

    RecoveryPath.pu1_OctetList = au1RecoveryPath;

    if (nmhGetFsMplsRsvpTeGrCapability (&i4GrCapability) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nRSVPTE Graceful Restart is ");

        if (i4GrCapability == RPTE_GR_CAPABILITY_NONE)
        {
            CliPrintf (CliHandle, "disabled\n");
            return;
        }
        else
        {
            if (i4GrCapability == RPTE_GR_CAPABILITY_FULL)
            {
                CliPrintf (CliHandle, "enabled: Configured as Full\n");
            }
            else
            {
                CliPrintf (CliHandle, "enabled: Configured as help-neighbor\n");
            }
        }
    }

    if (nmhGetFsMplsRsvpTeGrRecoveryPathCapability (&RecoveryPath) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\rRecovery Path Capability is  ");

        i4RecoveryPath = RecoveryPath.pu1_OctetList[0];

        if (i4RecoveryPath == RPTE_ZERO)
        {
            CliPrintf (CliHandle, "disabled\n");
        }
        else
        {
            CliPrintf (CliHandle, "enabled with following bits set: \n");

            if (RPTE_GR_RECOVERY_PATH_RX & i4RecoveryPath)
            {
                CliPrintf (CliHandle,
                           " \r     R (Recoverypath message reception) \n ");
            }

            if (RPTE_GR_RECOVERY_PATH_TX & i4RecoveryPath)
            {
                CliPrintf (CliHandle,
                           " \r     T (Recoverypath message transmission) \n ");
            }

            if (RPTE_GR_RECOVERY_PATH_SREFRESH & i4RecoveryPath)
            {
                CliPrintf (CliHandle,
                           " \r     S (S Refresh Recoverypath message capable) \n ");
            }
        }
    }

    if (nmhGetFsMplsRsvpTeGrRecoveryTime (&i4RecoveryTime) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\rRecovery Time: %d seconds\n", i4RecoveryTime);
    }

    if (nmhGetFsMplsRsvpTeGrRestartTime (&i4RestartTime) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\rRestart Time: %d seconds\n", i4RestartTime);
    }

    if (nmhGetFsMplsRsvpTeGrProgressStatus (&i4GrProgressStatus) ==
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\rGR Progress Status:");

        switch (i4GrProgressStatus)
        {
            case RPTE_GR_NOT_STARTED:
                CliPrintf (CliHandle, " GR Not Started\n");
                break;

            case RPTE_GR_SHUT_DOWN_IN_PROGRESS:
                CliPrintf (CliHandle, " GR Shutdown in Progress\n");
                break;

            case RPTE_GR_RESTART_IN_PROGRESS:
                CliPrintf (CliHandle, " GR Restart in Progress\n");
                break;

            case RPTE_GR_ABORTED:
                CliPrintf (CliHandle, " GR Aborted\n");
                break;

            case RPTE_GR_RECOVERY_IN_PROGRESS:
                CliPrintf (CliHandle, " GR Recovery in Progress\n");
                break;

            case RPTE_GR_COMPLETED:
                CliPrintf (CliHandle, " GR Recovery Completed\n");
                break;

            default:
                CliPrintf (CliHandle, "None \n");
                break;
        }
    }

    return;
}

/******************************************************************************
 * * Function Name : RpteCliDisplayNbrGrConfigurations
 * * Description   : This routine Displays the Configurations of neighbor GR parameters
 * * Input(s)      : CliHandle - Cli Context Handle
 * *                 pNbrEntry - Ponter to the neighbor entry structure
 * * Return(s)     : NONE
 * *****************************************************************************/

VOID
RpteCliDisplayNbrGrConfigurations (tCliHandle CliHandle, tNbrEntry * pNbrEntry)
{
    INT4                i4IfIndex = RPTE_ZERO;
    UINT4               u4Addr = RPTE_ZERO;
    INT4                i4RestartTime = RPTE_ZERO;
    INT4                i4RecoveryTime = RPTE_ZERO;
    INT4                i4RecoveryPath = RPTE_ZERO;
    INT4                i4GrProgressStatus = RPTE_ZERO;
    tSNMP_OCTET_STRING_TYPE RecoveryPath;
    UINT1               au1RecoveryPath[RPTE_FOUR] = { RPTE_ZERO };

    RecoveryPath.pu1_OctetList = au1RecoveryPath;

    i4IfIndex = (INT4) IF_ENTRY_IF_INDEX (NBR_ENTRY_IF_ENTRY (pNbrEntry));
    u4Addr = NBR_ENTRY_ADDR (pNbrEntry);

    if ((nmhGetFsMplsRsvpTeNbrGrRestartTime (i4IfIndex, u4Addr,
                                             &i4RestartTime) == SNMP_SUCCESS)
        && (i4RestartTime == RPTE_ZERO))
    {
        CliPrintf (CliHandle, "\rNeighbor doesn't have restart capability\n\n");
        return;
    }

    CliPrintf (CliHandle, "GR Properties:\n");
    if (nmhGetFsMplsRsvpTeNbrGrRecoveryPathCapability (i4IfIndex, u4Addr,
                                                       &RecoveryPath)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r    Recovery Path Capability is  ");

        i4RecoveryPath = RecoveryPath.pu1_OctetList[0];

        if (i4RecoveryPath == RPTE_ZERO)
        {
            CliPrintf (CliHandle, "disabled\n");
        }
        else
        {
            CliPrintf (CliHandle, "enabled with following bits set: \n");
            if (RPTE_GR_RECOVERY_PATH_RX & i4RecoveryPath)
            {
                CliPrintf (CliHandle,
                           " \r             R (Recoverypath message reception) \n ");
            }
            if (RPTE_GR_RECOVERY_PATH_TX & i4RecoveryPath)
            {
                CliPrintf (CliHandle,
                           " \r             T (Recoverypath message transmission) \n ");
            }
            if (RPTE_GR_RECOVERY_PATH_SREFRESH & i4RecoveryPath)
            {
                CliPrintf (CliHandle,
                           " \r             S (S Refresh Recoverypath message capable) \n ");
            }
        }
    }

    if (nmhGetFsMplsRsvpTeNbrGrRecoveryTime (i4IfIndex, u4Addr,
                                             &i4RecoveryTime) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r    Recovery Time: %d seconds\n",
                   i4RecoveryTime);
    }

    CliPrintf (CliHandle, "\r    Restart Time: %d seconds\n", i4RestartTime);

    if (nmhGetFsMplsRsvpTeNbrGrProgressStatus (i4IfIndex, u4Addr,
                                               &i4GrProgressStatus)
        == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r    GR Progress Status:");

        switch (i4GrProgressStatus)
        {
            case RPTE_GR_NOT_STARTED:
                CliPrintf (CliHandle, " GR Not Started\n");
                break;

            case RPTE_GR_SHUT_DOWN_IN_PROGRESS:
                CliPrintf (CliHandle, " GR Shutdown in Progress\n");
                break;

            case RPTE_GR_RESTART_IN_PROGRESS:
                CliPrintf (CliHandle, " GR Restart in Progress\n");
                break;

            case RPTE_GR_ABORTED:
                CliPrintf (CliHandle, " GR Aborted\n");
                break;

            case RPTE_GR_RECOVERY_IN_PROGRESS:
                CliPrintf (CliHandle, " GR Recovery in Progress\n");
                break;

            case RPTE_GR_COMPLETED:
                CliPrintf (CliHandle, " GR Recovery Completed\n");
                break;

            default:
                CliPrintf (CliHandle, " None\n");
                break;
        }
    }

    CliPrintf (CliHandle, "\n");
    return;
}

/******************************************************************************
 * Function Name : RpteCliAddNeighbor
 * Description   : This routine adds a RSVP-neighbor entry
 * Input(s)      : CliHandle - Cli Context Handle
 *                 i4IfIndex - Interface Index   
 *                 u4NbrId - Neighbor node address
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 * *****************************************************************************/

PRIVATE INT4
RpteCliAddNeighbor (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4NbrId)
{
    UINT4               u4Error = 0;

    if ((nmhTestv2FsMplsRsvpTeNbrStatus (&u4Error, i4IfIndex,
                                         u4NbrId,
                                         CREATE_AND_WAIT) != SNMP_SUCCESS)
        || (nmhSetFsMplsRsvpTeNbrStatus (i4IfIndex, u4NbrId, CREATE_AND_WAIT) !=
            SNMP_SUCCESS))
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to change neighbor-entry status to CREATE_AND_WAIT.\n");
        return CLI_FAILURE;
    }

    if ((nmhTestv2FsMplsRsvpTeIfStatus (&u4Error, i4IfIndex,
                                        NOT_IN_SERVICE) != SNMP_SUCCESS)
        || (nmhSetFsMplsRsvpTeIfStatus (i4IfIndex,
                                        NOT_IN_SERVICE) != SNMP_SUCCESS))
    {
        nmhSetFsMplsRsvpTeNbrStatus (i4IfIndex, u4NbrId, DESTROY);
        CliPrintf (CliHandle, "\r%% Can not make the interface "
                   "down or ensure that RSVP interface exist\r\n");
        return CLI_FAILURE;

    }
    if ((nmhTestv2FsMplsRsvpTeNbrHelloSupport (&u4Error, i4IfIndex,
                                               u4NbrId,
                                               RPTE_NBR_HELLO_SPRT_ENA) !=
         SNMP_SUCCESS)
        ||
        (nmhSetFsMplsRsvpTeNbrHelloSupport
         (i4IfIndex, u4NbrId, RPTE_NBR_HELLO_SPRT_ENA) != SNMP_SUCCESS))
    {
        nmhSetFsMplsRsvpTeNbrStatus (i4IfIndex, u4NbrId, DESTROY);
        nmhSetFsMplsRsvpTeIfStatus (i4IfIndex, ACTIVE);

        CliPrintf (CliHandle, "\r%% Can not enable Neighbor "
                   "Hello Support\r\n");
        return CLI_FAILURE;
    }
    if ((nmhTestv2FsMplsRsvpTeNbrEncapsType (&u4Error, i4IfIndex,
                                             u4NbrId,
                                             RPTE_IP_ENCAP) != SNMP_SUCCESS)
        || (nmhSetFsMplsRsvpTeNbrEncapsType (i4IfIndex, u4NbrId, RPTE_IP_ENCAP)
            != SNMP_SUCCESS))
    {
        nmhSetFsMplsRsvpTeNbrStatus (i4IfIndex, u4NbrId, DESTROY);
        nmhSetFsMplsRsvpTeIfStatus (i4IfIndex, ACTIVE);

        CliPrintf (CliHandle, "\r%% Unable to set Neighbor Encaps type \n");
        return CLI_FAILURE;
    }
    if ((nmhTestv2FsMplsRsvpTeNbrStatus (&u4Error, i4IfIndex,
                                         u4NbrId, ACTIVE) != SNMP_SUCCESS)
        || (nmhSetFsMplsRsvpTeNbrStatus (i4IfIndex, u4NbrId,
                                         ACTIVE) != SNMP_SUCCESS))
    {
        nmhSetFsMplsRsvpTeNbrStatus (i4IfIndex, u4NbrId, DESTROY);
        nmhSetFsMplsRsvpTeIfStatus (i4IfIndex, ACTIVE);
        CliPrintf (CliHandle, "\r%% Rsvp neighbor status can not be set \n");
        return CLI_FAILURE;
    }
    if ((nmhTestv2FsMplsRsvpTeIfStatus (&u4Error, i4IfIndex,
                                        ACTIVE) != SNMP_SUCCESS)
        || (nmhSetFsMplsRsvpTeIfStatus (i4IfIndex, ACTIVE) != SNMP_SUCCESS))
    {
        CliPrintf (CliHandle, "\r%% Can not make the interface "
                   "up or ensure that RSVP interface exist \r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : RpteCliDelNeighbor
 * Description   : This routine deletes a RSVP-neighbor entry
 * Input(s)      : CliHandle - Cli Context Handle
 *                 i4IfIndex - Interface Index
 *                 u4NbrId - Neighbor node address
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 * *****************************************************************************/

PRIVATE INT4
RpteCliDelNeighbor (tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4NbrId)
{
    UINT4               u4Error = 0;
    tIfEntry           *pIfEntry = NULL;

    if (RpteGetIfEntry (i4IfIndex, &pIfEntry) == RPTE_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Interface Entry does not exist.\n");
        return CLI_FAILURE;
    }

    if (RPTE_IF_NUM_TNL_ASSOC (pIfEntry) == RSVPTE_ZERO)
    {
        if ((nmhTestv2FsMplsRsvpTeNbrStatus (&u4Error, i4IfIndex,
                                             u4NbrId, DESTROY) != SNMP_SUCCESS)
            || (nmhSetFsMplsRsvpTeNbrStatus (i4IfIndex,
                                             u4NbrId, DESTROY) != SNMP_SUCCESS))
        {
            CliPrintf (CliHandle, "\r%% Unable to Delete neighbor-entry.\n");
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r%% Tunnels are associated with this neighbor-entry, "
                   "\nunable to Delete neighbor-entry.\n");
        return CLI_FAILURE;
    }
}

/******************************************************************************
 * Function Name : RpteCliCheckRsvpCommand
 * Description   : This routine to check whether rsvp command is configured or not
 * Input(s)      : u1RsvpFlag - Flag for Rsvp mode
 * Output(s)     : None.
 * Return(s)     : None.
 * *****************************************************************************/

VOID
RpteCliCheckRsvpCommand (tCliHandle CliHandle, UINT1 *u1RsvpFlag)
{

    if ((*u1RsvpFlag) == RPTE_FALSE)
    {
        CliPrintf (CliHandle, "\r\nrsvp");
        FilePrintf (CliHandle, "\r\nrsvp");
        *u1RsvpFlag = RPTE_TRUE;
    }
    return;
}

/*****************************************************************************
 * Function Name : RpteFrrFacShowRunningConfig
 * Description   : This routine displays config commands for FRR facility
 *                 backup configuration
 * Input(s)      : CliHandle - Index of current CLI context
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 ******************************************************************************/

INT4
RpteFrrFacShowRunningConfig (tCliHandle CliHandle)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4L3IfIndex = RPTE_ZERO;
    tCfaIfInfo          CfaIfInfo;
    tIfEntry           *pIfEntry = NULL;
    tTMO_DLL           *pIfFacTnlsList = NULL;
    tTMO_DLL_NODE      *pIfFacTnlsNode = NULL;
    tFacilityRpteBkpTnl *pTmpFacilityRpteBkpTnl = NULL;
    INT1               *pi1IfName = NULL;
    BOOL1               bLockRequest = FALSE;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { RPTE_ZERO };

    pi1IfName = (INT1 *) &au1IfName[0];
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (CfaApiGetFirstMplsIfInfo (&u4IfIndex, &CfaIfInfo) == CLI_SUCCESS)
    {
        do
        {
            if (CLI_SUCCESS == CfaUtilGetL3IfFromMplsIf (u4IfIndex,
                                                         &u4L3IfIndex,
                                                         bLockRequest))
            {
                MPLS_RSVPTE_LOCK ();
                if (RpteGetIfEntry ((INT4) u4IfIndex, &pIfEntry) !=
                    RPTE_FAILURE)
                {
                    pIfFacTnlsList =
                        (tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfEntry));
                    if (pIfFacTnlsList != NULL)
                    {
                        TMO_DLL_Scan (pIfFacTnlsList, pIfFacTnlsNode,
                                      tTMO_DLL_NODE *)
                        {
                            if (CLI_FAILURE !=
                                CfaCliConfGetIfName (u4L3IfIndex, pi1IfName))
                            {
                                CliPrintf (CliHandle, "\n!");
                                FilePrintf (CliHandle, "\n!");
                                CliPrintf (CliHandle, "\r\ninterface %s",
                                           pi1IfName);
                                FilePrintf (CliHandle, "\r\ninterface %s",
                                            pi1IfName);
                                pTmpFacilityRpteBkpTnl =
                                    (tFacilityRpteBkpTnl *) (pIfFacTnlsNode);
                                CliPrintf (CliHandle,
                                           "\r\nmpls traffic-eng backup-path %u",
                                           RPTE_FRR_FACILITY_BKP_TNL_INDEX
                                           (pTmpFacilityRpteBkpTnl));
                                FilePrintf (CliHandle,
                                            "\r\nmpls traffic-eng backup-path %u",
                                            RPTE_FRR_FACILITY_BKP_TNL_INDEX
                                            (pTmpFacilityRpteBkpTnl));
                                CliPrintf (CliHandle, "\n!");
                                FilePrintf (CliHandle, "\n!");
                            }
                        }
                    }
                }
                MPLS_RSVPTE_UNLOCK ();
            }
        }
        while (CfaApiGetNextMplsIfInfo (u4IfIndex, &u4IfIndex, &CfaIfInfo)
               == CLI_SUCCESS);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : RpteCliSetReoptimizeTimerValue 
 * Description   : This routine is used to configure Lsp Reoptimization Time
 * Input(s)      : CliHandle - Cli Context Handle
 *                 i4RsvpTeReoptimizeTime - Reoptimize Time
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 * *****************************************************************************/

PRIVATE INT4
RpteCliSetReoptimizeTimerValue (tCliHandle CliHandle,
                                INT4 i4RsvpTeReoptimizeTime)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsMplsRsvpTeReoptimizeTime (&u4Error, i4RsvpTeReoptimizeTime)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid Reoptimize Time\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsMplsRsvpTeReoptimizeTime (i4RsvpTeReoptimizeTime);
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : RpteCliSetEroCacheTimerValue
 * Description   : This routine deletes a RSVP-neighbor entry
 * Input(s)      : CliHandle - Cli Context Handle
 *                 i4RsvpTeEroCacheTime - ERO Cache Time
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 * *****************************************************************************/

PRIVATE INT4
RpteCliSetEroCacheTimerValue (tCliHandle CliHandle, INT4 i4RsvpTeEroCacheTime)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsMplsRsvpTeEroCacheTime (&u4Error, i4RsvpTeEroCacheTime)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%Invalid Ero Cache Time\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsMplsRsvpTeEroCacheTime (i4RsvpTeEroCacheTime);

    return CLI_SUCCESS;

}

/******************************************************************************
 * Function Name : RpteCliLspReoptLinkMaintenance
 * Description   : This routine deletes a RSVP-neighbor entry
 * Input(s)      : CliHandle - Cli Context Handle
 *                 i4IfIndex - Interface Index
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 * *****************************************************************************/

PRIVATE INT4
RpteCliLspReoptLinkMaintenance (tCliHandle CliHandle, INT4 i4IfIndex)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsMplsRsvpTeReoptLinkMaintenance (&u4Error, i4IfIndex)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Link Maintenance Failed\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsRsvpTeReoptLinkMaintenance (i4IfIndex) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Link Maintenance Failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : RpteCliLspReoptNodeMaintenance
 * Description   : This routine deletes a RSVP-neighbor entry
 * Input(s)      : CliHandle - Cli Context Handle
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 * *****************************************************************************/

PRIVATE INT4
RpteCliLspReoptNodeMaintenance (tCliHandle CliHandle)
{
    UINT4               u4Error = 0;

    if (nmhTestv2FsMplsRsvpTeReoptNodeMaintenance (&u4Error, RPTE_ZERO)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Node Maintenance Failed\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsMplsRsvpTeReoptNodeMaintenance (RPTE_ZERO);

    return CLI_SUCCESS;

}

/******************************************************************************
 * Function Name : RpteCliLspReoptNodeMaintenance
 * Description   : This routine deletes a RSVP-neighbor entry
 * Input(s)      : CliHandle - Cli Context Handle
 * Output(s)     : None.
 * Return(s)     : CLI_FAILURE/CLI_SUCCESS
 * *****************************************************************************/

PRIVATE INT4
RpteCliShowLspReoptParameters (tCliHandle CliHandle)
{
    INT4                i4ReoptimizeTime = 0;
    INT4                i4EroCacheTime = 0;

    nmhGetFsMplsRsvpTeReoptimizeTime (&i4ReoptimizeTime);
    nmhGetFsMplsRsvpTeEroCacheTime (&i4EroCacheTime);

    CliPrintf (CliHandle, "\rLSP Reoptimization Parameters:\r\n");
    CliPrintf (CliHandle, "\r\tReoptimize Time : %d\r\n", i4ReoptimizeTime);
    CliPrintf (CliHandle, "\r\tERO Cache Time : %d\r\n", i4EroCacheTime);

    return CLI_SUCCESS;
}

#endif
