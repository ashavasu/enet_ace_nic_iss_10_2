/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rsvptesz.c,v 1.4 2014/12/24 10:58:29 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _RSVPTESZ_C
#include "rpteincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
RsvpteSizingMemCreateMemPools ()
{
    UINT4               u4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RSVPTE_MAX_SIZING_ID; i4SizingId++)
    {
        u4RetVal =
            MemCreateMemPool (FsRSVPTESizingParams[i4SizingId].u4StructSize,
                              FsRSVPTESizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(RSVPTEMemPoolIds[i4SizingId]));
        if (u4RetVal ==  MEM_FAILURE)
        {
            RsvpteSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
RsvpteSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsRSVPTESizingParams);
    IssSzRegisterModulePoolId (pu1ModName, RSVPTEMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
RsvpteSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < RSVPTE_MAX_SIZING_ID; i4SizingId++)
    {
        if (RSVPTEMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (RSVPTEMemPoolIds[i4SizingId]);
            RSVPTEMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
