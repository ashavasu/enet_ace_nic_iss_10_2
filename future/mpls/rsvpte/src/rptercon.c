
/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: rptercon.c,v 1.14 2014/12/24 10:58:29 siva Exp $
 *
 ****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved                        
 ****************************************************************************
 * FILE NAME             : rptercon.c                                        
 * PRINCIPAL AUTHOR      : Aricent Inc.                     
 * SUBSYSTEM NAME        : MPLS                                              
 * MODULE NAME           : RSVP-TE                                           
 * LANGUAGE              : ANSI-C                                            
 * TARGET ENVIRONMENT    : Linux (Portable)                                  
 * DATE OF FIRST RELEASE :                                                   
 * DESCRIPTION           : This file contains the functions of  Resv Conf    
 *                         Handler module.                                   
 *--------------------------------------------------------------------------*/

#include "rpteincs.h"

/****************************************************************************/
/* Function Name   : RpteRchProcessResvConfMsg                              */
/* Description     : This function receives a Resv Conf message and         */
/*                   forwards to the destination address in the Resv Conf   */
/*                   object of the Packet Map                               */
/* Input (s)       : *pPktMap - Pointer to Packet Map                       */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRchProcessResvConfMsg (tPktMap * pPktMap)
{
    tResvConf          *pResvConf = NULL;
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pBaseCtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pCtTmpTnlInfo = NULL;
    tRsvpTeTnlInfo     *pCtPrevInTnlInfo = NULL;
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    tRpteKey            RpteKey;
    BOOL1               bFound = RPTE_FALSE;
    BOOL1               bIsBkpTnl = RPTE_NO;
    UINT1               u1DetourGrpId = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_RCH_ETEXT, "RpteRchProcessResvConfMsg : ENTRY \n");

    pResvConf = &RESV_CONF_OBJ (PKT_MAP_RESV_CONF_OBJ (pPktMap));

    if (RPTE_CHECK_TNL_RESV_CONF_MSG (pPktMap, pCtTnlInfo) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RCON: TnlInfo doesnot exist for the received message - "
                    "mesg dropped \n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RCH_ETEXT,
                    "RpteRchProcessResvConfMsg : INTMD-EXIT \n");
        return;
    }
    pBaseCtTnlInfo = pCtTnlInfo;
    if ((RpteUtlIsUpStrMsgRcvdOnBkpPath (pPktMap, &pCtTnlInfo,
                                         &pCtPrevInTnlInfo,
                                         &bIsBkpTnl)) == RPTE_FAILURE)
    {
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RCH_ETEXT,
                    "RpteRchProcessResvConfMsg : INTMD-EXIT \n");
        return;
    }
    if (((RPTE_IS_MP (RPTE_FRR_NODE_STATE (pBaseCtTnlInfo))) &&
         (bIsBkpTnl == RPTE_YES)) ||
        (RptePvmLocalAddr (OSIX_NTOHL (RESV_CONF_ADDR (pResvConf)))
         == RPTE_SUCCESS))
    {
        /* "Dst Addr matches the local interface address\n") 
         * Disable the Subsequent sending of Resv Conf Object 
         */
        if (RSVPTE_TNL_RSB (pCtTnlInfo) == NULL)
        {
            RSVPTE_DBG (RSVPTE_RCH_ETEXT,
                        "RpteRchProcessResvConfMsg : RSB is NULL INTMD EXIT \n");
            return;
        }
        RPTE_TNL_RESV_CONF_ADDR (pCtTnlInfo) = RSVPTE_ZERO;
        RpteUtlCleanPktMap (pPktMap);
        /* NOTE : Since ResvConf Object is removed from the resv state
         * a different msg id is generated for the next outgoing resv
         * msg of the tunnel. */
        if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) &
             RPTE_OUT_RESV_MSG_ID_PRESENT) == RPTE_OUT_RESV_MSG_ID_PRESENT)
        {
            RpteKey.u4_Key = RPTE_TNL_OUT_RESV_MSG_ID (pCtTnlInfo);
            RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                 RPTE_LCL_MSG_ID_DB_PREFIX,
                                 RpteMIHReleaseTrieInfo);
        }
        RSVPTE_DBG (RSVPTE_RCH_ETEXT, "RpteRchProcessResvConfMsg : EXIT \n");
        return;
    }

    if (RPTE_IS_FRR_CAPABLE (pCtTnlInfo) != RPTE_TRUE)
    {
        PKT_MAP_DST_ADDR (pPktMap) = pCtTnlInfo->u4DnNextHop;
        PKT_MAP_OUT_IF_ENTRY (pPktMap) = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB
                                                           (pCtTnlInfo));
        if (PKT_MAP_OUT_IF_ENTRY (pPktMap) == NULL)
        {
            RSVPTE_DBG (RSVPTE_RCH_ETEXT,
                        "RpteRchProcessResvConfMsg : INTMD EXIT \n");
            return;
        }
        PKT_MAP_SRC_ADDR (pPktMap) =
            IF_ENTRY_ADDR (PKT_MAP_OUT_IF_ENTRY (pPktMap));

        RptePbSendMsg (pPktMap);
        RSVPTE_DBG (RSVPTE_RCH_ETEXT, "RpteRchProcessResvConfMsg : EXIT \n");
        return;
    }
    /* FRR PLR processing */
    pRsvpTeFastReroute =
        &PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pBaseCtTnlInfo));

    /* If FAST_REROUTE is not present, default method followed is
     * One-to-One. */
    if (((pRsvpTeFastReroute->u1Flags == RPTE_ZERO) ||
         (pRsvpTeFastReroute->u1Flags == RSVPTE_TNL_FRR_ONE2ONE_METHOD))
        && (RPTE_IS_PLR_OR_PLRAWAIT (RPTE_FRR_NODE_STATE (pBaseCtTnlInfo))))
    {
        /* If node state is PLR or PLR_AWAIT and if local protection is
         * not in use, then PATH Tear is duplicated on both path */
        if ((RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) & LOCAL_PROT_IN_USE) ==
            LOCAL_PROT_IN_USE)
        {
            pCtTnlInfo = RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo);
        }
    }
    else if (((pRsvpTeFastReroute->u1Flags != RPTE_ZERO) &&
              (pRsvpTeFastReroute->u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD))
             && (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pBaseCtTnlInfo))))
    {
        if ((RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) & LOCAL_PROT_IN_USE) ==
            LOCAL_PROT_IN_USE)
        {
            pCtTnlInfo->u1SendToByPassTnl = RPTE_TRUE;
        }
    }

    if (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pBaseCtTnlInfo)) &&
        (RSVPTE_FRR_DETOUR_MERGING_CAPABLE (gpRsvpTeGblInfo) == RPTE_TRUE))
    {
        u1DetourGrpId = RPTE_FRR_DETOUR_GRP_ID (pCtTnlInfo);
        pCtTmpTnlInfo = pCtTnlInfo;
        do
        {
            if (RPTE_FRR_DETOUR_GRP_ID (pCtTmpTnlInfo) != u1DetourGrpId)
            {
                pCtTmpTnlInfo = RPTE_FRR_IN_TNL (pCtTmpTnlInfo);
                continue;
            }
            if (RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pCtTmpTnlInfo) ==
                RPTE_TRUE)
            {
                bFound = RPTE_TRUE;
                pCtTnlInfo = pCtTmpTnlInfo;
                break;
            }
            pCtTmpTnlInfo = RPTE_FRR_IN_TNL (pCtTmpTnlInfo);
        }
        while (pCtTmpTnlInfo != NULL);
        /* Eventually pRsvpTeTnlInfo contains the RSVP Tnl Info which has the 
           least no. of ER-HOPS to reach the destination */
        if (bFound != RPTE_TRUE)
        {
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_RCH_ETEXT,
                        "RpteRchProcessResvConfMsg : INTMD-EXIT \n");
            return;

        }
    }
    RPTE_PKT_MAP_TNL_INFO (pPktMap) = pCtTnlInfo;
    PKT_MAP_DST_ADDR (pPktMap) = pCtTnlInfo->u4DnNextHop;
    if ((RSVPTE_TNL_PSB (pCtTnlInfo) == NULL) ||
        (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pCtTnlInfo)) == NULL))
    {
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RCH_ETEXT,
                    "RpteRchProcessResvConfMsg : INTMD-EXIT \n");
        return;
    }

    PKT_MAP_OUT_IF_ENTRY (pPktMap) = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB
                                                       (pCtTnlInfo));
    PKT_MAP_SRC_ADDR (pPktMap) = IF_ENTRY_ADDR (PKT_MAP_OUT_IF_ENTRY (pPktMap));

    RptePbSendMsg (pPktMap);
    RSVPTE_DBG (RSVPTE_RCH_ETEXT, "RpteRchProcessResvConfMsg : EXIT \n");
}

/****************************************************************************/
/* Function Name   : RpteRchSendResvConf                                    */
/* Description     : This procedures frames and sends resv confirm  message */
/*                   for the Egress of the tunnel                           */
/* Input (s)       : pCtTnlInfo     - Pointer to RsvpTeTnlInfo              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRchSendResvConf (const tRsvpTeTnlInfo * pCtTnlInfo)
{
    UINT4               u4TmpAddr = RSVPTE_ZERO;
    UINT1              *pPduPtr = NULL;
    tObjHdr            *pObjHdr = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tRsb               *pRsb = NULL;
    tIfEntry           *pIfEntry = NULL;
    tPktMap             PktMap;
    UINT2               u2ResvConfSize = RSVPTE_ZERO;
    UINT2               u2FSpecObjLength = RSVPTE_ZERO;
    tFlowSpec          *pFwdFlowSpec = NULL;

    RSVPTE_DBG (RSVPTE_RCH_ETEXT, "RpteRchSendResvConf : ENTRY \n");

    RpteUtlInitPktMap (&PktMap);
    pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
    if (pRsb == NULL)
    {
        RSVPTE_DBG (RSVPTE_RCH_PRCS, "RCON : RSB is NULL \n");
        RSVPTE_DBG (RSVPTE_RCH_ETEXT, " RpteRchSendResvConf : INTMD-EXIT \n");
        return;
    }
    pIfEntry = RSB_OUT_IF_ENTRY (pRsb);
    if ((IF_ENTRY_STATUS (pIfEntry) != ACTIVE) ||
        (IF_ENTRY_ENABLED (pIfEntry) != RPTE_ENABLED))
    {
        RSVPTE_DBG (RSVPTE_RCH_PRCS,
                    "RCON : Outgoing Interface is not Active \n");
        RSVPTE_DBG (RSVPTE_RCH_ETEXT, " RpteRchSendResvConf : INTMD-EXIT \n");
        return;
    }
    /* Calulate the ResvConfMesg size */
    u2ResvConfSize = sizeof (tRsvpHdr) + RPTE_IPV4_SSN_OBJ_LEN +
        sizeof (tResvConfObj) + sizeof (tErrorSpecObj) + sizeof (tStyleObj) +
        RPTE_IPV4_FLTR_SPEC_OBJ_LEN + sizeof (tFlowSpecObj);
    pFwdFlowSpec = &RSB_FLOW_SPEC (pRsb);

    if (((tFlowSpec *) pFwdFlowSpec)->SrvHdr.u1SrvID == FLOWSPEC_CLS_HDR_NUMBER)
    {
        u2FSpecObjLength = sizeof (tFlowSpecObj) - sizeof (tGsRSpec);
        u2ResvConfSize = (UINT2)(u2ResvConfSize - sizeof (tGsRSpec));
    }
    else
    {
        u2FSpecObjLength = sizeof (tFlowSpecObj);
    }

    /* Allocate memory for the RSVP packet */
    PKT_MAP_RSVP_PKT (&PktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);
    if (PKT_MAP_RSVP_PKT (&PktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_RCH_PRCS,
                    "RCON: Mem Alloc failed in RpteRchSendResvConf..\n");
        RSVPTE_DBG (RSVPTE_RCH_ETEXT, "RpteRchSendResvConf : INTMD-EXIT \n");
        return;
    }
    MEMSET (PKT_MAP_RSVP_PKT (&PktMap), RSVPTE_ZERO, u2ResvConfSize);
    PKT_MAP_RSVP_PKT_ALLOC (&PktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    /* Copy Source address i.e this node's address */
    PKT_MAP_SRC_ADDR (&PktMap) = OSIX_NTOHL (RSVPTE_TNL_INGRESS (pCtTnlInfo));

    /* Assuming by default ResvConf Msg will be sent with Router Alert option */
    /* 
     * NOTE: Currently RSVPTE assumes there won't be any IP options other
     * than the Router Alert present in the IpHdr while calculating Msg Size 
     */
    /* Enable RA option needed */

    /* RESV Confirm Msg of E2E LSP over FA TELink should not have RAO */
    if (pCtTnlInfo->u4FALspSrcOrDest == RPTE_ZERO)
    {
        PKT_MAP_RA_OPT_NEEDED (&PktMap) = RPTE_YES;
        PKT_MAP_OPT_LEN (&PktMap) = sizeof (tRaOpt);
        /* Copy Destination address i.e next node's address */
        PKT_MAP_DST_ADDR (&PktMap) = pCtTnlInfo->u4DnNextHop;
    }
    else
    {
        /* For E2E LSP at ingress of FA LSP, Destination of IP Header should be
         * Egress of FA LSP */
        PKT_MAP_DST_ADDR (&PktMap) = pCtTnlInfo->u4FALspSrcOrDest;
    }

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (&PktMap);
    MEMSET (pRsvpHdr, RSVPTE_ZERO, sizeof (tRsvpHdr));

    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = RESVCONF_MSG;
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (u2ResvConfSize);

    pObjHdr =
        (tObjHdr *) (VOID *) (PKT_MAP_RSVP_PKT (&PktMap) + sizeof (tRsvpHdr));

    /* copy RsvpTeSessionObj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);
    pPduPtr = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    u4TmpAddr = RSVPTE_TNL_EGRESS (pCtTnlInfo);
    MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPduPtr += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPduPtr, RSVPTE_ZERO);    /* reserved field */

    RPTE_PUT_2_BYTES (pPduPtr,
                      (UINT2) OSIX_HTONS (RSVPTE_TNL_TNLINDX (pCtTnlInfo)));
    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pCtTnlInfo)), u4TmpAddr);

    MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPduPtr += RSVPTE_IPV4ADR_LEN;

    pObjHdr = (tObjHdr *) (VOID *) pPduPtr;

    /* Copy ErrorSpec */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tErrorSpecObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_ERROR_SPEC_CLASS_NUM_TYPE);
    pPduPtr = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    u4TmpAddr = IF_ENTRY_ADDR (pIfEntry);
    u4TmpAddr = OSIX_NTOHL (u4TmpAddr);
    MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);
    pPduPtr += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_4_BYTES (pPduPtr, RSVPTE_ZERO);    /* Error Flags ,Error Code and 
                                                   Error Value are set to 0 */

    pObjHdr = (tObjHdr *) (VOID *) pPduPtr;

    /* Fill ResvConf Obj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tResvConfObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RESV_CONFIRM_CLASS_NUM_TYPE);
    pPduPtr = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    u4TmpAddr = RSVPTE_TNL_EGRESS (pCtTnlInfo);
    MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);
    pPduPtr += RSVPTE_IPV4ADR_LEN;
    pObjHdr = (tObjHdr *) (VOID *) pPduPtr;

    /* Fill  Style Obj * */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tStyleObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS (IPV4_STYLE_CLASS_NUM_TYPE);
    STYLE_OBJ ((tStyleObj *) pObjHdr) = RSB_STYLE (pRsb);
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* Copy FlowSpec or RsvpTeFlowSpec Obj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (u2FSpecObjLength);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_FLOW_SPEC_CLASS_NUM_TYPE);
    FLOW_SPEC_OBJ ((tFlowSpecObj *) (VOID *) pObjHdr) = *pFwdFlowSpec;
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* copy RsvpTeFilterSpecObj */

    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_FLTR_SPEC_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS
        (IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE);
    RPTE_FILTER_SPEC_OBJ ((tRsvpTeFilterSpecObj *) pObjHdr) =
        pCtTnlInfo->pRsb->RsvpTeFilterSpec;
    pObjHdr = NEXT_OBJ (pObjHdr);

    PKT_MAP_BUILD_PKT (&PktMap) = RPTE_YES;
    PKT_MAP_OUT_IF_ENTRY (&PktMap) = RSB_OUT_IF_ENTRY (pRsb);
    PKT_MAP_TX_TTL (&PktMap) = IF_ENTRY_TTL (pIfEntry);
    RSVP_HDR_SEND_TTL (pRsvpHdr) = IF_ENTRY_TTL (pIfEntry);
    RptePbSendMsg (&PktMap);
    RSVPTE_DBG (RSVPTE_RCH_ETEXT, "RpteRchSendResvConf : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*                       End of file rptercon.c                              */
/*---------------------------------------------------------------------------*/
