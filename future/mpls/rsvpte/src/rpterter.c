/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rpterter.c,v 1.33 2017/06/08 11:40:32 siva Exp $
 *
 * Description: This file contains routines for the ResvTear Module.
 ********************************************************************/
#include "rpteincs.h"

/****************************************************************************/
/* Function Name   : RpteRthProcessResvTearMsg                              */
/* Description     : This function receives a Resv Tear message and deletes */
/*                   the corressponding Rsb according to reservation style. */
/*                   It forwards the Resv Tear message to the Previous hop  */
/* Input (s)       : *pPktMap - Pointer to Packet Map                       */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRthProcessResvTearMsg (tPktMap * pPktMap)
{
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;
    tRsvpTeTnlInfo     *pCtInTnlInfo = NULL;
    tRsvpTeSession     *pRsvpTeSession = NULL;
    tPsb               *pPsb = NULL;
    tRsb               *pRsb = NULL;
    tRsb               *pTmpRsb = NULL;
    tRsvpHop           *pNhop = NULL;
    tStyle             *pStyle = NULL;
    tRsvpTeFilterSpec  *pRsvpTeFilterSpec = NULL;
    tErrorSpec          ErrorSpec;
    tFlowSpec          *pFlowSpec = NULL;
    tIfEntry           *pIfEntry = NULL;
    tRpteKey            RpteKey;
    UINT2               u2FSpecObjLength = RSVPTE_ZERO;
    UINT1               u1ResvRefreshNeeded;
    UINT1               u1ResvRsrc = RPTE_FALSE;
    UINT1               u1DetourGrpId = RPTE_ZERO;
    BOOL1               bIsBkpTnl = RPTE_NO;

    u1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;

    RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthProcessResvTearMsg : ENTRY \n");

    /* Check for mandatory objects */
    if ((RpteUtlResvManObjHandler (pPktMap, &pRsvpTeSession, &pNhop,
                                   &pFlowSpec,
                                   &pRsvpTeFilterSpec)) != RPTE_SUCCESS)
    {
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RTH_PRCS,
                    "RESVTEAR: Received message has mandatory objects "
                    "missing\n");
        RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                    " RpteRthProcessResvTearMsg : INTMD-EXIT \n");
        return;
    }

    pIfEntry = PKT_MAP_IF_ENTRY (pPktMap);

    pStyle = &STYLE_OBJ (PKT_MAP_STYLE_OBJ (pPktMap));
    /* Check whether TnlInfo exists for the ResvTear mesg */
    if (RPTE_CHECK_TNL_RESV_TEAR_MSG (pPktMap, pCtTnlInfo) == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_RTH_PRCS,
                    "RESVTEAR: TnlInfo doesnot exist for the received message "
                    "- Resv Tear Mesg Dropped \n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                    " RpteRthProcessResvTearMsg : INTMD-EXIT \n");
        return;
    }

    /* Let's not allow to hack our stack. */
    if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_EGRESS)
    {
        RSVPTE_DBG (RSVPTE_RTH_PRCS,
                    "RESVTEAR: REsv Tear msg is not valid at egress node "
                    "- Resv Tear Mesg Dropped \n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                    " RpteRthProcessResvTearMsg : INTMD-EXIT \n");
        return;
    }
    if (pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
        MPLS_TE_DEDICATED_ONE2ONE)
    {
        pCtTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_NOT_AVAIL;
    }

    /* Out of Order Processing */
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        if (((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) & RPTE_DSTR_MSG_ID_PRESENT)
             == RPTE_DSTR_MSG_ID_PRESENT) &&
            ((INT4) RPTE_TNL_DSTR_MAX_MSG_ID (pCtTnlInfo) >=
             (INT4) RPTE_PKT_MAP_MSG_ID (pPktMap)))
        {
            RSVPTE_DBG (RSVPTE_RTH_PRCS,
                        "Msg ID Out Of Order. Sliently Dropping sshhh....\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        "RpteRthProcessResvTearMsg : INTMD-EXIT \n");
            return;
        }
        RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) |= RPTE_DSTR_MSG_ID_PRESENT;
        RPTE_TNL_DSTR_MAX_MSG_ID (pCtTnlInfo) = RPTE_PKT_MAP_MSG_ID (pPktMap);
    }

    /* Check whether Rsb exists for the ResvTear mesg */
    pRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
    if (pRsb == NULL)
    {
        RSVPTE_DBG (RSVPTE_RTH_PRCS,
                    "RESVTEAR: Rsb doesnot exist for the received message "
                    "- Resv Tear Mesg Dropped \n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                    " RpteRthProcessResvTearMsg : INTMD-EXIT \n");
        return;
    }

    /* Compare the Style of ResvTear mesg with the style present in RsbCmnHdr */
    if (RpteUtlCompareStyle (pStyle, &RSB_STYLE (pRsb)) != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_RTH_PRCS,
                    "RESVTEAR: Incoming Style contradicts with prev Style "
                    "- Resv Tear Mesg Dropped\n");
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                    " RpteRthProcessResvTearMsg : INTMD-EXIT \n");
        return;
    }

    /* Compare & ensure the FlowSpecs before Tearing it down */
    if (((tFlowSpec *) pFlowSpec)->SrvHdr.u1SrvID == FLOWSPEC_CLS_HDR_NUMBER)
    {
        u2FSpecObjLength = sizeof (tFlowSpecObj) - sizeof (tGsRSpec);
    }
    else
    {
        u2FSpecObjLength = sizeof (tFlowSpecObj);
    }

    u2FSpecObjLength = (UINT2) (u2FSpecObjLength - sizeof (tObjHdr));

    /* Check whether RESV Tear Message is received on the backup path or 
     * primary path */
    /* FRR NON-CAPABLE case handle */
    if (RPTE_IS_FRR_CAPABLE (pCtTnlInfo) != RPTE_TRUE)
    {
        if ((MEM_CMP (pFlowSpec, &RSB_FLOW_SPEC (pRsb),
                      u2FSpecObjLength)) != RSVPTE_ZERO)
        {
            RSVPTE_DBG (RSVPTE_RTH_PRCS,
                        "RESVTEAR: Incoming IntServ FlowSpec contradicts with "
                        "prev FlowSpec - Resv Tear Mesg Dropped\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        " RpteRthProcessResvTearMsg : INTMD-EXIT \n");
            return;
        }

        /* Delete the Rsb associated and free the resources, */
        /* Mlib updation to be done                          */
        if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
        {
            RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE, RSVPTE_ZERO,
                                      pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
        }
        else
        {
            RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                      pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
            RpteRhFreeLabel (RSB_IN_IF_ENTRY (RSVPTE_TNL_RSB (pCtTnlInfo)),
                             &(pCtTnlInfo->DownstrInLbl));
            pCtTnlInfo->DownstrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
        }
        if (pCtTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION)
        {
            RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                      pCtTnlInfo, GMPLS_SEGMENT_REVERSE);
            RpteRhFreeLabel (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pCtTnlInfo)),
                             &(pCtTnlInfo->UpStrInLbl));
            pCtTnlInfo->UpStrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
        }

        if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) != RPTE_EGRESS)
        {
            RpteDSRelTrafficControl (pCtTnlInfo);
            RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                        &u1ResvRefreshNeeded, &ErrorSpec);
        }
        RSVPTE_DBG (RSVPTE_RTH_PRCS, "RESVTEAR: Resources are freed \n");
    }
    else if (RPTE_IS_PLR_OR_PLRAWAIT (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
    {
        /* Check whether RESV Tear is received on the primary or backup path,
         * Also, verify whether RESV Tear is received on proper interface. */
        if (IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) ==
            IF_ENTRY_ADDR (RSB_OUT_IF_ENTRY (pCtTnlInfo->pRsb)))
        {
            pTmpRsb = RSVPTE_TNL_RSB (pCtTnlInfo);
            bIsBkpTnl = RPTE_NO;
        }
        else if ((RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) != NULL) &&
                 (RSVPTE_TNL_RSB (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo)) == NULL))
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR: Message received on wrong interface or "
                        " with invalid PSB\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        "RpteRthProcessResvTearMsg : INTMD-EXIT \n");
            return;
        }
        else if ((RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) != NULL) &&
                 (IF_ENTRY_ADDR (PKT_MAP_IF_ENTRY (pPktMap)) ==
                  IF_ENTRY_ADDR (RSB_OUT_IF_ENTRY
                                 (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo)->pRsb))))
        {
            pTmpRsb = RSVPTE_TNL_RSB (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo));
            bIsBkpTnl = RPTE_YES;
        }
        else
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR: Message received on wrong interface or "
                        " with invalid PSB\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        "RpteRthProcessResvTearMsg : INTMD-EXIT \n");
            return;
        }
        if (pTmpRsb == NULL)
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR: Rsb doesnot exist for the received message "
                        "- Resv Tear Mesg Dropped \n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        " RpteRthProcessResvTearMsg : INTMD-EXIT \n");
            return;
        }

        if ((MEM_CMP (pFlowSpec, &RSB_FLOW_SPEC (pTmpRsb),
                      u2FSpecObjLength)) != RSVPTE_ZERO)
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR: Incoming IntServ FlowSpec contradicts with "
                        "prev FlowSpec - Resv Tear Mesg Dropped\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        "RpteRthProcessResvTearMsg : INTMD-EXIT \n");
            return;
        }

        if ((bIsBkpTnl == RPTE_NO)
            && ((RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) == NULL)
                || (RpteUtlIsNodeProtDesired (pCtTnlInfo) != RPTE_YES)))
        {
            /* RESV Tear is received on the primary path, and Out Tunnel Info
             * does not exist. So, removing the ILM or TNL Entries from the 
             * MPLS DB. */
            if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
            {
                RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE, RSVPTE_ZERO,
                                          pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
            }
            else
            {
                RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                          pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
                RpteRhFreeLabel (RSB_IN_IF_ENTRY
                                 (RSVPTE_TNL_RSB (pCtTnlInfo)),
                                 &(pCtTnlInfo->DownstrInLbl));
                pCtTnlInfo->DownstrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
            }
            if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) != RPTE_EGRESS)
            {
                RpteDSRelTrafficControl (pCtTnlInfo);
                RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                            &u1ResvRefreshNeeded, &ErrorSpec);
            }
            RSVPTE_DBG (RSVPTE_FRR_DBG, "RESVTEAR: Resources are freed \n");
        }
        else if ((bIsBkpTnl == RPTE_NO) &&
                 (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) != NULL) &&
                 (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pCtTnlInfo))))
        {
            /* FRR CAPABLE case handle */
            /* RESV Tear is received on the primary path, but Out Tunnel Info
             * exist. So, removing the ILM or TNL Entries from the 
             * MPLS DB for this tunnel and updating ILM or TNL Entries of 
             * ReRouted Tunnel (Out Tunnel Info) into MPLS DB. */
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR: Message received on primary path, "
                        "Out tnl exists"
                        " - Updating MPLS DB with Out Tnl Info. \n");
            if ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pCtTnlInfo)).
                 u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD))
            {
                /* Facility case handling */
                /* Double stacking ILM Create operation */
                /*----------------
                 *                | 
                 *ByPass Tnl Lable|
                 *                |
                 *----------------
                 *                | 
                 *Prot Tnl Lable  |
                 *                |
                 *----------------*/

                if (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
                {
                    RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                              RSVPTE_ZERO, pCtTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);

                    RPTE_FRR_STACK_BIT (pCtTnlInfo) = RPTE_TRUE;
                    RpteRhSendMplsMlibUpdate (RPTE_MPLS_MLIB_ILM_CREATE,
                                              RSVPTE_ZERO, pCtTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);
                }
                else if (RPTE_IS_HEADEND_PLR (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
                {
                    RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE,
                                              RSVPTE_ZERO, pCtTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);
                    RPTE_FRR_STACK_BIT (pCtTnlInfo) = RPTE_TRUE;
                    RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE,
                                              RSVPTE_ZERO, pCtTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);
                }

                /* As per RFC 4090 Section 7.2 ,we MUST not clear the PSB and RSB immediately */

                RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS
                    (RPTE_TE_TNL (pCtTnlInfo)) = TE_TNL_FRR_PROT_STATUS_ACTIVE;
            }
            else
            {
                if (RPTE_IS_HEADEND_PLR (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
                {
                    RSVPTE_DBG (RSVPTE_FRR_DBG,
                                "RESVTEAR: Headend + PLR handle \n");
                    RpteFrrHandleHeadEndPlrFailure (pCtTnlInfo);
                }
                else if (RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
                {
                    RSVPTE_DBG (RSVPTE_FRR_DBG, "RESVTEAR: PLR handle \n");
                    RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                              RSVPTE_ZERO, pCtTnlInfo,
                                              GMPLS_SEGMENT_FORWARD);
                    MEMCPY (&
                            (RSVPTE_TNL_INLBL
                             (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo))),
                            &RSVPTE_TNL_INLBL (pCtTnlInfo), sizeof (uLabel));
                    RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE,
                                              RSVPTE_ZERO,
                                              RPTE_FRR_OUT_TNL_INFO
                                              (pCtTnlInfo),
                                              GMPLS_SEGMENT_FORWARD);
                }

                RpteUtlSetDetourStatus (pCtTnlInfo, RPTE_TRUE);

                RSVPTE_DBG (RSVPTE_FRR_DBG, "RESVTEAR: Detour is Active \n");
            }
            RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) |= LOCAL_PROT_IN_USE;
        }
    }

    if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) & RPTE_IN_RESV_MSG_ID_PRESENT)
        == RPTE_IN_RESV_MSG_ID_PRESENT)
    {
        RpteKey.u8_Key[RPTE_ZERO] = NBR_ENTRY_ADDR (RPTE_TNL_DSTR_NBR
                                                    (pCtTnlInfo));
        RpteKey.u8_Key[RPTE_ONE] = RPTE_TNL_IN_RESV_MSG_ID (pCtTnlInfo);
        RpteTrieDeleteEntry (&RPTE_64_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_NBR_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
    }

    if ((RPTE_TNL_MSG_ID_FLAG (pCtTnlInfo) & RPTE_OUT_RESV_MSG_ID_PRESENT)
        == RPTE_OUT_RESV_MSG_ID_PRESENT)
    {
        RpteKey.u4_Key = RPTE_TNL_OUT_RESV_MSG_ID (pCtTnlInfo);
        RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                             RPTE_LCL_MSG_ID_DB_PREFIX, RpteMIHReleaseTrieInfo);
    }
    /* If the ResvTear mesg has arrived in the Ingress there is no other
     * node to forward the mesg hence it can be dropped after freeing the
     * resources */

    /* Deleting the Neighbour Entry created due to this Resv message */

    pIfEntry = RSB_OUT_IF_ENTRY (pRsb);
    if ((RPTE_IS_FRR_CAPABLE (pCtTnlInfo) != RPTE_TRUE) ||
        ((RPTE_FRR_NODE_STATE (pCtTnlInfo) == RPTE_FRR_NODE_STATE_HEADEND)))
    {
        /* If the timer is present stop the timer */
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &RPTE_TNL_RRO_TIMER (pCtTnlInfo));
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &RSB_TIMER (pRsb));

        if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
        {
            /* since the resvtear message has reached ingress the Tunnel
             * is deleted & the path tear is sent form the ingress */
            RptePhGeneratePathTear (pCtTnlInfo);
            RPTE_TNL_DEL_TNL_FLAG (pCtTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_RTH_PRCS,
                        "RESVTEAR: Mesg has reached Ingress, pathtear is sent..\n");
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        " RpteRthProcessResvTearMsg : EXIT \n");
            return;
        }
        pPsb = RSVPTE_TNL_PSB (pCtTnlInfo);
        /* Check whether Psb exists for the ResvTear mesg from which the PHop 
         * info is available for forwarding the mesg */
        if (pPsb == NULL)
        {
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_RTH_PRCS,
                        "RESVTEAR: Psb does not exist. Message cannot be "
                        "forwarded ..\n");
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        " RpteRthProcessResvTearMsg : INTMD-EXIT \n");
            return;
        }
        /* Forward the ResvTear mesg to the PHop and free the Label
         * to the LabelPool */
        if (RpteUtlPsbRoutesToOutIf (pPsb, pIfEntry, pNhop) == RPTE_YES)
        {
            RpteUtlCleanPktMap (pPktMap);
            RpteRthSendResvTear (RSVPTE_TNL_RSB (pCtTnlInfo));
            RpteUtlDeleteRsb (pRsb);
            RSVPTE_DBG (RSVPTE_RTH_PRCS,
                        "RESVTEAR: Label is freed and mesg is forwarded\n");
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        " RpteRthProcessResvTearMsg : EXIT \n");
            if ((pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                 MPLS_TE_FULL_REROUTE) ||
                (pCtTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                 MPLS_TE_DEDICATED_ONE2ONE))
            {
                RptePhGeneratePathTear (pCtTnlInfo);
                RPTE_TNL_DEL_TNL_FLAG (pCtTnlInfo) = RPTE_TRUE;
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
            }
            return;
        }
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthProcessResvTearMsg : EXIT \n");
        return;
    }
    else if ((RPTE_IS_FRR_CAPABLE (pCtTnlInfo) == RPTE_TRUE) &&
             (RPTE_FRR_NODE_STATE (pCtTnlInfo) == RPTE_FRR_NODE_STATE_UNKNOWN))
    {
        pCtInTnlInfo = pCtTnlInfo;
        RpteUtlGetTnlInfoAndGrpId (pPktMap, pCtTnlInfo, &u1DetourGrpId);
        do
        {
            if (RPTE_FRR_DETOUR_GRP_ID (pCtInTnlInfo) != u1DetourGrpId)
            {
                continue;
            }

            if (RSVPTE_TNL_RSB (pCtInTnlInfo) == NULL)
            {
                continue;
            }

            RpteRthSendResvTear (RSVPTE_TNL_RSB (pCtInTnlInfo));
            /* Mlib updation to be done                */
            if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
            {
                RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE, RSVPTE_ZERO,
                                          pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
            }
            else
            {
                RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                          pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
                RpteRhFreeLabel (RSB_IN_IF_ENTRY (RSVPTE_TNL_RSB (pCtTnlInfo)),
                                 &(pCtTnlInfo->DownstrInLbl));
                pCtTnlInfo->DownstrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
            }
            if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) != RPTE_EGRESS)
            {
                RpteDSRelTrafficControl (pCtTnlInfo);
                RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                            &u1ResvRefreshNeeded, &ErrorSpec);
            }
            RSVPTE_DBG (RSVPTE_FRR_DBG, "RESVTEAR: Resources are freed \n");
            /* If the timer is present stop the timer */
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &RPTE_TNL_RRO_TIMER (pCtTnlInfo));
            TmrStopTimer (RSVP_GBL_TIMER_LIST, &RSB_TIMER (pRsb));

            RpteUtlDeleteRsb (RSVPTE_TNL_RSB (pCtInTnlInfo));
        }
        while ((pCtInTnlInfo = RPTE_FRR_IN_TNL_INFO (pCtInTnlInfo)) != NULL);

        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_FRR_DBG,
                    "RESVTEAR: Label is freed and mesg is forwarded\n");
        RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthProcessResvTearMsg : EXIT \n");
        return;
    }
    else
    {
        RpteRthProcessResvTearForFrr (pPktMap, pCtTnlInfo, pNhop, pIfEntry,
                                      bIsBkpTnl);
    }
    RpteUtlCleanPktMap (pPktMap);
    RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthProcessResvTearMsg : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteRthSendResvTear                                    */
/* Description     : This procedure frames and sends resvtear message to    */
/*                   the Phop                                               */
/* Input (s)       : pRsb - Pointer to Rsb                                  */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteRthSendResvTear (tRsb * pRsb)
{
    UINT4               u4TmpAddr = RSVPTE_ZERO;
    tObjHdr            *pObjHdr = NULL;
    UINT1              *pPduPtr = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tIfEntry           *pIfEntry = NULL;
    tPktMap             PktMap;
    UINT2               u2Retval = RSVPTE_ZERO;
    UINT2               u2ResvTearSize = RSVPTE_ZERO;
    tRsvpTeTnlInfo     *pCtTnlInfo = NULL;
    tFlowSpec          *pFwdFlowSpec = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tTimerBlock        *pTimerBlock = NULL;
    tRpteKey            RpteKey;
    tuTrieInfo         *pTrieInfo = NULL;
    UINT4               u4ResvTearMsgId;
    UINT1              *pPdu = NULL;
    UINT4               u4Val;
    UINT2               u2FSpecObjLength = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthSendResvTear : ENTRY \n");
    if (pRsb == NULL)
    {
        RSVPTE_DBG (RSVPTE_RTH_PRCS,
                    "RESVTEAR : Reserve State Block is NULL \n");
        RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthSendResvTear : INTMD-EXIT \n");
        return;
    }

    if (RSVPTE_TNL_NODE_RELATION (pRsb->pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        RSVPTE_DBG (RSVPTE_RTH_PRCS,
                    "RESVTEAR: Ingress Node cannot generate RESV TEAR\n");
        return;
    }

    RpteUtlInitPktMap (&PktMap);

    pIfEntry = (tIfEntry *) RSB_IN_IF_ENTRY (pRsb);
    if ((pIfEntry == NULL) || (IF_ENTRY_STATUS (pIfEntry)) != ACTIVE)
    {
        RSVPTE_DBG (RSVPTE_RTH_PRCS,
                    "RESVTEAR : Outgoing Interface is not Active \n");
        RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthSendResvTear : INTMD-EXIT \n");
        return;
    }
    pCtTnlInfo = RSB_RPTE_TNL_INFO (pRsb);
    /* Calulate the ResvMesg size */

    u2ResvTearSize = sizeof (tRsvpHdr) + RPTE_IPV4_SSN_OBJ_LEN +
        sizeof (tStyleObj) + sizeof (tFlowSpecObj) +
        RPTE_IPV4_FLTR_SPEC_OBJ_LEN;

    u2Retval
        = RpteUtlCalculateRSVPHopSize (pCtTnlInfo->b1UpStrOob,
                                       pCtTnlInfo->u4UpStrDataTeLinkIfId,
                                       pCtTnlInfo->pPsb->pIncIfEntry->u4Addr);
    u2ResvTearSize = (UINT2) (u2ResvTearSize + u2Retval);

    pFwdFlowSpec = &RSB_FLOW_SPEC (pRsb);

    if (((tFlowSpec *) pFwdFlowSpec)->SrvHdr.u1SrvID == FLOWSPEC_CLS_HDR_NUMBER)
    {
        u2FSpecObjLength = sizeof (tFlowSpecObj) - sizeof (tGsRSpec);
        u2ResvTearSize = (UINT2) (u2ResvTearSize - sizeof (tGsRSpec));
    }
    else
    {
        u2FSpecObjLength = sizeof (tFlowSpecObj);
    }

    pNbrEntry = RPTE_TNL_USTR_NBR (pCtTnlInfo);
    RPTE_PKT_MAP_NBR_ENTRY (&PktMap) = pNbrEntry;

    if ((gu4RMDFlags & RPTE_RMD_RESV_TEAR_MSG) &&
        (NBR_ENTRY_RMD_CAPABLE (RPTE_PKT_MAP_NBR_ENTRY (&PktMap)) == RPTE_TRUE))
    {
        if (RPTE_TNL_RT_TIMER_BLOCK (pCtTnlInfo) == NULL)
        {
            /* Allocate memory if timer block is not present */
            pTimerBlock = (tTimerBlock *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TIMER_BLK_POOL_ID);
            if (pTimerBlock == NULL)
            {
                /* If the allocation of the Timer Block fails
                 * the message will be sent without Msg Id */
                RSVPTE_DBG (RSVPTE_RTH_MEM,
                            "Failed to allocate timer block. "
                            "Sending Resv Tear msg without msg id.\n");
                pTimerBlock = NULL;
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) = RPTE_ZERO;
            }
            else
            {
                RPTE_TNL_RT_TIMER_BLOCK (pCtTnlInfo) = pTimerBlock;
                /* Back Off Timer value is set to the initial value */
                TIMER_BLK_BACKOFF_INTERVAL (pTimerBlock) =
                    (IF_ENTRY_REFRESH_INTERVAL (RSB_IN_IF_ENTRY
                                                (pRsb)) /
                     RPTE_BACK_OFF_FACTOR) / DEFAULT_CONV_TO_SECONDS;

                /* Msg Id is generated for the outgoing path error
                 * message */
                RpteMIHGenerateMsgId (&u4ResvTearMsgId);

                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) = RPTE_NONZERO;

                /* Since RMD is enabled for Resv Tear, back off timer is
                 * started for the same. */
                TIMER_PARAM_RPTE_TNL ((&TIMER_BLK_TIMER_PARAM
                                       (pTimerBlock))) = pCtTnlInfo;
                TIMER_BLK_MSG_ID (pTimerBlock) = u4ResvTearMsgId;
                TIMER_PARAM_ID ((&TIMER_BLK_TIMER_PARAM (pTimerBlock))) =
                    RPTE_RESVTEAR;

                RSVP_TIMER_NAME ((&TIMER_BLK_APP_TIMER (pTimerBlock))) =
                    (FS_ULONG) & TIMER_BLK_TIMER_PARAM (pTimerBlock);

                pTrieInfo = (tuTrieInfo *)
                    RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_TRIE_INFO_POOL_ID);

                if (pTrieInfo == NULL)
                {
                    RSVPTE_DBG (RSVPTE_RTH_MEM,
                                "Failed to allocate Trie Info "
                                "Sending Resv Tear msg without msg id.\n");
                    RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) = RPTE_ZERO;
                    MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                        (UINT1 *) pTimerBlock);
                    pTimerBlock = NULL;
                    pTrieInfo = NULL;
                }
                else
                {
                    MEMSET (pTrieInfo, RSVPTE_ZERO, sizeof (tuTrieInfo));
                    RPTE_TRIE_INFO_MSG_TYPE (pTrieInfo) = RPTE_RESVTEAR;
                    RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = pTimerBlock;

                    RpteKey.u4_Key = u4ResvTearMsgId;

                    if (RpteTrieAddEntry (&RPTE_32_BIT_TRIE_ROOT_NODE,
                                          &RpteKey, pTrieInfo) != RPTE_SUCCESS)
                    {
                        RSVPTE_DBG (RSVPTE_RTH_PRCS,
                                    "PVM : Trie Addition Failed. "
                                    "- in Resv Tear\n");
                        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) =
                            RPTE_ZERO;
                        MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                            (UINT1 *) pTrieInfo);
                        RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = NULL;
                        MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                            (UINT1 *) pTimerBlock);
                        pTimerBlock = NULL;
                        pTrieInfo = NULL;
                    }
                }
            }
        }
        else
        {
            pTimerBlock = RPTE_TNL_RT_TIMER_BLOCK (pCtTnlInfo);

            RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) = RPTE_NONZERO;
            u4ResvTearMsgId = TIMER_BLK_MSG_ID (pTimerBlock);

            RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);
            if (RpteTrieLookupEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                     &pTrieInfo) != RPTE_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_RTH_PRCS,
                            "Look Up Failed - in Resv Tear\n"
                            "ATTENTION : TimerBlock Present, but msg id "
                            "not present in Trie\n");
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) = RPTE_ZERO;
                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
            }
        }

        if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) != RPTE_ZERO)
        {
            if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                                   (pTimerBlock),
                                   TIMER_BLK_BACKOFF_INTERVAL (pTimerBlock) *
                                   SYS_NUM_OF_TIME_UNITS_IN_A_SEC) !=
                TMR_SUCCESS)
            {
                RSVPTE_DBG (RSVPTE_RTH_PRCS,
                            "PVM : Start Timer failed. - in Resv Tear "
                            "Sending Resv Tear without msg id\n");
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) = RPTE_ZERO;
                MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                    (UINT1 *) pTimerBlock);
                RPTE_TRIE_INFO_TIMER_BLK (pTrieInfo) = NULL;
                MemReleaseMemBlock (RSVPTE_TRIE_INFO_POOL_ID,
                                    (UINT1 *) pTrieInfo);
                pTrieInfo = NULL;
                pTimerBlock = NULL;
            }
        }
    }
    else
    {
        /* In the case of RT Timer Block already present,
         * Stop the timer, Delete the Msg Id already present in
         * the Data base and continue. */
        pTimerBlock = RPTE_TNL_RT_TIMER_BLOCK (pCtTnlInfo);

        if (pTimerBlock != NULL)
        {
            TmrStopTimer (RSVP_GBL_TIMER_LIST, &TIMER_BLK_APP_TIMER
                          (pTimerBlock));
            RpteKey.u4_Key = TIMER_BLK_MSG_ID (pTimerBlock);

            RpteTrieDeleteEntry (&RPTE_32_BIT_TRIE_ROOT_NODE, &RpteKey,
                                 RPTE_LCL_MSG_ID_DB_PREFIX,
                                 RpteMIHReleaseTrieInfo);

            MemReleaseMemBlock (RSVPTE_TIMER_BLK_POOL_ID,
                                (UINT1 *) pTimerBlock);
            pTimerBlock = NULL;
            pTrieInfo = NULL;
        }
        RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) = RPTE_ZERO;
    }

    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) == RPTE_ZERO)
    {
        RPTE_TNL_RT_TIMER_BLOCK (pCtTnlInfo) = NULL;
    }
    else
    {
        RPTE_PKT_MAP_MSG_ID_EPOCH (&PktMap) = gu4Epoch;
        RPTE_PKT_MAP_MSG_ID_FLAGS (&PktMap) = RPTE_MSG_ID_ACK_DESIRED;
        RPTE_PKT_MAP_MSG_ID (&PktMap) = u4ResvTearMsgId;
        u2ResvTearSize = (UINT2) (u2ResvTearSize + RPTE_MSG_ID_OBJ_LEN);
    }

    if (pNbrEntry != NULL)
    {
        if (NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) == RPTE_TRUE)
        {
            RpteRRSRefreshThreshold (pNbrEntry, RPTE_ZERO);
        }
    }

    /* Allocate memory for the RSVP packet */
    PKT_MAP_RSVP_PKT (&PktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);
    if (PKT_MAP_RSVP_PKT (&PktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_RH_PRCS,
                    "RESVTEAR : Mem Alloc failed in RpteRthSendResvTear..\n");
        RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthSendResvTear : INTMD-EXIT \n");
        return;
    }
    MEMSET (PKT_MAP_RSVP_PKT (&PktMap), RSVPTE_ZERO, u2ResvTearSize);
    PKT_MAP_RSVP_PKT_ALLOC (&PktMap) = RPTE_PKTMAP_RSVP_ALLOC;
    /* Copy Source address i.e this node's address */
    if (IF_ENTRY_ADDR (pIfEntry) != RPTE_ZERO)
    {
        PKT_MAP_SRC_ADDR (&PktMap) = IF_ENTRY_ADDR (pIfEntry);
    }
    else
    {
        CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                            u4TmpAddr);
        u4TmpAddr = OSIX_NTOHL (u4TmpAddr);

        PKT_MAP_SRC_ADDR (&PktMap) = u4TmpAddr;
    }
    /* Copy Destination address i.e Prev node's address */
    PKT_MAP_DST_ADDR (&PktMap) = RSVP_HOP_ADDR (&RSB_FWD_RSVP_HOP (pRsb));
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (&PktMap);
    MEMSET (pRsvpHdr, RSVPTE_ZERO, sizeof (tRsvpHdr));

    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = RESVTEAR_MSG;
    RSVP_HDR_SEND_TTL (pRsvpHdr) = IF_ENTRY_TTL (pIfEntry);
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (u2ResvTearSize);
    pObjHdr =
        (tObjHdr *) (VOID *) (PKT_MAP_RSVP_PKT (&PktMap) + sizeof (tRsvpHdr));
    /* Msg Id Obj update, if present */
    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (&PktMap) != RPTE_ZERO)
    {
        /* Filling the Header */
        OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_MSG_ID_OBJ_LEN);
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_MESSAGE_ID_CLASS_NUM_TYPE);

        pPdu = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));

        /* Filling the Epoch and Flags fields */
        u4Val = (RPTE_PKT_MAP_MSG_ID_EPOCH (&PktMap) |
                 (UINT4) (RPTE_PKT_MAP_MSG_ID_FLAGS (&PktMap) <<
                          RPTE_3_BYTE_SHIFT));
        u4Val = OSIX_HTONL (u4Val);
        MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4Val, RPTE_FLAG_EPOCH_LEN);

        pPdu += RPTE_FLAG_EPOCH_LEN;

        /* Filling the Message Identifier field */
        u4Val = OSIX_HTONL (RPTE_PKT_MAP_MSG_ID (&PktMap));
        MEMCPY ((UINT1 *) pPdu, (UINT1 *) &u4Val, RPTE_MSG_ID_LEN);
        pPdu += RPTE_MSG_ID_LEN;

        pObjHdr = NEXT_OBJ (pObjHdr);
    }
    /* copy RsvpTeSessionObj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_SSN_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_RPTE_SSN_CLASS_NUM_TYPE);
    pPduPtr = (UINT1 *) ((UINT1 *) (pObjHdr) + sizeof (tObjHdr));
    u4TmpAddr = RSVPTE_TNL_EGRESS (pCtTnlInfo);
    MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPduPtr += RSVPTE_IPV4ADR_LEN;

    RPTE_PUT_2_BYTES (pPduPtr, RSVPTE_ZERO);    /* reserved field */

    RPTE_PUT_2_BYTES (pPduPtr,
                      (UINT2) OSIX_HTONS (RSVPTE_TNL_TNLINDX (pCtTnlInfo)));
    CONVERT_TO_INTEGER ((RSVPTE_TNL_EXTN_TNLID (pCtTnlInfo)), u4TmpAddr);

    MEMCPY ((UINT1 *) pPduPtr, (UINT1 *) &u4TmpAddr, RSVPTE_IPV4ADR_LEN);

    pPduPtr += RSVPTE_IPV4ADR_LEN;

    pObjHdr = (tObjHdr *) (VOID *) pPduPtr;

    /* copy RsvpHopObj */
    RptePvmFillRsvpHopObj (pCtTnlInfo, &pObjHdr, RESVTEAR_MSG);
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* Fill  Style Obj  */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tStyleObj));
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS (IPV4_STYLE_CLASS_NUM_TYPE);
    STYLE_OBJ ((tStyleObj *) pObjHdr) = RSB_STYLE (pRsb);
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* Copy FlowSpec */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (u2FSpecObjLength);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
        OSIX_HTONS (IPV4_FLOW_SPEC_CLASS_NUM_TYPE);
    FLOW_SPEC_OBJ ((tFlowSpecObj *) (VOID *) pObjHdr) = *pFwdFlowSpec;
    pObjHdr = NEXT_OBJ (pObjHdr);

    /* copy RsvpTeFilterSpecObj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (RPTE_IPV4_FLTR_SPEC_OBJ_LEN);
    OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) = OSIX_HTONS
        (IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE);
    RPTE_FILTER_SPEC_OBJ ((tRsvpTeFilterSpecObj *) pObjHdr) =
        pCtTnlInfo->pRsb->RsvpTeFilterSpec;
    pObjHdr = NEXT_OBJ (pObjHdr);

    PKT_MAP_BUILD_PKT (&PktMap) = RPTE_YES;
    PKT_MAP_OUT_IF_ENTRY (&PktMap) = RSB_IN_IF_ENTRY (pRsb);
    PKT_MAP_TX_TTL (&PktMap) = IF_ENTRY_TTL ((tIfEntry *) RSB_IN_IF_ENTRY
                                             (pRsb));
    RSVP_HDR_SEND_TTL (pRsvpHdr) = IF_ENTRY_TTL ((tIfEntry *)
                                                 RSB_IN_IF_ENTRY (pRsb));

    pNbrEntry = RPTE_TNL_USTR_NBR (pCtTnlInfo);
    if (pNbrEntry != NULL)
    {
        if (NBR_ENTRY_SREFRESH_STARTED (pNbrEntry) == RPTE_TRUE)
        {
            RpteRRSRefreshThreshold (pNbrEntry, RPTE_ZERO);
        }
    }

    RptePbSendMsg (&PktMap);
    RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthSendResvTear : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteRthProcessResvTearForFrr
 * Description     : This function processes the Resv Tear message for FRR.
 * Input (s)       : *pPktMap - Pointer to Packet Map
 *                 : *pRsvpTeTnlInfo - Pointer to ResvpTeTnlInfo
 *                    pNhop - Next Hop Info.
 *                    pIfEntry - Incoming RSVP interface
 *                   bIsBkpTnl - Is Bkp Tunnel flag
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteRthProcessResvTearForFrr (tPktMap * pPktMap, tRsvpTeTnlInfo * pCtTnlInfo,
                              tRsvpHop * pNhop, tIfEntry * pIfEntry,
                              BOOL1 bIsBkpTnl)
{
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    tRsvpTeTnlInfo     *pTmpCtTnlInfo = NULL;
    tPsb               *pPsb = NULL;
    tErrorSpec          ErrorSpec;
    BOOL1               bFlag = RPTE_FALSE;
    UINT1               u1ResvRsrc = RPTE_FALSE;
    UINT1               u1DetourGrpId = RPTE_ZERO;
    UINT1               u1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;

    pIfEntry = PKT_MAP_IF_ENTRY (pPktMap);

    pRsvpTeFastReroute =
        &PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pCtTnlInfo));

    /* FRR CAPABLE HANDLE */
    if (RPTE_IS_PLR_OR_PLRAWAIT (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
    {
        if (((bIsBkpTnl == RPTE_NO)
             && (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) == NULL)) ||
            (RPTE_IS_PLRAWAIT (RPTE_FRR_NODE_STATE (pCtTnlInfo))))
        {
            if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
            {
                /* since the resvtear message has reached ingress the Tunnel
                 * is deleted & the path tear is sent form the ingress */

                RptePhGeneratePathTear (pCtTnlInfo);
                RPTE_TNL_DEL_TNL_FLAG (pCtTnlInfo) = RPTE_TRUE;
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);

                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "RESVTEAR: Mesg has reached Ingress, pathtear is sent..\n");
                RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                            " RpteRthProcessResvTearForFrr : EXIT \n");
                return;
            }

            pTmpCtTnlInfo = RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo);
            if (pTmpCtTnlInfo != NULL)
            {
                RptePhGeneratePathTear (pTmpCtTnlInfo);
                RPTE_TNL_DEL_TNL_FLAG (pTmpCtTnlInfo) = RPTE_TRUE;
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                     pTmpCtTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_FALSE);
            }

            pPsb = RSVPTE_TNL_PSB (pCtTnlInfo);
            /* Check whether Psb exists for the ResvTear mesg from which the PHop 
             * info is available for forwarding the mesg */
            if (pPsb == NULL)
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "RESVTEAR: Psb does not exist. Message cannot be "
                            "forwarded ..\n");
                RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                            " RpteRthProcessResvTearForFrr : INTMD-EXIT \n");
                return;
            }
            /* Forward the ResvTear mesg to the PHop and free the Label
             * to the LabelPool */
            if (RpteUtlPsbRoutesToOutIf (pPsb, pIfEntry, pNhop) == RPTE_YES)
            {
                RpteRthSendResvTear (RSVPTE_TNL_RSB (pCtTnlInfo));
                /* If the timer is present stop the timer */
                TmrStopTimer (RSVP_GBL_TIMER_LIST,
                              &RPTE_TNL_RRO_TIMER (pCtTnlInfo));
                TmrStopTimer (RSVP_GBL_TIMER_LIST,
                              &RSB_TIMER (RSVPTE_TNL_RSB (pCtTnlInfo)));

                RpteUtlDeleteRsb (RSVPTE_TNL_RSB (pCtTnlInfo));
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "RESVTEAR: Label is freed and mesg is forwarded\n");
                RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                            " RpteRthProcessResvTearForFrr : EXIT \n");
                return;
            }

        }
        /* RESV Tear message is received on primary path */
        if ((bIsBkpTnl == RPTE_NO)
            && (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) != NULL))
        {
            /* RESV Tear is received on primary path and Out Tunnel exists. 
             * So, We can refresh its timer block */
            /* PSB Time To Die updation */
            pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pCtTnlInfo));
            PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pCtTnlInfo)) =
                RpteUtlComputeLifeTime (pIfEntry,
                                        IF_ENTRY_REFRESH_INTERVAL (pIfEntry));
            PSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_PSB (pCtTnlInfo)) =
                (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                 DEFAULT_CONV_TO_SECONDS);

            /* RSB Time To Die updation */
            pIfEntry = RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pCtTnlInfo));
            RSB_TIME_TO_DIE (RSVPTE_TNL_RSB (pCtTnlInfo)) =
                RpteUtlComputeLifeTime (pIfEntry,
                                        IF_ENTRY_REFRESH_INTERVAL (pIfEntry));
            RSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_RSB (pCtTnlInfo)) =
                (IF_ENTRY_REFRESH_INTERVAL (pIfEntry) /
                 DEFAULT_CONV_TO_SECONDS);

            /* Setting node state to DNLOST */
            RPTE_FRR_NODE_STATE (pCtTnlInfo) |= RPTE_FRR_NODE_STATE_DNLOST;

            if (pRsvpTeFastReroute->u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD)
            {
                pCtTnlInfo->u1SendToByPassTnl = RPTE_TRUE;
            }
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR: Time to Die for the Protected Tunnel"
                        " Updated\n");
            RSVPTE_DBG (RSVPTE_PE_ETEXT,
                        "RpteRthProcessResvTearForFrr : EXIT \n");
            return;
        }

        if ((RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) != NULL) &&
            (pRsvpTeFastReroute->u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD))
        {
            RpteRthSendResvTear (RSVPTE_TNL_RSB (pCtTnlInfo));
            /* If the timer is present stop the timer */
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &RPTE_TNL_RRO_TIMER (pCtTnlInfo));
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &RSB_TIMER (RSVPTE_TNL_RSB (pCtTnlInfo)));

            RpteUtlDeleteRsb (RSVPTE_TNL_RSB (pCtTnlInfo));
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR : RESV Tear Message Forwarded.  \n");
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        " RpteRthProcessResvTearForFrr : EXIT \n");
            return;
        }

        /* RESV Tear Message is received on backup path */
        if (!(RPTE_IS_DNLOST (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
            && (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) != NULL))
        {
            RSVPTE_TNL_RRO_FLAGS (pCtTnlInfo) &= (UINT1) (~(LOCAL_PROT_AVAIL));

            RptePhGeneratePathTear (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo));
            RPTE_TNL_DEL_TNL_FLAG (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo))
                = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                 RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo),
                                 TE_TNL_CALL_FROM_SIG, RPTE_FALSE);

            RpteResetPlrStatus (pCtTnlInfo);

            RpteUpdateDetourStats (RPTE_TWO);

            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR: PATH Tear Forwarded for backup tunnel, "
                        "Backup Tunnel Pointer removed from protected tunnel, "
                        "Protected Tunnel State Modified  \n");
            RSVPTE_DBG (RSVPTE_PE_ETEXT,
                        "RpteRthProcessResvTearForFrr : EXIT \n");
            return;
        }
        else if ((RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) != NULL) &&
                 (RPTE_IS_HEADEND (RPTE_FRR_NODE_STATE (pCtTnlInfo))))
        {
            RptePhGeneratePathTear (pCtTnlInfo);

            RPTE_TNL_DEL_TNL_FLAG (pCtTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pCtTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);

            RpteUpdateDetourStats (RPTE_TWO);

            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR: PATH Tear Forwarded for backup tunnel, "
                        "Tunnel Entry for protected and backup tunnel, "
                        "removed.  \n");
            RSVPTE_DBG (RSVPTE_PE_ETEXT,
                        "RpteRthProcessResvTearForFrr : EXIT \n");
            return;
        }

        if (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo) != NULL)
        {
            RptePhGeneratePathTear (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo));
            RPTE_TNL_DEL_TNL_FLAG (RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo))
                = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo,
                                 RPTE_FRR_OUT_TNL_INFO (pCtTnlInfo),
                                 TE_TNL_CALL_FROM_SIG, RPTE_FALSE);

            RSVPTE_DBG4 (RSVPTE_FRR_DBG,
                         "PLR deleted for %d %d %x %x\n",
                         RSVPTE_TNL_TNLINDX (pCtTnlInfo),
                         RSVPTE_TNL_TNLINST (pCtTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pCtTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pCtTnlInfo)));

            RpteResetPlrStatus (pCtTnlInfo);

            RpteUpdateDetourStats (RPTE_TWO);

            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR: PATH Tear Forwarded for backup tunnel, "
                        "Backup Tunnel Pointer removed from protected tunnel, "
                        "Protected Tunnel State Modified  \n");
            RSVPTE_DBG (RSVPTE_PE_ETEXT,
                        "RpteRthProcessResvTearForFrr : EXIT \n");
        }

        if (!(RPTE_IS_MP (RPTE_FRR_NODE_STATE (pCtTnlInfo)) ||
              RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pCtTnlInfo))))
        {
            RpteRthSendResvTear (RSVPTE_TNL_RSB (pCtTnlInfo));
            /* If the timer is present stop the timer */
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &RPTE_TNL_RRO_TIMER (pCtTnlInfo));
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &RSB_TIMER (RSVPTE_TNL_RSB (pCtTnlInfo)));

            RpteUtlDeleteRsb (RSVPTE_TNL_RSB (pCtTnlInfo));
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "RESVTEAR : RESV Tear Message Forwarded.  \n");
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        " RpteRthProcessResvTearForFrr : EXIT \n");
            return;
        }
    }
    if (RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
    {
        /* Get the Group ID of the Message, so that RESV Tear can be 
         * duplicated to the Group having same ID. */
        if (RpteUtlGetGrpIdForDMP (pCtTnlInfo, PKT_MAP_IF_ENTRY (pPktMap),
                                   &u1DetourGrpId) == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_RTH_ETEXT,
                        " RpteRthProcessResvTearForFrr : INTMD EXIT \n");
            return;
        }
    }
    if (RPTE_IS_MP (RPTE_FRR_NODE_STATE (pCtTnlInfo)) ||
        RPTE_IS_DMP (RPTE_FRR_NODE_STATE (pCtTnlInfo)))
    {
        do
        {
            if (u1DetourGrpId != RPTE_FRR_DETOUR_GRP_ID (pCtTnlInfo))
            {
                continue;
            }

            if (RSVPTE_TNL_RSB (pCtTnlInfo) == NULL)
            {
                continue;
            }

            RpteRthSendResvTear (RSVPTE_TNL_RSB (pCtTnlInfo));
            /* Mlib updation to be done                */
            if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) == RPTE_INGRESS)
            {
                RpteRhSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE, RSVPTE_ZERO,
                                          pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
            }
            else
            {
                RpteRhSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, RSVPTE_ZERO,
                                          pCtTnlInfo, GMPLS_SEGMENT_FORWARD);
                RpteRhFreeLabel (RSB_IN_IF_ENTRY (RSVPTE_TNL_RSB (pCtTnlInfo)),
                                 &(pCtTnlInfo->DownstrInLbl));
                pCtTnlInfo->DownstrInLbl.u4GenLbl = MPLS_INVALID_LABEL;
            }
            if (RSVPTE_TNL_NODE_RELATION (pCtTnlInfo) != RPTE_EGRESS)
            {
                RpteDSRelTrafficControl (pCtTnlInfo);
                RpteRhUpdateTrafficControl (pCtTnlInfo, u1ResvRsrc,
                                            &u1ResvRefreshNeeded, &ErrorSpec);
            }
            RSVPTE_DBG (RSVPTE_FRR_DBG, "RESVTEAR: Resources are freed \n");

            /* If the timer is present stop the timer */
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &RPTE_TNL_RRO_TIMER (pCtTnlInfo));
            TmrStopTimer (RSVP_GBL_TIMER_LIST,
                          &RSB_TIMER (RSVPTE_TNL_RSB (pCtTnlInfo)));
            RpteUtlDeleteRsb (RSVPTE_TNL_RSB (pCtTnlInfo));
            /* Updating Statistical Values. */
            if ((bFlag == RPTE_TRUE) && (RSVPTE_FRR_DETOUR_INCOMING_NUM
                                         (gpRsvpTeGblInfo) != RPTE_ZERO))
            {
                RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
            }
            bFlag = RPTE_TRUE;
        }
        while ((pCtTnlInfo = RPTE_FRR_IN_TNL (pCtTnlInfo)) != NULL);
    }
    RSVPTE_DBG (RSVPTE_RTH_ETEXT, " RpteRthProcessResvTearForFrr : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rpterter.c                             */
/*---------------------------------------------------------------------------*/
