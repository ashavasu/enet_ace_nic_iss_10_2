
/* $Id: rpteget1.c,v 1.19 2018/01/03 11:31:23 siva Exp $*/
/********************************************************************
 *
 * $RCSfile: rpteget1.c,v $
 *
 * $Date: 2018/01/03 11:31:23 $
 *
 * $Revision: 1.19 $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rpteget1.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVPTE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  : 
 *    DESCRIPTION            : This file contains the low level SET routines
 *                             for the MIB objects present in the rsvpte.mib
 *                             and Get Routines for Scalar Objects in 
 *                             fsmpfrr.mib.
 *---------------------------------------------------------------------------*/

# include  "rpteincs.h"
# include  "fsmpfrlw.h"

/* LOW LEVEL Routines for Table : FsMplsRsvpTeIfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsRsvpTeIfTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsRsvpTeIfTable (INT4 i4FsMplsRsvpTeIfIndex)
{
/* Curently all the Tunnel index value is checked against a range. */
    if (i4FsMplsRsvpTeIfIndex <= RSVPTE_ZERO)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsRsvpTeIfTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsRsvpTeIfTable (INT4 *pi4FsMplsRsvpTeIfIndex)
{
    return (nmhGetFirstIndexRsvpIfTable (pi4FsMplsRsvpTeIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsRsvpTeIfTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                nextFsMplsRsvpTeIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsRsvpTeIfTable (INT4 i4FsMplsRsvpTeIfIndex,
                                    INT4 *pi4NextFsMplsRsvpTeIfIndex)
{
    return
        (nmhGetNextIndexRsvpIfTable (i4FsMplsRsvpTeIfIndex,
                                     pi4NextFsMplsRsvpTeIfIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfLblSpace
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfLblSpace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfLblSpace (INT4 i4FsMplsRsvpTeIfIndex,
                              INT4 *pi4RetValFsMplsRsvpTeIfLblSpace)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeIfLblSpace = RPTE_IF_LBL_SPACE (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfLblType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfLblType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfLblType (INT4 i4FsMplsRsvpTeIfIndex,
                             INT4 *pi4RetValFsMplsRsvpTeIfLblType)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeIfLblType = RPTE_IF_LBL_TYPE (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeAtmMinVpi
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeAtmMinVpi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeAtmMinVpi (INT4 i4FsMplsRsvpTeIfIndex,
                             INT4 *pi4RetValFsMplsRsvpTeAtmMinVpi)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeAtmMinVpi = RPTE_IF_MINVPI (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeAtmMinVci
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeAtmMinVci
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeAtmMinVci (INT4 i4FsMplsRsvpTeIfIndex,
                             INT4 *pi4RetValFsMplsRsvpTeAtmMinVci)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeAtmMinVci = RPTE_IF_MINVCI (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeAtmMaxVpi
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeAtmMaxVpi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeAtmMaxVpi (INT4 i4FsMplsRsvpTeIfIndex,
                             INT4 *pi4RetValFsMplsRsvpTeAtmMaxVpi)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeAtmMaxVpi = RPTE_IF_MAXVPI (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeAtmMaxVci
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeAtmMaxVci
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeAtmMaxVci (INT4 i4FsMplsRsvpTeIfIndex,
                             INT4 *pi4RetValFsMplsRsvpTeAtmMaxVci)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeAtmMaxVci = RPTE_IF_MAXVCI (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfMtu
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfMtu (INT4 i4FsMplsRsvpTeIfIndex,
                         INT4 *pi4RetValFsMplsRsvpTeIfMtu)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeIfMtu = (INT4) RPTE_IF_MAX_MTU (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfUdpNbrs
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfUdpNbrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfUdpNbrs (INT4 i4FsMplsRsvpTeIfIndex,
                             UINT4 *pu4RetValFsMplsRsvpTeIfUdpNbrs)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfUdpNbrs = IF_ENTRY_UDP_NBRS (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfIpNbrs
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfIpNbrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfIpNbrs (INT4 i4FsMplsRsvpTeIfIndex,
                            UINT4 *pu4RetValFsMplsRsvpTeIfIpNbrs)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfIpNbrs = IF_ENTRY_IP_NBRS (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNbrs
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNbrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNbrs (INT4 i4FsMplsRsvpTeIfIndex,
                          UINT4 *pu4RetValFsMplsRsvpTeIfNbrs)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNbrs = IF_ENTRY_NBRS (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfRefreshMultiple
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfRefreshMultiple
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfRefreshMultiple (INT4 i4FsMplsRsvpTeIfIndex,
                                     INT4
                                     *pi4RetValFsMplsRsvpTeIfRefreshMultiple)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeIfRefreshMultiple =
            (INT4) IF_ENTRY_REFRESH_MULTIPLE (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfTTL
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfTTL
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfTTL (INT4 i4FsMplsRsvpTeIfIndex,
                         INT4 *pi4RetValFsMplsRsvpTeIfTTL)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeIfTTL = (INT4) IF_ENTRY_TTL (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfRefreshInterval
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfRefreshInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfRefreshInterval (INT4 i4FsMplsRsvpTeIfIndex,
                                     INT4
                                     *pi4RetValFsMplsRsvpTeIfRefreshInterval)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeIfRefreshInterval =
            (INT4) IF_ENTRY_REFRESH_INTERVAL (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfRouteDelay
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfRouteDelay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfRouteDelay (INT4 i4FsMplsRsvpTeIfIndex,
                                INT4 *pi4RetValFsMplsRsvpTeIfRouteDelay)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeIfRouteDelay =
            (INT4) IF_ENTRY_ROUTE_DELAY (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfUdpRequired
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfUdpRequired
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfUdpRequired (INT4 i4FsMplsRsvpTeIfIndex,
                                 INT4 *pi4RetValFsMplsRsvpTeIfUdpRequired)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeIfUdpRequired =
            (INT4) IF_ENTRY_UDP_REQUIRED (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfHelloSupported
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfHelloSupported
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfHelloSupported (INT4 i4FsMplsRsvpTeIfIndex,
                                    INT4 *pi4RetValFsMplsRsvpTeIfHelloSupported)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeIfHelloSupported =
            (INT4) IF_ENTRY_HELLO_SPRTD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfLinkAttr
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfLinkAttr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfLinkAttr (INT4 i4FsMplsRsvpTeIfIndex,
                              UINT4 *pu4RetValFsMplsRsvpTeIfLinkAttr)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfLinkAttr = IF_ENTRY_LINK_ATTR (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfStatus
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfStatus (INT4 i4FsMplsRsvpTeIfIndex,
                            INT4 *pi4RetValFsMplsRsvpTeIfStatus)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeIfStatus = (INT4) IF_ENTRY_STATUS (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 * Function    :  nmhGetFsMplsRsvpTeIfPlrId
 * Input       :  The Indices
 *                FsMplsRsvpTeIfIndex
 *                
 *                The Object 
 *                retValFsMplsRsvpTeIfPlrId
 * Output      :  The Get Low Lev Routine Take the Indices &
 *                store the Value requested in the Return val.
 *                Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfPlrId (INT4 i4FsMplsRsvpTeIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsMplsRsvpTeIfPlrId)
{
    UINT1               u1RetVal = SNMP_FAILURE;
    tIfEntry           *pIfEntry = NULL;

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        return RPTE_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);

    if (u1RetVal == RPTE_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_INTEGER_TO_OCTETSTRING (IF_ENTRY_PLR_ID (pIfEntry),
                                 pRetValFsMplsRsvpTeIfPlrId);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfAvoidNodeId
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfAvoidNodeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfAvoidNodeId (INT4 i4FsMplsRsvpTeIfIndex,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pRetValFsMplsRsvpTeIfAvoidNodeId)
{
    UINT1               u1RetVal = SNMP_FAILURE;
    tIfEntry           *pIfEntry = NULL;

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        return RPTE_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);

    if (u1RetVal == RPTE_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_INTEGER_TO_OCTETSTRING (IF_ENTRY_AVOID_NODE_ID (pIfEntry),
                                 pRetValFsMplsRsvpTeIfAvoidNodeId);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfStorageType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfStorageType (INT4 i4FsMplsRsvpTeIfIndex,
                                 INT4 *pi4RetValFsMplsRsvpTeIfStorageType)
{
    UNUSED_PARAM (i4FsMplsRsvpTeIfIndex);
    UNUSED_PARAM (pi4RetValFsMplsRsvpTeIfStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeLsrID
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeLsrID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeLsrID (tSNMP_OCTET_STRING_TYPE * pRetValFsMplsRsvpTeLsrID)
{
    CPY_TO_SNMP (pRetValFsMplsRsvpTeLsrID, RSVPTE_LSR_ID (gpRsvpTeGblInfo),
                 RSVPTE_IPV4ADR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeMaxTnls
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeMaxTnls
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeMaxTnls (INT4 *pi4RetValFsMplsRsvpTeMaxTnls)
{
    *pi4RetValFsMplsRsvpTeMaxTnls = RSVPTE_MAX_RSVPTETNL (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeMaxErhopsPerTnl
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeMaxErhopsPerTnl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeMaxErhopsPerTnl (INT4 *pi4RetValFsMplsRsvpTeMaxErhopsPerTnl)
{
    *pi4RetValFsMplsRsvpTeMaxErhopsPerTnl =
        RSVPTE_MAX_ERPERTNL (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeMaxActRoutePerTnl
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeMaxActRoutePerTnl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeMaxActRoutePerTnl (INT4
                                     *pi4RetValFsMplsRsvpTeMaxActRoutePerTnl)
{
    *pi4RetValFsMplsRsvpTeMaxActRoutePerTnl =
        RSVPTE_MAX_ARPERTNL (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeMaxIfaces
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeMaxIfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeMaxIfaces (INT4 *pi4RetValFsMplsRsvpTeMaxIfaces)
{
    *pi4RetValFsMplsRsvpTeMaxIfaces = RSVPTE_MAX_IFACES (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeMaxNbrs
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeMaxNbrs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeMaxNbrs (INT4 *pi4RetValFsMplsRsvpTeMaxNbrs)
{
    *pi4RetValFsMplsRsvpTeMaxNbrs = RSVPTE_MAX_NEIGHBOURS (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeAtmMergeCap
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeAtmMergeCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeAtmMergeCap (INT4 i4FsMplsRsvpTeIfIndex,
                               INT4 *pi4RetValFsMplsRsvpTeAtmMergeCap)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeAtmMergeCap = RPTE_IF_ATM_MERGE_CAP (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeAtmVcDirection
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeAtmVcDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeAtmVcDirection (INT4 i4FsMplsRsvpTeIfIndex,
                                  INT4 *pi4RetValFsMplsRsvpTeAtmVcDirection)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pi4RetValFsMplsRsvpTeAtmVcDirection = RPTE_IF_ATM_VC_DIR (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeHelloSupprtd
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeHelloSupprtd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeHelloSupprtd (INT4 *pi4RetValFsMplsRsvpTeHelloSupprtd)
{
    if (RSVPTE_HELLO_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE)
    {
        *pi4RetValFsMplsRsvpTeHelloSupprtd = RPTE_SNMP_TRUE;
    }
    else if (RSVPTE_HELLO_SPRTD (gpRsvpTeGblInfo) == RPTE_FALSE)
    {
        *pi4RetValFsMplsRsvpTeHelloSupprtd = RPTE_SNMP_FALSE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeSockSupprtd
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeSockSupprtd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeSockSupprtd (INT4 *pi4RetValFsMplsRsvpTeSockSupprtd)
{
    if (RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE)
    {
        *pi4RetValFsMplsRsvpTeSockSupprtd = RPTE_SNMP_TRUE;
    }
    else if (RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) == RPTE_FALSE)
    {
        *pi4RetValFsMplsRsvpTeSockSupprtd = RPTE_SNMP_FALSE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGenLblSpaceMinLbl
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGenLblSpaceMinLbl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGenLblSpaceMinLbl (INT4
                                     *pi4RetValFsMplsRsvpTeGenLblSpaceMinLbl)
{
    *pi4RetValFsMplsRsvpTeGenLblSpaceMinLbl =
        (INT4) RSVPTE_GENLBL_MINLBL (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGenLblSpaceMaxLbl
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGenLblSpaceMaxLbl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGenLblSpaceMaxLbl (INT4
                                     *pi4RetValFsMplsRsvpTeGenLblSpaceMaxLbl)
{
    *pi4RetValFsMplsRsvpTeGenLblSpaceMaxLbl =
        (INT4) RSVPTE_GENLBL_MAXLBL (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGenDebugFlag
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGenDebugFlag
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
*************************************************************************/
INT1
nmhGetFsMplsRsvpTeGenDebugFlag (UINT4 *pu4RetValFsMplsRsvpTeGenDebugFlag)
{
    *pu4RetValFsMplsRsvpTeGenDebugFlag = RSVPTE_DBG_FLAG;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGenPduDumpLevel
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGenPduDumpLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGenPduDumpLevel (INT4 *pi4RetValFsMplsRsvpTeGenPduDumpLevel)
{
    *pi4RetValFsMplsRsvpTeGenPduDumpLevel =
        (INT4) RSVPTE_DUMP_LVL (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGenPduDumpMsgType
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGenPduDumpMsgType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGenPduDumpMsgType (INT4
                                     *pi4RetValFsMplsRsvpTeGenPduDumpMsgType)
{
    *pi4RetValFsMplsRsvpTeGenPduDumpMsgType =
        (INT4) RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGenPduDumpDirection
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGenPduDumpDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGenPduDumpDirection (INT4
                                       *pi4RetValFsMplsRsvpTeGenPduDumpDirection)
{
    *pi4RetValFsMplsRsvpTeGenPduDumpDirection =
        (INT4) gpRsvpTeGblInfo->u4RsvpTeDumpDirn;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeOperStatus
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeOperStatus (INT4 *pi4RetValFsMplsRsvpTeOperStatus)
{
    *pi4RetValFsMplsRsvpTeOperStatus = RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeOverRideOption
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeOverRideOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeOverRideOption (INT4 *pi4RetValFsMplsRsvpTeOverRideOption)
{
    *pi4RetValFsMplsRsvpTeOverRideOption = (INT4) RSVPTE_DS_OVER_RIDE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceFsMplsRsvpTeNbrTable (INT4 i4FsMplsRsvpTeIfIndex,
                                              UINT4 u4FsMplsRsvpTeNbrIfAddr)
{
    if (i4FsMplsRsvpTeIfIndex <= RSVPTE_ZERO)
    {
        return SNMP_FAILURE;
    }

    if ((u4FsMplsRsvpTeNbrIfAddr == RPTE_MIN_UINT4_VALUE) ||
        (u4FsMplsRsvpTeNbrIfAddr == RPTE_MAX_UINT4_VALUE) ||
        ((u4FsMplsRsvpTeNbrIfAddr >= RPTE_MIN_MCAST_ADDR_VALUE) &&
         (u4FsMplsRsvpTeNbrIfAddr <= RPTE_MAX_MCAST_ADDR_VALUE)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsRsvpTeNbrTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsMplsRsvpTeNbrTable (INT4 *pi4FsMplsRsvpTeIfIndex,
                                      UINT4 *pu4FsMplsRsvpTeNbrIfAddr)
{
    INT4                i4RsvpTeIfIndex = 0;
    INT4                i4RsvpTeNextIfIndex = 0;
    UINT1               u1RetVal;
    INT1                i1RetVal;
    tTMO_SLL           *pNbrList = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tIfEntry           *pIfEntry = NULL;

    i1RetVal = nmhGetFirstIndexFsMplsRsvpTeIfTable (&i4RsvpTeIfIndex);

    if (i1RetVal == SNMP_SUCCESS)
    {
        u1RetVal = RpteGetIfEntry (i4RsvpTeIfIndex, &pIfEntry);
        if (u1RetVal == RPTE_SUCCESS)
        {
            pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));
            pNbrEntry = (tNbrEntry *) TMO_SLL_First (pNbrList);

            if (pNbrEntry != NULL)
            {
                *pu4FsMplsRsvpTeNbrIfAddr = NBR_ENTRY_ADDR (pNbrEntry);
                *pi4FsMplsRsvpTeIfIndex = i4RsvpTeIfIndex;
                return SNMP_SUCCESS;
            }
        }
    }
    else
    {
        return SNMP_FAILURE;
    }

    while (RSVPTE_ONE)
    {
        i1RetVal =
            nmhGetNextIndexFsMplsRsvpTeIfTable (i4RsvpTeIfIndex,
                                                &i4RsvpTeNextIfIndex);

        if (i1RetVal == SNMP_SUCCESS)
        {
            u1RetVal = RpteGetIfEntry (i4RsvpTeNextIfIndex, &pIfEntry);

            if (u1RetVal == RPTE_SUCCESS)
            {
                pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));
                pNbrEntry = (tNbrEntry *) TMO_SLL_First (pNbrList);

                if (pNbrEntry != NULL)
                {
                    *pu4FsMplsRsvpTeNbrIfAddr = NBR_ENTRY_ADDR (pNbrEntry);
                    *pi4FsMplsRsvpTeIfIndex = i4RsvpTeNextIfIndex;
                    return SNMP_SUCCESS;
                }
            }
        }
        else
        {
            return SNMP_FAILURE;
        }
        i4RsvpTeIfIndex = i4RsvpTeNextIfIndex;
    }
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsRsvpTeNbrTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                nextFsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr
                nextFsMplsRsvpTeNbrIfAddr
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexFsMplsRsvpTeNbrTable (INT4 i4FsMplsRsvpTeIfIndex,
                                     INT4 *pi4NextFsMplsRsvpTeIfIndex,
                                     UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                     UINT4 *pu4NextFsMplsRsvpTeNbrIfAddr)
{
    UINT1               u1RetVal;
    INT1                i1RetVal;
    INT4                i4RsvpTeNextIfIndex = 0;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tTMO_SLL           *pNbrList = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);

    if (u1RetVal == RPTE_SUCCESS)
    {
        pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));
        TMO_SLL_Scan (pNbrList, pNbrEntry, tNbrEntry *)
        {
            if (NBR_ENTRY_ADDR (pNbrEntry) > u4FsMplsRsvpTeNbrIfAddr)
            {
                *pu4NextFsMplsRsvpTeNbrIfAddr = NBR_ENTRY_ADDR (pNbrEntry);
                *pi4NextFsMplsRsvpTeIfIndex = i4FsMplsRsvpTeIfIndex;
                return SNMP_SUCCESS;
            }
        }
    }

    while (RSVPTE_ONE)
    {
        i1RetVal =
            nmhGetNextIndexFsMplsRsvpTeIfTable (i4FsMplsRsvpTeIfIndex,
                                                &i4RsvpTeNextIfIndex);

        if (i1RetVal == SNMP_SUCCESS)
        {
            u1RetVal = RpteGetIfEntry (i4RsvpTeNextIfIndex, &pIfEntry);
            if (u1RetVal == RPTE_SUCCESS)
            {
                pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));
                pNbrEntry = (tNbrEntry *) TMO_SLL_First (pNbrList);

                if (pNbrEntry != NULL)
                {
                    *pu4NextFsMplsRsvpTeNbrIfAddr = NBR_ENTRY_ADDR (pNbrEntry);
                    *pi4NextFsMplsRsvpTeIfIndex = i4RsvpTeNextIfIndex;
                    return SNMP_SUCCESS;
                }
            }
        }
        else
        {
            return SNMP_FAILURE;
        }
        i4FsMplsRsvpTeIfIndex = i4RsvpTeNextIfIndex;
    }
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable (INT4 i4FsMplsRsvpTeIfIndex)
{
    if (i4FsMplsRsvpTeIfIndex <= RSVPTE_ZERO)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsRsvpTeIfStatsTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsMplsRsvpTeIfStatsTable (INT4 *pi4FsMplsRsvpTeIfIndex)
{
    return (nmhGetFirstIndexRsvpIfTable (pi4FsMplsRsvpTeIfIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsRsvpTeIfStatsTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                nextFsMplsRsvpTeIfIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexFsMplsRsvpTeIfStatsTable (INT4 i4FsMplsRsvpTeIfIndex,
                                         INT4 *pi4NextFsMplsRsvpTeIfIndex)
{
    return
        (nmhGetNextIndexRsvpIfTable (i4FsMplsRsvpTeIfIndex,
                                     pi4NextFsMplsRsvpTeIfIndex));

}

/* Get Routines for statif table */
/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumTnls
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumTnls
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumTnls (INT4 i4FsMplsRsvpTeIfIndex,
                             UINT4 *pu4RetValFsMplsRsvpTeIfNumTnls)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumTnls =
            RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumMsgSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumMsgSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumMsgSent (INT4 i4FsMplsRsvpTeIfIndex,
                                UINT4 *pu4RetValFsMplsRsvpTeIfNumMsgSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumMsgSent =
            RPTE_IF_STAT_ENTRY_NUM_MSG_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumMsgRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumMsgRcvd
 Output      :  The Ger Low Lev Routine Take the Indices &
 rCode = SNMP_ERR_NO_CREATION;
                 return SNMP_FAILURE;
                 
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumMsgRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                UINT4 *pu4RetValFsMplsRsvpTeIfNumMsgRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumMsgRcvd =
            RPTE_IF_STAT_ENTRY_NUM_MSG_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumHelloSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumHelloSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumHelloSent (INT4 i4FsMplsRsvpTeIfIndex,
                                  UINT4 *pu4RetValFsMplsRsvpTeIfNumHelloSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumHelloSent =
            RPTE_IF_STAT_ENTRY_NUM_HELLO_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumHelloRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumHelloRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumHelloRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                  UINT4 *pu4RetValFsMplsRsvpTeIfNumHelloRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumHelloRcvd =
            RPTE_IF_STAT_ENTRY_NUM_HELLO_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumPathErrSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumPathErrSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPathErrSent (INT4 i4FsMplsRsvpTeIfIndex,
                                    UINT4
                                    *pu4RetValFsMplsRsvpTeIfNumPathErrSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPathErrSent =
            RPTE_IF_STAT_ENTRY_PATH_ERR_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumPathErrRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumPathErrRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPathErrRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                    UINT4
                                    *pu4RetValFsMplsRsvpTeIfNumPathErrRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPathErrRcvd =
            RPTE_IF_STAT_ENTRY_PATH_ERR_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumPathTearSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumPathTearSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPathTearSent (INT4 i4FsMplsRsvpTeIfIndex,
                                     UINT4
                                     *pu4RetValFsMplsRsvpTeIfNumPathTearSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPathTearSent =
            RPTE_IF_STAT_ENTRY_PATH_TEAR_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumPathTearRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumPathTearRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPathTearRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                     UINT4
                                     *pu4RetValFsMplsRsvpTeIfNumPathTearRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPathTearRcvd =
            RPTE_IF_STAT_ENTRY_PATH_TEAR_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumResvErrSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumResvErrSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumResvErrSent (INT4 i4FsMplsRsvpTeIfIndex,
                                    UINT4
                                    *pu4RetValFsMplsRsvpTeIfNumResvErrSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumResvErrSent =
            RPTE_IF_STAT_ENTRY_RESV_ERR_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumResvErrRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumResvErrRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumResvErrRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                    UINT4
                                    *pu4RetValFsMplsRsvpTeIfNumResvErrRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumResvErrRcvd =
            RPTE_IF_STAT_ENTRY_RESV_ERR_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumResvTearSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumResvTearSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumResvTearSent (INT4 i4FsMplsRsvpTeIfIndex,
                                     UINT4
                                     *pu4RetValFsMplsRsvpTeIfNumResvTearSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumResvTearSent =
            RPTE_IF_STAT_ENTRY_RESV_TEAR_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumResvTearRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumResvTearRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumResvTearRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                     UINT4
                                     *pu4RetValFsMplsRsvpTeIfNumResvTearRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumResvTearRcvd =
            RPTE_IF_STAT_ENTRY_RESV_TEAR_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumResvConfSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumResvConfSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumResvConfSent (INT4 i4FsMplsRsvpTeIfIndex,
                                     UINT4
                                     *pu4RetValFsMplsRsvpTeIfNumResvConfSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumResvConfSent =
            RPTE_IF_STAT_ENTRY_RESV_CONF_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumResvConfRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumResvConfRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumResvConfRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                     UINT4
                                     *pu4RetValFsMplsRsvpTeIfNumResvConfRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumResvConfRcvd =
            RPTE_IF_STAT_ENTRY_RESV_CONF_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumBundleMsgSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumBundleMsgSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumBundleMsgSent (INT4 i4FsMplsRsvpTeIfIndex,
                                      UINT4
                                      *pu4RetValFsMplsRsvpTeIfNumBundleMsgSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumBundleMsgSent =
            RPTE_IF_STAT_ENTRY_BUNDLE_MSG_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumBundleMsgRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumBundleMsgRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumBundleMsgRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                      UINT4
                                      *pu4RetValFsMplsRsvpTeIfNumBundleMsgRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumBundleMsgRcvd =
            RPTE_IF_STAT_ENTRY_BUNDLE_MSG_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumSRefreshMsgSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumSRefreshMsgSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumSRefreshMsgSent (INT4 i4FsMplsRsvpTeIfIndex,
                                        UINT4
                                        *pu4RetValFsMplsRsvpTeIfNumSRefreshMsgSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumSRefreshMsgSent =
            RPTE_IF_STAT_ENTRY_SREFRESH_MSG_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumSRefreshMsgRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumSRefreshMsgRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumSRefreshMsgRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                        UINT4
                                        *pu4RetValFsMplsRsvpTeIfNumSRefreshMsgRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumSRefreshMsgRcvd =
            RPTE_IF_STAT_ENTRY_SREFRESH_MSG_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrRRCapable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrRRCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrRRCapable (INT4 i4FsMplsRsvpTeIfIndex,
                                UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                INT4 *pi4RetValFsMplsRsvpTeNbrRRCapable)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {

        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrRRCapable =
                NBR_ENTRY_RR_CAPABLE (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrRRState
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrRRState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrRRState (INT4 i4FsMplsRsvpTeIfIndex,
                              UINT4 u4FsMplsRsvpTeNbrIfAddr,
                              INT4 *pi4RetValFsMplsRsvpTeNbrRRState)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {

        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrRRState = NBR_ENTRY_RR_STATE (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrRMDCapable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrRMDCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrRMDCapable (INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                 INT4 *pi4RetValFsMplsRsvpTeNbrRMDCapable)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {

            *pi4RetValFsMplsRsvpTeNbrRMDCapable =
                NBR_ENTRY_RMD_CAPABLE (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrEncapsType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrEncapsType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrEncapsType (INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                 INT4 *pi4RetValFsMplsRsvpTeNbrEncapsType)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);

    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry)
            == RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrEncapsType = NBR_ENTRY_ENCAP (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrHelloSupport
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrHelloSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrHelloSupport (INT4 i4FsMplsRsvpTeIfIndex,
                                   UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                   INT4 *pi4RetValFsMplsRsvpTeNbrHelloSupport)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrHelloSupport =
                NBR_ENTRY_HELLO_SPRT (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrHelloState
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrHelloState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrHelloState (INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                 INT4 *pi4RetValFsMplsRsvpTeNbrHelloState)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrHelloState =
                NBR_ENTRY_HELLO_STATE (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrHelloRelation
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrHelloRelation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrHelloRelation (INT4 i4FsMplsRsvpTeIfIndex,
                                    UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                    INT4 *pi4RetValFsMplsRsvpTeNbrHelloRelation)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrHelloRelation =
                NBR_ENTRY_HELLO_REL (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrSrcInstInfo
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrSrcInstInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrSrcInstInfo (INT4 i4FsMplsRsvpTeIfIndex,
                                  UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                  INT4 *pi4RetValFsMplsRsvpTeNbrSrcInstInfo)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrSrcInstInfo =
                (INT4) NBR_ENTRY_SRC_INST_INFO (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrDestInstInfo
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrDestInstInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrDestInstInfo (INT4 i4FsMplsRsvpTeIfIndex,
                                   UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                   INT4 *pi4RetValFsMplsRsvpTeNbrDestInstInfo)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrDestInstInfo =
                (INT4) NBR_ENTRY_DEST_INST_INFO (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrCreationTime
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrCreationTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrCreationTime (INT4 i4FsMplsRsvpTeIfIndex,
                                   UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                   UINT4 *pu4RetValFsMplsRsvpTeNbrCreationTime)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pu4RetValFsMplsRsvpTeNbrCreationTime =
                NBR_ENTRY_NBR_CREATE_TIME (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrLclRprDetectionTime
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrLclRprDetectionTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrLclRprDetectionTime (INT4 i4FsMplsRsvpTeIfIndex,
                                          UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                          UINT4
                                          *pu4RetValFsMplsRsvpTeNbrLclRprDetectionTime)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pu4RetValFsMplsRsvpTeNbrLclRprDetectionTime =
                NBR_ENTRY_NBR_LCL_RP_DETECT_TIME (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrNumTunnels
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrNumTunnels
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrNumTunnels (INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                 UINT4 *pu4RetValFsMplsRsvpTeNbrNumTunnels)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pu4RetValFsMplsRsvpTeNbrNumTunnels =
                NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrStatus
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrStatus (INT4 i4FsMplsRsvpTeIfIndex,
                             UINT4 u4FsMplsRsvpTeNbrIfAddr,
                             INT4 *pi4RetValFsMplsRsvpTeNbrStatus)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrStatus = NBR_ENTRY_STATUS (pNbrEntry);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrGrRecoveryPathCapability
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrGrRecoveryPathCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrGrRecoveryPathCapability (INT4 i4FsMplsRsvpTeIfIndex,
                                               UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                               tSNMP_OCTET_STRING_TYPE
                                               *
                                               pRetValFsMplsRsvpTeNbrGrRecoveryPathCapability)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            CPY_TO_SNMP (pRetValFsMplsRsvpTeNbrGrRecoveryPathCapability,
                         &pNbrEntry->u1RecoveryPathCapability, RPTE_ONE);

            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrGrRestartTime
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrGrRestartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrGrRestartTime (INT4 i4FsMplsRsvpTeIfIndex,
                                    UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                    INT4 *pi4RetValFsMplsRsvpTeNbrGrRestartTime)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrGrRestartTime = pNbrEntry->u2RestartTime;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrGrRecoveryTime
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrGrRecoveryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrGrRecoveryTime (INT4 i4FsMplsRsvpTeIfIndex,
                                     UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                     INT4
                                     *pi4RetValFsMplsRsvpTeNbrGrRecoveryTime)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrGrRecoveryTime = pNbrEntry->u2RecoveryTime;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrGrProgressStatus
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrGrProgressStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrGrProgressStatus (INT4 i4FsMplsRsvpTeIfIndex,
                                       UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                       INT4
                                       *pi4RetValFsMplsRsvpTeNbrGrProgressStatus)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry) ==
            RPTE_SUCCESS)
        {
            *pi4RetValFsMplsRsvpTeNbrGrProgressStatus =
                pNbrEntry->u1GrProgressState;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNbrStorageType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                retValFsMplsRsvpTeNbrStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNbrStorageType (INT4 i4FsMplsRsvpTeIfIndex,
                                  UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                  INT4 *pi4RetValFsMplsRsvpTeNbrStorageType)
{
    UNUSED_PARAM (i4FsMplsRsvpTeIfIndex);
    UNUSED_PARAM (u4FsMplsRsvpTeNbrIfAddr);
    UNUSED_PARAM (pi4RetValFsMplsRsvpTeNbrStorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeHelloIntervalTime
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeHelloIntervalTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeHelloIntervalTime (INT4
                                     *pi4RetValFsMplsRsvpTeHelloIntervalTime)
{
    *pi4RetValFsMplsRsvpTeHelloIntervalTime =
        (INT4) (RSVPTE_HELLO_INTVL_TIME (gpRsvpTeGblInfo) *
                NO_OF_MSECS_PER_UNIT);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeRRCapable
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeRRCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeRRCapable (INT4 *pi4RetValFsMplsRsvpTeRRCapable)
{
    *pi4RetValFsMplsRsvpTeRRCapable = gu1RRCapable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeMsgIdCapable
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeMsgIdCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeMsgIdCapable (INT4 *pi4RetValFsMplsRsvpTeMsgIdCapable)
{
    if (gu1IntMsgIdCapable == RPTE_FALSE)
    {
        *pi4RetValFsMplsRsvpTeMsgIdCapable = gu1MsgIdCapable;
    }
    else
    {
        *pi4RetValFsMplsRsvpTeMsgIdCapable = RPTE_DISABLED;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeRMDPolicyObject
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeRMDPolicyObject
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeRMDPolicyObject (tSNMP_OCTET_STRING_TYPE *
                                   pRetValFsMplsRsvpTeRMDPolicyObject)
{
    CPY_TO_SNMP (pRetValFsMplsRsvpTeRMDPolicyObject, &gu4RMDFlags, RPTE_ONE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeMinTnlsWithMsgId
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeMinTnlsWithMsgId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeMinTnlsWithMsgId (UINT4
                                    *pu4RetValFsMplsRsvpTeMinTnlsWithMsgId)
{
    *pu4RetValFsMplsRsvpTeMinTnlsWithMsgId =
        RSVPTE_MIN_TNLS_WITH_MSG_ID (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNotificationEnabled
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeNotificationEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNotificationEnabled (INT4
                                       *pi4RetValFsMplsRsvpTeNotificationEnabled)
{
    *pi4RetValFsMplsRsvpTeNotificationEnabled =
        gpRsvpTeGblInfo->RsvpTeCfgParams.u1NotificationEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNotifyMsgRetransmitIntvl
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeNotifyMsgRetransmitIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNotifyMsgRetransmitIntvl (UINT4
                                            *pu4RetValFsMplsRsvpTeNotifyMsgRetransmitIntvl)
{
    *pu4RetValFsMplsRsvpTeNotifyMsgRetransmitIntvl =
        gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitIntvl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNotifyMsgRetransmitDecay
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeNotifyMsgRetransmitDecay
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNotifyMsgRetransmitDecay (UINT4
                                            *pu4RetValFsMplsRsvpTeNotifyMsgRetransmitDecay)
{
    *pu4RetValFsMplsRsvpTeNotifyMsgRetransmitDecay =
        gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitDecay;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeNotifyMsgRetransmitLimit
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeNotifyMsgRetransmitLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeNotifyMsgRetransmitLimit (UINT4
                                            *pu4RetValFsMplsRsvpTeNotifyMsgRetransmitLimit)
{
    *pu4RetValFsMplsRsvpTeNotifyMsgRetransmitLimit =
        gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitLimit;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeAdminStatusTimeIntvl
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeAdminStatusTimeIntvl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeAdminStatusTimeIntvl (UINT4
                                        *pu4RetValFsMplsRsvpTeAdminStatusTimeIntvl)
{
    *pu4RetValFsMplsRsvpTeAdminStatusTimeIntvl =
        gpRsvpTeGblInfo->RsvpTeCfgParams.u4AdminStatusTimerValue;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTePathStateRemovedSupport
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTePathStateRemovedSupport
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTePathStateRemovedSupport (INT4
                                           *pi4RetValFsMplsRsvpTePathStateRemovedSupport)
{
    *pi4RetValFsMplsRsvpTePathStateRemovedSupport =
        gpRsvpTeGblInfo->RsvpTeCfgParams.u1PathStateRemovedFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeLabelSetEnabled
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeLabelSetEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeLabelSetEnabled (INT4 *pi4RetValFsMplsRsvpTeLabelSetEnabled)
{
    *pi4RetValFsMplsRsvpTeLabelSetEnabled =
        gpRsvpTeGblInfo->RsvpTeCfgParams.u1LabelSetEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeAdminStatusCapability
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeAdminStatusCapabiliy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeAdminStatusCapability (INT4
                                         *pi4RetValFsMplsRsvpTeAdminStatusCapability)
{
    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1IntAdminStatusCapable == RPTE_FALSE)
    {
        *pi4RetValFsMplsRsvpTeAdminStatusCapability =
            gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable;
    }
    else
    {
        *pi4RetValFsMplsRsvpTeAdminStatusCapability = RPTE_DISABLED;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGrCapability
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGrCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGrCapability (INT4 *pi4RetValFsMplsRsvpTeGrCapability)
{
    *pi4RetValFsMplsRsvpTeGrCapability =
        gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGrRecoveryPathCapability
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGrRecoveryPathCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGrRecoveryPathCapability (tSNMP_OCTET_STRING_TYPE *
                                            pRetValFsMplsRsvpTeGrRecoveryPathCapability)
{
    CPY_TO_SNMP (pRetValFsMplsRsvpTeGrRecoveryPathCapability,
                 &gpRsvpTeGblInfo->RsvpTeCfgParams.u1RecoveryPathCapability,
                 RPTE_ONE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGrRestartTime
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGrRestartTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGrRestartTime (INT4 *pi4RetValFsMplsRsvpTeGrRestartTime)
{
    *pi4RetValFsMplsRsvpTeGrRestartTime =
        gpRsvpTeGblInfo->RsvpTeCfgParams.u2RestartTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGrRecoveryTime
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGrRecoveryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGrRecoveryTime (INT4 *pi4RetValFsMplsRsvpTeGrRecoveryTime)
{
    *pi4RetValFsMplsRsvpTeGrRecoveryTime =
        gpRsvpTeGblInfo->RsvpTeCfgParams.u2RecoveryTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGrProgressStatus
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGrProgressStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGrProgressStatus (INT4 *pi4RetValFsMplsRsvpTeGrProgressStatus)
{
    *pi4RetValFsMplsRsvpTeGrProgressStatus = gpRsvpTeGblInfo->u1GrProgressState;
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsFrrDetourIncoming
 Input       :  The Indices

                The Object 
                retValFsMplsFrrDetourIncoming
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrDetourIncoming (INT4 *pi4RetValFsMplsFrrDetourIncoming)
{
    *pi4RetValFsMplsFrrDetourIncoming =
        (INT4) RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrDetourOutgoing
 Input       :  The Indices

                The Object 
                retValFsMplsFrrDetourOutgoing
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrDetourOutgoing (INT4 *pi4RetValFsMplsFrrDetourOutgoing)
{
    *pi4RetValFsMplsFrrDetourOutgoing =
        (INT4) RSVPTE_FRR_DETOUR_OUTGOING_NUM (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrDetourOriginating
 Input       :  The Indices

                The Object 
                retValFsMplsFrrDetourOriginating
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrDetourOriginating (INT4 *pi4RetValFsMplsFrrDetourOriginating)
{
    *pi4RetValFsMplsFrrDetourOriginating =
        (INT4) RSVPTE_FRR_DETOUR_ORIG_NUM (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrSwitchover
 Input       :  The Indices

                The Object 
                retValFsMplsFrrSwitchover
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrSwitchover (UINT4 *pu4RetValFsMplsFrrSwitchover)
{
    *pu4RetValFsMplsFrrSwitchover =
        RSVPTE_FRR_SWITCH_OVER_NUM (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConfIfs
 Input       :  The Indices

                The Object 
                retValFsMplsFrrConfIfs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConfIfs (INT4 *pi4RetValFsMplsFrrConfIfs)
{
    *pi4RetValFsMplsFrrConfIfs =
        (INT4) RSVPTE_FRR_CONF_IF_NUM (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrActProtectedIfs
 Input       :  The Indices

                The Object 
                retValFsMplsFrrActProtectedIfs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrActProtectedIfs (UINT4 *pu4RetValFsMplsFrrActProtectedIfs)
{
    *pu4RetValFsMplsFrrActProtectedIfs =
        RSVPTE_FRR_ACT_PROT_IF_NUM (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConfProtectionTuns
 Input       :  The Indices

                The Object 
                retValFsMplsFrrConfProtectionTuns
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConfProtectionTuns (UINT4 *pu4RetValFsMplsFrrConfProtectionTuns)
{
    *pu4RetValFsMplsFrrConfProtectionTuns =
        RSVPTE_FRR_CONF_PROT_TUN_NUM (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrActProtectionTuns
 Input       :  The Indices

                The Object 
                retValFsMplsFrrActProtectionTuns
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrActProtectionTuns (UINT4 *pu4RetValFsMplsFrrActProtectionTuns)
{
    *pu4RetValFsMplsFrrActProtectionTuns =
        RSVPTE_FRR_ACT_PROT_TUN_NUM (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrActProtectedLSPs
 Input       :  The Indices

                The Object 
                retValFsMplsFrrActProtectedLSPs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrActProtectedLSPs (UINT4 *pu4RetValFsMplsFrrActProtectedLSPs)
{
    *pu4RetValFsMplsFrrActProtectedLSPs =
        RSVPTE_FRR_ACT_PROT_LSP_NUM (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrRevertiveMode
 Input       :  The Indices

                The Object 
                retValFsMplsFrrRevertiveMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrRevertiveMode (tSNMP_OCTET_STRING_TYPE *
                              pRetValFsMplsFrrRevertiveMode)
{
    pRetValFsMplsFrrRevertiveMode->pu1_OctetList[RPTE_ZERO] =
        (UINT1) RSVPTE_FRR_REVERTIVE_MODE (gpRsvpTeGblInfo);
    pRetValFsMplsFrrRevertiveMode->i4_Length = RPTE_ONE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrDetourMergingEnabled
 Input       :  The Indices

                The Object 
                retValFsMplsFrrDetourMergingEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrDetourMergingEnabled (INT4
                                     *pi4RetValFsMplsFrrDetourMergingEnabled)
{
    *pi4RetValFsMplsFrrDetourMergingEnabled =
        (INT4) RSVPTE_FRR_DETOUR_MERGING_CAPABLE (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrDetourEnabled
 Input       :  The Indices

                The Object 
                retValFsMplsFrrDetourEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrDetourEnabled (INT4 *pi4RetValFsMplsFrrDetourEnabled)
{
    *pi4RetValFsMplsFrrDetourEnabled =
        (INT4) RSVPTE_FRR_DETOUR_CAPABLE (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrCspfRetryInterval
 Input       :  The Indices

                The Object 
                retValFsMplsFrrCspfRetryInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrCspfRetryInterval (INT4 *pi4RetValFsMplsFrrCspfRetryInterval)
{
    *pi4RetValFsMplsFrrCspfRetryInterval =
        (INT4) RSVPTE_FRR_CSPF_RETRY_INTERVAL (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrCspfRetryCount
 Input       :  The Indices

                The Object 
                retValFsMplsFrrCspfRetryCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrCspfRetryCount (UINT4 *pu4RetValFsMplsFrrCspfRetryCount)
{
    *pu4RetValFsMplsFrrCspfRetryCount =
        RSVPTE_FRR_CSPF_RETRY_COUNT (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrNotifsEnabled
 Input       :  The Indices

                The Object 
                retValFsMplsFrrNotifsEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrNotifsEnabled (INT4 *pi4RetValFsMplsFrrNotifsEnabled)
{
    *pi4RetValFsMplsFrrNotifsEnabled =
        (INT4) RSVPTE_FRR_NOTIFS_ENABLED (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrMakeAfterBreakEnabled
 Input       :  The Indices

                The Object
                retValFsMplsFrrMakeAfterBreakEnabled
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrMakeAfterBreakEnabled (INT4
                                      *pi4RetValFsMplsFrrMakeAfterBreakEnabled)
{
    *pi4RetValFsMplsFrrMakeAfterBreakEnabled =
        (INT4) RSVPTE_FRR_MAKE_AFTER_BREAK_ENABLED (gpRsvpTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumPathSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumPathSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPathSent (INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 *pu4RetValFsMplsRsvpTeIfNumPathSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPathSent =
            RPTE_IF_STAT_ENTRY_PATH_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumPathRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumPathRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPathRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 *pu4RetValFsMplsRsvpTeIfNumPathRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPathRcvd =
            RPTE_IF_STAT_ENTRY_PATH_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumResvSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumResvSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumResvSent (INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 *pu4RetValFsMplsRsvpTeIfNumResvSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumResvSent =
            RPTE_IF_STAT_ENTRY_RESV_SENT (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumResvRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumResvRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumResvRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 *pu4RetValFsMplsRsvpTeIfNumResvRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumResvRcvd =
            RPTE_IF_STAT_ENTRY_RESV_RCVD (pIfEntry);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumNotifyMsgSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfNumNotifyMsgSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumNotifyMsgSent (INT4 i4FsMplsRsvpTeIfIndex,
                                      UINT4
                                      *pu4RetValFsMplsRsvpTeIfNumNotifyMsgSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumNotifyMsgSent =
            (IF_ENTRY_STATS_INFO (pIfEntry)).u4IfNumNotifyMsgSent;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumNotifyMsgRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfNumNotifyMsgRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumNotifyMsgRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                      UINT4
                                      *pu4RetValFsMplsRsvpTeIfNumNotifyMsgRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumNotifyMsgRcvd =
            (IF_ENTRY_STATS_INFO (pIfEntry)).u4IfNumNotifyMsgRcvd;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumRecoveryPathSent
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumRecoveryPathSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhGetFsMplsRsvpTeIfNumRecoveryPathSent (INT4 i4FsMplsRsvpTeIfIndex,
                                         UINT4
                                         *pu4RetValFsMplsRsvpTeIfNumRecoveryPathSent)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumRecoveryPathSent =
            pIfEntry->IfStatsInfo.u4IfNumRecoveryPathSent;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumRecoveryPathRcvd
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                retValFsMplsRsvpTeIfNumRecoveryPathRcvd
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumRecoveryPathRcvd (INT4 i4FsMplsRsvpTeIfIndex,
                                         UINT4
                                         *pu4RetValFsMplsRsvpTeIfNumRecoveryPathRcvd)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumRecoveryPathRcvd =
            pIfEntry->IfStatsInfo.u4IfNumRecoveryPathRcvd;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumPathSentWithRecoveryLbl
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfNumPathSentWithRecoveryLbl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPathSentWithRecoveryLbl (INT4 i4FsMplsRsvpTeIfIndex,
                                                UINT4
                                                *pu4RetValFsMplsRsvpTeIfNumPathSentWithRecoveryLbl)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPathSentWithRecoveryLbl =
            pIfEntry->IfStatsInfo.u4IfNumPathSentWithRecoveryLbl;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumPathRcvdWithRecoveryLbl
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfNumPathRcvdWithRecoveryLbl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPathRcvdWithRecoveryLbl (INT4 i4FsMplsRsvpTeIfIndex,
                                                UINT4
                                                *pu4RetValFsMplsRsvpTeIfNumPathRcvdWithRecoveryLbl)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPathRcvdWithRecoveryLbl =
            pIfEntry->IfStatsInfo.u4IfNumPathRcvdWithRecoveryLbl;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumPathSentWithSuggestedLbl
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfNumPathSentWithSuggestedLbl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPathSentWithSuggestedLbl (INT4 i4FsMplsRsvpTeIfIndex,
                                                 UINT4
                                                 *pu4RetValFsMplsRsvpTeIfNumPathSentWithSuggestedLbl)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPathSentWithSuggestedLbl =
            pIfEntry->IfStatsInfo.u4IfNumPathSentWithSuggestedLbl;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumPathRcvdWithSuggestedLbl
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfNumPathRcvdWithSuggestedLbl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPathRcvdWithSuggestedLbl (INT4 i4FsMplsRsvpTeIfIndex,
                                                 UINT4
                                                 *pu4RetValFsMplsRsvpTeIfNumPathRcvdWithSuggestedLbl)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPathRcvdWithSuggestedLbl =
            pIfEntry->IfStatsInfo.u4IfNumPathRcvdWithSuggestedLbl;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumHelloSentWithRestartCap
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfNumHelloSentWithRestartCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumHelloSentWithRestartCap (INT4 i4FsMplsRsvpTeIfIndex,
                                                UINT4
                                                *pu4RetValFsMplsRsvpTeIfNumHelloSentWithRestartCap)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumHelloSentWithRestartCap =
            pIfEntry->IfStatsInfo.u4IfNumHelloSentWithRestartCap;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumHelloRcvdWithRestartCap
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfNumHelloRcvdWithRestartCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumHelloRcvdWithRestartCap (INT4 i4FsMplsRsvpTeIfIndex,
                                                UINT4
                                                *pu4RetValFsMplsRsvpTeIfNumHelloRcvdWithRestartCap)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumHelloRcvdWithRestartCap =
            pIfEntry->IfStatsInfo.u4IfNumHelloRcvdWithRestartCap;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumHelloSentWithCapability
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfNumHelloSentWithCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumHelloSentWithCapability (INT4 i4FsMplsRsvpTeIfIndex,
                                                UINT4
                                                *pu4RetValFsMplsRsvpTeIfNumHelloSentWithCapability)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumHelloSentWithCapability =
            pIfEntry->IfStatsInfo.u4IfNumHelloSentWithCapability;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeIfNumHelloRcvdWithCapability
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                retValFsMplsRsvpTeIfNumHelloRcvdWithCapability
 Output      :  The Get Low Lev Routine Take the Indices &                                                                                    store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumHelloRcvdWithCapability (INT4 i4FsMplsRsvpTeIfIndex,
                                                UINT4
                                                *pu4RetValFsMplsRsvpTeIfNumHelloRcvdWithCapability)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumHelloRcvdWithCapability =
            pIfEntry->IfStatsInfo.u4IfNumHelloRcvdWithCapability;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

 /****************************************************************************
  Function    :  nmhGetFsMplsRsvpTeIfNumPktDiscrded
  Input       :  The Indices
                 FsMplsRsvpTeIfIndex
                 
                 The Object
                 retValFsMplsRsvpTeIfNumNotifyMsgSent
  Output      :  The Get Low Lev Routine Take the Indices &
                 store the Value requested in the Return val.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMplsRsvpTeIfNumPktDiscrded (INT4 i4FsMplsRsvpTeIfIndex,
                                    UINT4
                                    *pu4RetValFsMplsRsvpTeIfNumPktDiscrded)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        *pu4RetValFsMplsRsvpTeIfNumPktDiscrded =
            RPTE_IF_STAT_ENTRY_PKT_DISCRDED (pIfEntry);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeReoptimizeTime
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeReoptimizeTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeReoptimizeTime (INT4 *pi4RetValFsMplsRsvpTeReoptimizeTime)
{
    *pi4RetValFsMplsRsvpTeReoptimizeTime =
        (INT4) gpRsvpTeGblInfo->RsvpTeCfgParams.u4ReoptimizeTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeEroCacheTime
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeEroCacheTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeEroCacheTime (INT4 *pi4RetValFsMplsRsvpTeEroCacheTime)
{
    *pi4RetValFsMplsRsvpTeEroCacheTime =
        (INT4) gpRsvpTeGblInfo->RsvpTeCfgParams.u4EroCacheTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeReoptLinkMaintenance
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeReoptLinkMaintenance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeReoptLinkMaintenance (INT4
                                        *pi4RetValFsMplsRsvpTeReoptLinkMaintenance)
{
    UNUSED_PARAM (pi4RetValFsMplsRsvpTeReoptLinkMaintenance);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeReoptNodeMaintenance
 Input       :  The Indices

                The Object
                retValFsMplsRsvpTeReoptNodeMaintenance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeReoptNodeMaintenance (INT4
                                        *pi4RetValFsMplsRsvpTeReoptNodeMaintenance)
{
    UNUSED_PARAM (pi4RetValFsMplsRsvpTeReoptNodeMaintenance);
    return SNMP_SUCCESS;
}

#ifdef MPLS_L3VPN_WANTED
/* RSVP-TE MPLS-L3VPN-TE */
/* LOW LEVEL Routines for Table : FsMplsL3VpnRsvpTeMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable
 Input       :  The Indices
                FsMplsL3VpnRsvpTeMapPrefixType
                FsMplsL3VpnRsvpTeMapPrefix
                FsMplsL3VpnRsvpTeMapMaskType
                FsMplsL3VpnRsvpTeMapMask
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable (INT4
                                                   i4FsMplsL3VpnRsvpTeMapPrefixType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsMplsL3VpnRsvpTeMapPrefix,
                                                   INT4
                                                   i4FsMplsL3VpnRsvpTeMapMaskType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pFsMplsL3VpnRsvpTeMapMask)
{
    UINT4               SourceAddrMin = 0;
    UINT4               SourceAddrMax = 0;
    UINT1               u1Counter = 0;
    UINT4               u4Mask = 0;
    UINT4               u4Prefix = 0;

    UNUSED_PARAM (i4FsMplsL3VpnRsvpTeMapPrefixType);
    UNUSED_PARAM (i4FsMplsL3VpnRsvpTeMapMaskType);

    MEMCPY (&SourceAddrMin,
            pFsMplsL3VpnRsvpTeMapPrefix->pu1_OctetList, IPV4_ADDR_LENGTH);
    SourceAddrMin = OSIX_NTOHL (SourceAddrMin);

    MEMCPY (&SourceAddrMax,
            pFsMplsL3VpnRsvpTeMapMask->pu1_OctetList, IPV4_ADDR_LENGTH);
    SourceAddrMax = OSIX_NTOHL (SourceAddrMax);

    if ((pFsMplsL3VpnRsvpTeMapPrefix->i4_Length != IPV4_ADDR_LENGTH) ||
        (pFsMplsL3VpnRsvpTeMapMask->i4_Length != IPV4_ADDR_LENGTH))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "ValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable Failed :Invalid Subnet/IP\t\n");
        return SNMP_FAILURE;
    }
    if (SourceAddrMin == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "ValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable Failed :Invalid IP\t\n");
        return SNMP_FAILURE;
    }
    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapPrefix, u4Prefix);
    if ((MPLS_IS_ADDR_LOOPBACK (u4Prefix)) ||
        (MPLS_IS_BROADCAST_ADDR (u4Prefix)))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "ValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable Failed : source loopback/bradcast IP\t\n");
        return SNMP_FAILURE;
    }

    if (CfaIpIfCheckLoopbackAddr (u4Prefix) == CFA_SUCCESS)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "ValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable Failed : source loopback IP\t\n");
        return SNMP_FAILURE;
    }

    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapMask, u4Mask);
    /* The valid subnet address is from 0 to 32 */
    for (u1Counter = 0; u1Counter <= RSVP_MAX_CIDR; ++u1Counter)
    {
        if (u4CidrSubnetMask[u1Counter] == u4Mask)
        {
            break;
        }
    }

    /* binding interface mask should be 32 bit mask. */
    if (u1Counter != 0)
    {
        if (u1Counter != CFA_MAX_CIDR)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "ValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable Failed :Invalid SubnetMask\t\n");
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsL3VpnRsvpTeMapTable
 Input       :  The Indices
                FsMplsL3VpnRsvpTeMapPrefixType
                FsMplsL3VpnRsvpTeMapPrefix
                FsMplsL3VpnRsvpTeMapMaskType
                FsMplsL3VpnRsvpTeMapMask
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsL3VpnRsvpTeMapTable (INT4
                                           *pi4FsMplsL3VpnRsvpTeMapPrefixType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMplsL3VpnRsvpTeMapPrefix,
                                           INT4
                                           *pi4FsMplsL3VpnRsvpTeMapMaskType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pFsMplsL3VpnRsvpTeMapMask)
{
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    *pi4FsMplsL3VpnRsvpTeMapPrefixType = 0;
    *pi4FsMplsL3VpnRsvpTeMapMaskType = 0;

    pL3VpnRsvpMapLabelEntry = L3vpnGetFirstRsvpMapLabelTable ();
    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MPLS_INTEGER_TO_OCTETSTRING (pL3VpnRsvpMapLabelEntry->u4Prefix,
                                 pFsMplsL3VpnRsvpTeMapPrefix);
    MPLS_INTEGER_TO_OCTETSTRING (pL3VpnRsvpMapLabelEntry->u4Mask,
                                 pFsMplsL3VpnRsvpTeMapMask);
    *pi4FsMplsL3VpnRsvpTeMapPrefixType = pL3VpnRsvpMapLabelEntry->u1PrefixType;
    *pi4FsMplsL3VpnRsvpTeMapMaskType = pL3VpnRsvpMapLabelEntry->u1MaskType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsL3VpnRsvpTeMapTable
 Input       :  The Indices
                FsMplsL3VpnRsvpTeMapPrefixType
                nextFsMplsL3VpnRsvpTeMapPrefixType
                FsMplsL3VpnRsvpTeMapPrefix
                nextFsMplsL3VpnRsvpTeMapPrefix
                FsMplsL3VpnRsvpTeMapMaskType
                nextFsMplsL3VpnRsvpTeMapMaskType
                FsMplsL3VpnRsvpTeMapMask
                nextFsMplsL3VpnRsvpTeMapMask
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsL3VpnRsvpTeMapTable (INT4 i4FsMplsL3VpnRsvpTeMapPrefixType,
                                          INT4
                                          *pi4NextFsMplsL3VpnRsvpTeMapPrefixType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMplsL3VpnRsvpTeMapPrefix,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextFsMplsL3VpnRsvpTeMapPrefix,
                                          INT4 i4FsMplsL3VpnRsvpTeMapMaskType,
                                          INT4
                                          *pi4NextFsMplsL3VpnRsvpTeMapMaskType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pFsMplsL3VpnRsvpTeMapMask,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextFsMplsL3VpnRsvpTeMapMask)
{
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;
    tL3VpnRsvpMapLabelEntry L3VpnRsvpMapLabelEntry;

    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapPrefix, u4Prefix);
    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapMask, u4Mask);
    L3VpnRsvpMapLabelEntry.u4Prefix = u4Prefix;
    L3VpnRsvpMapLabelEntry.u4Mask = u4Mask;
    L3VpnRsvpMapLabelEntry.u1PrefixType =
        (UINT1) i4FsMplsL3VpnRsvpTeMapPrefixType;
    L3VpnRsvpMapLabelEntry.u1MaskType = (UINT1) i4FsMplsL3VpnRsvpTeMapMaskType;

    pL3VpnRsvpMapLabelEntry =
        L3vpnGetNextRsvpMapLabelTable (&L3VpnRsvpMapLabelEntry);
    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    MPLS_INTEGER_TO_OCTETSTRING (pL3VpnRsvpMapLabelEntry->u4Prefix,
                                 pNextFsMplsL3VpnRsvpTeMapPrefix);
    MPLS_INTEGER_TO_OCTETSTRING (pL3VpnRsvpMapLabelEntry->u4Mask,
                                 pNextFsMplsL3VpnRsvpTeMapMask);
    *pi4NextFsMplsL3VpnRsvpTeMapPrefixType =
        pL3VpnRsvpMapLabelEntry->u1PrefixType;
    *pi4NextFsMplsL3VpnRsvpTeMapMaskType = pL3VpnRsvpMapLabelEntry->u1MaskType;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsL3VpnRsvpTeMapTnlIndex
 Input       :  The Indices
                FsMplsL3VpnRsvpTeMapPrefixType
                FsMplsL3VpnRsvpTeMapPrefix
                FsMplsL3VpnRsvpTeMapMaskType
                FsMplsL3VpnRsvpTeMapMask

                The Object
                retValFsMplsL3VpnRsvpTeMapTnlIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetFsMplsL3VpnRsvpTeMapTnlIndex (INT4 i4FsMplsL3VpnRsvpTeMapPrefixType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMplsL3VpnRsvpTeMapPrefix,
                                    INT4 i4FsMplsL3VpnRsvpTeMapMaskType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMplsL3VpnRsvpTeMapMask,
                                    INT4 *pi4RetValFsMplsL3VpnRsvpTeMapTnlIndex)
{
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapPrefix, u4Prefix);
    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapMask, u4Mask);
    pL3VpnRsvpMapLabelEntry =
        L3vpnGetRsvpMapLabelEntry ((UINT1) i4FsMplsL3VpnRsvpTeMapPrefixType,
                                   u4Prefix,
                                   (UINT1) i4FsMplsL3VpnRsvpTeMapMaskType,
                                   u4Mask);
    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMplsL3VpnRsvpTeMapTnlIndex =
        (INT4) pL3VpnRsvpMapLabelEntry->u4TnlIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsL3VpnRsvpTeMapRowStatus
 Input       :  The Indices
                FsMplsL3VpnRsvpTeMapPrefixType
                FsMplsL3VpnRsvpTeMapPrefix
                FsMplsL3VpnRsvpTeMapMaskType
                FsMplsL3VpnRsvpTeMapMask

                The Object
                retValFsMplsL3VpnRsvpTeMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsL3VpnRsvpTeMapRowStatus (INT4 i4FsMplsL3VpnRsvpTeMapPrefixType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMplsL3VpnRsvpTeMapPrefix,
                                     INT4 i4FsMplsL3VpnRsvpTeMapMaskType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMplsL3VpnRsvpTeMapMask,
                                     INT4
                                     *pi4RetValFsMplsL3VpnRsvpTeMapRowStatus)
{
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapPrefix, u4Prefix);
    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapMask, u4Mask);

    pL3VpnRsvpMapLabelEntry =
        L3vpnGetRsvpMapLabelEntry ((UINT1) i4FsMplsL3VpnRsvpTeMapPrefixType,
                                   u4Prefix,
                                   (UINT1) i4FsMplsL3VpnRsvpTeMapMaskType,
                                   u4Mask);
    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMplsL3VpnRsvpTeMapRowStatus =
        (INT4) pL3VpnRsvpMapLabelEntry->u1RowStatus;

    return SNMP_SUCCESS;
}
#endif

/*----------------------------------------------------------------------------*/
/*                        End of File rpteget1.c                              */
/*----------------------------------------------------------------------------*/
