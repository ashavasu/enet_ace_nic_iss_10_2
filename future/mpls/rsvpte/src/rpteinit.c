
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rpteinit.c,v 1.73 2018/01/03 11:31:23 siva Exp $
 *              
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rpteinit.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : RSVP-TE 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for the initialsation
 *                             and management of memory associated with 
 *                             RSVP TE tunnels.
 *---------------------------------------------------------------------------*/

#define RSVP_INIT_C

#include "rpteincs.h"
#include "rptegbl.h"
#include "mplscli.h"
#include "iss.h"
#ifdef SNMP_2_WANTED
#include "fsrsvpwr.h"
#include "fsmpfrwr.h"
#endif

PRIVATE INT4 RpteRecipientRBCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 RpteTnlRBCmp PROTO ((tRBElem *, tRBElem *));
PRIVATE INT4 RpteFrrTnlPortMappingRBCmp PROTO ((tRBElem *, tRBElem *));

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteStartRsvpTask
 * Description     : This function Starts the RSVP Task
 * Input (s)       : None
 * Output (s)      : None
 * Returns         : OSIX_FAILURE / OSIX_SUCCESS
 */
/*---------------------------------------------------------------------------*/

PUBLIC INT4
RpteStartRsvpTask (VOID)
{
    /*stop Max-wait timer im mpls-rtr module */
    MplsStopRequestedTimers (MPLS_RPTE_MAX_WAIT_TMR_EVENT, NULL);

    return (INT4) (OsixTskCrt ((UINT1 *) "RPTE", 150 | OSIX_SCHED_RR,
                               (UINT4) (3 * OSIX_DEFAULT_STACK_SIZE),
                               (OsixTskEntry) RpteRsvpTaskMain, 0,
                               &RSVP_TSK_ID));
}

/*****************************************************************************/
/* Function Name : RsvpeTeDeleteMemPools                                     */
/* Description   : This routine deletes memory pools                         */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
RsvpeTeDeleteMemPools ()
{
    RsvpteSizingMemDeleteMemPools ();
}

/*****************************************************************************/
/* Function Name : RpteCreateRsvpTeMem                                       */
/* Description   : This routine allocates all the memory required for the    */
/*                 support of RSVP TE Tunnels.                               */
/* Input(s)      : pRsvpTeGblInfo. - Pointer to the Rsvp Te Global           */
/*                 information.                                              */
/* Output(s)     : pRsvpTeGblInfo - Pointer to the allocated memory block in */
/*                 case of successful allocation.                            */
/* Return(s)     : RPTE_SUCCESS in case of successful allocation otherwise   */
/*                 RPTE_FAILURE                                              */
/*****************************************************************************/
UINT1
RpteCreateRsvpTeMem (tRsvpTeGblInfo * pRsvpTeGblInfo)
{

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateRsvpTeMem : ENTRY \n");

    /* Memory pools creation */
    if (RsvpteSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Creation of Memory Pools failed\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteCreateRsvpTeMem : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    RSVPTE_TNL_GBL_TRIE (pRsvpTeGblInfo) =
        (tRpteTrieGbl *) (VOID *) MemAllocMemBlk (RSVPTE_TRIE_GBL_POOL_ID);

    if (RSVPTE_TNL_GBL_TRIE (pRsvpTeGblInfo) == NULL)
    {
        RsvpeTeDeleteMemPools ();
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Creation of Gbl Trie info failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteCreateRsvpTeMem : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    MEMSET (RSVPTE_TNL_GBL_TRIE (pRsvpTeGblInfo), RPTE_ZERO,
            sizeof (tRpteTrieGbl));

    if (RpteCreateTrie (KEY_SIZE_4) == RPTE_FAILURE)
    {
        MemReleaseMemBlock (RSVPTE_TRIE_GBL_POOL_ID,
                            (UINT1 *) RSVPTE_TNL_GBL_TRIE (pRsvpTeGblInfo));
        RsvpeTeDeleteMemPools ();
        RSVPTE_DBG (RSVPTE_MAIN_MEM,
                    "Creation of local msg id database failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteCreateRsvpTeMem : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (RpteCreateTrie (KEY_SIZE_8) == RPTE_FAILURE)
    {
        MemReleaseMemBlock (RSVPTE_TRIE_GBL_POOL_ID,
                            (UINT1 *) RSVPTE_TNL_GBL_TRIE (pRsvpTeGblInfo));
        RsvpeTeDeleteMemPools ();
        RSVPTE_DBG (RSVPTE_MAIN_MEM,
                    "Creation of neighbour msg id database failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteCreateRsvpTeMem : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (RpteOpenUdpInterface () == RPTE_FAILURE)
    {
        MemReleaseMemBlock (RSVPTE_TRIE_GBL_POOL_ID,
                            (UINT1 *) RSVPTE_TNL_GBL_TRIE (pRsvpTeGblInfo));

        /* Earlier created memory pools are deleted. */
        RsvpeTeDeleteMemPools ();

        RSVPTE_DBG (RSVPTE_MAIN_MISC, "UDP Interface Initialization Failed\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateRsvpTeMem : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateRsvpTeMem : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteDeleteRsvpTeMem                                       */
/* Description   : This routine release all the memory pools and hash tables */
/*                 allocated for the support of RSVP TE Tunnels.             */
/* Input(s)      : pRsvpTeGblInfo. - Pointer to the Rsvp Te Global           */
/*                 information.                                              */
/* Output(s)     : None. (All memory released pointed by pRsvpTeGblInfo)     */
/* Return(s)     : None. (As memory release is expected to be always success */
/*                 -ful, no value is returned.                               */
/*****************************************************************************/
VOID
RpteDeleteRsvpTeMem (const tRsvpTeGblInfo * pRsvpTeGblInfo)
{
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteDeleteRsvpTeMem : ENTRY \n");

    /* Deletion of the Tunnel Hash Table */
    RpteUtlDeleteTnlTbl ();

    if (RSVP_GBL_IF_HSH_TBL != NULL)
    {
        RpteCleanIfTable ();
    }

    if (RPTE_64_BIT_TRIE_ALLOC_FLAG == RPTE_TRUE)
    {
        RPTE_64_BIT_TRIE_ALLOC_FLAG = RPTE_FALSE;
    }

    if (RPTE_32_BIT_TRIE_ALLOC_FLAG == RPTE_TRUE)
    {
        RPTE_32_BIT_TRIE_ALLOC_FLAG = RPTE_FALSE;
    }

    MemReleaseMemBlock (RSVPTE_TRIE_GBL_POOL_ID,
                        (UINT1 *) RSVPTE_TNL_GBL_TRIE (pRsvpTeGblInfo));

    /* Deletion of the Memory Pools */
    RsvpeTeDeleteMemPools ();

    /* Close the Udp Interface */
    RpteCloseUdpInterface ();

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteDeleteRsvpTeMem : EXIT \n");

    return;
}

/*****************************************************************************/
/* Function Name : RpteInitRsvpTe                                            */
/* Description   : This routine allocates the memory for the main structure  */
/*                 to provide RSVP TE support, and intialises the configurat */
/*                 ion parameters with default values.                       */
/* Input(s)      : ppRsvpTeGblInfo. - Pointer to the Rsvp Te Global          */
/*                 information.                                              */
/* Output(s)     : ppRsvpTeGblInfo - Pointer to the allocated memory block   */
/*                 in case of successful allocation.                         */
/* Return(s)     : RPTE_SUCCESS in case of successful allocation otherwise   */
/*                 RPTE_FAILURE                                              */
/*****************************************************************************/
UINT1
RpteInitRsvpTe (VOID)
{
    tRsvpTeGblInfo     *pTmpRsvpTeInfo = &gRsvpTeGblInfo;
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteInitRsvpTe : ENTRY \n");

    if (RpteQueueInitRsvpTe () != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM,
                    "MAIN: Failed to Create RSVP Message Queue.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteInitRsvpTe : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* Allocation for RSVP TE Main data block */

    /* Initialising the configuration variables with the default values */
    RSVPTE_MAX_RSVPTETNL (pTmpRsvpTeInfo)
        =
        (UINT2) FsRSVPTESizingParams[MAX_RSVPTE_TUNNEL_SIZING_ID].
        u4PreAllocatedUnits;
    RSVPTE_MAX_NEWSUBOBJ_PERTNL (pTmpRsvpTeInfo) =
        (UINT2) FsRSVPTESizingParams[MAX_RSVPTE_NEWSUBOBJ_INFO_SIZING_ID].
        u4PreAllocatedUnits;

    /*RSVPTE_RROBJ_SPRTD (pTmpRsvpTeInfo) = RPTE_TRUE; */
    RSVPTE_HELLO_SPRTD (pTmpRsvpTeInfo) = RPTE_FALSE;
    RSVPTE_SOCK_SPRTD (pTmpRsvpTeInfo) = RPTE_TRUE;
    RSVPTE_QOS_SPRTD (pTmpRsvpTeInfo) = RPTE_FALSE;

    RSVPTE_HELLO_INTVL_TIME (pTmpRsvpTeInfo)
        = (HELLO_REFRESH_INTERVAL / NO_OF_MSECS_PER_UNIT);
    RSVPTE_GENLBL_MINLBL (pTmpRsvpTeInfo)
        = gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange;
    RSVPTE_GENLBL_MAXLBL (pTmpRsvpTeInfo)
        = gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange;
    RSVPTE_MAX_IFACES (pTmpRsvpTeInfo)
        = (UINT2) FsRSVPTESizingParams[MAX_RSVPTE_IF_ENTRY_SIZING_ID].
        u4PreAllocatedUnits;
    RSVPTE_MAX_NEIGHBOURS (pTmpRsvpTeInfo)
        = (UINT2) FsRSVPTESizingParams[MAX_RSVPTE_NBR_ENTRY_SIZING_ID].
        u4PreAllocatedUnits;

    RSVPTE_MIN_TNLS_WITH_MSG_ID (pTmpRsvpTeInfo)
        = RSVPTE_MAX_RSVPTETNL (pTmpRsvpTeInfo);

    RSVPTE_ADMIN_STATUS (pTmpRsvpTeInfo) = RPTE_ADMIN_DOWN;
    RSVPTE_DUMP_LVL (pTmpRsvpTeInfo) = RSVPTE_DUMP_LVL_MIN;
    RSVPTE_DUMP_DIR (pTmpRsvpTeInfo) = RSVPTE_DEBUG_NONE;
    RSVPTE_DUMP_TYPE (pTmpRsvpTeInfo) = RSVPTE_DEBUG_NONE;

    RSVPTE_DBG_FLAG = RSVPTE_DEBUG_NONE;

    RSVPTE_DS_OVER_RIDE = TE_DS_OVERRIDE_SET;

    gu1RRCapable = RPTE_DISABLED;
    gu1MsgIdCapable = RPTE_DISABLED;
    gu4RMDFlags = RPTE_ZERO;

    RSVPTE_FRR_CSPF_RETRY_INTERVAL (pTmpRsvpTeInfo)
        = RPTE_FRR_CSPF_RETRY_INTERVAL_DEF;
    RSVPTE_FRR_CSPF_RETRY_COUNT (pTmpRsvpTeInfo)
        = RPTE_FRR_CSPF_RETRY_COUNT_DEF;
    RSVPTE_FRR_GBL_TNL_INSTANCE (pTmpRsvpTeInfo) = RPTE_DETOUR_TNL_INSTANCE;
    RSVPTE_FRR_REVERTIVE_MODE (pTmpRsvpTeInfo) = RPTE_FRR_REVERTIVE_LOCAL;
    RSVPTE_FRR_DETOUR_MERGING_CAPABLE (pTmpRsvpTeInfo) = RPTE_SNMP_TRUE;
    RSVPTE_FRR_DETOUR_CAPABLE (pTmpRsvpTeInfo) = RPTE_SNMP_TRUE;
    RSVPTE_FRR_NOTIFS_ENABLED (pTmpRsvpTeInfo) = RPTE_SNMP_FALSE;
    RSVPTE_FRR_MAKE_AFTER_BREAK_ENABLED (pTmpRsvpTeInfo) = RPTE_SNMP_FALSE;

    /* Intialize "Path State Removed" capability as disable */
    gpRsvpTeGblInfo->RsvpTeCfgParams.u1PathStateRemovedFlag = RPTE_DISABLED;

    gpRsvpTeGblInfo->RsvpTeCfgParams.u4AdminStatusTimerValue =
        RPTE_ADMIN_GRACEFUL_DEL_TIME;

    gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitIntvl =
        RPTE_NOTIFY_RETRY_TIME;

    gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitLimit =
        RPTE_NOTIFY_RETRY_LIMIT;

    /* Initialize Max Erhop per tunnel, to the value set in TE */
    gpRsvpTeGblInfo->RsvpTeCfgParams.u2MaxTempErHops = RSVPTE_TNLERHOP_DEF_VAL;
    /* Initialize GR related parameters */

    gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability = RPTE_GR_CAPABILITY_NONE;

    gpRsvpTeGblInfo->RsvpTeCfgParams.u1RecoveryPathCapability =
        RPTE_GR_RECOVERY_PATH_DEFAULT;

    gpRsvpTeGblInfo->RsvpTeCfgParams.u2RestartTime =
        RPTE_GR_RESTART_DEFAULT_VAL;

    gpRsvpTeGblInfo->RsvpTeCfgParams.u2RecoveryTime =
        RPTE_GR_RECOVERY_DEFAULT_VAL;

    /*Initialize LSP Reoptimization Related Parameters */

    RSVPTE_REOPTIMIZE_INTERVAL (gpRsvpTeGblInfo) = RPTE_REOPT_TIME_DEFAULT_VAL;

    RSVPTE_ERO_CACHE_INTERVAL (gpRsvpTeGblInfo) =
        RPTE_ERO_CACHE_TIME_DEFAULT_VAL;

#ifdef CLI_WANTED
    /* Allocate memory for CSR to support Graceful Restart */
    if (CsrMemAllocation (RSVPTE_MODULE_ID) == CSR_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Err - CSR Memory Alloc  fail.\n");
        return RPTE_FAILURE;
    }
#endif

    /* RBTree */

    gpRsvpTeGblInfo->RpteTnlTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tRsvpTeTnlInfo,
                                             RpteTnlRbNode), RpteTnlRBCmp);

    if (gpRsvpTeGblInfo->RpteTnlTree == NULL)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM,
                    "MAIN: Failed to Create RSVP Tnl RBTree.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateRsvpTeMem : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    gpRsvpTeGblInfo->RpteFrrTnlPortMappingTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tRsvpTeFrrTnlPortMapping,
                                             RpteFrrTnlPortMappingRbNode),
                              RpteFrrTnlPortMappingRBCmp);

    if (gpRsvpTeGblInfo->RpteFrrTnlPortMappingTree == NULL)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM,
                    "MAIN: Failed to Create RSVP Frr Tnl Port Mapping RBTree.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateRsvpTeMem : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (RpteCreateGblRsrc () != RPTE_SUCCESS)
    {
        /* Release the resources */
        RBTreeDestroy (gpRsvpTeGblInfo->RpteTnlTree, NULL, 0);
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Err - RpteCreateGblRsrc  fail.\n");
        return RPTE_FAILURE;
    }
    RSVP_INITIALISED = TRUE;
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteInitRsvpTe : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteTaskInitRsvpTe                                        */
/* Description   : This routine allocates the memory for the main structure  */
/*                 to provide RSVP TE support, and intialises the configurat */
/*                 ion parameters with default values.                       */
/* Input(s)      : None                                                      */
/* Output(s)     : gRsvpTeGblInfo - Pointer to the allocated memory block    */
/*                 in case of successful allocation.                         */
/* Return(s)     : RPTE_SUCCESS in case of successful allocation otherwise   */
/*                 RPTE_FAILURE                                              */
/*****************************************************************************/
UINT1
RpteTaskInitRsvpTe (VOID)
{
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteTaskInitRsvpTe : ENTRY \n");

    /* Allocation for RSVP TE Main data block */
    MEMSET (&gRsvpTeGblInfo, RSVPTE_ZERO, sizeof (tRsvpTeGblInfo));

    if (OsixTskIdSelf (&RSVP_TSK_ID) == OSIX_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                    "MAIN : RsvpTaskId : Getting Task Id Failed\n");
        return RPTE_FAILURE;

    }
    /* Semaphore created for Mutual exclusion on access of common
     * data structure between RSVPTE and other task */

    if ((OsixCreateSem ((const UINT1 *) MPLS_RSVPTE_SEM_NAME, 1, 0,
                        &RSVP_GBL_SEM_ID)) != OSIX_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                    "RpteInitRsvpTe : Semaphore Create Failed EXIT \n");
        return RPTE_FAILURE;
    }

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteTaskInitRsvpTe : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteQueueInitRsvpTe                                       */
/* Description   : This routine creates the message queue required to support*/
/*                 RSVP TE                                                   */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : RPTE_SUCCESS in case of successful creation otherwise     */
/*                 RPTE_FAILURE                                              */
/*****************************************************************************/
UINT1
RpteQueueInitRsvpTe (VOID)
{
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteQueueInitRsvpTe : ENTRY \n");
    /* Create  RSVP Q for Internal processing */

    if ((OsixQueCrt ((UINT1 *) RSVP_QUE, OSIX_MAX_Q_MSG_LEN, RSVP_QDEPTH,
                     (tOsixQId *) & (RSVP_GBL_QID))) != OSIX_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Internal Q Creation Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteQueueInitRsvpTe : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

#ifdef LNXIP4_WANTED
    /* Create Q for RSVP <-> IP Interaction */
    if ((OsixQueCrt ((UINT1 *) IP_RSVP_QUE, OSIX_MAX_Q_MSG_LEN, RSVP_QDEPTH,
                     (tOsixQId *) & (RSVP_GBL_IP_QID))) != OSIX_SUCCESS)

    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "IP-RSVP Q Creation Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteQueueInitRsvpTe : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
#endif

    if ((OsixQueCrt
         ((UINT1 *) ROUTE_CHANGE_NOTIFICATION_QUE, OSIX_MAX_Q_MSG_LEN,
          RSVP_QDEPTH,
          (tOsixQId *) & (RSVP_GBL_RT_CHG_NTF_QID))) != OSIX_SUCCESS)

    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "RouteChange Q Creation Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteQueueInitRsvpTe : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if ((OsixQueCrt ((UINT1 *) IF_CHANGE_NOTIFICATION_QUE, OSIX_MAX_Q_MSG_LEN,
                     RSVP_QDEPTH,
                     (tOsixQId *) & (RSVP_GBL_IF_CHG_NTF_QID))) != OSIX_SUCCESS)

    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "IfChange Q Creation Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteQueueInitRsvpTe : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if ((OsixQueCrt ((UINT1 *) RSVP_GBL_CSPF_QUE, OSIX_MAX_Q_MSG_LEN,
                     RSVP_QDEPTH,
                     (tOsixQId *) & (RSVP_GBL_CSPF_QID))) != OSIX_SUCCESS)

    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "CSPF Q Creation Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteQueueInitRsvpTe : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteQueueInitRsvpTe : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteDeInitRsvpTe                                          */
/* Description   : This routine release all the memory allocated for the     */
/*                 support of RSVP TE Tunnels.                               */
/* Input(s)      : None                                                      */
/*                                                                           */
/* Output(s)     : None. (All memory released pointed by pRsvpTeGblInfo)     */
/* Return(s)     : None. (As memory release is expected to be always success */
/*                 -ful, no value is returned.                               */
/*****************************************************************************/
VOID
RpteDeInitRsvpTe (VOID)
{
    tMsgBuf            *pMsgBuf = NULL;
    tRsvpteRtEntryInfo *pRouteInfo = NULL;
    tRpteCspfInfo      *pRpteCspfInfo = NULL;
    tNotifyRecipient   *pNotifyRecipient = NULL;
    tRpteEnqueueMsgInfo *pRpteEvntInfo = NULL;
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteDeInitRsvpTe : ENTRY \n");

    if (FALSE == RSVP_INITIALISED)
    {
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteDeInitRsvpTe : "
                    "Already deinitialized : INTMD-EXIT \n");
        return;
    }

    RSVP_INITIALISED = FALSE;

    while (OsixQueRecv
           (RSVP_GBL_RT_CHG_NTF_QID, (UINT1 *) (&pRouteInfo),
            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        MemReleaseMemBlock (RPTE_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
        /* Release the Buffer since Admin is Down, only event
         * handled is Admin UP event */
    }
    while (OsixQueRecv
           (RSVP_GBL_IF_CHG_NTF_QID, (UINT1 *) (&pMsgBuf),
            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        BUF_RELEASE (pMsgBuf);
        pMsgBuf = NULL;
    }

    while (OsixQueRecv
           (RSVP_GBL_QID, (UINT1 *) (&pRpteEvntInfo), OSIX_DEF_MSG_LEN,
            OSIX_NO_WAIT) == OSIX_SUCCESS)

    {
        RSVP_RELEASE_MEM_BLOCK (RSVPTE_INTERNAL_EVNT_POOL_ID, pRpteEvntInfo);
        pRpteEvntInfo = NULL;
    }
    /* CSPF Response message with backup path hops */
    while (OsixQueRecv
           (RSVP_GBL_CSPF_QID, (UINT1 *) (&pRpteCspfInfo),
            OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        RSVP_RELEASE_MEM_BLOCK (RPTE_CSPF_POOL_ID, pRpteCspfInfo);
    }

    if (gpRsvpTeGblInfo->NotifyRecipientTree != NULL)
    {
        pNotifyRecipient =
            RBTreeGetFirst (gpRsvpTeGblInfo->NotifyRecipientTree);

        while (pNotifyRecipient != NULL)
        {
            RpteNhDelRBNode (pNotifyRecipient);
            pNotifyRecipient =
                RBTreeGetFirst (gpRsvpTeGblInfo->NotifyRecipientTree);
        }

        RBTreeDestroy (gpRsvpTeGblInfo->NotifyRecipientTree, NULL, 0);
        gpRsvpTeGblInfo->NotifyRecipientTree = NULL;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_UP)
    {
        /* Release the Resources */
        if (RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE)
        {
            /* The Timer used for Socket Polling is stopped */
            if (RSVP_GBL_TIMER_LIST != RSVPTE_ZERO)
            {
                TmrStopTimer (RSVP_GBL_TIMER_LIST,
                              &SOCK_TIMER (gpRsvpTeGblInfo));
            }
        }
        /* Deletion of the RSVP-TE Mempools & RSVP Tables */
        RpteDeleteRsvpTeMem (gpRsvpTeGblInfo);
        /*Closing raw socket if it is open */
        if (RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE)
        {
            if (RpteCloseRawSocket (RSVP_GBL_SOCK_FD) == RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_MAIN_MEM, "Raw Socket Deletion Failed.\n");
            }
        }
    }

    RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) = RPTE_ADMIN_DOWN;

    if (gpRsvpTeGblInfo->RpteTnlTree != NULL)
    {
        RBTreeDestroy (gpRsvpTeGblInfo->RpteTnlTree, NULL, 0);
        gpRsvpTeGblInfo->RpteTnlTree = NULL;
    }

    if (gpRsvpTeGblInfo->RpteFrrTnlPortMappingTree != NULL)
    {
        RBTreeDestroy (gpRsvpTeGblInfo->RpteFrrTnlPortMappingTree, NULL, 0);
        gpRsvpTeGblInfo->RpteFrrTnlPortMappingTree = NULL;
    }

    RpteTcFreeAppResources ();

    RpteDeRegisterWithTc ();

    /* The Timer started for Hello Supprt is stopped */
    if ((RSVPTE_HELLO_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE) &&
        (RSVP_GBL_TIMER_LIST != RSVPTE_ZERO))
    {
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &HELLO_TIMER (gpRsvpTeGblInfo));
    }

    TmrStopTimer (RSVP_GBL_TIMER_LIST, &gpRsvpTeGblInfo->GrTimer);

    /* Delete RSVP Timer Lists that are created */
    if ((RSVP_GBL_TIMER_LIST != RSVPTE_ZERO) &&
        (TmrDeleteTimerList ((tTimerListId) RSVP_GBL_TIMER_LIST)
         != TMR_SUCCESS))
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Timer Deletion Failed.\n");
    }
    RSVP_GBL_TIMER_LIST = 0;
    /* Delete RSVP Iface Hash Table */
    if (RSVP_GBL_IF_HSH_TBL != NULL)
    {
        TMO_HASH_Delete_Table (RSVP_GBL_IF_HSH_TBL, NULL);
        RSVP_GBL_IF_HSH_TBL = NULL;
    }

    /* Delete RSVP Internal Q */
    OsixQueDel (RSVP_GBL_QID);
    RSVP_GBL_QID = RSVPTE_ZERO;

#ifdef LNXIP4_WANTED
    /* Delete RSVP <-> IP Q */
    OsixQueDel (RSVP_GBL_IP_QID);
    RSVP_GBL_IP_QID = RSVPTE_ZERO;
#endif
    /* Delete RSVP Route Change Notification  Q */

    OsixQueDel (RSVP_GBL_RT_CHG_NTF_QID);
    RSVP_GBL_RT_CHG_NTF_QID = RSVPTE_ZERO;

    /* Delete RSVP Route Change Notification  Q */
    OsixQueDel (RSVP_GBL_IF_CHG_NTF_QID);
    RSVP_GBL_IF_CHG_NTF_QID = RSVPTE_ZERO;

    /* Delete RSVP Route Change Notification  Q */
    OsixQueDel (RSVP_GBL_CSPF_QID);
    RSVP_GBL_CSPF_QID = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteDeInitRsvpTe : EXIT \n");
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RpteProcessLspSetupEvent                                */
/* Description     : This function processes LSP Setup event function for    */
/*                    RSVPTE.                                                */
/* Input (s)       : aTaskEnqMsgInfo - info about LSP setup events.          */
/* Output (s)      : None                                                    */
/* Returns         : RPTE_SUCCESS or RPTE_FAILURE                            */
/*****************************************************************************/
UINT1
RpteProcessLspSetupEvent (tRpteEnqueueMsgInfo * pRpteEnqueueMsgInfo)
{
    BOOL1               bIsCSPFRequired = RPTE_FALSE;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = pRpteEnqueueMsgInfo->u.pRsvpTeTnlInfo;
#ifdef MPLS_L3VPN_WANTED
    /*tRsvpTeTnlInfo     *pL3VpnRsvpTeTnlInfo = pRpteEnqueueMsgInfo->u.pRsvpTeTnlInfo; */
    tL3VpnRsvpTeLspEventInfo L3VpnRsvpTeLspEventInfo;

    MEMSET (&L3VpnRsvpTeLspEventInfo, 0, sizeof (L3VpnRsvpTeLspEventInfo));
#endif
    UINT4               u4IngressId = RPTE_ZERO;
    UINT4               u4EgressId = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteProcessLspSetupEvent : ENTRY \n");

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteProcessLspSetupEvent: "
                    " RSVPTE Globally Down : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* If Fast ReRoute Bit is set in the mplsTunnelSessionAttribute object,
     * Set FRR Capable for this tunnel.  */
    if ((RSVPTE_TNL_SSN_ATTR (RPTE_TE_TNL (pRsvpTeTnlInfo)) &
         RSVPTE_SSN_FAST_REROUT_BIT) == RSVPTE_SSN_FAST_REROUT_BIT)
    {
        RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) = RPTE_TRUE;
        RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) = RPTE_FRR_NODE_STATE_HEADEND;
        RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) = RPTE_FRR_PROTECTED_TNL;
    }

    CONVERT_TO_INTEGER (pRsvpTeTnlInfo->pTeTnlInfo->TnlIngressLsrId,
                        u4IngressId);
    CONVERT_TO_INTEGER (pRsvpTeTnlInfo->pTeTnlInfo->TnlEgressLsrId, u4EgressId);

    bIsCSPFRequired = RpteUtlIsCspfComputationReqd (pRsvpTeTnlInfo);

    if (bIsCSPFRequired == RPTE_TRUE)
    {
        /*Check for Full Re-route 1:1 Protection */
        if ((RPTE_TE_TNL_FRR_INFO (pRsvpTeTnlInfo) != NULL) &&
            ((TE_TNL_FRR_CONST_PROT_METHOD (RPTE_TE_TNL_FRR_INFO
                                            (pRsvpTeTnlInfo)) ==
              RSVPTE_TNL_FRR_ONE2ONE_METHOD)))

        {
            RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                        "Cspf Computation Not Required for Fast Re-route 1:1 tunnel \n");
        }
        else
        {

            if (RpteCspfCompForNormalTnl (pRsvpTeTnlInfo) == RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_MAIN_MISC,
                            "MAIN: Err - CSPF constraint formation "
                            "in LspSetUpEvent - Failed.\n");
                RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                            " RpteProcessLspSetupEvent : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }

            RSVPTE_DBG5 (RSVPTE_MAIN_MISC,
                         "CSPF Computation requested for the tunnel %d %d %x %x - "
                         "Current Instance - %d\n",
                         pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                         pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance,
                         OSIX_NTOHL (u4IngressId), OSIX_NTOHL (u4EgressId),
                         pRsvpTeTnlInfo->pTeTnlInfo->u4TnlPrimaryInstance);

            return RPTE_SUCCESS;
        }
    }

    /* Call RptePhSetupTunnel to construct PSB and send Path Msg from Ingress */
    if (RptePhSetupTunnel (pRsvpTeTnlInfo) != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM,
                    "MAIN: Err - PSB Creation in LspSetUpEvent - Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                    " RpteProcessLspSetupEvent : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /*RSVP-TE MPLS-L3VPN-TE: When L3VPN_RSVPTE_TNL_UP is triggerd */
    /* we will notify pRsvpTeTnlInfo tunnel info to L3VPN to be added */

#ifdef MPLS_L3VPN_WANTED
    if (pRsvpTeTnlInfo->pTeTnlInfo != NULL)
    {
        L3VpnRsvpTeLspEventInfo.u4TnlIndex =
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex;
        L3VpnRsvpTeLspEventInfo.u4TnlXcIndex =
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlXcIndex;
        L3VpnRsvpTeLspEventInfo.u4EventType = L3VPN_RSVPTE_TNL_UP;
        L3VpnRsvpTeLspStatusChangeEventHandler (L3VpnRsvpTeLspEventInfo);
    }
#endif

    RSVPTE_DBG (RSVPTE_MAIN_MEM,
                "MAIN: PSB Creation in LspSetUpEvent - Success.\n");

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteProcessLspSetupEvent : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteProcessReoptTnlManualTrigger                          */
/* Description   : This routine is called to trigger Manual Lsp              */
/*                 Reoptimization.                                           */
/* Input(s)      : pRpteEnqueueMsgInfo                                       */
/* Output(s)     : NONE.                                                     */
/* Return(s)     : RPTE_SUCCESS/RPTE_FAILURE.                                */
/*****************************************************************************/
UINT1
RpteProcessReoptTnlManualTrigger (tRpteEnqueueMsgInfo * pRpteEnqueueMsgInfo)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = pRpteEnqueueMsgInfo->u.pRsvpTeTnlInfo;

    RpteTriggerReoptimization (pRsvpTeTnlInfo, RPTE_REOPT_MANUAL_TRIGGER);

    return RPTE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : RpteProcessTeTnlManualReoptEvent                          */
/* Description   : This routine is called to trigger Manual Lsp              */
/*                 Reoptimization.                                           */
/* Input(s)      : pRpteEnqueueMsgInfo                                       */
/* Output(s)     : NONE.                                                     */
/* Return(s)     : RPTE_SUCCESS/RPTE_FAILURE.                                */
/*****************************************************************************/

UINT1
RpteProcessTeTnlReoptManualEvent (tTeTnlInfo * pTeTnlInfo)
{
    tRsvpTeTnlInfo     *pAdnlTnlInfo = NULL;
    UINT4               u4TunnelIndex = RSVPTE_ZERO;
    UINT4               u4TunnelInstance = RSVPTE_ZERO;
    UINT4               u4TunnelIngressLSRId = RSVPTE_ZERO;
    UINT4               u4TunnelEgressLSRId = RSVPTE_ZERO;

    u4TunnelIndex = RSVPTE_TNL_INDEX (pTeTnlInfo);
    u4TunnelInstance = pTeTnlInfo->u4TnlInstance;
    CONVERT_TO_INTEGER (RSVPTE_TNL_INGRESS_RTR_ID (pTeTnlInfo),
                        u4TunnelIngressLSRId);
    u4TunnelIngressLSRId = OSIX_NTOHL (u4TunnelIngressLSRId);
    CONVERT_TO_INTEGER (RSVPTE_TNL_EGRESS_RTR_ID (pTeTnlInfo),
                        u4TunnelEgressLSRId);
    u4TunnelEgressLSRId = OSIX_NTOHL (u4TunnelEgressLSRId);

    if (RpteCheckAdnlTnlInTnlTable (gpRsvpTeGblInfo,
                                    u4TunnelIndex, u4TunnelInstance,
                                    u4TunnelIngressLSRId, u4TunnelEgressLSRId,
                                    &pAdnlTnlInfo) == RPTE_FAILURE)
    {
        return RPTE_FAILURE;
    }

    if (RSVPTE_TNL_ROLE (pAdnlTnlInfo->pTeTnlInfo) == RPTE_INGRESS)
    {
        if (RpteTriggerInternalEvent (pAdnlTnlInfo,
                                      RPTE_TNL_MANUAL_REOPT_EVENT) ==
            RPTE_FAILURE)
        {
            return RPTE_FAILURE;
        }
    }

    if (RPTE_REOPTIMIZE_NODE_STATE (pAdnlTnlInfo) == RPTE_MID_POINT_NODE)
    {
        if (RpteTriggerInternalEvent (pAdnlTnlInfo,
                                      RPTE_TNL_MANUAL_REOPT_EVENT) ==
            RPTE_FAILURE)
        {
            if (rpteTeReleaseTnlInfo (pTeTnlInfo) == RPTE_TE_FAILURE)
            {
                return RPTE_FAILURE;
            }
            return RPTE_FAILURE;
        }
        if (rpteTeReleaseTnlInfo (pTeTnlInfo) == RPTE_TE_FAILURE)
        {
            return RPTE_FAILURE;
        }
    }

    return RPTE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RpteProcessLspDestroyEvent                              */
/* Description     : This function processes LSP destroy event function for  */
/*                   RSVPTE.                                                 */
/* Input (s)       : aTaskEnqMsgInfo - info about LSP destroy events.        */
/* Output (s)      : None                                                    */
/* Returns         : RPTE_SUCCESS or RPTE_FAILURE                            */
/*****************************************************************************/
UINT1
RpteProcessLspDestroyEvent (tRpteEnqueueMsgInfo * pRpteEnqueueMsgInfo)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = pRpteEnqueueMsgInfo->u.pRsvpTeTnlInfo;
#ifdef MPLS_L3VPN_WANTED
    tL3VpnRsvpTeLspEventInfo L3VpnRsvpTeLspEventInfo;

    MEMSET (&L3VpnRsvpTeLspEventInfo, 0, sizeof (L3VpnRsvpTeLspEventInfo));
#endif
    tRsvpTeTnlInfo     *pChkRsvpTeTnlInfo = NULL;
    UINT4               u4TnlInst = RSVPTE_ZERO;
    UINT1               u1TnlRel = RSVPTE_ZERO;
    UINT1               u1RemoveFlag = RPTE_TRUE;

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteProcessLspDestroyEvent : ENTRY \n");

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteProcessLspDestroyEvent: "
                    " RSVPTE Globally Down : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
#ifdef MPLS_L3VPN_WANTED
    if (pRsvpTeTnlInfo->pTeTnlInfo != NULL)
    {
        RSVPTE_DBG2 (RSVPTE_MAIN_ETEXT,
                     "RpteProcessLspDestroyEvent : TnlIndex:%d XcIndex:%d\n",
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlXcIndex);
        L3VpnRsvpTeLspEventInfo.u4TnlIndex =
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex;
        L3VpnRsvpTeLspEventInfo.u4TnlXcIndex =
            pRsvpTeTnlInfo->pTeTnlInfo->u4TnlXcIndex;
        L3VpnRsvpTeLspEventInfo.u4EventType = L3VPN_RSVPTE_TNL_DOWN;
        L3VpnRsvpTeLspStatusChangeEventHandler (L3VpnRsvpTeLspEventInfo);
    }
#endif

    if (pRsvpTeTnlInfo->pTeTnlInfo == NULL)    /*COverity fix */
    {
        return RPTE_FAILURE;
    }

    RptePmRemoveTnlFromPrioList (pRsvpTeTnlInfo);
    u1TnlRel = RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo);
    switch (u1TnlRel)
    {
        case RPTE_INGRESS:
            RptePhGeneratePathTear (pRsvpTeTnlInfo);
            break;

        case RPTE_INTERMEDIATE:
            RptePhGeneratePathTear (pRsvpTeTnlInfo);
            RpteRthSendResvTear (pRsvpTeTnlInfo->pRsb);
            break;

        case RPTE_EGRESS:
            RpteRthSendResvTear (pRsvpTeTnlInfo->pRsb);
            break;
        default:
            break;
    }

    if (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL)
    {
        u1RemoveFlag = RPTE_FALSE;
    }

    /* Checking for the globle revertive behavior tunnel,
     * if present sent path tear for that tunnel also. */
    u4TnlInst = (RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo)) + RPTE_ONE;
    if ((RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,
                                 RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                                 u4TnlInst,
                                 OSIX_NTOHL (RSVPTE_TNL_INGRESS
                                             (pRsvpTeTnlInfo)),
                                 OSIX_NTOHL (RSVPTE_TNL_EGRESS
                                             (pRsvpTeTnlInfo)),
                                 &pChkRsvpTeTnlInfo) == RPTE_SUCCESS)
        && (RPTE_IS_FRR_CAPABLE (pChkRsvpTeTnlInfo) == RPTE_TRUE)
        && (RSVPTE_TNL_GBL_REVERT_FLAG (pChkRsvpTeTnlInfo) == RPTE_TRUE))
    {
        RptePhGeneratePathTear (pChkRsvpTeTnlInfo);
        /* Delete the existing Tnl Info from the Tnl Table */
        RPTE_TNL_DEL_TNL_FLAG (pChkRsvpTeTnlInfo) = RPTE_TRUE;
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pChkRsvpTeTnlInfo,
                             TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
    }
    /* Delete the existing Tnl Info from the Tnl Table */
    RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
    if (RPTE_TE_TNL (pRsvpTeTnlInfo)->u1TnlRelStatus ==
        TE_SIGMOD_TNLREL_AWAITED)
    {
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                             TE_TNL_CALL_FROM_ADMIN, u1RemoveFlag);
    }
    else
    {
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                             TE_TNL_CALL_FROM_SIG, u1RemoveFlag);
    }

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteProcessLspDestroyEvent : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RpteProcessAdminUpEvent                                 */
/* Description     : This function processes Admin up event function for     */
/*                   RSVPTE.                                                 */
/* Input (s)       :                                                        */
/* Output (s)      : None                                                    */
/* Returns         : RPTE_SUCCESS/RPTE_FAILURE                               */
/*****************************************************************************/
UINT1
RpteProcessAdminUpEvent (VOID)
{
    tKeyInfoStruct      LblRangeInfo;
    UINT2               u2LblRngCount = RSVPTE_ONE;

    /* NOTE: Only one set of Lbl Rng Supported for Per platform mode */
    tTimerParam        *pTmrParam = NULL;

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteProcessAdminUpEvent : ENTRY \n");

    if (RpteCreateRsvpTeMem (gpRsvpTeGblInfo) != RPTE_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Err : Failed to Create RsvpTeMem \n");
        RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) = RPTE_ADMIN_DOWN;
        return RPTE_FAILURE;
    }
    else
    {
        /* creating Lbl space for per platform lbl range */
        LblRangeInfo.u4Key1Min = RSVPTE_ZERO;
        LblRangeInfo.u4Key1Max = RSVPTE_ZERO;
        LblRangeInfo.u4Key2Min = RSVPTE_GENLBL_MINLBL (gpRsvpTeGblInfo);
        LblRangeInfo.u4Key2Max = RSVPTE_GENLBL_MAXLBL (gpRsvpTeGblInfo);
        /* Allocating Label Resources for General label space */
        if (RPTE_MPLS_LBLMGR_CREATE_LBLSPACE (RPTE_LBL_ALLOC_BOTH_NUM,
                                              &LblRangeInfo,
                                              u2LblRngCount,
                                              RSVPTE_LBL_MODULE_ID,
                                              PER_PLATFORM_INTERFACE_INDEX,
                                              &RSVPTE_GENLBL_GRPID
                                              (gpRsvpTeGblInfo)) ==
            RPTE_LBL_MNGR_FAILURE)
        {
            RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) = RPTE_ADMIN_DOWN;
            RpteDeleteRsvpTeMem (gpRsvpTeGblInfo);
            RSVPTE_DBG (RSVPTE_MAIN_MEM,
                        "Failed : Gen Lbl space Creation Fail.\n");
            RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                        "RpteProcessAdminUpEvent : INTMD_EXIT \n");
            return RPTE_FAILURE;
        }
        if (RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE)
        {
            RSVP_GBL_SOCK_FD = RpteOpenRawSocket ();
            if (RSVP_GBL_SOCK_FD < RSVPTE_ZERO)
            {
                RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) = RPTE_ADMIN_DOWN;
                RpteDeleteRsvpTeMem (gpRsvpTeGblInfo);
                /* Deleting previously created per platform lbl space */
                RPTE_MPLS_LBLMGR_DELETE_LBLSPACE
                    (RSVPTE_GENLBL_GRPID (gpRsvpTeGblInfo));
                RSVPTE_DBG (RSVPTE_MAIN_MEM, "Raw Socket Creation Failed.\n");
                RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                            "RpteProcessAdminUpEvent : INTMD_EXIT \n");
                return RPTE_FAILURE;
            }

            gu2GenLblSpaceGrpId = RSVPTE_GENLBL_GRPID (gpRsvpTeGblInfo);

            /* started a timer  for socket polling */
            pTmrParam = (tTimerParam *) & SOCK_TIMER_PARAM (gpRsvpTeGblInfo);
            TIMER_PARAM_ID (pTmrParam) = RPTE_SOCK_POLL;
            (&SOCK_TIMER (gpRsvpTeGblInfo))->u4Data = (FS_ULONG) pTmrParam;

            if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST,
                                   &SOCK_TIMER (gpRsvpTeGblInfo),
                                   SOCK_POLL_INTERVAL *
                                   SYS_NUM_OF_TIME_UNITS_IN_A_SEC) !=
                TMR_SUCCESS)
            {
                RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) = RPTE_ADMIN_DOWN;
                RpteDeleteRsvpTeMem (gpRsvpTeGblInfo);
                /* Deleting previously created per platform lbl space */
                RPTE_MPLS_LBLMGR_DELETE_LBLSPACE
                    (RSVPTE_GENLBL_GRPID (gpRsvpTeGblInfo));
                /* Delete the Raw Socket created previously */
                RpteCloseRawSocket (RSVP_GBL_SOCK_FD);
                /* Start Timer failed. */
                RSVPTE_DBG (RSVPTE_MAIN_MEM,
                            "Failed : Tmr Sock Timer failed \n");
                RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                            "RpteProcessAdminUpEvent : INTMD_EXIT \n");
                return RPTE_FAILURE;
            }
        }
        /* Assign the Timer parameters to the Hello timer and start the 
         * timer. The Hello Timer assignments are done only once to increase
         * performance
         */
        if (RSVPTE_HELLO_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE)
        {
            pTmrParam = (tTimerParam *) & HELLO_TIMER_PARAM (gpRsvpTeGblInfo);
            TIMER_PARAM_ID (pTmrParam) = RPTE_HELLO_REFRESH;
            TIMER_PARAM_HELLO (pTmrParam) = (tRsvpTeGblInfo *) gpRsvpTeGblInfo;
            RSVP_TIMER_NAME (&HELLO_TIMER (gpRsvpTeGblInfo))
                = (FS_ULONG) pTmrParam;
            if (RpteHhStartHelloRefreshTmr (gpRsvpTeGblInfo) == RPTE_FAILURE)
            {
                RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) = RPTE_ADMIN_DOWN;
                RpteDeleteRsvpTeMem (gpRsvpTeGblInfo);
                /* Deleting previously created per platform lbl space */
                RPTE_MPLS_LBLMGR_DELETE_LBLSPACE
                    (RSVPTE_GENLBL_GRPID (gpRsvpTeGblInfo));
                /* Delete the Raw Socket created previously */
                if (RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE)
                {
                    RpteCloseRawSocket (RSVP_GBL_SOCK_FD);
                    TmrStopTimer (RSVP_GBL_TIMER_LIST,
                                  &SOCK_TIMER (gpRsvpTeGblInfo));
                }
                /* Start Timer failed. */
                RSVPTE_DBG (RSVPTE_MAIN_MEM,
                            "Failed : Hello Refresh Tmr failed \n");
                RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                            "RpteProcessAdminUpEvent : INTMD_EXIT \n");
                return RPTE_FAILURE;
            }
        }
    }
    gpRsvpTeGblInfo->NotifyRecipientTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tNotifyRecipient,
                                             NotifyMsgTnlRbNode),
                              RpteRecipientRBCmp);

    if (gpRsvpTeGblInfo->NotifyRecipientTree == NULL)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Recipient RB tree creation failed. \n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                    " RpteProcessAdminUpEvent : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    RpteMIHGenerateEpoch ();
    RpteMIHInitMsgId ();

    /* Initialize the global detour tunnel instance value to 65535. */
    RSVPTE_FRR_GBL_TNL_INSTANCE (gpRsvpTeGblInfo) = RPTE_DETOUR_TNL_INSTANCE;
    RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) = RPTE_ADMIN_UP;

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteProcessAdminUpEvent : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RpteProcessAdminDownEvent                               */
/* Description     : This function processes Admin Down event function for   */
/*                   RSVPTE.                                                 */
/* Input (s)       : aTaskEnqMsgInfo - info about LSP setup events.          */
/* Output (s)      : None                                                    */
/* Returns         : RPTE_SUCCESS/RPTE_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
UINT1
RpteProcessAdminDownEvent (VOID)
{
    tNotifyRecipient   *pNotifyRecipient = NULL;

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteProcessAdminDownEvent : ENTRY \n");

    if (RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE)
    {
        /* The Timer used for Socket Polling is stopped */
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &SOCK_TIMER (gpRsvpTeGblInfo));
    }
    if (RSVPTE_HELLO_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE)
    {
        /* The Timer started for Hello Supprt is stopped */
        TmrStopTimer (RSVP_GBL_TIMER_LIST, &HELLO_TIMER (gpRsvpTeGblInfo));
    }

    if (gpRsvpTeGblInfo->NotifyRecipientTree != NULL)
    {
        pNotifyRecipient =
            RBTreeGetFirst (gpRsvpTeGblInfo->NotifyRecipientTree);

        while (pNotifyRecipient != NULL)
        {
            RpteNhDelRBNode (pNotifyRecipient);
            pNotifyRecipient =
                RBTreeGetFirst (gpRsvpTeGblInfo->NotifyRecipientTree);
        }

        RBTreeDestroy (gpRsvpTeGblInfo->NotifyRecipientTree, NULL, 0);
        gpRsvpTeGblInfo->NotifyRecipientTree = NULL;
    }
    gu4MyHelloInstance = DEF_SRC_INSTANCE;

    gpRsvpTeGblInfo->u1GrProgressState = RPTE_ZERO;

    TmrStopTimer (RSVP_GBL_TIMER_LIST, &gpRsvpTeGblInfo->GrTimer);
    RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) = RPTE_ADMIN_DOWN;
    /* Release the allocated Rsvp Te Mem */
    RpteDeleteRsvpTeMem (gpRsvpTeGblInfo);

    RpteTcFreeAppResources ();

    if ((RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE) &&
        (RpteCloseRawSocket (RSVP_GBL_SOCK_FD) == RPTE_FAILURE))
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Raw Socket Deletion Failed.\n");
    }
    gu1TeAdminFlag = RPTE_FALSE;
    rpteTeSigAdminStatus (RPTE_PROT, TE_ADMIN_DOWN);
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteProcessAdminDownEvent : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RpteProcessTeGoingDownEvent                             */
/* Description     : This function processes Admin up event function for     */
/*                   RSVPTE.                                                 */
/* Input (s)       : None                                                    */
/* Output (s)      : None                                                    */
/* Returns         : RPTE_SUCCESS/RPTE_FAILURE                               */
/*****************************************************************************/
UINT1
RpteProcessTeGoingDownEvent ()
{
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteProcessTeGoingDownEvent : ENTRY\n");
    RpteUtlDeleteTnlTbl ();
    gu1TeAdminFlag = RPTE_FALSE;
    rpteTeSigAdminStatus (RPTE_PROT, TE_ADMIN_DOWN);
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, "RpteProcessTeGoingDownEvent : EXIT \n");
    return RPTE_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function Name   : RpteRsvpTaskMain                                        */
/* Description     : This is the main function for the RSVP task. It         */
/*                   intializes the Rsvp Task and waits for the external     */
/*                   events and takes appropriate action                     */
/* Input (s)       : None                                                    */
/* Output (s)      : None                                                    */
/* Returns         : None                                                    */
/*                                                                           */
/*****************************************************************************/
VOID
RpteRsvpTaskMain (INT1 *pDummy)
{
    tMsgBuf            *pMsgBuf = NULL;
    tRsvpteRtEntryInfo *pRouteInfo = NULL;
    tRsvpIfMsg          RsvpIfMsg;
    UINT4               u4Events;
    tRpteCspfInfo      *pRpteCspfInfo = NULL;
    tRpteEnqueueMsgInfo *pRpteEnqueueMsgInfo = NULL;

    /* Dummy pointer for system sizing */
    tRpteDefPktSize    *pRpteDefPktSize = NULL;

    UNUSED_PARAM (pRpteDefPktSize);
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteRsvpTaskMain : ENTRY \n");

#ifdef SNMP_2_WANTED
    RegisterFSRSVP ();
    RegisterFSMPFR ();
    RegisterFSMPLSRsvpteObj ();
#endif

    UNUSED_PARAM (pDummy);

    if (RpteTaskInitRsvpTe () != RPTE_SUCCESS)
    {
        RpteDeInitRsvpTe ();
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Err - RpteTaskInitRsvpTe fail.\n");
        return;
    }

    if (RpteInitRsvpTe () != RPTE_SUCCESS)
    {
        RpteDeInitRsvpTe ();
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Err - RpteInitRsvpTe fail.\n");
        return;
    }

    MPLS_INIT_COMPLETE (OSIX_SUCCESS);
    IssSetModuleSystemControl (RSVPTE_MODULE_ID, MODULE_START);

    while (RSVPTE_ONE)
    {
        if (OsixEvtRecv (RSVP_TSK_ID, (IP_PKT_RXED_EVENT |
                                       UDP_PKT_RXED_EVENT |
                                       CSPF_RESPONSE_RXED_EVENT |
                                       ROUTE_CHANGE_NOTIFICATION |
                                       IF_CHANGE_NOTIFICATION |
                                       RSVP_TIMER_EXPIRED_EVENT |
                                       RPTE_INTERNAL_EVENT),
                         OSIX_WAIT, (UINT4 *) &(u4Events)) != OSIX_SUCCESS)
        {
            /* Event Reception Failed.  */
            continue;
        }

        MPLS_RSVPTE_LOCK ();

        if (RSVP_INITIALISED != TRUE)
        {
            MPLS_RSVPTE_UNLOCK ();
            continue;
        }

        /* RsvpTe Admin Status Disabled */
        if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
        {
            if ((u4Events & ROUTE_CHANGE_NOTIFICATION) ==
                ROUTE_CHANGE_NOTIFICATION)
            {
                while (OsixQueRecv
                       (RSVP_GBL_RT_CHG_NTF_QID, (UINT1 *) (&pRouteInfo),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    MemReleaseMemBlock (RPTE_IP_RT_POOL_ID,
                                        (UINT1 *) pRouteInfo);
                    /* Release the Buffer since Admin is Down, only event
                     * handled is Admin UP event */
                }
            }
            if ((u4Events & RPTE_INTERNAL_EVENT) == RPTE_INTERNAL_EVENT)
            {
                while (OsixQueRecv
                       (RSVP_GBL_QID, (UINT1 *) (&pRpteEnqueueMsgInfo),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)

                {
                    if (pRpteEnqueueMsgInfo != NULL)
                    {
                        /* Processing the Msgs that are EnQ */
                        RPTE_PROC_EVT_FUNC
                            [pRpteEnqueueMsgInfo->u4TeTnlEvent]
                            (pRpteEnqueueMsgInfo);
                        /* Message contents copied hence released */
                        RSVP_RELEASE_MEM_BLOCK (RSVPTE_INTERNAL_EVNT_POOL_ID,
                                                pRpteEnqueueMsgInfo);
                        pRpteEnqueueMsgInfo = NULL;
                    }
                }
            }
            if ((u4Events & CSPF_RESPONSE_RXED_EVENT) ==
                CSPF_RESPONSE_RXED_EVENT)
            {
                /* CSPF Response message with backup path hops */
                while (OsixQueRecv
                       (RSVP_GBL_CSPF_QID, (UINT1 *) (&pRpteCspfInfo),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
                {
                    RSVP_RELEASE_MEM_BLOCK (RPTE_CSPF_POOL_ID, pRpteCspfInfo);
                }
            }
            MPLS_RSVPTE_UNLOCK ();
            continue;
        }

        if ((u4Events & RSVP_TIMER_EXPIRED_EVENT) == RSVP_TIMER_EXPIRED_EVENT)
        {
            RpteInitProcessTimeOut ();
        }

        if ((u4Events & ROUTE_CHANGE_NOTIFICATION) == ROUTE_CHANGE_NOTIFICATION)
        {
            while (OsixQueRecv
                   (RSVP_GBL_RT_CHG_NTF_QID, (UINT1 *) (&pRouteInfo),
                    OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)

            {
                RptePhHandleRouteChange (pRouteInfo);
                MemReleaseMemBlock (RPTE_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
            }
        }

        if ((u4Events & IF_CHANGE_NOTIFICATION) == IF_CHANGE_NOTIFICATION)
        {
            while (OsixQueRecv
                   (RSVP_GBL_IF_CHG_NTF_QID, (UINT1 *) (&pMsgBuf),
                    OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
            {
                if (BUF_COPY_FROM_CHAIN (pMsgBuf,
                                         (UINT1 *) &RsvpIfMsg,
                                         RSVP_DEF_OFFSET,
                                         sizeof (tRsvpIfMsg))
                    != sizeof (tRsvpIfMsg))
                {
                    /*
                     * Buffer not required and hence released.
                     */
                    BUF_RELEASE (pMsgBuf);
                    RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                "Error while copying From Buffer Chain\n");
                    continue;
                }
                /*
                 * Buffer contents copied and hence relesed.
                 */
                BUF_RELEASE (pMsgBuf);
                RptePhHandleIfChange (&RsvpIfMsg);
            }
        }

        if ((u4Events & IP_PKT_RXED_EVENT) == IP_PKT_RXED_EVENT)
        {
            RsvpProcessRawIpMsg ();
        }
        if ((u4Events & UDP_PKT_RXED_EVENT) == UDP_PKT_RXED_EVENT)
        {
            RsvpProcessUdpMsg ();
        }
        if ((u4Events & CSPF_RESPONSE_RXED_EVENT) == CSPF_RESPONSE_RXED_EVENT)
        {
            /* CSPF Response message with backup path hops */
            while (OsixQueRecv (RSVP_GBL_CSPF_QID, (UINT1 *) (&pRpteCspfInfo),
                                OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
            {
                RsvpProcessCspfMsg (pRpteCspfInfo);
                RSVP_RELEASE_MEM_BLOCK (RPTE_CSPF_POOL_ID, pRpteCspfInfo);
            }
        }

        if ((u4Events & RPTE_INTERNAL_EVENT) == RPTE_INTERNAL_EVENT)
        {
            while (OsixQueRecv (RSVP_GBL_QID, (UINT1 *) (&pRpteEnqueueMsgInfo),
                                OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
            {
                if (pRpteEnqueueMsgInfo != NULL)
                {
                    if (pRpteEnqueueMsgInfo->u4TeTnlEvent ==
                        RPTE_TNL_L3VPN_EVENT)
                    {
                        RpteProcessL3VPNEvent (pRpteEnqueueMsgInfo);
                    }

                    else if (pRpteEnqueueMsgInfo->u4TeTnlEvent ==
                             RPTE_TNL_L2VPN_EVENT)
                    {
                        RpteProcessL2VPNEvent (pRpteEnqueueMsgInfo);
                    }
                    else
                    {
                        /* Processing the Msgs that are EnQ */
                        RPTE_PROC_EVT_FUNC[pRpteEnqueueMsgInfo->u4TeTnlEvent]
                            (pRpteEnqueueMsgInfo);
                    }
                    /* Message contents copied hence released */
                    RSVP_RELEASE_MEM_BLOCK (RSVPTE_INTERNAL_EVNT_POOL_ID,
                                            pRpteEnqueueMsgInfo);
                    pRpteEnqueueMsgInfo = NULL;
                }
            }
        }
        MPLS_RSVPTE_UNLOCK ();
    }                            /* end of while (1)  */
}

/****************************************************************************/
/* Function Name   : RpteInitIfEntry                                        */
/* Description     : This function initalises an If table entry             */
/* Input (s)       : pIfEntry                                               */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteInitIfEntry (tIfEntry * pIfEntry)
{
    UINT1               u1Range = RSVPTE_ZERO;
    UINT1               u1PscIndex = RSVPTE_ZERO;
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteInitIfEntry : ENTRY \n");
    MEMSET (pIfEntry, RSVPTE_ZERO, sizeof (tIfEntry));
    TMO_SLL_Init (&IF_ENTRY_NBR_LIST (pIfEntry));
    TMO_DLL_Init (&IF_ENTRY_FAC_TNL_LIST (pIfEntry));
    for (u1Range = RSVPTE_ZERO; u1Range < RPTE_HOLD_PRIO_RANGE; u1Range++)
    {
        TMO_SLL_Init (&(pIfEntry->aRpteDnStrPrioTnlList[u1Range]));
    }
    for (u1Range = RSVPTE_ZERO; u1Range < RPTE_HOLD_PRIO_RANGE; u1Range++)
    {
        TMO_SLL_Init (&(pIfEntry->aRpteUpStrPrioTnlList[u1Range]));
    }
    IF_ENTRY_ENABLED (pIfEntry) = RPTE_DISABLED;
    IF_ENTRY_IP_NBRS (pIfEntry) = RSVPTE_ZERO;
    IF_ENTRY_NBRS (pIfEntry) = RSVPTE_ZERO;
    IF_ENTRY_UDP_NBRS (pIfEntry) = RSVPTE_ZERO;
    IF_ENTRY_UDP_REQUIRED (pIfEntry) = RPTE_SNMP_FALSE;
    IF_ENTRY_REFRESH_MULTIPLE (pIfEntry) = DEFAULT_REFRESH_MULTIPLE;
    IF_ENTRY_TTL (pIfEntry) = DEFAULT_TTL;
    IF_ENTRY_REFRESH_INTERVAL (pIfEntry) = DEFAULT_REFRESH_INTERVAL;
    IF_ENTRY_ROUTE_DELAY (pIfEntry) = DEFAULT_ROUTE_DELAY;
    IF_NUM (IF_ENTRY_IF_ID (pIfEntry)) = 0xFF;
    IF_ENTRY_STATUS (pIfEntry) = DESTROY;
    IF_ENTRY_REFRESH_BLOCKADE_MULTIPLE (pIfEntry) =
        DEFAULT_REFRESH_BLOCK_MULTIPLE;
    RPTE_IF_LBL_SPACE (pIfEntry) = RPTE_PERPLATFORM;
    RPTE_IF_LBL_TYPE (pIfEntry) = RPTE_ETHERNET;
    RPTE_IF_LBL_FLAGS (pIfEntry) = RSVPTE_ZERO;
    RPTE_IF_MINVPI (pIfEntry) = RPTE_MINVPI_DEFVAL;
    RPTE_IF_MINVCI (pIfEntry) = RPTE_MINVCI_DEFVAL;
    RPTE_IF_MAXVPI (pIfEntry) = RPTE_MAXVPI_DEFVAL;
    RPTE_IF_MAXVCI (pIfEntry) = RPTE_MAXVCI_DEFVAL;
    RPTE_IF_LBLSPCID (pIfEntry) = 0;
    RPTE_IF_MAX_MTU (pIfEntry) = RPTE_IF_MIN_MTU_SIZE;
    RPTE_IF_LINK_ATTR_ASSOC (pIfEntry) = RPTE_IF_LINK_ATTR_DEF;
    RPTE_IF_ATM_MERGE_CAP (pIfEntry) = RPTE_NO_MRG;
    RPTE_IF_ATM_VC_DIR (pIfEntry) = RPTE_VC_BI_DIR;
    if ((gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability ==
         RPTE_GR_CAPABILITY_FULL)
        || (gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability ==
            RPTE_GR_CAPABILITY_HELPER))
    {
        IF_ENTRY_HELLO_SPRTD (pIfEntry) = HELLO_SUPPRT;
    }
    else
    {
        IF_ENTRY_HELLO_SPRTD (pIfEntry) = HELLO_NOT_SUPPRT;
    }
    for (u1PscIndex = RSVPTE_ZERO; u1PscIndex < RPTE_DS_MAX_NO_OF_PSCS;
         u1PscIndex++)
    {
        TMO_SLL_Init (&(RSVPTE_HOLD_PSC_OF_TNLS_LIST_ARRAY
                        (pIfEntry, u1PscIndex)));
    }

    MEMSET (&(IF_ENTRY_STATS_INFO (pIfEntry)), RPTE_ZERO, sizeof (tIfStatInfo));
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteInitIfEntry : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteCreateGblRsrc                                      */
/* Description     : This function initializes all the global variables     */
/*                   and allocates memory for the timer lists and Queues    */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : If successful, returns RPTE_SUCCESS else               */
/*                   returns RPTE_FAILURE                                   */
/****************************************************************************/
UINT1
RpteCreateGblRsrc (VOID)
{
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateGblRsrc : ENTRY \n");

    GBL_IF_ENTRY_OFFSET_ELMNT (RPTE_ZERO) =
        (UINT2) RPTE_OFFSET (tIfEntry, u4IfIndex);
    GBL_IF_ENTRY_LENGTH_ELMNT (RPTE_ZERO) =
        (UINT2) RPTE_MEMBER_LENGTH (tIfEntry, u4IfIndex);
    GBL_IF_ENTRY_BYTE_ORDER_ELMNT (RPTE_ZERO) = HOST_ORDER;
    GBL_IF_ENTRY_NODE_OFFSET = (UINT2) RPTE_OFFSET (tIfEntry, NextIfHashNode);

    if (RpteRegisterWithTc () == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Registration with TC Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateGblRsrc : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (TmrCreateTimerList ((const UINT1 *) RSVP_TASK_NAME,
                            RSVP_TIMER_EXPIRED_EVENT,
                            NULL, (tTimerListId *) & (RSVP_GBL_TIMER_LIST))
        != TMR_SUCCESS)
    {

        RSVPTE_DBG (RSVPTE_MAIN_MEM, "Timer Creation Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateGblRsrc : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    RSVP_GBL_IF_HSH_TBL = TMO_HASH_Create_Table (RSVPTE_HASH_SIZE, NULL,
                                                 RPTE_ZERO);

    if (RSVP_GBL_IF_HSH_TBL == NULL)
    {
        RSVPTE_DBG (RSVPTE_MAIN_MEM, "If Hash Table Creation Failed.\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateGblRsrc : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCreateGblRsrc : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteCleanIfTable                                       */
/* Description     : This function deletes off all the Label spaces created */
/*                   for each Interface Entry. Then it deletes all the      */
/*                   Interface Entries present in the Interface Hash        */
/*                   Table, releasing the Interface Entries to the mempool  */
/*                   in the process. Then finally the Interface Entry       */
/*                   mempool is deleted.                                    */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteCleanIfTable (VOID)
{
    UINT4               u4HashIndex;
    tIfEntry           *pIfEntry = NULL;

    TMO_HASH_Scan_Table (RSVP_GBL_IF_HSH_TBL, u4HashIndex)
    {
        pIfEntry = (tIfEntry *) TMO_HASH_Get_First_Bucket_Node
            (RSVP_GBL_IF_HSH_TBL, u4HashIndex);
        while (pIfEntry != NULL)
        {
            if ((IF_ENTRY_STATUS (pIfEntry) == RPTE_ACTIVE) &&
                (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM))
            {
                if (RPTE_IF_ENTRY_LBL_SPACE_ID (pIfEntry) != RSVPTE_ZERO)
                {
                    RPTE_MPLS_LBLMGR_DELETE_LBLSPACE
                        (RPTE_IF_ENTRY_LBL_SPACE_ID (pIfEntry));
                }
            }

            /* Deleting node from the bucket */
            TMO_HASH_Delete_Node (RSVP_GBL_IF_HSH_TBL,
                                  &(pIfEntry->NextIfHashNode), u4HashIndex);
            pIfEntry = (tIfEntry *) TMO_HASH_Get_First_Bucket_Node
                (RSVP_GBL_IF_HSH_TBL, u4HashIndex);
        }
    }
    /* do not remove label space when the node is restarted with GR capability */
    if (gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_SHUT_DOWN_IN_PROGRESS)
    {
        return;
    }
    /* Deleting per platform lbl space */
    if (RSVPTE_GENLBL_GRPID (gpRsvpTeGblInfo) != RSVPTE_ZERO)
    {

        RPTE_MPLS_LBLMGR_DELETE_LBLSPACE (RSVPTE_GENLBL_GRPID
                                          (gpRsvpTeGblInfo));
        RSVPTE_GENLBL_GRPID (gpRsvpTeGblInfo) = RSVPTE_ZERO;
        gu2GenLblSpaceGrpId = RSVPTE_ZERO;
    }
    return;
}

/****************************************************************************/
/* Function Name   : RpteInitProcessTimeOut                                 */
/* Description     : This function processes the timeout event. Depending on*/
/*                   id of expired timer, it will call appropriate functions*/
/*                   to process the timeout                                 */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteInitProcessTimeOut (VOID)
{
    tTimerParam        *pTmrParam = NULL;
    tRSVP_TIMER        *pExpiredTmr = NULL;
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;
    MEMSET (&RpteEnqueueMsgInfo, RPTE_ZERO, sizeof (tRpteEnqueueMsgInfo));
    /*    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteInitProcessTimeOut : ENTRY \n"); */
    if (RSVP_INITIALISED != TRUE)
    {
        return;
    }
    pExpiredTmr = TmrGetNextExpiredTimer (RSVP_GBL_TIMER_LIST);
    while (pExpiredTmr != NULL)
    {

        pTmrParam = (tTimerParam *) RSVP_TIMER_NAME (pExpiredTmr);
        /*
         * This is needed because while processing the expired timer
         * it may be again linked to the timer list. In that case the 
         * the Next Timer info will be changed. 
         */
        switch (TIMER_PARAM_ID (pTmrParam))
        {
            case RPTE_PATH_REFRESH:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "Psb Refreshtimed out \n");
                RptePhProcessPsbRefreshTimeOut (pExpiredTmr);
                break;

            case RPTE_RESV_REFRESH:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "Rsb Refreshtimed out \n");
                RpteRhProcessRsbRefreshTimeOut (pExpiredTmr);
                break;

            case RPTE_ROUTE_DELAY:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "RouteDelayTimedOut \n");
                RptePhProcessRouteDelayTimeOut (pExpiredTmr);
                break;

            case RPTE_PSB_SNMP_WAIT:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "PSB_SNMP Timed out \n");
                RptePhProcessSnmpTimeOut (pExpiredTmr);
                break;

            case RPTE_PH_RRO_BOFF_TIMER:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "RRO Err Back off Timed out \n");
                RptePhProcRROBackoffTimeOut (pExpiredTmr);
                break;

            case RPTE_RH_RRO_BOFF_TIMER:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "RRO Err Back off Timed out \n");
                RpteRhProcRROBackoffTimeOut (pExpiredTmr);
                break;

            case RPTE_MAX_WAIT_TIMER:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "Maximum WaitTimer Timed out \n");
                RptePhRhProcessWaitTimeOut (pExpiredTmr);
                break;

            case RPTE_HELLO_REFRESH:
                /*                RSVPTE_DBG (RSVPTE_MAIN_MISC, "Hello Refresh timed out \n"); */
                RpteHhProcessWaitTimeOut (pExpiredTmr);
                break;

            case RPTE_EVENT_TO_L2VPN:
                RpteProcessEventToL2vpnTimeOut (TIMER_PARAM_RPTE_TNL
                                                (pTmrParam));
                break;
            case RPTE_PATHERR:
            case RPTE_RESVERR:
            case RPTE_PATHTEAR:
            case RPTE_RESVTEAR:
            case RPTE_GR_RECOVERY_PATH:
                RSVPTE_DBG (RSVPTE_MAIN_MISC,
                            "Reliable Message Delivery Timer timed out\n");
                RpteMIHProcessRMDTimeOut (pExpiredTmr);
                break;

            case RPTE_SREFRESH:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "SRefresh Timer Timed Out\n");
                RpteRRProcessSRefreshTimeOut (pExpiredTmr);
                break;
            case RPTE_CSPF_INTERVAL:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "SRefresh Timer Timed Out\n");
                RpteCspfCompForBkpOrProtTnl (TIMER_PARAM_RPTE_TNL (pTmrParam));
                break;
            case RPTE_MAX_GBL_REVERT_TIMER:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "SRefresh Timer Timed Out\n");
                RpteFrrGblRevertiveOptPathTrigger (TIMER_PARAM_RPTE_TNL
                                                   (pTmrParam));
                break;
            case RPTE_GRACEFUL_DEL_EXPIRE:
                RSVPTE_DBG (RSVPTE_MAIN_MISC,
                            "Graceful Deletion Timer Timed Out\n");
                /* calling RpteProcessLspDestroyEvent function deleting the Tunnel */
                if (((pTmrParam->u.pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                      u4AdminStatus) & GMPLS_ADMIN_DELETE_IN_PROGRESS) ||
                    ((pTmrParam->u.pRsvpTeTnlInfo->pRsb->u4AdminStatus) &
                     GMPLS_ADMIN_DELETE_IN_PROGRESS))
                {
                    /*Changing it to Original Value */
                    pTmrParam->u.pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                        u4AdminStatus &=
                        (UINT4) (~GMPLS_ADMIN_DELETE_IN_PROGRESS);
                    pTmrParam->u.pRsvpTeTnlInfo->pRsb->u4AdminStatus &=
                        (UINT4) (~GMPLS_ADMIN_DELETE_IN_PROGRESS);
                    RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo =
                        pTmrParam->u.pRsvpTeTnlInfo;
                    RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
                }
                break;
            case RPTE_NOTIFY_GROUPING_TMR_EXPIRE:
                RSVPTE_DBG (RSVPTE_MAIN_MISC,
                            "Notify grouping Timer Timed Out\n");
                RpteNhSendNotifyMsg (pTmrParam->u.pNotifyRecipient);
                break;
            case RPTE_NOTIFY_RETRY_TMR_EXPIRE:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "Notify retry Timer Timed Out\n");
                RpteNhProcessRetryTimeOut (pTmrParam->u.pNotifyRecipient);
                break;
            case RPTE_GR_NBR_RESTART_TIMER:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "Nbr Restart time timed out\n");
                RpteHlprProcessRestartTimeOut (pTmrParam->u.pNbrEntry);
                break;
            case RPTE_GR_NBR_RECOVERY_TIMER:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "Nbr Restart time timed out\n");
                RpteHlprProcessRecoveryTimeOut (pTmrParam->u.pNbrEntry);
                break;
            case RPTE_GR_RECOVERY_TIMER:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "Nbr Restart time timed out\n");
                RpteGrProcessRecoveryTimeOut (pTmrParam->u.pRsvpTeGblInfo);
                break;
            case RPTE_GR_RECOVERY_PATH_REFRESH:
                RSVPTE_DBG (RSVPTE_MAIN_MISC, "Recovery Path time timed out\n");
                RpteHlprGenerateRecoveryPath (pTmrParam->u.pRsvpTeTnlInfo);
                break;
            case RPTE_REOPTIMIZE_INTERVAL:
                RSVPTE_DBG (RSVPTE_REOPT_DBG, "Reoptimize time timed out\n");
                RpteReoptimizeTimeOut (TIMER_PARAM_RPTE_TNL (pTmrParam));
                break;
            case RPTE_ERO_CACHE_INTERVAL:
                RSVPTE_DBG (RSVPTE_REOPT_DBG, "Ero Cache time timed out\n");
                RpteEroCacheTimeOut (TIMER_PARAM_RPTE_TNL (pTmrParam));
                break;
            case RPTE_LINK_UP_WAIT_INTERVAL:
                RSVPTE_DBG (RSVPTE_REOPT_DBG, "Link up wait time timed out\n");
                RpteLinkUpWaitTimeOut (TIMER_PARAM_RPTE_TNL (pTmrParam));
                break;
            default:
                break;
        }
        pExpiredTmr = TmrGetNextExpiredTimer (RSVP_GBL_TIMER_LIST);
    }
    /*   RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteInitProcessTimeOut : EXIT \n"); */
    return;
}

/****************************************************************************/
/* Function Name   : RpteOpenUdpInterface                                   */
/* Description     : This function open the UDP read and Write Ports        */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : If successful, returns RPTE_SUCCESS else               */
/*                   returns RPTE_FAILURE                                   */
/****************************************************************************/
INT1
RpteOpenUdpInterface (VOID)
{
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteOpenUdpInterface : ENTRY \n");
    if (RpteUdpOpen (UDP_READ_PORT, RSVPTE_ZERO) == RPTE_FAILURE)
    {

        RpteUdpClose (UDP_READ_PORT);
        RSVPTE_DBG (RSVPTE_MAIN_MISC, "-E- : Opening  Port Failed\n");
        RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteOpenUdpInterface : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteOpenUdpInterface : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteCloseUdpInterface                                  */
/* Description     : This function open the UDP red and Write Ports         */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteCloseUdpInterface (VOID)
{
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCloseUdpInterface : ENTRY \n");
    RpteUdpClose (UDP_READ_PORT);
    RSVPTE_DBG (RSVPTE_MAIN_ETEXT, " RpteCloseUdpInterface : EXIT \n");
    return;
}

/*****************************************************************************/
/* Function Name : RsvpIfStChgEventHandler                                    */
/* Description   : This routine is called by If module, for informing the    */
/*                 the State changes.                                        */
/*                 Corresponding event is posted to LDP task along           */
/*                 with necessary parameters.                                */
/* Input(s)      : u4IfIndex - Interface If Index                            */
/*                 u4IpAddr  - IP Address of the Interface                   */
/*                 u1OperState - Interface Status                            */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
RsvpIfStChgEventHandler (UINT4 u4IfIndex, UINT4 u4IpAddr, UINT1 u1OperState,
                         INT4 i4DataTeIfIndex)
{
    tMsgBuf            *pMsgBuf = NULL;
    tRsvpIfMsg          RsvpIfMsg;

    if (RSVP_INITIALISED != TRUE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "Err : RSVPTE is not initialised\n");
        return;
    }
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "Err : RSVPTE Admin status is not UP\n");
        return;
    }
    MEMSET (&RsvpIfMsg, 0, sizeof (tRsvpIfMsg));

    RsvpIfMsg.u4IfIndex = u4IfIndex;
    RsvpIfMsg.u4DataTeIfIndex = (UINT4) i4DataTeIfIndex;
    RsvpIfMsg.u4Addr = u4IpAddr;

    if (u1OperState == CFA_IF_UP)
    {
        RsvpIfMsg.u4Status = RPTE_ENABLED;
    }
    else
    {
        RsvpIfMsg.u4Status = RPTE_DISABLED;
    }
    pMsgBuf = BUF_ALLOC (sizeof (tRsvpIfMsg), RSVPTE_ZERO);
    if (pMsgBuf == NULL)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to Alloc Msg for If change Msg to RSVP-TE Q\n");
        return;
    }

    if (BUF_COPY_OVER_CHAIN (pMsgBuf, (UINT1 *) &RsvpIfMsg, RSVPTE_ZERO,
                             sizeof (tRsvpIfMsg)) != CRU_SUCCESS)
    {
        BUF_RELEASE (pMsgBuf);
        return;
    }
    if (OsixQueSend (RSVP_GBL_IF_CHG_NTF_QID,
                     (UINT1 *) (&pMsgBuf), OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)

    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to EnQ If change Msg to RSVP-TE Q\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RsvpIfStChgEventHandler : INTMD-EXIT \n");
        BUF_RELEASE (pMsgBuf);
        return;
    }

    if (OsixEvtSend (RSVP_TSK_ID, IF_CHANGE_NOTIFICATION) != OSIX_SUCCESS)

    {
        RSVPTE_DBG (RSVPTE_IF_PRCS,
                    "Err : Failed to send If change event to RSVP-TE\n");
        RSVPTE_DBG (RSVPTE_PORT_ETEXT,
                    "RsvpIfStChgEventHandler : INTMD-EXIT \n");
        return;
    }
    RSVPTE_DBG (RSVPTE_PORT_ETEXT, "RsvpIfStChgEventHandler : EXIT \n");
    return;
}

/************************************************************************
 *  Function Name   : RsvpTeLock 
 *  Description     : Function used in SNMP context to lock the RSVPTE
 *                    module before accessing RSVPTE SNMP APIs 
 *                    (nmh routines)
 *  Input           : None
 *  Output          : None
 *  Returns         : SNMP_SUCCESS on Success otherwise SNMP_FAILURE 
 ************************************************************************/
INT4
RsvpTeLock (VOID)
{
    if (OsixSemTake (RSVP_GBL_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : RsvpTeUnLock 
 *  Description     : Function used in SNMP context to unlock the RsvpTe 
 *                    module after accessing RsvpTe SNMP APIs 
 *                    (nmh routines)
 *  Input           : None
 *  Output          : None
 *  Returns         : SNMP_SUCCESS on Success otherwise SNMP_FAILURE 
 ************************************************************************/

INT4
RsvpTeUnLock (VOID)
{
    if (OsixSemGive (RSVP_GBL_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/***************************************************************************
 * FUNCTION NAME    : RpteRecipientRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the Notify
 *                    Recipient table.
 *                    Indices of this table are - u4RecipientAddr,
 *                    u4MsgId
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : RPTE_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    RPTE_RB_LESSER    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    RPTE_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
RpteRecipientRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tNotifyRecipient   *pNotifyRecipientA = NULL;
    tNotifyRecipient   *pNotifyRecipientB = NULL;

    pNotifyRecipientA = (tNotifyRecipient *) pRBElem1;
    pNotifyRecipientB = (tNotifyRecipient *) pRBElem2;

    if (pNotifyRecipientA->u4RecipientAddr < pNotifyRecipientB->u4RecipientAddr)
    {
        return RPTE_RB_LESSER;
    }
    else if (pNotifyRecipientA->u4RecipientAddr >
             pNotifyRecipientB->u4RecipientAddr)
    {
        return RPTE_RB_GREATER;
    }

    if (pNotifyRecipientA->u1ErrorCode < pNotifyRecipientB->u1ErrorCode)
    {
        return RPTE_RB_LESSER;
    }
    else if (pNotifyRecipientA->u1ErrorCode > pNotifyRecipientB->u1ErrorCode)
    {
        return RPTE_RB_GREATER;
    }

    if (pNotifyRecipientA->u2ErrorValue < pNotifyRecipientB->u2ErrorValue)
    {
        return RPTE_RB_LESSER;
    }
    else if (pNotifyRecipientA->u2ErrorValue > pNotifyRecipientB->u2ErrorValue)
    {
        return RPTE_RB_GREATER;
    }

    if (pNotifyRecipientA->u4MsgId < pNotifyRecipientB->u4MsgId)
    {
        return RPTE_RB_LESSER;
    }
    else if (pNotifyRecipientA->u4MsgId > pNotifyRecipientB->u4MsgId)
    {
        return RPTE_RB_GREATER;
    }

    return RPTE_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : RpteTnlRBCmp
 *
 * DESCRIPTION      : This is the RB Tree compare function for the RSVPTE
 *                    Tunnel table.
 *                    Indices of this table are - u4TnlIndex, u4TnlInstance
 *                    IngressId and EgressId
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : RPTE_RB_EQUAL   - if all the keys matched for both
 *                                       the nodes
 *                    RPTE_RB_LESS    - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    RPTE_RB_GREATER - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
RpteTnlRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    INT4                i4Return = RPTE_EQUAL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfoA = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfoB = NULL;

    pRsvpTeTnlInfoA = (tRsvpTeTnlInfo *) pRBElem1;
    pRsvpTeTnlInfoB = (tRsvpTeTnlInfo *) pRBElem2;

    if (pRsvpTeTnlInfoA->pTeTnlInfo->u4TnlIndex >
        pRsvpTeTnlInfoB->pTeTnlInfo->u4TnlIndex)
    {
        return RPTE_RB_GREATER;
    }
    else if (pRsvpTeTnlInfoA->pTeTnlInfo->u4TnlIndex <
             pRsvpTeTnlInfoB->pTeTnlInfo->u4TnlIndex)
    {
        return RPTE_RB_LESSER;
    }
    if ((i4Return = MEMCMP (pRsvpTeTnlInfoA->pTeTnlInfo->TnlIngressLsrId,
                            pRsvpTeTnlInfoB->pTeTnlInfo->TnlIngressLsrId,
                            IPV4_ADDR_LENGTH)) != 0)
    {
        return i4Return;
    }
    if ((i4Return = MEMCMP (pRsvpTeTnlInfoA->pTeTnlInfo->TnlEgressLsrId,
                            pRsvpTeTnlInfoB->pTeTnlInfo->TnlEgressLsrId,
                            IPV4_ADDR_LENGTH)) != 0)
    {
        return i4Return;
    }
    if (pRsvpTeTnlInfoA->pTeTnlInfo->u4TnlPrimaryInstance >
        pRsvpTeTnlInfoB->pTeTnlInfo->u4TnlPrimaryInstance)
    {
        return RPTE_RB_GREATER;
    }
    else if (pRsvpTeTnlInfoA->pTeTnlInfo->u4TnlPrimaryInstance <
             pRsvpTeTnlInfoB->pTeTnlInfo->u4TnlPrimaryInstance)
    {
        return RPTE_RB_LESSER;
    }
    return RPTE_EQUAL;
}

/***************************************************************************
 * FUNCTION NAME    : RpteFrrTnlPortMappingRBCmp
 *
 * DESCRIPTION      : This fucntion check for the presence of frr backup
 *                    Tunnel id in frr tunnel port mapping RB Tree.
 *
 * INPUT            : pRBElem1 - Pointer to the node1 for comparision
 *                    pRBElem2 - Pointer to the node2 for comparision
 *
 * OUTPUT           : None
 *
 * RETURNS          : RPTE_EQUAL       - if all the keys match for both
 *                                       the nodes.
 *                    RPTE_RB_LESSER   - if node pRBElem1's key is less than
 *                                       node pRBElem2's key.
 *                    RPTE_RB_GREATER  - if node pRBElem1's key is greater
 *                                       than node pRBElem2's key.
 *
 **************************************************************************/
PRIVATE INT4
RpteFrrTnlPortMappingRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tRsvpTeFrrTnlPortMapping *pRsvpTeFrrTnlPortMappingInfoA = NULL;
    tRsvpTeFrrTnlPortMapping *pRsvpTeFrrTnlPortMappingInfoB = NULL;

    pRsvpTeFrrTnlPortMappingInfoA = (tRsvpTeFrrTnlPortMapping *) pRBElem1;
    pRsvpTeFrrTnlPortMappingInfoB = (tRsvpTeFrrTnlPortMapping *) pRBElem2;

    if (pRsvpTeFrrTnlPortMappingInfoA->u2TnlId >
        pRsvpTeFrrTnlPortMappingInfoB->u2TnlId)
    {
        return RPTE_RB_GREATER;
    }

    if (pRsvpTeFrrTnlPortMappingInfoA->u2TnlId <
        pRsvpTeFrrTnlPortMappingInfoB->u2TnlId)
    {
        return RPTE_RB_LESSER;
    }

    return RPTE_EQUAL;
}

/*****************************************************************************/
/* Function Name : RpteShutDownProcess                                       */
/* Description   : This routine deletes the memory, queue and thread when    */
/*                 the RSVP component is killed                              */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
RpteShutDownProcess ()
{
    MPLS_RSVPTE_LOCK ();

    RpteDeInitRsvpTe ();

    MPLS_RSVPTE_UNLOCK ();

    /* Semophore deletion */
    OsixSemDel (RSVP_GBL_SEM_ID);
    MEMSET (&gRsvpTeGblInfo, RSVPTE_ZERO, sizeof (tRsvpTeGblInfo));

    /* Task deletion */
    OsixDeleteTask (SELF, (const UINT1 *) "RPTE");
#ifdef SNMP_2_WANTED
    UnRegisterFSRSVP ();
    UnRegisterFSMPFR ();
#endif
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rpteinit.c                             */
/*---------------------------------------------------------------------------*/
