/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptehelo.c,v 1.39 2017/06/08 11:40:32 siva Exp $
 *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptehelo.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : RSVP-TE 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for the Hello message
 *                             Handler Module.                          
 *                                             
 *---------------------------------------------------------------------------*/
#include "rpteincs.h"

/****************************************************************************/
/* Function Name   : RpteHhProcessHelloMsg                                  */
/* Description     : This function Handles the Hello message.               */
/* Input (s)       : pPktMap   - Pointer to PktMap                          */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteHhProcessHelloMsg (tPktMap * pPktMap)
{
    tObjHdr            *pObjHdr = NULL;
    tHelloNbrInfo      *pHelloNbrInfo = NULL;
    tTMO_SLL           *pNbrList = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    UINT4               u4Time = RSVPTE_ZERO;
    UINT1               u1IsStartRecoveryProcedure = RPTE_NO;

    RSVPTE_DBG (RSVPTE_HH_ETEXT, " RpteHhProcessHelloMsg : ENTRY \n ");

    if (IF_ENTRY_HELLO_SPRTD (PKT_MAP_IF_ENTRY (pPktMap)) != HELLO_SUPPRT)
    {
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_HH_ETEXT,
                    " Interface Does not support Hello : EXIT \n");
        return;
    }

    pNbrList = &(IF_ENTRY_NBR_LIST (PKT_MAP_IF_ENTRY (pPktMap)));
    TMO_SLL_Scan (pNbrList, pNbrEntry, tNbrEntry *)
    {
        u1IsStartRecoveryProcedure = RPTE_NO;
        /* Check for the matching Hello neighbour */
        if (PKT_MAP_SRC_ADDR (pPktMap) != NBR_ENTRY_ADDR (pNbrEntry))
        {
            continue;
        }

        if (pNbrEntry->bIsHelloActive == RPTE_FALSE)
        {
            continue;
        }

        pObjHdr = &pPktMap->pHelloObj->ObjHdr;
        pHelloNbrInfo = &pNbrEntry->HelloNbrInfo;
        NBR_ENTRY_HELLO_SPRT (pNbrEntry) = RPTE_NBR_HELLO_SPRT_ENA;
        NBR_ENTRY_HELLO_STATE (pNbrEntry) = pHelloNbrInfo->u1HelloState;
        NBR_ENTRY_SRC_INST_INFO (pNbrEntry) =
            OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns);
        NBR_ENTRY_DEST_INST_INFO (pNbrEntry) =
            OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4DstIns);

        /* Set the Hello Time to Die for the maximum waiting time for 
         * receiving a hello message 
         */
        OsixGetSysTime ((tOsixSysTime *) & (u4Time));
        HELLO_TIME_TO_DIE (pHelloNbrInfo) = u4Time +
            (UINT4) (HELLO_MAX_RETRY *
                     RSVPTE_HELLO_INTVL_TIME (gpRsvpTeGblInfo));
        RpteGrStoreNbrProperties (pNbrEntry, pPktMap);

        if (OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)) ==
            RPTE_HELLO_REQ_CLASS_NUM_TYPE)
        {
            /* First time Hello msg */
            RSVPTE_DBG4 (RSVPTE_HH_PRCS,
                         "process hello req for NBR: %x, currTime: %u HELLO_TIME_TO_DIE: %d with state: %d\n",
                         NBR_ENTRY_ADDR (pNbrEntry), u4Time,
                         HELLO_TIME_TO_DIE (pHelloNbrInfo),
                         pHelloNbrInfo->u1HelloState);
            switch (pHelloNbrInfo->u1HelloState)
            {
                case STATUS_UNKNOWN:
                case HELLO_NOT_SUPPRT:
                    if (pHelloNbrInfo->u1HelloRel != UPSTREAM)
                    {
                        pHelloNbrInfo->u1HelloRel = DOWNSTREAM;
                    }

                    if ((pHelloNbrInfo->u4DstIns == RPTE_ZERO) &&
                        (OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns)
                         != RPTE_ZERO))
                    {
                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "Nbr %x has sent Hello_Req with src_inst "
                                     "Non-zero and nbr_src_inst is zero.\n",
                                     NBR_ENTRY_ADDR (pNbrEntry));

                        pHelloNbrInfo->u1HelloState = HELLO_SUPPRT;
                        RpteHhGetTnlListForLocalRecProc (PKT_MAP_IF_ENTRY
                                                         (pPktMap),
                                                         pNbrEntry,
                                                         HELLO_NBR_UP);
                        pHelloNbrInfo->u4DstIns =
                            OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns);
                        /* If the restarting node receives first hello during its restart,
                         * It has to start recovery timer and set the neighbor hello
                         * synchronization flag
                         * */
                        if ((gpRsvpTeGblInfo->u1GrProgressState ==
                             RPTE_GR_RESTART_IN_PROGRESS)
                            || (gpRsvpTeGblInfo->u1GrProgressState ==
                                RPTE_GR_RECOVERY_IN_PROGRESS))
                        {
                            pNbrEntry->bIsHelloSynchronoziedAfterRestart
                                = RPTE_TRUE;
                        }
                        else
                        {

                            pHelloNbrInfo->u4SrcIns = DEF_SRC_INSTANCE;
                            gu4MyHelloInstance = pHelloNbrInfo->u4SrcIns;
                        }
                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "nbr_src_inst updated with new value %d\n",
                                     pHelloNbrInfo->u4DstIns);
                    }
                    break;

                case HELLO_SUPPRT_RESET:
                    pHelloNbrInfo->u1HelloState = HELLO_SUPPRT;

                    /* During the restart period, if the helper receives src instance differnt
                     * than the src instance received prior to restart, its a nodal fault.
                     * If its same, then the fault is control channel fault. This situation happens when
                     * the restarting node gets restarted after the helper reaches RESET state
                     *  */
                    if ((pNbrEntry->u1GrProgressState ==
                         RPTE_GR_RESTART_IN_PROGRESS)
                        && (OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns) !=
                            RPTE_ZERO))
                    {

                        if (pHelloNbrInfo->u4DstIns !=
                            (OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns)))
                        {
                            pNbrEntry->u1GrFaultType = RPTE_GR_NODAL_FAULT;
                        }
                        else
                        {
                            pNbrEntry->u1GrFaultType =
                                RPTE_GR_CONTROL_CHANNEL_FAULT;
                        }
                        u1IsStartRecoveryProcedure = RPTE_YES;
                        /* The hello relation of the helper node should be downstream */
                        pHelloNbrInfo->u1HelloRel = DOWNSTREAM;
                    }
                    else
                    {
                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "Nbr %x setting HELLO_NBR_RESET in HELLO_SUPPRT_RESET\n",
                                     NBR_ENTRY_ADDR (pNbrEntry));

                        RpteHhGetTnlListForLocalRecProc (PKT_MAP_IF_ENTRY
                                                         (pPktMap), pNbrEntry,
                                                         HELLO_NBR_RESET);
                    }
                    pHelloNbrInfo->u4DstIns =
                        OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns);
                    break;

                case HELLO_SUPPRT:
                    /* Compare the Received Hello message with that of previous 
                     * received message
                     */
                    /*if ((pHelloNbrInfo->u1HelloRel == UPSTREAM) &&
                       (NetIpv4IsLoopbackAddress (pPktMap->u4DestAddr) != NETIPV4_SUCCESS) &&
                       (MEMCMP (&pPktMap->u4DestAddr, &pPktMap->u4SrcAddr,
                       sizeof (UINT4)) < RSVPTE_ZERO))
                       {
                       RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                       "Nbr %x u1HelloRel set as DOWNSTREAM/PASSIVE HELLO_SUPPRT\n",
                       NBR_ENTRY_ADDR (pNbrEntry));                        
                       pHelloNbrInfo->u1HelloRel = DOWNSTREAM;
                       } */
                    /* During the restart period, if the helper receives src instance differnt
                     * than the src instance received prior to restart, its a nodal fault.
                     * This situation happens when the restarting node gets restarted 
                     *  before the helper reaches RESET state
                     */
                    if ((pNbrEntry->u1GrProgressState ==
                         RPTE_GR_RESTART_IN_PROGRESS)
                        && (OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns) !=
                            RPTE_ZERO)
                        && (pHelloNbrInfo->u4DstIns !=
                            (OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns)))
                        && (pNbrEntry->u2RecoveryTime != RPTE_ZERO))
                    {
                        pNbrEntry->u1GrFaultType = RPTE_GR_NODAL_FAULT;
                        u1IsStartRecoveryProcedure = RPTE_YES;
                        break;
                    }

                    if ((OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4DstIns)
                         != RPTE_ZERO) &&
                        (OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4DstIns) !=
                         pHelloNbrInfo->u4SrcIns))
                    {
                        RpteUtlCleanPktMap (pPktMap);
                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "Nbr %x has sent a Hello_Req with dest_inst "
                                     "different from src_inst recently txmtted "
                                     "to it\n", NBR_ENTRY_ADDR (pNbrEntry));
                    }

                    if ((pHelloNbrInfo->u4DstIns !=
                         OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns)) ||
                        (OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns)
                         == RPTE_ZERO))
                    {
                        /* GR node has restarted before this helper node
                         * detects the expiry. In this case also, helper node
                         * has to help the GR node to recover its state
                         * */
                        if ((pNbrEntry->u2RestartTime != RPTE_ZERO) &&
                            (gpRsvpTeGblInfo->RsvpTeCfgParams.
                             u1GRCapability != RPTE_GR_CAPABILITY_NONE))
                        {
                            pNbrEntry->u1GrProgressState =
                                RPTE_GR_RESTART_IN_PROGRESS;

                            RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                         "Nbr %x calling TnlListForLocalRecProc with "
                                         "HELLO_NBR_DOWN "
                                         "RPTE_GR_RESTART_IN_PROGRESS \n",
                                         NBR_ENTRY_ADDR (pNbrEntry));
                            RpteHhGetTnlListForLocalRecProc (PKT_MAP_IF_ENTRY
                                                             (pPktMap),
                                                             pNbrEntry,
                                                             HELLO_NBR_DOWN);
                            pNbrEntry->u1GrFaultType = RPTE_GR_NODAL_FAULT;
                            u1IsStartRecoveryProcedure = RPTE_YES;
                            pHelloNbrInfo->u4DstIns =
                                OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns);
                            break;
                        }

                        pHelloNbrInfo->u1HelloState = HELLO_SUPPRT_RESET;
                        pHelloNbrInfo->u4TimeWait = RESET_MAX_WAIT;

                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "Nbr %x calling TnlListForLocalRecProc with "
                                     "HELLO_NBR_RESET"
                                     "HELLO_SUPPRT_RESET \n",
                                     NBR_ENTRY_ADDR (pNbrEntry));

                        /* Trigger for Node failure */
                        RpteHhGetTnlListForLocalRecProc (PKT_MAP_IF_ENTRY
                                                         (pPktMap),
                                                         pNbrEntry,
                                                         HELLO_NBR_RESET);
                        RpteUtlCleanPktMap (pPktMap);

                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "Nbr %x has sent a Hello_Req with zero "
                                     "src_inst or different from previously "
                                     "txmtted one\n",
                                     NBR_ENTRY_ADDR (pNbrEntry));

                        RSVPTE_DBG (RSVPTE_HH_PRCS, "Resetting the Nbr\n");
                        return;
                    }
                    break;

                default:
                    RpteUtlCleanPktMap (pPktMap);
                    RSVPTE_DBG (RSVPTE_HH_ETEXT, "RpteHhProcessHelloMsg"
                                ":INTMD-EXIT \n");
                    return;
            }
            /* Send a Hello message appropriately */
            RpteHhGenerateHello (pNbrEntry, RPTE_HELLO_ACK_CTYPE,
                                 PKT_MAP_IF_ENTRY (pPktMap));

            /* Start recovery procedure after sending hello msg */
            if (u1IsStartRecoveryProcedure == RPTE_YES)
            {
                RpteHlprRecoveryProcedure (pNbrEntry);
            }
        }
        else
        {                        /* Hello Acknowledge message */
            RSVPTE_DBG4 (RSVPTE_HH_PRCS,
                         "process hello ack for NBR: %x, currTime: %u HELLO_TIME_TO_DIE: %d with state\n",
                         NBR_ENTRY_ADDR (pNbrEntry), u4Time,
                         HELLO_TIME_TO_DIE (pHelloNbrInfo),
                         pHelloNbrInfo->u1HelloState);
            /* First time Hello msg */
            switch (pHelloNbrInfo->u1HelloState)
            {
                case STATUS_UNKNOWN:
                    if ((pHelloNbrInfo->u4DstIns == RPTE_ZERO) &&
                        (OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns)
                         != RPTE_ZERO))
                    {
                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "Nbr %x has sent Hello_Ack with src_inst "
                                     "Non-zero and nbr_src_inst is zero.\n",
                                     NBR_ENTRY_ADDR (pNbrEntry));

                        pHelloNbrInfo->u1HelloState = HELLO_SUPPRT;
                        RpteHhGetTnlListForLocalRecProc (PKT_MAP_IF_ENTRY
                                                         (pPktMap),
                                                         pNbrEntry,
                                                         HELLO_NBR_UP);
                        pHelloNbrInfo->u4DstIns =
                            OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns);

                        /* If the restarting node receives first hello during its restart,
                         * It has to start recovery timer and set the neigbhor
                         * hello synchronization flag
                         * */
                        if ((gpRsvpTeGblInfo->u1GrProgressState ==
                             RPTE_GR_RESTART_IN_PROGRESS)
                            || (gpRsvpTeGblInfo->u1GrProgressState ==
                                RPTE_GR_RECOVERY_IN_PROGRESS))
                        {
                            pNbrEntry->bIsHelloSynchronoziedAfterRestart
                                = RPTE_TRUE;
                        }
                        else
                        {
                            pHelloNbrInfo->u4SrcIns = DEF_SRC_INSTANCE;
                            gu4MyHelloInstance = pHelloNbrInfo->u4SrcIns;
                        }

                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "nbr_src_inst updated with new value %d\n",
                                     pHelloNbrInfo->u4DstIns);
                    }
                    break;

                case HELLO_SUPPRT_RESET:
                    pHelloNbrInfo->u1HelloState = HELLO_SUPPRT;
                    RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                 "Nbr %x Hello Ack: setting HELLO_NBR_RESET in else HELLO_SUPPRT_RESET\n",
                                 NBR_ENTRY_ADDR (pNbrEntry));
                    RpteHhGetTnlListForLocalRecProc (PKT_MAP_IF_ENTRY (pPktMap),
                                                     pNbrEntry,
                                                     HELLO_NBR_RESET);

                    pHelloNbrInfo->u4DstIns =
                        OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns);
                    break;

                case HELLO_SUPPRT:
                    if ((OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4DstIns)
                         != RPTE_ZERO) &&
                        (OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4DstIns) !=
                         pHelloNbrInfo->u4SrcIns))
                    {
                        RpteUtlCleanPktMap (pPktMap);
                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "Nbr %x has sent a Hello_Ack with dest_inst "
                                     "different from src_inst recently txmtted "
                                     "to it\n", NBR_ENTRY_ADDR (pNbrEntry));
                    }

                    if ((pHelloNbrInfo->u4DstIns !=
                         OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns)) ||
                        (OSIX_NTOHL (pPktMap->pHelloObj->Hello.u4SrcIns)
                         == RPTE_ZERO))
                    {
                        pHelloNbrInfo->u1HelloState = HELLO_SUPPRT_RESET;
                        pHelloNbrInfo->u4TimeWait = RESET_MAX_WAIT;

                        /* Trigger for Node failure */
                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "Nbr %x calling TnlListForLocalRecProc with "
                                     "HELLO_NBR_RESET"
                                     "HELLO_SUPPRT_RESET \n",
                                     NBR_ENTRY_ADDR (pNbrEntry));

                        RpteHhGetTnlListForLocalRecProc (PKT_MAP_IF_ENTRY
                                                         (pPktMap),
                                                         pNbrEntry,
                                                         HELLO_NBR_RESET);
                        RpteUtlCleanPktMap (pPktMap);

                        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                     "Nbr %x has sent a Hello_Ack with zero "
                                     "src_inst or different from previously "
                                     "txmtted one\n",
                                     NBR_ENTRY_ADDR (pNbrEntry));

                        RSVPTE_DBG (RSVPTE_HH_PRCS, "Resetting the Nbr\n");
                        return;
                    }
                    break;

                default:
                    RpteUtlCleanPktMap (pPktMap);

                    RSVPTE_DBG (RSVPTE_HH_ETEXT, "RpteHhProcessHelloMsg"
                                ": INTMD-EXIT /n");

                    return;
            }                    /* end switch */
        }                        /* end else */
    }                            /* end SLL Scan */
    RpteUtlCleanPktMap (pPktMap);

    RSVPTE_DBG (RSVPTE_HH_ETEXT, "RpteHhProcessHelloMsg : EXIT \n");

    return;
}

/****************************************************************************/
/* Function Name   : RpteHhGenerateHello                                    */
/* Description     : This function Generates a Hello message whenever it is */
/*                   called. It verifies the Hello state of the neighbour   */
/*                   before generating a Hello message.                     */
/* Input (s)       : pNbrEntry      - pointer to neighbour entry            */
/*                 : u1HelloMsgType - Type of Hello message                 */
/*                 : pIfEntry       - Pointer to Interface Entry            */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteHhGenerateHello (tNbrEntry * pNbrEntry,
                     UINT1 u1HelloMsgType, tIfEntry * pIfEntry)
{
    UINT4               u4Time = RSVPTE_ZERO;

    tHelloNbrInfo      *pHelloNbrInfo = NULL;

    RSVPTE_DBG (RSVPTE_HH_ETEXT, " RpteHhGenerateHello : ENTRY \n");

    pHelloNbrInfo = &pNbrEntry->HelloNbrInfo;

    RSVPTE_DBG4 (RSVPTE_HH_ETEXT,
                 " RpteHhGenerateHello : for Nbr: %x currtime: %u "
                 "msgType: %u & state: %d\n", NBR_ENTRY_ADDR (pNbrEntry),
                 u4Time, u1HelloMsgType, pHelloNbrInfo->u1HelloState);

    switch (pHelloNbrInfo->u1HelloState)
    {
            /* First time Hello msg */
        case STATUS_UNKNOWN:
            pHelloNbrInfo->u1HelloRel = UPSTREAM;
            /* In GR case, for nodal fault, source instance has to be incremented,
             * when the node is restarted 
             */
            if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability ==
                RPTE_GR_CAPABILITY_FULL)
            {
                pHelloNbrInfo->u4SrcIns = gu4MyHelloInstance;
            }
            else
            {
                pHelloNbrInfo->u4SrcIns = DEF_SRC_INSTANCE;
            }
            pHelloNbrInfo->u4DstIns = DEF_DST_INSTANCE;
            gu4MyHelloInstance = pHelloNbrInfo->u4SrcIns;
            RpteHhSendHello (pNbrEntry, u1HelloMsgType, pIfEntry);
            /* Set the maximum hello waiting time */
            OsixGetSysTime ((tOsixSysTime *) & (u4Time));
            HELLO_TIME_TO_DIE (pHelloNbrInfo) = u4Time +
                (UINT4) (HELLO_MAX_RETRY *
                         RSVPTE_HELLO_INTVL_TIME (gpRsvpTeGblInfo));
            break;

        case HELLO_SUPPRT:
            RpteHhSendHello (pNbrEntry, u1HelloMsgType, pIfEntry);
            break;

        case HELLO_SUPPRT_RESET:
            /* Since the Hello message has not been received, change the source
             * instance & try sending the message
             */
            /* In GR case, For nodal fault only source instance has to be incremented
             * For control channal fault, source instance has not to be incremented
             */
            /*if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability ==
               RPTE_GR_CAPABILITY_NONE)
               {
               pHelloNbrInfo->u4SrcIns++;
               } */
            gu4MyHelloInstance = pHelloNbrInfo->u4SrcIns;
            RpteHhSendHello (pNbrEntry, u1HelloMsgType, pIfEntry);
            /* Set the maximum hello waiting time */
            OsixGetSysTime ((tOsixSysTime *) & (u4Time));
            HELLO_TIME_TO_DIE (pHelloNbrInfo) = u4Time +
                (UINT4) (HELLO_MAX_RETRY *
                         RSVPTE_HELLO_INTVL_TIME (gpRsvpTeGblInfo));
            break;

        default:
            break;
    }

    RSVPTE_DBG (RSVPTE_HH_ETEXT, " RpteHhGenerateHello : EXIT \n");

    return;
}

/****************************************************************************/
/* Function Name   : RpteHhSendHello                                        */
/* Description     : This function Sends a Hello message whenever it is     */
/*                   called. It Constructs a Packet Map and sends the Hello */
/*                   message.                                               */
/* Input (s)       : pNbrEntry      - Pointer to neighbor entry             */
/*                 : u1HelloMsgType - Type of Hello message                 */
/*                 : tIfEntry       - Pointer to Interface Entry            */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteHhSendHello (tNbrEntry * pNbrEntry,
                 UINT1 u1HelloMsgType, tIfEntry * pIfEntry)
{
    tPktMap             PktMap;
    INT4                i4HelloSize = RSVPTE_ZERO;
    tObjHdr            *pObjHdr = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    UINT4               u4TmpAddr = RSVPTE_ZERO;
    UINT1               u1RecoveryPathCapability = RSVPTE_ZERO;
    UINT2               u2RestartTime = RSVPTE_ZERO;
    UINT2               u2RecoveryTime = RSVPTE_ZERO;
    tHelloNbrInfo      *pHelloNbrInfo = NULL;

    RSVPTE_DBG (RSVPTE_HH_ETEXT, "RpteHhSendHello : ENTRY \n");

    pHelloNbrInfo = &pNbrEntry->HelloNbrInfo;

    RpteUtlInitPktMap (&PktMap);

    i4HelloSize = sizeof (tRsvpHdr) + sizeof (tHelloObj);

    i4HelloSize += RpteUtlCalculateHelloMsgSize (pNbrEntry, pIfEntry);

    /* Allocate memory for the RSVP packet */
    PKT_MAP_RSVP_PKT (&PktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);
    if (PKT_MAP_RSVP_PKT (&PktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_HH_PRCS,
                    "HELO: Mem Alloc failed in RpteHhSendHello.\n");
        RSVPTE_DBG (RSVPTE_HH_ETEXT, "RpteHhSendHello : INTMD-EXIT \n");
        return;
    }
    MEMSET (PKT_MAP_RSVP_PKT (&PktMap), RSVPTE_ZERO, i4HelloSize);
    PKT_MAP_RSVP_PKT_ALLOC (&PktMap) = RPTE_PKTMAP_RSVP_ALLOC;
    /* Copy Source address i.e this node's address 
     * For unnumbered interfaces, the Hello Request messages transmitted from 
     * a node shall use the local node-id value as the source IP address value
     * */
    if (IF_ENTRY_ADDR (pIfEntry) != RPTE_ZERO)
    {
        PKT_MAP_SRC_ADDR (&PktMap) = IF_ENTRY_ADDR (pIfEntry);
    }
    else
    {
        CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                            u4TmpAddr);
        u4TmpAddr = OSIX_NTOHL (u4TmpAddr);

        PKT_MAP_SRC_ADDR (&PktMap) = u4TmpAddr;
    }

    /* Copy Destination address i.e Prev node's address */
    PKT_MAP_DST_ADDR (&PktMap) = pNbrEntry->u4Addr;
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (&PktMap);
    /* Fill the Rsvp Header */
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = HELLO_MSG;
    /* Fill TTL value as 1 according to RFC-3209 SECTION-5.1 */
    RSVP_HDR_SEND_TTL (pRsvpHdr) = RSVPTE_IF_SENDTTL_MIN_VAL;
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (i4HelloSize);

    pObjHdr =
        (tObjHdr *) (VOID *) (PKT_MAP_RSVP_PKT (&PktMap) + sizeof (tRsvpHdr));

    /* Fill HelloObj */
    OBJ_HDR_LENGTH (pObjHdr) = OSIX_HTONS (sizeof (tHelloObj));
    if (u1HelloMsgType == RPTE_HELLO_REQ_CTYPE)
    {
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_HELLO_REQ_CLASS_NUM_TYPE);
    }
    else
    {
        OBJ_HDR_CLASS_NUM_TYPE (pObjHdr) =
            OSIX_HTONS (RPTE_HELLO_ACK_CLASS_NUM_TYPE);
    }
    HELLO_SRC_INS (&HELLO_OBJ ((tHelloObj *) (VOID *) pObjHdr)) =
        OSIX_HTONL (HELLO_SRC_INS (pHelloNbrInfo));
    HELLO_DST_INS (&HELLO_OBJ ((tHelloObj *) (VOID *) pObjHdr)) =
        OSIX_HTONL (HELLO_DST_INS (pHelloNbrInfo));

    if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability !=
        RPTE_GR_CAPABILITY_NONE)
    {
        if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability ==
            RPTE_GR_CAPABILITY_FULL)
        {
            if (gi4MplsSimulateFailure != RPTE_SIM_FAILURE_RESTART_ZERO)
            {
                u2RestartTime = gpRsvpTeGblInfo->RsvpTeCfgParams.u2RestartTime;
            }
            if ((gi4MplsSimulateFailure != RPTE_SIM_FAILURE_NO_ILM) &&
                (gi4MplsSimulateFailure != RPTE_SIM_FAILURE_RECOVERY_ZERO))
            {
                u2RecoveryTime =
                    gpRsvpTeGblInfo->RsvpTeCfgParams.u2RecoveryTime;
            }
        }
        pObjHdr =
            (tObjHdr *) (VOID *) (((UINT1 *) pObjHdr) + sizeof (tHelloObj));
        RptePvmFillRestartCapObj (u2RestartTime, u2RecoveryTime, &pObjHdr);
        if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1RecoveryPathCapability !=
            RPTE_ZERO)
        {
            u1RecoveryPathCapability = gpRsvpTeGblInfo->RsvpTeCfgParams.
                u1RecoveryPathCapability;
            /* If the restart time value is zero, hello msg should be sent
             * with R bit 0
             * */
            if ((u2RestartTime == RPTE_ZERO) ||
                (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_NO_RESTART_CAP_OBJ))
            {
                u1RecoveryPathCapability &= (UINT1) (~RPTE_GR_RECOVERY_PATH_RX);
            }
            RptePvmFillCapabilityObj (&pObjHdr, pNbrEntry,
                                      u1RecoveryPathCapability);
        }
    }

    PKT_MAP_BUILD_PKT (&PktMap) = RPTE_YES;
    PKT_MAP_OUT_IF_ENTRY (&PktMap) = pIfEntry;
    PKT_MAP_TX_TTL (&PktMap) = IF_ENTRY_TTL (pIfEntry);
    RptePbSendMsg (&PktMap);

    RSVPTE_DBG (RSVPTE_HH_ETEXT, "RpteHhSendHello : EXIT \n");

    return;
}

/****************************************************************************/
/* Function Name   : RpteHhStartHelloRefreshTmr                             */
/* Description     : This function starts the Refresh Timer for the         */
/*                   RsvpTeGblInfo                                          */
/* Input (s)       : pRsvpTeGblInfo - Pointer to the RsvpTeGblInfo          */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
UINT1
RpteHhStartHelloRefreshTmr (tRsvpTeGblInfo * pRsvpTeGblInfo)
{
    tTimerParam        *pTmrParam = NULL;
    /*  RSVPTE_DBG (RSVPTE_HH_ETEXT, " RpteHhStartHelloRefreshTmr : ENTRY \n"); */

    pTmrParam = (tTimerParam *) & HELLO_TIMER_PARAM (pRsvpTeGblInfo);
    /* If the timer is present stop the timer */
    TmrStopTimer (RSVP_GBL_TIMER_LIST, &HELLO_TIMER (pRsvpTeGblInfo));

    TIMER_PARAM_ID (pTmrParam) = RPTE_HELLO_REFRESH;
    TIMER_PARAM_HELLO (pTmrParam) = (tRsvpTeGblInfo *) pRsvpTeGblInfo;
    RSVP_TIMER_NAME (&HELLO_TIMER (pRsvpTeGblInfo)) = (FS_ULONG) pTmrParam;
    if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST, &HELLO_TIMER (pRsvpTeGblInfo),
                           RSVPTE_HELLO_INTVL_TIME (pRsvpTeGblInfo)) !=
        TMR_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_HH_PRCS,
                    "HELLO: Starting of HelloRefreshTimer failed : "
                    "INTMD-EXIT\n");
        return RPTE_FAILURE;
    }

    /* RSVPTE_DBG (RSVPTE_HH_ETEXT, "RpteHhStartHelloRefreshTmr : EXIT \n"); */

    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteHhProcessWaitTimeOut                               */
/* Description     : This function processes the Expired Hello Wait Timer    */
/* Input (s)       : *pExpiredTimer - Pointer to the Expired Timer List     */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteHhProcessWaitTimeOut (tTmrAppTimer * pExpiredTmr)
{
    tHelloNbrInfo      *pHelloNbrInfo = NULL;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tTMO_SLL           *pNbrList = NULL;
    UINT4               u4Time = RSVPTE_ZERO;
    UINT4               u4HashIndex;

    pExpiredTmr = (tTmrAppTimer *) pExpiredTmr;
    /* RSVPTE_DBG (RSVPTE_HH_ETEXT, " RpteHhProcessWaitTimeOut : ENTRY \n"); */

    TMO_HASH_Scan_Table (RSVP_GBL_IF_HSH_TBL, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (RSVP_GBL_IF_HSH_TBL, u4HashIndex, pIfEntry,
                              tIfEntry *)
        {
            if (IF_ENTRY_HELLO_SPRTD (pIfEntry) != HELLO_SUPPRT)
            {
                continue;
            }

            pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));
            if (TMO_SLL_Count (pNbrList) == RSVPTE_ZERO)
            {
                continue;
            }
            TMO_SLL_Scan (pNbrList, pNbrEntry, tNbrEntry *)
            {
                /* When more than one unnumbered link exist between two nodes,
                 * there shall be only one node id based hello message 
                 * session between that nodes
                 * */
                if (pNbrEntry->bIsHelloActive == RPTE_FALSE)
                {
                    continue;
                }
                if ((pNbrEntry->u1GrProgressState ==
                     RPTE_GR_RESTART_IN_PROGRESS)
                    || (gpRsvpTeGblInfo->u1GrProgressState ==
                        RPTE_GR_RESTART_IN_PROGRESS))
                {
                    continue;
                }
                pHelloNbrInfo = &pNbrEntry->HelloNbrInfo;
                OsixGetSysTime ((tOsixSysTime *) & (u4Time));
                switch (pHelloNbrInfo->u1HelloState)
                {
                    case HELLO_NOT_SUPPRT:
                        break;

                    case STATUS_UNKNOWN:
                        /* 
                         * During creation of NbrEntry the Hello
                         * Time to Die will be set to zero, hence a 
                         * Hello Req message is generated & the Hello 
                         * time to die value is set to def value. 
                         * If a Hello Ack is not received within a 
                         * specified time interval then the Hello 
                         * status is set to HELLO_NOT_SUPPRT
                         */
                        if ((u4Time >= HELLO_TIME_TO_DIE (pHelloNbrInfo)) &&
                            (HELLO_TIME_TO_DIE (pHelloNbrInfo) != RSVPTE_ZERO))
                        {
                            RSVPTE_DBG2 (RSVPTE_HH_PRCS,
                                         "HELLO: WaitTimeOut CurrTime = %d "
                                         "HelloTimeToDie = %d\n",
                                         u4Time,
                                         HELLO_TIME_TO_DIE (pHelloNbrInfo));
                            pHelloNbrInfo->u1HelloState = HELLO_NOT_SUPPRT;
                        }
                        else if (pHelloNbrInfo->u1HelloRel != UPSTREAM)
                        {
                            RpteHhGenerateHello (pNbrEntry,
                                                 RPTE_HELLO_REQ_CTYPE,
                                                 pIfEntry);
                        }
                        else
                        {
                            RpteHhSendHello (pNbrEntry,
                                             RPTE_HELLO_REQ_CTYPE, pIfEntry);
                        }
                        break;

                    case HELLO_SUPPRT:
                        /*
                         * If a neighbour supports Hello, then either
                         * Hello Req or Hello Ack message has to come 
                         * within a specified Hello interval. If the 
                         * message is not received or message is received 
                         * with a wrong instance, then it is assumed that
                         * the neighbour has reset his state. Then a 
                         * trigger/message is  sent to all the Tunnels
                         * which are passing through the particular 
                         * neighbour about the status. If we have to 
                         * generate the Hello message then Hello Req is 
                         * generated. */
                        /* Compare the current time with the hello 
                         * time to die field */
                        if (u4Time >= HELLO_TIME_TO_DIE (pHelloNbrInfo))
                        {
                            /*
                             * If the timer has expired & no valid 
                             * HelloMesg is received from the NextHop
                             * then status is set to RESET
                             */
                            RSVPTE_DBG2 (RSVPTE_HH_PRCS,
                                         "HELLO: WaitTimeOut CurrTime = %d "
                                         "HelloTimeToDie = %d\n",
                                         u4Time,
                                         HELLO_TIME_TO_DIE (pHelloNbrInfo));
                            pHelloNbrInfo->u1HelloState = HELLO_SUPPRT_RESET;
                            pHelloNbrInfo->u4TimeWait = RESET_MAX_WAIT;

                            /* If the Restart time received from the neighbor is
                             * not equal to 0 and the node is a helper, then
                             * start restart timer.
                             * */
                            if ((pNbrEntry->u2RestartTime != RPTE_ZERO) &&
                                (gpRsvpTeGblInfo->RsvpTeCfgParams.
                                 u1GRCapability != RPTE_GR_CAPABILITY_NONE))
                            {
                                pNbrEntry->u1GrProgressState =
                                    RPTE_GR_RESTART_IN_PROGRESS;
                                pNbrEntry->bIsHelloSynchronoziedAfterRestart
                                    = RPTE_FALSE;
                                RpteHlprStartRestartTmr (pNbrEntry);
                            }

                            RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                                         "Nbr %x calling TnlListForLocalRecProc with "
                                         "HELLO_NBR_DOWN "
                                         "HELLO_SUPPRT \n",
                                         NBR_ENTRY_ADDR (pNbrEntry));

                            RpteHhGetTnlListForLocalRecProc (pIfEntry,
                                                             pNbrEntry,
                                                             HELLO_NBR_DOWN);

                            /* Since the Hello message has not been received, change the source
                             * instance & try sending the message
                             */
                            /* In GR case, For nodal fault only source instance has to be incremented
                             * For control channal fault, source instance has not to be incremented
                             */
                            if (gpRsvpTeGblInfo->RsvpTeCfgParams.
                                u1GRCapability == RPTE_GR_CAPABILITY_NONE)
                            {
                                pHelloNbrInfo->u4SrcIns++;
                            }

                        }
                        else if (pHelloNbrInfo->u1HelloRel == UPSTREAM)
                        {
                            RpteHhSendHello (pNbrEntry,
                                             RPTE_HELLO_REQ_CTYPE, pIfEntry);
                        }
                        break;

                    case HELLO_SUPPRT_RESET:
                        /*
                         * If a neighbour supports Hello, but currently
                         * the status has been reset. Then if we have to
                         * generate Hello message a series of Hello Req
                         * are generated after a particular time interval.
                         * If the neighbour has come up then a Hello ack
                         * will be generated by that neighbour and state 
                         * will turn to the support state.  */
                        if (pHelloNbrInfo->u4TimeWait != RSVPTE_ZERO)
                        {
                            pHelloNbrInfo->u4TimeWait--;
                        }
                        /* Compare the current time with the 
                         * hello time to die field */
                        else if ((u4Time >= HELLO_TIME_TO_DIE (pHelloNbrInfo))
                                 && (pHelloNbrInfo->u1HelloResetSent ==
                                     RPTE_YES))
                        {
                            /*
                             * If the timer has expired & no valid HelloMesg is 
                             * received from the NextHop then the u4TimeWait is
                             * increased to the default value. 
                             */
                            pHelloNbrInfo->u4TimeWait = RESET_MAX_WAIT;
                            pHelloNbrInfo->u1HelloResetSent = RPTE_NO;
                        }
                        else if (pHelloNbrInfo->u1HelloRel == UPSTREAM)
                        {
                            if (pHelloNbrInfo->u1HelloResetSent == RPTE_NO)
                            {
                                RpteHhGenerateHello (pNbrEntry,
                                                     RPTE_HELLO_REQ_CTYPE,
                                                     pIfEntry);
                                pHelloNbrInfo->u1HelloResetSent = RPTE_YES;
                            }
                            else
                            {
                                RpteHhSendHello (pNbrEntry,
                                                 RPTE_HELLO_REQ_CTYPE,
                                                 pIfEntry);
                            }
                        }
                        else
                        {        /* Downstream */
                            pHelloNbrInfo->u4TimeWait = RESET_MAX_WAIT;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
    RpteHhStartHelloRefreshTmr (gpRsvpTeGblInfo);

    /* RSVPTE_DBG (RSVPTE_HH_ETEXT, " RpteHhProcessWaitTimeOut : EXIT \n"); */

    return;
}

/*****************************************************************************/
/* Function Name : RpteHhGetTnlListForLocalRecProc                           */
/* Description   : This routine Checks all the tunnels associated with the   */
/*                 particular neighbour and reset their status as Local      */
/*                 Protection in use/ Local Protection avail appropriately   */
/* Input(s)      : pIncIfEntry  - Pointer to the Interface Entry             */
/*                 pNbrEntry    - Pointer to the Neighbour Entry             */
/*                 u1Msg        - Message to be conveyed                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
RpteHhGetTnlListForLocalRecProc (tIfEntry * pIncIfEntry,
                                 tNbrEntry * pNbrEntry, UINT1 u1Msg)
{
    UNUSED_PARAM (pIncIfEntry);
    RSVPTE_DBG (RSVPTE_HH_ETEXT, " RpteHhGetTnlListForLocalRecProc : ENTRY \n");

    if (u1Msg == HELLO_NBR_DOWN)
    {
        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                     "Max Hello Time expired for Nbr %x - "
                     "Tunnels associated to be handled\n",
                     NBR_ENTRY_ADDR (pNbrEntry));

        RpteHandleNbrStatusChange (pNbrEntry, RPTE_DISABLED, RPTE_ZERO,
                                   RPTE_FALSE);
    }
    else if (u1Msg == HELLO_NBR_RESET)
    {
        RSVPTE_DBG1 (RSVPTE_HH_PRCS,
                     "Hello received for the first time or Hello instance "
                     "got changed from the Nbr %x\n",
                     NBR_ENTRY_ADDR (pNbrEntry));

        RSVPTE_DBG (RSVPTE_HH_PRCS,
                    "Local revertive behavior if enabled will be handled for "
                    "FRR Protected tunnels\n");

        RpteHandleNbrStatusChange (pNbrEntry, RPTE_ENABLED, RPTE_ZERO,
                                   RPTE_FALSE);
    }

    RSVPTE_DBG (RSVPTE_HH_ETEXT, " RpteHhGetTnlListForLocalRecProc : EXIT \n");

    return;
}

/*****************************************************************************/
/* Function Name : RpteHandleNbrStatusChange                                         */
/* Description   : This routine handles Neighbor down event. This routine    */
/*                 scans the downstream tunnels associated with this         */
/*                 neighbor and tears down Unprotected and protects FRR      */
/*                 tunnels. This routine scans upstream tunnels associated   */
/*                 with this neighbor and tears down them.                   */
/* Input(s)      : pNbrEntry  - Pointer to the Neighbor Entry                */
/*                 u4Status   - RPTE_DISABLED or RPTE_ENABLED                */
/*                 u4DataTeIfIndex - Data Te link interface                  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
RpteHandleNbrStatusChange (tNbrEntry * pNbrEntry, UINT4 u4Status,
                           UINT4 u4DataTeIfIndex, UINT1 u1IsIfEvent)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;
    UINT1               u1FrrNodeState = RPTE_ZERO;
    UINT2               u2ErrValue = RPTE_ZERO;
    UINT4               u4PsbNotifyAddr = RPTE_ZERO;
    UINT4               u4RsbNotifyAddr = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteHandleNbrStatusChange Entry\n");

    /* Down stream tunnels associated with the Neighbor is deleted here. */
    RSVPTE_DBG1 (RSVPTE_NBR_DBG,
                 "Downstream tnls associated with Nbr %x processed\n",
                 NBR_ENTRY_ADDR (pNbrEntry));

    if (u4Status == RPTE_DISABLED)
    {
        u2ErrValue = RPTE_LSP_LOCALLY_FAILED;
    }
    else
    {
        u2ErrValue = RPTE_LSP_RECOVERED;
    }

    UTL_DLL_OFFSET_SCAN (&(pNbrEntry->DnStrTnlList), pRsvpTeTnlInfo,
                         pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
    {
        if (pRsvpTeTnlInfo->pTeTnlInfo == NULL)
        {
            continue;
        }
        if ((u4Status == RPTE_ENABLED) &&
            (u1IsIfEvent == RPTE_TRUE) &&
            (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL) &&
            ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
              u1Flags) == RSVPTE_TNL_FRR_FACILITY_METHOD))
        {
            if (RpteUtlIsNodeProtDesired (pRsvpTeTnlInfo) != RPTE_YES)
            {
                RpteHhGenerateHello (pNbrEntry,
                                     RPTE_HELLO_REQ_CTYPE, pNbrEntry->pIfEntry);
            }
            continue;
        }

        if ((u4DataTeIfIndex != RPTE_ZERO) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf != RPTE_ZERO) &&
            (u4DataTeIfIndex !=
             pRsvpTeTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf))
        {
            continue;
        }
        /* Set the Tunnel status as not synchronized at TE level */
        if (pNbrEntry->u1GrProgressState == RPTE_GR_RESTART_IN_PROGRESS)
        {
            TeSigSetGrSynchronizedStatus (pRsvpTeTnlInfo->pTeTnlInfo,
                                          TNL_GR_NOT_SYNCHRONIZED);
            continue;
        }
        u4PsbNotifyAddr = RPTE_ZERO;
        u4RsbNotifyAddr = RPTE_ZERO;

        if (pRsvpTeTnlInfo->pPsb != NULL)
        {
            u4PsbNotifyAddr = pRsvpTeTnlInfo->pPsb->u4NotifyAddr;
        }

        if (pRsvpTeTnlInfo->pRsb != NULL)
        {
            u4RsbNotifyAddr = pRsvpTeTnlInfo->pRsb->u4NotifyAddr;
        }

        if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL)
            && (RPTE_FRR_OUT_TNL_INFO (pRsvpTeTnlInfo) != NULL))
        {
            RSVPTE_DBG5 (RSVPTE_NBR_DBG,
                         "FRR Tnl %d %d %x %x handled for status change %d\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                         u4Status);

            u1FrrNodeState = RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo);

            if (RPTE_IS_PLR (u1FrrNodeState))
            {
                RptePhHandleIfChangeForFrr (pRsvpTeTnlInfo, (UINT1) u4Status);

                if (u4Status == RPTE_DISABLED)
                {
                    RpteHhSetNbrHelloState (pNbrEntry);
                    RSVPTE_FRR_SWITCH_OVER_NUM (gpRsvpTeGblInfo)++;
                    RSVPTE_FRR_ACT_PROT_LSP_NUM (gpRsvpTeGblInfo)++;
                    RSVPTE_FRR_ACT_PROT_TUN_NUM (gpRsvpTeGblInfo)++;
                }
            }
            else if (u4Status == RPTE_DISABLED)
            {
                /* Delete the PLR_AWAIT tunnel upon downstream neighbor down */
                /* Below destroy event takes care of deleting the base *
                 * and out tnl information */

                MEMSET (&RpteEnqueueMsgInfo, 0, sizeof (tRpteEnqueueMsgInfo));

                RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pRsvpTeTnlInfo;

                RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
            }

            continue;
        }

        if ((u4Status == RPTE_ENABLED) && (u4PsbNotifyAddr == RPTE_ZERO) &&
            (u4RsbNotifyAddr == RPTE_ZERO))
        {
            continue;
        }

        if (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL)
        {
            RSVPTE_DBG4 (RSVPTE_NBR_DBG,
                         "FRR Bkp Tnl %d %d %x %x handled for disabled status "
                         "change\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));

            RpteHandlePsbOrHelloTimeOutForFrr (pRsvpTeTnlInfo);
        }
        else
        {
            RSVPTE_DBG5 (RSVPTE_NBR_DBG,
                         "Normal Tnl %d %d %x %x handled for "
                         "status change - to %u\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                         u4Status);

            /* Condition will be saatisfied when
             * 1. Notification is not enabled
             * 2. Protection type is one to one and the protection-in-use bit is not set
             * */

            if (((u4PsbNotifyAddr == RPTE_ZERO) &&
                 (u4RsbNotifyAddr == RPTE_ZERO)) ||
                ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                  i4E2EProtectionType == MPLS_TE_DEDICATED_ONE2ONE)
                 && (u4Status == RPTE_DISABLED)
                 &&
                 ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                   LOCAL_PROT_NOT_AVAIL)
                  || (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                      LOCAL_PROT_NOT_APPLICABLE))))
            {
                /* Before deleting the protection path, if the protection-in-use bit of the
                 * working tunnel is set, working tunnel should be deleted 
                 * */

                if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType ==
                     RPTE_TNL_PROTECTION_PATH)
                    && (pRsvpTeTnlInfo->pMapTnlInfo != NULL)
                    && (pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
                        u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
                {
                    MEMSET (&RpteEnqueueMsgInfo, 0,
                            sizeof (tRpteEnqueueMsgInfo));

                    RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo =
                        pRsvpTeTnlInfo->pMapTnlInfo;

                    RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
                }

                if (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) ==
                    RPTE_FRR_PROTECTED_TNL)
                {
                    if ((RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL) &&
                        (PSB_RPTE_FRR_FAST_REROUTE
                         (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).u1Flags ==
                         RSVPTE_TNL_FRR_FACILITY_METHOD))
                    {
                        if (!(RPTE_IS_MP (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
                            &&
                            !(RPTE_IS_PLR
                              (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo))))
                        {
                            pRsvpTeTnlInfo->pPsb->u4TimeToDie =
                                RpteUtlComputeLifeTime (pNbrEntry->pIfEntry,
                                                        PSB_IN_REFRESH_INTERVAL
                                                        (RSVPTE_TNL_PSB
                                                         (pRsvpTeTnlInfo)));
                            RSVPTE_DBG5 (RSVPTE_NBR_DBG,
                                         "Facility FRR  Tnl %d %d %x %x handled for status change %d Downstream Lost\n",
                                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                                         OSIX_NTOHL (RSVPTE_TNL_INGRESS
                                                     (pRsvpTeTnlInfo)),
                                         OSIX_NTOHL (RSVPTE_TNL_EGRESS
                                                     (pRsvpTeTnlInfo)),
                                         u4Status);

                            pRsvpTeTnlInfo->
                                b1IsPsbTimeToDieIncreasedForProtTunnel =
                                RPTE_TRUE;
                            continue;
                        }
                    }
                }
                MEMSET (&RpteEnqueueMsgInfo, 0, sizeof (tRpteEnqueueMsgInfo));

                RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pRsvpTeTnlInfo;

                RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
            }
            else
            {
                if (pNbrEntry->u4NbrStatus == u4Status)
                {
                    return;
                }

                if ((u1IsIfEvent == TRUE) && (u4Status == RPTE_DISABLED) &&
                    ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
                     || (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) ==
                         RPTE_EGRESS))
                    &&
                    ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                      i4E2EProtectionType == MPLS_TE_DEDICATED_ONE2ONE)
                     && (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse !=
                         LOCAL_PROT_NOT_AVAIL)))
                {
                    RpteNhProcessOneToOneTnls (pRsvpTeTnlInfo, RPTE_ZERO,
                                               u2ErrValue);
                    continue;
                }
                if (u2ErrValue == RPTE_LSP_RECOVERED)
                {
                    pRsvpTeTnlInfo->u1NotifyMsgProtState = RPTE_ZERO;
                }
                RpteNhHandlePsbOrRsbOrHelloTimeOut (pRsvpTeTnlInfo, u2ErrValue);

            }

        }
    }

    /* Up stream tunnels associated with the Neighbor is deleted here. */
    RSVPTE_DBG1 (RSVPTE_NBR_DBG,
                 "Upstream tnls associated with Nbr %x processed\n",
                 NBR_ENTRY_ADDR (pNbrEntry));

    UTL_DLL_OFFSET_SCAN (&(pNbrEntry->UpStrTnlList), pRsvpTeTnlInfo,
                         pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
    {
        if ((u4Status == RPTE_ENABLED) &&
            (u1IsIfEvent == RPTE_TRUE) &&
            (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL) &&
            ((PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
              u1Flags) == RSVPTE_TNL_FRR_FACILITY_METHOD))
        {
            if (RpteUtlIsNodeProtDesired (pRsvpTeTnlInfo) != RPTE_YES)
            {
                RpteHhGenerateHello (pNbrEntry,
                                     RPTE_HELLO_REQ_CTYPE, pNbrEntry->pIfEntry);
            }
            continue;
        }

        if ((u4DataTeIfIndex != RPTE_ZERO) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf != RPTE_ZERO) &&
            (u4DataTeIfIndex !=
             pRsvpTeTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf))
        {
            continue;
        }
        /* Set the Tunnel status as not synchronized at TE level */
        if (pNbrEntry->u1GrProgressState == RPTE_GR_RESTART_IN_PROGRESS)
        {
            TeSigSetGrSynchronizedStatus (pRsvpTeTnlInfo->pTeTnlInfo,
                                          TNL_GR_NOT_SYNCHRONIZED);
            continue;
        }
        u4PsbNotifyAddr = RPTE_ZERO;
        u4RsbNotifyAddr = RPTE_ZERO;

        if (pRsvpTeTnlInfo->pPsb == NULL)
        {
            continue;
        }

        u4PsbNotifyAddr = pRsvpTeTnlInfo->pPsb->u4NotifyAddr;

        if (pRsvpTeTnlInfo->pRsb != NULL)
        {
            u4RsbNotifyAddr = pRsvpTeTnlInfo->pRsb->u4NotifyAddr;
        }
        if ((u4Status == RPTE_ENABLED) && (u4PsbNotifyAddr == RPTE_ZERO) &&
            (u4RsbNotifyAddr == RPTE_ZERO))
        {
            continue;
        }
        if (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL)
        {
            if (PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)).
                u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD)
            {
                if (!(RPTE_IS_MP (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo)))
                    && !(RPTE_IS_PLR (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo))))
                {
                    pRsvpTeTnlInfo->pPsb->u4TimeToDie =
                        RpteUtlComputeLifeTime (pNbrEntry->pIfEntry,
                                                PSB_IN_REFRESH_INTERVAL
                                                (RSVPTE_TNL_PSB
                                                 (pRsvpTeTnlInfo)));
                    RSVPTE_DBG5 (RSVPTE_NBR_DBG,
                                 "Facility FRR  Tnl %d %d %x %x handled for status change %d Upstream Lost\n",
                                 RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                                 RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                                 OSIX_NTOHL (RSVPTE_TNL_INGRESS
                                             (pRsvpTeTnlInfo)),
                                 OSIX_NTOHL (RSVPTE_TNL_EGRESS
                                             (pRsvpTeTnlInfo)), u4Status);

                    pRsvpTeTnlInfo->b1IsPsbTimeToDieIncreasedForProtTunnel =
                        RPTE_TRUE;
                    continue;
                }
            }
        }
        if ((RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_PROTECTED_TNL)
            && (RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo) != NULL))
        {
            RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) |= RPTE_FRR_NODE_STATE_UPLOST;

            pRsvpTeTnlInfo->pPsb->u4TimeToDie =
                RpteUtlComputeLifeTime (pNbrEntry->pIfEntry,
                                        PSB_IN_REFRESH_INTERVAL
                                        (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)));

            RSVPTE_DBG5 (RSVPTE_NBR_DBG,
                         "FRR Tnl %d %d %x %x handled for status change %d Upstream Lost\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                         u4Status);
            continue;
        }
        else if (RPTE_FRR_TNL_INFO_TYPE (pRsvpTeTnlInfo) == RPTE_FRR_BACKUP_TNL)
        {
            RSVPTE_DBG4 (RSVPTE_NBR_DBG,
                         "FRR Bkp Tnl %d %d %x %x handled for disabled status "
                         "change\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));

            RpteHandlePsbOrHelloTimeOutForFrr (pRsvpTeTnlInfo);
        }
        else
        {
            RSVPTE_DBG5 (RSVPTE_NBR_DBG,
                         "Normal Tnl %d %d %x %x handled for status "
                         "change - to: %u\n",
                         RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                         RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                         OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                         OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)),
                         u4Status);

            if (((u4PsbNotifyAddr == RPTE_ZERO) &&
                 (u4RsbNotifyAddr == RPTE_ZERO)) ||
                ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                  i4E2EProtectionType == MPLS_TE_DEDICATED_ONE2ONE)
                 && (u4Status == RPTE_DISABLED)
                 &&
                 ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                   LOCAL_PROT_NOT_AVAIL)
                  || (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                      LOCAL_PROT_NOT_APPLICABLE))))
            {
                if ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType ==
                     RPTE_TNL_PROTECTION_PATH)
                    && (pRsvpTeTnlInfo->pMapTnlInfo != NULL)
                    && (pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
                        u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
                {
                    MEMSET (&RpteEnqueueMsgInfo, 0,
                            sizeof (tRpteEnqueueMsgInfo));

                    RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo =
                        pRsvpTeTnlInfo->pMapTnlInfo;

                    RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
                }

                MEMSET (&RpteEnqueueMsgInfo, 0, sizeof (tRpteEnqueueMsgInfo));

                RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pRsvpTeTnlInfo;

                RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
            }
            else
            {
                if (pNbrEntry->u4NbrStatus == u4Status)
                {
                    return;
                }
                if ((u1IsIfEvent == TRUE) && (u4Status == RPTE_DISABLED) &&
                    ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
                     || (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) ==
                         RPTE_EGRESS))
                    &&
                    ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                      i4E2EProtectionType == MPLS_TE_DEDICATED_ONE2ONE)
                     && (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse !=
                         LOCAL_PROT_NOT_AVAIL)))
                {
                    RpteNhProcessOneToOneTnls (pRsvpTeTnlInfo, RPTE_ZERO,
                                               u2ErrValue);
                    continue;
                }
                if (u2ErrValue == RPTE_LSP_RECOVERED)
                {
                    pRsvpTeTnlInfo->u1NotifyMsgProtState = RPTE_ZERO;
                }

                if ((u1IsIfEvent == TRUE) && (u4Status == RPTE_DISABLED) &&
                    ((RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
                     || (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) ==
                         RPTE_EGRESS))
                    &&
                    ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                      i4E2EProtectionType == MPLS_TE_DEDICATED_ONE2ONE)
                     && (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse !=
                         LOCAL_PROT_NOT_AVAIL)))
                {
                    pRsvpTeTnlInfo->u1NotifyMsgProtState = RPTE_ZERO;
                    RpteNhProcessOneToOneTnls (pRsvpTeTnlInfo, RPTE_ZERO,
                                               u2ErrValue);
                    continue;
                }
                if (u2ErrValue == RPTE_LSP_RECOVERED)
                {
                    pRsvpTeTnlInfo->u1NotifyMsgProtState = RPTE_ZERO;
                }
                RpteNhHandlePsbOrRsbOrHelloTimeOut (pRsvpTeTnlInfo, u2ErrValue);
            }
        }
    }
    pNbrEntry->u4NbrStatus = u4Status;

    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteHandleNbrStatusChange Exit\n");
}

/************************************************************************
 *  Function Name   : RpteHhSetNbrHelloActive
 *  Description     : Function used to set the neighbor entry hello active state
 *  Input           : pNbrEntry        - Pointer to neighbor entry
 *                    pIfEntry         - Pointer to interface entry
 *                    u1Flag           - Flag value
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
RpteHhSetNbrHelloActive (tNbrEntry * pNbrEntry, tIfEntry * pIfEntry,
                         UINT1 u1Flag)
{
    tNbrEntry          *pTempNbrEntry = NULL;
    tIfEntry           *pTempIfEntry = NULL;
    tTMO_SLL           *pTempNbrList = NULL;
    UINT4               u4HashIndex;

    if (pIfEntry->u4Addr != RPTE_ZERO)
    {
        return;
    }

    /* check whether this neighbor is alredy present in some other interface
     * entry list. If present, set bIsHelloActive as true. This flag will be
     * checked in hello msg send thread. If it is set, hello msg will not be
     * sent for this neighbor. In case of deletion, set the another interface's
     * neighbor entry hello state as active.
     * This code is done for mapping the requirement There shall be only one 
     * node id based hello message session between a pair of nodes. This 
     * generally happens when more than one unnumbered link exist between a two nodes
     * */

    TMO_HASH_Scan_Table (RSVP_GBL_IF_HSH_TBL, u4HashIndex)
    {
        TMO_HASH_Scan_Bucket (RSVP_GBL_IF_HSH_TBL, u4HashIndex, pTempIfEntry,
                              tIfEntry *)
        {
            if (pIfEntry->u4IfIndex == pTempIfEntry->u4IfIndex)
            {
                continue;
            }
            if (pTempIfEntry->u4Addr != RPTE_ZERO)
            {
                continue;
            }
            pTempNbrList = &(IF_ENTRY_NBR_LIST (pTempIfEntry));

            TMO_SLL_Scan (pTempNbrList, pTempNbrEntry, tNbrEntry *)
            {
                if ((NBR_ENTRY_STATUS (pTempNbrEntry) == ACTIVE) &&
                    (NBR_ENTRY_ADDR (pNbrEntry) ==
                     NBR_ENTRY_ADDR (pTempNbrEntry)))
                {
                    if (u1Flag == RPTE_FALSE)
                    {
                        pTempNbrEntry->bIsHelloActive = RPTE_TRUE;
                        return;
                    }
                    else
                    {
                        pNbrEntry->bIsHelloActive = RPTE_FALSE;
                    }
                    break;
                }
            }
        }
    }
    return;
}

/************************************************************************
 *  Function Name   : RpteHhSetNbrHelloState
 *  Description     : Function used to set the neighbor entry hello state
 *  Input           : pNbrEntry        - Pointer to neighbor entry
 *                    pIfEntry         - Pointer to interface entry
 *                    u1Flag           - Flag value
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
RpteHhSetNbrHelloState (tNbrEntry * pNbrEntry)
{
    tHelloNbrInfo      *pHelloNbrInfo = NULL;

    RSVPTE_DBG1 (RSVPTE_NBR_DBG, "RpteHhSetNbrHelloState Entry for Nbr: 0x%x\n",
                 NBR_ENTRY_ADDR (pNbrEntry));

    pHelloNbrInfo = &pNbrEntry->HelloNbrInfo;
    pHelloNbrInfo->u1HelloState = HELLO_SUPPRT_RESET;

    RSVPTE_DBG (RSVPTE_NBR_DBG, "RpteHhSetNbrHelloState Exit\n");
    return;
}

/*---------------------------------------------------------------------------*/
/*                       End of file rptehelo.c                              */
/*---------------------------------------------------------------------------*/
