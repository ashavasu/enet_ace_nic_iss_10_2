
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rpteset.c,v 1.40 2018/01/03 11:31:23 siva Exp $
 ********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rpteset.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVPTE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  : 
 *    DESCRIPTION            : This file contains the low level SET routines
 *                             for the MIB objects present in the rsvpte.mib
 *                             and Set Routines for Scalar Objects in 
 *                             fsmpfrr.mib.
 *---------------------------------------------------------------------------*/

# include  "rpteincs.h"
# include  "fsmpfrlw.h"

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfLblSpace
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfLblSpace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfLblSpace (INT4 i4FsMplsRsvpTeIfIndex,
                              INT4 i4SetValFsMplsRsvpTeIfLblSpace)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS)
        && ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
            (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        RPTE_IF_LBL_SPACE (pIfEntry) = (UINT1) i4SetValFsMplsRsvpTeIfLblSpace;
        RPTE_IF_LBL_FLAGS (pIfEntry) |= IF_LBL_SPACE_SET;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfLblType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfLblType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfLblType (INT4 i4FsMplsRsvpTeIfIndex,
                             INT4 i4SetValFsMplsRsvpTeIfLblType)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS)
        && ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
            (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        RPTE_IF_LBL_TYPE (pIfEntry) = (UINT1) i4SetValFsMplsRsvpTeIfLblType;
        RPTE_IF_LBL_FLAGS (pIfEntry) |= IF_LBL_TYPE_SET;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeAtmMergeCap
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeAtmMergeCap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeAtmMergeCap (INT4 i4FsMplsRsvpTeIfIndex,
                               INT4 i4SetValFsMplsRsvpTeAtmMergeCap)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS) &&
        (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY))
    {
        RPTE_IF_ATM_MERGE_CAP (pIfEntry) =
            (UINT1) i4SetValFsMplsRsvpTeAtmMergeCap;
        RPTE_IF_LBL_FLAGS (pIfEntry) |= IF_ATM_MERGE_CAP_SET;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeAtmVcDirection
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeAtmVcDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeAtmVcDirection (INT4 i4FsMplsRsvpTeIfIndex,
                                  INT4 i4SetValFsMplsRsvpTeAtmVcDirection)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS) &&
        (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY))
    {
        RPTE_IF_ATM_VC_DIR (pIfEntry) =
            (UINT1) i4SetValFsMplsRsvpTeAtmVcDirection;
        RPTE_IF_LBL_FLAGS (pIfEntry) |= IF_ATM_VC_DIR_SET;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeAtmMinVpi
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeAtmMinVpi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeAtmMinVpi (INT4 i4FsMplsRsvpTeIfIndex,
                             INT4 i4SetValFsMplsRsvpTeAtmMinVpi)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS) &&
        (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY))
    {
        RPTE_IF_MINVPI (pIfEntry) = (UINT2) i4SetValFsMplsRsvpTeAtmMinVpi;
        RPTE_IF_LBL_FLAGS (pIfEntry) |= IF_LBL_MINVPI_SET;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeAtmMinVci
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeAtmMinVci
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeAtmMinVci (INT4 i4FsMplsRsvpTeIfIndex,
                             INT4 i4SetValFsMplsRsvpTeAtmMinVci)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS) &&
        (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY))
    {
        RPTE_IF_MINVCI (pIfEntry) = (UINT2) i4SetValFsMplsRsvpTeAtmMinVci;
        RPTE_IF_LBL_FLAGS (pIfEntry) |= IF_LBL_MINVCI_SET;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeAtmMaxVpi
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeAtmMaxVpi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeAtmMaxVpi (INT4 i4FsMplsRsvpTeIfIndex,
                             INT4 i4SetValFsMplsRsvpTeAtmMaxVpi)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS) &&
        (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY))
    {
        RPTE_IF_MAXVPI (pIfEntry) = (UINT2) i4SetValFsMplsRsvpTeAtmMaxVpi;
        RPTE_IF_LBL_FLAGS (pIfEntry) |= IF_LBL_MAXVPI_SET;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeAtmMaxVci
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeAtmMaxVci
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeAtmMaxVci (INT4 i4FsMplsRsvpTeIfIndex,
                             INT4 i4SetValFsMplsRsvpTeAtmMaxVci)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS) &&
        (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY))
    {
        RPTE_IF_MAXVCI (pIfEntry) = (UINT2) i4SetValFsMplsRsvpTeAtmMaxVci;
        RPTE_IF_LBL_FLAGS (pIfEntry) |= IF_LBL_MAXVCI_SET;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfMtu
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfMtu
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfMtu (INT4 i4FsMplsRsvpTeIfIndex,
                         INT4 i4SetValFsMplsRsvpTeIfMtu)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS)
        && ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
            (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        RPTE_IF_MAX_MTU (pIfEntry) = (UINT4) i4SetValFsMplsRsvpTeIfMtu;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfRefreshMultiple
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfRefreshMultiple
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfRefreshMultiple (INT4 i4FsMplsRsvpTeIfIndex,
                                     INT4 i4SetValFsMplsRsvpTeIfRefreshMultiple)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS)
        && ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
            (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        IF_ENTRY_REFRESH_MULTIPLE (pIfEntry) =
            (UINT2) i4SetValFsMplsRsvpTeIfRefreshMultiple;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfTTL
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfTTL
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfTTL (INT4 i4FsMplsRsvpTeIfIndex,
                         INT4 i4SetValFsMplsRsvpTeIfTTL)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS)
        && ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
            (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        IF_ENTRY_TTL (pIfEntry) = (UINT1) i4SetValFsMplsRsvpTeIfTTL;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfRefreshInterval
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfRefreshInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfRefreshInterval (INT4 i4FsMplsRsvpTeIfIndex,
                                     INT4 i4SetValFsMplsRsvpTeIfRefreshInterval)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS)
        && ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
            (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        IF_ENTRY_REFRESH_INTERVAL (pIfEntry) =
            (UINT4) i4SetValFsMplsRsvpTeIfRefreshInterval;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfRouteDelay
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfRouteDelay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfRouteDelay (INT4 i4FsMplsRsvpTeIfIndex,
                                INT4 i4SetValFsMplsRsvpTeIfRouteDelay)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS)
        && ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
            (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        IF_ENTRY_ROUTE_DELAY (pIfEntry) =
            (UINT4) i4SetValFsMplsRsvpTeIfRouteDelay;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfUdpRequired
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfUdpRequired
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfUdpRequired (INT4 i4FsMplsRsvpTeIfIndex,
                                 INT4 i4SetValFsMplsRsvpTeIfUdpRequired)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS)
        && ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
            (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        IF_ENTRY_UDP_REQUIRED (pIfEntry) =
            (UINT1) i4SetValFsMplsRsvpTeIfUdpRequired;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfHelloSupported
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfHelloSupported
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfHelloSupported (INT4 i4FsMplsRsvpTeIfIndex,
                                    INT4 i4SetValFsMplsRsvpTeIfHelloSupported)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if ((u1RetVal == RPTE_SUCCESS)
        && ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
            (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        IF_ENTRY_HELLO_SPRTD (pIfEntry) =
            (UINT1) i4SetValFsMplsRsvpTeIfHelloSupported;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfLinkAttr
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfLinkAttr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfLinkAttr (INT4 i4FsMplsRsvpTeIfIndex,
                              UINT4 u4SetValFsMplsRsvpTeIfLinkAttr)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        IF_ENTRY_LINK_ATTR (pIfEntry) = u4SetValFsMplsRsvpTeIfLinkAttr;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfStatus
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfStatus (INT4 i4FsMplsRsvpTeIfIndex,
                            INT4 i4SetValFsMplsRsvpTeIfStatus)
{
    UINT2               u2LblRngCount = 1;
    /* NOTE: Only one set of ATM Lbl Rng Supported */
    tIfEntry           *pIfEntry = NULL;
    tKeyInfoStruct      LblRangeInfo;
    UINT4               u4HashIndex;
    UINT1               u1RetVal = RPTE_FAILURE;
    UINT4               u4MinLabel;
    UINT4               u4MaxLabel;
    tTMO_DLL_NODE      *pIfTnlsNode = NULL;

    UINT4               u4InterfaceIndex;
    tCfaIfInfo          IfInfo;

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        return RPTE_FAILURE;
    }
    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);

    switch (i4SetValFsMplsRsvpTeIfStatus)
    {
        case RPTE_ACTIVE:

            if (u1RetVal == RPTE_FAILURE)
            {
                return SNMP_FAILURE;
            }
            if ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
                (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE))
            {
                if (RpteIpGetIfAddr (i4FsMplsRsvpTeIfIndex,
                                     &IF_ENTRY_ADDR (pIfEntry)) != RPTE_SUCCESS)
                {
                    /* Failed to Find an Entry in IP  */
                    RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                                "Failed : IF Entry Creation Fail - "
                                "Entry not found in IP.\n");
                    return SNMP_FAILURE;
                }

                if (((RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ETHERNET) &&
                     (RPTE_IF_LBL_SPACE (pIfEntry) != RPTE_PERPLATFORM)) ||
                    ((RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM) &&
                     (RPTE_IF_LBL_SPACE (pIfEntry) != RPTE_PERINTERFACE)))
                {
                    RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                                "Failed : Illegal Configuration during IF "
                                "Entry Creation.\n");
                    return SNMP_FAILURE;
                }

                IF_ENTRY_MASK (pIfEntry) =
                    RpteIpGetIfMask ((UINT4) i4FsMplsRsvpTeIfIndex);
                RpteIpGetIfTtl (pIfEntry, &IF_ENTRY_TTL (pIfEntry));

                MEMSET (&IF_ENTRY_IF_ID (pIfEntry), RPTE_ZERO,
                        sizeof (tCRU_INTERFACE));
                RpteIpGetIfId (i4FsMplsRsvpTeIfIndex,
                               &IF_ENTRY_IF_ID (pIfEntry));

                if ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) &&
                    (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM))
                {

                    LblRangeInfo.u4Key1Min = RPTE_IF_ENTRY_MIN_VPI (pIfEntry);
                    LblRangeInfo.u4Key1Max = RPTE_IF_ENTRY_MAX_VPI (pIfEntry);
                    LblRangeInfo.u4Key2Min = RPTE_IF_ENTRY_MIN_VCI (pIfEntry);
                    LblRangeInfo.u4Key2Max = RPTE_IF_ENTRY_MAX_VCI (pIfEntry);

                    /* Interface Index Value is assigned  */

                    u4InterfaceIndex = pIfEntry->u4IfIndex;

                    /* Allocating Label Resources for ATM Lbl Range */

                    if (RPTE_MPLS_LBLMGR_CREATE_LBLSPACE
                        (RPTE_LBL_ALLOC_BOTH_NUM, &LblRangeInfo, u2LblRngCount,
                         RSVPTE_LBL_MODULE_ID, u4InterfaceIndex,
                         &RPTE_IF_ENTRY_LBL_SPACE_ID (pIfEntry)) ==
                        RPTE_LBL_MNGR_FAILURE)
                    {
                        /* Failed to Allocate Label Space  */
                        RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                                    "Failed : IF Entry Creation Fail - "
                                    "Label Space Alloc Fail.\n");
                        return SNMP_FAILURE;
                    }

                    /* Copying Min Val of label space */
                    u4MinLabel = RPTE_IF_ENTRY_MIN_VCI (pIfEntry);
                    u4MinLabel = u4MinLabel << 16;
                    u4MinLabel |= RPTE_IF_ENTRY_MIN_VPI (pIfEntry);

                    /* Copying Max val of label space */
                    u4MaxLabel = RPTE_IF_ENTRY_MAX_VCI (pIfEntry);
                    u4MaxLabel = u4MaxLabel << 16;
                    u4MaxLabel |= RPTE_IF_ENTRY_MAX_VPI (pIfEntry);
                }

                if (CfaGetIfInfo ((UINT2) i4FsMplsRsvpTeIfIndex,
                                  &IfInfo) != CFA_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS,
                                "Success : IfEntry Status get failed\r\n");
                    return SNMP_FAILURE;
                }

                if (IfInfo.u1IfOperStatus == CFA_IF_UP)
                {
                    RsvpIfStChgEventHandler ((UINT4) i4FsMplsRsvpTeIfIndex,
                                             IF_ENTRY_ADDR (pIfEntry),
                                             RPTE_IF_UP, RPTE_ZERO);
                }
                IF_ENTRY_STATUS (pIfEntry) = RPTE_ACTIVE;
                RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS,
                            "Success : IfEntry Row Status made active.\n");
                return SNMP_SUCCESS;
            }
            else if (IF_ENTRY_STATUS (pIfEntry) == RPTE_ACTIVE)
            {
                RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS,
                            "Success : IfEntry Row Status made active.\n");
                return SNMP_SUCCESS;
            }
            break;

        case RPTE_CREATEANDWAIT:

            if (u1RetVal == RPTE_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            /* Allocating memory for IfEntry */
            pIfEntry = (tIfEntry *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_IF_ENTRY_POOL_ID);

            if (pIfEntry == NULL)
            {
                RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                            "Failed : IF Entry Creation Fail - "
                            "Mem Alloc Fail.\n");
                return SNMP_FAILURE;
            }
            RpteInitIfEntry (pIfEntry);
            IF_ENTRY_IF_INDEX (pIfEntry) = (UINT4) i4FsMplsRsvpTeIfIndex;

            u4HashIndex =
                RSVPTE_COMPUTE_HASH_INDEX ((UINT4) i4FsMplsRsvpTeIfIndex);
            u1RetVal =
                HASHInsertInOrder (GBL_IF_ENTRY_OFFSET_TBL,
                                   GBL_IF_ENTRY_LENGTH_TBL,
                                   GBL_IF_ENTRY_BYTE_ORDER_TBL,
                                   RPTE_IF_ENTRY_NUM_INDICES,
                                   RSVP_GBL_IF_HSH_TBL, pIfEntry, u4HashIndex,
                                   GBL_IF_ENTRY_NODE_OFFSET);
            if (u1RetVal == MPLS_FAILURE)
            {
                RSVP_RELEASE_MEM_BLOCK (RSVPTE_IF_ENTRY_POOL_ID,
                                        (UINT1 *) pIfEntry);
                RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                            "Failed : IF Entry Creation Fail - "
                            "Hash Insertion Fail.\n");
                return SNMP_FAILURE;
            }

            IF_ENTRY_STATUS (pIfEntry) = RPTE_NOTREADY;
            RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS,
                        "Success : If Entry Creation - createAndWait\n");
            KW_FALSEPOSITIVE_FIX (pIfEntry);
            return SNMP_SUCCESS;

        case RPTE_NOTINSERVICE:
            if (u1RetVal == RPTE_FAILURE)
            {
                return SNMP_FAILURE;
            }
            RsvpIfStChgEventHandler ((UINT4) i4FsMplsRsvpTeIfIndex,
                                     IF_ENTRY_ADDR (pIfEntry), RPTE_IF_DOWN,
                                     RPTE_ZERO);
            IF_ENTRY_STATUS (pIfEntry) = RPTE_NOTINSERVICE;
            return SNMP_SUCCESS;

        case RPTE_CREATEANDGO:
            RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                        "Failed : If Entry assign - CreateAndGo Not "
                        "supported.\n");
            return SNMP_FAILURE;

        case RPTE_DESTROY:

            if (u1RetVal == RPTE_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (((IF_ENTRY_STATUS (pIfEntry) == RPTE_ACTIVE) ||
                 (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)) &&
                (RPTE_IF_NUM_TNL_ASSOC (pIfEntry) != RSVPTE_ZERO))
            {
                return SNMP_FAILURE;
            }

            if (TMO_DLL_Count (&IF_ENTRY_FAC_TNL_LIST (pIfEntry)) != RPTE_ZERO)
            {
                pIfTnlsNode = TMO_DLL_First (&IF_ENTRY_FAC_TNL_LIST (pIfEntry));
                while (pIfTnlsNode != NULL)
                {
                    ((tFacilityRpteBkpTnl *) (pIfTnlsNode))->pRsvpTeTnlInfo =
                        NULL;
                    pIfTnlsNode =
                        TMO_DLL_First (&IF_ENTRY_FAC_TNL_LIST (pIfEntry));
                }
            }
            if (((IF_ENTRY_STATUS (pIfEntry) == RPTE_ACTIVE) ||
                 (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)) &&
                (RPTE_IF_NUM_TNL_ASSOC (pIfEntry) == RSVPTE_ZERO))
            {
                if (RPTE_IF_LBL_TYPE (pIfEntry) == RPTE_ATM)
                {
                    RPTE_MPLS_LBLMGR_DELETE_LBLSPACE (RPTE_IF_ENTRY_LBL_SPACE_ID
                                                      (pIfEntry));
                }
                u4HashIndex = RSVPTE_COMPUTE_HASH_INDEX (IF_ENTRY_IF_INDEX
                                                         (pIfEntry));
                TMO_HASH_Delete_Node (RSVP_GBL_IF_HSH_TBL,
                                      &(pIfEntry->NextIfHashNode), u4HashIndex);
                MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID,
                                    (UINT1 *) pIfEntry);
                RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS,
                            "Success : If Entry prsnt and active - "
                            "destroyed \n");
                return SNMP_SUCCESS;
            }
            else if (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY)
            {
                u4HashIndex = RSVPTE_COMPUTE_HASH_INDEX (IF_ENTRY_IF_INDEX
                                                         (pIfEntry));
                TMO_HASH_Delete_Node (RSVP_GBL_IF_HSH_TBL,
                                      &(pIfEntry->NextIfHashNode), u4HashIndex);
                MemReleaseMemBlock (RSVPTE_IF_ENTRY_POOL_ID,
                                    (UINT1 *) pIfEntry);
                RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS,
                            "Success : If Entry prsnt and notready - "
                            "destroyed \n");
                return SNMP_SUCCESS;
            }

        default:
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfPlrId
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                setValFsMplsRsvpTeIfPlrId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfPlrId (INT4 i4FsMplsRsvpTeIfIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValFsMplsRsvpTeIfPlrId)
{
    UINT1               u1RetVal = SNMP_FAILURE;
    tIfEntry           *pIfEntry = NULL;

    if (pSetValFsMplsRsvpTeIfPlrId->i4_Length != sizeof (UINT4))
    {
        return RPTE_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        return RPTE_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);

    if (u1RetVal == RPTE_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_OCTETSTRING_TO_INTEGER (pSetValFsMplsRsvpTeIfPlrId,
                                 IF_ENTRY_PLR_ID (pIfEntry));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfAvoidNodeId
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                setValFsMplsRsvpTeIfAvoidNodeId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfAvoidNodeId (INT4 i4FsMplsRsvpTeIfIndex,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pSetValFsMplsRsvpTeIfAvoidNodeId)
{
    UINT1               u1RetVal = SNMP_FAILURE;
    tIfEntry           *pIfEntry = NULL;

    if (pSetValFsMplsRsvpTeIfAvoidNodeId->i4_Length != sizeof (UINT4))
    {
        return RPTE_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        return RPTE_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);

    if (u1RetVal == RPTE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MPLS_OCTETSTRING_TO_INTEGER (pSetValFsMplsRsvpTeIfAvoidNodeId,
                                 IF_ENTRY_AVOID_NODE_ID (pIfEntry));
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeIfStorageType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                setValFsMplsRsvpTeIfStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeIfStorageType (INT4 i4FsMplsRsvpTeIfIndex,
                                 INT4 i4SetValFsMplsRsvpTeIfStorageType)
{
    UNUSED_PARAM (i4FsMplsRsvpTeIfIndex);
    UNUSED_PARAM (i4SetValFsMplsRsvpTeIfStorageType);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeIfTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeIfTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeLsrID
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeLsrID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeLsrID (tSNMP_OCTET_STRING_TYPE * pSetValFsMplsRsvpTeLsrID)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        CPY_FROM_SNMP (RSVPTE_LSR_ID (gpRsvpTeGblInfo),
                       pSetValFsMplsRsvpTeLsrID, RSVPTE_IPV4ADR_LEN);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeHelloSupprtd
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeHelloSupprtd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeHelloSupprtd (INT4 i4SetValFsMplsRsvpTeHelloSupprtd)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        if (i4SetValFsMplsRsvpTeHelloSupprtd == RPTE_SNMP_TRUE)
        {
            RSVPTE_HELLO_SPRTD (gpRsvpTeGblInfo) = RPTE_TRUE;
        }
        else if (i4SetValFsMplsRsvpTeHelloSupprtd == RPTE_SNMP_FALSE)
        {
            RSVPTE_HELLO_SPRTD (gpRsvpTeGblInfo) = RPTE_FALSE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeSockSupprtd
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeSockSupprtd
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeSockSupprtd (INT4 i4SetValFsMplsRsvpTeSockSupprtd)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        if (i4SetValFsMplsRsvpTeSockSupprtd == RPTE_SNMP_TRUE)
        {
            RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) = RPTE_TRUE;
        }
        else if (i4SetValFsMplsRsvpTeSockSupprtd == RPTE_SNMP_FALSE)
        {
            RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) = RPTE_FALSE;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGenLblSpaceMinLbl
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGenLblSpaceMinLbl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGenLblSpaceMinLbl (INT4 i4SetValFsMplsRsvpTeGenLblSpaceMinLbl)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_GENLBL_MINLBL (gpRsvpTeGblInfo) =
            (UINT4) i4SetValFsMplsRsvpTeGenLblSpaceMinLbl;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGenLblSpaceMaxLbl
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGenLblSpaceMaxLbl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGenLblSpaceMaxLbl (INT4 i4SetValFsMplsRsvpTeGenLblSpaceMaxLbl)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_GENLBL_MAXLBL (gpRsvpTeGblInfo) =
            (UINT4) i4SetValFsMplsRsvpTeGenLblSpaceMaxLbl;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGenDebugFlag
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGenDebugFlag
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGenDebugFlag (UINT4 u4SetValFsMplsRsvpTeGenDebugFlag)
{
    RSVPTE_DBG_FLAG = u4SetValFsMplsRsvpTeGenDebugFlag;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGenPduDumpLevel
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGenPduDumpLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGenPduDumpLevel (INT4 i4SetValFsMplsRsvpTeGenPduDumpLevel)
{
    RSVPTE_DUMP_LVL (gpRsvpTeGblInfo) =
        (UINT4) i4SetValFsMplsRsvpTeGenPduDumpLevel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGenPduDumpMsgType
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGenPduDumpMsgType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGenPduDumpMsgType (INT4 i4SetValFsMplsRsvpTeGenPduDumpMsgType)
{
    RSVPTE_DUMP_TYPE (gpRsvpTeGblInfo) =
        (UINT4) i4SetValFsMplsRsvpTeGenPduDumpMsgType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGenPduDumpDirection
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGenPduDumpDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGenPduDumpDirection (INT4
                                       i4SetValFsMplsRsvpTeGenPduDumpDirection)
{
    RSVPTE_DUMP_DIR (gpRsvpTeGblInfo) =
        (UINT4) i4SetValFsMplsRsvpTeGenPduDumpDirection;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeOperStatus
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeOperStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeOperStatus (INT4 i4SetValFsMplsRsvpTeOperStatus)
{
    UINT1               au1ZeroVal[ROUTER_ID_LENGTH];

    MEMSET (au1ZeroVal, 0, ROUTER_ID_LENGTH);

    switch (i4SetValFsMplsRsvpTeOperStatus)
    {
        case RPTE_ADMIN_UP:
            if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
            {
                if (MEMCMP (RSVPTE_LSR_ID (gpRsvpTeGblInfo), au1ZeroVal,
                            ROUTER_ID_LENGTH) == MPLS_ZERO)
                {
                    RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                                "Failed : Router ID is ZERO.\n");
                    return SNMP_FAILURE;
                }
                if (gu1RRCapable == RPTE_ENABLED)
                {
                    if (gu1MsgIdCapable != RPTE_ENABLED)
                    {
                        gu1MsgIdCapable = RPTE_ENABLED;
                        gu1IntMsgIdCapable = RPTE_TRUE;
                    }

                }
                RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) = RPTE_ADMIN_UP_IN_PRGRS;
                if (RpteProcessAdminUpEvent () == RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                                "Failed : RsvpTe Oper Status admin up.\n");
                    return SNMP_FAILURE;
                }
                RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS, "Success : Admin UP.\n");
                return SNMP_SUCCESS;
            }
            else if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) ==
                     RPTE_ADMIN_UP_IN_PRGRS)
            {
                RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                            "Success : Admin up In Progress - "
                            "Not allowed to set.\n");
                return SNMP_FAILURE;

            }
            else
            {
                RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS,
                            "Success : Admin up - No action.\n");
                return SNMP_SUCCESS;
            }

        case RPTE_ADMIN_DOWN:

            if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_UP)
            {
                RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) =
                    RPTE_ADMIN_DOWN_IN_PRGRS;
                if (RpteProcessAdminDownEvent () == RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                                "Failed : RsvpTe Oper Status admin down.\n");
                    return SNMP_FAILURE;
                }
                RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS, "Success : Admin down.\n");
                return SNMP_SUCCESS;
            }
            else if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) ==
                     RPTE_ADMIN_DOWN_IN_PRGRS)
            {
                RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                            "Success : Admin down In Progress - "
                            "Not allowed to set.\n");
                return SNMP_FAILURE;

            }
            else
            {
                RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS, "Success : Admin down.\n");
                return SNMP_SUCCESS;
            }
        default:
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeOverRideOption
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeOverRideOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeOverRideOption (INT4 i4SetValFsMplsRsvpTeOverRideOption)
{
    RSVPTE_DS_OVER_RIDE = (UINT4) i4SetValFsMplsRsvpTeOverRideOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeMaxTnls
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeMaxTnls
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeMaxTnls (INT4 i4SetValFsMplsRsvpTeMaxTnls)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_MAX_RSVPTETNL (gpRsvpTeGblInfo) =
            (UINT2) i4SetValFsMplsRsvpTeMaxTnls;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeMaxErhopsPerTnl
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeMaxErhopsPerTnl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeMaxErhopsPerTnl (INT4 i4SetValFsMplsRsvpTeMaxErhopsPerTnl)
{
    /* This check is added because, At the time of mib restore if it gets value
     * zero then it should be changed to the default value */
    if (i4SetValFsMplsRsvpTeMaxErhopsPerTnl == RSVPTE_ZERO)
    {
        i4SetValFsMplsRsvpTeMaxErhopsPerTnl = RSVPTE_TNLERHOP_DEF_VAL;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_MAX_ERPERTNL (gpRsvpTeGblInfo) =
            (UINT2) i4SetValFsMplsRsvpTeMaxErhopsPerTnl;
        /* Updating Max Erhop value in TE */
        rpteTeUpdateMaxErhopsPerTnl (i4SetValFsMplsRsvpTeMaxErhopsPerTnl);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeMaxActRoutePerTnl
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeMaxActRoutePerTnl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeMaxActRoutePerTnl (INT4 i4SetValFsMplsRsvpTeMaxActRoutePerTnl)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_MAX_ARPERTNL (gpRsvpTeGblInfo) =
            (UINT2) i4SetValFsMplsRsvpTeMaxActRoutePerTnl;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeMaxIfaces
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeMaxIfaces
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeMaxIfaces (INT4 i4SetValFsMplsRsvpTeMaxIfaces)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_MAX_IFACES (gpRsvpTeGblInfo) =
            (UINT2) i4SetValFsMplsRsvpTeMaxIfaces;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeMaxNbrs
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeMaxNbrs
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeMaxNbrs (INT4 i4SetValFsMplsRsvpTeMaxNbrs)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_MAX_NEIGHBOURS (gpRsvpTeGblInfo) =
            (UINT2) i4SetValFsMplsRsvpTeMaxNbrs;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* SET ROUTINES FOR NBR TABLE*/

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeNbrRRCapable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                setValFsMplsRsvpTeNbrRRCapable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeNbrRRCapable (INT4 i4FsMplsRsvpTeIfIndex,
                                UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                INT4 i4SetValFsMplsRsvpTeNbrRRCapable)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry)
            == RPTE_SUCCESS)
        {
            if (i4SetValFsMplsRsvpTeNbrRRCapable == RPTE_ENABLED)
            {
                if (gu1RRCapable == RPTE_DISABLED)
                {
                    return SNMP_FAILURE;
                }
            }
            if (NBR_ENTRY_STATUS (pNbrEntry) == RPTE_ACTIVE)
            {
                if (i4SetValFsMplsRsvpTeNbrRRCapable == RPTE_ENABLED)
                {
                    NBR_ENTRY_RMD_CAPABLE (pNbrEntry) = RPTE_ENABLED;
                    NBR_ENTRY_RR_CAPABLE (pNbrEntry) = RPTE_ENABLED;
                    return SNMP_SUCCESS;
                }
            }
            NBR_ENTRY_RR_CAPABLE (pNbrEntry) =
                (UINT1) i4SetValFsMplsRsvpTeNbrRRCapable;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeNbrRMDCapable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                setValFsMplsRsvpTeNbrRMDCapable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeNbrRMDCapable (INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                 INT4 i4SetValFsMplsRsvpTeNbrRMDCapable)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if (RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry)
            == RPTE_SUCCESS)
        {
            if (i4SetValFsMplsRsvpTeNbrRMDCapable == RPTE_ENABLED)
            {
                if (gu1MsgIdCapable == RPTE_DISABLED)
                {
                    return SNMP_FAILURE;
                }
            }
            if (NBR_ENTRY_STATUS (pNbrEntry) == RPTE_ACTIVE)
            {
                if (i4SetValFsMplsRsvpTeNbrRMDCapable == RPTE_DISABLED)
                {
                    NBR_ENTRY_RMD_CAPABLE (pNbrEntry) = RPTE_DISABLED;
                    NBR_ENTRY_RR_CAPABLE (pNbrEntry) = RPTE_DISABLED;
                    return SNMP_SUCCESS;
                }
            }
            NBR_ENTRY_RMD_CAPABLE (pNbrEntry) =
                (UINT1) i4SetValFsMplsRsvpTeNbrRMDCapable;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeNbrEncapsType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                setValFsMplsRsvpTeNbrEncapsType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeNbrEncapsType (INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                 INT4 i4SetValFsMplsRsvpTeNbrEncapsType)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if ((RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry)
             == RPTE_SUCCESS) &&
            ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
             (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
        {
            NBR_ENTRY_ENCAP (pNbrEntry) =
                (UINT1) i4SetValFsMplsRsvpTeNbrEncapsType;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeNbrHelloSupport
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                setValFsMplsRsvpTeNbrHelloSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeNbrHelloSupport (INT4 i4FsMplsRsvpTeIfIndex,
                                   UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                   INT4 i4SetValFsMplsRsvpTeNbrHelloSupport)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal == RPTE_SUCCESS)
    {
        if ((RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry)
             == RPTE_SUCCESS) &&
            ((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
             (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
        {
            NBR_ENTRY_HELLO_SPRT (pNbrEntry) =
                (UINT1) i4SetValFsMplsRsvpTeNbrHelloSupport;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeNbrStatus
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                setValFsMplsRsvpTeNbrStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeNbrStatus (INT4 i4FsMplsRsvpTeIfIndex,
                             UINT4 u4FsMplsRsvpTeNbrIfAddr,
                             INT4 i4SetValFsMplsRsvpTeNbrStatus)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    tTMO_SLL           *pNbrList = NULL;

    if (RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry) == RPTE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry);

    switch (i4SetValFsMplsRsvpTeNbrStatus)
    {
        case RPTE_ACTIVE:

            if (u1RetVal == RPTE_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if (NBR_ENTRY_STATUS (pNbrEntry) == RPTE_NOTREADY)
            {
                if ((NBR_ENTRY_RR_CAPABLE (pNbrEntry) == RPTE_DISABLED) &&
                    (NBR_ENTRY_RMD_CAPABLE (pNbrEntry) == RPTE_ENABLED))
                {
                    RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                                "Failure : RR and RMD value not in sync.\n");
                    return SNMP_FAILURE;
                }
                NBR_ENTRY_STATUS (pNbrEntry) = RPTE_ACTIVE;
                /* Set the Neighbor hello active flag */
                RpteHhSetNbrHelloActive (pNbrEntry, pIfEntry, RPTE_TRUE);
                if (NBR_ENTRY_ENCAP (pNbrEntry) != RPTE_ZERO)
                {
                    ++IF_ENTRY_NBRS (pIfEntry);
                }
                if (NBR_ENTRY_ENCAP (pNbrEntry) == UDP_ENCAP)
                {
                    ++IF_ENTRY_UDP_NBRS (pIfEntry);
                }
                else
                {
                    ++IF_ENTRY_IP_NBRS (pIfEntry);
                }

                RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS,
                            "Success : NbrEntry Row Status made active.\n");

                return SNMP_SUCCESS;
            }
            else if (NBR_ENTRY_STATUS (pIfEntry) == RPTE_ACTIVE)
            {
                RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS,
                            "Success : NbrEntry Row Status made active.\n");
                return SNMP_SUCCESS;
            }
            break;

        case RPTE_CREATEANDWAIT:

            if (u1RetVal == RPTE_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));
            TMO_SLL_Init (pNbrList);
            pNbrEntry = (tNbrEntry *)
                RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_NBR_ENTRY_POOL_ID);

            if (pNbrEntry == NULL)
            {
                RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                            "Failed : Nbr Entry Creation Fail - "
                            "Mem Alloc Fail.\n");
                return SNMP_FAILURE;
            }

            MEMSET (pNbrEntry, RPTE_ZERO, sizeof (tNbrEntry));
            NBR_ENTRY_ADDR (pNbrEntry) = u4FsMplsRsvpTeNbrIfAddr;
            NBR_ENTRY_RR_CAPABLE (pNbrEntry) = gu1RRCapable;
            NBR_ENTRY_RR_STATE (pNbrEntry) = gu1RRCapable;
            NBR_ENTRY_RMD_CAPABLE (pNbrEntry) = gu1MsgIdCapable;
            NBR_ENTRY_ENCAP (pNbrEntry) = IP_ENCAP;
            NBR_ENTRY_HELLO_SPRT (pNbrEntry) = RPTE_NBR_HELLO_SPRT_DIS;
            NBR_ENTRY_HELLO_REL (pNbrEntry) = RPTE_NBR_HELLO_REL_PASSIVE;
            NBR_ENTRY_STATUS (pNbrEntry) = RPTE_NOTREADY;
            NBR_ENTRY_NBR_NUM_TNLS (pNbrEntry) = RPTE_ZERO;
            NBR_ENTRY_IF_ENTRY (pNbrEntry) = pIfEntry;
            pNbrEntry->bIsHelloActive = RPTE_TRUE;
            UTL_DLL_INIT (&(pNbrEntry->UpStrTnlList),
                          RPTE_OFFSET (tRsvpTeTnlInfo, UpStrNbrTnl));
            UTL_DLL_INIT (&(pNbrEntry->DnStrTnlList),
                          RPTE_OFFSET (tRsvpTeTnlInfo, DnStrNbrTnl));
            TMO_SLL_Init (&NBR_ENTRY_SREFRESH_LIST (pNbrEntry));

            /* Set the Neighbor hello active flag */
            RpteHhSetNbrHelloActive (pNbrEntry, pIfEntry, RPTE_TRUE);
            TMO_SLL_Add (pNbrList, (tRSVP_SLL_NODE *) (&(pNbrEntry->pNext)));

            RSVPTE_DBG (RSVPTE_LLVL_SET_SCSS,
                        "Success : NbrEntry Row Status made notready.\n");

            return SNMP_SUCCESS;

        case RPTE_CREATEANDGO:
        case RPTE_NOTINSERVICE:

            RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                        "Failed : Nbr Entry assign - "
                        "NotInService Not supported.\n");
            return SNMP_FAILURE;

        case RPTE_DESTROY:

            if (u1RetVal == RPTE_FAILURE)
            {
                return SNMP_FAILURE;
            }

            pNbrList = &(IF_ENTRY_NBR_LIST (pIfEntry));

            if (((NBR_ENTRY_STATUS (pNbrEntry) == RPTE_ACTIVE)
                 || (NBR_ENTRY_STATUS (pNbrEntry) == RPTE_NOTREADY))
                && (RPTE_IF_NUM_TNL_ASSOC (pIfEntry) == RSVPTE_ZERO))
            {
                pNbrEntry = (tNbrEntry *) TMO_SLL_First (pNbrList);

                while (pNbrEntry != NULL)
                {
                    if (pNbrEntry->u4Addr == u4FsMplsRsvpTeNbrIfAddr)
                    {
                        break;
                    }
                    pNbrEntry = (tNbrEntry *) TMO_SLL_Next (pNbrList,
                                                            (tRSVP_SLL_NODE *) &
                                                            (pNbrEntry->pNext));
                }
                if (pNbrEntry != NULL)
                {
                    /* Reset the Neighbor hello active flag */
                    RpteHhSetNbrHelloActive (pNbrEntry, pIfEntry, RPTE_FALSE);
                    TMO_SLL_Delete (pNbrList,
                                    (tRSVP_SLL_NODE *) & (pNbrEntry->pNext));

                    if (RSVP_RELEASE_MEM_BLOCK (RSVPTE_NBR_ENTRY_POOL_ID,
                                                (UINT1 *) pNbrEntry) ==
                        MEM_FAILURE)
                    {
                        RSVPTE_DBG (RSVPTE_LLVL_SET_FAIL,
                                    "Failed : Nbr Entry Creation Fail - "
                                    "Mem Release Fail.\n");
                    }

                }
            }
            return SNMP_SUCCESS;
        default:
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeNbrStorageType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                setValFsMplsRsvpTeNbrStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeNbrStorageType (INT4 i4FsMplsRsvpTeIfIndex,
                                  UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                  INT4 i4SetValFsMplsRsvpTeNbrStorageType)
{
    UNUSED_PARAM (i4FsMplsRsvpTeIfIndex);
    UNUSED_PARAM (u4FsMplsRsvpTeNbrIfAddr);
    UNUSED_PARAM (i4SetValFsMplsRsvpTeNbrStorageType);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeNbrTable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeNbrTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*SET ROUTINES FOR GENOBJECTS*/

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeHelloIntervalTime
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeHelloIntervalTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeHelloIntervalTime (INT4 i4SetValFsMplsRsvpTeHelloIntervalTime)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_HELLO_INTVL_TIME (gpRsvpTeGblInfo)
            =
            (UINT4) (i4SetValFsMplsRsvpTeHelloIntervalTime /
                     NO_OF_MSECS_PER_UNIT);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeRRCapable
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeRRCapable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeRRCapable (INT4 i4SetValFsMplsRsvpTeRRCapable)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        gu1RRCapable = (UINT1) i4SetValFsMplsRsvpTeRRCapable;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeMsgIdCapable
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeMsgIdCapable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeMsgIdCapable (INT4 i4SetValFsMplsRsvpTeMsgIdCapable)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        gu1MsgIdCapable = (UINT1) i4SetValFsMplsRsvpTeMsgIdCapable;
        gu1IntMsgIdCapable = RPTE_FALSE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeRMDPolicyObject
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeRMDPolicyObject
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeRMDPolicyObject (tSNMP_OCTET_STRING_TYPE *
                                   pSetValFsMplsRsvpTeRMDPolicyObject)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        gu4RMDFlags = pSetValFsMplsRsvpTeRMDPolicyObject->pu1_OctetList[0];
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeMinTnlsWithMsgId
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeMinTnlsWithMsgId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeMinTnlsWithMsgId (UINT4 u4SetValFsMplsRsvpTeMinTnlsWithMsgId)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_DOWN)
    {
        RSVPTE_MIN_TNLS_WITH_MSG_ID (gpRsvpTeGblInfo) =
            u4SetValFsMplsRsvpTeMinTnlsWithMsgId;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeNotificationEnabled
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeNotificationEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeNotificationEnabled (INT4
                                       i4SetValFsMplsRsvpTeNotificationEnabled)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u1NotificationEnabled =
        (UINT1) i4SetValFsMplsRsvpTeNotificationEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeNotifyMsgRetransmitIntvl
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeNotifyMsgRetransmitIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeNotifyMsgRetransmitIntvl (UINT4
                                            u4SetValFsMplsRsvpTeNotifyMsgRetransmitIntvl)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitIntvl =
        u4SetValFsMplsRsvpTeNotifyMsgRetransmitIntvl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeNotifyMsgRetransmitDecay
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeNotifyMsgRetransmitDecay
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeNotifyMsgRetransmitDecay (UINT4
                                            u4SetValFsMplsRsvpTeNotifyMsgRetransmitDecay)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitDecay =
        u4SetValFsMplsRsvpTeNotifyMsgRetransmitDecay;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeNotifyMsgRetransmitLimit
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeNotifyMsgRetransmitLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeNotifyMsgRetransmitLimit (UINT4
                                            u4SetValFsMplsRsvpTeNotifyMsgRetransmitLimit)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitLimit =
        u4SetValFsMplsRsvpTeNotifyMsgRetransmitLimit;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeAdminStatusTimeIntvl
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeAdminStatusTimeIntvl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeAdminStatusTimeIntvl (UINT4
                                        u4SetValFsMplsRsvpTeAdminStatusTimeIntvl)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u4AdminStatusTimerValue =
        u4SetValFsMplsRsvpTeAdminStatusTimeIntvl;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTePathStateRemovedSupport
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTePathStateRemovedSupport
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTePathStateRemovedSupport (INT4
                                           i4SetValFsMplsRsvpTePathStateRemovedSupport)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u1PathStateRemovedFlag =
        (UINT1) i4SetValFsMplsRsvpTePathStateRemovedSupport;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeLabelSetEnabled
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeLabelSetEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeLabelSetEnabled (INT4 i4SetValFsMplsRsvpTeLabelSetEnabled)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u1LabelSetEnabled =
        (UINT1) i4SetValFsMplsRsvpTeLabelSetEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeAdminStatusCapability
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeAdminStatusCapability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeAdminStatusCapability (INT4
                                         i4SetValFsMplsRsvpTeAdminStatusCapability)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u1AdminStatusCapable =
        (UINT1) i4SetValFsMplsRsvpTeAdminStatusCapability;
    gpRsvpTeGblInfo->RsvpTeCfgParams.u1IntAdminStatusCapable =
        (UINT1) RPTE_FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGrCapability
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGrCapability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGrCapability (INT4 i4SetValFsMplsRsvpTeGrCapability)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability =
        (UINT1) i4SetValFsMplsRsvpTeGrCapability;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGrRecoveryPathCapability
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGrRecoveryPathCapability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGrRecoveryPathCapability (tSNMP_OCTET_STRING_TYPE
                                            *
                                            pSetValFsMplsRsvpTeGrRecoveryPathCapability)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u1RecoveryPathCapability =
        pSetValFsMplsRsvpTeGrRecoveryPathCapability->pu1_OctetList[0];
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGrRestartTime
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGrRestartTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGrRestartTime (INT4 i4SetValFsMplsRsvpTeGrRestartTime)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u2RestartTime =
        (UINT2) i4SetValFsMplsRsvpTeGrRestartTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGrRecoveryTime
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGrRecoveryTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGrRecoveryTime (INT4 i4SetValFsMplsRsvpTeGrRecoveryTime)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u2RecoveryTime =
        (UINT2) i4SetValFsMplsRsvpTeGrRecoveryTime;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsFrrRevertiveMode
 Input       :  The Indices

                The Object 
                setValFsMplsFrrRevertiveMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrRevertiveMode (tSNMP_OCTET_STRING_TYPE
                              * pSetValFsMplsFrrRevertiveMode)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }
    RSVPTE_FRR_REVERTIVE_MODE (gpRsvpTeGblInfo) =
        pSetValFsMplsFrrRevertiveMode->pu1_OctetList[RPTE_ZERO];
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrDetourMergingEnabled
 Input       :  The Indices

                The Object 
                setValFsMplsFrrDetourMergingEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrDetourMergingEnabled (INT4 i4SetValFsMplsFrrDetourMergingEnabled)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }
    RSVPTE_FRR_DETOUR_MERGING_CAPABLE (gpRsvpTeGblInfo) =
        (UINT1) i4SetValFsMplsFrrDetourMergingEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrDetourEnabled
 Input       :  The Indices

                The Object 
                setValFsMplsFrrDetourEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrDetourEnabled (INT4 i4SetValFsMplsFrrDetourEnabled)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }
    RSVPTE_FRR_DETOUR_CAPABLE (gpRsvpTeGblInfo) =
        (UINT1) i4SetValFsMplsFrrDetourEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrCspfRetryInterval
 Input       :  The Indices

                The Object 
                setValFsMplsFrrCspfRetryInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrCspfRetryInterval (INT4 i4SetValFsMplsFrrCspfRetryInterval)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }
    RSVPTE_FRR_CSPF_RETRY_INTERVAL (gpRsvpTeGblInfo) =
        (UINT4) i4SetValFsMplsFrrCspfRetryInterval;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrCspfRetryCount
 Input       :  The Indices

                The Object 
                setValFsMplsFrrCspfRetryCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrCspfRetryCount (UINT4 u4SetValFsMplsFrrCspfRetryCount)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }
    RSVPTE_FRR_CSPF_RETRY_COUNT (gpRsvpTeGblInfo) =
        u4SetValFsMplsFrrCspfRetryCount;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrNotifsEnabled
 Input       :  The Indices

                The Object 
                setValFsMplsFrrNotifsEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrNotifsEnabled (INT4 i4SetValFsMplsFrrNotifsEnabled)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }
    RSVPTE_FRR_NOTIFS_ENABLED (gpRsvpTeGblInfo) =
        (UINT1) i4SetValFsMplsFrrNotifsEnabled;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrMakeAfterBreakEnabled
 Input       :  The Indices

                The Object
                setValFsMplsFrrMakeAfterBreakEnabled
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrMakeAfterBreakEnabled (INT4
                                      i4SetValFsMplsFrrMakeAfterBreakEnabled)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }
    RSVPTE_FRR_MAKE_AFTER_BREAK_ENABLED (gpRsvpTeGblInfo) =
        (UINT1) i4SetValFsMplsFrrMakeAfterBreakEnabled;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsBypassTunnelRowStatus
 Input       :  The Indices
                FsMplsBypassTunnelIfIndex
                FsMplsBypassTunnelIndex
                FsMplsBypassTunnelInstance
                FsMplsBypassTunnelIngressLSRId
                FsMplsBypassTunnelEgressLSRId

                The Object 
                setValFsMplsBypassTunnelRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsBypassTunnelRowStatus (INT4 i4FsMplsBypassTunnelIfIndex,
                                   UINT4 u4FsMplsBypassTunnelIndex,
                                   UINT4 u4FsMplsBypassTunnelIngressLSRId,
                                   UINT4 u4FsMplsBypassTunnelEgressLSRId,
                                   INT4 i4SetValFsMplsBypassTunnelRowStatus)
{

    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    tIfEntry           *pIfEntry = NULL;
    tTMO_DLL           *pIfFacTnlsList = NULL;
    tTMO_DLL           *pFacFrrList = NULL;
    tTMO_DLL           *pFacInFrrList = NULL;
    tTMO_DLL_NODE      *pIfFacTnlsNode = NULL;
    tTMO_DLL_NODE      *pFacFrrProtTnl = NULL;
    tTMO_DLL_NODE      *pFacInFrrProtTnl = NULL;
    tFacilityRpteBkpTnl *pFacilityRpteBkpTnl = NULL;
    tFacilityRpteBkpTnl *pTmpFacilityRpteBkpTnl = NULL;
    tRsvpTeFrrTnlPortMapping *pRsvpTeFrrTnlPortMapping = NULL;
    tRsvpTeFrrTnlPortMapping RsvpTeLocFrrTnlPortMapping;
    tRsvpTeFrrTnlPortMapping *pRsvpTeLocFrrTnlPortMapping = NULL;
    UINT1               u1RetVal = RPTE_FAILURE;
    INT4                i4ArrayIndex = RPTE_ZERO;

#ifdef CFA_WANTED
    UINT4               u4L3Intf = RPTE_ZERO;
    UINT4               u4LocL3Intf = RPTE_ZERO;
#endif

    BOOL1               bFound = RPTE_FALSE;
    BOOL1               bRsvpTeTnlFound = RPTE_FALSE;
    BOOL1               bRsvpL3InfFound = RPTE_FALSE;

    MEMSET (&RsvpTeLocFrrTnlPortMapping, RPTE_ZERO,
            sizeof (tRsvpTeFrrTnlPortMapping));

    /* Operations other than CREATE AND GO and DESTROY is not permitted for
     * this table */
    if ((i4SetValFsMplsBypassTunnelRowStatus != RPTE_CREATEANDGO) &&
        (i4SetValFsMplsBypassTunnelRowStatus != RPTE_DESTROY))
    {
        return SNMP_FAILURE;
    }

    /* MPLS Should have been enabled on the Interface Passed */
#ifdef CFA_WANTED
    if (CfaUtilGetL3IfFromMplsIf ((UINT4) i4FsMplsBypassTunnelIfIndex,
                                  &u4L3Intf, RPTE_TRUE) == CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }
#endif

    MPLS_RSVPTE_LOCK ();

    /* Associated pIfEntry for the interface passed should be there */
    if ((RpteGetIfEntry (i4FsMplsBypassTunnelIfIndex, &pIfEntry)
         == RPTE_FAILURE) || (pIfEntry == NULL))
    {
        MPLS_RSVPTE_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Check associated tunnel entry present in 
     * RsvpTeTnlInfo or not */
    u1RetVal = RpteGetTnlInTnlTable (gpRsvpTeGblInfo,
                                     u4FsMplsBypassTunnelIndex,
                                     RPTE_ZERO,
                                     u4FsMplsBypassTunnelIngressLSRId,
                                     u4FsMplsBypassTunnelEgressLSRId,
                                     &pRsvpTeTnlInfo);

    if ((u1RetVal == RPTE_SUCCESS) && (pRsvpTeTnlInfo != NULL))
    {
        bRsvpTeTnlFound = RPTE_TRUE;
    }

    /* Check whether the tunnel is already associated. */
    pIfFacTnlsList = (tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfEntry));

    TMO_DLL_Scan (pIfFacTnlsList, pIfFacTnlsNode, tTMO_DLL_NODE *)
    {
        pTmpFacilityRpteBkpTnl = (tFacilityRpteBkpTnl *) (pIfFacTnlsNode);

        if ((RPTE_FRR_PROT_IF_INDEX (pTmpFacilityRpteBkpTnl)
             == (UINT4) i4FsMplsBypassTunnelIfIndex) &&
            (RPTE_FRR_FACILITY_BKP_TNL_INDEX (pTmpFacilityRpteBkpTnl)
             == u4FsMplsBypassTunnelIndex))
        {
            bFound = RPTE_TRUE;
            break;
        }
    }

    /* Get the tunnel index and interface mapping in Rbtree */
    if (((i4SetValFsMplsBypassTunnelRowStatus == RPTE_CREATEANDGO) &&
         (!bFound)) ||
        ((i4SetValFsMplsBypassTunnelRowStatus == RPTE_DESTROY) && (bFound)))
    {
        RsvpTeLocFrrTnlPortMapping.u2TnlId = (UINT2) u4FsMplsBypassTunnelIndex;

        pRsvpTeLocFrrTnlPortMapping = (tRsvpTeFrrTnlPortMapping *)
            RBTreeGet (gpRsvpTeGblInfo->RpteFrrTnlPortMappingTree,
                       (tRBElem *) & RsvpTeLocFrrTnlPortMapping);

#ifdef CFA_WANTED
        u4LocL3Intf = u4L3Intf;

        /* find the actual position of L3 interface in au4L3Intf */
        for (i4ArrayIndex = (MAX_RSVPTE_L3_INF_ARRAY_INDEX - 1);
             i4ArrayIndex >= RPTE_ZERO; i4ArrayIndex--)
        {
            if ((u4LocL3Intf != RPTE_ZERO) &&
                (u4LocL3Intf <= RPTE_TOTAL_BITS_IN_UINT4))
            {
                RsvpTeLocFrrTnlPortMapping.au4L3Intf[i4ArrayIndex] |=
                    (UINT4) RPTE_ONE << (u4LocL3Intf - RPTE_ONE);
                break;
            }
            /* L3 interface does not belongs to this array index */
            u4LocL3Intf -= RPTE_TOTAL_BITS_IN_UINT4;
        }
#endif

    }

    /* If Operation is CREATE AND GO then, then the Tunnel is Added to 
     * the Interface List */
    if (i4SetValFsMplsBypassTunnelRowStatus == RPTE_CREATEANDGO)
    {
        if (bFound == RPTE_TRUE)
        {
            /* Tunnel is already associated with the interface. */
            MPLS_RSVPTE_UNLOCK ();
            return SNMP_SUCCESS;
        }

        /* If Tunnel is not already associated with the interface, associate it */
        pFacilityRpteBkpTnl = (tFacilityRpteBkpTnl *)
            RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_FAC_TNL_POOL_ID);

        if (pFacilityRpteBkpTnl == NULL)
        {
            MPLS_RSVPTE_UNLOCK ();
            return SNMP_FAILURE;
        }

        TMO_DLL_Init_Node ((tTMO_DLL_NODE *)
                           &
                           (RPTE_TE_FACILITY_BKP_TNL_NODE
                            (pFacilityRpteBkpTnl)));

        TMO_DLL_Add ((tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfEntry)),
                     (tTMO_DLL_NODE *) & (RPTE_TE_FACILITY_BKP_TNL_NODE
                                          (pFacilityRpteBkpTnl)));

        /* Fill the entries in frr facility node */
        RPTE_FRR_FACILITY_BKP_TNL_INDEX (pFacilityRpteBkpTnl)
            = u4FsMplsBypassTunnelIndex;
        RPTE_FRR_PROT_IF_INDEX (pFacilityRpteBkpTnl)
            = (UINT4) i4FsMplsBypassTunnelIfIndex;

        /* if backup tunnel is already present in rpte */
        if (bRsvpTeTnlFound == RPTE_TRUE)
        {
            RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID (pFacilityRpteBkpTnl)
                = u4FsMplsBypassTunnelIngressLSRId;
            RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID (pFacilityRpteBkpTnl)
                = u4FsMplsBypassTunnelEgressLSRId;

            RPTE_FRR_FACILITY_BKP_TNL (pFacilityRpteBkpTnl) = pRsvpTeTnlInfo;
            RSVPTE_TNL_PROT_IF_INDEX (RPTE_TE_TNL (pRsvpTeTnlInfo))
                = RPTE_FRR_PROT_IF_INDEX (pFacilityRpteBkpTnl);

            RSVPTE_FRR_CONF_IF_NUM (gpRsvpTeGblInfo)++;
            RSVPTE_FRR_CONF_PROT_TUN_NUM (gpRsvpTeGblInfo)++;
        }
        /* Associated tunnel is not present in RsvpTeTnlInfo */
        else
        {
            /* Below entries are explicitly filled with NULL here 
             * because to handle the case when backup tunnel is not
             * yet signalled in the system, these entries are filled 
             * at the time of backup tunnel signalling in the system */
            RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID (pFacilityRpteBkpTnl)
                = RPTE_ZERO;
            RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID (pFacilityRpteBkpTnl)
                = RPTE_ZERO;

            RPTE_FRR_FACILITY_BKP_TNL (pFacilityRpteBkpTnl) = NULL;

        }

        /* Assosiated tunnel is not present in Rbtree 
         * add new node in Rb Tree */
        if (pRsvpTeLocFrrTnlPortMapping == NULL)
        {
            pRsvpTeFrrTnlPortMapping = (tRsvpTeFrrTnlPortMapping *)
                MemAllocMemBlk (RSVPTE_FRRTNL_PORT_MAPPING_POOL_ID);

            if (pRsvpTeFrrTnlPortMapping == NULL)
            {
                MPLS_RSVPTE_UNLOCK ();
                return SNMP_FAILURE;
            }

            MEMSET (pRsvpTeFrrTnlPortMapping, 0,
                    sizeof (tRsvpTeFrrTnlPortMapping));

            MEMCPY (pRsvpTeFrrTnlPortMapping, &RsvpTeLocFrrTnlPortMapping,
                    sizeof (tRsvpTeFrrTnlPortMapping));

            /* Add the pRsvpTeFrrTnlPortMapping info in Rbtree */
            if (RBTreeAdd (gpRsvpTeGblInfo->RpteFrrTnlPortMappingTree,
                           (tRBElem *) pRsvpTeFrrTnlPortMapping) != RB_SUCCESS)
            {
                MemReleaseMemBlock (RSVPTE_FRRTNL_PORT_MAPPING_POOL_ID,
                                    (UINT1 *) pRsvpTeFrrTnlPortMapping);
                MPLS_RSVPTE_UNLOCK ();
                return SNMP_FAILURE;
            }
        }
        else
        {
            /* Associated tunnel is already protecting some interface 
             * just update information in Rbtree */
            pRsvpTeLocFrrTnlPortMapping->au4L3Intf[i4ArrayIndex] |=
                RsvpTeLocFrrTnlPortMapping.au4L3Intf[i4ArrayIndex];
        }

    }
    else
    {
        /* Tunnel is not associated with the interface. 
         * Dont try to deassociate it. */
        if (bFound == RPTE_FALSE)
        {
            MPLS_RSVPTE_UNLOCK ();
            return SNMP_FAILURE;
        }

        /* If the tunnel information is present in Frr facility List
         * FrrTnlPortMapping must be present in FrrTnlPortMapping Rbtree */
        if (pRsvpTeLocFrrTnlPortMapping == NULL)
        {
            MPLS_RSVPTE_UNLOCK ();
            return SNMP_FAILURE;
        }

        /* Here Operation is DESTROY. So, the Tunnel is deleted from 
         * the Interface List */

        TMO_DLL_Delete ((tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfEntry)),
                        (tTMO_DLL_NODE *) & (RPTE_TE_FACILITY_BKP_TNL_NODE
                                             (pTmpFacilityRpteBkpTnl)));

        TMO_DLL_Init_Node ((tTMO_DLL_NODE *) & (RPTE_TE_FACILITY_BKP_TNL_NODE
                                                (pTmpFacilityRpteBkpTnl)));

        MemReleaseMemBlock (RSVPTE_FAC_TNL_POOL_ID,
                            (UINT1 *) pTmpFacilityRpteBkpTnl);

        /*Update the l3 interface information in Rbtree */
        pRsvpTeLocFrrTnlPortMapping->au4L3Intf[i4ArrayIndex] &= ~
            (RsvpTeLocFrrTnlPortMapping.au4L3Intf[i4ArrayIndex]);

        /* If no other interface is protecting by associated tunnel 
         * Delete the information in Rbtree and Release memory from POOL */
        for (i4ArrayIndex = (MAX_RSVPTE_L3_INF_ARRAY_INDEX - 1);
             i4ArrayIndex >= RPTE_ZERO; i4ArrayIndex--)
        {
            if (pRsvpTeLocFrrTnlPortMapping->au4L3Intf[i4ArrayIndex] !=
                RPTE_ZERO)
            {
                bRsvpL3InfFound = RPTE_TRUE;
                break;
            }
        }

        if (!bRsvpL3InfFound)
        {
            RBTreeRemove (gpRsvpTeGblInfo->RpteFrrTnlPortMappingTree,
                          pRsvpTeLocFrrTnlPortMapping);

            /* Release the memory from POOL */
            MEMSET ((UINT1 *) pRsvpTeLocFrrTnlPortMapping, RPTE_ZERO,
                    sizeof (tRsvpTeFrrTnlPortMapping));
            MemReleaseMemBlock (RSVPTE_FRRTNL_PORT_MAPPING_POOL_ID,
                                (UINT1 *) pRsvpTeLocFrrTnlPortMapping);
        }

        /* Bypass tunnel is  present in RsvpTe */
        if (bRsvpTeTnlFound == RPTE_TRUE)
        {
            RSVPTE_TNL_PROT_IF_INDEX (RPTE_TE_TNL (pRsvpTeTnlInfo)) = RPTE_ZERO;

            if (RSVPTE_FRR_CONF_IF_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
            {
                RSVPTE_FRR_CONF_IF_NUM (gpRsvpTeGblInfo)--;
            }

            if (RSVPTE_FRR_CONF_PROT_TUN_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
            {
                RSVPTE_FRR_CONF_PROT_TUN_NUM (gpRsvpTeGblInfo)--;
            }
        }
    }
    if (bRsvpTeTnlFound == RPTE_TRUE)
    {
        pFacFrrList = &(RPTE_FRR_FAC_PROT_TNL_LIST (pRsvpTeTnlInfo));
        if (TMO_DLL_Count (pFacFrrList) != RPTE_ZERO)
        {
            pFacFrrProtTnl = TMO_DLL_First (pFacFrrList);
            while (pFacFrrProtTnl != NULL)
            {
                pTmpRsvpTeTnlInfo = (tRsvpTeTnlInfo *)
                    (((FS_ULONG) pFacFrrProtTnl
                      - (RPTE_OFFSET (tRsvpTeTnlInfo, FacFrrProtTnl))));

                /* making the protected tnl's bypass tnl NULL */
                RPTE_FRR_OUT_TNL_INFO (pTmpRsvpTeTnlInfo) = NULL;

                TMO_DLL_Delete (pFacFrrList, pFacFrrProtTnl);
                pFacFrrProtTnl = TMO_DLL_First (pFacFrrList);

                RSVPTE_TNL_PROT_IF_INDEX
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
                RSVPTE_TE_TNL_FRR_BKP_TNL_INDEX
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
                RSVPTE_TE_TNL_FRR_BKP_TNL_INST
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
                MEMSET (RSVPTE_TE_TNL_FRR_BKP_TNL_INGRESS_ID
                        (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)), RSVPTE_ZERO,
                        ROUTER_ID_LENGTH);
                MEMSET (RSVPTE_TE_TNL_FRR_BKP_TNL_EGRESS_ID
                        (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)), RSVPTE_ZERO,
                        ROUTER_ID_LENGTH);
                RSVPTE_TE_TNL_FRR_ONE2ONE_PLR_ID
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
                RSVPTE_TE_TNL_FRR_ONE2ONE_SENDER_ADDR_TYPE
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
                RSVPTE_TE_TNL_FRR_ONE2ONE_SENDER_ADDR
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
                RSVPTE_TE_TNL_FRR_ONE2ONE_AVOID_ADDR_TYPE
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
                RSVPTE_TE_TNL_FRR_ONE2ONE_AVOID_ADDR
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) = RSVPTE_ZERO;
                RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS
                    (RPTE_TE_TNL (pTmpRsvpTeTnlInfo)) =
                    TE_TNL_FRR_PROT_STATUS_PARTIAL;
                RSVPTE_TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH (RPTE_TE_TNL
                                                          (pTmpRsvpTeTnlInfo)) =
                    RSVPTE_ZERO;

                RpteUtlSetDetourStatus (pTmpRsvpTeTnlInfo, RPTE_FALSE);
                RSVPTE_TE_TNL_FRR_DETOUR_MERGING (RPTE_TE_TNL
                                                  (pTmpRsvpTeTnlInfo)) =
                    RSVPTE_ZERO;

                RpteResetPlrStatus (pTmpRsvpTeTnlInfo);
            }
        }
        pFacInFrrList = &(pRsvpTeTnlInfo->FacInFrrList);

        if (TMO_DLL_Count (pFacInFrrList) != RPTE_ZERO)
        {
            pFacInFrrProtTnl = TMO_DLL_First (pFacInFrrList);
            while (pFacInFrrProtTnl != NULL)
            {
                pTmpRsvpTeTnlInfo = (tRsvpTeTnlInfo *)
                    (((FS_ULONG) pFacInFrrProtTnl
                      - (RPTE_OFFSET (tRsvpTeTnlInfo, FacInFrrProtTnl))));
                RPTE_FRR_IN_TNL_INFO (pTmpRsvpTeTnlInfo) = NULL;
                TMO_DLL_Delete (pFacInFrrList, pFacInFrrProtTnl);
                pFacInFrrProtTnl = TMO_DLL_First (pFacInFrrList);
                RPTE_FRR_NODE_STATE (pTmpRsvpTeTnlInfo) &=
                    (UINT1) (~(RPTE_FRR_NODE_STATE_MP));
            }
        }
    }
    MPLS_RSVPTE_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeLsrID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeLsrID (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeMaxTnls
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeMaxTnls (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeMaxErhopsPerTnl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeMaxErhopsPerTnl (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeMaxActRoutePerTnl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeMaxActRoutePerTnl (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeMaxIfaces
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeMaxIfaces (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeMaxNbrs
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeMaxNbrs (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeSockSupprtd
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeSockSupprtd (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeHelloSupprtd
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeHelloSupprtd (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeHelloIntervalTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeHelloIntervalTime (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeRRCapable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeRRCapable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeMsgIdCapable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeMsgIdCapable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeRMDPolicyObject
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeRMDPolicyObject (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGenLblSpaceMinLbl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeGenLblSpaceMinLbl (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGenLblSpaceMaxLbl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeGenLblSpaceMaxLbl (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGenDebugFlag
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeGenDebugFlag (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGenPduDumpLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeGenPduDumpLevel (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGenPduDumpMsgType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeGenPduDumpMsgType (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGenPduDumpDirection
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeGenPduDumpDirection (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeOperStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeOperStatus (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeOverRideOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeOverRideOption (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeMinTnlsWithMsgId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeMinTnlsWithMsgId (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsFrrDetourMergingEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsFrrDetourMergingEnabled (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsFrrDetourEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsFrrDetourEnabled (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsFrrCspfRetryInterval
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsFrrCspfRetryInterval (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsFrrCspfRetryCount
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsFrrCspfRetryCount (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsFrrNotifsEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsFrrNotifsEnabled (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsFrrMakeAfterBreakEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsFrrMakeAfterBreakEnabled (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsBypassTunnelIfTable
 Input       :  The Indices
                FsMplsBypassTunnelIfIndex
                FsMplsBypassTunnelIndex
                FsMplsBypassTunnelInstance
                FsMplsBypassTunnelIngressLSRId
                FsMplsBypassTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsBypassTunnelIfTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsFrrRevertiveMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsFrrRevertiveMode (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeNotificationEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeNotificationEnabled (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitIntvl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitIntvl (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitDecay
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitDecay (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitLimit
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitLimit (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeAdminStatusTimeIntvl
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeAdminStatusTimeIntvl (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTePathStateRemovedSupport
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTePathStateRemovedSupport (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeLabelSetEnabled
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeLabelSetEnabled (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeAdminStatusCapability
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeAdminStatusCapability (UINT4 *pu4ErrorCode,
                                           tSnmpIndexList * pSnmpIndexList,
                                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGrCapability
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeGrCapability (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGrRecoveryPathCapability
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeGrRecoveryPathCapability (UINT4 *pu4ErrorCode,
                                              tSnmpIndexList * pSnmpIndexList,
                                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGrRestartTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhDepv2FsMplsRsvpTeGrRestartTime (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGrRecoveryTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhDepv2FsMplsRsvpTeGrRecoveryTime (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeReoptimizeTime
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeReoptimizeTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeReoptimizeTime (INT4 i4SetValFsMplsRsvpTeReoptimizeTime)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u4ReoptimizeTime =
        (UINT4) i4SetValFsMplsRsvpTeReoptimizeTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeEroCacheTime
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeEroCacheTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeEroCacheTime (INT4 i4SetValFsMplsRsvpTeEroCacheTime)
{
    gpRsvpTeGblInfo->RsvpTeCfgParams.u4EroCacheTime =
        (UINT4) i4SetValFsMplsRsvpTeEroCacheTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeReoptLinkMaintenance
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeReoptLinkMaintenance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeReoptLinkMaintenance (INT4
                                        i4SetValFsMplsRsvpTeReoptLinkMaintenance)
{
    UINT4               u4IfIndex =
        (UINT4) i4SetValFsMplsRsvpTeReoptLinkMaintenance;
    tIfEntry           *pRpteIfEntry = NULL;
    tTMO_SLL_NODE      *pNbrNode = NULL;
    tTMO_SLL_NODE      *pTmpNbrNode = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;
    tNbrEntry          *pNbrEntry = NULL;
    UINT4               u4MplsIfIndex = 0;
    UINT4               u4TempLocalAddr = 0;
    UINT4               u4RemoteIp = 0;

    if (CfaGetIfIpAddr ((INT4) u4IfIndex, &u4TempLocalAddr) != CFA_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "nmhSetFsMplsRsvpTeReoptLinkMaintenance : CfaGetIfIpAddr failed\n");
    }

    if (CfaUtilGetMplsIfFromIfIndex (u4IfIndex, &u4MplsIfIndex, TRUE) ==
        CFA_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (RpteGetIfEntry ((INT4) u4MplsIfIndex, &pRpteIfEntry) == RPTE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    TMO_DYN_SLL_Scan (&(IF_ENTRY_NBR_LIST (pRpteIfEntry)), pNbrNode,
                      pTmpNbrNode, tTMO_SLL_NODE *)
    {
        pNbrEntry = (tNbrEntry *) pNbrNode;

        UTL_DLL_OFFSET_SCAN (&(pNbrEntry->DnStrTnlList), pRsvpTeTnlInfo,
                             pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
        {
            if (RSVPTE_TE_TNL_REOPTIMIZE_STATUS (pRsvpTeTnlInfo->pTeTnlInfo)
                == RPTE_TE_REOPTIMIZE_ENABLE)
            {
                if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo)
                    == RPTE_INGRESS)
                {
                    RptePortTlmGetRemoteIP (pRsvpTeTnlInfo->pTeTnlInfo->
                                            u4DnStrDataTeLinkIf, &u4RemoteIp);
                    RPTE_REOPT_LINK_MAINTENANCE_ADDR (pRsvpTeTnlInfo) =
                        u4RemoteIp;

                    RpteUtlFillExcludeHopList (pRsvpTeTnlInfo,
                                               OSIX_NTOHL (u4RemoteIp));
                    pRsvpTeTnlInfo->u1ReoptMaintenanceType =
                        RPTE_REOPT_LINK_MAINTENANCE;
                    RpteTriggerReoptimization (pRsvpTeTnlInfo,
                                               RPTE_REOPT_LINK_MAINTENANCE);
                }

                if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo)
                    == RPTE_INTERMEDIATE)
                {
                    if (RPTE_REOPTIMIZE_NODE_STATE (pRsvpTeTnlInfo) ==
                        RPTE_MID_POINT_NODE)
                    {
                        RptePortTlmGetRemoteIP (pRsvpTeTnlInfo->pTeTnlInfo->
                                                u4DnStrDataTeLinkIf,
                                                &u4RemoteIp);
                        RPTE_REOPT_LINK_MAINTENANCE_ADDR (pRsvpTeTnlInfo) =
                            u4RemoteIp;

                        RpteUtlFillExcludeHopList (pRsvpTeTnlInfo,
                                                   OSIX_NTOHL (u4RemoteIp));

                        pRsvpTeTnlInfo->u1ReoptMaintenanceType =
                            RPTE_REOPT_LINK_MAINTENANCE;

                        if (RpteCspfCompForReoptimizeTnlAtMidNode
                            (pRsvpTeTnlInfo) == RPTE_FAILURE)
                        {
                            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                                        "RpteTriggerReoptimization : Cspf Computation Request Failed\n");
                            return SNMP_FAILURE;
                        }

                    }
                    else if (pRsvpTeTnlInfo->u4DnStrDataTeLinkIfAddr ==
                             u4TempLocalAddr)
                    {
                        /*TODO : Code for unnumbered and check u4FwdComponentIfIndex */
                        RptePortTlmGetRemoteIP (pRsvpTeTnlInfo->pTeTnlInfo->
                                                u4DnStrDataTeLinkIf,
                                                &u4RemoteIp);
                        RPTE_REOPT_LINK_MAINTENANCE_ADDR (pRsvpTeTnlInfo) =
                            u4RemoteIp;
                        RpteConstructPktMapAndSendPathErr (pRsvpTeTnlInfo,
                                                           RPTE_NOTIFY,
                                                           RPTE_LOCAL_LINK_MAINTENANCE);
                    }
                }
            }
        }

        UTL_DLL_OFFSET_SCAN (&(pNbrEntry->UpStrTnlList), pRsvpTeTnlInfo,
                             pTmpRsvpTeTnlInfo, tRsvpTeTnlInfo *)
        {
            if (RSVPTE_TE_TNL_REOPTIMIZE_STATUS (pRsvpTeTnlInfo->pTeTnlInfo)
                == RPTE_TE_REOPTIMIZE_ENABLE)
            {

                if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo) == RPTE_EGRESS)
                {
                    if (pRsvpTeTnlInfo->u4UpStrDataTeLinkIfAddr ==
                        u4TempLocalAddr)
                    {
                        RPTE_REOPT_LINK_MAINTENANCE_ADDR (pRsvpTeTnlInfo) =
                            u4TempLocalAddr;
                        RpteConstructPktMapAndSendPathErr (pRsvpTeTnlInfo,
                                                           RPTE_NOTIFY,
                                                           RPTE_LOCAL_LINK_MAINTENANCE);
                    }
                }

                if (RSVPTE_TNL_ROLE (pRsvpTeTnlInfo->pTeTnlInfo)
                    == RPTE_INTERMEDIATE)
                {
                    if (pRsvpTeTnlInfo->u4UpStrDataTeLinkIfAddr ==
                        u4TempLocalAddr)
                    {
                        RPTE_REOPT_LINK_MAINTENANCE_ADDR (pRsvpTeTnlInfo) =
                            u4TempLocalAddr;
                        RpteConstructPktMapAndSendPathErr (pRsvpTeTnlInfo,
                                                           RPTE_NOTIFY,
                                                           RPTE_LOCAL_LINK_MAINTENANCE);
                    }
                }
            }
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeReoptNodeMaintenance
 Input       :  The Indices

                The Object
                setValFsMplsRsvpTeReoptNodeMaintenance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeReoptNodeMaintenance (INT4
                                        i4SetValFsMplsRsvpTeReoptNodeMaintenance)
{

    tRsvpTeTnlInfo     *pTmpRsvpTeTnlInfo = NULL;

    pTmpRsvpTeTnlInfo = RBTreeGetFirst (gpRsvpTeGblInfo->RpteTnlTree);

    while (pTmpRsvpTeTnlInfo != NULL)
    {

        if ((RSVPTE_TNL_ROLE (pTmpRsvpTeTnlInfo->pTeTnlInfo) == RPTE_INGRESS) ||
            (RSVPTE_TNL_ROLE (pTmpRsvpTeTnlInfo->pTeTnlInfo) == RPTE_EGRESS))
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }

        if (RPTE_REOPTIMIZE_NODE_STATE (pTmpRsvpTeTnlInfo) ==
            RPTE_MID_POINT_NODE)
        {
            pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                               (tRBElem *) pTmpRsvpTeTnlInfo,
                                               NULL);
            continue;
        }

        if ((RSVPTE_TE_TNL_REOPTIMIZE_STATUS (pTmpRsvpTeTnlInfo->pTeTnlInfo)
             == RPTE_TE_REOPTIMIZE_ENABLE) &&
            (RSVPTE_TNL_ROLE (pTmpRsvpTeTnlInfo->pTeTnlInfo) ==
             RPTE_INTERMEDIATE))
        {
            RpteConstructPktMapAndSendPathErr (pTmpRsvpTeTnlInfo,
                                               RPTE_NOTIFY,
                                               RPTE_LOCAL_NODE_MAINTENANCE);
        }

        pTmpRsvpTeTnlInfo = RBTreeGetNext (gpRsvpTeGblInfo->RpteTnlTree,
                                           (tRBElem *) pTmpRsvpTeTnlInfo, NULL);
    }

    UNUSED_PARAM (i4SetValFsMplsRsvpTeReoptNodeMaintenance);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeReoptimizeTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeReoptimizeTime (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeEroCacheTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeEroCacheTime (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeReoptLinkMaintenance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeReoptLinkMaintenance (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeReoptNodeMaintenance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeReoptNodeMaintenance (UINT4 *pu4ErrorCode,
                                          tSnmpIndexList * pSnmpIndexList,
                                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#ifdef MPLS_L3VPN_WANTED
 /* RSVP-TE MPLS-L3VPN-TE */
 /* Low Level SET Routine for All Objects  */
 /****************************************************************************
  Function    :  nmhSetFsMplsL3VpnRsvpTeMapTnlIndex
  Input       :  The Indices
                 FsMplsL3VpnRsvpTeMapPrefixType
                 FsMplsL3VpnRsvpTeMapPrefix
                 FsMplsL3VpnRsvpTeMapMaskType
                 FsMplsL3VpnRsvpTeMapMask

                 The Object
                 setValFsMplsL3VpnRsvpTeMapTnlIndex
  Output      :  The Set Low Lev Routine Take the Indices &
                 Sets the Value accordingly.
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMplsL3VpnRsvpTeMapTnlIndex (INT4 i4FsMplsL3VpnRsvpTeMapPrefixType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMplsL3VpnRsvpTeMapPrefix,
                                    INT4 i4FsMplsL3VpnRsvpTeMapMaskType,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pFsMplsL3VpnRsvpTeMapMask,
                                    INT4 i4SetValFsMplsL3VpnRsvpTeMapTnlIndex)
{
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo          TeTnlInfo;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;
    /*    tL3VpnRsvpTeLspEventInfo  L3VpnRsvpTeLspEventInfo; */
    tL3VpnGlobalTnlInfo L3VpnGlobalTnlInfo;
    /*    tRsvpTeTnlInfo RpteInTnlInfo; */
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "nmhSetFsMplsL3VpnRsvpTeMapTnlIndex Entry\t\n");
    MEMSET (&TeTnlInfo, 0, sizeof (tTeTnlInfo));
    MEMSET (&L3VpnGlobalTnlInfo, 0, sizeof (tL3VpnGlobalTnlInfo));

    pTeTnlInfo = &TeTnlInfo;
    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapPrefix, u4Prefix);
    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapMask, u4Mask);

    if ((pL3VpnRsvpMapLabelEntry =
         L3vpnGetRsvpMapLabelEntry ((UINT1) i4FsMplsL3VpnRsvpTeMapPrefixType,
                                    u4Prefix,
                                    (UINT1) i4FsMplsL3VpnRsvpTeMapMaskType,
                                    u4Mask)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "nmhSetFsMplsL3VpnRsvpTeMapTnlIndex Failed :Prefix Entry is not Present\t\n");
        return SNMP_FAILURE;
    }
    if (TeUtlFetchTnlInfoFromTnlTable
        ((UINT4) i4SetValFsMplsL3VpnRsvpTeMapTnlIndex,
         &TeTnlInfo) != TE_SUCCESS)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "nmhSetFsMplsL3VpnRsvpTeMapTnlIndex Failed :No Tunnel Entry Present\t\n");
        return SNMP_FAILURE;
    }

    pL3VpnRsvpMapLabelEntry->u4TnlIndex =
        (UINT4) i4SetValFsMplsL3VpnRsvpTeMapTnlIndex;
    pL3VpnRsvpMapLabelEntry->u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
    pL3VpnRsvpMapLabelEntry->u1TnlSgnlPrtcl = pTeTnlInfo->u1TnlSgnlPrtcl;
    if (L3VpnGetGlobalTnlTable
        (pL3VpnRsvpMapLabelEntry->u4TnlIndex,
         &L3VpnGlobalTnlInfo) == OSIX_SUCCESS)
    {
        pL3VpnRsvpMapLabelEntry->u4TnlXcIndex = L3VpnGlobalTnlInfo.u4TnlXcIndex;
        pL3VpnRsvpMapLabelEntry->u4TnlProXcIndex =
            L3VpnGlobalTnlInfo.u4TnlProXcIndex;
    }

    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "nmhSetFsMplsL3VpnRsvpTeMapTnlIndex Exit\t\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetFsMplsL3VpnRsvpTeMapRowStatus
Input       :  The Indices
FsMplsL3VpnRsvpTeMapPrefixType
FsMplsL3VpnRsvpTeMapPrefix
FsMplsL3VpnRsvpTeMapMaskType
FsMplsL3VpnRsvpTeMapMask

The Object
setValFsMplsL3VpnRsvpTeMapRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetFsMplsL3VpnRsvpTeMapRowStatus (INT4 i4FsMplsL3VpnRsvpTeMapPrefixType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMplsL3VpnRsvpTeMapPrefix,
                                     INT4 i4FsMplsL3VpnRsvpTeMapMaskType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pFsMplsL3VpnRsvpTeMapMask,
                                     INT4 i4SetValFsMplsL3VpnRsvpTeMapRowStatus)
{
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapPrefix, u4Prefix);
    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapMask, u4Mask);

    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "SetFsMplsL3VpnRsvpTeMapRowStatus Entry \t\n");
    pL3VpnRsvpMapLabelEntry =
        L3vpnGetRsvpMapLabelEntry ((UINT1) i4FsMplsL3VpnRsvpTeMapPrefixType,
                                   u4Prefix,
                                   (UINT1) i4FsMplsL3VpnRsvpTeMapMaskType,
                                   u4Mask);

    switch (i4SetValFsMplsL3VpnRsvpTeMapRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pL3VpnRsvpMapLabelEntry != NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetFsMplsL3VpnRsvpTeMapRowStatus creation Failed: Entry is already exists \t\n");
                return SNMP_FAILURE;
            }
            pL3VpnRsvpMapLabelEntry =
                L3VpnCreateRsvpMapLabelTable ((UINT1)
                                              i4FsMplsL3VpnRsvpTeMapPrefixType,
                                              u4Prefix,
                                              (UINT1)
                                              i4FsMplsL3VpnRsvpTeMapMaskType,
                                              u4Mask);

            if (pL3VpnRsvpMapLabelEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetFsMplsL3VpnRsvpTeMapRowStatus creation Failed \t\n");
                return SNMP_FAILURE;
            }
            pL3VpnRsvpMapLabelEntry->u1RowStatus = NOT_READY;
            break;

        case ACTIVE:
            if (pL3VpnRsvpMapLabelEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetFsMplsL3VpnRsvpTeMapRowStatus activation Failed:No entry exists \t\n");
                return SNMP_FAILURE;
            }
            L3VpnBgpNotificationForTnlBind (pL3VpnRsvpMapLabelEntry->u4TnlIndex,
                                            L3VPN_RSVPTE_LSP_UP);
            pL3VpnRsvpMapLabelEntry->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            if (pL3VpnRsvpMapLabelEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetFsMplsL3VpnRsvpTeMapRowStatus Failed:No entry exists \t\n");
                return SNMP_FAILURE;
            }
            pL3VpnRsvpMapLabelEntry->u1RowStatus = NOT_IN_SERVICE;
            break;

        case NOT_READY:
            if (pL3VpnRsvpMapLabelEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetFsMplsL3VpnRsvpTeMapRowStatus Failed:No entry exists \t\n");
                return SNMP_FAILURE;
            }
            pL3VpnRsvpMapLabelEntry->u1RowStatus = NOT_READY;
            break;

        case DESTROY:
            if (pL3VpnRsvpMapLabelEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetFsMplsL3VpnRsvpTeMapRowStatus Failed:No entry exists \t\n");
                return SNMP_FAILURE;
            }
            if ((L3VpnDeleteRsvpMapLabelTable (pL3VpnRsvpMapLabelEntry)) ==
                OSIX_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetFsMplsL3VpnRsvpTeMapRowStatus destroy Failed \t\n");
                return SNMP_FAILURE;
            }
            break;
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "SetFsMplsL3VpnRsvpTeMapRowStatus Failed \t\n");
            return SNMP_FAILURE;
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "SetFsMplsL3VpnRsvpTeMapRowStatus Exit \t\n");
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsMplsL3VpnRsvpTeMapTable
Input       :  The Indices
FsMplsL3VpnRsvpTeMapPrefixType
FsMplsL3VpnRsvpTeMapPrefix
FsMplsL3VpnRsvpTeMapMaskType
FsMplsL3VpnRsvpTeMapMask
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhDepv2FsMplsL3VpnRsvpTeMapTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
#endif

/*----------------------------------------------------------------------------*/
/*                      End of File rpteset.c                                 */
/*----------------------------------------------------------------------------*/
