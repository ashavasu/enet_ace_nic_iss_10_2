
 /* $Id: fsrsvpwr.c,v 1.15 2018/01/03 11:31:23 siva Exp $ */

#include "lr.h"
#include "fssnmp.h"
#include "fsrsvpwr.h"
#include "fsrsvpdb.h"
#include "rpteincs.h"

VOID
RegisterFSRSVP ()
{
    SNMPRegisterMibWithLock (&fsrsvpOID, &fsrsvpEntry, RsvpTeLock,
                             RsvpTeUnLock, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsrsvpOID, (const UINT1 *) "fsMplsRsvpTeMIB");
}

VOID
UnRegisterFSRSVP ()
{
    SNMPUnRegisterMib (&fsrsvpOID, &fsrsvpEntry);
    SNMPDelSysorEntry (&fsrsvpOID, (const UINT1 *) "fsrsvpte");
}

INT4
FsMplsRsvpTeIfLblSpaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeIfLblSpace (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfLblSpaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeIfLblSpace (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfLblSpaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfLblSpace (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfLblTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeIfLblType (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfLblTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeIfLblType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfLblTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfLblType (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMergeCapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeAtmMergeCap (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMergeCapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeAtmMergeCap (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMergeCapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeAtmMergeCap (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmVcDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeAtmVcDirection (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmVcDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeAtmVcDirection
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmVcDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeAtmVcDirection
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMinVpiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeAtmMinVpi (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMinVpiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeAtmMinVpi (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMinVpiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeAtmMinVpi (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMinVciTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeAtmMinVci (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMinVciSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeAtmMinVci (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMinVciGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeAtmMinVci (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMaxVpiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeAtmMaxVpi (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMaxVpiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeAtmMaxVpi (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMaxVpiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeAtmMaxVpi (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMaxVciTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeAtmMaxVci (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMaxVciSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeAtmMaxVci (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAtmMaxVciGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeAtmMaxVci (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfMtuTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeIfMtu (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfMtuSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeIfMtu (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfMtuGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfMtu (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfUdpNbrsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfUdpNbrs (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfIpNbrsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfIpNbrs (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNbrsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNbrs (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfRefreshMultipleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeIfRefreshMultiple (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfRefreshMultipleSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeIfRefreshMultiple
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfRefreshMultipleGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfRefreshMultiple
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfTTLTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeIfTTL (pu4Error,
                                        pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfTTLSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeIfTTL (pMultiIndex->pIndex[0].i4_SLongValue,
                                     pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfTTLGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfTTL (pMultiIndex->pIndex[0].i4_SLongValue,
                                     &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfRefreshIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeIfRefreshInterval (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfRefreshIntervalSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeIfRefreshInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfRefreshIntervalGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfRefreshInterval
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfRouteDelayTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeIfRouteDelay (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfRouteDelaySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeIfRouteDelay
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfRouteDelayGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfRouteDelay
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfUdpRequiredTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeIfUdpRequired (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfUdpRequiredSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeIfUdpRequired
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfUdpRequiredGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfUdpRequired
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfHelloSupportedTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeIfHelloSupported (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfHelloSupportedSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeIfHelloSupported
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfHelloSupportedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfHelloSupported
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfLinkAttrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsRsvpTeIfLinkAttr (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             i4_SLongValue,
                                             pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfLinkAttrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsRsvpTeIfLinkAttr (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
FsMplsRsvpTeIfLinkAttrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfLinkAttr (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeIfStatus (pu4Error,
                                           pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeIfStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                        &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeIfPlrIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfPlrId (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsMplsRsvpTeIfAvoidNodeIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfAvoidNodeId
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsRsvpTeIfStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsRsvpTeIfStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeIfPlrIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsRsvpTeIfPlrId (pMultiIndex->pIndex[0].i4_SLongValue,
                                       pMultiData->pOctetStrValue));

}

INT4
FsMplsRsvpTeIfAvoidNodeIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsRsvpTeIfAvoidNodeId
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsRsvpTeIfStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsRsvpTeIfStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsRsvpTeIfPlrIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsRsvpTeIfPlrId (pu4Error,
                                          pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->pOctetStrValue));

}

INT4
FsMplsRsvpTeIfAvoidNodeIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsRsvpTeIfAvoidNodeId (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->pOctetStrValue));

}

INT4
FsMplsRsvpTeIfStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsRsvpTeIfStorageType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsMplsRsvpTeIfTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeIfTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeIfNumTnlsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumTnls (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumMsgSentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumMsgSent
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumMsgRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumMsgRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumHelloSentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumHelloSent
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumHelloRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumHelloRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumPathErrSentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPathErrSent
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumPathErrRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPathErrRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumPathTearSentGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPathTearSent
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumPathTearRcvdGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPathTearRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumResvErrSentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumResvErrSent
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumResvErrRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumResvErrRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumResvTearSentGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumResvTearSent
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumResvTearRcvdGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumResvTearRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumResvConfSentGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumResvConfSent
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumResvConfRcvdGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumResvConfRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumBundleMsgSentGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumBundleMsgSent
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumBundleMsgRcvdGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumBundleMsgRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumSRefreshMsgSentGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumSRefreshMsgSent
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumSRefreshMsgRcvdGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumSRefreshMsgRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeIfNumPathSentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPathSent
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumPathRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPathRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumResvSentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumResvSent
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumResvRcvdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumResvRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumNotifyMsgSentGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumNotifyMsgSent
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumNotifyMsgRcvdGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumNotifyMsgRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumRecoveryPathSentGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsRsvpTeIfNumRecoveryPathSent
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumRecoveryPathRcvdGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsRsvpTeIfNumRecoveryPathRcvd
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumPathSentWithRecoveryLblGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPathSentWithRecoveryLbl
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumPathRcvdWithRecoveryLblGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPathRcvdWithRecoveryLbl
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumPathSentWithSuggestedLblGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPathSentWithSuggestedLbl
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumPathRcvdWithSuggestedLblGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPathRcvdWithSuggestedLbl
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumHelloSentWithRestartCapGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumHelloSentWithRestartCap
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumHelloRcvdWithRestartCapGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumHelloRcvdWithRestartCap
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumHelloSentWithCapabilityGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumHelloSentWithCapability
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumHelloRcvdWithCapabilityGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumHelloRcvdWithCapability
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeIfNumPktDiscrdedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeIfNumPktDiscrded
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsRsvpTeNbrRRCapableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeNbrRRCapable (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrRRCapableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeNbrRRCapable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrRRCapableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrRRCapable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrRRStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrRRState (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrRMDCapableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeNbrRMDCapable (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrRMDCapableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeNbrRMDCapable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrRMDCapableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrRMDCapable
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrEncapsTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeNbrEncapsType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                i4_SLongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrEncapsTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeNbrEncapsType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrEncapsTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrEncapsType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrHelloSupportTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeNbrHelloSupport (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrHelloSupportSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeNbrHelloSupport
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrHelloSupportGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrHelloSupport
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrHelloStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrHelloState
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrHelloRelationGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrHelloRelation
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrSrcInstInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrSrcInstInfo
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrDestInstInfoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrDestInstInfo
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrCreationTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrCreationTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeNbrLclRprDetectionTimeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrLclRprDetectionTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeNbrNumTunnelsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrNumTunnels
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, &pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeNbrStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{

    return (nmhTestv2FsMplsRsvpTeNbrStatus (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeNbrTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeNbrStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    return (nmhSetFsMplsRsvpTeNbrStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{

    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsRsvpTeNbrStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         &pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNbrGrRecoveryPathCapabilityGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsRsvpTeNbrGrRecoveryPathCapability
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsRsvpTeNbrGrRestartTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsRsvpTeNbrGrRestartTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsRsvpTeNbrGrRecoveryTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsRsvpTeNbrGrRecoveryTime
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsRsvpTeNbrGrProgressStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsRsvpTeNbrGrProgressStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsRsvpTeNbrStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsRsvpTeNbrTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsRsvpTeNbrStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeNbrStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsRsvpTeNbrStorageType
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsRsvpTeNbrStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsRsvpTeNbrStorageType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeLsrIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeLsrID (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsMplsRsvpTeLsrIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeLsrID (pMultiData->pOctetStrValue));
}

INT4
FsMplsRsvpTeLsrIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeLsrID (pMultiData->pOctetStrValue));
}

INT4
FsMplsRsvpTeMaxTnlsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeMaxTnls (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxTnlsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeMaxTnls (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxTnlsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeMaxTnls (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxErhopsPerTnlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeMaxErhopsPerTnl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxErhopsPerTnlSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeMaxErhopsPerTnl (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxErhopsPerTnlGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeMaxErhopsPerTnl (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxActRoutePerTnlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeMaxActRoutePerTnl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxActRoutePerTnlSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeMaxActRoutePerTnl (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxActRoutePerTnlGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeMaxActRoutePerTnl (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxIfacesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeMaxIfaces
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxIfacesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeMaxIfaces (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxIfacesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeMaxIfaces (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxNbrsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeMaxNbrs (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxNbrsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeMaxNbrs (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMaxNbrsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeMaxNbrs (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeSockSupprtdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeSockSupprtd
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeSockSupprtdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeSockSupprtd (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeSockSupprtdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeSockSupprtd (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeHelloSupprtdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeHelloSupprtd
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeHelloSupprtdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeHelloSupprtd (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeHelloSupprtdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeHelloSupprtd (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeHelloIntervalTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeHelloIntervalTime
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeHelloIntervalTimeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeHelloIntervalTime (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeHelloIntervalTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeHelloIntervalTime (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeRRCapableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeRRCapable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeRRCapableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeRRCapable (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeRRCapableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeRRCapable (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMsgIdCapableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeMsgIdCapable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMsgIdCapableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeMsgIdCapable (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMsgIdCapableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeMsgIdCapable (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeRMDPolicyObjectTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeRMDPolicyObject
            (pu4Error, pMultiData->pOctetStrValue));
}

INT4
FsMplsRsvpTeRMDPolicyObjectSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeRMDPolicyObject (pMultiData->pOctetStrValue));
}

INT4
FsMplsRsvpTeRMDPolicyObjectGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeRMDPolicyObject (pMultiData->pOctetStrValue));
}

INT4
FsMplsRsvpTeGenLblSpaceMinLblTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeGenLblSpaceMinLbl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenLblSpaceMinLblSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeGenLblSpaceMinLbl (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenLblSpaceMinLblGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGenLblSpaceMinLbl (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenLblSpaceMaxLblTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeGenLblSpaceMaxLbl
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenLblSpaceMaxLblSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeGenLblSpaceMaxLbl (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenLblSpaceMaxLblGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGenLblSpaceMaxLbl (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenDebugFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeGenDebugFlag
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeGenDebugFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeGenDebugFlag (pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeGenDebugFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGenDebugFlag (&pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeGenPduDumpLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeGenPduDumpLevel
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenPduDumpLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeGenPduDumpLevel (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenPduDumpLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGenPduDumpLevel (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenPduDumpMsgTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeGenPduDumpMsgType
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenPduDumpMsgTypeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeGenPduDumpMsgType (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenPduDumpMsgTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGenPduDumpMsgType (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenPduDumpDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeGenPduDumpDirection
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenPduDumpDirectionSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeGenPduDumpDirection (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGenPduDumpDirectionGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGenPduDumpDirection (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeOperStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeOperStatus
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeOperStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeOperStatus (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeOperStatus (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeOverRideOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeOverRideOption
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeOverRideOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeOverRideOption (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeOverRideOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeOverRideOption (&pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeMinTnlsWithMsgIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeMinTnlsWithMsgId
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeLsrIDDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeLsrID
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeMaxTnlsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeMaxTnls
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeMaxErhopsPerTnlDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeMaxErhopsPerTnl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeMaxActRoutePerTnlDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeMaxActRoutePerTnl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeMaxIfacesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeMaxIfaces
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeMaxNbrsDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeMaxNbrs
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeSockSupprtdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeSockSupprtd
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeHelloSupprtdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeHelloSupprtd
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeHelloIntervalTimeDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeHelloIntervalTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeRRCapableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeRRCapable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeMsgIdCapableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeMsgIdCapable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeRMDPolicyObjectDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeRMDPolicyObject
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeGenLblSpaceMinLblDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGenLblSpaceMinLbl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeGenLblSpaceMaxLblDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGenLblSpaceMaxLbl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeGenDebugFlagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGenDebugFlag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeGenPduDumpLevelDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGenPduDumpLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeGenPduDumpMsgTypeDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGenPduDumpMsgType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeGenPduDumpDirectionDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGenPduDumpDirection
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeOperStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeOperStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeOverRideOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeOverRideOption
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeMinTnlsWithMsgIdDep (UINT4 *pu4Error,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeMinTnlsWithMsgId
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeMinTnlsWithMsgIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeMinTnlsWithMsgId (pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeMinTnlsWithMsgIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeMinTnlsWithMsgId (&pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeNotificationEnabledGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeNotificationEnabled
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitIntvlGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeNotifyMsgRetransmitIntvl
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitDecayGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeNotifyMsgRetransmitDecay
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitLimitGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeNotifyMsgRetransmitLimit
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsMplsRsvpTeAdminStatusTimeIntvlGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeAdminStatusTimeIntvl
            (&(pMultiData->u4_ULongValue)));
}

INT4
FsMplsRsvpTePathStateRemovedSupportGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTePathStateRemovedSupport
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeLabelSetEnabledGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeLabelSetEnabled (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeAdminStatusCapabilityGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeAdminStatusCapability
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeGrCapabilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGrCapability (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeGrRecoveryPathCapabilityGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGrRecoveryPathCapability
            (pMultiData->pOctetStrValue));
}

INT4
FsMplsRsvpTeGrRestartTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGrRestartTime (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeGrRecoveryTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGrRecoveryTime (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeGrProgressStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeGrProgressStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeReoptimizeTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeReoptimizeTime (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeEroCacheTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeEroCacheTime (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeReoptLinkMaintenanceGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeReoptLinkMaintenance
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeReoptNodeMaintenanceGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsRsvpTeReoptNodeMaintenance
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsRsvpTeGrCapabilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeGrCapability (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGrRecoveryPathCapabilitySet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeGrRecoveryPathCapability
            (pMultiData->pOctetStrValue));
}

INT4
FsMplsRsvpTeGrRestartTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeGrRestartTime (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGrRecoveryTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeGrRecoveryTime (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeReoptimizeTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeReoptimizeTime (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeEroCacheTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeEroCacheTime (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeReoptLinkMaintenanceSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeReoptLinkMaintenance (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeReoptNodeMaintenanceSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeReoptNodeMaintenance (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGrCapabilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeGrCapability (pu4Error,
                                               pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGrRecoveryPathCapabilityTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeGrRecoveryPathCapability (pu4Error,
                                                           pMultiData->
                                                           pOctetStrValue));
}

INT4
FsMplsRsvpTeGrRestartTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeGrRestartTime (pu4Error,
                                                pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeGrRecoveryTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeGrRecoveryTime (pu4Error,
                                                 pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeReoptimizeTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeReoptimizeTime
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeEroCacheTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeEroCacheTime
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeReoptLinkMaintenanceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeReoptLinkMaintenance
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeReoptNodeMaintenanceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeReoptNodeMaintenance
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNotificationEnabledSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeNotificationEnabled (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitIntvlSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeNotifyMsgRetransmitIntvl
            (pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitDecaySet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeNotifyMsgRetransmitDecay
            (pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitLimitSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeNotifyMsgRetransmitLimit
            (pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeAdminStatusTimeIntvlSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeAdminStatusTimeIntvl (pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTePathStateRemovedSupportSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTePathStateRemovedSupport
            (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeLabelSetEnabledSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeLabelSetEnabled (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAdminStatusCapabilitySet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsRsvpTeAdminStatusCapability
            (pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNotificationEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeNotificationEnabled
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitIntvlTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitIntvl
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitDecayTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitDecay
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitLimitTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitLimit
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTeAdminStatusTimeIntvlTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeAdminStatusTimeIntvl
            (pu4Error, pMultiData->u4_ULongValue));
}

INT4
FsMplsRsvpTePathStateRemovedSupportTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTePathStateRemovedSupport
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeLabelSetEnabledTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeLabelSetEnabled
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeAdminStatusCapabilityTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsRsvpTeAdminStatusCapability
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsRsvpTeNotificationEnabledDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeNotificationEnabled
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitIntvlDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitIntvl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitDecayDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitDecay
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeNotifyMsgRetransmitLimitDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitLimit
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeAdminStatusTimeIntvlDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeAdminStatusTimeIntvl
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTePathStateRemovedSupportDep (UINT4 *pu4Error,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTePathStateRemovedSupport
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeLabelSetEnabledDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeLabelSetEnabled
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeAdminStatusCapabilityDep (UINT4 *pu4Error,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeAdminStatusCapability
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsRsvpTeNbrTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    INT4                i4fsMplsRsvpTeIfIndex;
    UINT4               u4fsMplsRsvpTeNbrIfAddr;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsRsvpTeNbrTable (&i4fsMplsRsvpTeIfIndex,
                                                  &u4fsMplsRsvpTeNbrIfAddr)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsRsvpTeNbrTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue, &i4fsMplsRsvpTeIfIndex,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &u4fsMplsRsvpTeNbrIfAddr) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4fsMplsRsvpTeIfIndex;
    pNextMultiIndex->pIndex[1].u4_ULongValue = u4fsMplsRsvpTeNbrIfAddr;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMplsRsvpTeIfStatsTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    INT4                i4fsMplsRsvpTeIfIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsRsvpTeIfStatsTable (&i4fsMplsRsvpTeIfIndex)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsRsvpTeIfStatsTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4fsMplsRsvpTeIfIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4fsMplsRsvpTeIfIndex;
    return SNMP_SUCCESS;
}

INT4
GetNextIndexFsMplsRsvpTeIfTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    INT4                i4fsMplsRsvpTeIfIndex;
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsRsvpTeIfTable (&i4fsMplsRsvpTeIfIndex)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsRsvpTeIfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &i4fsMplsRsvpTeIfIndex) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    pNextMultiIndex->pIndex[0].i4_SLongValue = i4fsMplsRsvpTeIfIndex;
    return SNMP_SUCCESS;
}

INT4
FsMplsRsvpTeGrCapabilityDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGrCapability
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeGrRecoveryPathCapabilityDep (UINT4 *pu4Error,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGrRecoveryPathCapability
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeGrRestartTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGrRestartTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeGrRecoveryTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGrRecoveryTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeReoptimizeTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeReoptimizeTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeEroCacheTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeEroCacheTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeReoptLinkMaintenanceDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeReoptLinkMaintenance
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeReoptNodeMaintenanceDep (UINT4 *pu4Error,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeReoptNodeMaintenance
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

#ifdef MPLS_L3VPN_WANTED
INT4
GetNextIndexFsMplsL3VpnRsvpTeMapTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsL3VpnRsvpTeMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsL3VpnRsvpTeMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMplsL3VpnRsvpTeMapTnlIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsL3VpnRsvpTeMapTnlIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsL3VpnRsvpTeMapRowStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsL3VpnRsvpTeMapRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsL3VpnRsvpTeMapTnlIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnRsvpTeMapTnlIndex
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsL3VpnRsvpTeMapRowStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnRsvpTeMapRowStatus
            (pMultiIndex->pIndex[0].i4_SLongValue,
             pMultiIndex->pIndex[1].pOctetStrValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsL3VpnRsvpTeMapTnlIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnRsvpTeMapTnlIndex (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[2].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[3].
                                                   pOctetStrValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
FsMplsL3VpnRsvpTeMapRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnRsvpTeMapRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[1].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[3].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
FsMplsL3VpnRsvpTeMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsL3VpnRsvpTeMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

#endif
