/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptentfy.c,v 1.10 2017/06/08 11:40:32 siva Exp $
 *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptentfy.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for the Notify message
 *                             Handler Module.
 *
 *---------------------------------------------------------------------------*/
#include "rpteincs.h"

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhAddTnlToNotifyRecipient
 * Description     : This function creates Recipeient Node
 * Input (s)       : pRsvpTeTnlInfo  - Pointer to RSVP-TE tnl Structure.
 *                   u4NotifyAddr    - Notify address
 *                   u4NotifyAddr    - Message Direction
 *                   u1IsGroupTmrReq - Timer flag
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteNhAddTnlToNotifyRecipient (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                               UINT4 u4NotifyAddr, UINT4 u4MsgDirection,
                               UINT1 u1IsGroupTmrReq, UINT1 u1ErrCode,
                               UINT2 u2ErrValue)
{
    tNotifyMsgTnl      *pNotifyMsgTnl = NULL;
    tNotifyRecipient   *pInNotifyRecipient = NULL;
    tNotifyRecipient   *pNotifyRecipient = NULL;
    tNotifyRecipient   *pTempNotifyRecipient = NULL;
    UINT4               u4MsgSize = RPTE_ZERO;
    UINT1               u1IsEntryFound = RPTE_ZERO;

    RSVPTE_DBG3 (RSVPTE_PH_ETEXT,
                 "RpteNhAddTnlToNotifyRecipient: ENTRY u2ErrValue: %u TnlId: %d TnlInst: %d\n",
                 u2ErrValue, pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                 pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);

    pInNotifyRecipient =
        (tNotifyRecipient *) MemAllocMemBlk (RSVPTE_NOTIFY_RECIPIENT_POOL_ID);

    if (pInNotifyRecipient == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RpteNhAddTnlToNotifyRecipient: INMD_EXIT1 \n");
        return;
    }

    pInNotifyRecipient->u4RecipientAddr = u4NotifyAddr;
    pInNotifyRecipient->u1ErrorCode = u1ErrCode;
    pInNotifyRecipient->u2ErrorValue = u2ErrValue;

    pNotifyRecipient = RBTreeGetNext (gpRsvpTeGblInfo->NotifyRecipientTree,
                                      (tRBElem *) pInNotifyRecipient, NULL);
    MemReleaseMemBlock (RSVPTE_NOTIFY_RECIPIENT_POOL_ID,
                        (UINT1 *) pInNotifyRecipient);

    while (pNotifyRecipient != NULL)
    {
        if ((pNotifyRecipient->u4MsgSize <=
             (RPTE_NOTIFY_ONE_NOTIFYMSG -
              RPTE_NOTIFY_ONE_NOTIFYMSG_HEADER_SIZE))
            && (pNotifyRecipient->u4RecipientAddr == u4NotifyAddr)
            && (pNotifyRecipient->u1ErrorCode == u1ErrCode)
            && (pNotifyRecipient->u2ErrorValue == u2ErrValue))
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RpteNhAddTnlToNotifyRecipient: INMD_EXIT2 \n");
            break;
        }
        pNotifyRecipient = RBTreeGetNext (gpRsvpTeGblInfo->NotifyRecipientTree,
                                          (tRBElem *) pNotifyRecipient, NULL);
    }

    if (pNotifyRecipient == NULL)
    {
        pNotifyRecipient =
            (tNotifyRecipient *)
            MemAllocMemBlk (RSVPTE_NOTIFY_RECIPIENT_POOL_ID);
        if (pNotifyRecipient == NULL)
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RpteNhAddTnlToNotifyRecipient: INMD_EXIT3 \n");
            return;
        }

        pNotifyRecipient->b1IsTimerStarted = RPTE_FALSE;

        pNotifyRecipient->u4MsgSize = RPTE_ZERO;
        pNotifyRecipient->u4RecipientAddr = u4NotifyAddr;
        RpteMIHGenerateMsgId (&pNotifyRecipient->u4MsgId);
        pNotifyRecipient->u1ErrorCode = u1ErrCode;
        pNotifyRecipient->u2ErrorValue = u2ErrValue;
        pNotifyRecipient->u4RetryInterval
            = gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitIntvl;
        pNotifyRecipient->u4RetryLimit
            = gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitLimit;

        TMO_SLL_Init (&pNotifyRecipient->NotifyMsgTnlList);

        if (RBTreeAdd (gpRsvpTeGblInfo->NotifyRecipientTree,
                       (tRBElem *) pNotifyRecipient) == RB_FAILURE)
        {
            if (MemReleaseMemBlock (RSVPTE_NOTIFY_RECIPIENT_POOL_ID,
                                    (UINT1 *) pNotifyRecipient) == RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_PH_ETEXT,
                            "RpteNhAddTnlToNotifyRecipient: INMD_EXIT4 \n");
                return;
            }
        }
    }
    TMO_SLL_Scan (&pNotifyRecipient->NotifyMsgTnlList, pNotifyMsgTnl,
                  tNotifyMsgTnl *)
    {
        if (pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.u2TnlId ==
            RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo))
        {
            if ((pRsvpTeTnlInfo->u1NotifyMsgProtState ==
                 RSVP_PROT_STATE_SB_RESP_SENT) &&
                (u2ErrValue == RPTE_LSP_RECOVERED) &&
                (pRsvpTeTnlInfo->b1IsTimerStarted == RPTE_TRUE))
            {
                RpteNhSendNotifyMsg (pNotifyRecipient);
            }
            else if ((pRsvpTeTnlInfo->u1NotifyMsgProtState ==
                      RPTE_ZERO) &&
                     (u2ErrValue == RPTE_LSP_RECOVERED) &&
                     (pRsvpTeTnlInfo->b1IsTimerStarted == RPTE_FALSE))
            {
                RSVPTE_DBG2 (RSVPTE_PH_ETEXT,
                             "Starting SB timer for existing TnlId: %d "
                             "TnlInst: %d\n",
                             pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                             pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);
                RpteNhStartGroupingTmr (pNotifyRecipient,
                                        RPTE_TUN_GROUP_TIMER_MAX_VALUE);
                pRsvpTeTnlInfo->b1IsTimerStarted = RPTE_TRUE;
                pNotifyRecipient->b1IsTimerStarted = RPTE_TRUE;
            }

            RSVPTE_DBG (RSVPTE_PH_ETEXT,
                        "RpteNhAddTnlToNotifyRecipient: INMD_EXIT5 \n");
            return;
        }
    }

    pNotifyMsgTnl =
        (tNotifyMsgTnl *) MemAllocMemBlk (RSVPTE_NOTIFY_TNL_POOL_ID);

    if (pNotifyMsgTnl == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "RpteNhAddTnlToNotifyRecipient: INMD_EXIT6 \n");
        return;
    }

    MEMSET (pNotifyMsgTnl, RPTE_ZERO, sizeof (tNotifyMsgTnl));

    TMO_SLL_Init_Node (&(pNotifyMsgTnl->NextTnl));

    pNotifyMsgTnl->u4MsgType = u4MsgDirection;

    /* session object */
    MEMCPY ((pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.TnlDestAddr),
            RSVPTE_TNL_EGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo)),
            IPV4_ADDR_LENGTH);

    MEMCPY ((pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.ExtnTnlId),
            RSVPTE_TNL_EXTN_TNLID (pRsvpTeTnlInfo), IPV4_ADDR_LENGTH);

    pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.u2TnlId =
        (UINT2) RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);

    u4MsgSize += sizeof (tRsvpTeSsnObj);
    /* Filter spec object for down stream */
    if (pNotifyMsgTnl->u4MsgType == DOWNSTREAM)
    {
        MEMCPY (pNotifyMsgTnl->RsvpTeFilterSpecObj.RsvpTeFilterSpec.TnlSndrAddr,
                RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                IPV4_ADDR_LENGTH);
        pNotifyMsgTnl->RsvpTeFilterSpecObj.RsvpTeFilterSpec.u2LspId =
            (UINT2) RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (pRsvpTeTnlInfo));
        u4MsgSize += sizeof (tRsvpTeFilterSpecObj);
    }
    /* Sender template object for up stream */
    else if (pNotifyMsgTnl->u4MsgType == UPSTREAM)
    {
        MEMCPY (pNotifyMsgTnl->RsvpTeSndrTmpObj.RsvpTeSndrTmp.TnlSndrAddr,
                RSVPTE_TNL_INGRESS_RTR_ID (RPTE_TE_TNL (pRsvpTeTnlInfo)),
                IPV4_ADDR_LENGTH);
        pNotifyMsgTnl->RsvpTeSndrTmpObj.RsvpTeSndrTmp.u2LspId =
            (UINT2) RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (pRsvpTeTnlInfo));
        u4MsgSize += sizeof (tRsvpTeSndrTmpObj);
    }

    pNotifyRecipient->u4MsgSize += u4MsgSize;
    pNotifyMsgTnl->u4MsgSize = u4MsgSize;

    TMO_SLL_Add (&(pNotifyRecipient->NotifyMsgTnlList),
                 &(pNotifyMsgTnl->NextTnl));

    if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
         MPLS_TE_DEDICATED_ONE2ONE)
        && ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == TE_INGRESS)
            || (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlRole == TE_EGRESS)))
    {

        if ((u2ErrValue == RPTE_LSP_RECOVERED) &&
            (pRsvpTeTnlInfo->b1IsTimerStarted == RPTE_FALSE) &&
            ((pRsvpTeTnlInfo->u1NotifyMsgProtState == RPTE_ZERO) ||
             (pRsvpTeTnlInfo->u1NotifyMsgProtState ==
              RSVP_PROT_STATE_SB_REQ_SENT)))
        {
            RSVPTE_DBG2 (RSVPTE_PH_ETEXT,
                         "Starting SB timer for TnlId: %d TnlInst: %d\n",
                         pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                         pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);
            RpteNhStartGroupingTmr (pNotifyRecipient,
                                    RPTE_TUN_GROUP_TIMER_MAX_VALUE);
            pRsvpTeTnlInfo->b1IsTimerStarted = RPTE_TRUE;
            pNotifyRecipient->b1IsTimerStarted = RPTE_TRUE;
            return;
        }
        else if ((u2ErrValue == RPTE_LSP_RECOVERED) &&
                 (pRsvpTeTnlInfo->b1IsTimerStarted == RPTE_TRUE) &&
                 (pRsvpTeTnlInfo->u1NotifyMsgProtState ==
                  RSVP_PROT_STATE_SB_REQ_SENT))
        {
            RSVPTE_DBG2 (RSVPTE_PH_ETEXT, "SB timer for TnlId: %d TnlInst: %d\n"
                         "already running and Notify rxed from transit node,\n"
                         "no action to be taken\n",
                         pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                         pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);
            return;
        }

        if (((u2ErrValue == RPTE_LSP_FAILURE)
             || (u2ErrValue == RPTE_LSP_LOCALLY_FAILED)))
        {
            pInNotifyRecipient =
                (tNotifyRecipient *)
                MemAllocMemBlk (RSVPTE_NOTIFY_RECIPIENT_POOL_ID);

            if (pInNotifyRecipient == NULL)
            {
                RSVPTE_DBG1 (RSVPTE_PVM_PRCS, "%s :INT_EXIT2\n", __func__);
                return;
            }

            pInNotifyRecipient->u4RecipientAddr = u4NotifyAddr;

            pTempNotifyRecipient =
                RBTreeGetNext (gpRsvpTeGblInfo->NotifyRecipientTree,
                               (tRBElem *) pInNotifyRecipient, NULL);

            MemReleaseMemBlock (RSVPTE_NOTIFY_RECIPIENT_POOL_ID,
                                (UINT1 *) pInNotifyRecipient);

            while (pTempNotifyRecipient != NULL)
            {
                if ((pTempNotifyRecipient->u4RecipientAddr == u4NotifyAddr) &&
                    (pTempNotifyRecipient->u2ErrorValue == RPTE_LSP_RECOVERED)
                    && (pTempNotifyRecipient->b1IsTimerStarted == RPTE_TRUE))
                {
                    /* If LSP_FAILURE event is received now but LSP_RECOVERED has received
                       +            * already and switch back revert timer is running, then remove the
                       +            * corresponding tunnel from switch back list */
                    TMO_SLL_Scan (&pTempNotifyRecipient->NotifyMsgTnlList,
                                  pNotifyMsgTnl, tNotifyMsgTnl *)
                    {
                        if (pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.u2TnlId ==
                            RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo))
                        {
                            pTempNotifyRecipient->u4MsgSize -=
                                pNotifyMsgTnl->u4MsgSize;
                            TMO_SLL_Delete (&pTempNotifyRecipient->
                                            NotifyMsgTnlList,
                                            &pNotifyMsgTnl->NextTnl);
                            u1IsEntryFound = RPTE_TRUE;
                            break;
                        }
                    }

                    /* Remove the notify entry if doesn't hold any tunnel entry */

                    if (TMO_SLL_Count (&pTempNotifyRecipient->NotifyMsgTnlList)
                        == RPTE_ZERO)
                    {
                        RpteNhStopGroupingTmr (pTempNotifyRecipient);
                        pRsvpTeTnlInfo->b1IsTimerStarted = RPTE_FALSE;
                        RpteNhDelRBNode (pTempNotifyRecipient);
                    }
                    if (u1IsEntryFound == RPTE_TRUE)
                    {
                        break;
                    }
                }
                pTempNotifyRecipient =
                    RBTreeGetNext (gpRsvpTeGblInfo->NotifyRecipientTree,
                                   (tRBElem *) pTempNotifyRecipient, NULL);
            }
        }

    }

    if ((u1IsGroupTmrReq == RPTE_FALSE)
        || (pNotifyRecipient->u4MsgSize == RPTE_NOTIFY_ONE_NOTIFYMSG))
    {
        RpteNhSendNotifyMsg (pNotifyRecipient);
    }
    else if (pNotifyRecipient->b1IsTimerStarted == RPTE_FALSE)
    {
        RSVPTE_DBG3 (RSVPTE_PH_ETEXT,
                     "Starting grouping timer for TnlId: %d TnlInst: %d with errorcode: %u\n",
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance, u2ErrValue);
        RpteNhStartGroupingTmr (pNotifyRecipient,
                                RPTE_TUN_GROUP_TIMER_DEFAULT_VALUE);
        pNotifyRecipient->b1IsTimerStarted = RPTE_TRUE;
    }

    RSVPTE_DBG1 (RSVPTE_PVM_PRCS, "%s :EXIT\n", __func__);
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhStartGroupingTmr
 * Description     : This function starts the Grouping Timer
 * Input (s)       : pNotifyRecipient - Pointer to NotifyRecipient Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteNhStartGroupingTmr (tNotifyRecipient * pNotifyRecipient,
                        UINT4 u4GroupTmrValue)
{
    tTimerParam        *pTmrParam = NULL;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNotifyStartGroupingTmr: ENTRY \n");

    pTmrParam = (tTimerParam *) & (pNotifyRecipient->NotifyTimerParam);
    TIMER_PARAM_ID (pTmrParam) = RPTE_NOTIFY_GROUPING_TMR_EXPIRE;
    pTmrParam->u.pNotifyRecipient = pNotifyRecipient;
    pNotifyRecipient->NotifyTimer.u4Data = (FS_ULONG) pTmrParam;

    if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST,
                           &(pNotifyRecipient->NotifyTimer),
                           (u4GroupTmrValue *
                            SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        /* Start Timer failed. */
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "Grouping Timer failed\n");
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNotifyStartGroupingTmr: EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhSendNotifyMsg
 * Description     : This function sends Notify message
 * Input (s)       : pNotifyRecipient - Pointer to NotifyRecipient Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteNhSendNotifyMsg (tNotifyRecipient * pNotifyRecipient)
{
    tPktMap            *pPktMap = NULL;
    tObjHdr            *pObjHdr = NULL;
    tRsvpHdr           *pRsvpHdr = NULL;
    tTMO_SLL           *pNotifyTnlList = NULL;
    tNotifyMsgTnl      *pNotifyMsgTnl = NULL;
    UINT2               u2NotifyMsgSize = RPTE_ZERO;
    UINT1              *pPdu = NULL;
    UINT4               u4TempAdd = RSVPTE_ZERO;
    UINT4               u4RecipientAddr = RSVPTE_ZERO;
    tErrorSpecObj       ErrorSpecObj;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNotifySendNotifyMsg : ENTRY \n");
    if ((pPktMap = MemAllocMemBlk (RSVPTE_PKT_MAP_POOL_ID)) == NULL)
    {
        return;
    }

    RpteUtlInitPktMap (pPktMap);

    CONVERT_TO_INTEGER (gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId,
                        u4TempAdd);
    u4TempAdd = OSIX_NTOHL (u4TempAdd);

    PKT_MAP_SRC_ADDR (pPktMap) = u4TempAdd;

    if (pNotifyRecipient->u4RecipientAddr == RSVPTE_ZERO)
    {
        u4RecipientAddr = u4TempAdd;
    }
    else
    {
        u4RecipientAddr = pNotifyRecipient->u4RecipientAddr;
    }

    PKT_MAP_DST_ADDR (pPktMap) = u4RecipientAddr;
    PKT_MAP_BUILD_PKT (pPktMap) = RPTE_YES;

    PKT_MAP_RSVP_PKT (pPktMap) = MemAllocMemBlk (RSVPTE_RSVP_PKT_POOL_ID);

    if (PKT_MAP_RSVP_PKT (pPktMap) == NULL)
    {
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "PATH : Failed to allocate memory for the packet \n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNotifySendNotifyMsg : INTMD-EXIT \n");
        MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
        return;
    }

    /* Formation of Error spec object */
    PKT_MAP_ERROR_SPEC_OBJ (pPktMap) = &ErrorSpecObj;
    ERROR_SPEC_ADDR (&ERROR_SPEC_OBJ (&ErrorSpecObj)) = u4TempAdd;
    /* Fill the Errospecc flag value */
    RptePvmFillErrorSpecFlags (&(ERROR_SPEC_FLAGS (&ERROR_SPEC_OBJ
                                                   (&ErrorSpecObj))));

    ERROR_SPEC_CODE (&ERROR_SPEC_OBJ (&ErrorSpecObj)) =
        pNotifyRecipient->u1ErrorCode;
    ERROR_SPEC_VALUE (&ERROR_SPEC_OBJ (&ErrorSpecObj)) =
        OSIX_HTONS (pNotifyRecipient->u2ErrorValue);

    u2NotifyMsgSize = (UINT2) RpteUtlNtfyMsgSizeCalculation (pNotifyRecipient);

    MEMSET (PKT_MAP_RSVP_PKT (pPktMap), RSVPTE_ZERO, u2NotifyMsgSize);
    PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_RSVP_ALLOC;

    /* Disable RA option */
    PKT_MAP_RA_OPT_NEEDED (pPktMap) = RPTE_YES;

    /* Header of RSVP Packet */
    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    MEMSET (pRsvpHdr, RSVPTE_ZERO, sizeof (tRsvpHdr));
    RSVP_HDR_VER_FLAG (pRsvpHdr) = (RSVPTE_ZERO |
                                    (RSVP_VERSION << LSHIFT_4BITS));
    RSVP_HDR_LENGTH (pRsvpHdr) = OSIX_HTONS (u2NotifyMsgSize);
    RSVP_HDR_MSG_TYPE (pRsvpHdr) = NOTIFY_MSG;
    RSVP_HDR_SEND_TTL (pRsvpHdr) = DEFAULT_TTL;

    pObjHdr =
        (tObjHdr *) (VOID *) (PKT_MAP_RSVP_PKT (pPktMap) + sizeof (tRsvpHdr));

    /* Msg Id Obj update, if present */
    if (gu1MsgIdCapable != RPTE_ENABLED)
    {
        gu1MsgIdCapable = RPTE_ENABLED;
        gu1IntMsgIdCapable = RPTE_TRUE;
    }
    RptePvmFillMsgIdObj (&pObjHdr, pNotifyRecipient->u4MsgId);
    pObjHdr = NEXT_OBJ (pObjHdr);

    pNotifyTnlList = &(pNotifyRecipient->NotifyMsgTnlList);
    TMO_SLL_Scan (pNotifyTnlList, pNotifyMsgTnl, tNotifyMsgTnl *)
    {
        RSVPTE_DBG3 (RSVPTE_PH_ETEXT, "Retry timer started for TnlId: %d"
                     " TnlInst: %d ErrorVal: %u\n",
                     pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.u2TnlId,
                     pNotifyMsgTnl->RsvpTeFilterSpecObj.RsvpTeFilterSpec.
                     u2LspId, pNotifyRecipient->u2ErrorValue);
        RptePvmFillSsnObj (&pObjHdr,
                           pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.
                           TnlDestAddr,
                           pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.u2TnlId,
                           pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.ExtnTnlId);

        if (pNotifyMsgTnl->u4MsgType == UPSTREAM)
        {
            RptePvmFillSndrTempObj (&pObjHdr, pNotifyMsgTnl->RsvpTeSndrTmpObj.
                                    RsvpTeSndrTmp.TnlSndrAddr,
                                    pNotifyMsgTnl->RsvpTeSndrTmpObj.
                                    RsvpTeSndrTmp.u2LspId);
        }
        else
        {
            RptePvmFillFilterSpecObj (&pObjHdr,
                                      pNotifyMsgTnl->RsvpTeFilterSpecObj.
                                      RsvpTeFilterSpec.TnlSndrAddr,
                                      pNotifyMsgTnl->RsvpTeFilterSpecObj.
                                      RsvpTeFilterSpec.u2LspId);
        }
    }

    /* Copy ErrorSpec */
    pPdu = (UINT1 *) pObjHdr;
    RptePvmFillErrorSpecObj (&pPdu, pPktMap);

    RptePbSendMsg (pPktMap);
    MemReleaseMemBlock (RSVPTE_PKT_MAP_POOL_ID, (UINT1 *) pPktMap);
    RpteNhStartRetryTmr (pNotifyRecipient);
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNhSendNotifyMsg : EXIT\n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhStartRetryTmr
 * Description     : This function starts the Retry Timer
 * Input (s)       : pNotifyRecipient - Pointer to NotifyRecipient Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RpteNhStartRetryTmr (tNotifyRecipient * pNotifyRecipient)
{
    tTimerParam        *pTmrParam = NULL;
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNotifyStartRetryTmr: ENTRY \n");
    pTmrParam = (tTimerParam *) & (pNotifyRecipient->NotifyTimerParam);
    TIMER_PARAM_ID (pTmrParam) = RPTE_NOTIFY_RETRY_TMR_EXPIRE;
    pTmrParam->u.pNotifyRecipient = pNotifyRecipient;
    pNotifyRecipient->NotifyTimer.u4Data = (FS_ULONG) pTmrParam;

    if (RpteUtlStartTimer (RSVP_GBL_TIMER_LIST,
                           &(pNotifyRecipient->NotifyTimer),
                           (pNotifyRecipient->u4RetryInterval *
                            SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) != TMR_SUCCESS)
    {
        /* Start Timer failed. */
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "Grouping Timer failed\n");
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNotifyStartGrRetryTmr: EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhProcessNotifyMsg
 * Description     : This function processes the notify message
 * Input (s)       : pPktMap - Pointer to packet map Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteNhProcessNotifyMsg (tPktMap * pPktMap)
{
    tErrorSpec         *pErrorSpec = NULL;
    tRecNotifyTnl      *pRecNotifyTnl = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;
    UINT4               u4TunnelIndex = RPTE_ZERO;
    UINT4               u4TunnelInstance = RPTE_ZERO;
    UINT4               u4TunnelIngress = RPTE_ZERO;
    UINT4               u4TunnelEgress = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNhNotifyProcessMsg : Entry \n");

    if (RpteNhValidateNtfyMsgObjs (pPktMap) != RPTE_SUCCESS)
    {
        RpteUtlCleanPktMap (pPktMap);    /* Clean pPktMap */
        RSVPTE_DBG (RSVPTE_PH_PRCS,
                    "NOTIFY : Validate notify Msg Objs failed - Pkt dropped..\n");
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNotifyProcessMsg : INTMD-EXIT \n");
        return;
    }

    pErrorSpec = &pPktMap->pErrorSpecObj->ErrorSpec;

    if ((OSIX_NTOHS (pErrorSpec->u2ErrValue) == RPTE_CONTROL_CHANNEL_DEGRADE) ||
        (OSIX_NTOHS (pErrorSpec->u2ErrValue) == RPTE_CONTROL_CHANNEL_ACTIVE))
    {
        return;
    }

    TMO_SLL_Scan (&pPktMap->NotifyMsgTnlList, pRecNotifyTnl, tRecNotifyTnl *)
    {
        u4TunnelIndex = pRecNotifyTnl->u4TunnelIndex;
        u4TunnelInstance = pRecNotifyTnl->u4TunnelInstance;
        u4TunnelIngress = pRecNotifyTnl->u4TunnelIngress;
        u4TunnelEgress = pRecNotifyTnl->u4TunnelEgress;

        if (RPTE_CHECK_NOTIFY_MSG_TNL
            (u4TunnelIndex, u4TunnelInstance, u4TunnelIngress, u4TunnelEgress,
             pRsvpTeTnlInfo) == RPTE_FAILURE)
        {
            /* If there is no entry present in Tnl Hash Table, drop the pkt */
            RSVPTE_DBG1 (RSVPTE_PT_PRCS,
                         "No entry present in Tnl Table with Tnl Id : %d\n",
                         OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID (pPktMap)));
            continue;
        }
        if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
             MPLS_TE_FULL_REROUTE) &&
            (OSIX_NTOHS (pErrorSpec->u2ErrValue) == RPTE_LSP_LOCALLY_FAILED))
        {
            RpteNhProcessFullyReRouteTnls (pRsvpTeTnlInfo,
                                           OSIX_NTOHL (pErrorSpec->
                                                       u4ErrNodeAddr));
        }
        else if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                  i4E2EProtectionType == MPLS_TE_DEDICATED_ONE2ONE)
                 &&
                 ((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                   LOCAL_PROT_IN_USE)
                  || (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                      LOCAL_PROT_AVAIL)))

        {
            if (((pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                  LOCAL_PROT_AVAIL) &&
                 (pRsvpTeTnlInfo->pTeTnlInfo->bIsOamEnabled != TRUE))
                || (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
                    LOCAL_PROT_IN_USE))
            {
                RpteNhProcessOneToOneTnls (pRsvpTeTnlInfo,
                                           PKT_MAP_SRC_ADDR (pPktMap),
                                           OSIX_NTOHS (pErrorSpec->u2ErrValue));
            }
            else
            {
                RSVPTE_DBG3 (RSVPTE_PT_PRCS,
                             "RpteNhProcessNotifyMsg for Tnl: %u %u "
                             "bIsOamEnabled: %d\n",
                             pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                             pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance,
                             pRsvpTeTnlInfo->pTeTnlInfo->bIsOamEnabled);
            }
        }

        else if (OSIX_NTOHS (pErrorSpec->u2ErrValue) != RPTE_LSP_RECOVERED)
        {
            if ((pRsvpTeTnlInfo->pMapTnlInfo != NULL) &&
                (pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo != NULL) &&
                (pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
                 u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
            {
                pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
                    u1TnlLocalProtectInUse = LOCAL_PROT_NOT_AVAIL;
                RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo =
                    pRsvpTeTnlInfo->pMapTnlInfo;
                RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
            }
            RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pRsvpTeTnlInfo;
            RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
            continue;
        }
    }

    RpteMIHSendAckMsg (NULL, pPktMap);
    RpteUtlCleanPktMap (pPktMap);
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhProcessRetryTimeOut
 * Description     : This function handles the retry time out
 * Input (s)       : pNotifyRecipient - Pointer to NotifyRecipient Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteNhProcessRetryTimeOut (tNotifyRecipient * pNotifyRecipient)
{
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tNotifyMsgTnl      *pNotifyMsgTnl = NULL;
    UINT4               u4RetryInterval = RPTE_ZERO;
    UINT4               u4TunnelIndex = RPTE_ZERO;
    UINT4               u4TunnelInstance = RPTE_ZERO;
    UINT4               u4TunnelIngress = RPTE_ZERO;
    UINT4               u4TunnelEgress = RPTE_ZERO;
    u4RetryInterval = pNotifyRecipient->u4RetryInterval;

    pNotifyRecipient->u4RetryLimit--;
    if (pNotifyRecipient->u4RetryLimit == RPTE_ZERO)
    {
        TMO_SLL_Scan (&pNotifyRecipient->NotifyMsgTnlList, pNotifyMsgTnl,
                      tNotifyMsgTnl *)
        {
            u4TunnelIndex = pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.u2TnlId;
            CONVERT_TO_INTEGER (pNotifyMsgTnl->RsvpTeSsnObj.RsvpTeSession.
                                TnlDestAddr, u4TunnelEgress);

            if (pNotifyMsgTnl->u4MsgType == UPSTREAM)
            {
                u4TunnelInstance =
                    pNotifyMsgTnl->RsvpTeSndrTmpObj.RsvpTeSndrTmp.u2LspId;
                CONVERT_TO_INTEGER (pNotifyMsgTnl->RsvpTeSndrTmpObj.
                                    RsvpTeSndrTmp.TnlSndrAddr, u4TunnelIngress);
            }
            else
            {
                u4TunnelInstance =
                    pNotifyMsgTnl->RsvpTeFilterSpecObj.RsvpTeFilterSpec.u2LspId;
                CONVERT_TO_INTEGER (pNotifyMsgTnl->RsvpTeFilterSpecObj.
                                    RsvpTeFilterSpec.TnlSndrAddr,
                                    u4TunnelIngress);
            }
            u4TunnelIngress = OSIX_NTOHL (u4TunnelIngress);
            u4TunnelEgress = OSIX_NTOHL (u4TunnelEgress);
            if (RPTE_CHECK_NOTIFY_MSG_TNL (u4TunnelIndex, u4TunnelInstance,
                                           u4TunnelIngress, u4TunnelEgress,
                                           pRsvpTeTnlInfo) == RPTE_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "RpteNhAddTnlToNotifyRecipient : exit3\n");
                return;
            }
            /* If Node does not receive ACK for the notification, and tunnel's protection
             *              * status is not equal to "In use", then delete the tunnel.
             *                           * This situation will happen @ intermediate node when the ingress is killed
             *                                        * */

            if ((pRsvpTeTnlInfo != NULL) && (pRsvpTeTnlInfo->pTeTnlInfo != NULL)
                &&
                (((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                   i4E2EProtectionType == MPLS_TE_DEDICATED_ONE2ONE)
                  && (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse !=
                      LOCAL_PROT_IN_USE))
                 || (pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.
                     i4E2EProtectionType == MPLS_TE_FULL_REROUTE)))

            {
                RpteEnqueueMsgInfo.u.pRsvpTeTnlInfo = pRsvpTeTnlInfo;
                RpteProcessLspDestroyEvent (&RpteEnqueueMsgInfo);
            }
        }
        RpteNhDelRBNode (pNotifyRecipient);
        return;
    }

    pNotifyRecipient->u4RetryInterval =
        RpteNhCalculateRetransmitIntvl (u4RetryInterval);
    RpteNhSendNotifyMsg (pNotifyRecipient);
    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RpteNhProcessRetryTimeOut: EXIT \n");

    return;
}

/****************************************************************************/
/* Function Name   : RpteNhCalculateRetransmitIntvl
 * Description     : This function calculates the retransmit interval of a
 *                   Notify Message based on the decay value configured by the
 *                   user.
 * Input (s)       : u4NotifyMsgRetryIntvl - Original Value of Notify Message
 *                   Retransmit Interval.
 * Output (s)      : None
 * Returns         : u4NotifyMsgRetryIntvl - New Value of Notify Message
 *                   Retransmit Interval.
 */
/****************************************************************************/
UINT4
RpteNhCalculateRetransmitIntvl (UINT4 u4NotifyMsgRetryIntvl)
{
    UINT4               u4RetransmitDecay = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                "RpteCalculateNotifyMsgRetransmitIntvl: ENTRY \n");

    u4RetransmitDecay = gpRsvpTeGblInfo->RsvpTeCfgParams.u4NotifRetransmitDecay;

    if (u4RetransmitDecay != RSVPTE_ZERO)
    {
        u4NotifyMsgRetryIntvl +=
            ((u4RetransmitDecay * u4NotifyMsgRetryIntvl) / 100);
    }

    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                "RpteCalculateNotifyMsgRetransmitIntvl: EXIT \n");
    return u4NotifyMsgRetryIntvl;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhGetAndDelNotifyRecipient
 * Description     : This function gets the Recipient node to be deleted
 * Input (s)       : u4NotifyAddr - Notify address
 *                   u4MsgId      - Message id  
 * Output (s)      : None
 * Returns         : RPTE_FAILURE or RPTE_SUCCESS
 */
/*---------------------------------------------------------------------------*/

INT4
RpteNhGetAndDelNotifyRecipient (UINT4 u4NotifyAddr, UINT4 u4MsgId)
{
    tNotifyRecipient   *pNotifyRecipient = NULL;
    tNotifyRecipient   *pInNotifyRecipient = NULL;

    pInNotifyRecipient =
        (tNotifyRecipient *) MemAllocMemBlk (RSVPTE_NOTIFY_RECIPIENT_POOL_ID);

    if (pInNotifyRecipient == NULL)
    {
        return RPTE_FAILURE;
    }

    pInNotifyRecipient->u4RecipientAddr = u4NotifyAddr;

    pNotifyRecipient = RBTreeGetFirst (gpRsvpTeGblInfo->NotifyRecipientTree);
    while (pNotifyRecipient != NULL)
    {
        if (u4NotifyAddr != pNotifyRecipient->u4RecipientAddr)
        {
            return RPTE_FAILURE;
        }
        if (pNotifyRecipient->u4MsgId == u4MsgId)
        {
            break;
        }
        pNotifyRecipient = RBTreeGetNext (gpRsvpTeGblInfo->NotifyRecipientTree,
                                          (tRBElem *) pNotifyRecipient, NULL);
    }

    MemReleaseMemBlock (RSVPTE_NOTIFY_RECIPIENT_POOL_ID,
                        (UINT1 *) pInNotifyRecipient);

    if (pNotifyRecipient == NULL)
    {
        return RPTE_FAILURE;
    }

    TmrStopTimer (RSVP_GBL_TIMER_LIST, &(pNotifyRecipient->NotifyTimer));

    RpteNhDelRBNode (pNotifyRecipient);

    return RPTE_SUCCESS;

}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhDelRBNode
 * Description     : This function deltes the Recipient node
 * Input (s)       : pNotifyRecipient - Pointer to NotifyRecipient Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteNhDelRBNode (tNotifyRecipient * pNotifyRecipient)
{
    tNotifyMsgTnl      *pNotifyMsgTnl = NULL;
    tNotifyMsgTnl      *pTempNotifyMsgTnl = NULL;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNhDelRBNode: ENTRY \n");
    RSVPTE_DBG1 (RSVPTE_PH_ETEXT,
                 "Node with Msg Id : %u is going to delete\n",
                 pNotifyRecipient->u4MsgId);

    TMO_DYN_SLL_Scan (&pNotifyRecipient->NotifyMsgTnlList, pNotifyMsgTnl,
                      pTempNotifyMsgTnl, tNotifyMsgTnl *)
    {
        TMO_SLL_Delete (&pNotifyRecipient->NotifyMsgTnlList,
                        &pNotifyMsgTnl->NextTnl);
        MemReleaseMemBlock (RSVPTE_NOTIFY_TNL_POOL_ID,
                            (UINT1 *) (pNotifyMsgTnl));
    }

    TmrStopTimer (RSVP_GBL_TIMER_LIST, &pNotifyRecipient->NotifyTimer);

    RBTreeRem (gpRsvpTeGblInfo->NotifyRecipientTree, pNotifyRecipient);
    MemReleaseMemBlock (RSVPTE_NOTIFY_RECIPIENT_POOL_ID,
                        (UINT1 *) pNotifyRecipient);
    pNotifyRecipient = NULL;

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNhDelRBNode: EXIT \n");

    return;

}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhProcessFullyReRouteTnls
 * Description     : This function does the required action items for fully rerouted
 *                   tunnels .
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 *                   u4ErrNodeAddr  - Error Node address
 * Output (s)      : None
 * Returns         : If successful returns RPTE_SUCCESS or else RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/

INT4
RpteNhProcessFullyReRouteTnls (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                               UINT4 u4ErrNodeAddr)
{
    if (pRsvpTeTnlInfo->u1NotifyMsgProtState == RSVP_PROT_STATE_SO_RESP_SENT)
    {
        return RPTE_FAILURE;
    }

    /* This will be called when
     * 1. Notify message received from intermediate node with error - LSP_Locally_
     * Failed
     * 2. Max_Wait_time_out expiry at ingress and egress
     * */

    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        RpteUtlFillExcludeHopList (pRsvpTeTnlInfo, u4ErrNodeAddr);
        rpteTeSetTnlOperStatus (pRsvpTeTnlInfo->pTeTnlInfo, TE_OPER_DOWN);
    }
    else
    {
        RpteNhAddTnlToNotifyRecipient (pRsvpTeTnlInfo,
                                       OSIX_NTOHL (RSVPTE_TNL_INGRESS
                                                   (pRsvpTeTnlInfo)),
                                       UPSTREAM, RPTE_FALSE,
                                       RPTE_NOTIFY, RPTE_LSP_LOCALLY_FAILED);
    }

    pRsvpTeTnlInfo->u1NotifyMsgProtState = RSVP_PROT_STATE_SO_RESP_SENT;
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhProcessOneToOneTnls
 * Description     : This function does the required action items for 1:1 protected
 *                   tunnels .
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 *                   u1ErrorCode    - Error code value   
 *                   u4SrcAddr      - Source addr of Notification message
 * Output (s)      : None
 * Returns         : If successful returns RPTE_SUCCESS or else RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/

VOID
RpteNhProcessOneToOneTnls (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT4 u4SrcAddr,
                           UINT2 u2ErrValue)
{
    UINT4               u4NotifyAddr = RPTE_ZERO;
    UINT4               u4MsgDirection = RPTE_ZERO;
    UINT1               u1SwitchType = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PVM_ETEXT, "RpteNhProcessOneToOneTnls : Entry \n");
    RSVPTE_DBG2 (RSVPTE_PVM_PRCS, "u2ErrValue: %u, u1NotifyMsgProtState: %u\n",
                 u2ErrValue, pRsvpTeTnlInfo->u1NotifyMsgProtState);
    RSVPTE_DBG2 (RSVPTE_PVM_PRCS, "Tunnel: %u %u\n",
                 pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                 pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);
    if (((u2ErrValue == RPTE_LSP_LOCALLY_FAILED) ||
         (u2ErrValue == RPTE_LSP_FAILURE)) &&
        ((pRsvpTeTnlInfo->u1NotifyMsgProtState == RSVP_PROT_STATE_SO_RESP_SENT)
         || (pRsvpTeTnlInfo->u1NotifyMsgProtState ==
             RSVP_PROT_STATE_SO_RESP_RCVD)))
    {
        RSVPTE_DBG (RSVPTE_PVM_PRCS, "RpteNhAddTnlToNotifyRecipient : exit5\n");
        return;
    }
    else if ((u2ErrValue == RPTE_LSP_FAILURE) &&
             (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
              LOCAL_PROT_IN_USE))
    {
        RSVPTE_DBG (RSVPTE_PVM_PRCS, "RpteNhAddTnlToNotifyRecipient : exit6\n");
        return;
    }
    else if ((u2ErrValue == RPTE_LSP_RECOVERED) &&
             ((pRsvpTeTnlInfo->u1NotifyMsgProtState ==
               RSVP_PROT_STATE_SB_RESP_SENT)
              || (pRsvpTeTnlInfo->u1NotifyMsgProtState ==
                  RSVP_PROT_STATE_SB_RESP_RCVD)))
    {
        return;
    }

    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        u4NotifyAddr = OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo));
        u4MsgDirection = DOWNSTREAM;
    }
    else if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS)
    {
        u4NotifyAddr = OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo));
        u4MsgDirection = UPSTREAM;
    }
    switch (u2ErrValue)
    {
        case RPTE_LSP_LOCALLY_FAILED:
        {
            /* This case will be called when 
             * 1. Notify message received from intermediate node with error - LSP_Locally_
             * Failed.
             * 2. Max_Wait_time_out expiry at ingress and egress
             * 3. Path message or Resv message with "A" bit in admin status object */

            /*Send Notify-switch-over Request */
            RpteNhAddTnlToNotifyRecipient (pRsvpTeTnlInfo, u4NotifyAddr,
                                           u4MsgDirection, RPTE_FALSE,
                                           RPTE_NOTIFY, RPTE_LSP_FAILURE);
            pRsvpTeTnlInfo->u1NotifyMsgProtState = RSVP_PROT_STATE_SO_REQ_SENT;
            RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                         "setting1 u1NotifyMsgProtState as %u\n",
                         pRsvpTeTnlInfo->u1NotifyMsgProtState);
            break;
        }

        case RPTE_LSP_FAILURE:
        {
            /* This case will be called for switch over request/response from
             * ingress or egress */

            if (pRsvpTeTnlInfo->u1NotifyMsgProtState ==
                RSVP_PROT_STATE_SO_REQ_SENT)
            {
                pRsvpTeTnlInfo->u1NotifyMsgProtState =
                    RSVP_PROT_STATE_SO_RESP_RCVD;
                RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                             "setting2 u1NotifyMsgProtState as %u\n",
                             pRsvpTeTnlInfo->u1NotifyMsgProtState);
            }
            else
            {
                pRsvpTeTnlInfo->u1NotifyMsgProtState =
                    RSVP_PROT_STATE_SO_REQ_RCVD;
                RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                             "setting3 u1NotifyMsgProtState as %u\n",
                             pRsvpTeTnlInfo->u1NotifyMsgProtState);
            }
            if (pRsvpTeTnlInfo->u1NotifyMsgProtState ==
                RSVP_PROT_STATE_SO_REQ_RCVD)
            {
                /*Send Notify-switch-over Response */
                RpteNhAddTnlToNotifyRecipient (pRsvpTeTnlInfo, u4NotifyAddr,
                                               DOWNSTREAM, RPTE_FALSE,
                                               RPTE_NOTIFY, RPTE_LSP_FAILURE);
                pRsvpTeTnlInfo->u1NotifyMsgProtState =
                    RSVP_PROT_STATE_SO_RESP_SENT;
                RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                             "setting4 u1NotifyMsgProtState as %u\n",
                             pRsvpTeTnlInfo->u1NotifyMsgProtState);
            }
            break;
        }

            /* This case will be called for 
             * (i) switch back request/response from ingress or egress 
             * (ii) Notify message received from intermediate node with error - LSP-RECOVERED
             * (iii) ath message or Resv message with "A" bit clear in admin status object
             * */

        case RPTE_LSP_RECOVERED:
        {
            if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
            {
                if (OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)) ==
                    u4SrcAddr)
                {
                    if ((pRsvpTeTnlInfo->u1NotifyMsgProtState ==
                         RSVP_PROT_STATE_SB_REQ_SENT) &&
                        (pRsvpTeTnlInfo->b1IsTimerStarted == RPTE_FALSE))

                    {
                        pRsvpTeTnlInfo->u1NotifyMsgProtState =
                            RSVP_PROT_STATE_SB_RESP_RCVD;
                        break;
                    }
                    else
                    {
                        pRsvpTeTnlInfo->u1NotifyMsgProtState =
                            RSVP_PROT_STATE_SB_RESP_SENT;

                    }
                }
                else
                {
                    pRsvpTeTnlInfo->u1NotifyMsgProtState =
                        RSVP_PROT_STATE_SB_REQ_SENT;
                }
            }

            else if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS)
            {
                if (OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)) ==
                    u4SrcAddr)
                {
                    if ((pRsvpTeTnlInfo->u1NotifyMsgProtState ==
                         RSVP_PROT_STATE_SB_REQ_SENT) &&
                        (pRsvpTeTnlInfo->b1IsTimerStarted == RPTE_FALSE))
                    {
                        pRsvpTeTnlInfo->u1NotifyMsgProtState =
                            RSVP_PROT_STATE_SB_RESP_RCVD;
                        break;
                    }
                    else
                    {
                        pRsvpTeTnlInfo->u1NotifyMsgProtState =
                            RSVP_PROT_STATE_SB_RESP_SENT;
                    }
                }
                else
                {
                    pRsvpTeTnlInfo->u1NotifyMsgProtState =
                        RSVP_PROT_STATE_SB_REQ_SENT;
                }
            }

            RSVPTE_DBG1 (RSVPTE_PVM_PRCS, "SB u1NotifyMsgProtState as %u\n",
                         pRsvpTeTnlInfo->u1NotifyMsgProtState);
            /*Send Notify-switch-back Response */
            RpteNhAddTnlToNotifyRecipient (pRsvpTeTnlInfo, u4NotifyAddr,
                                           DOWNSTREAM, RPTE_FALSE,
                                           RPTE_NOTIFY, RPTE_LSP_RECOVERED);
            break;
        default:
            break;
        }
    }
    if ((pRsvpTeTnlInfo->u1NotifyMsgProtState == RSVP_PROT_STATE_SO_RESP_SENT)
        || (pRsvpTeTnlInfo->u1NotifyMsgProtState ==
            RSVP_PROT_STATE_SO_RESP_RCVD))
    {
        u1SwitchType = RPTE_PROTECTION_SWITCH_OVER;
        RpteUtlSwitchTraffic (pRsvpTeTnlInfo, u1SwitchType);
    }
    else if ((pRsvpTeTnlInfo->u1NotifyMsgProtState ==
              RSVP_PROT_STATE_SB_RESP_SENT)
             || (pRsvpTeTnlInfo->u1NotifyMsgProtState ==
                 RSVP_PROT_STATE_SB_RESP_RCVD))

    {
        u1SwitchType = RPTE_PROTECTION_SWITCH_BACK;
        RpteUtlSwitchTraffic (pRsvpTeTnlInfo, u1SwitchType);
        pRsvpTeTnlInfo->b1IsTimerStarted = RPTE_FALSE;
    }
    RSVPTE_DBG1 (RSVPTE_PVM_PRCS, "%s :EXIT \n", __func__);
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhValidateNtfyMsgObjs
 * Description     : This function validates Notify Message objects.
 * Input (s)       : pPktMap - Pointer to PktMap
 * Output (s)      : None
 * Returns         : If successful returns RPTE_SUCCESS or else RPTE_FAILURE
 */
/*---------------------------------------------------------------------------*/
UINT1
RpteNhValidateNtfyMsgObjs (tPktMap * pPktMap)
{
    tRsvpHdr           *pRsvpHdr = NULL;
    tObjHdr            *pObjHdr = NULL;
    tRecNotifyTnl      *pRecNotifyTnl = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    UINT1              *pEndOfPkt = NULL;
    UINT4               u4TunnelIndex = RPTE_ZERO;
    UINT4               u4TunnelInstance = RPTE_ZERO;
    UINT4               u4TunnelIngress = RPTE_ZERO;
    UINT4               u4TunnelEgress = RPTE_ZERO;
    UINT4               u4Epoch = RPTE_ZERO;
    UINT1              *pu1Pdu = NULL;

    pRsvpHdr = (tRsvpHdr *) (VOID *) PKT_MAP_RSVP_PKT (pPktMap);
    pEndOfPkt = (UINT1 *) pRsvpHdr + OSIX_NTOHS (RSVP_HDR_LENGTH (pRsvpHdr));
    PKT_MAP_UNKNOWN_OBJ_HDR (pPktMap) = NULL;
    pObjHdr = (tObjHdr *) (VOID *) (((UINT1 *) pRsvpHdr) + sizeof (tRsvpHdr));

    while (pObjHdr < (tObjHdr *) (VOID *) pEndOfPkt)
    {
        if (VALIDATE_OBJHDR (pObjHdr, pEndOfPkt))
        {
            RSVPTE_DBG (RSVPTE_PVM_PRCS, "Validate obj hdr failed \n");
            RpteUtlCleanPktMap (pPktMap);
            return RPTE_FAILURE;
        }
        switch (OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)))
        {
            case IPV4_RPTE_SSN_CLASS_NUM_TYPE:
                /* Check for valid Ssn Obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    RPTE_IPV4_SSN_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS, "Err : Invalid Ssn Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pRsvpTeSsnObj = (tRsvpTeSsnObj *) pObjHdr;
                u4TunnelIndex = OSIX_NTOHS (pPktMap->pRsvpTeSsnObj->
                                            RsvpTeSession.u2TnlId);
                u4TunnelIngress = OSIX_NTOHL (RpteUtilConvertToInteger
                                              (pPktMap->pRsvpTeSsnObj->
                                               RsvpTeSession.ExtnTnlId));
                u4TunnelEgress =
                    OSIX_NTOHL (RpteUtilConvertToInteger
                                (pPktMap->pRsvpTeSsnObj->RsvpTeSession.
                                 TnlDestAddr));
                break;

            case IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE:

                /* Check for valid Sndr Tmp Obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    RPTE_IPV4_SNDR_TMP_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Sndr Tmp Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pRsvpTeSndrTmpObj = (tRsvpTeSndrTmpObj *) pObjHdr;
                u4TunnelInstance = OSIX_NTOHS (pPktMap->pRsvpTeSndrTmpObj->
                                               RsvpTeSndrTmp.u2LspId);
                break;

            case IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE:

                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr))
                    != RPTE_IPV4_FLTR_SPEC_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Filter Spec Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                pPktMap->pRsvpTeFilterSpecObj =
                    (tRsvpTeFilterSpecObj *) pObjHdr;
                u4TunnelInstance =
                    OSIX_NTOHS (pPktMap->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.
                                u2LspId);
                break;

            case IPV4_ERROR_SPEC_CLASS_NUM_TYPE:
                /* ERROR_SPEC Object */
                /* Check for Valid Err Spec obj, if not present return not ok */
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    RPTE_IPV4_ERR_SPEC_OBJ_LEN)
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid Err Spec Obj \n");
                    RSVPTE_DBG (RSVPTE_PVM_ETEXT,
                                "RptePvmValidateObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_ERROR_SPEC_OBJ (pPktMap) =
                    (tErrorSpecObj *) (VOID *) pObjHdr;
                break;

            case RPTE_MESSAGE_ID_CLASS_NUM_TYPE:
                pu1Pdu = (UINT1 *) ((UINT1 *) pObjHdr + sizeof (tObjHdr));

                /* Set the Msg-Id Present flag */
                RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) = RPTE_NONZERO;

                RPTE_EXT_4_BYTES (pu1Pdu, u4Epoch);
                RPTE_PKT_MAP_MSG_ID_FLAGS (pPktMap)
                    = (UINT1) ((u4Epoch & RPTE_WORD_UPPER_1_BYTE_MASK)
                               >> RPTE_3_BYTE_SHIFT);
                RPTE_PKT_MAP_MSG_ID_EPOCH (pPktMap)
                    = (u4Epoch & RPTE_WORD_LOWER_3_BYTE_MASK);
                RPTE_EXT_4_BYTES (pu1Pdu, RPTE_PKT_MAP_MSG_ID (pPktMap));

                break;

            default:
                /*
                 *  The Further processing depends on the Class Num and
                 *  Class Type values
                 */
                PKT_MAP_UNKNOWN_OBJ_HDR (pPktMap) = pObjHdr;
                RSVPTE_DBG1 (RSVPTE_PVM_PRCS,
                             "\nRcvd Unknown Obj of Class Num Type 0x%x\n",
                             OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)));
                break;
        }

        if ((OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)) !=
             IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE) &&
            (OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)) !=
             IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE))
        {
            pObjHdr = NEXT_OBJ (pObjHdr);
            continue;
        }
        if (RPTE_CHECK_NOTIFY_MSG_TNL
            (u4TunnelIndex, u4TunnelInstance, u4TunnelIngress, u4TunnelEgress,
             pRsvpTeTnlInfo) == RPTE_FAILURE)
        {
            pObjHdr = NEXT_OBJ (pObjHdr);
            continue;
        }
        pRecNotifyTnl = (tRecNotifyTnl *)
            RSVP_ALLOCATE_MEM_BLOCK (RSVPTE_REC_NTFY_TNL_POOL_ID);
        if (pRecNotifyTnl == NULL)
        {
            pObjHdr = NEXT_OBJ (pObjHdr);
            continue;
        }

        TMO_SLL_Init_Node (&(pRecNotifyTnl->NextRecNtfyTnl));

        pRecNotifyTnl->u4TunnelIndex = u4TunnelIndex;
        pRecNotifyTnl->u4TunnelInstance = u4TunnelInstance;
        pRecNotifyTnl->u4TunnelIngress = u4TunnelIngress;
        pRecNotifyTnl->u4TunnelEgress = u4TunnelEgress;

        TMO_SLL_Add (&pPktMap->NotifyMsgTnlList,
                     &(pRecNotifyTnl->NextRecNtfyTnl));
        pObjHdr = NEXT_OBJ (pObjHdr);

    }
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RpteNhHandlePsbOrRsbOrHelloTimeOut
 * Description     : This function handles the Psb or Rsb or Hello time out 
 *                   scenorios for one to one protetion tunnel .
 * Input (s)       : pRsvpTeTnlInfo - Pointer to RSVP-TE tnl info
 *                   u2ErrValue     - Error  value
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/

VOID
RpteNhHandlePsbOrRsbOrHelloTimeOut (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                    UINT2 u2ErrValue)
{
    UINT4               u4NotifyAddr = RPTE_ZERO;

    if (((u2ErrValue == RPTE_LSP_LOCALLY_FAILED) ||
         (u2ErrValue == RPTE_LSP_FAILURE)) &&
        ((pRsvpTeTnlInfo->u1NotifyMsgProtState == RSVP_PROT_STATE_SO_RESP_SENT)
         || (pRsvpTeTnlInfo->u1NotifyMsgProtState ==
             RSVP_PROT_STATE_SO_RESP_RCVD)))
    {
        RSVPTE_DBG (RSVPTE_PH_ETEXT,
                    "In HelloTimeOut returning when RPTE_LSP_LOCALLY_FAILED or RPTE_LSP_FAILURE \n");
        return;
    }
    else if ((u2ErrValue == RPTE_LSP_RECOVERED) &&
             ((pRsvpTeTnlInfo->u1NotifyMsgProtState ==
               RSVP_PROT_STATE_SB_RESP_SENT)
              || (pRsvpTeTnlInfo->u1NotifyMsgProtState ==
                  RSVP_PROT_STATE_SB_RESP_RCVD)))
    {
        RSVPTE_DBG2 (RSVPTE_PVM_PRCS,
                     "RpteNhProcessOneToOneTnls:INT_EXIT2 for tunnel : %d inst: %d \n",
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlIndex,
                     pRsvpTeTnlInfo->pTeTnlInfo->u4TnlInstance);
        return;
    }

    if (pRsvpTeTnlInfo->pPsb != NULL)
    {
        u4NotifyAddr = pRsvpTeTnlInfo->pPsb->u4NotifyAddr;
    }

    if (u4NotifyAddr != RPTE_ZERO)
    {
        RpteNhAddTnlToNotifyRecipient (pRsvpTeTnlInfo, u4NotifyAddr, DOWNSTREAM,
                                       RPTE_TRUE, RPTE_NOTIFY, u2ErrValue);
    }

    u4NotifyAddr = RPTE_ZERO;
    if (pRsvpTeTnlInfo->pRsb != NULL)
    {
        u4NotifyAddr = pRsvpTeTnlInfo->pRsb->u4NotifyAddr;
    }

    if (u4NotifyAddr != RPTE_ZERO)
    {
        RpteNhAddTnlToNotifyRecipient (pRsvpTeTnlInfo, u4NotifyAddr, UPSTREAM,
                                       RPTE_TRUE, RPTE_NOTIFY, u2ErrValue);
    }

    return;
}

/*---------------------------------------------------------------------------*/
/*
+ * Function Name   : RpteNhStopGroupingTmr
+ * Description     : This function stops the Grouping Timer
+ * Input (s)       : pNotifyRecipient - Pointer to NotifyRecipient Structure.
+ * Output (s)      : None
+ * Returns         : None
+ */
/*---------------------------------------------------------------------------*/
VOID
RpteNhStopGroupingTmr (tNotifyRecipient * pNotifyRecipient)
{
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNotifyStopGroupingTmr: ENTRY \n");

    if (RpteUtlStopTimer (RSVP_GBL_TIMER_LIST,
                          &(pNotifyRecipient->NotifyTimer)) != TMR_SUCCESS)
    {
        /* Stop Timer failed. */
        RSVPTE_DBG (RSVPTE_PH_ETEXT, "Grouping Timer failed\n");
    }
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteNotifyStopGroupingTmr: EXIT \n");
    return;
}
