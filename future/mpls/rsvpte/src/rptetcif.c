
/********************************************************************
 *
 * $RCSfile: rptetcif.c,v $
 *
 * $Date: 2007/11/27 09:20:12 $
 *
 * $Revision: 1.4 $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptetcif.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : RSVP-TE 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for the Traffic 
 *                             Controller Interface Module.
 *                                             
 *---------------------------------------------------------------------------*/

#include "rpteincs.h"

/****************************************************************************/
/* Function Name   : RpteRegisterWithTc                                     */
/* Description     : This function interfaces with TC for Registeration of  */
/*                   RSVP-TE with TC                                        */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS in case the Registration is a success   */
/*                   RPTE_FAILURE in case the Registration is a failure   */
/****************************************************************************/
UINT1
RpteRegisterWithTc (VOID)
{
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteRegisterWithTc : ENTRY\n");
    if (RPTE_TC_REG_RSVP_PRTCL (TC_RSVPTE_APPID, NULL) == RPTE_TC_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_TCIF_PRCS, "Registration with TC Failed.\n");
        RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteRegisterWithTc : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteRegisterWithTc : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteDeRegisterWithTc                                   */
/* Description     : This function interfaces with TC for DeRegisteration of*/
/*                   RSVP-TE with TC                                        */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteDeRegisterWithTc (VOID)
{
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteDeRegisterWithTc : ENTRY\n");

    RPTE_TC_DEREG_RSVP_PRTCL (TC_RSVPTE_APPID);

    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteDeRegisterWithTc : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteTcFreeResources                                    */
/* Description     : This function interfaces with TC to free the reserved  */
/*                   resources                                              */
/* Input (s)       : u4Rhandle -  holds Rhandle                             */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteTcFreeResources (UINT4 u4Rhandle)
{
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcFreeResources : ENTRY\n");

    RPTE_TC_FREE_RESOURCES (u4Rhandle);

    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcFreeResources : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteTcResvResources                                    */
/* Description     : This function interfaces with TC for reserving         */
/*                   Resources                                              */
/* Input (s)       : pIfEntry      - pointer to IfEntry                     */
/*                   pFlowSpec     - pointer to FlowSpec                    */
/*                   u1ResPoolType - holds ResPoolType Value                */
/*                   pRHandle      - pointer to hold ResvHandle             */
/*                   pu1ErrCode    - pointer to hold ErrorCode              */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS in case the Resource reservation is a     */
/*                   success                                                */
/*                   RPTE_FAILURE in case the Resource reservation is a     */
/*                   failure                                                */
/****************************************************************************/
UINT1
RpteTcResvResources (const tIfEntry * pIfEntry, tFlowSpec * pFlowSpec,
                     UINT1 u1ResPoolType, UINT4 *pRhandle, UINT1 *pu1ErrCode)
{
    tuTcResSpec         TcResSpec;

    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcResvResources : ENTRY\n");

    MEMSET (&TcResSpec, RSVPTE_ZERO, sizeof (tuTcResSpec));
    MEMCPY (&TcResSpec.RsvpTrfcParams, pFlowSpec, sizeof (tFlowSpec));

    if (RPTE_TC_RESV_RESOURCES (TC_RSVPTE_APPID,
                                IF_ENTRY_IF_INDEX (pIfEntry),
                                RPTE_TC_RES_SPEC_TYPE,
                                &TcResSpec,
                                u1ResPoolType,
                                pRhandle, pu1ErrCode) == RPTE_TC_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_TCIF_PRCS, "Resources Not Available\n");
        RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcResvResources : INTMD-EXIT \n");
        if (*pu1ErrCode == RPTE_TC_ERR_BW_NA)
        {
            *pu1ErrCode = ADMISSION_CONTROL_FAILURE;
        }
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcResvResources : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteTcAssociateFilterInfo                              */
/* Description     : This function interfaces with TC for Associating       */
/*                   FilterInfo                                             */
/* Input (s)       : u4Rhandle         - holds Rhandle                      */
/*                   pRsvpTeTnlInfo    - pointer to RsvpTeTnlInfo           */
/*                   pFlowSpec         - pointer to FlowSpec                */
/*                   pu1ErrCode        - pointer to hold ErrorCode          */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS in case the Association is a success      */
/*                   RPTE_FAILURE in case the Association is a failure      */
/****************************************************************************/
UINT1
RpteTcAssociateFilterInfo (UINT4 u4Rhandle, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                           tFlowSpec * pFlowSpec, UINT1 *pu1ErrCode)
{
    tuTcResSpec         TcResSpec;
    tTcFilterInfo       TcFilterInfo;

    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcAssociateFilterInfo : ENTRY\n");

    MEMSET (&TcResSpec, RSVPTE_ZERO, sizeof (tuTcResSpec));
    MEMSET (&TcFilterInfo, RSVPTE_ZERO, sizeof (tTcFilterInfo));

    MEMCPY (&TcResSpec.RsvpTrfcParams, pFlowSpec, sizeof (tFlowSpec));
    TcFilterInfo.u4SrcAddr = RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo);
    TcFilterInfo.u4DestAddr = RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo);
    TcFilterInfo.u4TnlId = RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);
    TcFilterInfo.u4TnlInstance = RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);

    if (RPTE_TC_ASSOC_FILTER_INFO (u4Rhandle,
                                   &TcFilterInfo,
                                   RPTE_TC_RES_SPEC_TYPE,
                                   &TcResSpec, pu1ErrCode) == RPTE_TC_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_TCIF_PRCS, "Association Of FilterInfo Failed \n");
        RSVPTE_DBG (RSVPTE_TCIF_ETEXT,
                    " RpteTcAssociateFilterInfo : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcAssociateFilterInfo : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteTcDissociateFilterInfo                             */
/* Description     : This function interfaces with TC for Dissociating      */
/*                   FilterInfo                                             */
/* Input (s)       : u4RHandle         - holds Rhandle                      */
/*                   pRsvpTeTnlInfo    - pointer to RsvpTeTnlInfo           */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteTcDissociateFilterInfo (UINT4 u4RHandle, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tTcFilterInfo       TcFilterInfo;

    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcDissociateFilterInfo : ENTRY\n");

    MEMSET (&TcFilterInfo, RSVPTE_ZERO, sizeof (tTcFilterInfo));
    TcFilterInfo.u4SrcAddr = RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo);
    TcFilterInfo.u4DestAddr = RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo);
    TcFilterInfo.u4TnlId = RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo);
    TcFilterInfo.u4TnlInstance = RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);

    RPTE_TC_DISASSOC_FILTER_INFO (u4RHandle, &TcFilterInfo);

    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcDissociateFilterInfo : EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteTcGenerateAdSpec                                   */
/* Description     : This function interfaces with TC for generating AdSpec */
/* Input (s)       : pIfEntry      - pointer to IfEntry                     */
/*                   pAdSpec       - pointer to AdSpec                      */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS in case of success else RPTE_FAILURE      */
/****************************************************************************/
UINT1
RpteTcGenerateAdSpec (const tIfEntry * pIfEntry, tAdSpec * pAdSpec)
{
    UINT1               u1ErrCode = RSVPTE_ZERO;
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcGenerateAdSpec: ENTRY\n");
    if (RPTE_TC_GENERATE_ADSPEC (TC_RSVPTE_APPID,
                                 IF_ENTRY_IF_INDEX (pIfEntry),
                                 RPTE_TC_INTSERV_TYPE,
                                 pAdSpec, &u1ErrCode) == RPTE_TC_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_TCIF_PRCS, "Generation Of AdSpec Failed\n");
        RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcGenerateAdSpec : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcGenerateAdSpec : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteTcCalcAdSpec                                       */
/* Description     : This function interfaces with TC for calculating the   */
/*                   AdSpec                                                 */
/* Input (s)       : pIfEntry      - pointer to IfEntry                     */
/*                   pAdSpec       - pointer to pAdSpec                     */
/*                   pSenderTspec  - pointer to pSenderTspec                */
/*                   pNewAdSpec    - pointer to pNewAdSpec                  */
/* Output (s)      : pNewAdSpec    - Adspec given by the TC                 */
/* Returns         : RPTE_SUCCESS in case the Deregistration is a success   */
/*                   RPTE_FAILURE in case the Deregistration is a failure   */
/****************************************************************************/
UINT1
RpteTcCalcAdSpec (const tIfEntry * pIfEntry, tAdSpec * pAdSpec,
                  tSenderTspec * pSenderTspec, tAdSpec * pNewAdSpec)
{
    UINT1               u1ErrCode = RSVPTE_ZERO;
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcCalcAdSpec : ENTRY\n");
    if (RPTE_TC_CALCULATE_ADSPEC (TC_RSVPTE_APPID,
                                  IF_ENTRY_IF_INDEX (pIfEntry),
                                  RPTE_TC_INTSERV_TYPE,
                                  pAdSpec,
                                  pSenderTspec,
                                  pNewAdSpec, &u1ErrCode) == RPTE_TC_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_TCIF_PRCS, "Calculation Of AdSpec Failed\n");
        RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcCalcAdSpec : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcCalcAdSpec : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteTcGenerateFlowSpec                                 */
/* Description     : This function is used to generate the required         */
/*                   FlowSpec                                               */
/* Input (s)       : pAdSpec      - pointer to AdSpec                       */
/*                   pSenderTspec - pointer to SenderTspec                  */
/*                   pFlowSpec    - pointer to FlowSpec                     */
/*                   pu1ErrCode   - pointer to hold ErrorCode               */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS in case of success else RPTE_FAILURE      */
/****************************************************************************/
UINT1
RpteTcGenerateFlowSpec (tAdSpec * pAdSpec, tSenderTspec * pSenderTspec,
                        tFlowSpec * pFlowSpec, UINT1 *pu1ErrCode)
{
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcGenerateFlowSpec: ENTRY\n");
    if (RPTE_TC_GENERATE_FLOWSPEC (TC_RSVPTE_APPID,
                                   pAdSpec,
                                   pSenderTspec,
                                   pFlowSpec, pu1ErrCode) == RPTE_TC_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_TCIF_PRCS, "Generation Of FlowSpec Failed\n");
        RSVPTE_DBG (RSVPTE_TCIF_ETEXT,
                    " RpteTcGenerateFlowSpec : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcGenerateFlowSpec : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteTcCalculateFlowSpec                                */
/* Description     : This function gets the FlowSpec given by TC            */
/* Input (s)       : pIfEntry      - pointer to IfEntry                     */
/*                   u1ResPoolType - holds ResPoolType value                */
/*                   pAdSpec       - pointer to AdSpec                      */
/*                   pFlowSpec     - pointer to FlowSpec                    */
/*                   pNewFlowSpec  - pointer to FlowSpec                    */
/*                   pu1ErrCode    - pointer to hold error code             */
/* Output (s)      : pNewFlowSpec  - pointer to  FlowSpec given by TC       */
/* Returns         : RPTE_SUCCESS in case of calculation is a success       */
/*                   RPTE_FAILURE in case of calculation is a failure       */
/****************************************************************************/
UINT1
RpteTcCalculateFlowSpec (const tIfEntry * pIfEntry, UINT1 u1ResPoolType,
                         tAdSpec * pAdSpec, tSenderTspec * pSenderTspec,
                         tFlowSpec * pFlowSpec, tFlowSpec * pNewFlowSpec,
                         UINT1 *pu1ErrCode)
{
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcCalculateFlowSpec : ENTRY\n");

    if (pIfEntry == NULL)
    {
        RSVPTE_DBG (RSVPTE_TCIF_PRCS, "Calculation Of FlowSpec Failed\n");
        RSVPTE_DBG (RSVPTE_TCIF_ETEXT,
                    " RpteTcCalculateFlowSpec : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    if (RPTE_TC_CALCULATE_FLOWSPEC (TC_RSVPTE_APPID,
                                    IF_ENTRY_IF_INDEX (pIfEntry),
                                    u1ResPoolType,
                                    pAdSpec,
                                    pSenderTspec,
                                    pFlowSpec,
                                    pNewFlowSpec,
                                    pu1ErrCode) == RPTE_TC_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_TCIF_PRCS, "Calculation Of FlowSpec Failed\n");
        RSVPTE_DBG (RSVPTE_TCIF_ETEXT,
                    " RpteTcCalculateFlowSpec : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcCalculateFlowSpec : EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteTcFreeAppResources                                 */
/* Description     : This function frees the AppResources                   */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteTcFreeAppResources (VOID)
{
    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcFreeAppResources : ENTRY\n");

    RPTE_TC_FREE_APP_RESOURCES (TC_RSVPTE_APPID);

    RSVPTE_DBG (RSVPTE_TCIF_ETEXT, " RpteTcFreeAppResources: EXIT\n");
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptetcif.c                             */
/*---------------------------------------------------------------------------*/
