/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptetst.c,v 1.34 2018/01/03 11:31:23 siva Exp $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptetst.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  : 
 *    DESCRIPTION            : This file contains the low level TEST routines
 *                             for the MIB objects present in the rsvpte.mib
 *                             and Test Routines for Scalar Objects in 
 *                             fsmpfrr.mib                             
 *---------------------------------------------------------------------------*/

#include  "rpteincs.h"
#include  "mplscli.h"
#include  "fsmpfrlw.h"
#define RSVP_TE_TNLINDEX_MINVAL 1
#define RSVP_TE_TNLINDEX_MAXVAL 65535

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfLblSpace
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfLblSpace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfLblSpace (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMplsRsvpTeIfIndex,
                                 INT4 i4TestValFsMplsRsvpTeIfLblSpace)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    switch (i4TestValFsMplsRsvpTeIfLblSpace)
    {
        case RPTE_PERPLATFORM:
        case RPTE_PERINTERFACE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfLblType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfLblType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfLblType (UINT4 *pu4ErrorCode, INT4 i4FsMplsRsvpTeIfIndex,
                                INT4 i4TestValFsMplsRsvpTeIfLblType)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    switch (i4TestValFsMplsRsvpTeIfLblType)
    {
        case RPTE_LBL_GENERIC:
        case RPTE_LBL_ATM:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeAtmMergeCap
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeAtmMergeCap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeAtmMergeCap (UINT4 *pu4ErrorCode,
                                  INT4 i4FsMplsRsvpTeIfIndex,
                                  INT4 i4TestValFsMplsRsvpTeAtmMergeCap)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    switch (i4TestValFsMplsRsvpTeAtmMergeCap)
    {
        case RPTE_NO_MRG:
            break;
        case RPTE_VC_MRG:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeAtmVcDirection
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeAtmVcDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeAtmVcDirection (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMplsRsvpTeIfIndex,
                                     INT4 i4TestValFsMplsRsvpTeAtmVcDirection)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;
    switch (i4TestValFsMplsRsvpTeAtmVcDirection)
    {
        case RPTE_VC_BI_DIR:
        case RPTE_VC_UNI_DIR_ODD:
        case RPTE_VC_UNI_DIR_EVEN:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeAtmMinVpi
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeAtmMinVpi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeAtmMinVpi (UINT4 *pu4ErrorCode, INT4 i4FsMplsRsvpTeIfIndex,
                                INT4 i4TestValFsMplsRsvpTeAtmMinVpi)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((RPTE_MINVPI_MINVAL != i4TestValFsMplsRsvpTeAtmMinVpi) &&
        ((i4TestValFsMplsRsvpTeAtmMinVpi < RPTE_MINVPI_MINVAL) ||
         (i4TestValFsMplsRsvpTeAtmMinVpi > RPTE_MINVPI_MAXVAL)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeAtmMinVci
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeAtmMinVci
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeAtmMinVci (UINT4 *pu4ErrorCode, INT4 i4FsMplsRsvpTeIfIndex,
                                INT4 i4TestValFsMplsRsvpTeAtmMinVci)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((RPTE_MINVCI_MINVAL != i4TestValFsMplsRsvpTeAtmMinVci) &&
        ((i4TestValFsMplsRsvpTeAtmMinVci < RPTE_MINVCI_MINVAL) ||
         (i4TestValFsMplsRsvpTeAtmMinVci > RPTE_MINVCI_MAXVAL)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeAtmMaxVpi
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeAtmMaxVpi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeAtmMaxVpi (UINT4 *pu4ErrorCode, INT4 i4FsMplsRsvpTeIfIndex,
                                INT4 i4TestValFsMplsRsvpTeAtmMaxVpi)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((RPTE_MAXVPI_MINVAL != i4TestValFsMplsRsvpTeAtmMaxVpi) &&
        ((i4TestValFsMplsRsvpTeAtmMaxVpi < RPTE_MAXVPI_MINVAL) ||
         (i4TestValFsMplsRsvpTeAtmMaxVpi > RPTE_MAXVPI_MAXVAL)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeAtmMaxVci
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeAtmMaxVci
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeAtmMaxVci (UINT4 *pu4ErrorCode, INT4 i4FsMplsRsvpTeIfIndex,
                                INT4 i4TestValFsMplsRsvpTeAtmMaxVci)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((RPTE_MAXVCI_MINVAL != i4TestValFsMplsRsvpTeAtmMaxVci) &&
        ((i4TestValFsMplsRsvpTeAtmMaxVci < RPTE_MAXVCI_MINVAL) ||
         (i4TestValFsMplsRsvpTeAtmMaxVci > RPTE_MAXVCI_MAXVAL)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfMtu
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                testValFsMplsRsvpTeIfMtu
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfMtu (UINT4 *pu4ErrorCode, INT4 i4FsMplsRsvpTeIfIndex,
                            INT4 i4TestValFsMplsRsvpTeIfMtu)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((i4TestValFsMplsRsvpTeIfMtu < RPTE_IF_MIN_MTU_SIZE) ||
        (i4TestValFsMplsRsvpTeIfMtu > RPTE_IF_MAX_MTU_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfRefreshMultiple
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfRefreshMultiple
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfRefreshMultiple (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMplsRsvpTeIfIndex,
                                        INT4
                                        i4TestValFsMplsRsvpTeIfRefreshMultiple)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((i4TestValFsMplsRsvpTeIfRefreshMultiple <
         RSVPTE_REFRESH_MULTIPLE_MIN_VAL)
        || (i4TestValFsMplsRsvpTeIfRefreshMultiple >
            RSVPTE_REFRESH_MULTIPLE_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfTTL
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfTTL
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfTTL (UINT4 *pu4ErrorCode, INT4 i4FsMplsRsvpTeIfIndex,
                            INT4 i4TestValFsMplsRsvpTeIfTTL)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((i4TestValFsMplsRsvpTeIfTTL < RSVPTE_IF_SENDTTL_MIN_VAL)
        || (i4TestValFsMplsRsvpTeIfTTL > RSVPTE_IF_SENDTTL_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfRefreshInterval
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfRefreshInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfRefreshInterval (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMplsRsvpTeIfIndex,
                                        INT4
                                        i4TestValFsMplsRsvpTeIfRefreshInterval)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((i4TestValFsMplsRsvpTeIfRefreshInterval < RSVPTE_REFRESH_INTRVL_MIN_VAL)
        || (i4TestValFsMplsRsvpTeIfRefreshInterval >
            RSVPTE_REFRESH_INTRVL_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfRouteDelay
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfRouteDelay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfRouteDelay (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMplsRsvpTeIfIndex,
                                   INT4 i4TestValFsMplsRsvpTeIfRouteDelay)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((i4TestValFsMplsRsvpTeIfRouteDelay < RSVPTE_ROUTE_DELAY_MIN_VAL) ||
        (i4TestValFsMplsRsvpTeIfRouteDelay > RSVPTE_ROUTE_DELAY_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfUdpRequired
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfUdpRequired
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfUdpRequired (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMplsRsvpTeIfIndex,
                                    INT4 i4TestValFsMplsRsvpTeIfUdpRequired)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((i4TestValFsMplsRsvpTeIfUdpRequired != RPTE_SNMP_TRUE) &&
        (i4TestValFsMplsRsvpTeIfUdpRequired != RPTE_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfHelloSupported
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfHelloSupported
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfHelloSupported (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMplsRsvpTeIfIndex,
                                       INT4
                                       i4TestValFsMplsRsvpTeIfHelloSupported)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((i4TestValFsMplsRsvpTeIfHelloSupported != RPTE_SNMP_TRUE) &&
        (i4TestValFsMplsRsvpTeIfHelloSupported != RPTE_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfLinkAttr
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfLinkAttr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfLinkAttr (UINT4 *pu4ErrorCode,
                                 INT4 i4FsMplsRsvpTeIfIndex,
                                 UINT4 u4TestValFsMplsRsvpTeIfLinkAttr)
{
    tIfEntry           *pIfEntry = NULL;
    UINT1               u1RetVal;

    if ((u4TestValFsMplsRsvpTeIfLinkAttr < RSVPTE_LINK_ATTR_MIN_VAL) ||
        (u4TestValFsMplsRsvpTeIfLinkAttr > RSVPTE_LINK_ATTR_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    if (u1RetVal != RPTE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (!((IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTREADY) ||
          (IF_ENTRY_STATUS (pIfEntry) == RPTE_NOTINSERVICE)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfStatus
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfStatus (UINT4 *pu4ErrorCode, INT4 i4FsMplsRsvpTeIfIndex,
                               INT4 i4TestValFsMplsRsvpTeIfStatus)
{
    UINT1               u1RetVal = RPTE_FAILURE;
    tIfEntry           *pIfEntry = NULL;
#ifdef CFA_WANTED
    UINT4               u4L3IfIndex = 0;

    if (CfaUtilGetL3IfFromMplsIf
        ((UINT4) i4FsMplsRsvpTeIfIndex, &u4L3IfIndex, TRUE) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#endif
    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);
    switch (i4TestValFsMplsRsvpTeIfStatus)
    {
        case RPTE_DESTROY:
        case RPTE_ACTIVE:
        case RPTE_NOTINSERVICE:
            if (u1RetVal == RPTE_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case RPTE_CREATEANDWAIT:
            if (u1RetVal == RPTE_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case RPTE_CREATEANDGO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfPlrId
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                testValFsMplsRsvpTeIfPlrId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfPlrId (UINT4 *pu4ErrorCode,
                              INT4 i4FsMplsRsvpTeIfIndex,
                              tSNMP_OCTET_STRING_TYPE
                              * pTestValFsMplsRsvpTeIfPlrId)
{
    UINT1               u1RetVal = SNMP_FAILURE;
    tIfEntry           *pIfEntry = NULL;

    if (pTestValFsMplsRsvpTeIfPlrId->i4_Length != sizeof (UINT4))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return RPTE_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return RPTE_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);

    if (u1RetVal == RPTE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfAvoidNodeId
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object
                testValFsMplsRsvpTeIfAvoidNodeId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfAvoidNodeId (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMplsRsvpTeIfIndex,
                                    tSNMP_OCTET_STRING_TYPE
                                    * pTestValFsMplsRsvpTeIfAvoidNodeId)
{
    UINT1               u1RetVal = SNMP_FAILURE;
    tIfEntry           *pIfEntry = NULL;

    if (pTestValFsMplsRsvpTeIfAvoidNodeId->i4_Length != sizeof (UINT4))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return RPTE_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return RPTE_FAILURE;
    }

    u1RetVal = RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry);

    if (u1RetVal == RPTE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeIfStorageType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex

                The Object 
                testValFsMplsRsvpTeIfStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeIfStorageType (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMplsRsvpTeIfIndex,
                                    INT4 i4TestValFsMplsRsvpTeIfStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMplsRsvpTeIfIndex);
    UNUSED_PARAM (i4TestValFsMplsRsvpTeIfStorageType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeLsrID
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeLsrID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeLsrID (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pTestValFsMplsRsvpTeLsrID)
{
    UINT4               u4IPAddr;

    if (pTestValFsMplsRsvpTeLsrID->i4_Length != RPTE_LSR_ID_LEN)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    CPY_FROM_SNMP ((UINT1 *) (&(u4IPAddr)), pTestValFsMplsRsvpTeLsrID,
                   RSVPTE_IPV4ADR_LEN);
    if ((u4IPAddr == RPTE_MAX_UINT4_VALUE) ||
        ((u4IPAddr >= RPTE_MIN_MCAST_ADDR_VALUE) &&
         (u4IPAddr <= RPTE_MAX_MCAST_ADDR_VALUE)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeHelloSupprtd
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeHelloSupprtd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeHelloSupprtd (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsMplsRsvpTeHelloSupprtd)
{
    switch (i4TestValFsMplsRsvpTeHelloSupprtd)
    {
        case RPTE_SNMP_TRUE:
        case RPTE_SNMP_FALSE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeSockSupprtd
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeSockSupprtd
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeSockSupprtd (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsMplsRsvpTeSockSupprtd)
{
    switch (i4TestValFsMplsRsvpTeSockSupprtd)
    {
        case RPTE_SNMP_TRUE:
        case RPTE_SNMP_FALSE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGenLblSpaceMinLbl
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGenLblSpaceMinLbl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeGenLblSpaceMinLbl (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsMplsRsvpTeGenLblSpaceMinLbl)
{
    if ((i4TestValFsMplsRsvpTeGenLblSpaceMinLbl <
         (INT4) gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange) ||
        (i4TestValFsMplsRsvpTeGenLblSpaceMinLbl >
         (INT4) gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGenLblSpaceMaxLbl
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGenLblSpaceMaxLbl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeGenLblSpaceMaxLbl (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsMplsRsvpTeGenLblSpaceMaxLbl)
{
    if ((i4TestValFsMplsRsvpTeGenLblSpaceMaxLbl <
         (INT4) gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange) ||
        (i4TestValFsMplsRsvpTeGenLblSpaceMaxLbl >
         (INT4) gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGenDebugFlag
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGenDebugFlag
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeGenDebugFlag (UINT4 *pu4ErrorCode,
                                   UINT4 u4TestValFsMplsRsvpTeGenDebugFlag)
{
    if ((u4TestValFsMplsRsvpTeGenDebugFlag | RSVPTE_DBG_ALL) == RSVPTE_DBG_ALL)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGenPduDumpLevel
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGenPduDumpLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeGenPduDumpLevel (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsMplsRsvpTeGenPduDumpLevel)
{
    if (i4TestValFsMplsRsvpTeGenPduDumpLevel > RSVPTE_DUMP_LVL_HDR)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGenPduDumpMsgType
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGenPduDumpMsgType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhTestv2FsMplsRsvpTeGenPduDumpMsgType ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhTestv2FsMplsRsvpTeGenPduDumpMsgType (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsMplsRsvpTeGenPduDumpMsgType)
{

    if (i4TestValFsMplsRsvpTeGenPduDumpMsgType > RSVPTE_DUMP_ALL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGenPduDumpDirection
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGenPduDumpDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeGenPduDumpDirection (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4TestValFsMplsRsvpTeGenPduDumpDirection)
{
    switch (i4TestValFsMplsRsvpTeGenPduDumpDirection)
    {
        case RSVPTE_DUMP_DIR_NONE:
        case RSVPTE_DUMP_DIR_IN:
        case RSVPTE_DUMP_DIR_OUT:
        case RSVPTE_DUMP_DIR_INOUT:
            return SNMP_SUCCESS;
        default:
            break;

    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeOperStatus
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeOperStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeOperStatus (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMplsRsvpTeOperStatus)
{
    switch (i4TestValFsMplsRsvpTeOperStatus)
    {
        case RPTE_ADMIN_UP:
            /* Allow Admin Status Up except when Down In Progress */
            if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) ==
                RPTE_ADMIN_DOWN_IN_PRGRS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case RPTE_ADMIN_DOWN:
            /* Allow Admin Status Down except when Up In Progress */
            if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) == RPTE_ADMIN_UP_IN_PRGRS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        default:
            break;

    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeOverRideOption
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeOverRideOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeOverRideOption (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsMplsRsvpTeOverRideOption)
{
    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsRsvpTeOverRideOption)
    {
        case TE_DS_OVERRIDE_SET:
            break;
        case TE_DS_OVERRIDE_NOT_SET:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeMaxTnls
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeMaxTnls
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeMaxTnls (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsMplsRsvpTeMaxTnls)
{
    if ((i4TestValFsMplsRsvpTeMaxTnls < RSVPTE_TNLS_MIN_VAL) ||
        (i4TestValFsMplsRsvpTeMaxTnls > RSVPTE_TNLS_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeMaxErhopsPerTnl
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeMaxErhopsPerTnl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeMaxErhopsPerTnl (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsMplsRsvpTeMaxErhopsPerTnl)
{
    if ((i4TestValFsMplsRsvpTeMaxErhopsPerTnl < RSVPTE_TNLERHOP_MIN_VAL) ||
        (i4TestValFsMplsRsvpTeMaxErhopsPerTnl > RSVPTE_TNLERHOP_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (RPTE_CLI_RSVP_ENABLE);
        return SNMP_FAILURE;
    }
    /* To check for the tunnels using Erhops more than 
     * i4TestValFsMplsRsvpTeMaxErhopsPerTnl */
    if (rpteTeCheckMaxErhopInTnlTbl (i4TestValFsMplsRsvpTeMaxErhopsPerTnl) !=
        RPTE_SUCCESS)
    {
        CLI_SET_ERR (RPTE_CLI_MAX_ER_HOP);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeMaxActRoutePerTnl
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeMaxActRoutePerTnl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeMaxActRoutePerTnl (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsMplsRsvpTeMaxActRoutePerTnl)
{
    if ((i4TestValFsMplsRsvpTeMaxActRoutePerTnl < RSVPTE_TNLARHOP_MIN_VAL) ||
        (i4TestValFsMplsRsvpTeMaxActRoutePerTnl > RSVPTE_TNLARHOP_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeMaxIfaces
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeMaxIfaces
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeMaxIfaces (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsMplsRsvpTeMaxIfaces)
{
    if ((i4TestValFsMplsRsvpTeMaxIfaces < RSVPTE_MAX_IFACES_MIN_VAL) ||
        (i4TestValFsMplsRsvpTeMaxIfaces > RSVPTE_MAX_IFACES_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeMaxNbrs
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeMaxNbrs
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeMaxNbrs (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsMplsRsvpTeMaxNbrs)
{
    if ((i4TestValFsMplsRsvpTeMaxNbrs < RSVPTE_MAX_NEIGHBOURS_MIN_VAL) ||
        (i4TestValFsMplsRsvpTeMaxNbrs > RSVPTE_MAX_NEIGHBOURS_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* TEST ROUTINES FOR NBR TABLE*/

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeNbrRRCapable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                testValFsMplsRsvpTeNbrRRCapable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeNbrRRCapable (UINT4 *pu4ErrorCode,
                                   INT4 i4FsMplsRsvpTeIfIndex,
                                   UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                   INT4 i4TestValFsMplsRsvpTeNbrRRCapable)
{
    UNUSED_PARAM (i4FsMplsRsvpTeIfIndex);
    UNUSED_PARAM (u4FsMplsRsvpTeNbrIfAddr);
    switch (i4TestValFsMplsRsvpTeNbrRRCapable)
    {
        case RPTE_RR_CAP_ENA:
        case RPTE_RR_CAP_DIS:
            return SNMP_SUCCESS;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeNbrRMDCapable
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                testValFsMplsRsvpTeNbrRMDCapable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeNbrRMDCapable (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMplsRsvpTeIfIndex,
                                    UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                    INT4 i4TestValFsMplsRsvpTeNbrRMDCapable)
{
    UNUSED_PARAM (i4FsMplsRsvpTeIfIndex);
    UNUSED_PARAM (u4FsMplsRsvpTeNbrIfAddr);
    switch (i4TestValFsMplsRsvpTeNbrRMDCapable)
    {
        case RPTE_RMD_CAP_ENA:
        case RPTE_RMD_CAP_DIS:
            return SNMP_SUCCESS;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeNbrEncapsType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                testValFsMplsRsvpTeNbrEncapsType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeNbrEncapsType (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMplsRsvpTeIfIndex,
                                    UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                    INT4 i4TestValFsMplsRsvpTeNbrEncapsType)
{
    UNUSED_PARAM (i4FsMplsRsvpTeIfIndex);
    UNUSED_PARAM (u4FsMplsRsvpTeNbrIfAddr);
    switch (i4TestValFsMplsRsvpTeNbrEncapsType)
    {
        case RPTE_IP_ENCAP:
        case RPTE_UDP_ENCAP:
        case RPTE_BOTH_ENCAP:
            return SNMP_SUCCESS;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;

    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeNbrHelloSupport
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                testValFsMplsRsvpTeNbrHelloSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeNbrHelloSupport (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMplsRsvpTeIfIndex,
                                      UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                      INT4 i4TestValFsMplsRsvpTeNbrHelloSupport)
{
    UNUSED_PARAM (i4FsMplsRsvpTeIfIndex);
    UNUSED_PARAM (u4FsMplsRsvpTeNbrIfAddr);
    switch (i4TestValFsMplsRsvpTeNbrHelloSupport)
    {
        case RPTE_NBR_HELLO_SPRT_ENA:
        case RPTE_NBR_HELLO_SPRT_DIS:
            return SNMP_SUCCESS;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeNbrStatus
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                testValFsMplsRsvpTeNbrStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeNbrStatus (UINT4 *pu4ErrorCode, INT4 i4FsMplsRsvpTeIfIndex,
                                UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                INT4 i4TestValFsMplsRsvpTeNbrStatus)
{
    UINT1               u1RetVal;
    tIfEntry           *pIfEntry = NULL;
    tNbrEntry          *pNbrEntry = NULL;

    UNUSED_PARAM (u4FsMplsRsvpTeNbrIfAddr);

    if (RpteGetIfEntry (i4FsMplsRsvpTeIfIndex, &pIfEntry) == RPTE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    u1RetVal = RpteGetNbrEntry (u4FsMplsRsvpTeNbrIfAddr, pIfEntry, &pNbrEntry);

    switch (i4TestValFsMplsRsvpTeNbrStatus)
    {
        case RPTE_DESTROY:
        case RPTE_ACTIVE:
            if (u1RetVal == RPTE_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            if ((i4TestValFsMplsRsvpTeNbrStatus == RPTE_ACTIVE) &&
                (NBR_ENTRY_STATUS (pNbrEntry) == RPTE_ACTIVE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;

        case RPTE_CREATEANDWAIT:
            if (u1RetVal == RPTE_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            return SNMP_SUCCESS;
        case RPTE_CREATEANDGO:
        case RPTE_NOTINSERVICE:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeNbrStorageType
 Input       :  The Indices
                FsMplsRsvpTeIfIndex
                FsMplsRsvpTeNbrIfAddr

                The Object 
                testValFsMplsRsvpTeNbrStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeNbrStorageType (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMplsRsvpTeIfIndex,
                                     UINT4 u4FsMplsRsvpTeNbrIfAddr,
                                     INT4 i4TestValFsMplsRsvpTeNbrStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4FsMplsRsvpTeIfIndex);
    UNUSED_PARAM (u4FsMplsRsvpTeNbrIfAddr);
    UNUSED_PARAM (i4TestValFsMplsRsvpTeNbrStorageType);
    return SNMP_FAILURE;
}

/* TEST ROUTINES FOR GENOBJECTS*/

/****************************************************************************
 Function    :  nmhTestv2FsMplssvpTeHelloIntervalTime
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeHelloIntervalTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeHelloIntervalTime (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsMplsRsvpTeHelloIntervalTime)
{
    if ((i4TestValFsMplsRsvpTeHelloIntervalTime < RSVPTE_HELLO_INTERVAL_MIN_VAL)
        ||
        (i4TestValFsMplsRsvpTeHelloIntervalTime >
         RSVPTE_HELLO_INTERVAL_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeRRCapable
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeRRCapable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeRRCapable (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsMplsRsvpTeRRCapable)
{
    switch (i4TestValFsMplsRsvpTeRRCapable)
    {
        case RPTE_RR_ENABLED:
        case RPTE_RR_DISABLED:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeMsgIdCapable
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeMsgIdCapable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeMsgIdCapable (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsMplsRsvpTeMsgIdCapable)
{
    switch (i4TestValFsMplsRsvpTeMsgIdCapable)
    {
        case RPTE_MSGID_ENABLED:
        case RPTE_MSGID_DISABLED:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeRMDPolicyObject
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeRMDPolicyObject
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeRMDPolicyObject (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValFsMplsRsvpTeRMDPolicyObject)
{
    UINT1               u1RmdPolicyObj = RPTE_ZERO;

    if (pTestValFsMplsRsvpTeRMDPolicyObject->i4_Length != RPTE_ONE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    CPY_FROM_SNMP ((UINT1 *) (&(u1RmdPolicyObj)),
                   pTestValFsMplsRsvpTeRMDPolicyObject, RPTE_ONE);
    if ((u1RmdPolicyObj > RPTE_RMD_ALL_MSGS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeMinTnlsWithMsgId
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeMinTnlsWithMsgId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeMinTnlsWithMsgId (UINT4 *pu4ErrorCode,
                                       UINT4
                                       u4TestValFsMplsRsvpTeMinTnlsWithMsgId)
{
    if ((u4TestValFsMplsRsvpTeMinTnlsWithMsgId < RSVPTE_TNLS_MIN_VAL) ||
        (u4TestValFsMplsRsvpTeMinTnlsWithMsgId > RSVPTE_TNLS_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeNotificationEnabled
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeNotificationEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeNotificationEnabled (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4TestValFsMplsRsvpTeNotificationEnabled)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u1NotificationEnabled =
        (UINT1) i4TestValFsMplsRsvpTeNotificationEnabled;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_NOTIFICATION_ENABLED);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitIntvl
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeNotifyMsgRetransmitIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitIntvl (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4TestValFsMplsRsvpTeNotifyMsgRetransmitIntvl)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u4NotifRetransmitIntvl =
        u4TestValFsMplsRsvpTeNotifyMsgRetransmitIntvl;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_NOTIFY_RETRANSMIT_INTERVAL);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitDecay
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeNotifyMsgRetransmitDecay
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitDecay (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4TestValFsMplsRsvpTeNotifyMsgRetransmitDecay)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u4NotifRetransmitDecay =
        u4TestValFsMplsRsvpTeNotifyMsgRetransmitDecay;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_NOTIFY_RETRANSMIT_DECAY);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitLimit
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeNotifyMsgRetransmitLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitLimit (UINT4 *pu4ErrorCode,
                                               UINT4
                                               u4TestValFsMplsRsvpTeNotifyMsgRetransmitLimit)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u4NotifRetransmitLimit =
        u4TestValFsMplsRsvpTeNotifyMsgRetransmitLimit;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_NOTIFY_RETRANSMIT_LIMIT);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeAdminStatusTimeIntvl
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeAdminStatusTimeIntvl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeAdminStatusTimeIntvl (UINT4 *pu4ErrorCode,
                                           UINT4
                                           u4TestValFsMplsRsvpTeAdminStatusTimeIntvl)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u4AdminStatusTimerValue =
        u4TestValFsMplsRsvpTeAdminStatusTimeIntvl;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_ADMIN_STATUS_TIMER);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTePathStateRemovedSupport
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTePathStateRemovedSupport
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTePathStateRemovedSupport (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4TestValFsMplsRsvpTePathStateRemovedSupport)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u1PathStateRemovedFlag =
        (UINT1) i4TestValFsMplsRsvpTePathStateRemovedSupport;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_PSB_REMOVED_FLAG);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeLabelSetEnabled
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeLabelSetEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeLabelSetEnabled (UINT4 *pu4ErrorCode,
                                      INT4 i4TestValFsMplsRsvpTeLabelSetEnabled)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u1LabelSetEnabled =
        (UINT1) i4TestValFsMplsRsvpTeLabelSetEnabled;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_LABEL_SET_ENABLED);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeAdminStatusCapability
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeAdminStatusCapability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeAdminStatusCapability (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4TestValFsMplsRsvpTeAdminStatusCapability)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u1AdminStatusCapable =
        (UINT1) i4TestValFsMplsRsvpTeAdminStatusCapability;
    i4RetVal =
        RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                RSVPTE_ADMIN_STATUS_CAPABILITY);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGrCapability
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGrCapability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeGrCapability (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsMplsRsvpTeGrCapability)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u1GRCapability = (UINT1) i4TestValFsMplsRsvpTeGrCapability;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_GR_CAPABILITY);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGrRecoveryPathCapability
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGrRecoveryPathCapability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeGrRecoveryPathCapability (UINT4 *pu4ErrorCode,
                                               tSNMP_OCTET_STRING_TYPE
                                               *
                                               pTestValFsMplsRsvpTeGrRecoveryPathCapability)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    UINT1               u1RecoveryPathCapability = RPTE_ZERO;
    CPY_FROM_SNMP ((UINT1 *) (&(u1RecoveryPathCapability)),
                   pTestValFsMplsRsvpTeGrRecoveryPathCapability, RPTE_ONE);
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u1RecoveryPathCapability = u1RecoveryPathCapability;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_GR_RECOVERY_PATH_CAPABILITY);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGrRestartTime
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGrRestartTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeGrRestartTime (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsMplsRsvpTeGrRestartTime)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u2RestartTime = (UINT2) i4TestValFsMplsRsvpTeGrRestartTime;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_GR_RESTART_TIME);
    return (INT1) i4RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGrRecoveryTime
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGrRecoveryTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeGrRecoveryTime (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsMplsRsvpTeGrRecoveryTime)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u2RecoveryTime =
        (UINT2) i4TestValFsMplsRsvpTeGrRecoveryTime;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_GR_RECOVERY_TIME);
    return (INT1) i4RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrRevertiveMode
 Input       :  The Indices

                The Object 
                testValFsMplsFrrRevertiveMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrRevertiveMode (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE
                                 * pTestValFsMplsFrrRevertiveMode)
{
    if (pTestValFsMplsFrrRevertiveMode->i4_Length != sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    if ((pTestValFsMplsFrrRevertiveMode->pu1_OctetList[RPTE_ZERO] !=
         RPTE_FRR_REVERTIVE_GLOBAL) &&
        (pTestValFsMplsFrrRevertiveMode->pu1_OctetList[RPTE_ZERO] !=
         RPTE_FRR_REVERTIVE_LOCAL) &&
        (pTestValFsMplsFrrRevertiveMode->pu1_OctetList[RPTE_ZERO] !=
         RPTE_FRR_REVERTIVE_BOTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrDetourMergingEnabled
 Input       :  The Indices

                The Object 
                testValFsMplsFrrDetourMergingEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrDetourMergingEnabled (UINT4 *pu4ErrorCode,
                                        INT4
                                        i4TestValFsMplsFrrDetourMergingEnabled)
{
    if ((i4TestValFsMplsFrrDetourMergingEnabled != RPTE_SNMP_TRUE) &&
        (i4TestValFsMplsFrrDetourMergingEnabled != RPTE_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrDetourEnabled
 Input       :  The Indices

                The Object 
                testValFsMplsFrrDetourEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrDetourEnabled (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMplsFrrDetourEnabled)
{
    if ((i4TestValFsMplsFrrDetourEnabled != RPTE_SNMP_TRUE) &&
        (i4TestValFsMplsFrrDetourEnabled != RPTE_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrCspfRetryInterval
 Input       :  The Indices

                The Object 
                testValFsMplsFrrCspfRetryInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrCspfRetryInterval (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsMplsFrrCspfRetryInterval)
{
    if ((i4TestValFsMplsFrrCspfRetryInterval <
         RPTE_FRR_CSPF_RETRY_INTERVAL_MIN) ||
        (i4TestValFsMplsFrrCspfRetryInterval >
         RPTE_FRR_CSPF_RETRY_INTERVAL_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrCspfRetryCount
 Input       :  The Indices

                The Object 
                testValFsMplsFrrCspfRetryCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrCspfRetryCount (UINT4 *pu4ErrorCode,
                                  UINT4 u4TestValFsMplsFrrCspfRetryCount)
{
    if ((u4TestValFsMplsFrrCspfRetryCount < RPTE_FRR_CSPF_RETRY_COUNT_MIN) ||
        (u4TestValFsMplsFrrCspfRetryCount > RPTE_FRR_CSPF_RETRY_COUNT_MAX))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrNotifsEnabled
 Input       :  The Indices

                The Object 
                testValFsMplsFrrNotifsEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrNotifsEnabled (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMplsFrrNotifsEnabled)
{
    if ((i4TestValFsMplsFrrNotifsEnabled != RPTE_SNMP_TRUE) &&
        (i4TestValFsMplsFrrNotifsEnabled != RPTE_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrMakeAfterBreakEnabled
 Input       :  The Indices

                The Object
                testValFsMplsFrrMakeAfterBreakEnabled
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrMakeAfterBreakEnabled (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsMplsFrrMakeAfterBreakEnabled)
{
    if ((i4TestValFsMplsFrrMakeAfterBreakEnabled != RPTE_SNMP_TRUE) &&
        (i4TestValFsMplsFrrMakeAfterBreakEnabled != RPTE_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsBypassTunnelRowStatus
 Input       :  The Indices
                FsMplsBypassTunnelIfIndex
                FsMplsBypassTunnelIndex
                FsMplsBypassTunnelInstance
                FsMplsBypassTunnelIngressLSRId
                FsMplsBypassTunnelEgressLSRId

                The Object 
                testValFsMplsBypassTunnelRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsBypassTunnelRowStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMplsBypassTunnelIfIndex,
                                      UINT4 u4FsMplsBypassTunnelIndex,
                                      UINT4 u4FsMplsBypassTunnelIngressLSRId,
                                      UINT4 u4FsMplsBypassTunnelEgressLSRId,
                                      INT4 i4TestValFsMplsBypassTunnelRowStatus)
{
    tIfEntry           *pIfEntry = NULL;
    tTMO_DLL           *pIfFacTnlsList = NULL;
    tTMO_DLL_NODE      *pIfFacTnlsNode = NULL;
    tFacilityRpteBkpTnl *pTmpFacilityRpteBkpTnl = NULL;
#ifdef CFA_WANTED
    UINT4               u4L3Intf = RPTE_ZERO;
#endif
    BOOL1               bFound = RPTE_FALSE;

    UNUSED_PARAM (u4FsMplsBypassTunnelIngressLSRId);
    UNUSED_PARAM (u4FsMplsBypassTunnelEgressLSRId);

    /* Operations other than CREATE AND GO and DESTROY is not permitted for
     * this object */
    if ((i4TestValFsMplsBypassTunnelRowStatus != RPTE_CREATEANDGO) &&
        (i4TestValFsMplsBypassTunnelRowStatus != RPTE_DESTROY))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

#ifdef CFA_WANTED
    /* MPLS Should have been enabled on the Interface Passed */
    if (CfaUtilGetL3IfFromMplsIf ((UINT4) i4FsMplsBypassTunnelIfIndex,
                                  &u4L3Intf, RPTE_TRUE) == CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#endif

    MPLS_RSVPTE_LOCK ();
    /* Associated pIfEntry for the interface passed should be there */
    if (RpteGetIfEntry (i4FsMplsBypassTunnelIfIndex, &pIfEntry) == RPTE_FAILURE)
    {
        MPLS_RSVPTE_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check whether the tunnel is already associated. */
    pIfFacTnlsList = (tTMO_DLL *) & (IF_ENTRY_FAC_TNL_LIST (pIfEntry));

    TMO_DLL_Scan (pIfFacTnlsList, pIfFacTnlsNode, tTMO_DLL_NODE *)
    {
        pTmpFacilityRpteBkpTnl = (tFacilityRpteBkpTnl *) (pIfFacTnlsNode);

        if ((RPTE_FRR_PROT_IF_INDEX (pTmpFacilityRpteBkpTnl)
             == (UINT4) i4FsMplsBypassTunnelIfIndex)
            && (RPTE_FRR_FACILITY_BKP_TNL_INDEX (pTmpFacilityRpteBkpTnl)
                == u4FsMplsBypassTunnelIndex))
        {
            bFound = RPTE_TRUE;
            break;
        }
    }

    if (i4TestValFsMplsBypassTunnelRowStatus == RPTE_CREATEANDGO)
    {
        if (bFound == RPTE_TRUE)
        {
            /* Tunnel is already associated with the interface. */
            MPLS_RSVPTE_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    else
    {
        /* Tunnel is not associated with the interace. 
         * Dont try to deassociate it. */
        if (bFound == RPTE_FALSE)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_RSVPTE_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    MPLS_RSVPTE_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  RsvpTeTestAllCfgParams
 Input       :  pu4ErrorCode
                pRsvpTeCfgParams
                u4MiBObject
 Description :  This function is used to test the scalar mib objects
                of fsrsvpte.mib
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
***************************************************************************/
INT4
RsvpTeTestAllCfgParams (tRsvpTeCfgParams * pRsvpTeCfgParams,
                        UINT4 *pu4ErrorCode, UINT4 u4MiBObject)
{
    if (gpRsvpTeGblInfo->u1RsvpTeAdminStatus != RPTE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    switch (u4MiBObject)
    {
            /* fsMplsRsvpTeNotificationEnabled object validation */
        case RSVPTE_NOTIFICATION_ENABLED:
            if ((pRsvpTeCfgParams->u1NotificationEnabled != RPTE_ENABLED) &&
                (pRsvpTeCfgParams->u1NotificationEnabled != RPTE_DISABLED))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTeNotifyMsgRetransmitIntvl object validation */
        case RSVPTE_NOTIFY_RETRANSMIT_INTERVAL:
            if ((pRsvpTeCfgParams->u4NotifRetransmitIntvl <
                 RPTE_NOTIFY_RETRY_INTVL_MIN_VAL) ||
                (pRsvpTeCfgParams->u4NotifRetransmitIntvl >
                 RPTE_NOTIFY_RETRY_INTVL_MAX_VAL))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTeNotifyMsgRetransmitDecay object validation */
        case RSVPTE_NOTIFY_RETRANSMIT_DECAY:
            if ((pRsvpTeCfgParams->u4NotifRetransmitDecay != RPTE_ENABLED) &&
                (pRsvpTeCfgParams->u4NotifRetransmitDecay != RPTE_DISABLED))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTeNotifyMsgRetransmitLimit object validation */
        case RSVPTE_NOTIFY_RETRANSMIT_LIMIT:
            if ((pRsvpTeCfgParams->u4NotifRetransmitLimit <
                 RPTE_NOTIFY_RETRY_LIMIT_MIN_VAL) ||
                (pRsvpTeCfgParams->u4NotifRetransmitLimit >
                 RPTE_NOTIFY_RETRY_LIMIT_MAX_VAL))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTeAdminStatusTimeIntvl object validation */
        case RSVPTE_ADMIN_STATUS_TIMER:
            if ((pRsvpTeCfgParams->u4AdminStatusTimerValue <
                 RPTE_ADMIN_STATUS_TIME_INTERVAL_MIN) ||
                (pRsvpTeCfgParams->u4AdminStatusTimerValue >
                 RPTE_ADMIN_STATUS_TIME_INTERVAL_MAX))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTePathStateRemovedSupport object validation */
        case RSVPTE_PSB_REMOVED_FLAG:
            if ((pRsvpTeCfgParams->u1PathStateRemovedFlag != RPTE_ENABLED) &&
                (pRsvpTeCfgParams->u1PathStateRemovedFlag != RPTE_DISABLED))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTeLabelSetEnabled object validation */
        case RSVPTE_LABEL_SET_ENABLED:
            if ((pRsvpTeCfgParams->u1LabelSetEnabled != RPTE_ENABLED) &&
                (pRsvpTeCfgParams->u1LabelSetEnabled != RPTE_DISABLED))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTeAdminStatusCapabiliy object validation */
        case RSVPTE_ADMIN_STATUS_CAPABILITY:
            if ((pRsvpTeCfgParams->u1AdminStatusCapable != RPTE_ENABLED) &&
                (pRsvpTeCfgParams->u1AdminStatusCapable != RPTE_DISABLED))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTeGrCapability object validation */
        case RSVPTE_GR_CAPABILITY:
            if ((pRsvpTeCfgParams->u1GRCapability != RPTE_GR_CAPABILITY_NONE) &&
                (pRsvpTeCfgParams->u1GRCapability != RPTE_GR_CAPABILITY_FULL) &&
                (pRsvpTeCfgParams->u1GRCapability != RPTE_GR_CAPABILITY_HELPER))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTeGrRecoveryPathCapability object validation */
        case RSVPTE_GR_RECOVERY_PATH_CAPABILITY:
            if ((pRsvpTeCfgParams->
                 u1RecoveryPathCapability & RPTE_GR_RECOVERY_PATH_SREFRESH)
                && (gu1RRCapable == RPTE_RR_DISABLED))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            switch (pRsvpTeCfgParams->u1RecoveryPathCapability)
            {
                case RPTE_GR_RECOVERY_PATH_TX:
                case RPTE_GR_RECOVERY_PATH_RX:
                case RPTE_GR_RECOVERY_PATH_SREFRESH:
                case (RPTE_GR_RECOVERY_PATH_TX + RPTE_GR_RECOVERY_PATH_RX):
                case (RPTE_GR_RECOVERY_PATH_TX + RPTE_GR_RECOVERY_PATH_SREFRESH):
                case (RPTE_GR_RECOVERY_PATH_RX + RPTE_GR_RECOVERY_PATH_SREFRESH):
                case RPTE_GR_RECOVERY_PATH_DEFAULT:
                case RPTE_GR_RECOVERY_PATH_FULL:
                    break;
                default:
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTeGrRestartTime object validation */
        case RSVPTE_GR_RESTART_TIME:
            if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability ==
                RPTE_GR_CAPABILITY_NONE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if ((pRsvpTeCfgParams->u2RestartTime <
                 RPTE_GR_RESTART_INTVL_MIN_VAL)
                || (pRsvpTeCfgParams->u2RestartTime >
                    RPTE_GR_RESTART_INTVL_MAX_VAL))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
            /* fsMplsRsvpTeGrRecoveryTime object validation */
        case RSVPTE_GR_RECOVERY_TIME:
            if (gpRsvpTeGblInfo->RsvpTeCfgParams.u1GRCapability ==
                RPTE_GR_CAPABILITY_NONE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if ((pRsvpTeCfgParams->u2RecoveryTime <
                 RPTE_GR_RECOVERY_INTVL_MIN_VAL)
                || (pRsvpTeCfgParams->u2RecoveryTime >
                    RPTE_GR_RECOVERY_INTVL_MAX_VAL))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case RSVPTE_REOPTIMIZE_TIME:
            if ((pRsvpTeCfgParams->u4ReoptimizeTime <
                 RPTE_REOPT_TIME_INTVL_MIN_VAL)
                || (pRsvpTeCfgParams->u4ReoptimizeTime >
                    RPTE_REOPT_TIME_INTVL_MAX_VAL))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        case RSVPTE_ERO_CACHE_TIME:
            if ((pRsvpTeCfgParams->u4EroCacheTime <
                 RPTE_ERO_CACHE_INTVL_MIN_VAL)
                || (pRsvpTeCfgParams->u4EroCacheTime >
                    RPTE_ERO_CACHE_INTVL_MAX_VAL))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        default:
            return SNMP_FAILURE;
            break;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeReoptimizeTime
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeReoptimizeTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeReoptimizeTime (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsMplsRsvpTeReoptimizeTime)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u4ReoptimizeTime =
        (UINT2) i4TestValFsMplsRsvpTeReoptimizeTime;
    i4RetVal =
        RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                RSVPTE_REOPTIMIZE_TIME);
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeEroCacheTime
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeEroCacheTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeEroCacheTime (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsMplsRsvpTeEroCacheTime)
{
    tRsvpTeCfgParams    RsvpTeCfgParams;
    INT4                i4RetVal = SNMP_FAILURE;
    MEMSET (&RsvpTeCfgParams, RSVPTE_ZERO, sizeof (tRsvpTeCfgParams));
    RsvpTeCfgParams.u4EroCacheTime = (UINT2) i4TestValFsMplsRsvpTeEroCacheTime;
    i4RetVal = RsvpTeTestAllCfgParams (&RsvpTeCfgParams, pu4ErrorCode,
                                       RSVPTE_ERO_CACHE_TIME);
    return (INT1) i4RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeReoptLinkMaintenance
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeReoptLinkMaintenance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeReoptLinkMaintenance (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValFsMplsRsvpTeReoptLinkMaintenance)
{
    UINT4               u4IfIndex =
        (UINT4) i4TestValFsMplsRsvpTeReoptLinkMaintenance;
    UINT4               u4MplsIfIndex = 0;

    if (CfaUtilGetMplsIfFromIfIndex (u4IfIndex, &u4MplsIfIndex, TRUE) ==
        CFA_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeReoptNodeMaintenance
 Input       :  The Indices

                The Object
                testValFsMplsRsvpTeReoptNodeMaintenance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeReoptNodeMaintenance (UINT4 *pu4ErrorCode,
                                           INT4
                                           i4TestValFsMplsRsvpTeReoptNodeMaintenance)
{

    if (rpteIsTeReoptimizeTnlListEmpty () == FALSE)
    {
        CLI_SET_ERR (RPTE_CLI_RSVP_NO_REOPT_TUNNEL);
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (i4TestValFsMplsRsvpTeReoptNodeMaintenance);
    return SNMP_SUCCESS;
}

#ifdef MPLS_L3VPN_WANTED
/* RSVP-TE MPLS-L3VPN-TE */
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsL3VpnRsvpTeMapTnlIndex
 Input       :  The Indices
                FsMplsL3VpnRsvpTeMapPrefixType
                FsMplsL3VpnRsvpTeMapPrefix
                FsMplsL3VpnRsvpTeMapMaskType
                FsMplsL3VpnRsvpTeMapMask

                The Object
                testValFsMplsL3VpnRsvpTeMapTnlIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL3VpnRsvpTeMapTnlIndex (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMplsL3VpnRsvpTeMapPrefixType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMplsL3VpnRsvpTeMapPrefix,
                                       INT4 i4FsMplsL3VpnRsvpTeMapMaskType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pFsMplsL3VpnRsvpTeMapMask,
                                       INT4
                                       i4TestValFsMplsL3VpnRsvpTeMapTnlIndex)
{
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    UINT4               u4EgressId = 0;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo          TeTnlInfo;
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    MEMSET (&TeTnlInfo, 0, sizeof (tTeTnlInfo));

    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapPrefix, u4Prefix);
    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapMask, u4Mask);
    pTeTnlInfo = &TeTnlInfo;

    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "Testv2FsMplsL3VpnRsvpTeMapTnlIndex  Entry \t\n");

    if ((pL3VpnRsvpMapLabelEntry =
         L3vpnGetRsvpMapLabelEntry ((UINT1) i4FsMplsL3VpnRsvpTeMapPrefixType,
                                    u4Prefix,
                                    (UINT1) i4FsMplsL3VpnRsvpTeMapMaskType,
                                    u4Mask)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Testv2FsMplsL3VpnRsvpTeMapTnlIndex Failed :No Entry Present\t\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* validate TnlIndex is exist or not */
    if (TeUtlFetchTnlInfoFromTnlTable
        ((UINT4) i4TestValFsMplsL3VpnRsvpTeMapTnlIndex,
         &TeTnlInfo) != TE_SUCCESS)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Testv2FsMplsL3VpnRsvpTeMapTnlIndex Failed :Tunnel is not Present in Rsvp-Te\t\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* validate tunnel EgressId is match to input prefix or not */
    CONVERT_TO_INTEGER (TE_TNL_EGRESS_LSRID (pTeTnlInfo), u4EgressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);
    if (u4Prefix != u4EgressId)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Testv2FsMplsL3VpnRsvpTeMapTnlIndex Failed :Prefix is not Present in Rsvp-Te\t\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* RSVP-Te is not enabled for corresponding tunnel */
    if (pTeTnlInfo->u1TnlSgnlPrtcl != TE_SIGPROTO_RSVP)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Testv2FsMplsL3VpnRsvpTeMapTnlIndex Failed :RSVP-Te is not enabled\t\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;

    }
    if ((pL3VpnRsvpMapLabelEntry->u1RowStatus == NOT_READY) ||
        (pL3VpnRsvpMapLabelEntry->u1RowStatus == NOT_IN_SERVICE))
    {
        if ((i4TestValFsMplsL3VpnRsvpTeMapTnlIndex >= RSVP_TE_TNLINDEX_MINVAL)
            && (i4TestValFsMplsL3VpnRsvpTeMapTnlIndex <=
                RSVP_TE_TNLINDEX_MAXVAL))
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Testv2FsMplsL3VpnRsvpTeMapTnlIndex  Exit \t\n");
            return SNMP_SUCCESS;
        }
    }
    /* If RowStatus is Active means, Tunnel entry is already exist.so, no need to config again */
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Testv2FsMplsL3VpnRsvpTeMapTnlIndex Failed:Entry is already present\t\n");
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "Testv2FsMplsL3VpnRsvpTeMapTnlIndex Failed\t\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsL3VpnRsvpTeMapRowStatus
 Input       :  The Indices
                FsMplsL3VpnRsvpTeMapPrefixType
                FsMplsL3VpnRsvpTeMapPrefix
                FsMplsL3VpnRsvpTeMapMaskType
                FsMplsL3VpnRsvpTeMapMask

                The Object
                testValFsMplsL3VpnRsvpTeMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsL3VpnRsvpTeMapRowStatus (UINT4 *pu4ErrorCode,
                                        INT4 i4FsMplsL3VpnRsvpTeMapPrefixType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMplsL3VpnRsvpTeMapPrefix,
                                        INT4 i4FsMplsL3VpnRsvpTeMapMaskType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pFsMplsL3VpnRsvpTeMapMask,
                                        INT4
                                        i4TestValFsMplsL3VpnRsvpTeMapRowStatus)
{
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
/*    UINT1                   u1Counter = 0;*/
    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;

    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "Testv2FsMplsL3VpnRsvpTeMapRowStatus Entry \t\n");
    if ((nmhValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable
         (i4FsMplsL3VpnRsvpTeMapPrefixType, pFsMplsL3VpnRsvpTeMapPrefix,
          i4FsMplsL3VpnRsvpTeMapMaskType,
          pFsMplsL3VpnRsvpTeMapMask)) == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Testv2FsMplsL3VpnRsvpTeMapRowStatus validation failure \t\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapPrefix, u4Prefix);
    MPLS_OCTETSTRING_TO_INTEGER (pFsMplsL3VpnRsvpTeMapMask, u4Mask);

    pL3VpnRsvpMapLabelEntry =
        L3vpnGetRsvpMapLabelEntry ((UINT1) i4FsMplsL3VpnRsvpTeMapPrefixType,
                                   u4Prefix,
                                   (UINT1) i4FsMplsL3VpnRsvpTeMapMaskType,
                                   u4Mask);

    switch (i4TestValFsMplsL3VpnRsvpTeMapRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pL3VpnRsvpMapLabelEntry != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed :Entry Present :\
                        Can't create\t\n");
                return SNMP_FAILURE;
            }
            if ((i4FsMplsL3VpnRsvpTeMapPrefixType != IPV4) ||
                (i4FsMplsL3VpnRsvpTeMapMaskType != IPV4))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed :Invalid address type\t\n");
                return SNMP_FAILURE;
            }
            break;
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed :Inconsistent Value\t\n");
            return SNMP_FAILURE;
        case ACTIVE:
            if (pL3VpnRsvpMapLabelEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL,
                     "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed :No Entry Present\t\n");
                return SNMP_FAILURE;
            }
            if ((pL3VpnRsvpMapLabelEntry->u1RowStatus == NOT_READY) ||
                (pL3VpnRsvpMapLabelEntry->u1RowStatus == NOT_IN_SERVICE))
            {
                if ((pL3VpnRsvpMapLabelEntry->u4TnlIndex != 0) &&
                    (pL3VpnRsvpMapLabelEntry->u1TnlSgnlPrtcl ==
                     TE_SIGPROTO_RSVP))
                {
                    return SNMP_SUCCESS;
                }
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed \t\n");
                return SNMP_FAILURE;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed \t\n");
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed :Mpls l3vpn bind error \t\n");
            return SNMP_FAILURE;
        case NOT_READY:
            if (pL3VpnRsvpMapLabelEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed :No Entry Present\t\n");
                return SNMP_FAILURE;
            }
            if ((pL3VpnRsvpMapLabelEntry->u1RowStatus == ACTIVE) ||
                (pL3VpnRsvpMapLabelEntry->u1RowStatus == NOT_IN_SERVICE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed \t\n");
                return SNMP_FAILURE;
            }
            break;

        case DESTROY:
            if (pL3VpnRsvpMapLabelEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL,
                     "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed :No Entry Present\t\n");
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Testv2FsMplsL3VpnRsvpTeMapRowStatus Failed \t\n");
            return SNMP_FAILURE;
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "Testv2FsMplsL3VpnRsvpTeMapRowStatus Exit \t\n");
    return SNMP_SUCCESS;

}

#endif

/*----------------------------------------------------------------------------*/
/*                      End of File rptetst.c                                 */
/*----------------------------------------------------------------------------*/
