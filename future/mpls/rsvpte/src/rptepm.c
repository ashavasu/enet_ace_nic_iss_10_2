
/********************************************************************
 *                                                                  *
 * $Id: rptepm.c,v 1.13 2016/01/28 09:40:29 siva Exp $             * 
 *                                                                  *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptepm.c 
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                     
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains entry point function to 
 *                             Pre-Emption Module(PM) and supporting routines. 
 *                             PM keeps track of all the RSVP-TE tunnels.
 *---------------------------------------------------------------------------*/

#include "rpteincs.h"
/******************************************************************************
 * Function Name : RptePmAddTnlToPrioList                                     *
 * Description   : This routine is called when a RSVP tunnel is to be added   *
 *               : to the priority list.                                      * 
 * Input(s)      : pIfEntry - Pointer to Interface Entry to which the tunnel  *
 *                 is to be added                                             *
 *               : u1Priority - Priority of the tunnel                        *
 *               : pRsvpTeTnlInfoInput - Pointer to tunnel info structure of  *
 *                 the tunnel to be added to the priority list.               *
 * Output(s)     : None                                                       *
 ******************************************************************************/
VOID
RptePmAddTnlToPrioList (tIfEntry * pIfEntry, UINT1 u1Priority,
                        UINT1 u1SegDirection,
                        tRsvpTeTnlInfo * pRsvpTeTnlInfoInput)
{
    tTMO_SLL           *pRsvpTePrioList = NULL;

    /* The two pointers defined below are used to traverse the Priority list. 
     */
    tTMO_SLL_NODE      *pTnlListPrevNode = NULL;
    tTMO_SLL_NODE      *pTnlListNextNode = NULL;

    /* The pointer to be added to the priority list */
    tTMO_SLL_NODE      *pTnlListCurNode = NULL;

    tRsvpTeTnlInfo     *pRsvpTeTnlNextInfo = NULL;
    RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmAddTnlToPrioList : ENTRY \n");

    /* Get the priority list and node to be added to that list based
     * on direction passed. */
    if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
    {
        pTnlListCurNode =
            RSVP_GET_TNL_LIST_HOLD_DN_STR_NODE (pRsvpTeTnlInfoInput);
        pRsvpTePrioList = &(pIfEntry->aRpteDnStrPrioTnlList[u1Priority]);
    }
    else
    {
        pTnlListCurNode =
            RSVP_GET_TNL_LIST_HOLD_UP_STR_NODE (pRsvpTeTnlInfoInput);
        pRsvpTePrioList = &(pIfEntry->aRpteUpStrPrioTnlList[u1Priority]);
    }

    /* If List Count is Zero  then simply append the node to the empty list
     */
    if (TMO_SLL_Count (pRsvpTePrioList) == RSVPTE_ZERO)
    {
        TMO_SLL_Add (pRsvpTePrioList, pTnlListCurNode);
        RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmAddTnlToPrioList : EXIT \n");
        return;
    }

    /* For listcount more than zero traverse the list node by node and
     * add the node such that the decreasing order of the traffic params
     * is maintained.
     * This is done to ensure that the least number of tunnels are
     * preempted.
     */
    TMO_SLL_Scan (pRsvpTePrioList, pTnlListNextNode, tTMO_SLL_NODE *)
    {
        if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
        {
            pRsvpTeTnlNextInfo = RPTE_OFFSET_DN_STR_HTBL (pTnlListNextNode);
        }
        else
        {
            pRsvpTeTnlNextInfo = RPTE_OFFSET_UP_STR_HTBL (pTnlListNextNode);
        }

        /* Insert the node in between the nodes with the Previous node having
         * peak data rate less and the next node having the peak data rate less
         * than or equal to the peak data rate of the tunnel which is to be
         * added to the Priority list.
         */

        if (RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfoInput) <
            RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlNextInfo))
        {

            /* This condition arises when there is only one node in the list */
            if (pTnlListPrevNode == NULL)
            {
                /* This condition arises when there is only one node in the list
                 * and the node is added as the first one in the prio list 
                 */
                TMO_SLL_Insert_In_Middle (pRsvpTePrioList,
                                          &(pRsvpTePrioList->Head),
                                          pTnlListCurNode, pTnlListNextNode);

                RSVPTE_DBG (RSVPTE_PM_ETEXT,
                            "RptePmAddTnlToPrioList : EXIT \n");
                return;
            }
            /* When there are more than one nodes in the priority list */
            TMO_SLL_Insert_In_Middle (pRsvpTePrioList,
                                      pTnlListPrevNode,
                                      pTnlListCurNode, pTnlListNextNode);
            RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmAddTnlToPrioList : EXIT \n");
            return;
        }

        /* Store the pointer of the next node and the structure pointer
         * as previous node pointer and previous structure pointer.
         */
        pTnlListPrevNode = pTnlListNextNode;
    }                            /* End of "TMO_SLL_Scan" */

    /* If the SCAN is exhausted then it indicates that the node to be added 
     * has peak data rate lower than all the nodes in the priority list. 
     * Hence It will be just appended to the list.
     */

    TMO_SLL_Add (pRsvpTePrioList, pTnlListCurNode);
    RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmAddTnlToPrioList : EXIT \n");
    return;
}

/******************************************************************************
 * Function Name : RptePmRemTnlFromPrioList                                   *
 * Description   : This routine is called when a RSVP tunnel is to be removed *
 *                 from the priority list                                     *
 * Input(s)      : pIfEntry - Pointer to Interface Entry from which the       *
 *                 tunnel is to be deleted                                    *
 *               : u1Priority - Priority of the tunnel                        *
 *               : pRsvpTeTnlInfoInput - Pointer to tunnel info structure     *
 *                 of the tunnel to be added                                  *
 * Output(s)     : None                                                       *
 ******************************************************************************/
VOID
RptePmRemTnlFromPrioList (tIfEntry * pIfEntry,
                          UINT1 u1Priority, UINT1 u1SegDirection,
                          tRsvpTeTnlInfo * pRsvpTeTnlInfoInput)
{
    tTMO_SLL_NODE      *pTnlListCurNode = NULL;
    tTMO_SLL           *pRsvpTePrioList = NULL;

    RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmRemTnlFromPrioList : ENTRY \n");

    /* Getting the priority list and node to be deleted from that list 
     * based on direction passed. */
    if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
    {
        pTnlListCurNode =
            RSVP_GET_TNL_LIST_HOLD_DN_STR_NODE (pRsvpTeTnlInfoInput);
        pRsvpTePrioList = &(pIfEntry->aRpteDnStrPrioTnlList[u1Priority]);
    }
    else
    {
        pTnlListCurNode =
            RSVP_GET_TNL_LIST_HOLD_UP_STR_NODE (pRsvpTeTnlInfoInput);
        pRsvpTePrioList = &(pIfEntry->aRpteUpStrPrioTnlList[u1Priority]);
    }

    TMO_SLL_Delete (pRsvpTePrioList, pTnlListCurNode);

    RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmRemTnlFromPrioList : EXIT \n");
    return;
}

/******************************************************************************
 * Function Name : RptePmModifyTnlPrioList                                    *
 * Description   : This routine modifies the  priority of a RSVP tunnel in    *
 * Input(s)      : pIfEntry - Pointer to current Interface Entry              *
 *               : u1CurrPriority - refers to tunnels current priority        *
 *               : u1NewPriority - refers the tunnels new priority            *
 *               : pRsvpTeTnlInfoInput- Pointer to tunnel info structure of   *
 *                 the tunnel to be added                                     *
 * Output(s)     : None                                                       *
 *****************************************************************************/
VOID
RptePmModifyTnlPrioList (tIfEntry * pIfEntry,
                         UINT1 u1CurrPriority,
                         UINT1 u1NewPriority, UINT1 u1SegDirection,
                         tRsvpTeTnlInfo * pRsvpTeTnlInfoInput)
{
    RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmModifyTnlPrioList : ENTRY \n");
    /* The tunnel is moved from one Priority list to the other priority list */

    RptePmRemTnlFromPrioList (pIfEntry, u1CurrPriority, u1SegDirection,
                              pRsvpTeTnlInfoInput);

    RptePmAddTnlToPrioList (pIfEntry, u1NewPriority, u1SegDirection,
                            pRsvpTeTnlInfoInput);
    RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmModifyTnlPrioList : EXIT \n");
    return;
}

/*****************************************************************************
 * Function Name : RptePmGetTnlFromPrioList                                  *
 * Description   : This routine returns the list of the RSVP tunnels which   *
 *               : can be preempted from the  priority list to meet the      *
 * Input(s)      : pIfEntry - Pointer to Interface Entry                     *
 *               : u1Priority - Priority of the tunnel                       *
 *               : pRsvpTeTnlInfoInput - Pointer to tunnel info structure of *
 *                 the tunnel to be added                                    *
 * Output(s)     : pCandidatePreemp - It's the pointer to the list of        *
 *                 candidate tunnels which can be preempted                  *
 ****************************************************************************/
UINT1
RptePmGetTnlFromPrioList (tIfEntry * pIfEntry,
                          UINT1 u1Priority,
                          tRsvpTeTnlInfo * pRsvpTeTnlInfoInput,
                          tTMO_SLL * pCandidatePreemp,UINT1 u1SegDirection)
{
    tTMO_SLL           *pRsvpTeFwdPrioList = NULL;
    tTMO_SLL           *pRsvpTeRevPrioList = NULL;
    tTMO_SLL_NODE      *pFwdPrioTnlInfo = NULL;
    tTMO_SLL_NODE      *pRevPrioTnlInfo = NULL;

    UINT4               u4Diffmin = 0;

    UINT1               u1Flag = RPTE_NO_MATCH;
    UINT1               u1TempPriority = RSVP_TNL_MIN_PRIO;

    /* Keeps track of the peak data rate while summing up */
    UINT4               u4Rate = RSVPTE_ZERO;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTempRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pCandidateTnl = NULL;

    /* List of tunnels which are stacked up */
    tTMO_SLL            StackList;
    tTMO_SLL           *pStackTnlList = NULL;
    RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmGetTnlFromPrioList : ENTRY \n");

    pStackTnlList = &StackList;

    /* Initialise the list */
    TMO_SLL_Init (pStackTnlList);
    TMO_SLL_Init (pCandidatePreemp);

    /* Note: If the priority of the tunnel to be established is 7, then
     * then no tunnels can be preempted.
     */
    if ((u1Priority == RSVP_TNL_MIN_PRIO) ||
        ((RSVPTE_TNL_TRFC_PARAM (RPTE_TE_TNL (pRsvpTeTnlInfoInput))) == NULL))
    {
        RSVPTE_DBG (RSVPTE_PM_PRCS, " The pre-emption is not possible ");
        RSVPTE_DBG (RSVPTE_PM_ETEXT,
                    "RptePmGetTnlFromPrioList : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    /* Best Match Check */
    /* Searches for a single Tunnel having a Peak data Rate greater than
     * or equal to the required Peak Data Rate. The search is started from the
     * lowest priority list. If no tunnel is found in the list, with a peak 
     * data rate greater than the peak data rate of the tunnel which is to be
     * established, then the search is carried on to the priority lists of the
     * subsequent higher priorities.
     */

    /* Start of For loop */
    for (u1TempPriority = RSVP_TNL_MIN_PRIO;
         u1TempPriority > u1Priority; u1TempPriority--)
    {
        pRsvpTeFwdPrioList = &(pIfEntry->aRpteDnStrPrioTnlList[u1TempPriority]);
        pRsvpTeRevPrioList = &(pIfEntry->aRpteUpStrPrioTnlList[u1TempPriority]);

        /* A scan of the list is done in order to search for the best 
         * match and if the tunnel is stacked up then it is not considered
         * for the best match search and that tunnel is added to the list
         * of tunnels which are stacked up
         */
        RPTE_Double_SLL_Scan (pRsvpTeFwdPrioList, pRsvpTeRevPrioList,
                              pFwdPrioTnlInfo, pRevPrioTnlInfo, tTMO_SLL_NODE *)
        {
            if (pFwdPrioTnlInfo != NULL)
            {
                pRsvpTeTnlInfo = RPTE_OFFSET_DN_STR_HTBL (pFwdPrioTnlInfo);

                if (RptePmGetBestMatchTnl (pRsvpTeTnlInfo, pRsvpTeTnlInfoInput,
                                           pStackTnlList, &pTempRsvpTeTnlInfo,
                                           &u1Flag, &u4Diffmin,
                                           GMPLS_SEGMENT_FORWARD,u1SegDirection) ==
                    RPTE_SUCCESS)
                {
                    if (RptePmAddCandidatePreempList (u1Flag, pCandidatePreemp,
                                                      pRsvpTeTnlInfo,
                                                      pTempRsvpTeTnlInfo,
                                                      &pCandidateTnl) ==
                        RPTE_SUCCESS)
                    {
                        return RPTE_SUCCESS;
                    }
                }
            }

            if (pRevPrioTnlInfo != NULL)
            {
                pRsvpTeTnlInfo = RPTE_OFFSET_UP_STR_HTBL (pRevPrioTnlInfo);

                if (RptePmGetBestMatchTnl (pRsvpTeTnlInfo, pRsvpTeTnlInfoInput,
                                           pStackTnlList, &pTempRsvpTeTnlInfo,
                                           &u1Flag, &u4Diffmin,
                                           GMPLS_SEGMENT_REVERSE,u1SegDirection) ==
                    RPTE_SUCCESS)
                {
                    if (RptePmAddCandidatePreempList (u1Flag, pCandidatePreemp,
                                                      pRsvpTeTnlInfo,
                                                      pTempRsvpTeTnlInfo,
                                                      &pCandidateTnl) ==
                        RPTE_SUCCESS)
                    {
                        return RPTE_SUCCESS;
                    }

                }
            }
        }                        /* End of Sll_Scan */

        /* Traverse the list with the next higher priority if the flag
         * is false which denotes that there is no tunnel which has peak
         * data rate greater than the tunnel which is to be established.
         */
        if (u1Flag == RPTE_BEST_MATCH)
        {
            while (TMO_SLL_Get (pStackTnlList) != NULL)
                continue;

            TMO_SLL_Add (pCandidatePreemp, &pCandidateTnl->NextPreemptTnl);

            RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmGetTnlFromPrioList : EXIT \n");
            return RPTE_SUCCESS;
        }
    }                            /* End of For Loop */

    u1TempPriority = RSVP_TNL_MIN_PRIO;

    /* If the best match is not found that means that there is no tunnel in the
     * priority list with the peak data rate greater or equal to the peak data
     * rate of the tunnel which is to be established.
     */
    while ((u4Rate < RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfoInput)) &&
           (u1TempPriority > u1Priority))
    {
        pRsvpTeFwdPrioList = &(pIfEntry->aRpteDnStrPrioTnlList[u1TempPriority]);
        pRsvpTeRevPrioList = &(pIfEntry->aRpteUpStrPrioTnlList[u1TempPriority]);

        /* Start of sll scan */
        RPTE_Double_SLL_Scan (pRsvpTeFwdPrioList, pRsvpTeRevPrioList,
                              pFwdPrioTnlInfo, pRevPrioTnlInfo, tTMO_SLL_NODE *)
        {
            if (pFwdPrioTnlInfo != NULL)
            {
                pRsvpTeTnlInfo = RPTE_OFFSET_DN_STR_HTBL (pFwdPrioTnlInfo);

                if (RptePmAddTnlsToBePreempted (pRsvpTeTnlInfo,
                                                pRsvpTeTnlInfoInput,
                                                pStackTnlList, pCandidatePreemp,
                                                &u4Rate, GMPLS_SEGMENT_FORWARD,u1SegDirection)
                    == RPTE_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_PM_ETEXT,
                                "RptePmGetTnlFromPrioList : EXIT \n");
                    return RPTE_SUCCESS;
                }
            }

            if (pRevPrioTnlInfo != NULL)
            {
                pRsvpTeTnlInfo = RPTE_OFFSET_UP_STR_HTBL (pRevPrioTnlInfo);

                if (RptePmAddTnlsToBePreempted (pRsvpTeTnlInfo,
                                                pRsvpTeTnlInfoInput,
                                                pStackTnlList, pCandidatePreemp,
                                                &u4Rate, GMPLS_SEGMENT_REVERSE,u1SegDirection)
                    == RPTE_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_PM_ETEXT,
                                "RptePmGetTnlFromPrioList : EXIT \n");
                    return RPTE_SUCCESS;
                }
            }
        }                        /* End of sll scan */
        u1TempPriority--;
    }                            /* End of While loop */

    /* The requirement of the peak data rate is not satisfied by
     * the tunnels which are not stacked up, then the list of stacked
     * up tunnels which was obtained while searching for the best match
     * is traversed to meet the requirements of the peak data rate
     */
    pFwdPrioTnlInfo = TMO_SLL_First (pStackTnlList);
    while (pFwdPrioTnlInfo != NULL)
    {

        TMO_SLL_Delete (pStackTnlList, pFwdPrioTnlInfo);
        pRsvpTeTnlInfo =
            ((tRsvpTeTnlInfo *) (VOID *) ((UINT1 *) pFwdPrioTnlInfo -
                                          RPTE_OFFSET (tRsvpTeTnlInfo,
                                                       NextStackTnl)));
        u4Rate = u4Rate + RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfo);
        /* Add to the pCandidatePreemp list */
        TMO_SLL_Add (pCandidatePreemp,
                     (tTMO_SLL_NODE *) & ((pRsvpTeTnlInfo)->NextPreemptTnl));
        if (u4Rate >= RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfoInput))
        {
            while (TMO_SLL_Get (pStackTnlList) != NULL)
                continue;
            RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmGetTnlFromPrioList : EXIT \n");
            return RPTE_SUCCESS;
        }
        pFwdPrioTnlInfo = TMO_SLL_First (pStackTnlList);
    }                            /*  End Of While */

    /* If the candidate preempt list is not equal to null, 
     * then the tunnels in the candidate preempt list can
     * be prempted. */

    if (TMO_SLL_Count (pCandidatePreemp) != RPTE_ZERO)
    {
        return RPTE_SUCCESS;
    }

    pFwdPrioTnlInfo = TMO_SLL_First (pCandidatePreemp);
    while (pFwdPrioTnlInfo != NULL)
    {
        TMO_SLL_Delete (pCandidatePreemp, pFwdPrioTnlInfo);
        pFwdPrioTnlInfo = TMO_SLL_First (pCandidatePreemp);
    }
    RSVPTE_DBG (RSVPTE_PM_PRCS,
                " The tunnels Availble can't meet the Res Required. \n");
    RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmGetTnlFromPrioList : INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/******************************************************************************
 * Function Name : RptePmRemoveTnlFromPrioList                                *
 * Description   : This routine is called when a RSVP tunnel is to be removed *
 *                 from the priority list for both Forward and Reverse        *
 *                 direction                                                  *
 * Input(s)      : pRsvpTeTnlInfo - Pointer to tunnel info structure          *
 *                 of the tunnel to be added                                  *
 * Output(s)     : None                                                       *
 ******************************************************************************/

VOID
RptePmRemoveTnlFromPrioList (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    if (pRsvpTeTnlInfo->pRsb != NULL)
    {
        RptePmRemTnlFromPrioList (RSB_OUT_IF_ENTRY
                                  (RSVPTE_TNL_RSB (pRsvpTeTnlInfo)),
                                  RSVPTE_TNL_HOLD_PRIO (RPTE_TE_TNL
                                                        (pRsvpTeTnlInfo)),
                                  GMPLS_SEGMENT_FORWARD, pRsvpTeTnlInfo);
    }
    if ((pRsvpTeTnlInfo->pTeTnlInfo->u4TnlMode == GMPLS_BIDIRECTION) &&
        (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) != NULL) && (pRsvpTeTnlInfo->pRsb != NULL))
    {
        RptePmRemTnlFromPrioList (PSB_IF_ENTRY
                                  (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                                  RSVPTE_TNL_HOLD_PRIO (RPTE_TE_TNL
                                                        (pRsvpTeTnlInfo)),
                                  GMPLS_SEGMENT_REVERSE, pRsvpTeTnlInfo);
    }
    return;
}

/******************************************************************************
 * Function Name : RptePmGetBestMatchTnl                                      *
 * Description   : This routine is used to find the best match tunnel from    *
 *                 priority list for preemption                               *
 * Input(s)      : pRsvpTeTnlInfo - Pointer to tunnel info structure          *
 *                 pRsvpTeTnlInfoInput - Pointer to tunnel info structure     *
 *                 pStackTnlList       - Stack tunnel list                    *
 *                 pu1Flag             - flag value                           *
 *                 pu4Diffmin          - min value                            *
 *                 of the tunnel to be added                                  *
 *                 u1SegDirection      - Segment direction
 * Return        : pTempRsvpTeTnlInfo  - pointer to tunnel info structure      *
 * Output(s)     : None                                                       *
 ******************************************************************************/
UINT1
RptePmGetBestMatchTnl (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                       tRsvpTeTnlInfo * pRsvpTeTnlInfoInput,
                       tTMO_SLL * pStackTnlList,
                       tRsvpTeTnlInfo ** pTempRsvpTeTnlInfo, UINT1 *pu1Flag,
                       UINT4 *pu4Diffmin, UINT1 u1SegDirection,UINT1 u1PreemptSegDirection)
{
    UINT4               u4DiffTrack = RPTE_ZERO;

    if (RptePmIsTnlEligibleForPreemption (pRsvpTeTnlInfo, pRsvpTeTnlInfoInput,
                                          u1SegDirection,u1PreemptSegDirection) == RPTE_FALSE)
    {
        return RPTE_FAILURE;
    }

    if (RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfo) <
        RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfoInput))
    {
        return RPTE_FAILURE;
    }

    /* Get the difference in the peak data rate */
    u4DiffTrack =
        RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfo) -
        RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfoInput);

    /* If the peak data rate perfectly matches the peak data
     * rate of the tunnel in contention , the tunnel is added
     * to the candidate Preemption list .
     */
    if (u4DiffTrack == RSVPTE_ZERO)
    {

        /* The nodes of the stack list are deleted because the
         * best match has been found and there is no need of the
         * stack list
         */
        while (TMO_SLL_Get (pStackTnlList) != NULL)
            continue;

        *pu1Flag = RPTE_EXACT_MATCH;
        return RPTE_SUCCESS;
    }

    /* Here to find the least difference: the difference in the
     * Peak data rate is tracked for every unstacked tunnel under
     * the current priority
     */
    if (u4DiffTrack > RSVPTE_ZERO)
    {
        if (*pu1Flag == RPTE_NO_MATCH)
        {
            *pu4Diffmin = u4DiffTrack;
            *pTempRsvpTeTnlInfo = pRsvpTeTnlInfo;
            *pu1Flag = RPTE_BEST_MATCH;
        }
        else
        {
            if ((*pu1Flag == RPTE_BEST_MATCH) && (u4DiffTrack < *pu4Diffmin))
            {
                *pu4Diffmin = u4DiffTrack;
                *pTempRsvpTeTnlInfo = pRsvpTeTnlInfo;
            }
        }
    }

    return RPTE_SUCCESS;
}

/******************************************************************************
 * Function Name : RptePmAddTnlsToBePreempted                                 *
 * Description   : This routine is used to find the tunnels to be preempted   *
 * Input(s)      : pRsvpTeTnlInfo      - Pointer to tunnel info structure     *
 *                 pRsvpTeTnlInfoInput - Pointer to tunnel info structure     *
 *                 pStackTnlList       - Stack tunnel list                    *
 *                 pCandidatePreemp    - Preemption list                      *
 *                 pu4Rate             - Data Rate vale                       *
 *                 of the tunnel to be added                                  *
 *                 u1SegDirection      - Segment direction
 * Output(s)     : None                                                       *
 ******************************************************************************/
UINT1
RptePmAddTnlsToBePreempted (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                            tRsvpTeTnlInfo * pRsvpTeTnlInfoInput,
                            tTMO_SLL * pStackTnlList,
                            tTMO_SLL * pCandidatePreemp, UINT4 *pu4Rate,
                            UINT1 u1SegDirection,UINT1 u1PreemptSegDirection)
{
    if (RptePmIsTnlEligibleForPreemption (pRsvpTeTnlInfo, pRsvpTeTnlInfoInput,
                                          u1SegDirection,u1PreemptSegDirection) == RPTE_FALSE)
    {
        return RPTE_FAILURE;
    }

    *pu4Rate = *pu4Rate + RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfo);

    TMO_SLL_Add (pCandidatePreemp, &pRsvpTeTnlInfo->NextPreemptTnl);

    if (*pu4Rate >= RSVPTE_TNLRSRC_PEAK_DATA_RATE (pRsvpTeTnlInfoInput))
    {
        while (TMO_SLL_Get (pStackTnlList) != NULL)
            continue;

        RSVPTE_DBG (RSVPTE_PM_ETEXT, "RptePmGetTnlFromPrioList : EXIT \n");
        return RPTE_SUCCESS;
    }

    return RPTE_FAILURE;
}

/******************************************************************************
 * Function Name : RptePmAddTnlsToBePreempted                                 *
 * Description   : This routine is used to check whether the tunnel is        *
 *                 eligible for preemption                                    *
 * Input(s)      : pRsvpTeTnlInfo      - Pointer to tunnel info structure     *
 *                 pRsvpTeTnlInfoInput - Pointer to tunnel info structure     *
 *                 u1SegDirection      - Segment direction
 * Output(s)     : None                                                       *
 ******************************************************************************/
UINT1
RptePmIsTnlEligibleForPreemption (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                  tRsvpTeTnlInfo * pRsvpTeTnlInfoInput,
                                  UINT1 u1SegDirection,UINT1 u1PreemptSegDirection)
{
    if (u1SegDirection == GMPLS_SEGMENT_FORWARD)
    {

		if (u1PreemptSegDirection == GMPLS_SEGMENT_REVERSE && pRsvpTeTnlInfoInput->pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL )
		{
			if((pRsvpTeTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf != RPTE_ZERO) &&
            (pRsvpTeTnlInfoInput->pTeTnlInfo->u4UpStrDataTeLinkIf != RPTE_ZERO) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf !=
             pRsvpTeTnlInfoInput->pTeTnlInfo->u4UpStrDataTeLinkIf))
			{
    			RSVPTE_DBG (RSVPTE_PM_ETEXT, "Match for Co-Routed Bidirectional Tunnel For Reverse Segment is not Satisfying \n");
				return RPTE_FALSE;
			}
	
		}
        else
		{
			 if((pRsvpTeTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf != RPTE_ZERO) &&
            (pRsvpTeTnlInfoInput->pTeTnlInfo->u4DnStrDataTeLinkIf != RPTE_ZERO) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u4DnStrDataTeLinkIf !=
             pRsvpTeTnlInfoInput->pTeTnlInfo->u4DnStrDataTeLinkIf))
        	{
            return RPTE_FALSE;
        	}	
    	}
	}	
    else
    {
        if ((pRsvpTeTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf != RPTE_ZERO) &&
            (pRsvpTeTnlInfoInput->pTeTnlInfo->u4UpStrDataTeLinkIf != RPTE_ZERO) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u4UpStrDataTeLinkIf !=
             pRsvpTeTnlInfoInput->pTeTnlInfo->u4UpStrDataTeLinkIf))
        {
            return RPTE_FALSE;
        }
    }

    if (RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pRsvpTeTnlInfoInput) >
        RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pRsvpTeTnlInfo))
    {
        return RPTE_FALSE;
    }

    if (TMO_SLL_Count ((tTMO_SLL *) & (pRsvpTeTnlInfo->StackTnlList))
        != RSVPTE_ZERO)
    {
        return RPTE_FALSE;
    }

    return RPTE_TRUE;
}

/******************************************************************************
 * Function Name : RptePmAddCandidatePreempList                               *
 * Description   : This routine is used to add the tunnels to be preempted in *
 * candiadated preempt list.                                                  *
 * Input(s)      : u1Flag              - flag value                           *
 *                 pCandidatePreemp    - candidate preempt tunnel list        *
 *                 pRsvpTeTnlInfo      - Pointer to RSVP-TE tunnel info       *
 * Output(s)     : pCandidateTnl       - Pointer to RSVP-TE tunnel info       *
 ******************************************************************************/
UINT1
RptePmAddCandidatePreempList (UINT1 u1Flag, tTMO_SLL * pCandidatePreemp,
                              tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                              tRsvpTeTnlInfo * pTempRsvpTeTnlInfo,
                              tRsvpTeTnlInfo ** pCandidateTnl)
{
    if (u1Flag == RPTE_EXACT_MATCH)
    {
        TMO_SLL_Add (pCandidatePreemp, &pRsvpTeTnlInfo->NextPreemptTnl);
        return RPTE_SUCCESS;
    }
    else if (u1Flag == RPTE_BEST_MATCH)
    {
        *pCandidateTnl = pTempRsvpTeTnlInfo;
    }
    return RPTE_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*                       End of file rptepm.c                                */
/*---------------------------------------------------------------------------*/
