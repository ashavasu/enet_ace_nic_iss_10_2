/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptefsip.c,v 1.23 2017/12/22 09:42:23 siva Exp $
 *
 ********************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 * FILE NAME             : rptefsip.c                                          
 * PRINCIPAL AUTHOR      : Aricent Inc.                      
 * SUBSYSTEM NAME        : MPLS                                                
 * MODULE NAME           : RSVP-TE                                              
 * LANGUAGE              : ANSI-C                                               
 * TARGET ENVIRONMENT    : Linux (Portable)                                    
 * DATE OF FIRST RELEASE :                                                     
 * DESCRIPTION           : This file contains the functions which are used     
 *                         to interface with IP, UDP and Routing Protocols     
 *--------------------------------------------------------------------------*/
#include "rpteincs.h"
#include "rtm.h"
#ifdef TLM_WANTED
#include "../../../tlm/inc/fstlmlw.h"
#include "../../../tlm/inc/stdtlmlw.h"
#endif

/*****************************************************************************/
/* Function Name : RpteIsLsrPartOfIpv4ErHop                                  */
/* Description   : This routine is called to check whether lsr is part of    */
/*                 Ipv4ErHop                                                 */
/* Input(s)      : u4ErHopAddr  - ErHop Address                              */
/*                 u4ErHopMask  - ErHop Address Mask                         */
/* Output(s)     : None                                                      */
/* Return(s)     : RPTE_TRUE/RPTE_FALSE.                                     */
/*****************************************************************************/
UINT1
RpteIsLsrPartOfIpv4ErHop (UINT4 u4ErHopAddr, UINT4 u4ErHopMask)
{
    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLsrPartOfIpv4ErHop : ENTRY \n");

    if (u4ErHopMask == RPTE_HOST_NETMASK)
    {
        if (NetIpv4IfIsOurAddress (u4ErHopAddr) == NETIPV4_SUCCESS)
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLsrPartOfIpv4ErHop : EXIT \n");
            return RPTE_TRUE;
        }
    }
    else
    {
        if (NetIpv4IpIsLocalNet (u4ErHopAddr) == NETIPV4_SUCCESS)
        {
            RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLsrPartOfIpv4ErHop : EXIT \n");
            return RPTE_TRUE;
        }
    }

    RSVPTE_DBG (RSVPTE_PH_ETEXT, "RpteIsLsrPartOfIpv4ErHop : INTMD-EXIT \n");
    return RPTE_FALSE;
}

/*****************************************************************************/
/* Function     : RpteIpIfSendPkt                                            */
/* Description  : This procedure provides an interface with the IP module.   */
/*                It transfers the specified packet to IP.                   */
/* Input        : pMsgBuf    - Message Buffer to be sent.                    */
/*                pPktMap    - Pointer to the tPktMap.                       */
/* Returns      : RPTE_SUCCESS, if packet is successfully transferred        */
/*                RPTE_FAILURE, otherwise                                    */
/*****************************************************************************/
PUBLIC INT4
RpteIpIfSendPkt (tMsgBuf * pMsgBuf, tPktMap * pPktMap)
{
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpIfSendPkt : ENTRY \n");

    RSVPTE_DBG (RSVPTE_IPIF_IF, " IPIF : IF ipif_get_tab_index: CALLED \n");
    if (RSVPTE_SOCK_SPRTD (gpRsvpTeGblInfo) == RPTE_TRUE)
    {
        RSVPTE_DBG (RSVPTE_IPIF_IF, " IPIF : RpteSendViaSocket : CALLED \n");
        if (RpteSendViaSocket (pMsgBuf, pPktMap) == RPTE_FAILURE)
        {
            RSVPTE_DBG (RSVPTE_IPIF_PRCS, " IPIF : RpteSendViaSocket Failed\n");
            return (RPTE_FAILURE);
        }
    }
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpIfSendPkt : EXIT \n");
    return (RPTE_SUCCESS);
}

/****************************************************************************/
/* Function Name   : RpteIpGetIfAddr                                        */
/* Description     : This function gets the address of a specific interface */
/* Input (s)       : i4IfIndex - Interface Number                           */
/*                   pu4Addr - Pointer to Interface Address                 */
/* Output (s)      : The Interface address is returned in pu4Addr           */
/* Returns         : If successful returns RPTE_SUCCESS.                    */
/*                   Else returns RPTE_FAILURE                              */
/****************************************************************************/
INT1
RpteIpGetIfAddr (INT4 i4IfIndex, UINT4 *pu4Addr)
{
    tNetIpv4IfInfo      NetIpv4IfInfo;
    UINT4               u4Port = 0;
    UINT4               u4L3Intf = 0;

#ifndef CFA_WANTED
    UNUSED_PARAM (i4IfIndex);
#endif
    MEMSET (&NetIpv4IfInfo, 0, sizeof (tNetIpv4IfInfo));
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpGetIfAddr : ENTRY \n");
    RSVPTE_DBG (RSVPTE_IPIF_IF, " IPIF : IF ipif_addr_of_iface: CALLED \n");
#ifdef CFA_WANTED
    if (CfaUtilGetL3IfFromMplsIf ((UINT4) i4IfIndex, &u4L3Intf, TRUE) ==
        CFA_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    " RpteIpGetIfAddr : L3 intf get failed EXIT \n");
        return SNMP_FAILURE;
    }
#endif
    if (NetIpv4GetPortFromIfIndex (u4L3Intf, &u4Port) == NETIPV4_SUCCESS)
    {
        if ((NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo)) == NETIPV4_SUCCESS)
        {
            *pu4Addr = NetIpv4IfInfo.u4Addr;
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpGetIfAddr : EXIT \n");
            return RPTE_SUCCESS;
        }
    }

    RSVPTE_DBG (RSVPTE_IPIF_PRCS, "Failed in Getting Address\n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteIpGetIfId                                          */
/* Description     : This function fills the interface Id of the interface  */
/*                   i4IfIndex                                              */
/* Input (s)       : i4IfIndex - Interface Number                           */
/*                   pIfId - Pointer to IfId                                */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteIpGetIfId (INT4 i4IfIndex, tCRU_INTERFACE * pIfId)
{
    UINT4               u4Port = 0;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    MEMSET (&NetIpv4IfInfo, 0, sizeof (tNetIpv4IfInfo));
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpGetIfId : ENTRY \n");
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port) ==
        NETIPV4_SUCCESS)
    {
        if ((NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo)) == NETIPV4_SUCCESS)
        {
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                        " Success in RpteIpGetIfId : EXIT \n");
            pIfId->u4IfIndex = (UINT4) i4IfIndex;    /* Revisit TODO */
            pIfId->u1_InterfaceType = (UINT1) NetIpv4IfInfo.u4IfType;
        }
    }
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpGetIfId : EXIT \n");
}

/****************************************************************************/
/* Function Name   : RpteIpGetIfTtl                                         */
/* Description     : This function fills the interface TTL of the interface */
/*                   i4IfIndex                                              */
/* Input (s)       : i4IfIndex - Interface Number                           */
/* Output (s)      : pu1Ttl - Pointer to Ttl                                */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteIpGetIfTtl (tIfEntry * pIfEntry, UINT1 *pu1Ttl)
{
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpGetIfTtl : ENTRY \n");
    *pu1Ttl = IF_ENTRY_TTL (pIfEntry);
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpGetIfTtl : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name : RpteIpLocalAddress                                       */
/* Description   : This function verifies whether the address matches       */
/*                 any of the local interface adresses                      */
/* Input (s)      :  u4Addr - Address to verify                             */
/* Output (s)     : None                                                    */
/* Returns        : RPTE_SUCCESS or RPTE_FAILURE                            */
/****************************************************************************/
UINT1
RpteIpLocalAddress (UINT4 u4Addr)
{
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpLocalAddress : ENTRY \n");
    if (NetIpv4IfIsOurAddress (u4Addr) == NETIPV4_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpLocalAddress : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpLocalAddress : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteIpGetIfMask                                        */
/* Description     : This function returns the if mask for a particular     */
/*                   IfIndex                                                */
/* Input (s)       : u4IfIndex - Interface Number                           */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_FAILURE or The If-NetMask                         */
/****************************************************************************/
UINT4
RpteIpGetIfMask (UINT4 u4IfIndex)
{
    UINT4               u4Port = 0;
    tNetIpv4IfInfo      NetIpv4IfInfo;
    MEMSET (&NetIpv4IfInfo, 0, sizeof (tNetIpv4IfInfo));
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpLocalAddress : ENTRY \n");
    if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port) == NETIPV4_SUCCESS)
    {
        if (NetIpv4GetIfInfo (u4Port, &NetIpv4IfInfo) == NETIPV4_SUCCESS)
        {

            return (NetIpv4IfInfo.u4NetMask);
        }
    }
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpLocalAddress : INTMD-EXIT \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteHandleRtChgNotification                            */
/* Description     : This function handles Route Change notification        */
/* Input (s)       : pRtInfo - pointer to Rt Info                           */
/*                   u4BitMap - Bitmap                                      */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteHandleRtChgNotification (tNetIpv4RtInfo * pNetIpv4RtInfo, UINT1 u1CmdType)
{
    tRsvpteRtEntryInfo *pRouteInfo = NULL;

    if ((RSVP_INITIALISED != TRUE) ||
        (RSVPTE_ADMIN_STATUS (gpRsvpTeGblInfo) != RPTE_ADMIN_UP))
    {
        return;
    }
    if (pNetIpv4RtInfo != NULL)
    {
        pRouteInfo = (tRsvpteRtEntryInfo *)
            RSVP_ALLOCATE_MEM_BLOCK (RPTE_IP_RT_POOL_ID);

        if (pRouteInfo == NULL)
        {
            RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                        "IF: Memory Allocation for Ip Route Info failed \n");
            return;
        }

        /* Copying information to Ldp's strucure */
        MPLS_IPV4_U4_ADDR (pRouteInfo->DestAddr) = pNetIpv4RtInfo->u4DestNet;
        MPLS_IPV4_U4_ADDR (pRouteInfo->DestMask) = pNetIpv4RtInfo->u4DestMask;
        MPLS_IPV4_U4_ADDR (pRouteInfo->NextHop) = pNetIpv4RtInfo->u4NextHop;
        pRouteInfo->u4RtIfIndx = pNetIpv4RtInfo->u4RtIfIndx;
        pRouteInfo->u4RtNxtHopAS = pNetIpv4RtInfo->u4RtNxtHopAs;
        pRouteInfo->i4Metric1 = pNetIpv4RtInfo->i4Metric1;
        pRouteInfo->u1RowStatus = (UINT1) pNetIpv4RtInfo->u4RowStatus;
        pRouteInfo->u1CmdType = u1CmdType;
    }
    else
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    " IF: No Route Information passed by IP : INTMD-EXIT \n");
        return;
    }

    if (OsixQueSend (RSVP_GBL_RT_CHG_NTF_QID,
                     (UINT1 *) (&pRouteInfo), OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)

    {
        MemReleaseMemBlock (RPTE_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
        RSVPTE_DBG (RSVPTE_IPIF_PRCS,
                    "Sent to Q failed in RpteHandleRtChgNotification.\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    " RpteHandleRtChgNotification : INTMD-EXIT \n");
        return;
    }

    if (OsixEvtSend (RSVP_TSK_ID, ROUTE_CHANGE_NOTIFICATION) != OSIX_SUCCESS)
    {
        RSVPTE_DBG (RSVPTE_IPIF_PRCS,
                    "Event send  to RSVP Main Task failed in "
                    "RpteHandleRtChgNotification.\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    " RpteHandleRtChgNotification : INTMD-EXIT \n");
        return;
    }
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteHandleRtChgNotification : EXIT \n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteIpUcastRouteQuery                                  */
/* Description     : This function interfaces with Unicast routing protocol */
/*                   and gets the route to u4Addr                           */
/* Input (s)       : u4Addr - Destination Address                           */
/* Output (s)      : ppOutIfEntry - Pointer to Out If Entry                 */
/*                   pu4OutGw - Pointer to the next hop address             */
/* Returns         : RPTE_SUCCESS in case of existence of a route, else     */
/*                   RPTE_FAILURE                                           */
/****************************************************************************/
UINT1
RpteIpUcastRouteQuery (UINT4 u4Addr, tIfEntry ** ppOutIfEntry, UINT4 *pu4OutGw)
{
    UINT1               u1RetVal = RPTE_FAILURE;
    UINT4               u4L3IfIndex;
    UINT4               u4MplsIfIndex;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    RtQuery.u4DestinationIpAddress = u4Addr;
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpUcastRouteQuery : ENTRY \n");
    RSVPTE_DBG (RSVPTE_IPIF_IF, " IPIF : IF IpGetRoute: CALLED \n");
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)

    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "-E- : IpGetRoute Failed\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    " RpteIpUcastRouteQuery : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    *pu4OutGw = (NetIpRtInfo.u4NextHop == 0) ? u4Addr : NetIpRtInfo.u4NextHop;
    /* If the route learnt is a local route, then the outgoing gateway
       (i.e. the next hop address) equals the network address for which
       the route lookup is done. */
    /* local route is not specfied in the new route query */
    if ((RpteIpLocalAddress (*pu4OutGw)) == RPTE_SUCCESS)
    {
        *pu4OutGw = u4Addr;
    }
    /* TE-LINK interface has to be taken if exists */
    if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                      &u4L3IfIndex) == NETIPV4_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "-E- :Failed to Get If Index from Port.\n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_IPIF_IF, " IPIF : IF IpGetIfIndexFromPort: CALLED \n");

    /* Get the Mpls If index which is stacked over this L3 interface */
    if (CfaUtilGetMplsIfFromIfIndex (u4L3IfIndex,
                                     &u4MplsIfIndex, TRUE) == CFA_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "-E- : Error in getting the Mpls Iface.\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    " RpteIpUcastRouteQuery : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    u1RetVal = RpteGetIfEntry ((INT4) u4MplsIfIndex, ppOutIfEntry);

    if (u1RetVal == RPTE_FAILURE)
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "-E- : Failed to Get If Entry.\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    " RpteIpUcastRouteQuery : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if ((IF_ENTRY_STATUS (*ppOutIfEntry) != ACTIVE) ||
        (IF_ENTRY_ENABLED (*ppOutIfEntry) != RPTE_ENABLED))
    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "-E- : If Entry is not enabled.\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    " RpteIpUcastRouteQuery : INTMD-EXIT \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_IPIF_ETEXT, " RpteIpUcastRouteQuery : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteIpDirectlyConnected                                */
/* Description     : This function checks whether the given addredd is      */
/*                   directly connected or not                              */
/* Input (s)       : u4Addr - Address                                       */
/* Returns         : RPTE_SUCCESS in case of directly connected, else       */
/*                   RPTE_FAILURE                                           */
/****************************************************************************/

UINT1
RpteIpDirectlyConnected (UINT4 u4Addr)
{
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
#ifdef TLM_WANTED
    UINT4               u4TeLinkRemoteRtrId = RPTE_ZERO;
    INT4                i4TeIfIndex = RPTE_ZERO;
#endif

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    RtQuery.u4DestinationIpAddress = u4Addr;
    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    RSVPTE_DBG1 (RSVPTE_IPIF_ETEXT, " RpteIpDirectlyConnected u4Addr: x0%x\n",
                 u4Addr);
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)

    {
        RSVPTE_DBG (RSVPTE_IF_PRCS, "-E- : NetIpv4GetRoute Failed\n");
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                    " RpteIpDirectlyConnected: INTMD-EXIT \n");
        return RPTE_FAILURE;
    }

    if (NetIpRtInfo.u4NextHop == RPTE_ZERO)
    {
        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "It is directly connected\n");
        return RPTE_SUCCESS;
    }
    else
    {
#ifdef TLM_WANTED
        nmhGetFirstIndexTeLinkTable (&i4TeIfIndex);
        do
        {
            if (i4TeIfIndex != RPTE_ZERO)
            {
                nmhGetFsTeLinkRemoteRtrId (i4TeIfIndex, &u4TeLinkRemoteRtrId);
                if (u4TeLinkRemoteRtrId == u4Addr)
                {
                    RSVPTE_DBG (RSVPTE_IPIF_ETEXT,
                                "It is directly connected\n");
                    return RPTE_SUCCESS;
                }
            }
            else
            {
                break;
            }
        }
        while (nmhGetNextIndexTeLinkTable (i4TeIfIndex, &i4TeIfIndex)
               == SNMP_SUCCESS);
#endif

        RSVPTE_DBG (RSVPTE_IPIF_ETEXT, "It is not directly connected\n");
        return RPTE_FAILURE;
    }
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptefsip.c                             */
/*---------------------------------------------------------------------------*/
