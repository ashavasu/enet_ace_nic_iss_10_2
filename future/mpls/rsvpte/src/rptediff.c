/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptediff.c,v 1.23 2016/07/22 09:45:47 siva Exp $
 *
 * Description:
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptediff.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : RSVP-TE 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines to handle 
 *                             Diffserv Objects in RSVP-TE.
 *---------------------------------------------------------------------------*/

#include "rpteincs.h"

/****************************************************************************/
/* Function Name   : RptePvmValidateDSObj                                   */
/* Description     : This function validates the Diffserv object Present in */
/*                   the path message.                                      */
/* Input (s)       : *pPktMap - Pointer to Packet Map                       */
/*                   *pObjHdr - Pointer to Object Header                    */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS or RPTE_FAILURE                           */
/****************************************************************************/
INT1
RptePvmValidateDSObj (tPktMap * pPktMap, tObjHdr * pObjHdr)
{
    tDiffServElspObj   *pDiffServElspObj = NULL;
    tElspTrafficProfileObj *pDiffElspTPObj = NULL;

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RptePvmValidateDSObj : ENTRY \n");

    switch (OSIX_NTOHS (OBJ_HDR_CLASS_NUM_TYPE (pObjHdr)))
    {
        case RPTE_DS_ELSP_OBJ_CLASS_NUM_TYPE:

            PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;

            /*  Check for valid DiffServElsp Obj,if not found return 
             *  not ok. 
             */

            pDiffServElspObj = (tDiffServElspObj *) pObjHdr;
            if (PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ (pPktMap) == NULL)
            {
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    (UINT2)(sizeof (tDiffServElspObj) -
                             ((RPTE_DIFFSERV_MAX_MAP_ENTRIES -
                               pDiffServElspObj->u1NoOfMapEntries) *
                              sizeof (tDiffServMapEntry))))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid DiffServElsp Obj \n");
                    RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                                "RptePvmValidateDSObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ (pPktMap) =
                    (tDiffServElspObj *) pObjHdr;
            }
            break;

        case RPTE_DS_LLSP_OBJ_CLASS_NUM_TYPE:

            PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;

            /* Check for valid DiffServLlsp Obj,if not found return
             * not ok.
             */

            if (PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ (pPktMap) == NULL)
            {
                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    sizeof (tDiffServLlspObj))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid DiffServLlsp Obj \n");
                    RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                                "RptePvmValidateDSObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ (pPktMap) =
                    (tDiffServLlspObj *) pObjHdr;
            }
            break;

        case RPTE_CLASS_TYPE_OBJ_CLASS_NUM_TYPE:
            /*Class Num and C-type is not supported */
            if ((gi4MplsSimulateFailure == RPTE_SIM_FAILURE_UNKNOWN_CNUM) ||
                (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_UNKNOWN_CTYPE))
            {
                PKT_MAP_UNKNOWN_OBJ_HDR (pPktMap) = pObjHdr;
                break;
            }
            /* Check for the duplicate Class type object */

            if (PKT_MAP_DIFFSERV_CLASS_TYPE_OBJ (pPktMap) == NULL)
            {
                PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;

                /* Check for valid DiffServClassType Obj,if not found 
                 * return not ok */

                if (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) !=
                    sizeof (tDiffServClassTypeObj))
                {
                    RSVPTE_DBG (RSVPTE_PVM_PRCS,
                                "Err : Invalid DiffServClassType Obj \n");
                    RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                                "RptePvmValidateDSObj : INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                PKT_MAP_DIFFSERV_CLASS_TYPE_OBJ (pPktMap) =
                    (tDiffServClassTypeObj *) pObjHdr;
            }
            break;
        case RPTE_ELSP_TP_OBJ_CLASS_NUM_TYPE:
            PKT_MAP_RPTE_MSG_FLAG (pPktMap) = RPTE_YES;
            pDiffElspTPObj = (tElspTrafficProfileObj *) (VOID *) pObjHdr;

            /* Check for valid ElspTrafficprofile Obj,if not found return 
             * not ok */

            if ((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr))) !=
                (UINT2) ((RPTE_DIFFSERV_MAX_MAP_ENTRIES +
                          ((pDiffElspTPObj->u1NoOfElspTPEntries) *
                           sizeof (tRpteDiffServElspTPEntry)))))
            {
                RSVPTE_DBG (RSVPTE_PVM_PRCS,
                            "Err : Invalid ElspTrafficProfile Obj \n");
                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            "RptePvmValidateDSObj : INTMD-EXIT \n");
                return RPTE_FAILURE;
            }
            PKT_MAP_DIFFSERV_ELSPTP_OBJ (pPktMap) =
                (tElspTrafficProfileObj *) (VOID *) pObjHdr;
            break;
        default:
            RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                        "RptePvmValidateDSObj : INTMD-EXIT \n");

            return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RptePvmValidateDSObj : EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteValidateDSPathMsgObjs                              */
/* Description     : This function validates the fields of the Diffserv     */
/*                   objects present in the path message.                   */
/* Input (s)       : *pPktMap - Pointer to Packet Map                       */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS or  RPTE_FAILURE                          */
/****************************************************************************/
UINT1
RpteValidateDSPathMsgObjs (tPktMap * pPktMap)
{
    UINT1               u1Dscp = RPTE_ZERO;
    UINT1               u1Bit;
    UINT1               u1MapArrayIndex;
    UINT2               u2ErrVal = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteValidateDSPathMsgObjs: ENTRY \n");

    /* If Elsp Diffserv Obj is present in the path message Exp fields are
     * Validated */

    if (PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ (pPktMap) != NULL)
    {
        if (PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ_MAP_ENTRIES (pPktMap) >
            RPTE_DIFFSERV_MAX_EXP)
        {
            RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                        "RpteValidateDSPathMsgObjs: INTMD_EXIT \n");
            return RPTE_FAILURE;
        }

        for (u1MapArrayIndex = RSVPTE_ZERO; u1MapArrayIndex <
             PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ_MAP_ENTRIES (pPktMap);
             u1MapArrayIndex++)
        {
            if (PKT_MAP_RPTE_DIFFSERV_ELSP_EXP (pPktMap, u1MapArrayIndex)
                >= RPTE_DIFFSERV_MAX_EXP)
            {
                RptePvmSendPathErr (pPktMap,
                                    RPTE_DIFFSERV_ERROR,
                                    RPTE_INVALID_EXPPHB_MAPPING);

                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            "RpteValidateDSPathMsgObjs: INTMD_EXIT \n");
                return RPTE_FAILURE;
            }

            /* Validation of the Phb is done by getting the 14th bit 
               of the Phb,if the 14 Bit is set to one then that Phb 
               is a Valid */

            (RPTE_DS_GET_PHBID_14TH_BIT (OSIX_NTOHS
                                         (PKT_MAP_RPTE_DIFFSERV_ELSP_PHBID
                                          (pPktMap, u1MapArrayIndex)), u1Bit));

            if (u1Bit != RSVPTE_ZERO)
            {
                RptePvmSendPathErr (pPktMap,
                                    RPTE_DIFFSERV_ERROR,
                                    RPTE_INVALID_EXPPHB_MAPPING);

                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            "RpteValidateDSPathMsgObjs: INTMD_EXIT \n");

                return RPTE_FAILURE;
            }

            /* Dscp of a Phb is Found and validation of that Dscp is 
               done to see if it a valid */

            RPTE_DS_DSCP_FOR_PHBID (OSIX_NTOHS (PKT_MAP_RPTE_DIFFSERV_ELSP_PHBID
                                                (pPktMap, u1MapArrayIndex)),
                                    u1Dscp);

            if (RpteDiffServValidateDscp (u1Dscp) == RPTE_FAILURE)
            {
                RptePvmSendPathErr (pPktMap,
                                    RPTE_DIFFSERV_ERROR,
                                    RPTE_INVALID_EXPPHB_MAPPING);

                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            "RpteValidateDSPathMsgObjs: INTMD_EXIT \n");
                return RPTE_FAILURE;
            }
        }
    }

    /* If LLsp DIffserv Object is present PSC Fields are Validated */

    if (PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ (pPktMap) != NULL)
    {
        /* Validation of the PSC is done by getting the 14th bit 
           of the PSC,if the 14 Bit is set to Zero then that PSC 
           is Valid */

        (RPTE_DS_GET_PHBID_14TH_BIT
         (OSIX_NTOHS (PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ_PSC (pPktMap)), u1Bit));

        if (u1Bit != RSVPTE_ONE)
        {
            RptePvmSendPathErr (pPktMap, RPTE_DIFFSERV_ERROR,
                                RPTE_INVALID_EXPPHB_MAPPING);
            RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                        "RpteValidateDSPathMsgObjs: INTMD_EXIT \n");
            return RPTE_FAILURE;
        }
        /* Dscp of a Psc is Found and validation of that Dscp is 
           done to see if it a valid */

        RPTE_DS_DSCP_FOR_PHBID (OSIX_NTOHS (PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ_PSC
                                            (pPktMap)), u1Dscp);

        if (RpteDiffServValidateDscp (u1Dscp) == RPTE_FAILURE)
        {
            RptePvmSendPathErr (pPktMap, RPTE_DIFFSERV_ERROR,
                                RPTE_INVALID_EXPPHB_MAPPING);

            RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                        "RpteValidateDSPathMsgObjs: INTMD_EXIT \n");

            return RPTE_FAILURE;
        }
    }
    /* Class Types are validated in the class type object */

    if (PKT_MAP_DIFFSERV_CLASS_TYPE_OBJ (pPktMap) != NULL)
    {
        if (RpteValidateClassObject (pPktMap, &u2ErrVal) == RPTE_FAILURE)
        {
            RptePvmSendPathErr (pPktMap, RPTE_DIFFSERV_TE_ERROR, u2ErrVal);
            RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                        "RpteValidateDSPathMsgObjs: INTMD_EXIT \n");
            return RPTE_FAILURE;
        }
    }

    /*   Elsp TP Obj is validated while updating the Tnl table 
     *   This is done as every signalling Phb there must be an OA entry */

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteValidateDSPathMsgObjs: EXIT \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteUpdateDSPathObjsSize                               */
/* Description     : This function updates the size of Diffserv objects     */
/*                   (DiffservElsp Object,DiffservLlsp Object,DiffservClass */
/*                   Type Object or DiffservElspTPObject Present in the Path*/
/*                   Message                                                */
/* Input (s)       : *pRsvpTeTnlInfo- Pointer to Tunnel Info                */
/*                 : *pu2PathSize   - Pointer to Path Size                  */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS or  RPTE_FAILURE                          */
/****************************************************************************/
VOID
RpteUpdateDSPathObjsSize (const tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                          UINT2 *pu2PathSize)
{

    UINT1               u1NumberOfEntries = RSVPTE_ZERO;
    UINT4               u4NoOfElspTpEntries = RSVPTE_ZERO;

    tMplsDiffServElspInfo *pTempElspInfo = NULL;

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteUpdateDSPathObjsSize: ENTRY \n");

    if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
    {
        if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) ==
            RPTE_DIFFSERV_ELSP)
        {
            /* If The Elsp is Preconfigured one then Diffserve Object is 
             * not included in the Path Message so the Pathsize is not 
             * changed */

            if (RSVPTE_TE_DS_TNL_ELSP_TYPE (pRsvpTeTnlInfo) ==
                RPTE_DIFFSERV_PRECONF)
            {
                *pu2PathSize = (UINT2)(*pu2PathSize + (sizeof (tObjHdr) + (sizeof (UINT4))));
            }
            else if (RSVPTE_TE_DS_TNL_ELSP_TYPE (pRsvpTeTnlInfo) ==
                     RPTE_DIFFSERV_SIGNALLED)
            {
                if(pRsvpTeTnlInfo->pTeTnlInfo->pMplsDiffServTnlInfo->pMplsDiffServElspList == NULL)
                {
                   return; 
                }
                RPTE_DS_NO_OF_ENTRIES (u1NumberOfEntries, pRsvpTeTnlInfo);
                *pu2PathSize = (UINT2)(*pu2PathSize + (sizeof (tObjHdr)
                                 + (sizeof (UINT4)) + (u1NumberOfEntries *
                                                       sizeof
                                                       (tDiffServMapEntry))));
            }
            /* Update the Pathsize in the Path Message if the Diffserve Lsp 
             * is LLSP */
        }
        else if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)
                 == RPTE_DIFFSERV_LLSP)
        {
            *pu2PathSize = (UINT2)(*pu2PathSize + (sizeof (tObjHdr) + (sizeof (UINT4))));
        }

        /* Update the Pathsize in the Path Message if ClassType is 1,2 or 3 if  
         * Classtype is 0 then the pathsize is not changed */

        if ((RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo) == RSVPTE_ZERO) &&
            (gi4MplsSimulateFailure != RPTE_SIM_FAILURE_CT_ZERO))
        {
            *pu2PathSize += RSVPTE_ZERO;
        }
        else
        {
            *pu2PathSize = (UINT2)(*pu2PathSize + (sizeof (tObjHdr) + (sizeof (UINT4))));
        }
        if (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_MULTI_CT_OBJ)
        {
            *pu2PathSize = (UINT2)(*pu2PathSize + (sizeof (tObjHdr) + (sizeof (UINT4))));
        }

        /* Update the Pathsize in the Path Message for the ELspTP Object 
         * by Counting the NoOfElspTpEntries Present in the ELspTPObject 
         * for that Particular Lsp */

        if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) != RSVPTE_ZERO)
        {
            TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST (pRsvpTeTnlInfo),
                          pTempElspInfo, tMplsDiffServElspInfo *)
            {
                if (RPTE_TE_DS_ELSP_INFO_TRFC_PARAMS (pTempElspInfo) != NULL)
                {
                    u4NoOfElspTpEntries++;
                }
            }
            if (u4NoOfElspTpEntries != RSVPTE_ZERO)
            {
                RPTE_DS_ELSPTP_OBJ_SIZE (u4NoOfElspTpEntries, *pu2PathSize);
            }
        }
        RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteUpdateDSPathObjsSize: EXIT \n");
        return;
    }
}

/****************************************************************************/
/* Function Name   : RpteValidateDSObjs                                     */
/* Description     : This function validates the feilds in                  */
/*                   (DiffservElsp Object,DiffservLlsp Object or            */
/*                   DiffservClass Type Object)Present in the message       */
/* Input (s)       : *pRsvpTeTnlInfo- Pointer to Tunnel Info                */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS or  RPTE_FAILURE                          */
/****************************************************************************/
UINT1
RpteValidateDSObjs (const tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tMplsDiffServElspInfo *pTempElspInfo = NULL;
    UINT1               u1PhbArrayIndex = RSVPTE_ZERO;
    UINT1               u1Dscp = RSVPTE_ZERO;
    UINT4               u4OutIfIndex = RSVPTE_ZERO;
    UINT1               u1PscIndex = RSVPTE_ZERO;
    UINT1               u1ClassIndex = RSVPTE_ZERO;

    if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
    {
        if (MplsApiValDiffservTeParams
            (RPTE_ZERO, RPTE_ZERO, RPTE_ZERO, RPTE_ZERO, MPLS_DSTE_ENABLED_FLAG,
             RSVPTE_ZERO) == RPTE_SUCCESS)
        {
            if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)
                == RPTE_DIFFSERV_ELSP)
            {
                if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) != RSVPTE_ZERO)
                {

                    TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST
                                  (pRsvpTeTnlInfo), pTempElspInfo,
                                  tMplsDiffServElspInfo *)
                    {
                        if (MplsApiValDiffservTeParams
                            (RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo),
                             RPTE_ZERO,
                             RPTE_TE_DS_ELSP_INFO_PHB_DSCP (pTempElspInfo),
                             RPTE_ZERO, MPLS_DSTE_PHB_FLAG,
                             RSVPTE_ZERO) == RPTE_FAILURE)
                        {
                            return RPTE_FAILURE;
                        }
                    }
                }
            }
            else
            {
                u1Dscp = RSVPTE_TE_DS_TNL_LLSP_DSCP (pRsvpTeTnlInfo);
                if (MplsApiValDiffservTeParams
                    (RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo), RPTE_ZERO,
                     RPTE_ZERO, u1Dscp, MPLS_DSTE_PSC_FLAG,
                     RSVPTE_ZERO) == RPTE_FAILURE)
                {
                    return RPTE_FAILURE;
                }

            }

            return RPTE_SUCCESS;
        }

        if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)
            == RPTE_DIFFSERV_ELSP)
        {

            /* If the Elsp is Signalled one then the Diffserv Object is 
             * Validated 
             */

            if (RSVPTE_TE_DS_TNL_ELSP_TYPE (pRsvpTeTnlInfo)
                == RPTE_DIFFSERV_SIGNALLED)
            {

                if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) !=
                    RSVPTE_ZERO)
                {
                    TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST
                                  (pRsvpTeTnlInfo), pTempElspInfo,
                                  tMplsDiffServElspInfo *)
                    {
                        if ((TE_DS_ELSP_INFO_PSC_TRFC_PARAMS (pTempElspInfo)
                             != NULL) &&
                            (TE_DS_ELSPINFO_RSVP_TRFC_PARAMS (pTempElspInfo)
                             == NULL))
                        {
                            RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                                        "RpteValidateDSObjs: -E- No RSVP"
                                        "Trfc params found in ElspInfo\n");
                            return RPTE_FAILURE;
                        }

                        for (u1PhbArrayIndex = RSVPTE_ZERO;
                             u1PhbArrayIndex < RPTE_DS_MAX_NO_OF_PHBS;
                             u1PhbArrayIndex++)
                        {
                            if (RPTE_RM_SUPPORTED_PHBS
                                (gDiffServRMGblInfo, u4OutIfIndex,
                                 u1PhbArrayIndex) ==
                                RPTE_TE_DS_ELSP_INFO_PHB_DSCP (pTempElspInfo))
                            {
                                break;
                            }
                        }
                        if (u1PhbArrayIndex == RPTE_DS_MAX_NO_OF_PHBS)
                        {
                            RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                                        "RpteValidateDSObjs: INTMD_EXIT\n");
                            return RPTE_FAILURE;
                        }

                    }
                }
                else
                {
                    RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                                "RpteValidateDSObjs: DS_ELSP_LIST is NULL .INTMD_EXIT\n");
                    return RPTE_FAILURE;
                }
            }
        }

        /* If the Lsp is LLsp  the Diffserv Object is Validated */

        else if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)
                 == RPTE_DIFFSERV_LLSP)
        {
            u1Dscp = RSVPTE_TE_DS_TNL_LLSP_DSCP (pRsvpTeTnlInfo);

            for (u1PscIndex = RSVPTE_ZERO; u1PscIndex < RPTE_DS_MAX_NO_OF_PSCS;
                 u1PscIndex++)
            {
                if (RPTE_RM_SUPPORTED_PSC
                    (gDiffServRMGblInfo, u4OutIfIndex, u1PscIndex) ==
                    RSVPTE_TE_DS_TNL_LLSP_DSCP (pRsvpTeTnlInfo))
                {
                    break;
                }
            }
            if (u1PscIndex == RPTE_DS_MAX_NO_OF_PSCS)
            {
                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            "RpteValidateDSObjs: INTMD_EXIT\n");
                return RPTE_FAILURE;
            }
        }

        /* Validation of the Class Type in the Class Type Object is Done */

        if (RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo) != RSVPTE_ZERO)
        {
            for (u1ClassIndex = RSVPTE_ZERO; u1ClassIndex
                 < RPTE_DS_MAX_NO_OF_CLASSTYPES; u1ClassIndex++)
            {
                if (RPTE_RM_SUPPORTED_CLASS_TYPES (gDiffServRMGblInfo,
                                                   u4OutIfIndex, u1ClassIndex)
                    == (RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo)))
                {
                    break;
                }
            }
            if (u1ClassIndex == RPTE_DS_MAX_NO_OF_CLASSTYPES)
            {
                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            "RpteValidateDSObjs: INTMD_EXIT\n");
                return RPTE_FAILURE;
            }
        }
    }

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteValidateDSObjs: EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteUpdateDSPathObjs                                   */
/* Description     : This function updates the Diffserv objects             */
/*                   (DiffservElsp Object,DiffservLlsp Object or            */
/*                   DiffservClass Type Object)Present in the Path message  */
/* Input (s)       : *pRsvpTeTnlInfo- Pointer to Tunnel Info                */
/*                   **ppObjHdr - Pointer to the Pointer of Object Header   */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteUpdateDSPathObjs (const tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                      tObjHdr ** ppObjHdr)
{
    UINT1              *pPdu = NULL;
    UINT2               u2ObjLen = RSVPTE_ZERO;
    UINT1               u1NumberOfEntries = RSVPTE_ZERO;
    UINT2               u2PhbId = RSVPTE_ZERO;
    UINT1               u1Dscp = RSVPTE_ZERO;
    tMplsDiffServElspInfo *pTempElspInfo = NULL;
    pPdu = (UINT1 *) *ppObjHdr;

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteUpdateDSPathObjs: ENTRY\n");

    if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
    {
        if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) ==
            RPTE_DIFFSERV_ELSP)
        {
            /* If the ELsp is Preconfigured one then Diffserv Object 
             * is not included in the Path Message */

            if (RSVPTE_TE_DS_TNL_ELSP_TYPE (pRsvpTeTnlInfo) ==
                RPTE_DIFFSERV_PRECONF)
            {
                u2ObjLen = (UINT2)(u2ObjLen + (sizeof (tObjHdr) + (sizeof (UINT4))));

                RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (u2ObjLen));
                RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS
                                  (RPTE_DS_ELSP_OBJ_CLASS_NUM_TYPE));
                RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);
                RPTE_PUT_1_BYTE (pPdu, RSVPTE_ZERO);
                RPTE_PUT_1_BYTE (pPdu, RSVPTE_ZERO);
            }

            /* If the Elsp is Signalled one then the Diffserv Object is Updated
             * and is included in the Path Message */

            else if (RSVPTE_TE_DS_TNL_ELSP_TYPE (pRsvpTeTnlInfo)
                     == RPTE_DIFFSERV_SIGNALLED)
            {
                if(pRsvpTeTnlInfo->pTeTnlInfo->pMplsDiffServTnlInfo->pMplsDiffServElspList == NULL)
                {
                   return;
                }

                RPTE_DS_NO_OF_ENTRIES (u1NumberOfEntries, pRsvpTeTnlInfo);

                u2ObjLen = (UINT2)(u2ObjLen + (sizeof (tObjHdr) + (sizeof (UINT4))
                             +
                             (u1NumberOfEntries * sizeof (tDiffServMapEntry))));

                RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (u2ObjLen));
                RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS
                                  (RPTE_DS_ELSP_OBJ_CLASS_NUM_TYPE));
                RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);
                RPTE_PUT_1_BYTE (pPdu, RSVPTE_ZERO);
                RPTE_PUT_1_BYTE (pPdu, u1NumberOfEntries);

                TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST (pRsvpTeTnlInfo),
                              pTempElspInfo, tMplsDiffServElspInfo *)
                {
                    u1Dscp = RPTE_TE_DS_ELSP_INFO_PHB_DSCP (pTempElspInfo);
                    RPTE_PUT_1_BYTE (pPdu, RSVPTE_ZERO);
                    RPTE_PUT_1_BYTE (pPdu, (UINT1) ((RPTE_TE_DS_ELSP_INFO_INDEX
                                                     (pTempElspInfo))));

                    RPTE_DS_DSCP_TO_ELSP_PHBID (u1Dscp, u2PhbId);
                    RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (u2PhbId));
                }
            }
        }
        /* If the Lsp is LLsp  the Diffserv Object is Updated and included 
         * in the Path Message */

        else if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)
                 == RPTE_DIFFSERV_LLSP)
        {

            u1Dscp = RSVPTE_TE_DS_TNL_LLSP_DSCP (pRsvpTeTnlInfo);
            u2ObjLen = (UINT2)(u2ObjLen + (sizeof (tObjHdr) + (sizeof (UINT4))));
            RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (u2ObjLen));
            RPTE_PUT_2_BYTES (pPdu,
                              OSIX_HTONS (RPTE_DS_LLSP_OBJ_CLASS_NUM_TYPE));
            RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);

            RPTE_DS_DSCP_TO_LLSP_PHBID (u1Dscp, u2PhbId);
            RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (u2PhbId));
        }

        /* If the Class Type is 1 ,2 or 3 Then the Class Type Object is 
         * Updated and  Included in the Path Message */

        if ((RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo) != RSVPTE_ZERO) ||
            (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_CT_ZERO))
        {
            u2ObjLen = RSVPTE_ZERO;

            u2ObjLen = (UINT2)(u2ObjLen + (sizeof (tObjHdr) + (sizeof (UINT4))));

            RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (u2ObjLen));

            RPTE_PUT_2_BYTES (pPdu,
                              OSIX_HTONS (RPTE_CLASS_TYPE_OBJ_CLASS_NUM_TYPE));

            RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);
            RPTE_PUT_1_BYTE (pPdu, RSVPTE_ZERO);
            RPTE_PUT_1_BYTE (pPdu,
                             RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo));
        }
        if (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_MULTI_CT_OBJ)
        {
            u2ObjLen = RSVPTE_ZERO;

            u2ObjLen = (UINT2)(u2ObjLen + (sizeof (tObjHdr) + (sizeof (UINT4))));

            RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (u2ObjLen));

            RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS
                              (RPTE_CLASS_TYPE_OBJ_CLASS_NUM_TYPE));
            RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);
            RPTE_PUT_1_BYTE (pPdu, RSVPTE_ZERO);
            RPTE_PUT_1_BYTE (pPdu,
                             RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo));
        }
    }
    *ppObjHdr = (tObjHdr *) (VOID *) pPdu;

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteUpdateDSPathObjs: EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteUpdateDSElspTpObj                                  */
/* Description     : This function updates the Diffserv ElspTPObject Present*/
/*                   in the Path Message                                    */
/* Input (s)       : *pRsvpTeTnlInfo - Pointer to Tunnel Info               */
/*                 : **ppObjHdr      - Pointer to Pointer of the object     */
/*                                     header                               */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS or  RPTE_FAILURE                          */
/****************************************************************************/
UINT1
RpteUpdateDSElspTpObj (const tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                       tObjHdr ** ppObjHdr)
{
    UINT1              *pPdu = NULL;
    UINT1               u1NoOfElspTpEntries = RSVPTE_ZERO;
    UINT2               u2ObjLen = RSVPTE_ZERO;
    UINT2               u2PhbId = RSVPTE_ZERO;
    UINT1               u1ElspDscp = RSVPTE_ZERO;
    UINT1               u1Psc = RSVPTE_ZERO;
    tMplsDiffServElspInfo *pMplsDiffServElspInfo = NULL;

    /* If the Lsp is ELsp and PerOA Based then the ELspTP Object is 
     * Updated and  Included in the Path Message */

    pPdu = (UINT1 *) *ppObjHdr;

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteUpdateDSElspTpObj: ENTRY\n");

    if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
    {
        if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) != RSVPTE_ZERO)
        {
            TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST (pRsvpTeTnlInfo),
                          pMplsDiffServElspInfo, tMplsDiffServElspInfo *)
            {
                if (RPTE_TE_DS_ELSP_INFO_TRFC_PARAMS (pMplsDiffServElspInfo)
                    != NULL)
                {
                    u1NoOfElspTpEntries++;
                }
            }
            if (u1NoOfElspTpEntries != RSVPTE_ZERO)
            {
                u2ObjLen = (UINT2)(u2ObjLen + (sizeof (tObjHdr) + (sizeof (UINT4)) +
                             (u1NoOfElspTpEntries *
                              sizeof (tRpteDiffServElspTPEntry))));

                RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (u2ObjLen));
                RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS
                                  (RPTE_ELSP_TP_OBJ_CLASS_NUM_TYPE));
                RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);
                RPTE_PUT_1_BYTE (pPdu, RSVPTE_ZERO);
                RPTE_PUT_1_BYTE (pPdu, u1NoOfElspTpEntries);

                TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST (pRsvpTeTnlInfo),
                              pMplsDiffServElspInfo, tMplsDiffServElspInfo *)
                {
                    if (RPTE_TE_DS_ELSP_INFO_TRFC_PARAMS (pMplsDiffServElspInfo)
                        != NULL)
                    {
                        RPTE_PUT_2_BYTES (pPdu, RSVPTE_ZERO);

                        u1ElspDscp = RPTE_TE_DS_ELSP_INFO_PHB_DSCP
                            (pMplsDiffServElspInfo);

                        u1Psc = rpteTeDiffServGetPhbPsc (u1ElspDscp);

                        RPTE_DS_DSCP_TO_ELSP_PHBID (u1Psc, u2PhbId);

                        RPTE_PUT_2_BYTES (pPdu, OSIX_HTONS (u2PhbId));

                        if (RPTE_TE_DS_ELSP_INFO_RSVP_TRFC_PARAMS(pMplsDiffServElspInfo) != NULL)
                        {

                            RPTE_PUT_4_BYTES (pPdu,
                                              OSIX_HTONL (RPTE_DS_ELSP_TPARAM_TBR
                                                          (pMplsDiffServElspInfo)));

                            RPTE_PUT_4_BYTES (pPdu,
                                              OSIX_HTONL (RPTE_DS_ELSP_TPARAM_TBS
                                                          (pMplsDiffServElspInfo)));

                            RPTE_PUT_4_BYTES (pPdu,
                                              OSIX_HTONL (RPTE_DS_ELSP_TPARAM_PDR
                                                          (pMplsDiffServElspInfo)));

                            RPTE_PUT_4_BYTES (pPdu, OSIX_HTONL
                                              (RPTE_DS_ELSP_TPARAM_MPU
                                               (pMplsDiffServElspInfo)));

                            RPTE_PUT_4_BYTES (pPdu, OSIX_HTONL
                                              (RPTE_DS_ELSP_TPARAM_MPS
                                               (pMplsDiffServElspInfo)));
                        }
                        *ppObjHdr = (tObjHdr *) (VOID *) pPdu;
                    }
                }
            }
        }
    }

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteUpdateDSElspTpObj: EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteCopyDiffServObjsToTnlInfo                          */
/* Description     : This function will copies all information related to   */
/*                   the Diffserv Lsp received in the Path Message,Through  */
/*                   PacketMap to RsvpteTnlInfo.                            */
/* Input (s)       : *pRsvpTeTnlInfo - Pointer to Tunnel Info               */
/*                 : *pPktMap        - Pointer to Packet Map                */
/* Output (s)      : Diffserv Tunnel Information will be updated in         */
/*                   RsvpTeTnlInfo                                          */
/* Returns         : RPTE_SUCCESS or  RPTE_FAILURE                          */
/****************************************************************************/
UINT1
RpteCopyDiffServObjsToTnlInfo (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                               tPktMap * pPktMap)
{
    UINT1               u1PscIndex = RSVPTE_ZERO;
    UINT1               u1ArrayIndex = RSVPTE_ZERO;
    UINT1               u1Dscp = RSVPTE_ZERO;
    UINT1               u1PscDscp1 = RSVPTE_ZERO;
    UINT1               u1PscDscp2 = RSVPTE_ZERO;
    UINT1               u1Exp = RSVPTE_ZERO;
    UINT1               u1DuplicateError = RSVPTE_ZERO;
    UINT1               u1Found = RSVPTE_ZERO;
    UINT1               u1PhbDscp = RSVPTE_ZERO;
    UINT1               u1Index = RSVPTE_ZERO;
    tRSVPTrfcParams     TeRSVPTrfcParm;
    tTeTrfcParams       TeTrfcParms;
    tTeTrfcParams      *pTeTrfcParms = NULL;
    tMplsDiffServTnlInfo MplsDiffServTnlInfo;
    tMplsDiffServTnlInfo *pTeDiffServTnlInfo = NULL;
    tMplsDiffServElspList *pMplsDiffServElspList = NULL;
    tMplsDiffServElspInfo aMplsDiffServElspInfo[RPTE_DIFFSERV_MAX_EXP];
    tMplsDiffServElspInfo *pTempElspInfo = NULL;
    tElspPhbMapEntry    aPreConfPhbMap[RPTE_DIFFSERV_MAX_EXP];
    tRSVP_SLL           ElspInfoList;

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteCopyDiffServObjsToTnlInfo: ENTRY\n");

    TMO_SLL_Init (&ElspInfoList);
    MEMSET (&MplsDiffServTnlInfo, RSVPTE_ZERO, sizeof (tMplsDiffServTnlInfo));
    MEMSET (&TeTrfcParms, RSVPTE_ZERO, sizeof (tTeTrfcParams));

    /* Initialization of pointers and memory content */
    RPTE_TE_DS_TNL (pRsvpTeTnlInfo) = &MplsDiffServTnlInfo;
    MplsDiffServTnlInfo.pMplsDiffServElspList = NULL;
    RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) = RSVPTE_ZERO;
    RPTE_RSVPTE_TRFC_PARAMS ((&TeTrfcParms)) = &TeRSVPTrfcParm;

    MEMSET (&aMplsDiffServElspInfo, RSVPTE_ZERO,
            sizeof (aMplsDiffServElspInfo));
    MEMSET (&aPreConfPhbMap, RSVPTE_ZERO, sizeof (aPreConfPhbMap));
    /* If a path Message is received with no Diffserv object and override
     * option is not set in the lsr then lsp is treated as preconfigured 
     * if the override option is set then non diffserv treatment is given 
     * to the lsp */

    if ((PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ (pPktMap) == NULL)
        && (PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ (pPktMap) == NULL))
    {
        if (RSVPTE_DS_OVER_RIDE == TE_DS_OVERRIDE_NOT_SET)
        {
            RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) = RPTE_DIFFSERV_ELSP;
            RSVPTE_TE_DS_TNL_ELSP_TYPE (pRsvpTeTnlInfo) = RPTE_DIFFSERV_PRECONF;
        }
        else
        {
            RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) =
                RPTE_NON_DIFFSERV_LSP;
        }
    }

    if (PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ (pPktMap) != NULL)
    {
        if (PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ_MAP_ENTRIES (pPktMap) != RSVPTE_ZERO)
        {
            RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) = RPTE_DIFFSERV_ELSP;
            RSVPTE_TE_DS_TNL_ELSP_TYPE (pRsvpTeTnlInfo) =
                RPTE_DIFFSERV_SIGNALLED;

            /* If the Lsp is ELsp Type PHB`s are Validated and Copied In To
             * RsvpTeTnlInfo */

            for (u1ArrayIndex = RSVPTE_ZERO; u1ArrayIndex <
                 PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ_MAP_ENTRIES (pPktMap);
                 u1ArrayIndex++)
            {
                u1Exp = PKT_MAP_RPTE_DIFFSERV_ELSP_EXP (pPktMap, u1ArrayIndex);
                RPTE_DS_DSCP_FOR_PHBID (OSIX_NTOHS
                                        (PKT_MAP_RPTE_DIFFSERV_ELSP_PHBID
                                         (pPktMap, u1ArrayIndex)), u1Dscp);

                u1DuplicateError = RPTE_FALSE;

                /* To Avoid Duplicate ExpPhb mappings */

                TMO_SLL_Scan (&ElspInfoList, pTempElspInfo,
                              tMplsDiffServElspInfo *)
                {
                    if (pTempElspInfo->u1PhbDscp == u1Dscp)
                    {
                        /* Duplicate Exp mapping */

                        u1DuplicateError = RPTE_TRUE;
                        break;
                    }
                }
                if (u1DuplicateError)
                {
                    continue;
                }
                TMO_SLL_Init_Node
                    (&(aMplsDiffServElspInfo[u1ArrayIndex].ElspInfoNext));

                TMO_SLL_Add
                    (&ElspInfoList,
                     &(aMplsDiffServElspInfo[u1ArrayIndex].ElspInfoNext));

                aMplsDiffServElspInfo[u1ArrayIndex].pTeTrfcParams = NULL;

                RPTE_TE_DS_ELSP_INFO_INDEX
                    (&(aMplsDiffServElspInfo[u1ArrayIndex])) =
                    (UINT1) (u1Exp);

                RPTE_TE_DS_ELSP_INFO_PHB_DSCP
                    (&(aMplsDiffServElspInfo[u1ArrayIndex])) = u1Dscp;

                RPTE_TE_DS_ELSP_INFO_ROW_STATUS
                    (&(aMplsDiffServElspInfo[u1ArrayIndex])) = RPTE_ACTIVE;
            }

            /* If Diffserv object is present in the Path Message and 
             * number of map entries are zero ,then the lsp is treated 
             * as preconfigured lsp */
        }
        else
        {
            RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) = RPTE_DIFFSERV_ELSP;
            RSVPTE_TE_DS_TNL_ELSP_TYPE (pRsvpTeTnlInfo) = RPTE_DIFFSERV_PRECONF;
        }
    }

    /* If The Lsp is LLsp Type The PSC is Validated and Copied To 
     * RsvpTeTnlInfo */

    if (PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ (pPktMap) != NULL)
    {
        RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) = RPTE_DIFFSERV_LLSP;
        RPTE_DS_DSCP_FOR_PHBID (OSIX_NTOHS
                                (PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ_PSC
                                 (pPktMap)), u1Dscp);
        (RSVPTE_TE_DS_TNL_LLSP_DSCP (pRsvpTeTnlInfo)) = u1Dscp;
    }

    /* For Class-Type Object Check If ClassTypes are Supported, 
     * and Copy In To RsvpTnlInfo */

    if (PKT_MAP_DIFFSERV_CLASS_TYPE_OBJ (pPktMap) != NULL)
    {
        if ((PKT_MAP_DIFFSERV_CLASS_TYPE
             (PKT_MAP_DIFFSERV_CLASS_TYPE_OBJ (pPktMap)) != RSVPTE_ZERO) ||
            (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_CT_ZERO))
        {
            if (PKT_MAP_DIFFSERV_CLASS_TYPE
                (PKT_MAP_DIFFSERV_CLASS_TYPE_OBJ
                 (pPktMap)) < RPTE_DS_MAX_NO_OF_CLASSTYPES)
            {

                RSVPTE_TE_DS_TNL_CLASS_TYPE (pRsvpTeTnlInfo) =
                    PKT_MAP_DIFFSERV_CLASS_TYPE
                    (PKT_MAP_DIFFSERV_CLASS_TYPE_OBJ (pPktMap));
            }
        }
    }

    /* If The ElspTP Object is Present */
    if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) == RPTE_DIFFSERV_ELSP)
    {
        if ((PKT_MAP_DIFFSERV_ELSPTP_OBJ (pPktMap) != NULL)
            && (PKT_MAP_DIFFSERV_ELSPTP_NO_OF_ENTRIES (pPktMap)) != RSVPTE_ZERO)
        {
            /* Check whether the Psc's are supported in all the 
             * signalled perOA If the Psc's are Supported then 
             *   Available resources are found and then Updated 
             *   in The RsvpTeTnlInfo */
            for (u1PscIndex = RSVPTE_ZERO; u1PscIndex <
                 PKT_MAP_DIFFSERV_ELSPTP_NO_OF_ENTRIES (pPktMap); u1PscIndex++)
            {
                u1Found = RPTE_FALSE;

                RPTE_DS_DSCP_FOR_PHBID (OSIX_NTOHS
                                        (PKT_MAP_DIFFSERV_ELSPTP_PSC
                                         (pPktMap, u1PscIndex)), u1PscDscp2);

                if (RSVPTE_TE_DS_TNL_ELSP_TYPE
                    (pRsvpTeTnlInfo) != RPTE_DIFFSERV_PRECONF)
                {
                    TMO_SLL_Scan
                        (&ElspInfoList, pTempElspInfo, tMplsDiffServElspInfo *)
                    {
                        u1PhbDscp = pTempElspInfo->u1PhbDscp;

                        u1PscDscp1 = rpteTeDiffServGetPhbPsc (u1PhbDscp);

                        if (u1PscDscp2 == u1PscDscp1)
                        {
                            u1Found = RPTE_TRUE;
                            break;
                        }
                    }
                }
                else
                {
                    if (RpteMplsDiffServGetPreConfPhbMap
                        (RSVPTE_ZERO, aPreConfPhbMap) == RPTE_MPLS_SUCCESS)
                    {
                        for (u1Index = RSVPTE_ZERO; u1Index <
                             RPTE_DIFFSERV_MAX_MAP_ENTRIES; u1Index++)
                        {
                            if (!aPreConfPhbMap[u1Index].u1IsExpValid)
                            {
                                continue;
                            }
                            u1PhbDscp = aPreConfPhbMap[u1Index].u1ElspPhbDscp;
                            u1PscDscp1 = rpteTeDiffServGetPhbPsc (u1PhbDscp);
                            if (u1PscDscp2 == u1PscDscp1)
                            {
                                TMO_SLL_Init_Node
                                    (&(aMplsDiffServElspInfo
                                       [u1PscIndex].ElspInfoNext));
                                TMO_SLL_Add
                                    (&ElspInfoList,
                                     &(aMplsDiffServElspInfo
                                       [u1PscIndex].ElspInfoNext));

                                RPTE_TE_DS_ELSP_INFO_INDEX
                                    (&(aMplsDiffServElspInfo
                                       [u1PscIndex])) = (UINT1) (u1Index);

                                RPTE_TE_DS_ELSP_INFO_PHB_DSCP
                                    (&(aMplsDiffServElspInfo
                                       [u1PscIndex])) = u1PhbDscp;

                                RPTE_TE_DS_ELSP_INFO_ROW_STATUS
                                    (&aMplsDiffServElspInfo
                                     [u1PscIndex]) = RPTE_ACTIVE;

                                pTempElspInfo
                                    = &(aMplsDiffServElspInfo[u1PscIndex]);
                                u1Found = RPTE_TRUE;
                                break;
                            }
                        }
                    }
                }
                if (!u1Found)
                {
                    RPTE_TE_DS_TNL (pRsvpTeTnlInfo) = NULL;
                    RptePvmSendPathErr
                        (pPktMap, RPTE_DIFFSERV_ERROR, RPTE_UNSUPPORTED_PSC);
                    RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                                "RpteCopyDiffServObjsToTnlInfo: "
                                "INTMD_EXIT \n ");
                    return RPTE_FAILURE;
                }

                RPTE_TPARAM_TBR ((&TeTrfcParms)) =
                    OSIX_NTOHL
                    (PKT_MAP_DIFFSERV_ELSPTP_TB_RATE (pPktMap, u1PscIndex));

                RPTE_TPARAM_TBS ((&TeTrfcParms)) =
                    OSIX_NTOHL
                    (PKT_MAP_DIFFSERV_ELSPTP_TB_SIZE (pPktMap, u1PscIndex));

                RPTE_TPARAM_PDR ((&TeTrfcParms)) =
                    OSIX_NTOHL
                    (PKT_MAP_DIFFSERV_ELSPTP_PARAM_PD_RATE
                     (pPktMap, u1PscIndex));

                RPTE_TPARAM_MPU ((&TeTrfcParms)) =
                    OSIX_NTOHL
                    (PKT_MAP_DIFFSERV_ELSPTP_PARAM_MIN_POL_UNIT
                     (pPktMap, u1PscIndex));

                RPTE_TPARAM_MPS ((&TeTrfcParms)) =
                    OSIX_NTOHL
                    (PKT_MAP_DIFFSERV_ELSPTP_PARAM_MAXPKTSIZE
                     (pPktMap, u1PscIndex));

                RPTE_TNLRSRC_ROW_STATUS ((&TeTrfcParms)) = TE_ACTIVE;

                if (rpteTeCreateTrfcParams
                    (RPTE_PROT, &pTeTrfcParms, &TeTrfcParms) != TE_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_PH_PRCS,
                                "PATH : Err - Tnl Traffic Param Mem "
                                "alloc failed \n ");
                    TMO_SLL_Scan (&ElspInfoList, pTempElspInfo,
                                  tMplsDiffServElspInfo *)
                    {
                        rpteTeDeleteTrfcParams (pTempElspInfo->pTeTrfcParams);
                        pTempElspInfo->pTeTrfcParams = NULL;
                    }
                    RPTE_TE_DS_TNL (pRsvpTeTnlInfo) = NULL;
                    RptePvmSendPathErr (pPktMap, RPTE_DIFFSERV_ERROR,
                                        RPTE_LSP_CONTEXT_ALLOC_FAIL);
                    RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                                "RpteCopyDiffServObjsToTnlInfo: "
                                "INTMD_EXIT \n");
                    return RPTE_FAILURE;
                }
                pTempElspInfo->pTeTrfcParams = pTeTrfcParms;
            }
        }
        if (TMO_SLL_Count (&ElspInfoList) != RSVPTE_ZERO)
        {
            if (rpteTeDiffServCreateDiffServElspList
                (&pMplsDiffServElspList, &ElspInfoList) != RPTE_TE_SUCCESS)
            {
                TMO_SLL_Scan
                    (&ElspInfoList, pTempElspInfo, tMplsDiffServElspInfo *)
                {
                    rpteTeDeleteTrfcParams (pTempElspInfo->pTeTrfcParams);
                    pTempElspInfo->pTeTrfcParams = NULL;
                }
                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            " PATH : Err - Diffserv Elsp List"
                            " creation failed");
                RPTE_TE_DS_TNL (pRsvpTeTnlInfo) = NULL;
                RptePvmSendPathErr (pPktMap, RPTE_DIFFSERV_ERROR,
                                    RPTE_LSP_CONTEXT_ALLOC_FAIL);
                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            " RpteCopyDiffServObjsToTnlInfo:INTMD_EXIT \n");

                return RPTE_FAILURE;
            }
            MplsDiffServTnlInfo.pMplsDiffServElspList = pMplsDiffServElspList;
            MplsDiffServTnlInfo.u4ElspListIndex
                = pMplsDiffServElspList->u4ElspInfoListIndex;
        }
    }

    if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) != RPTE_NON_DIFFSERV_LSP)
    {
        if (rpteTeDiffServCreateDiffServTnl
            (&pTeDiffServTnlInfo, &MplsDiffServTnlInfo) != RPTE_SUCCESS)
        {
            RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                        "  PATH :  Err - DiffServ Tnl creation failed \n");
            RPTE_TE_DS_TNL (pRsvpTeTnlInfo) = NULL;
            if (&MplsDiffServTnlInfo.pMplsDiffServElspList != NULL)
            {
                rpteTeDiffServDeleteDiffServElspList
                    (MplsDiffServTnlInfo.pMplsDiffServElspList);
            }
            if (pTeTrfcParms != NULL)
            {
                TMO_SLL_Scan
                    (&ElspInfoList, pTempElspInfo, tMplsDiffServElspInfo *)
                {
                    rpteTeDeleteTrfcParams (pTempElspInfo->pTeTrfcParams);
                    pTempElspInfo->pTeTrfcParams = NULL;
                }
            }
            RptePvmSendPathErr (pPktMap, RPTE_DIFFSERV_ERROR,
                                RPTE_LSP_CONTEXT_ALLOC_FAIL);

            RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                        " RpteCopyDiffServObjsToTnlInfo: INTMD_EXIT \n ");
            return RPTE_FAILURE;
        }
        RPTE_TE_DS_TNL (pRsvpTeTnlInfo) = pTeDiffServTnlInfo;
    }
    else
    {
        RPTE_TE_DS_TNL (pRsvpTeTnlInfo) = NULL;
    }
    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, " RpteCopyDiffServObjsToTnlInfo: EXIT \n ");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteValidateSupprtdDSObjs                              */
/* Description     : This function will copies all information related to   */
/*                   the Diffserv Lsp received in the Path Message,Through  */
/*                   PacketMap to RsvpteTnlInfo.                            */
/* Input (s)       : *pRsvpTeTnlInfo - Pointer to Tunnel Info               */
/*                 : *pOutIfEntry    - Pointer to Out Interface Entry       */
/*                 : *pPktMap        - Pointer to Packet Map                */
/* Output (s)      : Diffserv Tunnel Information will be updated in         */
/*                   RsvpTeTnlInfo                                          */
/* Returns         : RPTE_SUCCESS or  RPTE_FAILURE                          */
/****************************************************************************/
UINT1
RpteValidateSupprtdDSObjs (const tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                           tIfEntry * pOutIfEntry, tPktMap * pPktMap)
{
    UINT1               u1PhbArrayIndex = RSVPTE_ZERO;
    UINT1               u1PscIndex = RSVPTE_ZERO;
    UINT1               u1ClassIndex = RSVPTE_ZERO;
    UINT4               u4OutIfIndex = RSVPTE_ZERO;

    tMplsDiffServElspInfo *pTempElspInfo = NULL;

    u4OutIfIndex = IF_ENTRY_IF_INDEX (pOutIfEntry);

    if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
    {
        if (MplsApiValDiffservTeParams
            (RPTE_ZERO, RPTE_ZERO, RPTE_ZERO, RPTE_ZERO, MPLS_DSTE_ENABLED_FLAG,
             RPTE_ZERO) == RPTE_SUCCESS)
        {
            /*The diffserv-Te  properties Are same for all the te-links.Validation
               is done when path message is received from InIfIndex.
               Hence validation is not required when path message is sent 
               from OutIfIndex */
            return RPTE_SUCCESS;
        }

        if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)
            == RPTE_DIFFSERV_ELSP)
        {
            /* If the Elsp is Signalled one then the Diffserv Elsp 
             * Object is Validated */

            if (RSVPTE_TE_DS_TNL_ELSP_TYPE (pRsvpTeTnlInfo)
                == RPTE_DIFFSERV_SIGNALLED)
            {
                if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) !=
                    RSVPTE_ZERO)
                {
                    TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST
                                  (pRsvpTeTnlInfo), pTempElspInfo,
                                  tMplsDiffServElspInfo *)
                    {
                        for (u1PhbArrayIndex = RSVPTE_ZERO;
                             u1PhbArrayIndex < RPTE_DS_MAX_NO_OF_PHBS;
                             u1PhbArrayIndex++)
                        {
                            if (RPTE_RM_SUPPORTED_PHBS
                                (gDiffServRMGblInfo, u4OutIfIndex,
                                 u1PhbArrayIndex) ==
                                RPTE_TE_DS_ELSP_INFO_PHB_DSCP (pTempElspInfo))
                            {
                                break;
                            }
                        }
                        if (u1PhbArrayIndex == RPTE_DS_MAX_NO_OF_PHBS)
                        {
                            RptePvmSendPathErr (pPktMap, RPTE_DIFFSERV_ERROR,
                                                RPTE_UNSUPPORTED_PHB);
                            RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                                        "RpteValidateSupprtdDSObjs: "
                                        "INTMD_EXIT\n");
                            return RPTE_FAILURE;
                        }

                    }
                }
            }
        }

        /* If the Lsp is LLsp  the Diffserv Llsp Object is Validated */

        else if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)
                 == RPTE_DIFFSERV_LLSP)
        {

            for (u1PscIndex = RSVPTE_ZERO; u1PscIndex < RPTE_DS_MAX_NO_OF_PSCS;
                 u1PscIndex++)
            {
                if (RPTE_RM_SUPPORTED_PSC
                    (gDiffServRMGblInfo, u4OutIfIndex, u1PscIndex) ==
                    RSVPTE_TE_DS_TNL_LLSP_DSCP (pRsvpTeTnlInfo))
                {
                    break;
                }
            }
            if (u1PscIndex == RPTE_DS_MAX_NO_OF_PSCS)
            {
                RptePvmSendPathErr (pPktMap, RPTE_DIFFSERV_ERROR,
                                    RPTE_UNSUPPORTED_PSC);

                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            "RpteValidateSupprtdDSObjs: INTMD_EXIT\n");
                return RPTE_FAILURE;
            }
        }

        /* Validation of the Class Type in the Class Type Object is Done */

        if (RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo) != RSVPTE_ZERO)
        {

            if (RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo) <
                RPTE_DS_MAX_NO_OF_CLASSTYPES)
            {
                for (u1ClassIndex = RSVPTE_ZERO; u1ClassIndex
                     < RPTE_DS_MAX_NO_OF_CLASSTYPES; u1ClassIndex++)
                {
                    if (RPTE_RM_SUPPORTED_CLASS_TYPES (gDiffServRMGblInfo,
                                                       u4OutIfIndex,
                                                       u1ClassIndex) ==
                        (RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo)))
                    {
                        break;
                    }
                }
                if (u1ClassIndex == RPTE_DS_MAX_NO_OF_CLASSTYPES)
                {
                    RptePvmSendPathErr (pPktMap, RPTE_DIFFSERV_TE_ERROR,
                                        RPTE_UNSUPPORTED_CLASS_TYPE);

                    RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                                "RpteCopyDiffServObjsToTnlInfo: "
                                "INTMD_EXIT \n ");
                    return RPTE_FAILURE;
                }
            }
            /* end of if IfEntry Supported Class Type */
            else
            {
                RptePvmSendPathErr (pPktMap, RPTE_DIFFSERV_TE_ERROR,
                                    RPTE_INVALID_CLASS_TYPE_VALUE);
                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            " RpteCopyDiffServObjsToTnlInfo:INTMD_EXIT \n ");
                return RPTE_FAILURE;
            }
        }
    }
    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteValidateDSObjs: EXIT\n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteUpdateDSResvObjsSize                               */
/* Description     : This function updates the size of Diffserv Elsp-Tp     */
/*                   object in the Resv message                             */
/* Input (s)       : *pRsvpTeTnlInfo- Pointer to Tunnel Info                */
/* Input (s)       : *pu2ResvSize   - Pointer to Resv Size                  */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteUpdateDSResvObjsSize (const tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                          UINT2 *pu2ResvSize)
{
    UINT1               u1NoOfElspTpEntries = RSVPTE_ZERO;
    tMplsDiffServElspInfo *pMplsDiffServElspInfo = NULL;

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, " RpteUpdateDSResvObjsSize: ENTRY\n");

    if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
    {

        if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) != RSVPTE_ZERO)
        {
            /* The Number of Tp Entries present in the Object is found 
               by scanning the DiffServElspInfoList */

            TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST
                          (pRsvpTeTnlInfo), pMplsDiffServElspInfo,
                          tMplsDiffServElspInfo *)
            {
                if (RPTE_TE_DS_ELSP_INFO_TRFC_PARAMS (pMplsDiffServElspInfo)
                    != NULL)
                {
                    u1NoOfElspTpEntries++;
                }
            }
        }
    }
    /* Diffserv ElspTp Object Size is calculated depending upon 
       the number of Tp Entries required */

    if (u1NoOfElspTpEntries != RSVPTE_ZERO)
    {
        RPTE_DS_ELSPTP_OBJ_SIZE (u1NoOfElspTpEntries, *pu2ResvSize);
    }
    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, " RpteUpdateDSResvObjsSize: EXIT\n");
    return;
}

/****************************************************************************/
/* Function Name   : RpteUtlDSInitPktMap                                    */
/* Description     : This function initializes the DIffserv Objects in the  */
/*                   PktMap structure                                       */
/* Input (s)       : pPktMap - Pointer to PktMap                            */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteUtlDSInitPktMap (tPktMap * pPktMap)
{
    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, " RpteUtlDSInitPktMap: ENTRY\n");

    PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ (pPktMap) = NULL;
    PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ (pPktMap) = NULL;
    PKT_MAP_DIFFSERV_CLASS_TYPE_OBJ (pPktMap) = NULL;
    PKT_MAP_DIFFSERV_ELSPTP_OBJ (pPktMap) = NULL;
    PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ (pPktMap) = NULL;

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, " RpteUtlDSInitPktMap: EXIT\n");

    return;
}

/****************************************************************************/
/* Function Name   : RpteRhDSUpdateTrafficControl                           */
/* Description     : This function sets the TC parameters to the Values     */
/*                   requested by the Receivers while Doing Reservation     */
/*                   PktMap structure                                       */
/* Input (s)       : pPktMap - Pointer to PktMap                            */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
UINT1
RpteRhDSUpdateTrafficControl (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                              UINT1 *pu1ResvRefreshNeeded,
                              tErrorSpec * pErrorSpec)
{
    UINT2               u2TempPsc = RSVPTE_ZERO;
    UINT1               u1PscCount = RSVPTE_ZERO;
    UINT1               u1IsPreEmpNeed = RPTE_FALSE;
    UINT1               u1ClassType = RSVPTE_ZERO;
    UINT1               u1Dscp = RSVPTE_ZERO;
    UINT1               u1PscDscp1 = RSVPTE_ZERO;
    UINT4               au4PeakdataRate[RPTE_DS_MAX_NO_OF_PSCS];

    tMplsDiffServElspInfo *pMplsDiffServElspInfo = NULL;
    tFlowSpec           FlowSpec;
    tFlowSpec           NewFlowSpec;
    tTMO_SLL            PreemptList;
    tIfEntry           *pIfEntry = NULL;
    tPsb               *pPsb = NULL;

    /* Initialise the list */
    TMO_SLL_Init (&PreemptList);

    MEMSET (&FlowSpec, RSVPTE_ZERO, sizeof (tFlowSpec));
    MEMSET (&NewFlowSpec, RSVPTE_ZERO, sizeof (tFlowSpec));

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, " RpteRhDSUpdateTrafficControl: ENTRY\n");

    if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
    {
        if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) ==
            RPTE_NON_DIFFSERV_LSP)
        {
            RSVPTE_DBG
                (RSVPTE_DIFF_ETEXT, " RpteRhDSUpdateTrafficControl:  EXIT n ");

            return RPTE_SUCCESS;
        }
    }

    if (pRsvpTeTnlInfo->pRsb != NULL)
    {
        pIfEntry = RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pRsvpTeTnlInfo));
    }

    if (RESOURCE_MANAGEMENT_TYPE
        (gDiffServRMGblInfo) == RPTE_TE_DS_PEROABASED_RESOURCES)
    {

        for (u1PscCount = RSVPTE_ONE; u1PscCount <= RPTE_DS_MAX_NO_OF_PSCS;
             u1PscCount++)
        {
            RPTE_PSC_FOUND (pRsvpTeTnlInfo, u1PscCount) = RPTE_FALSE;
        }

        if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
        {
            if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)
                == RPTE_DIFFSERV_ELSP)
            {
                if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) !=
                    RSVPTE_ZERO)
                {
                    TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST
                                  (pRsvpTeTnlInfo), pMplsDiffServElspInfo,
                                  tMplsDiffServElspInfo *)
                    {

                        if (RPTE_TE_DS_ELSP_INFO_TRFC_PARAMS
                            (pMplsDiffServElspInfo) != NULL)
                        {
                            u1Dscp = RPTE_TE_DS_ELSP_INFO_PHB_DSCP
                                (pMplsDiffServElspInfo);

                            u1PscDscp1 = rpteTeDiffServGetPhbPsc (u1Dscp);
                            u2TempPsc = RpteDiffServGetPscIndex (u1PscDscp1);
                            if (u2TempPsc == RPTE_FAILURE)
                            {
                                continue;
                            }
                            RESV_TSPEC_PEAK_RATE (FlowSpec) =
                                (FLOAT)OSIX_HTONL (RPTE_DS_ELSP_TPARAM_PDR
                                            (pMplsDiffServElspInfo));
                            RESV_TSPEC_BKT_RATE (FlowSpec) =
                                (FLOAT)OSIX_HTONL (RPTE_DS_ELSP_TPARAM_TBR
                                            (pMplsDiffServElspInfo));
                            RESV_TSPEC_BKT_SIZE (FlowSpec) =
                               (FLOAT)OSIX_HTONL (RPTE_DS_ELSP_TPARAM_TBS
                                            (pMplsDiffServElspInfo));
                            RESV_TSPEC_MIN_SIZE (FlowSpec) =
                                OSIX_HTONL (RPTE_DS_ELSP_TPARAM_MPU
                                            (pMplsDiffServElspInfo));
                            RESV_TSPEC_MAX_SIZE (FlowSpec) =
                                OSIX_HTONL (RPTE_DS_ELSP_TPARAM_MPS
                                            (pMplsDiffServElspInfo));

                            pPsb = RSVPTE_TNL_PSB (pRsvpTeTnlInfo);
                            if ((RpteTcCalculateFlowSpec
                                 (PSB_OUT_IF_ENTRY (pPsb),
                                  (UINT1) u2TempPsc, &PSB_ADSPEC (pPsb),
                                  &pPsb->SenderTspecObj.SenderTspec, &FlowSpec,
                                  &NewFlowSpec, &pErrorSpec->u1ErrCode))
                                == RPTE_FAILURE)
                            {
                                RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                            "Calculate FlowSpec Failed.\n");
                                RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                                            "RpteRhDSUpdateTrafficControl : "
                                            "INTMD-EXIT \n");
                                return RPTE_FAILURE;
                            }

                            if (RpteTcResvResources
                                (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB
                                                   (pRsvpTeTnlInfo)),
                                 &NewFlowSpec,
                                 (UINT1) u2TempPsc,
                                 &RPTE_PSC_HANDLE (pRsvpTeTnlInfo,
                                                   u2TempPsc),
                                 &pErrorSpec->u1ErrCode) == RPTE_FAILURE)
                            {
                                RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                            "Reserving of Resources Failed.\n");
                                RPTE_PSC_FOUND (pRsvpTeTnlInfo, u2TempPsc) =
                                    RPTE_PREEMPT;
                                u1IsPreEmpNeed = RPTE_TRUE;
                                au4PeakdataRate[u2TempPsc] =
                                    RPTE_DS_ELSP_TPARAM_PDR
                                    (pMplsDiffServElspInfo);
                            }
                            else
                            {
                                if (RpteTcAssociateFilterInfo
                                    (RPTE_PSC_HANDLE
                                     (pRsvpTeTnlInfo, u2TempPsc),
                                     pRsvpTeTnlInfo, &NewFlowSpec,
                                     &pErrorSpec->u1ErrCode) == RPTE_FAILURE)
                                {
                                    RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                                "Association of Filter Info "
                                                "Failed.\n");
                                    RpteTcFreeResources
                                        (RPTE_PSC_HANDLE (pRsvpTeTnlInfo,
                                                          u2TempPsc));
                                    RPTE_PSC_FOUND (pRsvpTeTnlInfo,
                                                    u2TempPsc) = RPTE_PREEMPT;
                                    au4PeakdataRate[u2TempPsc] =
                                        RPTE_DS_ELSP_TPARAM_PDR
                                        (pMplsDiffServElspInfo);
                                }
                                else
                                {
                                    RPTE_PSC_FOUND (pRsvpTeTnlInfo,
                                                    u2TempPsc) = RPTE_TRUE;
                                }
                            }
                        }
                    }
                }
            }
            else if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) ==
                     RPTE_DIFFSERV_LLSP)
            {
                u1Dscp = RSVPTE_TE_DS_TNL_LLSP_DSCP (pRsvpTeTnlInfo);
                u2TempPsc = RpteDiffServGetPscIndex (u1Dscp);

                if (pRsvpTeTnlInfo->pRsb == NULL)
                {
                    RSVPTE_DBG
                        (RSVPTE_DIFF_ETEXT,
                         " RpteRhDSUpdateTrafficControl RSB is NULL: INTMD_EXIT\n");
                    return RPTE_FAILURE;
                }
                if ((RpteTcCalculateFlowSpec
                     (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                      (UINT1) u2TempPsc,
                      &PSB_ADSPEC (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                      &pRsvpTeTnlInfo->pPsb->SenderTspecObj.SenderTspec,
                      &pRsvpTeTnlInfo->pRsb->FlowSpec,
                      &FlowSpec, &pErrorSpec->u1ErrCode)) == RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                "Calculate FlowSpec Failed.\n");
                    RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                                "RpteRhDSUpdateTrafficControl: INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }

                if (RpteTcResvResources
                    (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                     &FlowSpec, (UINT1) u2TempPsc,
                     &RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc),
                     &pErrorSpec->u1ErrCode) == RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                "Reserving of Resources Failed.\n");
                    RPTE_PSC_FOUND (pRsvpTeTnlInfo, u2TempPsc) = RPTE_PREEMPT;
                    u1IsPreEmpNeed = RPTE_TRUE;
                    au4PeakdataRate[u2TempPsc] =
                        (UINT4) OSIX_NTOHF (RESV_TSPEC_PEAK_RATE (FLOW_SPEC_OBJ
                                                                  (RSVPTE_TNL_RSB
                                                                   (pRsvpTeTnlInfo))));
                }
                else
                {
                    if (RpteTcAssociateFilterInfo
                        (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc),
                         pRsvpTeTnlInfo, &FlowSpec,
                         &pErrorSpec->u1ErrCode) == RPTE_FAILURE)
                    {
                        RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                    "Association of Filter Info Failed.\n");
                        RpteTcFreeResources
                            (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc));
                        RPTE_PSC_FOUND (pRsvpTeTnlInfo, u2TempPsc) =
                            RPTE_PREEMPT;
                        u1IsPreEmpNeed = RPTE_TRUE;
                        au4PeakdataRate[u2TempPsc] =
                            (UINT4) OSIX_NTOHF (RESV_TSPEC_PEAK_RATE
                                                (FLOW_SPEC_OBJ
                                                 (RSVPTE_TNL_RSB
                                                  (pRsvpTeTnlInfo))));
                    }
                    else
                    {
                        RPTE_PSC_FOUND (pRsvpTeTnlInfo, u2TempPsc) = RPTE_TRUE;
                    }
                }
            }
        }

        if (u1IsPreEmpNeed == RPTE_TRUE)
        {
            if (RptePmDSGetTnlFromPrioList (pRsvpTeTnlInfo, au4PeakdataRate,
                                            &PreemptList) == RPTE_FAILURE)
            {
                RSVPTE_DBG
                    (RSVPTE_DIFF_ETEXT,
                     " RpteRhDSUpdateTrafficControl: INTMD_EXIT\n");

                return RPTE_FAILURE;
            }
            else
            {
                /* Tear down the tunnels present in the PreemptList */

                if (RpteRhPreemptTunnel (&PreemptList) != RPTE_SUCCESS)
                {
                    RSVPTE_DBG
                        (RSVPTE_DIFF_ETEXT,
                         " RpteRhDSUpdateTrafficControl: INTMD_EXIT\n");
                    return RPTE_FAILURE;
                }
            }
            /* This is used to delay the setup of high priority tunnel
             * which preempts the low priority tunnel at intermediate nodes. 
             * This is not required at ingress since after the preemption
             * process, ingress deletes the lower priority tunnel and 
             * sends path tear to downstream*/
            if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS)
            {
                *pu1ResvRefreshNeeded = RPTE_REFRESH_MSG_NO;
                return RPTE_SUCCESS;
            }

        }

        if (pIfEntry == NULL)
        {
            return RPTE_FAILURE;
        }

        for (u2TempPsc = RSVPTE_ONE; u2TempPsc <= RPTE_DS_MAX_NO_OF_PSCS;
             u2TempPsc++)
        {
            if (RPTE_PSC_FOUND (pRsvpTeTnlInfo, u2TempPsc) == RPTE_TRUE)
            {
                if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
                {
                    if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)
                        == RPTE_DIFFSERV_LLSP)
                    {
                        TMO_SLL_Init_Node (RPTE_NEXT_PMT_TUNNEL_IN_PSC
                                           (pRsvpTeTnlInfo, u2TempPsc));

                        TMO_SLL_Add (&RPTE_PMT_HOLD_PSC_OF_TNL_LIST
                                     (pIfEntry, u2TempPsc),
                                     RPTE_NEXT_PMT_TUNNEL_IN_PSC
                                     (pRsvpTeTnlInfo, u2TempPsc));
                        break;
                    }
                    else if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)
                             == RPTE_DIFFSERV_ELSP)
                    {
                        TMO_SLL_Init_Node (RPTE_NEXT_PMT_TUNNEL_IN_PSC
                                           (pRsvpTeTnlInfo, u2TempPsc));

                        TMO_SLL_Add (&RPTE_PMT_HOLD_PSC_OF_TNL_LIST
                                     (pIfEntry, u2TempPsc),
                                     RPTE_NEXT_PMT_TUNNEL_IN_PSC
                                     (pRsvpTeTnlInfo, u2TempPsc));
                    }
                    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo)
                        != RPTE_INGRESS)
                    {
                        *pu1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
                    }
                }
            }
        }

        RSVPTE_DBG (RSVPTE_DIFF_ETEXT, " RpteRhDSUpdateTrafficControl: EXIT\n");
        return RPTE_SUCCESS;
    }

    else if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo)
             == RPTE_TE_DS_CLASSTYPE_RESOURCES)
    {

        if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
        {
            u1ClassType = RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE (pRsvpTeTnlInfo);

            if ((RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo)) ==
                RPTE_DIFFSERV_ELSP)
            {
                if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) !=
                    RSVPTE_ZERO)
                {
                    TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST
                                  (pRsvpTeTnlInfo), pMplsDiffServElspInfo,
                                  tMplsDiffServElspInfo *)
                    {

                        if (RPTE_TE_DS_ELSP_INFO_TRFC_PARAMS
                            (pMplsDiffServElspInfo) != NULL)
                        {
                            u1Dscp = RPTE_TE_DS_ELSP_INFO_PHB_DSCP
                                (pMplsDiffServElspInfo);

                            u1PscDscp1 = rpteTeDiffServGetPhbPsc (u1Dscp);
                            u2TempPsc = RpteDiffServGetPscIndex (u1PscDscp1);
                            if (u2TempPsc == RPTE_FAILURE)
                            {
                                continue;
                            }

                            RESV_TSPEC_PEAK_RATE (FlowSpec) =
                                (FLOAT)OSIX_HTONL (RPTE_DS_ELSP_TPARAM_PDR
                                            (pMplsDiffServElspInfo));
                            RESV_TSPEC_BKT_RATE (FlowSpec) =
                                (FLOAT)OSIX_HTONL (RPTE_DS_ELSP_TPARAM_TBR
                                            (pMplsDiffServElspInfo));
                            RESV_TSPEC_BKT_SIZE (FlowSpec) =
                                (FLOAT)OSIX_HTONL (RPTE_DS_ELSP_TPARAM_TBS
                                            (pMplsDiffServElspInfo));
                            RESV_TSPEC_MIN_SIZE (FlowSpec) =
                                OSIX_HTONL (RPTE_DS_ELSP_TPARAM_MPU
                                            (pMplsDiffServElspInfo));
                            RESV_TSPEC_MAX_SIZE (FlowSpec) =
                                OSIX_HTONL (RPTE_DS_ELSP_TPARAM_MPS
                                            (pMplsDiffServElspInfo));
                            pPsb = RSVPTE_TNL_PSB (pRsvpTeTnlInfo);
                            if ((RpteTcCalculateFlowSpec
                                 (PSB_OUT_IF_ENTRY (pPsb),
                                  u1ClassType, &PSB_ADSPEC (pPsb),
                                  &pPsb->SenderTspecObj.SenderTspec, &FlowSpec,
                                  &NewFlowSpec, &pErrorSpec->u1ErrCode))
                                == RPTE_FAILURE)
                            {
                                RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                            "Calculate FlowSpec Failed.\n");
                                RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                                            "RpteRhDSUpdateTrafficControl : "
                                            "INTMD-EXIT \n");
                                return RPTE_FAILURE;
                            }
                            if (RpteTcResvResources
                                (PSB_OUT_IF_ENTRY (pPsb), &NewFlowSpec,
                                 u1ClassType, &RPTE_PSC_HANDLE (pRsvpTeTnlInfo,
                                                                u2TempPsc),
                                 &pErrorSpec->u1ErrCode) == RPTE_FAILURE)
                            {
                                RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                            "Reserving of Resources Failed.\n");
                            }
                            else
                            {
                                if (RpteTcAssociateFilterInfo
                                    (RPTE_PSC_HANDLE
                                     (pRsvpTeTnlInfo, u2TempPsc),
                                     pRsvpTeTnlInfo, &NewFlowSpec,
                                     &pErrorSpec->u1ErrCode) == RPTE_FAILURE)
                                {
                                    RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                                "Association of Filter "
                                                "Info Failed.\n");
                                    RpteTcFreeResources
                                        (RPTE_PSC_HANDLE (pRsvpTeTnlInfo,
                                                          u2TempPsc));
                                }
                                else
                                {
                                    RPTE_PSC_FOUND (pRsvpTeTnlInfo,
                                                    u2TempPsc) = RPTE_TRUE;
                                }
                            }
                        }
                    }
                }

                if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS)
                {
                    *pu1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
                }

                RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                            " RpteRhDSUpdateTrafficControl: EXIT\n");

                return RPTE_SUCCESS;
            }
            else if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) ==
                     RPTE_DIFFSERV_LLSP)
            {
                u1Dscp = RSVPTE_TE_DS_TNL_LLSP_DSCP (pRsvpTeTnlInfo);
                u2TempPsc = RpteDiffServGetPscIndex (u1Dscp);
                if (pRsvpTeTnlInfo->pRsb == NULL)
                {
                    RSVPTE_DBG
                        (RSVPTE_DIFF_ETEXT,
                         " RpteRhDSUpdateTrafficControl RSB is NULL: INTMD_EXIT\n");
                    return RPTE_FAILURE;
                }
                if ((RpteTcCalculateFlowSpec
                     (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                      u1ClassType,
                      &PSB_ADSPEC (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                      &pRsvpTeTnlInfo->pPsb->SenderTspecObj.SenderTspec,
                      &pRsvpTeTnlInfo->pRsb->FlowSpec,
                      &FlowSpec, &pErrorSpec->u1ErrCode)) == RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                "Calculate FlowSpec Failed.\n");
                    RSVPTE_DBG (RSVPTE_MAIN_ETEXT,
                                "RpteRhDSUpdateTrafficControl: INTMD-EXIT \n");
                    return RPTE_FAILURE;
                }
                if (RpteTcResvResources
                    (PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo)),
                     &FlowSpec,
                     u1ClassType, &RPTE_PSC_HANDLE
                     (pRsvpTeTnlInfo, u2TempPsc),
                     &pErrorSpec->u1ErrCode) == RPTE_FAILURE)
                {
                    RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                "Reserving of Resources Failed.\n");
                    RPTE_PSC_FOUND (pRsvpTeTnlInfo, u2TempPsc) = RPTE_PREEMPT;
                    u1IsPreEmpNeed = RPTE_TRUE;
                    au4PeakdataRate[u2TempPsc] =
                        (UINT4) OSIX_NTOHF (RESV_TSPEC_PEAK_RATE (FLOW_SPEC_OBJ
                                                                  (RSVPTE_TNL_RSB
                                                                   (pRsvpTeTnlInfo))));
                }
                else
                {
                    if (RpteTcAssociateFilterInfo
                        (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc),
                         pRsvpTeTnlInfo, &FlowSpec,
                         &pErrorSpec->u1ErrCode) == RPTE_FAILURE)
                    {
                        RSVPTE_DBG (RSVPTE_MAIN_MEM,
                                    "Association of Filter Info Failed.\n");
                        RpteTcFreeResources
                            (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc));
                        RPTE_PSC_FOUND (pRsvpTeTnlInfo, u2TempPsc) =
                            RPTE_PREEMPT;
                        u1IsPreEmpNeed = RPTE_TRUE;
                        au4PeakdataRate[u2TempPsc] =
                            (UINT4) OSIX_NTOHF (RESV_TSPEC_PEAK_RATE
                                                (FLOW_SPEC_OBJ
                                                 (RSVPTE_TNL_RSB
                                                  (pRsvpTeTnlInfo))));
                    }
                    else
                    {
                        RPTE_PSC_FOUND (pRsvpTeTnlInfo, u2TempPsc) = RPTE_TRUE;
                    }
                }

                if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) != RPTE_INGRESS)
                {
                    *pu1ResvRefreshNeeded = RPTE_REFRESH_MSG_YES;
                }
            }
        }
        RSVPTE_DBG (RSVPTE_DIFF_ETEXT, " RpteRhDSUpdateTrafficControl: EXIT\n");
        return RPTE_SUCCESS;
    }
    else
    {
        RSVPTE_DBG (RSVPTE_DIFF_ETEXT, " RpteRhDSUpdateTrafficControl: EXIT\n");
        return RPTE_SUCCESS;
    }
}

/*****************************************************************************/
/* Function Name : RptePmDSGetTnlFromPrioList                                */
/* Description   : This routine returns the list of the RSVP tunnels which   */
/*               : can be preempted from the  priority list to meet the      */
/*                 resource requirements                                     */
/* Input(s)      : pCtTnlInfo - Pointer to the tunnel Info                   */
/*               : pau4PeakDataRate - Contains peak data rate required for   */
/*                 Per PSC                                                   */
/* Output(s)     : pCandidatePreemp - It's the pointer to the list of        */
/*                 candidate tunnels which can be preempted                  */
/*****************************************************************************/
UINT1
RptePmDSGetTnlFromPrioList (tRsvpTeTnlInfo * pCtTnlInfo,
                            UINT4 *pau4PeakDataRate,
                            tTMO_SLL * pCandidatePreemp)
{

    UINT4               u4DiffTrack = RSVPTE_ZERO;
    UINT4               u4Diffmin = RSVPTE_ZERO;
    UINT1               u1Flag = RPTE_FALSE;
    UINT1               au1IsRscFound[RPTE_DS_MAX_NO_OF_PSCS];
    UINT1               au1PscToCheck[RPTE_DS_MAX_NO_OF_PSCS];
    /* Keeps track of the peak data rate while summing up */
    UINT4               u4TnlpeakDataRate = RSVPTE_ZERO;
    UINT2               u2Flag = RPTE_FALSE;
    UINT1               u1TempPsc;
    UINT1               u1PscCount;
    UINT1               u1PscIndex;
    tTMO_SLL            StackList;
    tTMO_SLL           *pRsvpTePscList = NULL;
    tTMO_SLL           *pStackTnlList = NULL;
    tTMO_SLL_NODE      *pPscTnlInfo = NULL;
    tIfEntry           *pIfEntry = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pTempRsvpTeTnlInfo = NULL;
    tMplsDiffServElspInfo *pMplsElspInfo = NULL;

    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, " RptePmDSGetTnlFromPrioList: ENTRY\n");

    pIfEntry = RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pCtTnlInfo));

    MEMSET ((VOID *) au1IsRscFound, RSVPTE_ZERO,
            (RPTE_DS_MAX_NO_OF_PSCS * sizeof (UINT1)));

    MEMSET (&au1PscToCheck, RSVPTE_ZERO, sizeof (au1PscToCheck));

    pStackTnlList = &StackList;
    /* Initialise the list */
    TMO_SLL_Init (pStackTnlList);
    TMO_SLL_Init (pCandidatePreemp);

    /* Note: If the priority of the tunnel to be established is 7, then
     * then no tunnels can be preempted.
     */

    if (RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pCtTnlInfo) == RSVP_TNL_MIN_PRIO)
    {
        RSVPTE_DBG (RSVPTE_PM_PRCS, " The pre-emption is not possible \n");
        RSVPTE_DBG (RSVPTE_PM_ETEXT,
                    "RptePmDSGetTnlFromPrioList : INTMD-EXIT \n");
        RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                    " RptePmDSGetTnlFromPrioList: INTMD_EXIT\n");
        return RPTE_FAILURE;
    }

    u1PscIndex = RSVPTE_ZERO;

    /*Check for a single tunnel which can be Preempted if it holds Resources 
     * for all the PSC`s */

    for (u1TempPsc = RSVPTE_ONE; u1TempPsc <= RPTE_DS_MAX_NO_OF_PSCS;
         u1TempPsc++)
    {
        if (RPTE_PSC_FOUND (pCtTnlInfo, u1TempPsc) == RPTE_PREEMPT)
        {
            au1PscToCheck[u1PscIndex++] = u1TempPsc;
        }
    }

    pRsvpTePscList = (tTMO_SLL *) & RSVPTE_HOLD_PSC_OF_TNLS_LIST_ARRAY
        (pIfEntry, au1PscToCheck[RSVPTE_ZERO]);

    TMO_SLL_Scan (pRsvpTePscList, pPscTnlInfo, tTMO_SLL_NODE *)
    {
        u1Flag = RPTE_FALSE;
        pRsvpTeTnlInfo = ((tRsvpTeTnlInfo *) (VOID *)
                          ((UINT1 *) pPscTnlInfo -
                           RPTE_OFFSET
                           (tRsvpTeTnlInfo,
                            aNextPreemptTnlInPSC[au1PscToCheck[RSVPTE_ZERO]])));

        if (RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pCtTnlInfo) >
            RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pRsvpTeTnlInfo))
        {
            continue;
        }

        /* Check if the tunnel has a stack list associated to it */
        if (TMO_SLL_Count ((tTMO_SLL *) & (pRsvpTeTnlInfo->StackTnlList))
            != RSVPTE_ZERO)
        {
            TMO_SLL_Add (pStackTnlList, (tTMO_SLL_NODE *)
                         & (pRsvpTeTnlInfo->NextStackTnl));
            continue;
        }
        RPTE_DS_GET_PDR_OF_PSC (au1PscToCheck[RSVPTE_ZERO], pRsvpTeTnlInfo,
                                pMplsElspInfo, u4TnlpeakDataRate);

        if ((pau4PeakDataRate[au1PscToCheck[RSVPTE_ZERO]] <= u4TnlpeakDataRate))
        {
            u4DiffTrack = (u4TnlpeakDataRate -
                           pau4PeakDataRate[au1PscToCheck[RSVPTE_ZERO]]);

            if (u1PscIndex == RSVPTE_ONE)
            {
                u1Flag = RPTE_TRUE;
            }

            for (u1PscCount = RSVPTE_ONE; u1PscCount < u1PscIndex; u1PscCount++)
            {

                RPTE_DS_GET_PDR_OF_PSC (au1PscToCheck[u1PscCount],
                                        pRsvpTeTnlInfo, pMplsElspInfo,
                                        u4TnlpeakDataRate);

                /* For any one PSC in the tunnel,if the required resources are
                 * not found, then check for the resources for remaining PSC`s 
                 * is not done in that particular tunnel */

                if (u4TnlpeakDataRate == RSVPTE_ZERO)
                {
                    u4DiffTrack = RSVPTE_ZERO;
                    break;
                }

                if ((pau4PeakDataRate[au1PscToCheck[u1PscCount]] <=
                     u4TnlpeakDataRate))
                {
                    u4DiffTrack +=
                        (u4TnlpeakDataRate -
                         pau4PeakDataRate[au1PscToCheck[u1PscCount]]);
                    if (u1PscCount == u1PscIndex - RSVPTE_ONE)
                    {
                        u1Flag = RPTE_TRUE;
                    }
                }
            }
            if (u1Flag == RPTE_TRUE)
            {
                if (u2Flag == RPTE_FALSE)
                {
                    u4Diffmin = u4DiffTrack;
                    pTempRsvpTeTnlInfo = pRsvpTeTnlInfo;
                    u2Flag = RPTE_TRUE;
                }
                else
                {
                    if ((u2Flag == RPTE_TRUE) && (u4DiffTrack < u4Diffmin))
                    {
                        u4Diffmin = u4DiffTrack;
                        pTempRsvpTeTnlInfo = pRsvpTeTnlInfo;
                    }
                }
            }

        }
    }

    if (u1Flag == RPTE_TRUE)
    {

        while (TMO_SLL_Get (pStackTnlList) != NULL)
            continue;
        TMO_SLL_Add (pCandidatePreemp, (tTMO_SLL_NODE *)
                     & ((pTempRsvpTeTnlInfo)->NextPreemptTnl));

        for (u1TempPsc = RSVPTE_ONE; u1TempPsc <= RPTE_DS_MAX_NO_OF_PSCS;
             u1TempPsc++)
        {
            if (RPTE_PSC_FOUND (pCtTnlInfo, u1TempPsc) == RPTE_PREEMPT)
            {
                RPTE_PSC_FOUND (pCtTnlInfo, u1TempPsc) = RPTE_TRUE;
            }
        }
        RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RptePmDSGetTnlFromPrioList :EXIT \n ");
        return RPTE_SUCCESS;
    }

    /*  Since a single tunnel could not help in getting for resources 
     *  in all the PSCs resources are tried from more than one tunnels.
     *  */

    pRsvpTePscList = (tTMO_SLL *) & RSVPTE_HOLD_PSC_OF_TNLS_LIST_ARRAY
        (pIfEntry, au1PscToCheck[RSVPTE_ZERO]);
    TMO_SLL_Scan (pRsvpTePscList, pPscTnlInfo, tTMO_SLL_NODE *)
    {

        u1Flag = RPTE_FALSE;
        pRsvpTeTnlInfo = ((tRsvpTeTnlInfo *) (VOID *)
                          ((UINT1 *) pPscTnlInfo -
                           RPTE_OFFSET
                           (tRsvpTeTnlInfo,
                            aNextPreemptTnlInPSC[au1PscToCheck[RSVPTE_ZERO]])));

        if (RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pCtTnlInfo) >
            RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pRsvpTeTnlInfo))
        {
            continue;
        }
        /* Check if the tunnel has a stack list associated to it */
        if (TMO_SLL_Count ((tTMO_SLL *) & (pRsvpTeTnlInfo->StackTnlList))
            != RSVPTE_ZERO)
        {
            continue;
        }
        for (u1PscCount = RSVPTE_ZERO; u1PscCount < u1PscIndex; u1PscCount++)
        {

            RPTE_DS_GET_PDR_OF_PSC (au1PscToCheck[u1PscCount],
                                    pRsvpTeTnlInfo, pMplsElspInfo,
                                    u4TnlpeakDataRate);

            if (au1IsRscFound[au1PscToCheck[u1PscCount]] != RPTE_TRUE)
            {
                if ((pau4PeakDataRate[au1PscToCheck[u1PscCount]] <=
                     u4TnlpeakDataRate))
                {
                    au1IsRscFound[au1PscToCheck[u1PscCount]] = RPTE_TRUE;
                    u1Flag = RPTE_TRUE;
                }
            }
        }

        if (u1Flag == RPTE_TRUE)
        {
            TMO_SLL_Add
                (pCandidatePreemp,
                 (tTMO_SLL_NODE *) & ((pRsvpTeTnlInfo)->NextPreemptTnl));
        }
    }

    for (u1PscCount = RSVPTE_ZERO; u1PscCount < u1PscIndex; u1PscCount++)
    {
        if (au1IsRscFound[au1PscToCheck[u1PscCount]] != RPTE_TRUE)
        {
            break;
        }
    }

    if (u1PscCount == u1PscIndex)
    {
        while (TMO_SLL_Get (pStackTnlList) != NULL)
            continue;

        for (u1TempPsc = RSVPTE_ONE; u1TempPsc <= RPTE_DS_MAX_NO_OF_PSCS;
             u1TempPsc++)
        {
            if (RPTE_PSC_FOUND (pCtTnlInfo, u1TempPsc) == RPTE_PREEMPT)
            {
                RPTE_PSC_FOUND (pCtTnlInfo, u1TempPsc) = RPTE_TRUE;
            }
        }
        RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RptePmDSGetTnlFromPrioList : EXIT \n");
        return RPTE_SUCCESS;
    }

    /* If both single tunnel and combination of tunnels cannot satisfy
     * the required resources ,then  stacked tunnels are checked if any 
     * one can satisfy for the required resources */

    pPscTnlInfo = TMO_SLL_First (pStackTnlList);
    while (pPscTnlInfo != NULL)
    {
        u1Flag = RPTE_FALSE;
        pRsvpTeTnlInfo = ((tRsvpTeTnlInfo *) (VOID *) ((UINT1 *) pPscTnlInfo -
                                                       RPTE_OFFSET
                                                       (tRsvpTeTnlInfo,
                                                        NextStackTnl)));

        TMO_SLL_Delete (pStackTnlList, pPscTnlInfo);

        if (RSVPTE_TNL_OR_ATTRLIST_SET_PRIO (pCtTnlInfo) >
            RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO (pRsvpTeTnlInfo))
        {
            pPscTnlInfo = TMO_SLL_First (pStackTnlList);
            continue;
        }

        for (u1PscCount = RSVPTE_ZERO; u1PscCount < u1PscIndex; u1PscCount++)
        {

            RPTE_DS_GET_PDR_OF_PSC (au1PscToCheck[u1PscCount],
                                    pRsvpTeTnlInfo, pMplsElspInfo,
                                    u4TnlpeakDataRate);

            if (au1IsRscFound[au1PscToCheck[u1PscCount]] != RPTE_TRUE)
            {
                if ((pau4PeakDataRate[au1PscToCheck[u1PscCount]] <=
                     u4TnlpeakDataRate))
                {
                    au1IsRscFound[au1PscToCheck[u1PscCount]] = RPTE_TRUE;
                    u1Flag = RPTE_TRUE;
                }
            }
        }

        if (u1Flag == RPTE_TRUE)
        {
            TMO_SLL_Add (pCandidatePreemp, (tTMO_SLL_NODE *)
                         & ((pRsvpTeTnlInfo)->NextPreemptTnl));
        }
        pPscTnlInfo = TMO_SLL_First (pStackTnlList);
    }

    for (u1PscCount = RSVPTE_ZERO; u1PscCount < u1PscIndex; u1PscCount++)
    {
        if (au1IsRscFound[au1PscToCheck[u1PscCount]] != RPTE_TRUE)
        {
            break;
        }
    }

    if (u1PscCount == u1PscIndex)
    {
        while (TMO_SLL_Get (pStackTnlList) != NULL)
            continue;

        for (u1TempPsc = RSVPTE_ONE; u1TempPsc <= RPTE_DS_MAX_NO_OF_PSCS;
             u1TempPsc++)
        {
            if (RPTE_PSC_FOUND (pCtTnlInfo, u1TempPsc) == RPTE_PREEMPT)
            {
                RPTE_PSC_FOUND (pCtTnlInfo, u1TempPsc) = RPTE_TRUE;
            }
        }
        RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RptePmDSGetTnlFromPrioList : EXIT \n");
        return RPTE_SUCCESS;
    }
    else
    {
        while (TMO_SLL_Get (pStackTnlList) != NULL)
            continue;

        pPscTnlInfo = TMO_SLL_First (pCandidatePreemp);
        while (pPscTnlInfo != NULL)
        {
            TMO_SLL_Delete (pCandidatePreemp, pPscTnlInfo);
            pPscTnlInfo = TMO_SLL_First (pCandidatePreemp);
        }
    }
    RSVPTE_DBG (RSVPTE_DIFF_ETEXT,
                "RptePmDSGetTnlFromPrioList : INTMD_EXIT \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteDSRelTrafficControl                                */
/* Description     : This Function Releases the reserved bandwidth to the   */
/*                   Interface                                              */
/* Input (s)       : pRsvpTeTnlInfo - Pointer to RsvpTeTnlinfo              */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteDSRelTrafficControl (tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    UINT2               u2TempPsc = RSVPTE_ZERO;
    UINT1               u1Dscp = RSVPTE_ZERO;
    UINT1               u1PscDscp1 = RSVPTE_ZERO;
    tMplsDiffServElspInfo *pMplsDiffServElspInfo = NULL;
    tIfEntry           *pIfEntry = NULL;

    /* Do not release bandwidth when the RSVP-TE component is made
     * shut with GR enabled
     * */
    if (gpRsvpTeGblInfo->u1GrProgressState == RPTE_GR_SHUT_DOWN_IN_PROGRESS)
    {
        return;
    }

    if (pRsvpTeTnlInfo->pRsb != NULL)
    {
        pIfEntry = RSB_OUT_IF_ENTRY (RSVPTE_TNL_RSB (pRsvpTeTnlInfo));
    }
    else
    {
        RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteDSRelTrafficControl:INTMD_EXIT \n");
        return;
    }

    if (RPTE_TE_DS_TNL (pRsvpTeTnlInfo) != NULL)
    {
        if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo)
            == RPTE_TE_DS_PEROABASED_RESOURCES)
        {
            if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) ==
                RPTE_DIFFSERV_LLSP)
            {
                for (u2TempPsc = RSVPTE_ONE;
                     u2TempPsc <= RPTE_DS_MAX_NO_OF_PSCS; u2TempPsc++)
                {
                    if (RPTE_PSC_FOUND (pRsvpTeTnlInfo, u2TempPsc) == RPTE_TRUE)
                    {
                        if (pRsvpTeTnlInfo->pRsb != NULL)
                        {
                            RpteTcDissociateFilterInfo
                                (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc),
                                 pRsvpTeTnlInfo);

                            RpteTcFreeResources
                                (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc));

                            TMO_SLL_Delete (&RPTE_PMT_HOLD_PSC_OF_TNL_LIST
                                            (pIfEntry, u2TempPsc),
                                            RPTE_NEXT_PMT_TUNNEL_IN_PSC
                                            (pRsvpTeTnlInfo, u2TempPsc));
                            break;
                        }
                    }
                }
            }
            else if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) ==
                     RPTE_DIFFSERV_ELSP)
            {
                if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) !=
                    RSVPTE_ZERO)
                {
                    TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST
                                  (pRsvpTeTnlInfo), pMplsDiffServElspInfo,
                                  tMplsDiffServElspInfo *)
                    {
                        if (RPTE_TE_DS_ELSP_INFO_TRFC_PARAMS
                            (pMplsDiffServElspInfo) != NULL)
                        {
                            u1Dscp = RPTE_TE_DS_ELSP_INFO_PHB_DSCP
                                (pMplsDiffServElspInfo);

                            u1PscDscp1 = rpteTeDiffServGetPhbPsc (u1Dscp);
                            u2TempPsc = RpteDiffServGetPscIndex (u1PscDscp1);
                            if (u2TempPsc == RPTE_FAILURE)
                            {
                                continue;
                            }
                            RpteTcDissociateFilterInfo
                                (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc),
                                 pRsvpTeTnlInfo);

                            RpteTcFreeResources
                                (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc));

                            TMO_SLL_Delete (&RPTE_PMT_HOLD_PSC_OF_TNL_LIST
                                            (pIfEntry, u2TempPsc),
                                            RPTE_NEXT_PMT_TUNNEL_IN_PSC
                                            (pRsvpTeTnlInfo, u2TempPsc));
                        }
                    }
                }
            }
            RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteDSRelTrafficControl:EXIT \n");
            return;
        }
        else if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo)
                 == RPTE_TE_DS_CLASSTYPE_RESOURCES)
        {
            if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) ==
                RPTE_DIFFSERV_LLSP)
            {
                for (u2TempPsc = RSVPTE_ONE;
                     u2TempPsc <= RPTE_DS_MAX_NO_OF_PSCS; u2TempPsc++)
                {
                    if (RPTE_PSC_FOUND (pRsvpTeTnlInfo, u2TempPsc) == RPTE_TRUE)
                    {
                        if (pRsvpTeTnlInfo->pRsb != NULL)
                        {
                            RpteTcDissociateFilterInfo
                                (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc),
                                 pRsvpTeTnlInfo);

                            RpteTcFreeResources
                                (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc));
                            break;
                        }
                    }
                }
            }
            else if (RSVPTE_TE_DS_TNL_SERVICE_TYPE (pRsvpTeTnlInfo) ==
                     RPTE_DIFFSERV_ELSP)
            {
                if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) !=
                    RSVPTE_ZERO)
                {
                    TMO_SLL_Scan (RSVPTE_TE_DS_TNL_ELSP_INFO_LIST
                                  (pRsvpTeTnlInfo), pMplsDiffServElspInfo,
                                  tMplsDiffServElspInfo *)
                    {
                        if (RPTE_TE_DS_ELSP_INFO_TRFC_PARAMS
                            (pMplsDiffServElspInfo) != NULL)
                        {
                            u1Dscp = RPTE_TE_DS_ELSP_INFO_PHB_DSCP
                                (pMplsDiffServElspInfo);

                            u1PscDscp1 = rpteTeDiffServGetPhbPsc (u1Dscp);
                            u2TempPsc = RpteDiffServGetPscIndex (u1PscDscp1);
                            if (u2TempPsc == RPTE_FAILURE)
                            {
                                continue;
                            }

                            RpteTcDissociateFilterInfo
                                (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc),
                                 pRsvpTeTnlInfo);

                            RpteTcFreeResources
                                (RPTE_PSC_HANDLE (pRsvpTeTnlInfo, u2TempPsc));

                        }
                    }
                }
            }
            RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteDSRelTrafficControl:EXIT \n");
            return;
        }
    }
    return;
}

/*****************************************************************************
* Function Name : RpteDiffServValidateDscp
* Description   : This routine is called to validate whether 
*                 such DSCP exists or not in the label request 
*                 message.    
* Input(s)      : u1Dscp - Dscp value to be validated 
* Output(s)     : None                                                      
* Return(s)     : RPTE_SUCCESS or RPTE_FAILURE
****************************************************************************/
UINT1
RpteDiffServValidateDscp (UINT1 u1Dscp)
{
    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteDiffServValidateDscp:ENTRY \n");
    switch (u1Dscp)
    {
        case RPTE_DS_DF_DSCP:
        case RPTE_DS_CS1_DSCP:
        case RPTE_DS_CS2_DSCP:
        case RPTE_DS_CS3_DSCP:
        case RPTE_DS_CS4_DSCP:
        case RPTE_DS_CS5_DSCP:
        case RPTE_DS_CS6_DSCP:
        case RPTE_DS_CS7_DSCP:
        case RPTE_DS_EF_DSCP:
        case RPTE_DS_AF11_DSCP:
        case RPTE_DS_AF12_DSCP:
        case RPTE_DS_AF13_DSCP:
        case RPTE_DS_AF21_DSCP:
        case RPTE_DS_AF22_DSCP:
        case RPTE_DS_AF23_DSCP:
        case RPTE_DS_AF31_DSCP:
        case RPTE_DS_AF32_DSCP:
        case RPTE_DS_AF33_DSCP:
        case RPTE_DS_AF41_DSCP:
        case RPTE_DS_AF42_DSCP:
        case RPTE_DS_AF43_DSCP:
	case RPTE_DS_EF1_DSCP:
            RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteDiffServValidateDscp:EXIT \n");
            return RPTE_SUCCESS;
        default:
            RSVPTE_DBG
                (RSVPTE_DIFF_ETEXT, "RpteDiffServValidateDscp:INTMD_EXIT \n");
            return RPTE_FAILURE;
    }
}

/*****************************************************************************
* Function Name : RpteDiffServGetPscIndex
* Description   : This routine is called to get the index for the PSC 
* Input(s)      : u1Psc - Psc value for which the index is needed 
* Output(s)     : None 
* Return(s)     : If a index is found for the Psc returns RPTE_SUCCESS and the 
                  Index ,if not found returns RPTE_FAILURE.
*****************************************************************************/
UINT2
RpteDiffServGetPscIndex (UINT1 u1Psc)
{
    RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteDiffServGetPscIndex:ENTRY \n");
    switch (u1Psc)
    {
        case RPTE_DS_DF_DSCP:
            return INDEX_DF;
        case RPTE_DS_CS1_DSCP:
            return INDEX_CS1;
        case RPTE_DS_CS2_DSCP:
            return INDEX_CS2;
        case RPTE_DS_CS3_DSCP:
            return INDEX_CS3;
        case RPTE_DS_CS4_DSCP:
            return INDEX_CS4;
        case RPTE_DS_CS5_DSCP:
            return INDEX_CS5;
        case RPTE_DS_CS6_DSCP:
            return INDEX_CS6;
        case RPTE_DS_CS7_DSCP:
            return INDEX_CS7;
        case RPTE_DS_EF_DSCP:
            return INDEX_EF;
        case RPTE_DS_AF1_PSC_DSCP:
            return INDEX_AF1;
        case RPTE_DS_AF2_PSC_DSCP:
            return INDEX_AF2;
        case RPTE_DS_AF3_PSC_DSCP:
            return INDEX_AF3;
        case RPTE_DS_AF4_PSC_DSCP:
            return INDEX_AF4;
	case RPTE_DS_EF1_DSCP:
	    return INDEX_EF1;	
        default:
            RSVPTE_DBG (RSVPTE_DIFF_ETEXT, "RpteDiffServGetPscIndex:EXIT \n");
            return RPTE_FAILURE;
    }
}

/*****************************************************************************
* Function Name : RpteValidateClassObject
* Description   : This routine validates the class type object
* Input(s)      : pPktMap       - Pointer to packet map structure
* Output(s)     : pu1ErrVal     - Error value
* Return(s)     : None
*****************************************************************************/

UINT4
RpteValidateClassObject (tPktMap * pPktMap, UINT2 *pu2ErrVal)
{
    UINT1               u1MapArrayIndex = RPTE_ZERO;
    UINT1               u1Dscp = RPTE_ZERO;
    UINT1               u1TeClassNum = RPTE_ZERO;

    if (MplsApiValDiffservTeParams (RPTE_ZERO, RPTE_ZERO, RPTE_ZERO, RPTE_ZERO,
                                    MPLS_DSTE_ENABLED_FLAG,
                                    RPTE_ZERO) == RPTE_FAILURE)
    {
        *pu2ErrVal = RPTE_UNEXPECTED_CLASS_TYPE_OBJ;
        return RPTE_FAILURE;
    }

    /* if the path msg does not contain a LABEL_REQUEST object,
       send path error with error value - "Unexpected CLASSTYPE Object */

    if ((pPktMap->pGenLblReqObj == NULL) ||
        (gi4MplsSimulateFailure == RPTE_SIM_FAILURE_NO_SESSION))
    {
        *pu2ErrVal = RPTE_UNEXPECTED_CLASS_TYPE_OBJ;
        return RPTE_FAILURE;
    }

    /* if the LSR determines that the received class type is invalid,
       send path error with error value - "Invalid class type */

    if ((PKT_MAP_DIFFSERV_CLASS_TYPE
         (PKT_MAP_DIFFSERV_CLASS_TYPE_OBJ (pPktMap)) == RPTE_ZERO))
    {
        *pu2ErrVal = RPTE_INVALID_CLASS_TYPE_VALUE;
        return RPTE_FAILURE;
    }

    /* If the LSR does not support the received class type.
       send path error with error value - Unsupported class type */

    if (MplsApiValDiffservTeParams (pPktMap->pRpteClassTypeObj->u1ClassType,
                                    RPTE_ZERO, RPTE_ZERO, RPTE_ZERO,
                                    MPLS_DSTE_CLASS_TYPE_FLAG,
                                    RPTE_ZERO) == RPTE_FAILURE)
    {
        *pu2ErrVal = RPTE_UNSUPPORTED_CLASS_TYPE;
        return RPTE_FAILURE;
    }

    /* if the LSR determines that the tuple formed by (i) this Class-Type and
       (ii) the setup priority signaled in the same Path message, is
       not one of the eight TE-Classes configured in the TE-class
       mapping, AND  determines that the tuple formed by (i) this Class-Type
       and (ii) the holding priority signaled in the same Path message,
       is not one of the eight TE-Classes configured in the TE-class  mapping,
       send path error with error value "Diffserv-aware TE Error" and an 
       error value of "CT and setup priority do not form a configured TE-Class 
       AND CT and holding priority do not form a configured TE-Class".  */

    if ((MplsApiValDiffservTeParams (pPktMap->pRpteClassTypeObj->u1ClassType,
                                     PKT_MAP_RPTE_SSN_ATTR_SPRIO (pPktMap),
                                     RPTE_ZERO, RPTE_ZERO,
                                     MPLS_DSTE_TE_CLASS_FLAG,
                                     &u1TeClassNum) == RPTE_FAILURE)
        &&
        (MplsApiValDiffservTeParams
         (pPktMap->pRpteClassTypeObj->u1ClassType,
          PKT_MAP_RPTE_SSN_ATTR_HPRIO (pPktMap), RPTE_ZERO, RPTE_ZERO,
          MPLS_DSTE_TE_CLASS_FLAG, &u1TeClassNum) == RPTE_FAILURE))
    {
        *pu2ErrVal = RPTE_UNSUPPORTED_CLASS_TYPE_HOLD_PRIO_SET_PRIO;
        return RPTE_FAILURE;
    }

    /* if the LSR determines that the tuple formed by (i) this Class-Type and
       (ii) the setup priority signaled in the same Path message, is
       not one of the eight TE-Classes configured in the TE-class,
       send path error with error value "CT and setup priority do not form a 
       configured TE-Class" */

    if (MplsApiValDiffservTeParams (pPktMap->pRpteClassTypeObj->u1ClassType,
                                    PKT_MAP_RPTE_SSN_ATTR_SPRIO (pPktMap),
                                    RPTE_ZERO, RPTE_ZERO,
                                    MPLS_DSTE_TE_CLASS_FLAG,
                                    &u1TeClassNum) == RPTE_FAILURE)
    {
        *pu2ErrVal = RPTE_UNSUPPORTED_CLASS_TYPE_SET_PRIO;
        return RPTE_FAILURE;
    }

    /* if the LSR determines that the tuple formed by (i) this Class-Type and
       (ii) the holding  priority signaled in the same Path message, is
       not one of the eight TE-Classes configured in the TE-class,
       send path error with error value "CT and setup priority do not form a
       configured TE-Class" */

    if (MplsApiValDiffservTeParams (pPktMap->pRpteClassTypeObj->u1ClassType,
                                    PKT_MAP_RPTE_SSN_ATTR_HPRIO (pPktMap),
                                    RPTE_ZERO, RPTE_ZERO,
                                    MPLS_DSTE_TE_CLASS_FLAG,
                                    &u1TeClassNum) == RPTE_FAILURE)
    {
        *pu2ErrVal = RPTE_UNSUPPORTED_CLASS_TYPE_HOLD_PRIO;
        return RPTE_FAILURE;
    }

    /* If PHB is inconsistent with DIFFSERV object,
       send path error with error value "Inconsistency between signaled 
       PHBs and signaled CT */

    if (PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ (pPktMap) != NULL)
    {
        for (u1MapArrayIndex = RSVPTE_ZERO; u1MapArrayIndex <
             PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ_MAP_ENTRIES (pPktMap);
             u1MapArrayIndex++)
        {
            RPTE_DS_DSCP_FOR_PHBID (OSIX_NTOHS (PKT_MAP_RPTE_DIFFSERV_ELSP_PHBID
                                                (pPktMap, u1MapArrayIndex)),
                                    u1Dscp);

            if (MplsApiValDiffservTeParams
                (pPktMap->pRpteClassTypeObj->u1ClassType, RPTE_ZERO, u1Dscp,
                 RPTE_ZERO, MPLS_DSTE_PHB_FLAG, RPTE_ZERO) == RPTE_FAILURE)
            {
                *pu2ErrVal = RPTE_CLASS_TYPE_AND_PHB_INCONSISTENT;
                return RPTE_FAILURE;
            }
        }
    }

    /* If PSC is inconsistent with DIFFSERV object,
       send path error with error value "Inconsistency between signaled
       PSC and signaled CT */

    else if (PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ (pPktMap) != NULL)
    {
        RPTE_DS_DSCP_FOR_PHBID (OSIX_NTOHS (PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ_PSC
                                            (pPktMap)), u1Dscp);

        if (MplsApiValDiffservTeParams (pPktMap->pRpteClassTypeObj->u1ClassType,
                                        RPTE_ZERO, RPTE_ZERO, u1Dscp,
                                        MPLS_DSTE_PSC_FLAG,
                                        RPTE_ZERO) == RPTE_FAILURE)
        {
            *pu2ErrVal = RPTE_CLASS_TYPE_AND_PSC_INCONSISTENT;
            return RPTE_FAILURE;
        }

    }
    return RPTE_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptediff.c                             */
/*---------------------------------------------------------------------------*/
