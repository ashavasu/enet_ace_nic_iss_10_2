/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptetrie.c,v 1.11 2014/12/12 11:56:45 siva Exp $
 *
 * Description: This file contains routines for the trie database of rsvpte.
 ********************************************************************/

#include "rpteincs.h"

/****************************************************************************/
/* Function Name   : RpteCreateTrie                                         */
/* Description     : Creates the memory space for the trie database         */
/*                   and initializes the root.                              */
/* Input (s)       : u1KeySize   - Key size for which trie data base needs  */
/*                                 to be created currently 4 Byte and 8     */
/*                                 Byte keysize is supported.               */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS or RPTE_FAILURE                           */
/****************************************************************************/
INT4
RpteCreateTrie (UINT1 u1KeySize)
{

    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteCreateTrie : ENTRY \n");
    if (u1KeySize == KEY_SIZE_8)
    {
        if (RPTE_64_BIT_TRIE_ALLOC_FLAG == RPTE_TRUE)
        {
            /*64 bit trie already created */
            RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteCreateTrie : EXIT1 \n");
            return RPTE_FAILURE;
        }

        MEMSET (&RPTE_64_BIT_TRIE_NULL_LEAF_ENTRY, RSVPTE_ZERO,
                sizeof (tRpteEntry));
        MEMSET (&RPTE_64_BIT_TRIE_ROOT_NODE, RSVPTE_ZERO,
                sizeof (tRpteTreeNode));

        RPTE_64_BIT_TRIE_ROOT_NODE.i1Flags |= RPTE_TREE_ROOT;
        RPTE_64_BIT_TRIE_ROOT_NODE.u1KeySize = u1KeySize;
        RPTE_64_BIT_TRIE_ALLOC_FLAG = RPTE_TRUE;
        RPTE_64_BIT_TRIE_ROOT_NODE.p_leaf = &RPTE_64_BIT_TRIE_NULL_LEAF_ENTRY;
        RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteCreateTrie : EXIT4 \n");
        return RPTE_SUCCESS;

    }

    if (u1KeySize == KEY_SIZE_4)
    {
        if (RPTE_32_BIT_TRIE_ALLOC_FLAG == RPTE_TRUE)
        {
            /*32 bit trie already created */
            RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteCreateTrie : EXIT5 \n");
            return RPTE_FAILURE;
        }
        MEMSET (&RPTE_32_BIT_TRIE_NULL_LEAF_ENTRY, RSVPTE_ZERO,
                sizeof (tRpteEntry));
        MEMSET (&RPTE_32_BIT_TRIE_ROOT_NODE, RSVPTE_ZERO,
                sizeof (tRpteTreeNode));

        RPTE_32_BIT_TRIE_ROOT_NODE.i1Flags |= RPTE_TREE_ROOT;
        RPTE_32_BIT_TRIE_ROOT_NODE.u1KeySize = u1KeySize;
        RPTE_32_BIT_TRIE_ALLOC_FLAG = RPTE_TRUE;
        RPTE_32_BIT_TRIE_ROOT_NODE.p_leaf = &RPTE_32_BIT_TRIE_NULL_LEAF_ENTRY;
        RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteCreateTrie : EXIT8 \n");
        return RPTE_SUCCESS;
    }
    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteCreateTrie : EXIT9 \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteTrieLookupEntry                                    */
/* Description     : Looks Up the pKey in trie database with Root           */
/*                   pRpteTrieRoot and returns the pointer to ppTrieInfo    */
/*                   associated.                                            */
/* Input (s)       : pRpteTrieRoot  -  pointer to Trie root.                */
/*                   pKey           -  pointer to the tRpteKey to be looked */
/*                                     up.                                  */
/* Output (s)      : ppTrieInfo      -  ponter to pTrieInfo.                */
/* Returns         : RPTE_SUCCESS or RPTE_FAILURE                           */
/****************************************************************************/
INT4
RpteTrieLookupEntry (tRpteTreeNode * pRpteTrieRoot, tRpteKey * pKey,
                     tuTrieInfo ** ppTrieInfo)
{
    UINT1               u1KeySize = RSVPTE_ZERO;
    INT4                i4Dir;

    tRpteTreeNode      *pRt = NULL, *pNext = NULL;

    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieLookupEntry : ENTRY \n");
    pRt = pRpteTrieRoot;
    u1KeySize = pRt->u1KeySize;

    while (RPTE_TRUE)
    {
        /* Place where Task Lock can go in */
        i4Dir = RpteTrieBitSet (pKey, u1KeySize, (INT4) pRt->i1Bitkey);
        /* Place where Task UnLock can go in */

        pNext = i4Dir ? pRt->p_right : pRt->p_left;

        if (pNext != NULL)
        {
            pRt = pNext;
            continue;
        }

        break;
    }

    /* 
     * Backtrack from this point to the top and stop when the root node
     * is reached
     */
    while ((pRt->i1Flags & RPTE_TREE_ROOT) == RPTE_FALSE)
    {
        if ((pRt->i1Flags & RPTE_TREE_BACKTRACK) != RPTE_ZERO)
        {
            /* Place where Task Lock can go in */
            if ((RpteTrieKeyMatch (&(pRt->p_leaf->rpteKey), pKey,
                                   pRt->p_leaf->u1Prefixlen, u1KeySize))
                == RPTE_TRUE)
            {
                /* Place where Task UnLock can go in */
                *ppTrieInfo = pRt->p_leaf->pTrieInfo;
                RSVPTE_DBG (RSVPTE_TRIE_ETEXT,
                            " RpteTrieLookupEntry : EXIT1 \n");
                return RPTE_SUCCESS;

            }
        }

        pRt = pRt->pParent;
    }

    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieLookupEntry : EXIT2 \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteTrieAddEntry                                       */
/* Description     : Creates and Adds the leaf entry with pTrieInfo with key*/
/*                   pointer pKey into the trie database with root          */
/*                   pRpteTrieRoot.                                         */
/* Input (s)       : pRpteTrieRoot  -  pointer to Trie root.                */
/*                   pKey           -  pointer to the tRpteKey to be added. */
/*                   pTrieInfo       -  pTrieInfo to be stored with the pKey.*/
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS or RPTE_FAILURE                           */
/****************************************************************************/
INT4
RpteTrieAddEntry (tRpteTreeNode * pRpteTrieRoot, tRpteKey * pKey,
                  tuTrieInfo * pTrieInfo)
{
    tRpteEntry         *pRpteEntry = NULL;

    tRpteTreeNode      *pRt = NULL, *pNext = NULL;
    UINT1               u1KeySize;
    INT4                i4Dir = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieAddEntry : ENTRY \n");
    pRt = pRpteTrieRoot;

    u1KeySize = pRt->u1KeySize;
    while (RPTE_TRUE)
    {
        /* Place where Task Lock can go in */
        i4Dir = RpteTrieBitSet (pKey, u1KeySize, (INT4) pRt->i1Bitkey);
        /* Place where Task UnLock can go in */

        pNext = i4Dir ? pRt->p_right : pRt->p_left;

        if (pNext != NULL)
        {
            pRt = pNext;
            continue;
        }
        break;
    }

    /* 
     * Backtrack from this point to the top and stop when the root node
     * is reached
     */
    while ((pRt->i1Flags & RPTE_TREE_ROOT) == RPTE_FALSE)
    {
        if ((pRt->i1Flags & RPTE_TREE_BACKTRACK) != RPTE_ZERO)
        {
            /* Place where Task Lock can go in */
            if ((RpteTrieKeyMatch (&(pRt->p_leaf->rpteKey), pKey,
                                   pRt->p_leaf->u1Prefixlen, u1KeySize))
                == RPTE_TRUE)
            {
                /* Place where Task UnLock can go in */
                pRpteEntry = pRt->p_leaf;
                break;
            }
        }
        pRt = pRt->pParent;
    }

    if (pRpteEntry == NULL)
    {
        pRpteEntry = RpteTrieEntryFill (pKey, u1KeySize, pTrieInfo);
        if (pRpteEntry == NULL)
        {
            /* 
             * The entry could not be added because of mem problem. 
             */
            RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieAddEntry : EXIT1 \n");
            return RPTE_FAILURE;
        }
        else
        {
            if (RpteAddTrie (pRpteTrieRoot, pRpteEntry) == RPTE_FAILURE)
            {

                /* To make sure that we don't access data from freed memory */
                MEMSET (pRt->p_leaf, RSVPTE_ZERO, sizeof (tRpteEntry));
                /* Release the allocated mem */
                if (MemReleaseMemBlock ((RPTE_TRIE_LEAF_POOL_ID
                                         (u1KeySize)),
                                        (UINT1 *) pRt->p_leaf) == MEM_FAILURE)
                {
                    /* Mem Release Failed */
                }
                RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieAddEntry : EXIT2 \n");
                return RPTE_FAILURE;
            }

        }
    }
    else                        /* entry already exists give error  */
    {
        RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieAddEntry : EXIT3 \n");
        return RPTE_FAILURE;
    }
    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieAddEntry : EXIT4 \n");
    return RPTE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : RpteTrieEntryFill                                      */
/* Description     : Allocates the memory for Leaf entry and fills it with  */
/*                   incoming values.                                       */
/* Input (s)       : pKey           - Pointer to tRpteKey.                  */
/*                   u1Prefixlen    - Prefix length with which key is added.*/
/*                   u1KeySize      - Key size.                             */
/*                   pTrieInfo       - Trie Info to be associated.          */
/* Output (s)      : None                                                   */
/* Returns         : Pointer to tRpteEntry  or NULL                         */
/****************************************************************************/
tRpteEntry         *
RpteTrieEntryFill (const tRpteKey * pKey, UINT1 u1KeySize,
                   tuTrieInfo * pTrieInfo)
{
    UINT1               u1Prefixlen;
    tRpteEntry         *pRt = NULL;

    u1Prefixlen = (u1KeySize == KEY_SIZE_8) ? PRE_LEN_64 : PRE_LEN_32;
    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieEntryFill : ENTRY \n");

    pRt = (tRpteEntry *)
        RSVP_ALLOCATE_MEM_BLOCK (RPTE_TRIE_LEAF_POOL_ID (u1KeySize));

    if (pRt == NULL)
    {
        RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieEntryFill : EXIT1 \n");
        return (NULL);
    }

    MEMSET (pRt, RSVPTE_ZERO, sizeof (tRpteEntry));

    /* Fill in the Rpte Key */

    MEMCPY (&(pRt->rpteKey), pKey, sizeof (tRpteKey));
    pRt->u1Prefixlen = u1Prefixlen;
    pRt->pTrieInfo = pTrieInfo;

    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieEntryFill : EXIT2 \n");
    return (pRt);
}

/****************************************************************************/
/* Function Name   : RpteAddTrie                                            */
/* Description     : Adds the pRpteEntry into the trie database with root   */
/*                   pointer pRpteTrieRoot.                                 */
/* Input (s)       : pRpteTrieRoot  - Pointer to the Root of trie Database  */
/*                                    where pRpteEntry needs to be added.   */
/*                   pRpteEntry     - Ponter to tRpteEntry  to be added.    */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS or RPTE_FAILURE                           */
/****************************************************************************/
INT4
RpteAddTrie (tRpteTreeNode * pRpteTrieRoot, tRpteEntry * pRpteEntry)
{

    INT4                i4Bit;
    INT4                i4Dir = RSVPTE_ZERO;
    INT1                i1_pbit = (INT1) (pRpteEntry->u1Prefixlen - 1);

    tRpteTreeNode      *pNode = NULL;
    tRpteTreeNode      *pParent = NULL;
    tRpteTreeNode      *pInter = NULL;
    tRpteTreeNode      *pNew = NULL;
    tRpteKey           *pKey = NULL;

    UINT4               u4TreeDepth = RSVPTE_ZERO;
    UINT1               u1KeySize = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteAddTrie : ENTRY \n");
    pKey = &pRpteEntry->rpteKey;
    pNode = pRpteTrieRoot;
    if (pNode == NULL)
    {
        return RPTE_FAILURE;
    }
    u1KeySize = pNode->u1KeySize;

    while(1)
    {

        if (pNode == NULL)
        {
            pNew = (tRpteTreeNode *)
                RSVP_ALLOCATE_MEM_BLOCK (RPTE_TRIE_NODE_POOL_ID (u1KeySize));

            if (pNew == NULL)
            {
                /* Allocate a new node to be added in the tree */
                RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteAddTrie : EXIT1 \n");
                return RPTE_FAILURE;
            }

            MEMSET (pNew, RSVPTE_ZERO, sizeof (tRpteTreeNode));
            pNew->i1Bitkey = i1_pbit;
            pNew->i1Flags = RPTE_TREE_BACKTRACK;

            pNew->u1KeySize = u1KeySize;
            pNew->u4Depth = u4TreeDepth;
            pNew->pParent = pParent;
            pNew->p_leaf = pRpteEntry;
            pRpteEntry->pBack = pNew;

            if (i4Dir != RSVPTE_ZERO)
            {
                pParent->p_right = pNew;
            }
            else if (pParent != NULL)
            {
                pParent->p_left = pNew;
            }
            RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteAddTrie : EXIT2 \n");
            return RPTE_SUCCESS;
        }

        ++u4TreeDepth;

        /* Place where Task Lock can go in */
        if ((RpteTrieKeyMatch (&(pNode->p_leaf->rpteKey), pKey,
                               (INT4) pNode->i1Bitkey, u1KeySize)) == RPTE_TRUE)
        {
            /* Place where Task UnLock can go in */
            if ((i1_pbit > pNode->i1Bitkey) || (i1_pbit == RSVPTE_ZERO))
            {
                /* Walk down the tree */

                /* Place where Task Lock can go in */
                i4Dir = RpteTrieBitSet (pKey, pNode->u1KeySize,
                                        (INT4) pNode->i1Bitkey);

                /* Place where Task UnLock can go in */

                pParent = pNode;

                pNode = i4Dir ? pNode->p_right : pNode->p_left;

                continue;
            }
        }

        /*
         * Since we don't have a common prefix anymore or
         * we have a less significant Entry, we've to insert an intermediate 
         * node on the list. This new node will point to the one we need to 
         * create and the current
         */

        pParent = pNode->pParent;

        /* Find 1st bit in difference between the 2 Keys */

        /* Place where Task Lock can go in */
        i4Bit = RpteKeyDiff (pKey, &(pNode->p_leaf->rpteKey), u1KeySize);
        /* Place where Task UnLock can go in */

        if (pRpteEntry->u1Prefixlen > (UINT1) i4Bit)
        {
            pInter = (tRpteTreeNode *)
                RSVP_ALLOCATE_MEM_BLOCK (RPTE_TRIE_NODE_POOL_ID (u1KeySize));

            if (pInter == NULL)
            {
                RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteAddTrie : EXIT4 \n");
                return RPTE_FAILURE;
            }

            MEMSET (pInter, RSVPTE_ZERO, sizeof (tRpteTreeNode));

            /*
             * New intermediate node. RPTE_TREE_BACKTRACK will
             * be off since that an Keys that chooses one of
             * the branches would not match less specific Entry
             * in the other branch
             */
            pInter->u1KeySize = u1KeySize;
            pInter->i1Bitkey = (INT1) i4Bit;
            pInter->u4Depth = u4TreeDepth++;
            pInter->i1Flags &= ~RPTE_TREE_BACKTRACK;
            pInter->pParent = pParent;
            pInter->p_leaf = pRpteEntry;

            /* Allocate memory for the new real node */
            pNew = (tRpteTreeNode *)
                RSVP_ALLOCATE_MEM_BLOCK (RPTE_TRIE_NODE_POOL_ID (u1KeySize));

            if (pNew == NULL)
            {
                RSVP_RELEASE_MEM_BLOCK (RPTE_TRIE_NODE_POOL_ID (u1KeySize),
                                        (UINT1 *) pInter);
                RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteAddTrie : EXIT5 \n");
                return RPTE_FAILURE;
            }

            /* Update parent pointer */
            if (i4Dir != RPTE_ZERO)
            {
                pParent->p_right = pInter;
            }
            else
            {
                pParent->p_left = pInter;
            }

            MEMSET (pNew, RSVPTE_ZERO, sizeof (tRpteTreeNode));
            pNew->u1KeySize = u1KeySize;
            pNew->i1Bitkey = i1_pbit;
            pNew->i1Flags = RPTE_TREE_BACKTRACK;
            pNew->u4Depth = u4TreeDepth;
            pNew->pParent = pInter;
            pNode->pParent = pInter;
            pNode->u4Depth = u4TreeDepth;
            pNew->p_leaf = pRpteEntry;

            pRpteEntry->pBack = pNew;

            /* Place where Task Lock can go in */
            if ((RpteTrieBitSet (pKey, u1KeySize, i4Bit)) != RSVPTE_ZERO)
                /* Place where Task UnLock can go in */
            {
                pInter->p_right = pNew;
                pInter->p_left = pNode;
            }
            else
            {
                pInter->p_left = pNew;
                pInter->p_right = pNode;
            }
            RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteAddTrie : EXIT6 \n");
            return RPTE_SUCCESS;
        }
    }

}

/****************************************************************************/
/* Function Name   : RpteTrieDeleteEntry                                    */
/* Description     : Deletes the Entries present with key  pKey and prefix  */
/*                   u1PrefixSize in the Trie Database with root            */
/*                   pRpteTrieRoot this procedure also invokes the Handle   */
/*                   deletion routine Function on the Handles (s) associated*/
/*                   with the each deleted key.                             */
/*                                                                          */
/* Input (s)       : pRpteTrieRoot    -  Pointer to the Tree Root.          */
/*                   pKey             -  Pointer to the key to be deleted.  */
/*                   u1PrefixSize     -  Size of the key Prefix to be       */
/*                                       deleted.                           */
/*                   pDelFn           -  Function Pointer to be called for  */
/*                                       releasing resources associated with*/
/*                                       pTrieInfo.                         */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_SUCCESS or RPTE_FAILURE                           */
/****************************************************************************/
INT4
RpteTrieDeleteEntry (tRpteTreeNode * pRpteTrieRoot, tRpteKey * pKey,
                     UINT1 u1PrefixSize, tDelFnType pDelFn)
{

    tRpteTreeNode      *pRt = NULL, *pNext = NULL, *pParent = NULL;
    UINT1               u1Prefixlen;
    UINT1               u1KeySize;
    INT4                i4Dir = RSVPTE_ZERO;

    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieDeleteEntry : ENTRY \n");

    pRt = pRpteTrieRoot;
    u1KeySize = pRt->u1KeySize;
    u1Prefixlen = u1PrefixSize;
    while (pRt->i1Bitkey < u1PrefixSize)
    {
        /* Place where Task Lock can go in */
        i4Dir = RpteTrieBitSet (pKey, u1KeySize, (INT4) pRt->i1Bitkey);
        /* Place where Task UnLock can go in */

        pNext = i4Dir ? pRt->p_right : pRt->p_left;

        if (pNext != NULL)
        {
            pRt = pNext;
            continue;
        }
        break;
    }
    if ((pRt->p_leaf->u1Prefixlen == u1Prefixlen) &&
        ((pRt->i1Flags & RPTE_TREE_BACKTRACK) != RPTE_ZERO))
    {
        if ((RpteTrieKeyMatch (&(pRt->p_leaf->rpteKey), pKey,
                               (INT4) u1Prefixlen, u1KeySize)) == RPTE_TRUE)
        {
            /* Call a function to release res for pTrieInfo in leaf */
            /* Delete the leaf and the node */
            (*pDelFn) (pRt->p_leaf->pTrieInfo);
            /* To make sure that we don't access data from freed memory */
            MEMSET (pRt->p_leaf, RSVPTE_ZERO, sizeof (tRpteEntry));
            if (MemReleaseMemBlock (RPTE_TRIE_LEAF_POOL_ID
                                    (u1KeySize),
                                    (UINT1 *) pRt->p_leaf) == MEM_FAILURE)
            {
                RSVPTE_DBG (RSVPTE_TRIE_PRCS,
                            "Memory Release Failed for Leaf Entry \n");
                /* Mem Release Failed */
            }
            pRt->p_leaf = NULL;
            RpteDeleteTreeNode (pRt);
            RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieDeleteEntry : EXIT1 \n");
            return RPTE_SUCCESS;
        }
    }
    else if ((pRt->p_leaf->u1Prefixlen > u1Prefixlen) &&
             ((pRt->i1Flags & RPTE_TREE_ROOT) == RPTE_FALSE))
    {
        if ((RpteTrieKeyMatch (&(pRt->p_leaf->rpteKey), pKey,
                               (INT4) u1Prefixlen, u1KeySize)) == RPTE_TRUE)
        {
            /* Delete the full sub tree */
            pParent = pRt->pParent;
            RpteDeleteSubTree (pRt, pDelFn);
            if ((pParent->i1Flags & RPTE_TREE_ROOT) != RPTE_TREE_ROOT)
            {
                RpteDeleteTreeNode (pParent);
            }
            RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieDeleteEntry : EXIT2 \n");
            return RPTE_SUCCESS;
        }
    }
    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteTrieDeleteEntry : EXIT3 \n");
    return RPTE_FAILURE;
}

/****************************************************************************/
/* Function Name   : RpteDeleteTreeNode                                     */
/* Description     : Deletes the tree node form the Trie tree database.     */
/* Input (s)       : pNode            - Pointer to the Node to be deleted.  */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteDeleteTreeNode (tRpteTreeNode * pNode)
{
    UINT4               u4Children = RSVPTE_ZERO, u4Dir = RSVPTE_ZERO;
    UINT4               u4_bit;

    tRpteTreeNode      *pChild = NULL;
    tRpteTreeNode      *pParent = NULL;
    UINT1               u1KeySize;

    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteDeleteTreeNode : ENTRY \n");
    u1KeySize = pNode->u1KeySize;
    RptePrintTreeNode (pNode);

    /*
     *  0 or one children: delete the node
     *  2 children: move the bit down
     */

    if (pNode->p_left != NULL)
    {
        u4Children++;
        u4Dir = RSVPTE_ZERO;
    }

    if (pNode->p_right != NULL)
    {
        u4Children++;
        u4Dir = RSVPTE_ONE;
    }

    if (u4Children < RSVPTE_TWO)
    {

        pChild = u4Dir ? pNode->p_right : pNode->p_left;

        if (pNode->pParent->p_left == pNode)
        {
            pNode->pParent->p_left = pChild;
        }
        else
        {
            pNode->pParent->p_right = pChild;
        }

        if (pChild != NULL)
        {
            pChild->pParent = pNode->pParent;
        }
        /*
         * Try to collapse on top
         */
        pParent = pNode->pParent;
        pNode->pParent = NULL;

        /* To make sure that we don't access data from freed memory */
        MEMSET (pNode, RSVPTE_ZERO, sizeof (tRpteTreeNode));
        /* Free the memory of the node */
        if (MemReleaseMemBlock (RPTE_TRIE_NODE_POOL_ID
                                (u1KeySize), (UINT1 *) pNode) == MEM_FAILURE)
        {
            /*Mem Release Failed */
        }
    }
    else
    {
        /* Place where Task Lock can go in */
        u4_bit = (UINT4) RpteKeyDiff (&pNode->p_left->p_leaf->rpteKey,
                                      &pNode->p_right->p_leaf->rpteKey,
                                      u1KeySize);
        /* Place where Task UnLock can go in */

        pNode->i1Bitkey = (INT1) u4_bit;
        pNode->i1Flags &= ~RPTE_TREE_BACKTRACK;
        pNode->p_leaf = pNode->p_left->p_leaf;
        pParent = pNode->pParent;
    }

    if ((pParent->i1Flags & (RPTE_TREE_BACKTRACK | RPTE_TREE_ROOT))
        == RPTE_FALSE)
    {
        RpteDeleteTreeNode (pParent);
    }

    RSVPTE_DBG (RSVPTE_TRIE_ETEXT, " RpteDeleteTreeNode : EXIT2 \n");
}

/****************************************************************************/
/* Function Name   : RpteDeleteSubTree                                      */
/* Description     : Deletes the Sub - Tree starting with node PNode.       */
/*                   Also takes care of calling the Handle Deletion         */
/*                   Routine Function on the Handle(s) associated with each */
/*                   deleted key in the Sub-Tree.                           */
/* Input (s)       : pNode            - Pointer to the Root Node of the     */
/*                                      Sub-Tree to be deleted.             */
/*                   pDelFn           -  Function Pointer to be called for  */
/*                                       releasing resources associated with*/
/*                                       pTrieInfo.                          */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RpteDeleteSubTree (tRpteTreeNode * pNode, tDelFnType pDelFn)
{
    UINT1               u1KeySize;
    u1KeySize = pNode->u1KeySize;

    if (pNode->p_left != NULL)
    {
        RpteDeleteSubTree (pNode->p_left, pDelFn);
    }
    if (pNode->p_right != NULL)
    {
        RpteDeleteSubTree (pNode->p_right, pDelFn);
    }
    if ((pNode->i1Flags & RPTE_TREE_ROOT) == RPTE_FALSE)
    {
        if ((pNode->p_leaf != NULL) &&
            ((pNode->i1Flags & RPTE_TREE_BACKTRACK) != RPTE_ZERO))
        {
            /* Call a routine for releasing Res with pTrieInfo */
            (*pDelFn) (pNode->p_leaf->pTrieInfo);
            /* To make sure that we don't access deleted data */
            MEMSET (pNode->p_leaf, RSVPTE_ZERO, sizeof (tRpteEntry));
            if (MemReleaseMemBlock ((RPTE_TRIE_LEAF_POOL_ID
                                     (u1KeySize)),
                                    (UINT1 *) pNode->p_leaf) == MEM_FAILURE)
            {
                /* Mem Release Failed */
            }
            pNode->p_leaf = NULL;
        }

        if (pNode->pParent->p_left == pNode)
        {
            pNode->pParent->p_left = NULL;
        }
        else
        {
            pNode->pParent->p_right = NULL;
        }
        /* To make sure that we don't access deleted data */
        MEMSET (pNode, RSVPTE_ZERO, sizeof (tRpteTreeNode));
        if (MemReleaseMemBlock (RPTE_TRIE_NODE_POOL_ID
                                (u1KeySize), (UINT1 *) pNode) == MEM_FAILURE)
        {
            /*Mem Release Failed */
        }
    }
}

/****************************************************************************/
/* Function Name   : RptePrintTreeNode                                      */
/* Description     : Prints the node pNode.                                 */
/* Input (s)       : pNode        - Pointer to Node to be printed.          */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
RptePrintTreeNode (const tRpteTreeNode * pNode)
{
    if (pNode != NULL)
    {
        RSVPTE_DBG3 (RSVPTE_TRIE_PRCS,
                     "       Bitkey    = %d Flag= %d Depth= %d \n",
                     pNode->i1Bitkey, pNode->i1Flags, pNode->u4Depth);

        if (pNode != NULL)
        {
            RSVPTE_DBG1 (RSVPTE_TRIE_PRCS, "       Node ptr= %p \n", pNode);
        }
        else
        {
            RSVPTE_DBG (RSVPTE_TRIE_PRCS,
                        "       Node ptr            = NULL \n");
        }
        if (pNode->pParent != NULL)
        {
            RSVPTE_DBG1 (RSVPTE_TRIE_PRCS,
                         "       Parent Node ptr     = %p \n", pNode->pParent);
        }
        else
        {
            RSVPTE_DBG (RSVPTE_TRIE_PRCS,
                        "       Parent Node ptr     = NULL \n");
        }
        if (pNode->p_left != NULL)
        {
            RSVPTE_DBG1 (RSVPTE_TRIE_PRCS,
                         "       Left Node ptr       = %p \n", pNode->p_left);
        }
        else
        {
            RSVPTE_DBG (RSVPTE_TRIE_PRCS,
                        "       Left Node ptr       = NULL \n");
        }
        if (pNode->p_right != NULL)
        {
            RSVPTE_DBG1 (RSVPTE_TRIE_PRCS,
                         "       Right Node ptr      = %p \n", pNode->p_right);
        }

        else
        {
            RSVPTE_DBG (RSVPTE_TRIE_PRCS,
                        "       Right Node ptr      = NULL \n");
        }
    }
    else
    {
        RSVPTE_DBG (RSVPTE_TRIE_PRCS, "        NULL \n");
    }

    return;
}

/****************************************************************************/
/* Function Name   : RpteTrieKeyMatch                                       */
/* Description     : Does the keys match between 2 given keys.              */
/* Input (s)       : pK1,pK2       - Keys to match.                         */
/*                   i4Prefixlen   - Prefix length to match.                */
/*                   u1KeySize     - Key Size of the keys to matched.       */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_TRUE if match occurs                              */
/*                   RPTE_FALSE is match doesn't occur                      */
/****************************************************************************/
INT4
RpteTrieKeyMatch (const tRpteKey * pK1, const tRpteKey * pK2, INT4 i4Prefixlen,
                  UINT1 u1KeySize)
{

    INT4                i4Pdw = RSVPTE_ZERO, i4_pbi = RSVPTE_ZERO;

    if ((pK1 == NULL) || (pK2 == NULL))
    {
        return RPTE_FALSE;
    }

    i4Pdw = i4Prefixlen >> RSHIFT_5BITS;
    /* Num of whole unsigned int in prefix */
    i4_pbi = i4Prefixlen & MODULUS_32_BIT_FLAG;
    /* Num of bits in incomplete UINT4 in 
     * prefix 
     */
    if (i4Pdw != RSVPTE_ZERO)
    {
        if (u1KeySize == KEY_SIZE_8)
        {
            if (MEMCMP (&pK1->u8_Key, &pK2->u8_Key, i4Pdw << LSHIFT_2BITS)
                != RPTE_ZERO)
            {
                return RPTE_FALSE;
            }
        }
        if (u1KeySize == KEY_SIZE_4)
        {
            if (MEMCMP (&pK1->u4_Key, &pK2->u4_Key, i4Pdw << LSHIFT_2BITS)
                != RPTE_ZERO)
            {
                return RPTE_FALSE;
            }
        }
    }

    if (i4_pbi != RSVPTE_ZERO)
    {
        UINT4               u4W1 = RSVPTE_ZERO, u4_w2 = RSVPTE_ZERO;
        UINT4               u4Mask = RSVPTE_ZERO;

        if (u1KeySize == KEY_SIZE_8)
        {
            u4W1 = pK1->u8_Key[i4Pdw];
            u4_w2 = pK2->u8_Key[i4Pdw];
        }
        if (u1KeySize == KEY_SIZE_4)
        {
            u4W1 = pK1->u4_Key;
            u4_w2 = pK2->u4_Key;
        }

        u4Mask = ((RPTE_MAX_UINT4_VALUE) << (RSVPTE_HEX_32 - i4_pbi));

        if (((u4W1 ^ u4_w2) & u4Mask) != RPTE_ZERO)
        {
            return RPTE_FALSE;
        }
    }
    return RPTE_TRUE;
}

/****************************************************************************/
/* Function Name   : RpteTrieBitSet                                         */
/* Description     : Checks whether the given bit of the key is set or not. */
/*                                                                          */
/* Input (s)       : pKey        - The Key.                                 */
/*                   u1KeySize   - Key Size of the Key.                     */
/*                   i4FnBit     - bit to check starting from 0 onwards     */
/* Output (s)      : None                                                   */
/* Returns         : 0 ,if Key bit is not set                               */
/*                   non-zero ,if Key bit is set                            */
/****************************************************************************/
INT4
RpteTrieBitSet (const tRpteKey * pKey, UINT1 u1KeySize, INT4 i4FnBit)
{

    INT4                i4Dw = RSVPTE_ZERO, i4Bit = i4FnBit;
    UINT4               u4B1 = RSVPTE_ZERO, u4Mask = RSVPTE_ZERO;

    if (u1KeySize == KEY_SIZE_8)
    {
        i4Dw = i4Bit >> RSHIFT_5BITS;

        u4B1 = pKey->u8_Key[i4Dw];
    }
    else
    {
        u4B1 = pKey->u4_Key;
    }

    i4Bit = ~i4Bit;
    i4Bit &= MODULUS_32_BIT_FLAG;

    u4Mask = (UINT4)(RSVPTE_ONE << i4Bit);
    return (INT4)(u4B1 & u4Mask);
}

/****************************************************************************/
/* Function Name   : RpteKeyDiff                                            */
/* Description     : Gives the bit at which the Keys are differing.         */
/* Input (s)       : pK1, pK2   - The two keys.                             */
/* Output (s)      : None                                                   */
/* Returns         : The difference bit  between the two Keys               */
/****************************************************************************/
INT4
RpteKeyDiff (const tRpteKey * pK1, const tRpteKey * pK2, UINT1 u1KeySize)
{
    INT4                i4_i, i4_res = RSVPTE_ZERO;
    INT4                i4_j = (RSVPTE_32 - RSVPTE_ONE);
    UINT4               u4B2 = RSVPTE_ZERO, u4_xb = RSVPTE_ZERO;
    UINT4               u4_bit = RSVPTE_ZERO;

    if (u1KeySize == KEY_SIZE_8)
    {
        for (i4_i = RSVPTE_ZERO; i4_i < RSVPTE_TWO; i4_i++)
        {
            u4_bit = pK1->u8_Key[i4_i];
            u4B2 = pK2->u8_Key[i4_i];

            u4_xb = u4_bit ^ u4B2;

            if (u4_xb != RSVPTE_ZERO)
            {
                while (RpteTestBit (i4_j, (INT4 *) &u4_xb) == RPTE_FALSE)
                {
                    i4_res++;
                    i4_j--;
                }

                return (i4_i * RSVPTE_32 + i4_res);
            }
        }
        return RSVPTE_64;
    }
    else                        /* (u1KeySize == KEY_SIZE_4) */
    {
        u4_bit = pK1->u4_Key;
        u4B2 = pK2->u4_Key;

        u4_xb = u4_bit ^ u4B2;

        if (u4_xb != RSVPTE_ZERO)
        {
            while (RpteTestBit (i4_j, (INT4 *) &u4_xb) == RPTE_FALSE)
            {
                i4_res++;
                i4_j--;
            }

            return (i4_res);
        }
        return RSVPTE_32;
    }
}

/****************************************************************************/
/* Function Name   : RpteTestBit                                            */
/* Description     : Test the Bit                                           */
/*                                                                          */
/* Input (s)       : i4Nr      -                                            */
/*                   p_i4_Key  -                                            */
/* Output (s)      : None                                                   */
/* Returns         : RPTE_TRUE or RPTE_FALSE                                */
/****************************************************************************/
int
RpteTestBit (INT4 i4Nr, INT4 *p_i4_key)
{
    INT4                i4Mask;

    (*p_i4_key) += i4Nr >> RSHIFT_5BITS;
    i4Mask = RSVPTE_ONE << (i4Nr & MODULUS_32_BIT_FLAG);
    if ((i4Mask & *p_i4_key) != RPTE_ZERO)
    {
        return RPTE_TRUE;
    }
    else
    {
        return RPTE_FALSE;
    }
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptetrie.c                             */
/*---------------------------------------------------------------------------*/
