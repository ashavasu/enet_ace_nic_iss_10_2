/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptepter.c,v 1.14 2016/05/20 10:10:14 siva Exp $
 *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptepter.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for 
 *                             Path Tear module
 *---------------------------------------------------------------------------*/

#include "rpteincs.h"

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePthForwardPathTear
 * Description     : This function processes the PathTear message
 * Input (s)       : *pPktMap - Pointer to Packet Map
 * Output (s)      : None 
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePthForwardPathTear (tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo)
{
    tIfEntry           *pIfEntry = NULL;

    RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthForwardPathTear : ENTRY \n");

    if (RSVPTE_TNL_PSB (pRsvpTeTnlInfo) == NULL)
    {
        RSVPTE_DBG (RSVPTE_PT_PRCS,
                    "PTEAR : PSB does not exist for the rcvd message..!\n");
        RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthForwardPathTear : INTMD-EXIT \n");
        return;
    }

    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS)
    {
        RSVPTE_DBG (RSVPTE_PT_PRCS, "Egress node cannot send PTEAR\n");
        return;
    }

    pIfEntry = PSB_OUT_IF_ENTRY (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));

    /* Check for Outgoing Iface status, if not enabled log err msg */
    if ((pIfEntry == NULL) ||
        ((pIfEntry != NULL) && ((IF_ENTRY_STATUS (pIfEntry) != RPTE_ACTIVE) ||
                                (IF_ENTRY_ENABLED (pIfEntry) != RPTE_ENABLED))))
    {
        RSVPTE_DBG (RSVPTE_PT_PRCS,
                    "PTEAR : Out going If Entry status is disabled ..!\n");
        RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthForwardPathTear : INTMD-EXIT \n");
        return;
    }
    PKT_MAP_OUT_IF_ENTRY (pPktMap) = pIfEntry;

    PKT_MAP_TX_TTL (pPktMap) = (UINT1) (PKT_MAP_RX_TTL (pPktMap) - 1);
    RPTE_PKT_MAP_TNL_INFO (pPktMap) = pRsvpTeTnlInfo;

    RptePhConstructAndSndPTMsg (pPktMap, pRsvpTeTnlInfo);
    RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthForwardPathTear : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : RptePthProcessPathTearMsg
 * Description     : This function deletes the path state which has received
 *                   a path tear message and the corressponding reserve
 *                   state block according to the style.
 * Input (s)       : pPktMap - pointer to the RSVP packet map
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
RptePthProcessPathTearMsg (tPktMap * pPktMap)
{
    tRsvpTeTnlInfo     *pRsvpTeTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeOrigTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeInTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTePrevInTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTmpTnlInfo = NULL;
    tRsvpTeTnlInfo     *pRsvpTeTempPrevInTnlInfo = NULL;
    tRsvpTeFastReroute *pRsvpTeFastReroute = NULL;
    UINT1               u1State = RPTE_ZERO;
    UINT1               u1IsDownStrTnl = RPTE_NO;
    BOOL1               bIsBkpTnl = RPTE_NO;
    BOOL1               bIsOperDone = RPTE_FALSE;
    UINT4               u4TnlInstance = RPTE_ZERO;
    UINT1               u1FrrNodeState = RPTE_ZERO;

    RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthProcessPathTearMsg : ENTRY \n");

    /* Get Tnl Info from the Tnl Hash table */
    if (RPTE_CHECK_PATH_TEAR_MSG_TNL (pPktMap, pRsvpTeTnlInfo) == RPTE_FAILURE)
    {
        /* If there is no entry present in Tnl Hash Table, drop the pkt */
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG1 (RSVPTE_PT_PRCS,
                     "PTEAR : No entry present in Tnl Table with Tnl Id : %d\n",
                     OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID (pPktMap)));
        RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthProcessPathTearMsg : EXIT \n");
        return;
    }

    RSVPTE_DBG4 (RSVPTE_PT_ETEXT,
                 "Path Tear received for the tunnel %d %d %x %x\n",
                 RSVPTE_TNL_TNLINDX (pRsvpTeTnlInfo),
                 RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo),
                 OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeTnlInfo)),
                 OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeTnlInfo)));

    /* Let's not allow to hack our stack. */
    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_INGRESS)
    {
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_FRR_DBG,
                    "Ingress: PATH TEAR message processing at ingress "
                    "tnl is invalid - ignored\n");
        return;
    }
    if (RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) == RPTE_TRUE)
    {
        pRsvpTeFastReroute =
            &PSB_RPTE_FRR_FAST_REROUTE (RSVPTE_TNL_PSB (pRsvpTeTnlInfo));

        if ((pRsvpTeFastReroute->u1Flags != RPTE_ZERO) &&
            (pRsvpTeFastReroute->u1Flags == RSVPTE_TNL_FRR_FACILITY_METHOD))
        {
            pRsvpTeTmpTnlInfo = pRsvpTeTnlInfo;

            if ((RpteUtlIsUpStrMsgRcvdOnBkpPath (pPktMap, &pRsvpTeTmpTnlInfo,
                                                 &pRsvpTeTempPrevInTnlInfo,
                                                 &bIsBkpTnl)) == RPTE_FAILURE)
            {
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "PTEAR : Message received on wrong interface \n");
                RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthProcessPathTearMsg : "
                            "INTMD-EXIT \n");
                return;
            }
            if ((pRsvpTeTnlInfo->b1IsPathMsgRecvOnBkpPath == RPTE_TRUE)
                && (bIsBkpTnl == RPTE_FALSE))
            {
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "RptePthProcessPathTearMsg : EXIT \n");
                return;
            }
            if ((RSVPTE_TNL_RRO_FLAGS (pRsvpTeTnlInfo) & LOCAL_PROT_IN_USE)
                == LOCAL_PROT_IN_USE)
            {
                pRsvpTeTnlInfo->u1SendToByPassTnl = RPTE_TRUE;
            }
            RptePthForwardPathTear (pPktMap, pRsvpTeTnlInfo);
            RpteUtlCleanPktMap (pPktMap);
            /* remove Tnl info */
            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
            RSVPTE_DBG (RSVPTE_FRR_DBG, "RptePthProcessPathTearMsg : EXIT \n");
            return;
        }

        pRsvpTeOrigTnlInfo = pRsvpTeTnlInfo;

        u1State = RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo);

        /* Checking whether received PATH Tear Message is valid, 
         * Also, whether PATH Tear Message is received on Backup Path or
         * Primary Path. If received on backup Path bIsBkpTnl==> YES,
         *               else  bIsBkpTnl==> NO.
         * Also, Previous Tunnel Pointer and current tunnel pointer are
         * retrieved. 
         */

        if ((RpteUtlIsUpStrMsgRcvdOnBkpPath (pPktMap, &pRsvpTeTnlInfo,
                                             &pRsvpTePrevInTnlInfo,
                                             &bIsBkpTnl)) == RPTE_FAILURE)
        {
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "PTEAR : Message received on wrong interface \n");
            RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthProcessPathTearMsg : "
                        "INTMD-EXIT \n");
            return;
        }

        /* If the PATH Tear is received on the Primary path and the 
         * current node state is MP and it has the backup tunnels 
         * Primary Tunnel Entry is not deleted and RSVP state is refreshed. */
        if (RPTE_IS_MP (u1State) && (bIsBkpTnl == RPTE_NO) &&
            (RPTE_FRR_IN_TNL_INFO (pRsvpTeOrigTnlInfo) != NULL))
        {
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "Path tear rcvd on primary path, Node state is MP,"
                        "Backup tunnel exist. So, Tunnel deletion skipped\n");
            PSB_TIME_TO_DIE (RSVPTE_TNL_PSB (pRsvpTeOrigTnlInfo)) =
                RpteUtlComputeLifeTime (PKT_MAP_IF_ENTRY (pPktMap),
                                        PSB_IN_REFRESH_INTERVAL (RSVPTE_TNL_PSB
                                                                 (pRsvpTeOrigTnlInfo)));
            RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo) |=
                RPTE_FRR_NODE_STATE_UPLOST;
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "PTEAR : MP - Backup Tunnel exist. "
                        "RSVP State Refreshed \n");
            RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthProcessPathTearMsg : EXIT \n");
            return;
        }

        /* The following condition occurs when a PATH Tear is received on 
         * backup path. */
        if (RPTE_IS_MP (u1State) && (bIsBkpTnl == RPTE_YES))
        {
            RptePrcsBkpPathTearOrTimeOut (pRsvpTeOrigTnlInfo,
                                          pRsvpTePrevInTnlInfo,
                                          pRsvpTeTnlInfo, &bIsOperDone);

            if (bIsOperDone == RPTE_YES)
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "PTEAR : MP - Operation completed \n");
                RpteUtlCleanPktMap (pPktMap);
                return;
            }

            /* This scenario will happen when there are 2 tnls
             * (Protected tunnel -> In Tunnel) and In Tunnel 
             * is getting deleted. Here, In Tunnel will be
             * completely freed and it should not be used
             * anymore. */
            if (pRsvpTePrevInTnlInfo == pRsvpTeOrigTnlInfo)
            {
                pRsvpTeTnlInfo = pRsvpTeOrigTnlInfo;
            }
        }

        /* The following code is verified for DMP case */
        if (RPTE_IS_DMP (u1State))
        {
            pRsvpTeInTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);

            /* If the PATH Tear is received on Primary backup path and 
             * "no" secondary backup path exist, PATH Tear is forwarded.
             *        Tunnel entry is removed. */
            if ((bIsBkpTnl == RPTE_NO) && (pRsvpTeInTnlInfo == NULL))
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "PTEAR: DMP - PATH Tear rcvd on primary path\n");

                RptePthForwardPathTear (pPktMap, pRsvpTeTnlInfo);
                RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);

                if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)
                    != RPTE_ZERO)
                {
                    RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
                }

                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "PTEAR : DMP - No Bkp Tnl Exist. PATH Tear sent\n");
                return;
            }
            if (RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeTnlInfo) ==
                RPTE_TRUE)
            {
                /* If timer has expired for the tunnel which was
                 * acting as downstream tunnel information, update it. */
                u1IsDownStrTnl = RPTE_YES;
            }
            if (bIsBkpTnl == RPTE_NO)
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "PTEAR : DMP - PATH Tear rcvd on primary path\n");

                /* If the PATH Tear is received on Primary backup path and
                 * some secondary backup path exist, Alternative secondary backup 
                 * path is added to the HASH Table. */
                /* Backup Tunnel Entry is removed. Node state is moved to UPLOST */
                RPTE_FRR_BASE_TNL (pRsvpTeInTnlInfo) = NULL;
                RPTE_FRR_NODE_STATE (pRsvpTeInTnlInfo) =
                    (RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo) |
                     RPTE_FRR_NODE_STATE_UPLOST);
                u4TnlInstance = RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);

                pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeInTnlInfo);
                while (pRsvpTeTmpTnlInfo != NULL)
                {
                    /* Base tunnel (Newly selected tnl) updation for the
                     * backup tunnels */
                    RPTE_FRR_BASE_TNL (pRsvpTeTmpTnlInfo) = pRsvpTeInTnlInfo;
                    pRsvpTeTmpTnlInfo =
                        RPTE_FRR_IN_TNL_INFO (pRsvpTeTmpTnlInfo);
                }

                if (RPTE_FRR_IN_TNL_INFO (pRsvpTeInTnlInfo) == NULL)
                {
                    RPTE_FRR_NODE_STATE (pRsvpTeInTnlInfo) &=
                        (UINT1)(~(RPTE_FRR_NODE_STATE_DMP));
                    RPTE_FRR_NODE_STATE (pRsvpTeInTnlInfo) &=
                        (UINT1)(~(RPTE_FRR_NODE_STATE_UPLOST));
                    RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST (pRsvpTeInTnlInfo)
                        = RPTE_TRUE;
                }

                RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
                if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)
                    != RPTE_ZERO)
                {
                    RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
                }

                RSVPTE_TNL_TNLINST (pRsvpTeInTnlInfo) = u4TnlInstance;
                RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (pRsvpTeInTnlInfo))
                    = u4TnlInstance;

                RSVPTE_DBG4 (RSVPTE_FRR_DBG,
                             "PT : DMP Tunnel %d %d %x %x added to hash table\n",
                             RSVPTE_TNL_TNLINDX (pRsvpTeInTnlInfo),
                             RSVPTE_TNL_TNLINST (pRsvpTeInTnlInfo),
                             OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeInTnlInfo)),
                             OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeInTnlInfo)));

                if (RBTreeAdd (gpRsvpTeGblInfo->RpteTnlTree,
                               (tRBElem *) pRsvpTeInTnlInfo) != RB_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_FRR_DBG,
                                "Failed to Add :RsvpTeTnlInfo to RpteTnlTree\n");
                    RpteUtlCleanPktMap (pPktMap);
                    return;
                }

                if (u1IsDownStrTnl == RPTE_YES)
                {
                    RSVPTE_DBG4 (RSVPTE_FRR_DBG,
                                 "Path Refresh for Tnl %d %d %x %x started\n",
                                 RSVPTE_TNL_TNLINDX (pRsvpTeInTnlInfo),
                                 RSVPTE_TNL_TNLINST (pRsvpTeInTnlInfo),
                                 OSIX_NTOHL (RSVPTE_TNL_INGRESS
                                             (pRsvpTeInTnlInfo)),
                                 OSIX_NTOHL (RSVPTE_TNL_EGRESS
                                             (pRsvpTeInTnlInfo)));

                    RptePhPathRefresh (pRsvpTeInTnlInfo);
                }

                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "PTEAR : DMP - Some Bkp Tnl Exist. PATH Tear Not "
                            "sent. Alt Bkp tnl added in Hash Table\n");
                RpteUtlCleanPktMap (pPktMap);
                return;
            }

            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "PTEAR: DMP - Path Tear rcvd on backup path\n");

            /* If the PATH Tear is received on secondary backup path,
             * Current Tunnel Pointer is removed from RsvpTeTnlInfo. */
            RPTE_FRR_IN_TNL_INFO (pRsvpTePrevInTnlInfo) =
                RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);

            /* Backup Tunnel Entry is removed. Node state is moved to UPLOST */
            RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo) |=
                RPTE_FRR_NODE_STATE_UPLOST;
            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_FALSE);

            if (RPTE_FRR_IN_TNL_INFO (pRsvpTeOrigTnlInfo) == NULL)
            {
                RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo) &=
                   (UINT1)(~(RPTE_FRR_NODE_STATE_UPLOST));
                RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo)
                    &= (UINT1)(~(RPTE_FRR_NODE_STATE_DMP));
            }

            if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
            {
                RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
            }
            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "PTEAR : DMP - Some Bkp Tnl Exist. PATH Tear not sent. "
                        "Alt Bkp tnl added to Hash Table\n");
            RpteUtlCleanPktMap (pPktMap);
            return;
        }
        /* The following code is verified for DMP case */
        if (u1State == RPTE_FRR_NODE_STATE_UNKNOWN)
        {
            pRsvpTeInTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);

            /* If the PATH Tear is received on Primary backup path and 
             * "no" secondary backup path exist, PATH Tear is forwarded.
             *        Tunnel entry is removed. */
            if ((bIsBkpTnl == RPTE_NO) && (pRsvpTeInTnlInfo == NULL))
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "PTEAR: Unknown - Path tear rcvd on primary path\n");

                RptePthForwardPathTear (pPktMap, pRsvpTeTnlInfo);
                RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);

                if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)
                    != RPTE_ZERO)
                {
                    RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
                }
                RpteUtlCleanPktMap (pPktMap);
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "No Bkp Tnl Exist. Path Tear sent.\n");
                return;
            }

            if (bIsBkpTnl == RPTE_NO)
            {
                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "PTEAR: Unknown - Path tear rcvd on primary path\n");

                /* If the PATH Tear is received on Primary backup path and
                 * some secondary backup path exist, Alternative secondary 
                 * backup path is added to the HASH Table. */

                /* Backup Tunnel Entry is removed. Node state is moved to UPLOST */
                RPTE_FRR_BASE_TNL (pRsvpTeInTnlInfo) = NULL;
                u1FrrNodeState = RPTE_FRR_NODE_STATE (pRsvpTeTnlInfo);
                u4TnlInstance = RSVPTE_TNL_TNLINST (pRsvpTeTnlInfo);
                pRsvpTeTmpTnlInfo = RPTE_FRR_IN_TNL_INFO (pRsvpTeInTnlInfo);
                while (pRsvpTeTmpTnlInfo != NULL)
                {
                    /* Base tunnel (Newly selected tnl) updation for the
                     * backup tunnels */
                    RPTE_FRR_BASE_TNL (pRsvpTeTmpTnlInfo) = pRsvpTeInTnlInfo;
                    pRsvpTeTmpTnlInfo =
                        RPTE_FRR_IN_TNL_INFO (pRsvpTeTmpTnlInfo);
                }

                RptePthForwardPathTear (pPktMap, pRsvpTeTnlInfo);

                RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
                RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                     TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
                if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)
                    != RPTE_ZERO)
                {
                    RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
                }

                RPTE_FRR_NODE_STATE (pRsvpTeInTnlInfo) = u1FrrNodeState;
                RSVPTE_DBG2 (RSVPTE_FRR_DBG,
                             "Value of Tnl Instance Old: %d New: %d\n",
                             RSVPTE_TNL_TNLINST (pRsvpTeInTnlInfo),
                             u4TnlInstance);
                RSVPTE_TNL_TNLINST (pRsvpTeInTnlInfo) = u4TnlInstance;
                RSVPTE_TNL_INSTANCE (RPTE_TE_TNL (pRsvpTeInTnlInfo))
                    = u4TnlInstance;

                RSVPTE_DBG4 (RSVPTE_FRR_DBG,
                             "PT: UNKN: Tunnel %d %d %x %x added to hash table\n",
                             RSVPTE_TNL_TNLINDX (pRsvpTeInTnlInfo),
                             RSVPTE_TNL_TNLINST (pRsvpTeInTnlInfo),
                             OSIX_NTOHL (RSVPTE_TNL_INGRESS (pRsvpTeInTnlInfo)),
                             OSIX_NTOHL (RSVPTE_TNL_EGRESS (pRsvpTeInTnlInfo)));
                if (RBTreeAdd (gpRsvpTeGblInfo->RpteTnlTree,
                               (tRBElem *) pRsvpTeInTnlInfo) != RB_SUCCESS)
                {
                    RSVPTE_DBG (RSVPTE_FRR_DBG,
                                "Failed to Add :RsvpTeTnlInfo to RpteTnlTree\n");
                    RpteUtlCleanPktMap (pPktMap);
                    return;
                }

                RpteUtlCleanPktMap (pPktMap);

                RSVPTE_DBG (RSVPTE_FRR_DBG,
                            "PTEAR : Unknown - Some Bkp Tnl Exist. PATH Tear "
                            "not sent. Alt Bkp tnl added to Hash Table\n");
                return;
            }

            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "PTEAR : Unknown - Path tear rcvd on backup path\n");

            /* If the PATH Tear is received on secondary backup path,
             * Current Tunnel Pointer is removed from RsvpTeTnlInfo. */
            RPTE_FRR_IN_TNL_INFO (pRsvpTePrevInTnlInfo) =
                RPTE_FRR_IN_TNL_INFO (pRsvpTeTnlInfo);

            RptePthForwardPathTear (pPktMap, pRsvpTeTnlInfo);

            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_FALSE);
            if (RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo) != RPTE_ZERO)
            {
                RSVPTE_FRR_DETOUR_INCOMING_NUM (gpRsvpTeGblInfo)--;
            }
            RpteUtlCleanPktMap (pPktMap);

            RSVPTE_DBG (RSVPTE_FRR_DBG,
                        "Path tear rcvd on secondary bkp path. Tnl removed\n");
            return;
        }
    }

    if (RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT (pPktMap) != RPTE_ZERO)
    {
        if ((RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) & RPTE_USTR_MSG_ID_PRESENT)
            && ((INT4) RPTE_TNL_USTR_MAX_MSG_ID (pRsvpTeTnlInfo) >=
                (INT4) RPTE_PKT_MAP_MSG_ID (pPktMap)))
        {
            RSVPTE_DBG (RSVPTE_PT_PRCS,
                        "Msg ID Out Of Order. Sliently Dropping sshhh....\n");
            RpteUtlCleanPktMap (pPktMap);
            RSVPTE_DBG (RSVPTE_PT_ETEXT,
                        "RptePthProcessPathTearMsg : INTMD-EXIT \n");
            return;
        }
        RPTE_TNL_MSG_ID_FLAG (pRsvpTeTnlInfo) |= RPTE_USTR_MSG_ID_PRESENT;
        RPTE_TNL_USTR_MAX_MSG_ID (pRsvpTeTnlInfo) =
            RPTE_PKT_MAP_MSG_ID (pPktMap);
    }

    /* If Egress, remove Tnl Info, else forward and remove Tnl Info */
    if (RSVPTE_TNL_NODE_RELATION (pRsvpTeTnlInfo) == RPTE_EGRESS)
    {
        /* if path tear msg is received for protection path and
         * protection in use bit of working path is set,
         * send resv tear for working path */

        if ((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
             MPLS_TE_DEDICATED_ONE2ONE) &&
            (pRsvpTeTnlInfo->pMapTnlInfo != NULL) &&
            (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType ==
             RPTE_TNL_PROTECTION_PATH)
            && (pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
                u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
        {
            pRsvpTeTnlInfo->pMapTnlInfo->pTeTnlInfo->
                u1TnlLocalProtectInUse = LOCAL_PROT_NOT_AVAIL;
            RpteRthSendResvTear (RSVPTE_TNL_RSB (pRsvpTeTnlInfo->pMapTnlInfo));
            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo->pMapTnlInfo) = RPTE_TRUE;
            RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo->pMapTnlInfo,
                                 TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
        }

		if((pRsvpTeTnlInfo->pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
				MPLS_TE_DEDICATED_ONE2ONE) &&
				(pRsvpTeTnlInfo->pTeTnlInfo->u1TnlPathType == RPTE_TNL_WORKING_PATH)
				&& (pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse ==
				LOCAL_PROT_AVAIL))
		{
				pRsvpTeTnlInfo->pTeTnlInfo->u1TnlLocalProtectInUse = LOCAL_PROT_NOT_AVAIL;
		}

        if (RPTE_TNL_RT_TIMER_BLOCK (pRsvpTeTnlInfo) == NULL)
        {
            /* If the node has sent out a ResvTear msg and is
             * waiting for its ack, the tunnel should not get
             * deleted, even during receipt of a path tear
             * message. */
            RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
        }
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                             TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthProcessPathTearMsg : EXIT \n");
        return;
    }

    if (PKT_MAP_MSG_BUF (pPktMap) != NULL)
    {
        BUF_RELEASE (PKT_MAP_MSG_BUF (pPktMap));
        PKT_MAP_MSG_BUF (pPktMap) = NULL;
    }
    if (PKT_MAP_RSVP_PKT_ALLOC (pPktMap) == RPTE_PKTMAP_IP_ALLOC)
    {
        MemReleaseMemBlock (RSVPTE_IP_PKT_POOL_ID,
                            (UINT1 *) PKT_MAP_RSVP_PKT (pPktMap));

        PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_NO_ALLOC;
    }
    else if (PKT_MAP_RSVP_PKT_ALLOC (pPktMap) == RPTE_PKTMAP_RSVP_ALLOC)
    {
        MemReleaseMemBlock (RSVPTE_RSVP_PKT_POOL_ID,
                            (UINT1 *) PKT_MAP_RSVP_PKT (pPktMap));
        PKT_MAP_RSVP_PKT_ALLOC (pPktMap) = RPTE_PKTMAP_NO_ALLOC;
    }

    if (RPTE_IS_FRR_CAPABLE (pRsvpTeTnlInfo) != RPTE_TRUE)
    {
        RptePthForwardPathTear (pPktMap, pRsvpTeTnlInfo);

        /* Remove Tunnel From Tunnel Priority List */
        RptePmRemoveTnlFromPrioList (pRsvpTeTnlInfo);
        /* remove Tnl info */
        RPTE_TNL_DEL_TNL_FLAG (pRsvpTeTnlInfo) = RPTE_TRUE;
        RpteRelTnlEntryFunc (gpRsvpTeGblInfo, pRsvpTeTnlInfo,
                             TE_TNL_CALL_FROM_SIG, RPTE_TRUE);
        RpteUtlCleanPktMap (pPktMap);
        RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthProcessPathTearMsg : EXIT \n");
        return;
    }
    /* FRR PLR processing */
    /* If FAST_REROUTE is not present, default method followed is 
     * One-to-One. */
    if (RPTE_IS_PLR_OR_PLRAWAIT (RPTE_FRR_NODE_STATE (pRsvpTeOrigTnlInfo)))
    {
        /* If node state is PLR or PLR_AWAIT and if local protection is 
         * not in use, then PATH Tear is duplicated on both path */
        RpteHandlePlrDown (pRsvpTeOrigTnlInfo);
    }
    RpteUtlCleanPktMap (pPktMap);
    RSVPTE_DBG (RSVPTE_PT_ETEXT, "RptePthProcessPathTearMsg : EXIT \n");
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file rptepter.c                             */
/*---------------------------------------------------------------------------*/
