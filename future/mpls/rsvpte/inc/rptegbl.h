
/********************************************************************
 *
 * $Id: rptegbl.h,v 1.7 2018/01/03 11:31:22 siva Exp $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptegbl.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the global variable
 *                             declarations associated with the RSVP TE Support.
 *---------------------------------------------------------------------------*/

#ifndef _RPTEGBL_H
#define _RPTEGBL_H

INT4                gi4MsgAlloc;
INT4                gi4MemAlloc;

tRsvpTeGblInfo      gRsvpTeGblInfo;
tRsvpTeGblInfo     *gpRsvpTeGblInfo = &gRsvpTeGblInfo;

/*
 * Following array is intialised with of function pointers that are invoked
 * to handle RSVPTE's Internal Events.
 */
tRsvpProcEvtFuncPtr     gaRpteProcEvtFunc[] = {
    NULL,
    RpteTEProcessTeEvent,
    RpteProcessL3VPNEvent,
    RpteTEProcessTeEvent,
    RpteProcessLspSetupEvent,
    RpteProcessLspDestroyEvent,
 NULL,
 RpteProcessReoptTnlManualTrigger
};

UINT4               gaRsvpTeIpv4AddrMask[33] = {
    0x00000000, 0x80000000, 0xC0000000, 0xE0000000,
    0xF0000000, 0xF8000000, 0xFC000000, 0xFE000000,
    0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
    0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000,
    0xFFFF0000, 0xFFFF8000, 0xFFFFC000, 0xFFFFE000,
    0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
    0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0,
    0xFFFFFFF0, 0xFFFFFFF8, 0xFFFFFFFC, 0xFFFFFFFE,
    0xFFFFFFFF
};

UINT1  gu1TeAdminFlag = RPTE_FALSE;

#endif /* _RPTEGBL_H */

/*---------------------------------------------------------------------------*/
/*                        End of file rptegbl.h                              */
/*---------------------------------------------------------------------------*/
