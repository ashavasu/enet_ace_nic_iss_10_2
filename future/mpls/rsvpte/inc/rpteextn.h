
/* $Id: rpteextn.h,v 1.6 2012/11/08 11:29:35 siva Exp $*/
/********************************************************************
 *
 * $RCSfile: rpteextn.h,v $
 *
 * $Date: 2012/11/08 11:29:35 $
 *
 * $Revision: 1.6 $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rpteextn.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the extern global variable
 *                             declarations associated with the RSVP TE Support.
 *---------------------------------------------------------------------------*/

#ifndef _RPTEEXTN_H
#define _RPTEEXTN_H

extern tRsvpTeGblInfo *gpRsvpTeGblInfo;

extern tRsvpTeGblInfo gRsvpTeGblInfo;

extern tRsvpProcEvtFuncPtr gaRpteProcEvtFunc[];

extern UINT4        gaRsvpTeIpv4AddrMask[];
extern UINT1 MplsMlibUpdate ARG_LIST ((UINT2 u2MlibOperation, tLspInfo * pLspInfo));
extern VOID
MplsGetDnStrInfoUsingRecoveryLbl ARG_LIST ((UINT4 u4RecoveryLabel, tTeTnlInfo **pTeTnlInfo,
                                            UINT4 *pu4OutLabel));
extern VOID
MplsGetDnStrInfoUsingRecoveryPath ARG_LIST ((UINT4 u4RecoveryLabel, UINT4 u4NextHop,
                                             tTeTnlInfo **pTeTnlInfo, UINT4 *pu4OutLabel));

#endif /* _RPTEEXTN_H */

/*---------------------------------------------------------------------------*/
/*                        End of file rpteextn.h                             */
/*---------------------------------------------------------------------------*/
