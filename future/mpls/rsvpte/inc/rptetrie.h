#ifndef _RPTETRIE_H
#define _RPTETRIE_H
/*
 * The routing table node structure
 */
typedef struct rptekey
{
    union 
    {
	    UINT4               au4Key1[2];
	    UINT4               u4Key;
    }
    rpte_key_u;
#define  u8_Key  rpte_key_u.au4Key1  
#define  u4_Key  rpte_key_u.u4Key  
}
tRpteKey;

typedef struct rptetree_node
{

    INT1                    i1Bitkey;   /* The bit key for facilitating the 
                                         * lookup and add into the tree
                                         */
    INT1                    i1Flags;    /* The flag telling what type of node
                                         * it is 
                                         */
    UINT1                   u1KeySize;                              
    UINT1                   u1Reserved1;
    UINT4                   u4Depth;    /* The depth in the tree */
    struct rptetree_node   *pParent;    /* The pointer to the parent node */
    struct rptetree_node   *p_left;     /* The pointer to the left node */
    struct rptetree_node   *p_right;    /* The pointer to the right node */
    struct rpte_entry      *p_leaf;     /* The pointer to the routing table 
                                         * table entry
                                         */
}
tRpteTreeNode;


/* 
 * The routing table entry structure
 */
typedef struct rpte_entry
{
    tRpteKey            rpteKey;    
    tuTrieInfo         *pTrieInfo;  /* Change For 64 Bit */
    tRpteTreeNode      *pBack;    /* The back pointer to the tree node
                                 * on which it is attached
                                 */
    UINT2               u2Rsvd;
    UINT1               u1Prefixlen;
    UINT1               u1Rsvd;                              
}
tRpteEntry;


typedef struct rpteTrieGbl
{
    tRpteTreeNode    rpte64bitTrieRoot;
    tRpteTreeNode    rpte32bitTrieRoot;
    tRpteEntry       rpte64nullLeafEntry;
    tRpteEntry       rpte32nullLeafEntry;
    UINT1            u1Rpte64TrieAllocFlag;
    UINT1            u1Rpte32TrieAllocFlag;
    UINT2            u2Rsvd;
}
tRpteTrieGbl;

typedef VOID (*tDelFnType)(tuTrieInfo *pTrieInfo); 

#define RPTE_TREE_ROOT                      0x01
#define RPTE_TREE_BACKTRACK                 0x02
#define RPTE_TREE_NO_BACKTRACK              0x04

#define KEY_SIZE_4                          4
#define KEY_SIZE_8                          8
#define PRE_LEN_64                          64
#define PRE_LEN_32                          32

#define LSHIFT_2BITS              2
#define RSHIFT_2BITS              2
#define LSHIFT_5BITS           0x05
#define RSHIFT_5BITS           0x05
#define MODULUS_32_BIT_FLAG    0x1f
#define RSVPTE_HEX_32          0x20
#define RSVPTE_32                32
#define RSVPTE_64                64

#define RPTE_TRIE_LEAF_POOL_ID(u1Keysize) \
        (((u1Keysize) == KEY_SIZE_8) ? RPTE_64_BIT_TRIE_LEAF_POOL_ID : \
	 RPTE_32_BIT_TRIE_LEAF_POOL_ID)
#define RPTE_TRIE_NODE_POOL_ID(u1Keysize) \
        (((u1Keysize) == KEY_SIZE_8) ? RPTE_64_BIT_TRIE_NODE_POOL_ID : \
	 RPTE_32_BIT_TRIE_NODE_POOL_ID)
#define RPTE_64_BIT_TRIE_NODE_POOL_ID  \
    		RSVPTEMemPoolIds[MAX_RSVPTE_64_BIT_TRIE_NODE_SIZING_ID] 
#define RPTE_32_BIT_TRIE_NODE_POOL_ID \
      		RSVPTEMemPoolIds[MAX_RSVPTE_32_BIT_TRIE_NODE_SIZING_ID] 
#define RPTE_64_BIT_TRIE_LEAF_POOL_ID \
       		RSVPTEMemPoolIds[MAX_RSVPTE_64_BIT_TRIE_LEAF_NODE_SIZING_ID] 
#define RPTE_32_BIT_TRIE_LEAF_POOL_ID \
		RSVPTEMemPoolIds[MAX_RSVPTE_32_BIT_TRIE_LEAF_NODE_SIZING_ID]
#define RPTE_64_BIT_TRIE_ALLOC_FLAG \
	(gpRsvpTeGblInfo->pRsvpteTrieGbl->u1Rpte64TrieAllocFlag)
#define RPTE_32_BIT_TRIE_ALLOC_FLAG \
	(gpRsvpTeGblInfo->pRsvpteTrieGbl->u1Rpte32TrieAllocFlag)
#define RPTE_64_BIT_TRIE_ROOT_NODE \
	(gpRsvpTeGblInfo->pRsvpteTrieGbl->rpte64bitTrieRoot)
#define RPTE_32_BIT_TRIE_ROOT_NODE \
	(gpRsvpTeGblInfo->pRsvpteTrieGbl->rpte32bitTrieRoot)
#define RPTE_64_BIT_TRIE_NULL_LEAF_ENTRY \
	(gpRsvpTeGblInfo->pRsvpteTrieGbl->rpte64nullLeafEntry)
#define RPTE_32_BIT_TRIE_NULL_LEAF_ENTRY \
	(gpRsvpTeGblInfo->pRsvpteTrieGbl->rpte32nullLeafEntry)

INT4 RpteCreateTrie ARG_LIST ((UINT1 u1KeySize));
INT4 RpteTrieLookupEntry ARG_LIST ((tRpteTreeNode *pRpteTrieRoot, 
			            tRpteKey *pKey, 
			            tuTrieInfo **ppTrieInfo));
INT4 RpteTrieAddEntry ARG_LIST (( tRpteTreeNode      *pRpteTrieRoot, 
                                  tRpteKey           *pKey, 
                                  tuTrieInfo *pTrieInfo));
tRpteEntry * RpteTrieEntryFill ARG_LIST (( const tRpteKey           *pKey,
                                           UINT1               u1KeySize,
                                           tuTrieInfo *pTrieInfo));
INT4 RpteAddTrie ARG_LIST (( tRpteTreeNode      *pRpteTrieRoot,
                             tRpteEntry         *pRpteEntry));

INT4 RpteTrieDeleteEntry ARG_LIST (( tRpteTreeNode      *pRpteTrieRoot,
                                     tRpteKey           *pKey,
                                     UINT1               u1PrefixSize, 
                                     tDelFnType          pDelFn));
VOID RpteDeleteTreeNode ARG_LIST (( tRpteTreeNode       *pNode));
VOID RpteDeleteSubTree ARG_LIST (( tRpteTreeNode       *pNode,
                                   tDelFnType          pDelFn));

VOID RptePrintTreeNode ARG_LIST (( const tRpteTreeNode       *pNode));
INT4 RpteTrieKeyMatch ARG_LIST (( const tRpteKey           *pK1,
                                  const tRpteKey           *pK2,
                                  INT4                i4Prefixlen,
                                  UINT1               u1KeySize));

INT4 RpteTrieBitSet ARG_LIST (( const tRpteKey           *pKey,
                                UINT1               u1KeySize,
                                INT4                i4FnBit));

INT4 RpteKeyDiff ARG_LIST (( const tRpteKey           *pK1,
                             const tRpteKey           *pK2,
                             UINT1               u1KeySize));

int RpteTestBit ARG_LIST (( INT4 i4Nr, INT4 *p_i4_key));

#endif
