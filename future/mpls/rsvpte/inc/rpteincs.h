

/* $Id: rpteincs.h,v 1.9 2012/10/29 12:07:20 siva Exp $*/
/********************************************************************
 *
 * $RCSfile: rpteincs.h,v $
 *
 * $Date: 2012/10/29 12:07:20 $
 *
 * $Revision: 1.9 $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rpteincs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains all the associated include
 *                             files for the RSVP-TE support.
 *---------------------------------------------------------------------------*/

#ifndef _RPTEINCS_H
#define _RPTEINCS_H


/* Common Include Files */
#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "include.h"
#include "mplcmndb.h"
/* label manager  includes */
#include "lblmgrex.h"

/* Trie includes */
#include "trieapif.h"
#include "triecidr.h"

/* mplsFM includes */
#include "mplssize.h"
#include "mplsdiff.h"
#include "mpls.h"
#include "mplsdefs.h"
#include "mplfmext.h"
#include "mplsutil.h"
/* Te includes */
#include "temacs.h"
#include "teextrn.h"
#include "tedsdefs.h"
#include "tedsmacs.h"

/* MPLS-TC includes */
#include "tcextrn.h"
#include "tcapi.h"

#include "mplsdsrm.h"
#include "dsrmgblex.h"

/* Mpls NP include */
#include "mplsnpex.h"
#ifdef TLM_WANTED
#include "tlm.h"
#endif
/* RSVP TE related files */
#include "rpteext.h"
#include "rptedefs.h"
#include "rptdsdef.h"
#include "rptete.h"
#include "rsvptdf1.h"
#include "rptdstdf.h"
#include "rsvptdf2.h"
#include "rptetrie.h"
#include "rsvpglob.h"
#include "rptetdf2.h"
#include "rptdsprt.h"
#include "rptemacs.h"
#include "rptedsmc.h"
#include "rpteprot.h"
#include "fsrsvlow.h"
#include "rpteport.h"
#include "rpteextn.h"
#include "rptedbg.h"
#include "rsvpprot.h"
#include "rptedste.h"
#include "rsvptesz.h"
#include "cli.h"
#include "iss.h"
#include "csr.h"
#ifdef SNMP_2_WANTED
#include "fsmplswr.h"
#endif
#endif /* _RPTEINCS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rpteincs.h                             */
/*---------------------------------------------------------------------------*/
