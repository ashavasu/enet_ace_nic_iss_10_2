
/********************************************************************
 * $Id: rptetdf2.h,v 1.38 2018/01/03 11:31:23 siva Exp $
*******************************************************************/
/********************************************************************
 *
 * $RCSfile: rptetdf2.h,v $
 *
 * $Date: 2018/01/03 11:31:23 $
 *
 * $Revision: 1.38 $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptetdf2.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the structure type
 *                             definitions declared and used for RSVP-TE 
 *                             funcitonality. 
 *---------------------------------------------------------------------------*/

#ifndef _RPTETDF2_H
#define _RPTETDF2_H
#include "rptetdf1.h"
#include "rptetrie.h"


/* Structure to hold information associated with a RSVP-TE Tunnel. */
typedef struct _RsvpTeTnlInfo
{
    tRBNodeEmbd         RpteTnlRbNode;
    tTMO_SLL_NODE       NextHoldPrioDnStrTnl;
    tTMO_SLL_NODE       NextHoldPrioUpStrTnl;
    tTMO_SLL_NODE       NextPreemptTnl;
    tTMO_SLL            StackTnlList;
    tTMO_SLL_NODE       NextStackTnl;

    /* Protected tnls for which this is acting as backup tunnel */
    tTMO_DLL            FacFrrList;
    tTMO_DLL_NODE       FacFrrProtTnl;

    /* Protected tnls for which this is acting as backup tunnel */
    tTMO_DLL            FacInFrrList;
    tTMO_DLL_NODE       FacInFrrProtTnl;

    
    tTMO_DLL_NODE       UpStrNbrTnl; /* Node for tnl list in the UpStream
                                        neighbour entry associating tunnels
                                        associated with that neighbour */
    tTMO_DLL_NODE       DnStrNbrTnl; /* Node for tnl list in the DownStream
                                        neighbour entry associating tunnels
                                        associated with that neighbour */
    uLabel              DownstrInLbl;
    uLabel              DownstrOutLbl;
    /* This varibel denotes the incoming label information of Reverse 
     * direction tunnel */
    uLabel              UpStrInLbl;   
   /* This varibel denotes the outgoing label information of Reverse
    *  direction tunnel */
    uLabel              UpStrOutLbl;
    tTMO_SLL            NewSubObjList;
    /* This varibale denotes the informatiion about Generic Label Request 
     * object */
    tGenLblReq            GenLblReq;
    /* This variable denotes the Admin status timer parameters
     * for this tunnel */
    tTimerParam         AdminStatusTmrParam;
    tTmrAppTimer        AdminStatusTimer;
    tAtmRsvpLblRngEntry AtmLblRngEntry;
    tTimerParam         RROTmrParam;
    tTmrAppTimer        RROTnlTmr;
    tTmrAppTimer        RecoveryPathTimer;
    tTimerParam         RecoveryPathTmrParam;
    tIpv4Addr           ExtnTnlId;
    tTMO_SLL_NODE       aNextPreemptTnlInPSC[RPTE_DS_MAX_NO_OF_PSCS];
    struct NeighbourEntry  *pUStrNbr;
    struct NeighbourEntry  *pDStrNbr;
    tTimerBlock        *pPETimerBlock;
    tTimerBlock        *pRETimerBlock;
    tTimerBlock        *pPTTimerBlock;
    tTimerBlock        *pRTTimerBlock;
    tTimerBlock         *pRPTimerBlock;
    tTePathListInfo     *pInTePathListInfo;
    tTePathInfo         *pTePathInfo; 
    tTePathListInfo     *pFrrOutTePathListInfo; /* Used to for facility backup 
                                                   node protection case, this list 
                                                   holds the complete ER-HOP list 
                                                   except the node to be avoided */
    tTePathInfo         *pFrrOutTePathInfo; /* Used to for facility backup
                                               node protection case, this list
                                               holds the complete ER-HOP list
                                               except the node to be avoided */
    tTeTnlInfo          *pTeTnlInfo; 
    struct PathStateBlock *pPsb;
    struct ReserveStateBlock *pRsb;
    
    /* Incoming onetoone or facility tunnel information */
    struct _RsvpTeTnlInfo *pInFrrTnlInfo; 
    /* Outgoing onetoone or facility tunnel information */
    struct _RsvpTeTnlInfo *pOutFrrProtTnlInfo;
    /* Base tnl information the one which has the in the HASH table */    
    struct _RsvpTeTnlInfo *pBaseFrrProtTnlInfo;

    struct _RsvpTeTnlInfo *pMapTnlInfo;

    tTmrAppTimer        CspfTmr;
    tTimerParam         CspfTmrParam;
    tTmrAppTimer        GblRevertTmr;
    tTimerParam         GblRevertTmrParam;
 
 /* Reoptimize timers for Lsp Reoptimization*/
    tTmrAppTimer        ReoptimizeTmr;
    tTimerParam         ReoptimizeTmrParam;
    tTmrAppTimer        EroCacheTmr;
    tTimerParam         EroCacheTmrParam;
    tTmrAppTimer        ReoptLinkUpWaitTmr;
    tTimerParam         ReoptLinkUpWaitTmrParam;
 
    tTmrAppTimer        EvtToL2vpnTmr;
    tTimerParam         EvtToL2vpnTmrParam;
    UINT4               au4ExcludeHop[MPLS_TE_MAX_HOPS];

    UINT4              *pStackTnlHead;
    UINT4               au4Handles[RPTE_DS_MAX_NO_OF_PSCS];
    UINT4               u4MRTime;
    UINT4               u4DnNextHop;
    UINT4               u4InPathMsgId;
    UINT4               u4OutPathMsgId;
    UINT4               u4InResvMsgId;
    UINT4               u4OutResvMsgId;
    UINT4               u4UstrMaxMsgId;
    UINT4               u4DstrMaxMsgId;

    /* This variable denotes upstream data channel TE-Link Interface Id */
    UINT4               u4UpStrDataTeLinkIfId;

    /* This variable denotes upstream data channel TE-Link Interface IP */
    UINT4               u4UpStrDataTeLinkIfAddr;

    /* This variable denotes downstream data channel TE-Link Interface Id */
    UINT4               u4DnStrDataTeLinkIfId;

    /* This variable denotes downstream data channel  TE-Link Interface IP */
    UINT4               u4DnStrDataTeLinkIfAddr;

    /* Refers to Other End of FA LSP. To be Used by E2E LSP for 
     * Sending Path Msg*/
    UINT4               u4FALspSrcOrDest;
    /* Resource color of the Te Link */
    UINT4               u4RsrcClassColor;
  /* Reoptimization Support, store the value of error link address received in
     * path error message.
     * In case of upstream link maintenance, so that Mid-point 
     * can request a PATH from CSPF as Link-address to exclude when 
     * there is no entry found for tunnel in ERO Cache.*/

    UINT4         u4ErrReoptLinkAddress;
    UINT4         u4ErrReoptLinkId;
 /*Reoptimization support, store the Address of link for which link maintenance
     *occured. */
    UINT4         u4ReoptLinkMaintainanceAddr;
    UINT4         u4ReoptLinkMaintainanceId;
    UINT2               u2L3pid;
    UINT2               u2BTime;
    /* This variable denotes the Explicit route object size 
     * of this tunnel */
    UINT2               u2EroSize;
    /* Specifies the C-Type of LSP_TNL_IF_ID_OBJ */
    UINT2               u2HlspCType;
    UINT2               u2RecoveryPathEroSize;
    UINT1               au1PscFound[RPTE_DS_MAX_NO_OF_PSCS];
    UINT1               u1Status;
    UINT1               u1MsgIdFlag;
    UINT1               u1DelTnlFlag;

    UINT1               u1RROErrFlag;
    UINT1               u1ReRouteTnl; /* Intermediate case it holds the FRR 
                                         CAPABLE flag value */
    UINT1               u1FrrNodeState; /* Unknown, head-end, head-end + PLR,
                                           PLR, PLR + MP, MP, MP + tail-end, 
                                           tail-end*/

    UINT1               u1RsvpTnlInfoType; /* This info. is for
                                              protected LSP or backup LSP */
    UINT1               u1RsvpCspfInfo; 
    UINT1               u1CspfIntCnt;
    UINT1               u1StackTnlBit;

    UINT1               u1FrrCapable;   /* Is Frr Capable */
    UINT1               u1DetourGrpId;
    UINT1               u1GblRevertFlag;
    /* This variable is used to find CSPF request is made for
     * which path (PRIMARY, MAKE BEFORE BREAK, BACKUP) */
    UINT1               u1CSPFPathRequested;

    
    UINT1               u1SendToByPassTnl; 
    UINT1               u1RroFlag;
    UINT1               u1LeastHopsToReachDest; /* RsvpTeTnlInfo which has the least no. of ER-Hops to
                                              reach the destination incase of DMP. */
    UINT1               u1IsBkpPathMsgToCfa;

    /* The below flag indicates whether control and data channels are separated
     * in the down stream direction */
    BOOL1               b1DnStrOob;
    
    /* The below flag indicates whether control and data channels are separated
     * in the upstream direction */
    BOOL1               b1UpStrOob;

    UINT1               u1NotifyMsgProtState;
    UINT1               u1NoOfExcludeHop;
    UINT1               u1IsGrTnl;
  /* Is set in case of Facility FRR when path message of protected tunnel is received 
    on MP node by Back up path */  
  BOOL1       b1IsPathMsgRecvOnBkpPath;
  /* Is used to increase rsb time to die once at wait timeout occurs in case of facility 
    * backup method, so that reserve message received from MP */  
 BOOL1       b1IsProcessWaitTimeOutForProtTunnel;
  /* Is used to block path refresh towards MP in case of Facility method when we hold the 
     * path-tear message for some time */ 
    BOOL1       b1IsPsbTimeToDieIncreasedForProtTunnel;
 /* This variable is introduced to support Reoptimization,
     * to store Node State like - Mid-Point Node */
 UINT1      u1ReoptimizeNodeState;   
 BOOL1    b1IsReoptimizeTimerState;
 BOOL1    b1IsPathRevaluationRequest;
 BOOL1    b1IsPreferablePathExist;
 UINT1               u1ReoptMaintenanceType;
 UINT1               u1ReoptTriggerType;
 BOOL1    b1IsReversionTriggered;
 BOOL1    b1IsTimerStarted;
 UINT1    au1Reserved[3];
} 
tRsvpTeTnlInfo;


typedef struct _MsgL3VpnRpteTnlInfo
{
    UINT4 u4TnlIfIndex;
    UINT4 u4RevTnlIfIndex;
    UINT4 u4WorkTnlIfIndex;
    UINT4 u4ProTnlIfIndex;
    UINT1 u1TnlOperStatus;
    UINT1 u1ProTnlOperStatus;
    UINT1 u1TnlLocalProtectInUse;
    UINT1 u1Padding[1];
}tMsgL3VpnRpteTnlInfo;


typedef struct t_RsvpTeL3VpnQMsg
{
    UINT4                    u4MsgType;
    UINT4                    u4Flag;
    UINT4                    u4ContextId;
    tMsgL3VpnRpteTnlInfo     L3VpnRpteMsg;

}tRsvpTeL3VpnQMsg;

typedef struct _NotifyRpteTnlInfo
{

    VOID     (*pRpteL3VpnNotifyLspStatusChange) (tMsgL3VpnRpteTnlInfo L3VpnRpteUpdateMsg);
    UINT2    u2MaskType;
    /* Possible values:
     * RSVP_L3VPN_LSP_DOWN_REQ
     * MPLS_L3VPN_LSP_UP_REQ
     * MPLS_L3VPN_TNL_DEL
     */
    UINT1    u1Padding[2];
}tNotifyRpteTnlInfo;


typedef struct _FacilityRpteBkpTnl
{
 /* Fast Reroute Facility backup tunnel information
  *  * assciated with the interface */

 tTMO_DLL_NODE       FacilityRpteBkpTnl;
 UINT4               u4TnlIfIndex;
 UINT4               u4TunnelIndex;
 UINT4               u4TunnelIngressLSRId;
 UINT4               u4TunnelEgressLSRId;
 tRsvpTeTnlInfo   *pRsvpTeTnlInfo;

}tFacilityRpteBkpTnl;

typedef struct _LblSubObjInfo
{
    tTMO_SLL_NODE       NextSubNode;
    uLabel             Label;
    UINT1               u1Type;
    UINT1               u1Len;
    UINT1               u1Flags;
    UINT1               u1Rsvd;
    UINT4               u4NodePos;
}
tLblSubObjInfo;

typedef struct _NewSubObjInfo
{
    tTMO_SLL_NODE       NextSubNode;
    UINT1               u1SubObjLen;
    UINT1               u1Rsvd;
    UINT2               u2Rsvd;
    UINT1               *pSubObj;
    UINT4               u4NodePos;

}
tNewSubObjInfo;


typedef struct _RsvpTeCfgParams
{
    tIpv4Addr           MplsTeLsrId;
    UINT4               u4HellointrvlTime;
    UINT4               u4RRCapable;
    UINT4               u4MsgIdCapable;
    UINT4               u4MinTnlsWithMsgId;
    /*This variable denotes the Notify Retransmit interval time value */
    UINT4               u4NotifRetransmitIntvl;
    /*This variable denotes the Notify Retransmit Decay value */
    UINT4               u4NotifRetransmitDecay;
    /*This variable denotes the Notify Retransmit Limit value */
    UINT4               u4NotifRetransmitLimit;
    /* This variable denotes the Admin Status timer value */
    UINT4               u4AdminStatusTimerValue;
    /* This variable denotes the Reoptimize Timer Value 
   * introduced in RFC 4736*/
 UINT4    u4ReoptimizeTime;
    /* This variable denotes the Reoptimize Timer Value 
   * introduced in RFC 4736*/
 UINT4    u4EroCacheTime;
    /* This variable denotes the restart timer value */
    UINT2               u2RestartTime;
    /* This variable denotes the recovery timer value */
    UINT2               u2RecoveryTime;
    UINT2               u2MaxIfaces;
    UINT2               u2MaxNeighbours;

    UINT2               u2MaxRsvpTeTnls;
    UINT2               u2MaxTempErHops;

    UINT2               u2MaxTempArHops;
    UINT2               u2MaxNewSubObjPerTnl;

    /* This variable denotes whether Path State Removed Feature is 
     * enabled */
    UINT1               u1PathStateRemovedFlag; 
    /* This variable denotes whether Notification feature is
     * enabled */
    UINT1               u1NotificationEnabled;
    /* This variable denotes whether Label set feature is
     * enabled */
    UINT1               u1LabelSetEnabled;
    /* This variable denotes whether Admins Status capability is 
     * enabled */
    UINT1               u1AdminStatusCapable;
    /* This variable denotes whether Admins Status capability is 
     * configured internally for protection */
    UINT1               u1IntAdminStatusCapable;

    UINT1               u1QoSSupprt;
    UINT1               u1HelloSprtd;
    UINT1               u1SockSupprt;
    UINT1               u1RmdPolicyObject;

    UINT1               u1BundleSize;
    UINT1               u1BundleTnlThresh;
    UINT1               u1MakeAfterBreakEnabled;
    /* This variable denotes whether GR capability is
     * enabled */
    UINT1               u1GRCapability;
    /* This variable denotes whether RecoveryPath message capability 
     * is enabeled */
    UINT1               u1RecoveryPathCapability;
    UINT1               u1Rsvd[2];
}
tRsvpTeCfgParams;

typedef struct NotifyRecipient
{
    tRBNodeEmbd      NotifyMsgTnlRbNode;
    tTMO_SLL         NotifyMsgTnlList;
    UINT4            u4RecipientAddr;
    tTimerParam      NotifyTimerParam;
    tTmrAppTimer     NotifyTimer;
    UINT4            u4MsgId;
    UINT4            u4MsgSize;
    UINT4            u4RetryInterval;
    UINT4            u4RetryLimit;
    BOOL1            b1IsTimerStarted;
    UINT1            u1ErrorCode;
    UINT2            u2ErrorValue;
}
tNotifyRecipient;

typedef struct NotifyMsgTnl
{
    tTMO_SLL_NODE         NextTnl;
    UINT4                 u4MsgType;
    UINT4                 u4MsgSize;
    tRsvpTeSsnObj         RsvpTeSsnObj;
    tSenderTspecObj       SenderTspecObj;
    tRsvpTeSndrTmpObj     RsvpTeSndrTmpObj;
    tRsvpTeFilterSpecObj  RsvpTeFilterSpecObj;
}
tNotifyMsgTnl;

typedef struct RecNotifyTnl
{
    tTMO_SLL_NODE      NextRecNtfyTnl;
    UINT4              u4TunnelIndex;
    UINT4              u4TunnelInstance;
    UINT4              u4TunnelIngress;
    UINT4              u4TunnelEgress;
}
tRecNotifyTnl;

typedef struct _RsvpTeGblInfo
{

    tTimerListId        RsvpTimerList;
    tTimerParam         HelloTimerParam;
    tTmrAppTimer        HelloTimer;
    tTimerParam         SockTimerParam;
    tTmrAppTimer        SockPollTmr;
    /* For GR - Recovery timer */
    tTimerParam         GrTimerParam;
    tTmrAppTimer        GrTimer;
    tRsvpTeCfgParams    RsvpTeCfgParams;
    tTMO_HASH_TABLE    *pIfHashTable;
    struct NeighbourEntry *pNbrTable;
    tRBTree             RpteTnlTree;
    tRpteTrieGbl       *pRsvpteTrieGbl;
    tOsixTaskId        gRsvpTeTaskId;
    tOsixSemId          gRsvpTeSemId;
    tOsixQId            gRsvpQId;      
    tRBTree             NotifyRecipientTree;
    tRBTree             RpteFrrTnlPortMappingTree;
#ifdef LNXIP4_WANTED
    tOsixQId            gRsvpIpQId;      
#endif    
    tOsixQId            gRsvpRtChgNtfQId;      
    tOsixQId            gRsvpIfChgNtfQId;      
    tOsixQId            gRsvpCspfQId;      

    UINT4               u4GenLblSpaceMinLbl;
    UINT4               u4GenLblSpaceMaxLbl;
    UINT4               u4RsvpTeDumpLvl;
    UINT4               u4RsvpTeDumpDirn;
    UINT4               u4RsvpTeDumpType;
    UINT4               u4OverRide;
    UINT4               u4RsvpTeDbgFlag;
    UINT4               u4RsvpTeDbgLvl; 
    UINT4               u4DetourIncomingNum;
    UINT4               u4DetourOutgoingNum;
    UINT4               u4DetourOrigNum;
    UINT4               u4FrrSwitchOverNum;
    UINT4               u4FrrConfIfNum;
    UINT4               u4FrrActProtIfNum;
    UINT4               u4FrrConfProtTunNum;
    UINT4               u4FrrActProtTunNum;
    UINT4               u4FrrActProtLspNum;
    UINT4               u4FrrCspfRetryInterval;
    UINT4               u4FrrCspfRetryCount;
    UINT4               u4TnlInstance;
    INT4                i4RsvpSockFd;
    INT4                i4RpteUdpReadSockFd;
    INT4                i4RpteUdpWriteSockFd;

    UINT2               u2GenbLblSpaceGrpId;
    UINT1               u1RevertiveMode;
    UINT1               u1DetourMergingCapable;

    UINT1               u1DetourCapable;
    UINT1               u1FrrNotifsEnabled;
    UINT1               u1RsvpTeAdminStatus;
    UINT1               u1AtmMergeSupport;

    UINT1               u1DetourGrpId;
    UINT1               u1RpteInitialised;
    /*This variable denotes the GR state of the node. 6 values are
     * applicable. 1.Not started, 2. GR shutdown in progress,
     * 3. Restart in progress, 4. Aborted 5. Recovery in progres,
     * 6. Completed */
    UINT1               u1GrProgressState;
    UINT1               au1Rsvd[1];
}
tRsvpTeGblInfo;
/*This is the temporary structure used to get te tunnel information
 * for tetnlinfo structure*/
typedef struct _RsvpTeGetTnlInfo
{
    UINT1              u1EncodingType;
    UINT1              u1SwitchingType;
    UINT2              u2Gpid;
}
tRsvpTeGetTnlInfo;
/* function pointers */
typedef UINT1       (*tRsvpProcEvtFuncPtr) (tRpteEnqueueMsgInfo *);

typedef tMplsRtEntryInfo tRsvpteRtEntryInfo;

/* This structure is used to create the mapping between FRR Backup Tunnels 
 * to its corresponding interfaces it is protecting */
typedef struct _RsvpTeFrrTnlPortMapping
{
    tRBNodeEmbd        RpteFrrTnlPortMappingRbNode;
    UINT4              au4L3Intf[MAX_RSVPTE_L3_INF_ARRAY_INDEX];
    UINT2              u2TnlId;
    UINT2              au2Rsvd[1];              
}
tRsvpTeFrrTnlPortMapping;

#endif /*_RPTETDF2_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file rptetdf2.h                             */
/*---------------------------------------------------------------------------*/
