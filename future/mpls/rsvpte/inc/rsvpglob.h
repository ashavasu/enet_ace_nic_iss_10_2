
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rsvpglob.h,v 1.10 2017/06/08 11:40:32 siva Exp $
 *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rsvpglob.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains global variable
 *                             declarations for RSVP.
 *---------------------------------------------------------------------------*/

#ifndef _RSVPGLOB_H
#define _RSVPGLOB_H

#ifdef RSVP_INIT_C

/* Global Variables */
UINT1        gu1MsgIdCapable;
UINT1        gu1IntMsgIdCapable;
UINT1        gu1RRCapable;
UINT1        gu1LabelSetEnable;
UINT4        gu4RMDFlags;
UINT4        gu4Epoch;
UINT4        gu4MsgId;
UINT4        gu4MyHelloInstance = DEF_SRC_INSTANCE;
tRpteTreeNode *gpLocalMsgIdDB = NULL;
tRpteTreeNode *gpNbrMsgIdDB = NULL;

UINT2  gau2IfEntryOffset[RPTE_IF_ENTRY_NUM_INDICES];
UINT2  gau2IfEntryLength[RPTE_IF_ENTRY_NUM_INDICES];
UINT1  gau1IfEntryByteOrder[RPTE_IF_ENTRY_NUM_INDICES];
UINT2  gu2IfEntryNodeOffset;
extern UINT2        gu2GenLblSpaceGrpId;

#else /* Define Extern declarations for all the Global variables above */

extern UINT1        gu1TeAdminFlag;

extern UINT1        gu1MsgIdCapable;
extern UINT1        gu1IntMsgIdCapable;
extern UINT1        gu1RRCapable;
extern UINT1        gu1LabelSetEnable;
extern UINT4        gu4RMDFlags;
extern UINT4        gu4Epoch;
extern UINT4        gu4MsgId;
extern INT4         gi4MplsSimulateFailure;
extern tRpteTreeNode *gpLocalMsgIdDB;
extern tRpteTreeNode *gpNbrMsgIdDB;

extern UINT2  gau2IfEntryOffset[RPTE_IF_ENTRY_NUM_INDICES];
extern UINT2  gau2IfEntryLength[RPTE_IF_ENTRY_NUM_INDICES];
extern UINT1  gau1IfEntryByteOrder[RPTE_IF_ENTRY_NUM_INDICES];
extern UINT2  gu2IfEntryNodeOffset;

extern UINT4  gu4MyHelloInstance;
extern UINT2  gu2GenLblSpaceGrpId;

#endif /* RSVP_INIT_C */

extern INT4         gi4MsgAlloc;
extern INT4         gi4MemAlloc;

#endif /* _RSVPGLOB_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rsvpglob.h                             */
/*---------------------------------------------------------------------------*/
