
 /* $Id: fsrsvlow.h,v 1.13 2018/01/03 11:31:22 siva Exp $*/
/* Proto Validate Index Instance for FsMplsRsvpTeIfTable. */
INT1
nmhValidateIndexInstanceFsMplsRsvpTeIfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsRsvpTeIfTable  */

INT1
nmhGetFirstIndexFsMplsRsvpTeIfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsRsvpTeIfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsRsvpTeIfLblSpace ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeIfLblType ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeAtmMergeCap ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeAtmVcDirection ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeAtmMinVpi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeAtmMinVci ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeAtmMaxVpi ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeAtmMaxVci ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeIfMtu ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeIfUdpNbrs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfIpNbrs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNbrs ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfRefreshMultiple ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeIfTTL ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeIfRefreshInterval ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeIfRouteDelay ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeIfUdpRequired ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeIfHelloSupported ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeIfLinkAttr ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfStatus ARG_LIST((INT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeIfPlrId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsRsvpTeIfAvoidNodeId ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsRsvpTeIfStorageType ARG_LIST((INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsRsvpTeIfLblSpace ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeIfLblType ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeAtmMergeCap ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeAtmVcDirection ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeAtmMinVpi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeAtmMinVci ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeAtmMaxVpi ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeAtmMaxVci ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeIfMtu ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeIfRefreshMultiple ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeIfTTL ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeIfRefreshInterval ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeIfRouteDelay ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeIfUdpRequired ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeIfHelloSupported ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeIfLinkAttr ARG_LIST((INT4  ,UINT4 ));

INT1
nmhSetFsMplsRsvpTeIfStatus ARG_LIST((INT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeIfPlrId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsRsvpTeIfAvoidNodeId ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsRsvpTeIfStorageType ARG_LIST((INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsRsvpTeIfLblSpace ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfLblType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeAtmMergeCap ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeAtmVcDirection ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeAtmMinVpi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeAtmMinVci ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeAtmMaxVpi ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeAtmMaxVci ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfMtu ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfRefreshMultiple ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfTTL ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfRefreshInterval ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfRouteDelay ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfUdpRequired ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfHelloSupported ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfLinkAttr ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfStatus ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeIfPlrId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsRsvpTeIfAvoidNodeId ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsRsvpTeIfStorageType ARG_LIST((UINT4 *  ,INT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsRsvpTeIfTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsRsvpTeIfStatsTable. */
INT1
nmhValidateIndexInstanceFsMplsRsvpTeIfStatsTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsRsvpTeIfStatsTable  */

INT1
nmhGetFirstIndexFsMplsRsvpTeIfStatsTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsRsvpTeIfStatsTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsRsvpTeIfNumTnls ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumMsgSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumMsgRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumHelloSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumHelloRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPathErrSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPathErrRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPathTearSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPathTearRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumResvErrSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumResvErrRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumResvTearSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumResvTearRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumResvConfSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumResvConfRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumBundleMsgSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumBundleMsgRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumSRefreshMsgSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumSRefreshMsgRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPathSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPathRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumResvSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumResvRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumNotifyMsgSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumNotifyMsgRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumRecoveryPathSent ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumRecoveryPathRcvd ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPathSentWithRecoveryLbl ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPathRcvdWithRecoveryLbl ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPathSentWithSuggestedLbl ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPathRcvdWithSuggestedLbl ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumHelloSentWithRestartCap ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumHelloRcvdWithRestartCap ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumHelloSentWithCapability ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumHelloRcvdWithCapability ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeIfNumPktDiscrded ARG_LIST((INT4 ,UINT4 *));

/* Proto Validate Index Instance for FsMplsRsvpTeNbrTable. */

INT1
nmhValidateIndexInstanceFsMplsRsvpTeNbrTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsRsvpTeNbrTable  */

INT1
nmhGetFirstIndexFsMplsRsvpTeNbrTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsRsvpTeNbrTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsRsvpTeNbrRRCapable ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrRRState ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrRMDCapable ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrEncapsType ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrHelloSupport ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrHelloState ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrHelloRelation ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrSrcInstInfo ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrDestInstInfo ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrCreationTime ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeNbrLclRprDetectionTime ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeNbrNumTunnels ARG_LIST((INT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsRsvpTeNbrStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrGrRecoveryPathCapability ARG_LIST((INT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsRsvpTeNbrGrRestartTime ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrGrRecoveryTime ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrGrProgressStatus ARG_LIST((INT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsRsvpTeNbrStorageType ARG_LIST((INT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsRsvpTeNbrRRCapable ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeNbrRMDCapable ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeNbrEncapsType ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeNbrHelloSupport ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeNbrStatus ARG_LIST((INT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsRsvpTeNbrStorageType ARG_LIST((INT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsRsvpTeNbrRRCapable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeNbrRMDCapable ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeNbrEncapsType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeNbrHelloSupport ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeNbrStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeNbrStorageType ARG_LIST((UINT4 *  ,INT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsRsvpTeNbrTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsRsvpTeLsrID ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsRsvpTeMaxTnls ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeMaxErhopsPerTnl ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeMaxActRoutePerTnl ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeMaxIfaces ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeMaxNbrs ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeSockSupprtd ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeHelloSupprtd ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeHelloIntervalTime ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeRRCapable ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeMsgIdCapable ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeRMDPolicyObject ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsRsvpTeGenLblSpaceMinLbl ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeGenLblSpaceMaxLbl ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeGenDebugFlag ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsRsvpTeGenPduDumpLevel ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeGenPduDumpMsgType ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeGenPduDumpDirection ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeOperStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeOverRideOption ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeMinTnlsWithMsgId ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsRsvpTeNotificationEnabled ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeNotifyMsgRetransmitIntvl ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsRsvpTeNotifyMsgRetransmitDecay ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsRsvpTeNotifyMsgRetransmitLimit ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsRsvpTeAdminStatusTimeIntvl ARG_LIST((UINT4 *));

INT1
nmhGetFsMplsRsvpTePathStateRemovedSupport ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeLabelSetEnabled ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeAdminStatusCapability ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeGrCapability ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeGrRecoveryPathCapability ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsRsvpTeGrRestartTime ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeGrRecoveryTime ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeGrProgressStatus ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeReoptimizeTime ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeEroCacheTime ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeReoptimizeTrigger ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeReoptLinkMaintenance ARG_LIST((INT4 *));

INT1
nmhGetFsMplsRsvpTeReoptNodeMaintenance ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsRsvpTeLsrID ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsRsvpTeMaxTnls ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeMaxErhopsPerTnl ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeMaxActRoutePerTnl ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeMaxIfaces ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeMaxNbrs ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeSockSupprtd ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeHelloSupprtd ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeHelloIntervalTime ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeRRCapable ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeMsgIdCapable ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeRMDPolicyObject ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsRsvpTeGenLblSpaceMinLbl ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeGenLblSpaceMaxLbl ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeGenDebugFlag ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsRsvpTeGenPduDumpLevel ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeGenPduDumpMsgType ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeGenPduDumpDirection ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeOperStatus ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeOverRideOption ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeMinTnlsWithMsgId ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsRsvpTeNotificationEnabled ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeNotifyMsgRetransmitIntvl ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsRsvpTeNotifyMsgRetransmitDecay ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsRsvpTeNotifyMsgRetransmitLimit ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsRsvpTeAdminStatusTimeIntvl ARG_LIST((UINT4 ));

INT1
nmhSetFsMplsRsvpTePathStateRemovedSupport ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeLabelSetEnabled ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeAdminStatusCapability ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeGrCapability ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeGrRecoveryPathCapability ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsRsvpTeGrRestartTime ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeGrRecoveryTime ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeReoptimizeTime ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeEroCacheTime ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeReoptimizeTrigger ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeReoptLinkMaintenance ARG_LIST((INT4 ));

INT1
nmhSetFsMplsRsvpTeReoptNodeMaintenance ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsRsvpTeLsrID ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsRsvpTeMaxTnls ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeMaxErhopsPerTnl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeMaxActRoutePerTnl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeMaxIfaces ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeMaxNbrs ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeSockSupprtd ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeHelloSupprtd ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeHelloIntervalTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeRRCapable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeMsgIdCapable ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeRMDPolicyObject ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsRsvpTeGenLblSpaceMinLbl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeGenLblSpaceMaxLbl ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeGenDebugFlag ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsRsvpTeGenPduDumpLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeGenPduDumpMsgType ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeGenPduDumpDirection ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeOperStatus ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeOverRideOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeMinTnlsWithMsgId ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsRsvpTeNotificationEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitIntvl ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitDecay ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsRsvpTeNotifyMsgRetransmitLimit ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsRsvpTeAdminStatusTimeIntvl ARG_LIST((UINT4 *  ,UINT4 ));

INT1
nmhTestv2FsMplsRsvpTePathStateRemovedSupport ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeLabelSetEnabled ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeAdminStatusCapability ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeGrCapability ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeGrRecoveryPathCapability ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsRsvpTeGrRestartTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeGrRecoveryTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeReoptimizeTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeEroCacheTime ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeReoptimizeTrigger ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeReoptLinkMaintenance ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsMplsRsvpTeReoptNodeMaintenance ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsRsvpTeLsrID ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeMaxTnls ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeMaxErhopsPerTnl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeMaxActRoutePerTnl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeMaxIfaces ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeMaxNbrs ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeSockSupprtd ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeHelloSupprtd ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeHelloIntervalTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeRRCapable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeMsgIdCapable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeRMDPolicyObject ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGenLblSpaceMinLbl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGenLblSpaceMaxLbl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGenDebugFlag ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGenPduDumpLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGenPduDumpMsgType ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGenPduDumpDirection ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeOperStatus ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeOverRideOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeMinTnlsWithMsgId ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
INT1
nmhDepv2FsMplsRsvpTeNotificationEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitIntvl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitDecay ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeNotifyMsgRetransmitLimit ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeAdminStatusTimeIntvl ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTePathStateRemovedSupport ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeLabelSetEnabled ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeAdminStatusCapability ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGrCapability ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGrRecoveryPathCapability ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGrRestartTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeGrRecoveryTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
/* Proto Validate Index Instance for FsMplsL3VpnRsvpTeMapTable. */
INT1
nmhValidateIndexInstanceFsMplsL3VpnRsvpTeMapTable ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for FsMplsL3VpnRsvpTeMapTable  */

INT1
nmhGetFirstIndexFsMplsL3VpnRsvpTeMapTable ARG_LIST((INT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsL3VpnRsvpTeMapTable ARG_LIST((INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

/*INT1
nmhGetFsMplsL3VpnRsvpTeMapTnlIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));*/

INT1
nmhGetFsMplsL3VpnRsvpTeMapRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

/*INT1
nmhSetFsMplsL3VpnRsvpTeMapTnlIndex ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetFsMplsL3VpnRsvpTeMapRowStatus ARG_LIST((INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));*/

/* Low Level TEST Routines for.  */

/*INT1
nmhTestv2FsMplsL3VpnRsvpTeMapTnlIndex ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2FsMplsL3VpnRsvpTeMapRowStatus ARG_LIST((UINT4 *  ,INT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));*/

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsL3VpnRsvpTeMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeReoptimizeTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeEroCacheTime ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeReoptimizeTrigger ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeReoptLinkMaintenance ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsMplsRsvpTeReoptNodeMaintenance ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

