
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: rpteprot.h,v 1.63 2018/01/03 11:31:23 siva Exp $
*****************************************************************************/
/*
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rpteprot.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the function prototypes
 *---------------------------------------------------------------------------*/

#ifndef _RPTEPROT_H
#define _RPTEPROT_H

/* Function prototypes in rpteinit.c */
UINT1 RpteCreateRsvpTeMem ARG_LIST ((tRsvpTeGblInfo * gRsvpTeGblInfo));
VOID RsvpUdpPktInSocket (INT4);
VOID RsvpRawPktInSocket (INT4);
UINT1                 RpteRelTnlEntryFunc
ARG_LIST ((const tRsvpTeGblInfo * pRsvpTeGblInfo,
           tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 u1CallerId,
           UINT1 u1RemoveFlag));

UINT1 RpteProcessLspSetupEvent ARG_LIST 
((tRpteEnqueueMsgInfo *pRpteEnqueueMsgInfo));

UINT1
RpteProcessReoptTnlManualTrigger ARG_LIST
((tRpteEnqueueMsgInfo * pRpteEnqueueMsgInfo));

UINT1 RpteProcessLspDestroyEvent ARG_LIST 
((tRpteEnqueueMsgInfo *pRpteEnqueueMsgInfo));

UINT1 RpteProcessDataTxEnableEvent ARG_LIST 
((tRpteEnqueueMsgInfo *pRpteEnqueueMsgInfo));

UINT1 RpteProcessDataTxDisableEvent ARG_LIST 
((tRpteEnqueueMsgInfo *pRpteEnqueueMsgInfo));

UINT1 RpteProcessAdminUpEvent ARG_LIST ((VOID));

UINT1 RpteProcessAdminDownEvent ARG_LIST ((VOID));

UINT1 rpteAllocTables ARG_LIST ((VOID));


UINT1 RptePhSetupTunnel ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo));

VOID RpteDeleteRsvpTeMem (const tRsvpTeGblInfo * pRsvpTeGblInfo);

UINT1 RpteTaskInitRsvpTe (VOID);
UINT1 RpteQueueInitRsvpTe (VOID);

UINT1 RpteCreateGblRsrc (VOID);
UINT1 rpteRegisterWithIp (VOID);
UINT1 rpteDeRegisterWithIp (VOID);

VOID RpteCleanIfTable (VOID);
VOID RpteInitProcessTimeOut ARG_LIST ((VOID));
/* Function prototypes in rpteutil.c */

UINT1               RpteCheckAllTnlInTnlTable
ARG_LIST ((const tRsvpTeGblInfo * pRsvpTeInfo,
           uLabel OutLabel, tIfEntry *pOutIfEntry));

UINT1               RpteCheckTnlInTnlTable
ARG_LIST ((const tRsvpTeGblInfo * pRsvpTeInfo,
           UINT4 u4TunnelIndex,
           UINT4 u4TunnelInstance, UINT4 u4TnlIngressAddr,
           UINT4 u4TnlEgressAddr, tRsvpTeTnlInfo ** ppRsvpTeTnlInfo)); 

UINT1               RpteCheckAdnlTnlInTnlTable
ARG_LIST ((const tRsvpTeGblInfo * pRsvpTeInfo,
           UINT4 u4TunnelIndex,
           UINT4 u4TunnelInstance, UINT4 u4TnlIngressAddr,
           UINT4 u4TnlEgressAddr, tRsvpTeTnlInfo ** ppRsvpTeTnlInfo));

VOID                rpteAddTnlInTnlTrieTable
ARG_LIST ((tRsvpTeGblInfo * pRsvpTeInfo,
          UINT4 u4TunnelIndex,
           UINT4 u4TunnelInstance, UINT4 u4TnlIngressAddr,
           UINT4 u4TnlEgressAddr)); 

UINT1                RpteDelTnlFromTnlTable
ARG_LIST ((const tRsvpTeGblInfo * pRsvpTeInfo, tRsvpTeTnlInfo *pRsvpTeTnlInfo, 
           UINT4 u4TunnelIndex,
           UINT4 u4TunnelInstance, UINT4 u4TnlIngressAddr,
           UINT4 u4TnlEgressAddr, UINT1 u1RemoveFlag));

VOID                rpteUtlDeleteResourceHashTbl 
ARG_LIST ((tTMO_HASH_TABLE    *pRsvpTeTnlRsrcTable));

VOID                RpteUtlDeleteTnlTbl 
ARG_LIST ((VOID));

tRsb *
RpteUtlCreateRsb ARG_LIST ((VOID));

tPsb *
RpteUtlCreatePsb ARG_LIST ((VOID));

UINT1               RpteGetIfEntry
ARG_LIST ((INT4 i4IfIndex, tIfEntry ** ppIfEntry));

UINT1               RpteGetNbrEntry
ARG_LIST ((UINT4 u4NbrAddr ,tIfEntry * pIfEntry, tNbrEntry ** ppNbrEntry));

UINT1
RpteGetNbrEntryFromNbrAddr ARG_LIST ((UINT4 u4NbrAddr,
                                      tNbrEntry ** ppNbrEntry));

UINT1             RpteCreateNewRsvpteTunnel
ARG_LIST ((UINT4 u4TunnelIndex,
           UINT4 u4TunnelInstance, UINT4 u4TnlIngressAddr,
           UINT4 u4TnlEgressAddr, tTeTnlInfo *pTeTnlInfo,
                            tRsvpTeTnlInfo **ppRsvpTeTnlInfo, BOOL1 bFlag));

VOID                rpteUtlFreeLblList ARG_LIST ((tTMO_SLL * pLblStack));

VOID                RpteUtlDecTnlCount ARG_LIST 
                           ((const tRsvpTeTnlInfo * pRsvpTeTnlInfo));

VOID                RpteUtlFreeLblSubObjList
ARG_LIST ((tTMO_SLL * pLblSubObjList));

VOID                RpteUtlFreeNewSubObjList
ARG_LIST ((tTMO_SLL * pNewSubObjList));

VOID                RpteUtlFreeRROList ARG_LIST ((tTMO_SLL * pRROList));

VOID                RpteUtlFreeEROList ARG_LIST ((tTMO_SLL * pEROList));

VOID                RpteUtlPhPreparePktMapRROErr
ARG_LIST ((tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo));

VOID                RpteUtlCleanTeLists 
ARG_LIST ((const tPktMap *pPktMap, tRsvpTeTnlInfo *pRsvpTeTnlInfo));

VOID                RpteUtlCleanPktMap ARG_LIST ((tPktMap * pPktMap));

INT1                rpteUtlCompareLabel
ARG_LIST ((uLabel * puLabel1, uLabel * puLabel2));

INT1                RpteUtlResvManObjHandler
ARG_LIST ((tPktMap * pPktMap,
           tRsvpTeSession ** ppRsvpTeSession,
           tRsvpHop ** ppRsvpHop,
           tFlowSpec ** ppFlowSpec,
           tRsvpTeFilterSpec ** ppRsvpTeFilterSpec));

INT1                RpteUtlCompareRRList
ARG_LIST ((tTMO_SLL RrHopList, tTMO_SLL OutArHopList));

VOID                rpteUtlLblStackHandler
ARG_LIST ((tTMO_SLL *pLblStack,
           uLabel   *pOutLabel, 
           UINT1    *pu1LblStackPreFlag));

VOID RpteUtlDeletePsb ARG_LIST ((tPsb *pPsb));
   
VOID RpteUtlDeleteRsb ARG_LIST ((tRsb * pPsb));

VOID                RpteUtlRhPreparePktMapRROErr
ARG_LIST ((tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo));

UINT1               rpteUtlPhCreateOutErHop
ARG_LIST ((tTMO_SLL * pPktMapErHopList));

VOID                RpteUtlFreeTnlNodeTnlTbl
ARG_LIST ((tRsvpTeTnlInfo * pTnlHashNode, BOOL1 bFlag));

VOID                rpteUtlFreeTrfcRsrcNode
ARG_LIST ((tTMO_SLL_NODE * pTrfcHashNode));

VOID                rpteUtlPrintRpteSsnObj
ARG_LIST ((tRsvpTeSsnObj * pRsvpTeSsnObj));

VOID                rpteUtlPrintRpteSndrTmpObj
ARG_LIST ((tRsvpTeSndrTmpObj * pRsvpTeSndrTmpObj));

UINT1               RpteUtlDeleteNbrTableEntry 
ARG_LIST ((tIfEntry * pIfEntry, tNbrEntry  *pDelNbrEntry));

UINT4               RpteUtlStartTimer
ARG_LIST ((tTimerListId TimerListId, tTmrAppTimer *pReference,
           UINT4 u4Duration));

UINT4               RpteUtlStopTimer
ARG_LIST ((tTimerListId TimerListId, tTmrAppTimer *pReference));

/* Prototypes for the RSVP Low Level routines used in RSVP-TE */
/* Proto Type for Low Level GET FIRST fn for RsvpIfTable  */

INT1 nmhGetFirstIndexRsvpIfTable ARG_LIST ((INT4 *pi4_ifIndex));

/* Proto type for GET_NEXT Routine.  */

INT1 nmhGetNextIndexRsvpIfTable
ARG_LIST ((INT4 i4_ifIndex, INT4 *pi4_next_ifIndex));

/* Function prototype in rptepath.c */


UINT1               RpteChkLsrPresInAbsNodeIpv4
ARG_LIST ((const UINT1 *pLsrId, tRsvpTeHopInfo* pRsvpTeErHopInfo));


VOID RptePhPathRefresh ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo));
VOID                RptePhGeneratePathTear
ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo));

VOID                RptePhConstructAndSndPTMsg
ARG_LIST ((tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo));


UINT1               RpteUpdateTnlTblInfo
ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo,
           tPktMap * pPktMap,
           UINT1 *pu1PathRefreshNeeded, UINT1 *pu1ResvRefreshNeeded));

UINT1               RpteCreateTnlTblPsb
ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo, tPktMap * pPktMap));

UINT1               RpteCreateTnlTblInfo
ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo, tPktMap * pPktMap));

INT1                RptePhRsrcAffVerProc
ARG_LIST ((const tRsvpTeTnlInfo * pRsvpTeTnlInfo));

UINT1               RpteIsLoopFound ARG_LIST ((tPktMap * pPktMap));

INT1                RpteCompERO
ARG_LIST ((const tTMO_SLL * pPktMapEROList, tTMO_SLL * pTnlEROList));
INT1                RpteCompRRO
ARG_LIST ((const tTMO_SLL * pPktMapRROList, tTMO_SLL * pTnlRROList));

VOID                RptePhStartRROBackOffTimer
ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo));

VOID                RptePhProcRROBackoffTimeOut
ARG_LIST ((const tTmrAppTimer * pExpiredTmr));


VOID                RptePhProcessRouteDelayTimeOut
ARG_LIST ((const tTmrAppTimer * pExpiredTmr));

VOID                RptePhStartLocalRepair ARG_LIST ((tPsb * pPsb));

VOID                RptePhProcessPsbRefreshTimeOut
ARG_LIST ((const tTmrAppTimer * pExpiredTmr));

VOID                RptePhProcessSnmpTimeOut
ARG_LIST ((const tTmrAppTimer * pExpiredTmr));

VOID                RptePhHandleRouteChange ARG_LIST ((tRsvpteRtEntryInfo *pRouteInfo));
VOID                RptePhHandleIfChange ARG_LIST ((tRsvpIfMsg *pRsvpIfMsg));

VOID                RsvpIfStChgEventHandler ARG_LIST ((UINT4 u4IfIndex, UINT4 u4IpAddr,
                                                       UINT1 u1OperState, INT4 i4CtrlMplsIfIndex));

INT1
RptePrcsAndUpdateErHopList ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                      tPktMap *pPktMap,
                                      UINT1 u1PathRefreshNeeded,
                                      UINT1 u1ResvRefreshNeeded,
                                      UINT1 u1NewTnl, UINT1 u1NewBkpTnl,
                                      UINT1 *pu1IsLooseHop));

INT1                RpteIntermediatePrcs
ARG_LIST ((UINT4 u4OutGw, tIfEntry * pIfEntry,
           tRsvpTeTnlInfo * pRsvpTeTnlInfo,
           tPktMap * pPktMap,
           UINT1 u1PathRefreshNeeded,
           UINT1 u1ResvRefreshNeeded, UINT1 u1NewTnl, UINT1 u1NewBkpTnl));

INT1                RpteEgrsPrcs
ARG_LIST ((tPktMap * pPktMap));

UINT1               RpteIsLsrPartOfIpv4ErHop
ARG_LIST ((UINT4 u4ErHopAddr, UINT4 u4ErHopMask));

UINT1               RpteIsLsrPartOfErHop
ARG_LIST ((const tRsvpTeHopInfo * pRsvpTeErHopInfo, UINT1 *pu1IsEgrOfHop));
UINT1               RpteIsLsrPartOfCHop                                                    ARG_LIST ((const tTeCHopInfo * pRsvpTeCHopInfo, UINT1 *pu1IsEgrOfHop));


/* Function prototype in rpteperr.c */

VOID                RptePehProcessPathErrMsg ARG_LIST ((tPktMap * pPktMap));

VOID                RptePehProcessPathErr
ARG_LIST ((tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 u1RemoveFlag));

/* Function prototype in rpteport.c */
UINT4 RpteRandomDelay ARG_LIST ((UINT4 u4Range));

UINT1               RpteTriggerInternalEvent
ARG_LIST ((const VOID *pInfo, UINT2 u2EventType));

UINT1               RpteEnqueueMsgToRsvpQ
ARG_LIST ((tRpteEnqueueMsgInfo *pRpteEnqueueMsgInfo));

UINT1               RpteIpUcastRouteQuery
ARG_LIST ((UINT4 u4Addr, tIfEntry **ppIfEntry, UINT4 *pu4OutGw));

UINT1               RpteIpDirectlyConnected
ARG_LIST ((UINT4 u4Addr));

UINT1               RpteRhSendMplsMlibUpdate
ARG_LIST ((UINT2 u2MlibOperation,
           UINT1 u1OperState, tRsvpTeTnlInfo * pCtTnlInfo,UINT1 u1SegDirection));


UINT4
RpteMplsDiffServGetPreConfPhbMap ARG_LIST ((UINT4 u4Incarn, tElspPhbMapEntry aElspPhbMapEntry[]));
 
/* Function prototype in rptepter.c */

VOID                RptePthForwardPathTear
ARG_LIST ((tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo));

VOID                RptePvmUtlSendPathErr
ARG_LIST ((tPktMap * pPktMap, UINT1 u1ErrCode, UINT2 u2ErrValue));

VOID                RptePvmConstructAndSndPEMsg
ARG_LIST ((tPktMap * pPktMap));

UINT1               RpteValidatePathMsgObjs ARG_LIST ((tPktMap * pPktMap));

tNbrEntry          *RptePvmCreateNbrTableEntry
ARG_LIST ((tIfEntry * pIfEntry, UINT4 u4Addr, UINT1 u1Encaps, UINT1 u1VersionFlag));

tNbrEntry          *RptePvmMatchNbrAddr
ARG_LIST ((tIfEntry * pIfEntry, UINT4 u4NeighAddr));

/* Function prototypes in rpteresv.c */

UINT1 RpteRhCreateRsb
ARG_LIST ((const tRsvpTeFilterSpec * pRsvpTeFilterSpec,tPktMap *pPktMap, tIfEntry *pOutIfEntry,tRsb  **ppRsb));

INT1                RpteRhBookTcParams
ARG_LIST (( tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT1 *pu1ErrorCode ,UINT1 u1ResPoolType));

UINT1               RpteRhGetLabel
ARG_LIST ((const tIfEntry * pGetLblIfEntry, uLabel * pLabel));

UINT1               RpteRhFreeLabel
ARG_LIST ((const tIfEntry * pRetLblIfEntry, uLabel * pLabel));

UINT1               RpteRhRroProc
ARG_LIST ((tPktMap * pPktMap,
           tRsvpTeTnlInfo * pCtTnlInfo, UINT1 *u1ResvRefreshNeeded));

UINT1               RpteRhPreemptTunnel ARG_LIST ((tTMO_SLL * pPreEmpTnlList));

VOID                RpteRhSendResv
ARG_LIST ((tRsb * pRsb));

INT1                RpteRhUpdateTrafficControl
ARG_LIST ((tRsvpTeTnlInfo * pCtTnlInfo,
           UINT1 u1ResvRsrc,
           UINT1 *pu1ResvRefreshNeeded, tErrorSpec * pErrorSpec));

VOID                RpteRhResvRefresh ARG_LIST ((tRsvpTeTnlInfo * pCtTnlInfo));

INT1                RptePhRhProcessResvMsg ARG_LIST ((tPktMap * pPktMap));

VOID                RptePvmConstructAndSndREMsg
ARG_LIST ((tPktMap * pPktMap));

VOID                RptePvmSendResvPathErr
ARG_LIST ((tPktMap * pPktMap, UINT1 u1ErrCode, UINT2 u2ErrValue));

VOID                RpteRhStartResvRefreshTmr
ARG_LIST ((tRsb *pRsb));

VOID                RpteRhProcessRsbRefreshTimeOut
ARG_LIST ((const tTmrAppTimer * pExpiredTmr));

VOID                RpteRhStartRROBackOffTimer
ARG_LIST ((tRsvpTeTnlInfo * pCtTnlInfo));

VOID                RpteRhProcRROBackoffTimeOut
ARG_LIST ((const tTmrAppTimer * pExpiredTmr));

VOID                RpteRhPhStartMaxWaitTmr
ARG_LIST ((tRsvpTeTnlInfo * pCtTnlInfo, UINT4 u4MaxWaitTime));

VOID    RpteRhStartReoptimizeTmr 
ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo));

VOID    RptePhStartEroCacheTmr 
ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo));


VOID                RptePhRhProcessWaitTimeOut
ARG_LIST ((const tTmrAppTimer * pExpiredTmr));

VOID                RpteRhUpdateRroPktMap
ARG_LIST ((tRsvpTeTnlInfo * pCtTnlInfo, tObjHdr * pObjHdr, UINT2 u2ObjSize));

VOID              RpteRhDelTcParams
ARG_LIST ((tRsvpTeTnlInfo * pRsvpTeTnlInfo));

/* Function prototypes in rptestat.c */
UINT1 RpteDumpAllRsvpTeTnls ARG_LIST ((INT4 *pNumLsps));

VOID                RptePrintRsvpTeTnls
ARG_LIST ((UINT4 u4TnlNum, tRsvpTeTnlInfo * pRsvpTeTnlInfo));

/* Function prototypes of rpterter.c */

VOID                RpteRthSendResvTear ARG_LIST ((tRsb * pRsb));

/* Function prototypes of rptercon.c */

VOID                RpteRchProcessResvConfMsg ARG_LIST ((tPktMap * pPktMap));

VOID                RpteRchSendResvConf
ARG_LIST ((const tRsvpTeTnlInfo * pCtTnlInfo));

/* Function prototypes of rptehelo.c */

VOID                RpteHhProcessHelloMsg ARG_LIST ((tPktMap * pPktMap));

VOID                RpteHhGenerateHello
ARG_LIST ((tNbrEntry * pNbrEntry,
           UINT1 u1HelloMsg, tIfEntry * pIfEntry));

VOID                RpteHhSendHello
ARG_LIST ((tNbrEntry * pNbrEntry,
           UINT1 u1HelloMsg, tIfEntry * pIfEntry));

UINT1                RpteHhStartHelloRefreshTmr
ARG_LIST ((tRsvpTeGblInfo * pRsvpTeGblInfo));

VOID                RpteHhProcessWaitTimeOut
ARG_LIST ((tTmrAppTimer * pExpiredTmr));

VOID                RpteHhGetTnlListForLocalRecProc
ARG_LIST ((tIfEntry * pIncIfEntry, tNbrEntry * pNbrEntry, UINT1 u1Msg));

/* Function prototypes */

UINT1 RpteRegisterWithTc ARG_LIST ((VOID));

VOID RpteDeRegisterWithTc ARG_LIST ((VOID));

VOID RpteTcFreeResources ARG_LIST ((UINT4 u4Rhandle));

UINT1 RpteTcResvResources ARG_LIST ((const tIfEntry *pIfEntry,
                                     tFlowSpec *pFlowSpec,
                                     UINT1 u1ResPoolType,
                                     UINT4 *pRhandle,
                                     UINT1 *pu1ErrCode));

UINT1 RpteTcAssociateFilterInfo ARG_LIST ((UINT4 u4Rhandle,
                                           tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                           tFlowSpec *pFlowSpec,
                                           UINT1 *pu1ErrCode));

VOID RpteTcDissociateFilterInfo ARG_LIST ((UINT4 u4RHandle, 
                                        tRsvpTeTnlInfo *pRsvpTeTnlInfo));

UINT1 RpteTcGenerateAdSpec ARG_LIST ((const tIfEntry *pIfEntry,
                                      tAdSpec *pAdSpec));

UINT1 RpteTcCalcAdSpec ARG_LIST ((const tIfEntry *pIfEntry,
                                  tAdSpec *pAdSpec,
                                  tSenderTspec *pSenderTspec,
                                  tAdSpec *pNewAdSpec));

UINT1 RpteTcGenerateFlowSpec ARG_LIST ((tAdSpec *pAdSpec,
                                        tSenderTspec *pSenderTspec,
                                        tFlowSpec *pFlowSpec ,
                                        UINT1 *pu1ErrCode));

UINT1 RpteTcCalculateFlowSpec ARG_LIST ((const tIfEntry *pIfEntry,
                                         UINT1 u1ResPoolType,
                                         tAdSpec * pAdSpec,
                                         tSenderTspec *pSenderTspec,
                                         tFlowSpec *pFlowSpec,
                                         tFlowSpec *pNewFlowSpec,
                                         UINT1 *pu1ErrCode));

VOID RpteTcFreeAppResources ARG_LIST ((VOID));

/* Function prototypes of rptepm.c */
VOID                RptePmAddTnlToPrioList
ARG_LIST ((tIfEntry * pIfEntry,
           UINT1 u1Priority, UINT1 u1SegDirection, tRsvpTeTnlInfo * pRsvpTeTnlInfoInput));

VOID                RptePmRemTnlFromPrioList
ARG_LIST ((tIfEntry * pIfEntry,
           UINT1 u1Priority, UINT1 u1SegDirection, tRsvpTeTnlInfo * pRsvpTeTnlInfoInput));

VOID                RptePmModifyTnlPrioList
ARG_LIST ((tIfEntry * pIfEntry,
           UINT1 u1CurrPriority,
           UINT1 u1NewPriority, UINT1 u1SegDirection, tRsvpTeTnlInfo * pRsvpTeTnlInfoInput));

UINT1               RptePmGetTnlFromPrioList
ARG_LIST ((tIfEntry * pIfEntry,
           UINT1 u1Priority, tRsvpTeTnlInfo * pRsvpTeTnlInfoInput, tTMO_SLL * pCandidatePreemp,UINT1 u1SegDirection));

VOID RptePmRemoveTnlFromPrioList (tRsvpTeTnlInfo *pRsvpTeTnlInfo);

VOID  RsvpteRecv (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2Len,
                              UINT2 u2Port, tCRU_INTERFACE IfId, UINT1 u1Flag);

UINT1 RpteSendViaSocket
(tCRU_BUF_CHAIN_HEADER * pMsgBuf, tPktMap * pPktMap);

UINT1 rpteSendViaUdpSocket(tCRU_BUF_CHAIN_HEADER * pMsgBuf, UINT4 u4DestAddr);

UINT1 RpteTEEventHandler ARG_LIST ((UINT4 u4Event, tTeTnlInfo *pTeTnlInfo, 
                                    UINT1 *pu1IsRpteAdminDown));

UINT1 RpteTEProcessTeEvent ARG_LIST 
((tRpteEnqueueMsgInfo *pRpteEnqueueMsgInfo));

/*UINT1 RpteL3VPNEventHandler ARG_LIST ((UINT4 u4Event, tTeTnlInfo *pTeTnlInfo));*/

VOID
RpteTEGetReoptTime ARG_LIST 
((tTeTnlInfo *pTeTnlInfo, UINT4 *pu4RemainingTime));

VOID RpteDumpObjHdr ARG_LIST ((tObjHdr * pObjHdr));

VOID RpteMsgObjDump
ARG_LIST ((UINT1 *pMsg, UINT4 u4MsgLen,UINT1 u1LblType));

VOID RpteDumpIncomRpteMsg ARG_LIST ((const tPktMap *pPktMap));

VOID RpteDumpInRpteBundleMsg  ARG_LIST ((const tPktMap *pPktMap));

VOID RpteDumpOutRpteMsg ARG_LIST ((const tPktMap *pPktMap));

VOID RpteDumpOutRpteBundleMsg  ARG_LIST ((const tPktMap *pPktMap));

VOID RpteDumpRroObj (tObjHdr *pObjHdr);

VOID RpteDumpSenderTSpecObj (tObjHdr *pObjHdr);

VOID RpteDumpAdSpecObj (tObjHdr *pObjHdr);

VOID RpteDumpFlowSpecObj (tObjHdr *pObjHdr);

VOID RpteDumpEroObj (tObjHdr *pObjHdr);

VOID RpteDumpElspTpObj (tObjHdr *pObjHdr);

VOID RpteDumpStyleObj (tObjHdr *pObjHdr);

VOID RsvpProcessRawIpMsg(VOID);
VOID RsvpProcessUdpMsg(VOID);

VOID
RpteFacilityStripErHopsTillMP (tRsvpTeTnlInfo **ppRsvpTeTnlInfo);
VOID RpteFrrConstructAndSendPathErr (tRsvpTeTnlInfo *pRsvpTeTnlInfo);
VOID RsvpProcessCspfMsg (tRpteCspfInfo *pRpteCspfInfo);
VOID RpteCspfCompForBkpOrProtTnl (tRsvpTeTnlInfo *pCtTnlInfo);
VOID RpteStartCspfIntervalTimer (tRsvpTeTnlInfo * pRsvpTeTnlInfo);
VOID RpteFrrBypassTnlIdentification (tRsvpTeTnlInfo **ppCtTnlInfo);
VOID RpteFrrMPBypassTnlIdentification (tRsvpTeTnlInfo **ppCtTnlInfo);
VOID
RpteRhBackupPathTrigger (tRsvpTeTnlInfo *pCtTnlInfo);
INT1 RpteFrrValidateBkpPathMsg (tPktMap *pPktMap, 
                                tRsvpTeTnlInfo *pRsvpTeTnlInfo);

INT1 RpteUtlIsUpStrMsgRcvdOnBkpPath (tPktMap *pPktMap,
                                     tRsvpTeTnlInfo **ppRsvpTeTnlInfo,
                                     tRsvpTeTnlInfo **ppRsvpTePrevTnlInfo,
                                     BOOL1 *bIsBkpTnl);
UINT1
RptePlrGenerateErHopsTowardEgress (tRsvpTeTnlInfo **ppRsvpTeOutTnlInfo,
                                   tCspfCompInfo *pCspfCompInfo,
                                   tOsTeAppPath *pCspfPath);
UINT1 RptePhConstructBkpPathConsts (tRsvpTeTnlInfo **ppRsvpTeTnlInfo);
UINT1
RptePhProcessBkpPathMsg (tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                         tTeTnlInfo * pTeTnlInfo,
                         tRsvpTeTnlInfo ** ppRsvpTeBkpTnlInfo,
                         UINT1 *pu1NewBkpTnl);
VOID
RpteRhRsbRefreshTimeOutDestroy (tRsvpTeTnlInfo *pCtTnlInfo);
VOID
RptePhConstructAndSndPTMsgOnBkpPath (tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo);
VOID
RptePhHandleIfChangeForFrr (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT1 u1Status);
VOID
RptePehProcessPathErrMsgForFrr (tPktMap * pPktMap,
                                tRsvpTeTnlInfo * pRsvpTeTnlInfo);
VOID
RpteRthProcessResvTearForFrr (tPktMap * pPktMap, tRsvpTeTnlInfo * pCtTnlInfo,
                              tRsvpHop *pNhop, tIfEntry *pIfEntry, BOOL1 bIsBkpTnl);
UINT1
RpteFrrBypassTnlSearch (UINT4 u4StrictAddr, tRsvpTeTnlInfo **ppCtTnlInfo);

UINT1
RpteFrrMPBypassTnlSearch (UINT4 u4StrictAddr, tRsvpTeTnlInfo **ppCtTnlInfo);

UINT1
RpteIfChangeForProtTnlLinkDown (tRsvpTeTnlInfo * pRsvpTeTnlInfo);
UINT1
RpteIfChangeForProtTnlLinkUp (tRsvpTeTnlInfo * pRsvpTeTnlInfo);

INT1
RpteUtlGetGrpIdForDMP (tRsvpTeTnlInfo *pRsvpTeTnlInfo, tIfEntry *pIfEntry,
                                              UINT1 *pu1DetourGrpId);
INT1
RpteUtlIsNodeProtDesired (tRsvpTeTnlInfo *pRsvpTeTnlInfo);
INT1
RpteUtlIsBwProtDesired (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                        UINT4 *pu4Bandwidth);
VOID
RptePhFrrSetupGblRevertOptTunnel (tRsvpTeTnlInfo *pRsvpTeTnlInfo, tCspfCompInfo *pCspfCompInfo,
                                  tOsTeAppPath * pCspfPath);
UINT1
RpteTnlScanForFrrMakeAfterBreakProt (tIfEntry * pOutIfEntry);
VOID
RpteFrrGblRevertiveOptPathTrigger (tRsvpTeTnlInfo * pCtTnlInfo);
VOID
RpteStartFrrGblRevertTimer (tRsvpTeTnlInfo * pRsvpTeTnlInfo);
VOID
RpteHandlePsbOrHelloTimeOutForFrr (tRsvpTeTnlInfo * pRsvpTeTnlInfo);
INT1
RpteUtlGetTnlInfoAndGrpId (tPktMap * pPktMap, tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                                      UINT1 *pu1DetourGrpId);

VOID
RpteHandleNbrStatusChange (tNbrEntry *pNbrEntry, UINT4 u4Status, UINT4 u4DataTeIfIndex,
                           UINT1 u1IsIfEvent);
UINT1
RpteFrrHandleHeadEndPlrFailure (tRsvpTeTnlInfo * pRsvpTeTnlInfo);
UINT1
RpteFrrHandleHeadEndPlrLocRev (tRsvpTeTnlInfo * pRsvpTeTnlInfo);

VOID RpteFrrUnknownPsbOrHelloTimeOut (tRsvpTeTnlInfo *pRsvpTeTnlInfo);
VOID RpteUtilGetPrevTnlInfo (tRsvpTeTnlInfo *pBaseTnlInfo,
                             tRsvpTeTnlInfo *pCurTnlInfo,
                             tRsvpTeTnlInfo **ppPrevTnlInfo);

VOID RpteResetPlrStatus (tRsvpTeTnlInfo *pRsvpTeTnlInfo);
VOID RpteUpdateDetourStats (UINT4 u4AddOrSub);
VOID
RpteFrrHandlePlrLocRev (tRsvpTeTnlInfo * pRsvpTeTnlInfo);
VOID
RpteHandlePlrDown (tRsvpTeTnlInfo *pRsvpTeOrigTnlInfo);
VOID
RptePrcsBkpPathTearOrTimeOut (tRsvpTeTnlInfo *pRsvpTeOrigTnlInfo,
                             tRsvpTeTnlInfo *pRsvpTePrevInTnlInfo,
                             tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                             BOOL1 *pbIsOperDone);
VOID
RpteConstructPktMapAndSendResvErr(tRsvpTeTnlInfo * pRsvpTeTnlInfo);
VOID
RpteConstructPktMapAndSendPathErr(tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                  UINT2 u2ErrCode, UINT2 u2ErrVal);

UINT4
RpteUtilConvertToInteger(tTeRouterId RouterId);

INT4 RsvpTeTestAllCfgParams (tRsvpTeCfgParams *pRsvpTeCfgParams,
                             UINT4 *pu4ErrorCode, UINT4 u4MiBObject);
INT1 RpteTeGetTnlInfo (tRsvpTeGetTnlInfo *pRsvpTeGetTnlInfo,
                       tRsvpTeTnlInfo  *pRsvpTeTnlInfo);
UINT2 RpteUtlCalculatePathMsgSize (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                   UINT2 *pu2SsnObjLen, UINT2 *pu2RROSize,
                                   UINT1 *pu1EroNeeded, UINT1 *pu1RroNeeded,
                                   UINT1 u1MsgType);
VOID RptePvmFillGenericLabelReqObj (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                    tObjHdr ** ppObjHdr, UINT1 u1MsgType);
VOID
RptePvmFillUpstreamGenericLblObj (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                  tObjHdr **ppObjHdr, UINT1 u1MsgType);
VOID RptePvmFillExplicitRouteObj (tRsvpTeTnlInfo *pRsvpTeTnlInfo, 
                                  UINT2 u2EroSize, tObjHdr ** ppObjHdr);
VOID RptePvmFillLabelSetObj (tRsvpTeTnlInfo *pRsvpTeTnlInfo, tObjHdr ** ppObjHdr
                             , UINT1 u1MsgType);
INT1
RptePvmValidateSwAndEncType (tIfEntry *pIfEntry, tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                             BOOL1 bIsDnStr);
UINT2 RptePvmValidateEncodingType (UINT1 u1EncType, tIfEntry *pIfEntry, UINT4 u4TeLinkIf);
UINT2 RptePvmValidateSwitchingCap (UINT1 u1SwCap, tIfEntry *pIfEntry, UINT4 u4TeLinkIf);
VOID RpteUtlCalculateEROSize (tRsvpTeTnlInfo *ppRsvpTeTnlInfo);
UINT2 RpteUtlCalculateRROSize (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT4 u4MsgType);
VOID RptePvmFillRecordRouteObj (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                UINT2 u2RroSize, tObjHdr ** ppObjHdr);
VOID RptePvmFillErrorSpecFlags (UINT1 *u1Flag);
VOID RsvpProcessCspfMsgForFRRTnl (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                  tCspfCompInfo *pCspfCompInfo,
                                  tOsTeAppPath *pCspfPath);
VOID RptePvmFillEROFromERHop (UINT1 **pPdu, tRsvpTeHopInfo *pRsvpTeHopInfo,
                              UINT1 u1LblCType);
VOID RptePvmFillEROFromCHop (UINT1 **pPdu, tTeCHopInfo *pTeCHopInfo,
                             UINT1 u1LblCType);
VOID RptePvmFillTopRRO (UINT1 **pPdu, tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                        tIfEntry *pIfEntry, UINT4 u4FwdLbl, UINT4 u4RevLbl);
VOID RptePvmFillDnStrOrUpStrRRO (UINT1 **pPdu, tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                 tTMO_SLL *pRroList);
VOID RptePvmFillIpv4SubObjInERO (UINT1 **pPdu, UINT1 u1LBit, UINT4 u4IpAddr,
                                 UINT1 u1PrefixLen);
VOID RptePvmFillUnnumIfSubObjInERO (UINT1 **pPdu, UINT1 u1LBit, UINT4 u4IpAddr,
                                    UINT4 u4UnnumIf);
VOID RptePvmFillLblSubObjInERO (UINT1 **pPdu, UINT1 u1LBit, UINT1 u1UBit,
                                UINT1 u1LblCType, UINT4 u4Label);
VOID RptePvmFillIpv4SubObjInRRO (UINT1 **pPdu, UINT4 u4IpAddr, 
                                 UINT1 u1PrefixLen, UINT1 u1Flags);
VOID RptePvmFillUnnumIfSubObjInRRO (UINT1 **pPdu, UINT1 u1Flags,
                                    UINT4 u4IpAddr, UINT4 u4UnnumIf);
VOID RptePvmFillLblSubObjInRRO (UINT1 **pPdu, UINT1 u1UBit, UINT1 u1Flags,
                                UINT1 u1LblCType, UINT4 u4Label);
INT1 RpteFindDestAddr (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT4 *pu4RemoteRtrAddr,
                       UINT4 *pu4RemoteLinkId, UINT4 *pu4RemoteLinkAddr);
VOID RptePhCheckAndUpdateOobInfo (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                  tIfEntry **ppIfEntry, UINT4 u4NHId, 
                                  UINT4 u4IpAddr, UINT4 u4NHIpAddr,
                                  UINT1 u1IsDnStr, UINT4 *pu4OutGw);
INT1 RpteTlmUpdateTrafficControl (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                  UINT4 u4TeIfIndex, tBandWidth ReqResBw,
                                  UINT1 u1ResvRsrc, UINT1 *pu1RefreshNeeded,
                                  UINT1 u1SegDirection);
INT1 RpteCspfCompForNormalTnl (tRsvpTeTnlInfo * pRsvpTeTnlInfo);

INT1 RpteCspfCompForReoptimizeTnlAtMidNode (tRsvpTeTnlInfo * pRsvpTeTnlInfo);

INT1 RpteFillExpRoute (tTMO_SLL *pEROList ,tCspfCompInfo *pCspfCompInfo,
       tTeTnlInfo *pTeTnlInfo );
VOID RsvpProcessCspfMsgForNormalTnl (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                     tOsTeAppPath *pCspfPath);
BOOL1 RpteUtlIsCspfComputationReqd (tRsvpTeTnlInfo * pRsvpTeTnlInfo);
INT1 RptePortIsOspfTeEnabled (VOID);
VOID RptePortTlmGetCompIfIndex (UINT4 u4L3If, UINT4 *pu4CompIfIndex);
INT4 RptePortTlmUpdateTeLinkUnResvBw (UINT4 u4TeIfIndex,
                                      UINT1 u1TnlHoldPrio, FLOAT ReqResBw,
                                      UINT1 u1ResvRsrc, UINT4 *u4CompIfIndex);
VOID RptePortTlmGetTeLinkUnResvBw (UINT4 u4TeIfIndex,UINT1 u1TnlSetPrio,
                                   FLOAT *pUnResvBw,FLOAT *pAvailBw);
VOID RptePortTlmGetRemoteRtrIp (UINT4 u4RemoteLinkAddr, UINT4 *pu4RemoteRouterIp);
VOID RptePortTlmGetLocalId (UINT4 u4UnnumIf, UINT4 u4RemoteRtrId, UINT4 *pu4LocalId, 
                            UINT4 *pu4IfIndex);
VOID RptePortTlmGetLocalIP (UINT4 u4RemoteAddr, UINT4 *pu4LocalIp, UINT4 *pu4IfIndex);
VOID RptePortTlmGetRemoteIP (UINT4 u4IfIndex, UINT4 *pu4RemoteAddr);
VOID RptePortTlmGetEncType (UINT4 u4IfIndex,UINT1 *pu1EncType);
VOID RptePortTlmGetSwCap (UINT4 u4IfIndex, UINT1 *pu1SwCap);
INT4
RptePortCfaGetTeLinkIfFromMplsIf (UINT4 u4MplsIfIndex, UINT4 *pu4TeLinkIf);
INT4
RptePortCfaGetTeLinkIfFromL3If (UINT4 u4L3IfIndex, UINT4 *pu4TeLinkIf);

INT4
RptePortCfaGetMplsIfFromTeLinkIf (UINT4 u4TeLinkIf, UINT4 *pu4MplsIf);
VOID
RptePhGetNextHopInfoFromCHop (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                              tTeCHopListInfo *pTeCHopListInfo,
                              UINT4 u4EgressId, UINT4 *pu4NHRtrAddr,
                              UINT4 *pu4NHId, UINT4 *pu4NHIpAddr);
VOID
RptePhGetNextHopInfoFromERHop (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                               tTePathInfo *pTePathInfo,
                               UINT4 u4EgressId, UINT4 *pu4NHRtrAddr,
                               UINT4 *pu4NHId, UINT4 *pu4NHIpAddr);

VOID RptePhGetNextHopInfo (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT4 *pu4NHRtrAddr,
                           UINT4 *pu4NHId, UINT4 *pu4NHIpAddr);
VOID
RpteDnStrLblProgForExpLblControl (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                  UINT4 u4FwdLbl, UINT4 u4RevLbl);

INT4
RptePhFindRouteAndFwdPathMsg (tTeHopInfo *pRsvpTeErHopInfo,
                              tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                              tPktMap *pPktMap, UINT1 u1PathRefreshNeeded,
                              UINT1 u1ResvRefreshNeeded, UINT1 u1NewTnl,
                              UINT1 u1NewBkpTnl, UINT1 u1ErrFlag,
                              UINT1 *pu1IsCspfReqd);

VOID 
RptePhActOnPathSetupFailure (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT1 u1ErrCode,
                             UINT2 u2ErrValue);
VOID
RptePvmFillRsvpHopObj (tRsvpTeTnlInfo *pRsvpTeTnlInfo,tObjHdr ** ppObjHdr,
                       UINT4 u4MsgType);
VOID
RptePvmFillRsvpNumHopObj (tObjHdr *pObjHdr, UINT4 u4Addr, UINT4 u4Lih);
VOID
RptePvmFillIfIdRsvpHopObj (tObjHdr *pObjHdr, UINT4 u4RouterId,
                           UINT4 u4InterfaceId, UINT4 u4Lih);
VOID
RptePvmFillIfIdNumRsvpHOpObj (tObjHdr *pObjHdr, UINT4 u4RouterId,
                           UINT4 u4InterfaceIp, UINT4 u4Lih);
INT1
RpteUpStrLblProgramming (tRsvpTeTnlInfo *pRsvpTeTnlInfo);

INT1
RptePortCfaCreateMplsTnlIf (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT4 u4MsgType);

INT1
RptePortCfaDeleteMplsTnlIf (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT4 u4MsgType);

VOID
RptePvmFillSrcAndDestAddr (tPktMap *pPktMap);

UINT2
RpteUtlCalculateRSVPHopSize (BOOL1 b1OobFlag, UINT4 u4InterfaceId,
                             UINT4 u4InterfaceAddr);
UINT2
RpteUtlCalculateErrorSpecSize (tPktMap *pPktMap);

VOID
RptePvmFormAndSendPEOrREMsg (tPktMap *pPktMap, UINT1 u1ErrCode, UINT2 u2ErrValue,
                             UINT1 u1MsgType);
VOID
RptePvmFormNormalErrorSpecObj (tPktMap *pPktMap,
                               tErrorSpecObj *pErrorSpecObj, UINT1 u1ErrCode,
                               UINT2 u2ErrValue);
VOID
RptePvmFormIfIdNumErrorSpecObj (tPktMap *pPktMap,
                                tIfIdRsvpNumErrObj *pIfIdNumErrorSpecObj,
                                UINT1 u1ErrCode, UINT2 u2ErrValue,
                                UINT4 u4LocalIp);
VOID
RptePvmFormIfIdErrorSpecObj (tPktMap *pPktMap,
                             tIfIdRsvpErrObj *pIfIdErrorSpecObj,
                             UINT1 u1ErrCode, UINT2 u2ErrValue, UINT4 u4LocalId);
VOID
RptePvmFillErrorSpecObj(UINT1 **ppu1Pdu,tPktMap *pPktMap);

VOID
RpteGetIfUnnumPeerMac (UINT4 u4IfIndex, UINT1 *pu1MacAddr);

VOID
RpteDeleteTrfcParamsAtEgress (tRsvpTeTnlInfo *pRsvpTeTnlInfo);
INT4
RptePhUpStrResvBwAndLblProg (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT1 *pu1ErrCode,
                             UINT2 *pu2ErrValue);
UINT1
RptePmGetBestMatchTnl (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                       tRsvpTeTnlInfo *pRsvpTeTnlInfoInput,
                       tTMO_SLL *pStackTnlList, tRsvpTeTnlInfo **pTempRsvpTeTnlInfo,
                       UINT1 *pu1Flag, UINT4 *pu4Diffmin,
                       UINT1 u1SegDirection,UINT1 u1PreemptSegDirection);

UINT1
RptePmAddTnlsToBePreempted (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                           tRsvpTeTnlInfo *pRsvpTeTnlInfoInput,
                           tTMO_SLL *pStackTnlList,
                           tTMO_SLL * pCandidatePreemp,UINT4 *pu4Rate,
                           UINT1 u1SegDirection,UINT1 u1PreemptSegDirection);

UINT1
RptePmIsTnlEligibleForPreemption (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                  tRsvpTeTnlInfo *pRsvpTeTnlInfoInput,
                                  UINT1 u1SegDirection,UINT1 u1PreemptSegDirection);

UINT1
RptePmAddCandidatePreempList (UINT1 u1Flag, tTMO_SLL *pCandidatePreemp,
                              tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                              tRsvpTeTnlInfo *pTempRsvpTeTnlInfo,
                              tRsvpTeTnlInfo **pCandidateTnl);

VOID
RptePvmFillAdminStatusObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo, tObjHdr ** ppObjHdr,                                                             UINT4 u4MsgType);

VOID
RptePhStartGracefulDelTmr (tRsvpTeTnlInfo *pRsvpTeTnlInfo);

VOID
RptePhAdminStatusActionItem (tRsvpTeTnlInfo *pRsvpTeTnlInfo);

VOID
RpteHhSetNbrHelloActive (tNbrEntry *pNbrEntry, tIfEntry *pIfEntry, UINT1 u1Flag);
VOID
RpteHhSetNbrHelloState (tNbrEntry * pNbrEntry);
VOID
RpteUtlFillErrorTable (UINT1 u1ErrCode, UINT2 u2ErrValue,                                                                                UINT4 u4ErrNodeAddr,
                       tRsvpTeTnlInfo *pRsvpTeTnlInfo);
VOID                                                                                                              RpteUtlGetHelpString (UINT1 u1ErrCode, UINT2 u2ErrValue,
                      UINT1 *pau1HelpString, UINT4 *pu4Length);

VOID
RpteHandleTunnelEvents (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                        UINT4 u4IngressId, UINT4 u4EgressId, UINT4 u4Event,
                        tTeTnlInfo *pTeTnlInfo);

VOID
RpteActOnTunnelEvent (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT4 u4Event);

VOID
RpteUtlSetDetourStatus (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT1 u1DetourStatus);

UINT1 RpteGetTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                            UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                            UINT4 u4TnlIngressAddr, UINT4 u4TnlEgressAddr,
                            tRsvpTeTnlInfo ** ppRsvpTeTnlInfo);
VOID
RptePvmFillLSPTnlIfIdObj (tObjHdr **ppObjHdr, tRsvpTeTnlInfo *pRsvpTeTnlInfo, 
                          UINT4 u4MsgId);
VOID
RptePvmFillProtectionObj (tObjHdr **pObjHdr, tRsvpTeTnlInfo *pRsvpTeTnlInfo);
VOID
RptePvmFillAssociationObj (tObjHdr **pObjHdr, tRsvpTeTnlInfo *pRsvpTeTnlInfo);
VOID
RptePvmFillNotifyReqObj (tObjHdr **ppObjHdr, tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                                  UINT4 u4MsgType);
UINT4
RpteValidateClassObject (tPktMap *pPktMap, UINT2 *pu2ErrVal);
VOID
RpteNhAddTnlToNotifyRecipient (tRsvpTeTnlInfo  *pRsvpTeTnlInfo,
                               UINT4 u4NotifyAddr, UINT4 u4MsgDirection,
                               UINT1 u1IsGroupTmrReq,
                               UINT1 u1ErrCode, UINT2 u1ErrValue);
VOID
RpteNhStartGroupingTmr (tNotifyRecipient *pNotifyRecipient, UINT4 u4IsGroupTmrValue);
VOID
RpteNhStopGroupingTmr (tNotifyRecipient *pNotifyRecipient);
VOID
RpteNhSendNotifyMsg (tNotifyRecipient *pNotifyRecipient);
VOID
RpteNhStartRetryTmr (tNotifyRecipient *pNotifyRecipient);
VOID
RpteNhProcessNotifyMsg (tPktMap * pPktMap);
VOID
RpteNhProcessRetryTimeOut (tNotifyRecipient *pNotifyRecipient);
UINT4
RpteNhCalculateRetransmitIntvl (UINT4 u4NotifyMsgRetryIntvl);
INT4
RpteNhGetAndDelNotifyRecipient (UINT4 u4NotifyAddr, UINT4 u4MsgId);
VOID
RpteNhDelRBNode (tNotifyRecipient *pNotifyRecipient);
VOID
RptePvmFillMsgIdObj (tObjHdr **ppObjHdr, UINT4 u4MsgId);
VOID
RptePvmFillSsnObj (tObjHdr **ppObjHdr, tIpv4Addr TnlDest, UINT4 u4TnlIndex,
                    tIpv4Addr ExtnTnlId);
VOID
RptePvmFillSndrTempObj (tObjHdr **ppObjHdr, tIpv4Addr TnlSndrAddr,
                        UINT2 u2LspId);
VOID
RptePvmFillFilterSpecObj (tObjHdr **ppObjHdr, tIpv4Addr TnlSndrAddr,
                          UINT2 u2LspId);
UINT4
RpteUtlNtfyMsgSizeCalculation (tNotifyRecipient *pNotifyRecipient);
UINT1
RpteNhValidateNtfyMsgObjs (tPktMap * pPktMap);

VOID
RptePhMapWorkAndProtTnls (tPktMap *pPktMap, tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                          UINT1 *pu1PathRefreshNeeded);
VOID
RpteUtlSwitchTraffic (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT1 u1SwitchType);
INT4
RpteNhProcessFullyReRouteTnls (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT4 u4ErrNodeAddr);

VOID
RpteNhProcessOneToOneTnls (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT4 u4DestAddr, UINT2 u2ErrValue);
VOID
RpteRhRelReroutedTnlBw (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT1 u1SegDirection);
VOID
RpteUtlFillExcludeHopList (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                           UINT4 u4ErrNodeAddr);
VOID
RptePbSendNotifyOrAck (tPktMap * pPktMap);
VOID
RpteUtlSetAdminFlagsAndRefreshMsg (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                   UINT1 u1SwitchType);
INT4
RptePortCreateFATELink (tRsvpTeTnlInfo   *pRsvpTeTnlInfo);
INT4
RpteDeleteFATeLink (tRsvpTeTnlInfo   *pRsvpTeTnlInfo);

VOID
RpteNhHandlePsbOrRsbOrHelloTimeOut (tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT2 u2ErrValue);

VOID
RpteSetTnlIfIndex (tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                   tRsvpTeTnlInfo *pRsvpTeMapTnlInfo, UINT4 u4TnlIfIndex,
                   UINT4 u4MsgType, BOOL1 b1ReRouteFlag);
VOID
RpteShutDownProcess (VOID);

UINT1
RpteUtlCheckTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                           UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                           UINT4 u4TnlIngressLsrId, UINT4 u4TnlEgressLsrId,
                           tRsvpTeTnlInfo ** ppRsvpTeTnlInfo,
                           tRsvpTeTnlInfo *pRpteInTnlInfo);
UINT1
RpteUtlCheckAdnlTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                            UINT4 u4TunnelIndex,
                             UINT4 u4TunnelInstance,
                             UINT4 u4TnlIngressAddr,
                             UINT4 u4TnlEgressAddr,
                             tRsvpTeTnlInfo ** ppRsvpTeTnlInfo,
                             tRsvpTeTnlInfo *pRsvpInTnlInfo);
UINT1
RpteUtlGetTnlInTnlTable (const tRsvpTeGblInfo * pRsvpTeInfo,
                      UINT4 u4TunnelIndex,
                      UINT4 u4TunnelInstance,
                      UINT4 u4TnlIngressAddr,
                      UINT4 u4TnlEgressAddr,
                      tRsvpTeTnlInfo ** ppRsvpTeTnlInfo,
                      tRsvpTeTnlInfo *pRpteInTnlInfo);
VOID
RpteGetTnlsAndHandleEvents (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                            UINT4 u4IngressId, UINT4 u4EgressId, UINT4 u4Event,
                            tTeTnlInfo * pTeTnlInfo, tRsvpTeTnlInfo *pInRpteTnlInfo);

VOID
RpteGrStoreDynamicInfo (VOID);
VOID
RptePvmFillRestartCapObj (UINT2 u2RestartTime, UINT2 u2RecoveryTime,
                           tObjHdr ** ppObjHdr);
VOID
RptePvmFillCapabilityObj (tObjHdr ** ppObjHdr, tNbrEntry *pNbrEntry,
                          UINT1 u1RecoveryPathCapability);

VOID
RptePvmFillRecoveryLblObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                  tObjHdr ** ppObjHdr, UINT1 u1MsgType);
VOID
RptePvmFillSuggestedLblObj (tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                  tObjHdr ** ppObjHdr);

VOID                                                                                                                          RpteGrStoreNbrProperties (tNbrEntry *pNbrEntry, tPktMap *pPktMap);

UINT1
RpteGrStartRecoveryTmr (tRsvpTeGblInfo * pRsvpTeGblInfo);
UINT1
RpteGrRecoveryLabelProcedure (tPktMap *pPktMap, tRsvpTeTnlInfo *pRsvpTeTnlInfo);
VOID
RpteHlprStartRestartTmr (tNbrEntry *pNbrEntry);
VOID
RpteHlprRecoveryProcedure(tNbrEntry *pNbrEntry);
VOID
RpteHlprStartRecoveryTmr (tNbrEntry *pNbrEntry);
VOID
RpteGrProcessRecoveryTimeOut (tRsvpTeGblInfo *gpRsvpTeGblInfo);
VOID
RpteHlprProcessRestartTimeOut (tNbrEntry *pNbrEntry);
VOID
RpteHlprProcessRecoveryTimeOut (tNbrEntry *pNbrEntry);
VOID
RpteHlprGenerateRecoveryPath (tRsvpTeTnlInfo *pRsvpTeTnlInfo);
VOID
RpteUtlCalEroSizeForRecoveryPathMsg (tRsvpTeTnlInfo *pRsvpTeTnlInfo);
VOID
RptePvmFillEROForRecoveryPathMsg (tRsvpTeTnlInfo * pRsvpTeTnlInfo, UINT2 u2EroSize,
                                  tObjHdr ** ppObjHdr);
VOID
RpteGrProcessRecoveryPathMsg (tPktMap *pPktMap);
UINT1
RpteGrRecoveryPathMsgProcedure (tTeTnlInfo *pTeTnlInfo, UINT4 u4DnInLbl,
                                UINT4 u4DnOutLbl, UINT4 u4UpOutLbl,
                                UINT4 u4UpInLbl);
UINT1
RpteGrProcessSRefreshRecoveryPathMsg (UINT4 u4MsgId);

UINT2
RpteUtlCalculateHelloMsgSize (tNbrEntry *pNbrEntry, tIfEntry * pIfEntry);

VOID
RpteHlprStartRecoveryPathRefreshTmr (tRsvpTeTnlInfo * pRsvpTeTnlInfo);

extern tIssBool MsrGetRestorationStatus PROTO ((VOID));
VOID
RpteProcessL2VPNEvent (tRpteEnqueueMsgInfo * pRpteEnqueueMsgInfo);

UINT1
RpteProcessL3VPNEvent (tRpteEnqueueMsgInfo * pRpteEnqueueMsgInfo);

VOID
RpteL2VPNEventHandler (tTeTnlInfo * pTeTnlInfo);

UINT1
RpteFrrFillFacilityListEntry (tRsvpTeTnlInfo *pCtTnlInfo);

UINT1
RpteUpdateOne2OneTunnelAddFtnEntry (tRsvpTeTnlInfo *pRsvpTeTnlInfo);

UINT1
RpteUpdateOne2OneTunnelDelFtnEntry (tRsvpTeTnlInfo *pRsvpTeTnlInfo);

VOID 
RpteReoptimizeTimeOut (tRsvpTeTnlInfo * pRsvpTeTnlInfo);

VOID 
RpteEroCacheTimeOut (tRsvpTeTnlInfo * pRsvpTeTnlInfo);

VOID
RpteTriggerReoptimization (tRsvpTeTnlInfo * pRsvpTeTnlInfo,UINT4 u4TriggerEvent);

INT1
RpteIsFirstHopAsLoose(tRsvpTeTnlInfo * pRsvpTeTnlInfo);

VOID
RpteStartLinkUpWaitTimer (tRsvpTeTnlInfo * pRsvpTeTnlInfo);

VOID
RpteHandleIfUpEvtForLspReoptimization(VOID);

VOID
RpteLinkUpWaitTimeOut (tRsvpTeTnlInfo * pRsvpTeTnlInfo);


UINT1
RpteProcessTeTnlReoptManualEvent(tTeTnlInfo *pTeTnlInfo);

UINT1
RpteStartEventToL2vpnTmr (tRsvpTeTnlInfo * pRsvpTeTnlInfo);

VOID
RpteProcessEventToL2vpnTimeOut (tRsvpTeTnlInfo * pCtTnlInfo);

/*UINT1
RpteUtlFetchTnlInfoFromTnlTable (UINT4 u4TunnelIndex,tRsvpTeTnlInfo * pRpteInTnlInfo);*/

#endif /* _RPTEPROT_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rpteprot.h                             */
/*---------------------------------------------------------------------------*/
