/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptedste.h,v 1.5 2016/07/22 09:45:47 siva Exp $
 *
 * Description:
 *
 *******************************************************************/


#ifndef _RPTEDSTE_H
#define _RPTEDSTE_H
/*****************************************************************************/

#define RPTE_TE_DS_ELSP_INFOLIST_INDEX(pElspList) \
                  TE_DS_ELSP_INFO_LIST_INDEX(pElspList)

#define RPTE_TE_DS_ELSP_INFOLIST_TNL_COUNT(pElspList)\
                  TE_DS_ELSP_INFO_LIST_TNL_COUNT(pElspList)

#define RPTE_TE_DS_ELSP_INFOLIST(pElspList)\
                  TE_DS_ELSP_INFO_LIST(pElspList)

/*****************************************************************************/
/*tMplsDiffServElspInfo macros*/
#define RPTE_TE_DS_ELSP_INFO_INDEX(pElspInfoNode)\
                             TE_DS_ELSP_INFO_INDEX(pElspInfoNode) 

#define RPTE_TE_DS_ELSP_INFO_PHB_DSCP(pElspInfoNode)\
                          TE_DS_ELSP_INFO_PHB_DSCP(pElspInfoNode) 

#define RPTE_TE_DS_ELSP_INFO_ROW_STATUS(pElspInfoNode)\
                          TE_DS_ELSP_INFO_ROW_STATUS(pElspInfoNode)

#define RPTE_TE_DS_ELSP_INFO_STORAGE_TYPE(pElspInfoNode)\
                          TE_DS_ELSP_INFO_STORAGE_TYPE(pElspInfoNode) 

#define RPTE_TE_DS_ELSP_INFO_TRFC_RSRC_INDEX(pElspInfoNode)\
                          TE_DS_ELSP_INFO_RSRC_INDEX(pElspInfoNode) 

#define RPTE_TE_DS_ELSP_INFO_TRFC_PARAMS(pElspInfoNode)\
                          TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode) 

#define RPTE_TE_DS_ELSP_INFO_RSVP_TRFC_PARAMS(pElspInfoNode)\
                          TE_DS_ELSPINFO_RSVP_TRFC_PARAMS(pElspInfoNode) 
/*****************************************************************************/
/* tMplsDiffServTnlInfo macros */

#define RPTE_TE_DS_TNL_CLASS_TYPE(pDiffServTnlInfo)\
             TE_DS_TNL_CLASS_TYPE(pDiffServTnlInfo) 

#define RPTE_TE_DS_TNL_ROW_STATUS(pDiffServTnlInfo)\
             TE_DS_TNL_ROW_STATUS(pDiffServTnlInfo) 

#define RPTE_TE_DS_TNL_STORAGE_TYPE(pDiffServTnlInfo)\
             TE_DS_TNL_STORAGE_TYPE(pDiffServTnlInfo)  

#define RPTE_TE_DS_TNL_SERVICE_TYPE(pDiffServTnlInfo)\
             TE_DS_TNL_SERVICE_TYPE(pDiffServTnlInfo)

#define RPTE_TE_DS_TNL_ELSP_LIST_INDEX(pDiffServTnlInfo)\
             TE_DS_TNL_ELSP_LIST_INDEX(pDiffServTnlInfo)

#define RPTE_TE_DS_TNL_CFG_FLAG(pDiffServTnlInfo)\
             TE_DS_TNL_CFG_FLAG(pDiffServTnlInfo) 

#define RPTE_TE_DS_TNL_ELSP_TYPE(pDiffServTnlInfo)\
             TE_DS_TNL_ELSP_TYPE(pDiffServTnlInfo) 

#define RPTE_TE_DS_TNL_LLSP_PSC_DSCP(pDiffServTnlInfo)\
             TE_DS_TNL_LLSP_PSC_DSCP(pDiffServTnlInfo)

#define RPTE_TE_DS_TNL_ELSP_INFO_LIST_INFO(pDiffServTnlInfo)\
             TE_DS_TNL_ELSP_INFO_LIST_INFO(pDiffServTnlInfo)

#define RPTE_TE_DS_PARAMS_ELSPLIST_PTR(pDiffServTnlInfo)\
             TE_DS_PARAMS_ELSPLIST_PTR(pDiffServTnlInfo)

#define RPTE_DS_ELSP_TPARAM_TBR     TE_ELSP_INFO_RSVPTE_TPARAM_TBR
#define RPTE_DS_ELSP_TPARAM_TBS     TE_ELSP_INFO_RSVPTE_TPARAM_TBS
#define RPTE_DS_ELSP_TPARAM_PDR     TE_ELSP_INFO_RSVPTE_TPARAM_PDR
#define RPTE_DS_ELSP_TPARAM_MPU     TE_ELSP_INFO_RSVPTE_TPARAM_MPU
#define RPTE_DS_ELSP_TPARAM_MPS     TE_ELSP_INFO_RSVPTE_TPARAM_MPS

#define RPTE_TE_DS_PEROABASED_RESOURCES   TE_DS_PEROABASED_RESOURCES 
#define RPTE_TE_DS_CLASSTYPE_RESOURCES    TE_DS_CLASSTYPE_RESOURCES 

#define RPTE_DS_DF_DSCP        TE_DS_DF_DSCP
#define RPTE_DS_CS1_DSCP       TE_DS_CS1_DSCP
#define RPTE_DS_CS2_DSCP       TE_DS_CS2_DSCP
#define RPTE_DS_CS3_DSCP       TE_DS_CS3_DSCP
#define RPTE_DS_CS4_DSCP       TE_DS_CS4_DSCP
#define RPTE_DS_CS5_DSCP       TE_DS_CS5_DSCP
#define RPTE_DS_CS6_DSCP       TE_DS_CS6_DSCP
#define RPTE_DS_CS7_DSCP       TE_DS_CS7_DSCP
#define RPTE_DS_EF_DSCP        TE_DS_EF_DSCP
#define RPTE_DS_AF11_DSCP      TE_DS_AF11_DSCP
#define RPTE_DS_AF12_DSCP      TE_DS_AF12_DSCP
#define RPTE_DS_AF13_DSCP      TE_DS_AF13_DSCP
#define RPTE_DS_AF21_DSCP      TE_DS_AF21_DSCP
#define RPTE_DS_AF22_DSCP      TE_DS_AF22_DSCP
#define RPTE_DS_AF23_DSCP      TE_DS_AF23_DSCP
#define RPTE_DS_AF31_DSCP      TE_DS_AF31_DSCP
#define RPTE_DS_AF32_DSCP      TE_DS_AF32_DSCP
#define RPTE_DS_AF33_DSCP      TE_DS_AF33_DSCP
#define RPTE_DS_AF41_DSCP      TE_DS_AF41_DSCP
#define RPTE_DS_AF42_DSCP      TE_DS_AF42_DSCP
#define RPTE_DS_AF43_DSCP      TE_DS_AF43_DSCP
#define RPTE_DS_AF1_PSC_DSCP   TE_DS_AF11_DSCP
#define RPTE_DS_AF2_PSC_DSCP   TE_DS_AF21_DSCP
#define RPTE_DS_AF3_PSC_DSCP   TE_DS_AF31_DSCP
#define RPTE_DS_AF4_PSC_DSCP   TE_DS_AF41_DSCP
#define RPTE_DS_EF1_DSCP       TE_DS_EF1_DSCP  

#endif
/*---------------------------------------------------------------------------*/
/*                        End of file rptedste.h                             */
/*---------------------------------------------------------------------------*/
