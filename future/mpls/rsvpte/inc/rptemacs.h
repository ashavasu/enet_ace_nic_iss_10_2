/********************************************************************
 *
 * $RCSfile: rptemacs.h,v $
 *
 * $Id: rptemacs.h,v 1.46 2018/01/03 11:31:23 siva Exp $
 *
 * $Date: 2018/01/03 11:31:23 $
 *
 * $Revision: 1.46 $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptemacs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux  (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains macros used for RSVPTE Support in 
 *                             RSVP.
 *---------------------------------------------------------------------------*/

#ifndef _RPTEMACS_H
#define _RPTEMACS_H

/* The following defs are taken from FutureRSVP - rsvpmacs.h */
#define RSVP_TE_GBL_INFO                   gpRsvpTeGblInfo

#define RSVP_TSK_ID                     gRsvpTeGblInfo.gRsvpTeTaskId
#define RSVP_INITIALISED                gRsvpTeGblInfo.u1RpteInitialised
#define RSVP_GBL_SEM_ID                 gRsvpTeGblInfo.gRsvpTeSemId
#define RSVP_GBL_QID                    gRsvpTeGblInfo.gRsvpQId

#define RSVP_SWITCH_BACK_DEFAULT        3
#define RSVP_SWITCH_BACK_ERROR          120

#ifdef LNXIP4_WANTED
#define RSVP_GBL_IP_QID                 gRsvpTeGblInfo.gRsvpIpQId
#endif

#define RSVP_GBL_RT_CHG_NTF_QID         gRsvpTeGblInfo.gRsvpRtChgNtfQId
#define RSVP_GBL_IF_CHG_NTF_QID         gRsvpTeGblInfo.gRsvpIfChgNtfQId
#define RSVP_GBL_CSPF_QID               gRsvpTeGblInfo.gRsvpCspfQId
#define RSVP_GBL_L3VPN_QID               gRsvpTeGblInfo.gRsvpL3VpnQId

#define RSVP_GBL_TE_DBG_FLAG            RSVP_TE_GBL_INFO->u4RsvpTeDbgFlag
#define RSVP_GBL_TE_DBG_LVL            RSVP_TE_GBL_INFO->u4RsvpTeDbgLvl 
#define RSVP_GBL_SOCK_FD                RSVP_TE_GBL_INFO->i4RsvpSockFd
#define RSVP_GBL_UDP_READ_SOCK_FD       RSVP_TE_GBL_INFO->i4RpteUdpReadSockFd
#define RSVP_GBL_UDP_WRITE_SOCK_FD      RSVP_TE_GBL_INFO->i4RpteUdpWriteSockFd

#define RSVP_GBL_IF_HSH_TBL             RSVP_TE_GBL_INFO->pIfHashTable
#define RSVP_GBL_SENDER_TBL             RSVP_TE_GBL_INFO->pSenderTable
#define RSVP_GBL_TIMER_LIST             RSVP_TE_GBL_INFO->RsvpTimerList


#define RSVP_IS_RR_BIT_SET(u1VersionFlag) \
        ((u1VersionFlag & RSVP_HDR_FLAG_WITH_RR) ? RPTE_TRUE : RPTE_FALSE)
#define RSVP_HDR_VER_FLAG(x)   ((x)->u1VerFlag)
#define RSVP_HDR_MSG_TYPE(x)   ((x)->u1MsgType)
#define RSVP_HDR_CHECK_SUM(x)  ((x)->u2CheckSum)
#define RSVP_HDR_SEND_TTL(x)   ((x)->u1SendTtl)
#define RSVP_HDR_LENGTH(x)     ((x)->u2Length)

#define OBJ_HDR_LENGTH(x)       ((x)->u2Length)
#define OBJ_HDR_CLASS_NUM(x)    ((x)->u1ClassNum)
#define OBJ_HDR_CLASS_TYPE(x)   ((x)->u1ClassType)
#define OBJ_HDR_CLASS_NUM_TYPE(x)   ((x)->u2ClassNumType)
#define NEXT_OBJ(x)  ( (tObjHdr *)(VOID *) ( (UINT1 *) (x) + OSIX_NTOHS ((x)->u2Length) ) )

#define VALIDATE_OBJHDR(pObjHdr, pEndOfPkt) \
              (((UINT4 *)(VOID *) pObjHdr != (UINT4 *)(VOID *) pEndOfPkt) && \
                   (((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr)) & 0x03) \
               || (OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr) ) < 4) \
                  || ((UINT4 *)(VOID *)NEXT_OBJ (pObjHdr) > (UINT4 *)(VOID *) pEndOfPkt)) \
                   || (((OSIX_NTOHS (OBJ_HDR_LENGTH (pObjHdr))) % 4 ) != 0)))

#define HELLO_OBJ(x)      ((x)->Hello)

#define HELLO_SRC_INS(x)  ((x)->u4SrcIns)
#define HELLO_DST_INS(x)  ((x)->u4DstIns)
#define HELLO_TIME_TO_DIE(x) ((x)->u4TimeToDie)

#define RSVP_HOP_OBJ_HDR(x)  ((x)->ObjHdr)
#define RSVP_HOP_OBJ(x)  ((x)->RsvpHop)

#define RSVP_HOP_ADDR(x)  ((x)->u4HopAddr)
#define RSVP_HOP_LIH(x)  ((x)->u4Lih)

#define TIME_VALUES_OBJ_HDR(x)  ((x)->ObjHdr)
#define TIME_VALUES_OBJ(x)  ((x)->TimeValues)

#define TIME_VALUES_PERIOD(x)  ((x)->u4RefreshPeriod)

#define ERROR_SPEC_OBJ_HDR(x)  ((x)->ObjHdr)
#define ERROR_SPEC_OBJ(x)  ((x)->ErrorSpec)

#define ERROR_SPEC_ADDR(x) ((x)->u4ErrNodeAddr)
#define ERROR_SPEC_FLAGS(x) ((x)->u1Flags)
#define ERROR_SPEC_CODE(x) ((x)->u1ErrCode)
#define ERROR_SPEC_VALUE(x) ((x)->u2ErrValue)

#define SCOPE_OBJ_HDR(x)  ((x)->ObjHdr)
#define SCOPE_OBJ(x)  ((x)->Scope)

#define SCOPE_ADDR(x) ((x)->u4Addr)

#define STYLE_OBJ_HDR(x)  ((x)->ObjHdr)
#define STYLE_OBJ(x)  ((x)->Style)

#define STYLE_FLAGS(x)  ((x)->u1Flags)
#define STYLE_OPT_VECT(x)  ((x)->au1OptVect)

#define FILTER_SPEC_OBJ_HDR(x)  ((x)->ObjHdr)
#define FILTER_SPEC_OBJ(x)  ((x)->FilterSpec)

#define FILTER_SPEC_ADDR(x)  ((x)->u4Addr)
#define FILTER_SPEC_RESERVED(x)  ((x)->u2Reserved)
#define FILTER_SPEC_PORT(x)  ((x)->u2Port)

#define SENDER_TEMPLATE_OBJ_HDR(x)  ((x)->ObjHdr)
#define SENDER_TEMPLATE_OBJ(x)  ((x)->SenderTemplate)

#define SENDER_TEMPLATE_ADDR(x)  ((x)->u4Addr)
#define SENDER_TEMPLATE_RESERVED(x)  ((x)->u2Reserved)
#define SENDER_TEMPLATE_PORT(x)  ((x)->u2Port)

#define SENDER_TSPEC_OBJ_HDR(x) ((x)->ObjHdr)
#define SENDER_TSPEC_OBJ(x) ((x)->SenderTspec)

#define FRR_DETOUR_OBJ_HDR(x) ((x).ObjHdr)

#define FRR_FAST_REROUTE(x) ((x)->RsvpTeFastReroute)

#define FRR_FAST_REROUTE_FLAGS(x) ((x)->u1Flags)

#define SENDER_TSPEC_RESERVED(x) ((x)->u4Reserved)

#define FLOW_SPEC_OBJ_HDR(x) ((x)->ObjHdr)
#define FLOW_SPEC_OBJ(x) ((x)->FlowSpec)

#define FLOW_SPEC_RESERVED(x) ((x)->u4Reserved)

#define RESV_TSPEC_BKT_RATE(x) ((x).ServiceParams.ClsService.TBParams.fBktRate)
#define RESV_TSPEC_PEAK_RATE(x) ((x).ServiceParams.ClsService.TBParams.fPeakRate)
#define RESV_TSPEC_BKT_SIZE(x) ((x).ServiceParams.ClsService.TBParams.fBktSize)
#define RESV_TSPEC_MIN_SIZE(x) ((x).ServiceParams.ClsService.TBParams.u4MinSize)
#define RESV_TSPEC_MAX_SIZE(x) ((x).ServiceParams.ClsService.TBParams.u4MaxSize)
#define RESV_RSPEC_RATE(x) ((x)->ServiceParams.GLService.GLRSpec.fRSpecRate)
#define RESV_RSPEC_SLACK(x) ((x)->ServiceParams.GLService.GLRSpec.u4Slack)

#define RESV_FWD_TSPEC_BKT_RATE(x) ((x)->ServiceParams.CLService.TBParams.fBktRate)
#define RESV_FWD_TSPEC_PEAK_RATE(x) ((x)->ServiceParams.CLService.TBParams.fPeakRate)
#define RESV_FWD_TSPEC_BKT_SIZE(x) ((x)->ServiceParams.CLService.TBParams.fBktSize)
#define RESV_FWD_TSPEC_MIN_SIZE(x) ((x)->ServiceParams.CLService.TBParams.u4MinSize)
#define RESV_FWD_TSPEC_MAX_SIZE(x) ((x)->ServiceParams.CLService.TBParams.u4MaxSize)
#define RESV_FWD_RSPEC_RATE(x) ((x)->ServiceParams.GLService.GLRSpec.fRSpecRate)
#define RESV_FWD_RSPEC_SLACK(x) ((x)->ServiceParams.GLService.GLRSpec.u4Slack)

#define RESV_CONF_OBJ_HDR(x)  ((x)->ObjHdr)
#define RESV_CONF_OBJ(x)  ((x)->ResvConf)

#define RESV_CONF_ADDR(x) ((x)->u4RecevAddr)

#define ADSPEC_OBJ(x) ((x)->AdSpec)

#define NBR_ENTRY_NEXT(pNbrEntry)              ((pNbrEntry)->pNext)
#define NBR_ENTRY_ADDR(pNbrEntry)              ((pNbrEntry)->u4Addr)
#define NBR_ENTRY_ENCAP(pNbrEntry)             ((pNbrEntry)->u1Encap)
#define NBR_ENTRY_STATUS(pNbrEntry)            ((pNbrEntry)->u1Status)
#define NBR_ENTRY_TNL_LIST(pNbrEntry)          ((pNbrEntry)->TnlList)
#define NBR_ENTRY_RR_CAPABLE(pNbrEntry)        ((pNbrEntry->u1RRCapable))
#define NBR_ENTRY_RR_STATE(pNbrEntry)          ((pNbrEntry->u1RRState))
#define NBR_ENTRY_RMD_CAPABLE(pNbrEntry)       ((pNbrEntry->u1RMDCapable))
#define NBR_ENTRY_SREFRESH_STARTED(pNbrEntry)  ((pNbrEntry->u1SRefreshStarted))
#define NBR_ENTRY_HELLO_SPRT(pNbrEntry)        ((pNbrEntry->u1HelloSupported))
#define NBR_ENTRY_HELLO_STATE(pNbrEntry)       ((pNbrEntry->u1HelloState))
#define NBR_ENTRY_HELLO_REL(pNbrEntry)         ((pNbrEntry->u1HelloRelation))
#define NBR_ENTRY_SRC_INST_INFO(pNbrEntry)     ((pNbrEntry->u4SrcInstInfo)) 
#define NBR_ENTRY_DEST_INST_INFO(pNbrEntry)    ((pNbrEntry->u4DstInstInfo)) 
#define NBR_ENTRY_NBR_CREATE_TIME(pNbrEntry)   ((pNbrEntry->u4NbrCreateTime)) 
#define NBR_ENTRY_NBR_LCL_RP_DETECT_TIME(pNbrEntry) \
                                   ((pNbrEntry->u4LclPrtDetectTime))
#define NBR_ENTRY_NBR_NUM_TNLS(pNbrEntry)           ((pNbrEntry->u4NbrNumTnls))
#define NBR_ENTRY_EPOCH_VALUE(pNbrEntry)       ((pNbrEntry->u4Epoch))
#define NBR_ENTRY_MAX_MSG_ID(pNbrEntry)        ((pNbrEntry->u4MaxMsgId))
#define NBR_ENTRY_IF_ENTRY(pNbrEntry)          ((pNbrEntry->pIfEntry))
#define NBR_ENTRY_SREFRESH_LIST(pNbrEntry)     ((pNbrEntry->SRefreshList))
#define NBR_ENTRY_SREFRESH_TMR_PARAM(pNbrEntry)\
        ((pNbrEntry->SRefreshTmrParam))
#define NBR_ENTRY_SREFRESH_TMR(pNbrEntry)      ((pNbrEntry->SRefreshTmr))

#define NBR_ENTRY_IF_MTU(pNbrEntry)\
        RPTE_IF_MAX_MTU(NBR_ENTRY_IF_ENTRY(pNbrEntry))

#define NBR_ENTRY_SREFRESH_LIST_SIZE(pNbrEntry)\
        (MAC_HEADER_LENGTH + SHIM_HDR_LEN + sizeof (tRsvpIpHdr) +\
         sizeof (tUdpHdr) + sizeof (tRsvpHdr) + RPTE_FLAG_EPOCH_LEN +\
         (TMO_SLL_Count (&NBR_ENTRY_SREFRESH_LIST(pNbrEntry)) * RPTE_MSG_ID_LEN))

#define IF_ENTRY_TIMER(x) ((x)->IfEntryTmr)
#define IF_ENTRY_TIMER_PARAM(x) ((x)->IfEntryTmrParam)
#define IF_ENTRY_IF_INDEX(x) ((x)->u4IfIndex)
#define IF_ENTRY_ADDR(x) ((x)->u4Addr)
#define IF_ENTRY_MASK(x) ((x)->u4Mask)
#define IF_ENTRY_UDP_NBRS(x) ((x)->u4UdpNbrs)
#define IF_ENTRY_IP_NBRS(x) ((x)->u4IpNbrs)
#define IF_ENTRY_NBRS(x) ((x)->u4Nbrs)
#define IF_ENTRY_LINK_ATTR(x) ((x)->RpteIfInfo.u4LinkAttr)
#define IF_ENTRY_ENABLED(x) ((x)->u1Enabled)
#define IF_ENTRY_UDP_REQUIRED(x) ((x)->u1UdpRequired)
#define IF_ENTRY_REFRESH_BLOCKADE_MULTIPLE(x) ((x)->u2RefreshBlockadeMultiple)
#define IF_ENTRY_REFRESH_MULTIPLE(x) ((x)->u2RefreshMultiple)
#define IF_ENTRY_TTL(x) ((x)->u1Ttl)
#define IF_ENTRY_REFRESH_INTERVAL(x) ((x)->u4RefreshInterval)
#define IF_ENTRY_PLR_ID(x) ((x)->u4PlrId)
#define IF_ENTRY_AVOID_NODE_ID(x) ((x)->u4AvoidNodeId)
#define IF_ENTRY_ROUTE_DELAY(x) ((x)->u4RouteDelay)
#define IF_ENTRY_NBR_LIST(x) ((x)->NbrList)
#define IF_ENTRY_FAC_TNL_LIST(x) ((x)->FacilityTnlList)
#define IF_ENTRY_STATUS(x) ((x)->u1Status)
#define IF_ENTRY_IF_ID(x) ((x)->IfId)
#define IF_ENTRY_HELLO_SPRTD(x)  ((x)->u1HelloSprt)
#define IF_ENTRY_MAX_NO_TNLS(x) ((x)->u2MaxTnls)
#define IF_ENTRY_NEXT_HASH_NODE(x) ((x)->NextIfHashNode)

#define IF_ENTRY_STATS_INFO(x)  ((x)->IfStatsInfo)

#define RPTE_IF_STAT_ENTRY_NUM_OF_TUNNELS(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumTnls)

#define RPTE_IF_STAT_ENTRY_NUM_MSG_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumMsgSent)

#define RPTE_IF_STAT_ENTRY_NUM_MSG_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumMsgRcvd)

#define RPTE_IF_STAT_ENTRY_NUM_HELLO_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumHelloSent)

#define RPTE_IF_STAT_ENTRY_NUM_HELLO_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumHelloRcvd)

#define RPTE_IF_STAT_ENTRY_PATH_ERR_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumPathErrSent)

#define RPTE_IF_STAT_ENTRY_PATH_ERR_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumPathErrRcvd)

#define RPTE_IF_STAT_ENTRY_PATH_TEAR_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumPathTearSent)

#define RPTE_IF_STAT_ENTRY_PATH_TEAR_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumPathTearRcvd)

#define RPTE_IF_STAT_ENTRY_RESV_ERR_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumResvErrSent)

#define RPTE_IF_STAT_ENTRY_RESV_ERR_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumResvErrRcvd)

#define RPTE_IF_STAT_ENTRY_RESV_TEAR_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumResvTearSent)

#define RPTE_IF_STAT_ENTRY_RESV_TEAR_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumResvTearRcvd)

#define RPTE_IF_STAT_ENTRY_RESV_CONF_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumResvConfSent)

#define RPTE_IF_STAT_ENTRY_RESV_CONF_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumResvConfRcvd)

#define RPTE_IF_STAT_ENTRY_BUNDLE_MSG_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumBundleMsgSent)

#define RPTE_IF_STAT_ENTRY_BUNDLE_MSG_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumBundleMsgRcvd)

#define RPTE_IF_STAT_ENTRY_SREFRESH_MSG_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumSRefreshMsgSent)

#define RPTE_IF_STAT_ENTRY_SREFRESH_MSG_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumSRefreshMsgRcvd)

#define RPTE_IF_STAT_ENTRY_PATH_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumPathSent)
    
#define RPTE_IF_STAT_ENTRY_PATH_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumPathRcvd)
    
#define RPTE_IF_STAT_ENTRY_RESV_SENT(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumResvSent)
    
#define RPTE_IF_STAT_ENTRY_RESV_RCVD(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumResvRcvd)
 
#define RPTE_IF_STAT_ENTRY_PKT_DISCRDED(pIfEntry)\
        ((IF_ENTRY_STATS_INFO(pIfEntry)).u4IfNumPktDiscrded)
    
#define RPTE_IF_ENTRY_MIN_VPI(x) ((x)->RpteIfInfo.u2MinVpi)
#define RPTE_IF_ENTRY_MIN_VCI(x) ((x)->RpteIfInfo.u2MinVci)
#define RPTE_IF_ENTRY_MAX_VPI(x) ((x)->RpteIfInfo.u2MaxVpi)
#define RPTE_IF_ENTRY_MAX_VCI(x) ((x)->RpteIfInfo.u2MaxVci)
#define RPTE_IF_ENTRY_LBL_SPACE_ID(x) ((x)->RpteIfInfo.u2LblSpaceId)

#define FLOW_DESCR_FILTER_SPEC_OBJ(x) ((x)->pFilterSpecObj)
#define FLOW_DESCR_FLOW_SPEC_OBJ(x) ((x)->pFlowSpecObj)
#define FLOW_DESCR_STATUS(x) ((x)->u1Status)
#define FLOW_DESCR_MATCHED_PSB_COUNT(x) ((x)->u4MatchedPsbCount)
#define FLOW_DESCR_NEXT(x) ((x)->pNext)

/* MsgIdObj Structure */
#define RPTE_MSG_ID_OBJ_FLAGS(pMsgIdObj) \
        (pMsgIdObj->u1Flags)
        
#define RPTE_MSG_ID_OBJ_IS_MSG_ID_PRESENT(pMsgIdObj) \
        (pMsgIdObj->u1IsMsgIdPresent)
        
#define RPTE_MSG_ID_OBJ_EPOCH(pMsgIdObj) \
        (pMsgIdObj->u4Epoch)
        
#define RPTE_MSG_ID_OBJ_MSG_ID(pMsgIdObj) \
        (pMsgIdObj->u4MsgId)

/* MsgIdNodeObj Structure */
#define RPTE_MSG_ID_OBJ_NODE_SLL_NODE(pMsgIdObjNode) \
        (pMsgIdObjNode->NextMsgIdObjNode)

#define RPTE_MSG_ID_OBJ_NODE_MSG_ID_OBJ(pMsgIdObjNode) \
        (pMsgIdObjNode->MsgIdObj)

#define RPTE_MSG_ID_OBJ_NODE_FLAGS(pMsgIdObjNode) \
        (RPTE_MSG_ID_OBJ_FLAGS ((&RPTE_MSG_ID_OBJ_NODE_MSG_ID_OBJ\
                                 (pMsgIdObjNode))))

#define RPTE_MSG_ID_OBJ_NODE_IS_MSG_ID_PRESENT(pMsgIdObjNode) \
        (RPTE_MSG_ID_OBJ_IS_MSG_ID_PRESENT ((&RPTE_MSG_ID_OBJ_NODE_MSG_ID_OBJ\
                                            (pMsgIdObjNode))))

#define RPTE_MSG_ID_OBJ_NODE_EPOCH(pMsgIdObjNode) \
        (RPTE_MSG_ID_OBJ_EPOCH ((&RPTE_MSG_ID_OBJ_NODE_MSG_ID_OBJ\
                                 (pMsgIdObjNode))))

#define RPTE_MSG_ID_OBJ_NODE_MSG_ID(pMsgIdObjNode) \
        (RPTE_MSG_ID_OBJ_MSG_ID ((&RPTE_MSG_ID_OBJ_NODE_MSG_ID_OBJ\
                                  (pMsgIdObjNode))))

/* MsgId List Structure */
#define RPTE_MSG_ID_LIST_OBJ_HDR(pMsgIdListObj) \
        (pMsgIdListObj->ObjHdr)

#define RPTE_MSG_ID_LIST_EPOCH(pMsgIdListObj) \
        (pMsgIdListObj->u4Epoch)

#define RPTE_MSG_ID_LIST_MSG_ID(pMsgIdListObj, u4Index) \
        (pMsgIdListObj->au4MsgId[(u4Index)])

/* Pkt Map Structure */
#define PKT_MAP_TNL_ALLOC_FLAG(x)            ((x)->u1TnlAlloc)
#define PKT_MAP_RE_ROUTE_FLAG(x)             ((x)->u1ReRtFlag)
#define PKT_MAP_RPTE_MSG_FLAG(x)             ((x)->u1RpteMsgFlag)
#define PKT_MAP_RPTE_SSN_OBJ(x)              ((x)->pRsvpTeSsnObj)
#define PKT_MAP_RPTE_ERO(x)                  ((x)->ErObj)
#define PKT_MAP_RPTE_ERO_HDR(x)              ((x)->ErObj.ObjHdr)
#define PKT_MAP_RPTE_ERO_LIST(x) \
        ((x)->ErObj.ErHopList)

#define PKT_MAP_RPTE_RRO(x)                  ((x)->RrObj)
#define PKT_MAP_RPTE_RRO_HDR(x)              ((x)->RrObj.ObjHdr)
#define PKT_MAP_RPTE_LBL_OBJ(x)              ((x)->pGenLblObj)
#define PKT_MAP_RPTE_ADNL_LBL_OBJ(x)         ((x)->pAdnlLblObj)
#define PKT_MAP_RPTE_ADNL_LBL_OBJ_HDR(x)     ((x)->AdnlLblObj.ObjHdr)
#define PKT_MAP_RPTE_LBL_OBJ_HDR(x)          ((x)->GenLblObj.ObjHdr)
#define PKT_MAP_RPTE_FILTER_SPEC_OBJ(x)      ((x)->pRsvpTeFilterSpecObj)
#define PKT_MAP_RPTE_SND_TMP_OBJ(x)          ((x)->pRsvpTeSndrTmpObj)
#define PKT_MAP_RPTE_FAST_REROUTE_OBJ(x)     ((x)->pRsvpTeFastRerouteObj)
#define PKT_MAP_RPTE_DETOUR_OBJ(x)           ((x)->RsvpTeDetourObj)
#define PKT_MAP_FLOW_SPEC_OBJ(x)             ((x)->pFlowSpecObj)
#define PKT_MAP_RPTE_ADNL_FILTER_SPEC_OBJ(x) ((x)->pAdnlRsvpTeFilterSpecObj)
#define PKT_MAP_HELLO_OBJ(x)                 ((x)->pHelloObj)

#define PKT_MAP_SNDR_TMP_LSP_ID(x)  \
                ((x)->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.u2LspId)

#define PKT_MAP_SNDR_TMP_SNDR_ADDR(x)  \
  (UINT4)(*((UINT4*)(&((x)->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.TnlSndrAddr))))

#define PKT_MAP_SSN_EGRESS_ADDR(x) \
  ((x)->pRsvpTeSsnObj->RsvpTeSession.TnlDestAddr)

#define PKT_MAP_LBL_REQ_OBJ(x) \
                ((x)->pGenLblReqObj->GenLblReq)

#define PKT_MAP_LBL_REQ_ATM_RNG(x) \
                ((x)->pLblReqAtmRngObj->GenLblReq)

#define PKT_MAP_LBL_REQ_ATM_RNG_ENTRY(x) \
                ((x)->pLblReqAtmRngObj->AtmLblRngEntry)

#define PKT_MAP_LBL_OBJ_LBL(x) \
                ((x)->pGenLblObj->Label)

#define PKT_MAP_LBL_OBJ_GEN_LBL(x) \
                ((x)->pGenLblObj->Label.u4GenLbl)

#define PKT_MAP_LBL_OBJ_ATM_LBL(x) \
                ((x)->pGenLblObj->Label.AtmLbl)

#define PKT_MAP_LBL_OBJ_ATM_LBL_VPI(x) \
                ((x)->pGenLblObj->Label.AtmLbl.u2Vpi)

#define PKT_MAP_LBL_OBJ_ATM_LBL_VCI(x) \
                ((x)->pGenLblObj->Label.AtmLbl.u2Vci)

#define PKT_MAP_ADNL_LBL_OBJ_LBL(x) \
                ((x)->pAdnlLblObj->Label)

#define PKT_MAP_ADNL_LBL_OBJ_GEN_LBL(x) \
                ((x)->pAdnlLblObj->Label.u4GenLbl)

#define PKT_MAP_ADNL_LBL_OBJ_ATM_LBL(x) \
                ((x)->pAdnlLblObj->Label.AtmLbl)

#define PKT_MAP_ADNL_LBL_OBJ_ATM_LBL_VPI(x) \
                ((x)->pAdnlLblObj->Label.AtmLbl.u2Vpi)

#define PKT_MAP_ADNL_LBL_OBJ_ATM_LBL_VCI(x) \
                ((x)->pAdnlLblObj->Label.AtmLbl.u2Vci)

#define PKT_MAP_RPTE_SSN_OBJ_TNL_ID(x) \
                ((x)->pRsvpTeSsnObj->RsvpTeSession.u2TnlId)

#define PKT_MAP_RPTE_SNDR_TMP_OBJ_LSP_ID(x) \
                     ((x)->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.u2LspId)

#define PKT_MAP_RPTE_SNDR_TMP_OBJ_SNDR_ADDR(x) \
                     ((x)->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.TnlSndrAddr)

#define PKT_MAP_RPTE_FILTER_SPEC_OBJ_LSP_ID(x) \
                    ((x)->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.u2LspId)

#define PKT_MAP_RPTE_ADNL_FILTER_SPEC_OBJ_LSP_ID(x) \
                    ((x)->pAdnlRsvpTeFilterSpecObj->RsvpTeFilterSpec.u2LspId)

#define PKT_MAP_RPTE_FILTER_SPEC_OBJ_SNDR_ADDR(x) \
                    ((x)->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.TnlSndrAddr)

#define PKT_MAP_TSPEC_BKT_RATE(x) \
             ((x)->pSenderTspecObj->SenderTspec.TBParams.fBktRate)

#define PKT_MAP_TSPEC_BKT_SIZE(x) \
             ((x)->pSenderTspecObj->SenderTspec.TBParams.fBktSize)

#define PKT_MAP_TSPEC_PEAK_RATE(x) \
             ((x)->pSenderTspecObj->SenderTspec.TBParams.fPeakRate)

#define PKT_MAP_TSPEC_MIN_SIZE(x) \
             ((x)->pSenderTspecObj->SenderTspec.TBParams.u4MinSize)

#define PKT_MAP_TSPEC_MAX_SIZE(x) \
             ((x)->pSenderTspecObj->SenderTspec.TBParams.u4MaxSize)

#define PKT_MAP_SSN_OBJ_TNL_ID(pPktMap) \
            ((pPktMap)->pRsvpTeSsnObj->RsvpTeSession.u2TnlId)

#define PKT_MAP_SNDR_TMP_OBJ_LSP_ID(pPktMap) \
            ((pPktMap)->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.u2LspId)

#define PKT_MAP_SNDR_TMP_OBJ_SNDR_ADDR(pPktMap) \
      ((pPktMap)->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.TnlSndrAddr)

/* RPTE Session Obj */
#define PKT_MAP_RPTE_SSN_OBJ_HLEN(pPktMap) \
      ((pPktMap)->pRsvpTeSsnObj->ObjHdr.u2Length)

#define PKT_MAP_RPTE_SSN_OBJ_HCNUMTYPE(pPktMap) \
      ((pPktMap)->pRsvpTeSsnObj->ObjHdr.u2ClassNumType)

#define PKT_MAP_RPTE_SSN_DEST_ADDR(pPktMap) \
      ((pPktMap)->pRsvpTeSsnObj->RsvpTeSession.TnlDestAddr)

#define PKT_MAP_RPTE_SSN_RSVD(pPktMap) \
      ((pPktMap)->pRsvpTeSsnObj->RsvpTeSession.u2Rsvd)

#define PKT_MAP_RPTE_SSN_TNL_ID(pPktMap) \
      ((pPktMap)->pRsvpTeSsnObj->RsvpTeSession.u2TnlId)

#define PKT_MAP_RPTE_SSN_EXTN_TNL_ID(pPktMap) \
      ((pPktMap)->pRsvpTeSsnObj->RsvpTeSession.ExtnTnlId)
/* Sender Template Obj */
#define PKT_MAP_RPTE_SNDR_TMP_HLEN(pPktMap) \
      ((pPktMap)->pRsvpTeSndrTmpObj->ObjHdr.u2Length)

#define PKT_MAP_RPTE_SNDR_TMP_HCNUMTYPE(pPktMap) \
      ((pPktMap)->pRsvpTeSndrTmpObj->ObjHdr.u2ClassNumType)

#define PKT_MAP_RPTE_ST_TNL_SNDR_ADDR(pPktMap) \
      ((pPktMap)->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.TnlSndrAddr)

#define PKT_MAP_RPTE_SNDR_TMP_RSVD(pPktMap) \
      ((pPktMap)->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.u2Rsvd)

#define PKT_MAP_RPTE_SNDR_TMP_LSPID(pPktMap) \
      ((pPktMap)->pRsvpTeSndrTmpObj->RsvpTeSndrTmp.u2LspId)

/* Filter Spec Obj */
#define PKT_MAP_RPTE_FLTR_SPEC_HLEN(pPktMap) \
      ((pPktMap)->pRsvpTeFilterSpecObj->ObjHdr.u2Length)

#define PKT_MAP_RPTE_FLTR_SPEC_HCNTYPE(pPktMap) \
      ((pPktMap)->pRsvpTeFilterSpecObj->ObjHdr.u2ClassNumType)

#define PKT_MAP_RPTE_FS_TNL_SNDR_ADDR(pPktMap) \
      ((pPktMap)->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.TnlSndrAddr)

#define PKT_MAP_RPTE_FLTR_SPEC_RSVD(pPktMap) \
      ((pPktMap)->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.u2Rsvd)

#define PKT_MAP_RPTE_FLTR_SPEC_LSPID(pPktMap) \
      ((pPktMap)->pRsvpTeFilterSpecObj->RsvpTeFilterSpec.u2LspId)

/* Adnl Filter spec Obj for reroute condition */
#define PKT_MAP_RPTE_AFLTR_SPEC_HLEN(pPktMap) \
      ((pPktMap)->pAdnlRsvpTeFilterSpecObj->ObjHdr.u2Length)

#define PKT_MAP_RPTE_AFLTR_SPEC_HCNTYPE(pPktMap) \
      ((pPktMap)->pAdnlRsvpTeFilterSpecObj->ObjHdr.u2ClassNumType)

#define PKT_MAP_RPTE_AFS_TNL_SNDR_ADDR(pPktMap) \
      ((pPktMap)->pAdnlRsvpTeFilterSpecObj->RsvpTeFilterSpec.TnlSndrAddr)

#define PKT_MAP_RPTE_AFLTR_SPEC_RSVD(pPktMap) \
      ((pPktMap)->pAdnlRsvpTeFilterSpecObj->RsvpTeFilterSpec.u2Rsvd)

#define PKT_MAP_RPTE_AFLTR_SPEC_LSPID(pPktMap) \
      ((pPktMap)->pAdnlRsvpTeFilterSpecObj->RsvpTeFilterSpec.u2LspId)

/* Ssn Attr Obj */
#define PKT_MAP_RPTE_SSN_ATTR_HLEN(pPktMap) \
      ((pPktMap)->SsnAttrObj.ObjHdr.u2Length)

#define PKT_MAP_RPTE_SSN_ATTR_HCNTYPE(pPktMap) \
      ((pPktMap)->SsnAttrObj.ObjHdr.u2ClassNumType)

#define PKT_MAP_RPTE_SSN_ATTR_SPRIO(pPktMap) \
      ((pPktMap)->SsnAttrObj.SsnAttr.u1SetPrio)

#define PKT_MAP_RPTE_SSN_ATTR_HPRIO(pPktMap) \
      ((pPktMap)->SsnAttrObj.SsnAttr.u1HoldPrio)

#define PKT_MAP_RPTE_SSN_ATTR_FLAGS(pPktMap) \
      ((pPktMap)->SsnAttrObj.SsnAttr.u1Flags)

#define PKT_MAP_RPTE_SSN_ATTR_NLEN(pPktMap) \
      ((pPktMap)->SsnAttrObj.SsnAttr.u1NameLen)

#define PKT_MAP_RPTE_SSN_ATTR_SNAME(pPktMap) \
      ((pPktMap)->SsnAttrObj.SsnAttr.au1SsnName)

/* RA Ssn Attr Obj */
#define PKT_MAP_RPTE_RA_SSN_ATTR_HLEN(pPktMap) \
      ((pPktMap)->RASsnAttrObj.ObjHdr.u2Length)

#define PKT_MAP_RPTE_RA_SSN_ATTR_HCNTYPE(pPktMap) \
      ((pPktMap)->RASsnAttrObj.ObjHdr.u2ClassNumType)

#define PKT_MAP_RPTE_RA_SSN_ATTR_SPRIO(pPktMap) \
      ((pPktMap)->RASsnAttrObj.RASsnAttr.SsnAttr.u1SetPrio)

#define PKT_MAP_RPTE_RA_SSN_ATTR_HPRIO(pPktMap) \
      ((pPktMap)->RASsnAttrObj.RASsnAttr.SsnAttr.u1HoldPrio)

#define PKT_MAP_RPTE_RA_SSN_ATTR_FLAGS(pPktMap) \
      ((pPktMap)->RASsnAttrObj.RASsnAttr.SsnAttr.u1Flags)

#define PKT_MAP_RPTE_RA_SSN_ATTR_NLEN(pPktMap) \
      ((pPktMap)->RASsnAttrObj.RASsnAttr.SsnAttr.u1NameLen)

#define PKT_MAP_RPTE_RA_SSN_ATTR_SNAME(pPktMap) \
      ((pPktMap)->RASsnAttrObj.RASsnAttr.SsnAttr.au1SsnName)

#define PKT_MAP_RPTE_RA_SSN_ATTR_RA_PARAM(pPktMap) \
      ((pPktMap)->RASsnAttrObj.RASsnAttr.RAParam)

#define PKT_MAP_RPTE_RA_SSN_ATTR_EX_ANY_ATTR(pPktMap) \
      ((pPktMap)->RASsnAttrObj.RASsnAttr.RAParam.u4ExAnyAttr)

#define PKT_MAP_RPTE_RA_SSN_ATTR_INC_ANY_ATTR(pPktMap) \
      ((pPktMap)->RASsnAttrObj.RASsnAttr.RAParam.u4IncAnyAttr)

#define PKT_MAP_RPTE_RA_SSN_ATTR_INC_ALL_ATTR(pPktMap) \
      ((pPktMap)->RASsnAttrObj.RASsnAttr.RAParam.u4IncAllAttr)

#define PKT_MAP_MSG_BUF(x) ((x)->pMsgBuf)
#define PKT_MAP_RSVP_PKT_ALLOC(x) ((x)->u1RsvpPktAlloc)
#define PKT_MAP_INC_IF_ID(x) ((x)->IncIfId)
#define PKT_MAP_RX_TTL(x) ((x)->u1RxTtl)
#define PKT_MAP_ENCAPS_TYPE(x) ((x)->u1EncapsType)
#define PKT_MAP_IF_ENTRY(x) ((x)->pIncIfEntry)
#define PKT_MAP_OUT_IF_ENTRY(x) ((x)->pOutIfEntry)
#define PKT_MAP_RSVP_PKT(x) ((x)->pRsvpPkt)
#define PKT_MAP_IP_PKT(x) ((x)->pIpPkt)
#define PKT_MAP_RSVP_HDR(x) ((x)->pRsvpHdr)
#define PKT_MAP_ADSPEC_OBJ(x) ((x)->pAdSpecObj)
#define PKT_MAP_RSVP_HOP_OBJ(x) ((x)->pRsvpHopObj)
#define PKT_MAP_SENDER_TEMPLATE_OBJ(x) ((x)->pSenderTemplateObj)
#define PKT_MAP_SENDER_TSPEC_OBJ(x) ((x)->pSenderTspecObj)
#define PKT_MAP_RESV_CONF_OBJ(x) ((x)->pResvConfObj)
#define PKT_MAP_STYLE_OBJ(x) ((x)->pStyleObj)
#define PKT_MAP_TIME_VALUES_OBJ(x) ((x)->pTimeValuesObj)
#define PKT_MAP_ERROR_SPEC_OBJ(x) ((x)->pErrorSpecObj)
#define PKT_MAP_SCOPE_OBJ(x) ((x)->pScopeObj)
#define PKT_MAP_FLOW_DESCR_LIST(x) ((x)->FlowDescrList)
#define PKT_MAP_SESSION_HASH_INDEX(x) ((x)->u1SessionHashIndex)
#define PKT_MAP_DST_ADDR(x) ((x)->u4DestAddr)
#define PKT_MAP_SRC_ADDR(x) ((x)->u4SrcAddr)
#define PKT_MAP_TOS(x) ((x)->u1Tos)
#define PKT_MAP_TX_TTL(x) ((x)->u1TxTtl)
#define PKT_MAP_PKT_ID(x) ((x)->u2Id)
#define PKT_MAP_FRAGMENT_CONTROL(x) ((x)->u1Df)
#define PKT_MAP_OPT_LEN(x) ((x)->i2Olen)
#define PKT_MAP_RA_OPT_NEEDED(x) ((x)->u1RaOptNeeded)
#define PKT_MAP_FLOW_SPEC_COUNT(x) ((x)->u4FlowSpecCount)
#define PKT_MAP_FILTER_SPEC_COUNT(x) ((x)->u4FilterSpecCount)
#define PKT_MAP_OPT(x) ((x)->pOpt)
#define PKT_MAP_OPT_LENGTH(x) ((x)->i2Olen)
#define PKT_MAP_OUT_IF_ID(x) ((x)->OutIfId)
#define PKT_MAP_BUILD_PKT(x) ((x)->u1BuildPkt)
#define PKT_MAP_UNKNOWN_OBJ_HDR(x) ((x)->pUnknownObjHdr)
#define PKT_MAP_UNKNOWN_OBJ(x) ((x)->u1UnknownObj)
#define PKT_MAP_SEND_TO_BYPASS_TUNNEL(x) ((x)->pRsvpTeTnlInfo->u1SendToByPassTnl)
#define PKT_MAP_IS_BKP_PATH_MSG_TO_CFA(x) ((x)->pRsvpTeTnlInfo->u1IsBkpPathMsgToCfa)
#define PKT_MAP_FRR_BKP_PATH_GW(x) ((x)->pRsvpTeTnlInfo->u4FrrBkpPathGw)

/* Msg Id Object */
#define RPTE_PKT_MAP_MSG_ID_ACK_LIST(pPktMap)     ((pPktMap)->MsgIdAckList)

#define RPTE_PKT_MAP_MSG_ID_NACK_LIST(pPktMap)    ((pPktMap)->MsgIdNackList)

#define RPTE_PKT_MAP_MSG_ID_OBJ(pPktMap)          ((pPktMap)->MsgIdObj)

    #define RPTE_PKT_MAP_MSG_ID_FLAGS(pPktMap) \
            RPTE_MSG_ID_OBJ_FLAGS ((&RPTE_PKT_MAP_MSG_ID_OBJ (pPktMap)))

    #define RPTE_PKT_MAP_MSG_ID_IS_MSG_ID_PRESENT(pPktMap) \
            RPTE_MSG_ID_OBJ_IS_MSG_ID_PRESENT ((&RPTE_PKT_MAP_MSG_ID_OBJ\
                                                (pPktMap)))

    #define RPTE_PKT_MAP_MSG_ID_EPOCH(pPktMap) \
            RPTE_MSG_ID_OBJ_EPOCH ((&RPTE_PKT_MAP_MSG_ID_OBJ (pPktMap)))

    #define RPTE_PKT_MAP_MSG_ID(pPktMap) \
            RPTE_MSG_ID_OBJ_MSG_ID ((&RPTE_PKT_MAP_MSG_ID_OBJ (pPktMap)))

#define RPTE_PKT_MAP_MSG_ID_LIST(pPktMap)         ((pPktMap)->pMsgIdListObj)

#define RPTE_PKT_MAP_NBR_ENTRY(pPktMap)           ((pPktMap)->pNbrEntry)

#define RPTE_PKT_MAP_TNL_INFO(pPktMap)            ((pPktMap)->pRsvpTeTnlInfo)
#define RPTE_PKT_LSP_TNL_IF_OBJ(pPktMap)           ((pPktMap)->pLspTnlIfIdObj)

#define RPTE_PKT_MAP_PKT_PATH(pPktMap)            ((pPktMap)->u1PktPath)

#define PSB_RPTE_SNDR_TMP(x)         ((x)->RsvpTeSndrTmp)
#define PSB_RPTE_FRR_DETOUR(x)       ((x)->RsvpTeDetour)
#define PSB_RPTE_FRR_FAST_REROUTE(x) ((x)->RsvpTeFastReroute)
#define PSB_RPTE_SNDR_TSPEC_OBJ(x)   ((x)->RsvpTeSndrTspecObj)
#define PSB_RPTE_SNDR_TSPEC(x)       ((x)->RsvpTeSndrTspecObj.RsvpTeSndrTspec)
#define PSB_SNDR_TSPEC_OBJ_LEN(x)    ((x)->SenderTspecObj.ObjHdr.u2Length
#define PSB_RPTE_TNL_INFO(x)         ((x)->pRsvpTeTnlInfo)

#define PSB_INDEX(x) ((x)->u4Number)
#define PSB_SENDER_TEMPLATE(x) ((x)->SenderTemplate)
#define PSB_SENDER_TSPEC_OBJ(x) ((x)->SenderTspecObj)
#define PSB_RSVP_HOP(x) ((x)->RsvpHop)
#define PSB_REMAINING_TTL(x) ((x)->u1RemainingTtl)
#define PSB_INC_IF_ID(x) ((x)->IncIfId)
#define PSB_REFRESH_INTERVAL(x) ((x)->u4RefreshInterval)
#define PSB_IN_REFRESH_INTERVAL(x) ((x)->u4InRefreshInterval)
#define PSB_LAST_CHANGE(x) ((x)->u4LastChange)
#define PSB_NON_RSVP_FLAG(x) ((x)->u1NonRsvpFlag)
#define PSB_OUT_IF_ID_LIST_LEN(x) ((x)->u4OutIfListLength)
#define PSB_IN_IF_ID(x) ((x)->InIfId)
#define PSB_OUT_IF_ID(x) ((x)->OutIfId)
#define PSB_TIME_TO_DIE(x) ((x)->u4TimeToDie)
#define PSB_TIMER(x) ((x)->PsbTmr)
#define RPTE_CSPF_TIMER(x) ((x)->CspfTmr)
#define RPTE_CSPF_TIMER_PARAM(x) ((x)->CspfTmrParam)
#define RPTE_EVT_TO_L2VPN_TIMER(x) ((x)->EvtToL2vpnTmr)
#define RPTE_EVT_TO_L2VPN_TIMER_PARAM(x) ((x)->EvtToL2vpnTmrParam)
#define RPTE_GBL_REVERT_TIMER(x) ((x)->GblRevertTmr)
#define RPTE_GBL_REVERT_TIMER_PARAM(x) ((x)->GblRevertTmrParam)

/* Lsp Reoptimization support, Macros for Reoptimize timer and
 * ERO Cache Timer*/

#define RPTE_REOPTIMIZE_TIMER(x)    ((x)->ReoptimizeTmr)
#define RPTE_REOPTIMIZE_TIMER_PARAM(x)  ((x)->ReoptimizeTmrParam)
#define RPTE_ERO_CACHE_TIMER(x)     ((x)->EroCacheTmr)
#define RPTE_ERO_CACHE_TIMER_PARAM(x)    ((x)->EroCacheTmrParam)
#define RPTE_LINK_UP_WAIT_TIMER(x)   ((x)->ReoptLinkUpWaitTmr)
#define RPTE_LINK_UP_WAIT_TIMER_PARAM(x) ((x)->ReoptLinkUpWaitTmrParam)
#define RPTE_REOPTIMIZE_NODE_STATE(x)   ((x)->u1ReoptimizeNodeState)
#define RPTE_REOPTIMIZE_ERR_LINK_ADDR(x) ((x)->u4ErrReoptLinkAddress)
#define RPTE_REOPTIMIZE_ERR_LINK_ID(x)      ((x)->u4ErrReoptLinkId)
#define RPTE_REOPTIMIZE_TIMER_STATE(x)  ((x)->b1IsReoptimizeTimerState)
#define RPTE_REOPT_LINK_MAINTENANCE_ADDR(x) ((x)->u4ReoptLinkMaintainanceAddr)
#define RPTE_REOPT_LINK_MAINTENANCE_ID(x)   ((x)->u4ReoptLinkMaintainanceId)
#define RPTE_REOP_PATH_REEVALUATION_REQ(x) ((x)->b1IsPathRevaluationRequest)
#define RPTE_REOPT_TRIGGER_TYPE(x)          ((x)->u1ReoptTriggerType)

#define PSB_TIMER_PARAM(x) ((x)->PsbTmrParam)
#define PSB_E_POLICE_FLAG(x) ((x)->u1EPoliceFlag)
#define PSB_RX_TTL(x) ((x)->u1RxTtl)
#define PSB_IF_ENTRY(x) ((x)->pIncIfEntry)
#define PSB_OUT_IF_ENTRY(x) ((x)->pOutIfEntry)
#define PSB_FRR_FAC_LOC_REVERT_PROT_TNL_IF(x) ((x)->u4FrrFacLocRevProtTnlIfIndex)
#define PSB_FRR_FAC_LOC_REVERT_BYPASS_TNL_IF(x) ((x)->u4FrrFacLocRevByPassTnlIfIndex)
#define PSB_FRR_FAC_LOC_REVERT_PROT_RSVP_IF(x) ((x)->u4FrrFacLocRevProtRsvpIfIndex)
#define PSB_ADSPEC(x) ((x)->AdSpec)
#define PSB_TIMER_TYPE(pPsb) ((pPsb)->u1TimerType)
#define PSB_LSP_TNL_IF_ID(x) ((x)->LspTnlIfId)

#define RSB_IN_IF_ENTRY(x) ((x)->pIncIfEntry)
#define RSB_OUT_IF_ENTRY(x) ((x)->pOutIfEntry)
#define RSB_STYLE(x) ((x)->Style)
#define RSB_FLOW_SPEC(x) ((x)->FlowSpec)
#define RSB_RESV_CONF(x) ((x)->ResvConf)
#define RSB_TC_TSPEC(x) ((x)->TCTspec)
#define RSB_TIMER(x) ((x)->Timer)
#define RSB_TIMER_PARAM(x) ((x)->TmrParam)
#define RSB_FILTER_SPEC(x) ((x)->RsvpTeFilterSpec)
#define RSB_NUMBER(x) ((x)->u4Number)
#define RSB_LAST_CHANGE(x) ((x)->u4LastChange)
#define RSB_TIME_TO_DIE(x) ((x)->u4TimeToDie)
#define RSB_STATIC(x) ((x)->u1Static)
#define RSB_PROT_STATUS(x) ((x)->u1ProtStatus)
#define RSB_NEXT_RSVP_HOP(x) ((x)->NextRsvpHop)
#define RSB_FWD_RSVP_HOP(x) ((x)->FwdRsvpHop)
#define RSB_RHANDLE(x) ((x)->u4Rhandle)
#define RSB_REFRESH_INTERVAL(x) ((x)->u4RefreshInterval)
#define RSB_IN_REFRESH_INTERVAL(x) ((x)->u4InRefreshInterval)
#define RSB_NON_RSVP_FLAG(x) ((x)->u1NonRsvpFlag)
#define RSB_TIMER_TYPE(x)    ((x)->u1TimerType)
#define RSB_LSP_TNL_IF_ID(x) ((x)->LspTnlIfId)


#define HELLO_TIMER(x) ((x)->HelloTimer)
#define HELLO_TIMER_PARAM(x) ((x)->HelloTimerParam)

#define SOCK_TIMER(x) ((x)->SockPollTmr)
#define SOCK_TIMER_PARAM(x) ((x)->SockTimerParam)

#define TIMER_PARAM_ID(x) ((x)->u1Id)
#define TIMER_PARAM_IF_ENTRY(x) ((x)->u.pIfEntry)
#define TIMER_PARAM_NBR_ENTRY(x) ((x)->u.pNbrEntry)
#define TIMER_PARAM_PSB(x) ((x)->u.pPsb)
#define TIMER_PARAM_RSB(x) ((x)->u.pRsb)
#define TIMER_PARAM_RESV_FWD_CMN_HDR(x) ((x)->u.pResvFwdCmnHdr)
#define TIMER_PARAM_STATUS(x) ((x)->u1Status)

#define TIMER_PARAM_RPTE_TNL(x) ((x)->u.pRsvpTeTnlInfo)
#define TIMER_PARAM_HELLO(x)    ((x)->u.pRsvpTeGblInfo)
#define RPTE_TNL_RRO_TIMER(x)   ((x)->RROTnlTmr)

#define RESV_REFRESH_PARAM_ADDR(x) ((x)->u4PhopAddr)

#define SNMP_RSVP_REQ(x) ((x)->u1ReqId)
#define SNMP_RSVP_PSB(x) ((x)->u.pPsb)
#define SNMP_RSVP_RSB(x) ((x)->u.pRsb)

/* End - rsvpmacs.h */

/* RSVP TE Pool Ids */

#define RSVPTE_AR_HOP_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_AR_HOP_TEMP_SIZING_ID]
#define RSVPTE_PRCS_UDP_DATA_POOL_ID  RSVPTEMemPoolIds[MAX_RSVPTE_PROCESSS_UDP_DATA_BUF_SIZING_ID]
#define RSVPTE_RAW_IP_DATA_BUFFER_POOL_ID RSVPTEMemPoolIds[MAX_RSVPTE_RAW_IP_DATA_BUF_SIZING_ID]
#define RSVPTE_DATA_BUFFER_POOL_ID       RSVPTEMemPoolIds[MAX_RSVPTE_UDP_DATA_BUF_SIZING_ID]
#define RSVPTE_RAW_PKT_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_RAW_PKT_SIZING_ID]
#define RSVPTE_TNL_POOL_ID                      RSVPTEMemPoolIds[MAX_RSVPTE_TUNNEL_SIZING_ID]
#define RSVPTE_FAC_TNL_POOL_ID                      RSVPTEMemPoolIds[MAX_RSVPTE_FACILITY_TUNNEL_SIZING_ID]
#define RSVPTE_ERHOP_POOL_ID        RSVPTEMemPoolIds[MAX_RSVPTE_HOP_INFO_SIZING_ID]
#define RSVPTE_RRHOP_POOL_ID        RSVPTEMemPoolIds[MAX_RSVPTE_AR_HOP_INFO_SIZING_ID] 
#define RSVPTE_NEWSUB_OBJ_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_NEWSUBOBJ_INFO_SIZING_ID] 
#define RSVPTE_IF_ENTRY_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_IF_ENTRY_SIZING_ID]
#define RSVPTE_NBR_ENTRY_POOL_ID    RSVPTEMemPoolIds[MAX_RSVPTE_NBR_ENTRY_SIZING_ID] 
#define RSVPTE_MSG_ID_LIST_POOL_ID    RSVPTEMemPoolIds[MAX_RSVPTE_MSGID_OBJNODE_SIZING_ID]
#define RPTE_IP_RT_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_RT_ENTRY_INFO_SIZING_ID]
#define RPTE_CSPF_COMP_INFO_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_CSPF_COMP_INFO_SIZING_ID]
#define RPTE_CSPF_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_CSPF_MSG_SIZING_ID]
#define RSVPTE_TRIE_INFO_POOL_ID  RSVPTEMemPoolIds[MAX_RSVPTE_TRIE_INFO_SIZING_ID] 
#define RSVPTE_TIMER_BLK_POOL_ID    RSVPTEMemPoolIds[MAX_RSVPTE_TIMER_BLOCK_SIZING_ID] 
#define RSVPTE_PSB_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_PSB_SIZING_ID]
#define RSVPTE_RSB_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_RSB_SIZING_ID]
#define RSVPTE_TRIE_GBL_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_GBL_TRIE_SIZING_ID]
#define RSVPTE_RSVP_PKT_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_RSVP_PKT_SIZING_ID]
#define RSVPTE_IP_PKT_POOL_ID   RSVPTEMemPoolIds[MAX_RSVPTE_IP_PKT_SIZING_ID]
#define RSVPTE_PKT_MAP_POOL_ID  RSVPTEMemPoolIds[MAX_RSVPTE_PKT_MAP_ENTRIES_SIZING_ID]
#define RSVPTE_NOTIFY_RECIPIENT_POOL_ID RSVPTEMemPoolIds[MAX_RSVPTE_NOTIFY_RECIPIENT_SIZING_ID]
#define RSVPTE_NOTIFY_TNL_POOL_ID  RSVPTEMemPoolIds[MAX_RSVPTE_NOTIFY_TUNNELS_SIZING_ID]
#define RSVPTE_REC_NTFY_TNL_POOL_ID  RSVPTEMemPoolIds[MAX_RSVPTE_REC_NTFY_TUNNEL_SIZING_ID]
#define RSVPTE_INTERNAL_EVNT_POOL_ID  RSVPTEMemPoolIds[MAX_RSVPTE_INTERNAL_EVNT_SIZING_ID]
#define RSVPTE_FRRTNL_PORT_MAPPING_POOL_ID RSVPTEMemPoolIds[MAX_RSVPTE_FRRTNL_PORT_MAPPING_SIZING_ID]

/* Config numbers - Max numbers */
#define RSVPTE_MAX_IFACES(x)        ((x)->RsvpTeCfgParams.u2MaxIfaces)
#define RSVPTE_MAX_NEIGHBOURS(x)    ((x)->RsvpTeCfgParams.u2MaxNeighbours)
#define RSVPTE_MAX_TNLS(x)          ((x)->RsvpTeCfgParams.u2MaxRsvpTeTnls)
#define RSVPTE_MAX_LBLSTACK_TNL(x)  ((x)->RsvpTeCfgParams.u2MaxRsvpTeTnls) *\
                                    ((x)->RsvpTeCfgParams.u2MaxLblStckPerTnl)
#define RSVPTE_MAX_ARPERTNL(x)      ((x)->RsvpTeCfgParams.u2MaxTempArHops)
#define RSVPTE_MAX_ERPERTNL(x)      ((x)->RsvpTeCfgParams.u2MaxTempErHops)
#define RSVPTE_MAX_LBLSUBOBJ_TNL(x) ((x)->RsvpTeCfgParams.u2MaxRsvpTeTnls) *\
                                    ((x)->RsvpTeCfgParams.u2MaxLblSubObjPerTnl)
#define RSVPTE_MAX_NEWSUBOBJ_TNL(x)\
        ((x)->RsvpTeCfgParams.u2MaxRsvpTeTnls) *\
        ((x)->RsvpTeCfgParams.u2MaxNewSubObjPerTnl)

#define RSVPTE_NEIGHBOUR_TABLE_SIZE(x)  RSVPTE_MAX_NEIGHBOURS(x)

/* Defintion of global hash tables */
#define RSVPTE_TNL_RSRC_HSH_TBL(x) ((x)->pRsvpTeTnlRsrcTable)
#define RSVPTE_TNL_HSH_TBL(x) ((x)->pRsvpTeTnlTable)

#define RSVPTE_TNL_GBL_TRIE(x)     ((x)->pRsvpteTrieGbl)

/* Definition of RSVP-TE Tunnel Trie table related info */
#define RSVPTE_TNL_TRIE_ROOT(x)      ((x)->pTnlTblTrieRoot)
#define RSVPTE_TNL_TRIE_APPID(x)     ((x)->u4TnlTrieAppid)

#define RSVPTE_COMPUTE_HASH_INDEX(u4Address) \
             ((((u4Address) >> 24) ^ (((u4Address) & 0xFF0000) >> 16) ^ \
               (((u4Address) & 0xFF00) >> 8) ^ \
               ((u4Address) & 0xFF)) % RSVPTE_HASH_SIZE)

/* Memory initialisation using mem_set */
#define INIT_RSVP_TNL_INFO(pRsvpTeTnlInfo) \
        MEMSET(pRsvpTeTnlInfo, 0, sizeof(tRsvpTeTnlInfo))

#define INIT_RSVP_ERHOP_INFO(pRsvpTeErHopInfo) \
        MEMSET(pRsvpTeErHopInfo, 0, sizeof(tRsvpTeErHopInfo))

#define INIT_RSVP_ARHOP_INFO(pRsvpTeArHopInfo) \
        MEMSET(pRsvpTeArHopInfo, 0, sizeof(tRsvpTeArHopInfo))

#define INIT_RSVP_TRFC_PARM_INFO(pRpTeTrfcParms) \
        MEMSET(pRpTeTrfcParms, 0, sizeof(tRpTeTrfcParms))


#define RPTE_TE_TNL(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->pTeTnlInfo)

#define RPTE_MAX_GBL_REVERT_TIME(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->pTeTnlInfo->u4MaxGblRevertTime)

#define RPTE_FRR_OUT_TNL_INFO(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->pOutFrrProtTnlInfo)

#define RPTE_FRR_STACK_BIT(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u1StackTnlBit)

#define RPTE_HLSP_STACK_BIT(pRsvpTeTnlInfo)\
        RPTE_FRR_STACK_BIT(pRsvpTeTnlInfo)

#define RPTE_FRR_IN_TNL_INFO(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->pInFrrTnlInfo)

#define RPTE_FRR_FAC_PROT_TNL_LIST(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->FacFrrList)

#define RPTE_FRR_FAC_PROT_TNL_NODE(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->FacFrrProtTnl)

#define RPTE_FRR_CSPF_INFO(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u1RsvpCspfInfo)

#define RPTE_FRR_CSPF_INTVL_COUNT(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u1CspfIntCnt)

#define RPTE_FRR_NODE_STATE(x) ((x)->u1FrrNodeState)

#define RPTE_FRR_IN_TNL(x) ((x)->pInFrrTnlInfo)

#define RPTE_FRR_BASE_TNL(x) ((x)->pBaseFrrProtTnlInfo)

#define RPTE_FRR_DETOUR_GRP_ID(x) ((x)->u1DetourGrpId)
    
#define RPTE_FRR_TNL_INFO_TYPE(x) ((x)->u1RsvpTnlInfoType)

#define RPTE_FRR_DMP_LEAST_HOPS_TO_REACH_DEST(x) ((x)->u1LeastHopsToReachDest)

#define RPTE_FRR_ONE_TO_ONE_PLR_ID(x) ((x)->u4One2OnePlrId)

#define RPTE_TE_FACILITY_BKP_TNL_NODE(x) ((x)->FacilityRpteBkpTnl)

#define RPTE_FRR_FACILITY_BKP_TNL_INDEX(x) ((x)->u4TunnelIndex)

#define RPTE_FRR_PROT_IF_INDEX(x) ((x)->u4TnlIfIndex)

#define RPTE_FRR_FACILITY_BKP_TNL_INGRESS_LSR_ID(x) ((x)->u4TunnelIngressLSRId)

#define RPTE_FRR_FACILITY_BKP_TNL_EGRESS_LSR_ID(x) ((x)->u4TunnelEgressLSRId)

#define RPTE_FRR_FACILITY_BKP_TNL(x) ((x)->pRsvpTeTnlInfo)

#define RPTE_TE_TNL_FRR_INFO(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->pTeTnlInfo->pTeFrrConstInfo)

#define RPTE_TNL_TRFC_PARMS(pRsvpTeTnlInfo) \
        RSVPTE_TNL_TRFC_PARAM (RPTE_TE_TNL (pRsvpTeTnlInfo))

#define RPTE_TNL_TRFC_PARM_INDEX(pRsvpTeTnlInfo) \
        RSVPTE_TNL_TPARAM_INDEX (RPTE_TE_TNL (pRsvpTeTnlInfo))

/* Macros to access tunnel information */
#define RSVPTE_TNL_TNLINDX(pRsvpTeTnlInfo) \
            RSVPTE_TNL_INDEX (RPTE_TE_TNL (pRsvpTeTnlInfo))

#define RSVPTE_TNL_TNLINST(pRsvpTeTnlInfo) \
            RSVPTE_TNL_PRIMARY_INSTANCE (RPTE_TE_TNL (pRsvpTeTnlInfo))

#define RSVPTE_TNL_TRFC_PARMS_NUM_TNLS_ASSC(pRsvpTeTnlInfo) \
            RPTE_TRFC_PARAM_NUM_TNLS(RPTE_TE_TNL (pRsvpTeTnlInfo))
/* New macros for tunnel ingress and egress id as an index to the tunnel table
 */
#define RSVPTE_TNL_INGRESS(pRsvpTeTnlInfo) \
     RpteUtilConvertToInteger (RSVPTE_TNL_INGRESS_RTR_ID( RPTE_TE_TNL( pRsvpTeTnlInfo)))

#define RSVPTE_TNL_EGRESS(pRsvpTeTnlInfo) \
        RpteUtilConvertToInteger (RSVPTE_TNL_EGRESS_RTR_ID( RPTE_TE_TNL( pRsvpTeTnlInfo)))


#define RSVPTE_TNL_L3PID(pRsvpTeTnlInfo) \
            (pRsvpTeTnlInfo)->u2L3pid

#define RSVPTE_TNL_SETUP_PRIO(pRsvpTeTnlInfo) \
            RSVPTE_TNL_SET_PRIO (RPTE_TE_TNL(pRsvpTeTnlInfo))

#define RSVPTE_INC_ANY_AFFINITY(pRsvpTeTnlInfo) \
            RSVPTE_TNL_INC_ANY_AFFINITY (RPTE_TE_TNL(pRsvpTeTnlInfo))

#define RSVPTE_EXC_ANY_AFFINITY(pRsvpTeTnlInfo) \
            RSVPTE_TNL_EXC_ANY_AFFINITY (RPTE_TE_TNL(pRsvpTeTnlInfo))

#define RSVPTE_INC_ALL_AFFINITY(pRsvpTeTnlInfo) \
            RSVPTE_TNL_INC_ALL_AFFINITY (RPTE_TE_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TNL_HLDNG_PRIO(pRsvpTeTnlInfo) \
            RSVPTE_TNL_HOLD_PRIO (RPTE_TE_TNL(pRsvpTeTnlInfo))

#define RSVPTE_SRLG_LIST(pRsvpTeTnlInfo) \
            RSVPTE_TNL_SRLG_LIST (RPTE_TE_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TNL_NODE_RELATION(pRsvpTeTnlInfo) \
            RSVPTE_TNL_ROLE (RPTE_TE_TNL(pRsvpTeTnlInfo))

#define RPTE_TNL_OWNER(pRsvpTeTnlInfo) \
            RSVPTE_TNL_OWNER (RPTE_TE_TNL(pRsvpTeTnlInfo))

#define RPTE_TNL_ISIF(pRsvpTeTnlInfo) \
            RSVPTE_TNL_ISIF (RPTE_TE_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TNL_INLBL(pRsvpTeTnlInfo) \
            (pRsvpTeTnlInfo)->DownstrInLbl

#define RSVPTE_TNL_OUTLBL(pRsvpTeTnlInfo) \
            (pRsvpTeTnlInfo)->DownstrOutLbl

#define RSVPTE_TNL_RRO_ERR_FLAG(pRsvpTeTnlInfo) \
            (pRsvpTeTnlInfo)->u1RROErrFlag

#define RSVPTE_TNL_CFG_STATUS(pRsvpTeTnlInfo) \
            (pRsvpTeTnlInfo)->u1Status

#define RSVPTE_TNL_EXTN_TNLID(pRsvpTeTnlInfo) \
            ((pRsvpTeTnlInfo)->ExtnTnlId)

#define RSVPTE_TNL_INERHOP_LIST_INFO(x) \
            ((x)->pInTePathListInfo)

#define RSVPTE_TNL_IN_PATH_OPTION(x) \
            ((x)->pTePathInfo)

#define RSVPTE_TNL_GBL_REVERT_FLAG(x) \
            ((x)->u1GblRevertFlag)

#define RSVPTE_TNL_OUTERHOP_LIST_INFO(pRsvpTeTnlInfo) \
            RSVPTE_TNL_PATH_LIST_INFO ( RPTE_TE_TNL (pRsvpTeTnlInfo)) 

#define RSVPTE_TNL_PATH_OPTION(pRsvpTeTnlInfo) \
            RSVPTE_TNL_PATH_INFO ( RPTE_TE_TNL (pRsvpTeTnlInfo)) 

#define RSVPTE_TNL_FIRST_PATH_OPTION(x) \
            (tTePathInfo *)(TMO_SLL_First (&((x)->PathOptionList))) 

#define RSVPTE_TNL_OUTERHOP_LIST(pRsvpTeTnlInfo) \
            RSVPTE_TNL_PATH_LIST (RPTE_TE_TNL (pRsvpTeTnlInfo)) 

#define RSVPTE_TNL_INARHOP_LIST(pRsvpTeTnlInfo) \
            RSVPTE_IN_ARHOP_LIST (RPTE_TE_TNL (pRsvpTeTnlInfo)) 

#define RSVPTE_TNL_OUTARHOP_LIST(pRsvpTeTnlInfo) \
            RSVPTE_OUT_ARHOP_LIST (RPTE_TE_TNL (pRsvpTeTnlInfo)) 

#define RSVPTE_OUT_ARHOP_LIST_INFO(pRsvpTeTnlInfo) \
            RSVPTE_OUT_AR_HOP_LIST_INFO (RPTE_TE_TNL (pRsvpTeTnlInfo))

#define RSVPTE_IN_ARHOP_LIST_INFO(pRsvpTeTnlInfo) \
            RSVPTE_IN_AR_HOP_LIST_INFO (RPTE_TE_TNL (pRsvpTeTnlInfo))

#define RSVPTE_TNL_RRO_FLAGS(pRsvpTeTnlInfo) \
            pRsvpTeTnlInfo->u1RroFlag

#define RSVPTE_TNL_LBL_REQ_OBJ(pRsvpTeTnlInfo) \
            ((pRsvpTeTnlInfo)->GenLblReq)

#define RSVPTE_TNL_LBL_REQ_ATM_RNG(pRsvpTeTnlInfo) \
            pRsvpTeTnlInfo->AtmLblRngEntry

#define RSVPTE_TNL_PSB(pRsvpTeTnlInfo) \
            (pRsvpTeTnlInfo)->pPsb

#define RSVPTE_TNL_RSB(pRsvpTeTnlInfo) \
            pRsvpTeTnlInfo->pRsb

#define RSVPTE_TNL_DN_NHOP(pRsvpTeTnlInfo) \
            pRsvpTeTnlInfo->u4DnNextHop

#define RPTE_TNL_IN_PATH_MSG_ID(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u4InPathMsgId)

#define RPTE_TNL_OUT_PATH_MSG_ID(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u4OutPathMsgId)

#define RPTE_TNL_IN_RESV_MSG_ID(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u4InResvMsgId)

#define RPTE_TNL_OUT_RESV_MSG_ID(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u4OutResvMsgId)

#define RPTE_TNL_USTR_MAX_MSG_ID(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u4UstrMaxMsgId)

#define RPTE_TNL_DSTR_MAX_MSG_ID(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u4DstrMaxMsgId)

#define RPTE_TNL_MSG_ID_FLAG(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u1MsgIdFlag)

#define RPTE_TNL_DEL_TNL_FLAG(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->u1DelTnlFlag)

#define RPTE_TNL_USTR_NBR(pRsvpTeTnlInfo)     (pRsvpTeTnlInfo->pUStrNbr)

#define RPTE_TNL_DSTR_NBR(pRsvpTeTnlInfo)     (pRsvpTeTnlInfo->pDStrNbr)

#define RPTE_TNL_PE_TIMER_BLOCK(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->pPETimerBlock)

#define RPTE_TNL_RE_TIMER_BLOCK(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->pRETimerBlock)

#define RPTE_TNL_PT_TIMER_BLOCK(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->pPTTimerBlock)

#define RPTE_TNL_RT_TIMER_BLOCK(pRsvpTeTnlInfo) \
        (pRsvpTeTnlInfo->pRTTimerBlock)

#define RPTE_TNL_PSB_TIMER_TYPE(pRsvpTeTnlInfo) \
            pRsvpTeTnlInfo->pPsb->u1TimerType

#define RPTE_TNL_PSB_REFRESH_INTERVAL(pRsvpTeTnlInfo) \
             pRsvpTeTnlInfo->pPsb->u4RefreshInterval

#define RPTE_TNL_RSB_TIMER_TYPE(pRsvpTeTnlInfo) \
            pRsvpTeTnlInfo->pRsb->u1TimerType

#define RPTE_TNL_RSB_REFRESH_INTERVAL(pRsvpTeTnlInfo) \
             pRsvpTeTnlInfo->pRsb->u4RefreshInterval

#define RPTE_TNL_PSB_STATUS(pRsvpTeTnlInfo) \
           pRsvpTeTnlInfo->pPsb->u1Status

#define RPTE_TNL_PSB_INDEX(pRsvpTeTnlInfo) \
           pRsvpTeTnlInfo->pPsb->u4Number

#define RPTE_TNL_RESV_CONF_ADDR(pRsvpTeTnlInfo) \
           pRsvpTeTnlInfo->pRsb->ResvConf.u4RecevAddr

/* Macros to access tunnel resource information */

#define RSVPTE_TNLRSRC_TKN_BKT_RATE(pRsvpTeTnlInfo) \
           RPTE_TPARAM_TBR (RSVPTE_TNL_TRFC_PARAM (RPTE_TE_TNL \
                                                   (pRsvpTeTnlInfo)))

#define RSVPTE_TNLRSRC_TKN_BKT_SIZE(pRsvpTeTnlInfo) \
           RPTE_TPARAM_TBS (RSVPTE_TNL_TRFC_PARAM (RPTE_TE_TNL \
                                                   (pRsvpTeTnlInfo)))

#define RSVPTE_TNLRSRC_PEAK_DATA_RATE(pRsvpTeTnlInfo) \
    (RpteGetAttrFromTnlOfAttrList(pRsvpTeTnlInfo->pTeTnlInfo, \
                                         TE_ATTR_BANDWIDTH_BITMASK))


#define RSVPTE_TNLRSRC_MIN_POLICED_UNIT(pRsvpTeTnlInfo) \
           RPTE_TPARAM_MPU (RSVPTE_TNL_TRFC_PARAM (RPTE_TE_TNL \
                                                   (pRsvpTeTnlInfo)))

#define RSVPTE_TNLRSRC_MAX_PKT_SIZE(pRsvpTeTnlInfo) \
           RPTE_TPARAM_MPS (RSVPTE_TNL_TRFC_PARAM (RPTE_TE_TNL \
                                                   (pRsvpTeTnlInfo)))

/* Macros to access OUT AR HOP related information */

#define RSVPTE_ARHOP_ADDRTYPE(pRsvpTeArHopInfo) \
        RSVPTE_TE_ARHOP_ADDR_TYPE(pRsvpTeArHopInfo)

#define RSVPTE_ARHOP_IPV4ADDR(pRsvpTeArHopInfo) \
        RSVPTE_TE_ARHOP_IP_ADDR(pRsvpTeArHopInfo)

#define RSVPTE_ARHOP_IPV4_PRFXLEN(pRsvpTeArHopInfo) \
        RSVPTE_TE_ARHOP_ADDR_PRFX_LEN(pRsvpTeArHopInfo)

#define RSVPTE_ARHOP_FLAGS(pRsvpTeArHopInfo) \
        RSVPTE_TE_ARHOP_FLAG(pRsvpTeArHopInfo)

/* Macros to access fields of the Global Info structure */
#define RSVPTE_LSR_ID(pRsvpTeGblInfo) \
            (&(pRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId))

#define RSVPTE_MAX_RSVPTETNL(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->RsvpTeCfgParams.u2MaxRsvpTeTnls

#define RSVPTE_RROBJ_SPRTD(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->RsvpTeCfgParams.u1RRObjSprtd

#define RSVPTE_HELLO_SPRTD(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->RsvpTeCfgParams.u1HelloSprtd

#define RSVPTE_SOCK_SPRTD(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->RsvpTeCfgParams.u1SockSupprt

#define RSVPTE_QOS_SPRTD(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->RsvpTeCfgParams.u1QoSSupprt
            
#define RSVPTE_MAX_NEWSUBOBJ_PERTNL(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->RsvpTeCfgParams.u2MaxNewSubObjPerTnl

#define RSVPTE_GBL_INFO_TRIE_APPID(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->u4TnlTrieAppid

#define RSVPTE_GENLBL_MINLBL(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->u4GenLblSpaceMinLbl

#define RSVPTE_GENLBL_MAXLBL(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->u4GenLblSpaceMaxLbl

#define RSVPTE_GENLBL_GRPID(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->u2GenbLblSpaceGrpId

#define RSVPTE_ADMIN_STATUS(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->u1RsvpTeAdminStatus

#define RSVPTE_DUMP_LVL(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->u4RsvpTeDumpLvl

#define RSVPTE_DUMP_DIR(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->u4RsvpTeDumpDirn

#define RSVPTE_DUMP_TYPE(pRsvpTeGblInfo) \
            pRsvpTeGblInfo->u4RsvpTeDumpType

#define RSVPTE_HELLO_INTVL_TIME(gpRsvpTeGblInfo) \
                              gpRsvpTeGblInfo->RsvpTeCfgParams.u4HellointrvlTime

#define RSVPTE_RR_CAPABLE(gpRsvpTeGblInfo) \
                              gpRsvpTeGblInfo->RsvpTeCfgParams.u4RRCapable

#define RSVPTE_MSGID_CAPABLE(gpRsvpTeGblInfo) \
                              gpRsvpTeGblInfo->RsvpTeCfgParams.u4MsgIdCapable

#define RSVPTE_RMD_POLICY_OBJECT(gpRsvpTeGblInfo) \
                              gpRsvpTeGblInfo->RsvpTeCfgParams.u1RmdPolicyObject
#define RSVPTE_MPLS_LSR_ID(gpRsvpTeGblInfo) \
                                  gpRsvpTeGblInfo->RsvpTeCfgParams.MplsTeLsrId

#define RSVPTE_BUNDLE_SIZE(gpRsvpTeGblInfo) \
                              gpRsvpTeGblInfo->RsvpTeCfgParams.u1BundleSize

#define RSVPTE_BNDL_TNL_THRESHOLD(gpRsvpTeGblInfo) \
                              gpRsvpTeGblInfo->RsvpTeCfgParams.u1BundleTnlThresh

#define RSVPTE_REOPTIMIZE_INTERVAL(gpRsvpTeGblInfo) \
   gpRsvpTeGblInfo->RsvpTeCfgParams.u4ReoptimizeTime

#define RSVPTE_ERO_CACHE_INTERVAL(gpRsvpTeGblInfo) \
   gpRsvpTeGblInfo->RsvpTeCfgParams.u4EroCacheTime

#define RSVPTE_MIN_TNLS_WITH_MSG_ID(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->RsvpTeCfgParams.u4MinTnlsWithMsgId

#define RSVPTE_FRR_DETOUR_INCOMING_NUM(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4DetourIncomingNum

#define RSVPTE_FRR_DETOUR_OUTGOING_NUM(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4DetourOutgoingNum

#define RSVPTE_FRR_DETOUR_ORIG_NUM(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4DetourOrigNum

#define RSVPTE_FRR_SWITCH_OVER_NUM(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4FrrSwitchOverNum

#define RSVPTE_FRR_CONF_IF_NUM(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4FrrConfIfNum

#define RSVPTE_FRR_ACT_PROT_IF_NUM(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4FrrActProtIfNum

#define RSVPTE_FRR_CONF_PROT_TUN_NUM(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4FrrConfProtTunNum

#define RSVPTE_FRR_ACT_PROT_TUN_NUM(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4FrrActProtTunNum

#define RSVPTE_FRR_ACT_PROT_LSP_NUM(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4FrrActProtLspNum

#define RSVPTE_FRR_CSPF_RETRY_INTERVAL(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4FrrCspfRetryInterval

#define RSVPTE_FRR_CSPF_RETRY_COUNT(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4FrrCspfRetryCount

#define RSVPTE_FRR_REVERTIVE_MODE(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u1RevertiveMode

#define RSVPTE_FRR_DETOUR_MERGING_CAPABLE(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u1DetourMergingCapable

#define RSVPTE_FRR_DETOUR_CAPABLE(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u1DetourCapable

#define RSVPTE_FRR_NOTIFS_ENABLED(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u1FrrNotifsEnabled

#define RSVPTE_FRR_MAKE_AFTER_BREAK_ENABLED(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->RsvpTeCfgParams.u1MakeAfterBreakEnabled

#define RSVPTE_FRR_GBL_DETOUR_GRP_ID(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u1DetourGrpId

#define RSVPTE_FRR_GBL_TNL_INSTANCE(gpRsvpTeGblInfo) \
            gpRsvpTeGblInfo->u4TnlInstance

#define RSVPTE_MAX_32BIT_TRIE_SIZE(gpRsvpTeGblInfo) \
            (RPTE_FOUR * RSVPTE_MIN_TNLS_WITH_MSG_ID(gpRsvpTeGblInfo))

#define RSVPTE_MAX_64BIT_TRIE_SIZE(gpRsvpTeGblInfo) \
            (RPTE_TWO * RSVPTE_MIN_TNLS_WITH_MSG_ID(gpRsvpTeGblInfo))

/* Macros to access Interface Entry related information */
#define RPTE_IF_LBL_SPACE(pIfEntry) \
            pIfEntry->RpteIfInfo.u1LblSpace

#define RPTE_IF_LBL_TYPE(pIfEntry) \
            pIfEntry->RpteIfInfo.u1LblType

#define RPTE_IF_ATM_MERGE_CAP(pIfEntry) \
            pIfEntry->RpteIfInfo.u1VcMerge

#define RPTE_IF_ATM_VC_DIR(pIfEntry) \
            pIfEntry->RpteIfInfo.u1VcDir

#define RPTE_IF_LBL_FLAGS(pIfEntry) \
            pIfEntry->RpteIfInfo.u2Flags

#define RPTE_IF_MINVPI(pIfEntry) \
            pIfEntry->RpteIfInfo.u2MinVpi

#define RPTE_IF_MINVCI(pIfEntry) \
            pIfEntry->RpteIfInfo.u2MinVci

#define RPTE_IF_MAXVPI(pIfEntry) \
            pIfEntry->RpteIfInfo.u2MaxVpi

#define RPTE_IF_MAXVCI(pIfEntry) \
            pIfEntry->RpteIfInfo.u2MaxVci

#define RPTE_IF_MAX_MTU(pIfEntry) \
            pIfEntry->RpteIfInfo.u4MaxMTUSize

#define RPTE_IF_LBLSPCID(pIfEntry) \
            pIfEntry->RpteIfInfo.u2LblSpaceId

#define RPTE_IF_NUM_TNL_ASSOC(pIfEntry) \
            pIfEntry->RpteIfInfo.u2NumTnlAssoc

#define RPTE_IF_LINK_ATTR_ASSOC(pIfEntry) \
            pIfEntry->RpteIfInfo.u4LinkAttr

/* General macros */
#define RSVPTE_INC(x,y) x = x + y
#define RSVPTE_DEC(x,y) x = x - y

/* Following are the two macros that are made use for copying to and from
 * tSNMP_OCTET_STRING_TYPE */

/* Macros to access Session object */
#define RPTE_SSN_OBJ(x)               ((x)->RsvpTeSession)
#define RPTE_SSN_TNL_DEST_ADDR(x)     ((x)->TnlDestAddr)
#define RPTE_SSN_TNL_ID(x)            ((x)->u2TnlId)
#define RPTE_SSN_EXTN_TNL_ID(x)       ((x)->ExtnTnlId)

/* Macros to access Filter Spec object */
#define RPTE_FILTER_SPEC_OBJ_HDR(x)  ((x)->ObjHdr)
#define RPTE_FILTER_SPEC_OBJ(x)      ((x)->RsvpTeFilterSpec)
#define RPTE_FILTER_TNL_SNDR_ADDR(x) ((x)->TnlSndrAddr)
#define RPTE_FILTER_TNL_LSPID(x)     ((x)->u2LspId)

#define RPTE_ADNL_FILTER_SPEC_OBJ_HDR(x)  ((x)->ObjHdr)
#define RPTE_ADNL_FILTER_SPEC_OBJ(x)      ((x)->RsvpTeFilterSpec)
#define RPTE_ADNL_FILTER_TNL_SNDR_ADDR(x) ((x)->TnlSndrAddr)
#define RPTE_ADNL_FILTER_TNL_LSPID(x)     ((x)->u2LspId)

/* Macros to access Sender Template object */
#define RPTE_SNDR_TEMP_OBJ_HDR(x)  ((x)->ObjHdr)
#define RPTE_SNDR_TEMP_OBJ(x)      ((x)->RsvpTeSndrTmp)
#define RPTE_SNDR_TNL_SNDR_ADDR(x) ((x)->TnlSndrAddr)
#define RPTE_SNDR_TNL_LSPID(x)     ((x)->u2LspId)

/* Macros to access  COS of RSVPTE*/
#define RPTE_SNDR_TSPEC_OBJ_HDR(x)   ((x)->ObjHdr)
#define RPTE_SNDR_TSPEC_OBJ(x)       ((x)->RsvpTeSndrTspec)
#define RPTE_MAX_PKT_SIZE(x)         ((x)->u2MaxPktSize)
#define RPTE_COS(x)                  ((x)->u1CoS)

/* Macros to access Label object */
#define RPTE_LBL_OBJ_HDR(x)      ((x)->ObjHdr)
#define RPTE_LBL_OBJ(x)          ((x)->Label)
#define RPTE_LBL_OBJ_GEN_LBL(x)  ((x)->Label.u4GenLbl)
#define RPTE_LBL_OBJ_ATM_LBL_VCI(x)  ((x)->Label.AtmLbl.u2Vci)
#define RPTE_LBL_OBJ_ATM_LBL_VPI(x)  ((x)->Label.AtmLbl.u2Vpi)

/* Macros to access Label Request ATM Range object */
#define RPTE_LBL_REQ_ATM_RNG_OBJ_HDR(x)   ((x)->ObjHdr)
#define RPTE_LBL_REQ_ATM_LBL_RNG(x)       ((x)->AtmLblRngEntry)
#define RPTE_LBL_REQ_ATM_LBL_LBVPI(x)     ((x)->AtmLblRngEntry.u2LBVpi)
#define RPTE_LBL_REQ_ATM_LBL_LBVCI(x)     ((x)->AtmLblRngEntry.u2LBVci)
#define RPTE_LBL_REQ_ATM_LBL_UBVPI(x)     ((x)->AtmLblRngEntry.u2UBVpi)
#define RPTE_LBL_REQ_ATM_LBL_UBVCI(x)     ((x)->AtmLblRngEntry.u2UBVci)

/* Macros to access Session Attribute object */
#define RPTE_SSNATTR_OBJ_HDR(x)     ((x)->ObjHdr)
#define RPTE_SSNATTR_OBJ(x)         ((x)->SsnAttr)
#define RPTE_SSNATTR_SET_PRIO(x)    ((x)->u1SetPrio)
#define RPTE_SSNATTR_HOLD_PRIO(x)   ((x)->u1HoldPrio)
#define RPTE_SSNATTR_FLAGS(x)       ((x)->u1Flags)
#define RPTE_SSNATTR_NAME_LEN(x)    ((x)->u1NameLen)
#define RPTE_SSNATTR_SSN_NAME(x)    ((x)->au1SsnName)


/* Macros to access Fast ReRoute Object */
#define RPTE_FAST_REROUTE_OBJ_HDR(x)           ((x)->ObjHdr)
#define RPTE_FAST_REROUTE_OBJ(x)               ((x)->RsvpTeFastReroute)
#define RPTE_FAST_REROUTE_SETUP_PRIORITY(x)    ((x)->u1SetPrio)
#define RPTE_FAST_REROUTE_HOLD_PRIORITY(x)     ((x)->u1HoldPrio)
#define RPTE_FAST_REROUTE_HOP_LIMIT(x)         ((x)->u1HopLmt)
#define RPTE_FAST_REROUTE_FLAGS(x)             ((x)->u1Flags)
#define RPTE_FAST_REROUTE_BANDWIDTH(x)         ((x)->fBandwth)
#define RPTE_FAST_REROUTE_EXANY_ATTR(x)        ((x)->RAParam.u4ExAnyAttr)
#define RPTE_FAST_REROUTE_INANY_ATTR(x)        ((x)->RAParam.u4IncAnyAttr)
#define RPTE_FAST_REROUTE_INALL_ATTR(x)        ((x)->RAParam.u4IncAllAttr)

            
#define RPTE_PROC_EVT_FUNC     gaRpteProcEvtFunc


#define VALIDATE_SUBOBJ_LEN(pObjHdr) \
               (((*((UINT1 *)pObjHdr + 1) ) < 4) \
               || ((*((UINT1 *)pObjHdr + 1) % 4 ) != 0) )

#define RPTE_GET_1_BYTE(x,y)  (y)=(*(UINT1 *)(x)); (x)+=1;
#define RPTE_GET_2_BYTES(x,y)\
{\
    MEMCPY(&y,x,sizeof(UINT2));\
    (x)+= 2;\
}
#define RPTE_GET_4_BYTES(x,y)\
{\
    MEMCPY(&y,x,sizeof(UINT4));\
    (x)+= 4;\
}

#define RPTE_EXT_1_BYTE(x,y)  (y)=(*(UINT1 *)(x)); (x)+=1;
#define RPTE_EXT_2_BYTES(x,y)\
{\
    UINT2 u2TmpVal;\
    MEMCPY(&u2TmpVal,x,sizeof(UINT2));\
    y = OSIX_NTOHS(u2TmpVal);\
    (x)+= 2;\
}
#define RPTE_EXT_4_BYTES(x,y)\
{\
    UINT4 u4TmpVal;\
    MEMCPY(&u4TmpVal,x,sizeof(UINT4));\
    y = OSIX_NTOHL(u4TmpVal);\
    (x)+= 4;\
}

#define RPTE_SNDR_TSPEC_OBJ_LEN(pRsvpTeTnlInfo) \
     pRsvpTeTnlInfo->pPsb->RsvpTeSndrTspecObj.ObjHdr.u2Length

#define RPTE_PUT_1_BYTE(x, y)   (*(UINT1 *)(x)) = (y); x += 1;
#define RPTE_PUT_2_BYTES(x, y)\
{\
    UINT2 u2TmpVal;\
    u2TmpVal = y;\
    MEMCPY(x, &u2TmpVal, sizeof(UINT2));\
    x +=2;\
}
#define RPTE_PUT_4_BYTES(x, y)\
{\
    UINT4 u4TmpVal;\
    u4TmpVal = y;\
    MEMCPY(x, &u4TmpVal, sizeof(UINT4));\
    x +=4;\
}

/* Following macro converts kilo bits per second 
 * to bytes per second */
#define RPTE_CONVERT_KBPS_TO_BYTES_PER_SEC(x)\
{\
   x = (x*1000)/8;\
}

/* Following macro converts bytes per second to 
 * kilo bits per second */
#define RPTE_CONVERT_BYTES_PER_SEC_TO_KBPS(x)\
{\
   x = (x/1000)*8;\
}

/* Resv msg processing related macros */

#define RPTE_TNL_TCSB_CMN_HDR(x)             ((x)->pTcsb->pTcsbCmnHdr)
#define RPTE_TNL_RSB_CMN_HDR(x)              ((x)->pRsb->pRsbCmnHdr)
#define RPTE_TNL_RESV_FWD_CMN_HDR(x)         ((x)->pResvFwd->pResvFwdCmnHdr)

#define RSB_RPTE_TNL_INFO(x)                 ((x)->pRsvpTeTnlInfo)
#define RESV_FWD_RPTE_TNL_INFO(x)            ((x)->pRsvpTeTnlInfo)
#define TCSB_RPTE_TNL_INFO(x)                ((x)->pRsvpTeTnlInfo)

#define RSB_RPTE_FILTER_SPEC(x)              ((x)->RsvpTeFilterSpec)
#define TCSB_RPTE_FILTER_SPEC(x)             ((x)->RsvpTeFilterSpec)
#define RESV_FWD_RPTE_FILTER_SPEC(x)         ((x)->RsvpTeFilterSpec)

#define RSB_CMN_HDR_RPTE_FLOW_SPEC(x)        ((x)->RsvpTeFlowSpec)
#define RESV_FWD_CMN_HDR_RPTE_FLOW_SPEC(x)   ((x)->RsvpTeFlowSpec)
#define TCSB_CMN_HDR_TC_RPTE_FLOW_SPEC(x)    ((x)->TcRpteFlowSpec)
#define TCSB_CMN_HDR_TC_RPTE_TSPEC(x)        ((x)->TcRpteTspec)

#define RPTE_FLOW_SPEC_OBJ(x)                ((x)->RsvpTeFlowSpec)

#define RPTE_TNL_STYLE(x) ((x)->pRsb->Style)

#define RPTE_IS_REROUTE_TNL(x) ((x)->u1ReRouteTnl)

#define RPTE_IS_FRR_CAPABLE(x) ((x)->u1FrrCapable)

#define RPTE_GET_MASK_FROM_PRFX_LEN(u1Len, u4Mask) \
{ \
        u4Mask = 0xffffffff; \
        u4Mask = u4Mask << (32 - u1Len); \
}

/* macros for Pre-emption support */
/* Pointer to the single link list node */

#define RSVP_GET_TNL_LIST_HOLD_DN_STR_NODE(pRsvpTeTnlInfo) \
                  ((tTMO_SLL_NODE *)&((pRsvpTeTnlInfo)->NextHoldPrioDnStrTnl))

#define RSVP_GET_TNL_LIST_HOLD_UP_STR_NODE(pRsvpTeTnlInfo) \
                      ((tTMO_SLL_NODE *)&((pRsvpTeTnlInfo)->NextHoldPrioUpStrTnl))

#define RSVP_GET_TNL_NEIGHBOUR_HOLD_NODE(pRsvpTeTnlInfo) \
                  ((tTMO_SLL_NODE *)&((pRsvpTeTnlInfo)->NbrEntryNextTnl))


/*  The two macros type cast the single list link node 'NextHoldPrio' to 
 *  tunnel info structure. As the first field of the structure is single 
 *  'HashLinkNode' and  the 'NextHoldPrio' is the second field,the base 
 *  address ofthe tunnel info structure can be reached by reducing it by 
 *  the size of 'HashLinkNode' field. 
 */

#define RPTE_OFFSET_DN_STR_HTBL(PrioSllNode) \
    (tRsvpTeTnlInfo *)(VOID *)((UINT1 *)(PrioSllNode) - \
                               MPLS_OFFSET(tRsvpTeTnlInfo,NextHoldPrioDnStrTnl))

#define RPTE_OFFSET_UP_STR_HTBL(PrioSllNode) \
    (tRsvpTeTnlInfo *)(VOID *)((UINT1 *)(PrioSllNode) - \
                               MPLS_OFFSET(tRsvpTeTnlInfo,NextHoldPrioUpStrTnl))


/* Gives the offset in the structure */
#define RPTE_OFFSET(x,y)  ((FS_ULONG)(&(((x *)0)->y)))
/* Gives the size of a particular member in the structure */
#define RPTE_MEMBER_LENGTH(x,y)  (sizeof(((x *)0)->y))

/* Macros for finding the existance of Tunnel */

#define RPTE_CHECK_PATH_MSG_TNL(pPktMap, pRsvpTeTnlInfo) \
          (RpteCheckTnlInTnlTable(gpRsvpTeGblInfo,\
                      OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID(pPktMap)),\
                      OSIX_NTOHS (PKT_MAP_SNDR_TMP_OBJ_LSP_ID(pPktMap)),\
                      OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap))),\
                      OSIX_NTOHL (RpteUtilConvertToInteger(PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap))),\
                      &pRsvpTeTnlInfo))

#define RPTE_CHECK_PATH_ERR_MSG_TNL(pPktMap, pRsvpTeTnlInfo) \
         (RpteCheckTnlInTnlTable(gpRsvpTeGblInfo,\
                      OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID(pPktMap)),\
                      OSIX_NTOHS (PKT_MAP_SNDR_TMP_OBJ_LSP_ID(pPktMap)),\
                      OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap))),\
                      OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap))),\
                      &pRsvpTeTnlInfo))

#define RPTE_CHECK_PATH_TEAR_MSG_TNL(pPktMap, pRsvpTeTnlInfo) \
        (RpteCheckTnlInTnlTable(gpRsvpTeGblInfo,\
                      OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID(pPktMap)),\
                      OSIX_NTOHS (PKT_MAP_SNDR_TMP_OBJ_LSP_ID(pPktMap)),\
                      OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap))),\
                      OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap))),\
                      &pRsvpTeTnlInfo))

#define RPTE_CHECK_TNL_PATH_MSG(pPktMap, pCtTnlInfo) \
        (RpteCheckTnlInTnlTable(gpRsvpTeGblInfo,\
                      OSIX_NTOHS (PKT_MAP_SSN_OBJ_TNL_ID(pPktMap)),\
                      OSIX_NTOHS (PKT_MAP_SNDR_TMP_OBJ_LSP_ID(pPktMap)),\
                      OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap))),\
                      OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap))),\
                      &pCtTnlInfo))

#define RPTE_CHECK_ADNL_TNL_PATH_MSG(pPktMap, pAdnlTnlInfo) \
        (RpteCheckAdnlTnlInTnlTable (gpRsvpTeGblInfo,\
                      OSIX_NTOHS (PKT_MAP_RPTE_SSN_OBJ_TNL_ID (pPktMap)),\
                      OSIX_NTOHS (PKT_MAP_RPTE_SNDR_TMP_OBJ_LSP_ID(pPktMap)),\
                 OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SNDR_TMP_OBJ_SNDR_ADDR \
                                          (pPktMap))),\
                 OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_DEST_ADDR \
                                          (pPktMap))),\
                      &pAdnlTnlInfo))

#define RPTE_CHECK_TNL_RESV_MSG(pPktMap, pCtTnlInfo) \
          (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,\
                 OSIX_NTOHS (PKT_MAP_RPTE_SSN_OBJ_TNL_ID (pPktMap)),\
                 OSIX_NTOHS (PKT_MAP_RPTE_FILTER_SPEC_OBJ_LSP_ID (pPktMap)),\
                 OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_EXTN_TNL_ID \
                                          (pPktMap))),\
                 OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_DEST_ADDR \
                                          (pPktMap))),\
                 &pCtTnlInfo))

#define RPTE_CHECK_TNL_RESV_ERR_MSG(pPktMap, pCtTnlInfo) \
           RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,\
               OSIX_NTOHS (PKT_MAP_RPTE_SSN_OBJ_TNL_ID (pPktMap)),\
               OSIX_NTOHS (PKT_MAP_RPTE_FILTER_SPEC_OBJ_LSP_ID (pPktMap)),\
               OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap))),\
               OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap))),\
               &pCtTnlInfo)

#define RPTE_CHECK_TNL_RESV_TEAR_MSG(pPktMap, pCtTnlInfo) \
          (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,\
               OSIX_NTOHS (PKT_MAP_RPTE_SSN_OBJ_TNL_ID (pPktMap)),\
               OSIX_NTOHS (PKT_MAP_RPTE_FILTER_SPEC_OBJ_LSP_ID (pPktMap)),\
               OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap))),\
               OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap))),\
               &pCtTnlInfo))


#define RPTE_CHECK_TNL_RESV_CONF_MSG(pPktMap, pCtTnlInfo) \
          (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,\
               OSIX_NTOHS (PKT_MAP_RPTE_SSN_OBJ_TNL_ID (pPktMap)),\
               OSIX_NTOHS (PKT_MAP_RPTE_FILTER_SPEC_OBJ_LSP_ID (pPktMap)),\
               OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_EXTN_TNL_ID (pPktMap))),\
               OSIX_NTOHL (RpteUtilConvertToInteger (PKT_MAP_RPTE_SSN_DEST_ADDR (pPktMap))),\
               &pCtTnlInfo))

#define RPTE_CHECK_NOTIFY_MSG_TNL(u4TunnelIndex, u4TunnelInstance, u4TunnelIngress, \
                                   u4TunnelEgress, pCtTnlInfo) \
           (RpteCheckTnlInTnlTable (gpRsvpTeGblInfo,u4TunnelIndex,u4TunnelInstance,\
                                       u4TunnelIngress,u4TunnelEgress,&pCtTnlInfo))

#define RPTE_TNL_REC_ROUTE_SET  \
    ((RSVPTE_TNL_SSN_ATTR (RPTE_TE_TNL (pRsvpTeTnlInfo))) & \
                                RSVPTE_TE_SSN_REC_ROUTE_BIT) 

#define RPTE_IN_ARHOP_LIST_SIZE  \
     (((TMO_SLL_Count (&RSVPTE_TNL_INARHOP_LIST  \
     (pRsvpTeTnlInfo))) + 1) * IPV4_RRO_LEN)
    
#define RPTE_OUT_ARHOP_LIST_SIZE  \
     (((TMO_SLL_Count (&RSVPTE_TNL_OUTARHOP_LIST  \
     (pRsvpTeTnlInfo))) + 1) * IPV4_RRO_LEN)

/* Timer Block Structure */
#define TIMER_BLK_APP_TIMER(pTimerBlock)\
        (pTimerBlock->Timer)

#define TIMER_BLK_TIMER_PARAM(pTimerBlock)\
        (pTimerBlock->TmrParam)

#define TIMER_BLK_MSG_ID(pTimerBlock)\
        (pTimerBlock->u4MsgId)

#define TIMER_BLK_BACKOFF_INTERVAL(pTimerBlock)\
        (pTimerBlock->u4BackOffInterval)

#define TIMER_BLK_ERROR_SPEC(pTimerBlock)\
        (pTimerBlock->ErrorSpec)

#define TIMER_BLK_TX_TTL_VALUE(pTimerBlock)\
        (pTimerBlock->u1TxTTL)

/* Trie Info Structure */
#define RPTE_TRIE_INFO_MSG_TYPE(pTrieInfo)\
        (pTrieInfo->u1MsgType)

#define RPTE_TRIE_INFO_TNL_INFO(pTrieInfo)\
        ((pTrieInfo->u.pRsvpTeTnlInfo))
        
#define RPTE_TRIE_INFO_TIMER_BLK(pTrieInfo)\
        ((pTrieInfo->u.pTimerBlock))

#define GBL_IF_ENTRY_OFFSET_TBL           gau2IfEntryOffset
#define GBL_IF_ENTRY_LENGTH_TBL           gau2IfEntryLength
#define GBL_IF_ENTRY_BYTE_ORDER_TBL       gau1IfEntryByteOrder
#define GBL_IF_ENTRY_OFFSET_ELMNT(x)      gau2IfEntryOffset[x]
#define GBL_IF_ENTRY_LENGTH_ELMNT(x)      gau2IfEntryLength[x]
#define GBL_IF_ENTRY_BYTE_ORDER_ELMNT(x)  gau1IfEntryByteOrder[x]
#define GBL_IF_ENTRY_NODE_OFFSET          gu2IfEntryNodeOffset

#define RPTE_IS_PLR(x) \
    ((x & RPTE_FRR_NODE_STATE_PLR) == RPTE_FRR_NODE_STATE_PLR)
#define RPTE_IS_PLRAWAIT(x) \
    ((x & RPTE_FRR_NODE_STATE_PLRAWAIT) == RPTE_FRR_NODE_STATE_PLRAWAIT)
#define RPTE_IS_PLR_OR_PLRAWAIT(x) \
    (RPTE_IS_PLR(x) || RPTE_IS_PLRAWAIT(x))
#define RPTE_IS_HEADEND(x) \
    ((x & RPTE_FRR_NODE_STATE_HEADEND) == RPTE_FRR_NODE_STATE_HEADEND)
#define RPTE_IS_MP(x) \
    ((x & RPTE_FRR_NODE_STATE_MP) == RPTE_FRR_NODE_STATE_MP)
#define RPTE_IS_DMP(x) \
    ((x & RPTE_FRR_NODE_STATE_DMP) == RPTE_FRR_NODE_STATE_DMP)
#define RPTE_IS_DNLOST(x) \
    ((x & RPTE_FRR_NODE_STATE_DNLOST) == RPTE_FRR_NODE_STATE_DNLOST)
#define RPTE_IS_UPLOST(x) \
    ((x & RPTE_FRR_NODE_STATE_UPLOST) == RPTE_FRR_NODE_STATE_UPLOST)
#define RPTE_IS_TAILEND(x) \
    ((x & RPTE_FRR_NODE_STATE_TAILEND) == RPTE_FRR_NODE_STATE_TAILEND)
#define RPTE_IS_HEADEND_PLR(x) \
    ((x & RPTE_FRR_NODE_STATE_HEADEND_PLR) == RPTE_FRR_NODE_STATE_HEADEND_PLR)
#define RPTE_IS_PLR_MP(x) \
    ((x & RPTE_FRR_NODE_STATE_PLR_MP) == RPTE_FRR_NODE_STATE_PLR_MP)

#define RSVPTE_TNL_OR_ATTRLIST_SET_PRIO(pRsvpTeTnlInfo) \
    (UINT1)(RpteGetAttrFromTnlOfAttrList(pRsvpTeTnlInfo->pTeTnlInfo, RSVPTE_ATTR_SETUPPRI_BITMASK))

#define RSVPTE_TNL_OR_ATTRLIST_HLDNG_PRIO(pRsvpTeTnlInfo) \
    (UINT1)(RpteGetAttrFromTnlOfAttrList(pRsvpTeTnlInfo->pTeTnlInfo,RSVPTE_ATTR_HOLDPRI_BITMASK))

#define RSVPTE_TNL_OR_ATTRLIST_CLASS_TYPE(pRsvpTeTnlInfo) \
    (UINT1)(RpteGetAttrFromTnlOfAttrList(pRsvpTeTnlInfo->pTeTnlInfo, RSVPTE_ATTR_CLASS_TYPE_BITMASK))

#define RSVPTE_TNL_OR_ATTRLIST_INC_ANY_AFF(pRsvpTeTnlInfo) \
    (RpteGetAttrFromTnlOfAttrList(pRsvpTeTnlInfo->pTeTnlInfo,RSVPTE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK))

#define RSVPTE_TNL_OR_ATTRLIST_INC_ALL_AFF(pRsvpTeTnlInfo) \
    (RpteGetAttrFromTnlOfAttrList(pRsvpTeTnlInfo->pTeTnlInfo,RSVPTE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK))

#define RSVPTE_TNL_OR_ATTRLIST_EXC_ANY_AFF(pRsvpTeTnlInfo) \
    (RpteGetAttrFromTnlOfAttrList(pRsvpTeTnlInfo->pTeTnlInfo,RSVPTE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK))

#define RSVPTE_TNL_OR_ATTRLIST_SSN_ATTR(pRsvpTeTnlInfo) \
    (UINT1)(RpteGetAttrFromTnlOfAttrList(pRsvpTeTnlInfo->pTeTnlInfo,RSVPTE_ATTR_LSP_SSN_ATTR_BITMASK))

#define RSVPTE_TNL_OR_ATTRLIST_SRLG_TYPE(pRsvpTeTnlInfo) \
    (UINT1)(RpteGetAttrFromTnlOfAttrList(pRsvpTeTnlInfo->pTeTnlInfo,RSVPTE_ATTR_SRLG_TYPE_BITMASK))

#define RPTE_Double_SLL_Scan(pList1, pList2, pNode1, pNode2, TypeCast)\
        for(pNode1 = (TypeCast) TMO_SLL_First (pList1), \
            pNode2 = (TypeCast) TMO_SLL_First (pList2); \
            ((pNode1 != NULL) || (pNode2 != NULL)); \
            ((pNode1 = ((pNode1 != NULL) ? \
                        ((TypeCast) TMO_SLL_Next (pList1, \
                                                  (tTMO_SLL_NODE *) \
                                                  (VOID *) (pNode1))) \
                        : NULL)), \
            ((pNode2 = ((pNode2 != NULL) ? \
                        ((TypeCast) TMO_SLL_Next (pList2, \
                                                  (tTMO_SLL_NODE *) \
                                                  (VOID *) (pNode2))) \
                        : NULL)))))

#endif /* _RPTEMACS_H */
/*----------------------------------------------------------------------------*/
/*                      End of File rptemacs.h                                */
/*----------------------------------------------------------------------------*/
