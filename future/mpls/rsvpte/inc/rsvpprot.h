
/* $Id: rsvpprot.h,v 1.15 2014/03/14 12:37:49 siva Exp $*/
/********************************************************************
 *                                                                  *
 * $RCSfile: rsvpprot.h,v $
 *                                                                  *
 * $Date: 2014/03/14 12:37:49 $
 *                                                                  *
 * $Revision: 1.15 $
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *  FILE NAME             : rsvpprot.h 
 *  PRINCIPAL AUTHOR      : Aricent Inc. 
 *  SUBSYSTEM NAME        : MPLS 
 *  MODULE NAME           : RSVP-TE                      
 *  LANGUAGE              : ANCI-C
 *  TARGET ENVIRONMENT    : Linux (Portable)
 *  DATE OF FIRST RELEASE :
 *  DESCRIPTION           : This file contains prototypes 
 *****************************************************************************/

#ifndef _RSVPPROT_H
#define _RSVPPROT_H


VOID RptePhProcessPathMsg ARG_LIST ((tPktMap *));
VOID RptePthProcessPathTearMsg ARG_LIST ((tPktMap *));

VOID RpteRhProcessResvMsg ARG_LIST ((tPktMap *));
VOID RpteRthProcessResvTearMsg ARG_LIST ((tPktMap *));
VOID RpteRehProcessResvErrMsg ARG_LIST ((tPktMap *));

INT1 RptePvmExtractRROList ARG_LIST ((tPktMap * pPktMap,
                                      tObjHdr ** ppObjHdr, UINT1 *pEndOfPkt));

INT1 RptePvmExtractEROList ARG_LIST ((tPktMap * pPktMap,
                                      tObjHdr ** ppObjHdr, UINT1 *pEndOfPkt));

INT1 rptePvmExtractAdnlLblList ARG_LIST ((tPktMap * pPktMap,
                                          tObjHdr ** ppObjHdr,
                                          UINT1 *pEndOfPkt));

INT1 rptePvmExtractLblList ARG_LIST ((tPktMap * pPktMap,
                                      tObjHdr ** ppObjHdr, UINT1 *pEndOfPkt));

/* rpteipif.c */
UINT1 RpteSendMsgToUdp
ARG_LIST ((tTMO_BUF_CHAIN_DESC * pMsgBuf, tPktMap * pPktMap));
INT1 RpteIpGetIfAddr ARG_LIST ((INT4 i4IfIndex, UINT4 *pu4Addr));
VOID RpteUdpClose   ARG_LIST ((INT4 i4Port));
INT1 RpteUdpOpen    ARG_LIST ((INT4 i4Port, INT4 i4IfIndex));
void RpteIpGetIfId  ARG_LIST ((INT4 i4IfIndex, tCRU_INTERFACE * pIfId));
void RpteIpGetIfTtl ARG_LIST ((tIfEntry *pIfEntry, UINT1 *pu1Ttl));
tMsgBuf            *BUF_DUP_DATA_DESCR ARG_LIST ((tMsgBuf * pMsgBuf));

UINT1 RpteIpLocalAddress (UINT4 u4DestAddr);
UINT4 RpteIpGetIfMask (UINT4 u4IfIndex);
INT4  RpteIpIfSendPkt ARG_LIST ((tMsgBuf * pMsgBuf, tPktMap * pPktMap));

/* rptesock.c */
UINT1 RpteRecvViaSocket (void);
void rpteProcessSockPollTmrEvent (tTmrAppTimer *pExpiredTmr);


INT4 RpteOpenRawSocket (VOID);
INT4 rpteOpenUdpSocket (INT4 i4Port);
UINT1 RpteCloseRawSocket (INT4 i4RawSockFd);
UINT1 rpteCloseUdpSocket (INT4 i4UdpSockFd);
VOID rpteSockTaskMain (INT1 *);

/* rpteinit.c */
void RpteInitIfEntry ARG_LIST ((tIfEntry * pIfEntry));
INT1 rpteAllocSenderTable ARG_LIST ((void));
INT1 rpteAllocRsbArray ARG_LIST ((void));
INT1 rpteAllocRsbCmnHdrArray ARG_LIST ((void));
INT1 rpteAllocTcsbArray ARG_LIST ((void));
INT1 rpteAllocTcsbCmnHdrArray ARG_LIST ((void));
INT1 rpteAllocResvFwdArray ARG_LIST ((void));
INT1 rpteAllocResvFwdCmnHdrArray ARG_LIST ((void));
INT1 RpteOpenUdpInterface ARG_LIST ((void));
VOID RpteCloseUdpInterface ARG_LIST ((void));

/* rptepath.c */
void RptePhStartPathRefreshTmr ARG_LIST ((tPsb * pPsb));
INT1                rptePhCompareIfList (tCRU_INTERFACE * pIfList1,
                                         tCRU_INTERFACE * pIfList2,
                                         UINT4 u4ListLen);
/* rptepb.c */
void rptePbInitRaOpt ARG_LIST ((tRaOpt * pRaOpt));
void RptePbSendMsg  ARG_LIST ((tPktMap * pPktMap));

/* rptepvm.c */
void RptePvmSendPathErr
ARG_LIST ((tPktMap * pPktMap, UINT1 u1ErrCode, UINT2 u2ErrValue));
void RptePvmSendResvErr
ARG_LIST ((tPktMap * pPktMap, UINT1 u1ErrCode, UINT2 u2ErrValue));
INT1 RptePvmLocalAddr ARG_LIST ((UINT4 u4Addr));
INT1 RptePvmProcessPkt ARG_LIST ((tPktMap * pPktMap));

/* rpteutil.c */
INT1 RpteUtlBufChainedData ARG_LIST ((const tTMO_BUF_CHAIN_DESC * pMsgBuf));
UINT4 RpteUtlCheckSum ARG_LIST ((UINT1 *pPkt, UINT2 u2Size));
void RpteUtlInitPktMap ARG_LIST ((tPktMap * pPktMap));
INT1 RpteUtlUdpEncapsRequired ARG_LIST ((const tIfEntry * pIfEntry));
INT1 RpteUtlMcastAddr ARG_LIST ((UINT4 u4Addr));
INT1 RpteUtlCompareRsvpHop
ARG_LIST ((const tRsvpHop * pRsvpHop1, const tRsvpHop * pRsvpHop2));
void RpteUtlEncodeLih ARG_LIST ((UINT4 *pu4Lih, tIfEntry * pIfEntry));
void RpteUtlDecodeLih ARG_LIST ((UINT4 u4Lih, tIfEntry * pIfEntry));
INT1 RpteUtlPsbRoutesToOutIf
ARG_LIST ((const tPsb * pPsb, tIfEntry * pIfEntry, tRsvpHop * pRsvpHop));
INT1
RpteUtlPsbRoutesToInIf 
ARG_LIST ((const tPsb * pPsb, tIfEntry * pIfEntry, tRsvpHop * pRsvpHop));
INT1
RpteUtlRsbRoutesToInIf 
ARG_LIST ((const tRsb * pRsb, tIfEntry * pIfEntry,
                        tRsvpHop * pRsvpHop));
UINT4 RpteUtlComputeLifeTime
ARG_LIST ((const tIfEntry * pIfEntry, UINT4 u4RefreshTime));
INT1 RpteUtlCompareStyle ARG_LIST ((const tStyle * pStyle1, tStyle * pStyle2));
INT1 RpteUtlCompareIfId
ARG_LIST ((const tCRU_INTERFACE * pIfId1, tCRU_INTERFACE * pIfId2));
INT1 RpteUtlValidateStyle ARG_LIST ((const tStyle * pStyle));

VOID RpteMIHGenerateMsgId
ARG_LIST ((UINT4 *pu4NewMsgId));

VOID RpteMIHProcessRMDTimeOut
ARG_LIST ((const tTmrAppTimer *pExpiredTmr));

UINT4 RpteMIHProcessMsgId
ARG_LIST ((tNbrEntry *pNbrEntry, UINT4 u4Epoch, UINT4 u4MsgId,
           tuTrieInfo **ppTrieInfo));

UINT4 RpteMIHProcessMsgIdList
ARG_LIST ((tNbrEntry *pNbrEntry, UINT4 u4MsgId, tuTrieInfo **ppTrieInfo));
                          
VOID RpteMIHProcessMsgIdNacks
ARG_LIST ((tNbrEntry *pNbrEntry, tTMO_SLL *pMsgIdNackList));

VOID RpteMIHProcessMsgIdAcks
ARG_LIST ((tNbrEntry *pNbrEntry, tTMO_SLL *pMsgIdAckList, UINT4 u4NotifyAddr));

VOID RpteMIHProcessSRefreshMsg
ARG_LIST ((tPktMap *pPktMap));

VOID RpteMIHSendAckMsg
ARG_LIST ((const tNbrEntry *pNbrEntry, tPktMap *pPktMap));

VOID RpteMIHSendNackMsg
ARG_LIST ((const tNbrEntry *pNbrEntry, tTMO_SLL *pMsgIdList));

VOID RpteMIHResetMsgId
ARG_LIST ((tuTrieInfo *pTrieInfo));

VOID RpteMIHReleaseTrieInfo
ARG_LIST ((tuTrieInfo *pTrieInfo));

VOID RpteMIHGenerateEpoch
ARG_LIST ((VOID));

VOID RpteMIHInitMsgId
ARG_LIST ((VOID));

VOID RpteRRUpdateNbrEntry
ARG_LIST ((tNbrEntry *pNbrEntry, UINT1 u1RsvpVersionFlag));

VOID RpteRRNbrAutoDiscovery
ARG_LIST ((tNbrEntry *pNbrEntry, UINT1 u1RsvpVersionFlag));

INT1 RpteRRSendSRefreshMsg
ARG_LIST ((const tRsvpTeTnlInfo *pRsvpTeTnlInfo, UINT1 u1MsgType));

INT4 RpteRRCalculateBackOffDelay
ARG_LIST ((const tIfEntry *pIfEntry, UINT4 u4CurrDelay, UINT4 *pu4NewDelay));

VOID RpteRRProcessPkt
ARG_LIST ((tMsgBuf * pMsgBuf, UINT1 u1Encaps));

VOID rpteRRReleaseTrieInfo
ARG_LIST ((tuTrieInfo * pTrieInfo));

INT1 RpteRRProcessBundleMessage
ARG_LIST ((tPktMap *pPktMap));

VOID RpteRRProcessSRefreshTimeOut
ARG_LIST ((const tTmrAppTimer *pAppTimer));

VOID RpteRRSRefreshThreshold
ARG_LIST ((tNbrEntry *pNbrEntry, UINT1 u1Flags));

INT1 RpteRRStartSRefresh
ARG_LIST ((tNbrEntry *pNbrEntry));

INT1 RpteRRSRefreshAddMsgId
ARG_LIST ((tNbrEntry *pNbrEntry, UINT4 u4MsgId, UINT1 u1Flags));



#endif /* _RSVPPROT_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rsvpprot.h                             */
/*---------------------------------------------------------------------------*/
