/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptdsdef.h,v 1.8 2017/06/08 11:40:32 siva Exp $
 *
 * Description: This file contains the definations of constants
 *              used for DIFFSERV Support in Rsvpte Module.
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptdsdef.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains definitions required for DiffServ
 *                             support in RSVP-TE
 *---------------------------------------------------------------------------*/

#ifndef _RPTEDSDEFS_H
#define _RPTEDSDEFS_H

/* Diffserv Error code and values */
#define RPTE_DIFFSERV_ERROR 27
#define RPTE_UNEXPECTED_DIFFSERV_OBJ   1
#define RPTE_UNSUPPORTED_PHB           2
#define RPTE_INVALID_EXPPHB_MAPPING    3
#define RPTE_UNSUPPORTED_PSC           4
#define RPTE_LSP_CONTEXT_ALLOC_FAIL    5

/* Diffserv Class-Type Obj Error code and Values */
#define RPTE_DIFFSERV_TE_ERROR 28
#define RPTE_UNEXPECTED_CLASS_TYPE_OBJ 1
#define RPTE_UNSUPPORTED_CLASS_TYPE    2
#define RPTE_INVALID_CLASS_TYPE_VALUE  3
#define RPTE_UNSUPPORTED_CLASS_TYPE_SET_PRIO  4
#define RPTE_UNSUPPORTED_CLASS_TYPE_HOLD_PRIO 5
#define RPTE_UNSUPPORTED_CLASS_TYPE_HOLD_PRIO_SET_PRIO 6
#define RPTE_CLASS_TYPE_AND_PSC_INCONSISTENT 7
#define RPTE_CLASS_TYPE_AND_PHB_INCONSISTENT 8


#define RPTE_DS_ELSP_OBJ_CLASS 65
/* For Elsp: DiffServ Class = 65 (0x41) and CType = 1 (0x01) */
#define RPTE_DS_ELSP_OBJ_CLASS_NUM_TYPE 0x4101
/* For Llsp: DiffServ Class = 65 (0x41) and CType = 2 (0x02) */
#define RPTE_DS_LLSP_OBJ_CLASS_NUM_TYPE 0x4102

#define RPTE_ELSP_TP_OBJ_CLASS 27 
#define RPTE_ELSP_TP_OBJ_CTYPE 1
#define RPTE_ELSP_TP_OBJ_CLASS_NUM_TYPE 0x1b01

#define RPTE_CLASS_TYPE_OBJ_CLASS 66
#define RPTE_CLASS_TYPE_OBJ_CTYPE 1
#define RPTE_CLASS_TYPE_OBJ_CLASS_NUM_TYPE 0x4201

/* Group Notify Timer */
#define RPTE_TUN_GROUP_TIMER_DEFAULT_VALUE    3
#define RPTE_TUN_GROUP_TIMER_MAX_VALUE        300

#define RPTE_NON_DIFFSERV_LSP        0
#define RPTE_DIFFSERV_ELSP           1
#define RPTE_DIFFSERV_LLSP           2
#define RPTE_INTSERV                 3

/* Elsp Types*/
#define RPTE_DIFFSERV_PRECONF        0 
#define RPTE_DIFFSERV_SIGNALLED      1

/* Default Values */
#define RPTE_DIFFSERV_MIN_EXP             0
#define RPTE_DIFFSERV_MAX_EXP             8
#define RPTE_DIFFSERV_MAX_MAP_ENTRIES     8
#define RPTE_PREEMPT                 3
#define RPTE_DS_MAX_NO_OF_PHBS       14
#define RPTE_DS_MAX_NO_OF_PSCS       13
#define RPTE_DS_MAX_NO_OF_CLASSTYPES 8
#define RPTE_DS_BIT14_MASK           0x0002
#define RPTE_DS_DSCP_PHBID_MASK      0xfc00
#define RPTE_DS_DSCP_TO_LLSP_PHBID_MASK 0x0002
#define RPTE_DS_DSCP_TO_ELSP_PHBID_MASK 0x0000

#endif /* _RPTEDSDEFS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rptdsdef.h                             */
/*---------------------------------------------------------------------------*/
