/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptedsmc.h,v 1.6 2010/11/16 14:09:44 prabuc Exp $
 *
 * Description: This file contains the macros Used for DIFFSERV Support
 *              in Rsvpte Module. 
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptedsmc.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains macro definitions for 
 *                             DiffServ functionality in RSVP-TE 
 *---------------------------------------------------------------------------*/

#ifndef _RPTEDSMACS_H
#define _RPTEDSMACS_H

/*******************************************************************/
/*Macros Related To MplsDiffServTnlInfo */

#define RPTE_TE_DS_TNL(pRsvpTeTnlInfo)\
        (pRsvpTeTnlInfo->pTeTnlInfo->pMplsDiffServTnlInfo)

#define RSVPTE_TE_DS_TNL_CLASS_TYPE(pRsvpTeTnlInfo)\
          RPTE_TE_DS_TNL_CLASS_TYPE(RPTE_TE_DS_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TE_DS_TNL_ROWSTATUS(pRsvpTeTnlInfo)\
          RPTE_TE_DS_TNL_ROW_STATUS(RPTE_TE_DS_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TE_DS_TNL_STORAGETYPE(pRsvpTeTnlInfo)\
          RPTE_TE_DS_TNL_STORAGE_TYPE(RPTE_TE_DS_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TE_DS_TNL_SERVICE_TYPE(pRsvpTeTnlInfo)\
          RPTE_TE_DS_TNL_SERVICE_TYPE(RPTE_TE_DS_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TE_DS_TNL_ELSPLISTINDEX(pRsvpTeTnlInfo)\
          RPTE_TE_DS_TNL_ELSP_LIST_INDEX(RPTE_TE_DS_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TE_DS_TNL_FLAG(pRsvpTeTnlInfo)\
          RPTE_TE_DS_TNL_CFG_FLAG(RPTE_TE_DS_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TE_DS_TNL_ELSP_TYPE(pRsvpTeTnlInfo)\
          RPTE_TE_DS_TNL_ELSP_TYPE(RPTE_TE_DS_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TE_DS_TNL_LLSP_DSCP(pRsvpTeTnlInfo)\
          RPTE_TE_DS_TNL_LLSP_PSC_DSCP(RPTE_TE_DS_TNL(pRsvpTeTnlInfo))

#define RSVPTE_TE_DS_TNL_LIST_PTR(pRsvpTeTnlInfo)\
          RPTE_TE_DS_TNL_ELSP_INFO_LIST_INFO(RPTE_TE_DS_TNL(pRsvpTeTnlInfo))

/************************************************************************/
/* Macros Related to DiffServElspList*/
#define RSVPTE_TE_DS_TNL_ELSP_INFO_LIST(pRsvpTeTnlInfo) \
         RPTE_TE_DS_ELSP_INFOLIST(RPTE_TE_DS_TNL_ELSP_INFO_LIST_INFO\
	                                  (RPTE_TE_DS_TNL(pRsvpTeTnlInfo))) 
#define RSVPTE_TE_DS_ELSP_INFO_LIST_INDEX(pRsvpTeTnlInfo) \
         RPTE_TE_DS_ELSPINFOLIST_INDEX(RPTE_TE_DS_TNL_ELSP_INFO_LIST_INFO\
	                                  (RPTE_TE_DS_TNL(pRsvpTeTnlInfo))) 

/************************************************************************/
/* Macros Related to Elsp Object */

#define PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ(pPktMap) (pPktMap->pDiffServElspObj)

#define PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ_MAP_ENTRIES(pPktMap) \
                      (pPktMap->pDiffServElspObj->u1NoOfMapEntries)

#define PKT_MAP_RPTE_DIFFSERV_ELSP_OBJ_MAP_ENTRY(pPktMap,x) \
                      (pPktMap->pDiffServElspObj->aDiffServMapEntry[(x)])

#define PKT_MAP_RPTE_DIFFSERV_ELSP_EXP(pPktMap,x) \
                      (pPktMap->pDiffServElspObj->aDiffServMapEntry[(x)].u1Exp)

#define PKT_MAP_RPTE_DIFFSERV_ELSP_PHBID(pPktMap,x) \
                     (pPktMap->pDiffServElspObj->aDiffServMapEntry[(x)].u2PhbId)

#define RPTE_DS_NO_OF_ENTRIES(x,y) \
                     (x) = (UINT1)TMO_SLL_Count(RSVPTE_TE_DS_TNL_ELSP_INFO_LIST((y)))

#define RPTE_DS_ELSPTP_OBJ_SIZE(n,Size) (Size) += (sizeof (tObjHdr) + (4  * sizeof(UINT1))  + ((n) * sizeof (tRpteDiffServElspTPEntry)))

#define RPTE_DS_GET_PHBID_14TH_BIT(u2PhbId,u1Bit14) \
        (u1Bit14) =  (UINT1) ((u2PhbId & RPTE_DS_BIT14_MASK) >> 1)

#define RPTE_DS_DSCP_FOR_PHBID(u2Var1,u1Var2) \
                  (u1Var2) = (UINT1)((u2Var1 & RPTE_DS_DSCP_PHBID_MASK ) >> 10)

#define RPTE_DS_DSCP_TO_LLSP_PHBID(u1Dscp,u2PhbId) \
        u2PhbId = u1Dscp;\
        u2PhbId = (UINT2)((u2PhbId << 10) | RPTE_DS_DSCP_TO_LLSP_PHBID_MASK)

#define RPTE_DS_DSCP_TO_ELSP_PHBID(u1Dscp,u2PhbId) \
        u2PhbId = u1Dscp;\
        u2PhbId = (UINT2)((u2PhbId << 10) | RPTE_DS_DSCP_TO_ELSP_PHBID_MASK)
/**************************************************************************/
/* Macros Related to Llsp Object */

#define PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ(pPktMap) (pPktMap->pDiffServLlspObj)

#define PKT_MAP_RPTE_DIFFSERV_LLSP_OBJ_PSC(pPktMap) \
                      (pPktMap->pDiffServLlspObj->u2Psc)
/**************************************************************************/
/* Macros Related to Class-Type Object */
#define PKT_MAP_DIFFSERV_CLASS_TYPE_OBJ(pPktMap) \
                                    (pPktMap->pRpteClassTypeObj)

#define PKT_MAP_DIFFSERV_CLASS_TYPE(x) ((x)->u1ClassType)

/* Macros Related to ElspTP Object */
#define PKT_MAP_DIFFSERV_ELSPTP_OBJ(pPktMap) (pPktMap->pElspTrfcProfObj)
                         
#define PKT_MAP_DIFFSERV_ELSPTP_NO_OF_ENTRIES(x) \
                            ((x)->pElspTrfcProfObj->u1NoOfElspTPEntries)

#define PKT_MAP_DIFFSERV_ELSPTP_PSC(pPktMap,x) \
           (pPktMap->pElspTrfcProfObj->aElspTPEntry[x].u2Psc)

#define PKT_MAP_DIFFSERV_ELSPTP_TB_RATE(pPktMap,x) \
           (pPktMap->pElspTrfcProfObj->aElspTPEntry[x].u4TokenBktRate)

#define PKT_MAP_DIFFSERV_ELSPTP_TB_SIZE(pPktMap,x) \
           (pPktMap->pElspTrfcProfObj->aElspTPEntry[x].u4TokenBktSize)

#define PKT_MAP_DIFFSERV_ELSPTP_PARAM_PD_RATE(pPktMap,x) \
           (pPktMap->pElspTrfcProfObj->aElspTPEntry[x].u4PeakDataRate)

#define PKT_MAP_DIFFSERV_ELSPTP_PARAM_MIN_POL_UNIT(pPktMap,x) \
           (pPktMap->pElspTrfcProfObj->aElspTPEntry[x].u4MinPolicedUnit)

#define PKT_MAP_DIFFSERV_ELSPTP_PARAM_MAXPKTSIZE(pPktMap,x) \
           (pPktMap->pElspTrfcProfObj->aElspTPEntry[x].u4MaxPktSize)

#define RPTE_RM_SUPPORTED_PHBS(x,y,z)        RM_SUPPORTED_PHB(y,z)

#define RPTE_RM_SUPPORTED_PSC(x,y,z)    RM_SUPPORTED_PSC(y,z)

#define RPTE_RM_SUPPORTED_CLASS_TYPES(x,y,z) RM_SUPPORTED_CLASSTYPE(y,z)
                            
#define RSVPTE_HOLD_PSC_OF_TNLS_LIST_ARRAY(x,y) \
                        ((x)->aRpteHoldPscOfTnls[(y)])

#define RSVPTE_DS_OVER_RIDE gpRsvpTeGblInfo->u4OverRide

/* Preemption Related */

#define RPTE_PSC_FOUND(pRsvpTeTnlInfo,x) (pRsvpTeTnlInfo->au1PscFound[x-1])

#define RPTE_PSC_HANDLE(pRsvpTeTnlInfo,x) (pRsvpTeTnlInfo->au4Handles[x-1])

#define RPTE_PSC_ARRAY(pRsvpTeTnlInfo) (pRsvpTeTnlInfo->au1PscFound)

#define RPTE_NEXT_PMT_TUNNEL_IN_PSC(x,y)  &((x)->aNextPreemptTnlInPSC[y-1])

#define RPTE_PMT_HOLD_PSC_OF_TNL_LIST(x,y)  ((x)->aRpteHoldPscOfTnls[y-1])

#define  RPTE_DS_GET_PDR_OF_PSC(Psc,x,pMplsDiffServElspInfo,Bwidth) \
if(RSVPTE_TE_DS_TNL_SERVICE_TYPE((x)) == RPTE_DIFFSERV_ELSP) {\
	if (RSVPTE_TE_DS_TNL_ELSPLISTINDEX (pRsvpTeTnlInfo) != RSVPTE_ZERO){\
       TMO_SLL_Scan(RSVPTE_TE_DS_TNL_ELSP_INFO_LIST((x)), \
                           pMplsDiffServElspInfo,tMplsDiffServElspInfo*) { \
             if (RpteDiffServGetPscIndex(RPTE_TE_DS_ELSP_INFO_PHB_DSCP\
				     (pMplsDiffServElspInfo)) == Psc) {\
         Bwidth = OSIX_NTOHL(RPTE_DS_ELSP_TPARAM_PDR(pMplsDiffServElspInfo));\
            break;\
       }\
       else{ Bwidth = 0;} \
       }\
   }\
}\
else\
{\
if(RSVPTE_TE_DS_TNL_SERVICE_TYPE((x)) == RPTE_DIFFSERV_LLSP)\
       Bwidth = (UINT4)OSIX_NTOHF(RESV_TSPEC_PEAK_RATE(FLOW_SPEC_OBJ\
                                          (RSVPTE_TNL_RSB(pRsvpTeTnlInfo))));}


#endif /* _RPTEDSMCS_H */           
/*---------------------------------------------------------------------------*/
/*                        End of file rptedsmc.h                             */
/*---------------------------------------------------------------------------*/
