/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rpteclip.h,v 1.18 2016/02/03 10:37:55 siva Exp $
 *
 ********************************************************************/

/*-----------------------------------------------------------------------------
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Licensee Aricent Inc.ware, 1999-2006
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : rpteclip.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the prototypes of functions 
 *                             present in rsvpcli.c
 *----------------------------------------------------------------------------*/


#ifndef __RPTECLIP_H__

#define __RPTECLIP_H__ 

#include "mplscli.h" 

PRIVATE INT4
RpteCliCreateRsvpInterface ARG_LIST ((tCliHandle CliHandle, UINT4 u4IfIndex));

PRIVATE INT4
RpteCliSetRsvpTeOperStatus ARG_LIST ((tCliHandle CliHandle, INT4 i4Status));

PRIVATE INT4
RpteCliSetRsvpAdminStatusCapability ARG_LIST ((tCliHandle CliHandle, INT4 i4AdminStatus));

PRIVATE INT4
RpteCliSetRsvpPathStateRemove ARG_LIST ((tCliHandle CliHandle, INT4 i4PathState));

PRIVATE INT4
RpteCliSetLabelSetSupport (tCliHandle CliHandle, INT4 i4LabelSet);

INT4 RpteCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel); 
INT4 RpteCliGetDebugLevelVal(tCliHandle CliHandle, UINT4 *pu4ArgsOne);

PRIVATE INT4
RpteCliSetGracefulDeletionTimerValue(tCliHandle CliHandle, UINT4 u4TimerValue);

PRIVATE INT4
RpteCliSetRsvpTeRouterId ARG_LIST ((tCliHandle CliHandle, INT4 i4Type,
                                    UINT4 u4Value));
PRIVATE INT4
RpteCliSetRsvpTeRMDPolicy ARG_LIST ((tCliHandle CliHandle,
                                     UINT4 u4RMDBitMask,
                                     INT4 i4Status));
PRIVATE INT4
RpteCliDbgSetMsgDumpAttr ARG_LIST ((tCliHandle CliHandle, INT4 i4MsgType,
                                    INT4 i4dumpDir));
PRIVATE INT4
RpteCliDbgReSetMsgDumpAttr ARG_LIST ((tCliHandle CliHandle, INT4 i4MsgType,
                                      INT4 i4dumpDir));

PRIVATE INT4
RpteCliDbgSetDumpMsgLvl ARG_LIST ((tCliHandle CliHandle, INT4 i4DumpLvl));

PRIVATE INT4
RpteCliDbgReSetDumpMsgLvl ARG_LIST ((tCliHandle CliHandle, INT4 i4DumpLvl));

PRIVATE INT4
RpteCliShowRsvpMsgCounters ARG_LIST ((tCliHandle CliHandle, UINT4 u4ifIndex));

PRIVATE INT4
RpteCliClearRsvpMsgCounters ARG_LIST ((tCliHandle CliHandle, UINT4 u4ifIndex));

PRIVATE INT4
RpteCliShowHelloInstInfo ARG_LIST ((tCliHandle CliHandle, UINT4 u4NbrAddr,
                                    INT4 i4DispType));
PRIVATE INT4
RpteCliShowHelloInstInfoBrief ARG_LIST ((tCliHandle CliHandle,
                                         tNbrEntry *pNbrEntry));
PRIVATE INT4
RpteCliShowHelloInstInfoDetail ARG_LIST ((tCliHandle CliHandle,
                                         tNbrEntry *pNbrEntry));
PRIVATE INT4
RpteCliShowNeighborInfo ARG_LIST ((tCliHandle CliHandle, UINT4 u4NbrAddr,
                                   INT4 i4DispType));
PRIVATE INT4
RpteCliShowNeighborInfoBrief ARG_LIST ((tCliHandle CliHandle,
                                        tNbrEntry * pNbrEntry));
PRIVATE INT4
RpteCliShowNeighborInfoDetail ARG_LIST ((tCliHandle CliHandle,
                                         tNbrEntry * pNbrEntry));
PRIVATE INT4
RpteCliShowPathReq ARG_LIST ((tCliHandle CliHandle, INT4 i4DispType));

PRIVATE INT4
RpteCliShowPathReqBrief ARG_LIST ((tCliHandle CliHandle,
                                   tRsvpTeTnlInfo *pRsvpTeTnlInfo));
PRIVATE INT4
RpteCliShowPathReqDetail ARG_LIST ((tCliHandle CliHandle,
                                    tRsvpTeTnlInfo *pRsvpTeTnlInfo));
PRIVATE INT4
RpteCliShowSrcPathReq ARG_LIST ((tCliHandle CliHandle, INT4 i4DispType,
                                  UINT4 u4Addr));
PRIVATE INT4
RpteCliShowDestPathReq ARG_LIST ((tCliHandle CliHandle, INT4 i4DispType,
                                  UINT4 u4Addr));
PRIVATE INT4
RpteCliSetRsvpTeOverRideOption ARG_LIST ((tCliHandle CliHandle, INT4 i4Value));

PRIVATE VOID
RpteCliRsvpTeDumpDirection (tCliHandle CliHandle, INT4 i4DumpDirection);

/*FRR*/
PRIVATE INT4
TeCliFrrDetourMerging ARG_LIST ((tCliHandle CliHandle, INT4 i4DeMerg));
PRIVATE INT4
TeCliFrrDetour ARG_LIST ((tCliHandle CliHandle, INT4 i4Detour));
PRIVATE INT4
TeCliFrrLocalRevertive ARG_LIST ((tCliHandle CliHandle));
PRIVATE INT4
TeCliFrrGlobalRevertive ARG_LIST ((tCliHandle CliHandle));
PRIVATE INT4
TeCliFrrCspf ARG_LIST ((tCliHandle CliHandle, INT4 i4RetryTimeInterval, 
               UINT4 u4RetryCount));
PRIVATE INT4
TeCliFrrBackupTunnel ARG_LIST((tCliHandle CliHandle,INT4 i4TunnelNo));
PRIVATE INT4
TeCliFrrNoBackupTunnel ARG_LIST((tCliHandle CliHandle, INT4 i4TunnelNo));
PRIVATE INT4
TeCliFrrPlrAndAvoidNodeId ARG_LIST((tCliHandle CliHandle, 
                                    UINT4 u4PlrId, UINT4 u4AvoidNodeId));
PRIVATE INT4
TeCliFrrMakeAfterBreak ARG_LIST((tCliHandle CliHandle, INT4 i4MakeAfterBreak));
/*End of FRR*/
PRIVATE INT4
RpteCliSetGRCapability (tCliHandle CliHandle, INT4 i4GrCapability,
                                                UINT4 u4RecoveryPathCapability);
PRIVATE INT4
RpteCliSetGrRestartTimerValue (tCliHandle CliHandle, INT4 i4TimerValue);
PRIVATE INT4
RpteCliSetGrRecoveryTimerValue (tCliHandle CliHandle, INT4 i4TimerValue);
VOID
RpteCliDisplayGrConfigurations (tCliHandle CliHandle);
VOID
RpteCliDisplayNbrGrConfigurations (tCliHandle CliHandle, tNbrEntry *pNbrEntry);
PRIVATE INT4
RpteCliAddNeighbor ARG_LIST((tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4NbrId));
PRIVATE INT4
RpteCliDelNeighbor ARG_LIST((tCliHandle CliHandle, INT4 i4IfIndex, UINT4 u4NbrId));
VOID RpteCliCheckRsvpCommand ARG_LIST((tCliHandle CliHandle, UINT1 *u1RsvpFlag));
PRIVATE INT4
RpteCliSetReoptimizeTimerValue ARG_LIST((tCliHandle CliHandle, INT4 i4ReoptimizeTime));
PRIVATE INT4
RpteCliSetEroCacheTimerValue ARG_LIST((tCliHandle CliHandle, INT4 i4EroCacheTime));
PRIVATE INT4
RpteCliLspReoptLinkMaintenance ARG_LIST((tCliHandle CliHandle, INT4 i4IfIndex));
PRIVATE INT4
RpteCliLspReoptNodeMaintenance ARG_LIST((tCliHandle CliHandle));
PRIVATE INT4
RpteCliShowLspReoptParameters ARG_LIST((tCliHandle CliHandle));
#endif 
