/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rsvptdf2.h,v 1.28 2014/12/12 11:56:47 siva Exp $
 *
 ********************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rsvptdf2.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the structure type
 *                             definitions declared and used for RSVP-TE
 *                             funcitonality. These declarations are used
 *                             by rptetdf1.h and rptetdf2.h 
 *----------------------------------------------------------------------------*/

#ifndef _RSVPTDF2_H
#define _RSVPTDF2_H

#include "rptedefs.h"
#include "rptetdf1.h"
#include "rptetdf2.h"

typedef struct PathStateBlock
{
    struct InterfaceEntry *pIncIfEntry;
    struct InterfaceEntry *pOutIfEntry;
    struct _RsvpTeTnlInfo *pRsvpTeTnlInfo;
    tTmrAppTimer        PsbTmr;
    tTimerParam         PsbTmrParam;
    tSenderTspecObj     SenderTspecObj;
    tRsvpHop            RsvpHop;
    /* This variable denotes the information about the RSVP-IF ID hop */
    tIfIdRsvpTlvObj     RsvpTlvObj; 
    tIfIdRsvpNumTlvObj  RsvpNumTlvObj;
    /* This variable denotes the information about the GenreicLabel 
     * Request */
    tGenLblReq          GenLblReq; 
    /* This variable denotes the information about Admin status object */
    tAdminStatusObj     AdminStatusObj; 
    /* This variable denotes the information about the LSP_TUNNEL_INTERFACE_ID 
     * object */
    tLspTnlIfId         LspTnlIfId;
    /* This variable denotes the information about the PROTECTION object */
    tProtection         Protection;
    /* This variable denotes the information about the ASSOCIATION object */
    tAssociation        Association;
    tAdSpec             AdSpec;
    tAtmRsvpLblRngEntry AtmLblRngEntry;
    tRsvpTeSndrTmp      RsvpTeSndrTmp;
    tRsvpTeDetour       RsvpTeDetour; 
    tRsvpTeFastReroute  RsvpTeFastReroute; 
    UINT4               u4RefreshInterval;
    UINT4               u4LastChange;
    UINT4               u4TimeToDie;
    UINT4               u4InRefreshInterval;

    UINT1               u1RemainingTtl;
    UINT1               u1NonRsvpFlag;
    UINT1               u1RxTtl;
    UINT1               u1TimerType;

    UINT4               u4FrrFacLocRevProtTnlIfIndex;
    UINT4               u4FrrFacLocRevProtRsvpIfIndex;
    UINT4               u4FrrFacLocRevByPassTnlIfIndex;
    /* Denotes the address to which Notification needs to be sent */
    UINT4               u4NotifyAddr;
}
tPsb;

typedef struct ReserveStateBlock
{
    struct InterfaceEntry *pIncIfEntry;
    struct InterfaceEntry *pOutIfEntry;
    struct _RsvpTeTnlInfo *pRsvpTeTnlInfo;
    tStyle              Style;
    tRsvpHop            NextRsvpHop;
    tRsvpHop            FwdRsvpHop;
    tFlowSpec           FlowSpec;
    tResvConf           ResvConf;
    tRsvpTeFilterSpec   RsvpTeFilterSpec;
    tSenderTspec        TCTspec;
    tTmrAppTimer        Timer;
    tTimerParam         TmrParam;
    UINT4               u4Rhandle;
    /* This variable denotes the information about the RSVP-IF ID hop */
    tIfIdRsvpTlvObj     RsvpTlvObj;
    /* This variable denotes the information about Admin status object */
    tAdminStatusObj     AdminStatusObj; /* Admin status object header*/
    /* This variable denotes the information about the LSP_TUNNEL_INTERFACE_ID 
     * object */
    tLspTnlIfId         LspTnlIfId;
    UINT4               u4AdminStatus;
    UINT4               u4LastChange;
    UINT4               u4TimeToDie;
    UINT4               u4RefreshInterval;
    UINT4               u4InRefreshInterval;
    /* Denotes the address to which Notification needs to be sent */
    UINT4               u4NotifyAddr;
    UINT1               u1FrrMlibUpdateReq;
    UINT1               u1NonRsvpFlag;
    UINT1               u1TimerType;
    UINT1               u1Rsvd;
}
tRsb;

typedef struct _RpteIfInfo
{
    UINT4               u4MaxMTUSize;  /* Maximum Tx Unit size for this
                                        * interface */
    UINT4               u4LinkAttr;    /* Link Attribute values associated */

    UINT2               u2Flags;       /* Flags to indicate the RpteInfo fields
                                        * are set or not */
    UINT2               u2MinVpi;

    UINT2               u2MinVci;
    UINT2               u2MaxVpi;

    UINT2               u2MaxVci;
    UINT2               u2LblSpaceId;  /* Lbl Space Id given by Lbl Mngr */

    UINT2               u2NumTnlAssoc; /* Num Tnls associted with the Iface */
                                       /* DLCI tobe added here if FR is 
                                        * supported */
    UINT1               u1LblSpace;    /* ATM/FR/Ethernet/Optical etc.. */
    UINT1               u1LblType;     /* ATM Lbl / FR Lbl */

    UINT1               u1VcMerge;
    UINT1               u1VcDir;
    UINT1               au1Pad[2];
}
tRpteIfInfo;

typedef struct IfStatsInfo
{
    UINT4 u4IfNumTnls;
    UINT4 u4IfNumMsgSent;
    UINT4 u4IfNumMsgRcvd;
    UINT4 u4IfNumHelloSent;
    UINT4 u4IfNumHelloRcvd;
    UINT4 u4IfNumPathErrSent;
    UINT4 u4IfNumPathErrRcvd;
    UINT4 u4IfNumPathTearSent;
    UINT4 u4IfNumPathTearRcvd;
    UINT4 u4IfNumResvErrSent;
    UINT4 u4IfNumResvErrRcvd;
    UINT4 u4IfNumResvTearSent;
    UINT4 u4IfNumResvTearRcvd;
    UINT4 u4IfNumResvConfSent;
    UINT4 u4IfNumResvConfRcvd;
    UINT4 u4IfNumBundleMsgSent;
    UINT4 u4IfNumBundleMsgRcvd;
    UINT4 u4IfNumSRefreshMsgSent;
    UINT4 u4IfNumSRefreshMsgRcvd;
    UINT4 u4IfNumPathSent;
    UINT4 u4IfNumPathRcvd;
    UINT4 u4IfNumResvSent;
    UINT4 u4IfNumResvRcvd;
    UINT4 u4IfNumPktDiscrded;

    /* This variable denotes the Notify message sent counter */
    UINT4 u4IfNumNotifyMsgSent;
    /* This variable denotes the Notify message received counter */
    UINT4 u4IfNumNotifyMsgRcvd;
    UINT4 u4IfNumRecoveryPathSent;
    UINT4 u4IfNumRecoveryPathRcvd;
    UINT4 u4IfNumPathSentWithRecoveryLbl;
    UINT4 u4IfNumPathRcvdWithRecoveryLbl;
    UINT4 u4IfNumPathSentWithSuggestedLbl;
    UINT4 u4IfNumPathRcvdWithSuggestedLbl;
    UINT4 u4IfNumHelloSentWithRestartCap;
    UINT4 u4IfNumHelloRcvdWithRestartCap;
    UINT4 u4IfNumHelloSentWithCapability;
    UINT4 u4IfNumHelloRcvdWithCapability;
}
tIfStatInfo;

typedef struct InterfaceEntry
{
    tTMO_HASH_NODE      NextIfHashNode;
    tTMO_SLL            NbrList;
    tTMO_DLL            FacilityTnlList; /* No. of Rsvp Tnls information 
                                            associated with this interface */
    tTMO_SLL            aRpteDnStrPrioTnlList[RPTE_HOLD_PRIO_RANGE];
    tTMO_SLL            aRpteUpStrPrioTnlList[RPTE_HOLD_PRIO_RANGE];
    tTMO_SLL            aRpteHoldPscOfTnls[RPTE_DS_MAX_NO_OF_PSCS];
    tTmrAppTimer        IfEntryTmr;
    tTimerParam         IfEntryTmrParam;
    tCRU_INTERFACE      IfId;
    tRpteIfInfo         RpteIfInfo;
    tIfStatInfo         IfStatsInfo;
    UINT4               u4IfIndex;
    UINT4               u4Addr;
    UINT4               u4Mask;
    UINT4               u4UdpNbrs;
    UINT4               u4IpNbrs;
    UINT4               u4Nbrs;
    UINT4               u4RefreshInterval;
    UINT4               u4RouteDelay;
    UINT4               u4PlrId;
    UINT4               u4AvoidNodeId;

    UINT2               u2MaxTnls;
    UINT2               u2RefreshBlockadeMultiple;

    UINT2               u2RefreshMultiple;
    UINT1               u1HelloSprt;
    UINT1               u1Enabled;

    UINT1               u1UdpRequired;
    UINT1               u1Ttl;
    UINT1               u1Status;
    UINT1               au1Pad[1];
}
tIfEntry;

/* If change notification interface structure */
typedef struct RsvpIfMsg
{
    UINT4               u4IfIndex;
    UINT4               u4DataTeIfIndex;
    UINT4               u4Addr;
    UINT4               u4Status;
}
tRsvpIfMsg;

typedef struct _HelloNbrInfo
{
    UINT4               u4SrcIns;
    UINT4               u4DstIns;
    UINT4               u4TimeToDie;
    UINT4               u4TimeWait;

    UINT1               u1HelloState;
    UINT1               u1HelloRel;
    UINT1               u1HelloResetSent;
    UINT1               u1Rsvd;
}
tHelloNbrInfo;

typedef struct NeighbourEntry
{
    tTMO_SLL_NODE      *pNext;
    tIfEntry           *pIfEntry;
    tTMO_DLL            UpStrTnlList;  /* This DLL holds list of Upstream Tunnels
                                          associated with this neighbor. */
    tTMO_DLL            DnStrTnlList;  /* This DLL holds list of Downstream Tunnels
                                          associcated with this neighbor. */
    tTMO_SLL            SRefreshList;
    tTimerParam         SRefreshTmrParam;
    tTmrAppTimer        SRefreshTmr;
    /*For both GR - restart timer and recovery timer */
    tTimerParam         GrTimerParam;
    tTmrAppTimer        GrTimer;
    tHelloNbrInfo       HelloNbrInfo;
    UINT4               u4Addr;
    UINT4               u4NbrCreateTime;
    UINT4               u4LclPrtDetectTime;
    UINT4               u4NbrNumTnls;
    UINT4               u4Epoch;
    UINT4               u4MaxMsgId;
    UINT4               u4NbrStatus;
    UINT4               u4AdminStatusTimerValue;
    /* This variable denotes the restart timer value of neighbor */
    UINT2               u2RestartTime;
    /* This variable denotes the recovery timer value of neighbor */
    UINT2               u2RecoveryTime;

    UINT4               u4SrcInstInfo;
    UINT4               u4DstInstInfo;

    UINT1               u1Encap;
    UINT1               u1Status;
    UINT1               u1RRCapable;
    UINT1               u1RRState;

    UINT1               u1RMDCapable;
    UINT1               u1SRefreshStarted;
    UINT1               u1HelloSupported;
    UINT1               u1HelloState;

    UINT1               u1HelloRelation;
    BOOL1               bIsHelloActive;
    /*This variable denotes the GR state of the neighbore. 5 values are
     * applicable. 1.Not started, 2. Restart in progress, 3. Aborted 
     * 4. Recovery in progres, 5. Completed */
    UINT1               u1GrProgressState;
    UINT1               u1GrFaultType;
    /* This variable denotes whether RecoveryPath message capability
     * is enabeled in Neighbor */
    UINT1               u1RecoveryPathCapability;
    BOOL1               bIsHelloSynchronoziedAfterRestart;
    UINT1               au1Rsvd[2];
}
tNbrEntry;

typedef struct PacketMap
{
    tCRU_BUF_CHAIN_HEADER *pMsgBuf;
    tObjHdr               *pUnknownObjHdr;
    tRsvpHdr              *pRsvpHdr;
    tAdSpecObj            *pAdSpecObj;
    tRsvpHopObj           *pRsvpHopObj;
    tTimeValuesObj        *pTimeValuesObj;
    tErrorSpecObj         *pErrorSpecObj;
    tScopeObj             *pScopeObj;
    tStyleObj             *pStyleObj;
    tSenderTspecObj       *pSenderTspecObj;
    tResvConfObj          *pResvConfObj;
    tIfEntry              *pIncIfEntry;
    tIfEntry              *pOutIfEntry;
    tRsvpTeSsnObj         *pRsvpTeSsnObj;
    tRsvpTeFastRerouteObj *pRsvpTeFastRerouteObj;
    tRsvpTeDetourObj       RsvpTeDetourObj;
    tGenLblObj             *pGenLblObj;
    /* This variable denotes the pointer to the Upstream Label object */
    tGenLblObj            *pUpStrLblObj;
    tGenLblObj             *pAdnlLblObj;
    tLblReqAtmRngObj      *pLblReqAtmRngObj;
    /* This varibal denotes the pointer to the Generic label request 
     * object */
    tGenLblReqObj         *pGenLblReqObj; 
    tHelloObj             *pHelloObj;
    tRsvpTeFilterSpecObj  *pRsvpTeFilterSpecObj;
    tRsvpTeSndrTmpObj     *pRsvpTeSndrTmpObj;
    tRsvpTeFilterSpecObj  *pAdnlRsvpTeFilterSpecObj;
    tFlowSpecObj          *pFlowSpecObj;
    tDiffServElspObj       *pDiffServElspObj;
    tDiffServLlspObj       *pDiffServLlspObj;
    tElspTrafficProfileObj *pElspTrfcProfObj;
    tDiffServClassTypeObj  *pRpteClassTypeObj;
    /* This variable denotes the pointer to the RSVP-IF ID Error spec object */
    tIfIdRsvpErrObj        *pIfIdRsvpErrObj;
    /* This variable denotes the pointer to the RSVP-IF ID Number Error spec 
     * object */
    tIfIdRsvpNumErrObj     *pIfIdRsvpNumErrObj;
    /* This variable denotes the pointer to the RSVP-IF ID hop object */
    tIfIdRsvpHopObj        *pIfIdRsvpHopObj;
    /* This variable denotes the pointer to the RSVP-IF ID Number hop object */
    tIfIdRsvpNumHopObj     *pIfIdRsvpNumHopObj;
    /* This variable denotes the pointer to the Admin status object */
    tAdminStatusObj    *pAdminStatusObj;
    /* This variable denotes the pointer to the label set object */
    tLabelSetObj           *pLabelSetObj; 
    /* This variable denotes the pointer to the notify request object */
    tNotifyRequestObj      *pNotifyRequestObj;
    /* This variable denotes the pointer to the Protection object */
    tProtectionObj         *pProtectionObj;
    /* This variable denotes the pointer to the Association object */
    tAssociationObj        *pAssociationObj;
    /* This variable denotes the pointer to the LSP Tunnel Id object */
    tLspTnlIfIdObj        *pLspTnlIfIdObj;
    /* This variable denotes the pointer to the Recovery Label object */
    tRecoveryLblObj        *pRecoveryLblObj;
    /* This variable denotes the pointer to the Suggested label object */
    tSuggestedLblObj       *pSuggestedLblObj;
    /* This variable denotes the pointer to the Restart_Cap object */
    tRestartCapObj         *pRestartCapObj;
    /* This variable denotes the pointer to the Capability object */
    tCapabilityObj         *pCapabilityObj;
    tNbrEntry              *pNbrEntry;
    tRsvpTeTnlInfo         *pRsvpTeTnlInfo;
    tMsgIdListObj          *pMsgIdListObj;
    UINT1                 *pIpPkt;
    UINT1                 *pRsvpPkt;
    UINT1                 *pOpt;
    tErObj                ErObj;
    tMsgIdObj              MsgIdObj;
    tTMO_SLL               MsgIdAckList;
    tTMO_SLL               MsgIdNackList;
    tTMO_SLL              NotifyMsgTnlList;
    tSsnAttrObj           SsnAttrObj;
    tRASsnAttrObj         RASsnAttrObj;
    tRrObj                RrObj;
    UINT4                 u4SrcAddr;
    UINT4                 u4DestAddr;

    UINT2                 u2Id;    /* Optional ID field for IP */
    INT2                  i2Olen;    /* Length of IP options if specified */

    UINT1                 u1RxTtl;
    UINT1                 u1EncapsType;
    UINT1                 u1RsvpPktAlloc;
    UINT1                 u1RaOptNeeded;

    UINT1                 u1Tos;
    UINT1                 u1TxTtl;
    UINT1                 u1Df;    /* Fragmentation control */
    UINT1                 u1SessionHashIndex;

    UINT1                 u1ReRtFlag;
    UINT1                 u1RpteMsgFlag;
    UINT1                 u1RROErrVal;
    UINT1                 u1EroErrType;

    UINT1                 u1TnlAlloc;
    UINT1                 u1BuildPkt;
    UINT1                 u1UnknownObj;
    UINT1                 u1PktPath;
}
tPktMap;

typedef struct SnmpRsvpRequest
{
    union
    {
        tPsb               *pPsb;
        tRsb               *pRsb;
    }
    u;
    UINT1               u1ReqId;
    UINT1               u1Rsvd;
    UINT2               u2Rsvd;
}
tSnmpRsvpReq;

#endif /* _RSVPTDF2_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rsvptdf2.h                             */
/*---------------------------------------------------------------------------*/
