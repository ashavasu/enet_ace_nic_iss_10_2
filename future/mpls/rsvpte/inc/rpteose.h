/*----------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved                                                   */
/* Licensee Aricent Inc., 2001-2002                                    */
/*----------------------------------------------------------------------------*/
/*    FILE  NAME             : rpteose.h                                                                 */
/*    PRINCIPAL AUTHOR       : Aricent Inc.                                                        */
/*    SUBSYSTEM NAME         : MPLS                                                                 */
/*    MODULE NAME            : RSVP (IPIFACE SUB-MODULE)                              */
/*    LANGUAGE               : ANSI-C                                                                            */
/*    TARGET ENVIRONMENT     : OSE 4.4                                                */
/*    DATE OF FIRST RELEASE  :                                                                                   */
/*    DESCRIPTION            : This file contains the OSE include files            */
/*                                             and the definition of the macros used for      */
/*                                             the RSVP- OSE INET Interactions                 */
/******************************************************************************/
#ifndef __RPTEOSE_H__
#define __RPTEOSE_H__

#include <string.h>
#ifdef USE_OSEDEF_H
#include "osedef.h"
#endif
#include "ose.h"
#include "inet.h"
#include "inet.sig"
#include "osetypes.h"

#include "rpteincs.h"

#define  RPTE_SIGALLOC                            alloc 
#define  RPTE_SIGFREE                             free_buf
#define  RPTE_MEMSET                              memset
#define  RPTE_SIGSEND                             send
#define  RPTE_SIGRECV                             receive
#define  RPTE_SIGRECV_WTMO                receive_w_tmo
#define  RPTE_OSESOCKET                           socket
#define  RPTE_IOCTL                               ioctl
#define  RPTE_STRCPY               strcpy
#define  ERR                       -1

#define O_NONBLOCK                                FIONBIO 
#define RPTE_INET_RESP_WAITTIME     50

extern PROCESS ose_inet_;
INT4   gi4RpteIfFd;

/* Static Function Declarations Begins Here */

static INT1 rpteGetGwInfo (UINT4 ,UINT4, UINT4 *);
static INT1 rpteGetIfNumFromIfName (UINT1 *, INT4 *);
static UINT4 rpteGetIfIpAddrFromIfName(UINT1 *);

/* Static Function Declarations Ends Here */

#endif /*__RPTEOSE_H__*/
