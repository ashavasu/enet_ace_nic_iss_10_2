
/********************************************************************
 *
 * $Id: rptedefs.h,v 1.44 2018/01/03 11:31:22 siva Exp $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptedefs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the constant  
 *                             definitions declared and used for RSVP-TE 
 *                             functionality. 
 *---------------------------------------------------------------------------*/

#ifndef _RPTEDEFS_H
#define _RPTEDEFS_H

#define ERR_SENT -1
#define LSHIFT_4BITS     4
#define RSHIFT_4BITS     4
#define RSHIFT_8BITS     8
#define RSHIFT_16BITS   16

#define RPTE_REFRESH_MSG_YES   1
#define RPTE_REFRESH_MSG_NO    0

/* RAW_SOCK */
#define RSVP_MAX_PKT_SIZE 300

#define IP_ADDR_LEN           32
#define SOCK_SEM_NAME         "SC1"
#define SOCK_SEM_NAME_LEN     4


#define RPTE_PKTMAP_IP_ALLOC 0xf0
#define RPTE_PKTMAP_RSVP_ALLOC 0x0f
#define RPTE_PKTMAP_NO_ALLOC 0x00

#define DISTINCT_RESERVATIONS 0x08
#define SHARED_RESERVATIONS   0x10

#define WILDCARD 0x01
#define EXPLICIT 0x02

#define RPTE_WF 0x11
#define RPTE_FF 0x0A
#define RPTE_SE 0x12
#define STYLE_MASK 0x1F

#define IP  1
#define INIT_RA_OPT(x) ((x)->u4Opt = OSIX_HTONL(0x94040000))
#define MAC_HEADER_LENGTH 14

#define GT 0x00
#define LT 0x01
#define EQ 0x02
#define UC 0x03

#define UDP_READ_PORT 1698
#define UDP_WRITE_PORT 1699

#define E_POLICE_FLAG 0x01

#define DEFAULT_TOS 0

#define RPTE_MIN_UINT4_VALUE            0x00000000
#define RPTE_MAX_UINT4_VALUE            0xffffffff
#define RPTE_MIN_MCAST_ADDR_VALUE       0xe0000000
#define RPTE_MAX_MCAST_ADDR_VALUE       0xe00000ff

#define RPTE_INVALID_HANDLE             0xffffffff

#define RSVP_VERSION 0x01
#define PATH_MSG        01
#define RESV_MSG        02
#define PATHERR_MSG     03
#define RESVERR_MSG     04
#define PATHTEAR_MSG    05
#define RESVTEAR_MSG    06
#define RESVCONF_MSG    07

#define BUNDLE_MSG      12
#define ACK_MSG         13
#define HELLO_MSG       20
#define DISCARD_MSG     50
#define SREFRESH_MSG    15
#define NOTIFY_MSG      21
#define RECOVERY_PATH_MSG 30

#define DEFAULT_REFRESH_BLOCK_MULTIPLE  2
#define DEFAULT_REFRESH_MULTIPLE        3
#define DEFAULT_TTL                     64

#define DEFAULT_REFRESH_INTERVAL        30000    /* In milli Seconds */
#define DEFAULT_CONV_TO_SECONDS         1000    /* should be modif */

/* 1 STUP  = 100 units = 1000msecs
 *             1 uni   = 1000/100 = 10msecs
 * 1 second duration is divided into 100 units and
 * Therefore, 1 unit = 10msecs  */
#define NO_OF_MSECS_PER_UNIT           10

#define GRACE_VALUE                     0.5 
#define GRACE_MULTIPLE                  1.5 

#define HELLO_REFRESH_INTERVAL        10000    /* Ideally 10000 milli Seconds */

#define SOCK_POLL_INTERVAL            5    /* In Seconds */

#define DEFAULT_ROUTE_DELAY             2    /* In Seconds */

/* Maximum Table Sizes */
#define MAXIMUM_NBR_TABLE_SIZE               500
#define MAXIMUM_SESSION_TABLE_SIZE           500
#define MAXIMUM_SENDER_TABLE_SIZE            500
#define MAXIMUM_RSB_ARRAY_SIZE               500
#define MAXIMUM_RSB_CMN_HDR_ARRAY_SIZE       500
#define MAXIMUM_TCSB_ARRAY_SIZE              500
#define MAXIMUM_TCSB_CMN_HDR_ARRAY_SIZE      500
#define MAXIMUM_RESV_FWD_ARRAY_SIZE          500
#define MAXIMUM_RESV_FWD_CMN_HDR_ARRAY_SIZE  500
#define MAXIMUM_FLOW_DESCR_POOL_SIZE         500
#define MAXIMUM_LIH_VALUE                    0x7FFFFFFF

/* Default Table Sizes */
#define RSVP_GBL_STATUS                       1

/* Max Ifaces for WF style */
#define RSVP_MAX_INTERFACES  4
#define RSVP_MAX_SESSIONS    2

/* Maximum number of interfaces being derived */
#define RSVP_GBL_MAX_INTERFACES RSVPTE_MAX_IFACES(gpRsvpTeGblInfo)

/* Maximum number of Neighbours being derived */
#define RSVP_GBL_MAX_NEIGHBOURS RSVPTE_MAX_NEIGHBOURS(gpRsvpTeGblInfo)
/* The Neighbour Table Size is derived as ((MaxIfaces) * (MaxNbrs)) */
#define RSVP_GBL_MAX_NBR_TABLE_SIZE \
   ((RSVP_GBL_MAX_INTERFACES)*(RSVP_GBL_MAX_NEIGHBOURS))

/* All these numbers are derived from the single variable MAX_TUNNELS */
/* RSVP-TE does not use rsvp session table hence a min value defined */
#define RSVP_GBL_SESSION_TABLE_SIZE    RSVP_MAX_SESSIONS
/* Each Tnl is from one Sndr, so no of Sndr equals to no of Tnls */
#define RSVP_GBL_MAX_SENDER_TABLE_SIZE     RSVPTE_MAX_TNLS(gpRsvpTeGblInfo)
/* There can be Two flow spec for the given Session */
#define RSVP_GBL_FLOW_DESCR_POOL_SIZE  ((RSVP_MAX_SESSIONS)*2)


/* There can be Two flow spec for the given Tnl */

#define RSVP_HDR_VER_MASK 0xFF

#define CLASS_NUM_MASK 0xFF00
#define CLASS_TYPE_MASK 0x00FF

#define CLASS_NUM_AND_VALUE 0xC0

#define CLASS_NUM_UNKNOWN_VALUE1 0x00
#define CLASS_NUM_UNKNOWN_VALUE2 0x40

#define CLASS_NUM_IG_VALUE       0x80
#define CLASS_NUM_STORE_VALUE    0xC0

#define IPV4_NULL_CLASS 0
#define IPV4_NULL_CLASS_NUM_TYPE 0x0000

#define IPV4_SESSION_CLASS 1
#define IPV4_SESSION_CTYPE 1
#define IPV4_SESSION_CLASS_NUM_TYPE 0x0101

#define IPV4_RSVP_HOP_CLASS 3
#define IPV4_RSVP_HOP_CTYPE 1
#define IPV4_RSVP_HOP_CLASS_NUM_TYPE 0x0301
#define IPV4_IF_ID_RSVP_HOP_CLASS          3
#define IPV4_IF_ID_RSVP_HOP_CTYPE          3
#define IPV4_IF_ID_RSVP_HOP_CLASS_NUM_TYPE 0x0303

#define IPV4_IF_ID_SUB_OBJ_RSVP_TYPE          3
#define IPV4_IF_ID_NUM_SUB_OBJ_RSVP_TYPE      1
#define IPV4_IF_ID_SUB_OBJ_RSVP_LENGTH        24
#define IPV4_IF_ID_NUM_SUB_OBJ_RSVP_LENGTH     20

#define IPV4_INTEGRITY_CLASS 4
#define IPV4_INTEGRITY_CTYPE 1
#define IPV4_INTEGRITY_CLASS_NUM_TYPE 0x0401

#define IPV4_TIME_VALUES_CLASS 5
#define IPV4_TIME_VALUES_CTYPE 1
#define IPV4_TIME_VALUES_CLASS_NUM_TYPE 0x0501

#define IPV4_ADSPEC_CLS_SRV_SPEC 0x05000000

#define IPV4_ERROR_SPEC_CLASS 6
#define IPV4_ERROR_SPEC_CTYPE 1
#define IPV4_ERROR_SPEC_CLASS_NUM_TYPE 0x0601

#define IPV4_IF_ID_ERROR_SPEC_CLASS 6
#define IPV4_IF_ID_ERROR_SPEC_CTYPE 3
#define IPV4_IF_ID_ERROR_SPEC_CLASS_NUM_TYPE 0x0603
#define IPV4_IF_ID_ERROR_OBJ_LEN  24
#define IPV4_IF_ID_ERROR_NUM_OBJ_LEN  20

#define IPV4_SCOPE_CLASS 7
#define IPV4_SCOPE_CTYPE 1
#define IPV4_SCOPE_CLASS_NUM_TYPE 0x0701

#define IPV4_STYLE_CLASS 8
#define IPV4_STYLE_CTYPE 1
#define IPV4_STYLE_CLASS_NUM_TYPE 0x0801

#define IPV4_FLOW_SPEC_CLASS 9
#define IPV4_FLOW_SPEC_CTYPE 2
#define IPV4_FLOW_SPEC_CLASS_NUM_TYPE 0x0902

#define IPV4_FILTER_SPEC_CLASS 10
#define IPV4_FILTER_SPEC_CTYPE 1
#define IPV4_FILTER_SPEC_CLASS_NUM_TYPE 0x0A01

#define IPV4_SENDER_TEMPLATE_CLASS 11
#define IPV4_SENDER_TEMPLATE_CTYPE 1
#define IPV4_SENDER_TEMPLATE_CLASS_NUM_TYPE 0x0B01

#define IPV4_SENDER_TSPEC_CLASS 12
#define IPV4_SENDER_TSPEC_CTYPE 2
#define IPV4_SENDER_TSPEC_CLASS_NUM_TYPE 0x0C02

#define IPV4_ADSPEC_CLASS 13
#define IPV4_ADSPEC_CTYPE 2
#define IPV4_ADSPEC_CLASS_NUM_TYPE 0x0D02

#define IPV4_POLICY_DATA_CLASS 14
#define IPV4_POLICY_DATA_CTYPE 1
#define IPV4_POLICY_DATA_CLASS_NUM_TYPE 0x0E01

#define IPV4_ADSPEC_WITH_GS_AND_CLS_MSG_HDR_LEN    OSIX_HTONS(0x0014)

#define IPV4_RESV_CONFIRM_CLASS 15
#define IPV4_RESV_CONFIRM_CTYPE 1
#define IPV4_RESV_CONFIRM_CLASS_NUM_TYPE 0x0F01

#define SESSION_HASH_TABLE_SIZE 0x100

#define ATM_MERGE_SUPPRT_CMP_MASK  0x0FFF
#define ATM_MERGE_SUPPRT_ADD_MASK  0x8000
#define ATM_MERGE_SUPPRT           1

/* Error Code Values in ERROR_SPEC */
#define CONFIRMATION                      0
#define ADMISSION_CONTROL_FAILURE         1
#define POLICY_CONTROL_FAILURE            2
#define NO_PATH_INFO_FOR_RESV_MSG         3
#define NO_SENDER_INFO_FOR_RESV_MSG       4
#define CONFLICTING_RESV_STYLE            5
#define UNKNOWN_RESV_STYLE                6
#define CONFLICTING_DST_PORT              7
#define AMBIGUOUS_PATH                    8
#define AMBIGUOS_FILTER_SPEC              9
#define SERVICE_PREEMPTED                12
#define UNKNOWN_OBJECT_CLASS             13
#define UNKNOWN_OBJECT_CTYPE             14
#define TRAFFIC_CONTROL_ERROR            21
#define TRAFFIC_CONTROL_SYSTEM_ERROR     22
#define RSVP_SYSTEM_ERROR                23

#define DEFAULT_ERROR_VALUE              0x00
#define ERROR_SPEC_MASK                  0xFF00

#define SNMP_TIMER_VALUE  30    /* Seconds */

/* Textual Conventions for RsvpEncapsulation */
#define IP_ENCAP   0x01
#define UDP_ENCAP  0x02
#define BOTH_ENCAP 0x03


#define RPTE_PH_RRO_BOFF_TIMER  0x08
#define RPTE_PATH_REFRESH 0x09
#define RPTE_ROUTE_DELAY  0x0a
#define RPTE_PSB_SNMP_WAIT    0x0b
#define RPTE_RESV_REFRESH     0x0c
#define RPTE_BSB_DELETE       0x0d
#define RPTE_RH_RRO_BOFF_TIMER 0x0e
#define RPTE_MAX_WAIT_TIMER 0x0f
#define RPTE_HELLO_REFRESH  0x10
#define RPTE_SOCK_POLL  0x11
#define RPTE_PATHERR               0x12
#define RPTE_RESVERR               0x13
#define RPTE_PATHTEAR              0x14
#define RPTE_RESVTEAR              0x15
#define RPTE_SREFRESH              0x16
#define RPTE_PATH_INCOMING         0x17
#define RPTE_RESV_INCOMING         0x18
#define RPTE_CSPF_INTERVAL         0x19
#define RPTE_MAX_GBL_REVERT_TIMER  0x1a
#define RPTE_GRACEFUL_DEL_EXPIRE   0x1b
#define RPTE_NOTIFY_GROUPING_TMR_EXPIRE  0x1c
#define RPTE_NOTIFY_RETRY_TMR_EXPIRE     0x1d
#define RPTE_GR_NBR_RESTART_TIMER  0x1e
#define RPTE_GR_RECOVERY_TIMER     0x1f
#define RPTE_GR_NBR_RECOVERY_TIMER 0x20
#define RPTE_GR_RECOVERY_PATH      0x21
#define RPTE_GR_RECOVERY_PATH_REFRESH 0x22
#define RPTE_REOPTIMIZE_INTERVAL 0x23
#define RPTE_ERO_CACHE_INTERVAL  0x24
#define RPTE_LINK_UP_WAIT_INTERVAL 0x25
#define RPTE_EVENT_TO_L2VPN     0x26
 
#define RPTE_EVENT_TO_L2VPN_TIMEOUT 10 /*millisecond*/

#define RPTE_LINK_UP_WAIT_TIME 300

/* ErrorSpec Flags */
#define IN_PLACE_FLAG_MASK  0x01
#define IN_PLACE_FLAG       0x01

#define NOT_GUILTY_FLAG_MASK  0x02
#define NOT_GUILTY_FLAG       0x02

#define CHECK_SUM_MASK        0xFFFF

/* Textual Convention for SessionType */
#define IPv4 0x01
#define IPv6 0x02

/* Textual Convention for Protocol */
#define UDP 17
#define TCP  6

/* SNMP -> RSVP Request Id's */
#define STATIC_PSB_CREATE 0x01
#define STATIC_RSB_CREATE 0x02
#define PSB_SNMP_TMR_START    0x03
#define PSB_SNMP_TMR_STOP     0x04
#define RSB_SNMP_TMR_START    0x05
#define RSB_SNMP_TMR_STOP     0x06
#define SNMP_PATH_REFRESH     0x07

#define RSVP_MEM_POOL_BLOCK_SIZE    64
#define RSVP_MEM_POOL_NO_OF_BLOCKS  512
#define SNMP_FLAG  1
#define TC_FLAG    2

#define IF_LBL_SPACE_SET      0x0001
#define IF_LBL_TYPE_SET       0x0002
#define IF_LBL_MINVPI_SET     0x0004
#define IF_LBL_MINVCI_SET     0x0008
#define IF_LBL_MAXVPI_SET     0x0010
#define IF_LBL_MAXVCI_SET     0x0020
#define IF_LBL_ATM_INFO_SET   0x0040
#define IF_ATM_MERGE_CAP_SET  0x0080
#define IF_ATM_VC_DIR_SET     0x0100

#define IF_LBL_SPACE_GEN      0x01
#define IF_LBL_SPACE_ATM      0x02

#define IMPLICIT_NULL_LABEL       3

#define RSVPTE_ZERO               0
#define RPTE_ZERO                 RSVPTE_ZERO
#define RSVPTE_ONE                1
#define RPTE_ONE                  RSVPTE_ONE
#define RSVPTE_TWO                2
#define RPTE_TWO                  RSVPTE_TWO
#define RSVPTE_THREE              3
#define RPTE_THREE                RSVPTE_THREE
#define RSVPTE_FOUR               4
#define RPTE_FOUR                 RSVPTE_FOUR
#define RSVPTE_EIGHT              8
#define RSVPTE_FIVE_TICKS         5
#define RSVPTE_IP_RTR_ALERT_LEN   4

#define RPTE_TRUE                 1
#define RPTE_FALSE                0
#define RPTE_EQUAL                0
#define RPTE_NOT_EQUAL           (~0)
#define RPTE_YES                  1
#define RPTE_NO                   0
#define RPTE_ILLEGAL              2

#define RSVPTE_HASH_SIZE          128

#define RPTE_ENQ_MSG_SIZE         4
#define RPTE_DEF_BUF_SIZE         16

#define RSVPTE_IPV4ADR_LEN      4
#define RSVPTE_IPV6ADR_LEN      16
#define RSVPTE_RTRID_LENGTH     4
#define RSVPTE_NULL_IPV4ADDR    0x00000000
#define RSVPTE_MCAST_ADDR_MASK  0xF0000000
#define RSVPTE_MCAST_IPV4ADDR   0xE0000000


/* 9 mem pools allocated */
#define RSVPTE_MAX_POOLS      22


#define RSVPTE_APPL_ID        1

/* SNMP related definitions */
#define RPTE_SNMP_TRUE        1
#define RPTE_SNMP_FALSE       2

#define RPTE_TNL_DIR_IN       1
#define RPTE_TNL_DIR_OUT      2
#define RPTE_TNL_DIR_INOUT    3

#define RPTE_TNL_IFINDEX_MINVAL      1
#define RPTE_TNL_IFINDEX_MAXVAL      65535    /* Currently marked to 64K to be
                                               changed if required. */

#define RPTE_IF_MIN_MTU_SIZE       1500    /* Min MTU size on this Iface */
#define RPTE_DEF_PKT_SIZE          RPTE_IF_MIN_MTU_SIZE
#define RPTE_IF_MAX_MTU_SIZE       4096    /* Max MTU size on this Iface */

#define RPTE_IF_LINK_ATTR_DEF  0x01    /* Link Attribute default defined for UT */

#define RPTE_HOLD_PRIO_RANGE       8

/*  Minimum tunnel priority  for a RSVP  tunnel */
#define RSVP_TNL_MIN_PRIO          7

#define RPTE_SSN_FLAG_MASK         0x1f
#define RPTE_SSN_LCL_PRTN_BIT      0x01
#define RPTE_SSN_INN_REROUT_BIT    0x04

/* Session Attr Mib defs */
#define RPTE_SSN_LCL_PRTN_FLAG      0x03
#define RPTE_SSN_MRG_PRMT_FLAG      0x01
#define RPTE_SSN_INN_REROUT_FLAG    0x00

#define RPTE_ADMIN_UP              1
#define RPTE_ADMIN_DOWN            2
#define RPTE_ADMIN_TSTNG           3
#define RPTE_ADMIN_UP_IN_PRGRS     4
#define RPTE_ADMIN_DOWN_IN_PRGRS   5

#define RPTE_OPER_UP               1
#define RPTE_OPER_DOWN             2
#define RPTE_OPER_TSTNG            3
#define RPTE_OPER_UNKWN            4
#define RPTE_OPER_DRMNT            5
#define RPTE_OPER_NOTPRES          6
#define RPTE_OPER_LLDWN            7

/* Reoptimization Node state */
#define RPTE_MID_POINT_NODE     1
#define RPTE_HEAD_MID_POINT_NODE   2

#define RPTE_REOPT_MANUAL_TRIGGER  1
#define RPTE_REOPT_TIMER_EXPIRY    2
#define RPTE_REOPT_LINK_UP         4
#define RPTE_REOPT_LINK_MAINTENANCE  8
#define RPTE_REOPT_NODE_MAINTENANCE  16
#define RPTE_REOPT_PREFER_PATH_EXIST 32

/* SNMP Row Status Types */
#define RPTE_ACTIVE                1
#define RPTE_NOTINSERVICE          2
#define RPTE_NOTREADY              3
#define RPTE_CREATEANDGO           4
#define RPTE_CREATEANDWAIT         5
#define RPTE_DESTROY               6

/* Valid Label space types */
#define RPTE_PERPLATFORM           1
#define RPTE_PERINTERFACE          2

/* Valid Label Types */
#define RPTE_LBL_GENERIC           1
#define RPTE_LBL_ATM               2

#define RPTE_ADMIN_STATUS_CAPABLE   1
#define RPTE_ADMIN_STATUS_INCAPABLE 2

#define PATH_STATE_REMOVE_ENABLE   1
#define PATH_STATE_REMOVE_DISABLE  2

/* VC Direction */
#define RPTE_VC_BI_DIR             0
#define RPTE_VC_UNI_DIR_ODD        1
#define RPTE_VC_UNI_DIR_EVEN       2

/* ATM VC Merge Capability */
#define RPTE_NO_MRG                0
#define RPTE_VP_MRG                1
#define RPTE_VC_MRG                2

/* FRR MLIB_UPDATE status */
#define RPTE_FRR_MLIB_UPDATE_TRUE      1
#define RPTE_FRR_MLIB_UPDATE_FALSE     2

/* Constants associated with internal events */
#define RPTE_TNL_TE_EVENT             0x0001
/*#define RPTE_TNL_ADMIN_DOWN_EVENT     0x0002 */
#define RPTE_TE_EVENT                 0x0003
#define RPTE_TNL_SETUP                0x0004
#define RPTE_TNL_DESTROY              0x0005
#define RPTE_TNL_L2VPN_EVENT          0x0006
#define RPTE_TNL_MANUAL_REOPT_EVENT   0x0007
#define RPTE_TNL_L3VPN_EVENT          0x0002
#define RPTE_ADMIN_UP_SCCS_EVENT      0x00800000
#define RPTE_ADMIN_UP_FAIL_EVENT      0x00400000

/* IF - MIN VPI values */
#define RPTE_MINVPI_MINVAL  0
#define RPTE_MINVPI_DEFVAL  0
#define RPTE_MINVPI_MAXVAL  4095    /* as VPI max is 12 bits */

/* IF - MIN VCI values */
#define RPTE_MINVCI_MINVAL  0
#define RPTE_MINVCI_DEFVAL  33
#define RPTE_MINVCI_MAXVAL  65535    /* as VCI max is 16 bits */

/* IF - MAX VPI values */
#define RPTE_MAXVPI_MINVAL  0
#define RPTE_MAXVPI_DEFVAL  0
#define RPTE_MAXVPI_MAXVAL  4095    /* as VPI max is 12 bits */

/* IF - MAX VCI values */
#define RPTE_MAXVCI_MINVAL  0
#define RPTE_MAXVCI_DEFVAL  160    /* 160 for 128 VC's  */
#define RPTE_MAXVCI_MAXVAL  65535    /* as VCI max is 16 bits */
/* Valid ER Hop types */
#define RPTE_ERHOP_STRICT           1
#define RPTE_ERHOP_LOOSE            2
#define RPTE_ERHOP_TYPE_MASK        0x80
#define RSVPTE_ERHOP_ADDR_SET_VALUE 0x7f
#define RPTE_HOST_NETMASK           0xffffffff


/* Configured constants for the mem blocks to be allocated */
#define RSVPTE_MAX_IFACES_MIN_VAL    1
#define RSVPTE_MAX_IFACES_MAX_VAL    65535

/* Configured constants for the mem blocks to be allocated */
#define RSVPTE_MAX_NEIGHBOURS_MIN_VAL  1
#define RSVPTE_MAX_NEIGHBOURS_MAX_VAL  65535

#define RSVPTE_TNLS_MIN_VAL    1
#define RSVPTE_TNLS_MAX_VAL    65535

#define RSVPTE_TNLERHOP_MIN_VAL  1
#define RSVPTE_TNLERHOP_MAX_VAL  TE_MAX_HOP_PER_TNL   /* Set to default value in TE */
#define RSVPTE_TNLERHOP_DEF_VAL  TE_MAX_HOP_PER_TNL    /* To be changed as required */

#define RSVPTE_TNLARHOP_MIN_VAL  1
#define RSVPTE_TNLARHOP_MAX_VAL  65535
#define RSVPTE_TNLARHOP_DEF_VAL  8    /* To be changed as required */

#define RSVPTE_REFRESH_MULTIPLE_MIN_VAL 1
#define RSVPTE_REFRESH_MULTIPLE_MAX_VAL 8

#define RSVPTE_LINK_ATTR_MIN_VAL 1
#define RSVPTE_LINK_ATTR_MAX_VAL 4294967295U

#define RSVPTE_IF_SENDTTL_MIN_VAL 1
#define RSVPTE_IF_SENDTTL_MAX_VAL 255

#define RSVPTE_REFRESH_INTRVL_MIN_VAL 0
#define RSVPTE_REFRESH_INTRVL_MAX_VAL 8640000

#define RSVPTE_ROUTE_DELAY_MIN_VAL    0
#define RSVPTE_ROUTE_DELAY_MAX_VAL    360000


#define RSVPTE_LBL_SUBOBJ_MINVAL  1
#define RSVPTE_LBL_SUBOBJ_MAXVAL  65535

#define RSVPTE_NEW_SUBOBJ_MINVAL  1
#define RSVPTE_NEW_SUBOBJ_MAXVAL  65535

#define RSVPTE_TNL_RSRC_MINVAL  1
#define RSVPTE_TNL_RSRC_MAXVAL  1024
#define RSVPTE_TNL_RSRC_DEFVAL  16    /* To be changed as required */

#define RSVPTE_MAX_IPROUTES     32    /* To be changed as required */


#define RSVPTE_DEF_MAX_64BIT_TRIE_SIZE    64
#define RSVPTE_DEF_MAX_32BIT_TRIE_SIZE    64
#define RSVPTE_MAX_32BIT_TRIE_SIZE_VAL    RPTE_MAX_UINT4_VALUE
#define RSVPTE_MAX_64BIT_TRIE_SIZE_VAL    RPTE_MAX_UINT4_VALUE
#define RSVPTE_BNDL_TNL_THRES_MAX_VAL     500 
#define RSVPTE_MAX_BUNDLE_SIZE            500
#define RSVPTE_HELLO_INTERVAL_MIN_VAL     1000 /* 1000 milli-seconds */
#define RSVPTE_HELLO_INTERVAL_MAX_VAL     30000 /* 30 seconds */


/* default minimum RRO back Off time value in seconds */
#define RPTE_DEF_BACK_OFF_TIME  15

/* RRO back off max retry count */
#define RPTE_MAX_RETRY          3

/* Hello Related defs */
#define STATUS_UNKNOWN          0
#define HELLO_SUPPRT            1
#define HELLO_NOT_SUPPRT        2
#define HELLO_SUPPRT_RESET      3
#define UPSTREAM                1
#define DOWNSTREAM              2
#define DEF_SRC_INSTANCE        1
#define DEF_DST_INSTANCE        0

#define HELLO_MAX_RETRY         3.5
#define RESET_MAX_WAIT          3

/* RPTE Internal Events */
#define RPTE_INTERNAL_EVENT       0x00010000
#define RPTE_LSP_SETUP_EVENT      0x00000000
#define RPTE_LSP_DESTROY_EVENT    0x00000001

/* RPTE Class and Num type macros */
#define RPTE_LBL_CLASS  16
#define RPTE_LBL_CTYPE  1
#define RPTE_LBL_CLASS_NUM_TYPE 0x1001

#define RPTE_GLBL_CLASS 16
#define RPTE_GLBL_CTYPE 2
/* denotes Class-Num - 16 and C-Type - 2 in Generalized label
 * object */
#define RPTE_GEN_LBL_CLASS_NUM_TYPE 0x1002

#define RPTE_UPSTR_GLBL_CLASS  35
#define RPTE_UPSTR_GLBL_CTYPE  2
#define RPTE_UPSTR_GLBL_CLASS_NUM_TYPE 0x2302

#define RPTE_LBL_REQ_CLASS  19
#define RPTE_LBL_REQ_CTYPE  1
#define RPTE_LBL_REQ_CLASS_NUM_TYPE 0x1301

#define RPTE_LBL_REQ_ATM_RNG_CLASS  19
#define RPTE_LBL_REQ_ATM_RNG_CTYPE  2
#define RPTE_LBL_REQ_ATM_RNG_CLASS_NUM_TYPE 0x1302

#define RPTE_GLBL_REQ_CLASS  19
#define RPTE_GLBL_REQ_CTYPE  4
#define RPTE_GLBL_REQ_CLASS_NUM_TYPE 0x1304

#define RPTE_ERO_CLASS  20
#define RPTE_ERO_CTYPE  1
#define RPTE_ERO_CLASS_NUM_TYPE 0x1401

#define RPTE_RRO_CLASS  21
#define RPTE_RRO_CTYPE  1
#define RPTE_RRO_CLASS_NUM_TYPE 0x1501

#define RPTE_HELLO_CLASS      22
#define RPTE_HELLO_REQ_CTYPE  1
#define RPTE_HELLO_ACK_CTYPE  2
#define RPTE_HELLO_REQ_CLASS_NUM_TYPE 0x1601
#define RPTE_HELLO_ACK_CLASS_NUM_TYPE 0x1602

#define IPV4_RPTE_SSN_CLASS  1
#define IPV4_RPTE_SSN_CTYPE  7
#define IPV4_RPTE_SSN_CLASS_NUM_TYPE 0x0107

#define IPV6_RPTE_SSN_CLASS  1
#define IPV6_RPTE_SSN_CTYPE  8
#define IPV6_RPTE_SSN_CLASS_NUM_TYPE 0x0108

#define IPV4_RPTE_FRR_CLASS  205
#define IPV4_RPTE_FRR_CTYPE  1
#define IPV4_RPTE_FRR_CLASS_NUM_TYPE 0xcd01

#define IPV4_RPTE_FRR_DETOUR_CLASS  63
#define IPV4_RPTE_FRR_DETOUR_CTYPE  7
#define IPV4_RPTE_FRR_DETOUR_CLASS_NUM_TYPE 0x3f07

#define IPV4_RPTE_SNDR_TEMP_CLASS  11
#define IPV4_RPTE_SNDR_TEMP_CTYPE  7
#define IPV4_RPTE_SNDR_TEMP_CLASS_NUM_TYPE 0x0b07

#define IPV6_RPTE_SNDR_TEMP_CLASS  11
#define IPV6_RPTE_SNDR_TEMP_CTYPE  8
#define IPV6_RPTE_SNDR_TEMP_CLASS_NUM_TYPE 0x0b08

#define IPV4_RPTE_FLTR_SPEC_CLASS  10
#define IPV4_RPTE_FLTR_SPEC_CTYPE  7
#define IPV4_RPTE_FLTR_SPEC_CLASS_NUM_TYPE 0x0a07

#define IPV6_RPTE_FLTR_SPEC_CLASS  10
#define IPV6_RPTE_FLTR_SPEC_CTYPE  8
#define IPV6_RPTE_FLTR_SPEC_CLASS_NUM_TYPE 0x0a08

#define RPTE_SSN_ATTR_CLASS  207
#define RPTE_SSN_ATTR_CTYPE  7
#define RPTE_SSN_ATTR_CLASS_NUM_TYPE 0xcf07
#define RPTE_RA_SSN_ATTR_CTYPE  1
#define RPTE_RA_SSN_ATTR_CLASS_NUM_TYPE 0xcf01

#define RPTE_COS_SNDR_TSPEC_CLASS  12
#define RPTE_COS_SNDR_TSPEC_CTYPE  3
#define RPTE_COS_SNDR_TSPEC_CLASS_NUM_TYPE 0x0c03

#define RPTE_COS_FLOW_SPEC_CLASS  9
#define RPTE_COS_FLOW_SPEC_CTYPE  3
#define RPTE_COS_FLOW_SPEC_CLASS_NUM_TYPE 0x0903

#define RPTE_HELLO_REQUEST_CLASS 22
#define RPTE_HELLO_REQUEST_CTYPE 1
#define RPTE_HELLO_REQUEST_CLASS_NUM_TYPE 0x1601

#define RPTE_HELLO_ACK_CLASS 22

#define RPTE_MESSAGE_ID_CLASS                23
#define RPTE_MESSAGE_ID_CTYPE                1
#define RPTE_MESSAGE_ID_CLASS_NUM_TYPE       0x1701

#define RPTE_MESSAGE_ID_ACK_CLASS            24
#define RPTE_MESSAGE_ID_ACK_CTYPE            1
#define RPTE_MESSAGE_ID_NACK_CTYPE           2
#define RPTE_MESSAGE_ID_ACK_CLASS_NUM_TYPE   0x1801
#define RPTE_MESSAGE_ID_NACK_CLASS_NUM_TYPE  0x1802

#define RPTE_MESSAGE_ID_LIST_CLASS           25
#define RPTE_MESSAGE_ID_LIST_CTYPE           1
#define RPTE_MESSAGE_ID_LIST_CLASS_NUM_TYPE  0x1901

/* Error codes and values used in RSVPTE */

#define RPTE_ROUTE_PROB   24
#define RPTE_NOTIFY       25

#define RPTE_BAD_EXPLICT_ROUTE_OBJ  1
#define RPTE_BAD_STRICT_NODE        2
#define RPTE_BAD_LOOSE_NODE         3
#define RPTE_BAD_INIT_SUB_OBJ       4
#define RPTE_NO_ROUTE_AVAIL         5
#define RPTE_UNACTBL_LABEL_VALUE    6
#define RPTE_RRO_IND_ROUTE_LOOP     7
#define RPTE_NON_RSVP_RTR           8
#define RPTE_LBL_ALLOC_FAIL         9
#define RPTE_UNSUPPORTED_L3PID      10
#define RPTE_LABEL_SET              11
#define RPTE_UNSUPPORTED_SWITCHING  12   /* As per GMPLS RFC 3473 section 13.1 */
#define RPTE_UNSUPPORTED_ENCODING   14   /* As per GMPLS RFC 3473 section 13.1 */

/* Error value for NOTIFY */
#define RPTE_RRO_TOO_LARGE          1
#define RPTE_RRO_NOTIFY             2
#define RPTE_TNL_LOCAL_REPAIR       3
#define RPTE_CONTROL_CHANNEL_ACTIVE 4
#define RPTE_CONTROL_CHANNEL_DEGRADE 5

#define RPTE_LSP_FAILURE            9
#define RPTE_LSP_RECOVERED          10
#define RPTE_LSP_LOCALLY_FAILED     11

/*Error Value Introduced For LSP Reoptimization*/

#define RPTE_PREFERABLE_PATH_EXIST 6
#define RPTE_LOCAL_LINK_MAINTENANCE 7
#define RPTE_LOCAL_NODE_MAINTENANCE 8 
  


/* RRO Error flag values used for RRO Err procesing */
#define RPTE_RRO_ERR_PATH_SET    0x01    /* set when RRO notify Err is Rcvd */
#define RPTE_RRO_ERR_PATH_RESET  0x02    /* default RRO No Err value */
#define RPTE_RRO_ERR_RESV_SET    0x04    /* set when RRO notify Err at Resv msg */
#define RPTE_RRO_ERR_RESV_RESET  0x08    /* default RRO No Err value */

#define RPTE_RRO_ERR_PATH_RESV_RESET 0x0a    /* default Path and Resv RRO 
                                               err reset */

#define RPTE_REOPTIMIZE_TIMER_STARTED 1
#define RPTE_REOPTIMIZE_TIMER_NOT_STARTED 0

/* Reroute flag */
#define RPTE_SE_STYLE_DESIRED         0x04
/* Label Recording Flag */
#define RPTE_LBL_RECORDING_DESIRED    0x02
/* Local Protection Flag */
#define RPTE_LOCAL_PROTECT_DESIRED    0x01

/* Value used to indicate the RRO support is enabled/disabled */
#define RPTE_RRO_SPRTD              1

#define RPTE_DEF_LSR_ID                0x00000000
#define RPTE_LSR_ID_LEN                4
#define RPTE_STD_IPV4_ETHERTYPE        0x0800     /* Standard Ethertype L3Pid */
#define RPTE_GMPLS_ETHERNET_ETHERTYPE  0x0200     
#define RPTE_LOOP_BACK_ADDR            0x7f000001 /* Standard Loop Back address */

/* Label Space Type */
#define RPTE_ETHERNET             1
#define RPTE_ATM                  2
#define RPTE_FR                   3
#define RPTE_UNKNOWN_LBL_SPACE    4

/* ERO Subobject Definitions */
#define ERO_TYPE_LBL_OBJ          0x03

#define ERO_TYPE_IPV4_LEN         8
#define ERO_TYPE_LBL_LEN          8
#define ERO_TYPE_UNNUM_LEN        12

#define ERO_TYPE_IPV4_RSVD        0x00
#define ERO_TYPE_LBL_RSVD         0x00
#define ERO_TYPE_LBL_UBIT_RSVD    0x80
#define ERO_TYPE_UNNUM_RSVD       0x00

#define ERO_TYPE_STRICT_SET       0
#define ERO_TYPE_LOOSE_SET        0x80
#define ERO_TYPE_MASK             0x80 
#define ERO_TYPE_SET_VALUE        0x7f
#define RPTE_HOST_NETMASK         0xffffffff

/* RRO Subobject Definitions */
#define RRO_TYPE_LBL_OBJ          0x03

#define RRO_TYPE_IPV4_LEN         8
#define RRO_TYPE_LBL_LEN          8
#define RRO_TYPE_UNNUM_LEN        12

#define RRO_TYPE_UNNUM_RSVD       0x00

#define RRO_TYPE_MASK             0x80
#define RRO_TYPE_SET_VALUE        0x7f
#define RRO_TYPE_FLAG_MASK        0x01

/* Shim Header Length */
#define SHIM_HDR_LEN           4

/* Label length in bytes */
#define RPTE_LBL_LEN           4

/* Length of ERO and RRO */
#define IPV4_RRO_LEN         8
#define IPV4_ERO_LEN         8

/* IPv4 Addr Optional Length in RSVP */
#define RPTE_IPV4_OPT_LEN    4

/* ERO Route Pinning type */
#define RPTE_RT_PINNED       1

/* IPV4 Type Objects Length */
#define RPTE_IPV4_SSN_OBJ_LEN            16
#define RPTE_IPV4_RSVP_HOP_OBJ_LEN       12
#define RPTE_IPV4_STYLE_OBJ_LEN          8
#define RPTE_IPV4_FLTR_SPEC_OBJ_LEN      12
#define RPTE_IPV4_SNDR_TMP_OBJ_LEN       12
#define RPTE_IPV4_FRR_OBJ_LEN            24
#define RPTE_IPV4_FRR_MIN_DETOUR_LEN     12
#define RPTE_IPV4_ERR_SPEC_OBJ_LEN       12
#define RPTE_MIN_SSN_ATTR_OBJ_LEN        16
#define RPTE_RA_MIN_SSN_ATTR_OBJ_LEN     28
#define RPTE_MIN_RRO_OBJ_LEN             12
#define RPTE_MIN_ERO_OBJ_LEN             12
#define RPTE_MIN_LBL_OBJ_LEN             8

/* word boundary */
#define WORD_BNDRY         4
#define SSN_NAME_MIN_LEN   8
/* Max Lbl Size in Per Platform(Ethernet) case */
#define RPTE_MAX_GEN_LBL_SIZE     1048575    /* as defined in
                                               draft-ietf-mpls-rsvp-lsp-tunnel-05.txt, page no:16 */

#define RPTE_DATA_TX_UP         1
#define RPTE_DATA_TX_DOWN       2

/* FRR Node state related macros */
#define RPTE_FRR_UNPROTECTED_TNL 0
#define RPTE_FRR_PROTECTED_TNL   1
#define RPTE_FRR_BACKUP_TNL      2


#define RPTE_FRR_PROTECTED_PATH   1
#define RPTE_FRR_BACKUP_PATH      2

#define RPTE_FRR_PROT_MAKE_BEFORE_BREAK   0
#define RPTE_FRR_PROT_MAKE_AFTER_BREAK    1
    
#define RPTE_FRR_NODE_STATE_UNKNOWN   0x00
#define RPTE_FRR_NODE_STATE_HEADEND   0x01
#define RPTE_FRR_NODE_STATE_PLR       0x02
#define RPTE_FRR_NODE_STATE_PLRAWAIT  0x04
#define RPTE_FRR_NODE_STATE_MP        0x08
#define RPTE_FRR_NODE_STATE_DMP       0x10
#define RPTE_FRR_NODE_STATE_DNLOST    0x20
#define RPTE_FRR_NODE_STATE_UPLOST    0x40
#define RPTE_FRR_NODE_STATE_TAILEND   0x80
#define RPTE_FRR_NODE_STATE_PLR_MP       0x0A
#define RPTE_FRR_NODE_STATE_HEADEND_PLR  0x03

/* FRR Ssn Attr related macros */
#define RPTE_FRR_SSN_ATTR_LOCAL_PROT_FLAG  TE_FRR_SSN_ATTR_LOCAL_PROT_FLAG
#define RPTE_FRR_SSN_ATTR_LBL_RECORD_FLAG  TE_FRR_SSN_ATTR_LBL_RECORD_FLAG
#define RPTE_FRR_SSN_ATTR_SE_STYLE_FLAG    TE_FRR_SSN_ATTR_SE_STYLE_FLAG
#define RPTE_FRR_SSN_ATTR_BANDWIDTH_FLAG   TE_FRR_SSN_ATTR_BANDWIDTH_FLAG
#define RPTE_FRR_SSN_ATTR_NODE_PROT_FLAG   TE_FRR_SSN_ATTR_NODE_PROT_FLAG
#define RPTE_SSN_ATTR_PATH_REEVAL_REQ      TE_SSN_ATTR_PATH_REEVAL_REQ

#define RPTE_FRR_CSPF_NO_START            0x00  /* CSPF Computation has not started */
#define RPTE_FRR_CSPF_STARTED             0x01  /* CSPF Computation has started */
#define RPTE_FRR_CSPF_COMPUTED            0x02  /* CSPF Computation has been successful */
#define RPTE_FRR_CSPF_FAILED              0x04  /* CSPF Computation has 
                                                   failed for both link and node */
#define RPTE_FRR_CSPF_NODE_FAILED         0x08  /* CSPF Compuation has failed for Node alone */

/* Default values for TSPEC Obj - can be changed if required */
#define TSPEC_MSGHDR_LEN            0x0007
#define TSPEC_SRVHDR_SRV_ID         0x01
#define TSPEC_SRVHDR_LEN            0x0006
#define TSPEC_PARAMHDR              0x7f000014
#define TSPEC_PARAMHDR_ID           0x7f
#define TSPEC_PARAMHDR_FLG          0
#define TSPEC_PARAMHDR_LEN          0x0005
/* Default values for Adspec Obj - can be changed if required */
#define ADSPEC_MSGHDR_LEN            0x0019
#define ADSPEC_SRVHDR_SRV_ID         0x01
#define ADSPEC_SRVHDR_LEN            0x0008

#define ADSPEC_HC_PARAMHDR           0x04000001
#define ADSPEC_HC_PARAMHDR_ID        0x04
#define ADSPEC_HC_PARAMHDR_FLG       0
#define ADSPEC_HC_PARAMHDR_LEN       0x0001
#define ADSPEC_HC                    0x00000004

#define ADSPEC_PBW_PARAMHDR          0x06000001
#define ADSPEC_PBW_PARAMHDR_ID       0x06
#define ADSPEC_PBW_PARAMHDR_FLG      0
#define ADSPEC_PBW_PARAMHDR_LEN      0x0001
#define ADSPEC_PBW                   0x00001000

#define ADSPEC_PATH_LAT_PARAMHDR     0x08000001
#define ADSPEC_PATH_LAT_PARAMHDR_ID  0x08
#define ADSPEC_PATH_LAT_PARAMHDR_FLG 0
#define ADSPEC_PATH_LAT_PARAMHDR_LEN 0x0001
#define ADSPEC_MIN_PATH_LAT          0x00001000

#define ADSPEC_MTU_PARAMHDR          0x0a000001
#define ADSPEC_MTU_PARAMHDR_ID       0x0a
#define ADSPEC_MTU_PARAMHDR_FLG      0
#define ADSPEC_MTU_PARAMHDR_LEN      0x0001
#define ADSPEC_MTU                   0x000005dc

#define ADSPEC_CLS_SRVHDR_ID         0x05
#define ADSPEC_GS_SRVHDR_ID          0x02
#define ADSPEC_GS_SRVHDR_LEN         0x0008

#define ADSPEC_GS_CTOT_PARAM_ID      133
#define ADSPEC_GS_CTOT_PARAM_LEN     1

#define ADSPEC_GS_DTOT_PARAM_ID      134
#define ADSPEC_GS_DTOT_PARAM_LEN     1
#define ADSPEC_GS_DTOT_PARAM_DTOT    1

#define ADSPEC_GS_CSUM_PARAM_ID      135
#define ADSPEC_GS_CSUM_PARAM_LEN     1

#define ADSPEC_GS_DSUM_PARAM_ID      136
#define ADSPEC_GS_DSUM_PARAM_LEN     1

#define FLOWSPEC_CLS_HDR_NUMBER      5

#define RPTE_IF_ENTRY_NUM_INDICES 1

#define RPTE_TRIG_MSG             0x01
#define RPTE_REFRESH_MSG          0x02
#define RPTE_OUT_OF_ORDER         0x03
#define RPTE_EPOCH_CHANGE         0x04
#define RPTE_MSG_ID_NOT_IN_DB     0x05

#define RPTE_WORD_LOWER_3_BYTE_MASK   0x00ffffff
#define RPTE_WORD_UPPER_1_BYTE_MASK   0xff000000

#define RPTE_3_BYTE_SHIFT             24

#define RPTE_NONZERO                  1

#define RSVP_OBJ_HDR_LEN              4

#define RPTE_UN_INIT_EPOCH            0xffffffff

#define RSVP_HDR_FLAG_WITH_RR         0x01
#define RPTE_MSG_ID_ACK_DESIRED       0x01
#define RPTE_MSG_ID_RECOVERY_PATH     0x02

#define RPTE_IN_PATH_MSG_ID_PRESENT   0x01
#define RPTE_IN_RESV_MSG_ID_PRESENT   0x02
#define RPTE_OUT_PATH_MSG_ID_PRESENT  0x04
#define RPTE_OUT_RESV_MSG_ID_PRESENT  0x08
#define RPTE_USTR_MSG_ID_PRESENT      0x10
#define RPTE_DSTR_MSG_ID_PRESENT      0x20

#define RPTE_IN_PATH_MSG_ID_NOT_PRESENT   0xfe
#define RPTE_IN_RESV_MSG_ID_NOT_PRESENT   0xfd
#define RPTE_OUT_PATH_MSG_ID_NOT_PRESENT  0xfb
#define RPTE_OUT_RESV_MSG_ID_NOT_PRESENT  0xf7
#define RPTE_USTR_MSG_ID_NOT_PRESENT      0xee
#define RPTE_DSTR_MSG_ID_NOT_PRESENT      0xde

#define RPTE_APPLY_RR_TECHNIQUES          0x01
#define RPTE_AVOID_RR_TECHNIQUES          0x02


#define RPTE_MSG_ID_OBJ_LEN               12
#define RPTE_MSG_ID_FLAG_LEN              1
#define RPTE_FLAG_EPOCH_LEN               4
#define RPTE_MSG_ID_LEN                   4

#define RPTE_NORMAL_TIMER                 0x01
#define RPTE_EXP_BACK_OFF_TIMER           0x02
#define RPTE_TIMER_TYPE_UNKNOWN           0x03

#define RPTE_BACK_OFF_FACTOR              3

#define RPTE_LCL_MSG_ID_DB_PREFIX         32
#define RPTE_NBR_MSG_ID_DB_PREFIX         64
#define RPTE_NBR_MSG_ID_DB_NBR_PREFIX     32

#define RPTE_ROUNDOFF_VALUE               0.5
#define RPTE_BACKOFF_DELTA                1.732
#define RPTE_MAX_BACKOFF_TIME_VALUE       50

#define RPTE_ENABLED                      1
#define RPTE_DISABLED                     2

#define RPTE_IF_UP     CFA_IF_UP
#define RPTE_IF_DOWN   CFA_IF_DOWN

#define RPTE_RR_CAP_ENA 1
#define RPTE_RR_CAP_DIS 2

#define RPTE_RR_STATE_ENA  1
#define RPTE_RR_STATE_DIS  2


#define RPTE_RMD_CAP_ENA  1
#define RPTE_RMD_CAP_DIS  2

#define PATH    0
#define RESV    1
#define PATHERR 2
#define RESVERR 3
#define PATHTER 4
#define RESVTER 5

#define RPTE_RR_ENABLED       1
#define RPTE_RR_DISABLED      2

#define RPTE_MSGID_ENABLED    RPTE_RR_ENABLED
#define RPTE_MSGID_DISABLED   RPTE_RR_DISABLED

#define RPTE_TNLREL_DONE      0


#define RPTE_IP_ENCAP     IP_ENCAP 
#define RPTE_UDP_ENCAP    UDP_ENCAP 
#define RPTE_BOTH_ENCAP   BOTH_ENCAP 

#define RPTE_NBR_HELLO_SPRT_ENA 1
#define RPTE_NBR_HELLO_SPRT_DIS 2

#define RPTE_NBR_HELLO_STATE_UNKNOWN  0
#define RPTE_NBR_HELLO_STATE_SPRT     1
#define RPTE_NBR_HELLO_STATE_NSPRT    2
#define RPTE_NBR_HELLO_STATE_SPRT_RES 3

#define RPTE_NBR_HELLO_REL_ACTIVE  1
#define RPTE_NBR_HELLO_REL_PASSIVE 2

#define RPTE_EPOCH_BIT_MASK           0x00ffffff

#define RPTE_MAX_DELIM_STR_LEN        80

#define RPTE_FRR_REVERTIVE_GLOBAL      1
#define RPTE_FRR_REVERTIVE_LOCAL       2
#define RPTE_FRR_REVERTIVE_BOTH        3

#define RPTE_FRR_CSPF_RETRY_INTERVAL_MIN  1
#define RPTE_FRR_CSPF_RETRY_INTERVAL_MAX  60000
#define RPTE_FRR_CSPF_RETRY_INTERVAL_DEF  30000

#define RPTE_FRR_CSPF_RETRY_COUNT_MIN  1
#define RPTE_FRR_CSPF_RETRY_COUNT_MAX  20
#define RPTE_FRR_CSPF_RETRY_COUNT_DEF  10

#define RPTE_MAX_DETOUR_IDS            6
#define RPTE_DETOUR_TNL_INSTANCE       TE_DETOUR_TNL_INSTANCE

#define GMPLS_LABEL_SET_CLASS 36
#define GMPLS_LABEL_SET_CTYPE 1
#define GMPLS_LABEL_SET_CLASS_NUM_TYPE 0x2401
#define GMPLS_LBLSET_INCLUDE_LIST 0
#define GMPLS_LBLSET_EXCLUDE_LIST 1
#define GMPLS_LBLSET_INCLUDE_LIST_RANGE 2
#define GMPLS_LBLSET_EXCLUDE_LIST_RANGE 3

#define RPTE_PATH_STATE_REMOVED    0x04 

/* Macros for Test All function for fsrsvpte.mib */

#define RSVPTE_NOTIFICATION_ENABLED         1
#define RSVPTE_NOTIFY_RETRANSMIT_INTERVAL   2
#define RSVPTE_NOTIFY_RETRANSMIT_DECAY      3
#define RSVPTE_NOTIFY_RETRANSMIT_LIMIT      4
#define RSVPTE_ADMIN_STATUS_TIMER           5
#define RSVPTE_PSB_REMOVED_FLAG             6
#define RSVPTE_LABEL_SET_ENABLED            7
#define RSVPTE_ADMIN_STATUS_CAPABILITY      8
#define RSVPTE_GR_CAPABILITY                9
#define RSVPTE_GR_RECOVERY_PATH_CAPABILITY  10
#define RSVPTE_GR_RESTART_TIME              11
#define RSVPTE_GR_RECOVERY_TIME             12
#define RSVPTE_REOPTIMIZE_TIME    13
#define RSVPTE_ERO_CACHE_TIME    14

/* Minimum and Maximum Value for Notify Message Retry Parameters */
#define RPTE_NOTIFY_RETRY_INTVL_MIN_VAL 10
#define RPTE_NOTIFY_RETRY_INTVL_MAX_VAL 30000
#define RPTE_NOTIFY_RETRY_LIMIT_MIN_VAL 1
#define RPTE_NOTIFY_RETRY_LIMIT_MAX_VAL 16

/* Notify Header Size */
#define RPTE_NOTIFY_COMMON_HEADER_SIZE          66
#define RPTE_NOTIFY_ONE_NOTIFYMSG_HEADER_SIZE   28
#define RPTE_NOTIFY_ONE_NOTIFYMSG              (RPTE_IF_MIN_MTU_SIZE - RPTE_NOTIFY_COMMON_HEADER_SIZE) 

/* Minimum and Maximum value for Admin status timer */
#define RPTE_ADMIN_STATUS_TIME_INTERVAL_MIN 1
#define RPTE_ADMIN_STATUS_TIME_INTERVAL_MAX 300

#define RPTE_CSPF_PRIMARY_PATH              1
#define RPTE_CSPF_MAKE_BEFORE_BREAK_PATH    2
#define RPTE_CSPF_BACKUP_PATH               3

#define RPTE_DIRECTION_OUT                  1
#define RPTE_DIRECTION_IN                   2

#define RPTE_NO_MATCH                       0
#define RPTE_EXACT_MATCH                    1
#define RPTE_BEST_MATCH                     2

#define RPTE_ADMIN_STATUS_CLASS         196
#define RPTE_ADMIN_STATUS_CTYPE         1
#define RPTE_ADMIN_STATUS_CLASS_NUM_TYPE    0xc401
#define RPTE_ADMIN_STATUS_OBJECT_LEN        8
#define RPTE_ADMIN_GRACEFUL_DEL_TIME    30

#define RPTE_LSP_TNL_IF_ID_OBJ_CLASS 193
#define RPTE_LSP_TNL_IF_ID_OBJ_CTYPE 1
#define RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE_1 0xc101
#define RPTE_LSP_TNL_IF_ID_OBJ_CLASS_NUM_TYPE 0xc104
#define RPTE_LSP_TNL_IF_ID_OBJ_LEN 16

#define RPTE_PROTEC_OBJ_CLASS  37
#define RPTE_PROTEC_OBJ_CTYPE  2
#define RPTE_PROTEC_OBJ_CLASS_NUM_TYPE  0x2502
#define RPTE_PROTEC_OBJ_LEN 12

#define RPTE_ASSOC_OBJ_CLASS  199
#define RPTE_ASSOC_OBJ_CTYPE  1
#define RPTE_ASSOC_OBJ_CLASS_NUM_TYPE  0xc701
#define RPTE_ASSOC_OBJ_LEN 12

#define RPTE_NOTIFY_REQUEST_CLASS 195
#define RPTE_NOTIFY_REQUEST_CTYPE 1
#define RPTE_NOTIFY_REQUEST_CLASS_NUM_TYPE 0xc301
#define RPTE_NOTIFY_REQUEST_OBJ_LEN 8
#define RPTE_NOTIFY_RETRY_TIME      10
#define RPTE_NOTIFY_RETRY_LIMIT     25

#define RPTE_PROTECTION_SECONDARY         0x80
#define RPTE_PROTECTION_PROTECTING        0x40
#define RPTE_PROTECTION_NOTIFICATION      0x20
#define RPTE_PROTECTION_OPERATIONAL       0x10

#define RSVP_PROT_STATE_SO_REQ_SENT       1
#define RSVP_PROT_STATE_SO_REQ_RCVD       2
#define RSVP_PROT_STATE_SO_RESP_SENT      3
#define RSVP_PROT_STATE_SO_RESP_RCVD      4
#define RSVP_PROT_STATE_SB_REQ_SENT       5
#define RSVP_PROT_STATE_SB_REQ_RCVD       6 
#define RSVP_PROT_STATE_SB_RESP_SENT      7
#define RSVP_PROT_STATE_SB_RESP_RCVD      8

#define RPTE_RB_EQUAL     0
#define RPTE_RB_GREATER   1
#define RPTE_RB_LESSER    -1

/* RSVPTE GR related configuration file */
#define RSVPTE_GR_CONF          (const UINT1 *)"rsvptegr.conf"

/* RSVPTE GR related parameters */
#define RPTE_GR_RESTART_INTVL_MIN_VAL    60
#define RPTE_GR_RESTART_INTVL_MAX_VAL    3600
#define RPTE_GR_RESTART_DEFAULT_VAL      120

#define RPTE_GR_RECOVERY_INTVL_MIN_VAL   60
#define RPTE_GR_RECOVERY_INTVL_MAX_VAL   480
#define RPTE_GR_RECOVERY_DEFAULT_VAL     120
#define RPTE_GR_RESTART_CAP_CLASS             131
#define RPTE_GR_RESTART_CAP_CTYPE             1
#define RPTE_GR_RESTART_CAP_CLASS_NUM_TYPE    0x8301

#define RPTE_GR_CAPABILITY_CLASS             134
#define RPTE_GR_CAPABILITY_CTYPE             1
#define RPTE_GR_CAPABILITY_CLASS_NUM_TYPE    0x8601

#define RPTE_RECOVERY_GLBL_CLASS 34
#define RPTE_RECOVERY_GLBL_CTYPE 2
#define RPTE_RECOVERY_GLBL_CLASS_NUM_TYPE 0x2202

#define RPTE_SUGGESTED_GLBL_CLASS 129
#define RPTE_SUGGESTED_GLBL_CTYPE 2
#define RPTE_SUGGESTED_GLBL_CLASS_NUM_TYPE 0x8102

enum  {
    RPTE_GR_NOT_STARTED = 1,
    RPTE_GR_SHUT_DOWN_IN_PROGRESS,
    RPTE_GR_RESTART_IN_PROGRESS,
    RPTE_GR_ABORTED,
    RPTE_GR_RECOVERY_IN_PROGRESS,
    RPTE_GR_COMPLETED
};

enum {
    RPTE_GR_CONTROL_CHANNEL_FAULT = 1,
    RPTE_GR_NODAL_FAULT = 2
};

#define RPTE_GR_PKT_RECOVERY_PATH_SREFRESH 0x00000001
#define RPTE_GR_PKT_RECOVERY_PATH_RX       0x00000002
#define RPTE_GR_PKT_RECOVERY_PATH_TX       0x00000004

/* for Frr tunnel port mapping */
#define RPTE_TOTAL_BITS_IN_UINT4    32

/* for rsvp message type */
#define DEBUG_PATH_MSG_BIT          2
#define DEBUG_PATHTEAR_MSG_BIT      4
#define DEBUG_PATHERR_MSG_BIT       6
#define DEBUG_RESV_MSG_BIT          8
#define DEBUG_RESVTEAR_MSG_BIT      10
#define DEBUG_RESVERR_MSG_BIT       12
#define DEBUG_RESVCONF_MSG_BIT      14
#define DEBUG_HELLO_MSG_BIT         16
#define DEBUG_BUNDLE_MSG_BIT        18
#define DEBUG_ACK_MSG_BIT           20
#define DEBUG_SREFRESH_MSG_BIT      22
#define DEBUG_NOTIFY_MSG_BIT        24
#define DEBUG_RECOVERY_PATH_MSG_BIT 26
#define DEBUG_RSVP_ALL_MSG_BIT      28

/* RSVP-TE LSP Reoptimization related parameters*/

#define RPTE_REOPT_TIME_DEFAULT_VAL 60
#define RPTE_ERO_CACHE_TIME_DEFAULT_VAL 5

#define RPTE_REOPT_TIME_INTVL_MIN_VAL    60
#define RPTE_REOPT_TIME_INTVL_MAX_VAL    7200

#define RPTE_ERO_CACHE_INTVL_MIN_VAL    5
#define RPTE_ERO_CACHE_INTVL_MAX_VAL    30

#endif /* _RPTEDEFS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rptedefs.h                             */
/*---------------------------------------------------------------------------*/
