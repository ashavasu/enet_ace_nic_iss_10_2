/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptdstdf.h,v 1.3 2007/02/13 14:00:37 iss Exp $
 *
 * Description: This file contains the structure type definitions 
 *              declared and used for Support of DIFFSERV in RSVP-TE 
 *              Module.
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptdstdf.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains all the data structures and
 *                             definitions required for DiffServ support 
 *                             in RSVP-TE.
 *---------------------------------------------------------------------------*/

#ifndef _RPTEDSTDF_H
#define _RPTEDSTDF_H

typedef struct _DiffServMapEntry
{
    UINT1                u1Resvd;
    UINT1                u1Exp;
    UINT2                u2PhbId;
}
tDiffServMapEntry;


typedef struct _DiffServElspObj
{
    tObjHdr              ObjHdr;
    UINT2                u2Resvd;
    UINT1                u1Resvd;
    UINT1                u1NoOfMapEntries;
    tDiffServMapEntry    aDiffServMapEntry[8];
}
tDiffServElspObj;

typedef struct _DiffServLlspObj
{
    tObjHdr              ObjHdr;
    UINT2                u2Resvd;
    UINT2                u2Psc;
}
tDiffServLlspObj;


typedef struct _RpteDiffServElspTPEntry
{
    UINT2               u2Resvd;
    UINT2               u2Psc;
    UINT4               u4TokenBktRate;
    UINT4               u4TokenBktSize;
    UINT4               u4PeakDataRate;
    UINT4               u4MinPolicedUnit;
    UINT4               u4MaxPktSize;
}
tRpteDiffServElspTPEntry;

typedef struct _ElspTrafficProfileObj
{
    tObjHdr              ObjHdr;
    UINT2                u2Resvd;
    UINT1                u1Resvd;
    UINT1                u1NoOfElspTPEntries;
    tRpteDiffServElspTPEntry aElspTPEntry[8];
}
tElspTrafficProfileObj;

typedef struct _DiffServClassTypeObj
{
    tObjHdr              ObjHdr;
    UINT2                u2Resvd;
    UINT1                u1Resvd;
    UINT1                u1ClassType;
}
tDiffServClassTypeObj;

#endif /*_RPTEDSTDF_H*/    
/*---------------------------------------------------------------------------*/
/*                        End of file rptdstdf.h                             */
/*---------------------------------------------------------------------------*/
