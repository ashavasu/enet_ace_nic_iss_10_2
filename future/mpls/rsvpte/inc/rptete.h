 /********************************************************************
 *  $RCSfile: rptete.h,v $
 *
 *  $Id: rptete.h,v 1.22 2016/03/11 11:32:50 siva Exp $
 * 
 *  $Date: 2016/03/11 11:32:50 $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptete.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains Api's, data structures and
 *                             Macros corresponding to TE module.
 *                             
 *---------------------------------------------------------------------------*/

#ifndef _RPTETE_H
#define _RPTETE_H

#define rpteTeCreateTunnel         TeSigCreateTunnel
#define rpteTeCreateNewTnl         TeSigCreateNewTnl
#define rpteTeDeleteTnlInfo        TeSigDeleteTnlInfo
#define rpteTeCreateTrfcParams     TeSigCreateTrfcParams
#define rpteTeDeleteTrfcParams     TeSigDeleteTrfcParams
#define rpteTeUpdateTrfcParams     TeSigUpdateTrfcParams
#define rpteTeUpdateTeTnl          TeSigUpdateTeTnl
#define rpteTeCreateArHopListInfo  TeSigCreateArHopListInfo
#define rpteTeDeleteArHopListInfo  TeSigDeleteArHopListInfo
#define rpteTeCreateHopListInfo    TeSigCreateHopListInfo
#define rpteTeDeleteHopListInfo    TeSigDeleteHopListInfo
#define rpteTeDeleteTnlHopInfo     TeSigDeleteHopInfo
#define rpteTeUpdateArHopListInfo  TeSigUpdateArHopListInfo
#define rpteTeCreatePathListAndPathOption TeSigCreatePathListAndPathOption
#define rpteTeCreateErHopsForCspfComputedHops TeSigCreateErHopsForCspfComputedHops
#define rpteTeCreateCHopListInfo      TeSigCreateCHopListInfo
#define rpteTeDeleteCHopListInfo   TeSigDeleteCHopListInfo
#define rpteTeCreateCHopsForCspfPaths TeSigCreateCHopsForCspfPaths
#define rpteTeCompareCHopInfo  TeSigCompareCHopInfo
#define rpteTeUpdatePrimaryInstance TeSigUpdatePrimaryInstance
#define rpteTeUpdateOriginalInstance TeSigUpdateOriginalInstance
#define rpteTeUpdateReoptTriggerState TeSigUpdateReoptTriggerState

#define rpteTeDiffServChkIsPerOAResrcConf    TeSigDiffServChkIsPerOAResrcConf
#define rpteTeDiffServGetPhbPsc              TeSigDiffServGetPhbPsc
#define rpteTeDiffServCreateDiffServTnl      TeSigCreateDiffServTnl 
#define rpteTeDiffServDeleteDiffServTnl      TeSigDeleteDiffServTnl 
#define rpteTeDiffServCreateDiffServElspList TeSigCreateDiffServElspList 
#define rpteTeDiffServDeleteDiffServElspList TeSigDeleteDiffServElspList 
#define rpteTeSigAdminStatus                 TeSigAdminStatus
#define rpteTeSetTnlOperStatus               TeSigSetTnlOperStatus
#define rpteTeFrrTnlAppDown                  TeSigSetTnlAppDown
#define rpteTeFrrTnlAppUp                    TeSigSetTnlAppUp
#define RpteGetAttrFromTnlOfAttrList         TeSigGetAttrFromTnlOrAttrListByMask
#define rpteSendAdminStatusTrapAndSyslog     TeSigSendAdminStatusTrapAndSyslog
#define rpteSigCreateTnlErrorTable           TeSigCreateTnlErrorTable
#define rpteSigDeleteTnlErrorTable           TeSigDeleteTnlErrorTable
#define rpteTeSetDetourStatus                TeSigSetDetourStatus
#define rpteTeGetTrafficParams               TeSigGetTrafficParams
#define rpteTeUpdateMaxErhopsPerTnl          TeSigUpdateMaxErhopsPerTnl
#define rpteTeCheckMaxErhopInTnlTbl          TeSigCheckMaxErhopInTnlTbl
#define rpteTeUpdateReoptimizeStatus   TeSigUpdateReoptimizeStatus
#define rpteTeGetTunnelInfo                  TeSigGetTunnelInfo
#define rpteTeCreateReoptimizeTunnel   TeSigCreateReoptimizeTunnel
#define rpteIsTeReoptimizeTnlListEmpty   IsTeSigReoptimizeTnlListEmpty
#define rpteTeCompareEroCacheAndCHopInfo  TeSigCompareEroCacheAndCHopInfo
#define rpteTeReleaseTnlInfo     TeSigReleaseTnlInfo

/* TE Data Structure  */
#define tRsvpTePathListInfo            tTePathListInfo
#define tRsvpTePathInfo                tTePathInfo
#define tRsvpTeHopInfo                 tTeHopInfo
#define tRsvpTeTrfcParams              tTeTrfcParams
#define tRsvpTeRSVPTrfcParams          tRSVPTrfcParams
#define tRsvpTeArHopListInfo           tTeArHopListInfo
#define tRsvpTeDiffServElspList        tMplsDiffServELspList
#define tRsvpTeArHopInfo               tTeArHopInfo

/*TE Macros*/
#define RSVPTE_TNL_INDEX                TE_TNL_TNL_INDEX
#define RSVPTE_TNL_INSTANCE             TE_TNL_TNL_INSTANCE
#define RSVPTE_TNL_INGRESS_RTR_ID       TE_TNL_INGRESS_LSRID
#define RSVPTE_TNL_EGRESS_RTR_ID        TE_TNL_EGRESS_LSRID
#define RSVPTE_TNL_PRIMARY_INSTANCE     TE_TNL_PRIMARY_TNL_INSTANCE
#define RSVPTE_TNL_NAME                 TE_TNL_NAME
#define RSVPTE_TNL_SGNL_PRTCL           TE_TNL_SIGPROTO
#define RSVPTE_TNL_SET_PRIO             TE_TNL_SETUP_PRIO
#define RSVPTE_TNL_HOLD_PRIO            TE_TNL_HLDNG_PRIO
#define RSVPTE_TNL_SRLG_TYPE            TE_TNL_SRLG_TYPE
#define RSVPTE_TNL_SRLG_LIST            TE_TNL_SRLG_LIST
#define RSVPTE_TNL_SSN_ATTR             TE_TNL_SSN_ATTR
#define RSVPTE_TNL_PATH_IN_USE          TE_TNL_PATH_IN_USE
#define RSVPTE_TNL_ROLE                 TE_TNL_ROLE
#define RSVPTE_TNL_OWNER                TE_TNL_OWNER
#define RSVPTE_TNL_RSRC_INDEX           TE_TNLRSRC_INDEX
#define RSVPTE_TNL_TRFC_PARAM           TE_TNL_TRFC_PARAM
#define RSVPTE_TNL_PATH_INFO            TE_TNL_PATH_INFO
#define RSVPTE_TNL_PATH_LIST_INFO       TE_TNL_PATH_LIST_INFO
#define RSVPTE_TNL_PATH_LIST            TE_HOP_LIST
#define RSVPTE_TNL_INC_ANY_AFFINITY     TE_TNL_INC_ANY_AFFINITY
#define RSVPTE_TNL_INC_ALL_AFFINITY     TE_TNL_INC_ALL_AFFINITY
#define RSVPTE_TNL_EXC_ANY_AFFINITY     TE_TNL_EXC_ANY_AFFINITY
#define RSVPTE_TNL_LBL_REC              TE_TNL_LBL_REC
#define RSVPTE_TNL_TPARAM_INDEX         TE_TNL_TRFC_PARAM_INDEX
#define RSVPTE_TNL_ROW_STATUS           TE_TNL_ROW_STATUS
#define RSVPTE_TNL_ADMIN_STATUS         TE_TNL_ADMIN_STATUS
#define RSVPTE_TNL_OPER_STATUS          TE_TNL_OPER_STATUS
#define RSVPTE_IN_ARHOP_LIST            TE_IN_ARHOP_LIST
#define RSVPTE_IN_AR_HOP_LIST_INFO      TE_IN_AR_HOP_LIST_INFO
#define RSVPTE_OUT_AR_HOP_LIST_INFO     TE_OUT_AR_HOP_LIST_INFO
#define RSVPTE_OUT_ARHOP_LIST           TE_OUT_ARHOP_LIST
#define RSVPTE_TNL_ISIF                 TE_TNL_ISIF
#define RSVPTE_TNL_FRR_FACILITY_METHOD  TE_TNL_FRR_FACILITY_METHOD
#define RSVPTE_TNL_FRR_ONE2ONE_METHOD   TE_TNL_FRR_ONE2ONE_METHOD
#define RSVPTE_TE_TNL_FRR_CONST_INFO    TE_TNL_FRR_CONST_INFO
#define RSVPTE_TE_TNL_FRR_CONST_SE_STYLE  TE_TNL_FRR_CONST_SE_STYLE
#define RSVPTE_TE_TNL_FRR_CONST_PROT_TYPE TE_TNL_FRR_CONST_PROT_TYPE 
#define RSVPTE_TE_TNL_FRR_PROT_METHOD   TE_TNL_FRR_PROT_METHOD 
#define RSVPTE_TE_TNL_REOPTIMIZE_STATUS  TE_TNL_REOPTIMIZE_STATUS
#define RSVPTE_TNL_PROT_IF_INDEX                       TE_TNL_FRR_PROT_IFINDEX
#define RSVPTE_TE_TNL_FRR_PROT_TYPE                    TE_TNL_FRR_PROT_TYPE
#define RSVPTE_TE_TNL_FRR_BKP_TNL_INDEX                TE_TNL_FRR_BKP_TNL_INDEX
#define RSVPTE_TE_TNL_FRR_BKP_TNL_INST                 TE_TNL_FRR_BKP_TNL_INST
#define RSVPTE_TE_TNL_FRR_BKP_TNL_INGRESS_ID           TE_TNL_FRR_BKP_TNL_INGRESS_ID
#define RSVPTE_TE_TNL_FRR_BKP_TNL_EGRESS_ID            TE_TNL_FRR_BKP_TNL_EGRESS_ID
#define RSVPTE_TE_TNL_FRR_ONE2ONE_PLR_ID               TE_TNL_FRR_ONE2ONE_PLR_ID
#define RSVPTE_TE_TNL_FRR_ONE2ONE_SENDER_ADDR_TYPE     TE_TNL_FRR_ONE2ONE_SENDER_ADDR_TYPE
#define RSVPTE_TE_TNL_FRR_ONE2ONE_SENDER_ADDR          TE_TNL_FRR_ONE2ONE_SENDER_ADDR
#define RSVPTE_TE_TNL_FRR_ONE2ONE_AVOID_ADDR_TYPE      TE_TNL_FRR_ONE2ONE_AVOID_ADDR_TYPE
#define RSVPTE_TE_TNL_FRR_ONE2ONE_AVOID_ADDR           TE_TNL_FRR_ONE2ONE_AVOID_ADDR
#define RSVPTE_TE_TNL_FRR_DETOUR_ACTIVE                TE_TNL_FRR_DETOUR_ACTIVE
#define RSVPTE_TE_TNL_FRR_DETOUR_MERGING               TE_TNL_FRR_DETOUR_MERGING
#define RSVPTE_TE_TNL_FRR_FAC_TNL_STATUS               TE_TNL_FRR_FAC_TNL_STATUS
#define RSVPTE_TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH       TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH
#define RSVPTE_TE_TNL_REOPTIMIZE_TRIGGER      TE_TNL_REOPTIMIZE_TRIGGER

/* Traffic Parameter Table Related */
#define RPTE_TRFC_PARAM_INDEX       TE_TNLRSRC_INDEX
#define RPTE_TNLRSRC_ROW_STATUS     TE_TNLRSRC_ROW_STATUS
#define RPTE_TNLRSRC_STORAGE_TYPE   TE_TNLRSRC_STORAGE_TYPE
#define RPTE_RSVPTE_TRFC_PARAMS     TE_RSVPTE_TRFC_PARAMS
#define RPTE_TRFC_PARAM_NUM_TNLS    TE_TNLRSRC_NUM_OF_TUNNELS
#define RPTE_TRFC_PARAM_ROLE        TE_TNLRSRC_ROLE

/* RSVPTE Traffic Param Table related */
#define RPTE_TPARAM_TBR          TE_RSVPTE_TPARAM_TBR
#define RPTE_TPARAM_TBS          TE_RSVPTE_TPARAM_TBS
#define RPTE_TPARAM_PDR          TE_RSVPTE_TPARAM_PDR
#define RPTE_TPARAM_MPU          TE_RSVPTE_TPARAM_MPU
#define RPTE_TPARAM_MPS          TE_RSVPTE_TPARAM_MPS
#define RPTE_TPARAM_CFG_FLAG     TE_RSVPTE_TPARAM_CFG_FLAG

 /* Path List Structure Related */
#define RSVPTE_HOP_LIST             TE_HOP_LIST

#define RSVPTE_HOPLIST_INDEX        TE_HOPLIST_INDEX
#define RSVPTE_HOP_ROLE             TE_HOP_ROLE
#define RSVPTE_HOP_PO_LIST          TE_HOP_PO_LIST

 /* PathOption Structure Related */
#define RSVPTE_PO_NUM_TUNNELS       TE_PO_NUM_TUNNELS
#define RSVPTE_PO_INDEX             TE_PO_INDEX
#define RSVPTE_PO_HOP_COUNT         TE_PO_HOP_COUNT
 
#define RSVPTE_ERHOP_ADDR_TYPE      TE_ERHOP_ADDR_TYPE
#define RSVPTE_ERHOP_INDEX          TE_ERHOP_INDEX
#define RSVPTE_ERHOP_IP_ADDR        TE_ERHOP_IP_ADDR
#define RSVPTE_ERHOP_IPV4_ADDR      TE_ERHOP_IPV4_ADDR
#define RSVPTE_ERHOP_ADDR_PRFX_LEN  TE_ERHOP_ADDR_PRFX_LEN
#define RSVPTE_ERHOP_LSPID          TE_ERHOP_LSPID
#define RSVPTE_ERHOP_AS_NUM         TE_ERHOP_AS_NUM
#define RSVPTE_ERHOP_TYPE           TE_ERHOP_TYPE
#define RSVPTE_ERHOP_ROW_STATUS     TE_ERHOP_ROW_STATUS

/* ArHop Table */
#define RSVPTE_TE_ARHOP_ADDR_TYPE     TE_ARHOP_ADDR_TYPE 
#define RSVPTE_TE_ARHOP_IP_ADDR       TE_ARHOP_IP_ADDR
#define RSVPTE_TE_ARHOP_AS_NUM        TE_ARHOP_AS_NUM
#define RSVPTE_TE_ARHOP_LSPID         TE_ARHOP_LSPID
#define RSVPTE_TE_ARHOP_FLAG          TE_ARHOP_FLAG
#define RSVPTE_TE_ARHOP_ADDR_PRFX_LEN TE_ARHOP_ADDR_PRFX_LEN
#define RSVPTE_TE_ARHOP_PROT_TYPE        TE_FRR_ARHOP_PROT_TYPE
#define RSVPTE_TE_ARHOP_PROT_TYPE_IN_USE TE_FRR_ARHOP_PROT_TYPE_IN_USE

#define RPTE_PROT                     TE_SIGPROTO_RSVP
#define RPTE_TNL_OWNER_RSVP           TE_TNL_OWNER_RSVP  
#define RPTE_TE_TNL_FRR_PROTECTION_LINK TE_TNL_FRR_PROT_LINK
#define RPTE_TE_TNL_FRR_PROTECTION_NODE TE_TNL_FRR_PROT_NODE

/* Tunnel Related Events */
#define RSVPTE_TNL_UP                       TE_TNL_UP
#define RSVPTE_TNL_DOWN                     TE_TNL_DOWN
#define RSVPTE_TNL_DESTROY                  TE_TNL_DESTROY
#define RSVPTE_TNL_MANUAL_REOPTIMIZATION  TE_TNL_MANUAL_REOPTIMIZATION
#define RSVPTE_TNL_TE_GOING_DOWN            TE_GOING_DOWN
#define RSVPTE_TNL_TX_PATH_MSG              TE_TNL_TX_PATH_MSG
#define RPTE_PROTECTION_SWITCH_OVER         TE_PROTECTION_SWITCH_OVER
#define RPTE_PROTECTION_SWITCH_BACK         TE_PROTECTION_SWITCH_BACK
/* Role of the LSR */
#define RPTE_INGRESS                  TE_INGRESS
#define RPTE_INTERMEDIATE             TE_INTERMEDIATE
#define RPTE_EGRESS                   TE_EGRESS

/* TE SUCCESS - FAILURE */
#define RPTE_TE_SUCCESS                TE_SUCCESS
#define RPTE_TE_FAILURE                TE_FAILURE

#define RPTE_TE_EQUAL       TE_EQUAL
#define RPTE_TE_NOT_EQUAL              TE_NOT_EQUAL

#define RPTE_TE_REOPTIMIZE_ENABLE   TE_REOPTIMIZE_ENABLE
#define RPTE_TE_REOPTIMIZE_DISABLE   TE_REOPTIMIZE_DISABLE

/* TE Tunnel Direction */
#define RPTE_TE_TNL_DIR_IN             TE_TNL_DIR_IN
#define RPTE_TE_TNL_DIR_OUT            TE_TNL_DIR_OUT

/* Er Hop Related Definitions */
#define ERO_TYPE_IPV4_ADDR            TE_ERHOP_IPV4_TYPE
#define ERO_TYPE_IPV6_ADDR            TE_ERHOP_IPV6_TYPE
#define ERO_TYPE_ASNUM                TE_ERHOP_ASNUM_TYPE
#define ERO_TYPE_UNNUM_IF             TE_ERHOP_UNNUM_TYPE

#define ERO_TYPE_IPV4_PRELEN          TE_ERHOP_IPV4_PREFIX_LEN

#define ERO_TYPE_STRICT               TE_STRICT_ER
#define ERO_TYPE_LOOSE                TE_LOOSE_ER

#define ERO_TYPE_LBL_FWD              TE_ERHOP_FORWARD_LBL_PRESENT
#define ERO_TYPE_LBL_REV              TE_ERHOP_REVERSE_LBL_PRESENT

/* RRO Related Definitions */
#define RRO_TYPE_IPV4_ADDR            TE_ARHOP_IPV4_TYPE
#define RRO_TYPE_IPV6_ADDR            TE_ARHOP_IPV6_TYPE
#define RRO_TYPE_ASNUM                TE_ARHOP_ASNUM_TYPE
#define RRO_TYPE_UNNUM_IF             TE_ARHOP_UNNUM_TYPE

#define RRO_TYPE_IPV4_PRELEN          TE_ARHOP_IPV4_PREFIX_LEN

#define RRO_TYPE_LBL_FWD              TE_ARHOP_FORWARD_LBL_PRESENT
#define RRO_TYPE_LBL_REV              TE_ARHOP_REVERSE_LBL_PRESENT
#define RRO_TYPE_LBL_FWD_GBL          TE_ARHOP_FORWARD_LBL_GLOBAL
#define RRO_TYPE_LBL_REV_GBL          TE_ARHOP_REVERSE_LBL_GLOBAL

/* Session Attribute related defn */
#define RSVPTE_SSN_FAST_REROUT_BIT  TE_SSN_FAST_REROUTE_BIT
#define RSVPTE_SSN_SE_STYLE_BIT     TE_SSN_SE_STYLE_BIT
#define RSVPTE_SSN_MRG_PRMT_BIT     TE_SSN_MRG_PRMT_BIT
#define RSVPTE_SSN_IS_PINNED_BIT    TE_SSN_IS_PINNED_BIT
#define RSVPTE_SSN_REC_ROUTE_BIT    TE_SSN_REC_ROUTE_BIT

#define RSVPTE_SSN_LBL_RECORD_BIT   TE_SSN_LBL_RECORD_BIT

/* Attribute list related defn */
#define RSVPTE_ATTR_SETUPPRI_BITMASK                 TE_ATTR_SETUPPRI_BITMASK
#define RSVPTE_ATTR_HOLDPRI_BITMASK                  TE_ATTR_HOLDPRI_BITMASK
#define RSVPTE_ATTR_LSP_SSN_ATTR_BITMASK             TE_ATTR_LSP_SSN_ATTR_BITMASK
#define RSVPTE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK     TE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK
#define RSVPTE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK     TE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK
#define RSVPTE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK     TE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK
#define RSVPTE_ATTR_BANDWIDTH_BITMASK                TE_ATTR_BANDWIDTH_BITMASK
#define RSVPTE_ATTR_CLASS_TYPE_BITMASK               TE_ATTR_CLASS_TYPE_BITMASK
#define RSVPTE_ATTR_SRLG_TYPE_BITMASK                TE_ATTR_SRLG_TYPE_BITMASK

#define RPTE_TNL_WORKING_PATH             TE_TNL_WORKING_PATH
#define RPTE_TNL_PROTECTION_PATH          TE_TNL_PROTECTION_PATH

#endif /* _RPTETE_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rptete.h                               */
/*---------------------------------------------------------------------------*/
