
/********************************************************************
 *
 * $Id: rpteport.h,v 1.17 2014/07/01 11:56:01 siva Exp $
 *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rpteport.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the RSVP-TE
 *                             porting related definitions.
 *---------------------------------------------------------------------------*/

#ifndef _RPTEPORT_H
#define _RPTEPORT_H

#define RPTE_IS_DEST_OUR_ADDR      RpteIpLocalAddress
#define RPTE_IPIF_SEND_PKT         RpteIpIfSendPkt

#define RPTE_IP_GET_PORT_FROM_IF_INDEX IpGetPortFromIfIndex
#define RPTE_IP_GET_IF_INDEX_FROM_PORT IpGetIfIndexFromPort
#define RPTE_IP_SUCCESS                IP_SUCCESS
#define RPTE_IP_SEND                   ip_send
#define RPTE_UDP_OPEN                  udp_open
#define RPTE_UDP_CLOSE                 udp_close
#define RPTE_IP_REG_RSVP_PRTCL         NetIpv4RegisterHigherLayerProtocol
#define RPTE_IP_DEREG_RSVP_PRTCL       NetIpv4DeRegisterHigherLayerProtocol
#define RPTE_IP_IF_VAL_INDEX           IpifValidateIfIndex 
#define RPTE_SRC_ADDR(x)               ((x)->ModuleData.u4Reserved1)
#define RPTE_DEST_ADDR(x)              ((x)->ModuleData.u4Reserved2)
/* LABEL Manager defs */
#define RPTE_MPLS_REL_LBL_TO_LBLGROUP       LblMgrRelLblToLblGroup
#define RPTE_MPLS_GET_LBL_FROM_LBLGROUP     lblMgrGetLblFromLblGroup
#define RPTE_MPLS_LBLMGR_CREATE_LBLSPACE    LblMgrCreateLabelSpaceGroup
#define RPTE_MPLS_LBLMGR_DELETE_LBLSPACE    LblMgrDeleteLabelSpaceGroup


#define RPTE_LBL_MNGR_SUCCESS     LBL_SUCCESS
#define RPTE_LBL_MNGR_FAILURE     LBL_FAILURE
#define RPTE_LBL_ALLOC_BOTH_NUM   LBL_ALLOC_BOTH_NUM
#define RPTE_LBL_ALLOC_ODD_NUM    LBL_ALLOC_ODD_NUM
#define RPTE_LBL_ALLOC_EVEN_NUM   LBL_ALLOC_EVEN_NUM

/* TC Related Defs*/
#define RPTE_TC_ERR_BW_NA            TC_ERR_BANDWIDTH_NOT_AVAILABLE
#define RPTE_TC_RES_SPEC_TYPE        TC_RSVPTE_TYPE_RES_SPEC
#define RPTE_TC_INTSERV_TYPE         TC_INTSERV             
#define RPTE_TC_FAILURE              TC_FAILURE        
#define RPTE_TC_SUCCESS              TC_SUCCESS        
#define RPTE_TC_REG_RSVP_PRTCL       TcRegisterApp
#define RPTE_TC_DEREG_RSVP_PRTCL     TcDeregisterApp 
#define RPTE_TC_RESV_RESOURCES       TcResvResources
#define RPTE_TC_FREE_RESOURCES       TcFreeResources
#define RPTE_TC_MODIFY_RESOURCES     TcModifyResources
#define RPTE_TC_ASSOC_FILTER_INFO    TcAssociateFilterInfo
#define RPTE_TC_DISASSOC_FILTER_INFO TcDissociateFilterInfo
#define RPTE_TC_GENERATE_ADSPEC      TcGenerateAdSpec
#define RPTE_TC_CALCULATE_ADSPEC     TcCalcAdSpec
#define RPTE_TC_GENERATE_FLOWSPEC    TcGenerateFlowSpec
#define RPTE_TC_CALCULATE_FLOWSPEC   TcCalculateFlowSpec
#define RPTE_TC_FREE_APP_RESOURCES   TcFreeAppResources

/* MPLS-FM related defs */
#define RPTE_MPLS_MLIB_UPDATE        MplsMlibUpdate
#define RPTE_MPLS_DIFF_PRECONF_MAP   MplsDiffServGetPreConfMap

 /* MPLS-FM General macros */
#define RPTE_MPLS_SUCCESS         MPLS_SUCCESS
#define RPTE_MPLS_FAILURE         MPLS_FAILURE

/* MLIB Operations  */
#define RPTE_MPLS_MLIB_ILM_CREATE    MPLS_MLIB_ILM_CREATE
#define RPTE_MPLS_MLIB_ILM_DELETE    MPLS_MLIB_ILM_DELETE
#define RPTE_MPLS_MLIB_ILM_MODIFY    MPLS_MLIB_ILM_MODIFY
#define RPTE_MPLS_MLIB_TNL_CREATE    MPLS_MLIB_TNL_CREATE
#define RPTE_MPLS_MLIB_TNL_DELETE    MPLS_MLIB_TNL_DELETE
#define RPTE_MPLS_MLIB_TNL_MODIFY    MPLS_MLIB_TNL_MODIFY

/* Operation to be performed on the Label Stack */

#define RPTE_MPLS_OPR_PUSH          MPLS_OPR_PUSH
#define RPTE_MPLS_OPR_PUSHES        MPLS_OPR_PUSHES
#define RPTE_MPLS_OPR_POP           MPLS_OPR_POP
#define RPTE_MPLS_OPR_POPS          MPLS_OPR_POPS
#define RPTE_MPLS_OPR_POP_PUSH      MPLS_OPR_POP_PUSH
#define RPTE_MPLS_OPR_POPS_PUSHES   MPLS_OPR_POPS_PUSHES

#define RSVPTE_SOCKET            socket
#define RSVPTE_SOCK_CLOSE        close
#define RSVPTE_FCNTL             fcntl
#define RSVPTE_SENDTO            sendto 
#define RSVPTE_RECVFROM          recvfrom
#define RSVPTE_SELECT            select

extern INT4 ip_send (UINT4 u4Src, UINT4 u4Dest, UINT1 u1Proto, UINT1 u1Tos,
                     UINT1 u1Ttl, tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2Len,
                     UINT2 u2Id, UINT1 u1Df, UINT2 u2Olen, UINT2 u2Rt_port);

/* OSIX related definitions and macros used in RSVP */

#define UDP_PKT_RXED_EVENT             (0x40000000)
#define ROUTE_CHANGE_NOTIFICATION      (0x20000000)
#define SNMP_RSVP_EVENT                (0x10000000)
#define RSVP_ADF_RESPONSE_RXED_EVENT   (0x08000000)
#define RSVP_SDF_RESPONSE_RXED_EVENT   (0x04000000)
#define RSVP_ADF_REQUEST_RXED_EVENT    (0x02000000)
#define RSVP_SDF_REQUEST_RXED_EVENT    (0x01000000)
#define IF_CHANGE_NOTIFICATION         (0x00100000)
#define CSPF_RESPONSE_RXED_EVENT       (0x00000002)

/* FSAP2 - RSVP - IP Interaction */
#define CRU_IP_ROUTING_RSVP_MODULE    0x0A
#define CRU_IP_FORWARD_MODULE         0x01
#define CRU_IP_ROUTING_UDP_MODULE     0x02
#define CRU_IP_RSVP_TASK_ID           1

#define RSVP_UDP_WAIT_EVENT           0x00001000

#define RSVP_QDEPTH                   2800
#define RSVP_QTIMEOUT                 0
#define RSVP_QMODE                    OSIX_LOCAL
#define RSVP_QFLAGS                   OSIX_NO_WAIT

#define RSVP_DEF_OFFSET               0
#define RPTE_IF_DEF_OFFSET            1

#define   RPTE_IP_IF_IS_INVALID_ID(IfId) \
          (CRU_BUF_Get_Interface_Type((IfId)) == RPTE_IP_IF_TYPE_INVALID)


typedef struct IpHeader
{
    UINT1               u1VerIhl;
    UINT1               u1Tos;
    UINT2               u2Length;
    UINT4               u4Ignore1;
    UINT1               u1Ttl;
    UINT1               u1Proto;
    UINT2               u2CheckSum;
    UINT4               u4SrcAddr;
    UINT4               u4DstAddr;
}
tRsvpIpHdr;

typedef struct UdpHeader
{
    UINT2               u2SrcPort;
    UINT2               u2DstPort;
    UINT2               u2Length;
    UINT2               u2CheckSum;
}
tUdpHdr;

typedef struct RouterAlertOption
{
    UINT4               u4Opt;
}
tRaOpt;

/* IP */
#define IP_HDR_LENGTH(pIpHdr)    ( ( ((pIpHdr)->u1VerIhl) & 0x0F) * 4 )
#define IP_HDR_DST_ADDR(pIpHdr)  ( (pIpHdr)->u4DstAddr )
#define IP_HDR_SRC_ADDR(pIpHdr)  ( (pIpHdr)->u4SrcAddr )
#define IP_HDR_TOS(x)            ( (x)->u1Tos )
#define IP_HDR_TTL(x)            ( (x)->u1Ttl )

#define CRU_BUF_MSG_PARAMS(pChainHdr) \
                  (pChainHdr->ModuleData.au1_UserData)
#define  IP_PARMS_SET_IP_SEND_PARMS(pBuf,pParms)\
         (*(UINT4 *)(CRU_BUF_MSG_PARAMS(pBuf))) = (UINT4)pParms;

    
/* UDP */
#define UDP_HDR_SRC_PORT(x) ((x)->u2SrcPort)
#define UDP_HDR_DST_PORT(x) ((x)->u2DestPort)
#define UDP_HDR_LENGTH(x)   ((x)->u2Length)
#define UDP_HDR_CHECKSUM(x) ((x)->u2CheckSum)

#define RSVP_TASK_NAME              "RPTE"
#define RSVP_BUF_NORMAL_REL         FALSE

/* Singly Linked List */
#define tRSVP_SLL                   tTMO_SLL
#define tRSVP_SLL_NODE              tTMO_SLL_NODE
#define RSVP_SLL_INIT(x)            TMO_SLL_Init (x)
#define RSVP_DLL_INIT(x)            TMO_DLL_Init (x)
#define RSVP_SLL_INIT_NODE(x)       TMO_SLL_Init_Node (x)
#define RSVP_SLL_ADD_NODE(x, y)     TMO_SLL_Add (x, (tRSVP_SLL_NODE *) y)
#define RSVP_SLL_FIRST(pList)       TMO_SLL_First(pList)
#define RSVP_SLL_NEXT(pList, pNode) TMO_SLL_Next(pList, pNode)
#define RSVP_SLL_COUNT(pList)       TMO_SLL_Count(pList)
#define RSVP_SLL_SCAN(pList,pNode,TypeCast) TMO_SLL_Scan(pList,pNode,TypeCast)
#define RSVP_SLL_IS_NODE_IN_LIST(pNode)     TMO_SLL_Is_Node_In_List(pNode)
#define RSVP_SLL_DELETE(pList, pNode)  \
        TMO_SLL_Delete (pList, (tRSVP_SLL_NODE *) pNode)

/* Timers */
#define tRSVP_TIMER_LIST   tTimerListId
#define tRSVP_TIMER        tTmrAppTimer
#define RSVP_START_TIMER   TmrStartTimer
#define RSVP_TIMER_NAME(x) ((x)->u4Data)

/* Buffer */
#define tTMO_BUF_CHAIN_DESC  tCRU_BUF_CHAIN_HEADER
#define tMsgBuf              tCRU_BUF_CHAIN_HEADER
#define tTMO_INTERFACE       tCRU_INTERFACE
#define IF_TYPE(IfId)        CRU_BUF_Get_Interface_Type(IfId)
#define IF_NUM(IfId)         CRU_BUF_Get_Interface_Num(IfId)
#define IF_INDEX(IfId)       CRU_BUF_Get_IfIndex(IfId)        
#define IF_VCNUM(IfId)       CRU_BUF_Get_Interface_SubRef(IfId)

#define BUF_DUP_MSG_HDR(pMsgBuf) CRU_BUF_Duplicate_BufChain(pMsgBuf);\
                                 ++gi4MsgAlloc

#define BUF_DUP_DATA_DESCR(pMsgBuf) CRU_BUF_Duplicate_BufChain(pMsgBuf);\
                                    ++gi4MsgAlloc

#define BUF_ALLOC(u4Length, u4Offset)  \
        (CRU_BUF_Allocate_MsgBufChain(u4Length, u4Offset)); ++gi4MsgAlloc

#define BUF_RELEASE(pMsgBuf) --gi4MsgAlloc; \
        (CRU_BUF_Release_MsgBufChain(pMsgBuf, RSVP_BUF_NORMAL_REL))

#define BUF_GET_INTERFACE_ID(pMsgBuf) (CRU_BUF_Get_InterfaceId(pMsgBuf))

#define BUF_FIRST_DATA_SIZE(pMsgBuf) \
        (pMsgBuf->pFirstDataDesc->u4_ValidByteCount)
#define BUF_FIRST_DATA_DESC(pMsgBuf) \
 (pMsgBuf->pFirstDataDesc)
#define BUF_LAST_DATA_DESC(pMsgBuf) \
 (pMsgBuf->pLastDataDesc)

#define BUF_GET_CHAIN_VALID_BYTE_COUNT(pBuf) \
          CRU_BUF_Get_ChainValidByteCount(pBuf)

#define BUF_CONCAT_CHAIN(pBuf1,pBuf2)  CRU_BUF_Concat_MsgBufChains(pBuf1,pBuf2)

#define BUF_DELETE_CHAIN_STARTING_DATA(pBuf, u4Size) \
        CRU_BUF_Move_ValidOffset(pBuf,u4Size)

#define BUF_COPY_OVER_CHAIN(pMsgBuf, pu1Data, u4Offset, u4Size) \
        CRU_BUF_Copy_OverBufChain(pMsgBuf, (UINT1 *) pu1Data,u4Offset,u4Size)

#define BUF_COPY_FROM_CHAIN(pMsgBuf, pu1Data, u4Offset, u4Size) \
        CRU_BUF_Copy_FromBufChain(pMsgBuf, (UINT1 *) pu1Data,u4Offset,u4Size)

/* Hashing */
#define tRSVP_HASH_TABLE  tTMO_HASH_TABLE
#define tRSVP_HASH_BUCKET tTMO_HASH_BUCKET
#define RsvpHashCreateTable(u4Size, pFunc, u1LockFlag) \
               TMO_HASH_Create_Table(u4Size, pFunc, u1LockFlag)

#define RsvpHashScanBucket(pHashTab,u4_HashIndex,pNodePtr,TypeCast) \
               TMO_HASH_Scan_Bucket(pHashTab,u4_HashIndex,pNodePtr,TypeCast)

#define RSVP_MEM_MALLOC(Size, Type)  MEM_MALLOC(Size, Type); ++gi4MemAlloc
#define RSVP_MEM_FREE(x)             --gi4MemAlloc; MEM_FREE(x)
#define MEM_COPY(Src, Dest, Size)  memcpy (Dest, Src, Size)

#define IP_RSVP_QUE          "IPRQ"
#define UDP_RSVP_QUE         "URSQ"
#define TC_RSVP_ADF_QUE      "TCAQ"
#define TC_RSVP_SDF_QUE      "TCSQ"
#define ROUTE_CHANGE_NOTIFICATION_QUE  "RTCQ"
#define IF_CHANGE_NOTIFICATION_QUE  "RIFQ"
#define RSVP_GBL_CSPF_QUE           "CSPQ"

#define TC_SNMP_SDF_QUE      "TSSQ"
#define TC_SNMP_ADF_QUE      "TSAQ"
#define RSVP_QUE             "RSPQ"

#define OR TMO_OR

#define RSVP_TIMER_EXPIRED_EVENT 0x00000001

#define RSVP_ALLOCATE_MEM_BLOCK(PoolId) \
   MemAllocMemBlk((tMemPoolId) PoolId)

#define RSVP_RELEASE_MEM_BLOCK(x,pu1Blk)\
   MemReleaseMemBlock((tMemPoolId)x,(UINT1 *)pu1Blk)

#define RSVPTE_DUMP PRINTF
#define RPTE_RAND   RAND

#endif /* _RPTEPORT_H */

/*---------------------------------------------------------------------------*/
/*                        End of file rpteport.h                             */
/*---------------------------------------------------------------------------*/
