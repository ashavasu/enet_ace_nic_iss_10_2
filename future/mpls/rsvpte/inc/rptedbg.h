/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptedbg.h,v 1.7 2016/02/03 10:37:55 siva Exp $
 *
 *----------------------------------------------------------------------------*/



/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptedbg.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains all the data structures and
 *                             definitions required for Debugging/Dumping 
 *                             in RSVP-TE.
 *---------------------------------------------------------------------------*/

#ifndef _RPTEDBG_H
#define _RPTEDBG_H
#include "utltrc.h"

/* Macros used in Debugging */
#define       RSVPTE_DBG_FLAG     RSVP_GBL_TE_DBG_FLAG
#define       RSVPTE_DBG_LVL     RSVP_GBL_TE_DBG_LVL 

/* Message Dumps related constants */
#define   RSVPTE_DUMP_NONE         0x00000000
#define   RSVPTE_DUMP_PATH         0x00000001
#define   RSVPTE_DUMP_PTEAR        0x00000002
#define   RSVPTE_DUMP_PERR         0x00000004
#define   RSVPTE_DUMP_RESV         0x00000008
#define   RSVPTE_DUMP_RTEAR        0x00000010
#define   RSVPTE_DUMP_RERR         0x00000020
#define   RSVPTE_DUMP_RCNFM        0x00000040
#define   RSVPTE_DUMP_HELLO        0x00000080
#define   RSVPTE_DUMP_BUNDLE       0x00000100
#define   RSVPTE_DUMP_ACK          0x00000200
#define   RSVPTE_DUMP_SREFRESH     0x00000400
#define   RSVPTE_DUMP_NOTIFY       0x00000800
#define   RSVPTE_DUMP_RECOVERY_PATH 0x00001000 
#define   RSVPTE_DUMP_ALL          0x00001fff

#define   RSVPTE_DUMP_LVL_NONE         0x00000000
#define   RSVPTE_DUMP_LVL_MIN          0x00000001
#define   RSVPTE_DUMP_LVL_MAX          0x00000002
#define   RSVPTE_DUMP_LVL_HDR          0x00000004

#define   RSVPTE_DUMP_DIR_NONE        0x00000000
#define   RSVPTE_DUMP_DIR_IN          0x00000001
#define   RSVPTE_DUMP_DIR_OUT         0x00000002
#define   RSVPTE_DUMP_DIR_INOUT       0x00000003

/* Values assigned to the sub-modules of RSVP and RSVPTE, for Logging purpose */
#define     RSVPTE_REOPT_DBG          0x80000000
#define     RSVPTE_GR_DBG             0x70000000
#define     RSVPTE_MAIN_DBG           0x20000000
#define     RSVPTE_IF_DBG             0x10000000
#define     RSVPTE_IPIF_DBG           0x10000000
#define     RSVPTE_PM_TCIF_DBG        0x10000000
#define     RSVPTE_PVM_DBG            0x08000000
#define     RSVPTE_PB_DBG             0x04000000
#define     RSVPTE_PH_DBG             0x02000000
#define     RSVPTE_RH_DBG             0x01000000
#define     RSVPTE_PE_DBG             0x00800000
#define     RSVPTE_PT_DBG             0x00400000
#define     RSVPTE_REH_DBG            0x00200000
#define     RSVPTE_RTH_DBG            0x00100000
#define     RSVPTE_RCH_DBG            0x00080000
#define     RSVPTE_HH_DBG             0x00040000
#define     RSVPTE_LLVL_DBG           0x00020000
#define     RSVPTE_UTL_STAT_DBG       0x00010000
#define     RSVPTE_PORT_DBG           0x00008000
#define     RSVPTE_DIFF_DBG           0x00004000
#define     RSVPTE_RR_DBG             0x00002000
#define     RSVPTE_MIH_TRIE_DBG       0x00001000
#define     RSVPTE_NBR_DBG            0x00000800
#define     RSVPTE_FRR_DBG            0x00000400
#define     RSVPTE_ALL_DBG            0xcffffc00

/* Log Types defined in RSVP, RSVPTE */
#define     RSVPTE_DBG_MEM     0x00000001
#define     RSVPTE_DBG_PRCS    0x00000002
#define     RSVPTE_DBG_MISC    0x00000004
#define     RSVPTE_DBG_ETEXT   0x00000008
#define     RSVPTE_DBG_SCSS    0x00000010
#define     RSVPTE_DBG_FAIL    0x00000020
#define     RSVPTE_DEBUG_ALL   0x0000003f
#define     RSVPTE_DEBUG_NONE  0x00000000

/* values used in RSVPTE_DBG depending on the categories under which 
 * the log message falls. */
#define     RSVPTE_DBG_ALL           (RSVPTE_ALL_DBG | RSVPTE_DEBUG_ALL)
#define     RSVPTE_DBG_DEF           (RSVPTE_DEBUG_NONE)

#define     RSVPTE_MAIN_MEM          (RSVPTE_MAIN_DBG | RSVPTE_DBG_MEM)
#define     RSVPTE_MAIN_MISC         (RSVPTE_MAIN_DBG | RSVPTE_DBG_MISC)
#define     RSVPTE_MAIN_ALL          (RSVPTE_MAIN_DBG | RSVPTE_DBG_ALL)

/* Debug values set for the debug support in low level routines */
#define     RSVPTE_LLVL_SET_SCSS     (RSVPTE_LLVL_DBG | RSVPTE_DBG_SCSS)
#define     RSVPTE_LLVL_SET_FAIL     (RSVPTE_LLVL_DBG | RSVPTE_DBG_SCSS)
#define     RSVPTE_LLVL_GET_SCSS     (RSVPTE_LLVL_DBG | RSVPTE_DBG_SCSS)
#define     RSVPTE_LLVL_GET_FAIL     (RSVPTE_LLVL_DBG | RSVPTE_DBG_SCSS)
#define     RSVPTE_LLVL_ALL          (RSVPTE_LLVL_DBG | RSVPTE_DBG_SCSS)
#define     RSVPTE_DBG_LLVL_ALL      (RSVPTE_LLVL_DBG | RSVPTE_DBG_SCSS)

/* RSVP-TE PVM Module */
#define   RSVPTE_PVM_PRCS      (RSVPTE_PVM_DBG | RSVPTE_DBG_PRCS)
#define   RSVPTE_PVM_MEM       (RSVPTE_PVM_DBG | RSVPTE_DBG_MEM)

/* RSVP-TE Path msg handler module */
#define   RSVPTE_PH_PRCS       (RSVPTE_PH_DBG | RSVPTE_DBG_PRCS)
#define   RSVPTE_PH_MEM        (RSVPTE_PH_DBG | RSVPTE_DBG_MEM) 

/* RSVP-TE Pkt Builder module */
#define   RSVPTE_PB_PRCS       (RSVPTE_PB_DBG | RSVPTE_DBG_PRCS)

/* RSVP-TE Path Err module */
#define   RSVPTE_PE_PRCS       (RSVPTE_PE_DBG | RSVPTE_DBG_PRCS)

/* RSVP-TE Interface module */
#define   RSVPTE_IF_PRCS       (RSVPTE_IF_DBG | RSVPTE_DBG_PRCS)

/* RSVP-TE Path Tear module */
#define   RSVPTE_PT_PRCS       (RSVPTE_PT_DBG | RSVPTE_DBG_PRCS)

/* RSVP-TE Path Tear module */
#define   RSVPTE_UTL_PRCS      (RSVPTE_UTL_STAT_DBG | RSVPTE_DBG_PRCS)

/* RSVP-TE Resv msg handler module */
#define   RSVPTE_RH_PRCS       (RSVPTE_RH_DBG | RSVPTE_DBG_PRCS)

/* RSVP_TE Resv Err Module  */
#define   RSVPTE_REH_PRCS      (RSVPTE_REH_DBG | RSVPTE_DBG_PRCS)

/* RSVP_TE Resv Tear Module */
#define   RSVPTE_RTH_PRCS      (RSVPTE_RTH_DBG | RSVPTE_DBG_PRCS)
#define   RSVPTE_RTH_MEM       (RSVPTE_RTH_DBG | RSVPTE_DBG_MEM)

/* RSVP_TE IPIF Module */
#define   RSVPTE_IPIF_PRCS     (RSVPTE_IPIF_DBG | RSVPTE_DBG_PRCS)

/* RSVP_TE TCIF Module */
#define   RSVPTE_TCIF_PRCS     (RSVPTE_PM_TCIF_DBG | RSVPTE_DBG_PRCS)

/* RSVP-TE Hello msg handler module */
#define   RSVPTE_HH_PRCS       (RSVPTE_HH_DBG | RSVPTE_DBG_PRCS)

/* RSVP-TE Resv Conf handler module */
#define   RSVPTE_RCH_PRCS      (RSVPTE_RCH_DBG | RSVPTE_DBG_PRCS)

/* RSVP-TE Pre-emption handler module */
#define   RSVPTE_PM_PRCS       (RSVPTE_PM_TCIF_DBG | RSVPTE_DBG_PRCS)

#define   RSVPTE_GR_PRCS       (RSVPTE_GR_DBG | RSVPTE_DBG_PRCS)
#define   RSVPTE_REOPT_PRCS    (RSVPTE_REOPT_DBG | RSVPTE_DBG_PRCS)

#define   RSVPTE_RR_PRCS        (RSVPTE_RR_DBG | RSVPTE_DBG_PRCS)
#define   RSVPTE_RR_MEM         (RSVPTE_RR_DBG | RSVPTE_DBG_MEM)
#define   RSVPTE_MIH_PRCS       (RSVPTE_MIH_TRIE_DBG | RSVPTE_DBG_PRCS)
#define   RSVPTE_MIH_MEM        (RSVPTE_MIH_TRIE_DBG | RSVPTE_DBG_MEM)
#define   RSVPTE_TRIE_PRCS      (RSVPTE_MIH_TRIE_DBG | RSVPTE_DBG_PRCS)

#define   RSVPTE_IPIF_IF        RSVPTE_IPIF_PRCS
#define   RSVPTE_PH_IF          RSVPTE_PH_PRCS

/* Module Specfic Entry and Exit  */
#define    RSVPTE_MAIN_ETEXT     (RSVPTE_MAIN_DBG     | RSVPTE_DBG_ETEXT)
#define    RSVPTE_PVM_ETEXT      (RSVPTE_PVM_DBG      | RSVPTE_DBG_ETEXT)
#define    RSVPTE_PH_ETEXT       (RSVPTE_PH_DBG       | RSVPTE_DBG_ETEXT)
#define    RSVPTE_PB_ETEXT       (RSVPTE_PB_DBG       | RSVPTE_DBG_ETEXT)
#define    RSVPTE_PE_ETEXT       (RSVPTE_PE_DBG       | RSVPTE_DBG_ETEXT)
#define    RSVPTE_IF_ETEXT       (RSVPTE_IF_DBG       | RSVPTE_DBG_ETEXT)
#define    RSVPTE_PT_ETEXT       (RSVPTE_PT_DBG       | RSVPTE_DBG_ETEXT)
#define    RSVPTE_UTL_ETEXT      (RSVPTE_UTL_STAT_DBG | RSVPTE_DBG_ETEXT)
#define    RSVPTE_RH_ETEXT       (RSVPTE_RH_DBG       | RSVPTE_DBG_ETEXT)
#define    RSVPTE_REH_ETEXT      (RSVPTE_REH_DBG      | RSVPTE_DBG_ETEXT)
#define    RSVPTE_RTH_ETEXT      (RSVPTE_RTH_DBG      | RSVPTE_DBG_ETEXT)
#define    RSVPTE_IPIF_ETEXT     (RSVPTE_IPIF_DBG     | RSVPTE_DBG_ETEXT)
#define    RSVPTE_TCIF_ETEXT     (RSVPTE_PM_TCIF_DBG  | RSVPTE_DBG_ETEXT)
#define    RSVPTE_HH_ETEXT       (RSVPTE_HH_DBG       | RSVPTE_DBG_ETEXT)
#define    RSVPTE_RCH_ETEXT      (RSVPTE_RCH_DBG      | RSVPTE_DBG_ETEXT)
#define    RSVPTE_PM_ETEXT       (RSVPTE_PM_TCIF_DBG  | RSVPTE_DBG_ETEXT)
#define    RSVPTE_PORT_ETEXT     (RSVPTE_PORT_DBG     | RSVPTE_DBG_ETEXT)
#define    RSVPTE_STAT_ETEXT     (RSVPTE_UTL_STAT_DBG | RSVPTE_DBG_ETEXT)
#define    RSVPTE_RR_ETEXT       (RSVPTE_RR_DBG       | RSVPTE_DBG_ETEXT)
#define    RSVPTE_MIH_ETEXT      (RSVPTE_MIH_TRIE_DBG | RSVPTE_DBG_ETEXT)
#define    RSVPTE_DIFF_ETEXT     (RSVPTE_DIFF_DBG     | RSVPTE_DBG_ETEXT)
#define    RSVPTE_TRIE_ETEXT     (RSVPTE_MIH_TRIE_DBG | RSVPTE_DBG_ETEXT)
#define    RSVPTE_GR_ETEXT       (RSVPTE_GR_DBG       | RSVPTE_DBG_ETEXT)
#define    RSVPTE_REOPT_ETEXT  (RSVPTE_REOPT_DBG    | RSVPTE_DBG_ETEXT)

#define RSVPTE_DBG(u4Value, pu1Format)     \
           if((u4Value) == (u4Value & RSVPTE_DBG_FLAG)) \
               UtlTrcLog  (RSVPTE_DBG_FLAG, u4Value, "RSVPTE", pu1Format)

#define RSVPTE_DBG1(u4Value, pu1Format, Arg1)     \
           if((u4Value) == (u4Value & RSVPTE_DBG_FLAG)) \
               UtlTrcLog  (RSVPTE_DBG_FLAG, u4Value, "RSVPTE", pu1Format, Arg1)

#define RSVPTE_DBG2(u4Value, pu1Format, Arg1, Arg2)     \
           if((u4Value) == (u4Value & RSVPTE_DBG_FLAG)) \
               UtlTrcLog  (RSVPTE_DBG_FLAG, u4Value, "RSVPTE", pu1Format, Arg1, Arg2)

#define RSVPTE_DBG3(u4Value, pu1Format, Arg1, Arg2, Arg3)     \
           if((u4Value) == (u4Value & RSVPTE_DBG_FLAG)) \
               UtlTrcLog  (RSVPTE_DBG_FLAG, u4Value, "RSVPTE", pu1Format, Arg1, Arg2, Arg3)
#define RSVPTE_DBG4(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4)     \
           if((u4Value) == (u4Value & RSVPTE_DBG_FLAG)) \
               UtlTrcLog  (RSVPTE_DBG_FLAG, u4Value, "RSVPTE", pu1Format, Arg1, Arg2, Arg3, Arg4)

#define RSVPTE_DBG5(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)   \
           if((u4Value) == (u4Value & RSVPTE_DBG_FLAG)) \
               UtlTrcLog  (RSVPTE_DBG_FLAG, u4Value, "RSVPTE", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)

#define RSVPTE_DBG6(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
           if((u4Value) == (u4Value & RSVPTE_DBG_FLAG)) \
               UtlTrcLog  (RSVPTE_DBG_FLAG, u4Value, "RSVPTE", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)


#define RSVPTE_DUMP_MSG(Buf, Type, Direction, Length, HdrFlag)  \
           if(((Type) == (Type & RSVPTE_DUMP_TYPE)) && ((Direction) == (Direction & RSVPTE_DUMP_DIR)) ) \
            UtlDmpMsg(RSVPTE_DUMP_TYPE, RSVPTE_DUMP_DIR, Buf, Type, Direction, \
                      Length, HdrFlag)

#endif /*_RPTEDBG_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file rptedbg.h                              */
/*---------------------------------------------------------------------------*/
