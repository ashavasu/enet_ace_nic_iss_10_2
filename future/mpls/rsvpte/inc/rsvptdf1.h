
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rsvptdf1.h,v 1.11 2015/08/10 09:19:41 siva Exp $
 *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rsvptdf1.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the structure type
 *                             definitions declared and used for RSVP-TE
 *                             funcitonality. These declarations are used
 *                             by RSVP in rsvptdf2.h file, as well as RSVPTE
 *                             header file rptetdf2.h 
 *---------------------------------------------------------------------------*/

#ifndef _RSVPTDF1_H
#define _RSVPTDF1_H

#include "ospfte.h"
typedef struct CommonHeader
{
    UINT1               u1VerFlag;
    UINT1               u1MsgType;
    UINT2               u2CheckSum;
    UINT1               u1SendTtl;
    UINT1               u1Reserved;
    UINT2               u2Length;
}
tRsvpHdr;

typedef struct ObjectHeader
{
    UINT2               u2Length;
    UINT2               u2ClassNumType;
}
tObjHdr;

typedef struct RsvpHop
{
    UINT4               u4HopAddr;
    UINT4               u4Lih;
}
tRsvpHop;

typedef struct RsvpHopObj
{
    tObjHdr             ObjHdr;
    tRsvpHop            RsvpHop;
}
tRsvpHopObj;

typedef struct TimeValues
{
    UINT4               u4RefreshPeriod;
}
tTimeValues;

typedef struct TimeValuesObj
{
    tObjHdr             ObjHdr;
    tTimeValues         TimeValues;
}
tTimeValuesObj;

typedef struct ErrorSpec
{
    UINT4               u4ErrNodeAddr;
    UINT1               u1Flags;
    UINT1               u1ErrCode;
    UINT2               u2ErrValue;
}
tErrorSpec;

typedef struct ErrorSpecObj
{
    tObjHdr             ObjHdr;
    tErrorSpec          ErrorSpec;
}
tErrorSpecObj;

/* As per RFC 3471 section 9.1.1,
 * The below structure denotes the If-Id RSVP Numbered Hop object
 * TLV information 
 * 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Type  (1)                     | Length (4)                    |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                        IP Address                             |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
typedef struct IfIdRsvpNumTlvObj
{
    UINT2               u2Type;
    UINT2               u2LengthValue;
    UINT4               u4HopAddr;
}tIfIdRsvpNumTlvObj;

/* As per RFC 3471 section 9.1.1,
 * The below structure denotes the If-Id RSVP Unnumbered Hop object
 * TLV information 
 * 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Type (3)                      | Length (8)                    |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                        IP Address                             |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                       Interface ID                            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
typedef struct IfIdRsvpTlvObj
{
    UINT2               u2Type;
    UINT2               u2LengthValue;
    UINT4               u4HopAddr;
    UINT4               u4InterfaceId;
}tIfIdRsvpTlvObj;

/* As per RFC 3473 section 8.1,
 * The below structure denotes the If-Id RSVP Num Hop object TLV 
 * information 
 * 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Length                      | Class-Num (3) | C-Type (3)      |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |            IPv4 Next/Previous Hop Address                     |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |               Logical Interface Handle                        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * .                       TLVs .
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+*/
typedef struct IfIdRsvpNumHopObj
{
   tObjHdr             ObjHdr;
   tRsvpHop            RsvpHop;
   tIfIdRsvpNumTlvObj  RsvpTlvNumObjHdr;
}tIfIdRsvpNumHopObj;

/* As per RFC 3473 section 8.1,
 * The below structure denotes the If-Id RSVP Unnum Hop object
 * information 
 * 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Length                      | Class-Num (3) | C-Type (3)      |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |            IPv4 Next/Previous Hop Address                     |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |               Logical Interface Handle                        |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * .                       TLVs                                    .
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
typedef struct IfIdRsvpHopObj
{
    tObjHdr             ObjHdr;
    tRsvpHop            RsvpHop;
    tIfIdRsvpTlvObj     RsvpTlvObjHdr;
}tIfIdRsvpHopObj;

/* As per RFC 3473 section 8.2,
 * The below structure denotes the If-Id RSVP Num Error Spec object 
 * information 
 * 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Length                      | Class-Num (6) | C-Type (3) |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                     IPv4 Error Node Address                   |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Flags       | Error Code    | Error Value                     |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 * .                            TLVs .
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+*/
typedef struct IfIdRsvpNumErrObj 
{
     tObjHdr           ObjHdr;
     tErrorSpec          ErrorSpec;
     tIfIdRsvpNumTlvObj  ErrorRsvpNumTlvObj;
}
tIfIdRsvpNumErrObj;

/* As per RFC 3473 section 8.2,
 * The below structure denotes the If-Id RSVP Error Spec object 
 * information 
 * 0                   1                   2                   3
 * 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Length                      | Class-Num (6) | C-Type (3) |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                     IPv4 Error Node Address                   |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * | Flags       | Error Code    | Error Value                     |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * |                                                               |
 *.                            TLVs .
 * |                                                               |
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+*/
typedef struct IfIdRsvpErrObj 
{
     tObjHdr           ObjHdr;
     tErrorSpec          ErrorSpec;
     tIfIdRsvpTlvObj     ErrorRsvpTlvObj;
}
tIfIdRsvpErrObj;

/* The below structure denotes the Admin status information */
typedef struct AdminStatusValue
{
    UINT4      u4AdminStatus;
}
tAdminStatus;

/* As per RFC 3473 section 7.1,
 * The below structure denotes the Admin status object information */
typedef struct AdminStatusObj
{
    tObjHdr         ObjHdr;
    tAdminStatus    AdminStatus;
}
tAdminStatusObj;

/* The below structure denotes the Label set information */
typedef struct LabelSet
{
    UINT1               u1Action;
    UINT1               u1Resvd;
    UINT2               u2LblType;
    UINT4               u4SubChannel1;
    UINT4               u4SubChannel2;
}
tLabelSet;

/* As per RFC 3473 section 2.6,
 * The below structure denotes the Label set object information */
typedef struct LabelSetObj
{
    tObjHdr             ObjHdr;
    tLabelSet           LabelSet;
}
tLabelSetObj;

/*As per RFC 3473 section 4.2
 * The below structure denotes the Notification request information */

/*  0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |            Length             | Class-Num (1) |  C-Type (1)   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    IPv4 Notify Node Address                   |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

typedef struct NotifyRequestObj
{
    tObjHdr         ObjHdr;
    UINT4           u4NotifyNodeAddr;
}
tNotifyRequestObj;

    
/*   RFC:6107 Sec: 3.1.2.
 *   The below structure denotes the LSP_TUNNEL_INTERFACE_ID Object */

/*  0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                        LSR's Router ID                        |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                    Interface ID (32 bits)                     |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |  Actions    |                Reserved                         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   ~                         TLVs                                  ~
*/

/* In Future, tLspTnlIfId can be extended to have different TLVs as per RFC */
typedef struct LspTnlIfId
{
    UINT4               u4RouterId;
    UINT4               u4InterfaceId;
    UINT1               u1Actions;
    UINT1               u1Resv[3];
}tLspTnlIfId;


/*As per RFC 3477 section 3.1
 * The below structure denotes the LSP Tunnel ID information */

typedef struct LspTnlIfIdObj
{
    tObjHdr             ObjHdr;
    tLspTnlIfId         LspTnlIfId;
}tLspTnlIfIdObj;

/*     0                   1                   2                   3
      0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |            Length             | Class-Num(37) | C-Type (2)    |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |S|P|N|O| Reserved  | LSP Flags |     Reserved      | Link Flags|
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
     |                           Reserved                            |
     +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

typedef struct _Protection{
    UINT1           u1ProtectionFlag;
    UINT1           u1LspFlag;
    UINT1           u1Resv1;
    UINT1           u1LinkFlag;
    UINT4           u4Resv2;
}
tProtection;

/*As per RFC 4872 section 14.1
 *The below structure denotes the protectioin information */
typedef struct _ProtectionObj
{
    tObjHdr             ObjHdr;
    tProtection         Protection;
}
tProtectionObj;

/*     0                   1                   2                   3
     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |            Length             | Class-Num(199)|  C-Type (1)   |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |       Association Type        |       Association ID          |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    |                  IPv4 Association Source                      |
    +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

typedef struct _Association{
    UINT2               u2AssocType;
    UINT2               u2AssocID;
    UINT4               u4AssocSource;
}
tAssociation;

/*As per RFC 4872 section 16.1
 *The below structure denotes the Association information */
typedef struct _AssociationObj
{
    tObjHdr             ObjHdr;
    tAssociation        Association;
}
tAssociationObj;

typedef struct Scope
{
    UINT4               au4Addr[1];
}
tScope;

typedef struct ScopeObj
{
    tObjHdr             ObjHdr;
    tScope              Scope;
}
tScopeObj;

typedef struct Style
{
    UINT1               u1Flags;
    UINT1               au1OptVect[3];
}
tStyle;

typedef struct StyleObj
{
    tObjHdr             ObjHdr;
    tStyle              Style;
}
tStyleObj;

typedef tFilterSpec tSenderTemplate;

typedef struct FilterSpecObj
{
    tObjHdr             ObjHdr;
    tFilterSpec         FilterSpec;
}
tFilterSpecObj;

typedef struct SenderTemplateObj
{
    tObjHdr             ObjHdr;
    tSenderTemplate     SenderTemplate;
}
tSenderTemplateObj;

typedef struct ResvConfirm
{
    UINT4               u4RecevAddr;
}
tResvConf;

typedef struct ResvConfirmObj
{
    tObjHdr             ObjHdr;
    tResvConf           ResvConf;
}
tResvConfObj;

typedef struct SenderTspecObj
{
    tObjHdr             ObjHdr;
    tSenderTspec        SenderTspec;
}
tSenderTspecObj;

typedef struct FlowSpecificationObj
{
    tObjHdr             ObjHdr;
    tFlowSpec           FlowSpec;
}
tFlowSpecObj;

typedef UINT4       tCtrlLoadSrvSpec;

typedef struct AdSpecObj
{
    tObjHdr             ObjHdr;
    tAdSpec             AdSpec;
    
}
tAdSpecObj;

typedef struct AdSpecWithoutGsAndClsObj
{
    tObjHdr                ObjHdr;
    tAdSpecWithoutGsAndCls AdSpec;
    tCtrlLoadSrvSpec       CtrlLoadSrvSpec;
}
tAdSpecWithoutGsAndClsObj;

typedef struct _MsgIdObj {
    UINT1                 u1Flags;
    UINT1                 u1IsMsgIdPresent;
    UINT2                 u2Rsvd;
    UINT4                 u4Epoch;
    UINT4                 u4MsgId;
}
tMsgIdObj; 
/* Structure tMsgIdObj defines the Message Id Object 
 * used for local storage */

typedef struct _MsgIdObjNode {
    tTMO_SLL_NODE         NextMsgIdObjNode;
    tMsgIdObj             MsgIdObj;
}
tMsgIdObjNode;

typedef struct _MsgIdListObj {
    tObjHdr               ObjHdr;
    UINT4                 u4Epoch;
    UINT4                 au4MsgId[1];
}
tMsgIdListObj; /* This structure holds the Message Id List Object */

typedef struct TimerParameters
{
    union
    {
        struct IfEntry     *pIfEntry;
        struct PathStateBlock *pPsb;
        struct ReserveStateBlock *pRsb;
        struct _RsvpTeTnlInfo *pRsvpTeTnlInfo;
        struct _RsvpTeGblInfo *pRsvpTeGblInfo;
        struct NeighbourEntry *pNbrEntry;
        struct NotifyRecipient *pNotifyRecipient;
    }
    u;
    UINT1               u1Id;
    UINT1               u1Rsvd;
    UINT2               u2Rsvd;
}
tTimerParam;

typedef struct TimerBlock
{
    tTmrAppTimer        Timer;
    tTimerParam         TmrParam;
    tErrorSpec          ErrorSpec;
    UINT4               u4MsgId;
    UINT4               u4BackOffInterval;
    UINT2               u2Rsvd;
    UINT1               u1TxTTL;
    UINT1               u1Rsvd;
}
tTimerBlock;

typedef struct TrieInfo
{
    union
    {
        tTimerBlock            *pTimerBlock;
        struct _RsvpTeTnlInfo  *pRsvpTeTnlInfo;
    }
    u;
    UINT2               u2Rsvd;
    UINT1               u1MsgType;
    UINT1               u1Rsvd;
}
tuTrieInfo;
typedef struct RpteCspfInfo{
    tCspfCompInfo CspfCompInfo;
    tOsTeAppPath  AppPath;
}tRpteCspfInfo;

#endif /* _RSVPTDF1_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rsvptdf1.h                             */
/*---------------------------------------------------------------------------*/
