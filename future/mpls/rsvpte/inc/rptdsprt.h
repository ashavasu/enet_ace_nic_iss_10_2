/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptdsprt.h,v 1.4 2018/01/03 11:31:22 siva Exp $
 *
 * Description: This file contains the function prototypes Used for 
 *              Support of DIFFSERV in Rsvpte Module. 
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptdsprt.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains prototype definitions for DiffServ
 *                             support in RSVP-TE
 *---------------------------------------------------------------------------*/

#ifndef _RPTEDSPROT_H
#define _RPTEDSPROT_H

INT1 RptePvmValidateDSObj ARG_LIST ((tPktMap *pPktMap,tObjHdr *pObjHdr));

UINT1 RpteValidateDSPathMsgObjs ARG_LIST ((tPktMap *pPktMap));

VOID RpteUpdateDSPathObjsSize ARG_LIST ((const tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                         UINT2 *pu2PathSize));

VOID RpteUpdateDSPathObjs ARG_LIST ((const tRsvpTeTnlInfo *pRsvpTeTnlInfo ,
                                     tObjHdr **ppObjHdr));

UINT1 RpteUpdateDSElspTpObj ARG_LIST ((const tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                       tObjHdr **ppObjHdr));

UINT1 RpteCopyDiffServObjsToTnlInfo ARG_LIST ((tRsvpTeTnlInfo *pRsvpTeTnlInfo,
                                              tPktMap        *pPktMap));

UINT1 rpteDiffServValidateElspTPPsc ARG_LIST ((UINT2    u2Psc, 
                                 tMplsDiffServElspInfo *pMplsDiffServElspInfo));

VOID RpteUpdateDSResvObjsSize ARG_LIST ((const tRsvpTeTnlInfo *pRsvpTeTnlInfo ,
                                         UINT2 *pu2ResvSize));

VOID RpteUtlDSInitPktMap ARG_LIST ((tPktMap *pPktMap));

UINT1 RpteRhDSUpdateTrafficControl ARG_LIST ((tRsvpTeTnlInfo *pRsvpTeTnlInfo,UINT1 *pu1ResvRefreshNeeded,tErrorSpec *pErrorSpec));

UINT1 RptePmDSGetTnlFromPrioList ARG_LIST ((tRsvpTeTnlInfo *pCtTnlInfo,
                                           UINT4          *pau4PeakdataRate,
                                           tTMO_SLL     *pCandidatePreemp));

VOID RpteDSRelTrafficControl ARG_LIST ((tRsvpTeTnlInfo *pRsvpTeTnlInfo));

UINT1 RpteDiffServValidateDscp ARG_LIST ((UINT1 u1Dscp));

UINT2 RpteDiffServGetPscIndex (UINT1 u1Dscp);

UINT1 RpteValidateDSObjs (const tRsvpTeTnlInfo *pRsvpTeTnlInfo);

UINT1 RpteValidateSupprtdDSObjs (const tRsvpTeTnlInfo * pRsvpTeTnlInfo,
                                 tIfEntry * pIfEntry, tPktMap * pPktMap);
UINT1
RpteUtlFetchTnlInfoFromTnlTable (UINT4 u4TunnelIndex,
        tRsvpTeTnlInfo * pRpteInTnlInfo);

#endif/* _RPTEDSPROT_H */
/*---------------------------------------------------------------------------*/
/*                        End of file rptdsprt.h                             */
/*---------------------------------------------------------------------------*/
