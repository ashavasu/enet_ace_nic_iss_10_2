
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: rptetdf1.h,v 1.9 2012/10/24 10:44:18 siva Exp $
 *              
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : rptetdf1.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the structure type
 *                             definitions declared and used for RSVP-TE
 *                             funcitonality. These declarations are used
 *                             by RSVP in rsvptdf2.h file, as well as RSVPTE
 *                             header file rptetdf2.h 
 *---------------------------------------------------------------------------*/

#ifndef _RPTETDF1_H
#define _RPTETDF1_H

#include "rsvptdf1.h"

/* Ssn Supports IPV4 only */
typedef struct _RsvpTeSession
{
    tIpv4Addr           TnlDestAddr;
    UINT2               u2Rsvd;
    UINT2               u2TnlId;
    tIpv4Addr           ExtnTnlId;
}
tRsvpTeSession;

typedef struct _RsvpTeSsnObj
{
    tObjHdr             ObjHdr;
    tRsvpTeSession      RsvpTeSession;
}
tRsvpTeSsnObj;

/* Fltr Spec Supports IPV4 only */
typedef struct _RsvpTeFilterSpec
{
    tIpv4Addr           TnlSndrAddr;
    UINT2               u2Rsvd;
    UINT2               u2LspId;
}
tRsvpTeFilterSpec;

typedef struct _RsvpTeFilterSpecObj
{
    tObjHdr             ObjHdr;
    tRsvpTeFilterSpec   RsvpTeFilterSpec;
}
tRsvpTeFilterSpecObj;

typedef tRsvpTeFilterSpec tRsvpTeSndrTmp;

typedef struct _RsvpTeSndrTmpObj
{
    tObjHdr             ObjHdr;
    tRsvpTeSndrTmp      RsvpTeSndrTmp;
}
tRsvpTeSndrTmpObj;

typedef struct _RAParam
{
    UINT4               u4ExAnyAttr;
    UINT4               u4IncAnyAttr;
    UINT4               u4IncAllAttr;
}
tRAParam;

typedef struct _RsvpTeFastReroute
{
    UINT1               u1SetPrio;
    UINT1               u1HoldPrio;
    UINT1               u1HopLmt;
    UINT1               u1Flags;
    FLOAT               fBandwth;
    tRAParam            RAParam;
}
tRsvpTeFastReroute;

typedef struct _RsvpTeFastRerouteObj
{
    tObjHdr             ObjHdr;
    tRsvpTeFastReroute  RsvpTeFastReroute;
}
tRsvpTeFastRerouteObj;

typedef struct _RsvpTeDetour
{
    UINT4               au4PlrId[RPTE_MAX_DETOUR_IDS];
    UINT4               au4AvoidNodeId[RPTE_MAX_DETOUR_IDS];
}
tRsvpTeDetour;

typedef struct _RsvpTeDetourObj
{
    tObjHdr             ObjHdr;
    tRsvpTeDetour       RsvpTeDetour;
}
tRsvpTeDetourObj;
/* As per RFC 3473 section 2.3,
 * The below structure contains the information about
 * Genric Label Object

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| Length                       | Class-Num (16)| C-Type (2)     |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                          Label                                |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

 Same structure is used for different label types (class types)*/

typedef struct _GenLblObj
{
    tObjHdr             ObjHdr;
    uLabel              Label;
}
tGenLblObj;

/* As per RFC 3473, section 9,
 * The below structure contains the information about
 * Recovery Label object */

typedef struct _RecoveryLblObj
{
    tObjHdr             ObjHdr;
    uLabel              Label;
}
tRecoveryLblObj;

/* As per RFC 3473, section 2.5,
 * The below structure contains the information about
 * Suggested Label object */

typedef struct _SuggestedLblObj
{
    tObjHdr             ObjHdr;
    uLabel              Label;
}
tSuggestedLblObj;


/* As per RFC 3473 section 2.1,
 * The below structure contains the information about 
 * Genric Label Request Object 

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| Length                       | Class-Num (19)| C-Type (4)     |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| LSP Enc. Type |Switching Type|            G-PID               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+ 
 
 Same structure is used for different label request types (class types)*/

typedef struct _GenLblReq
{
    UINT1               u1EncType;
    UINT1               u1SwitchingType;
    UINT2               u2GPid;
}
tGenLblReq;

typedef struct _Hello
{
    UINT4               u4SrcIns;
    UINT4               u4DstIns;
}
tHello;
typedef struct _AtmRsvpLabelRange
{
    UINT2               u2LBVpi;
    UINT2               u2LBVci;
    UINT2               u2UBVpi;
    UINT2               u2UBVci;
}
tAtmRsvpLblRngEntry;

typedef struct _LblReqAtmRngObj
{
    tObjHdr               ObjHdr;
    tGenLblReq            GenLblReq;
    tAtmRsvpLblRngEntry   AtmLblRngEntry;
}
tLblReqAtmRngObj;

/* As per RFC 3473, section 2.1
 * The below structure contains Genric Label Request Object*/
typedef struct _GenLblReqObj
{
    tObjHdr            ObjHdr;
    tGenLblReq         GenLblReq;
}
tGenLblReqObj;

typedef struct _HelloObj
{
    tObjHdr             ObjHdr;
    tHello              Hello;
}
tHelloObj;

/* As per RFC 3473, section 9.1,
 * The below structure contains the information about
 * Restart_Cap object 

0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
| Length                        | Class-Num(131) | C-Type (1) |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|               Restart Time                                  |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
|               Recovery Time                                 |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

typedef struct _RestartCap
{
    UINT4               u4RestartTime;
    UINT4               u4RecoveryTime;
}
tRestartCap;

typedef struct _RestartCapObj
{
    tObjHdr             ObjHdr;
    tRestartCap         RestartCap;
}
tRestartCapObj;

/* As per RFC 5063, section 4.2,
 * The below structure contains the information about
 * Capability object
 *
0                     1                 2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
| Length                        | Class-Num(134) | C-Type (1) |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
| Reserved                                              |T|R|S|
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-
*/

typedef struct _Capability
{
    UINT4               u4RecoveryPathCapable;
}
tCapability;

typedef struct _CapabilityObj
{
    tObjHdr             ObjHdr;
    tCapability         Capability;
}
tCapabilityObj;

typedef struct _ErObj
{
    tObjHdr             ObjHdr;
  tTMO_SLL            ErHopList;
}
tErObj;

typedef struct _RrObj
{
    tObjHdr             ObjHdr;
    tTMO_SLL            RrHopList;
    tTMO_SLL            NewSubObjList;
}
tRrObj;

typedef struct _SsnAttr
{
    UINT1               u1SetPrio;
    UINT1               u1HoldPrio;
    UINT1               u1Flags;
    UINT1               u1NameLen;
    UINT1               au1SsnName[LSP_TNLNAME_LEN];
}
tSsnAttr;

typedef struct _RASsnAttr
{
    tRAParam            RAParam;
    tSsnAttr            SsnAttr;
}
tRASsnAttr;

typedef struct _SsnAttrObj
{
    tObjHdr             ObjHdr;
    tSsnAttr            SsnAttr;
}
tSsnAttrObj;

typedef struct _RASsnAttrObj
{
    tObjHdr             ObjHdr;
    tRASsnAttr          RASsnAttr;
}
tRASsnAttrObj;

/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */

typedef struct _RpteDefPktSize
{
    UINT1               au1RpteDefPkt[RPTE_DEF_PKT_SIZE];
}
tRpteDefPktSize;

#endif /*_RPTETDF1_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file rptetdf1.h                             */
/*---------------------------------------------------------------------------*/
