/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsrsvpdb.h,v 1.18 2018/01/03 11:31:22 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSRSVPDB_H
#define _FSRSVPDB_H

UINT1 FsMplsRsvpTeIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMplsRsvpTeIfStatsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMplsRsvpTeNbrTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_IP_ADDR_PRIM};
UINT1 FsMplsL3VpnRsvpTeMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};


UINT4 fsrsvp [] ={1,3,6,1,4,1,2076,13,2};
tSNMP_OID_TYPE fsrsvpOID = {9, fsrsvp};


UINT4 FsMplsRsvpTeIfIndex [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,1};
UINT4 FsMplsRsvpTeIfLblSpace [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,2};
UINT4 FsMplsRsvpTeIfLblType [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,3};
UINT4 FsMplsRsvpTeAtmMergeCap [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,4};
UINT4 FsMplsRsvpTeAtmVcDirection [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,5};
UINT4 FsMplsRsvpTeAtmMinVpi [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,6};
UINT4 FsMplsRsvpTeAtmMinVci [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,7};
UINT4 FsMplsRsvpTeAtmMaxVpi [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,8};
UINT4 FsMplsRsvpTeAtmMaxVci [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,9};
UINT4 FsMplsRsvpTeIfMtu [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,10};
UINT4 FsMplsRsvpTeIfUdpNbrs [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,11};
UINT4 FsMplsRsvpTeIfIpNbrs [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,12};
UINT4 FsMplsRsvpTeIfNbrs [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,13};
UINT4 FsMplsRsvpTeIfRefreshMultiple [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,14};
UINT4 FsMplsRsvpTeIfTTL [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,15};
UINT4 FsMplsRsvpTeIfRefreshInterval [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,16};
UINT4 FsMplsRsvpTeIfRouteDelay [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,17};
UINT4 FsMplsRsvpTeIfUdpRequired [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,18};
UINT4 FsMplsRsvpTeIfHelloSupported [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,19};
UINT4 FsMplsRsvpTeIfLinkAttr [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,20};
UINT4 FsMplsRsvpTeIfStatus [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,21};
UINT4 FsMplsRsvpTeIfPlrId [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,22};
UINT4 FsMplsRsvpTeIfAvoidNodeId [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,23};
UINT4 FsMplsRsvpTeIfStorageType [ ] ={1,3,6,1,4,1,2076,13,2,1,1,1,24};
UINT4 FsMplsRsvpTeIfNumTnls [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,1};
UINT4 FsMplsRsvpTeIfNumMsgSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,2};
UINT4 FsMplsRsvpTeIfNumMsgRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,3};
UINT4 FsMplsRsvpTeIfNumHelloSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,4};
UINT4 FsMplsRsvpTeIfNumHelloRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,5};
UINT4 FsMplsRsvpTeIfNumPathErrSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,6};
UINT4 FsMplsRsvpTeIfNumPathErrRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,7};
UINT4 FsMplsRsvpTeIfNumPathTearSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,8};
UINT4 FsMplsRsvpTeIfNumPathTearRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,9};
UINT4 FsMplsRsvpTeIfNumResvErrSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,10};
UINT4 FsMplsRsvpTeIfNumResvErrRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,11};
UINT4 FsMplsRsvpTeIfNumResvTearSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,12};
UINT4 FsMplsRsvpTeIfNumResvTearRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,13};
UINT4 FsMplsRsvpTeIfNumResvConfSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,14};
UINT4 FsMplsRsvpTeIfNumResvConfRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,15};
UINT4 FsMplsRsvpTeIfNumBundleMsgSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,16};
UINT4 FsMplsRsvpTeIfNumBundleMsgRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,17};
UINT4 FsMplsRsvpTeIfNumSRefreshMsgSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,18};
UINT4 FsMplsRsvpTeIfNumSRefreshMsgRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,19};
UINT4 FsMplsRsvpTeIfNumPathSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,20};
UINT4 FsMplsRsvpTeIfNumPathRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,21};
UINT4 FsMplsRsvpTeIfNumResvSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,22};
UINT4 FsMplsRsvpTeIfNumResvRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,23};
UINT4 FsMplsRsvpTeIfNumNotifyMsgSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,24};
UINT4 FsMplsRsvpTeIfNumNotifyMsgRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,25};
UINT4 FsMplsRsvpTeIfNumRecoveryPathSent [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,26};
UINT4 FsMplsRsvpTeIfNumRecoveryPathRcvd [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,27};
UINT4 FsMplsRsvpTeIfNumPathSentWithRecoveryLbl [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,28};
UINT4 FsMplsRsvpTeIfNumPathRcvdWithRecoveryLbl [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,29};
UINT4 FsMplsRsvpTeIfNumPathSentWithSuggestedLbl [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,30};
UINT4 FsMplsRsvpTeIfNumPathRcvdWithSuggestedLbl [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,31};
UINT4 FsMplsRsvpTeIfNumHelloSentWithRestartCap [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,32};
UINT4 FsMplsRsvpTeIfNumHelloRcvdWithRestartCap [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,33};
UINT4 FsMplsRsvpTeIfNumHelloSentWithCapability [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,34};
UINT4 FsMplsRsvpTeIfNumHelloRcvdWithCapability [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,35};
UINT4 FsMplsRsvpTeIfNumPktDiscrded [ ] ={1,3,6,1,4,1,2076,13,2,1,2,1,36};
UINT4 FsMplsRsvpTeNbrIfAddr [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,1};
UINT4 FsMplsRsvpTeNbrRRCapable [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,2};
UINT4 FsMplsRsvpTeNbrRRState [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,3};
UINT4 FsMplsRsvpTeNbrRMDCapable [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,4};
UINT4 FsMplsRsvpTeNbrEncapsType [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,5};
UINT4 FsMplsRsvpTeNbrHelloSupport [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,6};
UINT4 FsMplsRsvpTeNbrHelloState [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,7};
UINT4 FsMplsRsvpTeNbrHelloRelation [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,8};
UINT4 FsMplsRsvpTeNbrSrcInstInfo [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,9};
UINT4 FsMplsRsvpTeNbrDestInstInfo [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,10};
UINT4 FsMplsRsvpTeNbrCreationTime [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,11};
UINT4 FsMplsRsvpTeNbrLclRprDetectionTime [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,12};
UINT4 FsMplsRsvpTeNbrNumTunnels [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,13};
UINT4 FsMplsRsvpTeNbrStatus [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,14};
UINT4 FsMplsRsvpTeNbrGrRecoveryPathCapability [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,15};
UINT4 FsMplsRsvpTeNbrGrRestartTime [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,16};
UINT4 FsMplsRsvpTeNbrGrRecoveryTime [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,17};
UINT4 FsMplsRsvpTeNbrGrProgressStatus [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,18};
UINT4 FsMplsRsvpTeNbrStorageType [ ] ={1,3,6,1,4,1,2076,13,2,1,3,1,19};
UINT4 FsMplsRsvpTeLsrID [ ] ={1,3,6,1,4,1,2076,13,2,2,1};
UINT4 FsMplsRsvpTeMaxTnls [ ] ={1,3,6,1,4,1,2076,13,2,2,2};
UINT4 FsMplsRsvpTeMaxErhopsPerTnl [ ] ={1,3,6,1,4,1,2076,13,2,2,3};
UINT4 FsMplsRsvpTeMaxActRoutePerTnl [ ] ={1,3,6,1,4,1,2076,13,2,2,4};
UINT4 FsMplsRsvpTeMaxIfaces [ ] ={1,3,6,1,4,1,2076,13,2,2,5};
UINT4 FsMplsRsvpTeMaxNbrs [ ] ={1,3,6,1,4,1,2076,13,2,2,6};
UINT4 FsMplsRsvpTeSockSupprtd [ ] ={1,3,6,1,4,1,2076,13,2,2,7};
UINT4 FsMplsRsvpTeHelloSupprtd [ ] ={1,3,6,1,4,1,2076,13,2,2,8};
UINT4 FsMplsRsvpTeHelloIntervalTime [ ] ={1,3,6,1,4,1,2076,13,2,2,9};
UINT4 FsMplsRsvpTeRRCapable [ ] ={1,3,6,1,4,1,2076,13,2,2,10};
UINT4 FsMplsRsvpTeMsgIdCapable [ ] ={1,3,6,1,4,1,2076,13,2,2,11};
UINT4 FsMplsRsvpTeRMDPolicyObject [ ] ={1,3,6,1,4,1,2076,13,2,2,12};
UINT4 FsMplsRsvpTeGenLblSpaceMinLbl [ ] ={1,3,6,1,4,1,2076,13,2,2,13};
UINT4 FsMplsRsvpTeGenLblSpaceMaxLbl [ ] ={1,3,6,1,4,1,2076,13,2,2,14};
UINT4 FsMplsRsvpTeGenDebugFlag [ ] ={1,3,6,1,4,1,2076,13,2,2,15};
UINT4 FsMplsRsvpTeGenPduDumpLevel [ ] ={1,3,6,1,4,1,2076,13,2,2,16};
UINT4 FsMplsRsvpTeGenPduDumpMsgType [ ] ={1,3,6,1,4,1,2076,13,2,2,17};
UINT4 FsMplsRsvpTeGenPduDumpDirection [ ] ={1,3,6,1,4,1,2076,13,2,2,18};
UINT4 FsMplsRsvpTeOperStatus [ ] ={1,3,6,1,4,1,2076,13,2,2,19};
UINT4 FsMplsRsvpTeOverRideOption [ ] ={1,3,6,1,4,1,2076,13,2,2,20};
UINT4 FsMplsRsvpTeMinTnlsWithMsgId [ ] ={1,3,6,1,4,1,2076,13,2,2,21};
UINT4 FsMplsRsvpTeNotificationEnabled [ ] ={1,3,6,1,4,1,2076,13,2,2,22};
UINT4 FsMplsRsvpTeNotifyMsgRetransmitIntvl [ ] ={1,3,6,1,4,1,2076,13,2,2,23};
UINT4 FsMplsRsvpTeNotifyMsgRetransmitDecay [ ] ={1,3,6,1,4,1,2076,13,2,2,24};
UINT4 FsMplsRsvpTeNotifyMsgRetransmitLimit [ ] ={1,3,6,1,4,1,2076,13,2,2,25};
UINT4 FsMplsRsvpTeAdminStatusTimeIntvl [ ] ={1,3,6,1,4,1,2076,13,2,2,26};
UINT4 FsMplsRsvpTePathStateRemovedSupport [ ] ={1,3,6,1,4,1,2076,13,2,2,27};
UINT4 FsMplsRsvpTeLabelSetEnabled [ ] ={1,3,6,1,4,1,2076,13,2,2,28};
UINT4 FsMplsRsvpTeAdminStatusCapability [ ] ={1,3,6,1,4,1,2076,13,2,2,29};
UINT4 FsMplsRsvpTeGrCapability [ ] ={1,3,6,1,4,1,2076,13,2,2,30};
UINT4 FsMplsRsvpTeGrRecoveryPathCapability [ ] ={1,3,6,1,4,1,2076,13,2,2,31};
UINT4 FsMplsRsvpTeGrRestartTime [ ] ={1,3,6,1,4,1,2076,13,2,2,32};
UINT4 FsMplsRsvpTeGrRecoveryTime [ ] ={1,3,6,1,4,1,2076,13,2,2,33};
UINT4 FsMplsRsvpTeGrProgressStatus [ ] ={1,3,6,1,4,1,2076,13,2,2,34};
UINT4 FsMplsRsvpTeReoptimizeTime [ ] ={1,3,6,1,4,1,2076,13,2,2,35};
UINT4 FsMplsRsvpTeEroCacheTime [ ] ={1,3,6,1,4,1,2076,13,2,2,36};
UINT4 FsMplsRsvpTeReoptLinkMaintenance [ ] ={1,3,6,1,4,1,2076,13,2,2,37};
UINT4 FsMplsRsvpTeReoptNodeMaintenance [ ] ={1,3,6,1,4,1,2076,13,2,2,38};
UINT4 FsMplsL3VpnRsvpTeMapPrefixType [ ] ={1,3,6,1,4,1,2076,13,2,1,4,1,1};
UINT4 FsMplsL3VpnRsvpTeMapPrefix [ ] ={1,3,6,1,4,1,2076,13,2,1,4,1,2};
UINT4 FsMplsL3VpnRsvpTeMapMaskType [ ] ={1,3,6,1,4,1,2076,13,2,1,4,1,3};
UINT4 FsMplsL3VpnRsvpTeMapMask [ ] ={1,3,6,1,4,1,2076,13,2,1,4,1,4};
UINT4 FsMplsL3VpnRsvpTeMapTnlIndex [ ] ={1,3,6,1,4,1,2076,13,2,1,4,1,5};
UINT4 FsMplsL3VpnRsvpTeMapRowStatus [ ] ={1,3,6,1,4,1,2076,13,2,1,4,1,6};




tMbDbEntry fsrsvpMibEntry[]= {

{{13,FsMplsRsvpTeIfIndex}, GetNextIndexFsMplsRsvpTeIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfLblSpace}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfLblSpaceGet, FsMplsRsvpTeIfLblSpaceSet, FsMplsRsvpTeIfLblSpaceTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "1"},

{{13,FsMplsRsvpTeIfLblType}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfLblTypeGet, FsMplsRsvpTeIfLblTypeSet, FsMplsRsvpTeIfLblTypeTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "1"},

{{13,FsMplsRsvpTeAtmMergeCap}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeAtmMergeCapGet, FsMplsRsvpTeAtmMergeCapSet, FsMplsRsvpTeAtmMergeCapTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsRsvpTeAtmVcDirection}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeAtmVcDirectionGet, FsMplsRsvpTeAtmVcDirectionSet, FsMplsRsvpTeAtmVcDirectionTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsRsvpTeAtmMinVpi}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeAtmMinVpiGet, FsMplsRsvpTeAtmMinVpiSet, FsMplsRsvpTeAtmMinVpiTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsRsvpTeAtmMinVci}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeAtmMinVciGet, FsMplsRsvpTeAtmMinVciSet, FsMplsRsvpTeAtmMinVciTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "33"},

{{13,FsMplsRsvpTeAtmMaxVpi}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeAtmMaxVpiGet, FsMplsRsvpTeAtmMaxVpiSet, FsMplsRsvpTeAtmMaxVpiTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsRsvpTeAtmMaxVci}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeAtmMaxVciGet, FsMplsRsvpTeAtmMaxVciSet, FsMplsRsvpTeAtmMaxVciTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "160"},

{{13,FsMplsRsvpTeIfMtu}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfMtuGet, FsMplsRsvpTeIfMtuSet, FsMplsRsvpTeIfMtuTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "1500"},

{{13,FsMplsRsvpTeIfUdpNbrs}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfUdpNbrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfIpNbrs}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfIpNbrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNbrs}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfNbrsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfRefreshMultiple}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfRefreshMultipleGet, FsMplsRsvpTeIfRefreshMultipleSet, FsMplsRsvpTeIfRefreshMultipleTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "3"},

{{13,FsMplsRsvpTeIfTTL}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfTTLGet, FsMplsRsvpTeIfTTLSet, FsMplsRsvpTeIfTTLTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "64"},

{{13,FsMplsRsvpTeIfRefreshInterval}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfRefreshIntervalGet, FsMplsRsvpTeIfRefreshIntervalSet, FsMplsRsvpTeIfRefreshIntervalTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "30000"},

{{13,FsMplsRsvpTeIfRouteDelay}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfRouteDelayGet, FsMplsRsvpTeIfRouteDelaySet, FsMplsRsvpTeIfRouteDelayTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "2"},

{{13,FsMplsRsvpTeIfUdpRequired}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfUdpRequiredGet, FsMplsRsvpTeIfUdpRequiredSet, FsMplsRsvpTeIfUdpRequiredTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "2"},

{{13,FsMplsRsvpTeIfHelloSupported}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfHelloSupportedGet, FsMplsRsvpTeIfHelloSupportedSet, FsMplsRsvpTeIfHelloSupportedTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "2"},

{{13,FsMplsRsvpTeIfLinkAttr}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfLinkAttrGet, FsMplsRsvpTeIfLinkAttrSet, FsMplsRsvpTeIfLinkAttrTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "1"},

{{13,FsMplsRsvpTeIfStatus}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfStatusGet, FsMplsRsvpTeIfStatusSet, FsMplsRsvpTeIfStatusTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 1, NULL},

{{13,FsMplsRsvpTeIfPlrId}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfPlrIdGet, FsMplsRsvpTeIfPlrIdSet, FsMplsRsvpTeIfPlrIdTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfAvoidNodeId}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfAvoidNodeIdGet, FsMplsRsvpTeIfAvoidNodeIdSet, FsMplsRsvpTeIfAvoidNodeIdTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfStorageType}, GetNextIndexFsMplsRsvpTeIfTable, FsMplsRsvpTeIfStorageTypeGet, FsMplsRsvpTeIfStorageTypeSet, FsMplsRsvpTeIfStorageTypeTest, FsMplsRsvpTeIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeIfTableINDEX, 1, 0, 0, "3"},

{{13,FsMplsRsvpTeIfNumTnls}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumTnlsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumMsgSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumMsgSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumMsgRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumMsgRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumHelloSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumHelloSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumHelloRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumHelloRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPathErrSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPathErrSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPathErrRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPathErrRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPathTearSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPathTearSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPathTearRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPathTearRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumResvErrSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumResvErrSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumResvErrRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumResvErrRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumResvTearSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumResvTearSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumResvTearRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumResvTearRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumResvConfSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumResvConfSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumResvConfRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumResvConfRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumBundleMsgSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumBundleMsgSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumBundleMsgRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumBundleMsgRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumSRefreshMsgSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumSRefreshMsgSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumSRefreshMsgRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumSRefreshMsgRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPathSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPathSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPathRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPathRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumResvSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumResvSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumResvRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumResvRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumNotifyMsgSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumNotifyMsgSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumNotifyMsgRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumNotifyMsgRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumRecoveryPathSent}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumRecoveryPathSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumRecoveryPathRcvd}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumRecoveryPathRcvdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPathSentWithRecoveryLbl}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPathSentWithRecoveryLblGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPathRcvdWithRecoveryLbl}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPathRcvdWithRecoveryLblGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPathSentWithSuggestedLbl}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPathSentWithSuggestedLblGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPathRcvdWithSuggestedLbl}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPathRcvdWithSuggestedLblGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumHelloSentWithRestartCap}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumHelloSentWithRestartCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumHelloRcvdWithRestartCap}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumHelloRcvdWithRestartCapGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumHelloSentWithCapability}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumHelloSentWithCapabilityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumHelloRcvdWithCapability}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumHelloRcvdWithCapabilityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeIfNumPktDiscrded}, GetNextIndexFsMplsRsvpTeIfStatsTable, FsMplsRsvpTeIfNumPktDiscrdedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY    , FsMplsRsvpTeIfStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrIfAddr}, GetNextIndexFsMplsRsvpTeNbrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_NOACCESS, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrRRCapable}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrRRCapableGet, FsMplsRsvpTeNbrRRCapableSet, FsMplsRsvpTeNbrRRCapableTest, FsMplsRsvpTeNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, "2"},

{{13,FsMplsRsvpTeNbrRRState}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrRRStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrRMDCapable}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrRMDCapableGet, FsMplsRsvpTeNbrRMDCapableSet, FsMplsRsvpTeNbrRMDCapableTest, FsMplsRsvpTeNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, "2"},

{{13,FsMplsRsvpTeNbrEncapsType}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrEncapsTypeGet, FsMplsRsvpTeNbrEncapsTypeSet, FsMplsRsvpTeNbrEncapsTypeTest, FsMplsRsvpTeNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, "1"},

{{13,FsMplsRsvpTeNbrHelloSupport}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrHelloSupportGet, FsMplsRsvpTeNbrHelloSupportSet, FsMplsRsvpTeNbrHelloSupportTest, FsMplsRsvpTeNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, "2"},

{{13,FsMplsRsvpTeNbrHelloState}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrHelloStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrHelloRelation}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrHelloRelationGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrSrcInstInfo}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrSrcInstInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrDestInstInfo}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrDestInstInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrCreationTime}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrCreationTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrLclRprDetectionTime}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrLclRprDetectionTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrNumTunnels}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrNumTunnelsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrStatus}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrStatusGet, FsMplsRsvpTeNbrStatusSet, FsMplsRsvpTeNbrStatusTest, FsMplsRsvpTeNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeNbrTableINDEX, 2, 0, 1, NULL},

{{13,FsMplsRsvpTeNbrGrRecoveryPathCapability}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrGrRecoveryPathCapabilityGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrGrRestartTime}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrGrRestartTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrGrRecoveryTime}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrGrRecoveryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrGrProgressStatus}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrGrProgressStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsRsvpTeNbrStorageType}, GetNextIndexFsMplsRsvpTeNbrTable, FsMplsRsvpTeNbrStorageTypeGet, FsMplsRsvpTeNbrStorageTypeSet, FsMplsRsvpTeNbrStorageTypeTest, FsMplsRsvpTeNbrTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsRsvpTeNbrTableINDEX, 2, 0, 0, "3"},
#ifdef MPLS_L3VPN_WANTED
{{13,FsMplsL3VpnRsvpTeMapPrefixType}, GetNextIndexFsMplsL3VpnRsvpTeMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMplsL3VpnRsvpTeMapTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsL3VpnRsvpTeMapPrefix}, GetNextIndexFsMplsL3VpnRsvpTeMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMplsL3VpnRsvpTeMapTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsL3VpnRsvpTeMapMaskType}, GetNextIndexFsMplsL3VpnRsvpTeMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMplsL3VpnRsvpTeMapTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsL3VpnRsvpTeMapMask}, GetNextIndexFsMplsL3VpnRsvpTeMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, FsMplsL3VpnRsvpTeMapTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsL3VpnRsvpTeMapTnlIndex}, GetNextIndexFsMplsL3VpnRsvpTeMapTable, FsMplsL3VpnRsvpTeMapTnlIndexGet, FsMplsL3VpnRsvpTeMapTnlIndexSet, FsMplsL3VpnRsvpTeMapTnlIndexTest, FsMplsL3VpnRsvpTeMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsL3VpnRsvpTeMapTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsL3VpnRsvpTeMapRowStatus}, GetNextIndexFsMplsL3VpnRsvpTeMapTable, FsMplsL3VpnRsvpTeMapRowStatusGet, FsMplsL3VpnRsvpTeMapRowStatusSet, FsMplsL3VpnRsvpTeMapRowStatusTest, FsMplsL3VpnRsvpTeMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsL3VpnRsvpTeMapTableINDEX, 4, 0, 1, NULL},
#endif
{{11,FsMplsRsvpTeLsrID}, NULL, FsMplsRsvpTeLsrIDGet, FsMplsRsvpTeLsrIDSet, FsMplsRsvpTeLsrIDTest, FsMplsRsvpTeLsrIDDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeMaxTnls}, NULL, FsMplsRsvpTeMaxTnlsGet, FsMplsRsvpTeMaxTnlsSet, FsMplsRsvpTeMaxTnlsTest, FsMplsRsvpTeMaxTnlsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeMaxErhopsPerTnl}, NULL, FsMplsRsvpTeMaxErhopsPerTnlGet, FsMplsRsvpTeMaxErhopsPerTnlSet, FsMplsRsvpTeMaxErhopsPerTnlTest, FsMplsRsvpTeMaxErhopsPerTnlDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "16"},

{{11,FsMplsRsvpTeMaxActRoutePerTnl}, NULL, FsMplsRsvpTeMaxActRoutePerTnlGet, FsMplsRsvpTeMaxActRoutePerTnlSet, FsMplsRsvpTeMaxActRoutePerTnlTest, FsMplsRsvpTeMaxActRoutePerTnlDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeMaxIfaces}, NULL, FsMplsRsvpTeMaxIfacesGet, FsMplsRsvpTeMaxIfacesSet, FsMplsRsvpTeMaxIfacesTest, FsMplsRsvpTeMaxIfacesDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeMaxNbrs}, NULL, FsMplsRsvpTeMaxNbrsGet, FsMplsRsvpTeMaxNbrsSet, FsMplsRsvpTeMaxNbrsTest, FsMplsRsvpTeMaxNbrsDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeSockSupprtd}, NULL, FsMplsRsvpTeSockSupprtdGet, FsMplsRsvpTeSockSupprtdSet, FsMplsRsvpTeSockSupprtdTest, FsMplsRsvpTeSockSupprtdDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsMplsRsvpTeHelloSupprtd}, NULL, FsMplsRsvpTeHelloSupprtdGet, FsMplsRsvpTeHelloSupprtdSet, FsMplsRsvpTeHelloSupprtdTest, FsMplsRsvpTeHelloSupprtdDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMplsRsvpTeHelloIntervalTime}, NULL, FsMplsRsvpTeHelloIntervalTimeGet, FsMplsRsvpTeHelloIntervalTimeSet, FsMplsRsvpTeHelloIntervalTimeTest, FsMplsRsvpTeHelloIntervalTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "10000"},

{{11,FsMplsRsvpTeRRCapable}, NULL, FsMplsRsvpTeRRCapableGet, FsMplsRsvpTeRRCapableSet, FsMplsRsvpTeRRCapableTest, FsMplsRsvpTeRRCapableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMplsRsvpTeMsgIdCapable}, NULL, FsMplsRsvpTeMsgIdCapableGet, FsMplsRsvpTeMsgIdCapableSet, FsMplsRsvpTeMsgIdCapableTest, FsMplsRsvpTeMsgIdCapableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMplsRsvpTeRMDPolicyObject}, NULL, FsMplsRsvpTeRMDPolicyObjectGet, FsMplsRsvpTeRMDPolicyObjectSet, FsMplsRsvpTeRMDPolicyObjectTest, FsMplsRsvpTeRMDPolicyObjectDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeGenLblSpaceMinLbl}, NULL, FsMplsRsvpTeGenLblSpaceMinLblGet, FsMplsRsvpTeGenLblSpaceMinLblSet, FsMplsRsvpTeGenLblSpaceMinLblTest, FsMplsRsvpTeGenLblSpaceMinLblDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeGenLblSpaceMaxLbl}, NULL, FsMplsRsvpTeGenLblSpaceMaxLblGet, FsMplsRsvpTeGenLblSpaceMaxLblSet, FsMplsRsvpTeGenLblSpaceMaxLblTest, FsMplsRsvpTeGenLblSpaceMaxLblDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeGenDebugFlag}, NULL, FsMplsRsvpTeGenDebugFlagGet, FsMplsRsvpTeGenDebugFlagSet, FsMplsRsvpTeGenDebugFlagTest, FsMplsRsvpTeGenDebugFlagDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsMplsRsvpTeGenPduDumpLevel}, NULL, FsMplsRsvpTeGenPduDumpLevelGet, FsMplsRsvpTeGenPduDumpLevelSet, FsMplsRsvpTeGenPduDumpLevelTest, FsMplsRsvpTeGenPduDumpLevelDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsMplsRsvpTeGenPduDumpMsgType}, NULL, FsMplsRsvpTeGenPduDumpMsgTypeGet, FsMplsRsvpTeGenPduDumpMsgTypeSet, FsMplsRsvpTeGenPduDumpMsgTypeTest, FsMplsRsvpTeGenPduDumpMsgTypeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeGenPduDumpDirection}, NULL, FsMplsRsvpTeGenPduDumpDirectionGet, FsMplsRsvpTeGenPduDumpDirectionSet, FsMplsRsvpTeGenPduDumpDirectionTest, FsMplsRsvpTeGenPduDumpDirectionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMplsRsvpTeOperStatus}, NULL, FsMplsRsvpTeOperStatusGet, FsMplsRsvpTeOperStatusSet, FsMplsRsvpTeOperStatusTest, FsMplsRsvpTeOperStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMplsRsvpTeOverRideOption}, NULL, FsMplsRsvpTeOverRideOptionGet, FsMplsRsvpTeOverRideOptionSet, FsMplsRsvpTeOverRideOptionTest, FsMplsRsvpTeOverRideOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsMplsRsvpTeMinTnlsWithMsgId}, NULL, FsMplsRsvpTeMinTnlsWithMsgIdGet, FsMplsRsvpTeMinTnlsWithMsgIdSet, FsMplsRsvpTeMinTnlsWithMsgIdTest, FsMplsRsvpTeMinTnlsWithMsgIdDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeNotificationEnabled}, NULL, FsMplsRsvpTeNotificationEnabledGet, FsMplsRsvpTeNotificationEnabledSet, FsMplsRsvpTeNotificationEnabledTest, FsMplsRsvpTeNotificationEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsMplsRsvpTeNotifyMsgRetransmitIntvl}, NULL, FsMplsRsvpTeNotifyMsgRetransmitIntvlGet, FsMplsRsvpTeNotifyMsgRetransmitIntvlSet, FsMplsRsvpTeNotifyMsgRetransmitIntvlTest, FsMplsRsvpTeNotifyMsgRetransmitIntvlDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},

{{11,FsMplsRsvpTeNotifyMsgRetransmitDecay}, NULL, FsMplsRsvpTeNotifyMsgRetransmitDecayGet, FsMplsRsvpTeNotifyMsgRetransmitDecaySet, FsMplsRsvpTeNotifyMsgRetransmitDecayTest, FsMplsRsvpTeNotifyMsgRetransmitDecayDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsMplsRsvpTeNotifyMsgRetransmitLimit}, NULL, FsMplsRsvpTeNotifyMsgRetransmitLimitGet, FsMplsRsvpTeNotifyMsgRetransmitLimitSet, FsMplsRsvpTeNotifyMsgRetransmitLimitTest, FsMplsRsvpTeNotifyMsgRetransmitLimitDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "3"},

{{11,FsMplsRsvpTeAdminStatusTimeIntvl}, NULL, FsMplsRsvpTeAdminStatusTimeIntvlGet, FsMplsRsvpTeAdminStatusTimeIntvlSet, FsMplsRsvpTeAdminStatusTimeIntvlTest, FsMplsRsvpTeAdminStatusTimeIntvlDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "30"},

{{11,FsMplsRsvpTePathStateRemovedSupport}, NULL, FsMplsRsvpTePathStateRemovedSupportGet, FsMplsRsvpTePathStateRemovedSupportSet, FsMplsRsvpTePathStateRemovedSupportTest, FsMplsRsvpTePathStateRemovedSupportDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMplsRsvpTeLabelSetEnabled}, NULL, FsMplsRsvpTeLabelSetEnabledGet, FsMplsRsvpTeLabelSetEnabledSet, FsMplsRsvpTeLabelSetEnabledTest, FsMplsRsvpTeLabelSetEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMplsRsvpTeAdminStatusCapability}, NULL, FsMplsRsvpTeAdminStatusCapabilityGet, FsMplsRsvpTeAdminStatusCapabilitySet, FsMplsRsvpTeAdminStatusCapabilityTest, FsMplsRsvpTeAdminStatusCapabilityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMplsRsvpTeGrCapability}, NULL, FsMplsRsvpTeGrCapabilityGet, FsMplsRsvpTeGrCapabilitySet, FsMplsRsvpTeGrCapabilityTest, FsMplsRsvpTeGrCapabilityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsMplsRsvpTeGrRecoveryPathCapability}, NULL, FsMplsRsvpTeGrRecoveryPathCapabilityGet, FsMplsRsvpTeGrRecoveryPathCapabilitySet, FsMplsRsvpTeGrRecoveryPathCapabilityTest, FsMplsRsvpTeGrRecoveryPathCapabilityDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsMplsRsvpTeGrRestartTime}, NULL, FsMplsRsvpTeGrRestartTimeGet, FsMplsRsvpTeGrRestartTimeSet, FsMplsRsvpTeGrRestartTimeTest, FsMplsRsvpTeGrRestartTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "120"},

{{11,FsMplsRsvpTeGrRecoveryTime}, NULL, FsMplsRsvpTeGrRecoveryTimeGet, FsMplsRsvpTeGrRecoveryTimeSet, FsMplsRsvpTeGrRecoveryTimeTest, FsMplsRsvpTeGrRecoveryTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "120"},

{{11,FsMplsRsvpTeGrProgressStatus}, NULL, FsMplsRsvpTeGrProgressStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeReoptimizeTime}, NULL, FsMplsRsvpTeReoptimizeTimeGet, FsMplsRsvpTeReoptimizeTimeSet, FsMplsRsvpTeReoptimizeTimeTest, FsMplsRsvpTeReoptimizeTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "60"},

{{11,FsMplsRsvpTeEroCacheTime}, NULL, FsMplsRsvpTeEroCacheTimeGet, FsMplsRsvpTeEroCacheTimeSet, FsMplsRsvpTeEroCacheTimeTest, FsMplsRsvpTeEroCacheTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "5"},

{{11,FsMplsRsvpTeReoptLinkMaintenance}, NULL, FsMplsRsvpTeReoptLinkMaintenanceGet, FsMplsRsvpTeReoptLinkMaintenanceSet, FsMplsRsvpTeReoptLinkMaintenanceTest, FsMplsRsvpTeReoptLinkMaintenanceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsRsvpTeReoptNodeMaintenance}, NULL, FsMplsRsvpTeReoptNodeMaintenanceGet, FsMplsRsvpTeReoptNodeMaintenanceSet, FsMplsRsvpTeReoptNodeMaintenanceTest, FsMplsRsvpTeReoptNodeMaintenanceDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fsrsvpEntry = { sizeof(fsrsvpMibEntry)/sizeof(fsrsvpMibEntry[0]), fsrsvpMibEntry };

#endif /* _FSRSVPDB_H */

