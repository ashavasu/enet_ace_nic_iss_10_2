/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamdbg.c,v 1.8 2014/07/19 12:52:32 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Oam 
*********************************************************************/

#include "oaminc.h"

/****************************************************************************
 Function    :  OamGetAllFsMplsTpGlobalConfigTable
 Input       :  The Indices
                pOamFsMplsTpGlobalConfigEntry
 Output      :  This Routine Take the Indices &
                Gets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamGetAllFsMplsTpGlobalConfigTable (tOamFsMplsTpGlobalConfigEntry *
                                    pOamGetFsMplsTpGlobalConfigEntry)
{
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;

    /* Check whether the node is already present */
    pOamFsMplsTpGlobalConfigEntry =
        RBTreeGet (gOamGlobals.OamGlbMib.FsMplsTpGlobalConfigTable,
                   (tRBElem *) pOamGetFsMplsTpGlobalConfigEntry);

    if (pOamFsMplsTpGlobalConfigEntry == NULL)
    {
        OAM_TRC ((OAM_UTIL_TRC,
                  "OamGetAllFsMplsTpGlobalConfigTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pOamGetFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpOamModuleStatus =
        pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpOamModuleStatus;

    pOamGetFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId =
        pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId;

    MEMCPY (pOamGetFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc,
            pOamFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc,
            pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen);

    pOamGetFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen =
        pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen;

    pOamGetFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpNodeIdentifier =
        pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpNodeIdentifier;

    pOamGetFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpErrorCode =
        pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpErrorCode;

    pOamGetFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpTraceLevel =
        pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpTraceLevel;

    pOamGetFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpNotificationEnable =
        pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpNotificationEnable;

    if (OamGetAllUtlFsMplsTpGlobalConfigTable
        (pOamGetFsMplsTpGlobalConfigEntry,
         pOamFsMplsTpGlobalConfigEntry) == OSIX_FAILURE)

    {
        OAM_TRC ((OAM_UTIL_TRC, "OamGetAllFsMplsTpGlobalConfigTable:"
                  "OamGetAllUtlFsMplsTpGlobalConfigTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OamSetAllFsMplsTpGlobalConfigTable
 Input       :  The Indices
                pOamFsMplsTpGlobalConfigEntry
                pOamIsSetFsMplsTpGlobalConfigEntry
 Output      :  This Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamSetAllFsMplsTpGlobalConfigTable (tOamFsMplsTpGlobalConfigEntry *
                                    pOamSetFsMplsTpGlobalConfigEntry,
                                    tOamIsSetFsMplsTpGlobalConfigEntry *
                                    pOamIsSetFsMplsTpGlobalConfigEntry)
{
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;
    tOamFsMplsTpGlobalConfigEntry OamOldFsMplsTpGlobalConfigEntry;

    MEMSET (&OamOldFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));

    /* Check whether the node is already present */
    pOamFsMplsTpGlobalConfigEntry =
        RBTreeGet (gOamGlobals.OamGlbMib.FsMplsTpGlobalConfigTable,
                   (tRBElem *) pOamSetFsMplsTpGlobalConfigEntry);

    if (pOamFsMplsTpGlobalConfigEntry == NULL)
    {
        OAM_TRC ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpGlobalConfigTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMplsTpGlobalConfigTableFilterInputs
        (pOamFsMplsTpGlobalConfigEntry, pOamSetFsMplsTpGlobalConfigEntry,
         pOamIsSetFsMplsTpGlobalConfigEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&OamOldFsMplsTpGlobalConfigEntry, pOamFsMplsTpGlobalConfigEntry,
            sizeof (tOamFsMplsTpGlobalConfigEntry));

    /* Assign values for the existing node */
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpOamModuleStatus !=
        OSIX_FALSE)
    {
        pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpOamModuleStatus =
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.
            i4FsMplsTpOamModuleStatus;
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpGlobalId != OSIX_FALSE)
    {
        pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId =
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId;
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpIcc != OSIX_FALSE)
    {
        MEMSET (pOamFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc, 0,
                pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen);

        MEMCPY (pOamFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc,
                pOamSetFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc,
                pOamSetFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen);

        pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen =
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen;
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNodeIdentifier !=
        OSIX_FALSE)
    {
        pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpNodeIdentifier =
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.
            u4FsMplsTpNodeIdentifier;
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpTraceLevel != OSIX_FALSE)
    {
        pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpTraceLevel =
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpTraceLevel;
    }

    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNotificationEnable !=
        OSIX_FALSE)
    {
        pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpNotificationEnable =
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.
            i4FsMplsTpNotificationEnable;
    }

    if (OamUtilUpdateFsMplsTpGlobalConfigTable
        (&OamOldFsMplsTpGlobalConfigEntry, pOamFsMplsTpGlobalConfigEntry,
         pOamIsSetFsMplsTpGlobalConfigEntry) != OSIX_SUCCESS)
    {
        if (OamSetAllFsMplsTpGlobalConfigTableTrigger
            (pOamSetFsMplsTpGlobalConfigEntry,
             pOamIsSetFsMplsTpGlobalConfigEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pOamFsMplsTpGlobalConfigEntry, &OamOldFsMplsTpGlobalConfigEntry,
                sizeof (tOamFsMplsTpGlobalConfigEntry));
        return OSIX_FAILURE;
    }

    if (OamSetAllFsMplsTpGlobalConfigTableTrigger
        (pOamSetFsMplsTpGlobalConfigEntry, pOamIsSetFsMplsTpGlobalConfigEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OamGetAllFsMplsTpNodeMapTable
 Input       :  The Indices
                pOamFsMplsTpNodeMapEntry
 Output      :  This Routine Take the Indices &
                Gets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamGetAllFsMplsTpNodeMapTable (tOamFsMplsTpNodeMapEntry *
                               pOamGetFsMplsTpNodeMapEntry)
{
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;

    /* Check whether the node is already present */
    pOamFsMplsTpNodeMapEntry =
        RBTreeGet (gOamGlobals.OamGlbMib.FsMplsTpNodeMapTable,
                   (tRBElem *) pOamGetFsMplsTpNodeMapEntry);

    if (pOamFsMplsTpNodeMapEntry == NULL)
    {
        OAM_TRC ((OAM_UTIL_TRC,
                  "OamGetAllFsMplsTpNodeMapTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pOamGetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapGlobalId =
        pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapGlobalId;

    pOamGetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId =
        pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId;

    pOamGetFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus =
        pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus;

    if (OamGetAllUtlFsMplsTpNodeMapTable
        (pOamGetFsMplsTpNodeMapEntry, pOamFsMplsTpNodeMapEntry) == OSIX_FAILURE)

    {
        OAM_TRC ((OAM_UTIL_TRC, "OamGetAllFsMplsTpNodeMapTable:"
                  "OamGetAllUtlFsMplsTpNodeMapTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OamSetAllFsMplsTpNodeMapTable
 Input       :  The Indices
                pOamFsMplsTpNodeMapEntry
                pOamIsSetFsMplsTpNodeMapEntry
                i4RowStatusLogic
                i4RowCreateOption
 Output      :  This Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamSetAllFsMplsTpNodeMapTable (tOamFsMplsTpNodeMapEntry *
                               pOamSetFsMplsTpNodeMapEntry,
                               tOamIsSetFsMplsTpNodeMapEntry *
                               pOamIsSetFsMplsTpNodeMapEntry,
                               INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;
    tOamFsMplsTpNodeMapEntry OamOldFsMplsTpNodeMapEntry;
    tOamFsMplsTpNodeMapEntry OamTrgFsMplsTpNodeMapEntry;
    tOamIsSetFsMplsTpNodeMapEntry OamTrgIsSetFsMplsTpNodeMapEntry;
    INT4                i4RowMakeActive = FALSE;

    MEMSET (&OamOldFsMplsTpNodeMapEntry, 0, sizeof (tOamFsMplsTpNodeMapEntry));
    MEMSET (&OamTrgFsMplsTpNodeMapEntry, 0, sizeof (tOamFsMplsTpNodeMapEntry));
    MEMSET (&OamTrgIsSetFsMplsTpNodeMapEntry, 0,
            sizeof (tOamIsSetFsMplsTpNodeMapEntry));

    /* Check whether the node is already present */
    pOamFsMplsTpNodeMapEntry =
        RBTreeGet (gOamGlobals.OamGlbMib.FsMplsTpNodeMapTable,
                   (tRBElem *) pOamSetFsMplsTpNodeMapEntry);

    if (pOamFsMplsTpNodeMapEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pOamSetFsMplsTpNodeMapEntry->MibObject.
             i4FsMplsTpNodeMapRowStatus == CREATE_AND_WAIT)
            || (pOamSetFsMplsTpNodeMapEntry->MibObject.
                i4FsMplsTpNodeMapRowStatus == CREATE_AND_GO)
            ||
            ((pOamSetFsMplsTpNodeMapEntry->MibObject.
              i4FsMplsTpNodeMapRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pOamFsMplsTpNodeMapEntry =
                (tOamFsMplsTpNodeMapEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPNODEMAPTABLE_POOLID);
            if (pOamFsMplsTpNodeMapEntry == NULL)
            {
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpNodeMapTable: Fail to Allocate Memory.\r\n"));
                return OSIX_FAILURE;
            }

            if ((OamInitializeFsMplsTpNodeMapTable (pOamFsMplsTpNodeMapEntry))
                == OSIX_FAILURE)
            {
                if (OamSetAllFsMplsTpNodeMapTableTrigger
                    (pOamSetFsMplsTpNodeMapEntry, pOamIsSetFsMplsTpNodeMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    return OSIX_FAILURE;

                }
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpNodeMapTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (OAM_FSMPLSTPNODEMAPTABLE_POOLID,
                                    (UINT1 *) pOamFsMplsTpNodeMapEntry);

                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpContextId != OSIX_FALSE)
            {
                pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId =
                    pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId;
            }

            if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapLocalNum !=
                OSIX_FALSE)
            {
                pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapLocalNum =
                    pOamSetFsMplsTpNodeMapEntry->MibObject.
                    u4FsMplsTpNodeMapLocalNum;
            }

            if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapGlobalId !=
                OSIX_FALSE)
            {
                pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapGlobalId =
                    pOamSetFsMplsTpNodeMapEntry->MibObject.
                    u4FsMplsTpNodeMapGlobalId;
            }

            if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapNodeId !=
                OSIX_FALSE)
            {
                pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId =
                    pOamSetFsMplsTpNodeMapEntry->MibObject.
                    u4FsMplsTpNodeMapNodeId;
            }

            if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus !=
                OSIX_FALSE)
            {
                pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus =
                    pOamSetFsMplsTpNodeMapEntry->MibObject.
                    i4FsMplsTpNodeMapRowStatus;
            }

            if ((pOamSetFsMplsTpNodeMapEntry->MibObject.
                 i4FsMplsTpNodeMapRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pOamSetFsMplsTpNodeMapEntry->MibObject.
                        i4FsMplsTpNodeMapRowStatus == ACTIVE)))
            {
                pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus =
                    ACTIVE;
                /* For MSR and RM Trigger */
                OamTrgFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpContextId =
                    pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId;
                OamTrgFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpNodeMapLocalNum =
                    pOamSetFsMplsTpNodeMapEntry->MibObject.
                    u4FsMplsTpNodeMapLocalNum;

                OamTrgFsMplsTpNodeMapEntry.MibObject.
                    i4FsMplsTpNodeMapRowStatus = CREATE_AND_WAIT;
                OamTrgIsSetFsMplsTpNodeMapEntry.bFsMplsTpNodeMapRowStatus =
                    OSIX_TRUE;

                if (OamSetAllFsMplsTpNodeMapTableTrigger
                    (&OamTrgFsMplsTpNodeMapEntry,
                     &OamTrgIsSetFsMplsTpNodeMapEntry,
                     SNMP_SUCCESS) != OSIX_SUCCESS)
                {
                    OAM_TRC
                        ((OAM_UTIL_TRC,
                          "OamSetAllFsMplsTpNodeMapTable: OamSetAllFsMplsTpNodeMapTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (OAM_FSMPLSTPNODEMAPTABLE_POOLID,
                                        (UINT1 *) pOamFsMplsTpNodeMapEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pOamSetFsMplsTpNodeMapEntry->MibObject.
                     i4FsMplsTpNodeMapRowStatus == CREATE_AND_WAIT)
            {
                pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus =
                    NOT_READY;
                /* For MSR and RM Trigger */
                OamTrgFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpContextId =
                    pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId;
                OamTrgFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpNodeMapLocalNum =
                    pOamSetFsMplsTpNodeMapEntry->MibObject.
                    u4FsMplsTpNodeMapLocalNum;

                OamTrgFsMplsTpNodeMapEntry.MibObject.
                    i4FsMplsTpNodeMapRowStatus = CREATE_AND_WAIT;
                OamTrgIsSetFsMplsTpNodeMapEntry.bFsMplsTpNodeMapRowStatus =
                    OSIX_TRUE;

                if (OamSetAllFsMplsTpNodeMapTableTrigger
                    (&OamTrgFsMplsTpNodeMapEntry,
                     &OamTrgIsSetFsMplsTpNodeMapEntry,
                     SNMP_SUCCESS) != OSIX_SUCCESS)
                {
                    OAM_TRC
                        ((OAM_UTIL_TRC,
                          "OamSetAllFsMplsTpNodeMapTable: OamSetAllFsMplsTpNodeMapTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (OAM_FSMPLSTPNODEMAPTABLE_POOLID,
                                        (UINT1 *) pOamFsMplsTpNodeMapEntry);
                    return OSIX_FAILURE;
                }
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gOamGlobals.OamGlbMib.FsMplsTpNodeMapTable,
                 (tRBElem *) pOamFsMplsTpNodeMapEntry) != RB_SUCCESS)
            {
                if (OamSetAllFsMplsTpNodeMapTableTrigger
                    (&OamTrgFsMplsTpNodeMapEntry,
                     &OamTrgIsSetFsMplsTpNodeMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    return OSIX_FAILURE;

                }
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpNodeMapTable: RBTreeAdd is failed.\r\n"));
                return OSIX_FAILURE;
            }
            if (OamUtilUpdateFsMplsTpNodeMapTable
                (NULL, pOamFsMplsTpNodeMapEntry,
                 pOamIsSetFsMplsTpNodeMapEntry) != OSIX_SUCCESS)
            {
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpNodeMapTable: OamUtilUpdateFsMplsTpNodeMapTable function returns failure.\r\n"));

                if (OamSetAllFsMplsTpNodeMapTableTrigger
                    (pOamSetFsMplsTpNodeMapEntry, pOamIsSetFsMplsTpNodeMapEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    return OSIX_FAILURE;

                }
                RBTreeRem (gOamGlobals.OamGlbMib.FsMplsTpNodeMapTable,
                           pOamFsMplsTpNodeMapEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPNODEMAPTABLE_POOLID,
                                    (UINT1 *) pOamFsMplsTpNodeMapEntry);
                return OSIX_FAILURE;
            }

            if (OamSetAllFsMplsTpNodeMapTableTrigger
                (pOamSetFsMplsTpNodeMapEntry, pOamIsSetFsMplsTpNodeMapEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpNodeMapTable:  OamSetAllFsMplsTpNodeMapTableTrigger function returns failure.\r\n"));
                return OSIX_FAILURE;
            }
            return OSIX_SUCCESS;

        }
        else
        {
            return OSIX_FAILURE;
        }
    }
    else if ((pOamSetFsMplsTpNodeMapEntry->MibObject.
              i4FsMplsTpNodeMapRowStatus == CREATE_AND_WAIT)
             || (pOamSetFsMplsTpNodeMapEntry->MibObject.
                 i4FsMplsTpNodeMapRowStatus == CREATE_AND_GO))
    {
        if (OamSetAllFsMplsTpNodeMapTableTrigger (pOamSetFsMplsTpNodeMapEntry,
                                                  pOamIsSetFsMplsTpNodeMapEntry,
                                                  SNMP_SUCCESS) != OSIX_SUCCESS)

        {

            return OSIX_FAILURE;
        }
        OAM_TRC
            ((OAM_UTIL_TRC,
              "OamSetAllFsMplsTpNodeMapTable: The row is already present.\r\n"));
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (&OamOldFsMplsTpNodeMapEntry, pOamFsMplsTpNodeMapEntry,
            sizeof (tOamFsMplsTpNodeMapEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pOamSetFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus ==
        DESTROY)
    {
        pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus =
            DESTROY;

        if (OamUtilUpdateFsMplsTpNodeMapTable (&OamOldFsMplsTpNodeMapEntry,
                                               pOamFsMplsTpNodeMapEntry,
                                               pOamIsSetFsMplsTpNodeMapEntry) !=
            OSIX_SUCCESS)
        {

            if (OamSetAllFsMplsTpNodeMapTableTrigger
                (pOamSetFsMplsTpNodeMapEntry, pOamIsSetFsMplsTpNodeMapEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                return OSIX_FAILURE;
            }
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpNodeMapTable: OamUtilUpdateFsMplsTpNodeMapTable function returns failure.\r\n"));
            return OSIX_FAILURE;
        }
        RBTreeRem (gOamGlobals.OamGlbMib.FsMplsTpNodeMapTable,
                   pOamFsMplsTpNodeMapEntry);
        if (OamSetAllFsMplsTpNodeMapTableTrigger
            (pOamSetFsMplsTpNodeMapEntry, pOamIsSetFsMplsTpNodeMapEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpNodeMapTable: OamSetAllFsMplsTpNodeMapTableTrigger function returns failure.\r\n"));
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (OAM_FSMPLSTPNODEMAPTABLE_POOLID,
                            (UINT1 *) pOamFsMplsTpNodeMapEntry);

        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMplsTpNodeMapTableFilterInputs
        (pOamFsMplsTpNodeMapEntry, pOamSetFsMplsTpNodeMapEntry,
         pOamIsSetFsMplsTpNodeMapEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus ==
         ACTIVE))
    {
        pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        OamTrgFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpContextId =
            pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId;
        OamTrgFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpNodeMapLocalNum =
            pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapLocalNum;

        OamTrgFsMplsTpNodeMapEntry.MibObject.i4FsMplsTpNodeMapRowStatus =
            NOT_IN_SERVICE;
        OamTrgIsSetFsMplsTpNodeMapEntry.bFsMplsTpNodeMapRowStatus = OSIX_TRUE;

        if (OamUtilUpdateFsMplsTpNodeMapTable (&OamOldFsMplsTpNodeMapEntry,
                                               pOamFsMplsTpNodeMapEntry,
                                               pOamIsSetFsMplsTpNodeMapEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pOamFsMplsTpNodeMapEntry, &OamOldFsMplsTpNodeMapEntry,
                    sizeof (tOamFsMplsTpNodeMapEntry));
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpNodeMapTable:                 OamUtilUpdateFsMplsTpNodeMapTable Function returns failure.\r\n"));

            if (OamSetAllFsMplsTpNodeMapTableTrigger
                (pOamSetFsMplsTpNodeMapEntry, pOamIsSetFsMplsTpNodeMapEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                return OSIX_FAILURE;
            }
            return OSIX_FAILURE;
        }

        if (OamSetAllFsMplsTpNodeMapTableTrigger (&OamTrgFsMplsTpNodeMapEntry,
                                                  &OamTrgIsSetFsMplsTpNodeMapEntry,
                                                  SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpNodeMapTable: OamSetAllFsMplsTpNodeMapTableTrigger function returns failure.\r\n"));
            return OSIX_FAILURE;
        }
    }

    if (pOamSetFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapGlobalId != OSIX_FALSE)
    {
        pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapGlobalId =
            pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapGlobalId;
    }
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapNodeId != OSIX_FALSE)
    {
        pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId =
            pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId;
    }
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus != OSIX_FALSE)
    {
        pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus =
            pOamSetFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus = ACTIVE;
    }

    if (OamUtilUpdateFsMplsTpNodeMapTable (&OamOldFsMplsTpNodeMapEntry,
                                           pOamFsMplsTpNodeMapEntry,
                                           pOamIsSetFsMplsTpNodeMapEntry) !=
        OSIX_SUCCESS)
    {

        if (OamSetAllFsMplsTpNodeMapTableTrigger (pOamSetFsMplsTpNodeMapEntry,
                                                  pOamIsSetFsMplsTpNodeMapEntry,
                                                  SNMP_FAILURE) != OSIX_SUCCESS)

        {
            return OSIX_FAILURE;

        }
        OAM_TRC
            ((OAM_UTIL_TRC,
              "OamSetAllFsMplsTpNodeMapTable: OamUtilUpdateFsMplsTpNodeMapTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pOamFsMplsTpNodeMapEntry, &OamOldFsMplsTpNodeMapEntry,
                sizeof (tOamFsMplsTpNodeMapEntry));
        return OSIX_FAILURE;
    }

    if (OamSetAllFsMplsTpNodeMapTableTrigger (pOamSetFsMplsTpNodeMapEntry,
                                              pOamIsSetFsMplsTpNodeMapEntry,
                                              SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  OamGetAllFsMplsTpMegTable
 Input       :  The Indices
                pOamFsMplsTpMegEntry
 Output      :  This Routine Take the Indices &
                Gets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamGetAllFsMplsTpMegTable (tOamFsMplsTpMegEntry * pOamGetFsMplsTpMegEntry)
{
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;

    /* Check whether the node is already present */
    pOamFsMplsTpMegEntry =
        RBTreeGet (gOamGlobals.OamGlbMib.FsMplsTpMegTable,
                   (tRBElem *) pOamGetFsMplsTpMegEntry);

    if (pOamFsMplsTpMegEntry == NULL)
    {
        OAM_TRC ((OAM_UTIL_TRC,
                  "OamGetAllFsMplsTpMegTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pOamGetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
            pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
            pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen);

    pOamGetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen =
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen;

    pOamGetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegOperatorType =
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegOperatorType;

    MEMCPY (pOamGetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
            pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
            pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen);

    pOamGetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen =
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen;

    MEMCPY (pOamGetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
            pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
            pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen);

    pOamGetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen =
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen;

    pOamGetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType =
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType;

    pOamGetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation =
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation;

    pOamGetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus =
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus;

    if (OamGetAllUtlFsMplsTpMegTable
        (pOamGetFsMplsTpMegEntry, pOamFsMplsTpMegEntry) == OSIX_FAILURE)

    {
        OAM_TRC ((OAM_UTIL_TRC, "OamGetAllFsMplsTpMegTable:"
                  "OamGetAllUtlFsMplsTpMegTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OamSetAllFsMplsTpMegTable
 Input       :  The Indices
                pOamFsMplsTpMegEntry
                pOamIsSetFsMplsTpMegEntry
                i4RowStatusLogic
                i4RowCreateOption
 Output      :  This Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamSetAllFsMplsTpMegTable (tOamFsMplsTpMegEntry * pOamSetFsMplsTpMegEntry,
                           tOamIsSetFsMplsTpMegEntry *
                           pOamIsSetFsMplsTpMegEntry, INT4 i4RowStatusLogic,
                           INT4 i4RowCreateOption)
{
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    tOamFsMplsTpMegEntry OamOldFsMplsTpMegEntry;
    tOamFsMplsTpMegEntry OamTrgFsMplsTpMegEntry;
    tOamIsSetFsMplsTpMegEntry OamTrgIsSetFsMplsTpMegEntry;
    INT4                i4RowMakeActive = FALSE;

    MEMSET (&OamOldFsMplsTpMegEntry, 0, sizeof (tOamFsMplsTpMegEntry));
    MEMSET (&OamTrgFsMplsTpMegEntry, 0, sizeof (tOamFsMplsTpMegEntry));
    MEMSET (&OamTrgIsSetFsMplsTpMegEntry, 0,
            sizeof (tOamIsSetFsMplsTpMegEntry));

    /* Check whether the node is already present */
    pOamFsMplsTpMegEntry =
        RBTreeGet (gOamGlobals.OamGlbMib.FsMplsTpMegTable,
                   (tRBElem *) pOamSetFsMplsTpMegEntry);

    if (pOamFsMplsTpMegEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus ==
             CREATE_AND_WAIT)
            || (pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus ==
                CREATE_AND_GO)
            ||
            ((pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pOamFsMplsTpMegEntry =
                (tOamFsMplsTpMegEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPMEGTABLE_POOLID);
            if (pOamFsMplsTpMegEntry == NULL)
            {
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpMegTable: Fail to Allocate Memory.\r\n"));
                return OSIX_FAILURE;
            }

            if ((OamInitializeFsMplsTpMegTable (pOamFsMplsTpMegEntry)) ==
                OSIX_FAILURE)
            {
                if (OamSetAllFsMplsTpMegTableTrigger (pOamSetFsMplsTpMegEntry,
                                                      pOamIsSetFsMplsTpMegEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    return OSIX_FAILURE;

                }
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpMegTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (OAM_FSMPLSTPMEGTABLE_POOLID,
                                    (UINT1 *) pOamFsMplsTpMegEntry);

                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpContextId != OSIX_FALSE)
            {
                pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId =
                    pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;
            }

            if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIndex != OSIX_FALSE)
            {
                pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex =
                    pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;
            }

            if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegName != OSIX_FALSE)
            {
                MEMSET (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName, 0,
                        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen);

                MEMCPY (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
                        pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
                        pOamSetFsMplsTpMegEntry->MibObject.
                        i4FsMplsTpMegNameLen);

                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen =
                    pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen;
            }

            if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegOperatorType !=
                OSIX_FALSE)
            {
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegOperatorType =
                    pOamSetFsMplsTpMegEntry->MibObject.
                    i4FsMplsTpMegOperatorType;
            }

            if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdIcc != OSIX_FALSE)
            {
                MEMSET (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc, 0,
                        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen);

                MEMCPY (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
                        pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
                        pOamSetFsMplsTpMegEntry->MibObject.
                        i4FsMplsTpMegIdIccLen);

                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen =
                    pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen;
            }

            if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdUmc != OSIX_FALSE)
            {
                MEMSET (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc, 0,
                        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen);

                MEMCPY (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
                        pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
                        pOamSetFsMplsTpMegEntry->MibObject.
                        i4FsMplsTpMegIdUmcLen);

                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen =
                    pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen;
            }

            if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegServiceType !=
                OSIX_FALSE)
            {
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType =
                    pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType;
            }

            if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegMpLocation != OSIX_FALSE)
            {
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation =
                    pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation;
            }

            if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus != OSIX_FALSE)
            {
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus =
                    pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus;
            }

            if ((pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pOamSetFsMplsTpMegEntry->MibObject.
                                        i4FsMplsTpMegRowStatus == ACTIVE)))
            {
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus = ACTIVE;
                /* For MSR and RM Trigger */
                OamTrgFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
                    pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;
                OamTrgFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
                    pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;

                OamTrgFsMplsTpMegEntry.MibObject.i4FsMplsTpMegRowStatus =
                    CREATE_AND_WAIT;
                OamTrgIsSetFsMplsTpMegEntry.bFsMplsTpMegRowStatus = OSIX_TRUE;

                if (OamSetAllFsMplsTpMegTableTrigger (&OamTrgFsMplsTpMegEntry,
                                                      &OamTrgIsSetFsMplsTpMegEntry,
                                                      SNMP_SUCCESS) !=
                    OSIX_SUCCESS)
                {
                    OAM_TRC
                        ((OAM_UTIL_TRC,
                          "OamSetAllFsMplsTpMegTable: OamSetAllFsMplsTpMegTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (OAM_FSMPLSTPMEGTABLE_POOLID,
                                        (UINT1 *) pOamFsMplsTpMegEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pOamSetFsMplsTpMegEntry->MibObject.
                     i4FsMplsTpMegRowStatus == CREATE_AND_WAIT)
            {
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus =
                    NOT_READY;
                /* For MSR and RM Trigger */
                OamTrgFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
                    pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;
                OamTrgFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
                    pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;

                OamTrgFsMplsTpMegEntry.MibObject.i4FsMplsTpMegRowStatus =
                    CREATE_AND_WAIT;
                OamTrgIsSetFsMplsTpMegEntry.bFsMplsTpMegRowStatus = OSIX_TRUE;

                if (OamSetAllFsMplsTpMegTableTrigger (&OamTrgFsMplsTpMegEntry,
                                                      &OamTrgIsSetFsMplsTpMegEntry,
                                                      SNMP_SUCCESS) !=
                    OSIX_SUCCESS)
                {
                    OAM_TRC
                        ((OAM_UTIL_TRC,
                          "OamSetAllFsMplsTpMegTable: OamSetAllFsMplsTpMegTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (OAM_FSMPLSTPMEGTABLE_POOLID,
                                        (UINT1 *) pOamFsMplsTpMegEntry);
                    return OSIX_FAILURE;
                }
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gOamGlobals.OamGlbMib.FsMplsTpMegTable,
                 (tRBElem *) pOamFsMplsTpMegEntry) != RB_SUCCESS)
            {
                if (OamSetAllFsMplsTpMegTableTrigger (&OamTrgFsMplsTpMegEntry,
                                                      &OamTrgIsSetFsMplsTpMegEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    return OSIX_FAILURE;

                }
                OAM_TRC ((OAM_UTIL_TRC,
                          "OamSetAllFsMplsTpMegTable: RBTreeAdd is failed.\r\n"));
                return OSIX_FAILURE;
            }
            if (OamUtilUpdateFsMplsTpMegTable
                (NULL, pOamFsMplsTpMegEntry, pOamSetFsMplsTpMegEntry,
                 pOamIsSetFsMplsTpMegEntry) != OSIX_SUCCESS)
            {
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpMegTable: OamUtilUpdateFsMplsTpMegTable function returns failure.\r\n"));

                if (OamSetAllFsMplsTpMegTableTrigger (pOamSetFsMplsTpMegEntry,
                                                      pOamIsSetFsMplsTpMegEntry,
                                                      SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    return OSIX_FAILURE;

                }
                RBTreeRem (gOamGlobals.OamGlbMib.FsMplsTpMegTable,
                           pOamFsMplsTpMegEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMEGTABLE_POOLID,
                                    (UINT1 *) pOamFsMplsTpMegEntry);
                return OSIX_FAILURE;
            }

            if (OamSetAllFsMplsTpMegTableTrigger (pOamSetFsMplsTpMegEntry,
                                                  pOamIsSetFsMplsTpMegEntry,
                                                  SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpMegTable:  OamSetAllFsMplsTpMegTableTrigger function returns failure.\r\n"));
                return OSIX_FAILURE;
            }
            return OSIX_SUCCESS;

        }
        else
        {
            return OSIX_FAILURE;
        }
    }
    else if ((pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus ==
              CREATE_AND_WAIT)
             || (pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus ==
                 CREATE_AND_GO))
    {
        if (OamSetAllFsMplsTpMegTableTrigger (pOamSetFsMplsTpMegEntry,
                                              pOamIsSetFsMplsTpMegEntry,
                                              SNMP_SUCCESS) != OSIX_SUCCESS)

        {

            return OSIX_FAILURE;
        }
        OAM_TRC ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpMegTable: The row is already present.\r\n"));
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (&OamOldFsMplsTpMegEntry, pOamFsMplsTpMegEntry,
            sizeof (tOamFsMplsTpMegEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus == DESTROY)
    {
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus = DESTROY;

        if (OamUtilUpdateFsMplsTpMegTable (&OamOldFsMplsTpMegEntry,
                                           pOamFsMplsTpMegEntry,
                                           pOamSetFsMplsTpMegEntry,
                                           pOamIsSetFsMplsTpMegEntry) !=
            OSIX_SUCCESS)
        {

            if (OamSetAllFsMplsTpMegTableTrigger (pOamSetFsMplsTpMegEntry,
                                                  pOamIsSetFsMplsTpMegEntry,
                                                  SNMP_FAILURE) != OSIX_SUCCESS)

            {
                return OSIX_FAILURE;
            }
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpMegTable: OamUtilUpdateFsMplsTpMegTable function returns failure.\r\n"));
            return OSIX_FAILURE;
        }
        RBTreeRem (gOamGlobals.OamGlbMib.FsMplsTpMegTable,
                   pOamFsMplsTpMegEntry);
        if (OamSetAllFsMplsTpMegTableTrigger
            (pOamSetFsMplsTpMegEntry, pOamIsSetFsMplsTpMegEntry,
             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpMegTable: OamSetAllFsMplsTpMegTableTrigger function returns failure.\r\n"));
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (OAM_FSMPLSTPMEGTABLE_POOLID,
                            (UINT1 *) pOamFsMplsTpMegEntry);

        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMplsTpMegTableFilterInputs
        (pOamFsMplsTpMegEntry, pOamSetFsMplsTpMegEntry,
         pOamIsSetFsMplsTpMegEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus == ACTIVE) &&
        (pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus !=
         NOT_IN_SERVICE))
    {
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        OamTrgFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
            pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;
        OamTrgFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
            pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;

        OamTrgFsMplsTpMegEntry.MibObject.i4FsMplsTpMegRowStatus =
            NOT_IN_SERVICE;
        OamTrgIsSetFsMplsTpMegEntry.bFsMplsTpMegRowStatus = OSIX_TRUE;

        if (OamUtilUpdateFsMplsTpMegTable (&OamOldFsMplsTpMegEntry,
                                           pOamFsMplsTpMegEntry,
                                           pOamSetFsMplsTpMegEntry,
                                           pOamIsSetFsMplsTpMegEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pOamFsMplsTpMegEntry, &OamOldFsMplsTpMegEntry,
                    sizeof (tOamFsMplsTpMegEntry));
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpMegTable:                 OamUtilUpdateFsMplsTpMegTable Function returns failure.\r\n"));

            if (OamSetAllFsMplsTpMegTableTrigger (pOamSetFsMplsTpMegEntry,
                                                  pOamIsSetFsMplsTpMegEntry,
                                                  SNMP_FAILURE) != OSIX_SUCCESS)

            {
                return OSIX_FAILURE;
            }
            return OSIX_FAILURE;
        }

        if (OamSetAllFsMplsTpMegTableTrigger (&OamTrgFsMplsTpMegEntry,
                                              &OamTrgIsSetFsMplsTpMegEntry,
                                              SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpMegTable: OamSetAllFsMplsTpMegTableTrigger function returns failure.\r\n"));
            return OSIX_FAILURE;
        }
    }

    if (pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegOperatorType != OSIX_FALSE)
    {
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegOperatorType =
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegOperatorType;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdIcc != OSIX_FALSE)
    {
        MEMSET (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc, 0,
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen);

        MEMCPY (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
                pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
                pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen);

        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen =
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdUmc != OSIX_FALSE)
    {
        MEMSET (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc, 0,
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen);

        MEMCPY (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
                pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
                pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen);

        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen =
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegServiceType != OSIX_FALSE)
    {
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType =
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegMpLocation != OSIX_FALSE)
    {
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation =
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus != OSIX_FALSE)
    {
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus =
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus = ACTIVE;
    }

    if (OamUtilUpdateFsMplsTpMegTable (&OamOldFsMplsTpMegEntry,
                                       pOamFsMplsTpMegEntry,
                                       pOamSetFsMplsTpMegEntry,
                                       pOamIsSetFsMplsTpMegEntry) !=
        OSIX_SUCCESS)
    {

        if (OamSetAllFsMplsTpMegTableTrigger (pOamSetFsMplsTpMegEntry,
                                              pOamIsSetFsMplsTpMegEntry,
                                              SNMP_FAILURE) != OSIX_SUCCESS)

        {
            return OSIX_FAILURE;

        }
        OAM_TRC
            ((OAM_UTIL_TRC,
              "OamSetAllFsMplsTpMegTable: OamUtilUpdateFsMplsTpMegTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pOamFsMplsTpMegEntry, &OamOldFsMplsTpMegEntry,
                sizeof (tOamFsMplsTpMegEntry));
        return OSIX_FAILURE;
    }

    if (OamSetAllFsMplsTpMegTableTrigger (pOamSetFsMplsTpMegEntry,
                                          pOamIsSetFsMplsTpMegEntry,
                                          SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/****************************************************************************
 Function    :  OamGetAllFsMplsTpMeTable
 Input       :  The Indices
                pOamFsMplsTpMeEntry
 Output      :  This Routine Take the Indices &
                Gets the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamGetAllFsMplsTpMeTable (tOamFsMplsTpMeEntry * pOamGetFsMplsTpMeEntry)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    /* Check whether the node is already present */
    pOamFsMplsTpMeEntry =
        RBTreeGet (gOamGlobals.OamGlbMib.FsMplsTpMeTable,
                   (tRBElem *) pOamGetFsMplsTpMeEntry);

    if (pOamFsMplsTpMeEntry == NULL)
    {
        OAM_TRC ((OAM_UTIL_TRC,
                  "OamGetAllFsMplsTpMeTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (pOamGetFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
            pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
            pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen);

    pOamGetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen =
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen;

    pOamGetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex =
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex;

    pOamGetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex;

    pOamGetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex;

    pOamGetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType =
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType;

    pOamGetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection =
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection;

    pOamGetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeProactiveOamSessIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeProactiveOamSessIndex;

    pOamGetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeProactiveOamPhbTCValue =
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeProactiveOamPhbTCValue;

    pOamGetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeOnDemandOamPhbTCValue =
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeOnDemandOamPhbTCValue;

    pOamGetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServiceSignaled =
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServiceSignaled;

    MEMCPY (pOamGetFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer,
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer,
            (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen *
             sizeof (UINT4)));
    pOamGetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen =
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;

    pOamGetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus =
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus;

    if (OamGetAllUtlFsMplsTpMeTable
        (pOamGetFsMplsTpMeEntry, pOamFsMplsTpMeEntry) == OSIX_FAILURE)

    {
        OAM_TRC ((OAM_UTIL_TRC, "OamGetAllFsMplsTpMeTable:"
                  "OamGetAllUtlFsMplsTpMeTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OamSetAllFsMplsTpMeTable
 Input       :  pOamSetFsMplsTpMeEntry
                pOamIsSetFsMplsTpMeEntry
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamSetAllFsMplsTpMeTable (tOamFsMplsTpMeEntry * pOamSetFsMplsTpMeEntry,
                          tOamIsSetFsMplsTpMeEntry * pOamIsSetFsMplsTpMeEntry,
                          INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    tOamFsMplsTpMeEntry *pOamOldFsMplsTpMeEntry = NULL;
    tOamFsMplsTpMeEntry *pOamTrgFsMplsTpMeEntry = NULL;
    tOamIsSetFsMplsTpMeEntry *pOamTrgIsSetFsMplsTpMeEntry = NULL;
    INT4                i4RowMakeActive = FALSE;

    pOamOldFsMplsTpMeEntry =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamOldFsMplsTpMeEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pOamTrgFsMplsTpMeEntry =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamTrgFsMplsTpMeEntry == NULL)
    {
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamOldFsMplsTpMeEntry);
        return OSIX_FAILURE;
    }
    pOamTrgIsSetFsMplsTpMeEntry =
        (tOamIsSetFsMplsTpMeEntry *)
        MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamTrgIsSetFsMplsTpMeEntry == NULL)
    {
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamOldFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgFsMplsTpMeEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pOamOldFsMplsTpMeEntry, 0, sizeof (tOamFsMplsTpMeEntry));
    MEMSET (pOamTrgFsMplsTpMeEntry, 0, sizeof (tOamFsMplsTpMeEntry));
    MEMSET (pOamTrgIsSetFsMplsTpMeEntry, 0, sizeof (tOamIsSetFsMplsTpMeEntry));

    /* Check whether the node is already present */
    pOamFsMplsTpMeEntry =
        RBTreeGet (gOamGlobals.OamGlbMib.FsMplsTpMeTable,
                   (tRBElem *) pOamSetFsMplsTpMeEntry);

    if (pOamFsMplsTpMeEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus ==
             CREATE_AND_WAIT)
            || (pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus ==
                CREATE_AND_GO)
            ||
            ((pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pOamFsMplsTpMeEntry =
                (tOamFsMplsTpMeEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
            if (pOamFsMplsTpMeEntry == NULL)
            {
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpMeTable: Fail to Allocate Memory.\r\n"));
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamOldFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                return OSIX_FAILURE;
            }

            if ((OamInitializeFsMplsTpMeTable (pOamFsMplsTpMeEntry)) ==
                OSIX_FAILURE)
            {
                if (OamSetAllFsMplsTpMeTableTrigger (pOamSetFsMplsTpMeEntry,
                                                     pOamIsSetFsMplsTpMeEntry,
                                                     SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamOldFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamTrgFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                    return OSIX_FAILURE;

                }
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpMeTable: Fail to Initialize the Objects.\r\n"));

                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamOldFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpContextId != OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMegIndex != OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeIndex != OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIndex != OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeName != OSIX_FALSE)
            {
                MEMSET (pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName, 0,
                        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen);

                MEMCPY (pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                        pOamSetFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                        pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen);

                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen =
                    pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIfIndex != OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSourceMepIndex !=
                OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.
                    u4FsMplsTpMeSourceMepIndex;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSinkMepIndex != OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpType != OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType =
                    pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMepDirection != OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection =
                    pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeProactiveOamPhbTCValue !=
                OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.
                    i4FsMplsTpMeProactiveOamPhbTCValue =
                    pOamSetFsMplsTpMeEntry->MibObject.
                    i4FsMplsTpMeProactiveOamPhbTCValue;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeOnDemandOamPhbTCValue !=
                OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.
                    i4FsMplsTpMeOnDemandOamPhbTCValue =
                    pOamSetFsMplsTpMeEntry->MibObject.
                    i4FsMplsTpMeOnDemandOamPhbTCValue;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServiceSignaled !=
                OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServiceSignaled =
                    pOamSetFsMplsTpMeEntry->MibObject.
                    i4FsMplsTpMeServiceSignaled;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer !=
                OSIX_FALSE)
            {
                MEMSET (pOamFsMplsTpMeEntry->MibObject.
                        au4FsMplsTpMeServicePointer, 0,
                        (pOamFsMplsTpMeEntry->MibObject.
                         i4FsMplsTpMeServicePointerLen * sizeof (UINT4)));

                MEMCPY (pOamFsMplsTpMeEntry->MibObject.
                        au4FsMplsTpMeServicePointer,
                        pOamSetFsMplsTpMeEntry->MibObject.
                        au4FsMplsTpMeServicePointer,
                        (pOamSetFsMplsTpMeEntry->MibObject.
                         i4FsMplsTpMeServicePointerLen * sizeof (UINT4)));

                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen =
                    pOamSetFsMplsTpMeEntry->MibObject.
                    i4FsMplsTpMeServicePointerLen;
            }

            if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus != OSIX_FALSE)
            {
                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus =
                    pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus;
            }

            if ((pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus ==
                 CREATE_AND_GO) || ((i4RowCreateOption == 1)
                                    && (pOamSetFsMplsTpMeEntry->MibObject.
                                        i4FsMplsTpMeRowStatus == ACTIVE)))
            {
                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus = ACTIVE;
                /* For MSR and RM Trigger */
                pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
                pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
                pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
                pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;

                pOamTrgFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus =
                    CREATE_AND_WAIT;
                pOamTrgIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus = OSIX_TRUE;

                if (OamSetAllFsMplsTpMeTableTrigger (pOamTrgFsMplsTpMeEntry,
                                                     pOamTrgIsSetFsMplsTpMeEntry,
                                                     SNMP_SUCCESS) !=
                    OSIX_SUCCESS)
                {
                    OAM_TRC
                        ((OAM_UTIL_TRC,
                          "OamSetAllFsMplsTpMeTable: OamSetAllFsMplsTpMeTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamOldFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamTrgFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus ==
                     CREATE_AND_WAIT)
            {
                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus =
                    NOT_READY;
                /* For MSR and RM Trigger */
                pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
                pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
                pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
                pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex =
                    pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;

                pOamTrgFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus =
                    CREATE_AND_WAIT;
                pOamTrgIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus = OSIX_TRUE;

                if (OamSetAllFsMplsTpMeTableTrigger (pOamTrgFsMplsTpMeEntry,
                                                     pOamTrgIsSetFsMplsTpMeEntry,
                                                     SNMP_SUCCESS) !=
                    OSIX_SUCCESS)
                {
                    OAM_TRC
                        ((OAM_UTIL_TRC,
                          "OamSetAllFsMplsTpMeTable: OamSetAllFsMplsTpMeTableTrigger function returns failure.\r\n"));

                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamOldFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamTrgFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                    return OSIX_FAILURE;
                }
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gOamGlobals.OamGlbMib.FsMplsTpMeTable,
                 (tRBElem *) pOamFsMplsTpMeEntry) != RB_SUCCESS)
            {
                if (OamSetAllFsMplsTpMeTableTrigger (pOamTrgFsMplsTpMeEntry,
                                                     pOamTrgIsSetFsMplsTpMeEntry,
                                                     SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamOldFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamTrgFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                    return OSIX_FAILURE;

                }
                OAM_TRC ((OAM_UTIL_TRC,
                          "OamSetAllFsMplsTpMeTable: RBTreeAdd is failed.\r\n"));

                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamOldFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                return OSIX_FAILURE;
            }
            if (OamUtilUpdateFsMplsTpMeTable
                (NULL, pOamFsMplsTpMeEntry,
                 pOamIsSetFsMplsTpMeEntry) != OSIX_SUCCESS)
            {
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpMeTable: OamUtilUpdateFsMplsTpMeTable function returns failure.\r\n"));

                if (OamSetAllFsMplsTpMeTableTrigger (pOamSetFsMplsTpMeEntry,
                                                     pOamIsSetFsMplsTpMeEntry,
                                                     SNMP_FAILURE) !=
                    OSIX_SUCCESS)

                {
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamOldFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamTrgFsMplsTpMeEntry);
                    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                        (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                    return OSIX_FAILURE;

                }
                RBTreeRem (gOamGlobals.OamGlbMib.FsMplsTpMeTable,
                           pOamFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamOldFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                return OSIX_FAILURE;
            }

            if (OamSetAllFsMplsTpMeTableTrigger (pOamSetFsMplsTpMeEntry,
                                                 pOamIsSetFsMplsTpMeEntry,
                                                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                OAM_TRC
                    ((OAM_UTIL_TRC,
                      "OamSetAllFsMplsTpMeTable:  OamSetAllFsMplsTpMeTableTrigger function returns failure.\r\n"));
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamOldFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                return OSIX_FAILURE;
            }
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamOldFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
            return OSIX_SUCCESS;

        }
        else
        {
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamOldFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus ==
              CREATE_AND_WAIT)
             || (pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus ==
                 CREATE_AND_GO))
    {
        if (OamSetAllFsMplsTpMeTableTrigger (pOamSetFsMplsTpMeEntry,
                                             pOamIsSetFsMplsTpMeEntry,
                                             SNMP_SUCCESS) != OSIX_SUCCESS)

        {
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamOldFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
            return OSIX_FAILURE;
        }
        OAM_TRC ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpMeTable: The row is already present.\r\n"));
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamOldFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pOamOldFsMplsTpMeEntry, pOamFsMplsTpMeEntry,
            sizeof (tOamFsMplsTpMeEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus == DESTROY)
    {
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus = DESTROY;

        if (OamUtilUpdateFsMplsTpMeTable (pOamOldFsMplsTpMeEntry,
                                          pOamFsMplsTpMeEntry,
                                          pOamIsSetFsMplsTpMeEntry) !=
            OSIX_SUCCESS)
        {

            if (OamSetAllFsMplsTpMeTableTrigger (pOamSetFsMplsTpMeEntry,
                                                 pOamIsSetFsMplsTpMeEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamOldFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                return OSIX_FAILURE;
            }
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpMeTable: OamUtilUpdateFsMplsTpMeTable function returns failure.\r\n"));
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamOldFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
            return OSIX_FAILURE;
        }
        RBTreeRem (gOamGlobals.OamGlbMib.FsMplsTpMeTable, pOamFsMplsTpMeEntry);
        if (OamSetAllFsMplsTpMeTableTrigger (pOamSetFsMplsTpMeEntry,
                                             pOamIsSetFsMplsTpMeEntry,
                                             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpMeTable: OamSetAllFsMplsTpMeTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamOldFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamOldFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMplsTpMeTableFilterInputs
        (pOamFsMplsTpMeEntry, pOamSetFsMplsTpMeEntry,
         pOamIsSetFsMplsTpMeEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamOldFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
        return OSIX_SUCCESS;
    }

    /* This condtion is to make the row NOT_IN_SERVICE before
     * modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus == ACTIVE) &&
        (pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus
         != NOT_IN_SERVICE))
    {
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId =
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
        pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex =
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
        pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex =
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
        pOamTrgFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex =
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;

        pOamTrgFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus =
            NOT_IN_SERVICE;
        pOamTrgIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus = OSIX_TRUE;

        if (OamUtilUpdateFsMplsTpMeTable (pOamOldFsMplsTpMeEntry,
                                          pOamFsMplsTpMeEntry,
                                          pOamIsSetFsMplsTpMeEntry) !=
            OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pOamFsMplsTpMeEntry, pOamOldFsMplsTpMeEntry,
                    sizeof (tOamFsMplsTpMeEntry));
            OAM_TRC
                ((OAM_UTIL_TRC, "OamSetAllFsMplsTpMeTable: "
                  "OamUtilUpdateFsMplsTpMeTable Function returns failure.\r\n"));

            if (OamSetAllFsMplsTpMeTableTrigger (pOamSetFsMplsTpMeEntry,
                                                 pOamIsSetFsMplsTpMeEntry,
                                                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamOldFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgFsMplsTpMeEntry);
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
                return OSIX_FAILURE;
            }
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamOldFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
            return OSIX_FAILURE;
        }

        if (OamSetAllFsMplsTpMeTableTrigger (pOamTrgFsMplsTpMeEntry,
                                             pOamTrgIsSetFsMplsTpMeEntry,
                                             SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamSetAllFsMplsTpMeTable: OamSetAllFsMplsTpMeTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamOldFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
            return OSIX_FAILURE;
        }
    }

    if (pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus == ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeName != OSIX_FALSE)
    {
        MEMSET (pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName, 0,
                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen);

        MEMCPY (pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                pOamSetFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen);

        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen =
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIfIndex != OSIX_FALSE)
    {
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex =
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSourceMepIndex != OSIX_FALSE)
    {
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex =
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSinkMepIndex != OSIX_FALSE)
    {
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex =
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpType != OSIX_FALSE)
    {
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType =
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMepDirection != OSIX_FALSE)
    {
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection =
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeProactiveOamPhbTCValue !=
        OSIX_FALSE)
    {
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeProactiveOamPhbTCValue =
            pOamSetFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeProactiveOamPhbTCValue;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeOnDemandOamPhbTCValue !=
        OSIX_FALSE)
    {
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeOnDemandOamPhbTCValue =
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeOnDemandOamPhbTCValue;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServiceSignaled != OSIX_FALSE)
    {
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServiceSignaled =
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServiceSignaled;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer != OSIX_FALSE)
    {
        MEMSET (pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer, 0,
                (pOamFsMplsTpMeEntry->MibObject.
                 i4FsMplsTpMeServicePointerLen * sizeof (UINT4)));

        MEMCPY (pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer,
                pOamSetFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer,
                (pOamSetFsMplsTpMeEntry->MibObject.
                 i4FsMplsTpMeServicePointerLen * sizeof (UINT4)));

        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen =
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus != OSIX_FALSE)
    {
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus =
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus = ACTIVE;
    }

    if (OamUtilUpdateFsMplsTpMeTable (pOamOldFsMplsTpMeEntry,
                                      pOamFsMplsTpMeEntry,
                                      pOamIsSetFsMplsTpMeEntry) != OSIX_SUCCESS)
    {

        if (OamSetAllFsMplsTpMeTableTrigger (pOamSetFsMplsTpMeEntry,
                                             pOamIsSetFsMplsTpMeEntry,
                                             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamOldFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
            return OSIX_FAILURE;
        }
        OAM_TRC
            ((OAM_UTIL_TRC,
              "OamSetAllFsMplsTpMeTable: OamUtilUpdateFsMplsTpMeTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pOamFsMplsTpMeEntry, pOamOldFsMplsTpMeEntry,
                sizeof (tOamFsMplsTpMeEntry));
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamOldFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
        return OSIX_FAILURE;
    }

    if (OamSetAllFsMplsTpMeTableTrigger (pOamSetFsMplsTpMeEntry,
                                         pOamIsSetFsMplsTpMeEntry,
                                         SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamOldFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgFsMplsTpMeEntry);
        MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                            (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamOldFsMplsTpMeEntry);
    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamTrgFsMplsTpMeEntry);
    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamTrgIsSetFsMplsTpMeEntry);
    return OSIX_SUCCESS;

}
