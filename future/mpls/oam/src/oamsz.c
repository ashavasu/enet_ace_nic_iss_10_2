/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oamsz.c,v 1.4 2013/11/29 11:04:14 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _MPLSOAMSZ_C
#include "oaminc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
MplsoamSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MPLSOAM_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsMPLSOAMSizingParams[i4SizingId].u4StructSize,
                              FsMPLSOAMSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(MPLSOAMMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            MplsoamSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
MplsoamSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsMPLSOAMSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, MPLSOAMMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
MplsoamSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MPLSOAM_MAX_SIZING_ID; i4SizingId++)
    {
        if (MPLSOAMMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (MPLSOAMMemPoolIds[i4SizingId]);
            MPLSOAMMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
