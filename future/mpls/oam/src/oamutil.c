/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: oamutil.c,v 1.14 2013/07/04 13:23:27 siva Exp $
*
* Description : This file contains the utility 
*               functions of the OAM module
*****************************************************************************/

#include "oaminc.h"
#include "oamext.h"
#include "l2vpextn.h"
#include "mplscli.h"
#include "mplsutil.h"

/******************************************************************************
 * Function   : OamUtilCreateDefaultContext
 * Description: Creates the default context for OAM
 * Input      : Context Id
 * Output     : None
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
INT4
OamUtilCreateContext (UINT4 u4ContextId)
{
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;

    OAM_TRC_FUNC ((OAM_FN_ENTRY, "FUNC:OamUtilCreateContext\n"));

    pOamFsMplsTpGlobalConfigEntry = (tOamFsMplsTpGlobalConfigEntry *)
        MemAllocMemBlk (OAM_FSMPLSTPGLOBALCONFIGTABLE_POOLID);

    if (pOamFsMplsTpGlobalConfigEntry == NULL)
    {
        OAM_TRC ((OAM_MAIN_TRC,
                  "\r%% Memory allocation Failed for global config table entry\r\n"));
        return OSIX_FAILURE;
    }
    else
    {
        OamUtilInitializeGblConfigTable (pOamFsMplsTpGlobalConfigEntry,
                                         u4ContextId);
        if (RBTreeAdd (gOamGlobals.OamGlbMib.FsMplsTpGlobalConfigTable,
                       (tRBElem *) pOamFsMplsTpGlobalConfigEntry) != RB_SUCCESS)
        {
            MemReleaseMemBlock (OAM_FSMPLSTPGLOBALCONFIGTABLE_POOLID,
                                (UINT1 *) pOamFsMplsTpGlobalConfigEntry);
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% RB Tree addition failed for global config node\r\n"));
            return OSIX_FAILURE;
        }
    }

    OAM_TRC_FUNC ((OAM_FN_EXIT, "EXIT:OamUtilCreateContext\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamUtilInitializeGblConfigTable
* Description : Initializes the global config structure with default values.
* Input       : pOamFsMplsTpGlobalConfigEntry and Context Id
* Output      : pOamFsMplsTpGlobalConfigEntry
* Returns     : None
*****************************************************************************/
VOID
OamUtilInitializeGblConfigTable (tOamFsMplsTpGlobalConfigEntry
                                 * pOamFsMplsTpGlobalConfigEntry,
                                 UINT4 u4ContextId)
{
    pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpContextId = u4ContextId;

    pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId = 0;

    pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpNodeIdentifier = 0;

    pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpOamModuleStatus =
        OAM_MODULE_STATUS_ENABLED;

    pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpErrorCode = 0;

    pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpNotificationEnable =
        MPLS_SNMP_FALSE;

    pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen = 0;

    MEMSET ((pOamFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc),
            0, OAM_ICC_ID_LEN);

    return;
}

/****************************************************************************
* Function    : OamUtilGetLocalMapNum
* Description : Returns the Local Map number for the given Global and Node Id 
* Input       : Context Id, Global Id, Node Id
* Output      : Local Map Number
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilGetLocalMapNum (UINT4 u4ContextId, UINT4 u4GlobalId, UINT4 u4NodeId,
                       UINT4 *pu4LocalMapNum)
{
    tOamFsMplsTpNodeMapEntry OamFsMplsTpNodeMapEntry;
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;

    OAM_TRC_FUNC ((OAM_FN_ENTRY, "FUNC:OamUtilGetLocalMapNum\n"));
    MEMSET (&OamFsMplsTpNodeMapEntry, 0, sizeof (tOamFsMplsTpNodeMapEntry));

    OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpContextId = u4ContextId;
    OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpNodeMapGlobalId = u4GlobalId;
    OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpNodeMapNodeId = u4NodeId;
    pOamFsMplsTpNodeMapEntry = RBTreeGet
        (gOamGlobals.OamGblNodeDualSearchTree,
         (tRBElem *) & OamFsMplsTpNodeMapEntry);

    if (pOamFsMplsTpNodeMapEntry == NULL)
    {
        OAM_TRC ((OAM_MAIN_TRC,
                  "\r%% Entry not found for the Global Node Id\r\n"));
        return OSIX_FAILURE;
    }
    *pu4LocalMapNum = pOamFsMplsTpNodeMapEntry->MibObject.
        u4FsMplsTpNodeMapLocalNum;

    OAM_TRC_FUNC ((OAM_FN_EXIT, "EXIT:OamUtilGetLocalMapNum\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamUtilGetGlobalIdNodeId
* Description : Returns the GlobalId and NodeId from Local map number
* Input       : Context Id, Local Map Number
* Output      : Global Id, Node Id
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilGetGlobalIdNodeId (UINT4 u4ContextId, UINT4 u4LocalMapNum,
                          UINT4 *pu4GlobalId, UINT4 *pu4NodeId)
{
    tOamFsMplsTpNodeMapEntry OamFsMplsTpNodeMapEntry;
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;

    OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpContextId = u4ContextId;
    OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpNodeMapLocalNum = u4LocalMapNum;

    pOamFsMplsTpNodeMapEntry = OamGetFsMplsTpNodeMapTable
        (&OamFsMplsTpNodeMapEntry);

    if (pOamFsMplsTpNodeMapEntry != NULL)
    {
        *pu4GlobalId = pOamFsMplsTpNodeMapEntry->MibObject.
            u4FsMplsTpNodeMapGlobalId;
        *pu4NodeId = pOamFsMplsTpNodeMapEntry->MibObject.
            u4FsMplsTpNodeMapNodeId;
    }
    else
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamUtilGetMegIndexFromMegName
* Description : Utility to Get Meg Index from MegName and ContextId
* Input       : Context Id and Meg name
* Output      : Meg Index
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilGetMegIndexFromMegName (UINT4 u4ContextId, UINT1 *pu1MegName,
                               UINT4 *pu4MegIndex)
{

    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;

    MEMSET (&OamFsMplsTpMegEntry, 0, sizeof (OamFsMplsTpMegEntry));

    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId = u4ContextId;
    MEMCPY (OamFsMplsTpMegEntry.MibObject.au1FsMplsTpMegName, pu1MegName,
            sizeof (OamFsMplsTpMegEntry.MibObject.au1FsMplsTpMegName));

    pOamFsMplsTpMegEntry = RBTreeGet (gOamGlobals.FsMplsTpMegNameTable,
                                      (tRBElem *) & OamFsMplsTpMegEntry);

    if (pOamFsMplsTpMegEntry != NULL)
    {
        *pu4MegIndex = pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;
        return OSIX_SUCCESS;
    }

    return OSIX_FAILURE;
}

/****************************************************************************
* Function    : OamUtilGetMeIndexFromMegIndex
* Description : Utility to Get Me Indices from ContextId, Meg Index & Me name
* Input       : ContextId, MEG Index, ME name
* Output      : ME Index and MP Index 
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilGetMeIndexFromMegIndex (UINT4 u4ContextId, UINT4 u4MegIndex,
                               UINT1 *pu1MeName, UINT4 *pu4MeIndex,
                               UINT4 *pu4MpIndex)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    OAM_LOCK;

    pOamFsMplsTpMeEntry = OamUtilGetMeEntryFromMegIndex (u4ContextId,
                                                         u4MegIndex, pu1MeName);

    if (pOamFsMplsTpMeEntry != NULL)
    {
        *pu4MeIndex = pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
        *pu4MpIndex = pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;

        OAM_UNLOCK;
        return OSIX_SUCCESS;
    }

    OAM_UNLOCK;
    return OSIX_FAILURE;
}

/****************************************************************************
* Function    : OamUtilGetMeRowStatus
* Description : Utility to Get Me entry's Rowstatus
* Input       : ContextId, MEG Index, ME Index, MP Index
* Output      : Rowstatus
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilGetMeRowStatus (UINT4 u4ContextId, UINT4 u4MegIndex,
                       UINT1 u4MeIndex, UINT4 u4MpIndex, UINT4 *pu4MeRowStatus)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryIn = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    pOamFsMplsTpMeEntryIn =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntryIn == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pOamFsMplsTpMeEntryIn, 0, sizeof (tOamFsMplsTpMeEntry));

    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpContextId = u4ContextId;
    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMegIndex = u4MegIndex;
    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMeIndex = u4MeIndex;
    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMeMpIndex = u4MpIndex;

    OAM_LOCK;

    pOamFsMplsTpMeEntry = OamGetFsMplsTpMeTable (pOamFsMplsTpMeEntryIn);

    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamFsMplsTpMeEntryIn);

    if (pOamFsMplsTpMeEntry != NULL)
    {
        *pu4MeRowStatus =
            (UINT4) pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus;

        OAM_UNLOCK;
        return OSIX_SUCCESS;
    }

    OAM_UNLOCK;
    return OSIX_FAILURE;
}

/****************************************************************************
* Function    : OamUtilGetMegRowStatus
* Description : Utility to Get Meg entry's Rowstatus
* Input       : ContextId, MEG Index
* Output      : Rowstatus
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilGetMegRowStatus (UINT4 u4ContextId, UINT4 u4MegIndex,
                        UINT4 *pu4MegRowStatus)
{
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;

    MEMSET (&OamFsMplsTpMegEntry, 0, sizeof (tOamFsMplsTpMegEntry));

    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId = u4ContextId;
    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex = u4MegIndex;

    OAM_LOCK;

    pOamFsMplsTpMegEntry = OamGetFsMplsTpMegTable (&OamFsMplsTpMegEntry);

    if (pOamFsMplsTpMegEntry != NULL)
    {
        *pu4MegRowStatus =
            (UINT4) pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus;

        OAM_UNLOCK;
        return OSIX_SUCCESS;
    }

    OAM_UNLOCK;
    return OSIX_FAILURE;
}

/****************************************************************************
* Function    : OamUtilGetMeEntryFromMegName
* Description : Utility to Get Me-Entry from MeName, MegName and ContextId
* Input       : Context Id, Meg name and Me name
* Output      : pointer to the ME structure
* Returns     : Pointer to the ME strucute or NULL
*****************************************************************************/
tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromMegName (UINT4 u4ContextId,
                              UINT1 *pu1MegName, UINT1 *pu1MeName)
{
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    UINT4               u4MegIndex = 0;

    MEMSET (&OamFsMplsTpMegEntry, 0, sizeof (OamFsMplsTpMegEntry));

    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId = u4ContextId;
    MEMCPY (OamFsMplsTpMegEntry.MibObject.au1FsMplsTpMegName, pu1MegName,
            sizeof (OamFsMplsTpMegEntry.MibObject.au1FsMplsTpMegName));

    pOamFsMplsTpMegEntry = RBTreeGet (gOamGlobals.FsMplsTpMegNameTable,
                                      (tRBElem *) & OamFsMplsTpMegEntry);

    if (pOamFsMplsTpMegEntry == NULL)
    {
        return NULL;
    }
    u4MegIndex = pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;

    return (OamUtilGetMeEntryFromMegIndex (u4ContextId, u4MegIndex, pu1MeName));
}

/****************************************************************************
* Function    : OamUtilGetMeEntryFromMegIccId
* Description : Utility to Get Me-Entry from Source ICC Mep-id and ContextId
* Input       : Context Id, ICC, UMC and Source MEP index
* Output      : pointer to the ME structure
* Returns     : Pointer to the ME strucute or NULL
*****************************************************************************/
tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromMegIccId (UINT4 u4ContextId, UINT1 *pu1Icc, UINT1 *pu1Umc,
                               UINT2 u2MepIndex)
{
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    UINT4               u4MegIndex = 0;

    MEMSET (&OamFsMplsTpMegEntry, 0, sizeof (OamFsMplsTpMegEntry));

    MEMCPY (OamFsMplsTpMegEntry.MibObject.au1FsMplsTpMegIdIcc, pu1Icc,
            sizeof (OamFsMplsTpMegEntry.MibObject.au1FsMplsTpMegIdIcc));

    MEMCPY (OamFsMplsTpMegEntry.MibObject.au1FsMplsTpMegIdUmc, pu1Umc,
            sizeof (OamFsMplsTpMegEntry.MibObject.au1FsMplsTpMegIdUmc));

    pOamFsMplsTpMegEntry = RBTreeGet (gOamGlobals.FsMplsTpMegIccIdTable,
                                      (tRBElem *) & OamFsMplsTpMegEntry);

    if (pOamFsMplsTpMegEntry == NULL)
    {
        return NULL;
    }
    u4MegIndex = pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;

    return (OamUtilGetMeEntryFromMegIccInfo
            (u4ContextId, u4MegIndex, u2MepIndex));
}

/****************************************************************************
* Function    : OamUtilGetMeEntryFromMegIndex
* Description : Utility to Get Me-Entry from MegIndex, MegName and ContextId
* Input       : Context Id, Meg Index and Me name 
* Output      : pointer to the ME structure
* Returns     : pOamFsMplsTpMeEntry or NULL 
*****************************************************************************/
tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromMegIndex (UINT4 u4ContextId,
                               UINT4 u4MegIndex, UINT1 *pu1MeName)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryIn = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    pOamFsMplsTpMeEntryIn =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntryIn == NULL)
    {
        return NULL;
    }
    MEMSET (pOamFsMplsTpMeEntryIn, 0, sizeof (tOamFsMplsTpMeEntry));

    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpContextId = u4ContextId;
    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMegIndex = u4MegIndex;
    pOamFsMplsTpMeEntry = RBTreeGetNext (gOamGlobals.OamGlbMib.FsMplsTpMeTable,
                                         (tRBElem *) pOamFsMplsTpMeEntryIn,
                                         NULL);

    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamFsMplsTpMeEntryIn);

    while (pOamFsMplsTpMeEntry != NULL)
    {
        if ((pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex != u4MegIndex) ||
            (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId != u4ContextId))
        {
            break;
        }

        if (STRCMP (pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                    pu1MeName) == 0)
        {
            return pOamFsMplsTpMeEntry;
        }

        pOamFsMplsTpMeEntry =
            RBTreeGetNext (gOamGlobals.OamGlbMib.FsMplsTpMeTable,
                           pOamFsMplsTpMeEntry, NULL);
    }
    return NULL;
}

/****************************************************************************
* Function    : OamUtilGetMeEntryFromMegIccInfo
* Description : Utility to Get Me-Entry from MegIndex, Mep Index and ContextId
* Input       : Context Id, Meg Index and Source MEP index
* Output      : pointer to the ME structure
* Returns     : pOamFsMplsTpMeEntry or NULL 
*****************************************************************************/
tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromMegIccInfo (UINT4 u4ContextId,
                                 UINT4 u4MegIndex, UINT2 u2MepIndex)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryIn = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    pOamFsMplsTpMeEntryIn =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntryIn == NULL)
    {
        return NULL;
    }
    MEMSET (pOamFsMplsTpMeEntryIn, 0, sizeof (tOamFsMplsTpMeEntry));

    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpContextId = u4ContextId;
    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMegIndex = u4MegIndex;
    pOamFsMplsTpMeEntry = RBTreeGetNext (gOamGlobals.OamGlbMib.FsMplsTpMeTable,
                                         (tRBElem *) pOamFsMplsTpMeEntryIn,
                                         NULL);

    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamFsMplsTpMeEntryIn);

    while (pOamFsMplsTpMeEntry != NULL)
    {
        if ((pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex != u4MegIndex) ||
            (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId != u4ContextId))
        {
            break;
        }

        if (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex
            == (UINT4) u2MepIndex)
        {
            return pOamFsMplsTpMeEntry;
        }

        pOamFsMplsTpMeEntry =
            RBTreeGetNext (gOamGlobals.OamGlbMib.FsMplsTpMeTable,
                           pOamFsMplsTpMeEntry, NULL);
    }
    return NULL;
}

/****************************************************************************
* Function    : OamUtilValidateBaseServiceOid
* Description : Utility to validate Base Service OID
* Input       : pOamFsMplsTpMeEntry
* Output      : pOutMplsApiInfo
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilValidateBaseServiceOid (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    UINT2               u2Len = 0;
    UINT1               u1Index = 0;
    BOOL1               bIsValid = OSIX_FALSE;

    pInMplsApiInfo = (tMplsApiInInfo *) MemAllocMemBlk (OAM_APIININFO_POOLID);
    if (pInMplsApiInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    pOutMplsApiInfo =
        (tMplsApiOutInfo *) MemAllocMemBlk (OAM_APIOUTINFO_POOLID);
    if (pOutMplsApiInfo == NULL)
    {
        MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
        return OSIX_FAILURE;
    }
    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    pOamFsMplsTpMegEntry = OamUtilGetAssociatedMegEntry (pOamFsMplsTpMeEntry);

    if (pOamFsMplsTpMegEntry == NULL)
    {
        OAM_TRC ((OAM_MAIN_TRC,
                  "\r%% Associated MEG entry does not exist.\r\n"));
        MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (OAM_APIOUTINFO_POOLID, (UINT1 *) pOutMplsApiInfo);
        return OSIX_FAILURE;
    }

    /* Service pointer with all zeros is valid to reset the service pointer */
    for (u1Index = 0;
         u1Index < pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;
         u1Index++)
    {
        if (pOamFsMplsTpMeEntry->MibObject.
            au4FsMplsTpMeServicePointer[u1Index] == 0)
        {
            bIsValid = OSIX_TRUE;
            continue;
        }
        else
        {
            bIsValid = OSIX_FALSE;
            break;
        }
    }
    if (bIsValid == OSIX_TRUE)
    {
        MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (OAM_APIOUTINFO_POOLID, (UINT1 *) pOutMplsApiInfo);
        return OSIX_SUCCESS;
    }
    /* Validate Base tunnel OID */
    if (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType
        == OAM_SERVICE_TYPE_LSP)
    {
        pInMplsApiInfo->u4SubReqType = MPLS_GET_BASE_TNL_OID;

        if (MplsCmnExtGetServicePointerOid (pInMplsApiInfo,
                                            pOutMplsApiInfo) == OSIX_FAILURE)
        {
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% Get service pointer OID failed.\r\n"));
            MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (OAM_APIOUTINFO_POOLID,
                                (UINT1 *) pOutMplsApiInfo);
            return OSIX_FAILURE;
        }

        u2Len = pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen;

        if (u2Len > OAM_MAX_OID_LEN)
        {
            MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (OAM_APIOUTINFO_POOLID,
                                (UINT1 *) pOutMplsApiInfo);
            return OSIX_FAILURE;
        }
        if (MEMCMP (pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
                    pOamFsMplsTpMeEntry->MibObject.
                    au4FsMplsTpMeServicePointer,
                    (u2Len <
                     MPLS_SERVICE_OID_LEN ? u2Len : MPLS_SERVICE_OID_LEN)) != 0)
        {
            OAM_TRC ((OAM_MAIN_TRC, "\r%% Invalid tunnel Row Pointer.\r\n"));
            MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (OAM_APIOUTINFO_POOLID,
                                (UINT1 *) pOutMplsApiInfo);
            return OSIX_FAILURE;
        }

        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen
            != (u2Len + TUNNEL_TABLE_INDICES))
        {
            OAM_TRC ((OAM_MAIN_TRC, "\r%% Invalid Row Pointer Length.\r\n"));
            MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (OAM_APIOUTINFO_POOLID,
                                (UINT1 *) pOutMplsApiInfo);
            return OSIX_FAILURE;
        }
    }
    /* Validate Base Pseudowire OID */
    else if (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType
             == OAM_SERVICE_TYPE_PW)
    {
        pInMplsApiInfo->u4SubReqType = MPLS_GET_BASE_PW_OID;

        /* L2VPN lock is not required as we directly 
         * access static memory of PwType */
        if (L2vpnExtGetPwServicePointerOid (pInMplsApiInfo,
                                            pOutMplsApiInfo) == OSIX_FAILURE)
        {
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% Get service pointer OID failed.\r\n"));
            MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (OAM_APIOUTINFO_POOLID,
                                (UINT1 *) pOutMplsApiInfo);
            return OSIX_FAILURE;
        }

        u2Len = pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen;

        if (u2Len > OAM_MAX_OID_LEN)
        {
            MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (OAM_APIOUTINFO_POOLID,
                                (UINT1 *) pOutMplsApiInfo);
            return OSIX_FAILURE;
        }

        if (MEMCMP (pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
                    pOamFsMplsTpMeEntry->MibObject.
                    au4FsMplsTpMeServicePointer,
                    (u2Len <
                     MPLS_SERVICE_OID_LEN ? u2Len : MPLS_SERVICE_OID_LEN)) != 0)
        {
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% Invalid Pseudowire Row Pointer.\r\n"));
            MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (OAM_APIOUTINFO_POOLID,
                                (UINT1 *) pOutMplsApiInfo);
            return OSIX_FAILURE;
        }

        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen
            != (u2Len + 1))
        {
            OAM_TRC ((OAM_MAIN_TRC, "\r%% Invalid Row Pointer Length.\r\n"));
            MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
            MemReleaseMemBlock (OAM_APIOUTINFO_POOLID,
                                (UINT1 *) pOutMplsApiInfo);
            return OSIX_FAILURE;
        }
    }

    MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (OAM_APIOUTINFO_POOLID, (UINT1 *) pOutMplsApiInfo);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamUtilGetTunnelInfo
* Description : Utility to Get Tunnel Information
* Input       : pOamFsMplsTpMeEntry
* Output      : pTeTnlInfo
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilGetTunnelInfo (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry,
                      tTeTnlInfo * pTeTnlInfo)
{
    UINT4               u4SrcTnlNum = 0;
    UINT4               u4LspNum = 0;
    UINT4               u4IngressLsrId = 0;
    UINT4               u4EgressLsrId = 0;
    UINT2               u2Len = 0;

    u2Len =
        (UINT2) pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;

    if ((u2Len < TUNNEL_TABLE_INDICES) || (u2Len >= OAM_MAX_OID_LEN))
    {
        return OSIX_FAILURE;
    }

    u2Len--;
    u4EgressLsrId = pOamFsMplsTpMeEntry->MibObject.
        au4FsMplsTpMeServicePointer[u2Len];

    u2Len--;
    u4IngressLsrId = pOamFsMplsTpMeEntry->MibObject.
        au4FsMplsTpMeServicePointer[u2Len];

    u2Len--;
    u4LspNum = pOamFsMplsTpMeEntry->MibObject.
        au4FsMplsTpMeServicePointer[u2Len];

    u2Len--;
    u4SrcTnlNum = pOamFsMplsTpMeEntry->MibObject.
        au4FsMplsTpMeServicePointer[u2Len];

    pTeTnlInfo = TeGetTunnelInfo (u4SrcTnlNum, u4LspNum, u4IngressLsrId,
                                  u4EgressLsrId);
    if (pTeTnlInfo == NULL)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Get Tunnel Information failed.\r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamUtilIsOamModuleEnabled
* Description : Utility to Check if OAM module is enabled or not
* Input       : Context Id
* Output      : Module status
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilIsOamModuleEnabled (UINT4 u4ContextId)
{
    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;

    MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));

    OamFsMplsTpGlobalConfigEntry.MibObject.u4FsMplsTpContextId = u4ContextId;

    pOamFsMplsTpGlobalConfigEntry = RBTreeGet
        (gOamGlobals.OamGlbMib.FsMplsTpGlobalConfigTable,
         (tRBElem *) & OamFsMplsTpGlobalConfigEntry);

    if (pOamFsMplsTpGlobalConfigEntry != NULL)
    {
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.
            i4FsMplsTpOamModuleStatus == OAM_MODULE_STATUS_ENABLED)
        {
            return OSIX_SUCCESS;
        }
    }
    return OSIX_FAILURE;
}

/****************************************************************************
* Function    : OamUtilGetFreeMeId
* Description : This function checks the existing ME entries associated
*               with the MEG and returns the available ME index.      
* Input       : Context Id and Meg Id
* Output      : Free Me Id
* Returns     : Free Me Id or OSIX_FALSE
*****************************************************************************/
UINT4
OamUtilGetFreeMeId (UINT4 u4ContextId, UINT4 u4MegIndex)
{

    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryIn = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    UINT4               u4AvailableIndex = OAM_ME_INDEX_MIN;

    pOamFsMplsTpMeEntryIn =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntryIn == NULL)
    {
        return OSIX_FALSE;
    }
    MEMSET (pOamFsMplsTpMeEntryIn, 0, sizeof (tOamFsMplsTpMeEntry));

    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpContextId = u4ContextId;
    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMegIndex = u4MegIndex;
    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMeMpIndex = OAM_MP_INDEX_MIN;

    OAM_LOCK;

    while (u4AvailableIndex < OAM_ME_INDEX_MAX)
    {
        pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMeIndex = u4AvailableIndex;

        pOamFsMplsTpMeEntry =
            RBTreeGet (gOamGlobals.OamGlbMib.FsMplsTpMeTable,
                       (tRBElem *) pOamFsMplsTpMeEntryIn);

        if (pOamFsMplsTpMeEntry == NULL)
        {
            OAM_UNLOCK;
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamFsMplsTpMeEntryIn);
            return u4AvailableIndex;
        }
        u4AvailableIndex++;
    }

    OAM_UNLOCK;
    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamFsMplsTpMeEntryIn);
    return OSIX_FALSE;
}

/****************************************************************************
* Function    : OamUtilGenMegName
* Description : This function generates a MEG name from the context Id
*               and MEG index and copies it to Meg name field of the 
*               given Meg entry
* Input       : pOamFsMplsTpMegEntry
* Output      : Meg Name
* Returns     : None
*****************************************************************************/
VOID
OamUtilGenMegName (tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry,
                   UINT1 *pu1MegName)
{
    /* Since the megname is of maximum 48 characters, fill max of 
     * 15 characters for context ID and 15 for MegIndex.
     * Hoping that ContextId and MegIndex will not be filled with big number.
     * Note that the returned megname will be searched for conflicts with 
     * existing names, before adding it to megname based search tree */
    SNPRINTF ((CHR1 *) pu1MegName, OAM_MEG_NAME_MAX_LEN,
              "gMEG%d%d",
              pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId,
              pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex);

}

/****************************************************************************
* Function    : OamUtilGenMeName
* Description : This function generates an ME name from the Me Index
*               and Mp index and copies it to Me name field of the 
*               given Me entry
* Input       : pOamFsMplsTpMeEntry
* Output      : Me Name
* Returns     : None
*****************************************************************************/
VOID
OamUtilGenMeName (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry, UINT1 *pMeName)
{
    /* Since the mename is of maximum 48 characters, fill max of 
     * 15 characters for context ID and 15 for MegIndex.
     * Hoping that ContextId and MegIndex will not be filled with big number.
     * Note that the returned mename will be searched for conflicts with 
     * existing names, before adding it to mename based search tree */
    SNPRINTF ((CHR1 *) pMeName, OAM_ME_NAME_MAX_LEN,
              "gME%d%d", pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
              pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex);

}

/****************************************************************************
 * * Function    : OamUtilGetAssociatedMegEntry
 * * Description : This function finds and returns the associated Meg entry
 * * Input       : pOamFsMplsTpMeEntry
 * * Output      : Associated Meg entry
 * * Returns     : pOamFsMplsTpMegEntry
 * **************************************************************************/
tOamFsMplsTpMegEntry *
OamUtilGetAssociatedMegEntry (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;

    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;

    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;

    /* Calling the exact GET function to find whether
     * there is an entry that matches the MegIndex. */
    pOamFsMplsTpMegEntry = OamGetFsMplsTpMegTable (&OamFsMplsTpMegEntry);

    return pOamFsMplsTpMegEntry;
}

/****************************************************************************
* Function    : OamUtilIsMeEntryExist
* Description : This function checks whether associated ME entry exists
*               for the given MEG entry or not.
* Input       : pOamFsMplsTpMegEntry
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilIsMeEntryExist (tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryIn = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    pOamFsMplsTpMeEntryIn =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntryIn == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pOamFsMplsTpMeEntryIn, 0, sizeof (tOamFsMplsTpMeEntry));

    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpContextId =
        pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;

    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMegIndex =
        pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;

    pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntryIn);

    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamFsMplsTpMeEntryIn);
    /* GetNext may return even if the MegIndex is not same.
     * Confirm it before returning success */
    if ((pOamFsMplsTpMeEntry == NULL) ||
        (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId !=
         pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId) ||
        (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex !=
         pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex))
    {
        /* GetNext has ended up with next higher value index entry. */
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamUtilGetMeEntryFromPwIndex
* Description : This function returns the ME entry associated with the PW
* Input       : pInMplsApiInfo - In API information
* Output      : ME entry
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromServiceInfo (tMplsApiInInfo * pInMplsApiInfo)
{
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryIn = NULL;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    UINT4               u4TnlIndex = 0;
    UINT4               u4TnlInst = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT2               u2Len = 0;
    BOOL1               bIsMeFound = FALSE;

    pOamFsMplsTpMeEntryIn =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntryIn == NULL)
    {
        return NULL;
    }

    MEMSET (pOamFsMplsTpMeEntryIn, 0, sizeof (tOamFsMplsTpMeEntry));

    /* Get the first MEG in the given context 
     * and then search for matching service ME entries */
    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
        pInMplsApiInfo->u4ContextId;
    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex = 0;

    pOamFsMplsTpMegEntry = OamGetNextFsMplsTpMegTable (&OamFsMplsTpMegEntry);
    while (pOamFsMplsTpMegEntry != NULL)
    {
        if (((pInMplsApiInfo->InServicePathId.u4PathType
              == MPLS_PATH_TYPE_TUNNEL) &&
             (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType
              != OAM_SERVICE_TYPE_LSP)) ||
            ((pInMplsApiInfo->InServicePathId.u4PathType
              == MPLS_PATH_TYPE_PW) &&
             (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType
              != OAM_SERVICE_TYPE_PW)))
        {
            pOamFsMplsTpMegEntry = OamGetNextFsMplsTpMegTable
                (pOamFsMplsTpMegEntry);
            continue;
        }
        /* Provide partial indices to fetch the first matching ME entry */
        pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpContextId =
            pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;

        pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMegIndex =
            pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;

        pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntryIn);
        while (pOamFsMplsTpMeEntry != NULL)
        {
            /* Break if ME entry goes beyond MEG's ME entries */
            if ((pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId !=
                 pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId) ||
                (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex !=
                 pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex))
            {
                break;
            }

            u2Len = (UINT2) pOamFsMplsTpMeEntry->MibObject.
                i4FsMplsTpMeServicePointerLen;

            if ((u2Len < TUNNEL_TABLE_INDICES) || (u2Len >= OAM_MAX_OID_LEN))
            {
                break;
            }

            if (pInMplsApiInfo->InServicePathId.u4PathType == MPLS_PATH_TYPE_PW)
            {
                if (pInMplsApiInfo->InServicePathId.PwId.u4PwIndex
                    == pOamFsMplsTpMeEntry->MibObject.
                    au4FsMplsTpMeServicePointer[u2Len - 1])
                {
                    bIsMeFound = TRUE;
                    break;
                }
            }
            else if (pInMplsApiInfo->InServicePathId.u4PathType
                     == MPLS_PATH_TYPE_TUNNEL)
            {
                u2Len--;
                u4EgressId = pOamFsMplsTpMeEntry->MibObject.
                    au4FsMplsTpMeServicePointer[u2Len];
                u2Len--;
                u4IngressId = pOamFsMplsTpMeEntry->MibObject.
                    au4FsMplsTpMeServicePointer[u2Len];
                u2Len--;
                u4TnlInst = pOamFsMplsTpMeEntry->MibObject.
                    au4FsMplsTpMeServicePointer[u2Len];
                u2Len--;
                u4TnlIndex = pOamFsMplsTpMeEntry->MibObject.
                    au4FsMplsTpMeServicePointer[u2Len];

                if ((u4TnlIndex ==
                     pInMplsApiInfo->InServicePathId.TnlId.u4SrcTnlNum) &&
                    (u4TnlInst ==
                     pInMplsApiInfo->InServicePathId.TnlId.u4LspNum) &&
                    (u4IngressId ==
                     pInMplsApiInfo->InServicePathId.TnlId.SrcNodeId.
                     MplsRouterId.u4_addr[0]) &&
                    (u4EgressId ==
                     pInMplsApiInfo->InServicePathId.TnlId.DstNodeId.
                     MplsRouterId.u4_addr[0]))
                {
                    bIsMeFound = TRUE;
                    break;
                }
            }
            pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable
                (pOamFsMplsTpMeEntry);
        }
        if (bIsMeFound)
        {
            break;
        }
        pOamFsMplsTpMegEntry =
            OamGetNextFsMplsTpMegTable (pOamFsMplsTpMegEntry);
    }

    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamFsMplsTpMeEntryIn);

    if (bIsMeFound)
    {
        return pOamFsMplsTpMeEntry;
    }
    return NULL;
}

/****************************************************************************
* Function    : OamUtilIsMeEntryExist
* Description : This function checks whether associated ME entry exists
*               for the given MEG entry or not.
* Input       : pOamFsMplsTpMegEntry
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilIsMeNameExist (UINT4 u4ContextId, UINT4 u4MegIndex, UINT4 u4MeIndex,
                      UINT4 u4MeMpIndex, UINT1 *pu1MeName)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryIn = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    pOamFsMplsTpMeEntryIn =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntryIn == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pOamFsMplsTpMeEntryIn, 0, sizeof (tOamFsMplsTpMeEntry));

    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpContextId = u4ContextId;
    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMegIndex = u4MegIndex;

    pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntryIn);

    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamFsMplsTpMeEntryIn);

    while (pOamFsMplsTpMeEntry != NULL)
    {
        if ((pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId == u4ContextId)
            && (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex == u4MegIndex)
            && (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex == u4MeIndex)
            && (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex ==
                u4MeMpIndex))
        {
            pOamFsMplsTpMeEntry =
                OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntry);
            continue;
        }

        if ((pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex != u4MegIndex) ||
            (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId != u4ContextId))
        {
            break;
        }

        if (STRCMP (pOamFsMplsTpMeEntry->MibObject.
                    au1FsMplsTpMeName, pu1MeName) == 0)
        {
            return OSIX_SUCCESS;
        }

        pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntry);
    }
    return OSIX_FAILURE;
}

/****************************************************************************
* Function    : OamUtilCheckIfActiveMeExists
* Description : This function checks whether active ME entry exists
*               for the given MEG entry or not.
* Input       : pOamFsMplsTpMegEntry
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilCheckIfActiveMeExists (tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryIn = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    pOamFsMplsTpMeEntryIn =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntryIn == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pOamFsMplsTpMeEntryIn, 0, sizeof (tOamFsMplsTpMeEntry));

    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpContextId =
        pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;

    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMegIndex =
        pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;

    pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntryIn);

    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamFsMplsTpMeEntryIn);

    while (pOamFsMplsTpMeEntry != NULL)
    {
        if ((pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId !=
             pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId) ||
            (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex !=
             pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex))
        {
            break;
        }
        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus == ACTIVE)
        {
            return OSIX_SUCCESS;
        }
        pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntry);
    }

    return OSIX_FAILURE;
}

/****************************************************************************
* Function    : OamUtilGetVcIdFromPwIndex
* Description : This function returns the VC-Id from the given PW Index
* Input       : Pw Index
* Output      : VC Id
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamUtilGetVcIdFromPwIndex (UINT4 u4PwIndex, UINT4 *pu4VcId)
{
    tMplsApiInInfo     *pInMplsApiInfo;
    tMplsApiOutInfo    *pOutMplsApiInfo;

    pInMplsApiInfo = (tMplsApiInInfo *) MemAllocMemBlk (OAM_APIININFO_POOLID);
    if (pInMplsApiInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    pOutMplsApiInfo =
        (tMplsApiOutInfo *) MemAllocMemBlk (OAM_APIOUTINFO_POOLID);
    if (pOutMplsApiInfo == NULL)
    {
        MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
        return OSIX_FAILURE;
    }
    MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));
    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));

    pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
    pInMplsApiInfo->InPathId.PwId.u4PwIndex = u4PwIndex;

    if (L2VpnApiHandleExternalRequest (MPLS_GET_PW_INFO, pInMplsApiInfo,
                                       pOutMplsApiInfo) == OSIX_SUCCESS)
    {
        *pu4VcId = pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4VcId;
        MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (OAM_APIOUTINFO_POOLID, (UINT1 *) pOutMplsApiInfo);
        return OSIX_SUCCESS;
    }

    MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (OAM_APIOUTINFO_POOLID, (UINT1 *) pOutMplsApiInfo);
    return OSIX_FAILURE;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  oamutil.c                     */
/*-----------------------------------------------------------------------*/
