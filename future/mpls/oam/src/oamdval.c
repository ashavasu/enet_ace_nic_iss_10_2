/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamdval.c,v 1.4 2012/03/19 11:28:20 siva Exp $
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Oam 
*********************************************************************/

#include "oaminc.h"

/****************************************************************************
* Function    : OamInitializeFsMplsTpNodeMapTable
* Input       : pOamFsMplsTpNodeMapEntry
* Output      : It Intialize the given structure
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamInitializeFsMplsTpNodeMapTable (tOamFsMplsTpNodeMapEntry *
                                   pOamFsMplsTpNodeMapEntry)
{
    OAM_TRC_FUNC ((OAM_FN_ENTRY, "FUNC:OamInitializeFsMplsTpNodeMapTable\n"));

    if (pOamFsMplsTpNodeMapEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((OamInitializeMibFsMplsTpNodeMapTable (pOamFsMplsTpNodeMapEntry)) ==
        OSIX_FAILURE)
    {
        OAM_TRC ((OAM_MAIN_TRC,
                  "\r%% Initialization failed for Node Map Table entry.\r\n"));
        return OSIX_FAILURE;
    }

    OAM_TRC_FUNC ((OAM_FN_EXIT, "EXIT:OamInitializeFsMplsTpNodeMapTable\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamInitializeFsMplsTpMegTable
* Input       : pOamFsMplsTpMegEntry
* Output      : It Intialize the given structure
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamInitializeFsMplsTpMegTable (tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry)
{
    OAM_TRC_FUNC ((OAM_FN_ENTRY, "FUNC:OamInitializeFsMplsTpMegTable\n"));

    if (pOamFsMplsTpMegEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((OamInitializeMibFsMplsTpMegTable (pOamFsMplsTpMegEntry)) ==
        OSIX_FAILURE)
    {
        OAM_TRC ((OAM_MAIN_TRC,
                  "\r%% Initialization failed for MEG Table entry.\r\n"));
        return OSIX_FAILURE;
    }

    OAM_TRC_FUNC ((OAM_FN_EXIT, "EXIT:OamInitializeFsMplsTpMegTable\n"));
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamInitializeFsMplsTpMeTable
* Input       : pOamFsMplsTpMeEntry
* Output      : It Intialize the given structure
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamInitializeFsMplsTpMeTable (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{
    OAM_TRC_FUNC ((OAM_FN_ENTRY, "FUNC:OamInitializeFsMplsTpMeTable\n"));

    if (pOamFsMplsTpMeEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pOamFsMplsTpMeEntry, 0, sizeof (tOamFsMplsTpMeEntry));

    if ((OamInitializeMibFsMplsTpMeTable (pOamFsMplsTpMeEntry)) == OSIX_FAILURE)
    {
        OAM_TRC ((OAM_MAIN_TRC,
                  "\r%% Initialization failed for ME Table entry.\r\n"));
        return OSIX_FAILURE;
    }

    OAM_TRC_FUNC ((OAM_FN_EXIT, "EXIT:OamInitializeFsMplsTpMeTable\n"));
    return OSIX_SUCCESS;
}
