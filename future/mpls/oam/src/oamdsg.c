/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamdsg.c,v 1.3 2012/03/19 11:28:20 siva Exp $
*
* Description: This file contains the routines for DataStructure access for the module Oam 
*********************************************************************/

#include "oaminc.h"

/****************************************************************************
 Function    :  OamGetFirstFsMplsTpGlobalConfigTable
 Input       :  The Indices
                NONE
 Output      :  This Routine is used to take
                first index from a table
 Returns     :  pOamFsMplsTpGlobalConfigEntry or NULL
****************************************************************************/
tOamFsMplsTpGlobalConfigEntry *
OamGetFirstFsMplsTpGlobalConfigTable ()
{
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;

    pOamFsMplsTpGlobalConfigEntry =
        (tOamFsMplsTpGlobalConfigEntry *) RBTreeGetFirst (gOamGlobals.OamGlbMib.
                                                          FsMplsTpGlobalConfigTable);

    return pOamFsMplsTpGlobalConfigEntry;
}

/****************************************************************************
 Function    :  OamGetNextFsMplsTpGlobalConfigTable
 Input       :  The Indices
                pCurrentOamFsMplsTpGlobalConfigEntry
 Output      :  This Routine is used to take
                next index from a table
 Returns     :  pNextOamFsMplsTpGlobalConfigEntry or NULL
****************************************************************************/
tOamFsMplsTpGlobalConfigEntry *
OamGetNextFsMplsTpGlobalConfigTable (tOamFsMplsTpGlobalConfigEntry *
                                     pCurrentOamFsMplsTpGlobalConfigEntry)
{
    tOamFsMplsTpGlobalConfigEntry *pNextOamFsMplsTpGlobalConfigEntry = NULL;

    pNextOamFsMplsTpGlobalConfigEntry =
        (tOamFsMplsTpGlobalConfigEntry *) RBTreeGetNext (gOamGlobals.OamGlbMib.
                                                         FsMplsTpGlobalConfigTable,
                                                         (tRBElem *)
                                                         pCurrentOamFsMplsTpGlobalConfigEntry,
                                                         NULL);

    return pNextOamFsMplsTpGlobalConfigEntry;
}

/****************************************************************************
 Function    :  OamGetFsMplsTpGlobalConfigTable
 Input       :  The Indices
                pOamFsMplsTpGlobalConfigEntry
 Output      :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetOamFsMplsTpGlobalConfigEntry or NULL
****************************************************************************/
tOamFsMplsTpGlobalConfigEntry *
OamGetFsMplsTpGlobalConfigTable (tOamFsMplsTpGlobalConfigEntry *
                                 pOamFsMplsTpGlobalConfigEntry)
{
    tOamFsMplsTpGlobalConfigEntry *pGetOamFsMplsTpGlobalConfigEntry = NULL;

    pGetOamFsMplsTpGlobalConfigEntry =
        (tOamFsMplsTpGlobalConfigEntry *) RBTreeGet (gOamGlobals.OamGlbMib.
                                                     FsMplsTpGlobalConfigTable,
                                                     (tRBElem *)
                                                     pOamFsMplsTpGlobalConfigEntry);

    return pGetOamFsMplsTpGlobalConfigEntry;
}

/****************************************************************************
 Function    :  OamGetFirstFsMplsTpNodeMapTable
 Input       :  The Indices
                NONE
 Output      :  This Routine is used to take
                first index from a table
 Returns     :  pOamFsMplsTpNodeMapEntry or NULL
****************************************************************************/
tOamFsMplsTpNodeMapEntry *
OamGetFirstFsMplsTpNodeMapTable ()
{
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;

    pOamFsMplsTpNodeMapEntry =
        (tOamFsMplsTpNodeMapEntry *) RBTreeGetFirst (gOamGlobals.OamGlbMib.
                                                     FsMplsTpNodeMapTable);

    return pOamFsMplsTpNodeMapEntry;
}

/****************************************************************************
 Function    :  OamGetNextFsMplsTpNodeMapTable
 Input       :  The Indices
                pCurrentOamFsMplsTpNodeMapEntry
 Output      :  This Routine is used to take
                next index from a table
 Returns     :  pNextOamFsMplsTpNodeMapEntry or NULL
****************************************************************************/
tOamFsMplsTpNodeMapEntry *
OamGetNextFsMplsTpNodeMapTable (tOamFsMplsTpNodeMapEntry *
                                pCurrentOamFsMplsTpNodeMapEntry)
{
    tOamFsMplsTpNodeMapEntry *pNextOamFsMplsTpNodeMapEntry = NULL;

    pNextOamFsMplsTpNodeMapEntry =
        (tOamFsMplsTpNodeMapEntry *) RBTreeGetNext (gOamGlobals.OamGlbMib.
                                                    FsMplsTpNodeMapTable,
                                                    (tRBElem *)
                                                    pCurrentOamFsMplsTpNodeMapEntry,
                                                    NULL);

    return pNextOamFsMplsTpNodeMapEntry;
}

/****************************************************************************
 Function    :  OamGetFsMplsTpNodeMapTable
 Input       :  The Indices
                pOamFsMplsTpNodeMapEntry
 Output      :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetOamFsMplsTpNodeMapEntry or NULL
****************************************************************************/
tOamFsMplsTpNodeMapEntry *
OamGetFsMplsTpNodeMapTable (tOamFsMplsTpNodeMapEntry * pOamFsMplsTpNodeMapEntry)
{
    tOamFsMplsTpNodeMapEntry *pGetOamFsMplsTpNodeMapEntry = NULL;

    pGetOamFsMplsTpNodeMapEntry =
        (tOamFsMplsTpNodeMapEntry *) RBTreeGet (gOamGlobals.OamGlbMib.
                                                FsMplsTpNodeMapTable,
                                                (tRBElem *)
                                                pOamFsMplsTpNodeMapEntry);

    return pGetOamFsMplsTpNodeMapEntry;
}

/****************************************************************************
 Function    :  OamGetFirstFsMplsTpMegTable
 Input       :  The Indices
                NONE
 Output      :  This Routine is used to take
                first index from a table
 Returns     :  pOamFsMplsTpMegEntry or NULL
****************************************************************************/
tOamFsMplsTpMegEntry *
OamGetFirstFsMplsTpMegTable ()
{
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;

    pOamFsMplsTpMegEntry =
        (tOamFsMplsTpMegEntry *) RBTreeGetFirst (gOamGlobals.OamGlbMib.
                                                 FsMplsTpMegTable);

    return pOamFsMplsTpMegEntry;
}

/****************************************************************************
 Function    :  OamGetNextFsMplsTpMegTable
 Input       :  The Indices
                pCurrentOamFsMplsTpMegEntry
 Output      :  This Routine is used to take
                next index from a table
 Returns     :  pNextOamFsMplsTpMegEntry or NULL
****************************************************************************/
tOamFsMplsTpMegEntry *
OamGetNextFsMplsTpMegTable (tOamFsMplsTpMegEntry * pCurrentOamFsMplsTpMegEntry)
{
    tOamFsMplsTpMegEntry *pNextOamFsMplsTpMegEntry = NULL;

    pNextOamFsMplsTpMegEntry =
        (tOamFsMplsTpMegEntry *) RBTreeGetNext (gOamGlobals.OamGlbMib.
                                                FsMplsTpMegTable,
                                                (tRBElem *)
                                                pCurrentOamFsMplsTpMegEntry,
                                                NULL);

    return pNextOamFsMplsTpMegEntry;
}

/****************************************************************************
 Function    :  OamGetFsMplsTpMegTable
 Input       :  The Indices
                pOamFsMplsTpMegEntry
 Output      :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetOamFsMplsTpMegEntry or NULL
****************************************************************************/
tOamFsMplsTpMegEntry *
OamGetFsMplsTpMegTable (tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry)
{
    tOamFsMplsTpMegEntry *pGetOamFsMplsTpMegEntry = NULL;

    pGetOamFsMplsTpMegEntry =
        (tOamFsMplsTpMegEntry *) RBTreeGet (gOamGlobals.OamGlbMib.
                                            FsMplsTpMegTable,
                                            (tRBElem *) pOamFsMplsTpMegEntry);

    return pGetOamFsMplsTpMegEntry;
}

/****************************************************************************
 Function    :  OamGetFirstFsMplsTpMeTable
 Input       :  The Indices
                NONE
 Output      :  This Routine is used to take
                first index from a table
 Returns     :  pOamFsMplsTpMeEntry or NULL
****************************************************************************/
tOamFsMplsTpMeEntry *
OamGetFirstFsMplsTpMeTable ()
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    pOamFsMplsTpMeEntry =
        (tOamFsMplsTpMeEntry *) RBTreeGetFirst (gOamGlobals.OamGlbMib.
                                                FsMplsTpMeTable);

    return pOamFsMplsTpMeEntry;
}

/****************************************************************************
 Function    :  OamGetNextFsMplsTpMeTable
 Input       :  The Indices
                pCurrentOamFsMplsTpMeEntry
 Output      :  This Routine is used to take
                next index from a table
 Returns     :  pNextOamFsMplsTpMeEntry or NULL
****************************************************************************/
tOamFsMplsTpMeEntry *
OamGetNextFsMplsTpMeTable (tOamFsMplsTpMeEntry * pCurrentOamFsMplsTpMeEntry)
{
    tOamFsMplsTpMeEntry *pNextOamFsMplsTpMeEntry = NULL;

    pNextOamFsMplsTpMeEntry =
        (tOamFsMplsTpMeEntry *) RBTreeGetNext (gOamGlobals.OamGlbMib.
                                               FsMplsTpMeTable,
                                               (tRBElem *)
                                               pCurrentOamFsMplsTpMeEntry,
                                               NULL);

    return pNextOamFsMplsTpMeEntry;
}

/****************************************************************************
 Function    :  OamGetFsMplsTpMeTable
 Input       :  The Indices
                pOamFsMplsTpMeEntry
 Output      :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetOamFsMplsTpMeEntry or NULL
****************************************************************************/
tOamFsMplsTpMeEntry *
OamGetFsMplsTpMeTable (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{
    tOamFsMplsTpMeEntry *pGetOamFsMplsTpMeEntry = NULL;

    pGetOamFsMplsTpMeEntry =
        (tOamFsMplsTpMeEntry *) RBTreeGet (gOamGlobals.OamGlbMib.
                                           FsMplsTpMeTable,
                                           (tRBElem *) pOamFsMplsTpMeEntry);

    return pGetOamFsMplsTpMeEntry;
}

/****************************************************************************
 Function    :  OamGetFsMplsTpOamContextName
 Input       :  The Indices
                pFsMplsTpOamContextName
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamGetFsMplsTpOamContextName (UINT1 *pFsMplsTpOamContextName)
{
    MEMCPY (pFsMplsTpOamContextName,
            gOamGlobals.OamGlbMib.au1FsMplsTpOamContextName,
            gOamGlobals.OamGlbMib.i4FsMplsTpOamContextNameLen);

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OamGetFsMplsTpOamMegOperStatus
 Input       :  The Indices
                pi4FsMplsTpOamMegOperStatus
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamGetFsMplsTpOamMegOperStatus (INT4 *pi4FsMplsTpOamMegOperStatus)
{
    *pi4FsMplsTpOamMegOperStatus =
        gOamGlobals.OamGlbMib.i4FsMplsTpOamMegOperStatus;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OamGetFsMplsTpOamMegSubOperStatus
 Input       :  The Indices
                pFsMplsTpOamMegSubOperStatus
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamGetFsMplsTpOamMegSubOperStatus (UINT1 *pFsMplsTpOamMegSubOperStatus)
{
    MEMCPY (pFsMplsTpOamMegSubOperStatus,
            gOamGlobals.OamGlbMib.au1FsMplsTpOamMegSubOperStatus,
            gOamGlobals.OamGlbMib.i4FsMplsTpOamMegSubOperStatusLen);

    return OSIX_SUCCESS;
}
