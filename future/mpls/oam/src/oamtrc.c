/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: oamtrc.c,v 1.3 2010/11/25 14:57:03 siva Exp $
*
* Description: This file contains the automatically generated trace 
*              files for MPLS OAM module.
*********************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "oaminc.h"

/****************************************************************************
*                                                                           *
* Function     : OamTrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
OamTrcPrint (const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime        SysTime = 0;
    const char         *pc1Fname = fname;

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
        {
            pc1Fname = (fname + 1);
        }
        fname++;
    }
    OsixGetSysTime (&SysTime);
    PRINTF ("OAM: %d:%s:%d:   %s", SysTime, pc1Fname, u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : OamTrcWrite                                               *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
OamTrcWrite (CHR1 * s)
{
    if (s != NULL)
    {
        PRINTF ("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : OamTrc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

CHR1               *
OamTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define OAM_TRC_BUF_SIZE    2000
    static CHR1         ac1buf[OAM_TRC_BUF_SIZE];

    if ((u4Flags & OAM_TRC_FLAG) > 0)
    {
        va_start (ap, fmt);
        vsprintf (&ac1buf[0], fmt, ap);
        va_end (ap);

        return (&ac1buf[0]);
    }
    return (NULL);
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  oamtrc.c                      */
/*-----------------------------------------------------------------------*/
