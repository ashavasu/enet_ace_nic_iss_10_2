/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamutl.c,v 1.10 2014/07/19 12:52:32 siva Exp $
*
* Description: This file contains utility functions used by protocol Oam
*********************************************************************/

#include "oaminc.h"
#include "mplsapi.h"
#include "teextrn.h"
#include "l2vpextn.h"
#include "mplsdefs.h"
#include "inmgrex.h"
#include "indexmgr.h"

INT4
OamGetAllUtlFsMplsTpGlobalConfigTable (tOamFsMplsTpGlobalConfigEntry *
                                       pOamGetFsMplsTpGlobalConfigEntry,
                                       tOamFsMplsTpGlobalConfigEntry *
                                       pOamdsFsMplsTpGlobalConfigEntry)
{
    UNUSED_PARAM (pOamGetFsMplsTpGlobalConfigEntry);
    UNUSED_PARAM (pOamdsFsMplsTpGlobalConfigEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OamUtilUpdateFsMplsTpGlobalConfigTable
 * Input       :  The Indices
                pOamFsMplsTpGlobalConfigEntry
                pOamSetFsMplsTpGlobalConfigEntry
 * Output      :  This Routine checks set value 
                with that of the value in database
                and do the necessary protocol operation
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamUtilUpdateFsMplsTpGlobalConfigTable (tOamFsMplsTpGlobalConfigEntry *
                                        pOamOldFsMplsTpGlobalConfigEntry,
                                        tOamFsMplsTpGlobalConfigEntry *
                                        pOamFsMplsTpGlobalConfigEntry,
                                        tOamIsSetFsMplsTpGlobalConfigEntry *
                                        pOamIsSetFsMplsTpGlobalConfigEntry)
{
    UNUSED_PARAM (pOamOldFsMplsTpGlobalConfigEntry);

    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpTraceLevel != OSIX_FALSE)
    {
        OAM_TRC_FLAG = pOamFsMplsTpGlobalConfigEntry->MibObject.
            u4FsMplsTpTraceLevel;
    }

    return OSIX_SUCCESS;
}

INT4
OamGetAllUtlFsMplsTpNodeMapTable (tOamFsMplsTpNodeMapEntry *
                                  pOamGetFsMplsTpNodeMapEntry,
                                  tOamFsMplsTpNodeMapEntry *
                                  pOamdsFsMplsTpNodeMapEntry)
{
    UNUSED_PARAM (pOamGetFsMplsTpNodeMapEntry);
    UNUSED_PARAM (pOamdsFsMplsTpNodeMapEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OamUtilUpdateFsMplsTpNodeMapTable
 * Input       :  The Indices
                pOamFsMplsTpNodeMapEntry
                pOamSetFsMplsTpNodeMapEntry
 * Output      :  This Routine checks set value 
                with that of the value in database
                and do the necessary protocol operation
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamUtilUpdateFsMplsTpNodeMapTable (tOamFsMplsTpNodeMapEntry *
                                   pOamOldFsMplsTpNodeMapEntry,
                                   tOamFsMplsTpNodeMapEntry *
                                   pOamFsMplsTpNodeMapEntry,
                                   tOamIsSetFsMplsTpNodeMapEntry *
                                   pOamIsSetFsMplsTpNodeMapEntry)
{
    /* Check if the rowstatus object is set. If so handle the rowstatus
     * change by calling the rowstatus handler. */
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus != OSIX_FALSE)
    {
        if (OamUtlNodeMapTableRowStatusHdl (pOamOldFsMplsTpNodeMapEntry,
                                            pOamFsMplsTpNodeMapEntry) ==
            OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

INT4
OamGetAllUtlFsMplsTpMegTable (tOamFsMplsTpMegEntry * pOamGetFsMplsTpMegEntry,
                              tOamFsMplsTpMegEntry * pOamdsFsMplsTpMegEntry)
{
    UNUSED_PARAM (pOamGetFsMplsTpMegEntry);
    UNUSED_PARAM (pOamdsFsMplsTpMegEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OamUtilUpdateFsMplsTpMegTable
 * Input       :  The Indices
                pOamFsMplsTpMegEntry
                pOamSetFsMplsTpMegEntry
 * Output      :  This Routine checks set value 
                with that of the value in database
                and do the necessary protocol operation
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamUtilUpdateFsMplsTpMegTable (tOamFsMplsTpMegEntry * pOamOldFsMplsTpMegEntry,
                               tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry,
                               tOamFsMplsTpMegEntry * pOamNewFsMplsTpMegEntry,
                               tOamIsSetFsMplsTpMegEntry *
                               pOamIsSetFsMplsTpMegEntry)
{
    /* If new meg name is configured, check if the Meg name
     * is already in use. If not add the name to the RB Tree,
     * else return failure. If Meg name was already configured,
     * remove the old name from the RB Tree. */
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegName != OSIX_FALSE)
    {
        if (RBTreeGet (gOamGlobals.FsMplsTpMegNameTable,
                       (tRBElem *) pOamNewFsMplsTpMegEntry) != NULL)
        {
            /* Meg name is already assigned */
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% Meg name conflict. User has to modify"
                      "Meg name.\r\n"));
            return OSIX_FAILURE;
        }

        if ((pOamOldFsMplsTpMegEntry != NULL) &&
            (pOamOldFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen != 0))
        {
            RBTreeRem (gOamGlobals.FsMplsTpMegNameTable,
                       (tRBElem *) pOamOldFsMplsTpMegEntry);
        }

        MEMSET (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName, 0,
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen);

        MEMCPY (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
                pOamNewFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
                pOamNewFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen);

        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen =
            pOamNewFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen;

        if (RBTreeAdd
            (gOamGlobals.FsMplsTpMegNameTable,
             (tRBElem *) pOamFsMplsTpMegEntry) != RB_SUCCESS)
        {
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% RBTree Add failed for Meg Name table.\r\n"));
            return OSIX_FAILURE;
        }
    }

    /* Check if the rowstatus object is set. If so handle the rowstatus
     * change by calling the rowstatus handler. */
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus != OSIX_FALSE)
    {
        if (OamUtlMegTableRowStatusHdl (pOamOldFsMplsTpMegEntry,
                                        pOamFsMplsTpMegEntry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

INT4
OamGetAllUtlFsMplsTpMeTable (tOamFsMplsTpMeEntry * pOamGetFsMplsTpMeEntry,
                             tOamFsMplsTpMeEntry * pOamdsFsMplsTpMeEntry)
{
    pOamGetFsMplsTpMeEntry->u4FsMplsTpMeOperStatus =
        pOamdsFsMplsTpMeEntry->u4FsMplsTpMeOperStatus;

    pOamGetFsMplsTpMeEntry->u4FsMplsTpMeApps =
        pOamdsFsMplsTpMeEntry->u4FsMplsTpMeApps;

    pOamGetFsMplsTpMeEntry->u4FsMplsTpPwVcId =
        pOamdsFsMplsTpMeEntry->u4FsMplsTpPwVcId;

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OamUtilUpdateFsMplsTpMeTable
 * Input       :  The Indices
                pOamFsMplsTpMeEntry
                pOamSetFsMplsTpMeEntry
 * Output      :  This Routine checks set value 
                with that of the value in database
                and do the necessary protocol operation
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamUtilUpdateFsMplsTpMeTable (tOamFsMplsTpMeEntry * pOamOldFsMplsTpMeEntry,
                              tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry,
                              tOamIsSetFsMplsTpMeEntry *
                              pOamIsSetFsMplsTpMeEntry)
{
    /* Check if the ME name exists in the MEG associated. */
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeName != OSIX_FALSE)
    {
        if (OamUtilIsMeNameExist
            (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
             pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
             pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
             pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
             pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName) == OSIX_SUCCESS)
        {
            /* Me name is already assigned */
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% Me name conflict. User has to modify Me name.\r\n"));
            return OSIX_FAILURE;
        }
    }

    /* Check if the rowstatus object is set. If so handle the rowstatus
     * change by calling the rowstatus handler. */
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer != OSIX_FALSE)
    {
        /* Call OamResetMegIndicesInLspOrPw to reset the megindices in 
         * the old PW/LSP entry */
        if ((pOamOldFsMplsTpMeEntry != NULL) &&
            (pOamOldFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeServicePointerLen != 0))
        {
            if (OamResetMegIndicesInLspOrPw (pOamOldFsMplsTpMeEntry)
                == OSIX_FAILURE)
            {
                OAM_TRC ((OAM_MAIN_TRC,
                          "\r%% Meg status notification failed.\r\n"));
                return OSIX_FAILURE;
            }
        }
    }

    /* Check if the rowstatus object is set. If so handle the rowstatus
     * change by calling the rowstatus handler. */
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus != OSIX_FALSE)
    {
        if (OamUtlMeTableRowStatusHdl (pOamOldFsMplsTpMeEntry,
                                       pOamFsMplsTpMeEntry) == OSIX_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OamUtlRBTreeDestroy
 * Input       :  None
 * Output      :  None 
 * Returns     :  None
****************************************************************************/
VOID
OamUtlRBTreeDestroy (VOID)
{

    RBTreeDestroy (gOamGlobals.OamGlbMib.FsMplsTpGlobalConfigTable, NULL, 0);
    RBTreeDestroy (gOamGlobals.OamGlbMib.FsMplsTpNodeMapTable, NULL, 0);
    RBTreeDestroy (gOamGlobals.OamGlbMib.FsMplsTpMegTable, NULL, 0);
    RBTreeDestroy (gOamGlobals.OamGlbMib.FsMplsTpMeTable, NULL, 0);
    RBTreeDestroy (gOamGlobals.FsMplsTpMegNameTable, NULL, 0);
    RBTreeDestroy (gOamGlobals.OamGblNodeDualSearchTree, NULL, 0);
    return;
}

/****************************************************************************
 * Function    :  OamUtlNodeMapTableRowStatusHdl
 * Input       :  pOamOldFsMplsTpNodeMapEntry,
 *                pOamFsMplsTpNodeMapEntry
 * Output      :  This routine handles the rowstatus change,
 *                does necessary validation and notification
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamUtlNodeMapTableRowStatusHdl (tOamFsMplsTpNodeMapEntry *
                                pOamOldFsMplsTpNodeMapEntry,
                                tOamFsMplsTpNodeMapEntry *
                                pOamFsMplsTpNodeMapEntry)
{
    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;

    UNUSED_PARAM (pOamOldFsMplsTpNodeMapEntry);

    MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));
    switch (pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus)
    {
        case ACTIVE:
            /* Check if Node Id is non-zero, if zero
             * return failure, as row cannot be made 
             * active without a valid Node Id. */
            if (pOamFsMplsTpNodeMapEntry->MibObject.
                u4FsMplsTpNodeMapNodeId == 0)
            {
                OAM_TRC ((OAM_MAIN_TRC, "\r%% Rowstatus cannot be made active \
                                    with invalid node Id.\r\n"));
                return OSIX_FAILURE;
            }
            /* Check if Global Id is non-zero, if zero
             * fill global Id from Global Config table.
             * It is assumed that Global config table will
             * have a valid Global Id. */
            else if (pOamFsMplsTpNodeMapEntry->MibObject.
                     u4FsMplsTpNodeMapGlobalId == 0)
            {
                OamFsMplsTpGlobalConfigEntry.MibObject.
                    u4FsMplsTpContextId =
                    pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId;

                pOamFsMplsTpGlobalConfigEntry = OamGetFsMplsTpGlobalConfigTable
                    (&OamFsMplsTpGlobalConfigEntry);

                if ((pOamFsMplsTpGlobalConfigEntry != NULL) &&
                    (pOamFsMplsTpGlobalConfigEntry->MibObject.
                     u4FsMplsTpGlobalId != 0))
                {
                    pOamFsMplsTpNodeMapEntry->MibObject.
                        u4FsMplsTpNodeMapGlobalId =
                        pOamFsMplsTpGlobalConfigEntry->MibObject.
                        u4FsMplsTpGlobalId;
                }
                else
                {
                    OAM_TRC ((OAM_MAIN_TRC,
                              "\r%% Global Config entry not found or "
                              "Global-ID not configured\r\n"));
                    return OSIX_FAILURE;
                }
            }

            if (RBTreeGet
                (gOamGlobals.OamGblNodeDualSearchTree,
                 (tRBElem *) pOamFsMplsTpNodeMapEntry) == NULL)
            {
                if (RBTreeAdd
                    (gOamGlobals.OamGblNodeDualSearchTree,
                     (tRBElem *) pOamFsMplsTpNodeMapEntry) != RB_SUCCESS)
                {
                    OAM_TRC ((OAM_MAIN_TRC,
                              "\r%% RBTree add failed for the Global \
                              Node Id Table.\r\n"));
                    return OSIX_FAILURE;
                }
            }
            else
            {
                OAM_TRC ((OAM_MAIN_TRC,
                          "\r%% Global Id and Node Id already exists.\r\n"));
                return OSIX_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
        case NOT_READY:
            break;

        case DESTROY:
            RBTreeRem (gOamGlobals.OamGblNodeDualSearchTree,
                       (tRBElem *) pOamFsMplsTpNodeMapEntry);
            break;

        default:
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OamUtlMegTableRowStatusHdl
 * Input       :  pOamOldFsMplsTpMegEntry,
 *                pOamFsMplsTpMegEntry
 * Output      :  This routine handles the rowstatus change,
 *                does necessary validation and notification
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamUtlMegTableRowStatusHdl (tOamFsMplsTpMegEntry * pOamOldFsMplsTpMegEntry,
                            tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry)
{

    switch (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus)
    {
        case ACTIVE:
            /* If the operator type is configured as ICC based,
             * check if ICC and UMC objects are configured.
             * If not return failure. These objects are
             * necessary to form ICC based MEG Identifier. */
            if (pOamFsMplsTpMegEntry->MibObject.
                i4FsMplsTpMegOperatorType == OAM_OPERATOR_TYPE_ICC)
            {
                if ((pOamFsMplsTpMegEntry->MibObject.
                     i4FsMplsTpMegIdIccLen == 0) ||
                    (pOamFsMplsTpMegEntry->MibObject.
                     i4FsMplsTpMegIdUmcLen == 0))
                {
                    OAM_TRC ((OAM_MAIN_TRC,
                              "\r%% ICC or UMC Id is not valid.\r\n"));
                    return OSIX_FAILURE;
                }
                if (RBTreeGet (gOamGlobals.FsMplsTpMegIccIdTable,
                               (tRBElem *) pOamFsMplsTpMegEntry) != NULL)
                {
                    /* We already have an entry with the same MEG ICC-ID. User
                     * is expected to modify the ICC-Id with an unique value 
                     * and then make the row status as active. */
                    OAM_TRC ((OAM_MAIN_TRC,
                              "\r%% Meg ICC-Id conflict. user has to modify ICC-Id \
                              before making the row as active.\r\n"));
                    return OSIX_FAILURE;
                }

                if (RBTreeAdd
                    (gOamGlobals.FsMplsTpMegIccIdTable,
                     (tRBElem *) pOamFsMplsTpMegEntry) != RB_SUCCESS)
                {
                    OAM_TRC ((OAM_MAIN_TRC,
                              "\r%% RBTree Add failed for Meg ICC-ID table.\r\n"));
                    return OSIX_FAILURE;
                }
            }

            /* Check if MEG name object is configured.
             * If not generate a Meg name. This object will
             * be used to access the MEG information from CLI
             * and from other modules. */
            if (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen == 0)
            {
                OamUtilGenMegName (pOamFsMplsTpMegEntry,
                                   pOamFsMplsTpMegEntry->MibObject.
                                   au1FsMplsTpMegName);

                if (RBTreeGet (gOamGlobals.FsMplsTpMegNameTable,
                               (tRBElem *) pOamFsMplsTpMegEntry) != NULL)
                {
                    /* We already have an entry with the same MEGNAME. User
                     * is expected to modify the megname with an unique megname
                     * by doing an explicit SET on megname. And then make the 
                     * row status as active. */
                    OAM_TRC ((OAM_MAIN_TRC,
                              "\r%% Meg name conflict. user has to modify megname \
                           before making the row as active.\r\n"));
                    return OSIX_FAILURE;
                }

                if (RBTreeAdd
                    (gOamGlobals.FsMplsTpMegNameTable,
                     (tRBElem *) pOamFsMplsTpMegEntry) != RB_SUCCESS)
                {
                    OAM_TRC ((OAM_MAIN_TRC,
                              "\r%% RBTree Add failed for Meg Name table.\r\n"));
                    return OSIX_FAILURE;
                }

                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen =
                    STRLEN (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName);
            }
            /* Set the MEG index into the index manager, since we don't get 
             * NOT_READY when we create the MEG entry from CLI */
            if (pOamOldFsMplsTpMegEntry == NULL)
            {
                if (MplsOamSetMegId
                    (pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex) !=
                    pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex)
                {
                    OAM_TRC ((OAM_MAIN_TRC,
                              "\r%% Index manager updation for Meg Index failed.\r\n"));
                    return OSIX_FAILURE;
                }
            }
            /* If MEG rowstatus was in NOT_IN_SERVICE
             * state previously, Check if  
             * associated ME entry exists. If exists
             * clear the MEG down bit in ME Oper
             * status object. */
            if (pOamOldFsMplsTpMegEntry != NULL)
            {
                if ((pOamOldFsMplsTpMegEntry->MibObject.
                     i4FsMplsTpMegRowStatus == NOT_IN_SERVICE) ||
                    (pOamOldFsMplsTpMegEntry->MibObject.
                     i4FsMplsTpMegRowStatus == NOT_READY))
                {
                    if (OamNotifyAndUpdateMegOperStatus (pOamFsMplsTpMegEntry,
                                                         MPLS_MEG_UP) ==
                        OSIX_FAILURE)
                    {
                        OAM_TRC ((OAM_MAIN_TRC,
                                  "\r%% Meg oper status updation failed.\r\n"));
                        return OSIX_FAILURE;
                    }
                }
            }

            break;

        case NOT_IN_SERVICE:

            /* If MEG rowstatus was in ACTIVE state 
             * previously, check if associated ME
             * entry exists. If exists set the 
             * MEG down bit in ME Oper status object. */
            if (pOamOldFsMplsTpMegEntry != NULL)
            {
                if (pOamOldFsMplsTpMegEntry->MibObject.
                    i4FsMplsTpMegRowStatus == ACTIVE)
                {
                    if (OamNotifyAndUpdateMegOperStatus (pOamFsMplsTpMegEntry,
                                                         MPLS_MEG_DOWN)
                        == OSIX_FAILURE)
                    {
                        OAM_TRC ((OAM_MAIN_TRC,
                                  "\r%% Meg oper status updation failed.\r\n"));
                        return OSIX_FAILURE;
                    }
                }
                if (pOamFsMplsTpMegEntry->MibObject.
                    i4FsMplsTpMegOperatorType == OAM_OPERATOR_TYPE_ICC)
                {
                    RBTreeRem (gOamGlobals.FsMplsTpMegIccIdTable,
                               (tRBElem *) pOamFsMplsTpMegEntry);
                }
            }
            break;

        case NOT_READY:
            /* When this function is called from CLI thread, this condition
             * not happen. Only when the row is created using SNMP, the row
             * status field will be set to NOT_READY before calling this 
             * function from SetAll. When it is SET from SNMP thread,
             * the MegIndex value should be reserved in the Index manager.*/
            /* Index manager will return the same value that is being set
             * if it is successful. */
            if (MplsOamSetMegId
                (pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex) !=
                pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex)
            {
                OAM_TRC ((OAM_MAIN_TRC,
                          "\r%% Index manager updation for Meg Index failed.\r\n"));
                return OSIX_FAILURE;
            }
            break;

        case DESTROY:

            if (pOamOldFsMplsTpMegEntry != NULL)
            {
                /* MegIndex is released back to index manager for both SNMP 
                 * based deletion and for CLI based deletion. */
                MplsOamRelMegId (pOamFsMplsTpMegEntry->MibObject.
                                 u4FsMplsTpMegIndex);
                RBTreeRem (gOamGlobals.FsMplsTpMegNameTable,
                           (tRBElem *) pOamFsMplsTpMegEntry);

                if (pOamFsMplsTpMegEntry->MibObject.
                    i4FsMplsTpMegOperatorType == OAM_OPERATOR_TYPE_ICC)
                {
                    RBTreeRem (gOamGlobals.FsMplsTpMegIccIdTable,
                               (tRBElem *) pOamFsMplsTpMegEntry);
                }
            }
            break;

        default:
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  OamUtlMeTableRowStatusHdl
 * Input       :  pOamOldFsMplsTpMeEntry,
 *                pOamFsMplsTpMeEntry
 * Output      :  This routine handles the rowstatus change,
 *                does necessary validation and notification
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamUtlMeTableRowStatusHdl (tOamFsMplsTpMeEntry * pOamOldFsMplsTpMeEntry,
                           tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{

    switch (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus)
    {
        case ACTIVE:
            /* Check if MEG entry exists for this ME entry, 
             * and rowstatus of MEG is active. If not return failure.*/
            if (OamMeValidateRsActiveConditions
                (pOamOldFsMplsTpMeEntry, pOamFsMplsTpMeEntry) == OSIX_FAILURE)
            {
                OAM_TRC ((OAM_MAIN_TRC, "\r%% Me cannot be activated since \
                          preconditions failed.\r\n"));
                return OSIX_FAILURE;
            }

            /* ME Row is becoming active. Hence, clear the ME DOWN bit */
            pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
                &= (~OAM_ME_OPER_STATUS_ME_DOWN);

            /* Whenever ME becomes active, we set the Path Status as DOWN for 
             * now. For PW, we post a message and expect L2vpn to come back 
             * to update the PW status. For LSP, we make LSP function call 
             * MplsOamIfCheckAndUpdateTnlForMeg to fetch the LSP Path Status 
             * in the code below. In principle, we are fetching the path status
             * afresh*/
            pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
                |= OAM_ME_OPER_STATUS_PATH_DOWN;

            /* update tunnel or PW about the MEG indices and send
             * notification to the OAM applications */
            if (OamHandleMeRowStatusActive (pOamFsMplsTpMeEntry)
                == OSIX_FAILURE)
            {
                /* ME Row becoming active has failed. Hence, 
                 * set the ME DOWN bit */
                pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
                    |= OAM_ME_OPER_STATUS_ME_DOWN;
                OAM_TRC ((OAM_MAIN_TRC,
                          "\r%% Meg status notification failed.\r\n"));
                return OSIX_FAILURE;
            }
            break;

        case NOT_IN_SERVICE:
        case NOT_READY:

            pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
                |= OAM_ME_OPER_STATUS_ME_DOWN;

            if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus
                == NOT_READY)
            {
                pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
                    |= OAM_ME_OPER_STATUS_PATH_DOWN;
            }
            /* Call API to send MEG DOWN
             * notification to OAM applications. */
            if (pOamOldFsMplsTpMeEntry != NULL)
            {
                if (pOamOldFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
                    == OAM_ME_OPER_STATUS_UP)
                {
                    /* Reset MEG Indices in Tunnel or PW table
                     * if valid service pointer exists. Also
                     * send MEG status to OAM applications. */

                    if (MplsOamIfNotifyMegStatusToApp (MPLS_MODULE,
                                                       pOamFsMplsTpMeEntry->
                                                       u4FsMplsTpMeApps,
                                                       pOamFsMplsTpMeEntry,
                                                       MPLS_MEG_DOWN) ==
                        OSIX_FAILURE)
                    {
                        OAM_TRC ((OAM_MAIN_TRC,
                                  "\r%% Meg status notification to OAM \
                                  applications failed.\r\n"));
                    }
                }
            }
            break;

        case DESTROY:

            /* There is no need to send oper status to applications like ELPS
             * and BFD since we are not allowing the deletion when such
             * applications have registered. */

            /* Call OamResetMegIndicesInLspOrPw to reset the megindices in 
             * PW/LSP entry */
            if (OamResetMegIndicesInLspOrPw (pOamFsMplsTpMeEntry)
                == OSIX_FAILURE)
            {
                OAM_TRC ((OAM_MAIN_TRC,
                          "\r%% Meg status notification failed.\r\n"));
                return OSIX_FAILURE;
            }

            break;

        default:
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PUBLIC INT4
OamFsMplsTpGlobalIdNodeIdTableCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOamFsMplsTpNodeMapEntry,
                       FsMplsTpGlobalIdNodeIdTableNode);

    if ((gOamGlobals.OamGblNodeDualSearchTree =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMplsTpGlobalIdNodeIdTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PUBLIC INT4
OamFsMplsTpFsMplsTpMegNameTableCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOamFsMplsTpMegEntry, FsMplsTpMegNameTableNode);

    if ((gOamGlobals.FsMplsTpMegNameTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMplsTpFsMplsTpMegNameTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PUBLIC INT4
OamFsMplsTpFsMplsTpMegIccIdTableCreate (VOID)
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOamFsMplsTpMegEntry, FsMplsTpMegIccIdTableNode);

    if ((gOamGlobals.FsMplsTpMegIccIdTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMplsTpFsMplsTpMegIccIdTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PUBLIC INT4
FsMplsTpGlobalIdNodeIdTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tOamFsMplsTpNodeMapEntry *pFsMplsTpNodeMapEntry1 =
        (tOamFsMplsTpNodeMapEntry *) pRBElem1;
    tOamFsMplsTpNodeMapEntry *pFsMplsTpNodeMapEntry2 =
        (tOamFsMplsTpNodeMapEntry *) pRBElem2;

    if (pFsMplsTpNodeMapEntry1->MibObject.u4FsMplsTpContextId >
        pFsMplsTpNodeMapEntry2->MibObject.u4FsMplsTpContextId)
    {
        return 1;
    }
    else if (pFsMplsTpNodeMapEntry1->MibObject.u4FsMplsTpContextId <
             pFsMplsTpNodeMapEntry2->MibObject.u4FsMplsTpContextId)
    {
        return -1;
    }

    if (pFsMplsTpNodeMapEntry1->MibObject.u4FsMplsTpNodeMapGlobalId >
        pFsMplsTpNodeMapEntry2->MibObject.u4FsMplsTpNodeMapGlobalId)
    {
        return 1;
    }
    else if (pFsMplsTpNodeMapEntry1->MibObject.u4FsMplsTpNodeMapGlobalId <
             pFsMplsTpNodeMapEntry2->MibObject.u4FsMplsTpNodeMapGlobalId)
    {
        return -1;
    }

    if (pFsMplsTpNodeMapEntry1->MibObject.u4FsMplsTpNodeMapNodeId >
        pFsMplsTpNodeMapEntry2->MibObject.u4FsMplsTpNodeMapNodeId)
    {
        return 1;
    }
    else if (pFsMplsTpNodeMapEntry1->MibObject.u4FsMplsTpNodeMapNodeId <
             pFsMplsTpNodeMapEntry2->MibObject.u4FsMplsTpNodeMapNodeId)
    {
        return -1;
    }
    return 0;
}

PUBLIC INT4
FsMplsTpFsMplsTpMegNameTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tOamFsMplsTpMegEntry *pFsMplsTpMegEntry1 =
        (tOamFsMplsTpMegEntry *) pRBElem1;
    tOamFsMplsTpMegEntry *pFsMplsTpMegEntry2 =
        (tOamFsMplsTpMegEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMplsTpMegEntry1->MibObject.u4FsMplsTpContextId >
        pFsMplsTpMegEntry2->MibObject.u4FsMplsTpContextId)
    {
        return 1;
    }
    else if (pFsMplsTpMegEntry1->MibObject.u4FsMplsTpContextId <
             pFsMplsTpMegEntry2->MibObject.u4FsMplsTpContextId)
    {
        return -1;
    }

    if ((i4MaxLength = MEMCMP (pFsMplsTpMegEntry1->MibObject.au1FsMplsTpMegName,
                               pFsMplsTpMegEntry2->MibObject.au1FsMplsTpMegName,
                               OAM_MEG_NAME_MAX_LEN)) != 0)
    {
        return i4MaxLength;
    }
    return 0;
}

PUBLIC INT4
FsMplsTpFsMplsTpMegIccIdTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tOamFsMplsTpMegEntry *pFsMplsTpMegEntry1 =
        (tOamFsMplsTpMegEntry *) pRBElem1;
    tOamFsMplsTpMegEntry *pFsMplsTpMegEntry2 =
        (tOamFsMplsTpMegEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if ((i4MaxLength =
         MEMCMP (pFsMplsTpMegEntry1->MibObject.au1FsMplsTpMegIdIcc,
                 pFsMplsTpMegEntry2->MibObject.au1FsMplsTpMegIdIcc,
                 OAM_ICC_ID_LEN)) != 0)
    {
        return i4MaxLength;
    }

    if ((i4MaxLength =
         MEMCMP (pFsMplsTpMegEntry1->MibObject.au1FsMplsTpMegIdUmc,
                 pFsMplsTpMegEntry2->MibObject.au1FsMplsTpMegIdUmc,
                 OAM_UMC_ID_LEN)) != 0)
    {
        return i4MaxLength;
    }

    return 0;
}

INT4
OamUtlConvertStringtoOid (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry,
                          UINT4 *pau4FsMplsTpMeServicePointer,
                          INT4 *pi4FsMplsTpMeServicePointerLen)
{
    MEMCPY (pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer,
            pau4FsMplsTpMeServicePointer,
            (sizeof (UINT4) * (*pi4FsMplsTpMeServicePointerLen)));
    pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen =
        *pi4FsMplsTpMeServicePointerLen;
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamMeValidateRsActiveConditions
* Description : This function checks the Service type from the corresponding
*               MEG entry and updates the MEG indices in the service table,
*               if MEG status is UP and resets the MEG indices if MEG is down.
* Input       : MEG status -UP or DOWN, ME entry 
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamMeValidateRsActiveConditions (tOamFsMplsTpMeEntry * pOamOldFsMplsTpMeEntry,
                                 tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{
    tMplsMegId          MplsMegId;
    tMplsTnlLspId       MplsTnlLspId;
    tMplsOamPathStatus  MplsOamPathStatus;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;

    MEMSET (&MplsMegId, 0, sizeof (tMplsMegId));
    MEMSET (&MplsTnlLspId, 0, sizeof (tMplsTnlLspId));
    MEMSET (&MplsOamPathStatus, 0, sizeof (tMplsOamPathStatus));

    OAM_TRC_FUNC ((OAM_FN_ENTRY, "FUNC:OamNotifySendMegStatus\n"));

    UNUSED_PARAM (pOamOldFsMplsTpMeEntry);
    /* Fetch the associated Meg entry */
    pOamFsMplsTpMegEntry = OamUtilGetAssociatedMegEntry (pOamFsMplsTpMeEntry);

    if ((pOamFsMplsTpMegEntry == NULL) ||
        (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus != ACTIVE))
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Associated MEG entry does not exist \
                  or the MEG entry is not Active.\r\n"));
        return OSIX_FAILURE;
    }
    if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen == 0)
    {
        /* ME service pointer is not configured */
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Service pointer is not assigned.\r\n"));
        return OSIX_FAILURE;
    }

    /* Check the operator type in MEG entry. If
     * ICC based, check source & sink MEP index 
     * is non-zero. If zero return failure. */
    if ((pOamFsMplsTpMegEntry->MibObject.
         i4FsMplsTpMegOperatorType == OAM_OPERATOR_TYPE_ICC) &&
        ((pOamFsMplsTpMeEntry->MibObject.
          u4FsMplsTpMeSourceMepIndex == 0) ||
         (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex == 0)))
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Source and Sink Mep Index should be non \
                  zero for ICC based MEG configuration.\r\n"));
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
