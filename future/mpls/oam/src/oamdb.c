/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamdb.c,v 1.8 2013/06/26 11:49:02 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Oam 
*********************************************************************/

#include "oaminc.h"
#include "mplscli.h"
#include "inmgrex.h"
#include "indexmgr.h"

/****************************************************************************
 Function    :  OamTestAllFsMplsTpGlobalConfigTable
 Input       :  The Indices
                pu4ErrorCode
                pOamFsMplsTpGlobalConfigEntry
                pOamIsSetFsMplsTpGlobalConfigEntry
 Output      :  This Routine Take the Indices &
                Test the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamTestAllFsMplsTpGlobalConfigTable (UINT4 *pu4ErrorCode,
                                     tOamFsMplsTpGlobalConfigEntry *
                                     pOamSetFsMplsTpGlobalConfigEntry,
                                     tOamIsSetFsMplsTpGlobalConfigEntry *
                                     pOamIsSetFsMplsTpGlobalConfigEntry)
{

    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;
    INT4                i4Counter = 0;

    /* Check whether the global config entry exists.
     * If not return failure. */
    pOamFsMplsTpGlobalConfigEntry = OamGetFsMplsTpGlobalConfigTable
        (pOamSetFsMplsTpGlobalConfigEntry);

    if (pOamFsMplsTpGlobalConfigEntry == NULL)
    {
        CLI_SET_ERR (CLI_OAM_ENTRY_NOT_FOUND);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpOamModuleStatus !=
        OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpGlobalConfigEntry->MibObject.
             i4FsMplsTpOamModuleStatus != OAM_MODULE_STATUS_ENABLED) &&
            (pOamSetFsMplsTpGlobalConfigEntry->MibObject.
             i4FsMplsTpOamModuleStatus != OAM_MODULE_STATUS_DISABLED))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpGlobalId != OSIX_FALSE)
    {
    }

    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpIcc != OSIX_FALSE)
    {
        /* ICC ID is a string of 1 to 6 characters,
         * each character being uppercase alphabet (A-Z)
         * or numeric (0-9).*/
        if (pOamSetFsMplsTpGlobalConfigEntry->MibObject.
            i4FsMplsTpIccLen > OAM_ICC_ID_LEN)
        {
            CLI_SET_ERR (CLI_OAM_INVALID_ICC_ID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /* Validating each character using their ASCII values */
        while (i4Counter < pOamSetFsMplsTpGlobalConfigEntry->
               MibObject.i4FsMplsTpIccLen)
        {
            if ((pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                 au1FsMplsTpIcc[i4Counter] > OAM_Z_ASCII_VALUE) ||
                ((pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                  au1FsMplsTpIcc[i4Counter] > OAM_9_ASCII_VALUE) &&
                 (pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                  au1FsMplsTpIcc[i4Counter] < OAM_A_ASCII_VALUE)))
            {
                CLI_SET_ERR (CLI_OAM_INVALID_ICC_ID);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            i4Counter++;
        }
    }

    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNodeIdentifier !=
        OSIX_FALSE)
    {
    }

    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpTraceLevel != OSIX_FALSE)
    {
        if (pOamSetFsMplsTpGlobalConfigEntry->MibObject.
            u4FsMplsTpTraceLevel > (OAM_ALL_TRC))
        {
            CLI_SET_ERR (CLI_OAM_INVALID_TRACE_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNotificationEnable !=
        OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpGlobalConfigEntry->MibObject.
             i4FsMplsTpNotificationEnable != MPLS_SNMP_TRUE) &&
            (pOamSetFsMplsTpGlobalConfigEntry->MibObject.
             i4FsMplsTpNotificationEnable != MPLS_SNMP_FALSE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OamTestAllFsMplsTpNodeMapTable
 Input       :  The Indices
                pu4ErrorCode
                pOamFsMplsTpNodeMapEntry
                pOamIsSetFsMplsTpNodeMapEntry
 Output      :  This Routine Take the Indices &
                Test the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamTestAllFsMplsTpNodeMapTable (UINT4 *pu4ErrorCode,
                                tOamFsMplsTpNodeMapEntry *
                                pOamSetFsMplsTpNodeMapEntry,
                                tOamIsSetFsMplsTpNodeMapEntry *
                                pOamIsSetFsMplsTpNodeMapEntry,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;
    INT4                i4FsMplsTpNodeMapRowStatus = 0;

    MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));

    if (pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapLocalNum == 0)
    {
        CLI_SET_ERR (CLI_OAM_INVALID_LOCAL_MAP_NUM);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return OSIX_FAILURE;
    }

    pOamFsMplsTpNodeMapEntry = OamGetFsMplsTpNodeMapTable
        (pOamSetFsMplsTpNodeMapEntry);

    if (pOamFsMplsTpNodeMapEntry == NULL)
    {
        if (pOamSetFsMplsTpNodeMapEntry->MibObject.
            i4FsMplsTpNodeMapRowStatus == DESTROY)
        {
            CLI_SET_ERR (CLI_OAM_ENTRY_NOT_FOUND);
            return OSIX_FAILURE;
        }

        OamFsMplsTpGlobalConfigEntry.MibObject.u4FsMplsTpContextId =
            pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId;

        pOamFsMplsTpGlobalConfigEntry = OamGetFsMplsTpGlobalConfigTable
            (&OamFsMplsTpGlobalConfigEntry);

        if (pOamFsMplsTpGlobalConfigEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if ((i4RowCreateOption == OSIX_FALSE) &&
            (pOamSetFsMplsTpNodeMapEntry->MibObject.
             i4FsMplsTpNodeMapRowStatus != CREATE_AND_WAIT) &&
            (pOamSetFsMplsTpNodeMapEntry->MibObject.
             i4FsMplsTpNodeMapRowStatus != CREATE_AND_GO))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (i4RowCreateOption == OSIX_TRUE)
        {
            if (RBTreeGet
                (gOamGlobals.OamGblNodeDualSearchTree,
                 (tRBElem *) pOamSetFsMplsTpNodeMapEntry) != NULL)
            {
                CLI_SET_ERR (CLI_OAM_GLOBAL_NODE_ID_ENTRY_EXISTS);
                return OSIX_FAILURE;
            }
        }
    }
    else
    {
        if ((i4RowCreateOption == OSIX_FALSE) &&
            ((pOamSetFsMplsTpNodeMapEntry->MibObject.
              i4FsMplsTpNodeMapRowStatus == CREATE_AND_WAIT) ||
             (pOamSetFsMplsTpNodeMapEntry->MibObject.
              i4FsMplsTpNodeMapRowStatus == CREATE_AND_GO)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if ((i4RowStatusLogic == OSIX_FALSE) &&
            (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus
             == OSIX_FALSE) && (pOamFsMplsTpNodeMapEntry->MibObject.
                                i4FsMplsTpNodeMapRowStatus == ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (i4RowCreateOption == OSIX_TRUE)
        {
            if (pOamSetFsMplsTpNodeMapEntry->MibObject.
                i4FsMplsTpNodeMapRowStatus == DESTROY)
            {
                if ((pOamSetFsMplsTpNodeMapEntry->MibObject.
                     u4FsMplsTpNodeMapGlobalId !=
                     pOamFsMplsTpNodeMapEntry->MibObject.
                     u4FsMplsTpNodeMapGlobalId) ||
                    (pOamSetFsMplsTpNodeMapEntry->MibObject.
                     u4FsMplsTpNodeMapNodeId !=
                     pOamFsMplsTpNodeMapEntry->MibObject.
                     u4FsMplsTpNodeMapNodeId))
                {
                    CLI_SET_ERR (CLI_OAM_ENTRY_NOT_FOUND);
                    return OSIX_FAILURE;
                }
            }
            else
            {
                CLI_SET_ERR (CLI_OAM_LOCAL_MAP_NUM_ENTRY_EXISTS);
                return OSIX_FAILURE;
            }
        }

        i4FsMplsTpNodeMapRowStatus =
            pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus;
    }

    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapGlobalId != OSIX_FALSE)
    {
        if (pOamSetFsMplsTpNodeMapEntry->MibObject.
            u4FsMplsTpNodeMapGlobalId == 0)
        {
            CLI_SET_ERR (CLI_OAM_INVALID_GLOBAL_ID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapNodeId != OSIX_FALSE)
    {
        if (pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId == 0)
        {
            CLI_SET_ERR (CLI_OAM_INVALID_NODE_ID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus != OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpNodeMapEntry->MibObject.
             i4FsMplsTpNodeMapRowStatus < ACTIVE) ||
            (pOamSetFsMplsTpNodeMapEntry->MibObject.
             i4FsMplsTpNodeMapRowStatus > DESTROY) ||
            (pOamSetFsMplsTpNodeMapEntry->MibObject.
             i4FsMplsTpNodeMapRowStatus == CREATE_AND_GO) ||
            (pOamSetFsMplsTpNodeMapEntry->MibObject.
             i4FsMplsTpNodeMapRowStatus == NOT_READY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        if ((pOamSetFsMplsTpNodeMapEntry->MibObject.
             i4FsMplsTpNodeMapRowStatus == NOT_IN_SERVICE) &&
            (i4FsMplsTpNodeMapRowStatus == ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if ((i4RowCreateOption == OSIX_FALSE) &&
            (pOamSetFsMplsTpNodeMapEntry->MibObject.
             i4FsMplsTpNodeMapRowStatus == ACTIVE) &&
            (pOamFsMplsTpNodeMapEntry != NULL))
        {
            if (RBTreeGet
                (gOamGlobals.OamGblNodeDualSearchTree,
                 (tRBElem *) pOamFsMplsTpNodeMapEntry) != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OamTestAllFsMplsTpMegTable
 Input       :  The Indices
                pu4ErrorCode
                pOamFsMplsTpMegEntry
                pOamIsSetFsMplsTpMegEntry
 Output      :  This Routine Take the Indices &
                Test the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamTestAllFsMplsTpMegTable (UINT4 *pu4ErrorCode,
                            tOamFsMplsTpMegEntry * pOamSetFsMplsTpMegEntry,
                            tOamIsSetFsMplsTpMegEntry *
                            pOamIsSetFsMplsTpMegEntry, INT4 i4RowStatusLogic,
                            INT4 i4RowCreateOption)
{
    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;
    UINT4               u4MegIndex = 0;
    INT4                i4Counter = 0;

    MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));

    pOamFsMplsTpMegEntry = OamGetFsMplsTpMegTable (pOamSetFsMplsTpMegEntry);

    OamFsMplsTpGlobalConfigEntry.MibObject.u4FsMplsTpContextId =
        pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;

    pOamFsMplsTpGlobalConfigEntry = OamGetFsMplsTpGlobalConfigTable
        (&OamFsMplsTpGlobalConfigEntry);

    if (pOamFsMplsTpMegEntry == NULL)
    {
        if (pOamFsMplsTpGlobalConfigEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if ((i4RowCreateOption == OSIX_FALSE) &&
            (pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegRowStatus != CREATE_AND_WAIT) &&
            (pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegRowStatus != CREATE_AND_GO))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        u4MegIndex = MplsOamCheckMegId (pOamSetFsMplsTpMegEntry->MibObject.
                                        u4FsMplsTpMegIndex);
        if (u4MegIndex != pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    else
    {
        if ((i4RowCreateOption == OSIX_FALSE) &&
            ((pOamSetFsMplsTpMegEntry->MibObject.
              i4FsMplsTpMegRowStatus == CREATE_AND_WAIT) ||
             (pOamSetFsMplsTpMegEntry->MibObject.
              i4FsMplsTpMegRowStatus == CREATE_AND_GO)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if ((i4RowStatusLogic == OSIX_FALSE) &&
            (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus
             == OSIX_FALSE) && (pOamFsMplsTpMegEntry->MibObject.
                                i4FsMplsTpMegRowStatus == ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if (i4RowStatusLogic == OSIX_TRUE)
        {
            if (OamUtilCheckIfActiveMeExists (pOamSetFsMplsTpMegEntry)
                == OSIX_SUCCESS)
            {
                if (pOamFsMplsTpGlobalConfigEntry != NULL)
                {
                    pOamFsMplsTpGlobalConfigEntry->MibObject.
                        i4FsMplsTpErrorCode = OAM_ACTIVE_ME_EXIST;
                }
                CLI_SET_ERR (CLI_OAM_ACTIVE_ME_EXISTS);
                return OSIX_FAILURE;
            }
        }
    }

    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegName != OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegNameLen < 1) ||
            (pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegNameLen > OAM_MEG_NAME_MAX_LEN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegOperatorType != OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegOperatorType != OAM_OPERATOR_TYPE_IP) &&
            (pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegOperatorType != OAM_OPERATOR_TYPE_ICC))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdIcc != OSIX_FALSE)
    {
        if (pOamSetFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegIdIccLen > OAM_ICC_ID_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /* Validating each character using their ASCII values */
        while (i4Counter < pOamSetFsMplsTpMegEntry->MibObject.
               i4FsMplsTpMegIdIccLen)
        {
            if ((pOamSetFsMplsTpMegEntry->MibObject.
                 au1FsMplsTpMegIdIcc[i4Counter] > OAM_Z_ASCII_VALUE) ||
                ((pOamSetFsMplsTpMegEntry->MibObject.
                  au1FsMplsTpMegIdIcc[i4Counter] > OAM_9_ASCII_VALUE) &&
                 (pOamSetFsMplsTpMegEntry->MibObject.
                  au1FsMplsTpMegIdIcc[i4Counter] < OAM_A_ASCII_VALUE)))
            {
                CLI_SET_ERR (CLI_OAM_INVALID_ICC_ID);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            i4Counter++;
        }
    }

    i4Counter = 0;

    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdUmc != OSIX_FALSE)
    {
        if (pOamSetFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegIdUmcLen > OAM_UMC_ID_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        /* Validating each character using their ASCII values */
        while (i4Counter < pOamSetFsMplsTpMegEntry->MibObject.
               i4FsMplsTpMegIdUmcLen)
        {
            if ((pOamSetFsMplsTpMegEntry->MibObject.
                 au1FsMplsTpMegIdUmc[i4Counter] > OAM_Z_ASCII_VALUE) ||
                ((pOamSetFsMplsTpMegEntry->MibObject.
                  au1FsMplsTpMegIdUmc[i4Counter] > OAM_9_ASCII_VALUE) &&
                 (pOamSetFsMplsTpMegEntry->MibObject.
                  au1FsMplsTpMegIdUmc[i4Counter] < OAM_A_ASCII_VALUE)))
            {
                CLI_SET_ERR (CLI_OAM_INVALID_UMC_ID);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return OSIX_FAILURE;
            }

            i4Counter++;
        }
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegServiceType != OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegServiceType != OAM_SERVICE_TYPE_LSP) &&
            (pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegServiceType != OAM_SERVICE_TYPE_PW))
        {
            CLI_SET_ERR (CLI_OAM_INVALID_SERVICE_TYPE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegMpLocation != OSIX_FALSE)
    {
        if (pOamSetFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegMpLocation != OAM_MEG_MP_LOCATION_PER_NODE)
        {
            CLI_SET_ERR (CLI_OAM_MP_LOCATION_NOT_SUPPORTED);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus != OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegRowStatus < ACTIVE) ||
            (pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegRowStatus > DESTROY) ||
            (pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegRowStatus == CREATE_AND_GO) ||
            (pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegRowStatus == NOT_READY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        if (pOamSetFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegRowStatus == DESTROY)
        {
            /* Check if an associated ME entry exists.
             * If exists, return failure. */
            if (OamUtilIsMeEntryExist (pOamSetFsMplsTpMegEntry) == OSIX_SUCCESS)
            {
                CLI_SET_ERR (CLI_OAM_DEL_MEG_FAILED_ME_EXISTS);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }

        if ((i4RowStatusLogic == OSIX_FALSE) &&
            (pOamSetFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegRowStatus == NOT_IN_SERVICE))
        {
            if (OamUtilCheckIfActiveMeExists (pOamSetFsMplsTpMegEntry)
                == OSIX_SUCCESS)
            {
                if (pOamFsMplsTpGlobalConfigEntry != NULL)
                {
                    pOamFsMplsTpGlobalConfigEntry->MibObject.
                        i4FsMplsTpErrorCode = OAM_ACTIVE_ME_EXIST;
                }
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  OamTestAllFsMplsTpMeTable
 Input       :  The Indices
                pu4ErrorCode
                pOamFsMplsTpMeEntry
                pOamIsSetFsMplsTpMeEntry
 Output      :  This Routine Take the Indices &
                Test the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
OamTestAllFsMplsTpMeTable (UINT4 *pu4ErrorCode,
                           tOamFsMplsTpMeEntry * pOamSetFsMplsTpMeEntry,
                           tOamIsSetFsMplsTpMeEntry * pOamIsSetFsMplsTpMeEntry,
                           INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{

    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;

    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4TnlIndex = 0;
    UINT4               u4TnlInst = 0;
    UINT4               u4TnlIngressId = 0;
    UINT4               u4TnlEgressId = 0;
    UINT4               u4FsMplsTpMeApps = 0;
    UINT4               u4FsMplsTpContextId = 0;
    UINT4               u4FsMplsTpMeSourceMepIndex = 0;
    UINT4               u4FsMplsTpMeSinkMepIndex = 0;
    INT4                i4OidLen = 0;
    INT4                i4OidTempLen = 0;
    INT4                i4TnlOidLen = 0;
    INT4                i4IsServiceSignaled = OAM_TRUE;
    INT4                i4ErrorCode = 0;

    MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));

    pOamFsMplsTpMeEntry = OamGetFsMplsTpMeTable (pOamSetFsMplsTpMeEntry);

    pOamFsMplsTpMegEntry = OamUtilGetAssociatedMegEntry
        (pOamSetFsMplsTpMeEntry);

    if (pOamFsMplsTpMeEntry == NULL)
    {
        if (pOamFsMplsTpMegEntry == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
        if ((i4RowCreateOption == OSIX_FALSE) &&
            (pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeRowStatus != CREATE_AND_WAIT) &&
            (pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeRowStatus != CREATE_AND_GO))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }
    }
    else
    {
        if ((i4RowCreateOption == OSIX_FALSE) &&
            ((pOamSetFsMplsTpMeEntry->MibObject.
              i4FsMplsTpMeRowStatus == CREATE_AND_WAIT) ||
             (pOamSetFsMplsTpMeEntry->MibObject.
              i4FsMplsTpMeRowStatus == CREATE_AND_GO)))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        if ((i4RowStatusLogic == OSIX_FALSE) &&
            (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus
             == OSIX_FALSE) && (pOamFsMplsTpMeEntry->MibObject.
                                i4FsMplsTpMeRowStatus == ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return OSIX_FAILURE;
        }

        u4FsMplsTpMeApps = pOamFsMplsTpMeEntry->u4FsMplsTpMeApps;
        u4FsMplsTpContextId =
            pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
        i4OidLen = pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;
        i4OidTempLen =
            pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;

        if (i4OidLen >= TUNNEL_TABLE_INDICES)
        {
            i4TnlOidLen = i4OidLen - 1;
            u4TnlEgressId = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[i4TnlOidLen];
            i4TnlOidLen = i4TnlOidLen - 1;
            u4TnlIngressId = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[i4TnlOidLen];
            i4TnlOidLen = i4TnlOidLen - 1;
            u4TnlInst = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[i4TnlOidLen];
            i4TnlOidLen = i4TnlOidLen - 1;
            u4TnlIndex = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[i4TnlOidLen];
        }

        i4IsServiceSignaled = pOamFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeServiceSignaled;
        u4FsMplsTpMeSourceMepIndex =
            pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex;
        u4FsMplsTpMeSinkMepIndex =
            pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex;
    }

    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeName != OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeNameLen < 1) ||
            (pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeNameLen > OAM_ME_NAME_MAX_LEN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIfIndex != OSIX_FALSE)
    {
        /* No validation required, since If index is not
         * currently supported. */
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSourceMepIndex != OSIX_FALSE)
    {
        if (pOamSetFsMplsTpMeEntry->MibObject.
            u4FsMplsTpMeSourceMepIndex > OAM_MAX_MEP_INDEX_VALUE)
        {
            CLI_SET_ERR (CLI_OAM_INVALID_MEP_INDEX);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSinkMepIndex != OSIX_FALSE)
    {
        if (pOamSetFsMplsTpMeEntry->MibObject.
            u4FsMplsTpMeSinkMepIndex > OAM_MAX_MEP_INDEX_VALUE)
        {
            CLI_SET_ERR (CLI_OAM_INVALID_MEP_INDEX);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpType != OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeMpType != OAM_ME_MP_TYPE_MEP) &&
            (pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeMpType != OAM_ME_MP_TYPE_MIP))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMepDirection != OSIX_FALSE)
    {
        if (pOamSetFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeMepDirection != OAM_MEP_DIRECTION_DOWN)
        {
            CLI_SET_ERR (CLI_OAM_MEP_DIRECTION_NOT_SUPPORTED);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeProactiveOamPhbTCValue !=
        OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeProactiveOamPhbTCValue < OAM_PHB_TC_VALUE_MIN) ||
            (pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeProactiveOamPhbTCValue > OAM_PHB_TC_VALUE_MAX))
        {
            CLI_SET_ERR (CLI_OAM_INVALID_PHB_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeOnDemandOamPhbTCValue !=
        OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeOnDemandOamPhbTCValue < OAM_PHB_TC_VALUE_MIN) ||
            (pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeOnDemandOamPhbTCValue > OAM_PHB_TC_VALUE_MAX))
        {
            CLI_SET_ERR (CLI_OAM_INVALID_PHB_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServiceSignaled != OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeServiceSignaled != OAM_FALSE) &&
            (pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeServiceSignaled != OAM_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer != OSIX_FALSE)
    {
        /* Validate base service OID */
        if (OamUtilValidateBaseServiceOid (pOamSetFsMplsTpMeEntry)
            == OSIX_FAILURE)
        {
            CLI_SET_ERR (CLI_OAM_INVALID_SERVICE_INDEX);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus != OSIX_FALSE)
    {
        if ((pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeRowStatus < ACTIVE) ||
            (pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeRowStatus > DESTROY) ||
            (pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeRowStatus == CREATE_AND_GO) ||
            (pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeRowStatus == NOT_READY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

        if (pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus == DESTROY)
        {
            /* Check if ELPS association exists. 
             * If so return failure. */
            if ((u4FsMplsTpMeApps & MPLS_APPLICATION_ELPS)
                || (u4FsMplsTpMeApps & MPLS_APPLICATION_BFD))
            {
                if ((u4FsMplsTpMeApps & MPLS_APPLICATION_ELPS)
                    && (u4FsMplsTpMeApps & MPLS_APPLICATION_BFD))
                {
                    i4ErrorCode = OAM_ELPS_PROACTIVE_SESSION_EXIST;
                    CLI_SET_ERR (CLI_OAM_ELPS_PROACTIVE_SESSION_EXISTS);
                }
                else if (u4FsMplsTpMeApps & MPLS_APPLICATION_ELPS)
                {
                    i4ErrorCode = OAM_ELPS_ASSOCIATION_EXISTS;
                    CLI_SET_ERR (CLI_OAM_ELPS_ASSOCIATION_EXISTS);
                }
                else
                {
                    i4ErrorCode = OAM_PROACTIVE_SESSION_EXISTS;
                    CLI_SET_ERR (CLI_OAM_PROACTIVE_SESSION_EXISTS);
                }

                OamFsMplsTpGlobalConfigEntry.MibObject.
                    u4FsMplsTpContextId = u4FsMplsTpContextId;

                pOamFsMplsTpGlobalConfigEntry = OamGetFsMplsTpGlobalConfigTable
                    (&OamFsMplsTpGlobalConfigEntry);

                if (pOamFsMplsTpGlobalConfigEntry != NULL)
                {
                    pOamFsMplsTpGlobalConfigEntry->MibObject.
                        i4FsMplsTpErrorCode = i4ErrorCode;
                }

                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }

        if ((pOamSetFsMplsTpMeEntry->MibObject.
             i4FsMplsTpMeRowStatus == ACTIVE) &&
            (pOamFsMplsTpMegEntry != NULL) &&
            (pOamFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP))
        {
            if (i4RowCreateOption == OSIX_TRUE)
            {
                if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer
                    != OSIX_FALSE)
                {
                    i4IsServiceSignaled = pOamSetFsMplsTpMeEntry->MibObject.
                        i4FsMplsTpMeServiceSignaled;
                    i4OidLen =
                        pOamSetFsMplsTpMeEntry->MibObject.
                        i4FsMplsTpMeServicePointerLen;

                    if (i4OidLen >= TUNNEL_TABLE_INDICES)
                    {
                        i4TnlOidLen = i4OidLen - 1;
                        u4TnlEgressId = pOamSetFsMplsTpMeEntry->MibObject.
                            au4FsMplsTpMeServicePointer[i4TnlOidLen];
                        i4TnlOidLen = i4TnlOidLen - 1;
                        u4TnlIngressId = pOamSetFsMplsTpMeEntry->MibObject.
                            au4FsMplsTpMeServicePointer[i4TnlOidLen];
                        i4TnlOidLen = i4TnlOidLen - 1;
                        u4TnlInst = pOamSetFsMplsTpMeEntry->MibObject.
                            au4FsMplsTpMeServicePointer[i4TnlOidLen];
                        i4TnlOidLen = i4TnlOidLen - 1;
                        u4TnlIndex = pOamSetFsMplsTpMeEntry->MibObject.
                            au4FsMplsTpMeServicePointer[i4TnlOidLen];
                    }
                }
            }

            if (i4IsServiceSignaled == OAM_FALSE)
            {
                pTeTnlInfo =
                    TeGetTunnelInfo (u4TnlIndex, u4TnlInst, u4TnlIngressId,
                                     u4TnlEgressId);
                if (pTeTnlInfo == NULL)
                {
                    CLI_SET_ERR (CLI_OAM_SERVICE_NOT_EXIST);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return OSIX_FAILURE;
                }
                if (pTeTnlInfo->u1MplsOamAppType == MPLS_OAM_TYPE_Y1731)
                {
                    CLI_SET_ERR (CLI_OAM_TUNNEL_ASSOC_WITH_Y1731);
                    return OSIX_FAILURE;
                }

            }
        }

        if ((pOamFsMplsTpMegEntry != NULL) &&
            (pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus == ACTIVE))
        {
            if (pOamFsMplsTpMegEntry->MibObject.
                i4FsMplsTpMegRowStatus != ACTIVE)
            {
                CLI_SET_ERR (CLI_OAM_MEG_ROWSTATUS_NOT_ACTIVE);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if ((pOamFsMplsTpMegEntry->MibObject.
                 i4FsMplsTpMegOperatorType == OAM_OPERATOR_TYPE_ICC) &&
                ((u4FsMplsTpMeSourceMepIndex == 0) ||
                 (u4FsMplsTpMeSinkMepIndex == 0)))
            {
                CLI_SET_ERR (CLI_OAM_MEP_INDEX_NOT_CONFIGURED);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }

            if ((i4RowCreateOption == OSIX_FALSE) && (i4OidTempLen == 0))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}
