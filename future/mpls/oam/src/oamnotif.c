/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: oamnotif.c,v 1.13 2014/12/12 11:56:46 siva Exp $
*
*
* Description : This file contains the notification related 
*               functions of the OAM module
*****************************************************************************/
#ifndef __OAMNOTIF_C__
#define __OAMNOTIF_C__

#include "oaminc.h"
#include "l2vpextn.h"
#include "oamnotif.h"
#include "fsmtpoam.h"

/******************************************************************************
* Function :   OamNotifMakeObjIdFrmString
* Description: This Function retuns the OID  of the given string for the 
*              proprietary MIB.
*
* Input    :   pi1TextStr - pointer to the string.
*              pTableName - TableName has to be fetched.
*
* Output   :   None.
*
* Returns  :   pOidPtr or NULL
*******************************************************************************/
tSNMP_OID_TYPE     *
OamNotifMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount = 0;
    INT1                ai1TempBuffer[MPLS_OBJECT_NAME_LEN + 1];
    UINT2               u2BufferLen = 0;
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    MEMSET (ai1TempBuffer, 0, sizeof (ai1TempBuffer));

    /* see if there is an alpha descriptor at begining */
    if (!pi1TextStr)
    {
        return NULL;
    }
    if (ISALPHA (*pi1TextStr) != 0)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pu1TempPtr = (UINT1 *) pi1TextStr;

        for (u2Index = 0;
             ((pu1TempPtr < (UINT1 *) pi1DotPtr) &&
              (u2Index < MPLS_OBJECT_NAME_LEN)); u2Index++)
        {
            ai1TempBuffer[u2Index] = *pu1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0; pTableName[u2Index].pName != NULL; u2Index++)
        {
            if ((STRLEN ((INT1 *) ai1TempBuffer) ==
                 STRLEN (pTableName[u2Index].pName))
                && (STRCMP (pTableName[u2Index].pName, (INT1 *) ai1TempBuffer)
                    == 0))
            {
                STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                         MPLS_STRLEN_MIN (ai1TempBuffer,
                                          pTableName[u2Index].pNumber));
                ai1TempBuffer[MPLS_STRLEN_MIN
                              (ai1TempBuffer, pTableName[u2Index].pNumber)] =
                    '\0';
                break;
            }

        }

        if (pTableName[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        u2BufferLen = (UINT2)(sizeof (ai1TempBuffer) - STRLEN (ai1TempBuffer));
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 (u2BufferLen <
                  STRLEN (pi1DotPtr) ? u2BufferLen : STRLEN (pi1DotPtr)));
    }
    else
    {
        /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr,
                 MPLS_STRLEN_MIN (ai1TempBuffer, pi1TextStr));
        ai1TempBuffer[MPLS_STRLEN_MIN (ai1TempBuffer, pi1TextStr)] = '\0';
    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    for (u2Index = 0; ((u2Index <= MPLS_OBJECT_NAME_LEN) &&
                       (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = (UINT4)(u2DotCount + 1);

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (OamNotifParseSubIdNew
            (&pu1TempPtr, &(pOidPtr->pu4_OidList[u2Index])) == OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
* Function :   OamNotifParseSubIdNew
* 
* Description : Parse the string format in number.number..format.
*
* Input       : ppu1TempPtr - pointer to the string.
*               pu4Value    - Pointer the OID List value.
*               
* Output      : value of ppu1TempPtr
*
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*******************************************************************************/
INT4
OamNotifParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

/****************************************************************************
* Function    : OamHandleMeRowStatusActive
* Description : This function checks the Service type from the corresponding
*               MEG entry and updates the MEG indices in the service table,
*               if MEG status is UP and resets the MEG indices if MEG is down.
* Input       : MEG status -UP or DOWN, ME entry 
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamHandleMeRowStatusActive (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{
    tMplsMegId          MplsMegId;
    tMplsTnlLspId       MplsTnlLspId;
    tMplsOamPathStatus  MplsOamPathStatus;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    UINT4               u4PwIndex = 0;
    UINT2               u2Len = 0;

    MEMSET (&MplsMegId, 0, sizeof (tMplsMegId));
    MEMSET (&MplsTnlLspId, 0, sizeof (tMplsTnlLspId));
    MEMSET (&MplsOamPathStatus, 0, sizeof (tMplsOamPathStatus));

    OAM_TRC_FUNC ((OAM_FN_ENTRY, "FUNC:OamHandleMeRowStatusActive\n"));

    /* Fetch the associated Meg entry so that we can get the service type */
    pOamFsMplsTpMegEntry = OamUtilGetAssociatedMegEntry (pOamFsMplsTpMeEntry);

    if (pOamFsMplsTpMegEntry == NULL)
    {
        OAM_TRC ((OAM_MAIN_TRC,
                  "\r%% Associated MEG entry does not exist.\r\n"));
        return OSIX_FAILURE;
    }

    /* Check if ME name is not configured. If not configured, auto-generate an
     * ME name. Check if the generated Me name conflicts with the existing
     * Me names in the Meg. If conflicts return failure. */
    if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen == 0)
    {
        OamUtilGenMeName (pOamFsMplsTpMeEntry,
                          pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName);

        if (OamUtilIsMeNameExist
            (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
             pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
             pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
             pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
             pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName) == OSIX_SUCCESS)
        {
            /* Me name is already assigned */
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% Me name conflict. User has to modify Me name. \
                      Cannot make row status as active.\r\n"));
            return OSIX_FAILURE;
        }

        pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen =
            STRLEN (pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName);
    }

    u2Len =
        (UINT2) pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;

    if ((u2Len < TUNNEL_TABLE_INDICES) || (u2Len >= OAM_MAX_OID_LEN))
    {
        return OSIX_FAILURE;
    }

    /* Update or Reset the MEG indices */
    MplsMegId.u4MegIndex = pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
    MplsMegId.u4MeIndex = pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
    MplsMegId.u4MpIndex = pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;

    /* Update the PW about the MEG status. */
    if (pOamFsMplsTpMegEntry->MibObject.
        i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_PW)
    {
        u4PwIndex = pOamFsMplsTpMeEntry->MibObject.
            au4FsMplsTpMeServicePointer[u2Len - 1];

        /* Even if ME is going from NIS to Active, the path status is 
         * set to DOWN now. After posting this msg to l2vpn module, it is
         * expected that l2vpn module will call Oam API to update the 
         * PW status. It can either inform that PW does not exist, or it
         * can update the PW as UP, or it can update the PW as DOWN.*/
        /* Post a message to L2VPN and let the L2VPN module store the 
         * Meg indices, and then call the OAM's external API to fill the 
         * PW's path status in the OAM data structure and take appropriate 
         * actions.*/
        /* Return after posting the message */

        pInMplsApiInfo =
            (tMplsApiInInfo *) MemAllocMemBlk (OAM_APIININFO_POOLID);
        if (pInMplsApiInfo == NULL)
        {
            return OSIX_FAILURE;
        }
        MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));

        pInMplsApiInfo->u4ContextId =
            pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;

        pInMplsApiInfo->InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
        pInMplsApiInfo->InPathId.MegId.u4MegIndex = MplsMegId.u4MegIndex;
        pInMplsApiInfo->InPathId.MegId.u4MeIndex = MplsMegId.u4MeIndex;
        pInMplsApiInfo->InPathId.MegId.u4MpIndex = MplsMegId.u4MpIndex;

        pInMplsApiInfo->InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
        pInMplsApiInfo->InServicePathId.PwId.u4PwIndex = u4PwIndex;

        if (L2VpnApiHandleExternalRequest (MPLS_OAM_MEG_ASSOC_WITH_PW,
                                           pInMplsApiInfo,
                                           NULL) == OSIX_FAILURE)
        {
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% Meg Index updation failed for Pseudowire.\r\n"));
            MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
        return OSIX_SUCCESS;
    }
    else if (pOamFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP)
    {
        /* Update the Tunnel about the MEG status. */
        u2Len--;
        MplsTnlLspId.u4DstLocalMapNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];
        u2Len--;
        MplsTnlLspId.u4SrcLocalMapNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];
        u2Len--;
        MplsTnlLspId.u4LspNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];
        u2Len--;
        MplsTnlLspId.u4SrcTnlNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];

        /* Before calling MplsOamIfCheckAndUpdateTnlForMeg, set the 
         * path status as DOWN in the variable. If the function
         * returns failure, it means that LSP does not exist. This can happen
         * in Egress node for DYNAMIC LSPs. For such LSPs, the LSP is
         * considered as DOWN until it is created. */
        MplsOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
        if (MplsOamIfCheckAndUpdateTnlForMeg (pOamFsMplsTpMeEntry->
                                              MibObject.u4FsMplsTpContextId,
                                              &MplsMegId, &MplsTnlLspId,
                                              &MplsOamPathStatus) ==
            OSIX_FAILURE)
        {
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% Meg Index updation failed for Tunnel.\r\n"));
            /* For static LSP, check whether the LSP exists.
             * For dynamic signalled LSP, need not check whether LSP exist. */
            if (pOamFsMplsTpMeEntry->MibObject.
                i4FsMplsTpMeServiceSignaled != OAM_TRUE)
            {
                return OSIX_FAILURE;
            }
        }
    }
    if (MplsOamPathStatus.u1PathStatus == MPLS_PATH_STATUS_UP)
    {
        pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
            &= (~OAM_ME_OPER_STATUS_PATH_DOWN);
    }
    /* Applicable only for tunnel */
    /* Set the service path status in the MEG oper status object. */

    /* Applicable only for Tunnel */
    if (pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus == OAM_ME_OPER_STATUS_UP)
    {
        if (MplsOamIfNotifyMegStatusToApp (MPLS_MODULE,
                                           pOamFsMplsTpMeEntry->
                                           u4FsMplsTpMeApps,
                                           pOamFsMplsTpMeEntry,
                                           MPLS_MEG_UP) == OSIX_FAILURE)
        {
            OAM_TRC ((OAM_MAIN_TRC, "\r%% Meg status notification to OAM \
                      applications failed.\r\n"));
        }
    }
    OAM_TRC_FUNC ((OAM_FN_EXIT, "EXIT:OamHandleMeRowStatusActive\n"));

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamResetMegIndicesInLspOrPw
* Description : This function checks the Service type from the corresponding
*               MEG entry and updates the MEG indices in the service table,
*               if MEG status is UP and resets the MEG indices if MEG is down.
* Input       : MEG status -UP or DOWN, ME entry 
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamResetMegIndicesInLspOrPw (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{
    tMplsMegId          MplsMegId;
    tMplsTnlLspId       MplsTnlLspId;
    tMplsOamPathStatus  MplsOamPathStatus;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    UINT4               u4PwIndex = 0;
    UINT2               u2Len = 0;

    MEMSET (&MplsMegId, 0, sizeof (tMplsMegId));
    MEMSET (&MplsTnlLspId, 0, sizeof (tMplsTnlLspId));
    MEMSET (&MplsOamPathStatus, 0, sizeof (tMplsOamPathStatus));

    OAM_TRC_FUNC ((OAM_FN_ENTRY, "FUNC:OamResetMegIndicesInLspOrPw\n"));

    /* Fetch the associated Meg entry so that we can get the service type */
    pOamFsMplsTpMegEntry = OamUtilGetAssociatedMegEntry (pOamFsMplsTpMeEntry);

    if (pOamFsMplsTpMegEntry == NULL)
    {
        OAM_TRC ((OAM_MAIN_TRC,
                  "\r%% Associated MEG entry does not exist.\r\n"));
        return OSIX_FAILURE;
    }

    u2Len =
        (UINT2) pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;
    if ((u2Len < TUNNEL_TABLE_INDICES) || (u2Len >= OAM_MAX_OID_LEN))
    {
        /* Service Pointer is already reset, if we allow this there 
         * will be a out of bound array access below */
        return OSIX_SUCCESS;
    }

    /* Update or Reset the MEG indices */
    MplsMegId.u4MegIndex = 0;
    MplsMegId.u4MeIndex = 0;
    MplsMegId.u4MpIndex = 0;

    /* Update the PW about the MEG status. */
    if (pOamFsMplsTpMegEntry->MibObject.
        i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_PW)
    {
        u4PwIndex = pOamFsMplsTpMeEntry->MibObject.
            au4FsMplsTpMeServicePointer[u2Len - 1];

        /* Even if ME is going from NIS to Active, the path status is 
         * set to DOWN now. After posting this msg to l2vpn module, it is
         * expected that l2vpn module will call Oam API to update the 
         * PW status. It can either inform that PW does not exist, or it
         * can update the PW as UP, or it can update the PW as DOWN.*/
        /* Post a message to L2VPN and let the L2VPN module store the 
         * Meg indices, and then call the OAM's external API to fill the 
         * PW's path status in the OAM data structure and take appropriate 
         * actions.*/
        /* Return after posting the message */
        pInMplsApiInfo =
            (tMplsApiInInfo *) MemAllocMemBlk (OAM_APIININFO_POOLID);
        if (pInMplsApiInfo == NULL)
        {
            return OSIX_FAILURE;
        }

        MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));

        pInMplsApiInfo->u4ContextId = pOamFsMplsTpMeEntry->MibObject.
            u4FsMplsTpContextId;

        pInMplsApiInfo->InPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
        pInMplsApiInfo->InPathId.MegId.u4MegIndex = MplsMegId.u4MegIndex;
        pInMplsApiInfo->InPathId.MegId.u4MeIndex = MplsMegId.u4MeIndex;
        pInMplsApiInfo->InPathId.MegId.u4MpIndex = MplsMegId.u4MpIndex;

        pInMplsApiInfo->InServicePathId.u4PathType = MPLS_PATH_TYPE_PW;
        pInMplsApiInfo->InServicePathId.PwId.u4PwIndex = u4PwIndex;

        if (L2VpnApiHandleExternalRequest (MPLS_OAM_MEG_ASSOC_WITH_PW,
                                           pInMplsApiInfo,
                                           NULL) == OSIX_FAILURE)
        {
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% Meg Index updation failed for Pseudowire.\r\n"));
            MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
        return OSIX_SUCCESS;
    }
    else if (pOamFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP)
    {
        /* Update the Tunnel about the MEG status. */
        u2Len--;
        MplsTnlLspId.u4DstLocalMapNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];
        u2Len--;
        MplsTnlLspId.u4SrcLocalMapNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];
        u2Len--;
        MplsTnlLspId.u4LspNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];
        u2Len--;
        MplsTnlLspId.u4SrcTnlNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];

        /* Update the MegIndices in Tunnel entry to zero and ignore the 
         * path status returned by the following function call.*/
        if (MplsOamIfCheckAndUpdateTnlForMeg (pOamFsMplsTpMeEntry->
                                              MibObject.u4FsMplsTpContextId,
                                              &MplsMegId, &MplsTnlLspId,
                                              &MplsOamPathStatus) ==
            OSIX_FAILURE)
        {
            OAM_TRC ((OAM_MAIN_TRC,
                      "\r%% Meg Index updation failed for Tunnel.\r\n"));
            if (pOamFsMplsTpMeEntry->MibObject.
                i4FsMplsTpMeServiceSignaled != OAM_TRUE)
            {
                return OSIX_FAILURE;
            }
        }
    }
    OAM_TRC_FUNC ((OAM_FN_EXIT, "EXIT:OamResetMegIndicesInLspOrPw\n"));

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : OamNotifSendTrapAndSyslog                            */
/*                                                                           */
/* Description        : This function will send an SNMP trap and syslog      */
/*                      message to the administrator for various OAM         */
/*                      conditions.                                          */
/*                                                                           */
/* Input(s)           : pNotifyInfo - Information to be sent in the Trap     */
/*                                    message.                               */
/*                      u4TrapType - Specific Type for Trap Message          */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VOID                                                 */
/*****************************************************************************/
VOID
OamNotifSendTrapAndSyslog (tOamNotifyInfo * pNotifyInfo, UINT4 u4TrapType)
{
    tSNMP_COUNTER64_TYPE CounterVal = { 0, 0 };
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pSnmpOid = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    UINT1               au1MegSubOperStatus[1];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT1               au1Buf[MPLS_OBJECT_NAME_LEN];
    CHR1                ac1OamSysLogMsg[MPLS_OBJECT_NAME_LEN];
    UINT4               u4OidLen = 0;
    UINT4               u4MegSubOperStatus = 0;

    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    MEMSET (au1MegSubOperStatus, 0, sizeof (au1MegSubOperStatus));
    MEMSET (ac1OamSysLogMsg, 0, sizeof (ac1OamSysLogMsg));
    MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));

    if (MplsPortVcmGetAliasName (pNotifyInfo->u4ContextId,
                                 au1ContextName) == OSIX_FAILURE)
    {
        return;
    }

    if (OamGetAllFsMplsTpGlobalConfigTable (&OamFsMplsTpGlobalConfigEntry)
        != OSIX_SUCCESS)
    {
        return;
    }

    if (OamFsMplsTpGlobalConfigEntry.MibObject.i4FsMplsTpNotificationEnable
        != MPLS_SNMP_TRUE)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r Send Trap: Notification not enabled.\r\n"));
        return;
    }

    SNPRINTF (ac1OamSysLogMsg, sizeof (ac1OamSysLogMsg),
              "Context: %s; MEG: %s; ME: %s; Status: %s; SubStatus: %s",
              au1ContextName, pNotifyInfo->au1MegName,
              pNotifyInfo->au1MeName,
              gapc1MegOperStatus[pNotifyInfo->i4MegOperStatus],
              gapc1MegSubOperStatus[pNotifyInfo->u1MegSubOperStatus]);

    u4OidLen = (sizeof (gau4SnmpTrapOid) / sizeof (UINT4));
    pSnmpOid = alloc_oid (u4OidLen);
    if (pSnmpOid == NULL)
    {
        return;
    }
    MEMCPY (pSnmpOid->pu4_OidList, gau4SnmpTrapOid, sizeof (gau4SnmpTrapOid));
    pSnmpOid->u4_Length = u4OidLen;

    SNPRINTF ((char *) au1Buf, sizeof (au1Buf), "fsMplsTpOamNotifications");
    pEnterpriseOid =
        OamNotifMakeObjIdFrmString ((INT1 *) au1Buf,
                                    (UINT1 *) fs_mpls_oam_mib_oid_table);
    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpOid);
        return;
    }
    pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] = u4TrapType;

    pVbList = (tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSnmpOid,
                                                       SNMP_DATA_TYPE_OBJECT_ID,
                                                       0L, 0, NULL,
                                                       pEnterpriseOid,
                                                       CounterVal);
    if (pVbList == NULL)
    {
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_FreeOid (pSnmpOid);
        return;
    }

    pStartVb = pVbList;

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SNPRINTF ((char *) au1Buf, sizeof (au1Buf), "fsMplsTpOamContextName");

    pOid =
        OamNotifMakeObjIdFrmString ((INT1 *) au1Buf,
                                    (UINT1 *) fs_mpls_oam_mib_oid_table);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pOstring = SNMP_AGT_FormOctetString (au1ContextName, VCM_ALIAS_MAX_LEN);
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *)
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                              pOstring, NULL, CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SNPRINTF ((char *) au1Buf, sizeof (au1Buf), "fsMplsTpMegName");

    pOid =
        OamNotifMakeObjIdFrmString ((INT1 *) au1Buf,
                                    (UINT1 *) fs_mpls_oam_mib_oid_table);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    pOid->pu4_OidList[pOid->u4_Length++] = pNotifyInfo->u4ContextId;
    pOid->pu4_OidList[pOid->u4_Length++] = pNotifyInfo->u4MegIndex;

    pOstring = SNMP_AGT_FormOctetString (pNotifyInfo->au1MegName,
                                         OAM_MEG_NAME_MAX_LEN);
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *)
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                              pOstring, NULL, CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SNPRINTF ((char *) au1Buf, sizeof (au1Buf), "fsMplsTpMeName");

    pOid =
        OamNotifMakeObjIdFrmString ((INT1 *) au1Buf,
                                    (UINT1 *) fs_mpls_oam_mib_oid_table);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }
    pOid->pu4_OidList[pOid->u4_Length++] = pNotifyInfo->u4ContextId;
    pOid->pu4_OidList[pOid->u4_Length++] = pNotifyInfo->u4MegIndex;
    pOid->pu4_OidList[pOid->u4_Length++] = pNotifyInfo->u4MeIndex;
    pOid->pu4_OidList[pOid->u4_Length++] = pNotifyInfo->u4MpIndex;

    pOstring = SNMP_AGT_FormOctetString (pNotifyInfo->au1MeName,
                                         OAM_ME_NAME_MAX_LEN);
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *)
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                              pOstring, NULL, CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SNPRINTF ((char *) au1Buf, sizeof (au1Buf), "fsMplsTpOamMegOperStatus");

    pOid =
        OamNotifMakeObjIdFrmString ((INT1 *) au1Buf,
                                    (UINT1 *) fs_mpls_oam_mib_oid_table);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *)
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_INTEGER32, 0,
                              pNotifyInfo->i4MegOperStatus,
                              NULL, NULL, CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SNPRINTF ((char *) au1Buf, sizeof (au1Buf), "fsMplsTpOamMegSubOperStatus");

    pOid =
        OamNotifMakeObjIdFrmString ((INT1 *) au1Buf,
                                    (UINT1 *) fs_mpls_oam_mib_oid_table);
    if (pOid == NULL)
    {
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    if (pNotifyInfo->u1MegSubOperStatus & OAM_ME_OPER_STATUS_MEG_DOWN)
    {
        u4MegSubOperStatus |= SNMP_ME_OPER_STATUS_MEG_DOWN;
    }
    if (pNotifyInfo->u1MegSubOperStatus & OAM_ME_OPER_STATUS_ME_DOWN)
    {
        u4MegSubOperStatus |= SNMP_ME_OPER_STATUS_ME_DOWN;
    }
    if (pNotifyInfo->u1MegSubOperStatus & OAM_ME_OPER_STATUS_OAM_DOWN)
    {
        u4MegSubOperStatus |= SNMP_ME_OPER_STATUS_OAM_DOWN;
    }
    if (pNotifyInfo->u1MegSubOperStatus & OAM_ME_OPER_STATUS_PATH_DOWN)
    {
        u4MegSubOperStatus |= SNMP_ME_OPER_STATUS_PATH_DOWN;
    }

    au1MegSubOperStatus[0] = (UINT1) u4MegSubOperStatus;
    pOstring = SNMP_AGT_FormOctetString (au1MegSubOperStatus, 1);
    if (pOstring == NULL)
    {
        SNMP_FreeOid (pOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind =
        (tSNMP_VAR_BIND *)
        SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OCTET_PRIM, 0, 0,
                              pOstring, NULL, CounterVal);
    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    MplsPortFmNotifyFaults (pStartVb, ac1OamSysLogMsg);

    return;
}

/****************************************************************************
* Function    : OamNotifyAndUpdateMegOperStatus
* Description : This function finds the associated Me entries and updates
*               the MEG status in the ME oper status object and sends the
*               MEG status to the OAM applications.
* Input       : pOamFsMplsTpMegEntry and Meg Status
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE 
*****************************************************************************/
INT4
OamNotifyAndUpdateMegOperStatus (tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry,
                                 UINT4 u4MegStatus)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryIn = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    UINT4               u4OldOperStatus = 0;
    UINT4               u4ContextId = 0;
    UINT4               u4MegIndex = 0;

    pOamFsMplsTpMeEntryIn =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntryIn == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pOamFsMplsTpMeEntryIn, 0, sizeof (tOamFsMplsTpMeEntry));

    u4ContextId = pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;
    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpContextId =
        pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;

    u4MegIndex = pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;
    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMegIndex =
        pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;

    /* MegIndex is passed as valid value. MpIndex and MeIndex as passed as zero.
     * It is expected that the GetNext API would return the first valid entry
     * from the ME table, that matches the MegIndex. */
    pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntryIn);

    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamFsMplsTpMeEntryIn);

    while (pOamFsMplsTpMeEntry != NULL)
    {
        if ((pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId != u4ContextId)
            || (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex !=
                u4MegIndex))
        {
            break;
        }

        u4OldOperStatus = pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus;
        if (u4MegStatus == MPLS_MEG_UP)
        {
            pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
                &= (~OAM_ME_OPER_STATUS_MEG_DOWN);

            if ((u4OldOperStatus != OAM_ME_OPER_STATUS_UP) &&
                (pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
                 == OAM_ME_OPER_STATUS_UP))
            {
                /* After resetting the MEG DOWN bit to zero, there are no 
                 * other DOWN bits set. It means that MEG is going from 
                 * DOWN to UP. Notify MEG status UP to applications */
                if (MplsOamIfNotifyMegStatusToApp
                    (MPLS_MODULE,
                     pOamFsMplsTpMeEntry->u4FsMplsTpMeApps,
                     pOamFsMplsTpMeEntry, MPLS_MEG_UP) == OSIX_FAILURE)
                {
                    OAM_TRC ((OAM_MAIN_TRC,
                              "\r%% Meg status notification to OAM \
                              applications failed.\r\n"));
                    return OSIX_FAILURE;
                }
            }
        }
        else
        {
            pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
                |= OAM_ME_OPER_STATUS_MEG_DOWN;

            /* After updating the new value, compare the old value.
             * If the old value is zero, it means that there was no error.
             * It means that the MEG was UP earlier. 
             * If the MEG was UP earlier, and it goes to DOWN now, 
             * notify Apps */
            if (u4OldOperStatus == 0)
            {
                /* Notify MEG down status to applications */
                if (MplsOamIfNotifyMegStatusToApp (MPLS_MODULE,
                                                   pOamFsMplsTpMeEntry->
                                                   u4FsMplsTpMeApps,
                                                   pOamFsMplsTpMeEntry,
                                                   MPLS_MEG_DOWN) ==
                    OSIX_FAILURE)
                {
                    OAM_TRC ((OAM_MAIN_TRC,
                              "\r%% Meg status notification to OAM \
                              applications failed.\r\n"));
                }

            }
        }
        pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntry);
    }

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  oamnotif.c                     */
/*-----------------------------------------------------------------------*/
#endif
