/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamdvalg.c,v 1.4 2012/03/19 11:28:20 siva Exp $
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Oam 
*********************************************************************/

#include "oaminc.h"

/****************************************************************************
* Function    : OamInitializeMibFsMplsTpNodeMapTable
* Input       : pOamFsMplsTpNodeMapEntry
* Output      : It Intialize the given structure
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamInitializeMibFsMplsTpNodeMapTable (tOamFsMplsTpNodeMapEntry *
                                      pOamFsMplsTpNodeMapEntry)
{

    pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId = 0;

    pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapLocalNum = 0;

    pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapGlobalId = 0;

    pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId = 0;

    pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus = 0;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamInitializeMibFsMplsTpMegTable
* Input       : pOamFsMplsTpMegEntry
* Output      : It Intialize the given structure
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamInitializeMibFsMplsTpMegTable (tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry)
{

    pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId = 0;

    pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex = 0;

    pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen = 0;

    MEMSET ((pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName),
            0, sizeof (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName));
    pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegOperatorType = 1;

    pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen = 0;

    MEMSET ((pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc),
            0, sizeof (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc));
    pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen = 0;

    MEMSET ((pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc),
            0, sizeof (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc));
    pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType = 1;

    pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation = 1;

    pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus = 0;

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : OamInitializeMibFsMplsTpMeTable
* Input       : pOamFsMplsTpMeEntry
* Output      : It Intialize the given structure
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamInitializeMibFsMplsTpMeTable (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{

    pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId = 0;

    pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex = 0;

    pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex = 0;

    pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex = 0;

    pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen = 0;

    MEMSET ((pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName),
            0, pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen);

    pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex = 0;

    pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex = 0;

    pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex = 0;

    pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType = 1;

    pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection = 2;

    pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeProactiveOamPhbTCValue = 1;

    pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeOnDemandOamPhbTCValue = 1;

    pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServiceSignaled = 2;

    pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus = 0;

    return OSIX_SUCCESS;
}
