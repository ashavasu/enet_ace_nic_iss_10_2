/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamclig.c,v 1.10 2013/12/20 03:25:55 siva Exp $
*
* Description: This file contains the Oam CLI related routines 
*********************************************************************/

#include "oaminc.h"
#include "mplscli.h"
#include "oamclig.h"
extern CONST CHR1  *OamCliErrString[];
/****************************************************************************
 * Function    :  cli_process_Oam_cmd
 * Description :  This function is exported to CLI module to handle the
                OAM cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Oam_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[OAM_CLI_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    tOamFsMplsTpGlobalConfigEntry *pOamSetFsMplsTpGlobalConfigEntry = NULL;
    tOamIsSetFsMplsTpGlobalConfigEntry *pOamIsSetFsMplsTpGlobalConfigEntry =
        NULL;
    tOamFsMplsTpNodeMapEntry *pOamSetFsMplsTpNodeMapEntry = NULL;
    tOamIsSetFsMplsTpNodeMapEntry *pOamIsSetFsMplsTpNodeMapEntry = NULL;
    tOamFsMplsTpMegEntry *pOamSetFsMplsTpMegEntry = NULL;
    tOamIsSetFsMplsTpMegEntry *pOamIsSetFsMplsTpMegEntry = NULL;
    tOamFsMplsTpMeEntry *pOamSetFsMplsTpMeEntry = NULL;
    tOamIsSetFsMplsTpMeEntry *pOamIsSetFsMplsTpMeEntry = NULL;
    INT4                i4Args = 0;

    UNUSED_PARAM (u4CmdType);
    CliRegisterLock (CliHandle, OamMainTaskLock, OamMainTaskUnLock);
    OAM_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        i4RetStatus = (UINT4) i4Inst;
    }

    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == OAM_CLI_MAX_ARGS)
            break;
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_OAM_FSMPLSTPGLOBALCONFIGTABLE:
            if (args[8] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[8]);
                OamCliSetDebugLevel (CliHandle, i4Args);
            }

            pOamSetFsMplsTpGlobalConfigEntry =
                (tOamFsMplsTpGlobalConfigEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPGLOBALCONFIGTABLE_POOLID);

            if (pOamSetFsMplsTpGlobalConfigEntry == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pOamSetFsMplsTpGlobalConfigEntry, 0,
                    sizeof (tOamFsMplsTpGlobalConfigEntry));

            pOamIsSetFsMplsTpGlobalConfigEntry =
                (tOamIsSetFsMplsTpGlobalConfigEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPGLOBALCONFIGTABLE_POOLID);

            if (pOamIsSetFsMplsTpGlobalConfigEntry == NULL)
            {
                MemReleaseMemBlock (OAM_FSMPLSTPGLOBALCONFIGTABLE_POOLID,
                                    (UINT1 *) pOamSetFsMplsTpGlobalConfigEntry);

                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pOamIsSetFsMplsTpGlobalConfigEntry, 0,
                    sizeof (tOamIsSetFsMplsTpGlobalConfigEntry));

            OAM_FILL_FSMPLSTPGLOBALCONFIGTABLE_ARGS ((pOamSetFsMplsTpGlobalConfigEntry), (pOamIsSetFsMplsTpGlobalConfigEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7]);

            i4RetStatus =
                OamCliSetFsMplsTpGlobalConfigTable (CliHandle,
                                                    (pOamSetFsMplsTpGlobalConfigEntry),
                                                    (pOamIsSetFsMplsTpGlobalConfigEntry));
            MemReleaseMemBlock (OAM_FSMPLSTPGLOBALCONFIGTABLE_POOLID,
                                (UINT1 *) pOamSetFsMplsTpGlobalConfigEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPGLOBALCONFIGTABLE_POOLID,
                                (UINT1 *) pOamIsSetFsMplsTpGlobalConfigEntry);

            break;

        case CLI_OAM_FSMPLSTPNODEMAPTABLE:

            pOamSetFsMplsTpNodeMapEntry =
                (tOamFsMplsTpNodeMapEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPNODEMAPTABLE_POOLID);

            if (pOamSetFsMplsTpNodeMapEntry == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pOamSetFsMplsTpNodeMapEntry, 0,
                    sizeof (tOamFsMplsTpNodeMapEntry));

            pOamIsSetFsMplsTpNodeMapEntry =
                (tOamIsSetFsMplsTpNodeMapEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPNODEMAPTABLE_POOLID);

            if (pOamIsSetFsMplsTpNodeMapEntry == NULL)
            {
                MemReleaseMemBlock (OAM_FSMPLSTPNODEMAPTABLE_POOLID,
                                    (UINT1 *) pOamSetFsMplsTpNodeMapEntry);

                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pOamIsSetFsMplsTpNodeMapEntry, 0,
                    sizeof (tOamIsSetFsMplsTpNodeMapEntry));

            OAM_FILL_FSMPLSTPNODEMAPTABLE_ARGS ((pOamSetFsMplsTpNodeMapEntry),
                                                (pOamIsSetFsMplsTpNodeMapEntry),
                                                args[0], args[1], args[2],
                                                args[3], args[4]);

            i4RetStatus =
                OamCliSetFsMplsTpNodeMapTable (CliHandle,
                                               (pOamSetFsMplsTpNodeMapEntry),
                                               (pOamIsSetFsMplsTpNodeMapEntry));
            MemReleaseMemBlock (OAM_FSMPLSTPNODEMAPTABLE_POOLID,
                                (UINT1 *) pOamSetFsMplsTpNodeMapEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPNODEMAPTABLE_POOLID,
                                (UINT1 *) pOamIsSetFsMplsTpNodeMapEntry);

            break;

        case CLI_OAM_FSMPLSTPMEGTABLE:

            pOamSetFsMplsTpMegEntry =
                (tOamFsMplsTpMegEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPMEGTABLE_POOLID);

            if (pOamSetFsMplsTpMegEntry == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pOamSetFsMplsTpMegEntry, 0, sizeof (tOamFsMplsTpMegEntry));

            pOamIsSetFsMplsTpMegEntry =
                (tOamIsSetFsMplsTpMegEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPMEGTABLE_POOLID);

            if (pOamIsSetFsMplsTpMegEntry == NULL)
            {
                MemReleaseMemBlock (OAM_FSMPLSTPMEGTABLE_POOLID,
                                    (UINT1 *) pOamSetFsMplsTpMegEntry);

                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pOamIsSetFsMplsTpMegEntry, 0,
                    sizeof (tOamIsSetFsMplsTpMegEntry));

            OAM_FILL_FSMPLSTPMEGTABLE_ARGS ((pOamSetFsMplsTpMegEntry),
                                            (pOamIsSetFsMplsTpMegEntry),
                                            args[0], args[1], args[2], args[3],
                                            args[4], args[5], args[6], args[7],
                                            args[8], args[9], args[10],
                                            args[11]);

            i4RetStatus =
                OamCliSetFsMplsTpMegTable (CliHandle, (pOamSetFsMplsTpMegEntry),
                                           (pOamIsSetFsMplsTpMegEntry));
            MemReleaseMemBlock (OAM_FSMPLSTPMEGTABLE_POOLID,
                                (UINT1 *) pOamSetFsMplsTpMegEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMEGTABLE_POOLID,
                                (UINT1 *) pOamIsSetFsMplsTpMegEntry);

            break;

        case CLI_OAM_FSMPLSTPMETABLE:

            pOamSetFsMplsTpMeEntry =
                (tOamFsMplsTpMeEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);

            if (pOamSetFsMplsTpMeEntry == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pOamSetFsMplsTpMeEntry, 0, sizeof (tOamFsMplsTpMeEntry));

            pOamIsSetFsMplsTpMeEntry =
                (tOamIsSetFsMplsTpMeEntry *)
                MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);

            if (pOamIsSetFsMplsTpMeEntry == NULL)
            {
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamSetFsMplsTpMeEntry);

                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pOamIsSetFsMplsTpMeEntry, 0,
                    sizeof (tOamIsSetFsMplsTpMeEntry));

            OAM_FILL_FSMPLSTPMETABLE_ARGS ((pOamSetFsMplsTpMeEntry),
                                           (pOamIsSetFsMplsTpMeEntry), args[0],
                                           args[1], args[2], args[3], args[4],
                                           args[5], args[6], args[7], args[8],
                                           args[9], args[10], args[11],
                                           args[12], args[13], args[14],
                                           args[15], args[16]);

            i4RetStatus =
                OamCliSetFsMplsTpMeTable (CliHandle, (pOamSetFsMplsTpMeEntry),
                                          (pOamIsSetFsMplsTpMeEntry));
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamSetFsMplsTpMeEntry);
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamIsSetFsMplsTpMeEntry);

            break;

    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_MPLS_OAM) &&
            (u4ErrCode < CLI_OAM_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s",
                       OamCliErrString[CLI_ERR_OFFSET_MPLS_OAM (u4ErrCode)]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    OAM_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
* Function    :  OamCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OamCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    OAM_TRC_LVL = 0;

    UNUSED_PARAM (CliHandle);
    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        OAM_TRC_LVL = OAM_FN_ENTRY |
            OAM_FN_EXIT |
            OAM_MGMT_TRC |
            OAM_MAIN_TRC | OAM_UTIL_TRC | OAM_RESOURCE_TRC | OAM_FAILURE_TRC;

    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        OAM_TRC_LVL = OAM_FN_ENTRY |
            OAM_FN_EXIT |
            OAM_MGMT_TRC |
            OAM_MAIN_TRC | OAM_UTIL_TRC | OAM_RESOURCE_TRC | OAM_FAILURE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        OAM_TRC_LVL = OAM_FAILURE_TRC | OAM_MGMT_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        OAM_TRC_LVL = OAM_UTIL_TRC | OAM_FAILURE_TRC | OAM_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        OAM_TRC_LVL =
            OAM_MAIN_TRC | OAM_UTIL_TRC | OAM_RESOURCE_TRC | OAM_FAILURE_TRC;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        OAM_TRC_LVL = OAM_MGMT_TRC |
            OAM_MAIN_TRC | OAM_UTIL_TRC | OAM_RESOURCE_TRC | OAM_FAILURE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  OamCliSetFsMplsTpGlobalConfigTable
* Description :
* Input       :  CliHandle 
*            pOamSetFsMplsTpGlobalConfigEntry
*            pOamIsSetFsMplsTpGlobalConfigEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OamCliSetFsMplsTpGlobalConfigTable (tCliHandle CliHandle,
                                    tOamFsMplsTpGlobalConfigEntry *
                                    pOamSetFsMplsTpGlobalConfigEntry,
                                    tOamIsSetFsMplsTpGlobalConfigEntry *
                                    pOamIsSetFsMplsTpGlobalConfigEntry)
{
    UINT4               u4ErrorCode;

    if (OamTestAllFsMplsTpGlobalConfigTable
        (&u4ErrorCode, pOamSetFsMplsTpGlobalConfigEntry,
         pOamIsSetFsMplsTpGlobalConfigEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (OamSetAllFsMplsTpGlobalConfigTable
        (pOamSetFsMplsTpGlobalConfigEntry,
         pOamIsSetFsMplsTpGlobalConfigEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  OamCliSetFsMplsTpNodeMapTable
* Description :
* Input       :  CliHandle 
*            pOamSetFsMplsTpNodeMapEntry
*            pOamIsSetFsMplsTpNodeMapEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OamCliSetFsMplsTpNodeMapTable (tCliHandle CliHandle,
                               tOamFsMplsTpNodeMapEntry *
                               pOamSetFsMplsTpNodeMapEntry,
                               tOamIsSetFsMplsTpNodeMapEntry *
                               pOamIsSetFsMplsTpNodeMapEntry)
{
    UINT4               u4ErrorCode;

    if (OamTestAllFsMplsTpNodeMapTable
        (&u4ErrorCode, pOamSetFsMplsTpNodeMapEntry,
         pOamIsSetFsMplsTpNodeMapEntry, OSIX_TRUE, OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (OamSetAllFsMplsTpNodeMapTable
        (pOamSetFsMplsTpNodeMapEntry, pOamIsSetFsMplsTpNodeMapEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  OamCliSetFsMplsTpMegTable
* Description :
* Input       :  CliHandle 
*            pOamSetFsMplsTpMegEntry
*            pOamIsSetFsMplsTpMegEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OamCliSetFsMplsTpMegTable (tCliHandle CliHandle,
                           tOamFsMplsTpMegEntry * pOamSetFsMplsTpMegEntry,
                           tOamIsSetFsMplsTpMegEntry *
                           pOamIsSetFsMplsTpMegEntry)
{
    UINT4               u4ErrorCode;

    if (OamTestAllFsMplsTpMegTable (&u4ErrorCode, pOamSetFsMplsTpMegEntry,
                                    pOamIsSetFsMplsTpMegEntry, OSIX_TRUE,
                                    OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (OamSetAllFsMplsTpMegTable
        (pOamSetFsMplsTpMegEntry, pOamIsSetFsMplsTpMegEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  OamCliSetFsMplsTpMeTable
* Description :
* Input       :  CliHandle 
*            pOamSetFsMplsTpMeEntry
*            pOamIsSetFsMplsTpMeEntry
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
OamCliSetFsMplsTpMeTable (tCliHandle CliHandle,
                          tOamFsMplsTpMeEntry * pOamSetFsMplsTpMeEntry,
                          tOamIsSetFsMplsTpMeEntry * pOamIsSetFsMplsTpMeEntry)
{
    UINT4               u4ErrorCode;

    if (OamTestAllFsMplsTpMeTable (&u4ErrorCode, pOamSetFsMplsTpMeEntry,
                                   pOamIsSetFsMplsTpMeEntry, OSIX_TRUE,
                                   OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (OamSetAllFsMplsTpMeTable
        (pOamSetFsMplsTpMeEntry, pOamIsSetFsMplsTpMeEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}
