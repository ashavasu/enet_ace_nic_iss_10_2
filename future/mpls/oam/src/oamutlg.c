/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamutlg.c,v 1.6 2013/07/04 13:23:27 siva Exp $
*
* Description: This file contains utility functions used by protocol Oam
*********************************************************************/

#include "oaminc.h"

PUBLIC INT4
OamFsMplsTpGlobalConfigEntryCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOamFsMplsTpGlobalConfigEntry,
                       MibObject.FsMplsTpGlobalConfigTableNode);

    if ((gOamGlobals.OamGlbMib.FsMplsTpGlobalConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMplsTpGlobalConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PUBLIC INT4
FsMplsTpGlobalConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tOamFsMplsTpGlobalConfigEntry *pFsMplsTpGlobalConfigEntry1 =
        (tOamFsMplsTpGlobalConfigEntry *) pRBElem1;
    tOamFsMplsTpGlobalConfigEntry *pFsMplsTpGlobalConfigEntry2 =
        (tOamFsMplsTpGlobalConfigEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMplsTpGlobalConfigEntry1->MibObject.u4FsMplsTpContextId >
        pFsMplsTpGlobalConfigEntry2->MibObject.u4FsMplsTpContextId)
    {
        return 1;
    }
    else if (pFsMplsTpGlobalConfigEntry1->MibObject.u4FsMplsTpContextId <
             pFsMplsTpGlobalConfigEntry2->MibObject.u4FsMplsTpContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  OamSetAllFsMplsTpGlobalConfigTableTrigger
 Input       :  The Indices
                pOamSetFsMplsTpGlobalConfigEntry
                pOamIsSetFsMplsTpGlobalConfigEntry
 Output      :  This Routine is used to send 
                MSR and RM indication
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
OamSetAllFsMplsTpGlobalConfigTableTrigger (tOamFsMplsTpGlobalConfigEntry *
                                           pOamSetFsMplsTpGlobalConfigEntry,
                                           tOamIsSetFsMplsTpGlobalConfigEntry *
                                           pOamIsSetFsMplsTpGlobalConfigEntry,
                                           INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMplsTpIccVal;
    UINT1               au1FsMplsTpIccVal[256];

    MEMSET (au1FsMplsTpIccVal, 0, sizeof (au1FsMplsTpIccVal));
    FsMplsTpIccVal.pu1_OctetList = au1FsMplsTpIccVal;
    FsMplsTpIccVal.i4_Length = 0;

    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpOamModuleStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpOamModuleStatus, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      u4FsMplsTpContextId,
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      i4FsMplsTpOamModuleStatus);
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpGlobalId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpGlobalId, 13, OamMainTaskLock, OamMainTaskUnLock,
                      0, 0, 1, i4SetOption, "%u %u",
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      u4FsMplsTpContextId,
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      u4FsMplsTpGlobalId);
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpIcc == OSIX_TRUE)
    {
        MEMCPY (FsMplsTpIccVal.pu1_OctetList,
                pOamSetFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc,
                pOamSetFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen);
        FsMplsTpIccVal.i4_Length =
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen;

        nmhSetCmnNew (FsMplsTpIcc, 13, OamMainTaskLock, OamMainTaskUnLock, 0, 0,
                      1, i4SetOption, "%u %s",
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      u4FsMplsTpContextId, &FsMplsTpIccVal);
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNodeIdentifier ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpNodeIdentifier, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      u4FsMplsTpContextId,
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      u4FsMplsTpNodeIdentifier);
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpTraceLevel == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpTraceLevel, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      u4FsMplsTpContextId,
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      u4FsMplsTpTraceLevel);
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNotificationEnable ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpNotificationEnable, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      u4FsMplsTpContextId,
                      pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                      i4FsMplsTpNotificationEnable);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsMplsTpGlobalConfigTableFilterInputs
 Input       :  The Indices
                pOamFsMplsTpGlobalConfigEntry
                pOamSetFsMplsTpGlobalConfigEntry
                pOamIsSetFsMplsTpGlobalConfigEntry
 Output      :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMplsTpGlobalConfigTableFilterInputs (tOamFsMplsTpGlobalConfigEntry *
                                       pOamFsMplsTpGlobalConfigEntry,
                                       tOamFsMplsTpGlobalConfigEntry *
                                       pOamSetFsMplsTpGlobalConfigEntry,
                                       tOamIsSetFsMplsTpGlobalConfigEntry *
                                       pOamIsSetFsMplsTpGlobalConfigEntry)
{
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpContextId == OSIX_TRUE)
    {
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpContextId ==
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpContextId)
            pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpContextId = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpOamModuleStatus ==
        OSIX_TRUE)
    {
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.
            i4FsMplsTpOamModuleStatus ==
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.
            i4FsMplsTpOamModuleStatus)
            pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpOamModuleStatus =
                OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpGlobalId == OSIX_TRUE)
    {
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId ==
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId)
            pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpGlobalId = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpIcc == OSIX_TRUE)
    {
        if ((MEMCMP (pOamFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc,
                     pOamSetFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc,
                     pOamSetFsMplsTpGlobalConfigEntry->MibObject.
                     i4FsMplsTpIccLen) == 0) &&
            (pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen ==
             pOamSetFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen))
            pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpIcc = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNodeIdentifier ==
        OSIX_TRUE)
    {
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpNodeIdentifier ==
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.
            u4FsMplsTpNodeIdentifier)
            pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNodeIdentifier =
                OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpTraceLevel == OSIX_TRUE)
    {
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpTraceLevel ==
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpTraceLevel)
            pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpTraceLevel =
                OSIX_FALSE;
    }

    if (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNotificationEnable ==
        OSIX_TRUE)
    {
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.
            i4FsMplsTpNotificationEnable ==
            pOamSetFsMplsTpGlobalConfigEntry->MibObject.
            i4FsMplsTpNotificationEnable)
            pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNotificationEnable =
                OSIX_FALSE;

    }

    if ((pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpContextId == OSIX_FALSE)
        && (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpContextId ==
            OSIX_FALSE)
        && (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpOamModuleStatus ==
            OSIX_FALSE)
        && (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpGlobalId == OSIX_FALSE)
        && (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpIcc == OSIX_FALSE)
        && (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNodeIdentifier ==
            OSIX_FALSE)
        && (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpTraceLevel ==
            OSIX_FALSE)
        && (pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNotificationEnable ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

PUBLIC INT4
OamFsMplsTpNodeMapEntryCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOamFsMplsTpNodeMapEntry,
                       MibObject.FsMplsTpNodeMapTableNode);

    if ((gOamGlobals.OamGlbMib.FsMplsTpNodeMapTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsMplsTpNodeMapTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PUBLIC INT4
FsMplsTpNodeMapTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tOamFsMplsTpNodeMapEntry *pFsMplsTpNodeMapEntry1 =
        (tOamFsMplsTpNodeMapEntry *) pRBElem1;
    tOamFsMplsTpNodeMapEntry *pFsMplsTpNodeMapEntry2 =
        (tOamFsMplsTpNodeMapEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMplsTpNodeMapEntry1->MibObject.u4FsMplsTpContextId >
        pFsMplsTpNodeMapEntry2->MibObject.u4FsMplsTpContextId)
    {
        return 1;
    }
    else if (pFsMplsTpNodeMapEntry1->MibObject.u4FsMplsTpContextId <
             pFsMplsTpNodeMapEntry2->MibObject.u4FsMplsTpContextId)
    {
        return -1;
    }

    if (pFsMplsTpNodeMapEntry1->MibObject.u4FsMplsTpNodeMapLocalNum >
        pFsMplsTpNodeMapEntry2->MibObject.u4FsMplsTpNodeMapLocalNum)
    {
        return 1;
    }
    else if (pFsMplsTpNodeMapEntry1->MibObject.u4FsMplsTpNodeMapLocalNum <
             pFsMplsTpNodeMapEntry2->MibObject.u4FsMplsTpNodeMapLocalNum)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  OamSetAllFsMplsTpNodeMapTableTrigger
 Input       :  The Indices
                pOamSetFsMplsTpNodeMapEntry
                pOamIsSetFsMplsTpNodeMapEntry
 Output      :  This Routine is used to send 
                MSR and RM indication
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
OamSetAllFsMplsTpNodeMapTableTrigger (tOamFsMplsTpNodeMapEntry *
                                      pOamSetFsMplsTpNodeMapEntry,
                                      tOamIsSetFsMplsTpNodeMapEntry *
                                      pOamIsSetFsMplsTpNodeMapEntry,
                                      INT4 i4SetOption)
{

    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapGlobalId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpNodeMapGlobalId, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pOamSetFsMplsTpNodeMapEntry->MibObject.
                      u4FsMplsTpContextId,
                      pOamSetFsMplsTpNodeMapEntry->MibObject.
                      u4FsMplsTpNodeMapLocalNum,
                      pOamSetFsMplsTpNodeMapEntry->MibObject.
                      u4FsMplsTpNodeMapGlobalId);
    }
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapNodeId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpNodeMapNodeId, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pOamSetFsMplsTpNodeMapEntry->MibObject.
                      u4FsMplsTpContextId,
                      pOamSetFsMplsTpNodeMapEntry->MibObject.
                      u4FsMplsTpNodeMapLocalNum,
                      pOamSetFsMplsTpNodeMapEntry->MibObject.
                      u4FsMplsTpNodeMapNodeId);
    }
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpNodeMapRowStatus, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 1, 2, i4SetOption, "%u %u %i",
                      pOamSetFsMplsTpNodeMapEntry->MibObject.
                      u4FsMplsTpContextId,
                      pOamSetFsMplsTpNodeMapEntry->MibObject.
                      u4FsMplsTpNodeMapLocalNum,
                      pOamSetFsMplsTpNodeMapEntry->MibObject.
                      i4FsMplsTpNodeMapRowStatus);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsMplsTpNodeMapTableFilterInputs
 Input       :  The Indices
                pOamFsMplsTpNodeMapEntry
                pOamSetFsMplsTpNodeMapEntry
                pOamIsSetFsMplsTpNodeMapEntry
 Output      :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMplsTpNodeMapTableFilterInputs (tOamFsMplsTpNodeMapEntry *
                                  pOamFsMplsTpNodeMapEntry,
                                  tOamFsMplsTpNodeMapEntry *
                                  pOamSetFsMplsTpNodeMapEntry,
                                  tOamIsSetFsMplsTpNodeMapEntry *
                                  pOamIsSetFsMplsTpNodeMapEntry)
{
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpContextId == OSIX_TRUE)
    {
        if (pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId ==
            pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId)
            pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpContextId = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapLocalNum == OSIX_TRUE)
    {
        if (pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapLocalNum ==
            pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapLocalNum)
            pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapLocalNum =
                OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapGlobalId == OSIX_TRUE)
    {
        if (pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapGlobalId ==
            pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapGlobalId)
            pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapGlobalId =
                OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapNodeId == OSIX_TRUE)
    {
        if (pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId ==
            pOamSetFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId)
            pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapNodeId = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus == OSIX_TRUE)
    {
        if (pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus ==
            pOamSetFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus)
            pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus =
                OSIX_FALSE;
    }

    if ((pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpContextId == OSIX_FALSE)
        && (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpContextId == OSIX_FALSE)
        && (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapLocalNum ==
            OSIX_FALSE)
        && (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapGlobalId ==
            OSIX_FALSE)
        && (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapNodeId == OSIX_FALSE)
        && (pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

PUBLIC INT4
OamFsMplsTpMegEntryCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOamFsMplsTpMegEntry, MibObject.FsMplsTpMegTableNode);

    if ((gOamGlobals.OamGlbMib.FsMplsTpMegTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsMplsTpMegTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PUBLIC INT4
FsMplsTpMegTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tOamFsMplsTpMegEntry *pFsMplsTpMegEntry1 =
        (tOamFsMplsTpMegEntry *) pRBElem1;
    tOamFsMplsTpMegEntry *pFsMplsTpMegEntry2 =
        (tOamFsMplsTpMegEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMplsTpMegEntry1->MibObject.u4FsMplsTpContextId >
        pFsMplsTpMegEntry2->MibObject.u4FsMplsTpContextId)
    {
        return 1;
    }
    else if (pFsMplsTpMegEntry1->MibObject.u4FsMplsTpContextId <
             pFsMplsTpMegEntry2->MibObject.u4FsMplsTpContextId)
    {
        return -1;
    }

    if (pFsMplsTpMegEntry1->MibObject.u4FsMplsTpMegIndex >
        pFsMplsTpMegEntry2->MibObject.u4FsMplsTpMegIndex)
    {
        return 1;
    }
    else if (pFsMplsTpMegEntry1->MibObject.u4FsMplsTpMegIndex <
             pFsMplsTpMegEntry2->MibObject.u4FsMplsTpMegIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  OamSetAllFsMplsTpMegTableTrigger
 Input       :  The Indices
                pOamSetFsMplsTpMegEntry
                pOamIsSetFsMplsTpMegEntry
 Output      :  This Routine is used to send 
                MSR and RM indication
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
OamSetAllFsMplsTpMegTableTrigger (tOamFsMplsTpMegEntry *
                                  pOamSetFsMplsTpMegEntry,
                                  tOamIsSetFsMplsTpMegEntry *
                                  pOamIsSetFsMplsTpMegEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMplsTpMegNameVal;
    UINT1               au1FsMplsTpMegNameVal[256];
    tSNMP_OCTET_STRING_TYPE FsMplsTpMegIdIccVal;
    UINT1               au1FsMplsTpMegIdIccVal[256];
    tSNMP_OCTET_STRING_TYPE FsMplsTpMegIdUmcVal;
    UINT1               au1FsMplsTpMegIdUmcVal[256];

    MEMSET (au1FsMplsTpMegNameVal, 0, sizeof (au1FsMplsTpMegNameVal));
    FsMplsTpMegNameVal.pu1_OctetList = au1FsMplsTpMegNameVal;
    FsMplsTpMegNameVal.i4_Length = 0;

    MEMSET (au1FsMplsTpMegIdIccVal, 0, sizeof (au1FsMplsTpMegIdIccVal));
    FsMplsTpMegIdIccVal.pu1_OctetList = au1FsMplsTpMegIdIccVal;
    FsMplsTpMegIdIccVal.i4_Length = 0;

    MEMSET (au1FsMplsTpMegIdUmcVal, 0, sizeof (au1FsMplsTpMegIdUmcVal));
    FsMplsTpMegIdUmcVal.pu1_OctetList = au1FsMplsTpMegIdUmcVal;
    FsMplsTpMegIdUmcVal.i4_Length = 0;

    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegName == OSIX_TRUE)
    {
        MEMCPY (FsMplsTpMegNameVal.pu1_OctetList,
                pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
                pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen);
        FsMplsTpMegNameVal.i4_Length =
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen;

        nmhSetCmnNew (FsMplsTpMegName, 13, OamMainTaskLock, OamMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %s",
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex,
                      &FsMplsTpMegNameVal);
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegOperatorType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMegOperatorType, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMegEntry->MibObject.
                      i4FsMplsTpMegOperatorType);
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdIcc == OSIX_TRUE)
    {
        MEMCPY (FsMplsTpMegIdIccVal.pu1_OctetList,
                pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
                pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen);
        FsMplsTpMegIdIccVal.i4_Length =
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen;

        nmhSetCmnNew (FsMplsTpMegIdIcc, 13, OamMainTaskLock, OamMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %s",
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex,
                      &FsMplsTpMegIdIccVal);
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdUmc == OSIX_TRUE)
    {
        MEMCPY (FsMplsTpMegIdUmcVal.pu1_OctetList,
                pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
                pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen);
        FsMplsTpMegIdUmcVal.i4_Length =
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen;

        nmhSetCmnNew (FsMplsTpMegIdUmc, 13, OamMainTaskLock, OamMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %s",
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex,
                      &FsMplsTpMegIdUmcVal);
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegServiceType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMegServiceType, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMegEntry->MibObject.
                      i4FsMplsTpMegServiceType);
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegMpLocation == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMegMpLocation, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMegEntry->MibObject.
                      i4FsMplsTpMegMpLocation);
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMegRowStatus, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 1, 2, i4SetOption, "%u %u %i",
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMegEntry->MibObject.
                      i4FsMplsTpMegRowStatus);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsMplsTpMegTableFilterInputs
 Input       :  The Indices
                pOamFsMplsTpMegEntry
                pOamSetFsMplsTpMegEntry
                pOamIsSetFsMplsTpMegEntry
 Output      :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMplsTpMegTableFilterInputs (tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry,
                              tOamFsMplsTpMegEntry * pOamSetFsMplsTpMegEntry,
                              tOamIsSetFsMplsTpMegEntry *
                              pOamIsSetFsMplsTpMegEntry)
{
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpContextId == OSIX_TRUE)
    {
        if (pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId ==
            pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId)
            pOamIsSetFsMplsTpMegEntry->bFsMplsTpContextId = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIndex == OSIX_TRUE)
    {
        if (pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex ==
            pOamSetFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex)
            pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIndex = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegName == OSIX_TRUE)
    {
        if ((MEMCMP (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
                     pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
                     pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen) ==
             0) && (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen ==
                    pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen))
            pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegName = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegOperatorType == OSIX_TRUE)
    {
        if (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegOperatorType ==
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegOperatorType)
            pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegOperatorType = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdIcc == OSIX_TRUE)
    {
        if ((MEMCMP (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
                     pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
                     pOamSetFsMplsTpMegEntry->MibObject.
                     i4FsMplsTpMegIdIccLen) == 0)
            && (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen ==
                pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen))
            pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdIcc = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdUmc == OSIX_TRUE)
    {
        if ((MEMCMP (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
                     pOamSetFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
                     pOamSetFsMplsTpMegEntry->MibObject.
                     i4FsMplsTpMegIdUmcLen) == 0)
            && (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen ==
                pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen))
            pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdUmc = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegServiceType == OSIX_TRUE)
    {
        if (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType ==
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType)
            pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegServiceType = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegMpLocation == OSIX_TRUE)
    {
        if (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation ==
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation)
            pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegMpLocation = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus == OSIX_TRUE)
    {
        if (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus ==
            pOamSetFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus)
            pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus = OSIX_FALSE;
    }

    if ((pOamIsSetFsMplsTpMegEntry->bFsMplsTpContextId == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMegEntry->bFsMplsTpContextId == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIndex == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegName == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegOperatorType == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdIcc == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdUmc == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegServiceType == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegMpLocation == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

PUBLIC INT4
OamFsMplsTpMeTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tOamFsMplsTpMeEntry, MibObject.FsMplsTpMeTableNode);

    if ((gOamGlobals.OamGlbMib.FsMplsTpMeTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsMplsTpMeTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

PUBLIC INT4
FsMplsTpMeTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tOamFsMplsTpMeEntry *pFsMplsTpMeEntry1 = (tOamFsMplsTpMeEntry *) pRBElem1;
    tOamFsMplsTpMeEntry *pFsMplsTpMeEntry2 = (tOamFsMplsTpMeEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsMplsTpMeEntry1->MibObject.u4FsMplsTpContextId >
        pFsMplsTpMeEntry2->MibObject.u4FsMplsTpContextId)
    {
        return 1;
    }
    else if (pFsMplsTpMeEntry1->MibObject.u4FsMplsTpContextId <
             pFsMplsTpMeEntry2->MibObject.u4FsMplsTpContextId)
    {
        return -1;
    }

    if (pFsMplsTpMeEntry1->MibObject.u4FsMplsTpMegIndex >
        pFsMplsTpMeEntry2->MibObject.u4FsMplsTpMegIndex)
    {
        return 1;
    }
    else if (pFsMplsTpMeEntry1->MibObject.u4FsMplsTpMegIndex <
             pFsMplsTpMeEntry2->MibObject.u4FsMplsTpMegIndex)
    {
        return -1;
    }

    if (pFsMplsTpMeEntry1->MibObject.u4FsMplsTpMeIndex >
        pFsMplsTpMeEntry2->MibObject.u4FsMplsTpMeIndex)
    {
        return 1;
    }
    else if (pFsMplsTpMeEntry1->MibObject.u4FsMplsTpMeIndex <
             pFsMplsTpMeEntry2->MibObject.u4FsMplsTpMeIndex)
    {
        return -1;
    }

    if (pFsMplsTpMeEntry1->MibObject.u4FsMplsTpMeMpIndex >
        pFsMplsTpMeEntry2->MibObject.u4FsMplsTpMeMpIndex)
    {
        return 1;
    }
    else if (pFsMplsTpMeEntry1->MibObject.u4FsMplsTpMeMpIndex <
             pFsMplsTpMeEntry2->MibObject.u4FsMplsTpMeMpIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  OamSetAllFsMplsTpMeTableTrigger
 Input       :  The Indices
                pOamSetFsMplsTpMeEntry
                pOamIsSetFsMplsTpMeEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
OamSetAllFsMplsTpMeTableTrigger (tOamFsMplsTpMeEntry * pOamSetFsMplsTpMeEntry,
                                 tOamIsSetFsMplsTpMeEntry *
                                 pOamIsSetFsMplsTpMeEntry, INT4 i4SetOption)
{
    tSNMP_OCTET_STRING_TYPE FsMplsTpMeNameVal;
    UINT1               au1FsMplsTpMeNameVal[256];
    tSNMP_OID_TYPE     *pFsMplsTpMeServicePointerVal = NULL;

    pFsMplsTpMeServicePointerVal = alloc_oid (MAX_OID_LEN);

    MEMSET (au1FsMplsTpMeNameVal, 0, sizeof (au1FsMplsTpMeNameVal));
    FsMplsTpMeNameVal.pu1_OctetList = au1FsMplsTpMeNameVal;
    FsMplsTpMeNameVal.i4_Length = 0;

    if (pFsMplsTpMeServicePointerVal == NULL)
    {
        return OSIX_FAILURE;
    }

    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeName == OSIX_TRUE)
    {
        MEMCPY (FsMplsTpMeNameVal.pu1_OctetList,
                pOamSetFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen);
        FsMplsTpMeNameVal.i4_Length =
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen;

        nmhSetCmnNew (FsMplsTpMeName, 13, OamMainTaskLock, OamMainTaskUnLock, 0,
                      0, 4, i4SetOption, "%u %u %u %u %s",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      &FsMplsTpMeNameVal);
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIfIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMeMpIfIndex, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %i",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex);
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSourceMepIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMeSourceMepIndex, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %u",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.
                      u4FsMplsTpMeSourceMepIndex);
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSinkMepIndex == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMeSinkMepIndex, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %u",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.
                      u4FsMplsTpMeSinkMepIndex);
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMeMpType, 13, OamMainTaskLock, OamMainTaskUnLock,
                      0, 0, 4, i4SetOption, "%u %u %u %u %i",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType);
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMepDirection == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMeMepDirection, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %i",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.
                      i4FsMplsTpMeMepDirection);
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeProactiveOamPhbTCValue ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMeProactiveOamPhbTCValue, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %i",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.
                      i4FsMplsTpMeProactiveOamPhbTCValue);
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeOnDemandOamPhbTCValue == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMeOnDemandOamPhbTCValue, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %i",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.
                      i4FsMplsTpMeOnDemandOamPhbTCValue);
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServiceSignaled == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMeServiceSignaled, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %i",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.
                      i4FsMplsTpMeServiceSignaled);
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer == OSIX_TRUE)
    {
        MEMCPY (pFsMplsTpMeServicePointerVal->pu4_OidList,
                pOamSetFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer,
                (pOamSetFsMplsTpMeEntry->MibObject.
                 i4FsMplsTpMeServicePointerLen * sizeof (UINT4)));
        pFsMplsTpMeServicePointerVal->u4_Length =
            (UINT4) pOamSetFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeServicePointerLen;

        nmhSetCmnNew (FsMplsTpMeServicePointer, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %o",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      pFsMplsTpMeServicePointerVal);
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsTpMeRowStatus, 13, OamMainTaskLock,
                      OamMainTaskUnLock, 0, 1, 4, i4SetOption, "%u %u %u %u %i",
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex,
                      pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus);
    }
    free_oid (pFsMplsTpMeServicePointerVal);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsMplsTpMeTableFilterInputs
 Input       :  The Indices
                pOamFsMplsTpMeEntry
                pOamSetFsMplsTpMeEntry
                pOamIsSetFsMplsTpMeEntry
 Output      :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMplsTpMeTableFilterInputs (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry,
                             tOamFsMplsTpMeEntry * pOamSetFsMplsTpMeEntry,
                             tOamIsSetFsMplsTpMeEntry *
                             pOamIsSetFsMplsTpMeEntry)
{
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpContextId == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId ==
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpContextId = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMegIndex == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex ==
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMegIndex = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeIndex == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex ==
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeIndex = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIndex == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex ==
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIndex = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeName == OSIX_TRUE)
    {
        if ((MEMCMP (pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                     pOamSetFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                     pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen) ==
             0)
            && (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen ==
                pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen))
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeName = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIfIndex == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex ==
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIfIndex = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSourceMepIndex == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex ==
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSourceMepIndex = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSinkMepIndex == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex ==
            pOamSetFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSinkMepIndex = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpType == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType ==
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpType = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMepDirection == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection ==
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMepDirection = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeProactiveOamPhbTCValue ==
        OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeProactiveOamPhbTCValue ==
            pOamSetFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeProactiveOamPhbTCValue)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeProactiveOamPhbTCValue =
                OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeOnDemandOamPhbTCValue == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeOnDemandOamPhbTCValue ==
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeOnDemandOamPhbTCValue)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeOnDemandOamPhbTCValue =
                OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServiceSignaled == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServiceSignaled ==
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServiceSignaled)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServiceSignaled = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer == OSIX_TRUE)
    {
        if ((MEMCMP (pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer,
                     pOamSetFsMplsTpMeEntry->MibObject.
                     au4FsMplsTpMeServicePointer,
                     pOamSetFsMplsTpMeEntry->MibObject.
                     i4FsMplsTpMeServicePointerLen * sizeof (UINT4)) == 0) &&
            (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen ==
             pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen))
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer = OSIX_FALSE;
    }
    if (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus == OSIX_TRUE)
    {
        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus ==
            pOamSetFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus)
            pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus = OSIX_FALSE;
    }

    if ((pOamIsSetFsMplsTpMeEntry->bFsMplsTpContextId == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpContextId == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMegIndex == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeIndex == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIndex == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeName == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIfIndex == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSourceMepIndex == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSinkMepIndex == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpType == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMepDirection == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeProactiveOamPhbTCValue ==
            OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeOnDemandOamPhbTCValue ==
            OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServiceSignaled == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer == OSIX_FALSE)
        && (pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

tOamFsMplsTpGlobalConfigEntry *
OamFsMplsTpGlobalConfigEntryCreateApi (tOamFsMplsTpGlobalConfigEntry *
                                       pSetOamFsMplsTpGlobalConfigEntry)
{
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;

    if (pSetOamFsMplsTpGlobalConfigEntry == NULL)
    {
        OAM_TRC
            ((OAM_UTIL_TRC,
              "OamFsMplsTpGlobalConfigEntryCreatApi: pSetOamFsMplsTpGlobalConfigEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pOamFsMplsTpGlobalConfigEntry =
        (tOamFsMplsTpGlobalConfigEntry *)
        MemAllocMemBlk (OAM_FSMPLSTPGLOBALCONFIGTABLE_POOLID);
    if (pOamFsMplsTpGlobalConfigEntry == NULL)
    {
        OAM_TRC
            ((OAM_UTIL_TRC,
              "OamFsMplsTpGlobalConfigEntryCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pOamFsMplsTpGlobalConfigEntry, pSetOamFsMplsTpGlobalConfigEntry,
                sizeof (tOamFsMplsTpGlobalConfigEntry));
        if (RBTreeAdd
            (gOamGlobals.OamGlbMib.FsMplsTpGlobalConfigTable,
             (tRBElem *) pOamFsMplsTpGlobalConfigEntry) != RB_SUCCESS)
        {
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamFsMplsTpGlobalConfigEntryCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (OAM_FSMPLSTPGLOBALCONFIGTABLE_POOLID,
                                (UINT1 *) pOamFsMplsTpGlobalConfigEntry);
            return NULL;
        }
        return pOamFsMplsTpGlobalConfigEntry;
    }
}
tOamFsMplsTpNodeMapEntry *
OamFsMplsTpNodeMapEntryCreateApi (tOamFsMplsTpNodeMapEntry *
                                  pSetOamFsMplsTpNodeMapEntry)
{
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;

    if (pSetOamFsMplsTpNodeMapEntry == NULL)
    {
        OAM_TRC
            ((OAM_UTIL_TRC,
              "OamFsMplsTpNodeMapEntryCreatApi: pSetOamFsMplsTpNodeMapEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pOamFsMplsTpNodeMapEntry =
        (tOamFsMplsTpNodeMapEntry *)
        MemAllocMemBlk (OAM_FSMPLSTPNODEMAPTABLE_POOLID);
    if (pOamFsMplsTpNodeMapEntry == NULL)
    {
        OAM_TRC
            ((OAM_UTIL_TRC,
              "OamFsMplsTpNodeMapEntryCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pOamFsMplsTpNodeMapEntry, pSetOamFsMplsTpNodeMapEntry,
                sizeof (tOamFsMplsTpNodeMapEntry));
        if (RBTreeAdd
            (gOamGlobals.OamGlbMib.FsMplsTpNodeMapTable,
             (tRBElem *) pOamFsMplsTpNodeMapEntry) != RB_SUCCESS)
        {
            OAM_TRC
                ((OAM_UTIL_TRC,
                  "OamFsMplsTpNodeMapEntryCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (OAM_FSMPLSTPNODEMAPTABLE_POOLID,
                                (UINT1 *) pOamFsMplsTpNodeMapEntry);
            return NULL;
        }
        return pOamFsMplsTpNodeMapEntry;
    }
}
tOamFsMplsTpMegEntry *
OamFsMplsTpMegEntryCreateApi (tOamFsMplsTpMegEntry * pSetOamFsMplsTpMegEntry)
{
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;

    if (pSetOamFsMplsTpMegEntry == NULL)
    {
        OAM_TRC
            ((OAM_UTIL_TRC,
              "OamFsMplsTpMegEntryCreatApi: pSetOamFsMplsTpMegEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pOamFsMplsTpMegEntry =
        (tOamFsMplsTpMegEntry *) MemAllocMemBlk (OAM_FSMPLSTPMEGTABLE_POOLID);
    if (pOamFsMplsTpMegEntry == NULL)
    {
        OAM_TRC ((OAM_UTIL_TRC,
                  "OamFsMplsTpMegEntryCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pOamFsMplsTpMegEntry, pSetOamFsMplsTpMegEntry,
                sizeof (tOamFsMplsTpMegEntry));
        if (RBTreeAdd
            (gOamGlobals.OamGlbMib.FsMplsTpMegTable,
             (tRBElem *) pOamFsMplsTpMegEntry) != RB_SUCCESS)
        {
            OAM_TRC ((OAM_UTIL_TRC,
                      "OamFsMplsTpMegEntryCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (OAM_FSMPLSTPMEGTABLE_POOLID,
                                (UINT1 *) pOamFsMplsTpMegEntry);
            return NULL;
        }
        return pOamFsMplsTpMegEntry;
    }
}
tOamFsMplsTpMeEntry *
OamFsMplsTpMeTableCreateApi (tOamFsMplsTpMeEntry * pSetOamFsMplsTpMeEntry)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    if (pSetOamFsMplsTpMeEntry == NULL)
    {
        OAM_TRC
            ((OAM_UTIL_TRC,
              "OamFsMplsTpMeTableCreatApi: pSetOamFsMplsTpMeEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pOamFsMplsTpMeEntry =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntry == NULL)
    {
        OAM_TRC ((OAM_UTIL_TRC,
                  "OamFsMplsTpMeTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pOamFsMplsTpMeEntry, pSetOamFsMplsTpMeEntry,
                sizeof (tOamFsMplsTpMeEntry));
        if (RBTreeAdd
            (gOamGlobals.OamGlbMib.FsMplsTpMeTable,
             (tRBElem *) pOamFsMplsTpMeEntry) != RB_SUCCESS)
        {
            OAM_TRC ((OAM_UTIL_TRC,
                      "OamFsMplsTpMeTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                (UINT1 *) pOamFsMplsTpMeEntry);
            return NULL;
        }
        return pOamFsMplsTpMeEntry;
    }
}
PUBLIC INT4
OamUtlCreateRBTree ()
{

    if (OamFsMplsTpGlobalConfigEntryCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (OamFsMplsTpNodeMapEntryCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (OamFsMplsTpMegEntryCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (OamFsMplsTpMeTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}
