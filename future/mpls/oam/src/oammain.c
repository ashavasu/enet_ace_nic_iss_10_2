/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: oammain.c,v 1.5 2011/10/25 09:29:43 siva Exp $
*
*
* Description: This file contains the oam task main loop
*              and the initialization routines.
*
*******************************************************************/
#define __OAMMAIN_C__
#include "oaminc.h"
#include "oamclip.h"
/* Proto types of the functions private to this file only */

PRIVATE UINT4 OamMainMemInit PROTO ((VOID));
PRIVATE VOID OamMainMemClear PROTO ((VOID));
extern UINT4        OamMainInit (VOID);
extern VOID         OamMainDeInit (VOID);
extern VOID         OamMainRegisterOamMibs (VOID);

#ifdef CLI_WANTED
extern tOamCliArgs  gaOamCliArgs[CLI_MAX_SESSIONS];
#endif

/****************************************************************************
*                                                                           *
* Function     : OamMainInit                                           *
*                                                                           *
* Description  : OAM initialization routine.                               *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/
PUBLIC UINT4
OamMainInit (VOID)
{
    OAM_TRC_FUNC ((OAM_FN_ENTRY, "FUNC:OamMainTaskInit\n"));

#ifdef OAM_TRACE_WANTED
    OAM_TRC_FLAG = ~((UINT4) 0);
#endif

    MEMSET (&gOamGlobals, 0, sizeof (gOamGlobals));

    if (OamUtlCreateRBTree () == OSIX_FAILURE)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% RB Tree Creation Failed.\r\n"));
        return OSIX_FAILURE;
    }

    if (OamFsMplsTpGlobalIdNodeIdTableCreate () == OSIX_FAILURE)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% GlobalId NodeId based RB Tree "
                  "Creation Failed.\r\n"));
        return OSIX_FAILURE;
    }
    if (OamFsMplsTpFsMplsTpMegNameTableCreate () == OSIX_FAILURE)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Meg name based RB Tree "
                  "Creation Failed.\r\n"));
        return OSIX_FAILURE;
    }

    if (OamFsMplsTpFsMplsTpMegIccIdTableCreate () == OSIX_FAILURE)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% ICC MEG Id based RB Tree "
                  "Creation Failed.\r\n"));
        return OSIX_FAILURE;
    }

    /* Create buffer pools for data structures */

    if (OamMainMemInit () == OSIX_FAILURE)
    {
        OAM_TRC ((OAM_MAIN_TRC, "Memory Pool Creation Failed\n"));
        return OSIX_FAILURE;
    }

    if (OamUtilCreateContext (OAM_DEFAULT_CXT_ID) == OSIX_FAILURE)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Default Context Creation Failed.\r\n"));
        return OSIX_FAILURE;
    }
#ifdef CLI_WANTED
    MEMSET (gaOamCliArgs, 0, (CLI_MAX_SESSIONS * sizeof (tOamCliArgs)));
#endif
    OAM_TRC_FUNC ((OAM_FN_EXIT, "FUNC:OamMainTaskInit\n"));
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OamMainDeInit                                             */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OamMainDeInit (VOID)
{
    OamUtlRBTreeDestroy ();
    OamMainMemClear ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OamMainMemInit                                            */
/*                                                                           */
/* Description  : Allocates all the memory that is required for OAM         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
OamMainMemInit (VOID)
{
    if (MplsoamSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : OamMainMemClear                                           */
/*                                                                           */
/* Description  : Clears all the Memory                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
OamMainMemClear (VOID)
{
    MplsoamSizingMemDeleteMemPools ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : OamMainRegisterOamMibs                                     */
/*                                                                           */
/* Description  : Registers the OAM mibs with SNMP agent                     */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
OamMainRegisterOamMibs (VOID)
{
    RegisterFSMPTP ();
    RegisterFSMTPO ();
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  oammain.c                     */
/*-----------------------------------------------------------------------*/
