/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: oamcli.c,v 1.14 2013/09/17 11:52:37 siva Exp $
*
* Description: This file contains the routines for the show commands in OAM CLI.
*********************************************************************/
#ifndef __OAMCLI_C__
#define __OAMCLI_C__

#include "oaminc.h"
#include "mplscli.h"
#include "oamclip.h"
#include "inmgrex.h"
#include "indexmgr.h"
extern INT4
    OamUtilGetLocalMapNum PROTO ((UINT4 u4ContextId, UINT4 u4GlobalId,
                                  UINT4 u4NodeId, UINT4 *pu4LocalMapNum));

tOamCliArgs         gaOamCliArgs[CLI_MAX_SESSIONS];
/****************************************************************************
* Function    :  cli_process_oam_show_cmd
* Description :  This function is exported to CLI module to handle the
*                OAM CLI show commands
* Input       :  CliHandle 
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_oam_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[OAM_CLI_MAX_ARGS];
    UINT4               u4ContextId = 0;
    UINT4               u4LocalMapNum = 0;
    UINT4               u4GlobalId = 0;
    UINT4               u4NodeId = 0;
    UINT4               u4DisplayMode = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4ArgsCount = 0;
    INT4                i4Inst = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT1              *pu1MegName = NULL;
    UINT1              *pu1MeName = NULL;
    INT1                i1ArgNo = 0;

    /* Check the Cli handle value with the allowed max session */
    if (CliHandle >= CLI_MAX_SESSIONS)
    {
        CliPrintf (CliHandle, "\r %%CLI max sessions reached\n");
        return CLI_FAILURE;
    }

    CliRegisterLock (CliHandle, OamMainTaskLock, OamMainTaskUnLock);
    OAM_LOCK;

    va_start (ap, u4Command);

    /* Third argument is always interface name/index */
    i4Inst = va_arg (ap, INT4);
    if (i4Inst != 0)
    {
        i4RetVal = i4Inst;
    }

    /* Walk through the rest of the arguements and store in args array.
     * Store OAM_CLI_MAX_ARGS arguements at the max. */

    MEMSET (args, 0, sizeof (args));

    while (1)
    {
        args[i1ArgNo++] = va_arg (ap, UINT4 *);
        if (i1ArgNo == OAM_CLI_MAX_ARGS)
        {
            break;
        }
    }
    va_end (ap);

    switch (u4Command)
    {
        case CLI_MPLS_SHOW_NODE_MAP_INFO_ALL:

            u4ContextId = CLI_PTR_TO_I4 (args[u4ArgsCount]);

            i4RetVal = OamCliShowNodeMapInfo (CliHandle, u4ContextId,
                                              u4LocalMapNum, u4GlobalId,
                                              u4NodeId, u4Command);
            break;

        case CLI_MPLS_SHOW_NODE_MAP_INFO_GID_NID:

            u4GlobalId = *(UINT4 *) args[u4ArgsCount];
            u4ArgsCount = u4ArgsCount + 1;
            u4NodeId = *(UINT4 *) args[u4ArgsCount];
            u4ArgsCount = u4ArgsCount + 1;
            u4ContextId = CLI_PTR_TO_I4 (args[u4ArgsCount]);

            i4RetVal = OamCliShowNodeMapInfo (CliHandle, u4ContextId,
                                              u4LocalMapNum, u4GlobalId,
                                              u4NodeId, u4Command);
            break;

        case CLI_MPLS_SHOW_NODE_MAP_INFO_LCL_MAP_NUM:

            u4LocalMapNum = *(UINT4 *) args[u4ArgsCount];
            u4ArgsCount = u4ArgsCount + 1;
            u4ContextId = CLI_PTR_TO_I4 (args[u4ArgsCount]);

            i4RetVal = OamCliShowNodeMapInfo (CliHandle, u4ContextId,
                                              u4LocalMapNum, u4GlobalId,
                                              u4NodeId, u4Command);
            break;

        case CLI_MPLS_OAM_SHOW_MEG_ALL:

            u4DisplayMode = CLI_PTR_TO_I4 (args[u4ArgsCount]);
            u4ArgsCount = u4ArgsCount + 1;
            u4ContextId = CLI_PTR_TO_I4 (args[u4ArgsCount]);

            i4RetVal = OamCliShowMegInfo (CliHandle, u4ContextId,
                                          NULL, u4Command, u4DisplayMode);
            break;

        case CLI_MPLS_OAM_SHOW_MEG_NAME:

            pu1MegName = (UINT1 *) args[u4ArgsCount];
            u4ArgsCount = u4ArgsCount + 1;
            u4DisplayMode = CLI_PTR_TO_I4 (args[u4ArgsCount]);
            u4ArgsCount = u4ArgsCount + 1;
            u4ContextId = CLI_PTR_TO_I4 (args[u4ArgsCount]);

            i4RetVal = OamCliShowMegInfo (CliHandle, u4ContextId,
                                          pu1MegName, u4Command, u4DisplayMode);
            break;

        case CLI_MPLS_OAM_SHOW_SERVICE_ALL:

            u4DisplayMode = CLI_PTR_TO_I4 (args[u4ArgsCount]);
            u4ArgsCount = u4ArgsCount + 1;
            u4ContextId = CLI_PTR_TO_I4 (args[u4ArgsCount]);

            i4RetVal = OamCliShowMeInfo (CliHandle, u4ContextId,
                                         pu1MegName, NULL,
                                         u4Command, u4DisplayMode);
            break;

        case CLI_MPLS_OAM_SHOW_MEG_NAME_SERVICE_NAME:

            pu1MegName = (UINT1 *) args[u4ArgsCount];
            u4ArgsCount = u4ArgsCount + 1;
            pu1MeName = (UINT1 *) args[u4ArgsCount];
            u4ArgsCount = u4ArgsCount + 1;
            u4DisplayMode = CLI_PTR_TO_I4 (args[u4ArgsCount]);
            u4ArgsCount = u4ArgsCount + 1;
            u4ContextId = CLI_PTR_TO_I4 (args[u4ArgsCount]);

            i4RetVal = OamCliShowMeInfo (CliHandle, u4ContextId,
                                         pu1MegName, pu1MeName,
                                         u4Command, u4DisplayMode);
            break;

        case CLI_MPLS_OAM_SHOW_MEG_NAME_ALL:

            pu1MegName = (UINT1 *) args[u4ArgsCount];
            u4ArgsCount = u4ArgsCount + 1;
            u4DisplayMode = CLI_PTR_TO_I4 (args[u4ArgsCount]);
            u4ArgsCount = u4ArgsCount + 1;
            u4ContextId = CLI_PTR_TO_I4 (args[u4ArgsCount]);

            i4RetVal = OamCliShowMeInfo (CliHandle, u4ContextId,
                                         pu1MegName, pu1MeName,
                                         u4Command, u4DisplayMode);
            break;

        case CLI_MPLS_SHOW_GLOBAL_INFO:

            u4ContextId = CLI_PTR_TO_I4 (args[u4ArgsCount]);

            i4RetVal = OamCliShowGlobalInfo (CliHandle, u4ContextId);
            break;

        default:
            CliPrintf (CliHandle, "\r%% Unknown Command\r\n");
            i4RetVal = CLI_FAILURE;
    }

    if ((i4RetVal == CLI_FAILURE) && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_MPLS_OAM) &&
            (u4ErrCode < CLI_OAM_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s",
                       OamCliErrString[CLI_ERR_OFFSET_MPLS_OAM (u4ErrCode)]);
        }

        CLI_SET_ERR (0);
    }

    CLI_SET_CMD_STATUS (i4RetVal);

    CliUnRegisterLock (CliHandle);
    OAM_UNLOCK;

    return i4RetVal;
}

/*****************************************************************************
 * Function Name : CliGetDebugLevel
 * Description   : This routine returns the debug trace value configured for 
 *                 the given context.
 * Input(s)      : CliHandle and Context Id
 * Output(s)     : Debug Trace value
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
CliGetDebugLevel (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 *pu4DebugLevel)
{
    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;

    UNUSED_PARAM (CliHandle);

    MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));

    OamFsMplsTpGlobalConfigEntry.MibObject.u4FsMplsTpContextId = u4ContextId;

    OAM_LOCK;

    pOamFsMplsTpGlobalConfigEntry = OamGetFsMplsTpGlobalConfigTable
        (&OamFsMplsTpGlobalConfigEntry);

    if (pOamFsMplsTpGlobalConfigEntry == NULL)
    {
        OAM_UNLOCK;
        return CLI_FAILURE;
    }

    *pu4DebugLevel =
        pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpTraceLevel;

    OAM_UNLOCK;
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : OamCliChangeMegMode
 * Description   : This routine changes the mode to MEG configuration mode
 * Input(s)      : CliHandle - Index of current CLI context
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 *****************************************************************************/
INT4
OamCliChangeMegMode (tCliHandle CliHandle)
{
    if (CliChangePath (MPLS_MEG_MODE) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "/r%% Unable to enter into MEG configuration mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : MplsGetMplsOamMegPrompt
 * Description   : This routine returns the MEG mode prompt
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
MplsGetMplsOamMegPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_MEG_MODE);

    /* Check proper mode name is set or not b4 setting the prompt str */
    if (STRNCMP (pi1ModeName, MPLS_MEG_MODE, u4Len) != 0)
    {
        return FALSE;
    }

    STRNCPY (pi1DispStr, "(config-meg)#", (MAX_PROMPT_LEN - 1));
    pi1DispStr[MAX_PROMPT_LEN - 1] = '\0';

    return TRUE;
}

/*****************************************************************************
 * Function Name : OamCliGetFreeMegId
 * Description   : This routine gets the free Meg identifier from the 
 *                 index manager
 * Input(s)      : None
 * Output(s)     : None
 * Return(s)     : u4MegIndex - Next available Meg Index
 *****************************************************************************/
UINT4
OamCliGetFreeMegId (VOID)
{
    return MplsOamGetMegId (MPLS_OAM_MEG_GRPID);
}

/*****************************************************************************
 * Function Name : OamCliGetFreeMeId
 * Description   : This routine gets the free ME index 
 * Input(s)      : None
 * Output(s)     : None
 * Return(s)     : u4MeIndex - Next available ME Index
 *****************************************************************************/
UINT4
OamCliGetFreeMeId (UINT4 u4ContextId, UINT4 u4MegIndex)
{
    return OamUtilGetFreeMeId (u4ContextId, u4MegIndex);
}

/*****************************************************************************
 * Function Name : OamCliGetFreeMpId
 * Description   : This routine gets the free MP index 
 * Input(s)      : None
 * Output(s)     : None
 * Return(s)     : u4MegIndex - Next available MP Index
 *****************************************************************************/
UINT4
OamCliGetFreeMpId (UINT4 u4ContextId, UINT4 u4MegIndex, UINT4 u4MeIndex)
{
    UNUSED_PARAM (u4ContextId);
    UNUSED_PARAM (u4MegIndex);
    UNUSED_PARAM (u4MeIndex);

    return OAM_MP_INDEX_MIN;
}

/*****************************************************************************
 * Function Name : OamCliSetMegIndex
 * Description   : This routine sets the Meg index in the gaOamCliArgs
 * Input(s)      : Context Id and Meg Index
 * Output(s)     : None
 * Return(s)     : None
 *****************************************************************************/
VOID
OamCliSetMegIndex (tCliHandle CliHandle, UINT4 u4ContextId, UINT4 u4MegIndex)
{
    /* Check the Cli handle value with the allowed max session */
    if (CliHandle >= CLI_MAX_SESSIONS)
    {
        return;
    }

    gaOamCliArgs[CliHandle].u4ContextId = u4ContextId;
    gaOamCliArgs[CliHandle].u4MegIndex = u4MegIndex;
}

/*****************************************************************************
 * Function Name : OamCliGetMegIndex
 * Description   : This routine gets the Meg index from the gaOamCliArgs
 * Input(s)      : CliHandle
 * Output(s)     : Context Id and Meg Index
 * Return(s)     : None
 *****************************************************************************/
VOID
OamCliGetMegIndex (tCliHandle CliHandle, UINT4 *pu4ContextId,
                   UINT4 *pu4MegIndex)
{
    /* Check the Cli handle value with the allowed max session */
    if (CliHandle >= CLI_MAX_SESSIONS)
    {
        return;
    }

    *pu4ContextId = gaOamCliArgs[CliHandle].u4ContextId;
    *pu4MegIndex = gaOamCliArgs[CliHandle].u4MegIndex;
}

/****************************************************************************
* Function    : OamCliGetPwIndexFromVcId
* Description : This function returns the PW index from the given VC-Id
* Input       : VC Id
* Output      : Pw Index
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
OamCliGetPwIndexFromVcId (UINT4 u4VcId, UINT4 *pu4PwIndex)
{
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;

    pInMplsApiInfo = (tMplsApiInInfo *) MemAllocMemBlk (OAM_APIININFO_POOLID);
    if (pInMplsApiInfo == NULL)
    {
        return OSIX_FAILURE;
    }
    pOutMplsApiInfo =
        (tMplsApiOutInfo *) MemAllocMemBlk (OAM_APIOUTINFO_POOLID);
    if (pOutMplsApiInfo == NULL)
    {
        MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
        return OSIX_FAILURE;
    }

    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

    pInMplsApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_VCID;
    pInMplsApiInfo->InPathId.PwId.u4VcId = u4VcId;

    if (L2VpnApiHandleExternalRequest (MPLS_GET_PW_INFO, pInMplsApiInfo,
                                       pOutMplsApiInfo) == OSIX_SUCCESS)
    {
        *pu4PwIndex = pOutMplsApiInfo->OutPwInfo.MplsPwPathId.u4PwIndex;
        MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
        MemReleaseMemBlock (OAM_APIOUTINFO_POOLID, (UINT1 *) pOutMplsApiInfo);
        return OSIX_SUCCESS;
    }

    MemReleaseMemBlock (OAM_APIININFO_POOLID, (UINT1 *) pInMplsApiInfo);
    MemReleaseMemBlock (OAM_APIOUTINFO_POOLID, (UINT1 *) pOutMplsApiInfo);
    return OSIX_FAILURE;
}

/*****************************************************************************
 * Function Name : OamCliGetCxtIdFromCxtName
 * Description   : This routine gets the Context Id from the Vrf Name
 * Input(s)      : CliHandle - CLI handle, pu1CxtName - Context Name
 * Output(s)     : pu4ContextId - Context ID
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
INT4
OamCliGetCxtIdFromCxtName (tCliHandle CliHandle, UINT1 *pu1CxtName,
                           UINT4 *pu4ContextId)
{
    /* Call the vcm module to get the context id
     * with respect to the vrf name */
    if (MplsPortIsVcmSwitchExist (pu1CxtName, pu4ContextId) != OSIX_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Invalid VRF\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : OamCliShowGlobalInfo
 * DESCRIPTION      : This function displays the context related information
 * INPUT            : u4ContextId - Context Identifier
 * OUTPUT           : None
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **************************************************************************/
INT4
OamCliShowGlobalInfo (tCliHandle CliHandle, UINT4 u4ContextId)
{

    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));
    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);

    OamFsMplsTpGlobalConfigEntry.MibObject.u4FsMplsTpContextId = u4ContextId;

    pOamFsMplsTpGlobalConfigEntry = OamGetFsMplsTpGlobalConfigTable
        (&OamFsMplsTpGlobalConfigEntry);

    if (pOamFsMplsTpGlobalConfigEntry == NULL)
    {
        return CLI_FAILURE;
    }

    if (MplsPortVcmGetAliasName (u4ContextId, au1ContextName) == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r\n   %-25s:", "Context Name");
    CliPrintf (CliHandle, "%s \r\n", au1ContextName);
    CliPrintf (CliHandle, "   %-25s:", "MPLS-TP Global ID");
    if (pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId == 0)
    {
        CliPrintf (CliHandle, "Not Configured \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "%u \r\n",
                   pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId);
    }

    CliPrintf (CliHandle, "   %-25s:", "MPLS-TP ICC ID");

    if (STRLEN (pOamFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc) != 0)
    {
        CliPrintf (CliHandle, "%s \r\n",
                   pOamFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc);
    }
    else
    {
        CliPrintf (CliHandle, "Not Configured \r\n");
    }

    CliPrintf (CliHandle, "   %-25s:", "MPLS-TP Node ID");
    if (pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpNodeIdentifier == 0)
    {
        CliPrintf (CliHandle, "Not Configured \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "%u \r\n",
                   pOamFsMplsTpGlobalConfigEntry->MibObject.
                   u4FsMplsTpNodeIdentifier);
    }

    CliPrintf (CliHandle, "   %-25s:", "MPLS-TP Error Code");

    if (pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpErrorCode == 0)
    {
        CliPrintf (CliHandle, "None \r\n");
    }
    else
    {

        if (pOamFsMplsTpGlobalConfigEntry->MibObject.
            i4FsMplsTpErrorCode & OAM_ELPS_ASSOCIATION_EXISTS)
        {
            CliPrintf (CliHandle, "Elps Association Exists \r\n");
        }
        else if (pOamFsMplsTpGlobalConfigEntry->MibObject.
                 i4FsMplsTpErrorCode & OAM_MEG_ASSOCIATION_EXISTS)
        {
            CliPrintf (CliHandle, "Meg Association Exists \r\n");
        }
        else if (pOamFsMplsTpGlobalConfigEntry->MibObject.
                 i4FsMplsTpErrorCode & OAM_PW_ASSOCIATION_EXISTS)
        {
            CliPrintf (CliHandle, "Pseudowire Association Exists \r\n");
        }
        else if (pOamFsMplsTpGlobalConfigEntry->MibObject.
                 i4FsMplsTpErrorCode & OAM_PROACTIVE_SESSION_EXISTS)
        {
            CliPrintf (CliHandle, "Proactive session Exists \r\n");
        }
        else if (pOamFsMplsTpGlobalConfigEntry->MibObject.
                 i4FsMplsTpErrorCode & OAM_ELPS_PROACTIVE_SESSION_EXIST)
        {
            CliPrintf (CliHandle, "Elps Proactive session Exists \r\n");
        }
        else if (pOamFsMplsTpGlobalConfigEntry->MibObject.
                 i4FsMplsTpErrorCode & OAM_ACTIVE_ME_EXIST)
        {
            CliPrintf (CliHandle, "Active ME Exists \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Invalid Error Code \r\n");
        }
    }

    CliPrintf (CliHandle, "   %-25s:", "MPLS OAM Module Status");
    if (pOamFsMplsTpGlobalConfigEntry->MibObject.
        i4FsMplsTpOamModuleStatus == OAM_MODULE_STATUS_ENABLED)
    {
        CliPrintf (CliHandle, "Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Disabled \r\n");
    }

    CliPrintf (CliHandle, "   %-25s:", "MPLS Trace Level");

    if (pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpTraceLevel == 0)
    {
        CliPrintf (CliHandle, "Not configured \r\n");
    }
    else if (pOamFsMplsTpGlobalConfigEntry->MibObject.
             u4FsMplsTpTraceLevel == (CLI_OAM_ALL_TRC))
    {
        CliPrintf (CliHandle, "All Traces \r\n");
    }

    else
    {
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.
            u4FsMplsTpTraceLevel & CLI_OAM_FN_ENTRY_EXIT_TRC)
        {
            CliPrintf (CliHandle, "%s", "Function entry-exit \r\n");
        }
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.
            u4FsMplsTpTraceLevel & CLI_OAM_MGMT_TRC)
        {
            CliPrintf (CliHandle, "%s", "Management \r\n");
        }
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.
            u4FsMplsTpTraceLevel & CLI_OAM_MAIN_TRC)
        {
            CliPrintf (CliHandle, "%s", "Main function \r\n");
        }

        if (pOamFsMplsTpGlobalConfigEntry->MibObject.
            u4FsMplsTpTraceLevel & CLI_OAM_UTIL_TRC)
        {
            CliPrintf (CliHandle, "%s", "Util function \r\n");
        }

        if (pOamFsMplsTpGlobalConfigEntry->MibObject.
            u4FsMplsTpTraceLevel & CLI_OAM_RESOURCE_TRC)
        {
            CliPrintf (CliHandle, "%s", "Resource allocation \r\n");
        }
        if (pOamFsMplsTpGlobalConfigEntry->MibObject.
            u4FsMplsTpTraceLevel & CLI_OAM_ALL_FAIL_TRC)
        {
            CliPrintf (CliHandle, "%s", "All failure traces \r\n");
        }
    }

    CliPrintf (CliHandle, "   %-25s:", "MPLS OAM Notification");

    if (pOamFsMplsTpGlobalConfigEntry->MibObject.
        i4FsMplsTpNotificationEnable == MPLS_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "Enabled \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "Disabled \r\n");
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : OamCliShowNodeMapInfo
 * DESCRIPTION      : This function displays the Node Map Table information
 *                    based on the command given.
 * INPUT            : Context Id, Local Map Number, Global Id, Node Id
 *                    and u4Command.
 * OUTPUT           : None
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **************************************************************************/
INT4
OamCliShowNodeMapInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                       UINT4 u4LocalMapNum, UINT4 u4GlobalId,
                       UINT4 u4NodeId, UINT4 u4Command)
{
    tOamFsMplsTpNodeMapEntry OamFsMplsTpNodeMapEntry;
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapNext = NULL;
    UINT1               u1PrintHeader = TRUE;

    MEMSET (&OamFsMplsTpNodeMapEntry, 0, sizeof (tOamFsMplsTpNodeMapEntry));

    OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpContextId = u4ContextId;

    switch (u4Command)
    {
        case CLI_MPLS_SHOW_NODE_MAP_INFO_LCL_MAP_NUM:

            OamFsMplsTpNodeMapEntry.MibObject.
                u4FsMplsTpNodeMapLocalNum = u4LocalMapNum;

            pOamFsMplsTpNodeMapEntry = OamGetFsMplsTpNodeMapTable
                (&OamFsMplsTpNodeMapEntry);

            if (pOamFsMplsTpNodeMapEntry != NULL)
            {
                OamCliPrintNodeMapInfo
                    (CliHandle, pOamFsMplsTpNodeMapEntry, u1PrintHeader);
            }
            break;

        case CLI_MPLS_SHOW_NODE_MAP_INFO_ALL:

            pOamFsMplsTpNodeMapEntry = OamGetFirstFsMplsTpNodeMapTable ();

            while (pOamFsMplsTpNodeMapEntry != NULL)
            {
                OamCliPrintNodeMapInfo
                    (CliHandle, pOamFsMplsTpNodeMapEntry, u1PrintHeader);

                u1PrintHeader = FALSE;
                pOamFsMplsTpNodeMapNext = OamGetNextFsMplsTpNodeMapTable
                    (pOamFsMplsTpNodeMapEntry);

                pOamFsMplsTpNodeMapEntry = pOamFsMplsTpNodeMapNext;
            }
            break;

        case CLI_MPLS_SHOW_NODE_MAP_INFO_GID_NID:

            if (OamUtilGetLocalMapNum (u4ContextId, u4GlobalId, u4NodeId,
                                       &u4LocalMapNum) == OSIX_FAILURE)
            {
                return CLI_FAILURE;
            }

            OamFsMplsTpNodeMapEntry.MibObject.
                u4FsMplsTpNodeMapLocalNum = u4LocalMapNum;

            pOamFsMplsTpNodeMapEntry = OamGetFsMplsTpNodeMapTable
                (&OamFsMplsTpNodeMapEntry);
            if (pOamFsMplsTpNodeMapEntry != NULL)
            {
                OamCliPrintNodeMapInfo
                    (CliHandle, pOamFsMplsTpNodeMapEntry, u1PrintHeader);
            }
            break;

        default:
            break;
    }
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : OamCliPrintNodeMapInfo
 * DESCRIPTION      : This function Prints the Node Map information
 * INPUT            : Cli handle, Pointer to the Node Map entry 
 *                    and print header flag
 * OUTPUT           : None
 * RETURNS          : None
 **************************************************************************/
VOID
OamCliPrintNodeMapInfo (tCliHandle CliHandle,
                        tOamFsMplsTpNodeMapEntry * pOamFsMplsTpNodeMapEntry,
                        UINT1 u1PrintHeader)
{
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    if (u1PrintHeader)
    {
        CliPrintf (CliHandle, "\r\n%-20s", "Context Name");
        CliPrintf (CliHandle, "%-20s", "LocalMap Number");
        CliPrintf (CliHandle, "%-20s", "GlobalId");
        CliPrintf (CliHandle, "%-20s\r\n", "NodeId");
        CliPrintf (CliHandle, "%-20s", "------------");
        CliPrintf (CliHandle, "%-20s", "---------------");
        CliPrintf (CliHandle, "%-20s", "---------");
        CliPrintf (CliHandle, "%-20s\r\n", "-------");
    }
    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);

    if (MplsPortVcmGetAliasName
        (pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId,
         au1ContextName) == OSIX_FAILURE)
    {
        return;
    }

    CliPrintf (CliHandle, "%-20s", au1ContextName);

    CliPrintf (CliHandle, "%-20u", pOamFsMplsTpNodeMapEntry->MibObject.
               u4FsMplsTpNodeMapLocalNum);

    CliPrintf (CliHandle, "%-20u", pOamFsMplsTpNodeMapEntry->MibObject.
               u4FsMplsTpNodeMapGlobalId);

    CliPrintf (CliHandle, "%-20u\r\n", pOamFsMplsTpNodeMapEntry->MibObject.
               u4FsMplsTpNodeMapNodeId);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : OamCliShowMegInfo
 * DESCRIPTION      : This function displays the MEG related information
 * INPUT            : Context Id, Meg name, Command and Display mode 
 * OUTPUT           : None
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **************************************************************************/
INT4
OamCliShowMegInfo (tCliHandle CliHandle, UINT4 u4ContextId,
                   UINT1 *pu1MegName, UINT4 u4Command, UINT4 u4DisplayMode)
{
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    UINT4               u4MegIndex = 0;
    INT4                i4RetVal = 0;
    UINT1               u1PrintHeader = TRUE;

    MEMSET (&OamFsMplsTpMegEntry, 0, sizeof (tOamFsMplsTpMegEntry));

    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId = u4ContextId;

    switch (u4Command)
    {
        case CLI_MPLS_OAM_SHOW_MEG_ALL:

            pOamFsMplsTpMegEntry = OamGetFirstFsMplsTpMegTable ();

            while (pOamFsMplsTpMegEntry != NULL)
            {
                if (u4DisplayMode == CLI_MPLS_DISPLAY_DETAIL)
                {
                    CliPrintf (CliHandle, "\r\n");
                }

                i4RetVal = OamCliPrintMegInfo
                    (CliHandle, pOamFsMplsTpMegEntry, u4DisplayMode,
                     u1PrintHeader);
                if (i4RetVal != CLI_SUCCESS)
                {
                    return CLI_FAILURE;
                }
                u1PrintHeader = FALSE;
                pOamFsMplsTpMegEntry = OamGetNextFsMplsTpMegTable
                    (pOamFsMplsTpMegEntry);
            }
            break;

        case CLI_MPLS_OAM_SHOW_MEG_NAME:

            if (OamUtilGetMegIndexFromMegName (u4ContextId, pu1MegName,
                                               &u4MegIndex) == OSIX_FAILURE)
            {
                return CLI_FAILURE;
            }

            OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId = u4ContextId;
            OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex = u4MegIndex;

            pOamFsMplsTpMegEntry = OamGetFsMplsTpMegTable
                (&OamFsMplsTpMegEntry);

            if (pOamFsMplsTpMegEntry == NULL)
            {
                return CLI_FAILURE;
            }

            i4RetVal = OamCliPrintMegInfo
                (CliHandle, pOamFsMplsTpMegEntry, u4DisplayMode, u1PrintHeader);
            if (i4RetVal != CLI_SUCCESS)
            {
                return CLI_FAILURE;
            }
            break;

        default:
            break;
    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : OamCliPrintMegInfo
 * DESCRIPTION      : This function Prints the MEG information
 * INPUT            : Pointer to the Meg structure and Display mode 
 * OUTPUT           : None
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **************************************************************************/
INT4
OamCliPrintMegInfo (tCliHandle CliHandle,
                    tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry,
                    UINT4 u4DisplayMode, UINT1 u1PrintHeader)
{
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];
    UINT1               au1IccId[OAM_ICC_ID_LEN + 1];
    UINT1               au1UmcId[OAM_UMC_ID_LEN + 1];
    UINT1               au1MegName[OAM_MEG_NAME_MAX_LEN + 1];

    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);
    MEMSET (au1IccId, 0, (OAM_ICC_ID_LEN + 1));
    MEMSET (au1UmcId, 0, (OAM_UMC_ID_LEN + 1));
    MEMSET (au1MegName, 0, (OAM_MEG_NAME_MAX_LEN + 1));

    if (MplsPortVcmGetAliasName
        (pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId,
         au1ContextName) == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }

    MEMCPY (au1MegName, pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
            pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen);

    if (u4DisplayMode != CLI_MPLS_DISPLAY_DETAIL)
    {
        if (TRUE == u1PrintHeader)
        {
            CliPrintf (CliHandle, "\r\n%-15s", " Context Name");
            CliPrintf (CliHandle, "%-15s", " MEG Index");
            CliPrintf (CliHandle, "%-15s", " MEG Name");
            CliPrintf (CliHandle, "%-15s\r\n", " Service Type");
            CliPrintf (CliHandle, "%-15s", "------------");
            CliPrintf (CliHandle, "%-15s", "---------");
            CliPrintf (CliHandle, "%-15s", "--------");
            CliPrintf (CliHandle, "%-15s\r\n", "------------");
        }
        CliPrintf (CliHandle, "%-15s", au1ContextName);

        CliPrintf (CliHandle, "%-15u", pOamFsMplsTpMegEntry->MibObject.
                   u4FsMplsTpMegIndex);

        CliPrintf (CliHandle, "%-15s", au1MegName);

        if (pOamFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP)
        {
            CliPrintf (CliHandle, "%-6s", "TP-LSP\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "%-6s", "TP-PW\r\n");
        }

    }
    else
    {

        CliPrintf (CliHandle, "   %-15s:", "Context Name");
        CliPrintf (CliHandle, "%s \r\n", au1ContextName);

        CliPrintf (CliHandle, "   %-15s:", "MEG Index");
        CliPrintf (CliHandle, "%u \r\n", pOamFsMplsTpMegEntry->MibObject.
                   u4FsMplsTpMegIndex);

        CliPrintf (CliHandle, "   %-15s:", "MEG Name");
        CliPrintf (CliHandle, "%s \r\n", au1MegName);

        CliPrintf (CliHandle, "   %-15s:", "Operator Type");
        if (pOamFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegOperatorType == OAM_OPERATOR_TYPE_IP)
        {
            CliPrintf (CliHandle, "IP Compatible \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "ICC Based \r\n");
        }

        CliPrintf (CliHandle, "   %-15s:", "Service Type");
        if (pOamFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP)
        {
            CliPrintf (CliHandle, "MPLS-TP LSP \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "MPLS-TP Pseudowire \r\n");
        }

        if (pOamFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegOperatorType == OAM_OPERATOR_TYPE_ICC)
        {
            MEMCPY (au1IccId,
                    pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
                    pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen);

            MEMCPY (au1UmcId,
                    pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
                    pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen);

            CliPrintf (CliHandle, "   %-15s:", "ICC Id");
            CliPrintf (CliHandle, "%s \r\n", au1IccId);

            CliPrintf (CliHandle, "   %-15s:", "UMC Id");
            CliPrintf (CliHandle, "%s \r\n", au1UmcId);
        }

        CliPrintf (CliHandle, "   %-15s:", "MP Location");
        if (pOamFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegMpLocation == OAM_MEG_MP_LOCATION_PER_NODE)
        {
            CliPrintf (CliHandle, "Per-Node \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Per-Interface \r\n\n");
        }

    }

    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : OamCliShowMeInfo
 * DESCRIPTION      : This function displays the ME information
 * INPUT            : Context Id, Meg name, Me name, Command and 
 *                    Display Mode
 * OUTPUT           : None
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **************************************************************************/
INT4
OamCliShowMeInfo (tCliHandle CliHandle, UINT4 u4ContextId, UINT1 *pu1MegName,
                  UINT1 *pu1MeName, UINT4 u4Command, UINT4 u4DisplayMode)
{

    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryIn = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeNext = NULL;
    UINT4               u4MegIndex = 0;

    pOamFsMplsTpMeEntryIn =
        (tOamFsMplsTpMeEntry *) MemAllocMemBlk (OAM_FSMPLSTPMETABLE_POOLID);
    if (pOamFsMplsTpMeEntryIn == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (pOamFsMplsTpMeEntryIn, 0, sizeof (tOamFsMplsTpMeEntry));

    pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpContextId = u4ContextId;

    switch (u4Command)
    {
        case CLI_MPLS_OAM_SHOW_SERVICE_ALL:

            pOamFsMplsTpMeEntry =
                OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntryIn);

            while (pOamFsMplsTpMeEntry != NULL)
            {
                OamCliPrintMeInfo (CliHandle, pOamFsMplsTpMeEntry,
                                   u4DisplayMode);
                pOamFsMplsTpMeNext =
                    OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntry);

                pOamFsMplsTpMeEntry = pOamFsMplsTpMeNext;
            }
            break;

        case CLI_MPLS_OAM_SHOW_MEG_NAME_SERVICE_NAME:

            pOamFsMplsTpMeEntry = OamUtilGetMeEntryFromMegName
                (u4ContextId, pu1MegName, pu1MeName);

            if (pOamFsMplsTpMeEntry != NULL)
            {
                OamCliPrintMeInfo (CliHandle, pOamFsMplsTpMeEntry,
                                   u4DisplayMode);
            }
            break;

        case CLI_MPLS_OAM_SHOW_MEG_NAME_ALL:

            if (OamUtilGetMegIndexFromMegName (u4ContextId, pu1MegName,
                                               &u4MegIndex) == OSIX_FAILURE)
            {
                MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                                    (UINT1 *) pOamFsMplsTpMeEntryIn);
                return CLI_FAILURE;
            }

            pOamFsMplsTpMeEntryIn->MibObject.u4FsMplsTpMegIndex = u4MegIndex;

            pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable
                (pOamFsMplsTpMeEntryIn);

            while (pOamFsMplsTpMeEntry != NULL)
            {
                OamCliPrintMeInfo (CliHandle, pOamFsMplsTpMeEntry,
                                   u4DisplayMode);
                pOamFsMplsTpMeNext =
                    OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntry);

                pOamFsMplsTpMeEntry = pOamFsMplsTpMeNext;
            }
            break;

        default:
            break;
    }

    MemReleaseMemBlock (OAM_FSMPLSTPMETABLE_POOLID,
                        (UINT1 *) pOamFsMplsTpMeEntryIn);
    return CLI_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : OamCliPrintMeInfo
 * DESCRIPTION      : This function Prints the ME information
 * INPUT            : Cli handle, Pointer to the ME table and display mode 
 * OUTPUT           : None
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **************************************************************************/
VOID
OamCliPrintMeInfo (tCliHandle CliHandle,
                   tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry,
                   UINT4 u4DisplayMode)
{
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);

    if (MplsPortVcmGetAliasName
        (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId,
         au1ContextName) == OSIX_FAILURE)
    {
        return;
    }

    CliPrintf (CliHandle, "\r\n");
    CliPrintf (CliHandle, "  %-30s:", "Context Name");
    CliPrintf (CliHandle, "%s \r\n", au1ContextName);

    CliPrintf (CliHandle, "  %-30s:", "MEG Index");
    CliPrintf (CliHandle, "%u \r\n", pOamFsMplsTpMeEntry->MibObject.
               u4FsMplsTpMegIndex);

    CliPrintf (CliHandle, "  %-30s:", "ME Index");
    CliPrintf (CliHandle, "%u \r\n", pOamFsMplsTpMeEntry->MibObject.
               u4FsMplsTpMeIndex);

    CliPrintf (CliHandle, "  %-30s:", "MP Index");
    CliPrintf (CliHandle, "%u \r\n", pOamFsMplsTpMeEntry->MibObject.
               u4FsMplsTpMeMpIndex);

    CliPrintf (CliHandle, "  %-30s:", "ME Name");
    CliPrintf (CliHandle, "%s \r\n", pOamFsMplsTpMeEntry->MibObject.
               au1FsMplsTpMeName);

    CliPrintf (CliHandle, "  %-30s:", "Source Mep Index");
    CliPrintf (CliHandle, "%u \r\n", pOamFsMplsTpMeEntry->MibObject.
               u4FsMplsTpMeSourceMepIndex);

    CliPrintf (CliHandle, "  %-30s:", "Sink Mep Index");
    CliPrintf (CliHandle, "%u \r\n", pOamFsMplsTpMeEntry->MibObject.
               u4FsMplsTpMeSinkMepIndex);

    CliPrintf (CliHandle, "  %-30s:", "MP Type");
    if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType == OAM_ME_MP_TYPE_MEP)
    {
        CliPrintf (CliHandle, "MEP \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "MIP \r\n");
    }

    if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType == OAM_ME_MP_TYPE_MEP)
    {
        CliPrintf (CliHandle, "  %-30s:", "MEP Direction");
        if (pOamFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeMepDirection == OAM_MEP_DIRECTION_DOWN)
        {
            CliPrintf (CliHandle, "DOWN \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "UP \r\n");
        }
    }

    if (u4DisplayMode != CLI_MPLS_DISPLAY_DETAIL)
    {
        CliPrintf (CliHandle, "\r\n");
    }
    else
    {
        if (pOamFsMplsTpMeEntry->MibObject.
            u4FsMplsTpMeProactiveOamSessIndex != 0)
        {
            CliPrintf (CliHandle, "  %-30s:", "Proactive Session Index");
            CliPrintf (CliHandle, "%u \r\n", pOamFsMplsTpMeEntry->MibObject.
                       u4FsMplsTpMeProactiveOamSessIndex);
        }

        CliPrintf (CliHandle, "  %-30s:", "Proactive PHB Value");
        CliPrintf (CliHandle, "%d \r\n", pOamFsMplsTpMeEntry->MibObject.
                   i4FsMplsTpMeProactiveOamPhbTCValue);

        CliPrintf (CliHandle, "  %-30s:", "On-demand PHB Value");
        CliPrintf (CliHandle, "%d \r\n", pOamFsMplsTpMeEntry->MibObject.
                   i4FsMplsTpMeOnDemandOamPhbTCValue);

        CliPrintf (CliHandle, "  %-30s:", "Service Setup Mode");
        if (pOamFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeServiceSignaled == OAM_TRUE)
        {
            CliPrintf (CliHandle, "Signaled \r\n");
        }
        else
        {
            CliPrintf (CliHandle, "Static \r\n");
        }

        OamCliPrintMeId (CliHandle, pOamFsMplsTpMeEntry);
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : OamCliPrintMeId
 * DESCRIPTION      : This function Prints the IP or ICC based ME Ids
 * INPUT            : 
 * OUTPUT           : None
 * RETURNS          : CLI_SUCCESS/CLI_FAILURE
 **************************************************************************/
VOID
OamCliPrintMeId (tCliHandle CliHandle,
                 tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressLsrId = 0;
    UINT4               u4EgressLsrId = 0;
    INT4                i4OidLen = 0;
    UINT1               au1IccId[OAM_ICC_ID_LEN + 1];
    UINT1               au1UmcId[OAM_UMC_ID_LEN + 1];

    MEMSET (&OamFsMplsTpMegEntry, 0, sizeof (tOamFsMplsTpMegEntry));
    MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));
    MEMSET (au1IccId, 0, (OAM_ICC_ID_LEN + 1));
    MEMSET (au1UmcId, 0, (OAM_UMC_ID_LEN + 1));

    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
    pOamFsMplsTpMegEntry = OamGetFsMplsTpMegTable (&OamFsMplsTpMegEntry);

    if (pOamFsMplsTpMegEntry == NULL)
    {
        return;
    }

    if (pOamFsMplsTpMegEntry->MibObject.
        i4FsMplsTpMegOperatorType == OAM_OPERATOR_TYPE_ICC)
    {
        MEMCPY (au1IccId, pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen);

        MEMCPY (au1UmcId, pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen);

        CliPrintf (CliHandle, "  %-30s:", "MEG Id");
        CliPrintf (CliHandle, "%s :: ", au1IccId);
        CliPrintf (CliHandle, "%s \r\n", au1UmcId);

        if (pOamFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeMpType == OAM_ME_MP_TYPE_MEP)
        {
            CliPrintf (CliHandle, "  %-30s:", "MEP Id");
            CliPrintf (CliHandle, "%s :: ", au1IccId);
            CliPrintf (CliHandle, "%s :: ", au1UmcId);
            CliPrintf (CliHandle, "%u \r\n",
                       pOamFsMplsTpMeEntry->MibObject.
                       u4FsMplsTpMeSourceMepIndex);

        }
        else if (pOamFsMplsTpMeEntry->MibObject.
                 i4FsMplsTpMeMpType == OAM_ME_MP_TYPE_MIP)
        {
            CliPrintf (CliHandle, "  %-30s:", "MIP Id");
            OamFsMplsTpGlobalConfigEntry.MibObject.
                u4FsMplsTpContextId =
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;

            pOamFsMplsTpGlobalConfigEntry = OamGetFsMplsTpGlobalConfigTable
                (&OamFsMplsTpGlobalConfigEntry);

            if (pOamFsMplsTpGlobalConfigEntry != NULL)
            {
                CliPrintf (CliHandle, "%u :: ",
                           pOamFsMplsTpGlobalConfigEntry->MibObject.
                           u4FsMplsTpGlobalId);
                CliPrintf (CliHandle, "%u :: ",
                           pOamFsMplsTpGlobalConfigEntry->MibObject.
                           u4FsMplsTpNodeIdentifier);
                CliPrintf (CliHandle, "0 \r\n");
            }
        }
    }

    CliPrintf (CliHandle, "  %-30s:", "Operational status");
    if (pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus == OAM_ME_OPER_STATUS_UP)
    {
        CliPrintf (CliHandle, "%s  \r\n", "Up");
    }
    else
    {
        if (pOamFsMplsTpMeEntry->
            u4FsMplsTpMeOperStatus & OAM_ME_OPER_STATUS_MEG_DOWN)
        {
            CliPrintf (CliHandle, "%s  ", "MEG down");
        }
        if (pOamFsMplsTpMeEntry->
            u4FsMplsTpMeOperStatus & OAM_ME_OPER_STATUS_ME_DOWN)
        {
            CliPrintf (CliHandle, "%s  ", "ME down");
        }
        if (pOamFsMplsTpMeEntry->
            u4FsMplsTpMeOperStatus & OAM_ME_OPER_STATUS_OAM_DOWN)
        {
            CliPrintf (CliHandle, "%s  ", "OAM down");
        }
        if (pOamFsMplsTpMeEntry->
            u4FsMplsTpMeOperStatus & OAM_ME_OPER_STATUS_PATH_DOWN)
        {
            CliPrintf (CliHandle, "%s  ", "PATH down");
        }
        CliPrintf (CliHandle, "\r\n");
    }

    i4OidLen = pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;

    if (i4OidLen != 0)
    {
        CliPrintf (CliHandle, "  %s", "Service Information \r\n");
        if (pOamFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP)
        {
            i4OidLen = i4OidLen - 1;
            u4EgressLsrId = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[i4OidLen];
            i4OidLen = i4OidLen - 1;
            u4IngressLsrId = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[i4OidLen];
            i4OidLen = i4OidLen - 1;
            u4TunnelInstance = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[i4OidLen];
            i4OidLen = i4OidLen - 1;
            u4TunnelIndex = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[i4OidLen];

            CliPrintf (CliHandle, "   %-29s:", "Tunnel Index");
            CliPrintf (CliHandle, "%u \r\n", u4TunnelIndex);
            CliPrintf (CliHandle, "   %-29s:", "Tunnel Instance");
            CliPrintf (CliHandle, "%u \r\n", u4TunnelInstance);
            CliPrintf (CliHandle, "   %-29s:", "Ingress LSR Id");
            CliPrintf (CliHandle, "%u \r\n", u4IngressLsrId);
            CliPrintf (CliHandle, "   %-29s:", "Egress LSR Id");
            CliPrintf (CliHandle, "%u \r\n\n", u4EgressLsrId);
        }
        else if (pOamFsMplsTpMegEntry->MibObject.
                 i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_PW)
        {
            CliPrintf (CliHandle, "   %-29s:", "Pseudowire Index");
            CliPrintf (CliHandle, "%u \r\n\n ", pOamFsMplsTpMeEntry->MibObject.
                       au4FsMplsTpMeServicePointer[i4OidLen - 1]);
        }
    }
    return;
}

/**************************************************************************
* Function    : OamCliGetMegIndexFromMegName
* Description : Utility to Get Meg Index from MegName and ContextId
* Input       : Context Id and Meg name
* Output      : Meg Index
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
***************************************************************************/
INT4
OamCliGetMegIndexFromMegName (UINT4 u4ContextId, UINT1 *pu1MegName,
                              UINT4 *pu4MegIndex)
{

    INT4                i4RetVal = 0;

    OAM_LOCK;

    i4RetVal = OamUtilGetMegIndexFromMegName (u4ContextId, pu1MegName,
                                              pu4MegIndex);

    OAM_UNLOCK;
    return i4RetVal;
}


/***************************************************************************/
/*                                                                         */
/*     Function Name : IssMplsOamShowDebugging                             */
/*                                                                         */
/*     Description   : This function is used to display debug level for    */
/*                     MPLS Oam module                                     */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : None                                                */
/*                                                                         */
/***************************************************************************/

VOID IssMplsOamShowDebugging(tCliHandle CliHandle)
{
    UINT4 u4DebugLevel = 0;
    UINT4 u4ContextId = CLI_OAM_DEFAULT_CXT_ID;

    CliGetDebugLevel (CliHandle, u4ContextId, &u4DebugLevel);

    if(u4DebugLevel == 0)
    {
        return;
    }

    CliPrintf(CliHandle,"\rMPLS OAM :");
    if((u4DebugLevel & CLI_OAM_FN_ENTRY_EXIT_TRC)!=0)
    {
        CliPrintf(CliHandle,"\r\n  MPLS oam function entry exit debugging is on");
    }
    if((u4DebugLevel & CLI_OAM_MGMT_TRC)!=0)
    {
        CliPrintf(CliHandle,"\r\n  MPLS oam management debugging is on");
    }
    if((u4DebugLevel & CLI_OAM_MAIN_TRC)!=0)
    {
        CliPrintf(CliHandle,"\r\n  MPLS oam main debugging is on");
    }
    if((u4DebugLevel & CLI_OAM_UTIL_TRC)!=0)
    {
        CliPrintf(CliHandle,"\r\n  MPLS oam util debugging is on");
    }
    if((u4DebugLevel & CLI_OAM_RESOURCE_TRC)!=0)
    {
        CliPrintf(CliHandle,"\r\n  MPLS oam resource debugging is on");
    }
    if((u4DebugLevel & CLI_OAM_ALL_FAIL_TRC)!=0)
    {
        CliPrintf(CliHandle,"\r\n  MPLS oam all-fail debugging is on");
    }
    CliPrintf(CliHandle,"\r\n");

    return;
}
#endif /*__OAMCLI_C__*/
