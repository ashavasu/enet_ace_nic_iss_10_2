
#include "oaminc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTpGlobalConfigTable
 Input       :  The Indices
                FsMplsTpContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTpGlobalConfigTable (UINT4 u4FsMplsTpContextId)
{
    if (u4FsMplsTpContextId != OAM_DEFAULT_CXT_ID)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Context ID not Valid.\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTpNodeMapTable
 Input       :  The Indices
                FsMplsTpContextId
                FsMplsTpNodeMapLocalNum
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTpNodeMapTable (UINT4 u4FsMplsTpContextId,
                                              UINT4 u4FsMplsTpNodeMapLocalNum)
{
    if (u4FsMplsTpContextId != OAM_DEFAULT_CXT_ID)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Context ID not Valid.\r\n"));
        return SNMP_FAILURE;
    }

    if (u4FsMplsTpNodeMapLocalNum == 0)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Invalid Local Map Number.\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTpMegTable
 Input       :  The Indices
                FsMplsTpContextId
                FsMplsTpMegIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTpMegTable (UINT4 u4FsMplsTpContextId,
                                          UINT4 u4FsMplsTpMegIndex)
{
    if (u4FsMplsTpContextId != OAM_DEFAULT_CXT_ID)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Context ID not Valid.\r\n"));
        return SNMP_FAILURE;
    }

    if (u4FsMplsTpMegIndex == 0)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Invalid MEG Index.\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTpMeTable
 Input       :  The Indices
                FsMplsTpContextId
                FsMplsTpMegIndex
                FsMplsTpMeIndex
                FsMplsTpMeMpIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTpMeTable (UINT4 u4FsMplsTpContextId,
                                         UINT4 u4FsMplsTpMegIndex,
                                         UINT4 u4FsMplsTpMeIndex,
                                         UINT4 u4FsMplsTpMeMpIndex)
{
    if (u4FsMplsTpContextId != OAM_DEFAULT_CXT_ID)
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Context ID not Valid.\r\n"));
        return SNMP_FAILURE;
    }

    if ((u4FsMplsTpMegIndex == 0) || (u4FsMplsTpMeIndex == 0)
        || (u4FsMplsTpMeMpIndex == 0))
    {
        OAM_TRC ((OAM_MAIN_TRC, "\r%% Invalid Index.\r\n"));
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
