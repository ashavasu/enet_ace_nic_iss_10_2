/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: oamsrc.c,v 1.10 2014/03/09 13:37:31 siva Exp $
*
* Description: This file contains the routines for the show running config 
*              commands in MPLS OAM module.
*********************************************************************/
#include "oaminc.h"
#include "cli.h"
#include "csr.h"
#include "oamext.h"

PRIVATE INT4        OamSRCFsMplsTpNodeMapTable (tCliHandle CliHandle);
PRIVATE INT4        OamSRCFsMplsTpGlobalConfigTable (tCliHandle CliHandle);
PRIVATE INT4        OamSRCFsMplsTpMegTable (tCliHandle CliHandle);
PRIVATE INT4        OamSRCFsMplsTpMeTable (tCliHandle CliHandle,
                                           tOamFsMplsTpMegEntry *,
                                           tOamFsMplsTpMeEntry *);

extern UINT4        MplsTunnelName[TUNNEL_BASE_OID_LEN];
extern UINT4        PwType[PSEUDOWIRE_BASE_OID_LEN];

INT4
OamShowRunningConfig (tCliHandle CliHandle)
{
    CliPrintf (CliHandle, "\n!");
    OamSRCFsMplsTpGlobalConfigTable (CliHandle);
    OamSRCFsMplsTpNodeMapTable (CliHandle);
    OamSRCFsMplsTpMegTable (CliHandle);
    CliPrintf (CliHandle, "\n!\n");
    return CLI_SUCCESS;
}

PRIVATE INT4
OamSRCFsMplsTpNodeMapTable (tCliHandle CliHandle)
{
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);

    pOamFsMplsTpNodeMapEntry = OamGetFirstFsMplsTpNodeMapTable ();

    while (pOamFsMplsTpNodeMapEntry != NULL)
    {
        CliPrintf (CliHandle,
                   "\nmpls node-map-id local-map-num %d global-id %d node-id %d ",
                   pOamFsMplsTpNodeMapEntry->MibObject.
                   u4FsMplsTpNodeMapLocalNum,
                   pOamFsMplsTpNodeMapEntry->MibObject.
                   u4FsMplsTpNodeMapGlobalId,
                   pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId);
        FilePrintf (CliHandle,
                    "\nmpls node-map-id local-map-num %d global-id %d node-id %d ",
                    pOamFsMplsTpNodeMapEntry->MibObject.
                    u4FsMplsTpNodeMapLocalNum,
                    pOamFsMplsTpNodeMapEntry->MibObject.
                    u4FsMplsTpNodeMapGlobalId,
                    pOamFsMplsTpNodeMapEntry->MibObject.
                    u4FsMplsTpNodeMapNodeId);

        if (pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId != 0)
        {
            if (MplsPortVcmGetAliasName
                (pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId,
                 au1ContextName) != OSIX_FAILURE)
            {
                CliPrintf (CliHandle, "vrf %s", au1ContextName);
                FilePrintf (CliHandle, "vrf %s", au1ContextName);
            }
        }

        pOamFsMplsTpNodeMapEntry =
            OamGetNextFsMplsTpNodeMapTable (pOamFsMplsTpNodeMapEntry);
    }
    return CLI_SUCCESS;

}

PRIVATE INT4
OamSRCFsMplsTpGlobalConfigTable (tCliHandle CliHandle)
{
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;
    UINT1               au1IccId[OAM_ICC_ID_LEN + 1];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (au1IccId, 0, (OAM_ICC_ID_LEN + 1));
    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);

    pOamFsMplsTpGlobalConfigEntry = OamGetFirstFsMplsTpGlobalConfigTable ();

    while (pOamFsMplsTpGlobalConfigEntry != NULL)
    {
        if ((pOamFsMplsTpGlobalConfigEntry->MibObject.
             u4FsMplsTpGlobalId != MPLS_ZERO) ||
            (pOamFsMplsTpGlobalConfigEntry->MibObject.
             au1FsMplsTpIcc[0] != '\0') ||
            (pOamFsMplsTpGlobalConfigEntry->MibObject.
             u4FsMplsTpNodeIdentifier != MPLS_ZERO))
        {
            CliPrintf (CliHandle, "\nmpls ");
            FilePrintf (CliHandle, "\nmpls ");

            if (pOamFsMplsTpGlobalConfigEntry->MibObject.
                u4FsMplsTpGlobalId != MPLS_ZERO)
            {
                CliPrintf (CliHandle, "global-id %u ",
                           pOamFsMplsTpGlobalConfigEntry->MibObject.
                           u4FsMplsTpGlobalId);
                FilePrintf (CliHandle, "global-id %u ",
                            pOamFsMplsTpGlobalConfigEntry->MibObject.
                            u4FsMplsTpGlobalId);

            }

            if (pOamFsMplsTpGlobalConfigEntry->MibObject.
                au1FsMplsTpIcc[0] != '\0')
            {
                MEMCPY (au1IccId, pOamFsMplsTpGlobalConfigEntry->MibObject.
                        au1FsMplsTpIcc,
                        pOamFsMplsTpGlobalConfigEntry->MibObject.
                        i4FsMplsTpIccLen);

                CliPrintf (CliHandle, "icc-id %s ", au1IccId);
                FilePrintf (CliHandle, "icc-id %s ", au1IccId);
            }

            if (pOamFsMplsTpGlobalConfigEntry->MibObject.
                u4FsMplsTpNodeIdentifier != MPLS_ZERO)
            {
                CliPrintf (CliHandle, "node-id %u ",
                           pOamFsMplsTpGlobalConfigEntry->MibObject.
                           u4FsMplsTpNodeIdentifier);
                FilePrintf (CliHandle, "node-id %u ",
                            pOamFsMplsTpGlobalConfigEntry->MibObject.
                            u4FsMplsTpNodeIdentifier);

            }

            if ((pOamFsMplsTpGlobalConfigEntry->
                 MibObject.u4FsMplsTpContextId != 0) &&
                (MplsPortVcmGetAliasName
                 (pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpContextId,
                  au1ContextName) == OSIX_SUCCESS))
            {
                CliPrintf (CliHandle, "vrf %s", au1ContextName);
                FilePrintf (CliHandle, "vrf %s", au1ContextName);
            }

            if (pOamFsMplsTpGlobalConfigEntry->MibObject.
                i4FsMplsTpOamModuleStatus != OAM_ENABLED)
            {
                CliPrintf (CliHandle, "\nno mpls oam enable ");
                FilePrintf (CliHandle, "\nno mpls oam enable ");

                if (pOamFsMplsTpGlobalConfigEntry->MibObject.
                    u4FsMplsTpContextId != 0)
                {
                    CliPrintf (CliHandle, "vrf %s", au1ContextName);
                    FilePrintf (CliHandle, "vrf %s", au1ContextName);
                }
            }
        }
        pOamFsMplsTpGlobalConfigEntry =
            OamGetNextFsMplsTpGlobalConfigTable (pOamFsMplsTpGlobalConfigEntry);
    }
    return CLI_SUCCESS;
}

PRIVATE INT4
OamSRCFsMplsTpMegTable (tCliHandle CliHandle)
{
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    static tOamFsMplsTpMeEntry OamFsMplsTpMeEntry;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    UINT1               au1IccId[OAM_ICC_ID_LEN + 1];
    UINT1               au1UmcId[OAM_UMC_ID_LEN + 1];
    UINT1               au1MegName[OAM_MEG_NAME_MAX_LEN + 1];
    UINT1               au1ContextName[VCM_ALIAS_MAX_LEN];

    MEMSET (&OamFsMplsTpMeEntry, 0, sizeof (tOamFsMplsTpMeEntry));
    MEMSET (au1ContextName, 0, VCM_ALIAS_MAX_LEN);
    MEMSET (au1IccId, 0, (OAM_ICC_ID_LEN + 1));
    MEMSET (au1UmcId, 0, (OAM_UMC_ID_LEN + 1));

    pOamFsMplsTpMegEntry = OamGetFirstFsMplsTpMegTable ();

    while (pOamFsMplsTpMegEntry != NULL)
    {
        MEMSET (au1MegName, 0, (OAM_MEG_NAME_MAX_LEN + 1));
        MEMCPY (au1MegName, pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen);

        CliPrintf (CliHandle, "\n!\nmpls oam meg %s ", au1MegName);
        FilePrintf (CliHandle, "\n!\nmpls oam meg %s ", au1MegName);

        if (pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId != 0)
        {
            if (MplsPortVcmGetAliasName
                (pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId,
                 au1ContextName) != OSIX_FAILURE)
            {
                CliPrintf (CliHandle, "vrf %s", au1ContextName);
                FilePrintf (CliHandle, "vrf %s", au1ContextName);
            }
        }

        if ((pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType
             != OAM_SERVICE_TYPE_LSP) ||
            ((pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc[0] != '\0') &&
             (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc[0] != '\0')))
        {
            CliPrintf (CliHandle, "\n set service type ");
            FilePrintf (CliHandle, "\n set service type ");
            if (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType ==
                OAM_SERVICE_TYPE_LSP)
            {
                CliPrintf (CliHandle, "lsp ");
                FilePrintf (CliHandle, "lsp ");
            }
            else if (pOamFsMplsTpMegEntry->MibObject.
                     i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_PW)
            {
                CliPrintf (CliHandle, "pw ");
                FilePrintf (CliHandle, "pw ");
            }
            else
            {
                CliPrintf (CliHandle, "section ");
                FilePrintf (CliHandle, "section ");
            }

            if ((pOamFsMplsTpMegEntry->MibObject.
                 i4FsMplsTpMegOperatorType == OAM_OPERATOR_TYPE_ICC) &&
                (pOamFsMplsTpMegEntry->MibObject.
                 au1FsMplsTpMegIdIcc[0] != '\0') &&
                (pOamFsMplsTpMegEntry->MibObject.
                 au1FsMplsTpMegIdUmc[0] != '\0'))
            {
                MEMCPY (au1IccId, pOamFsMplsTpMegEntry->MibObject.
                        au1FsMplsTpMegIdIcc,
                        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen);

                MEMCPY (au1UmcId, pOamFsMplsTpMegEntry->MibObject.
                        au1FsMplsTpMegIdUmc,
                        pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen);

                CliPrintf (CliHandle, "icc %s ", au1IccId);
                FilePrintf (CliHandle, "icc %s ", au1IccId);
                CliPrintf (CliHandle, "umc %s ", au1UmcId);
                FilePrintf (CliHandle, "umc %s ", au1UmcId);
            }

            if (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation ==
                OAM_MEG_MP_LOCATION_PER_INTERFACE)
            {
                CliPrintf (CliHandle, "per-interface");
                FilePrintf (CliHandle, "per-interface");
            }
        }

        OamFsMplsTpMeEntry.MibObject.u4FsMplsTpMegIndex =
            pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex;
        OamFsMplsTpMeEntry.MibObject.u4FsMplsTpContextId =
            pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId;

        pOamFsMplsTpMeEntry = OamGetNextFsMplsTpMeTable (&OamFsMplsTpMeEntry);

        while (pOamFsMplsTpMeEntry != NULL)
        {
            if ((pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex !=
                 pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex) ||
                (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId !=
                 pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId))
            {
                break;
            }

            OamSRCFsMplsTpMeTable (CliHandle, pOamFsMplsTpMegEntry,
                                   pOamFsMplsTpMeEntry);
            pOamFsMplsTpMeEntry =
                OamGetNextFsMplsTpMeTable (pOamFsMplsTpMeEntry);
        }
        pOamFsMplsTpMegEntry =
            OamGetNextFsMplsTpMegTable (pOamFsMplsTpMegEntry);
    }

    return CLI_SUCCESS;
}

PRIVATE INT4
OamSRCFsMplsTpMeTable (tCliHandle CliHandle,
                       tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry,
                       tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry)
{
    UINT4               u4PwIndex = 0;
    UINT4               u4VcId = 0;
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressLsrId = 0;
    UINT4               u4EgressLsrId = 0;
    INT4                i4TunnelRowPointerLen = 0;
    INT4                i4PwRowPointerLen = 0;
    UINT1               u1OidLen = 0;
    INT1                ai1IfName[OAM_IF_NAME_MAX_LEN];

    MEMSET (ai1IfName, 0, sizeof (ai1IfName));

    CliPrintf (CliHandle, "\n\n service %s ",
               pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName);
    FilePrintf (CliHandle, "\n\n service %s ",
                pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName);

    if ((pOamFsMplsTpMegEntry->MibObject.
         i4FsMplsTpMegOperatorType == OAM_OPERATOR_TYPE_ICC) &&
        (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex != 0) &&
        (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex != 0))
    {
        CliPrintf (CliHandle, "\n mep crosscheck mpid %d %d service %s",
                   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex,
                   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex,
                   pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName);
        FilePrintf (CliHandle, "\n mep crosscheck mpid %d %d service %s",
                    pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex,
                    pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex,
                    pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName);

    }

    i4TunnelRowPointerLen = (sizeof (MplsTunnelName) / sizeof (UINT4)) +
        TUNNEL_TABLE_INDICES;
    i4PwRowPointerLen = (sizeof (PwType) / sizeof (UINT4)) + 1;

    if ((pOamFsMplsTpMeEntry->MibObject.
         i4FsMplsTpMeServicePointerLen == i4TunnelRowPointerLen) ||
        (pOamFsMplsTpMeEntry->MibObject.
         i4FsMplsTpMeServicePointerLen == i4PwRowPointerLen))

    {
        CliPrintf (CliHandle, "\n mpls oam ");
        FilePrintf (CliHandle, "\n mpls oam ");

        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType ==
            OAM_ME_MP_TYPE_MEP)
        {
            CliPrintf (CliHandle, "mep ");
            FilePrintf (CliHandle, "mep ");

            if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection ==
                OAM_MEP_DIRECTION_UP)
            {
                CliPrintf (CliHandle, "inward ");
                FilePrintf (CliHandle, "inward ");
            }
        }
        else
        {
            CliPrintf (CliHandle, "mip ");
            FilePrintf (CliHandle, "mip ");
        }

        CliPrintf (CliHandle, "service %s ",
                   pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName);
        FilePrintf (CliHandle, "service %s ",
                    pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName);

        if (pOamFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeServiceSignaled == OAM_TRUE)
        {
            CliPrintf (CliHandle, "signaled ");
            FilePrintf (CliHandle, "signaled ");
        }

        u1OidLen =
            (UINT1) (pOamFsMplsTpMeEntry->MibObject.
                     i4FsMplsTpMeServicePointerLen);
        if (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType ==
            OAM_SERVICE_TYPE_LSP)
        {
            u1OidLen--;
            u4EgressLsrId = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[u1OidLen];
            u1OidLen--;
            u4IngressLsrId = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[u1OidLen];
            u1OidLen--;
            u4TunnelInstance = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[u1OidLen];
            u1OidLen--;
            u4TunnelIndex = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[u1OidLen];

            CliPrintf (CliHandle, "lsp %d %d %d %d ",
                       u4TunnelIndex, u4TunnelInstance,
                       u4IngressLsrId, u4EgressLsrId);
            FilePrintf (CliHandle, "lsp %d %d %d %d ",
                        u4TunnelIndex, u4TunnelInstance,
                        u4IngressLsrId, u4EgressLsrId);
        }
        else
        {
            u4PwIndex = pOamFsMplsTpMeEntry->MibObject.
                au4FsMplsTpMeServicePointer[u1OidLen - 1];

            OamUtilGetVcIdFromPwIndex (u4PwIndex, &u4VcId);

            CliPrintf (CliHandle, "pw %d ", u4VcId);
            FilePrintf (CliHandle, "pw %d ", u4VcId);
        }

        if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex != 0)
        {
            if (CfaCliConfGetIfName ((UINT4) pOamFsMplsTpMeEntry->MibObject.
                                     i4FsMplsTpMeMpIfIndex,
                                     ai1IfName) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle, "%s ", ai1IfName);
                FilePrintf (CliHandle, "%s ", ai1IfName);
            }
        }
    }

    if (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType == OAM_ME_MP_TYPE_MEP)
    {
        if (pOamFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeProactiveOamPhbTCValue != 1)
        {
            CliPrintf (CliHandle, "\n mpls oam service %s mep proactive phb %d",
                       pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                       pOamFsMplsTpMeEntry->MibObject.
                       i4FsMplsTpMeProactiveOamPhbTCValue);
            FilePrintf (CliHandle,
                        "\n mpls oam service %s mep proactive phb %d",
                        pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                        pOamFsMplsTpMeEntry->MibObject.
                        i4FsMplsTpMeProactiveOamPhbTCValue);
        }

        if (pOamFsMplsTpMeEntry->MibObject.
            i4FsMplsTpMeOnDemandOamPhbTCValue != 1)
        {
            CliPrintf (CliHandle, "\n mpls oam service %s mep on-demand phb %d",
                       pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                       pOamFsMplsTpMeEntry->MibObject.
                       i4FsMplsTpMeOnDemandOamPhbTCValue);
            FilePrintf (CliHandle,
                        "\n mpls oam service %s mep on-demand phb %d",
                        pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                        pOamFsMplsTpMeEntry->MibObject.
                        i4FsMplsTpMeOnDemandOamPhbTCValue);
        }
    }

    return CLI_SUCCESS;
}
