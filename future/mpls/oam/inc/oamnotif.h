/* $Id: oamnotif.h,v 1.3 2010/10/28 19:15:55 prabuc Exp $ */

#ifndef _OAM_NOTIF_H
#define _OAM_NOTIF_H

#define OAM_TRAP_DEFECT_CONDITION      1

typedef struct  _OamNotifyInfo
{
    UINT4 u4ContextId;
    UINT4 u4MegIndex;
    UINT4 u4MeIndex;
    UINT4 u4MpIndex;
    UINT1 au1MegName[OAM_MEG_NAME_MAX_LEN];
    UINT1 au1MeName[OAM_ME_NAME_MAX_LEN];
    INT4  i4MegOperStatus;
    UINT1 u1MegSubOperStatus;
    UINT1 au1Pad[3];
}tOamNotifyInfo;

#ifdef __OAMNOTIF_C__

UINT4 gau4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

CONST CHR1 *gapc1MegOperStatus[] = 
   { 
       NULL,
       "UP",
       "DOWN"
   };

CONST CHR1 *gapc1MegSubOperStatus[] =
   {
       "NONE",
       "MEG DOWN",
       "ME DOWN",
       "MEG DOWN, ME DOWN",
       "OAM APP DOWN",
       "MEG DOWN, OAM APP DOWN",
       "ME DOWN, OAM APP DOWN",
       "MEG DOWN, ME DOWN, OAM APP DOWN",
       "PATH DOWN",
       "MEG DOWN, PATH DOWN",
       "ME DOWN, PATH DOWN",
       "MEG DOWN, ME DOWN, PATH DOWN",
       "OAM APP DOWN, PATH DOWN",
       "MEG DOWN, OAM APP DOWN, PATH DOWN",
       "ME DOWN, OAM APP DOWN, PATH DOWN",
       "MEG DOWN, ME DOWN, OAM APP DOWN, PATH DOWN",
   };
#endif

tSNMP_OID_TYPE *OamNotifMakeObjIdFrmString PROTO ((INT1 *pi1TextStr, UINT1 *pu1TableName));

INT4 OamNotifParseSubIdNew PROTO ((UINT1 **ppu1TempPtr, UINT4 *pu4Value));

VOID OamNotifSendTrapAndSyslog PROTO ((tOamNotifyInfo * pNotifyInfo, UINT4 u4TrapType));

#endif
