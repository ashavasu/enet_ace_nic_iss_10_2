/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamdefg.h,v 1.4 2012/03/19 11:28:23 siva Exp $
*
* Description: Proto types & DataStructure definition 
*********************************************************************/


/* Macro used to fill the CLI structure for FsMplsTpGlobalConfigTable 
 using the input given in def file */

#define  OAM_FILL_FSMPLSTPGLOBALCONFIGTABLE_ARGS(pOamFsMplsTpGlobalConfigEntry,\
   pOamIsSetFsMplsTpGlobalConfigEntry,\
   pau1FsMplsTpContextId,\
   pau1FsMplsTpOamModuleStatus,\
   pau1FsMplsTpGlobalId,\
   pau1FsMplsTpIcc,\
   pau1FsMplsTpIccLen,\
   pau1FsMplsTpNodeIdentifier,\
   pau1FsMplsTpTraceLevel,\
            pau1FsMplsTpNotificationEnable)\
  {\
  if (pau1FsMplsTpContextId != NULL)\
  {\
   pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpContextId = *(UINT4 *) (pau1FsMplsTpContextId);\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpContextId = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpOamModuleStatus != NULL)\
  {\
   pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpOamModuleStatus = *(INT4 *) (pau1FsMplsTpOamModuleStatus);\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpOamModuleStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpOamModuleStatus = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpGlobalId != NULL)\
  {\
   pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId = *(UINT4 *) (pau1FsMplsTpGlobalId);\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpGlobalId = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpGlobalId = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpIcc != NULL)\
  {\
   MEMCPY (pOamFsMplsTpGlobalConfigEntry->MibObject.au1FsMplsTpIcc, pau1FsMplsTpIcc, *(INT4 *)pau1FsMplsTpIccLen);\
   pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpIccLen = *(INT4 *)pau1FsMplsTpIccLen;\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpIcc = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpIcc = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpNodeIdentifier != NULL)\
  {\
   pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpNodeIdentifier = *(UINT4 *) (pau1FsMplsTpNodeIdentifier);\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNodeIdentifier = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNodeIdentifier = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpTraceLevel != NULL)\
  {\
   pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpTraceLevel = *(UINT4 *) (pau1FsMplsTpTraceLevel);\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpTraceLevel = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpTraceLevel = OSIX_FALSE;\
  }\
        if (pau1FsMplsTpNotificationEnable != NULL)\
        {\
            pOamFsMplsTpGlobalConfigEntry->MibObject.i4FsMplsTpNotificationEnable = *(INT4 *) (pau1FsMplsTpNotificationEnable);\
            pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNotificationEnable = OSIX_TRUE;\
        }\
        else\
        {\
            pOamIsSetFsMplsTpGlobalConfigEntry->bFsMplsTpNotificationEnable = OSIX_FALSE;\
        }\
        }
/* Macro used to fill the index values in  structure for FsMplsTpGlobalConfigTable 
 using the input given in def file */

#define  OAM_FILL_FSMPLSTPGLOBALCONFIGTABLE_INDEX(pOamFsMplsTpGlobalConfigEntry,\
   pau1FsMplsTpContextId)\
  {\
  if (pau1FsMplsTpContextId != NULL)\
  {\
   pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpContextId = *(UINT4 *) (pau1FsMplsTpContextId);\
  }\
  }




/* Macro used to fill the CLI structure for FsMplsTpNodeMapTable 
 using the input given in def file */

#define  OAM_FILL_FSMPLSTPNODEMAPTABLE_ARGS(pOamFsMplsTpNodeMapEntry,\
   pOamIsSetFsMplsTpNodeMapEntry,\
   pau1FsMplsTpContextId,\
   pau1FsMplsTpNodeMapLocalNum,\
   pau1FsMplsTpNodeMapGlobalId,\
   pau1FsMplsTpNodeMapNodeId,\
   pau1FsMplsTpNodeMapRowStatus)\
  {\
  if (pau1FsMplsTpContextId != NULL)\
  {\
   pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId = *(UINT4 *) (pau1FsMplsTpContextId);\
   pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpContextId = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpNodeMapLocalNum != NULL)\
  {\
   pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapLocalNum = *(UINT4 *) (pau1FsMplsTpNodeMapLocalNum);\
   pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapLocalNum = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapLocalNum = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpNodeMapGlobalId != NULL)\
  {\
   pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapGlobalId = *(UINT4 *) (pau1FsMplsTpNodeMapGlobalId);\
   pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapGlobalId = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapGlobalId = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpNodeMapNodeId != NULL)\
  {\
   pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId = *(UINT4 *) (pau1FsMplsTpNodeMapNodeId);\
   pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapNodeId = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapNodeId = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpNodeMapRowStatus != NULL)\
  {\
   pOamFsMplsTpNodeMapEntry->MibObject.i4FsMplsTpNodeMapRowStatus = *(INT4 *) (pau1FsMplsTpNodeMapRowStatus);\
   pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpNodeMapEntry->bFsMplsTpNodeMapRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsMplsTpNodeMapTable 
 using the input given in def file */

#define  OAM_FILL_FSMPLSTPNODEMAPTABLE_INDEX(pOamFsMplsTpNodeMapEntry,\
   pau1FsMplsTpContextId,\
   pau1FsMplsTpNodeMapLocalNum)\
  {\
  if (pau1FsMplsTpContextId != NULL)\
  {\
   pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpContextId = *(UINT4 *) (pau1FsMplsTpContextId);\
  }\
  if (pau1FsMplsTpNodeMapLocalNum != NULL)\
  {\
   pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapLocalNum = *(UINT4 *) (pau1FsMplsTpNodeMapLocalNum);\
  }\
  }




/* Macro used to fill the CLI structure for FsMplsTpMegTable 
 using the input given in def file */

#define  OAM_FILL_FSMPLSTPMEGTABLE_ARGS(pOamFsMplsTpMegEntry,\
   pOamIsSetFsMplsTpMegEntry,\
   pau1FsMplsTpContextId,\
   pau1FsMplsTpMegIndex,\
   pau1FsMplsTpMegName,\
   pau1FsMplsTpMegNameLen,\
   pau1FsMplsTpMegOperatorType,\
   pau1FsMplsTpMegIdIcc,\
   pau1FsMplsTpMegIdIccLen,\
   pau1FsMplsTpMegIdUmc,\
   pau1FsMplsTpMegIdUmcLen,\
   pau1FsMplsTpMegServiceType,\
   pau1FsMplsTpMegMpLocation,\
   pau1FsMplsTpMegRowStatus)\
  {\
  if (pau1FsMplsTpContextId != NULL)\
  {\
   pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId = *(UINT4 *) (pau1FsMplsTpContextId);\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpContextId = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMegIndex != NULL)\
  {\
   pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex = *(UINT4 *) (pau1FsMplsTpMegIndex);\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIndex = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMegName != NULL)\
  {\
   MEMCPY (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName, pau1FsMplsTpMegName, *(INT4 *)pau1FsMplsTpMegNameLen);\
   pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen = *(INT4 *)pau1FsMplsTpMegNameLen;\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegName = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegName = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMegOperatorType != NULL)\
  {\
   pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegOperatorType = *(INT4 *) (pau1FsMplsTpMegOperatorType);\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegOperatorType = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegOperatorType = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMegIdIcc != NULL)\
  {\
   MEMCPY (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc, pau1FsMplsTpMegIdIcc, *(INT4 *)pau1FsMplsTpMegIdIccLen);\
   pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen = *(INT4 *)pau1FsMplsTpMegIdIccLen;\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdIcc = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdIcc = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMegIdUmc != NULL)\
  {\
   MEMCPY (pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc, pau1FsMplsTpMegIdUmc, *(INT4 *)pau1FsMplsTpMegIdUmcLen);\
   pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen = *(INT4 *)pau1FsMplsTpMegIdUmcLen;\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdUmc = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegIdUmc = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMegServiceType != NULL)\
  {\
   pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType = *(INT4 *) (pau1FsMplsTpMegServiceType);\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegServiceType = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegServiceType = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMegMpLocation != NULL)\
  {\
   pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation = *(INT4 *) (pau1FsMplsTpMegMpLocation);\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegMpLocation = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegMpLocation = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMegRowStatus != NULL)\
  {\
   pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus = *(INT4 *) (pau1FsMplsTpMegRowStatus);\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMegEntry->bFsMplsTpMegRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsMplsTpMegTable 
 using the input given in def file */

#define  OAM_FILL_FSMPLSTPMEGTABLE_INDEX(pOamFsMplsTpMegEntry,\
   pau1FsMplsTpContextId,\
   pau1FsMplsTpMegIndex)\
  {\
  if (pau1FsMplsTpContextId != NULL)\
  {\
   pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpContextId = *(UINT4 *) (pau1FsMplsTpContextId);\
  }\
  if (pau1FsMplsTpMegIndex != NULL)\
  {\
   pOamFsMplsTpMegEntry->MibObject.u4FsMplsTpMegIndex = *(UINT4 *) (pau1FsMplsTpMegIndex);\
  }\
  }




/* Macro used to fill the CLI structure for FsMplsTpMeTable 
 using the input given in def file */

#define  OAM_FILL_FSMPLSTPMETABLE_ARGS(pOamFsMplsTpMeEntry,\
   pOamIsSetFsMplsTpMeEntry,\
   pau1FsMplsTpContextId,\
   pau1FsMplsTpMegIndex,\
   pau1FsMplsTpMeIndex,\
   pau1FsMplsTpMeMpIndex,\
   pau1FsMplsTpMeName,\
   pau1FsMplsTpMeNameLen,\
   pau1FsMplsTpMeMpIfIndex,\
   pau1FsMplsTpMeSourceMepIndex,\
   pau1FsMplsTpMeSinkMepIndex,\
   pau1FsMplsTpMeMpType,\
   pau1FsMplsTpMeMepDirection,\
   pau1FsMplsTpMeProactiveOamPhbTCValue,\
   pau1FsMplsTpMeOnDemandOamPhbTCValue,\
   pau1FsMplsTpMeServiceSignaled,\
   pau1FsMplsTpMeServicePointer,\
   pau1FsMplsTpMeServicePointerLen,\
   pau1FsMplsTpMeRowStatus)\
  {\
  if (pau1FsMplsTpContextId != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId = *(UINT4 *) (pau1FsMplsTpContextId);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpContextId = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMegIndex != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex = *(UINT4 *) (pau1FsMplsTpMegIndex);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMegIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMegIndex = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeIndex != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex = *(UINT4 *) (pau1FsMplsTpMeIndex);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeIndex = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeMpIndex != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex = *(UINT4 *) (pau1FsMplsTpMeMpIndex);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIndex = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeName != NULL)\
  {\
   MEMCPY (pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName, pau1FsMplsTpMeName, *(INT4 *)pau1FsMplsTpMeNameLen);\
   pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen = *(INT4 *)pau1FsMplsTpMeNameLen;\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeName = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeName = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeMpIfIndex != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex = *(INT4 *) (pau1FsMplsTpMeMpIfIndex);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIfIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpIfIndex = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeSourceMepIndex != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex = *(UINT4 *) (pau1FsMplsTpMeSourceMepIndex);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSourceMepIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSourceMepIndex = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeSinkMepIndex != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex = *(UINT4 *) (pau1FsMplsTpMeSinkMepIndex);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSinkMepIndex = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeSinkMepIndex = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeMpType != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType = *(INT4 *) (pau1FsMplsTpMeMpType);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpType = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMpType = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeMepDirection != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection = *(INT4 *) (pau1FsMplsTpMeMepDirection);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMepDirection = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeMepDirection = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeProactiveOamPhbTCValue != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeProactiveOamPhbTCValue = *(INT4 *) (pau1FsMplsTpMeProactiveOamPhbTCValue);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeProactiveOamPhbTCValue = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeProactiveOamPhbTCValue = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeOnDemandOamPhbTCValue != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeOnDemandOamPhbTCValue = *(INT4 *) (pau1FsMplsTpMeOnDemandOamPhbTCValue);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeOnDemandOamPhbTCValue = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeOnDemandOamPhbTCValue = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeServiceSignaled != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServiceSignaled = *(INT4 *) (pau1FsMplsTpMeServiceSignaled);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServiceSignaled = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServiceSignaled = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeServicePointer != NULL)\
  {\
   OamUtlConvertStringtoOid (pOamFsMplsTpMeEntry,\
       (UINT4 *)pau1FsMplsTpMeServicePointer,(INT4 *)pau1FsMplsTpMeServicePointerLen);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeServicePointer = OSIX_FALSE;\
  }\
  if (pau1FsMplsTpMeRowStatus != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus = *(INT4 *) (pau1FsMplsTpMeRowStatus);\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pOamIsSetFsMplsTpMeEntry->bFsMplsTpMeRowStatus = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsMplsTpMeTable 
 using the input given in def file */

#define  OAM_FILL_FSMPLSTPMETABLE_INDEX(pOamFsMplsTpMeEntry,\
   pau1FsMplsTpContextId,\
   pau1FsMplsTpMegIndex,\
   pau1FsMplsTpMeIndex,\
   pau1FsMplsTpMeMpIndex)\
  {\
  if (pau1FsMplsTpContextId != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId = *(UINT4 *) (pau1FsMplsTpContextId);\
  }\
  if (pau1FsMplsTpMegIndex != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex = *(UINT4 *) (pau1FsMplsTpMegIndex);\
  }\
  if (pau1FsMplsTpMeIndex != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex = *(UINT4 *) (pau1FsMplsTpMeIndex);\
  }\
  if (pau1FsMplsTpMeMpIndex != NULL)\
  {\
   pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex = *(UINT4 *) (pau1FsMplsTpMeMpIndex);\
  }\
  }




/* Macro used to convert the input 
  to required datatype for FsMplsTpOamContextName*/

#define  OAM_FILL_FSMPLSTPOAMCONTEXTNAME(pFsMplsTpOamContextName,\
   pau1FsMplsTpOamContextName)\
  {\
  if (pau1FsMplsTpOamContextName != NULL)\
  {\
   STRCPY (pFsMplsTpOamContextName, pau1FsMplsTpOamContextName);\
  }\
  }




/* Macro used to convert the input 
  to required datatype for FsMplsTpOamMegOperStatus*/

#define  OAM_FILL_FSMPLSTPOAMMEGOPERSTATUS(i4FsMplsTpOamMegOperStatus,\
   pau1FsMplsTpOamMegOperStatus)\
  {\
  if (pau1FsMplsTpOamMegOperStatus != NULL)\
  {\
   i4FsMplsTpOamMegOperStatus = *(INT4 *) (pau1FsMplsTpOamMegOperStatus);\
  }\
  }




/* Macro used to convert the input 
  to required datatype for FsMplsTpOamMegSubOperStatus*/

#define  OAM_FILL_FSMPLSTPOAMMEGSUBOPERSTATUS(pFsMplsTpOamMegSubOperStatus,\
   pau1FsMplsTpOamMegSubOperStatus)\
  {\
  if (pau1FsMplsTpOamMegSubOperStatus != NULL)\
  {\
   STRCPY (pFsMplsTpOamMegSubOperStatus, pau1FsMplsTpOamMegSubOperStatus);\
  }\
  }




