/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  
 * $Id: oammacs.h,v 1.4 2011/03/01 05:07:02 siva Exp $
 *  
 * Description: This file contains macro definitions for Oam module.
 **********************************************************************/


#define OAM_TRUE                           1
#define OAM_FALSE                          2

#define OAM_ICC_ID_LEN                     MPLS_ICC_LENGTH
#define OAM_UMC_ID_LEN                     MPLS_UMC_LENGTH

#define OAM_MEG_NAME_MAX_LEN               48
#define OAM_ME_NAME_MAX_LEN                48
#define OAM_IF_NAME_MAX_LEN                48

#define OAM_PHB_TC_VALUE_MIN               1
#define OAM_PHB_TC_VALUE_MAX               6

#define OAM_ELPS_ASSOCIATION_EXISTS        1
#define OAM_MEG_ASSOCIATION_EXISTS         2
#define OAM_PW_ASSOCIATION_EXISTS          3
#define OAM_PROACTIVE_SESSION_EXISTS       4
#define OAM_ELPS_PROACTIVE_SESSION_EXIST   5
#define OAM_ACTIVE_ME_EXIST                6

#define OAM_DEFAULT_CXT_ID                 0

#define OAM_MAX_MEP_INDEX_VALUE            65535

#define OAM_NOTIFY_CALLER_MEG              1
#define OAM_NOTIFY_CALLER_ME               2

#define OAM_ME_INDEX_MIN                   1
#define OAM_ME_INDEX_MAX                   255
#define OAM_MP_INDEX_MIN                   1

#define OAM_MAX_OID_LEN                    256

#define OAM_Z_ASCII_VALUE                  90
#define OAM_9_ASCII_VALUE                  57
#define OAM_A_ASCII_VALUE                  65

#define TUNNEL_TABLE_INDICES               4
#define TUNNEL_BASE_OID_LEN                13
#define PSEUDOWIRE_BASE_OID_LEN            13
