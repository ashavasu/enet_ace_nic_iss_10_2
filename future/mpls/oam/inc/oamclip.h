/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamclip.h,v 1.4 2013/06/26 11:48:58 siva Exp $
*
* This file contains prototypes for functions defined in oamcli.c
*********************************************************************/

typedef struct
{
    UINT4 u4ContextId;
    UINT4 u4MegIndex;
}tOamCliArgs;

INT4 OamCliShowGlobalInfo PROTO ((tCliHandle CliHandle, UINT4 u4ContextId));

INT4 OamCliShowNodeMapInfo PROTO ((tCliHandle CliHandle, UINT4 u4ContextId,
                                   UINT4 u4LocalMapNum, UINT4 u4GlobalId,
                                   UINT4 u4NodeId, UINT4 u4Command));

VOID OamCliPrintNodeMapInfo PROTO ((tCliHandle CliHandle,
                        tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry, 
                        UINT1 u1PrintHeader));

INT4 OamCliShowMegInfo PROTO ((tCliHandle CliHandle, UINT4 u4ContextId,
                               UINT1 *pu1MegName, UINT4 u4Command, 
                               UINT4 u4DisplayMode));

INT4 OamCliPrintMegInfo PROTO ((tCliHandle CliHandle,
                                tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry,
                                UINT4 u4DisplayMode, UINT1 u1PrintHeader));

INT4 OamCliShowMeInfo PROTO ((tCliHandle CliHandle, UINT4 u4ContextId,
                              UINT1 *pu1MegName, UINT1 *pu1MeName,
                              UINT4 u4Command, UINT4 u4DisplayMode));

VOID OamCliPrintMeInfo PROTO ((tCliHandle CliHandle,
                               tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry,
                               UINT4 u4DisplayMode));

VOID OamCliPrintMeId PROTO ((tCliHandle CliHandle,
                             tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry));
