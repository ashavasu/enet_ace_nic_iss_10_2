/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oaminc.h,v 1.2 2011/10/25 09:29:42 siva Exp $
 *
 * Description: This file contains include files of oam module.
 *******************************************************************/

#ifndef __OAMINC_H__
#define __OAMINC_H__ 

#include "lr.h"
#include "cli.h"
#include "mplsutil.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "params.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "oamg.h"

#include "mplcmndb.h"
#include "oamdefn.h"
#include "oamtdfsg.h"
#include "oamtdfs.h"
#include "oammacs.h"
#include "oamtrc.h"

#ifdef __OAMMAIN_C__

#include "oamglob.h"
#include "oamlwg.h"
#include "oamdefg.h"
#include "oamsz.h"
#include "oamwrg.h"

#else

#include "oamextn.h"
#include "oammibclig.h"
#include "oamlwg.h"
#include "oamdefg.h"
#include "oamsz.h"
#include "oamwrg.h"

#endif /* __OAMMAIN_C__*/

#include "mplsapi.h"
#include "oamprot.h"
#include "oamprotg.h"
#include "mpcmnapi.h"
#endif   /* __OAMINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file oaminc.h                       */
/*-----------------------------------------------------------------------*/

