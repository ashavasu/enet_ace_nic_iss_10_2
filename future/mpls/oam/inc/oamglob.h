/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains declaration of global variables 
 *              of oam module.
 *******************************************************************/

#ifndef __OAMGLOBAL_H__
#define __OAMGLOBAL_H__

tOamGlobals gOamGlobals;

#endif  /* __OAMGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file oamglob.h                      */
/*-----------------------------------------------------------------------*/
