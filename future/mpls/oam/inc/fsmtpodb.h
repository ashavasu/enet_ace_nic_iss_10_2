/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmtpodb.h,v 1.1.1.1 2010/10/12 08:42:37 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMTPODB_H
#define _FSMTPODB_H

UINT1 FsMplsTpMegTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsTpMeTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmtpo [] ={1,3,6,1,4,1,2076,13,9};
tSNMP_OID_TYPE fsmtpoOID = {9, fsmtpo};


UINT4 FsMplsTpMegIndex [ ] ={1,3,6,1,4,1,2076,13,9,1,2,1,1};
UINT4 FsMplsTpMegName [ ] ={1,3,6,1,4,1,2076,13,9,1,2,1,2};
UINT4 FsMplsTpMegOperatorType [ ] ={1,3,6,1,4,1,2076,13,9,1,2,1,3};
UINT4 FsMplsTpMegIdIcc [ ] ={1,3,6,1,4,1,2076,13,9,1,2,1,4};
UINT4 FsMplsTpMegIdUmc [ ] ={1,3,6,1,4,1,2076,13,9,1,2,1,5};
UINT4 FsMplsTpMegServiceType [ ] ={1,3,6,1,4,1,2076,13,9,1,2,1,6};
UINT4 FsMplsTpMegMpLocation [ ] ={1,3,6,1,4,1,2076,13,9,1,2,1,7};
UINT4 FsMplsTpMegRowStatus [ ] ={1,3,6,1,4,1,2076,13,9,1,2,1,8};
UINT4 FsMplsTpMeIndex [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,1};
UINT4 FsMplsTpMeMpIndex [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,2};
UINT4 FsMplsTpMeName [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,3};
UINT4 FsMplsTpMeMpIfIndex [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,4};
UINT4 FsMplsTpMeSourceMepIndex [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,5};
UINT4 FsMplsTpMeSinkMepIndex [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,6};
UINT4 FsMplsTpMeMpType [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,7};
UINT4 FsMplsTpMeMepDirection [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,8};
UINT4 FsMplsTpMeProactiveOamSessIndex [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,9};
UINT4 FsMplsTpMeProactiveOamPhbTCValue [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,10};
UINT4 FsMplsTpMeOnDemandOamPhbTCValue [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,11};
UINT4 FsMplsTpMeServiceSignaled [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,12};
UINT4 FsMplsTpMeServicePointer [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,13};
UINT4 FsMplsTpMeRowStatus [ ] ={1,3,6,1,4,1,2076,13,9,1,3,1,14};




tMbDbEntry fsmtpoMibEntry[]= {

{{13,FsMplsTpMegIndex}, GetNextIndexFsMplsTpMegTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsTpMegTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsTpMegName}, GetNextIndexFsMplsTpMegTable, FsMplsTpMegNameGet, FsMplsTpMegNameSet, FsMplsTpMegNameTest, FsMplsTpMegTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsTpMegTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsTpMegOperatorType}, GetNextIndexFsMplsTpMegTable, FsMplsTpMegOperatorTypeGet, FsMplsTpMegOperatorTypeSet, FsMplsTpMegOperatorTypeTest, FsMplsTpMegTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMegTableINDEX, 2, 0, 0, "1"},

{{13,FsMplsTpMegIdIcc}, GetNextIndexFsMplsTpMegTable, FsMplsTpMegIdIccGet, FsMplsTpMegIdIccSet, FsMplsTpMegIdIccTest, FsMplsTpMegTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsTpMegTableINDEX, 2, 0, 0, ""},

{{13,FsMplsTpMegIdUmc}, GetNextIndexFsMplsTpMegTable, FsMplsTpMegIdUmcGet, FsMplsTpMegIdUmcSet, FsMplsTpMegIdUmcTest, FsMplsTpMegTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsTpMegTableINDEX, 2, 0, 0, ""},

{{13,FsMplsTpMegServiceType}, GetNextIndexFsMplsTpMegTable, FsMplsTpMegServiceTypeGet, FsMplsTpMegServiceTypeSet, FsMplsTpMegServiceTypeTest, FsMplsTpMegTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMegTableINDEX, 2, 0, 0, "1"},

{{13,FsMplsTpMegMpLocation}, GetNextIndexFsMplsTpMegTable, FsMplsTpMegMpLocationGet, FsMplsTpMegMpLocationSet, FsMplsTpMegMpLocationTest, FsMplsTpMegTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMegTableINDEX, 2, 0, 0, "1"},

{{13,FsMplsTpMegRowStatus}, GetNextIndexFsMplsTpMegTable, FsMplsTpMegRowStatusGet, FsMplsTpMegRowStatusSet, FsMplsTpMegRowStatusTest, FsMplsTpMegTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMegTableINDEX, 2, 0, 1, NULL},

{{13,FsMplsTpMeIndex}, GetNextIndexFsMplsTpMeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsTpMeTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTpMeMpIndex}, GetNextIndexFsMplsTpMeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsTpMeTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTpMeName}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeNameGet, FsMplsTpMeNameSet, FsMplsTpMeNameTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTpMeMpIfIndex}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeMpIfIndexGet, FsMplsTpMeMpIfIndexSet, FsMplsTpMeMpIfIndexTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 0, "0"},

{{13,FsMplsTpMeSourceMepIndex}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeSourceMepIndexGet, FsMplsTpMeSourceMepIndexSet, FsMplsTpMeSourceMepIndexTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 0, "0"},

{{13,FsMplsTpMeSinkMepIndex}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeSinkMepIndexGet, FsMplsTpMeSinkMepIndexSet, FsMplsTpMeSinkMepIndexTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 0, "0"},

{{13,FsMplsTpMeMpType}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeMpTypeGet, FsMplsTpMeMpTypeSet, FsMplsTpMeMpTypeTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 0, "1"},

{{13,FsMplsTpMeMepDirection}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeMepDirectionGet, FsMplsTpMeMepDirectionSet, FsMplsTpMeMepDirectionTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 0, "2"},

{{13,FsMplsTpMeProactiveOamSessIndex}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeProactiveOamSessIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTpMeTableINDEX, 4, 0, 0, "0"},

{{13,FsMplsTpMeProactiveOamPhbTCValue}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeProactiveOamPhbTCValueGet, FsMplsTpMeProactiveOamPhbTCValueSet, FsMplsTpMeProactiveOamPhbTCValueTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 0, "1"},

{{13,FsMplsTpMeOnDemandOamPhbTCValue}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeOnDemandOamPhbTCValueGet, FsMplsTpMeOnDemandOamPhbTCValueSet, FsMplsTpMeOnDemandOamPhbTCValueTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 0, "1"},

{{13,FsMplsTpMeServiceSignaled}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeServiceSignaledGet, FsMplsTpMeServiceSignaledSet, FsMplsTpMeServiceSignaledTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 0, "2"},

{{13,FsMplsTpMeServicePointer}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeServicePointerGet, FsMplsTpMeServicePointerSet, FsMplsTpMeServicePointerTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTpMeRowStatus}, GetNextIndexFsMplsTpMeTable, FsMplsTpMeRowStatusGet, FsMplsTpMeRowStatusSet, FsMplsTpMeRowStatusTest, FsMplsTpMeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpMeTableINDEX, 4, 0, 1, NULL},
};
tMibData fsmtpoEntry = { 22, fsmtpoMibEntry };

#endif /* _FSMTPODB_H */

