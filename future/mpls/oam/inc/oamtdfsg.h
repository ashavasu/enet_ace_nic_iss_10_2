/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: oamtdfsg.h,v 1.6 2012/08/17 09:13:28 siva Exp $
*
* Description: This file contains the automatically generated 
*              data structures that are required for Oam module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in FsMplsTpGlobalConfigEntry */
#ifndef __OAMTDFSG_H__
#define __OAMTDFSG_H__

typedef struct
{
 BOOL1  bFsMplsTpContextId;
 BOOL1  bFsMplsTpOamModuleStatus;
 BOOL1  bFsMplsTpGlobalId;
 BOOL1  bFsMplsTpIcc;
 BOOL1  bFsMplsTpNodeIdentifier;
 BOOL1  bFsMplsTpTraceLevel;
    BOOL1  bFsMplsTpNotificationEnable;
} tOamIsSetFsMplsTpGlobalConfigEntry;


/* Structure used by Oam protocol for FsMplsTpGlobalConfigEntry */

typedef struct
{
 tRBNodeEmbd  FsMplsTpGlobalConfigTableNode;
 UINT4 u4FsMplsTpContextId;
 UINT4 u4FsMplsTpGlobalId;
 UINT4 u4FsMplsTpNodeIdentifier;
 UINT4 u4FsMplsTpTraceLevel;
 INT4 i4FsMplsTpOamModuleStatus;
 INT4 i4FsMplsTpErrorCode;
    INT4 i4FsMplsTpNotificationEnable;
    INT4 i4FsMplsTpIccLen;
 UINT1 au1FsMplsTpIcc[6];
 UINT1 au1Align[2];
} tOamMibFsMplsTpGlobalConfigEntry;
/* Structure used by CLI to indicate which 
 all objects to be set in FsMplsTpNodeMapEntry */

typedef struct
{
 BOOL1  bFsMplsTpContextId;
 BOOL1  bFsMplsTpNodeMapLocalNum;
 BOOL1  bFsMplsTpNodeMapGlobalId;
 BOOL1  bFsMplsTpNodeMapNodeId;
 BOOL1  bFsMplsTpNodeMapRowStatus;
} tOamIsSetFsMplsTpNodeMapEntry;


/* Structure used by Oam protocol for FsMplsTpNodeMapEntry */

typedef struct
{
 tRBNodeEmbd  FsMplsTpNodeMapTableNode;
 UINT4 u4FsMplsTpContextId;
 UINT4 u4FsMplsTpNodeMapLocalNum;
 UINT4 u4FsMplsTpNodeMapGlobalId;
 UINT4 u4FsMplsTpNodeMapNodeId;
 INT4 i4FsMplsTpNodeMapRowStatus;
} tOamMibFsMplsTpNodeMapEntry;
/* Structure used by CLI to indicate which 
 all objects to be set in FsMplsTpMegEntry */

typedef struct
{
 BOOL1  bFsMplsTpContextId;
 BOOL1  bFsMplsTpMegIndex;
 BOOL1  bFsMplsTpMegName;
 BOOL1  bFsMplsTpMegOperatorType;
 BOOL1  bFsMplsTpMegIdIcc;
 BOOL1  bFsMplsTpMegIdUmc;
 BOOL1  bFsMplsTpMegServiceType;
 BOOL1  bFsMplsTpMegMpLocation;
 BOOL1  bFsMplsTpMegRowStatus;
} tOamIsSetFsMplsTpMegEntry;


/* Structure used by Oam protocol for FsMplsTpMegEntry */

typedef struct
{
 tRBNodeEmbd  FsMplsTpMegTableNode;
 UINT4 u4FsMplsTpContextId;
 UINT4 u4FsMplsTpMegIndex;
 INT4 i4FsMplsTpMegOperatorType;
 INT4 i4FsMplsTpMegServiceType;
 INT4 i4FsMplsTpMegMpLocation;
 INT4 i4FsMplsTpMegRowStatus;
 INT4 i4FsMplsTpMegNameLen;
 INT4 i4FsMplsTpMegIdIccLen;
 INT4 i4FsMplsTpMegIdUmcLen;
 UINT1 au1FsMplsTpMegName[48];
 UINT1 au1FsMplsTpMegIdIcc[7];
 UINT1 au1FsMplsTpMegIdUmc[8];
 UINT1 au1Align[1];
} tOamMibFsMplsTpMegEntry;
/* Structure used by CLI to indicate which 
 all objects to be set in FsMplsTpMeEntry */

typedef struct
{
 BOOL1  bFsMplsTpContextId;
 BOOL1  bFsMplsTpMegIndex;
 BOOL1  bFsMplsTpMeIndex;
 BOOL1  bFsMplsTpMeMpIndex;
 BOOL1  bFsMplsTpMeName;
 BOOL1  bFsMplsTpMeMpIfIndex;
 BOOL1  bFsMplsTpMeSourceMepIndex;
 BOOL1  bFsMplsTpMeSinkMepIndex;
 BOOL1  bFsMplsTpMeMpType;
 BOOL1  bFsMplsTpMeMepDirection;
 BOOL1  bFsMplsTpMeProactiveOamPhbTCValue;
 BOOL1  bFsMplsTpMeOnDemandOamPhbTCValue;
 BOOL1  bFsMplsTpMeServiceSignaled;
 BOOL1  bFsMplsTpMeServicePointer;
 BOOL1  bFsMplsTpMeRowStatus;
} tOamIsSetFsMplsTpMeEntry;


/* Structure used by Oam protocol for FsMplsTpMeEntry */

typedef struct
{
 tRBNodeEmbd  FsMplsTpMeTableNode;
 UINT4 u4FsMplsTpContextId;
 UINT4 u4FsMplsTpMegIndex;
 UINT4 u4FsMplsTpMeIndex;
 UINT4 u4FsMplsTpMeMpIndex;
 UINT4 u4FsMplsTpMeSourceMepIndex;
 UINT4 u4FsMplsTpMeSinkMepIndex;
 UINT4 u4FsMplsTpMeProactiveOamSessIndex;
 UINT4 au4FsMplsTpMeServicePointer[256];
 INT4 i4FsMplsTpMeMpIfIndex;
 INT4 i4FsMplsTpMeMpType;
 INT4 i4FsMplsTpMeMepDirection;
 INT4 i4FsMplsTpMeProactiveOamPhbTCValue;
 INT4 i4FsMplsTpMeOnDemandOamPhbTCValue;
 INT4 i4FsMplsTpMeServiceSignaled;
 INT4 i4FsMplsTpMeRowStatus;
 INT4 i4FsMplsTpMeNameLen;
 INT4 i4FsMplsTpMeServicePointerLen;
 UINT1 au1FsMplsTpMeName[48];
} tOamMibFsMplsTpMeEntry;
#endif
