/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: oamsz.h,v 1.3 2012/03/19 11:28:23 siva Exp $
 *
 ********************************************************************/

enum {
    MAX_MPLSOAM_APIININFO_SIZING_ID,
    MAX_MPLSOAM_APIOUTINFO_SIZING_ID,
    MAX_MPLSOAM_FSMPLSTPGLOBALCONFIGTABLE_SIZING_ID,
    MAX_MPLSOAM_FSMPLSTPMEGTABLE_SIZING_ID,
    MAX_MPLSOAM_FSMPLSTPMETABLE_SIZING_ID,
    MAX_MPLSOAM_FSMPLSTPNODEMAPTABLE_SIZING_ID,
    MPLSOAM_MAX_SIZING_ID
};


#ifdef  _MPLSOAMSZ_C
tMemPoolId MPLSOAMMemPoolIds[ MPLSOAM_MAX_SIZING_ID];
INT4  MplsoamSizingMemCreateMemPools(VOID);
VOID  MplsoamSizingMemDeleteMemPools(VOID);
INT4  MplsoamSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _MPLSOAMSZ_C  */
extern tMemPoolId MPLSOAMMemPoolIds[ ];
extern INT4  MplsoamSizingMemCreateMemPools(VOID);
extern VOID  MplsoamSizingMemDeleteMemPools(VOID);
extern INT4  MplsoamSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _MPLSOAMSZ_C  */


#ifdef  _MPLSOAMSZ_C
tFsModSizingParams FsMPLSOAMSizingParams [] = {
{ "tMplsApiInInfo", "MAX_MPLSOAM_APIININFO", sizeof(tMplsApiInInfo),MAX_MPLSOAM_APIININFO, MAX_MPLSOAM_APIININFO,0 },
{ "tMplsApiOutInfo", "MAX_MPLSOAM_APIOUTINFO", sizeof(tMplsApiOutInfo),MAX_MPLSOAM_APIOUTINFO, MAX_MPLSOAM_APIOUTINFO,0 },
{ "tOamFsMplsTpGlobalConfigEntry", "MAX_MPLSOAM_FSMPLSTPGLOBALCONFIGTABLE", sizeof(tOamFsMplsTpGlobalConfigEntry),MAX_MPLSOAM_FSMPLSTPGLOBALCONFIGTABLE, MAX_MPLSOAM_FSMPLSTPGLOBALCONFIGTABLE,0 },
{ "tOamFsMplsTpMegEntry", "MAX_MPLSOAM_FSMPLSTPMEGTABLE", sizeof(tOamFsMplsTpMegEntry),MAX_MPLSOAM_FSMPLSTPMEGTABLE, MAX_MPLSOAM_FSMPLSTPMEGTABLE,0 },
{ "tOamFsMplsTpMeEntry", "MAX_MPLSOAM_FSMPLSTPMETABLE", sizeof(tOamFsMplsTpMeEntry),MAX_MPLSOAM_FSMPLSTPMETABLE, MAX_MPLSOAM_FSMPLSTPMETABLE,0 },
{ "tOamFsMplsTpNodeMapEntry", "MAX_MPLSOAM_FSMPLSTPNODEMAPTABLE", sizeof(tOamFsMplsTpNodeMapEntry),MAX_MPLSOAM_FSMPLSTPNODEMAPTABLE, MAX_MPLSOAM_FSMPLSTPNODEMAPTABLE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _MPLSOAMSZ_C  */
extern tFsModSizingParams FsMPLSOAMSizingParams [];
#endif /*  _MPLSOAMSZ_C  */


