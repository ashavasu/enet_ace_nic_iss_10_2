
 /* $Id: oamclig.h,v 1.4 2013/12/07 10:58:55 siva Exp $*/
INT4 OamCliSetFsMplsTpGlobalConfigTable (tCliHandle CliHandle,tOamFsMplsTpGlobalConfigEntry * pOamSetFsMplsTpGlobalConfigEntry,tOamIsSetFsMplsTpGlobalConfigEntry * pOamIsSetFsMplsTpGlobalConfigEntry);
INT4 OamCliSetFsMplsTpNodeMapTable (tCliHandle CliHandle,tOamFsMplsTpNodeMapEntry * pOamSetFsMplsTpNodeMapEntry,tOamIsSetFsMplsTpNodeMapEntry * pOamIsSetFsMplsTpNodeMapEntry);
INT4 OamCliSetFsMplsTpMegTable (tCliHandle CliHandle,tOamFsMplsTpMegEntry * pOamSetFsMplsTpMegEntry,tOamIsSetFsMplsTpMegEntry * pOamIsSetFsMplsTpMegEntry);
INT4 OamCliSetFsMplsTpMeTable (tCliHandle CliHandle,tOamFsMplsTpMeEntry * pOamSetFsMplsTpMeEntry,tOamIsSetFsMplsTpMeEntry * pOamIsSetFsMplsTpMeEntry);
INT4 OamCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel);
