/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamlwg.h,v 1.3 2011/01/24 09:16:18 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMplsTpGlobalConfigTable. */
INT1
nmhValidateIndexInstanceFsMplsTpGlobalConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTpGlobalConfigTable  */

INT1
nmhGetFirstIndexFsMplsTpGlobalConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTpGlobalConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTpOamModuleStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpGlobalId ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTpIcc ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTpNodeIdentifier ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTpErrorCode ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpTraceLevel ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTpNotificationEnable ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTpOamModuleStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsMplsTpGlobalId ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTpIcc ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsTpNodeIdentifier ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTpTraceLevel ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTpNotificationEnable ARG_LIST((UINT4  ,INT4 ));
/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTpOamModuleStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTpGlobalId ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTpIcc ARG_LIST((UINT4 * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsTpNodeIdentifier ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTpTraceLevel ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTpNotificationEnable ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTpGlobalConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsTpNodeMapTable. */
INT1
nmhValidateIndexInstanceFsMplsTpNodeMapTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTpNodeMapTable  */

INT1
nmhGetFirstIndexFsMplsTpNodeMapTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTpNodeMapTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTpNodeMapGlobalId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTpNodeMapNodeId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTpNodeMapRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTpNodeMapGlobalId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTpNodeMapNodeId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTpNodeMapRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTpNodeMapGlobalId ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTpNodeMapNodeId ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTpNodeMapRowStatus ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTpNodeMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsTpMegTable. */
INT1
nmhValidateIndexInstanceFsMplsTpMegTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTpMegTable  */

INT1
nmhGetFirstIndexFsMplsTpMegTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTpMegTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTpMegName ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTpMegOperatorType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpMegIdIcc ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTpMegIdUmc ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTpMegServiceType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpMegMpLocation ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpMegRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTpMegName ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsTpMegOperatorType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTpMegIdIcc ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsTpMegIdUmc ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsTpMegServiceType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTpMegMpLocation ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTpMegRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTpMegName ARG_LIST((UINT4 * , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsTpMegOperatorType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTpMegIdIcc ARG_LIST((UINT4 * , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsTpMegIdUmc ARG_LIST((UINT4 * , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsTpMegServiceType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTpMegMpLocation ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTpMegRowStatus ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTpMegTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsTpMeTable. */
INT1
nmhValidateIndexInstanceFsMplsTpMeTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTpMeTable  */

INT1
nmhGetFirstIndexFsMplsTpMeTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTpMeTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTpMeName ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTpMeMpIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpMeSourceMepIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTpMeSinkMepIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTpMeMpType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpMeMepDirection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpMeProactiveOamSessIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTpMeProactiveOamPhbTCValue ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpMeOnDemandOamPhbTCValue ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpMeServiceSignaled ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTpMeServicePointer ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsMplsTpMeRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTpMeName ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsTpMeMpIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTpMeSourceMepIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTpMeSinkMepIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTpMeMpType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTpMeMepDirection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTpMeProactiveOamPhbTCValue ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTpMeOnDemandOamPhbTCValue ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTpMeServiceSignaled ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTpMeServicePointer ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsMplsTpMeRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTpMeName ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsTpMeMpIfIndex ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTpMeSourceMepIndex ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTpMeSinkMepIndex ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTpMeMpType ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTpMeMepDirection ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTpMeProactiveOamPhbTCValue ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTpMeOnDemandOamPhbTCValue ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTpMeServiceSignaled ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTpMeServicePointer ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsMplsTpMeRowStatus ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTpMeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto type for Low Level GET Routine All Objects.  */

/* Proto type for Low Level GET Routine All Objects.  */
