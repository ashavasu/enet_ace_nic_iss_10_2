/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * Description: This file contains extern declaration of global 
 *              variables of the oam module.
 *******************************************************************/

#ifndef __OAMEXTN_H__
#define __OAMEXTN_H__

PUBLIC tOamGlobals gOamGlobals;

#endif/*__OAMEXTN_H__*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file oamextn.h                      */
/*-----------------------------------------------------------------------*/

