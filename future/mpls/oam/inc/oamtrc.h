/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: oamtrc.h,v 1.3 2013/12/07 10:58:55 siva Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/


#ifndef __OAMTRC_H__
#define __OAMTRC_H__

#define  OAM_TRC_FLAG  gOamGlobals.u4OamTrc
#define  OAM_TRC_LVL  gOamGlobals.u4OamTrcLvl 
#define  OAM_NAME      "OAM"               

#ifdef OAM_TRACE_WANTED

#define  OAM_TRC(x)       \
        OamTrcPrint( __FILE__, __LINE__, OamTrc x)
#define  OAM_TRC_FUNC(x)  \
        OamTrcPrint( __FILE__, __LINE__, OamTrc x)
#define  OAM_TRC_CRIT(x)  \
        OamTrcPrint( __FILE__, __LINE__, OamTrc x)
#define  OAM_TRC_PKT(x)   \
        OamTrcWrite( OamTrc x)
#else /* OAM_TRACE_WANTED */

#define  OAM_TRC(x) 
#define  OAM_TRC_FUNC(x)
#define  OAM_TRC_CRIT(x)
#define  OAM_TRC_PKT(x)

#endif /* OAM_TRACE_WANTED */


#define  OAM_FN_ENTRY       0x100000
#define  OAM_FN_EXIT        0x100000
#define  OAM_MGMT_TRC       0x010000
#define  OAM_MAIN_TRC       0x001000
#define  OAM_UTIL_TRC       0x000100
#define  OAM_RESOURCE_TRC   0x000010
#define  OAM_FAILURE_TRC    0x000001
#define  OAM_ALL_TRC        OAM_FN_ENTRY |\
                            OAM_FN_EXIT  |\
                            OAM_MGMT_TRC  |\
                            OAM_MAIN_TRC |\
                            OAM_UTIL_TRC  |\
                            OAM_RESOURCE_TRC |\
                            OAM_FAILURE_TRC


#endif /* _OAMTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  oamtrc.h                      */
/*-----------------------------------------------------------------------*/
