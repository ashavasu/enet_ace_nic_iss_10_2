/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oamprotg.h,v 1.3 2014/07/19 12:52:32 siva Exp $
*
* This file contains prototypes for functions defined in Oam.
*********************************************************************/




PUBLIC INT4 OamUtlCreateRBTree PROTO ((VOID));

VOID  RegisterFSMPTP PROTO ((VOID));
VOID  UnRegisterFSMPTP PROTO ((VOID));
VOID  RegisterFSMTPO PROTO ((VOID));
VOID  UnRegisterFSMTPO PROTO ((VOID));
PUBLIC INT4 OamFsMplsTpGlobalConfigEntryCreate PROTO ((VOID));

PUBLIC INT4 FsMplsTpGlobalConfigTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 OamFsMplsTpNodeMapEntryCreate PROTO ((VOID));

PUBLIC INT4 FsMplsTpNodeMapTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 OamFsMplsTpMegEntryCreate PROTO ((VOID));

PUBLIC INT4 FsMplsTpMegTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 OamFsMplsTpMeTableCreate PROTO ((VOID));

PUBLIC INT4 FsMplsTpMeTableRBCmp PROTO ((tRBElem *, tRBElem *));


INT4 OamUtlConvertStringtoOid PROTO((tOamFsMplsTpMeEntry *, UINT4 *, INT4 *));
tOamFsMplsTpGlobalConfigEntry * OamFsMplsTpGlobalConfigEntryCreateApi PROTO ((tOamFsMplsTpGlobalConfigEntry *));
tOamFsMplsTpNodeMapEntry * OamFsMplsTpNodeMapEntryCreateApi PROTO ((tOamFsMplsTpNodeMapEntry *));
tOamFsMplsTpMegEntry * OamFsMplsTpMegEntryCreateApi PROTO ((tOamFsMplsTpMegEntry *));
tOamFsMplsTpMeEntry * OamFsMplsTpMeTableCreateApi PROTO ((tOamFsMplsTpMeEntry *));
tOamFsMplsTpGlobalConfigEntry * OamGetFirstFsMplsTpGlobalConfigTable PROTO ((VOID));
tOamFsMplsTpGlobalConfigEntry * OamGetNextFsMplsTpGlobalConfigTable PROTO ((tOamFsMplsTpGlobalConfigEntry *));
tOamFsMplsTpGlobalConfigEntry * OamGetFsMplsTpGlobalConfigTable PROTO ((tOamFsMplsTpGlobalConfigEntry *));
INT4 OamGetAllFsMplsTpGlobalConfigTable PROTO ((tOamFsMplsTpGlobalConfigEntry *));
INT4 OamGetAllUtlFsMplsTpGlobalConfigTable PROTO((tOamFsMplsTpGlobalConfigEntry *, tOamFsMplsTpGlobalConfigEntry *));
INT4 OamTestAllFsMplsTpGlobalConfigTable PROTO ((UINT4 *, tOamFsMplsTpGlobalConfigEntry *, tOamIsSetFsMplsTpGlobalConfigEntry *));
INT4 OamSetAllFsMplsTpGlobalConfigTable PROTO ((tOamFsMplsTpGlobalConfigEntry *, tOamIsSetFsMplsTpGlobalConfigEntry *));
INT4 OamSetAllFsMplsTpGlobalConfigTableTrigger PROTO ((tOamFsMplsTpGlobalConfigEntry *, tOamIsSetFsMplsTpGlobalConfigEntry *, INT4));
INT4 FsMplsTpGlobalConfigTableFilterInputs PROTO ((tOamFsMplsTpGlobalConfigEntry *, tOamFsMplsTpGlobalConfigEntry *, tOamIsSetFsMplsTpGlobalConfigEntry *));
INT4 OamUtilUpdateFsMplsTpGlobalConfigTable PROTO ((tOamFsMplsTpGlobalConfigEntry *, tOamFsMplsTpGlobalConfigEntry *, tOamIsSetFsMplsTpGlobalConfigEntry *));
tOamFsMplsTpNodeMapEntry * OamGetFirstFsMplsTpNodeMapTable PROTO ((VOID));
tOamFsMplsTpNodeMapEntry * OamGetNextFsMplsTpNodeMapTable PROTO ((tOamFsMplsTpNodeMapEntry *));
tOamFsMplsTpNodeMapEntry * OamGetFsMplsTpNodeMapTable PROTO ((tOamFsMplsTpNodeMapEntry *));
INT4 OamGetAllFsMplsTpNodeMapTable PROTO ((tOamFsMplsTpNodeMapEntry *));
INT4 OamGetAllUtlFsMplsTpNodeMapTable PROTO((tOamFsMplsTpNodeMapEntry *, tOamFsMplsTpNodeMapEntry *));
INT4 OamTestAllFsMplsTpNodeMapTable PROTO ((UINT4 *, tOamFsMplsTpNodeMapEntry *, tOamIsSetFsMplsTpNodeMapEntry *, INT4 , INT4));
INT4 OamSetAllFsMplsTpNodeMapTable PROTO ((tOamFsMplsTpNodeMapEntry *, tOamIsSetFsMplsTpNodeMapEntry *, INT4 , INT4 ));

INT4 OamInitializeFsMplsTpNodeMapTable PROTO ((tOamFsMplsTpNodeMapEntry *));

INT4 OamInitializeMibFsMplsTpNodeMapTable PROTO ((tOamFsMplsTpNodeMapEntry *));
INT4 OamSetAllFsMplsTpNodeMapTableTrigger PROTO ((tOamFsMplsTpNodeMapEntry *, tOamIsSetFsMplsTpNodeMapEntry *, INT4));
INT4 FsMplsTpNodeMapTableFilterInputs PROTO ((tOamFsMplsTpNodeMapEntry *, tOamFsMplsTpNodeMapEntry *, tOamIsSetFsMplsTpNodeMapEntry *));
INT4 OamUtilUpdateFsMplsTpNodeMapTable PROTO ((tOamFsMplsTpNodeMapEntry *, tOamFsMplsTpNodeMapEntry *, tOamIsSetFsMplsTpNodeMapEntry *));
tOamFsMplsTpMegEntry * OamGetFirstFsMplsTpMegTable PROTO ((VOID));
tOamFsMplsTpMegEntry * OamGetNextFsMplsTpMegTable PROTO ((tOamFsMplsTpMegEntry *));
tOamFsMplsTpMegEntry * OamGetFsMplsTpMegTable PROTO ((tOamFsMplsTpMegEntry *));
INT4 OamGetAllFsMplsTpMegTable PROTO ((tOamFsMplsTpMegEntry *));
INT4 OamGetAllUtlFsMplsTpMegTable PROTO((tOamFsMplsTpMegEntry *, tOamFsMplsTpMegEntry *));
INT4 OamTestAllFsMplsTpMegTable PROTO ((UINT4 *, tOamFsMplsTpMegEntry *, tOamIsSetFsMplsTpMegEntry *, INT4 , INT4));
INT4 OamSetAllFsMplsTpMegTable PROTO ((tOamFsMplsTpMegEntry *, tOamIsSetFsMplsTpMegEntry *, INT4 , INT4 ));

INT4 OamInitializeFsMplsTpMegTable PROTO ((tOamFsMplsTpMegEntry *));

INT4 OamInitializeMibFsMplsTpMegTable PROTO ((tOamFsMplsTpMegEntry *));
INT4 OamSetAllFsMplsTpMegTableTrigger PROTO ((tOamFsMplsTpMegEntry *, tOamIsSetFsMplsTpMegEntry *, INT4));
INT4 FsMplsTpMegTableFilterInputs PROTO ((tOamFsMplsTpMegEntry *, tOamFsMplsTpMegEntry *, tOamIsSetFsMplsTpMegEntry *));
INT4 OamUtilUpdateFsMplsTpMegTable PROTO ((tOamFsMplsTpMegEntry *, tOamFsMplsTpMegEntry *, tOamFsMplsTpMegEntry *, tOamIsSetFsMplsTpMegEntry *));
tOamFsMplsTpMeEntry * OamGetFirstFsMplsTpMeTable PROTO ((VOID));
tOamFsMplsTpMeEntry * OamGetNextFsMplsTpMeTable PROTO ((tOamFsMplsTpMeEntry *));
tOamFsMplsTpMeEntry * OamGetFsMplsTpMeTable PROTO ((tOamFsMplsTpMeEntry *));
INT4 OamGetAllFsMplsTpMeTable PROTO ((tOamFsMplsTpMeEntry *));
INT4 OamGetAllUtlFsMplsTpMeTable PROTO((tOamFsMplsTpMeEntry *, tOamFsMplsTpMeEntry *));
INT4 OamTestAllFsMplsTpMeTable PROTO ((UINT4 *, tOamFsMplsTpMeEntry *, tOamIsSetFsMplsTpMeEntry *, INT4 , INT4));
INT4 OamSetAllFsMplsTpMeTable PROTO ((tOamFsMplsTpMeEntry *, tOamIsSetFsMplsTpMeEntry *, INT4 , INT4 ));

INT4 OamInitializeFsMplsTpMeTable PROTO ((tOamFsMplsTpMeEntry *));

INT4 OamInitializeMibFsMplsTpMeTable PROTO ((tOamFsMplsTpMeEntry *));
INT4 OamSetAllFsMplsTpMeTableTrigger PROTO ((tOamFsMplsTpMeEntry *, tOamIsSetFsMplsTpMeEntry *, INT4));
INT4 FsMplsTpMeTableFilterInputs PROTO ((tOamFsMplsTpMeEntry *, tOamFsMplsTpMeEntry *, tOamIsSetFsMplsTpMeEntry *));
INT4 OamUtilUpdateFsMplsTpMeTable PROTO ((tOamFsMplsTpMeEntry *, tOamFsMplsTpMeEntry *, tOamIsSetFsMplsTpMeEntry *));
INT4 OamGetFsMplsTpOamContextName PROTO ((UINT1 *));
INT4 OamGetFsMplsTpOamMegOperStatus PROTO ((INT4 *));
INT4 OamGetFsMplsTpOamMegSubOperStatus PROTO ((UINT1 *));

PUBLIC INT4 OamLock (VOID);
PUBLIC INT4 OamUnLock (VOID);

