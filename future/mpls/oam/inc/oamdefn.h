/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: oamdefn.h,v 1.4 2010/10/31 21:59:02 prabuc Exp $
 *
 *
 * Description: This file contains definitions for oam module.
 *******************************************************************/

#ifndef __OAMDEFN_H__
#define __OAMDEFN_H__

#define OAM_SUCCESS        (OSIX_SUCCESS)
#define OAM_FAILURE        (OSIX_FAILURE)

#define  OAM_MUT_EXCL_SEM_NAME          (const UINT1 *) "OAMM"
#define  OAM_SEM_CREATE_INIT_CNT        1


#define  OAM_ENABLED                    1
#define  OAM_DISABLED                   2

#define  OAM_MEG_OPER_UP                1
#define  OAM_MEG_OPER_DOWN              2

#define  SNMP_ME_OPER_STATUS_MEG_DOWN    0x80
#define  SNMP_ME_OPER_STATUS_ME_DOWN     0x40
#define  SNMP_ME_OPER_STATUS_OAM_DOWN    0x20
#define  SNMP_ME_OPER_STATUS_PATH_DOWN   0x10

#define  MAX_OAM_DUMMY                  1

#define  OamMainTaskLock    CmnLock 
#define  OamMainTaskUnLock  CmnUnLock

#define  OAM_LOCK          OamMainTaskLock ()
#define  OAM_UNLOCK        OamMainTaskUnLock ()

#endif  /* __OAMDEFN_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file oamdefn.h                      */
/*-----------------------------------------------------------------------*/

