/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: oamtdfs.h,v 1.5 2013/12/07 10:58:55 siva Exp $
*
* Description: This file contains type definitions for Oam module.
*********************************************************************/
#ifndef __OAMTDFS_H__
#define __OAMTDFS_H__

/*structure for all scalars in the mib*/
typedef struct
{

 tRBTree   FsMplsTpGlobalConfigTable;
 tRBTree   FsMplsTpNodeMapTable;
 tRBTree   FsMplsTpMegTable;
 tRBTree   FsMplsTpMeTable; 
 INT4      i4FsMplsTpOamContextNameLen;
 UINT1     au1FsMplsTpOamContextName[32];
 INT4      i4FsMplsTpOamMegOperStatus;
 INT4      i4FsMplsTpOamMegSubOperStatusLen;
 UINT1     au1FsMplsTpOamMegSubOperStatus[256];

} tOamGlbMib;



typedef struct
{
 tOamMibFsMplsTpGlobalConfigEntry       MibObject;
} tOamFsMplsTpGlobalConfigEntry;


typedef struct
{
 tOamMibFsMplsTpNodeMapEntry       MibObject;
    tRBNodeEmbd                       FsMplsTpGlobalIdNodeIdTableNode;
} tOamFsMplsTpNodeMapEntry;


typedef struct
{
 tOamMibFsMplsTpMegEntry       MibObject;
    tRBNodeEmbd                   FsMplsTpMegNameTableNode;
    tRBNodeEmbd                   FsMplsTpMegIccIdTableNode; /* ICC MEG Id based
                                                                RBTree Node */
} tOamFsMplsTpMegEntry;


typedef struct
{
 tOamMibFsMplsTpMeEntry       MibObject;
 UINT4                        u4FsMplsTpMeOperStatus;
 UINT4                        u4FsMplsTpMeApps;
    UINT4                        u4FsMplsTpPwVcId;
} tOamFsMplsTpMeEntry;

/* -------------------------------------------------------
 *
 *                OAM   Data Structures
 *
 * ------------------------------------------------------*/

typedef struct OAM_GLOBALS {
 UINT4               u4OamTrc;
 UINT4               u4OamTrcLvl; 
 tOamGlbMib           OamGlbMib;
    tRBTree             FsMplsTpMegNameTable;
 tRBTree             OamGblNodeDualSearchTree;
    tRBTree             FsMplsTpMegIccIdTable; /* RBTree based on 
                                                  ICC MEG Id (ICC and UMC) */
} tOamGlobals;


#endif  /* __OAMTDFS_H__ */
/*-----------------------------------------------------------------------*/


