/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: oammibclig.h,v 1.3 2011/01/24 09:16:18 siva Exp $
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */

extern UINT4 FsMplsTpOamModuleStatus[13];

extern UINT4 FsMplsTpGlobalId[13];

extern UINT4 FsMplsTpIcc[13];

extern UINT4 FsMplsTpNodeIdentifier[13];

extern UINT4 FsMplsTpTraceLevel[13];

extern UINT4 FsMplsTpNotificationEnable[13];

extern UINT4 FsMplsTpNodeMapGlobalId[13];

extern UINT4 FsMplsTpNodeMapNodeId[13];

extern UINT4 FsMplsTpNodeMapRowStatus[13];

extern UINT4 FsMplsTpMegName[13];

extern UINT4 FsMplsTpMegOperatorType[13];

extern UINT4 FsMplsTpMegIdIcc[13];

extern UINT4 FsMplsTpMegIdUmc[13];

extern UINT4 FsMplsTpMegServiceType[13];

extern UINT4 FsMplsTpMegMpLocation[13];

extern UINT4 FsMplsTpMegRowStatus[13];

extern UINT4 FsMplsTpMeName[13];

extern UINT4 FsMplsTpMeMpIfIndex[13];

extern UINT4 FsMplsTpMeSourceMepIndex[13];

extern UINT4 FsMplsTpMeSinkMepIndex[13];

extern UINT4 FsMplsTpMeMpType[13];

extern UINT4 FsMplsTpMeMepDirection[13];

extern UINT4 FsMplsTpMeProactiveOamPhbTCValue[13];

extern UINT4 FsMplsTpMeOnDemandOamPhbTCValue[13];

extern UINT4 FsMplsTpMeServiceSignaled[13];

extern UINT4 FsMplsTpMeServicePointer[13];

extern UINT4 FsMplsTpMeRowStatus[13];


