/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: oamwrg.h,v 1.3 2011/01/24 09:16:18 siva Exp $
*
* Description: This file contains the automatically generated 
*              prototype for the MPLS OAM MIB wrapper functions.
*********************************************************************/
#ifndef _OAMWR_H
#define _OAMWR_H

INT4 GetNextIndexFsMplsTpGlobalConfigTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsTpOamModuleStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpGlobalIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpIccGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeIdentifierGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpErrorCodeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNotificationEnableGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpOamModuleStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpGlobalIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpIccTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeIdentifierTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNotificationEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpOamModuleStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpGlobalIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpIccSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeIdentifierSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNotificationEnableSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpGlobalConfigTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsTpNodeMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsTpNodeMapGlobalIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeMapNodeIdGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeMapGlobalIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeMapNodeIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeMapGlobalIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeMapNodeIdSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpNodeMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsTpMegTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsTpMegNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegOperatorTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegIdIccGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegIdUmcGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegServiceTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegMpLocationGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegOperatorTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegIdIccTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegIdUmcTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegServiceTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegMpLocationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegNameSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegOperatorTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegIdIccSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegIdUmcSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegServiceTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegMpLocationSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMegTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsTpMeTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsTpMeNameGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeMpIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeSourceMepIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeSinkMepIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeMpTypeGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeMepDirectionGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeProactiveOamSessIndexGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeProactiveOamPhbTCValueGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeOnDemandOamPhbTCValueGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeServiceSignaledGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeServicePointerGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeMpIfIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeSourceMepIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeSinkMepIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeMpTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeMepDirectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeProactiveOamPhbTCValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeOnDemandOamPhbTCValueTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeServiceSignaledTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeServicePointerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeNameSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeMpIfIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeSourceMepIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeSinkMepIndexSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeMpTypeSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeMepDirectionSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeProactiveOamPhbTCValueSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeOnDemandOamPhbTCValueSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeServiceSignaledSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeServicePointerSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsTpMeTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

#endif /* _OAMWR_H */
