/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmptpdb.h,v 1.4 2012/03/02 12:28:31 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPTPDB_H
#define _FSMPTPDB_H

UINT1 FsMplsTpGlobalConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsTpNodeMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmptp [] ={1,3,6,1,4,1,2076,13,8};
tSNMP_OID_TYPE fsmptpOID = {9, fsmptp};


UINT4 FsMplsTpContextId [ ] ={1,3,6,1,4,1,2076,13,8,1,2,1,1};
UINT4 FsMplsTpOamModuleStatus [ ] ={1,3,6,1,4,1,2076,13,8,1,2,1,2};
UINT4 FsMplsTpGlobalId [ ] ={1,3,6,1,4,1,2076,13,8,1,2,1,3};
UINT4 FsMplsTpIcc [ ] ={1,3,6,1,4,1,2076,13,8,1,2,1,4};
UINT4 FsMplsTpNodeIdentifier [ ] ={1,3,6,1,4,1,2076,13,8,1,2,1,5};
UINT4 FsMplsTpErrorCode [ ] ={1,3,6,1,4,1,2076,13,8,1,2,1,6};
UINT4 FsMplsTpTraceLevel [ ] ={1,3,6,1,4,1,2076,13,8,1,2,1,7};
UINT4 FsMplsTpNotificationEnable [ ] ={1,3,6,1,4,1,2076,13,8,1,2,1,8};
UINT4 FsMplsTpNodeMapLocalNum [ ] ={1,3,6,1,4,1,2076,13,8,1,3,1,1};
UINT4 FsMplsTpNodeMapGlobalId [ ] ={1,3,6,1,4,1,2076,13,8,1,3,1,2};
UINT4 FsMplsTpNodeMapNodeId [ ] ={1,3,6,1,4,1,2076,13,8,1,3,1,3};
UINT4 FsMplsTpNodeMapRowStatus [ ] ={1,3,6,1,4,1,2076,13,8,1,3,1,4};




tMbDbEntry fsmptpMibEntry[]= {

{{13,FsMplsTpContextId}, GetNextIndexFsMplsTpGlobalConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsTpGlobalConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsTpOamModuleStatus}, GetNextIndexFsMplsTpGlobalConfigTable, FsMplsTpOamModuleStatusGet, FsMplsTpOamModuleStatusSet, FsMplsTpOamModuleStatusTest, FsMplsTpGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpGlobalConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsMplsTpGlobalId}, GetNextIndexFsMplsTpGlobalConfigTable, FsMplsTpGlobalIdGet, FsMplsTpGlobalIdSet, FsMplsTpGlobalIdTest, FsMplsTpGlobalConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTpGlobalConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsTpIcc}, GetNextIndexFsMplsTpGlobalConfigTable, FsMplsTpIccGet, FsMplsTpIccSet, FsMplsTpIccTest, FsMplsTpGlobalConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsTpGlobalConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsTpNodeIdentifier}, GetNextIndexFsMplsTpGlobalConfigTable, FsMplsTpNodeIdentifierGet, FsMplsTpNodeIdentifierSet, FsMplsTpNodeIdentifierTest, FsMplsTpGlobalConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTpGlobalConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsTpErrorCode}, GetNextIndexFsMplsTpGlobalConfigTable, FsMplsTpErrorCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsTpGlobalConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsTpTraceLevel}, GetNextIndexFsMplsTpGlobalConfigTable, FsMplsTpTraceLevelGet, FsMplsTpTraceLevelSet, FsMplsTpTraceLevelTest, FsMplsTpGlobalConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTpGlobalConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsTpNotificationEnable}, GetNextIndexFsMplsTpGlobalConfigTable, FsMplsTpNotificationEnableGet, FsMplsTpNotificationEnableSet, FsMplsTpNotificationEnableTest, FsMplsTpGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpGlobalConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsMplsTpNodeMapLocalNum}, GetNextIndexFsMplsTpNodeMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsTpNodeMapTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsTpNodeMapGlobalId}, GetNextIndexFsMplsTpNodeMapTable, FsMplsTpNodeMapGlobalIdGet, FsMplsTpNodeMapGlobalIdSet, FsMplsTpNodeMapGlobalIdTest, FsMplsTpNodeMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTpNodeMapTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsTpNodeMapNodeId}, GetNextIndexFsMplsTpNodeMapTable, FsMplsTpNodeMapNodeIdGet, FsMplsTpNodeMapNodeIdSet, FsMplsTpNodeMapNodeIdTest, FsMplsTpNodeMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTpNodeMapTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsTpNodeMapRowStatus}, GetNextIndexFsMplsTpNodeMapTable, FsMplsTpNodeMapRowStatusGet, FsMplsTpNodeMapRowStatusSet, FsMplsTpNodeMapRowStatusTest, FsMplsTpNodeMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTpNodeMapTableINDEX, 2, 0, 1, NULL},
};
tMibData fsmptpEntry = { 12, fsmptpMibEntry };

#endif /* _FSMPTPDB_H */

