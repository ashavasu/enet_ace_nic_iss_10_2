/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: oamprot.h,v 1.6 2012/03/19 11:28:23 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of oam module.
 *******************************************************************/

#ifndef __OAMPROT_H__
#define __OAMPROT_H__ 

/* Add Prototypes here */

/**************************oamtrc.c*******************************/


CHR1  *OamTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   OamTrcPrint      PROTO(( const char *fname, UINT4 u4Line, const char *s));
VOID   OamTrcWrite      PROTO(( CHR1 *s));

/*************************oamutil.c*******************************/

INT4 
OamUtilCreateContext PROTO ((UINT4 u4ContextId));

VOID 
OamUtilInitializeGblConfigTable PROTO ((tOamFsMplsTpGlobalConfigEntry
                          *pOamFsMplsTpGlobalConfigEntry, UINT4 u4ContextId));

INT4 
OamNotifySendMegStatus PROTO ((UINT4 u4MegStatus,
                               tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry));

tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromMegName PROTO ((UINT4 u4ContextId, UINT1 *pu1MegName, 
                                     UINT1 *pu1MeName));
tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromMegIccId PROTO ((UINT4 u4ContextId, UINT1 *pu1Icc, 
                                      UINT1 *pu1Umc, UINT2 u2MepIndex));
tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromMegIndex PROTO ((UINT4 u4ContextId, UINT4 u4MegIndex, 
                                     UINT1 *pu1MeName));
tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromMegIccInfo PROTO ((UINT4 u4ContextId,
                                        UINT4 u4MegIndex, UINT2 u2MepIndex));
INT4 
OamUtilValidateBaseServiceOid PROTO ((tOamFsMplsTpMeEntry 
                                      *pOamFsMplsTpMeEntry));

INT4
OamUtilValidateBaseOidGetTnlInfo PROTO ((tOamFsMplsTpMeEntry 
                                         *pOamFsMplsTpMeEntry,
                                      tMplsApiOutInfo *pOutMplsApiInfo));

UINT4 
OamUtilGetFreeMeId PROTO ((UINT4 u4ContextId, UINT4 u4MegIndex));

PUBLIC INT4 
FsMplsTpNodeIdGlobalIdTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 
FsMplsTpMegNameTableRBCmp PROTO ((tRBElem *, tRBElem *));

VOID
OamUtilGenMegName PROTO ((tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry, UINT1 *pMegName));

VOID
OamUtilGenMeName PROTO ((tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry, UINT1 *pMeName));

INT4
OamUtilCheckIfActiveMeExists PROTO ((tOamFsMplsTpMegEntry *
                                     pOamFsMplsTpMegEntry));

INT4
OamUtilGetVcIdFromPwIndex PROTO ((UINT4 u4PwIndex, UINT4 *pu4VcId));
/*************************oamutl.c*******************************/

VOID
OamUtlRBTreeDestroy PROTO ((VOID));

INT4
OamUtlNodeMapTableRowStatusHdl PROTO ((tOamFsMplsTpNodeMapEntry *
                                       pOamOldFsMplsTpNodeMapEntry,
                                       tOamFsMplsTpNodeMapEntry *
                                       pOamFsMplsTpNodeMapEntry));

INT4
OamUtlMegTableRowStatusHdl PROTO ((tOamFsMplsTpMegEntry * pOamOldFsMplsTpMegEntry,
                                   tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry));
INT4
OamUtlMeTableRowStatusHdl (tOamFsMplsTpMeEntry * pOamOldFsMplsTpMeEntry,
                           tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry);

PUBLIC INT4
OamFsMplsTpGlobalIdNodeIdTableCreate PROTO ((VOID));

PUBLIC INT4
OamFsMplsTpFsMplsTpMegNameTableCreate PROTO ((VOID));

PUBLIC INT4
OamFsMplsTpFsMplsTpMegIccIdTableCreate PROTO ((VOID));

PUBLIC INT4
FsMplsTpGlobalIdNodeIdTableRBCmp PROTO ((tRBElem * pRBElem1, 
                                         tRBElem * pRBElem2));

PUBLIC INT4
FsMplsTpFsMplsTpMegNameTableRBCmp PROTO ((tRBElem * pRBElem1, 
                                          tRBElem * pRBElem2));

PUBLIC INT4
FsMplsTpFsMplsTpMegIccIdTableRBCmp PROTO ((tRBElem * pRBElem1, 
                                          tRBElem * pRBElem2));
INT4
OamNotifyAndUpdateMegOperStatus PROTO ((tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry,
                                    UINT4 u4MegStatus));

tOamFsMplsTpMegEntry *
OamUtilGetAssociatedMegEntry PROTO ((tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry));

INT4
OamUtilIsMeEntryExist PROTO ((tOamFsMplsTpMegEntry * pOamFsMplsTpMegEntry));

INT4
OamUtilGetTunnelInfo PROTO ((tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry,
                             tTeTnlInfo *pTeTnlInfo));

/*************************oamutl.c*******************************/

INT4 
OamNotifyMegStatus PROTO ((UINT4 u4MegStatus, 
                           tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry));
tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromVcId PROTO ((UINT4 u4ContextId, UINT4 u4PwIndex));

INT4
OamMeValidateRsActiveConditions PROTO ((tOamFsMplsTpMeEntry * pOamOldFsMplsTpMeEntry,
                                 tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry));

INT4
OamHandleMeRowStatusActive PROTO ((tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry));

tOamFsMplsTpMeEntry *
OamUtilGetMeEntryFromServiceInfo (tMplsApiInInfo *pInMplsApiInfo);

INT4
OamUtilIsMeNameExist (UINT4 u4ContextId, UINT4 u4MegIndex, UINT4 u4MeIndex,
                      UINT4 u4MeMpIndex, UINT1 *pu1MeName);
INT4
OamResetMegIndicesInLspOrPw (tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry);
#endif   /* __OAMPROT_H__ */

/*-----------------------------------------------------------------------*/
/*                       End of the file  oamprot.h                     */
/*-----------------------------------------------------------------------*/
