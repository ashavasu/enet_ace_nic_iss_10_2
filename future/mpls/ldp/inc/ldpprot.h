/********************************************************************
 *                                                                  *
 * $Id: ldpprot.h,v 1.45 2016/10/28 08:09:26 siva Exp $
 *                                                                   *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpprot.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains prototypes of all functions
 *---------------------------------------------------------------------------*/
#ifndef _LDP_PROT_H
#define _LDP_PROT_H
/* Prototypes of PDU Process  related routines */
/* MPLS_IPv6 mod start*/
UINT1 LdpSendMsg    ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2MsgLen,
                               tLdpEntity * pLdpEntity, UINT1 u1IsHello,
                               tGenAddr u4PeerInfo, tGenU4Addr u4IfAddr, UINT4 u4IfIndex));
/* MPLS_IPv6 mod end*/
UINT1 LdpHandleTcpPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pPDU, UINT4 u4TcpConnId,UINT4 u4PeerAddr,
            UINT2 u2IncarnId));
UINT1 LdpHandleUdpPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pPDU, UINT4 u4IfIndex,
            UINT4 u4PeerAddr, UINT2 u2IncarnId));
#if 0
VOID LdpPrintTime(VOID);
#endif

/* MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED
UINT1 LdpHandleIpv6UdpPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pPDU, UINT4 u4IfIndex,
                                 tIpAddr u4PeerAddr, UINT2 u2IncarnId, UINT1 u1DiscardV6LHello));
UINT1 LdpConstructIpv6HelloMsg ARG_LIST ((tLdpEntity * pLdpEntity,
                                      UINT1 u1IsTrgtHello,
                                      tCRU_BUF_CHAIN_HEADER ** ppMsg,
                                      UINT2 *pu2MsgLen, tIp6Addr u4IfAddr));
UINT1 LdpHandleTcp6Pkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pPDU, UINT4 u4TcpConnId,tIp6Addr PeerAddr,
                                  UINT2 u2IncarnId));
UINT1 IsEntityDualStack ARG_LIST((tLdpEntity *pLdpEntity));
#endif
UINT1 LdpNeInitTcpConn ARG_LIST((tLdpSession * pSessionEntry, UINT1 u1SsnRole, UINT2 u2IncarnId));
/* MPLS_IPv6 add end*/

/* MPLS_IPv6 mod start*/
UINT1 LdpHandleHelloMsg ARG_LIST ((UINT1 *pMsg, tLdpId PeerLdpId,
                                   tGenAddr u4PeerAddr, UINT4 u4IfIndex,
                                   UINT2 u2IncarnId, UINT1 u1DiscardV6LHello));

UINT1 LdpValidatePduHdr ARG_LIST ((tLdpPduHdr * pPduHdr,
            tLdpSession * pSessionEntry, UINT2 u2BufSize,
            UINT1 *pu1StatusType));

UINT1 LdpValidateMsgHdr ARG_LIST ((UINT2 u2BufLen, UINT2 u2MsgLen,
            UINT2 u2MsgType, UINT1 *pu1StatusType));

UINT1 LdpValidateTlvHdr (UINT1 * pMsg,UINT2 u2MsgLen,UINT1 *pu1StatusType);

UINT1 LdpGetSsnFromConnIdOrPeerLdpId ARG_LIST ((tLdpSession ** ppSessionEntry,
            tLdpEntity * pLdpEntity,
            UINT4 u4TcpConnId,
            UINT1 *pu1PeerLdpId,
            UINT1 u1SrchConnId));

UINT1 LdpGetEntityFromIfIndex ARG_LIST ((UINT4 u4IncarnId, UINT4 u4IfIndex,
                                         UINT1 u1LocalFlag,
                                         tLdpEntity ** ppLdpEntity,
                                         tGenAddr u4PeerAddr,
                                         tLdpPeer **pLdpPeer,
                                         tLdpId PeerLdpId));

/* MPLS_IPv6 mod end*/
UINT1 LdpGetTargetedEntity (tTMO_SLL * pEntityList, UINT4 u4IfIndex,
        UINT4 u4PeerAddr, tLdpEntity ** ppLdpEntity);
/* MPLS_IPv6 mod start*/
UINT1 LdpCreateLdpPeer ARG_LIST ((tLdpEntity * pLdpEntity, tLdpId PeerLdpId,
                                  UINT1 u1RemoteFlag, tLdpPeer ** ppLdpPeer, UINT1 u1PeerAddrType));
#ifdef MPLS_LDP_BFD_WANTED
UINT1 LdpCreateBfdSession ARG_LIST ((tLdpEntity * pLdpEntity,
                                     tLdpSession *pLdpSession,UINT4 u4IfIndex,UINT1 u1AddrType));
#endif
/* MPLS_IPv6 mod end*/

/* MPLS_IPv6 mod start*/
UINT1 LdpCreateAdj  ARG_LIST ((tLdpSession * pSessionEntry,
                               UINT4 u4IfIndex, UINT2 u2HoldTime,
                               tLdpId PeerLdpId, UINT2 u2IncarnId, tGenAddr PeerAddr));
/* MPLS_IPv6 mod end*/

UINT1 LdpCreateSession ARG_LIST ((tLdpPeer * pLdpPeer,
            tLdpEntity * pLdpEntity,
            UINT1 u1SessionRole, UINT2 u2IncarnId));

VOID LdpGetSsnStateEvent ARG_LIST ((UINT2 u2MsgType, UINT1 *pu1SsnFsmEvent));

UINT1 LdpIsAdjEntryPresent
ARG_LIST ((tLdpSession * pSsnEntry, UINT4 u4IfIndex, UINT1 u1IPV6HelloRcvd));
#ifdef LDP_GR_WANTED
UINT1
LdpRefreshStaleLdpSession (tLdpSession *pLdpSession, tLdpEntity *pLdpEntity,
        UINT1 u1TrAddrPresent, UINT4 u4TrAddr,
        UINT4 u4PeerAddr, UINT4 u4IfIndex);

#ifdef MPLS_IPV6_WANTED
UINT1
LdpRefreshStaleIpv6LdpSession (tLdpSession *pLdpSession, tLdpEntity *pLdpEntity,
                           tIp6Addr *pPeerAddr, UINT4 u4IfIndex,UINT1 u1RemoteFlag);
#endif

VOID
LdpGrDeleteStaleCtrlBlockEntries (tLdpPeer *pLdpPeer);

UINT1
LdpGrEstbLspForStaleILMHwListEntry (tILMHwListEntry *pILMHwListEntry);


UINT1
LdpIsLblRcvdFromDownstreamRtr (tLdpSession *pUpstrLdpSession,
                               tFec Fec,
                               tGenU4Addr * pNextHop,UINT4 u4RtIfIndx);
tLdpSession *
LdpGetSessionFromIfIndex (UINT4 u4IfIndex);

UINT1
LdpGrFreeLabel (UINT4 u4Label);

VOID
LdpEntityDownAtGoStandBy(VOID);

VOID
LdpEntityUpAtGoStandBy(VOID);

#endif

UINT1 LdpGetCrossConnectIndex ARG_LIST ((UINT4 *pu4XCIndex));

/* Prototypes of Notification related routines - ldpnotif.c */

UINT1 LdpSendNotifMsg ARG_LIST ((tLdpSession * pSessionEntry, UINT1 *pu1Msg,
            UINT1 u1PDUFlag, UINT1 u1StatusType,
            tLspCtrlBlock * pLspCtrlBlock));
UINT1 LdpSendCrLspNotifMsg ARG_LIST ((tLdpSession * pSessionEntry,
            tLspCtrlBlock * pLspCtrlBlock,
            UINT1 u1StatusType, UINT1 *pu1Msg));

UINT1 LdpHandleNotifMsg ARG_LIST ((tLdpSession * pSessionEntry, UINT1 *pu1Msg));

VOID LdpSendLblResAvlNotifMsg ARG_LIST ((tLdpEntity * pLdpEntity));

/* Prototypes of Interface Sub module */
VOID fsmplsStartLdpProtocol ARG_LIST ((VOID));
VOID ldpDeQAndPrcsInMsgs ARG_LIST ((UINT4 u4RcvdEventsMask));
UINT1 LdpCreateIncarn ARG_LIST ((UINT2 u2Incarn));
UINT1 LdpDeleteIncarn ARG_LIST ((UINT2 u2Incarn, UINT4 u4Status));
UINT1 LdpProcessSnmpEvents ARG_LIST ((tLdpSnmpEvtInfo*));
UINT1 LdpProcessUdpEvents ARG_LIST ((tLdpUdpEvtInfo*));
/* MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED
UINT1 LdpProcessIpv6UdpEvents ARG_LIST ((tLdpUdpEvtInfo*));
#endif
/* MPLS_IPv6 add end*/

UINT1 LdpProcessTcpEvents ARG_LIST ((tLdpTcpEvtInfo*));
UINT1 LdpProcessIpEvents ARG_LIST ((tLdpIpEvtInfo*));
#ifdef LDP_GR_WANTED
VOID LdpGrProcessLblRelEvent (tLdpGrLblRelEvtInfo *pLdpGrLblRelEvtInfo);
#endif
UINT1 LdpProcessLdpInternalEvents ARG_LIST ((tLdpIntEvtInfo*));
VOID  LdpProcessTimerEvents ARG_LIST ((VOID));
UINT1 LdpCreateLdpEntity ARG_LIST ((tTMO_SLL * pLdpEntList, UINT2 u2IncarnId,
            tLdpId LdpId, UINT4 u4EntityIndex,
            tLdpEntity ** ppLdpEntity));
UINT1 LdpDeleteLdpEntity ARG_LIST ((tLdpEntity * pLdpEntity));
UINT1 LdpDownLdpEntity ARG_LIST ((tLdpEntity * pLdpEntity));
UINT1 LdpUpLdpEntity ARG_LIST ((tLdpEntity * pLdpEntity));
VOID ldpDelPeerIfAddrHashTable ARG_LIST ((tTMO_HASH_TABLE * pPeerIfAddrTable,
            UINT2 u2IncarnId));
VOID LdpDeAllocateIncarnTables ARG_LIST((UINT2 u2Incarn));

UINT1 ldpCreateRemoteLdpPeer
ARG_LIST ((tTMO_SLL * pLdpPeerList, UINT2 u2IncarnId, tLdpEntity * pLdpEntity,
            UINT4 u4LdpPeerIndex, tLdpPeer ** ppLdpPeer));
UINT1 LdpCreateCrlspTnl
ARG_LIST ((UINT2 u2IncarnId, UINT4 u4CrlspTnlIndex,
            tCrlspTnlInfo ** ppCrlspTnl));

UINT1 LdpDumpAllNormalLsps ARG_LIST ((INT4 *pNumLsps));

UINT1 LdpDumpAllCrLsps ARG_LIST ((INT4 *pNumLsps));

UINT1 LdpDeleteLdpPeer ARG_LIST ((tLdpPeer * pLdpPeer));

UINT1 LdpGetCrLsp   ARG_LIST ((UINT4 u4IncarnId, UINT4 u4IngressId,
            INT4 i4CrLspIndex, UINT1 u1CrLspTnlInstance,
            tCrlspTnlInfo ** ppCrlspTnlInfo));
UINT1 LdpGetLdpAdjacency ARG_LIST ((UINT4 u4IncarnId,
            tLdpId LdpId,
            UINT4 u4EntityIndex,
            tLdpId PeerId,
            UINT4 u4AdjacencyIndex,
            tLdpAdjacency ** ppLdpAdjacency));

UINT1 LdpGetLdpSession ARG_LIST ((UINT4 u4IncarnId,
            tLdpId LdpId,
            UINT4 u4EntityIndex,
            tLdpId PeerId, tLdpSession ** ppLdpSession));
UINT1 LdpGetLdpPeer ARG_LIST ((UINT4 u4IncarnId, tLdpId LdpId,
            UINT4 u4EntityIndex, tLdpId PeerId,
            tLdpPeer ** ppLdpPeer));
#ifdef MPLS_LDP_BFD_WANTED
UINT1 LdpGetLdpBfdSession ARG_LIST ((tLdpEntity *pLdpEntity, UINT4 u4TcpConnId,
                               tLdpBfdSession  **ppLdpBfdSession));
UINT1 LdpGetDisabledLdpBfdSession ARG_LIST ((tLdpEntity *pLdpEntity, tGenAddr DestAddr,
                                   tGenAddr  SrcAddr, UINT1 u1AddrType,
                                   tLdpBfdSession  **ppLdpBfdSession));
#endif
UINT1 LdpGetLdpEntity ARG_LIST ((UINT4 u4IncarnId, tLdpId LdpId,
            UINT4 u4EntityIndex,
            tLdpEntity ** ppLdpEntity));

UINT1 LdpGetLdpEntityEthParams ARG_LIST ((UINT4 u4IncarnId, tLdpEntity *pLdpEntity,
            UINT4 u4MinLabel, UINT4 u4MaxLabel,
            tLdpEthParams **pLdpEthEntry));

UINT1 LdpGetLdpEntityIfEntry ARG_LIST ((UINT4 u4IncarnId, tLdpEntity *pLdpEntity,
            UINT4 u4IfIndex, tLdpIfTableEntry ** ppLdpIfTableEntry));

UINT1 ldpAsgnKeyGroupId ARG_LIST ((UINT2 u2IncarnId));

/* Function prototypes of ldpport.c */
VOID LdpTmrExprHandler ARG_LIST ((tTimerListId TimerListId));
UINT1 LdpSNMPEventHandler ARG_LIST ((UINT4 u4Event, VOID *pSnmpParms,
            UINT2 u2IncarnId));
UINT1 LdpTcpEventHandler ARG_LIST ((UINT4 u4TcpEvent, UINT4 u4DataType,
            UINT4 u4ConnId, tCRU_BUF_CHAIN_HEADER * pPdu));
UINT1 LdpEnqueueMsgToLdpQ ARG_LIST ((UINT1,tLdpIfMsg*));
UINT1 LdpRegisterWithIP ARG_LIST ((VOID));
UINT1 LdpDeRegisterWithIP ARG_LIST ((VOID));

UINT1 LdpSendMplsMlibUpdate ARG_LIST ((UINT2 u2MlibOperation,
            UINT2 u2LabelOperation,
            tLspCtrlBlock * pLspCtrlBlock,
            tUstrLspCtrlBlock * pUstrLspCtrlBlock));
UINT1 LdpMplsMlibUpdate ARG_LIST ((UINT2 u2MlibOperation,
            UINT2 u2LabelOperation,
            tLspCtrlBlock * pLspCtrlBlock,
            tUstrLspCtrlBlock * pUstrCtrlBlock, tLspInfo *pLspInfo));
UINT1 LdpEstbLspsForAllRouteEntries ARG_LIST ((tLdpSession * pLdpSession));
UINT1 LdpEstbTargetLspForEgpRouteEntry ARG_LIST ((UINT4 u4DestNet, 
            UINT4 u4DestMask, 
            UINT4 u4NextHop, 
            UINT4 u4RtIfIndx));

UINT1 LdpDelTargetLspForRouteEntry ARG_LIST ((UINT4 u4DestNet, 
            UINT4 u4DestMask, 
            UINT4 u4NextHop, 
            UINT4 u4RtIfIndx));


VOID LdpEstbLspsForDoDMrgSsns ARG_LIST ((tLdpIpRtInfo *pInRtInfo, 
            tLdpSession * pLdpSession));
VOID LdpGetLspRouteInfo ARG_LIST ((tLdpIpRtInfo *pIpRtInfo,
            tLdpSession *pLdpSession));

VOID LdpSetupLspForAllRt ARG_LIST ((tLdpSession * pLdpSession));

UINT1 LdpIpGetIfAddr (INT4 i4IfIndex, UINT4 *pu4Addr);
UINT1 ldpIpGetIfId (UINT2 u2Port, tCRU_INTERFACE * pIfId);
INT4 ldpIpUtlGetLspRouteInfo (tLdpTrieOutParams * pTrieOutParams);
INT1 LdpIpUcastRouteQuery (UINT4 u4Addr, UINT2 *pu2RtPort, UINT4 *pu4RtGw);
INT1 ldpIpifGetIfInfo (UINT4 u4IfAddress, UINT4 *pu4IfMask, UINT4 *pu4IfIndex);
UINT1 ldplpLocalAddress (UINT4 u4DestAddr, UINT2 *pu2RtPort);
UINT1 LdpIpIsAddrLocal (UINT4 u4DestAddr);
UINT2 ldpIpGetIfType (INT4 i4ifIndex);
VOID ldpIpGetIfIndexFromPort (UINT2 u2RtPort , UINT4 *pu4IfIndex);
INT1 LdpIpGetExactRoute (UINT4 u4Addr, UINT4 u4DestMask,
        UINT4 *pu4RtPort, UINT4 *pu4RtGw);

/* Prototypes of Session Sub-Module */
/* Functions exist in ldpssm.c */
VOID LdpSsmNonExistentStateHandler ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 u1Event, UINT1 *pMsg));

VOID LdpSsmInitializedStateHandler ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 u1Event, UINT1 *pMsg));

VOID LdpSsmOpenSentStateHandler ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 u1Event, UINT1 *pMsg));
VOID LdpSsmOpenRecvStateHandler ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 u1Event, UINT1 *pMsg));

VOID LdpSsmOperationalStateHandler ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 u1Event, UINT1 *pMsg));

VOID LdpSsmSessionRetryStateHandler ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 u1Event, UINT1 *pMsg));

UINT1 LdpSsmSendNotifCloseSsn ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 u1StatType, UINT1 *pMsg,
            UINT1 u1IsPdu));

UINT1 LdpDeleteLdpSession ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 u1StatTyp));

UINT1 LdpProcessInitMsg ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 *pMsg, UINT1 *pu1StatType));

UINT1 LdpPrcsCmnSsnParamsTlv ARG_LIST ((UINT1 **ppTlv,
            tLdpSession * pSessionEntry,
            UINT1 *pu1StatType));

UINT1 LdpPrcsAtmSsnParamsTlv ARG_LIST ((UINT1 **ppTlv,
            tLdpSession * pSessionEntry,
            UINT1 *pu1StatType));

UINT1 ldpCheckLblIntersection ARG_LIST ((tTMO_SLL * pEntityLblRangeList,
            tTMO_SLL * pPeerLblRangeList));

UINT1 LdpHandleAdjExpiry ARG_LIST ((tLdpAdjacency * pAdjEntry, UINT1 u1StatusCode));
tLdpSession        *LdpGetSsnFromTcpConnTbl ARG_LIST ((UINT2 u2IncarnId,
            UINT4 u4TcpConnId));
VOID LdpDelSsnFromTcpConnTbl ARG_LIST ((tLdpSession * pLdpSession));
VOID LdpDelPeerIfAdrInfo ARG_LIST ((tLdpSession * pSessionEntry));


#ifdef MPLS_IPV6_WANTED
UINT1 LdpIpv6IsAddrLocal (tIp6Addr *pDestAddr);
INT1  LdpIpUcastIpv6RouteQuery (tIp6Addr *pDestAddr, UINT4 *pu4RtPort, tIp6Addr *pRtGw);
INT1  LdpIpv6GetExactRoute(tIp6Addr *pDestAddr, UINT1 u1DestPrefixLen, UINT4 *pu4RtPort, tIp6Addr *pRtGw);
UINT1 LdpGetIpv6IfAddr (INT4 i4IfIndex,tIp6Addr *pAddr);
UINT1 LdpIsLsrPartOfIpv6ErHopOrFec (tIp6Addr *pErHopAddr,UINT1 u1ErHopPrefixLength);
UINT1 LdpGetIpv6IfSiteLocalGlbUniqAddr (UINT4 u4IfIndex,tIp6Addr *pSiteLocalAddr,tIp6Addr *pGlobalUniqAddr);
UINT1 LdpGetIpv6IfAllAddr (INT4 i4IfIndex,tIp6Addr *pLnklocalIpv6Addr,tIp6Addr *pSiteLocalAddr,tIp6Addr *pGlobalUniqAddr);
#endif



/* Functions exist in ldpshand.c */

UINT1 LdpHandleAddrMsg ARG_LIST ((tLdpSession * pSessionEntry, UINT1 *pMsg));

UINT1 LdpHandleAddrWithdrawMsg ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 *pMsg));

/* Functions exist in ldpssnd.c */

UINT1 LdpSendHelloMsg ARG_LIST ((tLdpEntity * pLdpEntity));

UINT1 LdpConstructHelloMsg ARG_LIST ((tLdpEntity * pLdpEntity,
            UINT1 u1IsTrgtHello,
            tCRU_BUF_CHAIN_HEADER ** ppMsg,
            UINT2 *pu2MsgLen, UINT4 u4IfAddr));

UINT1 LdpSendKeepAliveMsg ARG_LIST ((tLdpSession * pSessionEntry));

UINT1 LdpSendInitMsg ARG_LIST ((tLdpSession * pSessionEntry));

UINT1
LdpSendAddrMsg (tLdpSession * pSessionEntry, tIpv4Addr * pAddress);

UINT1 LdpSendAddrWithdrawMsg ARG_LIST ((tLdpSession * pSessionEntry,
            uGenAddr u4Addr));

/* Prototypes of Advertisement Sub module */
/* functions exist in ldpadsm1.c */
UINT1 LdpHandleAdvertMsg ARG_LIST ((tLdpSession * pSessionEntry, UINT1 *pMsg));
tLspCtrlBlock      *LdpGetLspCtrlBlock ARG_LIST ((tLdpSession * pSessionEntry,
            UINT2 u2MsgType,
            UINT4 u4LblOrReqId,
            tFec Fec));
VOID LdpInitLspCtrlBlock ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock));
VOID LdpDeleteLspCtrlBlock ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock));

UINT1 LdpStrToFec   ARG_LIST ((UINT1 *pu1Fec, UINT2 u2FecTlvLen, tFec * pFec,UINT1 *pu1StatusType));
UINT1 LdpIsLoopFound ARG_LIST ((UINT1 *pu1HCTlv, UINT1 *pu1PVTlv,
            UINT2 *pu2PvCount, UINT1 *pu1HcValue,
            UINT2 u2IncarnId,tLdpSession *pLdpSession,
            UINT1 u1LsrEgress));
UINT1 LdpIsLsrEgress ARG_LIST ((tFec * pFec, UINT1 *pu1NoRouteStatus,
                                uGenU4Addr *pNextHopAddr, UINT4 *pu4IfIndex,
                                UINT2 u2IncarnId));
UINT1 LdpGetLabel  
ARG_LIST ((tLdpEntity * pLdpEntity, uLabel * pLabel, UINT4 u4SessIfIndex));
UINT1 LdpFreeLabel  ARG_LIST ((tLdpEntity * pLdpEntity, uLabel * pLabel,
            UINT4 u4SessIfIndex));

VOID LdpNonMrgIdRequest ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgIdLdpMap ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgIdRel ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgIdDnNak ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgIdIntSetup
ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRspMap ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRspRel ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRspUpAbort
ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRspDnNak ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRspUpLost ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRspDnLost ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRspIntDestroy ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
            UINT1 *pMsg));
VOID LdpNonMrgRspIntNewNH ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
            UINT1 *pMsg));
VOID LdpNonMrgRspPreEmpTnl ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
            UINT1 *pMsg));
VOID LdpNonMrgEstLdpMap ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgEstLdpRel ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgEstLdpWth ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgEstUpAbort
ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgEstUpLost ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgEstDnLost ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgEstIntDestroy ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
            UINT1 *pMsg));
VOID LdpNonMrgEstIntXCon
ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgEstIntNewNH
ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgEstPreEmpTnl
ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRelLdpMap ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRelLdpRel ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRelLdpWth ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRelUpAbort
ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgRelUpLost ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgInvStateEvent
ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));
VOID LdpNonMrgIntLspSetReq
ARG_LIST ((UINT2 u2IncarnId, tIpv4Addr NetworkAddr, UINT1 u1PrefLen,
            tIpv4Addr NextHopAddr, UINT4 u4IfIndex,
            tFecTableEntry * pFecTableEntry));
VOID LdpIntLspSetReq ARG_LIST ((tIntLspSetupInfo * pIntLspSetupInfo));

UINT1 LdpGetPeerSession ARG_LIST ((tGenU4Addr *pNextHopAddr, UINT4 u4IfIndex,
            tLdpSession ** ppPeerSession,
            UINT2 u2IncarnId));
UINT1 LdpGetLdpOverRsvpPeerSession ARG_LIST ((UINT4 u4NextHopAddr, 
            UINT4 u4IfIndex, 
            tLdpSession ** ppPeerSession, 
            UINT2 u2IncarnId));
VOID LdpNonMrgNhopChngIdlNewNh ARG_LIST ((tLspTrigCtrlBlock * pNHCtrlBlock,
            UINT1 *pMsg));

VOID LdpNonMrgNhopChngInvStateEvt ARG_LIST ((tLspTrigCtrlBlock * pNHCtrlBlock,
            UINT1 *pMsg));

VOID LdpNonMrgNhopChngRetryNewNh ARG_LIST ((tLspTrigCtrlBlock * pNHCtrlBlock,
            UINT1 *pMsg));
VOID LdpNonMrgNhopChngRetryRetryTmout ARG_LIST ((tLspTrigCtrlBlock
            * pNHCtrlBlock, UINT1 *pMsg));

VOID LdpNonMrgNhopChngRetryDestroy ARG_LIST ((tLspTrigCtrlBlock
            * pNHCtrlBlock, UINT1 *pMsg));

VOID LdpNonMrgNhopChngRspAwaitNewNh ARG_LIST ((tLspTrigCtrlBlock
            * pNHCtrlBlock, UINT1 *pMsg));

VOID LdpNonMrgNhopChngRspAwaitLspup ARG_LIST ((tLspTrigCtrlBlock
            * pNHCtrlBlock, UINT1 *pMsg));

VOID LdpNonMrgNhopChngRspAwaitNak ARG_LIST ((tLspTrigCtrlBlock * pNHCtrlBlock,
            UINT1 *pMsg));

VOID LdpNonMrgNhopChngRspAwaitDestroy ARG_LIST ((tLspTrigCtrlBlock
            * pNHCtrlBlock, UINT1 *pMsg));

VOID LdpDeleteTrigCtrlBlk ARG_LIST ((tLspTrigCtrlBlock * pTrigCtrlBlock));
VOID LdpNonMrgNhopChngIntLspDown ARG_LIST ((tLspTrigCtrlBlock * pNHCtrlBlock,
            UINT1 *pMsg));

/* Functions exist in ldpadsnd.c */
UINT1 LdpSendLblReqMsg ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
            UINT1 u1HcValue,
            UINT1 *pu1PvList, UINT1 u1PvCount,
            UINT1 *pu1UnknownTlv, UINT1 u1UnknownTlvLen));

UINT1 LdpSendLblMappingMsg ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
            UINT1 u1HcValue, UINT1 *pu1PvList,
            UINT1 u1PvCount));

UINT1 LdpSendLblAbortReqMsg ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock));

UINT1 LdpSendLblRelMsg ARG_LIST ((tFec * pFecElement, uLabel * pLabel,
            tLdpSession * pSessionEntry));

UINT1 LdpSendLblWithdrawMsg ARG_LIST ((tFec * pFecElement, uLabel * pLabel,
            tLdpSession * pSessionEntry));

UINT1               LdpCompLabels (uLabel Label, UINT4 u4RcvLbl,
        UINT1 u1LblType);

UINT1               LdpHandleLblWdrwMsg (tLdpSession * pSessionEntry,
        UINT1 *pMsg);

UINT1               LdpHandleLblRelMsg (tLdpSession * pSessionEntry,
        UINT1 *pMsg);

UINT1               LdpHandleLblAbrtMsg (tLdpSession * pSessionEntry,
        UINT1 *pMsg);

UINT1               LdpHandleLblMapMsg (tLdpSession * pSessionEntry,
        UINT1 *pMsg);

UINT1               LdpHandleLblReqMsg (tLdpSession * pSessionEntry,
        UINT1 *pMsg);

UINT1 LdpSendCrlspLblReqMsg ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock));
UINT1 LdpSendCrlspLblMappingMsg ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock));


/* Function prototypes of functions declared in ldpcrlsp.c */
UINT1 LdpNonMrgIntCrlspSetReq ARG_LIST ((UINT2 u2IncarnId,
            tCrlspTnlInfo * pCrlspTnlInfo));
UINT1 LdpUpdateCrlspTnlErHopInfo ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo,
            UINT1 *pu1ErHopTlv,
            UINT2 u2IncarnId,
            UINT1 *pu1ErrType));
VOID LdpUpdateCrlspTnlTrfcInfo ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo,
            UINT1 *pu1TrfcParmTlv));
UINT1 LdpUpdateCrlspTnlLspidInfo ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo,
            UINT1 *pu1LspidTlv));
VOID LdpNonMrgCrlspIdRequest ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
            UINT1 *pMsg));
UINT1 LdpCrlspResvModTrfcParms ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
            UINT1 *pTrfcParmsTlv));
VOID LdpNonMrgCrlspRspMap ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
            UINT1 *pMsg));
VOID LdpDeleteCrlspTnlInfo ARG_LIST ((UINT2 u2IncarnId,
            tCrlspTnlInfo * pCrlspTnlInfo));
UINT1 LdpPrcsAndUpdateErHopList ARG_LIST ((UINT2 u2IncarnId,
            tLspCtrlBlock * pLCB,
            UINT1 *pu1PrcsStatus));
UINT1 LdpIsGwPartOfErHop ARG_LIST ((UINT4 u4GwAddr,
            tLdpTeHopInfo * pErHop));
UINT1 LdpIsLsrPartOfIpv4ErHopOrFec ARG_LIST ((UINT4 u4ErHopAddr, 
            UINT4 u4ErHopMask));

UINT1 LdpIsLsrPartOfErHop ARG_LIST ((UINT2 u2IncarnId,
            tLdpTeHopInfo * pHop,
            tLspCtrlBlock * pLCB,
            UINT1 *pu1IsEgrOfHop));
UINT1 LdpGetReachabilityToErHop ARG_LIST ((UINT2 u2IncarnId,
            tLdpTeHopInfo * pHop,
            UINT4 *pu4GwAddr,
            UINT2 *pu2OutIfIndex,
            tLdpSession ** ppDStrSsn));
INT2 LdpUpdateCrLCBWithFwdInfo ARG_LIST ((UINT2 u2IncarnId, UINT4 u4GwAddr,
            UINT2 u2OutIfIndex,
            tLdpSession * pDStrSsn,
            tLspCtrlBlock * pLCB));
VOID LdpPreEmptTunnels ARG_LIST ((tTMO_SLL * pCandidateList));

VOID LdpPreEmptStackTunnels ARG_LIST ((tTMO_SLL * pStackList));

UINT1 LdpGetTnlIncarnNode ARG_LIST ((UINT2 u2IncarnId,
            UINT4 u4CrlspTnlIndex, 
            tTnlIncarnNode **ppTnlIncarnNode));

VOID LdpGetIncarnFromTnlIndex ARG_LIST ((INT4 i4TnlIndex, UINT4 *pu4IncarnId));

UINT1 LdpSetupCrlspTunnel ARG_LIST ((tLdpTeTnlInfo *pTeTnlInfo));

UINT1 LdpDestroyCrlspTunnel ARG_LIST ((tLdpTeTnlInfo *pTeTnlInfo));

UINT1 LdpTEEventHandler ARG_LIST ((UINT4 u4Event, tTeTnlInfo *pTeTnlInfo));

UINT1 LdpProcessDataTxEnableEvent ARG_LIST ((tLdpTeTnlInfo *pTeTnlInfo));

UINT1 LdpProcessDataTxDisableEvent ARG_LIST ((tLdpTeTnlInfo *pTeTnlInfo));


/* Function prototypes of functions declared in ldpudp.c */
UINT1 LdpUdpOpen    ARG_LIST ((UINT2 u2Port));
UINT1 LdpUdpSend   
ARG_LIST ((UINT2 u2SrcPort, UINT4 u4DestAddr, UINT2 u2DestPort,
            tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2BufLen, UINT4 u4IfAddr));
UINT1 LdpUdpClose   ARG_LIST ((UINT2 u2Port));

/* MPLS_IPv6 add start*/
UINT1 LdpGetV4SsnRole ARG_LIST ((tLdpPeer *pLdpPeer, tGenAddr PeerAddr, 
                                                  UINT4 u4TempLocalAddr, UINT1 u1RemoteFlag));
#ifdef MPLS_IPV6_WANTED
UINT1 LdpIpv6UdpOpen    ARG_LIST ((UINT2 u2Port));
UINT1 LdpIpv6TcpClose   ARG_LIST ((UINT4 u4TcpConnId));
UINT1 LdpIpv6UdpSend   
ARG_LIST ((UINT2 u2SrcPort, tIp6Addr V6DestAddr, UINT2 u2DestPort,
           tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2BufLen, tIp6Addr V6IfAddr, UINT4 u4IfIndex));
UINT1 LdpIpv6UdpClose   ARG_LIST ((UINT2 u2Port));
UINT1 LdpUdpJoinV6Multicast ARG_LIST ((INT4 i4Udp6SockFd, tIp6Addr u4LocalIfAddr,
                                     tIp6Addr u4MulticastAddr, UINT4 u4IfIndex));
/* MPLS_IPv6 mod start*/
UINT1 LdpUdpLeaveV6Multicast ARG_LIST ((INT4 i4UdpSockFd, tIp6Addr u4LocalIfAddr,
                                      tIp6Addr *pV6MulticastAddr, UINT4 u4IfIndex));
/* MPLS_IPv6 mod end*/
UINT1 Sock6Connect   ARG_LIST ((tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo));
UINT1 LdpGetV6SsnRole ARG_LIST ((tLdpPeer *pLdpPeer, tGenAddr PeerAddr,
                                                             tIp6Addr Ipv6TempLocalAddr, UINT1 u1RemoteFlag));
#endif
/* MPLS_IPv6 add end*/

/* MPLS_IPv6 mod start*/
UINT1 LdpTcpOpen    ARG_LIST ((tGenAddr u4DestAddr, UINT2 u2Port, UINT1 u1OpenFlag,
            UINT4 *pu4TcpConnId, UINT2 u2IncarnId,
            tLdpSession * pSessionEntry));
UINT1 LdpTcpClose   ARG_LIST ((UINT4 u4TcpConnId));
UINT1 LdpTcpSend    ARG_LIST ((UINT4 u4TcpConnId,
            tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2BufLen));

/* MPLS_IPv6 mod start*/
UINT1 SockOpen      ARG_LIST ((tGenAddr u4DestAddr, UINT2 u2Port,
                               UINT1 u1ConnType, UINT4 *pu4ConnId,
                               UINT1 *pu1MD5Passwd, tGenAddr u4SrcAddr, UINT4 u4SsnIfIndex));
/* MPLS_IPv6 mod end*/
UINT1 SockConnect   ARG_LIST ((tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo));

VOID SockRead ARG_LIST ((tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo));

UINT1 SockClose     ARG_LIST ((UINT4 u4ConnId));
UINT1 SockSend      ARG_LIST ((UINT4 u4ConnId, tCRU_BUF_CHAIN_HEADER * pBuf,
            UINT2 u2BufLen, UINT1 u1DataFlag));

UINT1 SockSendPendingData ARG_LIST ((tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo));

VOID
SockAddSelAddReadFd ARG_LIST ((tLdpTcpUdpSockInfo * pLdpTcpUdpSockInfo));

VOID LdpAddSsnToTcpConnTbl ARG_LIST ((tLdpSession * pLdpSession));
UINT1 LdpCheckAtmLblIntersection ARG_LIST ((tTMO_SLL * pEntityLblRangeList,
            tTMO_SLL * pPeerLblRangeList,
            tLdpSession * pSessionEntry,
            UINT1 *pu1Stattype));
VOID LdpDumpLdpPdu  ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, INT4 i4DbgMsgDir));

VOID LdpDisplayDumpHdr ARG_LIST ((INT4 i4DbgMsgDir));

UINT1 LdpUdpJoinMulticast ARG_LIST ((INT4 i4UdpSockFd, UINT4 u4LocalIfAddr,
            UINT4 u4MulticastAddr));
UINT1 LdpUdpLeaveMulticast ARG_LIST ((INT4 i4UdpSockFd, UINT4 u4LocalIfAddr,
            UINT4 u4MulticastAddr));
UINT1 LdpDeallocateEthLabelRnges ARG_LIST ((tLdpEntity * pLdpEntity));

UINT1 LdpDisjointMultiCastIfaces
ARG_LIST ((UINT4 u4IfAddr, tLdpEntity * pLdpEntity));

UINT1 LdpSliSetMd5SockOpt ARG_LIST ((UINT4 u4TcpStdServConnId,
            UINT1 Md5Passwd[],
            INT4  i4MD5KeyCount));


VOID                LdpCrlspAddTnlToPrioList (UINT4 u4IfIndex,
        UINT1 u1Priority,
        tCrlspTnlInfo *
        pCrlspTnlInfoInput);

VOID                LdpCrlspRemTnlFromPrioList (UINT4 u4IfIndex,
        UINT1 u1Priority,
        tCrlspTnlInfo *
        pCrlspTnlInfoInput);

VOID LdpCrlspModifyTnlPrioList ARG_LIST ((UINT4 u4CurrIfIndex,
            UINT1 u1CurrPriority,
            UINT4 u4NewIfIndex,
            UINT1 u1NewPriority,
            tCrlspTnlInfo * pCrlspTnlInfoInput));

UINT1 LdpCrlspGetTnlFromPrioList ARG_LIST ((UINT4 u4IfIndex,
            UINT1 u1Priority,
            tCrlspTnlInfo * pCrlspTnlInfoInput,
            tTMO_SLL * pCandidatePreemp));

/* Function prototypes of functions declared in ldputil.c */
VOID LdpDumpPduHdr ARG_LIST ((tLdpPduHdr * pPduHdr));
VOID LdpDumpHelloMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
VOID LdpDumpInitMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
VOID LdpDumpKeepAliveMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
VOID LdpDumpAddrMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
VOID LdpDumpAddrWithDrawMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
VOID LdpDumpLblMapMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
VOID LdpDumpLblReqMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
VOID LdpDumpLblWithDrawMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
VOID LdpDumpLblRelMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
VOID LdpDumpLblAbortReqMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
VOID LdpDumpStatusCode ARG_LIST ((UINT4 u4StatusCode));
VOID LdpDumpNotifMsg ARG_LIST ((UINT1 *pMsg, tLdpPduHdr * pPduHdr));
tLdpIfTableEntry* LdpCreateLdpIfTableEntry ARG_LIST ((UINT4 u4IfIndex,
            UINT1 u1OperStatus,
            tLdpEntity *pLdpEntity,
            UINT4 u4IfAddr));

VOID 
LdpUpdateLdpIfTableEntry(tLdpIfTableEntry *pLdpIfTableEntry,
  UINT1 u1InterfaceType,
  UINT1 u1IfOperStatus,UINT4 u4IfAddr);
#ifdef MPLS_IPV6_WANTED
tLdpIfTableEntry*
LdpCreateLdpIpv6IfTableEntry (UINT4 u4IfIndex,
                UINT1 u1IfOperStatus,
                tLdpEntity * pLdpEntity, UINT1 u1InterfaceType,UINT4 u4IfAddr,
                tIp6Addr* pLnklocalIpv6Addr,
                tIp6Addr* pSitelocalIpv6Addr,
                tIp6Addr* pGlbUniqueIpv6Addr);
#endif
UINT1 LdpAssociateIfEntry ARG_LIST ((tLdpEntity *pLdpEntity));
VOID  LdpDisassociateIfEntry ARG_LIST ((tLdpEntity *pLdpEntity));



/* Function prototypes of functions declared in ldputil.c */
VOID LdpPrintNormLCB ARG_LIST ((UINT2 u2LspNum, tLspCtrlBlock * pLspCtrlBlk));
VOID LdpPrintCrlspLCB ARG_LIST ((UINT2 u2CrLspNum, tCrlspTnlInfo *
            pCrlspTnlInfo));
VOID
LdpDelHashTable ARG_LIST ((
            tTMO_HASH_TABLE              * pTable, 
            UINT2                      u2IncarnId, 
            tMemPoolId                 PoolId));

UINT1 LdpTcResvResources ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo));

UINT1 LdpTcModifyResources ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo));

VOID LdpTcFreeResources ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo));

UINT1 LdpRegisterWithTC ARG_LIST ((VOID));

VOID LdpDeRegisterWithTC ARG_LIST ((VOID));

/*Routines related to VC merge State Mc */
VOID
LdpVcMergeNhopSm  ARG_LIST ((tLspTrigCtrlBlock *pNHCtrlBlock, UINT1 u1Event,
            UINT1* pMsg));
VOID
LdpMrgNhopChngIdlNewNh ARG_LIST ((tLspTrigCtrlBlock * pNHCtrlBlock,
            UINT1 *pMsg));
VOID
LdpMrgNhopChngInvStateEvt (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg);

VOID
LdpMrgNhopChngRetryNewNh (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg);

VOID
LdpMrgNhopChngRetryRetryTmout (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg);
VOID
LdpMrgNhopChngRetryDestroy (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg);
VOID
LdpMrgNhopChngRspAwaitNewNh (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg);
VOID
LdpMrgNhopChngRspAwaitDStrMap (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg);
VOID
LdpMrgNhopChngRspAwaitNak (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg);
VOID
LdpMrgNhopChngRspAwaitDestroy (tLspTrigCtrlBlock *pNHCtrlBlock, UINT1 *pMsg);

VOID LdpMrgDnIdlIntAddUpstr (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo *pMsg);
VOID LdpMrgDnRspAwtIntAddUpstr (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo *pMsg); 
VOID LdpMrgDnRspAwtIntDelUpstr (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo *pMsg);
VOID LdpMrgDnRspAwtLblMap (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo *pMsg);
VOID LdpMrgDnRspAwtLblWdraw (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo *pMsg);
VOID LdpMrgDnRspAwtIntDstrNak (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo *pMsg);
VOID LdpMrgDnRspAwtDstrLost (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo *pMsg);
VOID LdpMrgDnEstIntAddUpstr (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo *pMsg);
VOID LdpMrgDnEstIntDelUpstr (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgDnEstLblMap (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgDnEstLblWdraw (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgDnEstIntDstrNak (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgDnEstIntDstrLost (tLspCtrlBlock *pDnstrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMrgDnInvStateEvt (tLspCtrlBlock *pDnstrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMergeDnSm  (tLspCtrlBlock *pLspCtrlBlock , UINT1 u1Event,
        tLdpMsgInfo *pMsg);
UINT1 LdpCreateDnstrCtrlBlock (tLspCtrlBlock **ppDnstrCtrlBlock);

VOID LdpMrgUpIdlLdpReq  (tUstrLspCtrlBlock *pUStrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgUpRspAwtLdpUpAbrt (tUstrLspCtrlBlock *pUStrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgUpRspAwtIntDStrNak (tUstrLspCtrlBlock *pUStrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMrgUpRspAwtUpLost (tUstrLspCtrlBlock *pUStrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMrgUpRspAwtIntNewNh (tUstrLspCtrlBlock *pUStrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMrgUpRspAwtIntDStrMap (tUstrLspCtrlBlock *pUStrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMrgUpEstLdpRel (tUstrLspCtrlBlock *pUStrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMrgUpEstIntDStrWdraw (tUstrLspCtrlBlock *pUStrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMrgUpEstIntDStrNak (tUstrLspCtrlBlock *pUStrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMrgUpEstUpLost (tUstrLspCtrlBlock *pUStrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMrgUpEstReXConnect (tUstrLspCtrlBlock *pUStrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgUpEstIntNewNh (tUstrLspCtrlBlock *pUStrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgUpEstIntDStrMap(tUstrLspCtrlBlock *pUStrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgUpRelAwtLdpRel (tUstrLspCtrlBlock *pUStrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMrgUpRelAwtLdpUpAbrt (tUstrLspCtrlBlock *pUStrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgUpRelAwtUpLost (tUstrLspCtrlBlock *pUStrCtrlBlock, tLdpMsgInfo* pMsg);
VOID LdpMrgUpInvStateEvt (tUstrLspCtrlBlock *pUStrCtrlBlock,tLdpMsgInfo* pMsg);
VOID LdpMergeUpSm  (tUstrLspCtrlBlock *pUStrCtrlBlock, UINT1 u1Event,
        tLdpMsgInfo* pMsg);
tUstrLspCtrlBlock * LdpGetUstrCtrlBlock (tLdpSession * pSessionEntry,
        UINT2 u2MsgType, UINT4 u4LabelOrReqId, tFec Fec);
VOID LdpCreateUpstrCtrlBlock (tUstrLspCtrlBlock **pUstrCtrlBlock);
VOID LdpInitUpstrCtrlBlock (tUstrLspCtrlBlock *pUstrCtrlBlock);
VOID LdpDeleteUpstrCtrlBlock (tUstrLspCtrlBlock *pUstrCtrlBlock);
VOID LdpDuDeleteUpLspCtrlBlock (tUstrLspCtrlBlock* pLspUpCtrlBlock);

UINT1 LdpMrgSendLblMappingMsg ARG_LIST ((tUstrLspCtrlBlock * pUstrCtrlBlock,
            UINT1 u1HcValue, UINT1 *pu1PvList,
            UINT1 u1PvCount, UINT1 *pu1UnknownTlv, 
            UINT1 u1UnknownTlvLen));

UINT1 LdpInformNewRtToNhSm (tLspCtrlBlock *pDnstrCtrlBlk, 
        const tLdpEntity * pLdpEntity,
        tLdpRouteEntryInfo *pRouteInfo);


VOID LdpDuUpInvStEvt ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuDnInvStEvt ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuDnIdLblReq ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuDnEstLblReq ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuDnIdLdpMap ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuDnIdLdpWR ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuDnIdDelFec ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuDnIdNHChg ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuDnIdDnLost ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuDnEstLdpMap ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuDnEstLdpWR ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuDnEstDelFec ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuDnEstNHChg ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuDnEstDnLost ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));

VOID LdpDuUpIdIntDnMap ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuUpIdLdpRel ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpIdIntWR ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuUpIdRsrcAvl ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpIdDelFec ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpIdUpLost ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));

VOID LdpDuUpEstIntDnMap ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
#ifdef LDP_GR_WANTED
VOID LdpDuUpEstIntRefreshEvt ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
#endif
VOID LdpDuUpEstLdpRel ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpEstIntWR ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuUpEstRsrcAvl ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpEstDelFec ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpEstUpLost ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));

VOID ldpDuUpRelIntDnMap ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpRelLdpRel ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuUpRelIntWR ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuUpRelRsrcAvl ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuUpRelDelFec ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpRelUpLost ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));

VOID ldpDuUpRsrcIntDnMap ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID ldpDuUpRsrcLdpRel ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpRsrcIntWR ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpRsrcRsrcAvl ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpRsrcDelFec ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));
VOID LdpDuUpRsrcUpLost ARG_LIST ((tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo *pLdpMsgInfo));


tLspCtrlBlock *LdpDuDnGetInstalledFtnCtrlBlock(tFec Fec);

VOID LdpEstbLspsForDuSsns ARG_LIST ((tLdpSession * pLdpSession, tLdpIpRtInfo* pInRtInfo));
VOID LdpDuLspSetReq(UINT2 u2IncarnId, tLdpIpRtInfo *pRtInfo);

UINT1
LdpIsSsnForFec(tGenU4Addr *pNextHopAddr, UINT4 u4OutIfIndex, 
        tLdpSession *pLdpSession);

VOID LdpInvCorrStMh (tLdpSession *pLdpSession, VOID *pCtrlBlock,
        UINT1 u1Event, tLdpMsgInfo *pMsg, UINT1 u1IsUS);

UINT1 LdpReRouteTime (UINT1 u1StartOrEnd);

VOID LdpDuUpIdLblReq(tUstrLspCtrlBlock * pLspUpCtrlBlock, 
        tLdpMsgInfo* pLdpMsgInfo);

VOID LdpDuUpIdLblAbrtReq(tUstrLspCtrlBlock * pLspUpCtrlBlock,
        tLdpMsgInfo * pLdpMsgInfo);
VOID LdpDuDnSm  (tLspCtrlBlock *pLspCtrlBlock , UINT1 u1Event,
        tLdpMsgInfo *pMsg);

VOID LdpDuUpSm (tUstrLspCtrlBlock *pUsLspCtrlBlock , UINT1 u1Event,
        tLdpMsgInfo *pMsg);

VOID LdpTriggerRtChgEvent (UINT2 u2IncarnId, UINT4 u4RtEvent,
        tLdpRouteEntryInfo *pRouteInfo,
        tLspCtrlBlock *pLspCtrlBlk,
        tLdpSession *pLdpSession);

VOID LdpLspSetReq (UINT2 u2IncarnId, tIpv4Addr NetworkAddr, 
        UINT4 u4DestMask, tIpv4Addr NextHopAddr, 
        UINT4 u4IfIndex);

VOID LdpGenerateRtChgEvents (UINT2 u2IncarnId);

VOID LdpPrintNormUstrCtrlBlk (UINT2 u2LspNum, tUstrLspCtrlBlock * pUstrCtrlBlk);

VOID LdpNotifySnmp(tLdpTrapInfo* pLdpTrapInfo);

VOID LdpProcessQueueMsgs(VOID);


UINT1 LdpCheckMapAttributes (UINT1 *pu1PvTlv, UINT1 u1HopCount, 
        tLspCtrlBlock *pLspCtrlBlock, tLdpMsgInfo *pMsg);
UINT1 LdpSetLoopDetectionMethod (UINT1 u1LoopDetectMethod, 
        UINT1 u1LoopDetectFlag, UINT4 u4FsMplsLdpLsrIncarnId);

UINT1 LdpProcessAppEvents ARG_LIST ((tLdpAppEvtInfo * pAppEvtInfo));
UINT1 LdpProcessL2vpnEvents ARG_LIST ((tL2VpnLdpPwVcEvtInfo * pL2vpnAppEvtInfo));
UINT1 LdpProcessTeEvent ARG_LIST ((tLdpTeEvtInfo *pTeEvtInfo));
UINT1 LdpProcessTgtSsnReqEvent ARG_LIST ((tPwVcSsnEvtInfo *pSsnEvtInfo));
UINT1 LdpProcessTgtSsnReleaseEvent ARG_LIST ((tPwVcSsnEvtInfo *pSsnEvtInfo));
UINT1 LdpPwVcHandleLblMap ARG_LIST ((tLdpSession * pLdpSession, UINT1 *pMsg));
UINT1 LdpPwVcHandleLblRel ARG_LIST ((tLdpSession * pLdpSession, UINT1 *pMsg));
UINT1 LdpPwVcHandleLblWdraw ARG_LIST ((tLdpSession *pLdpSession, UINT1 *pMsg));
UINT1 LdpSendPwVcLblMapMsg ARG_LIST ((tLdpSession * pLdpPeerSession, 
            tPwVcLblMsgInfo * pFecInfo, 
            uLabel * pLabel));
UINT1 LdpSendPwVcLblWdrawMsg ARG_LIST ((tLdpSession * pLdpPeerSession, 
            tPwVcLblMsgInfo * pFecInfo));
UINT1 LdpSendPwVcLblRelMsg ARG_LIST ((tLdpSession * pLdpPeerSession, 
            tPwVcLblMsgInfo * pFecInfo));
UINT1 LdpSendPwVcNotifMsg ARG_LIST ((tLdpSession * pLdpPeerSession, 
            tPwVcNotifEvtInfo * pFecInfo));
UINT1 LdpSendPwVcAddrWdrawMsg ARG_LIST ((tLdpSession * pLdpPeerSession, 
            tPwVcNotifEvtInfo * pFecInfo));
UINT1 LdpPwVcHandleNotifMsg ARG_LIST ((tLdpSession *pLdpSession, UINT1 *pMsg));
UINT1 LdpPwVcHandleAddrWdraw ARG_LIST ((tLdpSession *pLdpSession, UINT1 *pMsg));
UINT1 LdpProcessPwVcNotifReqEvent ARG_LIST ((tPwVcNotifEvtInfo  * pNotifMsgInfo));
UINT1 LdpProcessPwVcAddrWdrawMsgEvent ARG_LIST ((tPwVcNotifEvtInfo  * pNotifMsgInfo));
INT4 LdpL2VpnEventHandler ARG_LIST ((VOID *pMsg, UINT4 u4Event));

/* PW Redundancy */
UINT1 LdpSendPwRedAppDataMsg ARG_LIST ((tLdpSession * pLdpPeerSession,
            tL2VpnLdpIccpEvtInfo * pIccpMsgInfo));
VOID LdpFillPwRedSyncRqstTlv ARG_LIST ((tPwRedDataEvtArgs *pPwRedData, UINT2 *pu2Len, 
            tPwRedSyncRqstTlv *pPwRedSyncRqstTlv));
VOID LdpFillIccpPwIdTlv ARG_LIST ((tL2vpnPwFec *pPwFec, tPwIdOrGenPwIdTlv *pPwIdOrGenPwIdTlv, 
            UINT2 *pu2Len, UINT1 *pu1Type));
VOID LdpFillPwRedSyncDataTlv ARG_LIST ((tPwRedDataEvtArgs *pPwRedData, UINT2 *pu2Len,
            tPwRedSyncDataTlv *pPwRedSyncDataTlv));
UINT1 LdpCopyGenPwIdTlvToBuf ARG_LIST ((tCRU_BUF_CHAIN_HEADER *pMsg, tGenPwIdTlv *pGenPwIdTlv,
            UINT4 *pu4OffSet));
UINT1 LdpHandleIccpMsg ARG_LIST ((tLdpSession * pSession, UINT1 *pMsg));
UINT1 LdpHandleIccpRgDataMsg ARG_LIST ((tLdpSession * pSession, UINT1 *pMsg));
UINT1 LdpProcessPwRedSyncRqstTlv ARG_LIST ((UINT1 *pMsg, UINT2 *pu2OffSet,
            tPwRedDataEvtArgs *pLdpIccpEvtInfo));
UINT1 LdpProcessPwRedSyncDataTlv ARG_LIST ((UINT1 *pMsg, UINT2 *pu2OffSet,
            tPwRedDataEvtArgs *pLdpIccpEvtInfo));
UINT1 LdpProcessPwVcIccpMsgEvent ARG_LIST ((tL2VpnLdpIccpEvtInfo *pIccpMsgInfo));
UINT1 LdpIccpExtractPwIdTlvFrmBuf ARG_LIST ((UINT2 u2TlvType, UINT1 *pMsg, 
            tL2vpnPwFec *pPwFec, UINT2 *pu2OffSet, 
            UINT2 *pu2SubTlvsLen));
UINT1 LdpIccpExtractPwConfigTlv ARG_LIST ((UINT1 *pMsg, tPwRedDataEvtArgs *pPwData,
            UINT2 *pu2OffSet, UINT2 *pu2SubTlvsLen));
UINT1 LdpIccpExtractPwStateTlv ARG_LIST ((UINT1 *pMsg, tPwRedDataEvtArgs *pPwData,
            UINT2 *pu2OffSet, UINT2 *pu2SubTlvsLen));
UINT1 LdpIccpGetPwDataByRoId ARG_LIST ((tPwRedDataEvtPwData **ppPwData,
            tPwRedDataEvtArgs *pPwRgData,
            tL2vpnIccpRoId *pRoId));

INT4 LdpL2VpnGetEventInfo (VOID **ppEvtInfo);
INT4 LdpL2VpnCleanEventInfo (VOID *pvEvtInfo, BOOL1 b1CleanEvent, BOOL1 b1CleanNodes);
INT4 LdpL2VpnSendEventInfo (VOID *pEvtInfo, UINT4 u4Event);
INT4 LdpL2VpnGetEventPwFec (VOID **ppEvtPwFec);
INT4 LdpL2VpnCleanEventPwFec (VOID *pEvtPwFec);
INT4 LdpL2VpnGetEventPwData (VOID **ppEvtPwData);
INT4 LdpL2VpnCleanEventPwData (VOID *pEvtPwData);

INT4 LdpRegisterApplication ARG_LIST ((UINT1 u1FecId, UINT4 *pu4AppId));
INT4 LdpDeRegisterApplication ARG_LIST ((UINT4 u4AppId));
UINT1 LdpProcessPwVcLblMsgEvent ARG_LIST ((tPwVcLblMsgInfo * pLblMsgInfo));
UINT1 LdpProcessPwVcLblReqEvent ARG_LIST ((tPwVcLblMsgInfo * pLblMsgInfo));
UINT1 LdpProcessPwVcCreateReqEvent ARG_LIST ((tPwVcLblMsgInfo * pLblMsgInfo));
UINT1 LdpProcessPwVcReleaseReqEvent ARG_LIST ((tPwVcLblMsgInfo * pLblMsgInfo));
UINT1 LdpPwVcHandleLblReq ARG_LIST ((tLdpSession * pLdpSession, UINT1 *pMsg));
UINT1 LdpProcessPwVcNotifMsgEvent ARG_LIST ((tPwVcNotifEvtInfo  * pNotifMsgInfo));
UINT1 LdpSendPwVcLblReqMsg ARG_LIST ((tLdpSession * pLdpPeerSession, 
            tPwVcLblMsgInfo * pFecInfo));
UINT1 LdpProcessPwVcWthReqEvent ARG_LIST ((tPwVcLblMsgInfo * pLblMsgInfo, 
            UINT1 u1WithType));
UINT1
LdpGetSessionWithPeer ARG_LIST ((UINT4 u4IncarnId, UINT4 u4LdpEntityID,
                                uGenU4Addr *pPeerAddr, UINT2 u2AddrType,UINT1 u1LblAdvtMode, UINT1 u1IsTgtType,
                                           tLdpSession ** ppLdpSession));
VOID  LdpInitRegTable ARG_LIST ((UINT2 u2IncarnId));
UINT1 LdpRegStatus ARG_LIST ((UINT1 u1FecType));
UINT1 LdpHandleFecMsg ARG_LIST ((tLdpSession * pSessionEntry, UINT1 *pMsg, 
            UINT4 u4MsgType, 
            UINT1 u1FecType));
UINT1 LdpL2VpnPostEvent ARG_LIST ((tL2VpnLdpPwVcEvtInfo *pPwVcEvtInfo));
UINT1 LdpNotifEventToL2Vpn ARG_LIST ((tPwVcLblMsgInfo * pLblMsgInfo, 
            UINT1 u1MsgCode, UINT4 u4SsnIfIndex));
VOID LdpSendSsnNotification ARG_LIST ((tLdpSession * pSessionEntry, 
            UINT4 u4NotifType));

VOID LdpHandleFecL2VpnMsg ARG_LIST ((tLdpSession * pSessionEntry,
            UINT1 *pMsg, UINT4 u4MsgType));

UINT1 LdpFecAppInit ARG_LIST ((UINT2 u2IncarnId));

UINT1 LdpFecAppDeInit ARG_LIST ((UINT2 u2IncarnId));

UINT4 LdpGetSrcTransAddrFromDestAddr 
             ARG_LIST ((tGenU4Addr *pDestTransAddr,tGenU4Addr *pSrcTransAddr, 
                        BOOL1 bIsManualPwVc));
VOID LdpSsnStatusChngNotif ARG_LIST ((tLdpSession * pSessionEntry, UINT4 u4NotifType));

VOID LdpUdpPktInSocket (INT4 i4SockFd);

/* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
VOID LdpIpv6UdpPktInSocket(INT4 i4SockFd);
UINT1 LdpDisjointV6MultiCastIfaces
ARG_LIST ((tIp6Addr u4IfAddr, tLdpEntity * pLdpEntity));
#endif
/* MPLS_IPv6 mod end*/

VOID  MplsFecUpdateSysTime (VOID);
VOID  MplsLdpEntityUpdateSysTime(VOID);
VOID  MplsLdpLspFecUpdateSysTime (VOID);
VOID  MplsLdpPeerUpdateSysTime (VOID);
VOID  MplsLdpSessionUpdateSysTime (tLdpSession *pLdpSession);

UINT4 MplsFecGetSysTime (VOID);
UINT4 MplsLdpEntityGetSysTime(VOID);
UINT4 MplsLdpLspFecGetSysTime (VOID);
UINT4 MplsLdpPeerGetSysTime (VOID);
UINT4 MplsLdpSessionGetSysTime (tLdpSession *pLdpSession);

INT1 MplsLdpEntityInitSsnThresholdNotify (tLdpEntity * pLdpEntity);
INT1 MplsSessionNotification (tLdpSession * pLdpSession, UINT1 u1State);
INT1 MplsLdpPVLimitNotification (tLdpPeer * pLdpPeer);
UINT1 LdpProcessLdpOverRsvpEvt (tLdpOverRsvpEvtInfo * pLdpOverRsvpEvtInfo);
VOID LdpHandleLdpUpDownEntityForLdpOverRsvp (tLdpEntity *pLdpEntity, UINT4 u4Status); 

INT1 LdpIsLdpInitialised PROTO ((VOID));
INT1 LdpGetFecInLabelForDestAddr(tL2vpnLabelArgs * pL2vpnLabelArgs, BOOL1 bIsManualPwVc);
INT1 LdpGetFecInLabelFromLoopbackIntf(BOOL1 bIsManualPwVc, tL2vpnLabelArgs * pL2vpnLabelArgs, UINT4 u4LdpIp); 

BOOL1
LdpIsPeerExist (UINT4 u4IncarnId, UINT4 u4IfIndex, UINT1 u1RemoteFlag,
                tGenAddr u4PeerAddr);
/* MPLS_IPv6 mod end*/
VOID
LdpMlibUpdateForEstbLsps (tLspCtrlBlock *pLspCtrlBlock);
VOID
LdpMlibDeleteForEstbLsps (tLspCtrlBlock * pLspCtrlBlock);
UINT1
LdpGetL3IfaceFromLdpOverRsvpOutTnl (tLdpEntity *pLdpEntity, UINT4 *pu4L3Intf);
UINT4 LdpGetInLabelFrmPrefix (tL2vpnLabelArgs * pL2vpnLabelArgs);
VOID LdpSendInvalidRgDataPkt (VOID);
/* LDP GR Related Functions */
VOID
LdpGrStartHelperProcess (tLdpSession * pSessionEntry);
VOID
LdpPrcsFTSsnTlv (UINT1 **ppTlv, tLdpSession * pSessionEntry, UINT2 u2TmpLen);
VOID
LdpSendDeleteStaleNotification (tLdpPeer * pLdpPeer, UINT4 u4Event);
VOID
LdpStartFwdHoldingTimer ( VOID );
VOID
LdpGrStartRecoveryTimer (tLdpSession * pSessionEntry);

#ifdef SNMP_2_WANTED
VOID LdpRegisterLDPMibs ( VOID );
VOID LdpUnRegisterLDPMibs ( VOID );
#endif

UINT1
LdpGrProcessRecoveryTimerEvent (tPwVcSsnEvtInfo * pSsnEvtInfo);
VOID
LdpGrInitProcess ( VOID );
VOID
LdpSendGrStartEventToL2Vpn(VOID);

VOID
LdpReserveLabelToLabelManager(VOID);

VOID
LdpDuIndLblReqOrNewFecProcedure (UINT1 u1DnCtlBlockPresent, tUstrLspCtrlBlock * pLspUpCtrlBlock,
                                 tLspCtrlBlock *pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo,
                                 tLdpSession *pLdpSessionEntry, tGenU4Addr *pNextHopAddr);
VOID
LdpDuOrderLblReqOrNewFecProcedure (UINT1 u1DnCtlBlockPresent, tUstrLspCtrlBlock * pLspUpCtrlBlock,
                                   tLspCtrlBlock *pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo,
                                   tLdpSession *pLdpSessionEntry, tGenU4Addr *pNextHopAddr);

VOID
LdpDuSendLblReq (tUstrLspCtrlBlock * pLspUpCtrlBlock, tLspCtrlBlock *pLspCtrlBlock,
                 tLdpMsgInfo * pLdpMsgInfo, tLdpSession *pLdpSessionEntry,
                 tGenU4Addr *pNextHopAddr);

VOID
LdpDuSendLblReqForIngress (tLdpSession *pLdpSessionEntry, tFec Fec,
                           tGenU4Addr *pNextHopAddr);
UINT4
LdpConvertLocalValToTLVVal (UINT1 u1StatusCode);

tLdpEntity *
LdpGetOtherEntityForActivation (tLdpEntity *pLdpEntity);
BOOL1
LdpCheckIfEntityBeActivated (tLdpEntity *pLdpEntity);

VOID
LdpGetPrevLdpSession (UINT2 u2IncarnId,tGenU4Addr *pPrefix, UINT1 u1PrefLen,
        tLspCtrlBlock **ppPrevLspCtrlBlk,
        tLdpSession **ppPrevLdpSession);
VOID 
LdpNonMrgSsnDnDeleteLspCtrlBlk(tLspCtrlBlock * pLspCtrlBlock);

VOID 
LdpNonMrgResAwtDeleteLspCtrlBlk(tLspCtrlBlock * pLspCtrlBlock);

VOID
LdpCheckAndRetriggerRtAddEvent (UINT2 u2IncarnId,
        tLdpRouteEntryInfo *pRouteInfo);

extern tIssBool MsrGetRestorationStatus PROTO ((VOID));
VOID
LdpMapCtrlBlkAndDnStrSession (tIntLspSetupInfo * pIntLspSetupInfo,
        tLspCtrlBlock *pLspCtrlBlk,
        tLspTrigCtrlBlock  *pLspTrigCtrlBlock);
VOID
LdpMapCtrlBlkNoStatChngDnStrSsn (tIntLspSetupInfo * pIntLspSetupInfo,
        tLspCtrlBlock *pLspCtrlBlk,
        tLspTrigCtrlBlock  *pLspTrigCtrlBlock);
VOID
LdpRetriggerEntitySessions (UINT4 u4IpAddress,
        UINT4 u4IfIndex,
        UINT4 u4IfaceIp, UINT4 u4Event);
#ifdef LDP_GR_WANTED
UINT1
LdpGrReserveLabel (UINT4 u4Label);

UINT1
LdpGrReserveResources (UINT1 u1TnlIntReservationfFlag);

VOID
LdpProcessFwdHoldTmrExpiry(VOID);

#endif


#ifdef MPLS_IPV6_WANTED
UINT1 LdpInterfaceIpv6Match(tIp6Addr* pLnklocalIpv6Addr,
                tIp6Addr* pSitelocalIpv6Addr,tIp6Addr* pGlbUniqueIpv6Addr);
tIp6Addr* LdpGetBasicIpv6TransAddrPref(tIp6Addr* pLnklocalIpv6Addr,
  tIp6Addr* pSitelocalIpv6Addr,
  tIp6Addr* pGlbUniqueIpv6Addr);
BOOL1 LdpGetIsLastIpv6Interface(UINT2 u2IncarnId,UINT4 u4IfIndex);
UINT1 LdpProcessIpv6Events (tLdpIpEvtInfo * pIpEvtInfo);
UINT1 LdpProcessAddrAddEvent(tLdpIpEvtInfo * pIpEvtInfo);
UINT1 LdpUpdateIpv6TransAddr4AddEvent(tLdpEntity *pLdpEntity,tIp6Addr *pIp6Addr,UINT4 u4IfIndex,UINT4 u4Cmd);
UINT1 LdpUpdateAddrIfTableEntry(tLdpIfTableEntry   *pIfEntry,tIp6Addr *pIp6Addr,UINT4 u1Cmd);
UINT1 LdpProcessAddrDelEvent(tLdpIpEvtInfo * pIpEvtInfo);
UINT1 LdpEstbLspsForAllIpv6RouteEntries(tLdpSession * pLdpSession);
VOID  LdpGetLspIpv6RouteInfo (tNetIpv6RtInfo * pNetIpv6RtInfo, tLdpSession * pLdpSession);
VOID  LdpEstbIpv6LspsForDoDMrgSsns (tNetIpv6RtInfo * NetIpv6RtInfo, tLdpSession * pLdpSession);
VOID  LdpEstbIpv6LspsForDuSsns(tLdpSession * pLdpSession,tNetIpv6RtInfo * NetIpv6RtInfo);
UINT1 LdpProcessIpv6RouteChangeEvent(tLdpIpEvtInfo * pIpEvtInfo);
VOID  LdpIpv6LspSetReq (UINT2 u2IncarnId, tIp6Addr *pNetworkAddr, UINT1 u1PrefixLen,
              tIp6Addr *pNextHopAddr, UINT4 u4IfIndex);
VOID  LdpUpdateInterfaceType(UINT4 u4Cmd, tLdpIfTableEntry* pIfEntry);
UINT1 LdpGetAllLoopBackIpv6Ip(tIp6Addr  *pLoopbackAddress,UINT2 *pLoopBackIntCount);
UINT1 LdpGetAllPppInterfaceIpv6Ip(tTMO_SLL *pIfList,tIp6Addr  *pPppInterfaceAddress,UINT2 *pPppIntCount);
UINT1 LdpGetAlllnklLoopBackIpv6Ip(tIp6Addr  *pLlLoopbackAddress,UINT2 *pLlLoopBackIntCount);
UINT1 LdpSendIpv6AddrMsg (tLdpSession * pSessionEntry,
                tIp6Addr * pLinkLocalAddr,
                tIp6Addr * pSiteLocalAddr,
                tIp6Addr * pGlbUniqueAddr);
UINT1
LdpSendIpv6AddrWithdrawMsg (tLdpSession * pSessionEntry,tIp6Addr *pLnklocalIpv6Addr,
                tIp6Addr *pSitelocalIpv6Addr,
                tIp6Addr *pGlbUniqueIpv6Addr);
VOID
LdpNonMrgIntIpv6LspSetReq (UINT2 u2IncarnId,
                       tIp6Addr *pNetworkAddr,
                       UINT1 u1PrefLen,
                       tIp6Addr *pNextHopAddr,
                       UINT4 u4IfIndex, tFecTableEntry * pFecTableEntry);
VOID  LdpUpdateLdpIpv6IfTableEntry(tLdpIfTableEntry *pLdpIfTableEntry,
                UINT1 u1IfOperStatus,
                UINT1 u1InterfaceType,UINT4 u4IfAddr,
                tIp6Addr* pLnklocalIpv6Addr,
                tIp6Addr* pSitelocalIpv6Addr,
                tIp6Addr* pGlbUniqueIpv6Addr);
BOOL1 LdpIfGetAddressIndex(tPeerIfAdrNode *pPeerIfAdrNode,UINT4 u4IfIndex);
UINT1 LdpGetIpv6IfGlbUniqAddr (UINT4 u4IfIndex,tNetIpv6AddrInfo *pGlobalUniqAddrInfo);
#endif

BOOL1 LdpIsAddressMatchWithAAddrType(uGenAddr *pGenAddr1,uGenU4Addr *pGenU4Addr2,UINT2 u2AddrType1,UINT2 u2AddrType2);
BOOL1 LdpIsAAddressMatch(uGenAddr *pGenAddr1,uGenU4Addr *pGenU4Addr2,UINT2 u2AddrType);

UINT1 LdpAddrAddUdpJoinMulticast(tLdpIfTableEntry *pLdpIfTableEntry,UINT1 u1InterfaceType);
UINT1 LdpAddrDelUdpLeaveMulticast(tLdpIfTableEntry *pLdpIfTableEntry,UINT1 u1InterfaceType);
UINT1 LdpAddrAddCfaOperUp(tLdpIpEvtInfo * pIpEvtInfo,UINT1 u1InterfaceType);
UINT1 LdpProcessCfaOperDownEvent(tLdpIpEvtInfo * pIpEvtInfo);
UINT1 LdpProcessCfaOperUpEvent(tLdpIpEvtInfo * pIpEvtInfo);
BOOL1 LdpGetIfAllEntityDownViaIntOperDown(UINT2 u2IncarnId);
UINT1 LdpAddrDelCfaOperDown(tLdpIpEvtInfo * pIpEvtInfo,UINT1 u1InterfaceType);
BOOL1 LdpGetIsIpv4ActiveInterface(VOID);
UINT1 LdpProcessRouteChangeEvent(tLdpIpEvtInfo * pIpEvtInfo);
BOOL1 LdpIsAddressMatch(uGenU4Addr *pGenU4Addr1,uGenU4Addr *pGenU4Addr2,UINT2 u2AddrType);
BOOL1 LdpIsAddressMatchWithAddrType(uGenU4Addr *pGenU4Addr1,uGenU4Addr *pGenU4Addr2,UINT2 u2AddrType1,UINT2 u2AddrType2);
BOOL1 LdpIsAddressZero(uGenU4Addr *pGenU4Addr,UINT2 u2AddrType);
BOOL1 LdpCheckIfDnCtrlBlkExistForFec (tLdpSession *pLdpSession,uGenU4Addr *Dest);
UINT1 LdpGetAllLoopBackIpv4Ip(tIpv4Addr  *pLoopbackAddress,UINT2 *pLoopBackIntCount);
UINT1 LdpGetAllPppInterfaceIpv4Ip(tTMO_SLL *pIfList,tIpv4Addr *pPppInterfaceAddress,UINT2 *pPppIntCount);
VOID  PrintInterfaceAddr(tLdpIpEvtInfo * pIpEvtInfo);
BOOL1 LdpCompareFec(tFec *pFec1, tFec *pFec2);
VOID  LdpCopyAAddr(uGenU4Addr *pGenU4Addr1,uGenAddr *pGenAddr2,UINT2 u2AddrType);
UINT4 LdpSessionGetIfIndex(tLdpSession *pSessionEntry,UINT2 u2AddrType);


#ifdef MPLS_LDP_BFD_WANTED
INT4 
LdpRegisterWithBfd (tLdpSession *pLdpSession);

INT4 
LdpDeRegisterWithBfd (tLdpSession *pLdpSession);

INT4 
LdpDeRegisterAllWithBfd (VOID);

INT1 
LdpProcessPeerDownNotification (tLdpBfdInfo *pLdpBfdMsgInfo);

INT1 
LdpHandleNbrPathStatusChange (UINT4 u4ContextId,
                              tBfdClientNbrIpPathInfo *pNbrPathInfo);

UINT1 
LdpProcessBfdEvent ARG_LIST ((tLdpBfdEvtInfo *pBfdEvtInfo));

INT4 
LdpDeRegisterDisabledBfdSess (tLdpBfdSession *pLdpBfdSession);
#endif

#endif /*_LDP_PROT_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpprot.h                              */
/*---------------------------------------------------------------------------*/
