
/********************************************************************
 *                                                                  *
 * $RCSfile: ldpdsgbl.h,v $                                         *
 *                                                                  *
 * $Date: 2007/02/01 14:57:00 $                                     *
 *                                                                  *
 * $Revision: 1.2 $                                             *
 *                                                                  *
 *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpdsgbl.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : DIFFSERV
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains definition of global variables
 *---------------------------------------------------------------------------*/
#ifndef _DS_GBL_H
#define _DS_GBL_H

UINT4 au4DiffServStatusData [LDP_DS_MAX_STATUS_TYPES]  = {
    LDP_DS_STAT_UNEXPECTED_DIFFSERV_TLV | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_UNSUPPORTED_PHB   | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_INVALID_EXP_PHB_MAPPING  | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_UNSUPPORTED_PSC          | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_PER_LSPCONTEXT_ALLOCFAIL           | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_UNSUPPORTED_CLASS_TYPE_TLV | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_INVALID_CLASS_TYPE_VALUE | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_ELSPTP_UNSUPPORTED_PSC | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_ELSPTP_UNAVL_OARSRC | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_RESOURCE_UNAVAIL | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_UNSUPPORTED_ELSPTP_TLV | LDP_ADVISORY_NOTIF,
    LDP_DS_STAT_UNEXP_CLASS_TYPE_TLV   | LDP_ADVISORY_NOTIF	    
};
UINT1 gu1TnlModel=0;

#endif
/*---------------------------------------------------------------------------*/
/*                        End of file ldpdsgbl.h                             */
/*---------------------------------------------------------------------------*/
