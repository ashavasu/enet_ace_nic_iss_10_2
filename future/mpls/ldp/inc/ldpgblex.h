/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 *  * $Id: ldpgblex.h,v 1.6 2013/12/11 10:07:35 siva Exp $
 *  *
 *  *******************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpgblex.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains extern definition of global 
 *                             variables
 *---------------------------------------------------------------------------*/
#ifndef _LDP_GBLEX_H
#define _LDP_GBLEX_H

extern tLdpGlobalInfo gLdpInfo;

extern tLdpProcEvtFuncPtr gLdpProcEvtFunc[];

extern tSsnFsmFuncPtr gLdpSsnFsm[];

extern              tNonMrgFsmFuncPtr
    gLdpNonMrgFsm[LDP_MAX_NON_MRG_STATES][LDP_MAX_NON_MRG_EVENTS];
/* LOC_RPR */
extern              tNonMrgNhopChngFsmFuncPtr
    gLdpNonMrgNhopChngFsm[LDP_MAX_NON_MRG_NHOP_CHNG_STATES]
    [LDP_MAX_NON_MRG_NHOP_CHNG_EVENTS];
    
/*Vc Merge Nhop State Mc */
extern              tMrgNhopChngFsmFuncPtr
    gLdpMrgNhopChngFsm[LDP_MAX_MRG_NHOP_CHNG_STATES]
    [LDP_MAX_MRG_NHOP_CHNG_EVENTS];

extern tMrgDnFsmFuncPtr
    gLdpMrgDnFsm[LDP_MAX_MRG_DNSTR_STATES][LDP_MAX_MRG_DNSTR_EVENTS]; 

extern tMrgUpFsmFuncPtr
    gLdpMrgUpFsm[LDP_MAX_MRG_UPSTR_STATES][LDP_MAX_MRG_UPSTR_EVENTS];

    
extern tDuDnFsmFuncPtr
    gLdpDuDnFsm[LDP_MAX_DU_DN_STATES][LDP_MAX_DU_DN_EVENTS]; 

extern tDuUpFsmFuncPtr
    gLdpDuUpFsm[LDP_MAX_DU_UP_STATES][LDP_MAX_DU_UP_EVENTS]; 


/* The following global variables are used in ldpcrlsp.c */
extern UINT4        gu4LdpIpv4AddrMask[];


/* Extern Declarations for Global Variable used in LDP Debugging. */
extern UINT4        gu4LdpDbg;
extern UINT4        gu4LdpLvl;

/* Extern Declarations for Debug Arrays */
extern CONST CHR1       *aEventName[LDP_MAX_MAJOR_EVENTS];
extern CONST CHR1       *au1SsnState[LDP_MAX_SESSION_STATES];
extern CONST CHR1       *au1SsnEvents[LDP_MAX_SESSION_EVENTS];
extern CONST CHR1       *au1LdpNhEvts[LDP_MAX_NON_MRG_NHOP_CHNG_EVENTS];
extern CONST CHR1       *au1LdpNhStates[LDP_MAX_NON_MRG_NHOP_CHNG_STATES];


/* Global Variables used for LDP Dumping of Msgs */
extern UINT4        gu4LdpDumpType;
extern UINT4        gu4LdpDumpDir;
extern UINT1        gu1LdpTeFlag;

extern UINT4        gu4LdpLspPersist;
extern UINT4        gu4LdpMd5Option;                        

/* Global Variables used for Diffserv */
extern tDiffServRMGblInfo gRMInfo;
extern UINT4 au4DiffServStatusData []; 
extern UINT4 au4DiffServElspInfoTableOid[];
extern UINT1 gu1TnlModel;

extern tLdpAppRegTbl gaLdpRegTable[LDP_MAX_INCARN][MAX_LDP_APPLICATIONS];

extern INT1 nmhGetIfIpAddr PROTO((INT4 , UINT4 *));
extern INT1 nmhGetIfType PROTO((INT4 , INT4 *));
extern INT1 nmhGetIfIpSubnetMask PROTO ((INT4, UINT4 *));

#endif /*_LDP_GBLEX_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpgblex.h                             */
/*---------------------------------------------------------------------------*/
