/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdldplw.h,v 1.4 2008/08/20 15:14:46 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpLsrId ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsLdpLsrLoopDetectionCapable ARG_LIST((INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpEntityLastChange ARG_LIST((UINT4 *));

INT1
nmhGetMplsLdpEntityIndexNext ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsLdpEntityTable. */
INT1
nmhValidateIndexInstanceMplsLdpEntityTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsLdpEntityTable  */

INT1
nmhGetFirstIndexMplsLdpEntityTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpEntityTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpEntityProtocolVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityOperStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityTcpPort ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityUdpDscPort ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityMaxPduLength ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityKeepAliveHoldTimer ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityHelloHoldTimer ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityInitSessionThreshold ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityLabelDistMethod ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityLabelRetentionMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityPathVectorLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityHopCountLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityTransportAddrKind ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityTargetPeer ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityTargetPeerAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityTargetPeerAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsLdpEntityLabelType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityDiscontinuityTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsLdpEntityProtocolVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetMplsLdpEntityAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityTcpPort ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetMplsLdpEntityUdpDscPort ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetMplsLdpEntityMaxPduLength ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetMplsLdpEntityKeepAliveHoldTimer ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetMplsLdpEntityHelloHoldTimer ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetMplsLdpEntityInitSessionThreshold ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityLabelDistMethod ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityLabelRetentionMode ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityPathVectorLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityHopCountLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityTransportAddrKind ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityTargetPeer ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityTargetPeerAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityTargetPeerAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsLdpEntityLabelType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsLdpEntityProtocolVersion ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsLdpEntityAdminStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityTcpPort ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsLdpEntityUdpDscPort ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsLdpEntityMaxPduLength ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsLdpEntityKeepAliveHoldTimer ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsLdpEntityHelloHoldTimer ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsLdpEntityInitSessionThreshold ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityLabelDistMethod ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityLabelRetentionMode ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityPathVectorLimit ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityHopCountLimit ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityTransportAddrKind ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityTargetPeer ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityTargetPeerAddrType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityTargetPeerAddr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsLdpEntityLabelType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsLdpEntityTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MplsLdpEntityStatsTable. */
INT1
nmhValidateIndexInstanceMplsLdpEntityStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsLdpEntityStatsTable  */

INT1
nmhGetFirstIndexMplsLdpEntityStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpEntityStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpEntityStatsSessionAttempts ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsSessionRejectedNoHelloErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsSessionRejectedAdErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsSessionRejectedMaxPduErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsSessionRejectedLRErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsBadLdpIdentifierErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsBadPduLengthErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsBadMessageLengthErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsBadTlvLengthErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsMalformedTlvValueErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsKeepAliveTimerExpErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsShutdownReceivedNotifications ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityStatsShutdownSentNotifications ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpPeerLastChange ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsLdpPeerTable. */
INT1
nmhValidateIndexInstanceMplsLdpPeerTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsLdpPeerTable  */

INT1
nmhGetFirstIndexMplsLdpPeerTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpPeerTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpPeerLabelDistMethod ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsLdpPeerPathVectorLimit ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsLdpPeerTransportAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsLdpPeerTransportAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for MplsLdpSessionTable. */
INT1
nmhValidateIndexInstanceMplsLdpSessionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsLdpSessionTable  */

INT1
nmhGetFirstIndexMplsLdpSessionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpSessionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpSessionStateLastChange ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsLdpSessionState ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsLdpSessionRole ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsLdpSessionProtocolVersion ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsLdpSessionKeepAliveHoldTimeRem ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsLdpSessionKeepAliveTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsLdpSessionMaxPduLength ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsLdpSessionDiscontinuityTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for MplsLdpSessionStatsTable. */
INT1
nmhValidateIndexInstanceMplsLdpSessionStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsLdpSessionStatsTable  */

INT1
nmhGetFirstIndexMplsLdpSessionStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpSessionStatsTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpSessionStatsUnknownMesTypeErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsLdpSessionStatsUnknownTlvErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto Validate Index Instance for MplsLdpHelloAdjacencyTable. */
INT1
nmhValidateIndexInstanceMplsLdpHelloAdjacencyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsLdpHelloAdjacencyTable  */

INT1
nmhGetFirstIndexMplsLdpHelloAdjacencyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpHelloAdjacencyTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpHelloAdjacencyHoldTimeRem ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpHelloAdjacencyHoldTime ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpHelloAdjacencyType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Proto Validate Index Instance for MplsInSegmentLdpLspTable. */
INT1
nmhValidateIndexInstanceMplsInSegmentLdpLspTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsInSegmentLdpLspTable  */

INT1
nmhGetFirstIndexMplsInSegmentLdpLspTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsInSegmentLdpLspTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsInSegmentLdpLspLabelType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsInSegmentLdpLspType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto Validate Index Instance for MplsOutSegmentLdpLspTable. */
INT1
nmhValidateIndexInstanceMplsOutSegmentLdpLspTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsOutSegmentLdpLspTable  */

INT1
nmhGetFirstIndexMplsOutSegmentLdpLspTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsOutSegmentLdpLspTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsOutSegmentLdpLspLabelType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsOutSegmentLdpLspType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsFecLastChange ARG_LIST((UINT4 *));

INT1
nmhGetMplsFecIndexNext ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsFecTable. */
INT1
nmhValidateIndexInstanceMplsFecTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsFecTable  */

INT1
nmhGetFirstIndexMplsFecTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsFecTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsFecType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsFecAddrType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsFecAddr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsFecAddrPrefixLength ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsFecStorageType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsFecRowStatus ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsFecType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsFecAddrType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsFecAddr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsFecAddrPrefixLength ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsFecStorageType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsFecRowStatus ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsFecType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsFecAddrType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsFecAddr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsFecAddrPrefixLength ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsFecStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsFecRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsFecTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpLspFecLastChange ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsLdpLspFecTable. */
INT1
nmhValidateIndexInstanceMplsLdpLspFecTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsLdpLspFecTable  */

INT1
nmhGetFirstIndexMplsLdpLspFecTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpLspFecTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpLspFecStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpLspFecRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsLdpLspFecStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpLspFecRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsLdpLspFecStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpLspFecRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsLdpLspFecTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MplsLdpSessionPeerAddrTable. */
INT1
nmhValidateIndexInstanceMplsLdpSessionPeerAddrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsLdpSessionPeerAddrTable  */

INT1
nmhGetFirstIndexMplsLdpSessionPeerAddrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpSessionPeerAddrTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpSessionPeerNextHopAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpSessionPeerNextHopAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
