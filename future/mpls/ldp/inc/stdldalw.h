/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdldalw.h,v 1.3 2010/09/24 06:25:30 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for MplsLdpEntityAtmTable. */
INT1
nmhValidateIndexInstanceMplsLdpEntityAtmTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsLdpEntityAtmTable  */

INT1
nmhGetFirstIndexMplsLdpEntityAtmTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpEntityAtmTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpEntityAtmIfIndexOrZero ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmMergeCap ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmLRComponents ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLdpEntityAtmVcDirectionality ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmLsrConnectivity ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmDefaultControlVpi ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmDefaultControlVci ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmUnlabTrafVpi ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmUnlabTrafVci ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsLdpEntityAtmIfIndexOrZero ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmMergeCap ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmVcDirectionality ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmLsrConnectivity ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmDefaultControlVpi ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmDefaultControlVci ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmUnlabTrafVpi ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmUnlabTrafVci ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsLdpEntityAtmIfIndexOrZero ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmMergeCap ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmVcDirectionality ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmLsrConnectivity ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmDefaultControlVpi ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmDefaultControlVci ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmUnlabTrafVpi ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmUnlabTrafVci ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Proto Validate Index Instance for MplsLdpEntityAtmLRTable. */
INT1
nmhValidateIndexInstanceMplsLdpEntityAtmLRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsLdpEntityAtmLRTable  */

INT1
nmhGetFirstIndexMplsLdpEntityAtmLRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpEntityAtmLRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpEntityAtmLRMaxVpi ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmLRMaxVci ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmLRStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityAtmLRRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsLdpEntityAtmLRMaxVpi ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmLRMaxVci ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmLRStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityAtmLRRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsLdpEntityAtmLRMaxVpi ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmLRMaxVci ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmLRStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityAtmLRRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , INT4  ,INT4 ));

/* Proto Validate Index Instance for MplsLdpAtmSessionTable. */
INT1
nmhValidateIndexInstanceMplsLdpAtmSessionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsLdpAtmSessionTable  */

INT1
nmhGetFirstIndexMplsLdpAtmSessionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , tSNMP_OCTET_STRING_TYPE *  , INT4 * , INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpAtmSessionTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , INT4 , INT4 * , INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpSessionAtmLRUpperBoundVpi ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhGetMplsLdpSessionAtmLRUpperBoundVci ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , tSNMP_OCTET_STRING_TYPE * , INT4  , INT4 ,INT4 *));

INT1
nmhDepv2MplsLdpEntityAtmTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2MplsLdpEntityAtmLRTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
