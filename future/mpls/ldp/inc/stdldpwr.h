
 /* $Id: stdldpwr.h,v 1.4 2012/10/24 10:44:06 siva Exp $*/

#ifndef _STDLDPWR_H
#define _STDLDPWR_H

VOID RegisterSTDLDP(VOID);
VOID UnRegisterSTDLDP(VOID);
INT4 MplsLdpLsrIdGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpLsrLoopDetectionCapableGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityLastChangeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsLdpEntityTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLdpEntityLdpIdGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityProtocolVersionGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTcpPortGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityUdpDscPortGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityMaxPduLengthGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityKeepAliveHoldTimerGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityHelloHoldTimerGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityInitSessionThresholdGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityLabelDistMethodGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityLabelRetentionModeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityPathVectorLimitGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityHopCountLimitGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTransportAddrKindGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTargetPeerGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTargetPeerAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTargetPeerAddrGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityLabelTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityProtocolVersionSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTcpPortSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityUdpDscPortSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityMaxPduLengthSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityKeepAliveHoldTimerSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityHelloHoldTimerSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityInitSessionThresholdSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityLabelDistMethodSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityLabelRetentionModeSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityPathVectorLimitSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityHopCountLimitSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTransportAddrKindSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTargetPeerSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTargetPeerAddrTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTargetPeerAddrSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityLabelTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityProtocolVersionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTcpPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityUdpDscPortTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityMaxPduLengthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityKeepAliveHoldTimerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityHelloHoldTimerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityInitSessionThresholdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityLabelDistMethodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityLabelRetentionModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityPathVectorLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityHopCountLimitTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTransportAddrKindTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTargetPeerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTargetPeerAddrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTargetPeerAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityLabelTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);



















INT4 GetNextIndexMplsLdpEntityStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLdpEntityStatsSessionAttemptsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsSessionRejectedNoHelloErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsSessionRejectedAdErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsSessionRejectedMaxPduErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsSessionRejectedLRErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsBadLdpIdentifierErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsBadPduLengthErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsBadMessageLengthErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsBadTlvLengthErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsMalformedTlvValueErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsKeepAliveTimerExpErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsShutdownReceivedNotificationsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityStatsShutdownSentNotificationsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpPeerLastChangeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsLdpPeerTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLdpPeerLdpIdGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpPeerLabelDistMethodGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpPeerPathVectorLimitGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpPeerTransportAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpPeerTransportAddrGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsLdpSessionTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLdpSessionStateLastChangeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionStateGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionRoleGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionProtocolVersionGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionKeepAliveHoldTimeRemGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionKeepAliveTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionMaxPduLengthGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsLdpSessionStatsTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLdpSessionStatsUnknownMesTypeErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionStatsUnknownTlvErrorsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsLdpHelloAdjacencyTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLdpHelloAdjacencyIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpHelloAdjacencyHoldTimeRemGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpHelloAdjacencyHoldTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpHelloAdjacencyTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsInSegmentLdpLspTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsInSegmentLdpLspIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentLdpLspLabelTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentLdpLspTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsOutSegmentLdpLspTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsOutSegmentLdpLspIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentLdpLspLabelTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentLdpLspTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsFecLastChangeGet(tSnmpIndex *, tRetVal *);
INT4 MplsFecIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsFecTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsFecIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsFecTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsFecAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsFecAddrGet(tSnmpIndex *, tRetVal *);
INT4 MplsFecAddrPrefixLengthGet(tSnmpIndex *, tRetVal *);
INT4 MplsFecStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsFecRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsFecTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsFecAddrTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsFecAddrSet(tSnmpIndex *, tRetVal *);
INT4 MplsFecAddrPrefixLengthSet(tSnmpIndex *, tRetVal *);
INT4 MplsFecStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsFecRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsFecTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFecAddrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFecAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFecAddrPrefixLengthTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFecStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFecRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFecTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);






INT4 MplsLdpLspFecLastChangeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsLdpLspFecTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLdpLspFecSegmentGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpLspFecSegmentIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpLspFecIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpLspFecStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpLspFecRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpLspFecStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpLspFecRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpLspFecStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpLspFecRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpLspFecTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexMplsLdpSessionPeerAddrTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLdpSessionPeerAddrIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionPeerNextHopAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionPeerNextHopAddrGet(tSnmpIndex *, tRetVal *);
#endif /* _STDLDPWR_H */
