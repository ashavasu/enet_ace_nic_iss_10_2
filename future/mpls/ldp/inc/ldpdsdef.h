/********************************************************************
 *                                                                  *
 * $RCSfile: ldpdsdef.h,v $
 *                                                                  *
 * $Date: 2010/09/24 06:25:30 $                                     *
 *                                                                  *
 * $Revision: 1.4 $                                             *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpdsdef.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : DIFFSERV 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants used across sub modules
 *---------------------------------------------------------------------------*/
#ifndef _LDP_DSDEF_H
#define _LDP_DSDEF_H

#define TBD ~0U            /*To be defined in the draft */

#define LDP_DS_ZERO 0


#define LDP_DS_FALSE LDP_FAILURE
#define LDP_DS_TRUE  LDP_SUCCESS

#define LDP_DS_SUCCESS LDP_SUCCESS
#define LDP_DS_FAILURE LDP_FAILURE

#define LDP_DS_MAX_CLASS_TYPE 3
#define LDP_DS_ELSP_PHB_VALID 1

#define INTSERV               TE_INTSERV 
#define NON_DIFFSERV_LSP      TE_NON_DIFFSERV_LSP
#define LDP_DS_ELSP           TE_DS_ELSP
#define LDP_DS_LLSP           TE_DS_LLSP

#define LDP_DS_DF_DSCP        TE_DS_DF_DSCP
#define LDP_DS_CS1_DSCP       TE_DS_CS1_DSCP 
#define LDP_DS_CS2_DSCP       TE_DS_CS2_DSCP
#define LDP_DS_CS3_DSCP       TE_DS_CS3_DSCP
#define LDP_DS_CS4_DSCP       TE_DS_CS4_DSCP
#define LDP_DS_CS5_DSCP       TE_DS_CS5_DSCP
#define LDP_DS_CS6_DSCP       TE_DS_CS6_DSCP
#define LDP_DS_CS7_DSCP       TE_DS_CS7_DSCP
#define LDP_DS_EF_DSCP        TE_DS_EF_DSCP
#define LDP_DS_AF11_DSCP      TE_DS_AF11_DSCP
#define LDP_DS_AF12_DSCP      TE_DS_AF12_DSCP
#define LDP_DS_AF13_DSCP      TE_DS_AF13_DSCP
#define LDP_DS_AF21_DSCP      TE_DS_AF21_DSCP
#define LDP_DS_AF22_DSCP      TE_DS_AF22_DSCP
#define LDP_DS_AF23_DSCP      TE_DS_AF23_DSCP
#define LDP_DS_AF31_DSCP      TE_DS_AF31_DSCP
#define LDP_DS_AF32_DSCP      TE_DS_AF32_DSCP
#define LDP_DS_AF33_DSCP      TE_DS_AF33_DSCP
#define LDP_DS_AF41_DSCP      TE_DS_AF41_DSCP
#define LDP_DS_AF42_DSCP      TE_DS_AF42_DSCP
#define LDP_DS_AF43_DSCP      TE_DS_AF43_DSCP
#define LDP_DS_AF1_PSC_DSCP   TE_DS_AF11_DSCP
#define LDP_DS_AF2_PSC_DSCP   TE_DS_AF21_DSCP
#define LDP_DS_AF3_PSC_DSCP   TE_DS_AF31_DSCP
#define LDP_DS_AF4_PSC_DSCP   TE_DS_AF41_DSCP

#define LDP_DS_STAT_UNEXPECTED_DIFFSERV_TLV            0x01000001
#define LDP_DS_STAT_UNSUPPORTED_PHB                    0x01000002
#define LDP_DS_STAT_INVALID_EXP_PHB_MAPPING            0x01000003
#define LDP_DS_STAT_UNSUPPORTED_PSC                    0x01000004
#define LDP_DS_STAT_PER_LSPCONTEXT_ALLOCFAIL           0x01000005

#define LDP_DS_STAT_UNSUPPORTED_CLASS_TYPE_TLV         TBD
#define LDP_DS_STAT_INVALID_CLASS_TYPE_VALUE           2*TBD
#define LDP_DS_STAT_UNEXP_CLASS_TYPE_TLV               3 *TBD


#define LDP_DS_STAT_ELSPTP_UNSUPPORTED_PSC             0x01000011
#define LDP_DS_STAT_ELSPTP_UNAVL_OARSRC                0x01000012
#define LDP_DS_STAT_RESOURCE_UNAVAIL                   0x04000005
#define LDP_DS_STAT_UNSUPPORTED_ELSPTP_TLV             0x01000010 

#define LDP_DS_MAX_STATUS_TYPES                13
#define LDP_DS_ELSPINFO_TABLE_DEF_OFFSET       TE_DS_ELSPINFO_TABLE_DEF_OFFSET

#define LDP_DS_STAT_TYPE_UNEXPECTED_DIFFSERV_TLV        0
#define LDP_DS_STAT_TYPE_UNSUPPORTED_PHB                1
#define LDP_DS_STAT_TYPE_INVALID_EXP_PHB_MAPPING        2
#define LDP_DS_STAT_TYPE_UNSUPPORTED_PSC                3
#define LDP_DS_STAT_TYPE_PER_LSPCONTEXT_ALLOCFAIL       4
#define LDP_DS_STAT_TYPE_UNSUPPORTED_CLASS_TYPE_TLV     5
#define LDP_DS_STAT_TYPE_INVALID_CLASS_TYPE_VALUE       6
#define LDP_DS_STAT_TYPE_ELSPTP_UNSUPPORTED_PSC         7
#define LDP_DS_STAT_TYPE_ELSPTP_UNAVL_OARSRC            8
#define LDP_DS_STAT_TYPE_RESOURCE_UNAVAIL               9
#define LDP_DS_STAT_TYPE_UNSUPPORTED_ELSPTP_TLV         10
#define LDP_DS_STAT_TYPE_UNEXP_CLASS_TYPE_TLV           11

#define LDP_DS_UNEXPECTED_TLV             1
#define LDP_DS_UNSUPPORTED_PHB            2
#define LDP_DS_INVALID_EXP_PHB_MAPPING    3
#define LDP_DS_UNSUPPORTED_PSC            4
#define LDP_DS_PER_LSP_CONTEXT_ALLOC_FAIL 5
#define LDP_DS_MEM_ALLOC_FAILURE          6
#define LDP_DS_ELSPTP_UNSUPPORTED_PSC     7
#define LDP_DS_ELSPTP_UNAVL_OARSRC        8
#define LDP_DS_ELSPTP_TLV_UNSUP           10

#define LDP_DIFFSERV_CRLSP                TE_DS_DIFFSERV_TLV
#define LDP_RSRC_PEROA_CRLSP              TE_DS_RSRC_PEROA_TLV
#define LDP_CLASSTYPE_CRLSP               TE_DS_CLASSTYPE_TLV

#define LDP_DS_BIT14_MASK                 0x0002
#define LDP_DS_DSCP_PHBID_MASK            0xfc00
#define LDP_DS_DSCP_TO_LLSP_PHBID_MASK    0x0002
#define LDP_DS_DSCP_TO_ELSP_PHBID_MASK    0x0000
#define LDP_DS_DSCP_TO_PSC_MASK           0x0002

#define CRLDP_DIFFSERV_TLV                0x0901
#define CRLDP_DIFFSERV_ELSPTP_TLV         0x910
#define CRLDP_DIFFSERV_CLASSTYPE_TLV      0x999

#define LDP_DS_ELSP_TBIT 0 
#define LDP_DS_LLSP_TBIT 1 
#define LDP_DS_RESERVED  0

#define LDP_DS_LLSP_TLV_LEN         4
#define LDP_DIFF_PRECONF_LEN        4   
#define LDP_DS_CLASSTYPE_TLV_LEN    4

#define LDP_DS_PRECONF_ELSP          TE_DS_PRECONF_ELSP
#define LDP_DS_SIG_ELSP              TE_DS_SIG_ELSP

#define LDP_DS_CLASS_TYPE0  TE_DS_CLASS_TYPE0 
#define LDP_DS_CLASS_TYPE1  TE_DS_CLASS_TYPE1
#define LDP_DS_CLASS_TYPE2  TE_DS_CLASS_TYPE2
#define LDP_DS_CLASS_TYPE3  TE_DS_CLASS_TYPE3
#define LDP_DS_CLASS_TYPE4  TE_DS_CLASS_TYPE4

/*Resource manager related defines */
#define LDP_DS_CLASSTYPE_RESOURCES  TE_DS_CLASSTYPE_RESOURCES 
#define LDP_DS_PEROABASED_RESOURCES TE_DS_PEROABASED_RESOURCES 


#define LDP_DS_MAX_NO_OF_CLASSTYPES 3
#define LDP_DS_MAX_NO_OF_PHBS       14
#define LDP_DS_MAX_NO_PSCS          13

#endif
/*---------------------------------------------------------------------------*/
/*                        End of file ldpdsdef.h                              */
/*---------------------------------------------------------------------------*/
