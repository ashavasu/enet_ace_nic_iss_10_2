/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ldpsz.h,v 1.6 2015/02/28 12:19:38 siva Exp $
*
* Description: This file contains sizing related MACROS
*
*******************************************************************/
enum {
    MAX_LDP_ADJS_SIZING_ID,
    MAX_LDP_ATM_LBL_KEY_INFO_SIZING_ID,
    MAX_LDP_ATM_LBL_RANGE_SIZING_ID,
    MAX_LDP_ATM_PARAMS_SIZING_ID,
    MAX_LDP_CRLSP_TNL_INFO_SIZING_ID,
    MAX_LDP_ENTITIES_SIZING_ID,
    MAX_LDP_FEC_TABLE_SIZING_ID,
    MAX_LDP_GEN_RANGE_BLKS_SIZING_ID,
    MAX_LDP_IFACES_SIZING_ID,
    MAX_LDP_INT_LSP_SIZING_ID,
    MAX_LDP_IP_RT_ENTRIES_SIZING_ID,
    MAX_LDP_L2VPN_IF_STR_INFO_SIZING_ID,
    MAX_LDP_L2VPN_PWVC_EVT_INFO_SIZING_ID,
    MAX_LDP_ICCP_PWVC_REQUESTS_SIZING_ID,
    MAX_LDP_ICCP_PWVC_DATA_ENTRIES_SIZING_ID,
    MAX_LDP_LSP_LCB_SIZING_ID,
    MAX_LDP_LSP_TRIG_LCB_SIZING_ID,
    MAX_LDP_LSP_UPSTR_LCB_SIZING_ID,
    MAX_LDP_MSGS_SIZING_ID,
    MAX_LDP_OA_TRFC_PROFILE_SIZING_ID,
    MAX_LDP_PEER_IF_SIZING_ID,
    MAX_LDP_PEER_PASSWD_INFO_SIZING_ID,
    MAX_LDP_PEERS_SIZING_ID,
    MAX_LDP_QDEPTH_SIZING_ID,
    MAX_LDP_RECV_BUF_SIZING_ID,
    MAX_LDP_SESSIONS_SIZING_ID,
    MAX_LDP_SOCK_RECV_BUF_SIZING_ID,
    MAX_LDP_TCP_PEND_WRITE_BUF_SIZING_ID,
    MAX_LDP_TCP_SEND_BUF_SIZING_ID,
    MAX_LDP_TCP_UDP_SOCK_INFO_SIZING_ID,
    MAX_LDP_TEMP_TE_HOP_INFO_SIZING_ID,
    MAX_LDP_TNL_INCARN_INFO_SIZING_ID,
    MAX_LDP_UDP_RECV_BUF_SIZING_ID,
    MAX_LDP_UDP_SEND_BUF_SIZING_ID,
#ifdef MPLS_LDP_BFD_WANTED
    MAX_LDP_BFD_SESSIONS_SIZING_ID,
#endif
    LDP_MAX_SIZING_ID
};


#ifdef  _LDPSZ_C
tMemPoolId LDPMemPoolIds[ LDP_MAX_SIZING_ID];
INT4  LdpSizingMemCreateMemPools(VOID);
VOID  LdpSizingMemDeleteMemPools(VOID);
INT4  LdpSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _LDPSZ_C  */
extern tMemPoolId LDPMemPoolIds[ ];
extern INT4  LdpSizingMemCreateMemPools(VOID);
extern VOID  LdpSizingMemDeleteMemPools(VOID);
extern INT4  LdpSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _LDPSZ_C  */


#ifdef  _LDPSZ_C
tFsModSizingParams FsLDPSizingParams [] = {
{ "tLdpAdjacency", "MAX_LDP_ADJS", sizeof(tLdpAdjacency),MAX_LDP_ADJS, MAX_LDP_ADJS,0 },
{ "tKeyInfoStructSize", "MAX_LDP_ATM_LBL_KEY_INFO", sizeof(tKeyInfoStructSize),MAX_LDP_ATM_LBL_KEY_INFO, MAX_LDP_ATM_LBL_KEY_INFO,0 },
{ "tAtmLdpLblRngEntry", "MAX_LDP_ATM_LBL_RANGE", sizeof(tAtmLdpLblRngEntry),MAX_LDP_ATM_LBL_RANGE, MAX_LDP_ATM_LBL_RANGE,0 },
{ "tLdpAtmParams", "MAX_LDP_ATM_PARAMS", sizeof(tLdpAtmParams),MAX_LDP_ATM_PARAMS, MAX_LDP_ATM_PARAMS,0 },
{ "tCrlspTnlInfo", "MAX_LDP_CRLSP_TNL_INFO", sizeof(tCrlspTnlInfo),MAX_LDP_CRLSP_TNL_INFO, MAX_LDP_CRLSP_TNL_INFO,0 },
{ "tLdpEntity", "MAX_LDP_ENTITIES", sizeof(tLdpEntity),MAX_LDP_ENTITIES, MAX_LDP_ENTITIES,0 },
{ "tFecTableEntrySize", "MAX_LDP_FEC_TABLE", sizeof(tFecTableEntrySize),MAX_LDP_FEC_TABLE, MAX_LDP_FEC_TABLE,0 },
{ "tLdpEthParams", "MAX_LDP_GEN_RANGE_BLKS", sizeof(tLdpEthParams),MAX_LDP_GEN_RANGE_BLKS, MAX_LDP_GEN_RANGE_BLKS,0 },
{ "tLdpIfTableEntry", "MAX_LDP_IFACES", sizeof(tLdpIfTableEntry),MAX_LDP_IFACES, MAX_LDP_IFACES,0 },
{ "tIntLspSetupInfo", "MAX_LDP_INT_LSP", sizeof(tIntLspSetupInfo),MAX_LDP_INT_LSP, MAX_LDP_INT_LSP,0 },
{ "tLdpRouteEntryInfo", "MAX_LDP_IP_RT_ENTRIES", sizeof(tLdpRouteEntryInfo),MAX_LDP_IP_RT_ENTRIES, MAX_LDP_IP_RT_ENTRIES,0 },
{ "tL2VpnLdpIfStrLenSize", "MAX_LDP_L2VPN_IF_STR_INFO", sizeof(tL2VpnLdpIfStrLenSize),MAX_LDP_L2VPN_IF_STR_INFO, MAX_LDP_L2VPN_IF_STR_INFO,0 },
{ "tL2VpnLdpPwVcEvtInfo", "MAX_LDP_L2VPN_PWVC_EVT_INFO", sizeof(tL2VpnLdpPwVcEvtInfo),MAX_LDP_L2VPN_PWVC_EVT_INFO, MAX_LDP_L2VPN_PWVC_EVT_INFO,0 },
{ "tPwRedDataEvtPwFec",         "MAX_L2VPN_ICCP_PWVC_REQUESTS",         sizeof (tPwRedDataEvtPwFec),        MAX_L2VPN_ICCP_PWVC_REQUESTS,       MAX_L2VPN_ICCP_PWVC_REQUESTS, 0 },  /* PW-RED SIZING INFO. */
{ "tPwRedDataEvtPwData",        "MAX_L2VPN_ICCP_PWVC_DATA_ENTRIES",     sizeof (tPwRedDataEvtPwData),       MAX_L2VPN_ICCP_PWVC_DATA_ENTRIES,   MAX_L2VPN_ICCP_PWVC_DATA_ENTRIES, 0 },
{ "tLspCtrlBlock", "MAX_LDP_LSP_LCB", sizeof(tLspCtrlBlock),MAX_LDP_LSP_LCB, MAX_LDP_LSP_LCB,0 },
{ "tLspTrigCtrlBlock", "MAX_LDP_LSP_TRIG_LCB", sizeof(tLspTrigCtrlBlock),MAX_LDP_LSP_TRIG_LCB, MAX_LDP_LSP_TRIG_LCB,0 },
{ "tUstrLspCtrlBlock", "MAX_LDP_LSP_UPSTR_LCB", sizeof(tUstrLspCtrlBlock),MAX_LDP_LSP_UPSTR_LCB, MAX_LDP_LSP_UPSTR_LCB,0 },
{ "tLdpMinMTUSize", "MAX_LDP_MSGS", sizeof(tLdpMinMTUSize),MAX_LDP_MSGS, MAX_LDP_MSGS,0 },
{ "tPerOATrfcProfileEntry", "MAX_LDP_OA_TRFC_PROFILE", sizeof(tPerOATrfcProfileEntry),MAX_LDP_OA_TRFC_PROFILE, MAX_LDP_OA_TRFC_PROFILE,0 },
{ "tPeerIfAdrNode", "MAX_LDP_PEER_IF", sizeof(tPeerIfAdrNode),MAX_LDP_PEER_IF, MAX_LDP_PEER_IF,0 },
{ "tLdpEntityPeerPasswdInfo", "MAX_LDP_PEER_PASSWD_INFO", sizeof(tLdpEntityPeerPasswdInfo),MAX_LDP_PEER_PASSWD_INFO, MAX_LDP_PEER_PASSWD_INFO,0 },
{ "tLdpPeer", "MAX_LDP_PEERS", sizeof(tLdpPeer),MAX_LDP_PEERS, MAX_LDP_PEERS,0 },
{ "tLdpIfMsg", "MAX_LDP_QDEPTH", sizeof(tLdpIfMsg),MAX_LDP_QDEPTH, MAX_LDP_QDEPTH,0 },
{ "tLdpMaxFragmentSize", "MAX_LDP_RECV_BUF", sizeof(tLdpMaxFragmentSize),MAX_LDP_RECV_BUF, MAX_LDP_RECV_BUF,0 },
{ "tLdpSession", "MAX_LDP_SESSIONS", sizeof(tLdpSession),MAX_LDP_SESSIONS, MAX_LDP_SESSIONS,0 },
{ "tLdpMaxFragmentSize", "MAX_LDP_SOCK_RECV_BUF", sizeof(tLdpMaxFragmentSize),MAX_LDP_SOCK_RECV_BUF, MAX_LDP_SOCK_RECV_BUF,0 },
{ "tLdpPendMaxFragmentSize", "MAX_LDP_TCP_PEND_WRITE_BUF", sizeof(tLdpPendMaxFragmentSize),MAX_LDP_TCP_PEND_WRITE_BUF, MAX_LDP_TCP_PEND_WRITE_BUF,0 },
{ "tLdpMinMTUSize", "MAX_LDP_TCP_SEND_BUF", sizeof(tLdpMinMTUSize),MAX_LDP_TCP_SEND_BUF, MAX_LDP_TCP_SEND_BUF,0 },
{ "tLdpTcpUdpSockInfoSize", "MAX_LDP_TCP_UDP_SOCK_INFO", sizeof(tLdpTcpUdpSockInfoSize),MAX_LDP_TCP_UDP_SOCK_INFO, MAX_LDP_TCP_UDP_SOCK_INFO,0 },
{ "tTeHopInfo", "MAX_LDP_TEMP_TE_HOP_INFO", sizeof(tTeHopInfo),MAX_LDP_TEMP_TE_HOP_INFO, MAX_LDP_TEMP_TE_HOP_INFO,0 },
{ "tTnlIncarnNode", "MAX_LDP_TNL_INCARN_INFO", sizeof(tTnlIncarnNode),MAX_LDP_TNL_INCARN_INFO, MAX_LDP_TNL_INCARN_INFO,0 },
{ "tLdpMinMTUSize", "MAX_LDP_UDP_RECV_BUF", sizeof(tLdpMinMTUSize),MAX_LDP_UDP_RECV_BUF, MAX_LDP_UDP_RECV_BUF,0 },
{ "tLdpMinMTUSize", "MAX_LDP_UDP_SEND_BUF", sizeof(tLdpMinMTUSize),MAX_LDP_UDP_SEND_BUF, MAX_LDP_UDP_SEND_BUF,0 },
#ifdef MPLS_LDP_BFD_WANTED
{ "tLdpBfdSession", "MAX_LDP_BFD_SESSIONS", sizeof(tLdpBfdSession),MAX_LDP_BFD_SESSIONS, MAX_LDP_BFD_SESSIONS,0},
#endif
{"\0","\0",0,0,0,0}
};
#else  /*  _LDPSZ_C  */
extern tFsModSizingParams FsLDPSizingParams [];
#endif /*  _LDPSZ_C  */


