/********************************************************************
 **                                                                  *
 ** $RCSfile: ldpdudsm.h,v $
 **
 ** $Id: ldpdudsm.h,v 1.5 2014/11/08 11:41:01 siva Exp $
 **                                                                  *
 ** $Date: 2014/11/08 11:41:01 $                                     *
 **                                                                  *
 ** $Revision: 1.5 $                                                 *
 **                                                                  *
 ********************************************************************/


/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : dudnsm.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains definition of global variable
 *                             associated with the DU Downstream LSP State
 *                             machine. The SEM is initialised in this file.
 *---------------------------------------------------------------------------*/

#ifndef _DUDNSM_H
#define _DUDNSM_H

tDuDnFsmFuncPtr
    gLdpDuDnFsm[LDP_MAX_DU_DN_STATES][LDP_MAX_DU_DN_EVENTS] =
{
    /* IDLE state */
    {
        LdpDuDnInvStEvt,      /* Label Req Event */ 
        LdpDuDnIdLdpMap,      /* LDP Mapping Event */
        LdpDuDnInvStEvt,        /* LDP Release Event */
        LdpDuDnInvStEvt,       /* LDP Withdraw Event */
        LdpDuDnInvStEvt,        /* LDP Up abort event */
        LdpDuDnInvStEvt,        /* Downstream NAK event */
        LdpDuDnInvStEvt,        /* Upstream Lost event */
        LdpDuDnInvStEvt,       /* Downstream Lost Event */
        LdpDuDnInvStEvt,        /* Internal setup event */
        LdpDuDnInvStEvt,        /* Internal Destroy event */
        LdpDuDnInvStEvt,        /* Internal X-connect event */
        LdpDuDnInvStEvt,       /* Next Hop Change Event */
        LdpDuDnInvStEvt,        /* PreEmption event */
        LdpDuDnIdDelFec,      /* Delete Fec Event */
        LdpDuDnInvStEvt       /* Resoure Available*/ 
    }
    ,
   /* INVALID state introduced for interoperation between
    *  DoD and DU */
    {
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,       
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt,      
        LdpDuDnInvStEvt
    }
    ,
    /* ESTABLISHED state */
    {
        LdpDuDnInvStEvt,     /* Label Req Event */ 
        LdpDuDnEstLdpMap,     /* LDP Mapping Event */
        LdpDuDnInvStEvt,        /* LDP Release Event */
        LdpDuDnEstLdpWR,      /* LDP Withdraw Event */
        LdpDuDnInvStEvt,        /* LDP Up abort Event */
        LdpDuDnInvStEvt,        /* Downstream NAK event */
        LdpDuDnInvStEvt,        /* Upstream lost Event */
        LdpDuDnEstDnLost,      /* Downstream Lost Event */
        LdpDuDnInvStEvt,        /* Internal setup event */
        LdpDuDnInvStEvt,        /* Internal Destroy event */
        LdpDuDnInvStEvt,        /* Internal X-connect event */
        LdpDuDnEstNHChg,      /* Next Hop Change Event */
        LdpDuDnInvStEvt,        /* PreEmption event */
        LdpDuDnEstDelFec,     /* Delete Fec Event */
        LdpDuDnInvStEvt       /* Resoure Available*/ 
    }
};

CONST CHR1  *au1LdpDuDnStates[LDP_MAX_DU_DN_STATES] = {
                                                          "IDLE",
                                                          "INVALID EVT",
                                                          "ESTABLISHED" 
                                               };
 
CONST CHR1 *au1LdpDuDnEvents[LDP_MAX_DU_DN_EVENTS] = {
                                                         "INVALID EVT",
                                                         "LDP MAPPING",
                                                         "INVALID EVT",
                                                         "LDP WITHDRAW",
                                                         "INVALID EVT",
                                                         "INVALID EVT",
                                                         "INVALID EVT",
                                                         "DOWNSTREAM LOST",
                                                         "INVALID EVT",
                                                         "INVALID EVT",
                                                         "INVALID EVT",
                                                         "NEXT HOP CHANGE EVT",
                                                         "INVALID EVT",
                                                         "DELETE FEC EVT",
                                                         "RSRC ABLE EVT"
                                                }; 



#endif /*_DUDNSM_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpadsm1.h                             */
/*---------------------------------------------------------------------------*/
