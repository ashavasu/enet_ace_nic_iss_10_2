#ifndef _STDLDAWR_H
#define _STDLDAWR_H
INT4 GetNextIndexMplsLdpEntityAtmTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDLDA(VOID);

VOID UnRegisterSTDLDA(VOID);
INT4 MplsLdpEntityAtmIfIndexOrZeroGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmMergeCapGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRComponentsGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmVcDirectionalityGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLsrConnectivityGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmDefaultControlVpiGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmDefaultControlVciGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmUnlabTrafVpiGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmUnlabTrafVciGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmIfIndexOrZeroSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmMergeCapSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmVcDirectionalitySet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLsrConnectivitySet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmDefaultControlVpiSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmDefaultControlVciSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmUnlabTrafVpiSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmUnlabTrafVciSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmIfIndexOrZeroTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmMergeCapTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmVcDirectionalityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLsrConnectivityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmDefaultControlVpiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmDefaultControlVciTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmUnlabTrafVpiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmUnlabTrafVciTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexMplsLdpEntityAtmLRTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLdpEntityAtmLRMaxVpiGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRMaxVciGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRMaxVpiSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRMaxVciSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRMaxVpiTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRMaxVciTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityAtmLRTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexMplsLdpAtmSessionTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLdpSessionAtmLRUpperBoundVpiGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpSessionAtmLRUpperBoundVciGet(tSnmpIndex *, tRetVal *);
#endif /* _STDLDAWR_H */
