/********************************************************************
 *                                                                  *
 * $RCSfile: ldpdspt.h,v $
 *                                                                  *
 * $Date: 2010/09/24 06:25:30 $                                     *
 *                                                                  *
 * $Revision: 1.4 $                                             *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpdspt.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : CRLDP-DIFFSERV
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains prototypes of all diffServ 
 *                             functions
 *---------------------------------------------------------------------------*/
#ifndef _LDP_DSPROT_H
#define _LDP_DSPROT_H

UINT1
LdpDiffServProcHandleDSTlv ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
                 UINT1 *pu1DiffServTlv, UINT1 *pMsg, UINT1 *pu1ClassTypeTlv));

UINT1
LdpDiffServUpdateElspInfo ARG_LIST ((tMplsDiffServTnlInfo * pDiffServParams,
                            UINT1 *pu1DiffServTlv, UINT1 *pu1ErrType,
                            UINT4 u4IncarnId, tLspCtrlBlock * pLspCtrlBlock));

UINT1
LdpDiffServUpdateLlspInfo ARG_LIST ((tMplsDiffServTnlInfo * pDiffServParams,
                            UINT1 *pu1DiffServTlv, UINT1 *pu1ErrType, tLspCtrlBlock * pLspCtrlBlock));

UINT1 LdpDiffServValidateDscp ARG_LIST ((UINT1 u1Dscp));

UINT1 LdpSendDiffServNotifMsg ARG_LIST ((tLdpSession *pSessionEntry, 
                                   UINT1 *pu1Msg, 
                                   UINT1 *pu1RetPDU, 
                                   UINT1 u1DiffServStatusType,
                                  tLspCtrlBlock *pLspCtrlBlock));

UINT1 LdpDiffServConstructCopyDSTlv ARG_LIST ((tCrlspTnlInfo* pCrlspTnlInfo,
                             tCRU_BUF_CHAIN_HEADER *pMsg, UINT4* pu4Offset));

UINT1  LdpDiffServGetOptionalDSTlvLength ARG_LIST ((tCrlspTnlInfo* pCrlspTnlInfo, UINT2* pu2OptionalLength));

UINT1
LdpDiffServDeleteMem ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo, UINT2 u2IncarnId));

UINT1
ldpDiffServDeleteElspInfoMem ARG_LIST ((tTeTnlInfo * pTeTnlInfo));

UINT1
ldpDiffServCheckIngressOfStackTnl ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock));

UINT1 ldpDiffServCheckForModels ARG_LIST ((tCrlspTnlInfo* pCrlspTnlInfo, tCrlspTnlInfo* pCrlspStkTnlInfo));

UINT1
LdpDiffServHandleElspTPTlv ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg, UINT1 *pu1ElspTPTlv));

UINT1
LdpDiffServConstructCopyElspTPTlv ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo,
                                    tCRU_BUF_CHAIN_HEADER * pMsg, UINT4 *pu4Offset));

UINT1
LdpDiffServHandleClassTypeTlv ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
                                UINT1 *pu1ClassTypeTlv, UINT1 *pMsg));
UINT1
LdpDiffServConstructCopyClassTypeTlv ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo,
                                       tCRU_BUF_CHAIN_HEADER *pMsg, UINT4 *pu4Offset));

UINT1
ldpDiffServUpdateElspInfoTable ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo));


UINT1 LdpDiffServGetOptionalElspTPLength ARG_LIST ((tCrlspTnlInfo* pCrlspTnlInfo, UINT2* pu2OptionalLength));

UINT1
LdpDiffServGetIndexForPsc ARG_LIST ((UINT1 u1Psc, UINT2 *pu2Index));

UINT1
ldpDiffServValidateElspTPPsc ARG_LIST ((UINT2 u2Psc, tCrlspTnlInfo * pCrlspTnlInfo, UINT4 u4Incarn));

UINT1
LdpDiffServTrfcRsrcForLLsp ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));

UINT1
LdpDiffServTcResvResources ARG_LIST ((tCrlspTnlInfo *pCrlspTnlInfo));

VOID
LdpDiffServHandlePerOARsrc ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));

VOID
LdpDiffServTcFreeResources ARG_LIST ((tCrlspTnlInfo *pCrlspTnlInfo));

UINT1
LdpDiffServCrlspGetTnlFromPrioList ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfoInput, tTMO_SLL * pCandidatePreemp));

UINT1
LdpDSTcResvResourcesInClassType ARG_LIST ((tCrlspTnlInfo *pCrlspTnlInfo));

VOID
LdpDSRelResourcesInClassType ARG_LIST((tCrlspTnlInfo *pCrlspTnlInfo));

UINT4 LdpDiffServGetSumOfTrfcParms ARG_LIST ((tCrlspTnlInfo *pCrlspTnlInfo));

UINT1
LdpDiffServHandleModElspTrfcProfile ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock,
                                      UINT1 *pMsg, UINT1 *pu1ElspTPTlv));

UINT1
LdpDiffServHandleSigPhbs ARG_LIST ((tTeTnlInfo * pTeTnlInfo, UINT4 u4OutIfIndex));
UINT1
LdpDiffServHandleDSIngressNode ARG_LIST ((tLspCtrlBlock *pLspCtrlBlock, UINT2 u2IncarnId));

UINT1
LdpDiffServHandlePerOATrfcParams ARG_LIST ((tCrlspTnlInfo * pCrlspTnlInfo, UINT2 u2IncarnId));

UINT1
LdpDiffServHandleRMPerOARsrc ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock));

UINT1 LdpDiffServGetPhbPsc ARG_LIST ((UINT1 u1PhbDscp));

UINT1 ldpDiffServElspListRowStatusActive ARG_LIST ((INT4  i4ElspInfoListIndex) );

UINT1 ldpDiffServChkIsPerOAResrcConf ARG_LIST((INT4  i4ElspInfoListIndex));

UINT1 ldpDiffServChkRsrcForPreConf ARG_LIST((tMplsDiffServParams *pDiffServParams));

UINT1 ldpDiffServGetElspInfoNode ARG_LIST((INT4  i4ElspInfoListIndex, 
                                    INT4  i4ElspInfoIndex,
             	tMplsDiffServElspInfo **ppElspInfoNode)); 

VOID
LdpDiffServAddTnlToPrioList ARG_LIST ((UINT4 u4IfIndex, UINT1 u1Priority,
                          tCrlspTnlInfo * pCrlspTnlInfoInput));

VOID
LdpDiffServModifyTnlPrioList ARG_LIST ((UINT4 u4CurrIfIndex, UINT1 u1CurrPriority,
                           UINT4 u4NewIfIndex, UINT1 u1NewPriority,
                           tCrlspTnlInfo * pCrlspTnlInfoInput));

VOID
LdpDiffServRemTnlFromPrioList ARG_LIST ((UINT4 u4IfIndex, UINT1 u1Priority,
                            tCrlspTnlInfo * pCrlspTnlInfoInput));
VOID
LdpDiffServPreEmptTunnels ARG_LIST ((tTMO_SLL * pCandidatePreemp));

UINT1
LdpDiffServElspTPForRM ARG_LIST ((tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg));

#endif
/*---------------------------------------------------------------------------*/
/*                        End of file ldpdspt.h                              */
/*---------------------------------------------------------------------------*/


