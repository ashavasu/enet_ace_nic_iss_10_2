
 /* $Id: stdgnlwr.h,v 1.4 2012/10/24 10:44:06 siva Exp $*/
#ifndef _STDGNLWR_H
#define _STDGNLWR_H
INT4 GetNextIndexMplsLdpEntityGenericLRTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDGNL(VOID);
VOID UnRegisterSTDGNL(VOID);
INT4 MplsLdpEntityGenericLRMinGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLRMaxGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLabelSpaceGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericIfIndexOrZeroGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLRStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLRRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLabelSpaceSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericIfIndexOrZeroSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLRStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLRRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLabelSpaceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericIfIndexOrZeroTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLRStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLRRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLdpEntityGenericLRTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




#endif /* _STDGNLWR_H */
