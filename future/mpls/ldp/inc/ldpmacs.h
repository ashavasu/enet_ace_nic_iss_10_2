
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpmacs.h,v 1.33 2016/02/28 10:58:24 siva Exp $
 *              
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpmacs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains macros used throughout LDP module
 *---------------------------------------------------------------------------*/
#ifndef _LDP_MACS_H
#define _LDP_MACS_H

#define LDP_PROC_EVT_FUNC     gLdpProcEvtFunc
#define LDP_SESSION_FSM       gLdpSsnFsm
#define LDP_NON_MRG_FSM       gLdpNonMrgFsm

#define LDP_TSK_ID             (gLdpInfo.TaskId)
#define LDP_INITIALISED        (gLdpInfo.u1LdpInitialised)
#define LDP_QID                (gLdpInfo.QId)
#define LDP_UDP_QID            (gLdpInfo.UdpQId)
#define LDP_TCP_QID            (gLdpInfo.gLdpTcpQId)
#define LDP_SYSLOG_ID          (gLdpInfo.i4SyslogId)
#define LDP_SEM_ID             (gLdpInfo.SemId)
#define LDP_ACTIVE_INCRN_COUNT (gLdpInfo.u4ActiveIncarn)
#define LDP_TIMER_LIST_ID      (gLdpInfo.TimerListId)
#define LDP_MIN(a,b) \
           (((a) < (b)) ? (a) : (b))

#define LDP_MAX(a, b) \
           (((a) > (b)) ? (a) : (b))

/* Macros for setting and getting major and minor LDP events */
#define LDP_SET_EVENT(u4Event,u4LdpMajEvent,u4LdpMinEvent) \
   u4Event = ( (u4Event | u4LdpMinEvent) << 16);\
   u4Event = ( u4Event | u4LdpMajEvent);

#define LDP_GET_MAJ_EVENT(u4MajEvent,u4RcvdEvent) \
   u4MajEvent = (u4RcvdEvent & LDP_MAJOR_EVENT_MASK);

#define LDP_GET_MIN_EVENT(u4MinEvent,u4RcvdEvent) \
   u4MinEvent = ( (u4RcvdEvent & LDP_MINOR_EVENT_MASK) >> 16 );

/* Configurable Parameters */
#define LDP_LSR_ID(x) \
             (gLdpInfo.LdpIncarn[(x)].LsrId)
#define LDP_NEW_LSR_ID(x) \
             (gLdpInfo.LdpIncarn[(x)].NewLsrId)
#define LDP_FORCE_OPTION(x) \
             (gLdpInfo.LdpIncarn[(x)].u1LdpForceOption)
#ifdef MPLS_IPV6_WANTED
#define LDP_LAST_V4IF_DOWN(x) \
  (gLdpInfo.LdpIncarn[(x)].u1LastV4InterfaceDown)
#endif
#define LDP_INCARN_ID(x) \
             (gLdpInfo.LdpIncarn[(x)].u2IncarnIndex)
#define LDP_LOOP_DETECT(x) \
             (gLdpInfo.LdpIncarn[(x)].u1LoopDetect)
#define LDP_LBL_RET_MODE(x) \
             (gLdpInfo.LdpIncarn[(x)].u1LabelRetentionMode)

#ifdef MPLS_LDP_BFD_WANTED
#define LDP_BFD_SESSION_COUNT(x) \
             (gLdpInfo.LdpIncarn[(x)].u2BfdSessionsCount)
#endif

#define LDP_MAX_ENTITIES(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u4MaxLdpEntities)
#define LDP_MAX_LOCAL_PEERS(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u2MaxLocalPeers)
#define LDP_MAX_REMOTE_PEERS(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u2MaxRemotePeers)
#define LDP_MAX_SESSIONS(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u2MaxSessions)
#define LDP_MAX_INTERFACES(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u2MaxIfaces)
#define LDP_MAX_LSPS(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u2MaxLsps)
#define LDP_MAX_CRLSP_TNLS(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u2MaxCrlspTnls)
#define LDP_MAX_ADJS(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u2MaxAdj)
#define LDP_MAX_ATM_RNG_BLKS(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u2MaxAtmLblRngBlks)
#define LDP_MAX_ATM_VC_MRG_COUNT(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u2MaxAtmVcMergeCount)
#define LDP_MAX_ATM_VP_MRG_COUNT(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u2MaxAtmVpMergeCount)
#define LDP_MAX_PATH_VECT_LIM(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u1PathVectorLimit)
#define LDP_MAX_HOP_CNT_LIM(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u1HopCountLimit)
#define LDP_MD5_SIG_OPT(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u1Md5SigOption)
#define LDP_MAX_TEMP_TE_HOP_INFO(x) \
             (gLdpInfo.LdpIncarn[(x)].LdpParams.u1MaxTempHopInfo)
#define LDP_SESS_TRAP_ENB(x) \
             (gLdpInfo.LdpIncarn[(x)].u2SessionTrapEnb)
#define LDP_ENTITY_IND_INFO_HEAD(x) \
             (gLdpInfo.LdpIncarn[(x)].EntityIndInfoHead)
#define LDP_FEC_INFO_HEAD(x) \
             (gLdpInfo.LdpIncarn[(x)].FecInfoHead)
#define LDP_PEER_IFADR_TABLE(x) \
             (gLdpInfo.LdpIncarn[(x)].pPeerIfAdrTable)

#define LDP_PEER_IPV6_IFADR_TABLE(x) \
 (gLdpInfo.LdpIncarn[(x)].pPeerIfIpv6AdrTable) 

#define LDP_CRLSP_TNL_TABLE(x) \
             (gLdpInfo.LdpIncarn[(x)].pCrlspTnlTbl)
#define LDP_TCPCONN_HSH_TABLE(x) \
             (gLdpInfo.LdpIncarn[(x)].pTcpConnHshTbl)

#define LDP_FECROUTE_TIMER(x) \
             (gLdpInfo.LdpIncarn[(x)].FecRouteTimer)
#define LDP_FECROUTE_ARRAY(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y])

#define LDP_FECROUTE_ARRAY_PREFIX(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y].Prefix)

#define LDP_FECROUTE_ARRAY_NHOP(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y].NextHop)

#define LDP_FECROUTE_ARRAY_IPV4_PREFIX(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y].Prefix.u4Addr)
#define LDP_FECROUTE_ARRAY_IPV4_NHOP(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y].NextHop.u4Addr)

#define LDP_FECROUTE_ARRAY_IPV6_PREFIX(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y].Prefix.Ip6Addr.u1_addr)
#define LDP_FECROUTE_ARRAY_IPV6_NHOP(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y].NextHop.Ip6Addr.u1_addr)


#define LDP_FECROUTE_ARRAY_OUTIFINDEX(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y].u4OutIfIndex)
#define LDP_FECROUTE_ARRAY_PRELEN(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y].u1PreLen)
#define LDP_FECROUTE_INDEX_AVBLE(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y].u4IsIndexAble)
#define LDP_FECROUTE_ARRAY_ADDRFM(x, y) \
             (gLdpInfo.LdpIncarn[(x)].aFecRoute[y].u2AddrFmly)

#define LDP_LABEL_ALLOC_METHOD(x) \
             (gLdpInfo.LdpIncarn[(x)].u1LabelAllocType)
#define LDP_INCARN_STATUS(x) \
             (gLdpInfo.LdpIncarn[(x)].u1RowStatus)
#define LDP_INCARN_ADMIN_STATUS(x) \
             (gLdpInfo.LdpIncarn[(x)].u2AdminStatus)
#define LDP_INCARN_PPEER_ARR(x,y) \
             (gLdpInfo.LdpIncarn[(x)].apPeerEntry[(y)])
#define LDP_UDP_SOCKFD(x) \
             (gLdpInfo.LdpIncarn[(x)].i4UdpSockDesc)
/* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
#define LDP_IPV6_UDP_SOCKFD(x) \
             (gLdpInfo.LdpIncarn[(x)].i4Ipv6UdpSockDesc)
#define LDP_V6LINK_HELLO_HLIMIT        255
#endif
/* MPLS_IPv6 mod end*/
#define LDP_GR_CAPABILITY(x) \
             (gLdpInfo.LdpIncarn[(x)].u1GrCapability)
#define LDP_GR_FWD_HOLDING_TIME(x) \
             (gLdpInfo.LdpIncarn[(x)].u2GrFwdHoldingTime)
#define LDP_GR_NBR_LIVENESS_TIME(x) \
             (gLdpInfo.LdpIncarn[(x)].u2NbrLivenessTime)
#define LDP_GR_MAX_RECOVERY_TIME(x) \
             (gLdpInfo.LdpIncarn[(x)].u2MaxRecoveryTime)
#define LDP_GR_PROGRESS_STATUS(x) \
             (gLdpInfo.LdpIncarn[(x)].u1GrProgressStatus)
#define LDP_REGISTRATION_TABLE(x, y) \
             (gaLdpRegTable[x][y])

#define LDP_INIT_INCARN(u2Incarn) \
        *(UINT4 *)(VOID *) &LDP_LSR_ID (u2Incarn) = LDP_DEF_LSR_ID;\
        LDP_INCARN_ID (u2Incarn) = u2Incarn;\
        LDP_LOOP_DETECT (u2Incarn) = LDP_LOOP_DETECT_NOT_CAPABLE;\
        LDP_LBL_RET_MODE (u2Incarn) = LDP_DEF_LBL_RET_MODE;\
        LDP_MAX_PATH_VECT_LIM (u2Incarn) = LDP_DEF_PATH_VECT_LIM;\
        LDP_MAX_HOP_CNT_LIM (u2Incarn) = LDP_DEF_HOP_CNT_LIM;\
        LDP_MD5_SIG_OPT (u2Incarn) = LDP_MD5_ENABLED;\
        LDP_LABEL_ALLOC_METHOD(u2Incarn) = LDP_ORDERED_MODE;\
        LDP_MAX_ATM_VC_MRG_COUNT(u2Incarn) = LDP_MAX_VC_MRG_COUNT; \
        LDP_MAX_ATM_VP_MRG_COUNT(u2Incarn) = LDP_MAX_VP_MRG_COUNT; \
        LDP_INCARN_STATUS (u2Incarn) = NOT_READY;\
        LDP_INCARN_ADMIN_STATUS (u2Incarn) = LDP_ADMIN_DOWN;\
        LDP_FORCE_OPTION (u2Incarn) = LDP_SNMP_FALSE;\
        TMO_SLL_Init (&LDP_ENTITY_LIST (u2Incarn));\
        TMO_SLL_Init (&LDP_TNL_INCARN_LIST (u2Incarn));\
        LDP_PEER_IFADR_TABLE (u2Incarn) = NULL; \
        LDP_CRLSP_TNL_TABLE (u2Incarn) = NULL; \
        LDP_TCPCONN_HSH_TABLE (u2Incarn) = NULL; \
        /*  TRAP support by default enabled */\
        gLdpInfo.LdpIncarn[u2Incarn].u2SessionTrapEnb = LDP_TRUE;\
        gLdpInfo.LdpIncarn[u2Incarn].u2CrlspTnlTrapEnb = LDP_TRUE;\
        LDP_GR_CAPABILITY(u2Incarn) = LDP_GR_CAPABILITY_NONE;\
  /* LDP_GR_PROGRESS_STATUS(u2Incarn) = LDP_GR_NOT_STARTED;*/\
        LDP_GR_FWD_HOLDING_TIME(u2Incarn) = LDP_DEF_FWD_HOLDING_TIME;\
        LDP_GR_NBR_LIVENESS_TIME(u2Incarn) = LDP_DEF_NBR_LIVENESS_TIME;\
        LDP_GR_MAX_RECOVERY_TIME(u2Incarn) = LDP_DEF_MAX_RECOVERY_TIME;\
        for (u4IfaceCount = 0; u4IfaceCount < MAX_LDP_IFACES;\
             u4IfaceCount++)\
        {\
            for (u2HoldCount = 0; u2HoldCount < HOLD_PRIO_RANGE; u2HoldCount++)\
            {\
                TMO_SLL_Init (CRLSP_TNL_HOLD_PRIO_LIST (u2Incarn,\
                                                     u4IfaceCount,\
                                                        u2HoldCount));\
            }\
           LDP_SET_NO_OF_ENT_MCASTCOUNT(u2Incarn, u4IfaceCount)=0;\
    LDP_SET_NO_OF_ENT_V6MCASTCOUNT(u2Incarn, u4IfaceCount)=0;\
        }\
        TmrStopTimer (LDP_TIMER_LIST_ID,\
                      (tTmrAppTimer *) & (gLdpInfo.LdpIncarn[(u2Incarn)].FwdHoldingTmr.AppTimer));\
        TmrStopTimer (LDP_TIMER_LIST_ID,\
                     (tTmrAppTimer *) & (gLdpInfo.LdpIncarn[(u2Incarn)].FecRouteTimer.AppTimer))


/* TE related macros */ 
#define LDP_TNL_INCARN_LIST(x) \
             (gLdpInfo.LdpIncarn[(x)].TnlIncarnList)
#define LDP_TNL_INCARN_TNL_INDEX(pTnlIncarnNode) \
             (pTnlIncarnNode->u4TnlIndex) 
#define LDP_TNL_INCARN_ROW_STATUS(pTnlIncarnNode) \
             (pTnlIncarnNode->u2RowStatus)
#define LDP_TNL_INCARN_STORAGE_TYPE(pTnlIncarnNode) \
             (pTnlIncarnNode->u2StorageType)
             
             
/* Ldp Entity's SNMP configurable objects related macros */
#define LDP_ENTITY_ROW_STATUS(pLdpEntity) \
             ((pLdpEntity)->u1RowStatus)
#define LDP_ENTITY_ADMIN_STATUS(pLdpEntity) \
             ((pLdpEntity)->u1AdminStatus)
#define LDP_LBL_SPACE_TYPE(pLdpEntity) \
            ((pLdpEntity)->u1LabelSpaceType)
#define LDP_ENTITY_IS_TARGET_TYPE(pLdpEntity) \
            ((pLdpEntity)->u1TargetFlag)
#define LDP_ENTITY_TARGET_ADDR_TYPE(pLdpEntity) \
            ((pLdpEntity)->NetAddrType)
#define LDP_ENTITY_LDP_OVER_RSVP_FLAG(pLdpEntity) \
            ((pLdpEntity)->u1LdpOverRsvpFlag)
#define LDP_ENTITY_DEF_VPI(pLdpEntity) \
           ((pLdpEntity)->pLdpAtmParams->u2EntityConfDefVpi)
#define LDP_ENTITY_DEF_VCI(pLdpEntity) \
           ((pLdpEntity)->pLdpAtmParams->u2EntityConfDefVci)
#define LDP_ENTITY_UNLABL_TRAF_VPI(pLdpEntity) \
           ((pLdpEntity)->pLdpAtmParams->u2UnLblTrafVpi)
#define LDP_ENTITY_UNLABL_TRAF_VCI(pLdpEntity) \
              ((pLdpEntity)->pLdpAtmParams->u2UnLblTrafVci)
#define LDP_ENTITY_MRG_TYPE(pLdpEntity) \
              ((pLdpEntity)->pLdpAtmParams->u1MergeType)
#define LDP_ENTITY_VC_DIRECTION(pLdpEntity) \
              ((pLdpEntity)->pLdpAtmParams->u1VcDirection)
#define LDP_ENTITY_ETH_LBL_RNGE_LIST(pLdpEntity) \
              &((pLdpEntity)->LdpEthParamsList)
#define LDP_ENTITY_ATM_LBL_RANGE_LIST(pLdpEntity) \
              &((pLdpEntity)->pLdpAtmParams->AtmLabelRangeList)
#define LDP_ETH_MIN_LBL(pLdpEntity) \
              ((pLdpEntity)->LdpEthParamsList.u4MinLabelVal)
#define LDP_ETH_MAX_LBL(pLdpEntity) \
              ((pLdpEntity)->LdpEthParamsList.u4MaxLabelVal)
#define LDP_ENTITY_TYPE(pLdpEntity) \
              ((pLdpEntity)->u1EntityType)
#define LDP_DISCOV_PORT(pLdpEntity) \
              ((pLdpEntity)->u2DiscoveryPort)
#define LDP_ENTITY_MTU(pLdpEntity) \
              ((pLdpEntity)->u4EntityConfMtu)
#define LDP_ENTITY_KEEP_ALIVE_TIME(pLdpEntity) \
              ((pLdpEntity)->u4KeepAliveHoldTime)
#define LDP_SSN_THRESHOLD(pLdpEntity) \
              ((pLdpEntity)->u4FailInitSessThreshold)
#define LDP_LBL_DISTR_TYPE(pLdpEntity) \
              ((pLdpEntity)->u1LabelDistrType)
#define LDP_ENTITY_INCARNID(pLdpEntity) \
              ((pLdpEntity)->u2IncarnId)
#define LDP_LBL_ALLOC_METHOD(pLdpEntity) \
               ((pLdpEntity)->u1LabelAllocType)
#define LDP_ENTITY_HELLO_HOLD_TIME(pLdpEntity) \
               ((pLdpEntity)->u2HelloHoldTime)
#define LDP_ENTITY_PROT_VERSION(pLdpEntity) \
                ((pLdpEntity)->u2ProtVersion)
#define LDP_ENTITY_HC_LIMIT(pLdpEntity) \
              ((pLdpEntity)->u2HopVecLimit)
#define LDP_ENTITY_PV_LIMIT(pLdpEntity) \
              ((pLdpEntity)->u1PathVecLimit)
#define LDP_ENTITY_CONF_CHG_STATUS(pLdpEntity) \
              ((pLdpEntity)->u1ConfChgFlag)
#define LDP_ENTITY_PHP_CONF(pLdpEntity) \
              ((pLdpEntity)->i1PhpConf)
#define LDP_ENTITY_TRANSPORT_ADDRKIND(pLdpEntity) \
              ((pLdpEntity)->u1TransportAddrKind)
#define LDP_ENTITY_IPV6_TRANSPORT_ADDRKIND(pLdpEntity) \
              ((pLdpEntity)->u1Ipv6TransportAddrKind)
#define LDP_ENTITY_TRANSPORT_ADDRESS(pLdpEntity) \
              ((pLdpEntity)->u4TransportAddress)
#define LDP_ENTITY_TRANSPORT_ADDRESS_TLV(pLdpEntity) \
              ((pLdpEntity)->u1TransAddrTlvEnable)

#define LDP_ENTITY_TRANSPORT_ADDRESS_IPV6_TLV(pLdpEntity) \
              ((pLdpEntity)->u1Ipv6TransAddrTlvEnable)

#define  INC_LDP_ENTY_MSG_ID(pLdpEntity)\
           pLdpEntity->u4MessageId += 1

#define IS_LSR_EGRESS_OF_TNL(pCrLCB)  \
         ((LCB_DSSN(pCrLCB) == NULL) ? LDP_TRUE : LDP_FALSE)
#define IS_ERHOP_STRICT(pHop) \
         (((pHop)->u1HopType == STRICT_CRLSP_PATH) ? LDP_TRUE : LDP_FALSE)

   /* Entity If Table related macros */
#define LDP_ENTITY_IF_INDEX(pLdpIfTableEntry) \
                              ((pLdpIfTableEntry)->u4IfIndex)
#define LDP_ENTITY_IF_ROW_STATUS(pLdpIfTableEntry) \
                                   ((pLdpIfTableEntry)->u4RowStatus)
#define LDP_ENTITY_IF_LIST(pLdpEntity) \
                                   &((pLdpEntity)->IfList)
#define LDP_ENTITY_IF_ADDRESS(pLdpIfTableEntry) \
              ((pLdpIfTableEntry)->IfAddr)
/* MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED
#define LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV(pLdpEntity) \
              ((pLdpEntity)->u1Ipv6TransAddrTlvEnable)
#define LDP_ENTITY_IPV6TRANSPORT_ADDRKIND(pLdpEntity) \
              ((pLdpEntity)->u1Ipv6TransportAddrKind)
#define LDP_ENTITY_V6TRANSPORT_ADDRESS(pLdpEntity) \
              ((pLdpEntity)->Ipv6TransportAddress.u1_addr)
#define LDP_ENTITY_IPV6_IF_LLADDRESS(pLdpIfTableEntry)\
 ((pLdpIfTableEntry)->LnklocalIpv6Addr)
#endif 
#define LDP_ENTITY_IF_TYPE(pLdpIfTableEntry) \
              ((pLdpIfTableEntry)->u1InterfaceType)
/* MPLS_IPv6 add end*/

/* Macro for getting the Frequency at which the KeepAlive Msgs have to be sent
 * in order to keep the Session Alive */
#define SSN_GET_KALIVE_MSG_FREQ(pSsn)  \
  ((UINT2)(((FLT4)((pSsn)->CmnSsnParams.u2KeepAliveTime))*LDP_KALIVE_MSG_FREQ))

/*  Peer table related macros */
#define LDP_PEER_ID(pLdpPeer) \
                    ((pLdpPeer)->PeerLdpId)
#define LDP_PEER_NET_ADDR(pLdpPeer) \
                    ((pLdpPeer)->NetAddr)
#define LDP_PEER_TRANS_ADDR(pLdpPeer) \
                    ((pLdpPeer)->TransAddr)
#define LDP_PEER_DEF_MTU(pLdpPeer) \
                    ((pLdpPeer)->u4DefMtu)
#define LDP_PEER_KEEP_ALIVE_HOLD_TIMER(pLdpPeer) \
                    ((pLdpPeer)->u4KeepAliveHoldTimer)
#define LDP_PEER_LBL_DIST_METHOD(pLdpPeer) \
                    ((pLdpPeer)->u1LabelDistrType)
#define LDP_PEER_REMOTE_TYPE(pLdpPeer) \
                    ((pLdpPeer)->u1RemoteFlag)
#define LDP_GET_PEER_LIST(pLdpPeer) \
             (&((pLdpPeer)->pLdpEntity->PeerList))
#define LDP_GET_PEER_ADDR_LIST(pLdpPeer) \
             (&((pLdpPeer)->IfAdrList))


/* Presently current incarn is being returned */
#define IFINDEX_GET_INCRN_ID(u4IfIndex, pu2IncarnId)  \
             (*(pu2IncarnId) = LDP_CUR_INCARN)

/* Pool Ids */
#define LDP_ENT_POOL_ID                 LDPMemPoolIds[MAX_LDP_ENTITIES_SIZING_ID]
#define LDP_ATM_LBL_RNG_POOL_ID         LDPMemPoolIds[MAX_LDP_ATM_LBL_RANGE_SIZING_ID]
#define LDP_ETH_LBL_RNG_POOL_ID         LDPMemPoolIds[MAX_LDP_GEN_RANGE_BLKS_SIZING_ID]
#define LDP_EIF_POOL_ID                 LDPMemPoolIds[MAX_LDP_IFACES_SIZING_ID]
#define LDP_PIF_POOL_ID                 LDPMemPoolIds[MAX_LDP_PEER_IF_SIZING_ID]
#define LDP_MSG_POOL_ID                 LDPMemPoolIds[MAX_LDP_MSGS_SIZING_ID]
#define LDP_ADJ_POOL_ID                 LDPMemPoolIds[MAX_LDP_ADJS_SIZING_ID]
#define LDP_PEER_POOL_ID                LDPMemPoolIds[MAX_LDP_PEERS_SIZING_ID]
#define LDP_SSN_POOL_ID                 LDPMemPoolIds[MAX_LDP_SESSIONS_SIZING_ID]

#ifdef MPLS_LDP_BFD_WANTED
#define LDP_BFD_SSN_POOL_ID             LDPMemPoolIds[MAX_LDP_BFD_SESSIONS_SIZING_ID]
#endif

#define LDP_LSP_POOL_ID                 LDPMemPoolIds[MAX_LDP_LSP_LCB_SIZING_ID]
#define LDP_UPSTR_POOL_ID               LDPMemPoolIds[MAX_LDP_LSP_UPSTR_LCB_SIZING_ID]
#define LDP_TRIG_POOL_ID                LDPMemPoolIds[MAX_LDP_LSP_TRIG_LCB_SIZING_ID]
#define LDP_IP_RT_POOL_ID               LDPMemPoolIds[MAX_LDP_IP_RT_ENTRIES_SIZING_ID]
#define LDP_CRLSPTNL_POOL_ID            LDPMemPoolIds[MAX_LDP_CRLSP_TNL_INFO_SIZING_ID]
#define LDP_TNL_INCARN_POOL_ID          LDPMemPoolIds[MAX_LDP_TNL_INCARN_INFO_SIZING_ID]
#define LDP_DS_ELSP_TRFC_POOL_ID        LDPMemPoolIds[MAX_LDP_OA_TRFC_PROFILE_SIZING_ID]
#define LDP_INT_LSPSETUP_POOL_ID        LDPMemPoolIds[MAX_LDP_INT_LSP_SIZING_ID]
#define LDP_MAX_MD5PASSWD_POOL_ID       LDPMemPoolIds[MAX_LDP_PEER_PASSWD_INFO_SIZING_ID]
#define LDP_ATM_PARMS_POOL_ID           LDPMemPoolIds[MAX_LDP_ATM_PARAMS_SIZING_ID]
#define LDP_TEMP_TE_HOP_INFO_POOL_ID    LDPMemPoolIds[MAX_LDP_TEMP_TE_HOP_INFO_SIZING_ID]
#define LDP_Q_POOL_ID                   LDPMemPoolIds[MAX_LDP_QDEPTH_SIZING_ID]
#define LDP_RCV_BUF_POOL_ID             LDPMemPoolIds[MAX_LDP_RECV_BUF_SIZING_ID]
#define LDP_DATA_BUF_POOL_ID            LDPMemPoolIds[MAX_LDP_UDP_RECV_BUF_SIZING_ID]
#define LDP_SOCK_RECV_BUF_POOL_ID       LDPMemPoolIds[MAX_LDP_SOCK_RECV_BUF_SIZING_ID]
#define LDP_UDP_DATA_BUF_POOL_ID        LDPMemPoolIds[MAX_LDP_UDP_SEND_BUF_SIZING_ID]
#define LDP_TCP_SEND_BUF_POOL_ID        LDPMemPoolIds[MAX_LDP_TCP_SEND_BUF_SIZING_ID]
#define LDP_SOCK_INFO_TAB_POOL_ID       LDPMemPoolIds[MAX_LDP_TCP_UDP_SOCK_INFO_SIZING_ID]
#define LDP_STATICFECPTR_POOL_ID        LDPMemPoolIds[MAX_LDP_FEC_TABLE_SIZING_ID]
#define LDP_L2VPN_EVT_INFO_POOL_ID      LDPMemPoolIds[MAX_LDP_L2VPN_PWVC_EVT_INFO_SIZING_ID]
#define LDP_L2VPN_IF_STR_POOL_ID        LDPMemPoolIds[MAX_LDP_L2VPN_IF_STR_INFO_SIZING_ID]
#define LDP_ATM_LBL_KEY_INFO_POOL_ID    LDPMemPoolIds[MAX_LDP_ATM_LBL_KEY_INFO_SIZING_ID]
#define LDP_SOCK_SEND_PEND_BUF_POOL_ID  LDPMemPoolIds[MAX_LDP_TCP_PEND_WRITE_BUF_SIZING_ID]

/* Pw Redundancy */
#define LDP_ICCP_PWVC_REQUESTS_POOL_ID          LDPMemPoolIds[MAX_LDP_ICCP_PWVC_REQUESTS_SIZING_ID]
#define LDP_ICCP_PWVC_DATA_ENTRIES_POOL_ID      LDPMemPoolIds[MAX_LDP_ICCP_PWVC_DATA_ENTRIES_SIZING_ID]

/* LDP entity related */
#define LDP_ENTITY_LIST(x)   (gLdpInfo.LdpIncarn[(x)].LdpEntityList)

#define LDP_STATS_ATMPT_SSNS(pLdpEntity) \
           ((pLdpEntity)->LdpStatsList.u4AttemptSessions)
#define LDP_REMOTE_PEER_LIST(pLdpEntity) \
                        ((pLdpEntity)->RemotePeerList)

/* Peer Related */
#define PEER_ATM_LBL_RANGE_LIST(pPeer) \
            (&((pPeer)->AtmLblRangeList))
/* Session related */
#define SSN_ULCB(x)          ((x)->USLspCtrlBlkList)
#define SSN_DLCB(x)          ((x)->DSLspCtrlBlkList)
#define SSN_STATE(x)         ((x)->u1SessionState)

#define SSN_MRGTYPE(x)       ((x)->u1SessionMergeType)

#define SSN_GET_PEER_TRANSADDR(u4TransAddr, pSessionEntry) \
{\
   MEMCPY((UINT1 *) &u4TransAddr,\
          (pSessionEntry)->pLdpPeer->TransAddr.Addr.au1Ipv4Addr, \
           LDP_IPV4ADR_LEN);\
   u4TransAddr = OSIX_NTOHL (u4TransAddr);\
}\

#define SSN_GET_PEER_IPV6_TRANSADDR(pTransAddr, pSessionEntry) \
{\
   MEMCPY(pTransAddr,\
          (pSessionEntry)->pLdpPeer->TransAddr.Addr.Ip6Addr, \
           LDP_IPV6ADR_LEN);\
}\

           
#define SSN_GET_PEERID(PeerLdpId, pSessionEntry) \
            MEMCPY(PeerLdpId, (pSessionEntry)->pLdpPeer->PeerLdpId, \
                    LDP_MAX_LDPID_LEN)
#define SSN_GET_SSN_STATE(pSessionEntry, u1SsnState) \
            (u1SsnState) = (pSessionEntry)->u1SessionState
#define SSN_GET_HELLO_HTIME(pSessionEntry) \
            ((pSessionEntry)->pLdpPeer->pLdpEntity->u2HelloHoldTime)
#define SSN_GET_LBL_RNGID(pSessionEntry) \
            ((pSessionEntry)->pLdpPeer->pLdpEntity->u2LabelRangeId)
#define SSN_GET_SSN_HTIME(pSessionEntry) \
            ((pSessionEntry)->CmnSsnParams.u2KeepAliveTime)
#define SSN_ADVTYPE(pSessionEntry) \
            ((pSessionEntry)->CmnSsnParams.u1LabelAdvertMode)
#define SSN_GET_INCRN_ID(pSessionEntry) \
              ((pSessionEntry)->pLdpPeer->pLdpEntity->u2IncarnId)
#define SSN_GET_PEER_IFADR_LIST(pSessionEntry) \
              &((pSessionEntry)->pLdpPeer->IfAdrList)
#define SSN_GET_ENTITY_IFLIST(pSessionEntry) \
              &((pSessionEntry)->pLdpPeer->pLdpEntity->IfList)
#define SSN_GET_ADDR_FMLY(pSessionEntry) \
              ((pSessionEntry)->pLdpPeer->pLdpEntity->NetAddrType)
#define SSN_GET_SSN_LIST(pSessionEntry) \
              &((pSessionEntry)->pLdpPeer->pLdpSession)
#define SSN_GET_LOOP_DETECT(pSessionEntry) \
         ((pSessionEntry)->CmnSsnParams.u1LoopDetect)
#define SSN_GET_ENTITY(pSessionEntry) \
         ((pSessionEntry)->pLdpPeer->pLdpEntity)
#define SSN_GET_LBL_ALLOC_MODE(pLdpSession) \
            LDP_LABEL_ALLOC_METHOD(SSN_GET_INCRN_ID(pLdpSession))
#define SSN_GET_MSGID(pSessionEntry) \
         ((pSessionEntry)->pLdpPeer->pLdpEntity->u4MessageId)

#define SSN_GET_LSR_ID(pSessionEntry) \
         (LDP_LSR_ID(SSN_GET_INCRN_ID((pSessionEntry))))
#define SSN_GET_LBL_TYPE(pSessionEntry) \
         ((pSessionEntry)->pLdpPeer->pLdpEntity->u1EntityType)
#define SSN_GET_ENTITY_ATM_PARAMS(pSessionEntry) \
         &((pSessionEntry)->pLdpPeer->pLdpEntity->pLdpAtmParams)
#define SSN_GET_ATM_LBLRNG_LIST(pSessionEntry) \
         &((pSessionEntry)->IntAtmLblRngList)
#define SSN_GET_ADJ_LIST(pSessionEntry) \
          &((pSessionEntry)->AdjacencyList)
#define SSN_GET_PEER_ATM_LBLRNG_LIST(pSessionEntry) \
            &((pSessionEntry)->pLdpPeer->AtmLblRangeList)
#define LDP_SESSION_ID(pLdpSession) \
                    ((pLdpSession)->SessionId)
#define LDP_SESSION_PROT_VER(pLdpSession) \
                    ((pLdpSession)->u2ProtoVersion)
#define LDP_SESSION_HOLD_TIME_REM(pLdpSession) \
                    ((pLdpSession)->u4HoldTimeRem)
#define LDP_SESSION_ROLE(pLdpSession) \
                    ((pLdpSession)->u1SessionRole)
#define LDP_SESSION_STATE(pLdpSession) \
                    ((pLdpSession)->u1SessionState)
#define LDP_SESSION_ATM_LB_VPI(pLdpSession) \
                    ((pLdpSession)->u2LBVpi)
#define LDP_SESSION_ATM_LB_VCI(pLdpSession) \
                    ((pLdpSession)->u2LBVci)
#define LDP_SESSION_ATM_UB_VPI(pLdpSession) \
                    ((pLdpSession)->u2UBVpi)
#define LDP_SESSION_ATM_UB_VCI(pLdpSession) \
                    ((pLdpSession)->u2UBVci)
#define LDP_SESSION_ROW_STATUS(pLdpSession) \
                    ((pLdpSession)->u1RowStatus)
#define LDP_SESSION_UNKNOWN_MSGTYPE_ERR(pLdpSession) \
                    ((pLdpSession)->u4StatsUnknownMsgTypeErrors)
#define LDP_SESSION_UNKNOWN_TLV_ERR(pLdpSession) \
                    ((pLdpSession)->u4StatsUnknownTlvErrors)

/* Macros for updating the Session Statistics */

#define LDP_SESSION_UPDATE_UNKNOWN_MSGTYPE_ERR(pLdpSession) \
                    ++((pLdpSession)->u4StatsUnknownMsgTypeErrors)
#define LDP_SESSION_UPDATE_UNKNOWN_TLV_ERR(pLdpSession) \
                    ++((pLdpSession)->u4StatsUnknownTlvErrors)

/* Adjacency  related macros */
#define LDP_ADJ_HOLD_TIME_REM(pLdpAdjacency) \
                    ((pLdpAdjacency)->u4HoldTimeRem)
#define ADJ_GET_ADJ_LIST(pAdjEntry) \
         &((pAdjEntry)->pSession->AdjacencyList)
#define ADJ_GET_PEER_TYPE(pAdjEntry) \
          ((pAdjEntry)->pSession->pLdpPeer->u1RemoteFlag)

/* Macro for computing the back off time */
#define LDP_BACK_OFF_TIME(u1BTime) \
          ((u1BTime) + 30)

/* Msg Related */
#define MSG_GET_FIRST_TLVTYPE(pMsg) \
            (OSIX_NTOHS(*(UINT2*)(VOID *)((pMsg) + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)))

#define  LDP_GET_FEC_TLV_LEN(pMsg, u2Len)           \
      LDP_GET_2_BYTES((pMsg + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN +  \
                       LDP_TLV_TYPE_LEN), u2Len);     \

#define LDP_EXT_LBL_TYPE(pMsg, u2LblType) \
{  \
   UINT2 u2FecLen ; \
   LDP_GET_FEC_TLV_LEN(pMsg, u2FecLen) ;  \
   pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN + LDP_TLV_HDR_LEN + u2FecLen); \
   LDP_GET_2_BYTES(pMsg, u2LblType) ; \
   pMsg += LDP_TLV_HDR_LEN ; \
}  \

/* PDU Related */
#define PDU_GET_FIRST_TLVTYPE(pPDU) \
         (OSIX_NTOHS(*(UINT2*)((pPDU) + LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +  \
                              LDP_MSG_ID_LEN)))

#define PDU_GET_PEER_LDPID(pPDU, PeerLdpId) \
       MEMCPY((PeerLdpId), ((pPDU) + LDP_PRTCL_VER_LEN + 2), LDP_MAX_LDPID_LEN)

#define LDP_STATICFECPTR(x) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry)
#define LDP_STATICFEC(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)])
#define LDP_STATICFEC_INDEX(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)].u4FecIndex)
#define LDP_STATICFEC_TYPE(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)].u1FecElmntType)
#define LDP_STATICFEC_ADDRLEN(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)].u1PreLen)
#define LDP_STATICFEC_ADDRFMLY(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)].u2AddrFmly)
#define LDP_STATICFEC_ADDR(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)].Ipv4Addr)
#define LDP_STATICFEC_STORTYPE(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)].u1StorageType)
#define LDP_STATICFEC_ROWSTATUS(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)].u1RowStatus)
#define LDP_STATICFEC_LDPLSP_FECINDEX(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)].u4LdpLspFecIndex)
#define LDP_STATICFEC_LDPLSP_ROWSTATUS(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)].u1RowStatus)
#define LDP_STATICFEC_LDPLSP_STORTYPE(x,y) \
        (gLdpInfo.LdpIncarn[(x)].paFecEntry[(y-1)].u1StorageType)

/* LSP Contol Block related */
#define LCB_NXT_ULCB(x)      ((x)->NextUSLspCtrlBlk)
#define LCB_NXT_DLCB(x)      ((x)->NextDSLspCtrlBlk)
#define LCB_UREQ_ID(x)       ((x)->u4UStrLblReqId)
#define LCB_DREQ_ID(x)       ((x)->u4DStrLblReqId)
#define LCB_STATE(x)         ((x)->u1LspState)

#define LCB_ULBL(x)          ((x)->UStrLabel)
#define LCB_DLBL(x)          ((x)->DStrLabel)
#define LCB_FEC(x)           ((x)->Fec)
#define LCB_USSN(x)          ((x)->pUStrSession)
#define LCB_DSSN(x)          ((x)->pDStrSession)
#define LCB_TRIG(x)          ((x)->pTrigCtrlBlk)
#define LCB_TRIG_TYPE(x)     ((x)->pTrigCtrlBlk->u1TrgType)
#define LCB_TRIG_ST(x)       ((x)->pTrigCtrlBlk->u1NhLspState)
#define LCB_NHOP(x)          ((x)->pNHCtrlBlk)
#define LCB_SUB_STATE(x)     ((x)->u1SubState)
#define LCB_HCOUNT(x)        ((x)->u1HopCount)
#define LCB_OUT_IFINDEX(x)   ((x)->u4OutIfIndex)
#define LCB_ROLE(x)          ((x)->u1LcbRole)
#define LCB_TNLINFO(x)       ((x)->pCrlspTnlInfo)
#define LCB_OLSP(x)          ((x)->pOrgLspCtrlBlk)

#define LCB_MLIB_UPD_STATUS(x) ((x)->u1MLibUpdStatus)
#define LCB_NHOP_ST(x)         ((x)->pNHCtrlBlk->u1NhLspState)
#define LCB_COMP_NHOPS(x,y,n) MEMCMP (&x, &y, n)
#define NH_TRGCB_ST(x)        ((x)->u1NhLspState)
#define NH_TRGCB_NLSP(x)      ((x)->pNewNhLspCtrlBlk)
#define NH_TRGCB_OLSP(x)      ((x)->pOrgLspCtrlBlk)
#define NH_TRGCB_ULABEL(x)     ((x)->UStrLabel)
#define NH_TRGCB_ULABEL_REQID(x) ((x)->u4UStrLblReqId)
#define NH_TRGCB_USESSION(x)     ((x)->pUStrSession)
#define NH_TRGCB_DSESSION(x)     ((x)->pDStrSession)


#define LCB_IS_LSR_INGRESS(pLspCtrlBlock) \
            ((LCB_USSN(pLspCtrlBlock) == NULL) ? LDP_TRUE : LDP_FALSE)

#define LDP_GET_ADDRLIST(pMsg, pu1AddrList) \
         (pu1AddrList) = ((pMsg) + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN +  \
                          LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN)
#define LDP_GET_ADDRLIST_LEN(pMsg) \
        (OSIX_NTOHS(*(UINT2*)((pMsg) + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN + \
                    LDP_TLV_TYPE_LEN)) - LDP_ADDR_FMLY_LEN)


/* Cr-Lsp Tunnel Info related */
/* The macros follow the convention
 * #define CRLSP_XXXX(pCrLspTnlInfo) \
 *              CRLSP_TE_XXXX(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
 * CRLSP_TE_TNL_INFO will point to the teTnlInfo structure present in 
 * tCrlspTnlInfo.
 * CRLSP_TE_XXXX maps to the macro present in TE module.
 */

#define CRLSP_TE_TNL_INFO(pCrLspTnlInfo)    ((pCrLspTnlInfo)->pTeTnlInfo)

#define CRLSP_TNL_NAME(pCrLspTnlInfo) \
        CRLSP_TE_TNL_NAME(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TNL_INDEX(pCrLspTnlInfo) \
        CRLSP_TE_TNL_INDEX(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TNL_INSTANCE(pCrLspTnlInfo) \
        CRLSP_TE_TNL_INSTANCE(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TNL_DIR(pCrLspTnlInfo) (pCrLspTnlInfo->u1TnlDirection)

#define CRLSP_SGNL_PRTCL(pCrLspTnlInfo) \
        CRLSP_TE_TNL_SGNL_PRTCL(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_INGRESS_LSRID(pCrLspTnlInfo) \
        CRLSP_TE_TNL_INGRESS_RTR_ID(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_EGRESS_LSRID(pCrLspTnlInfo) \
        CRLSP_TE_TNL_EGRESS_RTR_ID(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_SECONDARY_STATUS(pCrLspTnlInfo) \
        (pCrLspTnlInfo->u2SecondaryStatus)
        
#define CRLSP_LSPID(pCrLspTnlInfo)       ((pCrLspTnlInfo)->u2LocalLspid)

#define CRLSP_SET_PRIO(pCrLspTnlInfo) \
        CRLSP_TE_TNL_SET_PRIO(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_HOLD_PRIO(pCrLspTnlInfo) \
        CRLSP_TE_TNL_HOLD_PRIO(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_IS_ROW_TO_BE_DELETED(pCrLspBlk)\
        ((pCrLspBlk)->u4IsRowToBeDeleted)

#define CRLSP_RTPIN_TYPE(pCrLspTnlInfo) ((pCrLspTnlInfo)->u1RoutePinningType)

#define CRLSP_ERHOP_COUNT(pCrLspTnlInfo) \
        CRLSP_TE_PO_HOP_COUNT(CRLSP_TNL_PATH_INFO(pCrLspTnlInfo))
        
#define CRLSP_TRFPARM_GRPID(pCrLspTnlInfo) \
        ((pCrLspTnlInfo)->u4RsvTrfcParmGrpId)
        
#define CRLSP_ERHOP_LIST(pCrLspTnlInfo) \
        (CRLSP_TE_HOP_LIST(CRLSP_TE_TNL_INFO(pCrLspTnlInfo)))
        
#define CRLSP_RES_INDEX(pCrLspTnlInfo)\
        (CRLSP_TE_TNL_TRFC_PARAM_INDEX(CRLSP_TE_TNL_INFO(pCrLspTnlInfo)))
        
#define CRLSP_HOP_LIST_INDEX(pCrLspTnlInfo) \
        (CRLSP_TE_TNL_HOP_LIST_INDEX(CRLSP_TE_TNL_INFO(pCrLspTnlInfo)))
        
#define CRLSP_HOP_PO_INDEX(pCrLspTnlInfo) \
        (CRLSP_TE_TNL_PATH_IN_USE(CRLSP_TE_TNL_INFO(pCrLspTnlInfo)))
        
#define CRLSP_TNL_ROLE(pCrLspTnlInfo) \
        (CRLSP_TE_TNL_ROLE(CRLSP_TE_TNL_INFO(pCrLspTnlInfo)))

#define CRLSP_ROW_STATUS(pCrLspTnlInfo) \
        (CRLSP_TE_TNL_ROW_STATUS(CRLSP_TE_TNL_INFO(pCrLspTnlInfo)))
        
#define CRLSP_ADMIN_STATUS(pCrLspTnlInfo) \
        (CRLSP_TE_TNL_ADMIN_STATUS(CRLSP_TE_TNL_INFO(pCrLspTnlInfo)))

#define CRLSP_OPER_STATUS(pCrLspTnlInfo) \
        (CRLSP_TE_TNL_OPER_STATUS(CRLSP_TE_TNL_INFO(pCrLspTnlInfo)))

#define CRLSP_STACK_TUNN(pCrLspTnlInfo) \
        ((pCrLspTnlInfo)->u1StackedTunnel)
        
#define CRLSP_IS_EGR_OF_TUNN(pCrLspTnlInfo) \
        ((pCrLspTnlInfo)->u1EgrOfTunnel)
        
#define CRLSP_IS_MOD_OF_TUNN(pCrLspTnlInfo) \
        ((pCrLspTnlInfo)->u1ModOfTunnel)
        
#define CRLSP_TRGT_SSN_FOR_STACK(pCrLspTnlInfo) \
        ((pCrLspTnlInfo)->u1TrgtSessForStckTunn)
        
#define CRLSP_LCB(pCrLspTnlInfo) \
        ((pCrLspTnlInfo)->pLspCtrlBlock)
        
#define CRLSP_INS_POIN(pCrLspTnlInfo) \
        ((pCrLspTnlInfo)->pCrlspTnlInstance)
        
#define CRLSP_PARM_FLAG(pCrLspTnlInfo) \
        ((pCrLspTnlInfo)->u2Flag)
        
#define CRLSP_STACK_LIST(pCrLspTnlInfo) \
        (tTMO_SLL *)&((pCrLspTnlInfo)->StackTnlList)
        
#define CRLSP_STACK_TNL_HEAD(pCrLspTnlInfo) \
        ((pCrLspTnlInfo)->pStackTnlHead)
        
#define CRLSP_TNL_PATH_INFO(pCrLspTnlInfo) \
        CRLSP_TE_TNL_PATH_INFO(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))

#define CRLSP_TNL_HOP_LIST_INFO(pCrLspTnlInfo) \
        CRLSP_TE_TNL_HOP_LIST_INFO(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))

#define CRLSP_TRF_PARMS(pCrLspTnlInfo) \
        CRLSP_TE_TNL_TRFC_PARAM(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TRF_RESINDEX(pCrLspTnlInfo) \
        CRLSP_TE_TNL_TPARAM_INDEX(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TRF_FLAGS(pCrLspTnlInfo) \
        CRLSP_TE_TNL_CTPARAM_FLAGS(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TRF_FREQ(pCrLspTnlInfo) \
        CRLSP_TE_TNL_CTPARAM_FREQ(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TRF_WGT(pCrLspTnlInfo) \
        CRLSP_TE_TNL_CTPARAM_WEIGHT(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TRF_PD(pCrLspTnlInfo) \
        CRLSP_TE_TNL_CTPARAM_PDR(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TRF_PB(pCrLspTnlInfo) \
        CRLSP_TE_TNL_CTPARAM_PBS(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TRF_CD(pCrLspTnlInfo) \
        CRLSP_TE_TNL_CTPARAM_CDR(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TRF_CB(pCrLspTnlInfo) \
        CRLSP_TE_TNL_CTPARAM_CBS(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
#define CRLSP_TRF_EB(pCrLspTnlInfo) \
        CRLSP_TE_TNL_CTPARAM_EBS(CRLSP_TE_TNL_INFO(pCrLspTnlInfo))
        
/* CR-LDP related macros */
/* The macro below gets the length, corresponding to the Tlv Type that is
 * passed */
#define CRLDP_GET_ERHOP_TLV_LEN(Type) \
           ((Type) == CRLDP_ERHOP_IPV4) ? LDP_IPV4_ERHOP_TLV_LEN : \
            (((Type) == CRLDP_ERHOP_IPV6) ? LDP_IPV6_ERHOP_TLV_LEN : \
             (((Type) == CRLDP_ERHOP_ASNUM) ? LDP_ERHOP_AS_TLV_LEN : \
              (((Type) == CRLDP_ERHOP_LSPID) ? LDP_ERHOP_LSPID_TLV_LEN : 0)))

#define OFFSET_SLL_TO_LCB (VAR_OFFSET(tLspCtrlBlock, NextDSLspCtrlBlk))
#define OFFSET_SLL_TO_USLCB (VAR_OFFSET(tLspCtrlBlock, NextDSLspCtrlBlk))
#define LDP_OFFSET(pIfAddrNode) \
         ((tPeerIfAdrNode*)(VOID *)((UINT1*)(pIfAddrNode) - (VAR_OFFSET(tPeerIfAdrNode, NextSllNode))))

#ifdef MPLS_IPV6_WANTED
#define LDP_IPV6_OFFSET(pIfAddrNode) \
 ((tPeerIfAdrNode*)(VOID *)((UINT1*)(pIfAddrNode) - (VAR_OFFSET(tPeerIfAdrNode, NextIpv6HashNode))))
#endif

#define LDP_SSN_OFFSET_HTBL(hashNode) \
         ((tLdpSession*)(VOID *)((UINT1*)hashNode - (VAR_OFFSET(tLdpSession, NextHashNode))))

#define SLL_TO_LCB(x) ((x == NULL) ? NULL : ((tLspCtrlBlock *)(VOID *)(((UINT1 *)x) - OFFSET_SLL_TO_LCB)))
#define LCB_TO_SLL(x)     ((tTMO_SLL_NODE *)(VOID *)(((UINT1 *)x) + OFFSET_SLL_TO_LCB))

#define LDP_OFFSET_TO_REQID(pMsg, pu1TmpMsg) \
   {   \
      /* Offset to Fec Tlv Len */     \
      pu1TmpMsg = (pMsg + LDP_MSG_HDR_LEN + LDP_MSGID_LEN + LDP_TLV_TYPE_LEN) ; \
      /* Offset by FecTlvLen */  \
      pu1TmpMsg += (LDP_UINT2_LEN + OSIX_NTOHS(*(UINT2*)(VOID *)(pu1TmpMsg)));  \
   }

/* Call back functions */
#define LDP_SNMP_CLBK_FUNC   LdpSNMPEventHandler
#define LDP_TCP_CLBK_FUNC    LdpTcpEventHandler
#define LDP_IP_CLBK_FUNC     ldpIpEventHandler 

#define LDP_GET_2_BYTES(x,y)\
 {\
    UINT2 u2TmpVal;\
    MEMCPY(&u2TmpVal,x,sizeof(UINT2));\
    y = OSIX_NTOHS(u2TmpVal);\
 }
#define LDP_GET_4_BYTES(x,y)\
 {\
    UINT4 u4TmpVal;\
    MEMCPY(&u4TmpVal,x,sizeof(UINT4));\
    y = OSIX_NTOHL(u4TmpVal);\
 }
#define LDP_EXT_1_BYTES(x,y) (y)=(*(UINT1 *)(x)); (x)+=1;

#define LDP_EXT_2_BYTES(x,y)\
{\
    UINT2 u2TmpVal;\
    MEMCPY(&u2TmpVal,x,sizeof(UINT2));\
    y = OSIX_NTOHS(u2TmpVal);\
    (x)+= 2;\
}
#define LDP_EXT_4_BYTES(x,y)\
{\
    UINT4 u4TmpVal;\
    MEMCPY(&u4TmpVal,x,sizeof(UINT4));\
    y = OSIX_NTOHL(u4TmpVal);\
    (x)+= 4;\
}

/* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
#define LDP_EXT_16_BYTES(x,y)\
{\
 MEMCPY(y, x, LDP_NET_ADDR6_LEN);\
 (x) += 16;\
}
#define LDP_IPV6_ADDR(Genaddr)     Genaddr.Ip6Addr.u1_addr
#endif
#define LDP_SET_NO_OF_ENT_V6MCASTCOUNT(x,y) \
       (((gLdpInfo.LdpIncarn[x].aIfaceEntry[y])).u4NoofEntyV6McastsCount)
#define LDP_IPV4_ADDR(Genaddr)     Genaddr.au1Ipv4Addr
/* MPLS_IPv6 mod end*/

#define LDP_SET_2_BYTES(x,y)\
{\
    UINT2 u2TmpVal;\
    u2TmpVal = OSIX_HTONS(y);\
    MEMCPY(x, &u2TmpVal, sizeof(UINT2))\
}
#define LDP_SET_4_BYTES(x,y)\
{\
    UINT4 u4TmpVal;\
    u4TmpVal = OSIX_HTONL(y);\
    MEMCPY(x, &u4TmpVal, sizeof(UINT4))\
}

#define LDP_SET_PWVC_C_BIT(u2PwVcTypeAndCBit, i1CBit)\
        u2PwVcTypeAndCBit = ((i1CBit) == LDP_ZERO) ?\
        (u2PwVcTypeAndCBit & LDP_PWVC_RESET_C_BIT_MASK) : \
        (u2PwVcTypeAndCBit | LDP_PWVC_SET_C_BIT_MASK)
/* Macro defination for L2Vpn Ssn Event Msg */
   
#define LDP_L2VPN_SSN_EVT_INFO_PEER_ADDR(pSsnEvtInfo) \
      ((pSsnEvtInfo)->u4PeerAddr) 

/* Given Prefix length in bits, gives Prefix length in bytes, padded to the
 * nearest byte boundary */
#define LDP_GET_PADDED_PRFX_LEN(u1PreLen)                     \
             ((u1PreLen) > (((u1PreLen) >> 3) << 3)) ?        \
            (UINT1)(((u1PreLen) >> 3) + 1) : (UINT1)(((u1PreLen) >> 3))

/* Extract Prefix of length(in bits) u1PreLen, padded to byte boundary. */
#define LDP_EXT_FEC_PREFIX(pu1Fec, u4Prefix, u1PreLen)               \
{  UINT1 u1Len = (UINT1)LDP_GET_PADDED_PRFX_LEN(u1PreLen) ;                \
   LDP_GET_4_BYTES(pu1Fec, u4Prefix);                                \
   pu1Fec += u1Len ;                                                 \
   u4Prefix &= ( LDP_MAX_NET_NUMBER << ((IPV4_ADDR_LENGTH - u1Len) << 3)); \
}

#define LDP_EXT_IPV6_FEC_PREFIX(pu1Fec,pIpv6Fec,u1PreLen) \
{ UINT1 u1Len = (UINT1)LDP_GET_PADDED_PRFX_LEN(u1PreLen); \
  MEMCPY(pIpv6Fec,pu1Fec,u1Len);\
  pu1Fec += u1Len ;\
}

/* For setting M, N and D bits in Atm Session Paramters Tlv */
#define LDP_SET_M(u4MNDResv, u1M) \
            ((u4MNDResv) |= ((UINT4)(u1M) << 30))
#define LDP_SET_N(u4MNDResv, u1N) \
            ((u4MNDResv) |= ((UINT4)(u1N) << 26))
#define LDP_SET_D(u4MNDResv, u1D) \
            ((u4MNDResv) |= ((UINT4)(u1D) << 25))
#define LDP_GET_N(u4MNDResv, u1N) \
            ((u1N) = (UINT1)(((u4MNDResv) & (0x3c000000)) >> 26))
#define LDP_GET_M(u4MNDResv,u1M) \
            ((u1M) = (UINT1)(((u4MNDResv) & (0xc0000000)) >> 30))
#define LDP_GET_D(u4MNDResv,u1D) \
            ((u1D) = (UINT1)(((u4MNDResv) & (0x02000000)) >> 25))

#define LDP_GET_A_BIT(a, b)    (b) = (UINT1)((a) >> 7)
#define LDP_GET_D_BIT(a, b)    (b) = (UINT1)(((a) >> 6) & (0x01))

/* LdpEntity Statistics Updation Macros */
#define IS_REMOTE_ADJ(pLdpAdj) \
           ((((pLdpAdj)->pPeer->u1RemoteFlag) == LDP_REMOTE_PEER) ? \
        LDP_TRUE : LDP_FALSE )

#define LDP_UPDATE_HELLOS_SENT(pLdpEntity, u1Hello) \
         if((u1Hello) == LDP_TRG_HELLOS) {   \
             ++((pLdpEntity)->LdpStatsList.u4THelloPktsSent);  \
         }    \
         else {  \
             ++((pLdpEntity)->LdpStatsList.u4BHelloPktsSent);  \
         }

#define LDP_UPDATE_HELLOS_RCVD(pLdpEntity, u1Hello) \
         if((u1Hello) == LDP_TRG_HELLOS) {   \
             ++((pLdpEntity)->LdpStatsList.u4THelloPktsRcvd); \
         }  \
         else { \
             ++((pLdpEntity)->LdpStatsList.u4BHelloPktsRcvd);  \
         }

/* Macros for retrieving the Entity Statistics */

#define LDP_ENTITY_EST_SESSION(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4EstbSessions)

#define LDP_ENTITY_ATTMPT_SESSION(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4AttemptSessions)

#define LDP_ENTITY_BAD_LDPID_ERR(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4BadLdpIdErr)

#define LDP_ENTITY_BAD_PDU_LEN_ERR(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4BadPduLenErr)

#define LDP_ENTITY_BAD_MSG_LEN_ERR(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4BadMsgLenErr)

#define LDP_ENTITY_BAD_TLV_LEN_ERR(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4BadTlvLenErr)

#define LDP_ENTITY_SESSREJ_NO_HELLO_ERR(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4SessionRejNoHelloErr)

#define LDP_ENTITY_SESSREJ_ADVRT_ERR(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4SessionRejAdvertErr)

#define LDP_ENTITY_SESSREJ_MAXPDU_ERR(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4SessionRejMaxPduErr)

#define LDP_ENTITY_SESSREJ_LBLRANGE_ERR(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4SessionRejLblRangeErr)

#define LDP_ENTITY_SHUTDOWN_NOTIF_SENT(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4LdpShutDownNotifSent)

#define LDP_ENTITY_MALFORMED_TLV_ERR(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4LdpMalFormedTlvErr)

#define LDP_ENTITY_KEEPALIVE_TIMER_EXP_ERR(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4KeepAliveTimerExpErr)

#define LDP_ENTITY_SHUTDOWN_NOTIF_RECVD(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4LdpShutDownNotifRec)

#define LDP_ENTITY_ESTB_LOCAL_PEERS(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4LdpEstbLocalPeers)

#define LDP_ENTITY_ESTB_REMOTE_PEERS(pLdpEntity) \
   (pLdpEntity->LdpStatsList.u4LdpEstbRemotePeers)


/* Macros for updating the Entity Statistics */
#define LDP_ENTITY_UPDATE_EST_SESSION(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4EstbSessions)

#define LDP_ENTITY_UPDATE_ATTMPT_SESSION(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4AttemptSessions)

#define LDP_ENTITY_UPDATE_BAD_LDPID_ERR(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4BadLdpIdErr)

#define LDP_ENTITY_UPDATE_BAD_PDU_LEN_ERR(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4BadPduLenErr)

#define LDP_ENTITY_UPDATE_BAD_MSG_LEN_ERR(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4BadMsgLenErr)

#define LDP_ENTITY_UPDATE_BAD_TLV_LEN_ERR(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4BadTlvLenErr)

#define LDP_ENTITY_UPDATE_SESSREJ_NO_HELLO_ERR(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4SessionRejNoHelloErr)

#define LDP_ENTITY_UPDATE_SESSREJ_ADVRT_ERR(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4SessionRejAdvertErr)

#define LDP_ENTITY_UPDATE_SESSREJ_MAXPDU_ERR(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4SessionRejMaxPduErr)

#define LDP_ENTITY_UPDATE_SESSREJ_LBLRANGE_ERR(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4SessionRejLblRangeErr)

#define LDP_ENTITY_UPDATE_SHUTDOWN_NOTIF_SENT(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4LdpShutDownNotifSent)

#define LDP_ENTITY_UPDATE_MALFORMED_TLV_ERR(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4LdpMalFormedTlvErr)

#define LDP_ENTITY_UPDATE_KEEPALIVE_TIMER_EXP_ERR(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4KeepAliveTimerExpErr)

#define LDP_ENTITY_UPDATE_SHUTDOWN_NOTIF_RECVD(pSessionEntry) \
 ++((pSessionEntry)->pLdpPeer->pLdpEntity->LdpStatsList.u4LdpShutDownNotifRec)

#define LDP_GET_IFINDEX_FOR_LCB_FROMDSSN(pLspCtrlBlock) \
   (((tLdpAdjacency *)TMO_SLL_First(SSN_GET_ADJ_LIST(LCB_DSSN(pLspCtrlBlock))))->u4IfIndex)

#define LDP_GET_IFINDEX_FOR_LCB_FROMUSSN(pLspCtrlBlock) \
   (((tLdpAdjacency *)TMO_SLL_First(SSN_GET_ADJ_LIST(LCB_USSN(pLspCtrlBlock))))->u4IfIndex)

#define LDP_GET_IFINDEX_FOR_UPLCB_FROMUSSN(pLspCtrlBlock) \
   (((tLdpAdjacency *)TMO_SLL_First(SSN_GET_ADJ_LIST(UPSTR_USSN(pLspCtrlBlock))))->u4IfIndex)


#define LDP_HC_TLV_PRSNT(u1HcValue) \
         (((u1HcValue) != LDP_INVALID_HOP_COUNT) ? LDP_TRUE : LDP_FALSE)

#define LDP_IS_ADDR_MSG(u2MsgType, pMsg)  \
            (((LDP_GET_2_BYTES(pMsg, u2MsgType)) == \
              LDP_ADDR_MSG) ? LDP_TRUE : LDP_FALSE)

#define LDP_IS_ADR_WDRAW_MSG(u2MsgType, pMsg)  \
            (((LDP_GET_2_BYTES(pMsg, u2MsgType)) == \
              LDP_ADDR_WITHDRAW_MSG) ? LDP_TRUE : LDP_FALSE)

#define LDP_GET_STAT_E_BIT(u4StatusCode, u1FatalFlag) \
            (u1FatalFlag) = ((UINT1) (((u4StatusCode) & LDP_STAT_E_BIT) \
                            >> 31)) ;

#define LDP_GET_STAT_F_BIT(u4StatusCode, u1FwdFlag) \
            (u1FwdFlag) = ((UINT1) (((u4StatusCode) & LDP_STAT_F_BIT) \
                            >> 30)) ;

#define LDP_GET_STAT_U_BIT(u2TlvType, u1UnknownTlvFlag) \
            (u1UnknownTlvFlag) = ((UINT1) (((u2TlvType) & LDP_STAT_U_BIT) \
                            >> 15)) ;

#define LDP_INCREMENT_HC_VALUE(u1HcValue)  \
           ((u1HcValue) == 0) ? 0 : (UINT1)((u1HcValue) + 1)

#define  LDP_IS_TFLAG_SET(u2TRField) \
             (((u2TRField & LDP_TARG_FLAG) == LDP_TARG_FLAG) ? LDP_TRUE \
              : LDP_FALSE)
#define  LDP_GET_TX_REQ_FLAG(u2TRField, u1TxReqFlag)  \
             (u1TxReqFlag) = (((u2TRField) & LDP_TX_REQ_FLAG) ?  \
                              LDP_TX_TRGT_HELLOS : LDP_NO_TX_TRGT_HELLOS);

#define LDP_IS_SSN_PRESENT(pLdpPeer) \
            ((pLdpPeer->pLdpSession) == NULL) ? LDP_FALSE : \
            LDP_TRUE

#define LDP_IS_ENTITY_TYPE_ATM(pSessionEntry) \
            ((((pSessionEntry)->pLdpPeer->pLdpEntity->u1EntityType) ==  \
            LDP_ATM_MODE) ? LDP_TRUE : LDP_FALSE)

#define LDP_GET_ENTITY_ATM_PARAMS(pSessionEntry, pLdpAtmParams) \
            if (((pSessionEntry)->pLdpPeer->pLdpEntity->u1EntityType) ==   \
                LDP_ATM_MODE) {     \
            (pLdpAtmParams) =  \
            ((pSessionEntry)->pLdpPeer->pLdpEntity->pLdpAtmParams); \
            }

#define LDP_GET_PRFX_LEN(u4Mask, u1Len) \
{  \
   UINT4    u4TmpMask = u4Mask;   \
         for (u1Len = 0; ((u1Len < 32) && (u4TmpMask)) ;  \
                         (u4TmpMask  = (u4TmpMask << 1)), ++u1Len); \
}

#define LDP_GET_MASK_FROM_PRFX_LEN(u1Len, u4Mask) \
{ \
        u4Mask = 0xffffffff; \
        u4Mask = (UINT4)(u4Mask << (32 - u1Len)); \
}

#define LDP_GET_HELLO_FREQ(u4HTime) \
{ \
    u4HTime = ((u4HTime * 1000)/3); \
}

#define LDP_GET_ADDR_FMLY(pMsg) \
              OSIX_NTOHS(*(UINT2*)(VOID *)((pMsg) + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN  \
                                 + LDP_TLV_HDR_LEN))

#define LDP_COMPUTE_HASH_INDEX(u4Address) \
             ((((u4Address) >> 24) ^ (((u4Address) & 0xFF0000) >> 16) ^ \
               (((u4Address) & 0xFF00) >> 8) ^ \
               ((u4Address) & 0xFF)) % LDP_HASH_SIZE)

/* Macros defined for handling CRLSP related actions */

#define CRLSP_TNL_HOLD_PRIO_LIST(x,y,z) \
        (tTMO_SLL*)(&(gLdpInfo.LdpIncarn[x].aIfaceEntry[y].aCrlspHoldPrio[z]))

#define LDP_SET_IFACE_SPEED(x,y,z) \
                  (((gLdpInfo.LdpIncarn[x].aIfaceEntry[y])).u4IfSpeed) = z;
#define LDP_SET_NO_OF_ENT_MCASTCOUNT(x,y) \
       (((gLdpInfo.LdpIncarn[x].aIfaceEntry[y])).u4NoofEntyMcastsCount)
#define LDP_GET_IFACE_PD(x,y) \
                  (gLdpInfo.LdpIncarn[x].aIfaceEntry[y].u4IfSpeed)

#define LDP_CRLSP_TNL_TBL(u2IncarnId)\
        (tTMO_HASH_TABLE*)(gLdpInfo.LdpIncarn[u2IncarnId].pCrlspTnlTbl)

#define LDP_COPY_TRFC_PARMS(TrfcParms1,TrfcParms2) \
   TrfcParms1->u4PeakDataRate = TrfcParms2->u4PeakDataRate; \
   TrfcParms1->u4PeakBurstSize = TrfcParms2->u4PeakBurstSize; \
   TrfcParms1->u4CommittedDataRate = TrfcParms2->u4CommittedDataRate; \
   TrfcParms1->u4CommittedBurstSize = TrfcParms2->u4CommittedBurstSize; \
   TrfcParms1->u4ExcessBurstSize = TrfcParms2->u4ExcessBurstSize;

   /* Macros used to validate an IP address */
#define LDP_IS_IP_ADDR_CLASS_D(u4IpAddr) \
     ((u4IpAddr &  0xf0000000) == 0xe0000000)

#define LDP_IS_IP_ADDR_CLASS_E(u4IpAddr) \
     ((u4IpAddr &  0xf8000000) == 0xf0000000)

#define    LDP_IS_IP_ADDRESS_INVALID(u4IpAddr)               \
           ((u4IpAddr < (UINT4) LDP_MIN_NET_NUMBER)    ||    \
           (u4IpAddr > (UINT4) LDP_MAX_NET_NUMBER)     ||    \
           (LDP_IS_IP_ADDR_CLASS_D(u4IpAddr) != FALSE) ||    \
           (LDP_IS_IP_ADDR_CLASS_E(u4IpAddr) != FALSE) ||    \
           (u4IpAddr == LDP_LOOPBACK_NET_ADDRESS))

/* Macro to initialise CRLSP tunnel Information */
#define LDP_INIT_CRLSP_TNL_INFO(pCrlspTnlInfo) \
   TMO_SLL_Init_Node(&(pCrlspTnlInfo->HashLinkNode));\
   TMO_SLL_Init_Node(&(pCrlspTnlInfo->NextHoldPrioTnl));\
   TMO_SLL_Init_Node(&(pCrlspTnlInfo->NextStackNode));\
   TMO_SLL_Init_Node(&(pCrlspTnlInfo->NextPreemptTnl));\
   TMO_SLL_Init(&(pCrlspTnlInfo->StackTnlList));\
   CRLSP_IS_ROW_TO_BE_DELETED(pCrlspTnlInfo) = LDP_TNL_DELETE;\
   CRLSP_RTPIN_TYPE(pCrlspTnlInfo)  = LDP_FALSE;\
   CRLSP_PARM_FLAG(pCrlspTnlInfo) = 0;\
   CRLSP_SECONDARY_STATUS(pCrlspTnlInfo) = LDP_TE_TNL_DELETE;\
   CRLSP_LCB(pCrlspTnlInfo)         = NULL;\
   CRLSP_INS_POIN(pCrlspTnlInfo)        = NULL;\
   CRLSP_TRFPARM_GRPID(pCrlspTnlInfo) = LDP_INVALID_RSRC_GRPID;\
   CRLSP_STACK_TUNN(pCrlspTnlInfo) = LDP_FALSE;\
   CRLSP_IS_EGR_OF_TUNN(pCrlspTnlInfo) = LDP_FALSE;\
   CRLSP_TRGT_SSN_FOR_STACK(pCrlspTnlInfo) = LDP_TRUE;\
   CRLSP_IS_MOD_OF_TUNN(pCrlspTnlInfo) = LDP_FALSE;\
   CRLSP_STACK_TNL_HEAD(pCrlspTnlInfo) = NULL; \
   for (u1MaxPscCount = 0; u1MaxPscCount < LDP_DS_MAX_NO_PSCS;\
             u1MaxPscCount++)\
   {\
          LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW (pCrlspTnlInfo,\
                                     u1MaxPscCount) = NULL;\
   }\

              /* Macros for PreEmption */

/* Pointer to the single link list node */
#define CRLSP_GET_TNL_LIST_HOLD_NODE(pCrlspTnlInfo) \
                  ((tTMO_SLL_NODE *)&((pCrlspTnlInfo)->NextHoldPrioTnl))

#define PM_CRLSP_TNL_OFFSET_HTBL(PrioSllNode) \
           ((tCrlspTnlInfo *)(VOID *)((UINT1*)PrioSllNode - sizeof (tTMO_HASH_NODE)))

#define PM_CRLSP_OFFSET(x,y)   FSAP_OFFSETOF(x,y)
 
/* Macro to suppress warnings */
#define LDP_SUPPRESS_WARNING(x) UNUSED_PARAM(x)

/*MACROS Added for VC Merge */

#define UPSTR_GET_ENTITY(x)     ((x)->pUpstrSession->pLdpPeer->pLdpEntity)
#define UPSTR_DSTR_PTR(x)       ((x)->pDnstrCtrlBlock)
#define UPSTR_STATE(x)          ((x)->u1LspState)
#define UPSTR_DSSN_LIST_NODE(x) ((x)->NextUpstrCtrlBlk)
#define UPSTR_DSTR_LIST_NODE(x) ((x)->NextUpstrNode)
#define UPSTR_USSN(x)           ((x)->pUpstrSession)
#define UPSTR_UREQ_ID(x)        ((x)->u4UpstrLblReqId)
#define UPSTR_TRIG(x)           ((x)->pTrigCtrlBlk)
#define UPSTR_NHOP(x)           ((x)->pNHCtrlBlk)
#define UPSTR_ULBL(x)           ((x)->UStrLabel)
#define UPSTR_LBL_TYPE(x)       ((x)->u1InLblType)
#define UPSTR_FEC(x)            ((x)->Fec)
#define UPSTR_INCOM_IFINDX(x)   ((x)->u4InComIfIndex)

/* Macros to be used for the message structure */
#define LDP_MSG_HC(x)              ((x)->u1HopCount)
#define LDP_MSG_PV_PTR(x)          ((x)->pu1PVTlv)
#define LDP_MSG_HC_PTR(x)          ((x)->pu1HCTlv)
#define LDP_MSG_PV_COUNT(x)        ((x)->u1PVCount)
#define LDP_MSG_FEC(x)             ((x)->Fec)
#define LDP_MSG_LABEL(x)           ((x)->Label)
#define LDP_MSG_PTR(x)             ((x)->pu1Msg)
#define LDP_MSG_CTRL_BLK_PTR(x)    ((x)->pUpstrCtrlBlk)

#define LCB_UPSTR_LIST(x)         ((x)->UpstrCtlBlkList)
#define VAR_OFFSET(x,y)        FSAP_OFFSETOF(x,y) 
#define OFFSET_SLL_TO_UCB (VAR_OFFSET(tUstrLspCtrlBlock, NextUpstrNode))
#define UPCB_TO_SLL(x)    ((tTMO_SLL_NODE *)(VOID *)(((UINT1 *)x) + OFFSET_SLL_TO_UCB))
#define SLL_TO_UPCB(x) ((x == NULL) ? NULL : ((tUstrLspCtrlBlock *)(VOID *)(((UINT1 *)x) - OFFSET_SLL_TO_UCB)))

#define LDP_DU_UP_FSM       gLdpDuUpFsm
#define LDP_DU_DN_FSM       gLdpDuDnFsm

#define LDP_GET_USTR_LSP_CTRL_BLK(pNHCtrlBlock) (pNHCtrlBlock->pUStrLspCtrlBlk)
#define LDP_GET_DSTR_LSP_CTRL_BLK(pNHCtrlBlock) (pNHCtrlBlock->pDStrLspCtrlBlk)
#define NH_USTR_CTRL_BLK(pNHCtrlBlock) (pNHCtrlBlock->pUStrLspCtrlBlock)
#define NH_ADDR(pNHCtrlBlock) (pNHCtrlBlock->nHopInfo.NHAddr)
#define NH_OUT_IFINDEX(pNHCtrlBlock) (pNHCtrlBlock->nHopInfo.u4IfIndex)
#define NH_NEW_UPSTR_PTR(pNHCtrlBlock) (pNHCtrlBlock->pNewUStrLspCtrlBlock)

/* Graceful Restart Related Macros / Enumuerations */
/* Maps to fsMplsLdpGrProgressStatus MIB Object */
enum  {
    LDP_GR_NOT_STARTED = 1,
    LDP_GR_SHUT_DOWN_IN_PROGRESS,
    LDP_GR_RECONNECT_IN_PROGRESS,
    LDP_GR_ABORTED,
    LDP_GR_RECOVERY_IN_PROGRESS,
    LDP_GR_COMPLETED
};

/* Maps to fsMplsLdpPeerGrProgressStatus MIBObject */
enum  {
    LDP_PEER_GR_NOT_SUPPORTED = 0,
    LDP_PEER_GR_NOT_STARTED,
    LDP_PEER_GR_RECONNECT_IN_PROGRESS,
    LDP_PEER_GR_ABORTED,
    LDP_PEER_GR_RECOVERY_IN_PROGRESS,
    LDP_PEER_GR_COMPLETED
};



#define LDP_CONVERT_TO_MILLI_SECONDS(x) x*1000
#define LDP_CONVERT_TO_SECONDS(x) x/1000

#define MAX_COLUMN_LENGTH                     80

#define LDP_IPV6_ADDR(Genaddr)     Genaddr.Ip6Addr.u1_addr
#define LDP_IPV4_ADDR(Genaddr)     Genaddr.au1Ipv4Addr

#define LDP_IPV6_U4_ADDR(Genaddr)     Genaddr.Ip6Addr.u1_addr
#define LDP_IPV4_U4_ADDR(Genaddr)     Genaddr.u4Addr


#define LDP_P_IPV6_ADDR(pGenaddr)     pGenaddr->Ip6Addr.u1_addr
#define LDP_P_IPV4_ADDR(pGenaddr)     pGenaddr->au1Ipv4Addr

#define LDP_P_IPV6_U4_ADDR(pGenaddr)     pGenaddr->Ip6Addr.u1_addr
#define LDP_P_IPV4_U4_ADDR(pGenaddr)     pGenaddr->u4Addr


#define LDP_ADDR_TYPE_IPV4 0x01
#define LDP_ADDR_TYPE_IPV6 0x02





#define LDP_ADDR_TYPE_IPV4 0x01
#define LDP_ADDR_TYPE_IPV6 0x02

#endif /*_LDP_MACS_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpmacs.h                              */
/*---------------------------------------------------------------------------*/
