
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldppw.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants used across sub modules
 *---------------------------------------------------------------------------*/
#ifndef _LDP_PW_DEFS_H
#define _LDP_PW_DEFS_H

#define MPLS_OCTET_TO_INTEGER(pOctet,u4Index) { \
                         UINT1 u1LocalCount=0;u4Index=0;\
                         while(pOctet[u1LocalCount]) {\
                         u4Index = u4Index <<8;\
                         u4Index = u4Index | pOctet[u1LocalCount]; \
                         u1LocalCount++;}}

#define LDP_PWVC_MPLS_TYPE_VCONLY                        4
#endif /*_LDP_PW_DEFS_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldppw.h                              */
/*---------------------------------------------------------------------------*/
