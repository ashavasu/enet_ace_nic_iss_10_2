
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpgbl.h,v 1.12 2014/11/08 11:41:01 siva Exp $
 * 
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpgbl.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains definition of global variables
 *---------------------------------------------------------------------------*/

#ifndef _LDP_GBL_H
#define _LDP_GBL_H
tLdpGlobalInfo      gLdpInfo;
UINT4               gu4TcpStdServConnId;

#ifdef MPLS_IPV6_WANTED 
UINT4               gu4Ipv6TcpStdServConnId;
#endif

#ifdef BSDCOMP_SLI_WANTED                                 
UINT4               gu4LdpUdpSockDesc;
#endif

/*
 * Following array is intialied with of function pointers that are invoked
 * to handle the different Session SEM Events in the corresponding LSP Session
 * SEM state.
 */
tSsnFsmFuncPtr      gLdpSsnFsm[] = {
    NULL,
    LdpSsmNonExistentStateHandler,
    LdpSsmInitializedStateHandler,
    LdpSsmOpenRecvStateHandler,
    LdpSsmOpenSentStateHandler,
    LdpSsmOperationalStateHandler,
    LdpSsmSessionRetryStateHandler
};

/* A data structure containing Status Data
   Each element in this array has to be in turn 'OR'ed with Forward bit, to get
   the complete Status Code as mentioned in the Status Tlv in draft-ldp-05 */

UINT4               au4StatusData[LDP_MAX_STATUS_TYPES] = {
    LDP_STAT_SUCCESS | LDP_ADVISORY_NOTIF,
    LDP_STAT_BAD_LDPID | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_BAD_PROT_VER | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_BAD_PDU_LEN | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_UNKNOWN_MSG_TYPE | LDP_ADVISORY_NOTIF,
    LDP_STAT_BAD_MSG_LEN | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_UNKNOWN_TLV | LDP_ADVISORY_NOTIF,
    LDP_STAT_BAD_TLV_LEN | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_CRPTD_TLV_VAL | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_HOLD_TMR_EXPRD | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_SHUT_DOWN | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_LOOP_DETECTED | LDP_ADVISORY_NOTIF,
    LDP_STAT_UNKNOWN_FEC | LDP_ADVISORY_NOTIF,
    LDP_STAT_NO_ROUTE | LDP_ADVISORY_NOTIF,
    LDP_STAT_NO_LBL_RSRC | LDP_ADVISORY_NOTIF,
    LDP_STAT_LBL_RSRC_AVBL | LDP_ADVISORY_NOTIF,
    LDP_STAT_NO_HELLO | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_ADVRT_MODE_REJECT | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_MAX_PDULEN_REJECT | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_LBL_RANGE_REJECT | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_KEEPALIVE_EXPRD | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_LBL_REQ_ABRTD | LDP_ADVISORY_NOTIF,
    LDP_STAT_MISSING_MSG_PARAM | LDP_ADVISORY_NOTIF,
    LDP_STAT_UNSUPP_ADDR_FMLY | LDP_ADVISORY_NOTIF,
    LDP_STAT_BAD_KEEPALIVE_TMR | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_INTERNAL_ERROR | LDP_FATAL_ERR_NOTIF,
    LDP_STAT_BAD_EROUTE_TLV_ERR | LDP_ADVISORY_NOTIF,
    LDP_STAT_BAD_STRICT_NODE_ERR | LDP_ADVISORY_NOTIF,
    LDP_STAT_BAD_LOOSE_NODE_ERR | LDP_ADVISORY_NOTIF,
    LDP_STAT_BAD_INIT_ER_HOP_ERR | LDP_ADVISORY_NOTIF,
    LDP_STAT_RESOURCE_UNAVAIL | LDP_ADVISORY_NOTIF,
    LDP_STAT_TRAF_PARMS_UNAVL | LDP_ADVISORY_NOTIF,
    LDP_STAT_CRLSP_SETUP_ABORT | LDP_ADVISORY_NOTIF,
    LDP_STAT_CRLSP_MODIFY_NOTSUP | LDP_ADVISORY_NOTIF,
    LDP_STAT_CRLSP_PREEMPTED | LDP_ADVISORY_NOTIF,
    LDP_STAT_TEMPORARY_SHUTDOWN | LDP_ADVISORY_NOTIF
};

/* Global Variables used by ldpcrlsp.c */
UINT4               gLdpIpv4AddrMask[33] = {
    0x00000000, 0x80000000, 0xC0000000, 0xE0000000,
    0xF0000000, 0xF8000000, 0xFC000000, 0xFE000000,
    0xFF000000, 0xFF800000, 0xFFC00000, 0xFFE00000,
    0xFFF00000, 0xFFF80000, 0xFFFC0000, 0xFFFE0000,
    0xFFFF0000, 0xFFFF8000, 0xFFFFC000, 0xFFFFE000,
    0xFFFFF000, 0xFFFFF800, 0xFFFFFC00, 0xFFFFFE00,
    0xFFFFFF00, 0xFFFFFF80, 0xFFFFFFC0, 0xFFFFFFE0,
    0xFFFFFFF0, 0xFFFFFFF8, 0xFFFFFFFC, 0xFFFFFFFE,
    0xFFFFFFFF
};

/* End of Global Variables used by ldpcrlsp.c */


/* Global Variable used for LDP Debugging. */
UINT4               gu4LdpDbg;
UINT4               gu4LdpLvl = DEBUG_DEF_LVL_FLAG;

/* Debug Arrays */
CONST CHR1              *aEventName[LDP_MAX_MAJOR_EVENTS] = {
               "TmrExprEvt",
               "SnmpEvt",
               "UdpEvt",
               "TcpEvt",
               "IPEvt",
               "InternalEvt",
             "ApplEvt",
     "OverRsvpEvt",
     "RMEvt",
     "GrLblRelEvt"
        "TeEvent",
        "Ipv6Evt"
};

CONST CHR1              *au1SsnState[LDP_MAX_SESSION_STATES] = {
               "INVALID",
               "NON_EXISTENT",
               "INITIALIZED",
               "OPEN_RECV",
               "OPEN_SENT",
               "OPERATIONAL",
               "RETRY"
};

CONST CHR1              *au1SsnEvents[LDP_MAX_SESSION_EVENTS] = {
               "INT_INIT_CON",
               "INT_SEND_INIT",
               "INT_DESTROY",
               "RECV_INIT",
               "KALIVE_MSG",
               "OTH_LDP_MSG",
               "TIMEOUT",
               "RECV_NOTIF_MSG",
               "RETRY",
               "ICCP_RG_DATA_MSG"
};

CONST CHR1              *au1LdpNhEvts[LDP_MAX_NON_MRG_NHOP_CHNG_EVENTS] = {
               "NEWNH",
               "RETRY-TMOUT",
               "LSPUP",
               "LSPNAK",
               "LSP-DESTROY"
};

CONST CHR1              *au1LdpNhStates[LDP_MAX_NON_MRG_NHOP_CHNG_STATES] = {
               "IDLE",
               "NEWNH-RETRY",
               "NEWNH-RSPAWT"
};

/* Global Variables used for LDP Dumping of Msgs */
UINT4               gu4LdpDumpType;
UINT4               gu4LdpDumpDir = LDP_DUMP_DIR_INOUT;
UINT1               gu1LdpTeFlag = LDP_FALSE;

UINT4               gu4LdpLspPersist=0;
UINT4               gu4LdpMd5Option=0;                                 
UINT4               au4CrldpTableOffSet[CRLDP_TABLE_DEF_OFFSET] =
    { 1, 3, 6, 1, 4, 1, 2076, 13, 2, 1, 1, 1, 1, 6, 'x' };

/* Applications supported */
tLdpAppRegTbl gaLdpRegTable[LDP_MAX_INCARN][MAX_LDP_APPLICATIONS] = {
 {
       { LdpFecAppInit,
         LdpFecAppDeInit,
         LdpHandleFecL2VpnMsg,
             LdpSendSsnNotification,
             LDP_DEREGISTER,
             LDP_FEC_PWVC_TYPE,
             LDP_APP_L2VPN
       },
       { NULL,
         NULL,
         LdpHandleFecL2VpnMsg,
         NULL,
         LDP_DEREGISTER,
         LDP_FEC_GEN_PWVC_TYPE,
         LDP_APP_L2VPN
       }
 }
};

#endif /*_LDP_GBL_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpgbl.h                               */
/*---------------------------------------------------------------------------*/
