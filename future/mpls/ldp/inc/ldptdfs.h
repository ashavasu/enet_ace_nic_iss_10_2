
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
 * $Id: ldptdfs.h,v 1.37 2016/03/10 13:14:53 siva Exp $
*
*******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldptdfs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the structure type
 *                             definitions declared and used in the LDP Module.
 *---------------------------------------------------------------------------*/
#ifndef _LDPTDFS_H 
#define _LDPTDFS_H

#include "mplsutil.h"

#ifdef MPLS_LDP_BFD_WANTED
#include "bfd.h"
#endif

/* General Definitions */

#define LDP_MAX_FEC_TABLE_SIZE  (sizeof (tFecTableEntry) * MAX_LDP_LSPS)
typedef UINT1       tLdpRouterId[ROUTER_ID_LENGTH];
typedef struct _LdpDiffServGlobal 
{ 
     tElspPhbMapEntry    aPreConfMap[8];
     UINT1               u1IsPreConfValid;
     UINT1               au1Pad[3];
} 
tldpDiffServGlobal; 

typedef uGenAddr    tLsrId;

typedef struct _PWFec
{
    UINT4               u4GroupID;
    UINT4               u4PWID;
    UINT2               u2PwType;
    UINT1               u1CBit;
    UINT1               u1VcInfoLen;
}
tPWFec;


typedef struct _PWIfParam
{
    UINT2               u2Pad;
    UINT1               u1ParamID;
    UINT1               u1Length;
}
tPWIfParam;

typedef struct _VccvParamTlv /* Carried in Interface parameter subTLV */
{
    UINT1               u1ParamID;  /* TLV Type (VCCV SubTlv: 0x0C) */
    UINT1               u1Length;   /* Length - 4bytes */ 
    UINT1               u1CCTypes;  /* Value: Control Channel Type bitmask */
    UINT1               u1CVTypes;  /* Value: Connectivity Verification Type bitmask */
}
tVccvParamTlv;


/* Structure to hold information associated with a CRLSP Tunnel. */
typedef struct _CrlspTnlInfo
{
    tTMO_HASH_NODE       HashLinkNode;
    tTMO_SLL_NODE        NextHoldPrioTnl;
    tTMO_SLL_NODE        NextPreemptTnl;
    tTMO_SLL_NODE        NextStackNode;
    tTMO_SLL             StackTnlList;
    tTeTnlInfo           *pTeTnlInfo;
    struct _LspCtrlBlock *pLspCtrlBlock;
    struct _CrlspTnlInfo *pCrlspTnlInstance;
    struct _PerOATrfcProfileEntry *pPerOATrfcProfilePointerArray[LDP_DS_MAX_NO_PSCS];
    UINT4                *pStackTnlHead;
    UINT4                u4IsRowToBeDeleted;
    UINT4                u4RsvTrfcParmGrpId;
    UINT4                u4RsvClassTypeTrfcParamGrpId;
    UINT2                u2Flag;
    UINT2                u2LocalLspid;
    UINT2                u2SecondaryStatus;
    UINT1                u1RoutePinningType;
    UINT1                u1StackedTunnel;
    UINT1                u1EgrOfTunnel;
    UINT1                u1TrgtSessForStckTunn;
    UINT1                u1ModOfTunnel;
    UINT1                au1Pad;
}
tCrlspTnlInfo;

typedef struct _LdpCfgParams
{
    UINT4               u4MaxLdpEntities;
    UINT2               u2MaxLocalPeers;
    UINT2               u2MaxRemotePeers;
    UINT2               u2MaxSessions;
    UINT2               u2MaxIfaces;
    UINT2               u2MaxLsps;
    UINT2               u2MaxCrlspTnls;
    UINT2               u2MaxAdj;
    UINT2               u2MaxAtmVcMergeCount;
    UINT2               u2MaxAtmVpMergeCount;
    UINT1               u1PathVectorLimit;
    UINT1               u1HopCountLimit;
    UINT1               u1Md5SigOption;
    UINT1               u1MaxTempHopInfo;
    UINT2               u2Pad;
}
tLdpCfgParams;

typedef struct _LdpTimer
{
    tTmrAppTimer        AppTimer;
    UINT4               u4Event;
    UINT1               *pu1EventInfo;
}
tLdpTimer;

typedef struct _AtmLdpLabelRange
{
    tTMO_SLL_NODE         NextEntry;
    UINT2               u2LBVpi;
    UINT2               u2LBVci;
    UINT2               u2UBVpi;
    UINT2               u2UBVci;
    UINT1               u1StorageType;
    UINT1               u1RowStatus;
    UINT2               u2Pad;
}
tAtmLdpLblRngEntry;

typedef struct _LdpAtmParams
{
    tTMO_SLL            AtmLabelRangeList;
    UINT4               u4IfIndex;
    UINT2               u2EntityConfDefVpi;
    UINT2               u2EntityConfDefVci;
    UINT2               u2UnLblTrafVpi;
    UINT2               u2UnLblTrafVci;
    UINT2               u2Connectivity;
    UINT2               u2NumOfLblRnges;
    UINT1               u1MergeType;
    UINT1               u1VcDirection;
    UINT1               u1StorageType;
    UINT1               u1RowStatus;
}
tLdpAtmParams;

typedef struct _LdpEthParams
{
    tTMO_SLL_NODE       NextRangeEntry;
    UINT4               u4MinLabelVal;
    UINT4               u4MaxLabelVal;
    UINT4               u4IfIndex; /* underlying L3 VLAN */
    UINT4               u4MplsIfIndex; /* MPLS (type 166) */
    UINT2               u2EthLblRangeId;
    UINT1               u1StorageType;
    UINT1               u1RowStatus;
    UINT1               u1LabelSpace;
    UINT1               au1Pad[3];
}
tLdpEthParams;

typedef struct _LdpEntityStats
{
    UINT4               u4EstbSessions;
    UINT4               u4AttemptSessions;
    UINT4               u4BHelloPktsSent;
    UINT4               u4BHelloPktsRcvd;
    UINT4               u4THelloPktsSent;
    UINT4               u4THelloPktsRcvd;
    UINT4               u4SessionRejNoHelloErr;
    UINT4               u4SessionRejAdvertErr;
    UINT4               u4SessionRejMaxPduErr;
    UINT4               u4SessionRejLblRangeErr;
    UINT4               u4BadLdpIdErr;
    UINT4               u4BadPduLenErr;
    UINT4               u4BadMsgLenErr;
    UINT4               u4BadTlvLenErr;
    UINT4               u4LdpMalFormedTlvErr;
    UINT4               u4KeepAliveTimerExpErr;
    UINT4               u4LdpShutDownNotifRec;
    UINT4               u4LdpShutDownNotifSent;
    UINT4               u4LdpEstbLocalPeers;
    UINT4               u4LdpEstbRemotePeers;
}
tLdpEntityStats;

typedef struct _LdpEntityPeerPasswdInfo
{
    UINT1               aMd5Password[MAX_MD5_KEY_SIZE+1];
    UINT1               u1StorageType;
    UINT1               u1RowStatus;
    UINT1               au1Pad;
}
tLdpEntityPeerPasswdInfo;

typedef struct _LdpEntity
{
    tTMO_SLL_NODE       NextLdpEntity;
    tLdpTimer           HelloTimer;
    tTMO_SLL            LdpEthParamsList;
    tTMO_SLL            IfList;
    tTMO_SLL            PeerList;
#ifdef MPLS_LDP_BFD_WANTED
    tTMO_SLL            BfdSessList;
#endif
    tLdpEntityStats     LdpStatsList;
    eGenAddrType        NetAddrType;  /* Pending to remove */
    tLdpAtmParams      *pLdpAtmParams;
    tLdpEntityPeerPasswdInfo *pEntityPeerPasswdInfo;
    tStackTnlInfo       InStackTnlInfo; /* LDP over RSVP handling.
                                         Used to hold the TE tunnel
                                         informations incase of
                                         targeted LDP */
    tStackTnlInfo       OutStackTnlInfo; /* LDP over RSVP handling.
                                         Used to hold the TE tunnel
                                         informations incase of
                                         targeted LDP */
    UINT4               u4FailInitSessThreshold;
    UINT4               u4ConfSeqNum;
    UINT4               u4MessageId;
    UINT4               u4EntityConfMtu;
    UINT4               u4KeepAliveHoldTime;
    UINT4               u4EntityIndex;
    UINT4               u4AppMask; /*Application associated*/
    UINT4               u4TransportAddress;
    UINT4               u4InTnlLabel;
#ifdef MPLS_IPV6_WANTED
    tIp6Addr           Ipv6TransportAddress;  /* For IPV6 Transport address TLV*/
#endif
    UINT4               u4TransIfIndex;
#ifdef MPLS_IPV6_WANTED
    UINT4               u4Ipv6TransIfIndex;  /* For IPV6 Transport address TLV */
#endif
#ifdef MPLS_LDP_BFD_WANTED
    INT4                i4BfdStatus;
#endif
    UINT2               u2TcpPort;
    UINT2               u2UdpPort;
    UINT2               u2IntSessTrapEnable;
    UINT2               u2LabelRet;
    UINT2               u2HopVecLimit;
    UINT2               u2MaxPduLen;
    UINT2               u2LabelRangeId;
    UINT2               u2HelloHoldTime;     /*HelloHoldTime is currently maintained 
                                              for an entity.So the hello hold timer
                                              will be started for the minimal hold time
                                              negotiated between the peers.But ideal 
                                              situation would be maintaining the hello 
                                              hold timer for each adajaceny between 
                                              the peers*/
    UINT2               u2IncarnId;
    UINT2               u2ProtVersion;
    UINT2               u2HelloConfHoldTime;
    tLdpId              LdpId;
    UINT1               u1AdminStatus;
    UINT1               u1OperStatus;
    UINT1               u1DisContTime;
    UINT1               u1TargetFlag;
    UINT1               u1PathVecLimit;
    UINT1               u1PathVecTrapCond;
       /* Per-Platform or Per-Interface */
    UINT1               u1LabelSpaceType;
    UINT1               u1LabelDistrType;
    /* Atm or Ethernet Type */
    UINT1               u1EntityType;
    UINT1               u1LabelAllocType;
    /* Specifies whether the Entity acts as LER or LSR */
    UINT1               u1RouterType;
    UINT1               u1LblResAvl;
    UINT1               u1StorageType;
    UINT1               u1RowStatus;
    UINT1               u1TransAddrTlvEnable;
#ifdef MPLS_IPV6_WANTED
    UINT1               u1Ipv6TransAddrTlvEnable;  /* For IPV6 Transport address TLV */
#endif
    UINT1               u1ConfSeqNumTlvEnable;
    UINT1               u1EntityIfStatus;
    UINT1               u1ConfChgFlag;
    INT1                i1PhpConf;
    UINT1               u1TransportAddrKind;
    UINT1               u1LdpOverRsvpFlag;
    BOOL1               bIsIntfMapped;
    BOOL1                bIsTransAddrIntfUp;
#ifdef MPLS_IPV6_WANTED
     BOOL1                bIsIpv6TransAddrIntfUp;
    UINT1               u1Ipv6TransportAddrKind; /* For IPV6 Transport address TLV */ 
    UINT1                au1Pad[2];
#else
   UINT1   u1pad;
#endif
}
tLdpEntity;

typedef struct _LdpPeer
{
    tTMO_SLL_NODE        NextLdpPeer;
    tTMO_SLL             IfAdrList;
    tTMO_SLL             AtmLblRangeList;
    /* Reconnect or Recovery Timer. Since one Timer will be 
     * running at a time, Single Timer block is sufficient.
     */
    tLdpTimer            GrTimer;
#ifdef MPLS_IPV6_WANTED
    tLdpTimer            Ipv6SessPrefTimer; /* SessPrefTimer for IPv6 */
#endif
    tGenAddr             NetAddr;       /* For IPv4 and IPv6 */
    tGenAddr             TransAddr;      /* For IPv4 and IPv6 */
    tLdpEntity          *pLdpEntity;
    struct _LdpSession *pLdpSession;
    UINT4                u4PeerIndex;
    UINT4                u4DefMtu;
    UINT4                u4KeepAliveHoldTimer;
    UINT4                u4ConfSeqNum;
    tLdpId               PeerLdpId;
    /* Maps to fsMplsLdpPeerGrReconnectTime of fsmpls MIB */
    UINT2                u2GrReconnectTime;
    /* Maps to fsMplsLdpPeerGrRecoveryTime of fsmpls MIB */
    UINT2                u2GrRecoveryTime;
    UINT1                u1LabelDistrType;
    UINT1                u1RemoteFlag;
    UINT1                u1TrgtHelloTxReqFlag;
    /* Maps to fsMplsLdpPeerGrProgressStatus of fsmpls MIB */
    UINT1                u1GrProgressState;
#ifdef MPLS_IPV6_WANTED
    UINT1                u1SsnPrefTmrStatus; /* For IPV6 */
    UINT1                au1Pad[3];
#endif
    UINT1                au1Rsvd[2];
}
tLdpPeer;

#ifdef MPLS_LDP_BFD_WANTED

typedef struct _tLdpBfdSession
{
    tTMO_SLL_NODE       NextBfdSess;
    
    tGenAddr            DestAddr;       /* For IPv4 and IPv6 */
    tGenAddr            SrcAddr; 
    struct _LdpSession *pLdpSession;
 UINT4               u4IfIndex;
    UINT1               u1AddrType;
    UINT1               u1SessionType;
 UINT1               au1Pad[2];
    
}
tLdpBfdSession;
#endif

typedef struct _LdpCmnSsnParams
{
    UINT2               u2ProtocolVer;
    UINT2               u2KeepAliveTime;
    UINT2               u2MaxPduLen;
    UINT1               u1LabelAdvertMode;
    UINT1               u1LoopDetect;
    UINT1               u1PvLimit;
    UINT1               au1Pad[3];
}
tLdpCmnSsnParams;

typedef struct _LdpSession
{
    tTMO_HASH_NODE      NextHashNode;
    tLdpCmnSsnParams    CmnSsnParams;
    tLdpTimer           SsnHoldTimer;
    tLdpTimer           KeepAliveTimer;
    tTMO_SLL            IntAtmLblRngList;
    tTMO_SLL            AdjacencyList;
    tTMO_SLL            IntLspSetupList;
    tTMO_SLL            USLspCtrlBlkList;
    tTMO_SLL            DSLspCtrlBlkList;
    tLdpPeer            *pLdpPeer;
#ifdef MPLS_LDP_BFD_WANTED
    INT4                i4SessBfdStatus;
#endif
    tOsixSysTime        u4LastChange;
    /* MPLS_IPv6 mod start*/
    tGenAddr           SrcTransAddr;
    tLdpId              SessionId;
   /*                     */
    UINT2               u2ProtoVersion;
    UINT4               u4Index;
    UINT4               u4StatsUnknownMsgTypeErrors;
    UINT4               u4StatsUnknownTlvErrors;
    UINT4               u4TcpConnId;
    UINT4               u4HoldTimeRem;
    UINT4               u4NoOfInitSent;
    UINT4               u4ExpNullCount;
    UINT4               u4L3Intf;
    /* MPLS_IPv6 mod start*/
    UINT4                u4SsnTcpIfIndex;
    UINT4               u4IfIndex;
    UINT2               u2DisContTime;
    UINT1               u1SessionRole;
    UINT1               u1SessionState;
    UINT1               u1StatusRecord;
    UINT1               u1BTime;
    UINT1               u1SessionMergeType;
    UINT1               u1StatusCode;
#ifdef LDP_GR_WANTED
    UINT1               u1StaleStatus;
    UINT1               au1Pad[3];
#endif
    UINT1               u1SessionInitStatus;
#ifdef MPLS_LDP_BFD_WANTED
    BOOL1               bIsBfdSessCtrlPlaneInDep;
    UINT1    au1pad[2];
#else
    UINT1               au1pad[3];
#endif
}
tLdpSession;

typedef struct _PeerIfAdrNode
{
    tTMO_HASH_NODE      NextHashNode;
    tTMO_SLL_NODE       NextSllNode;

#ifdef MPLS_IPV6_WANTED
    tTMO_HASH_NODE      NextIpv6HashNode;
#endif

    uGenAddr            IfAddr;
    eGenAddrType        AddrType; 
    tLdpSession         *pSession;
    UINT4               u4SessionPeerAddrIndex;
}
tPeerIfAdrNode;


typedef struct _NHopInfo
{
    uGenU4Addr          NHAddr;
    UINT4               u4IfIndex;
    UINT2               u2AddrType;
    UINT1               u1Pad[2];
}
tNhopInfo;

typedef struct _FEC
{
    uGenU4Addr          Prefix;
    UINT4               u4Index;
    UINT2               u2AddrFmly;
    UINT1               u1FecElmntType;
    UINT1               u1PreLen;
}
tFec;


typedef struct _LspTrggCtrlBlock
{
    struct _LspCtrlBlock        *pOrgLspCtrlBlk;
    struct _LspCtrlBlock        *pNewNhLspCtrlBlk;
    struct _UstrLspCtrlBlock    *pUStrLspCtrlBlock;
    struct _UstrLspCtrlBlock    *pNewUStrLspCtrlBlock;
    tLdpTimer                    retryTimer;
    tNhopInfo                    nHopInfo;
    uGenU4Addr                   oRgNextHopAddr;
    tLdpSession                  *pUStrSession;
    tLdpSession                  *pDStrSession;
    struct _LspTrggCtrlBlock     *pTrigCtrlBlkInOrgLsp;
    tFec                         Fec;
    uLabel                       UStrLabel;
    UINT4                        u4UStrLblReqId;
    UINT1                        u1NhLspState;
    UINT1                        u1TrgType;
    UINT2                        u2AddrType;
}
tLspTrigCtrlBlock;

typedef struct _LdpAdjacency
{
    tTMO_SLL_NODE       NextAdjacency;
    tLdpTimer           PeerAdjTimer;
    tLdpSession        *pSession;
    tLdpPeer           *pPeer;
    uGenAddr            PeerIpAddr; /* To save PeerIp Address, 
           helpful in Address  message IP processing,
           to identify the IfIndex */
    UINT4               u4Index;
    UINT4               u4HoldTimeRem;
    UINT4               u4IfIndex;
    UINT1               u1AddrType;
    UINT1               u2Pad[3];

}
tLdpAdjacency;

typedef struct _FECElmnt
{
    tFec                Fec;
    struct _FECElmnt   *pNxtFECElmnt;
}
tFECElmnt;

typedef struct _LspSetupInfo
{
 tTMO_SLL_NODE       NextSetupNode;
 tLdpSession         *pLdpSession;
 uGenU4Addr          NextHop;
 UINT4               u4IfIndex;
 uGenU4Addr          Dest;
 UINT2               u2IncarnId;
        UINT2               u2AddrType;
 UINT1               u1PrefixLen;
 UINT1               au1Pad[3];
}
tIntLspSetupInfo;


typedef struct _LspCtrlBlock
{
    tTMO_SLL_NODE       NextUSLspCtrlBlk;
    tTMO_SLL_NODE       NextDSLspCtrlBlk;
    tLdpTimer           PerstTimer;
    tLdpTimer           TardyTimer;
    uLabel              UStrLabel;
    uLabel              DStrLabel;
    tFec                Fec;
    tTMO_SLL            UpstrCtlBlkList; /* List of Upstream Ctrl Blocks
                                          * associated with this downstream 
                                          * control block.
                                          */
    tLdpSession         *pUStrSession;
    tLdpSession         *pDStrSession;
    tLspTrigCtrlBlock   *pTrigCtrlBlk;
    tLspTrigCtrlBlock   *pNHCtrlBlk;
    tCrlspTnlInfo       *pCrlspTnlInfo;
    UINT1               *pu1PVTlv;
    UINT4               u4UStrLblReqId;
    UINT4               u4DStrLblReqId;
    uGenU4Addr          NextHopAddr;
    UINT4               u4InComIfIndex;
    UINT4               u4OutIfIndex;
    UINT4               u4OrgOutIfIndex;
    UINT4               u4CrossConnectIndex;
    UINT1               u1IsLspUsedByL2VPN;
    UINT1               u1MLibUpdStatus; /* Valid only in DU Libreal mode 
                                            if route does not exist. */
    UINT1               u1LspState;
    UINT1               u1InLblType;
    UINT1               u1OutLblType;
    UINT1               u1LblType;
    UINT1               u1HopCount;    /* No of Hops the pkt has to go, for */
    UINT1               u1SubState;    /* States, if Abort ReqMsg has been sent 
                                       and is waiting for ReqAborted or Notif */
    UINT1               u1PVCount;
    UINT1               u1LblReqFwdFlag; 
    UINT1               u1IsStaticLsp;  /* This variable is set to LDP_TRUE
                                           if the LSP control block is created in
                                           response to STATIC FEC creation */
#ifdef LDP_GR_WANTED
    UINT1               u1StaleStatus;
#else
    UINT1               u1Rsvd;
#endif
    UINT2               u2AddrType;
    UINT1               au1Pad[2];
}
tLspCtrlBlock;

/* The following structure is associated with the Merge and DU state machines.
 */
typedef struct _UstrLspCtrlBlock
{
    tTMO_SLL_NODE       NextUpstrCtrlBlk; /* Placed in Session US CB list */
    
    tTMO_SLL_NODE       NextUpstrNode;    /* Placed in DSCB upstream LCB List */
    
    uLabel              UStrLabel;        /* Label Assigned to Upstream LSR */
    
    tFec                Fec;              /* Fec for which the label request 
                                           * is received.
                                           */
    
    tLdpSession         *pUpstrSession;   /* Pointer to the Upstream Session 
                                           * associated with this Upstream 
                                           * Ctrl Blk.
                                           */
    
    tLspTrigCtrlBlock   *pTrigCtrlBlk;    /* Pointer to the Trigger Ctrl Blk */
    
    tLspTrigCtrlBlock   *pNHCtrlBlk;      /* Pointer to the Next Hop Ctrl Blk
                                           * in case the Next Hop change
                                           * occurs.
                                           */
    
    tLspCtrlBlock       *pDnstrCtrlBlock; /* Pointer to the Downstream Ctrl Blk
                                           * associated with this Upstream Ctrl
                                           * Blk.
                                           */
    
    UINT4               u4UpstrLblReqId;  /* Label Request Id of the Lbl Req Msg
                                           * rcvd from upstream LSR
                                           */
    
    UINT4               u4InComIfIndex;   /* Interface Index on which the label
                                           * Request Msg is received.
                                           */
    
    UINT4               u4CrossConnectIndex; /* cross connect index */
    UINT2               u2Pad;
    UINT1               u1InLblType;      /* Type of Label allocated to the 
                                           * Upstream LSR.
                                           */
    UINT1               u1LspState;       /* State of the Upstream Ctrl Blk*/
    BOOL1               bIsConnectedRoute; /* True: If FEC associated with this
                                                    LSP Control block is
                                                    connected route.
                                              False: If FEC associated with this
                                                     LSP Control block is
                                                     not connected route.
                                            */
#ifdef LDP_GR_WANTED
    UINT1               u1StaleStatus;
    UINT1               au1Rsvd[2];
#else
    UINT1               au1Rsvd[3];
#endif
}
tUstrLspCtrlBlock;


/* This structure is used for 2 purposes:-
 * (a)Exchange of information between upstream and downstream
 * state event machine
 * (b) Parse LDP message and store the necessary
 * information in this structure
 */
typedef struct _LdpMsgInfo
{
    tUstrLspCtrlBlock     *pUpstrCtrlBlk;   /*Used by DU and VC Merge 
                                             *State Machines */
    tFec       Fec;
    uLabel     Label;
    UINT1      *pu1HCTlv;       /* Hop Count TLV present in Received
                                 * LDP PDU 
                                 */
    UINT1      *pu1PVTlv;       /* Path Vector TLV present in Received
                                 * LDP PDU 
                                 */
    UINT1      *pu1Msg;         /* Received LDP PDU or NULL */
    UINT1      au1UnknownTlv [LDP_MAX_UNKNOWN_TLV_LEN];   /* Used to forword unknownTLV */
    UINT4      u4NextHopAddr;   /* Used by Next Hop State Machine
                                 * of VC Merge */
    UINT4      u4OutIfIndex;    /* Same as above.. */
    UINT1      u1HopCount;      /* Hop Count from Received PDU */
    UINT1      u1PVCount;       /* Path Count from Received PDU */
    UINT1      u1UnknownTlvLen; /* Unknown TLV length */
    UINT1      u1IsLblChanged;  /* LDP_TRUE, If Label is changed */           
}
tLdpMsgInfo;

 
typedef struct _IfEntry
{
    tTMO_SLL            aCrlspHoldPrio[HOLD_PRIO_RANGE];
    tTMO_SLL            aDSPerOAHoldPrio[HOLD_PRIO_RANGE];
    UINT4               u4IfIndex;
    UINT4               u4IfSpeed;
    UINT4               u4NoofEntyMcastsCount;
    /* MPLS_IPv6 add start*/
    UINT4               u4NoofEntyV6McastsCount;
    /* MPLS_IPv6 add end*/ 
    UINT4 u4aIfSpeedPerOA[LDP_DS_MAX_NO_PSCS];
    UINT4 u4aIfSpeedClassType[LDP_DS_MAX_CLASS_TYPE];
}
tLdpIfEntry;

typedef struct _FecTableEntry
{
    tTMO_HASH_NODE      NextHashNode;
    tIpv4Addr           Ipv4Addr;
    tIpv6Addr           Ipv6Addr;
    tLspCtrlBlock       *pLspCtrlBlock;
    UINT4               u4FecIndex;
    UINT2               u2AddrFmly;
    UINT1               u1FecElmntType;
    UINT1               u1PreLen;
    UINT1               u1StorageType;
    UINT1               u1RowStatus;
    UINT1               u1LdpLspStorageType;
    UINT1               u1LdpLspRowStatus;
}
tFecTableEntry;

/* This structure is added for system sizing.
 * It should not be used for any other purpose 
 */

typedef struct _FecTableEntrySize
{
    UINT1               au1FecTableEntry[LDP_MAX_FEC_TABLE_SIZE];
}
tFecTableEntrySize;

/* This structure is added for system sizing.
 * It should not be used for any other purpose 
 */

typedef struct _L2VpnLdpIfStrLenSize
{
    UINT1               au1L2VpnLdpIfStrLen[MAX_LDP_L2VPN_IF_STR_LEN];
}
tL2VpnLdpIfStrLenSize;


/* This structure is added for system sizing.
 * It should not be used for any other purpose 
 */

typedef struct _LdpMinMTUSize
{
    UINT1              au1LdpMinMTU[LDP_MIN_MTU];
}
tLdpMinMTUSize;


/* This structure is added for system sizing.
 * It should not be used for any other purpose 
 */

typedef struct _LdpMaxFragmentSize
{
    UINT1              au1LdpMaxFragment[LDP_MAX_FRAGMENT];
}
tLdpMaxFragmentSize;

typedef struct _LdpPendMaxFragmentSize
{
    UINT1              au1LdpPendMaxFragment[LDP_PEND_MAX_FRAGMENT];
}
tLdpPendMaxFragmentSize;


typedef struct _LdpIfTableEntry
{
 tTMO_SLL_NODE       NextIfEntry;
 tLdpEntityPeerPasswdInfo *pEntityPeerPasswdInfo;
 tIpv4Addr           IfAddr;
#ifdef MPLS_IPV6_WANTED
 tIp6Addr            LnklocalIpv6Addr;     /* IPv6 Link Local Address */
 tIp6Addr            SitelocalIpv6Addr;    /* IPv6 Site Local Address */
 tIp6Addr            GlbUniqueIpv6Addr;    /* IPv6 Globally Unique Address */
#endif
 UINT4               u4IfIndex;
 UINT2               u2Count;
 UINT1               u1OperStatus;
 UINT1               u1InterfaceType;         /* Interface Type */
}
tLdpIfTableEntry;

typedef struct _LdpRouteEntryInfo
{
 uGenU4Addr          DestAddr;    /* The destination address */
 uGenU4Addr          DestMask;    /* The net mask to be applied to get
           the non-host portion for the 
           destination */
 uGenU4Addr          NextHop;     /* The nexthop address to which any
           datagrams destined to the destination,
           to be forwarded.
           (In some special cases) */    
 UINT4               u4RtIfIndx;    /* The interface through which the 
           route is learnt */
 UINT4               u4RtNxtHopAS;  /* The autonomous system number of 
           next hop  */
 INT4                i4Metric1;     /* The reachability cost for the 
           destination */
 UINT4               u4RowStatus;   /* status of the row */
 UINT4               u4Flag; /* Indicates reachablity of route,Best route & Hardware status */
 UINT2               u2AddrType;
 UINT1               au1Pad[2];
}
tLdpRouteEntryInfo;

typedef struct _TnlIncarnNode {
    tTMO_SLL_NODE       NextTnlIndex;
    UINT4               u4TnlIndex;
    UINT2               u2RowStatus;
    UINT2               u2StorageType;
} tTnlIncarnNode;

typedef struct _RouteFec {
    uGenU4Addr Prefix;
    UINT4 u4IsIndexAble;
    uGenU4Addr  NextHop;
    UINT4 u4OutIfIndex;
    UINT2 u2AddrFmly;
    UINT1 u1PreLen;
    UINT1 u1Resvd;
} tRouteFec;

typedef struct _LdpAppRegTbl
{
    UINT1 (*pCallBackFnAppInit) (UINT2);
    UINT1 (*pCallBackFnAppDeInit) (UINT2);
    VOID (*pCallBackFnAppPktIn) (tLdpSession *, UINT1 *, UINT4);
    VOID (*pCallBackFnSsnStatusChng) (tLdpSession *, UINT4);
    UINT1 u1RegFlag;
    UINT1 u1FecType;
    UINT2 u2AppId;
} tLdpAppRegTbl;

typedef struct _LdpIncarnInfo
{

    tLdpCfgParams       LdpParams;
    tTMO_SLL            TnlIncarnList; 
    tTMO_SLL            LdpEntityList;
    tLdpIfEntry         aIfaceEntry[MAX_LDP_IFACES + 1];
    tldpDiffServGlobal  DiffServGlobal;
    tTMO_HASH_TABLE     *pPeerIfAdrTable;
#ifdef MPLS_IPV6_WANTED
 tTMO_HASH_TABLE     *pPeerIfIpv6AdrTable; /* Hash table For IPv6 Address table*/ 
#endif
    tTMO_HASH_TABLE     *pCrlspTnlTbl;
    tTMO_HASH_TABLE     *pTcpConnHshTbl;
    tFecTableEntry      *paFecEntry;
    tLdpPeer            *apPeerEntry[LDP_MAX_PASSIVE_PEERS];
    tLdpTimer           FecRouteTimer;
    /* Forwarding Entry Holding Timer */
    tLdpTimer           FwdHoldingTmr;
    /* Registration Information*/
    tRouteFec           aFecRoute[MAX_LDP_IP_RT_ENTRIES];
    /* Array of FecRoute Entries and Fec Route Timer are
     * used for better Next Hop simulation as there is no
     * such event from Routing protocol */
    UINT4               u4TcpStdServConnId;
    /* Sock descriptor of the opened Udp socket */
    INT4                i4UdpSockDesc;
#ifdef MPLS_IPV6_WANTED
 INT4                i4Ipv6UdpSockDesc; /* UdpSockDesc For IPv6 */
#endif
    tLsrId              LsrId;
    tLsrId              NewLsrId;
    UINT2               u2IncarnIndex;
    UINT2               u2AdminStatus;
    UINT2               u2SessionTrapEnb;
    UINT2               u2CrlspTnlTrapEnb;
#ifdef MPLS_LDP_BFD_WANTED
    UINT2               u2BfdSessionsCount;
#endif
    /* Maps to fsMplsLdpGrNeighborLivenessTime of fsmpls MIB */
    UINT2               u2NbrLivenessTime;
    /* Maps to fsMplsLdpGrMaxRecoveryTime of fsmpls MIB */
    UINT2               u2MaxRecoveryTime;
    /* Maps to fsMplsLdpGrForwardEntryHoldTime of fsmpls MIB */
    UINT2               u2GrFwdHoldingTime;
    /* Maps to fsMplsLdpGrProgressStatus of fsmpls MIB */
    UINT1               u1GrProgressStatus;
    /* Maps to fsMplsLdpGrCapability of fsmpls MIB */
    UINT1               u1GrCapability;
    UINT1               u1LoopDetect;
    UINT1               u1LabelRetentionMode;
    UINT1               u1RowStatus;
    UINT1               u1LabelAllocType;
    UINT1               u1LdpForceOption;
    UINT1               u1ConfigSeqTlvOption;
#ifdef MPLS_IPV6_WANTED
    UINT1               u1LastV4InterfaceDown;
    UINT1   au1pad[3];
#endif
#ifndef  MPLS_LDP_BFD_WANTED
  UINT1               au1Rsvd[2];
#endif

}
tLdpIncarnInfo;

typedef struct _ldpRMInfo{

UINT4  u4LdpRmState;
UINT4  u4PeerCount;
UINT4  u4DynBulkUpdatStatus;
UINT4  u4LdpRmGoActiveReason;
UINT1  u1BulkUpdModuleStatus;
BOOL1  b1IsBulkReqRcvd;
UINT1  u1AdminState;
UINT1  u1Pad;

}tLdpRMInfo;


typedef struct _LdpGlobalInfo
{
    tOsixTaskId         TaskId;
    tOsixQId            QId;
    tOsixQId            UdpQId;
    tOsixQId            gLdpTcpQId;
    tOsixSemId          SemId;
    tTimerListId        TimerListId;
    tLdpRMInfo          ldpRmInfo;
    INT4                i4SyslogId;
    UINT4               u4ActiveIncarn;
    tLdpIncarnInfo      LdpIncarn[LDP_MAX_INCARN];
    UINT1               u1IncarnUp;
    UINT1               u1Policy; /*Policy Flag :
                                   To distribute only connected routes or
                                   both connected and not connected routes*/
    UINT1               u1LdpInitialised;
    UINT1               au1Pad;
}
tLdpGlobalInfo;

/*
 *  Following defintions are done in order to ease the processing of 
 *  LDP Messages.
 */
typedef struct _LdpPduHdr
{
    UINT2               u2Version;
    UINT2               u2PduLen;
    tLdpId              LdpId;
    UINT2               u2Pad;
}
tLdpPduHdr;

typedef struct _LdpMsgHdrAndId
{
    UINT2               u2MsgType;
    UINT2               u2MsgLen;
    UINT4               u4MsgId;
}
tLdpMsgHdrAndId;

typedef struct _TlvHdr
{
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
}
tTlvHdr;

typedef struct _PwGroupingIdTlv
{
    tTlvHdr             TlvHdr;
    UINT4               u4GrpId;
}
tPwGroupingIdTlv;

typedef struct _PwStatusTlv
{
    tTlvHdr             TlvHdr;
    UINT4               u4PwStatusCode;
}
tPwStatusTlv;

typedef struct _TransportAddrTlv
{
    tTlvHdr             TlvHdr;
    uGenAddr            TAddr;
}
tTransportAddrTlv;

typedef struct _ConfSeqNumTlv
{
    tTlvHdr             TlvHdr;
    UINT4               u4ConfSeqNum;
}
tConfSeqNumTlv;

typedef struct _FECTlv
{
    tTlvHdr             TlvHdr;
    tFECElmnt          *pFECElmnts;
}
tFECTlv;

typedef struct _GenLblTlv
{
    tTlvHdr             GenLblTlvHdr;
    UINT4               u4GenLbl;
}
tGenLblTlv;

typedef struct _AtmLblTlv
{
    tTlvHdr             AtmLblTlvHdr;
    UINT2               u2Vpi;
    UINT2               u2Vci;
}
tAtmLblTlv;

typedef struct _AddrTlv
{
    tTlvHdr             AddrTlvHdr;
    UINT2               u2AddrType;
    UINT2               u2Resvd;
    tTMO_SLL            AddrList;
}
tAddrTlv;

typedef struct _HopCountTlv
{
    tTlvHdr             HcHdr;
    UINT1               u1HcValue;
    UINT1               au1Pad[LDP_THREE];
}
tHopCountTlv;

typedef struct _LsrIdList
{
    tLsrId              LsrId;
    struct _LsrIdList  *pNxtLsrId;
}
tLsrIdList;

typedef struct _PathVecTlv
{
    tTlvHdr             PVTlvHdr;
    tLsrIdList         *pLsrIds;
}
tPathVecTlv;

typedef struct _StatusTlv
{
    tTlvHdr             StatusTlvHdr;
    UINT4               u4StatusCode;
    UINT4               u4MsgId;
    UINT2               u2MsgType;
    UINT2               u2Pad;
}
tStatusTlv;

typedef struct _ExtndStatusTlv
{
    tTlvHdr             TlvHdr;
    UINT4               u4ExtndStatus;
}
tExtndStatusTlv;

typedef struct _ReturnPduTlv
{
    tTlvHdr             TlvHdr;
    UINT1              *pu1ReturnPdu;
}
tReturnPduTlv;

typedef struct _ReturnMsgTlv
{
    tTlvHdr             TlvHdr;
    UINT1              *pu1ReturnMsg;
}
tReturnMsgTlv;

typedef struct _LblReqMsgIdTlv
{
    tTlvHdr             TlvHdr;
    UINT4               u4LblReqMsgId;
}
tLblReqMsgIdTlv;

typedef struct _CmnHelloParmsTlv
{
    tTlvHdr             CmnHelloParmsTlvHdr;
    UINT2               u2HoldTime;
    UINT2               u2TRField;
}
tCmnHelloParmsTlv;

typedef struct _CmnSsnParmsTlv
{
    tTlvHdr             CmnSsnParmsTlvHdr;
    UINT2               u2ProtclVer;
    UINT2               u2KeepAliveTime;
    UINT1               u1ADResv;
    UINT1               u1PvLimit;
    UINT2               u2MaxPduLen;
    tLdpId              RcvrLdpId;
    UINT2               u2Pad;
}
tCmnSsnParmsTlv;

typedef struct _AtmLblRange
{
    UINT2               u2MinVpi;
    UINT2               u2MinVci;
    UINT2               u2MaxVpi;
    UINT2               u2MaxVci;
}
tAtmLblRange;

typedef struct _AtmLblRngComp
{
    tAtmLblRange        AtmLblRange;
    struct _AtmLblRngComp *pNxtLblRngComp;
}
tAtmLblRngComp;

typedef struct {
    tLdpSession* pSession;
    tLdpEntity*  pEntity;
    UINT1        PeerPvl;
    UINT1        u1TrapType; /* LDP_TRAP_SESS_UP, LDP_TRAP_SESS_DN, 
                              * LDP_TRAP_PVL_MISMH, LDP_TRAP_SESS_THOLD_EXCD
                              */  
    UINT2        u2Pad;
}tLdpTrapInfo;

typedef struct _AtmSsnParmsTlv
{
    tTlvHdr             AtmSsnTlvHdr;
    UINT4               u4MNDResv;
    tAtmLblRngComp     *pAtmLblRanges;
}
tAtmSsnParmsTlv;

/* 
 * Following TLV types are declared to process CRLSP related LDP Messages.
 */
typedef struct _ExpRouteTlv
{
    tTlvHdr             TlvHdr;
}
tExpRouteTlv;

typedef struct _CrlspTrfcParmsTlv
{
    tTlvHdr             TlvHdr;
    UINT1               u1Flags;
    UINT1               u1Frequency;
    UINT1               u1Reserved;
    UINT1               u1Weight;
    UINT4               u4PeakDataRate;
    UINT4               u4PeakBurstSize;
    UINT4               u4CommittedDataRate;
    UINT4               u4CommittedBurstSize;
    UINT4               u4ExcessBurstSize;
}
tCrlspTrfcParmsTlv;

typedef struct _CrlspPreemptTlv
{
    tTlvHdr             TlvHdr;
    UINT1               u1SetPrio;
    UINT1               u1HoldPrio;
    UINT2               u2Reserved;
}
tCrlspPreemptTlv;

typedef struct _CrlspLspidTlv
{
    tTlvHdr             TlvHdr;
    UINT2               u2Reserved;
    UINT2               u2LocalLspid;
    tLdpRouterId        IngressLsrRtrId;
}
tCrlspLspIdTlv;

typedef struct _CrlspResClsTlv
{
    tTlvHdr             TlvHdr;
    UINT4               u4ResourceClass;
}
tCrlspResClsTlv;

typedef struct _ErHopIpv4Tlv
{
    tTlvHdr             TlvHdr;
    UINT1               u1CrlspPathType;
    UINT1               u1Reserved1;
    UINT1               u1Reserved2;
    UINT1               u1PrefLen;
    tIpv4Addr           Ipv4Address;
}
tErHopIpv4Tlv;

typedef struct _ErHopIpv6Tlv
{
    tTlvHdr             TlvHdr;
    UINT1               u1CrlspPathType;
    UINT1               u1Reserved1;
    UINT1               u1Reserved2;
    UINT1               u1PrefLen;
    tIpv6Addr           Ipv6Address;
}
tErHopIpv6Tlv;

typedef struct _ErHopAsTlv
{
    tTlvHdr             TlvHdr;
    UINT1               u1CrlspPathType;
    UINT1               u1Reserved;
    UINT2               u2AsNumber;
}
tErHopAsTlv;

typedef struct _ErHopLspidTlv
{
    tTlvHdr             TlvHdr;
    UINT1               u1CrlspPathType;
    UINT1               u1Reserved;
    UINT2               u2LocalLspid;
    tLdpRouterId        IngressLsrRtrId;
}
tErHopLspidTlv;

typedef struct _RoutePinningTlv
{
    tTlvHdr             TlvHdr;
    UINT1               u1RoutePinningType;
    UINT1               u1Reserved;
    UINT2               u2Reserved;
}
tRoutePinningTlv;

/*
    0                   1                   2                   3
    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |1|0| FT Session TLV (0x0503)   |      Length (= 12)            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |     FT Flags                  |      Reserved                 |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                FT Reconnect Timeout (in milliseconds)         |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                Recovery Time (in milliseconds)                |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/

typedef struct _FTSessionTlv
{
    tTlvHdr             FTSessionTlvHdr;
    UINT2               u2FTFlags;
    UINT2               u2Rsvd;
    UINT4               u4FTReconnectTimeout; /* In milliseconds */
    UINT4               u4FTRecoverTime; /* In milliseconds */
}
tFTSessionTlv;

/* ICCP TLVs */

typedef struct _RedundantGrpIdTlv
{
    tTlvHdr             TlvHdr;
    UINT4               u4RgId;
} tRgIdTlv;

typedef struct _ServiceNameTlv
{
    tTlvHdr                   TlvHdr;
    tL2vpnIccpServiceName     PwServiceName;
    UINT1                     au1Pad [3];
} tServiceNameTlv;

typedef struct _PwIdTlv
{
    tTlvHdr         TlvHdr;
    tLdpRouterId    PeerRouterId;
    UINT4           u4PwGroupId;
    UINT4           u4PwId;
} tPwIdTlv;

typedef struct _GenPwIdTlv
{
    tTlvHdr             TlvHdr;
    UINT1               u1AgiType;
    UINT1               u1AgiLen;
    UINT1               au1Agi [L2VPN_PWVC_AGI1_LEN];
    UINT1               u1SaiiType;
    UINT1               u1SaiiLen;
    UINT1               au1Saii [L2VPN_PWVC_AII2_LEN];
    UINT1               u1TaiiType;
    UINT1               u1TaiiLen;
    UINT1               au1Taii [L2VPN_PWVC_AII2_LEN];
    UINT1               au1Pad [2];
} tGenPwIdTlv;

typedef struct _PwRedConfigTlvHdr
{
    tTlvHdr             TlvHdr;
    tL2vpnIccpRoId      RoId;
    UINT2               u2PwPriority;
    UINT2               u2Flags;
} tPwRedConfigTlvHdr;

typedef struct _PwRedStateTlv
{
    tTlvHdr             TlvHdr;
    tL2vpnIccpRoId      RoId;
    UINT4               u4LocalPwState;
    UINT4               u4RemotePwState;
} tPwRedStateTlv;

typedef struct _PwRedSyncRqstTlvHdr
{
    tTlvHdr             TlvHdr;
    UINT2               u2RqstId;
    UINT2               u2RqstType;
} tPwRedSyncRqstTlvHdr;

typedef struct _PwRedSyncDataTlvHdr
{
    tTlvHdr             TlvHdr;
    UINT2               u2RqstId;
    UINT2               u2Flags;
} tPwRedSyncDataTlvHdr;

typedef union
{
    tPwIdTlv            PwIdTlv;
    tGenPwIdTlv         GenPwIdTlv;
} tPwIdOrGenPwIdTlv;

typedef union
{
    tServiceNameTlv     ServiceNameTlv; 
    tPwIdOrGenPwIdTlv   PwIdOrGenPwIdTlv;
} tPwRedSyncRqstOptTlv;

typedef struct
{
    tPwRedSyncRqstTlvHdr RqstTlvHdr;
    tPwRedSyncRqstOptTlv RqstOptTlv [LDP_ICCP_MAX_PW_IN_RG];
    UINT1 u1Type;
    UINT1 u1Count;
    UINT1 au1Pad [2];
} tPwRedSyncRqstTlv;

typedef struct 
{
    tPwRedConfigTlvHdr   PwRedConfigTlvHdr;
    tServiceNameTlv      ServiceNameTlv;
    tPwIdOrGenPwIdTlv    PwIdOrGenPwIdTlv;
    UINT1                u1FecType;
    UINT1                au1Pad [3];
} tPwRedConfigTlv;

typedef struct
{
    tPwRedSyncDataTlvHdr  SyncDataTlvHdr [2];
    tPwRedConfigTlv       PwRedConfigTlv [LDP_ICCP_MAX_PW_IN_RG];
    tPwRedStateTlv        PwRedStateTlv [LDP_ICCP_MAX_PW_IN_RG];
    UINT1                 u1ConfigTlvCount;
    UINT1                 u1StateTlvCount;
    UINT1                 au1Pad [2];
} tPwRedSyncDataTlv;

#ifdef LDP_HA_WANTED
typedef struct _LdpRmEvtInfo
{
    tRmMsg        *pBuff;     /* Message given by RM module. */
    UINT2          u2Length;   /* Length of message given by RM module. */
    UINT1          u1Event;    /* Event given by RM module. */
    UINT1          u1Reserved; /* Added for alignment purpose */
}tLdpRmEvtInfo;
#endif

#ifdef LDP_GR_WANTED
typedef struct _LdpGrLblRelEvtInfo
{
    uLabel Label;
}tLdpGrLblRelEvtInfo;
#endif


/* Parameters for TCP Event */
typedef struct {
    UINT1* pPdu;
    UINT4  u4IncarnNo;
    UINT4  u4ConnId;
    UINT4  u4Evt;
}tLdpTcpEvtInfo;

/* Parameters for UDP Event */ 
typedef struct {
    UINT1* pPdu;
    UINT4  u4IfIndex;
    UINT4  u4PeerAddr;
    UINT4  u4Evt;
    /* MPLS_IPv6 add start*/
    #ifdef MPLS_IPV6_WANTED
    tIpAddr PeerAddr;
    UINT1   u1DiscardV6LHello;
    UINT1  au1Pad[3]; 
    #endif 
    /* MPLS_IPv6 add end*/ 
  
}tLdpUdpEvtInfo;

/* Parameters for SNMP Event */
typedef struct {
    UINT4   u4SubEvt;
    UINT4   u4IncarnId;
    VOID    *pParams; 
}tLdpSnmpEvtInfo;


/* IP Event Info */
typedef struct {
 UINT1*  pRouteInfo;
 UINT4   u4IfIndex;  
 UINT4   u4IpAddr;
#ifdef MPLS_IPV6_WANTED
 tIp6Addr              LnklocalIpv6Addr;
 tIp6Addr              SitelocalIpv6Addr;
 tIp6Addr              GlbUniqueIpv6Addr;
#endif
 UINT4   u4IncarnId;
 UINT4   u4BitMap;
}tLdpIpEvtInfo;

/* Internal Event Info */
typedef struct {
    UINT4  u4SubEvt;   /* RES_AVL_EVENT or EST_LSP */
    UINT1* pSession;
    UINT1* pEntity;
    UINT4 u4IncarnNo;  
    UINT2 u2AddrType;
    UINT1 u1Pad[2];
}tLdpIntEvtInfo;

typedef struct _LdpAppEvtInfo
{
    UINT4            u4AppId;
    UINT4            u4EvtType;
    UINT4            u4DataLen;
    VOID             *pData;
} 
tLdpAppEvtInfo;

typedef struct _LdpTeEvtInfo
{
  UINT4 u4TeEvt;
  tLdpTeTnlInfo *pTeTnlInfo;
}tLdpTeEvtInfo;

/* LDP over RSVP event information */
typedef struct _LdpOverRsvpEvtInfo
{
  UINT4 u4LdpEntIndex;
  UINT4 u4TnlIndex;
  UINT4 u4TnlInstance;
  UINT4 u4IngressId;
  UINT4 u4EgressId;
  UINT4 u4XcOrIfIndex;
  UINT4 u4TnlLabel;
  UINT1 u1EvtType;
  UINT1 au1Rsvd[3];
}tLdpOverRsvpEvtInfo;

#ifdef MPLS_LDP_BFD_WANTED

typedef struct _LdpBfdEvtInfo
{
  #define  tLdpBfdInfo tBfdClientNbrIpPathInfo 
   UINT4 u4BfdEvt;
   tLdpBfdInfo  LdpBfdMsgInfo;  
}tLdpBfdEvtInfo;

#endif

typedef struct {
    UINT4 u4MsgType; /* LDP_SNMP_EVENT
                      * LDP_UDP_EVENT
                      * LDP_UDP6_EVENT
                      * LDP_TCP_EVENT
                      * LDP_IP_EVENT
                      * LDP_INTERNAL_EVENT
                      * LDP_APP_EVENT
                      * LDP_TE_EVENT
                      * LDP_OVER_RSVP_EVENT
                      * LDP_RM_EVENT
                      * LDP_BFD_EVENT
                      */
    union {
        tLdpUdpEvtInfo    UdpEvt; 
        tLdpTcpEvtInfo    TcpEvt;
        tLdpIntEvtInfo    IntEvt;
        tLdpIpEvtInfo   IpEvt; 
        tLdpSnmpEvtInfo SnmpEvt;
        tLdpAppEvtInfo  AppEvt;
        tLdpTeEvtInfo   TeEvt;
        tLdpOverRsvpEvtInfo  LdpOverRsvpEvt;
#ifdef LDP_HA_WANTED
        tLdpRmEvtInfo   LdpRmEvt;
#endif
#ifdef LDP_GR_WANTED
        tLdpGrLblRelEvtInfo  LdpGrLblRelEvt;
#endif

#ifdef MPLS_LDP_BFD_WANTED
       tLdpBfdEvtInfo  BfdEvt;
#endif
         }u;
}tLdpIfMsg;

/* function pointers */
typedef UINT1       (*tLdpProcEvtFuncPtr) (UINT4[]);
typedef VOID        (*tSsnFsmFuncPtr) (tLdpSession *, UINT1, UINT1 *);
typedef VOID        (*tNonMrgFsmFuncPtr) (tLspCtrlBlock *, UINT1 *);
typedef VOID        (*tNonMrgNhopChngFsmFuncPtr) (tLspTrigCtrlBlock *, UINT1 *);
typedef VOID        (*tMrgNhopChngFsmFuncPtr) (tLspTrigCtrlBlock *, UINT1 *);
typedef VOID        (*tMrgDnFsmFuncPtr) (tLspCtrlBlock *, tLdpMsgInfo *);
typedef VOID        (*tMrgUpFsmFuncPtr) (tUstrLspCtrlBlock*, tLdpMsgInfo *);
typedef VOID        (*tDuDnFsmFuncPtr) (tLspCtrlBlock *, tLdpMsgInfo *);
typedef VOID        (*tDuUpFsmFuncPtr) (tUstrLspCtrlBlock*, tLdpMsgInfo *);

#endif /*_LDPTDFS_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldptdfs.h                              */
/*---------------------------------------------------------------------------*/
