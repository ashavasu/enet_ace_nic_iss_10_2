
 /* $Id: ldpincs.h,v 1.10 2014/11/08 11:41:01 siva Exp $*/
/********************************************************************
 *                                                                  *
 * $$Id: ldpincs.h,v 1.10 2014/11/08 11:41:01 siva Exp $
 *                                                                  *
 * $Date: 2014/11/08 11:41:01 $                                     *
 *                                                                  *
 * $Revision: 1.10 $                                                 *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpincs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains all include files
 *---------------------------------------------------------------------------*/
#ifndef _LDP_H
#define _LDP_H

#include "lr.h"
#include "ip.h"
#include "ip6util.h"

#include "include.h"

#include "trieapif.h"
#include "triecidr.h"

#include "mplsdiff.h"
#include "mplcmndb.h"
#ifdef LDP_GR_WANTED
#include "mplshwlist.h"
#include "mplslsr.h"
#include "mplsftn.h"
#endif

#include "mpls.h"
#include "cfa.h"
#include "lblmgrex.h"
#include "mplscli.h"

/* Te includes */
#include "tedsmacs.h"
#include "temacs.h"
#include "teextrn.h"
#include "tedsdefs.h"

/*mplsinc includes */
#include "ldpext.h"
#include "mplssize.h"
#include "mplsdiff.h"
#include "mplsdefs.h"
#include "mplsdsrm.h"
#include "dsrmgblex.h"
#include "mplfmext.h"

#include "tcextrn.h"
#include "tcapi.h"

/* Mpls NP include */
#include "mplsnpex.h"

/* The other header files in the LDP */
#include "ldpte.h"
#include "l2vpextn.h"
#include "ldpdefs.h"
#include "ldpdsdef.h"
#include "ldptdfs.h"
#include "ldpsli.h"
#include "ldpstevt.h"
#include "ldpmacs.h"
#include "ldpport.h"
#include "ldpextif.h"
#include "ldpgblex.h"
#include "ldpprot.h"
#include "ldpdbg.h"
#include "ldpdstdf.h"
#include "ldpdsmcs.h"
#include "ldpdspt.h"
#include "ldpdste.h"
#include "ldppw.h"

#include "ldpsz.h"
#include "fsmplslw.h"
#include "cli.h"
#include "iss.h"
#include "csr.h"
#include "stdgnlwr.h"
#include "fsmplswr.h"
#include "stdldpwr.h"
#ifdef LDP_HA_WANTED
#include "ldprm.h"
#endif

#include "../../../inc/rmgr.h"
#ifdef LDP_TEST_WANTED
#include "fsldptwr.h"
#endif
#ifdef MPLS_LDP_BFD_WANTED
#include "bfd.h"
#endif
#endif /*_LDP_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpincs.h                              */
/*---------------------------------------------------------------------------*/
