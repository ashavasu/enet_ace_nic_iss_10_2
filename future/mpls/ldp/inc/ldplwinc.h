
/********************************************************************
 *                                                                  *
 * $RCSfile: ldplwinc.h,v $
 *                                                                  *
 * $Date: 2007/02/13 13:54:01 $                                     
 *                                                                  *
 * $Revision: 1.3 $
 *                                                                  *
 *******************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldplwinc.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP (LDP - Low Level Routines)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains Include files and few defs
 *                             that are common to the files containing the 
 *                             Low Level routines of the LDP Module.
 *---------------------------------------------------------------------------*/
#ifndef _LDPLWINC_H_
#define _LDPLWINC_H_


 /* Following are the two macros that are made use for copying to and from
    * tSNMP_OCTET_STRING_TYPE */

#define SNMP_DECLARE_ONE_OCTET_STRING(x)\
    tSNMP_OCTET_STRING_TYPE x;\
    UINT1             OctectVal[16];\
    x.pu1_OctetList = OctectVal;

#define SNMP_DECLARE_TWO_OCTET_STRING(x,y)\
    tSNMP_OCTET_STRING_TYPE x;\
    tSNMP_OCTET_STRING_TYPE y;\
    UINT1             OctectVal1[16];\
    UINT1             OctectVal2[16];\
    x.pu1_OctetList = OctectVal1;\
    y.pu1_OctetList = OctectVal2;

#define SNMP_OCTET_STRING_LIST(x) (x->pu1_OctetList)

#define SNMP_OCTET_STRING_LENGTH(x) (x->i4_Length)

#define SNMP_OID_LIST_ELEMENT(x,y) (x->pu4_OidList[y])

#define SNMP_OID_LIST(x) (x->pu4_OidList)

#define SNMP_OID_LENGTH(x) (x->u4_Length)

#endif /* _LDPLWINC_H_ */

/*----------------------------------------------------------------------------*/
/*                         End of file ldplwinc.h                             */
/*----------------------------------------------------------------------------*/
