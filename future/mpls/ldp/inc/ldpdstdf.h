/********************************************************************
 *                                                                  *
 * $RCSfile: ldpdstdf.h,v $                                         
 *                                                                  *
 * $Date: 2007/02/13 13:54:01 $                                     *
 *                                                                  *
 * $Revision: 1.3 $                                             *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldptdfs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : CRLDP-DIFFSERV
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the structure type
 *                             definitions declared and used in the
 *                             CRLDP-DIFFSERV Module.
 *---------------------------------------------------------------------------*/
#ifndef _LDPDSTDFS_H 
#define _LDPDSTDFS_H

typedef struct _DiffServElspTlvPrefix 
{
    UINT1               u1Tbit;
    UINT1               u1Reserved;
    UINT1               u1Reserved1;
    UINT1               u1NoOfMapEntries;    
}
tDiffServElspTlvPrefix;

typedef struct _DiffServElspMapEntry 
{
     UINT1               u1Reserved;
     UINT1               u1Exp;
     UINT2               u2PhbId;
}
tDiffServElspMapEntry;

typedef struct _DiffServLlspTlv 
{
     UINT1               u1Tbit;
     UINT1               u1Reserved;
     UINT2               u2Psc;
}
tDiffServLlspTlv;


typedef struct  _PerOATrfcProfileEntry 
{
      UINT4                    u4RsvTrfcParmPerOAGrpId;
      UINT2                    u2Resvd;
      UINT1                    u1PscDscp;
      UINT1                    u1IsRsrcAvail;  /*Initialize it to false*/
      struct _TeTrfcParams     *pPerOATrfcParms;
}
tPerOATrfcProfileEntry;

typedef struct _DiffServTPTlvPrefix 
{
      UINT2               u2Reserved;
      UINT2               u2NoOfElspTPEntries;
}
tDiffServElspTPTlvPrefix;

typedef struct _LdpDiffServElspTPEntry
{
      UINT2               u2Reserved;
      UINT2               u2Psc;
      UINT1               u1Flags;
      UINT1               u1Frequency;
      UINT1               u1Reserved;
      UINT1               u1Weight;
      UINT4               u4PeakDataRate;
      UINT4               u4PeakBurstSize;
      UINT4               u4CommittedDataRate;
      UINT4               u4CommittedBurstSize;
      UINT4               u4ExcessBurstSize;
}
tLdpDiffServElspTPEntry;

typedef struct _DiffServClassTypeTlv
{
      UINT2               u2Reserved;
      UINT2               u2ClassType;
}
tDiffServClassTypeTlv;

#endif
/*---------------------------------------------------------------------------*/
/*                        End of file ldpdstdf.h                              */
/*---------------------------------------------------------------------------*/
