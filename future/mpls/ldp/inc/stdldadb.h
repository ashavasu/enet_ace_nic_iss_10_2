/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdldadb.h,v 1.3 2008/08/20 15:14:46 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDLDADB_H
#define _STDLDADB_H

UINT1 MplsLdpEntityAtmTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsLdpEntityAtmLRTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};
UINT1 MplsLdpAtmSessionTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_INTEGER32};

UINT4 stdlda [] ={1,3,6,1,2,1,10,166,5};
tSNMP_OID_TYPE stdldaOID = {9, stdlda};


UINT4 MplsLdpEntityAtmIfIndexOrZero [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,1};
UINT4 MplsLdpEntityAtmMergeCap [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,2};
UINT4 MplsLdpEntityAtmLRComponents [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,3};
UINT4 MplsLdpEntityAtmVcDirectionality [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,4};
UINT4 MplsLdpEntityAtmLsrConnectivity [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,5};
UINT4 MplsLdpEntityAtmDefaultControlVpi [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,6};
UINT4 MplsLdpEntityAtmDefaultControlVci [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,7};
UINT4 MplsLdpEntityAtmUnlabTrafVpi [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,8};
UINT4 MplsLdpEntityAtmUnlabTrafVci [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,9};
UINT4 MplsLdpEntityAtmStorageType [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,10};
UINT4 MplsLdpEntityAtmRowStatus [ ] ={1,3,6,1,2,1,10,166,5,1,1,1,1,11};
UINT4 MplsLdpEntityAtmLRMinVpi [ ] ={1,3,6,1,2,1,10,166,5,1,1,2,1,1};
UINT4 MplsLdpEntityAtmLRMinVci [ ] ={1,3,6,1,2,1,10,166,5,1,1,2,1,2};
UINT4 MplsLdpEntityAtmLRMaxVpi [ ] ={1,3,6,1,2,1,10,166,5,1,1,2,1,3};
UINT4 MplsLdpEntityAtmLRMaxVci [ ] ={1,3,6,1,2,1,10,166,5,1,1,2,1,4};
UINT4 MplsLdpEntityAtmLRStorageType [ ] ={1,3,6,1,2,1,10,166,5,1,1,2,1,5};
UINT4 MplsLdpEntityAtmLRRowStatus [ ] ={1,3,6,1,2,1,10,166,5,1,1,2,1,6};
UINT4 MplsLdpSessionAtmLRLowerBoundVpi [ ] ={1,3,6,1,2,1,10,166,5,1,2,1,1,1};
UINT4 MplsLdpSessionAtmLRLowerBoundVci [ ] ={1,3,6,1,2,1,10,166,5,1,2,1,1,2};
UINT4 MplsLdpSessionAtmLRUpperBoundVpi [ ] ={1,3,6,1,2,1,10,166,5,1,2,1,1,3};
UINT4 MplsLdpSessionAtmLRUpperBoundVci [ ] ={1,3,6,1,2,1,10,166,5,1,2,1,1,4};


tMbDbEntry stdldaMibEntry[]= {

{{14,MplsLdpEntityAtmIfIndexOrZero}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmIfIndexOrZeroGet, MplsLdpEntityAtmIfIndexOrZeroSet, MplsLdpEntityAtmIfIndexOrZeroTest, MplsLdpEntityAtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityAtmMergeCap}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmMergeCapGet, MplsLdpEntityAtmMergeCapSet, MplsLdpEntityAtmMergeCapTest, MplsLdpEntityAtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityAtmLRComponents}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmLRComponentsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsLdpEntityAtmTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityAtmVcDirectionality}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmVcDirectionalityGet, MplsLdpEntityAtmVcDirectionalitySet, MplsLdpEntityAtmVcDirectionalityTest, MplsLdpEntityAtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityAtmLsrConnectivity}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmLsrConnectivityGet, MplsLdpEntityAtmLsrConnectivitySet, MplsLdpEntityAtmLsrConnectivityTest, MplsLdpEntityAtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmTableINDEX, 2, 0, 0, "1"},

{{14,MplsLdpEntityAtmDefaultControlVpi}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmDefaultControlVpiGet, MplsLdpEntityAtmDefaultControlVpiSet, MplsLdpEntityAtmDefaultControlVpiTest, MplsLdpEntityAtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmTableINDEX, 2, 0, 0, "0"},

{{14,MplsLdpEntityAtmDefaultControlVci}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmDefaultControlVciGet, MplsLdpEntityAtmDefaultControlVciSet, MplsLdpEntityAtmDefaultControlVciTest, MplsLdpEntityAtmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsLdpEntityAtmTableINDEX, 2, 0, 0, "32"},

{{14,MplsLdpEntityAtmUnlabTrafVpi}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmUnlabTrafVpiGet, MplsLdpEntityAtmUnlabTrafVpiSet, MplsLdpEntityAtmUnlabTrafVpiTest, MplsLdpEntityAtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmTableINDEX, 2, 0, 0, "0"},

{{14,MplsLdpEntityAtmUnlabTrafVci}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmUnlabTrafVciGet, MplsLdpEntityAtmUnlabTrafVciSet, MplsLdpEntityAtmUnlabTrafVciTest, MplsLdpEntityAtmTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsLdpEntityAtmTableINDEX, 2, 0, 0, "32"},

{{14,MplsLdpEntityAtmStorageType}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmStorageTypeGet, MplsLdpEntityAtmStorageTypeSet, MplsLdpEntityAtmStorageTypeTest, MplsLdpEntityAtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmTableINDEX, 2, 0, 0, "3"},

{{14,MplsLdpEntityAtmRowStatus}, GetNextIndexMplsLdpEntityAtmTable, MplsLdpEntityAtmRowStatusGet, MplsLdpEntityAtmRowStatusSet, MplsLdpEntityAtmRowStatusTest, MplsLdpEntityAtmTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmTableINDEX, 2, 0, 1, NULL},

{{14,MplsLdpEntityAtmLRMinVpi}, GetNextIndexMplsLdpEntityAtmLRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsLdpEntityAtmLRTableINDEX, 4, 0, 0, NULL},

{{14,MplsLdpEntityAtmLRMinVci}, GetNextIndexMplsLdpEntityAtmLRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, MplsLdpEntityAtmLRTableINDEX, 4, 0, 0, NULL},

{{14,MplsLdpEntityAtmLRMaxVpi}, GetNextIndexMplsLdpEntityAtmLRTable, MplsLdpEntityAtmLRMaxVpiGet, MplsLdpEntityAtmLRMaxVpiSet, MplsLdpEntityAtmLRMaxVpiTest, MplsLdpEntityAtmLRTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmLRTableINDEX, 4, 0, 0, NULL},

{{14,MplsLdpEntityAtmLRMaxVci}, GetNextIndexMplsLdpEntityAtmLRTable, MplsLdpEntityAtmLRMaxVciGet, MplsLdpEntityAtmLRMaxVciSet, MplsLdpEntityAtmLRMaxVciTest, MplsLdpEntityAtmLRTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsLdpEntityAtmLRTableINDEX, 4, 0, 0, NULL},

{{14,MplsLdpEntityAtmLRStorageType}, GetNextIndexMplsLdpEntityAtmLRTable, MplsLdpEntityAtmLRStorageTypeGet, MplsLdpEntityAtmLRStorageTypeSet, MplsLdpEntityAtmLRStorageTypeTest, MplsLdpEntityAtmLRTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmLRTableINDEX, 4, 0, 0, "3"},

{{14,MplsLdpEntityAtmLRRowStatus}, GetNextIndexMplsLdpEntityAtmLRTable, MplsLdpEntityAtmLRRowStatusGet, MplsLdpEntityAtmLRRowStatusSet, MplsLdpEntityAtmLRRowStatusTest, MplsLdpEntityAtmLRTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityAtmLRTableINDEX, 4, 0, 1, NULL},

{{14,MplsLdpSessionAtmLRLowerBoundVpi}, GetNextIndexMplsLdpAtmSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsLdpAtmSessionTableINDEX, 5, 0, 0, NULL},

{{14,MplsLdpSessionAtmLRLowerBoundVci}, GetNextIndexMplsLdpAtmSessionTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, MplsLdpAtmSessionTableINDEX, 5, 0, 0, NULL},

{{14,MplsLdpSessionAtmLRUpperBoundVpi}, GetNextIndexMplsLdpAtmSessionTable, MplsLdpSessionAtmLRUpperBoundVpiGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsLdpAtmSessionTableINDEX, 5, 0, 0, NULL},

{{14,MplsLdpSessionAtmLRUpperBoundVci}, GetNextIndexMplsLdpAtmSessionTable, MplsLdpSessionAtmLRUpperBoundVciGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, MplsLdpAtmSessionTableINDEX, 5, 0, 0, NULL},
};
tMibData stdldaEntry = { 21, stdldaMibEntry };
#endif /* _STDLDADB_H */

