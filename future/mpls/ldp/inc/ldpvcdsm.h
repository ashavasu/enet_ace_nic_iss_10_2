
#ifndef _LDP_VCDS_H
#define _LDP_VCDS_H

/* State machine array */

tMrgDnFsmFuncPtr
    gLdpMrgDnFsm[LDP_MAX_MRG_DNSTR_STATES][LDP_MAX_MRG_DNSTR_EVENTS] =
{

  /* Idle State */
  {
     LdpMrgDnIdlIntAddUpstr,    /* Internal Add Upstream    event */
     LdpMrgDnInvStateEvt,       /* Label Mapping            event */
     LdpMrgDnInvStateEvt,       /* Not Applicable           event */
     LdpMrgDnInvStateEvt,       /* Label WithDraw           event */
     LdpMrgDnInvStateEvt,       /* Not Applicable           event */
     LdpMrgDnInvStateEvt,       /* LDP Downstream NAK       event */
     LdpMrgDnInvStateEvt,       /* Internal Delete Upstream event */
     LdpMrgDnInvStateEvt,       /* DownStream Lost          event */

  }
  ,
  /* Downstream Response Awaited state */
  { 
     LdpMrgDnRspAwtIntAddUpstr, /* Internal Add Upstream    event */
     LdpMrgDnRspAwtLblMap,      /* Label Mapping            event */
     LdpMrgDnInvStateEvt,       /* Not Applicable           event */
     LdpMrgDnRspAwtLblWdraw,    /* Label WithDraw           event */
     LdpMrgDnInvStateEvt,       /* Not Applicable           event */
     LdpMrgDnRspAwtIntDstrNak,  /* LDP Downstream NAK       event */
     LdpMrgDnRspAwtIntDelUpstr, /* Internal Delete Upstream event */
     LdpMrgDnRspAwtDstrLost     /* Downstream Lost          event */

  }
  ,
  /* Downstream Established state */
  { 
     LdpMrgDnEstIntAddUpstr,    /* Internal Add Upstream    event */
     LdpMrgDnEstLblMap,         /* Label Mapping            event */
     LdpMrgDnInvStateEvt,       /* Not Applicable           event */
     LdpMrgDnEstLblWdraw,       /* Label WithDraw           event */
     LdpMrgDnInvStateEvt,       /* Not Applicable           event */
     LdpMrgDnEstIntDstrNak,     /* LDP Downstream NAK       event */
     LdpMrgDnEstIntDelUpstr,    /* Internal Delete Upstream event */
     LdpMrgDnEstIntDstrLost     /* DownStream Lost          event */

  }

};
CONST CHR1  *au1LdpVcDnStates[LDP_MAX_MRG_DNSTR_STATES] = {
                                                          "IDLE",
                                                          "RESP AWAITED", 
                                                          "ESTABLISHED" 
                                               };
 
CONST CHR1 *au1LdpVcDnEvents[LDP_MAX_MRG_DNSTR_EVENTS] = {
                                                         "INT ADD UPSTR",
                                                         "INT DEL UPSTR",
                                                         "LDP MAPPING",
                                                         "LDP WITHDRAW",
                                                         "LDP NAK",
                                                         "DSTR LOST"
                                             }; 


#endif /* _LDP_VCDS_H */
