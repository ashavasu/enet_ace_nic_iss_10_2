/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpduusm.h,v 1.6 2014/08/25 11:58:49 siva Exp $
 *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : duupsm.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains definition of global variable
 *                             associated with the DU Upstream LSP State
 *                             machine. The SEM is initialised in this file.
 *---------------------------------------------------------------------------*/

#ifndef _DUUPSM_H
#define _DUUPSM_H

tDuUpFsmFuncPtr
    gLdpDuUpFsm[LDP_MAX_DU_UP_STATES][LDP_MAX_DU_UP_EVENTS] =
{
    /* IDLE state */
    {
        LdpDuUpIdLblReq,        /* Label Req Event */ 
        LdpDuUpIdIntDnMap,      /* Internal Label Mapping Event */
        LdpDuUpInvStEvt,        /* LDP Release Event */
        LdpDuUpIdIntWR,         /* Internal WithDraw Event */
        LdpDuUpIdLblAbrtReq,    /* LDP Up abort Event */
        LdpDuUpInvStEvt,          /* Downstream NAK event */
        LdpDuUpIdUpLost,         /* Upstream Lost Event */
        LdpDuUpInvStEvt,          /* Downstream Lost event */
        LdpDuUpInvStEvt,          /* Internal setup event */
        LdpDuUpInvStEvt,          /* Internal Destroy event */
        LdpDuUpInvStEvt,          /* Internal X-connect event */
        LdpDuUpInvStEvt,          /* Next Hop change event */
        LdpDuUpInvStEvt,          /* PreEmption Event */
        LdpDuUpIdDelFec,        /* Delete FEC Event */
        LdpDuUpInvStEvt,       /* Resources Available Event */
#ifdef LDP_GR_WANTED
        LdpDuUpInvStEvt        /* Internal refresh event */
#endif
 
    }
    ,
    /* Introduced Invalid state for Interoperation between DoD and DU */
    {
        LdpDuUpInvStEvt,       
        LdpDuUpInvStEvt,     
        LdpDuUpInvStEvt,       
        LdpDuUpInvStEvt,       
        LdpDuUpInvStEvt,        
        LdpDuUpInvStEvt,        
        LdpDuUpInvStEvt,         
        LdpDuUpInvStEvt,         
        LdpDuUpInvStEvt,         
        LdpDuUpInvStEvt,         
        LdpDuUpInvStEvt,         
        LdpDuUpInvStEvt,     
        LdpDuUpInvStEvt,      
        LdpDuUpInvStEvt,      
        LdpDuUpInvStEvt,
#ifdef LDP_GR_WANTED
        LdpDuUpInvStEvt     
#endif  
    }
    ,
    /* ESTABLISHED state */
    {
        LdpDuUpIdLblReq,        /* Label Req Event */ /* As per RFC 3036, section A.1.1
                                If an LSR receives label req event in DU mode, it has to
                                process that event even it has sent label mapping message
                                to upstream (established state) already */
        LdpDuUpEstIntDnMap,      /* Internal Label Mapping Event */
        LdpDuUpEstLdpRel,        /* LDP Release Event */
        LdpDuUpEstIntWR,         /* Internal WithDraw Event */
        LdpDuUpIdLblAbrtReq,         /* LDP Up abort Event */
        LdpDuUpInvStEvt,         /* Downstream NAK event */
        LdpDuUpEstUpLost,        /* Upstream Lost Event */
        LdpDuUpInvStEvt,         /* Downstream Lost event */
        LdpDuUpInvStEvt,         /* Internal setup event */
        LdpDuUpInvStEvt,         /* Internal Destroy event */
        LdpDuUpInvStEvt,         /* Internal X-connect event */
        LdpDuUpInvStEvt,         /* Next Hop change event */
        LdpDuUpInvStEvt,         /* PreEmption Event */
        LdpDuUpEstDelFec,       /* Delete FEC Event */
        LdpDuUpInvStEvt,       /* Resources Available Event */
#ifdef LDP_GR_WANTED
        LdpDuUpEstIntRefreshEvt        /* Internal refresh event */
#endif
    }
    ,
    /* RELEASE_AWAITED state */
    {
        LdpDuUpInvStEvt,        /* Label Req Event */ 
        LdpDuUpInvStEvt,      /* Internal Label Mapping Event */
        LdpDuUpRelLdpRel,        /* LDP Release Event */
        LdpDuUpInvStEvt,         /* Internal WithDraw Event */
        LdpDuUpInvStEvt,           /* LDP Up abort Event */
        LdpDuUpInvStEvt,           /* Downstream NAK event */
        LdpDuUpRelUpLost,        /* Upstream Lost Event */
        LdpDuUpInvStEvt,           /* Downstream Lost event */
        LdpDuUpInvStEvt,           /* Internal setup event */
        LdpDuUpInvStEvt,           /* Internal Destroy event */
        LdpDuUpInvStEvt,           /* Internal X-connect event */
        LdpDuUpInvStEvt,           /* Next Hop change event */
        LdpDuUpInvStEvt,           /* PreEmption Event */
        LdpDuUpInvStEvt,        /* Delete FEC Event */
        LdpDuUpInvStEvt,       /* Resources Available Event */
#ifdef LDP_GR_WANTED
        LdpDuUpInvStEvt       /* Internal refresh event */
#endif
    }
    ,
    /* RESOURCES_AWAITED state */
    {
        LdpDuUpInvStEvt,        /* Label Req Event */ 
        LdpDuUpInvStEvt,      /* Internal Label Mapping Event */
        LdpDuUpInvStEvt,        /* LDP Release Event */
        LdpDuUpRsrcIntWR,         /* Internal WithDraw Event */
        LdpDuUpInvStEvt,            /* LDP Up abort Event */
        LdpDuUpInvStEvt,            /* Downstream NAK event */
        LdpDuUpRsrcUpLost,        /* Upstream Lost Event */
        LdpDuUpInvStEvt,            /* Downstream Lost event */
        LdpDuUpInvStEvt,            /* Internal setup event */
        LdpDuUpInvStEvt,            /* Internal Destroy event */
        LdpDuUpInvStEvt,            /* Internal X-connect event */
        LdpDuUpInvStEvt,            /* Next Hop change event */
        LdpDuUpInvStEvt,            /* PreEmption Event */
        LdpDuUpRsrcDelFec,        /* Delete FEC Event */
        LdpDuUpRsrcRsrcAvl,       /* Resources Available Event */
#ifdef LDP_GR_WANTED
        LdpDuUpInvStEvt           /* Internal refresh event */
#endif
    }
};

CONST CHR1 *au1LdpDuUpStates[LDP_MAX_DU_UP_STATES] = {
                                                       "idle",
                                                       "invalid-state",
                                                       "established",
                                                       "release-awaited",
                                                       "resource-awaited"   
                                           };


CONST CHR1 *au1LdpDuUpEvents[LDP_MAX_DU_UP_EVENTS] = {
                                                     "label-req",
                                                     "intl-downstrm-map",
                                                     "ldp-release",
                                                     "internal-withdraw",
                                                     "up-abort",
                                                     "downstream-nak",
                                                     "upstream-lost",
                                                     "downstream-lost",
                                                     "internal-setup",
                                                     "internal-dest",
                                                     "internal-xcnt",
                                                     "next-hopchg",
                                                     "preemp-event",
                                                     "delete-fec",
                                                     "resource-available",
#ifdef LDP_GR_WANTED
                                                     "intl-refresh-event"
#endif
                                        };


#endif /*_DUUPSM_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file duupsm.h                             */
/*---------------------------------------------------------------------------*/
