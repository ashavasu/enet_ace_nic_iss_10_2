
/********************************************************************
 *                                                                  *
 * $RCSfile: ldpadsm1.h,v $
 *                                                                  *
 * $Date: 2007/02/01 14:57:00 $                                     *
 *                                                                  *
 * $Revision: 1.3 $                                                 *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpadsm1.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains definition of global variable
 *                             associated with the non vc-merge LSP State
 *                             machine. The SEM is initialised in this file.
 *---------------------------------------------------------------------------*/

#ifndef _LDP_ADSM1_H
#define _LDP_ADSM1_H

tNonMrgFsmFuncPtr
    gLdpNonMrgFsm[LDP_MAX_NON_MRG_STATES][LDP_MAX_NON_MRG_EVENTS] =
{
    /* IDLE state */
    {
        LdpNonMrgIdRequest,        /* Label Request Event */
            LdpNonMrgIdLdpMap,    /* Label Mapping Event */
            LdpNonMrgIdRel,        /* Label Release Event */
            LdpNonMrgInvStateEvent,    /* Label Withdraw Event */
            LdpNonMrgInvStateEvent,    /* Label Abort Event */
            LdpNonMrgIdDnNak,    /* Downstream Nak Event */
            LdpNonMrgInvStateEvent,    /* Upstream Lost Event */
            LdpNonMrgInvStateEvent,    /* Downstream Lost Event */
            LdpNonMrgIdIntSetup,    /* Internal Setup Event */
            LdpNonMrgInvStateEvent,    /* Internal Destroy Event */
            LdpNonMrgInvStateEvent,    /* Internal X-connect Event */
            LdpNonMrgInvStateEvent,    /* Internal NewNextHop Evt */
            LdpNonMrgInvStateEvent    /* PreEmption Event */
    }
    ,
        /* RESPONSE_AWAITED state */
    {
        LdpNonMrgInvStateEvent,    /*Label Request Event */
            LdpNonMrgRspMap,    /* Label Mapping Event */
            LdpNonMrgRspRel,    /* Label Release Event */
            LdpNonMrgInvStateEvent,    /* Label Withdraw Event */
            LdpNonMrgRspUpAbort,    /* Label Abort Event */
            LdpNonMrgRspDnNak,    /* Downstream Nak Event */
            LdpNonMrgRspUpLost,    /* Upstream Lost Event */
            LdpNonMrgRspDnLost,    /* Downstream Lost Event */
            LdpNonMrgInvStateEvent,    /* Internal Setup Event */
            LdpNonMrgRspIntDestroy,    /* Internal Destroy Event */
            LdpNonMrgInvStateEvent,    /* Internal X-connect Evt */
            LdpNonMrgRspIntNewNH,    /* Internal NewNextHop Evt */
            LdpNonMrgRspPreEmpTnl    /* PreEmption Event */
    }
    ,
        /* ESTABLISHED state */
    {
        LdpNonMrgInvStateEvent,    /* Label Request Event */
            LdpNonMrgEstLdpMap,    /* Label Mapping Event */
            LdpNonMrgEstLdpRel,    /* Label Release Event */
            LdpNonMrgEstLdpWth,    /* Label Withdraw Event */
            LdpNonMrgEstUpAbort,    /* Label Abort Event */
            LdpNonMrgInvStateEvent,    /* Downstream Nak Event */
            LdpNonMrgEstUpLost,    /* Upstream Lost Event */
            LdpNonMrgEstDnLost,    /* Downstream Lost Event */
            LdpNonMrgInvStateEvent,    /* Internal Setup Event */
            LdpNonMrgEstIntDestroy,    /* Internal Destroy Event */
            LdpNonMrgEstIntXCon,    /* Internal X-connect Event */
            LdpNonMrgEstIntNewNH,    /* Internal NewNextHop Evt */
            LdpNonMrgEstPreEmpTnl    /* PreEmption Event */
    }
    ,
        /* RELEASE_AWAITED state */
    {
        LdpNonMrgInvStateEvent,    /* Label Request Event */
            LdpNonMrgRelLdpMap,    /* Label Mapping Event */
            LdpNonMrgRelLdpRel,    /* Label Release Event */
            LdpNonMrgRelLdpWth,    /* Label Withdraw Event */
            LdpNonMrgRelUpAbort,    /* Label Abort Event */
            LdpNonMrgInvStateEvent,    /* Downstream Nak Event */
            LdpNonMrgRelUpLost,    /* Upstream Lost Event */
            LdpNonMrgInvStateEvent,    /* Downstream Lost Event */
            LdpNonMrgInvStateEvent,    /* Internal Setup Event */
            LdpNonMrgInvStateEvent,    /* Internal Destroy Event */
            LdpNonMrgInvStateEvent,    /* Internal X-connect Event */
            LdpNonMrgInvStateEvent,    /* Internal NewNextHop Evt */
            LdpNonMrgInvStateEvent    /* PreEmption Event */
    }
};

#endif /*_LDP_ADSM1_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpadsm1.h                             */
/*---------------------------------------------------------------------------*/
