
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
 * $Id: ldpdbg.h,v 1.10 2014/11/15 12:29:00 siva Exp $
*
*******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpdbg.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains all the data structures and
 *    definitions required for Debugging/Dumping in LDP.
 *----------------------------------------------------------------------------*/

#ifndef _LDP_DBG_H
#define _LDP_DBG_H
#include "utltrc.h"

/* Macros used in LDP Debugging */
#define       LDP_DBG_FLAG      gu4LdpDbg
#define     LDP_LVL_FLAG         gu4LdpLvl

#define     LDP_DEF_DBG_FLAG     0x00000000
/* Values assigned to the sub-modules of LDP, for Logging purpose */
#define     MAIN_DEBUG           0x10000000
#define     INTERFACE_DEBUG      0x20000000
#define     ADVT_DEBUG           0x40000000
#define     SESSION_DEBUG        0x00800000
#define     PRCS_DEBUG           0x01000000
#define     NOTIFICATION_DEBUG   0x02000000
#define     LDP_DS_DEBUG         0x04000000
#define     PREEMPTION_DEBUG     0x08000000
#define     REROUTETIME_DBG      0x00100000
#define     GRACEFUL_DEBUG       0x00200000
#define     HA_DEBUG             0x00400000

/* Log Types defined in LDP */
#define     LDP_DBG_MEM     0x00000001
#define     LDP_DBG_TIMER   0x00000002
#define     LDP_DBG_SEM     0x00000004
#define     LDP_DBG_RX      0x00000008
#define     LDP_DBG_TX      0x00000010
#define     LDP_DBG_PRCS    0x00000020
#define     LDP_DBG_SNMP    0x00000040
#define     LDP_DBG_TCP     0x00000080
#define     LDP_DBG_UDP     0x00000100
#define     LDP_DBG_MISC    0x00000200
#define     LDP_DBG_RRTI    0x00000400

 /* values used in LDP_DBG depending on the categories under which 
  * the log message falls. 
  */

/* Main Module : */
#define     LDP_MAIN_MEM    0x10000001
#define     LDP_MAIN_MISC   0x10000002
#define     LDP_MAIN_ALL    0x100000ff

/* Interface Module : */
#define     LDP_IF_MEM      0x20000001
#define     LDP_IF_TMR      0x20000002
#define     LDP_IF_RX       0x20000008
#define     LDP_IF_TX       0x20000010
#define     LDP_IF_PRCS     0x20000020
#define     LDP_IF_SNMP     0x20000040
#define     LDP_IF_TCP      0x20000080
#define     LDP_IF_UDP      0x20000100
#define     LDP_IF_MISC     0x20000200
#define     LDP_IF_ALL      0x20000fff

/* Advertisement Module :*/
#define     LDP_ADVT_MEM    0x40000001
#define     LDP_ADVT_TMR    0x40000002
#define     LDP_ADVT_SEM    0x40000004
#define     LDP_ADVT_RX     0x40000008
#define     LDP_ADVT_TX     0x40000010
#define     LDP_ADVT_PRCS   0x40000020
#define     LDP_ADVT_MISC   0x40000200
#define     LDP_ADVT_ALL    0x40000fff

/* Session Module: */
#define     LDP_SSN_MEM     0x00800001
#define     LDP_SSN_TMR     0x00800002
#define     LDP_SSN_SEM     0x00800004
#define     LDP_SSN_RX      0x00800008
#define     LDP_SSN_TX      0x00800010
#define     LDP_SSN_PRCS    0x00800020
#define     LDP_SSN_MISC    0x00800200
#define     LDP_SSN_ALL     0x00800fff

/* PDU Processing  Module: */
#define     LDP_PRCS_MEM    0x01000001
#define     LDP_PRCS_SEM    0x01000004
#define     LDP_PRCS_RX     0x01000008
#define     LDP_PRCS_TX     0x01000010
#define     LDP_PRCS_PRCS   0x01000020
#define     LDP_PRCS_MISC   0x01000200
#define     LDP_PRCS_ALL    0x01000fff

/* Notification  Module: */
#define     LDP_NOTIF_MEM   0x02000001
#define     LDP_NOTIF_TMR   0x02000002
#define     LDP_NOTIF_RX    0x02000008
#define     LDP_NOTIF_TX    0x02000010
#define     LDP_NOTIF_PRCS  0x02000020
#define     LDP_NOTIF_MISC  0x02000200
#define     LDP_NOTIF_ALL   0x020000ff
#define     LDP_PDU_DUMP    0x04000001

/* PreEmption Module */
#define     LDP_PREMPT_DUMP    0x08000001

/* PreEmption Module */
#define     LDP_RERTIM_DUMP    0x00100401

/* L2vpn Module */
#define LDP_APP_MEM          0x00200001
#define LDP_APP_PRCS         0x00200020

/* DiffServ Module */
#define LDP_DS_MEM  (LDP_DS_DEBUG | LDP_DBG_MEM)
#define LDP_DS_RX   (LDP_DS_DEBUG | LDP_DBG_RX)
#define LDP_DS_TX   (LDP_DS_DEBUG | LDP_DBG_TX)
#define LDP_DS_PRCS (LDP_DS_DEBUG | LDP_DBG_PRCS)
#define LDP_DS_SNMP (LDP_DS_DEBUG | LDP_DBG_SNMP)
#define LDP_DS_MISC (LDP_DS_DEBUG | LDP_DBG_MISC)


/* Macros defined for accessing the above global flags */
#define LDP_DUMP_TYPE    gu4LdpDumpType
#define LDP_DUMP_DIR     gu4LdpDumpDir

#define   LDP_DUMP_NONE        0x00000000
#define   LDP_DUMP_HELLO       0x10000000
#define   LDP_DUMP_INIT        0x20000000
#define   LDP_DUMP_KEEPALIVE   0x40000000

#define   LDP_DUMP_ADDRW       0x01000000
#define   LDP_DUMP_LDP_RQ      0x02000000
#define   LDP_DUMP_LDP_MAP     0x04000000
#define   LDP_DUMP_LDP_ABORT   0x08000000
#define   LDP_DUMP_LDP_WDRAW   0x00100000
#define   LDP_DUMP_LDP_REL     0x00200000
#define   LDP_DUMP_LDP_NOTIF   0x00400000
#define   LDP_DUMP_ADDR        0x00800000
#define   LDP_DUMP_ALL         0x7ff00000

#define   LDP_DUMP_DIRN_NONE   0x00000000
#define   LDP_DUMP_DIR_IN      0x00000001
#define   DUMP_DIR_OUT         0x00000002
#define   DUMP_DIR_INOUT       0x00000003

#define LDP_DBG(u4Value, pu1Format)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
                  UtlTrcLog  (LDP_DBG_FLAG, u4Value, "LDP", pu1Format)

#define LDP_DBG1(u4Value, pu1Format, Arg1)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "LDP", pu1Format, Arg1)

#define LDP_DBG2(u4Value, pu1Format, Arg1, Arg2)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "LDP", pu1Format, Arg1, Arg2)

#define LDP_DBG3(u4Value, pu1Format, Arg1, Arg2, Arg3)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "LDP", pu1Format, Arg1, Arg2, Arg3)

#define LDP_DBG4(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "LDP", pu1Format, Arg1, Arg2, Arg3, Arg4)

#define LDP_DBG5(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)   \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "LDP", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)

#define LDP_DBG6(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "LDP", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#define LDP_DBG8(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "LDP", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8)


#define PM_LDP_DBG(u4Value, pu1Format) \
            if(u4Value == (u4Value & LDP_DBG_FLAG)) \
                 UtlTrcLog(LDP_DBG_FLAG, u4Value, "PM", pu1Format)

/* STAT DBG */

#define LDP_DBG_STAT(u4Value, pu1Format)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
                  UtlTrcLog  (LDP_DBG_FLAG, u4Value, "", pu1Format)

#define LDP_DBG_STAT1(u4Value, pu1Format, Arg1)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "", pu1Format, Arg1)

#define LDP_DBG_STAT2(u4Value, pu1Format, Arg1, Arg2)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "", pu1Format, Arg1, Arg2)

#define LDP_DBG_STAT3(u4Value, pu1Format, Arg1, Arg2, Arg3)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "", pu1Format, Arg1, Arg2, Arg3)

#define LDP_DBG_STAT4(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "", pu1Format, Arg1, Arg2, Arg3, Arg4)

#define LDP_DBG_STAT5(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)   \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)

#define LDP_DBG_STAT6(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)     \
           if(u4Value == (u4Value & LDP_DBG_FLAG)) \
             UtlTrcLog  (LDP_DBG_FLAG, u4Value, "", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)
#endif /*_LDP_DBG_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpdbg.h                               */
/*---------------------------------------------------------------------------*/
