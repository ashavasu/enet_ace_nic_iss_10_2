/********************************************************************
 *                                                                  *
 * $RCSfile: ldpdste.h,v $
 *                                                                  *
 * $Date: 2007/02/01 14:57:00 $                                     *
 *                                                                  *
 * $Revision: 1.3 $                                             *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpdste.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains macros that map on to the TE related
 *                             constants or function name or a macro in 
 *                             TE module.
 *---------------------------------------------------------------------------*/
#ifndef _LDP_DSTE_H
#define _LDP_DSTE_H

#define LDP_TE_MPLS_DIFFSERV_TNL_INFO(pTeTnlInfo)\
           TE_DS_TNL_INFO(pTeTnlInfo)

/*tMplsDiffServElspList macros*/

#define LDP_TE_DS_ELSPINFOLIST_INDEX(pMplsDiffServElspListPtr)\
                  TE_DS_ELSP_INFO_LIST_INDEX(pMplsDiffServElspListPtr)

#define LDP_TE_DS_ELSPINFOLIST_TNL_COUNT(pMplsDiffServElspListPtr)\
                  TE_DS_ELSP_INFO_LIST_TNL_COUNT(pMplsDiffServElspListPtr)

#define LDP_TE_DS_ELSPINFOLIST(pMplsDiffServElspListPtr)\
                  TE_DS_ELSP_INFO_LIST(pMplsDiffServElspListPtr)

/*tMplsDiffServElspInfo macros*/
#define LDP_TE_DS_ELSPINFO_NEXT(pElspInfoNode)\
                  TE_DS_ELSP_INFO_NEXT(pElspInfoNode) 

#define LDP_TE_DS_ELSPINFO_INDEX(pElspInfoNode)\
                  TE_DS_ELSP_INFO_INDEX(pElspInfoNode) 

#define LDP_TE_DS_ELSPINFO_PHB_DSCP(pElspInfoNode)\
               TE_DS_ELSP_INFO_PHB_DSCP(pElspInfoNode) 

#define LDP_TE_DS_ELSPINFO_ROW_STATUS(pElspInfoNode)\
                  TE_DS_ELSP_INFO_ROW_STATUS(pElspInfoNode)

#define LDP_TE_DS_ELSPINFO_STORAGE_TYPE(pElspInfoNode)\
                  TE_DS_ELSP_INFO_STORAGE_TYPE(pElspInfoNode) 

#define LDP_TE_DS_ELSPINFO_TRFC_RSRC_INDEX(pElspInfoNode)\
             TE_DS_ELSP_INFO_RSRC_INDEX(pElspInfoNode) 

#define LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS(pElspInfoNode)\
             TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pElspInfoNode) 

/*tMplsDiffServTnlInfo macros */

#define LDP_TE_DS_CLASS_TYPE(pDiffServTnlInfo)\
                        TE_DS_TNL_CLASS_TYPE(pDiffServTnlInfo) 

#define LDP_TE_DS_PARAMS_ROWSTATUS(pDiffServTnlInfo)\
             TE_DS_TNL_ROW_STATUS(pDiffServTnlInfo) 

#define LDP_TE_DS_PARAMS_STORAGETYPE(pDiffServTnlInfo)\
              TE_DS_TNL_STORAGE_TYPE(pDiffServTnlInfo)  

#define LDP_TE_DS_LSP_SERVICE_TYPE(pDiffServTnlInfo)\
             TE_DS_TNL_SERVICE_TYPE(pDiffServTnlInfo)

#define LDP_TE_DS_PARAMS_ELSPLISTINDEX(pDiffServTnlInfo)\
             TE_DS_TNL_ELSP_LIST_INDEX(pDiffServTnlInfo)

#define LDP_TE_DS_PARAM_FLAG(pDiffServTnlInfo)\
             TE_DS_TNL_CFG_FLAG(pDiffServTnlInfo) 

#define LDP_TE_DS_ELSP_TYPE(pDiffServTnlInfo)\
             TE_DS_TNL_ELSP_TYPE(pDiffServTnlInfo) 

#define LDP_TE_DS_LLSP_DSCP(pDiffServTnlInfo)\
              TE_DS_TNL_LLSP_PSC_DSCP(pDiffServTnlInfo)

#define LDP_TE_DS_PARAMS_ELSPLIST_PTR(pDiffServTnlInfo)\
             TE_DS_TNL_ELSP_INFO_LIST_INFO(pDiffServTnlInfo)


#define ldpTeCreateDiffServTnl        TeSigCreateDiffServTnl
#define ldpTeDeleteDiffServTnl        TeSigDeleteDiffServTnl 
#define ldpTeUpdateTrfcParamsElspList TeSigUpdateTrfcParamsElspList
#define ldpTeCreateDiffServElspList   TeSigCreateDiffServElspList
#define ldpTeDiffServGetPhbPsc        TeSigDiffServGetPhbPsc
#define ldpTeDeleteDiffServElspList   TeSigDeleteDiffServElspList
#define LDP_TE_TP_ADD  TE_TP_ADD

/*The following macros are for interface with FM */
#define LDP_FM_DS_LSP_SERVICE_TYPE(pDiffServParams)\
                         MPLS_LSP_SERVICE_TYPE(pDiffServParams)
#define LDP_FM_DS_CREATER(pDiffServParams) pDiffServParams->u1Creator 
#define LDP_FM_DS_LLSP_DSCP(pDiffServParams) MPLS_LLSP_DSCP(pDiffServParams)
#define LDP_FM_DS_ELSP_TYPE(pDiffServParams) MPLS_ELSP_TYPE(pDiffServParams)
#define LDP_FM_DS_SIG_ELSP(pDiffServParams) MPLS_SIG_ELSP_MAP(pDiffServParams)
#define ldpMplsGetPreConfMap   MplsDiffServGetPreConfMap

#endif
