#ifndef _LDP_VCUSM_H
#define _LDP_VCUSM_H



tMrgUpFsmFuncPtr
    gLdpMrgUpFsm[LDP_MAX_MRG_UPSTR_STATES]
    [LDP_MAX_MRG_UPSTR_EVENTS] =
{
  /* Idle State */
  {
     LdpMrgUpIdlLdpReq,            /* LDP Request event */
     LdpMrgUpInvStateEvt,          /* Internal DStr Mapping event */
     LdpMrgUpInvStateEvt,          /* LDP Release event */
     LdpMrgUpInvStateEvt,          /* Internal DStr Withdraw event */
     LdpMrgUpInvStateEvt,          /* LDP Up Abort Event */
     LdpMrgUpInvStateEvt,          /* Internal DStr NAK event */
     LdpMrgUpInvStateEvt,          /* Up Lost event */
     LdpMrgUpInvStateEvt,          /* Internal Re-XConnect event */
     LdpMrgUpInvStateEvt          /* Internal New Nh event */
  }
  ,
  /* Upstream Response Await state */
  { 
     LdpMrgUpInvStateEvt,          /* LDP Request event */
     LdpMrgUpRspAwtIntDStrMap,     /* Internal DStr Mapping event */
     LdpMrgUpInvStateEvt,          /* LDP Release event */
     LdpMrgUpInvStateEvt,          /* Internal DStr Withdraw event */
     LdpMrgUpRspAwtLdpUpAbrt,      /* LDP Up Abort Event */
     LdpMrgUpRspAwtIntDStrNak,     /* Internal DStr NAK event */
     LdpMrgUpRspAwtUpLost,         /* Up Lost event */
     LdpMrgUpInvStateEvt,          /* Internal Re-XConnect event */
     LdpMrgUpRspAwtIntNewNh        /* Internal New Nh event */

  }
  ,
  /* Upstream Established state */
  { 
     LdpMrgUpInvStateEvt,          /* LDP Request event */
     LdpMrgUpEstIntDStrMap,        /* Internal DStr Mapping event */
     LdpMrgUpEstLdpRel,            /* LDP Release event */
     LdpMrgUpEstIntDStrWdraw,      /* Internal DStr Withdraw event */
     LdpMrgUpInvStateEvt,          /* LDP Up Abort Event */
     LdpMrgUpEstIntDStrNak,        /* Internal DStr NAK event */
     LdpMrgUpEstUpLost,            /* Up Lost event */
     LdpMrgUpEstReXConnect,        /* Internal Re-XConnect event */
     LdpMrgUpEstIntNewNh           /* Internal New Nh event */
  }
  ,
  /* Upstream Release Await state */
  { 
     LdpMrgUpInvStateEvt,          /* LDP Request event */
     LdpMrgUpInvStateEvt,          /* Internal DStr Mapping event */
     LdpMrgUpRelAwtLdpRel,         /* LDP Release event */
     LdpMrgUpInvStateEvt,          /* Internal DStr Withdraw event */
     LdpMrgUpRelAwtLdpUpAbrt,      /* LDP Up Abort Event */
     LdpMrgUpInvStateEvt,          /* Internal DStr NAK event */
     LdpMrgUpRelAwtUpLost,         /* Up Lost event */
     LdpMrgUpInvStateEvt,          /* Internal Re-XConnect event */
     LdpMrgUpInvStateEvt           /* Internal New Nh event */
  }
};

CONST CHR1  *au1LdpVcUpStates[LDP_MAX_MRG_UPSTR_STATES] = {
                                                          "IDLE",
                                                          "RESP AWAITED", 
                                                          "ESTABLISHED", 
                                                          "REL AWAITED" 
                                               };
 
CONST CHR1 *au1LdpVcUpEvents[LDP_MAX_MRG_UPSTR_EVENTS] = {
                                                         "LDP REQ",
                                                         "LDP MAPPING",
                                                         "LDP REL",
                                                         "LDP WITHDRAW",
                                                         "LDP UP ABORT",
                                                         "LDP NAK",
                                                         "RE X CONNECT",
                                                         "NEW NHOP"
                                             }; 


#endif /*_LDP_VCUSM_H */
