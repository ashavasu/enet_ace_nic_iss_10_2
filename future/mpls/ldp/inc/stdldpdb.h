/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdldpdb.h,v 1.4 2008/08/20 15:14:46 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDLDPDB_H
#define _STDLDPDB_H

UINT1 MplsLdpEntityTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsLdpEntityStatsTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsLdpPeerTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsLdpSessionTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsLdpSessionStatsTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsLdpHelloAdjacencyTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsInSegmentLdpLspTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsOutSegmentLdpLspTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsFecTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsLdpLspFecTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsLdpSessionPeerAddrTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdldp [] ={1,3,6,1,2,1,10,166,4};
tSNMP_OID_TYPE stdldpOID = {9, stdldp};


UINT4 MplsLdpLsrId [ ] ={1,3,6,1,2,1,10,166,4,1,1,1};
UINT4 MplsLdpLsrLoopDetectionCapable [ ] ={1,3,6,1,2,1,10,166,4,1,1,2};
UINT4 MplsLdpEntityLastChange [ ] ={1,3,6,1,2,1,10,166,4,1,2,1};
UINT4 MplsLdpEntityIndexNext [ ] ={1,3,6,1,2,1,10,166,4,1,2,2};
UINT4 MplsLdpEntityLdpId [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,1};
UINT4 MplsLdpEntityIndex [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,2};
UINT4 MplsLdpEntityProtocolVersion [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,3};
UINT4 MplsLdpEntityAdminStatus [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,4};
UINT4 MplsLdpEntityOperStatus [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,5};
UINT4 MplsLdpEntityTcpPort [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,6};
UINT4 MplsLdpEntityUdpDscPort [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,7};
UINT4 MplsLdpEntityMaxPduLength [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,8};
UINT4 MplsLdpEntityKeepAliveHoldTimer [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,9};
UINT4 MplsLdpEntityHelloHoldTimer [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,10};
UINT4 MplsLdpEntityInitSessionThreshold [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,11};
UINT4 MplsLdpEntityLabelDistMethod [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,12};
UINT4 MplsLdpEntityLabelRetentionMode [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,13};
UINT4 MplsLdpEntityPathVectorLimit [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,14};
UINT4 MplsLdpEntityHopCountLimit [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,15};
UINT4 MplsLdpEntityTransportAddrKind [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,16};
UINT4 MplsLdpEntityTargetPeer [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,17};
UINT4 MplsLdpEntityTargetPeerAddrType [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,18};
UINT4 MplsLdpEntityTargetPeerAddr [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,19};
UINT4 MplsLdpEntityLabelType [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,20};
UINT4 MplsLdpEntityDiscontinuityTime [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,21};
UINT4 MplsLdpEntityStorageType [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,22};
UINT4 MplsLdpEntityRowStatus [ ] ={1,3,6,1,2,1,10,166,4,1,2,3,1,23};
UINT4 MplsLdpEntityStatsSessionAttempts [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,1};
UINT4 MplsLdpEntityStatsSessionRejectedNoHelloErrors [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,2};
UINT4 MplsLdpEntityStatsSessionRejectedAdErrors [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,3};
UINT4 MplsLdpEntityStatsSessionRejectedMaxPduErrors [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,4};
UINT4 MplsLdpEntityStatsSessionRejectedLRErrors [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,5};
UINT4 MplsLdpEntityStatsBadLdpIdentifierErrors [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,6};
UINT4 MplsLdpEntityStatsBadPduLengthErrors [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,7};
UINT4 MplsLdpEntityStatsBadMessageLengthErrors [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,8};
UINT4 MplsLdpEntityStatsBadTlvLengthErrors [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,9};
UINT4 MplsLdpEntityStatsMalformedTlvValueErrors [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,10};
UINT4 MplsLdpEntityStatsKeepAliveTimerExpErrors [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,11};
UINT4 MplsLdpEntityStatsShutdownReceivedNotifications [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,12};
UINT4 MplsLdpEntityStatsShutdownSentNotifications [ ] ={1,3,6,1,2,1,10,166,4,1,2,4,1,13};
UINT4 MplsLdpPeerLastChange [ ] ={1,3,6,1,2,1,10,166,4,1,3,1};
UINT4 MplsLdpPeerLdpId [ ] ={1,3,6,1,2,1,10,166,4,1,3,2,1,1};
UINT4 MplsLdpPeerLabelDistMethod [ ] ={1,3,6,1,2,1,10,166,4,1,3,2,1,2};
UINT4 MplsLdpPeerPathVectorLimit [ ] ={1,3,6,1,2,1,10,166,4,1,3,2,1,3};
UINT4 MplsLdpPeerTransportAddrType [ ] ={1,3,6,1,2,1,10,166,4,1,3,2,1,4};
UINT4 MplsLdpPeerTransportAddr [ ] ={1,3,6,1,2,1,10,166,4,1,3,2,1,5};
UINT4 MplsLdpSessionStateLastChange [ ] ={1,3,6,1,2,1,10,166,4,1,3,3,1,1};
UINT4 MplsLdpSessionState [ ] ={1,3,6,1,2,1,10,166,4,1,3,3,1,2};
UINT4 MplsLdpSessionRole [ ] ={1,3,6,1,2,1,10,166,4,1,3,3,1,3};
UINT4 MplsLdpSessionProtocolVersion [ ] ={1,3,6,1,2,1,10,166,4,1,3,3,1,4};
UINT4 MplsLdpSessionKeepAliveHoldTimeRem [ ] ={1,3,6,1,2,1,10,166,4,1,3,3,1,5};
UINT4 MplsLdpSessionKeepAliveTime [ ] ={1,3,6,1,2,1,10,166,4,1,3,3,1,6};
UINT4 MplsLdpSessionMaxPduLength [ ] ={1,3,6,1,2,1,10,166,4,1,3,3,1,7};
UINT4 MplsLdpSessionDiscontinuityTime [ ] ={1,3,6,1,2,1,10,166,4,1,3,3,1,8};
UINT4 MplsLdpSessionStatsUnknownMesTypeErrors [ ] ={1,3,6,1,2,1,10,166,4,1,3,4,1,1};
UINT4 MplsLdpSessionStatsUnknownTlvErrors [ ] ={1,3,6,1,2,1,10,166,4,1,3,4,1,2};
UINT4 MplsLdpHelloAdjacencyIndex [ ] ={1,3,6,1,2,1,10,166,4,1,3,5,1,1,1};
UINT4 MplsLdpHelloAdjacencyHoldTimeRem [ ] ={1,3,6,1,2,1,10,166,4,1,3,5,1,1,2};
UINT4 MplsLdpHelloAdjacencyHoldTime [ ] ={1,3,6,1,2,1,10,166,4,1,3,5,1,1,3};
UINT4 MplsLdpHelloAdjacencyType [ ] ={1,3,6,1,2,1,10,166,4,1,3,5,1,1,4};
UINT4 MplsInSegmentLdpLspIndex [ ] ={1,3,6,1,2,1,10,166,4,1,3,6,1,1};
UINT4 MplsInSegmentLdpLspLabelType [ ] ={1,3,6,1,2,1,10,166,4,1,3,6,1,2};
UINT4 MplsInSegmentLdpLspType [ ] ={1,3,6,1,2,1,10,166,4,1,3,6,1,3};
UINT4 MplsOutSegmentLdpLspIndex [ ] ={1,3,6,1,2,1,10,166,4,1,3,7,1,1};
UINT4 MplsOutSegmentLdpLspLabelType [ ] ={1,3,6,1,2,1,10,166,4,1,3,7,1,2};
UINT4 MplsOutSegmentLdpLspType [ ] ={1,3,6,1,2,1,10,166,4,1,3,7,1,3};
UINT4 MplsFecLastChange [ ] ={1,3,6,1,2,1,10,166,4,1,3,8,1};
UINT4 MplsFecIndexNext [ ] ={1,3,6,1,2,1,10,166,4,1,3,8,2};
UINT4 MplsFecIndex [ ] ={1,3,6,1,2,1,10,166,4,1,3,8,3,1,1};
UINT4 MplsFecType [ ] ={1,3,6,1,2,1,10,166,4,1,3,8,3,1,2};
UINT4 MplsFecAddrType [ ] ={1,3,6,1,2,1,10,166,4,1,3,8,3,1,4};
UINT4 MplsFecAddr [ ] ={1,3,6,1,2,1,10,166,4,1,3,8,3,1,5};
UINT4 MplsFecAddrPrefixLength [ ] ={1,3,6,1,2,1,10,166,4,1,3,8,3,1,3};
UINT4 MplsFecStorageType [ ] ={1,3,6,1,2,1,10,166,4,1,3,8,3,1,6};
UINT4 MplsFecRowStatus [ ] ={1,3,6,1,2,1,10,166,4,1,3,8,3,1,7};
UINT4 MplsLdpLspFecLastChange [ ] ={1,3,6,1,2,1,10,166,4,1,3,9};
UINT4 MplsLdpLspFecSegment [ ] ={1,3,6,1,2,1,10,166,4,1,3,10,1,1};
UINT4 MplsLdpLspFecSegmentIndex [ ] ={1,3,6,1,2,1,10,166,4,1,3,10,1,2};
UINT4 MplsLdpLspFecIndex [ ] ={1,3,6,1,2,1,10,166,4,1,3,10,1,3};
UINT4 MplsLdpLspFecStorageType [ ] ={1,3,6,1,2,1,10,166,4,1,3,10,1,4};
UINT4 MplsLdpLspFecRowStatus [ ] ={1,3,6,1,2,1,10,166,4,1,3,10,1,5};
UINT4 MplsLdpSessionPeerAddrIndex [ ] ={1,3,6,1,2,1,10,166,4,1,3,11,1,1};
UINT4 MplsLdpSessionPeerNextHopAddrType [ ] ={1,3,6,1,2,1,10,166,4,1,3,11,1,2};
UINT4 MplsLdpSessionPeerNextHopAddr [ ] ={1,3,6,1,2,1,10,166,4,1,3,11,1,3};


tMbDbEntry stdldpMibEntry[]= {

{{12,MplsLdpLsrId}, NULL, MplsLdpLsrIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,MplsLdpLsrLoopDetectionCapable}, NULL, MplsLdpLsrLoopDetectionCapableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,MplsLdpEntityLastChange}, NULL, MplsLdpEntityLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{12,MplsLdpEntityIndexNext}, NULL, MplsLdpEntityIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,MplsLdpEntityLdpId}, GetNextIndexMplsLdpEntityTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityIndex}, GetNextIndexMplsLdpEntityTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityProtocolVersion}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityProtocolVersionGet, MplsLdpEntityProtocolVersionSet, MplsLdpEntityProtocolVersionTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "1"},

{{14,MplsLdpEntityAdminStatus}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityAdminStatusGet, MplsLdpEntityAdminStatusSet, MplsLdpEntityAdminStatusTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "1"},

{{14,MplsLdpEntityOperStatus}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityTcpPort}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityTcpPortGet, MplsLdpEntityTcpPortSet, MplsLdpEntityTcpPortTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "646"},

{{14,MplsLdpEntityUdpDscPort}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityUdpDscPortGet, MplsLdpEntityUdpDscPortSet, MplsLdpEntityUdpDscPortTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "646"},

{{14,MplsLdpEntityMaxPduLength}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityMaxPduLengthGet, MplsLdpEntityMaxPduLengthSet, MplsLdpEntityMaxPduLengthTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "4096"},

{{14,MplsLdpEntityKeepAliveHoldTimer}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityKeepAliveHoldTimerGet, MplsLdpEntityKeepAliveHoldTimerSet, MplsLdpEntityKeepAliveHoldTimerTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "40"},

{{14,MplsLdpEntityHelloHoldTimer}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityHelloHoldTimerGet, MplsLdpEntityHelloHoldTimerSet, MplsLdpEntityHelloHoldTimerTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "0"},

{{14,MplsLdpEntityInitSessionThreshold}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityInitSessionThresholdGet, MplsLdpEntityInitSessionThresholdSet, MplsLdpEntityInitSessionThresholdTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "8"},

{{14,MplsLdpEntityLabelDistMethod}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityLabelDistMethodGet, MplsLdpEntityLabelDistMethodSet, MplsLdpEntityLabelDistMethodTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityLabelRetentionMode}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityLabelRetentionModeGet, MplsLdpEntityLabelRetentionModeSet, MplsLdpEntityLabelRetentionModeTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityPathVectorLimit}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityPathVectorLimitGet, MplsLdpEntityPathVectorLimitSet, MplsLdpEntityPathVectorLimitTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityHopCountLimit}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityHopCountLimitGet, MplsLdpEntityHopCountLimitSet, MplsLdpEntityHopCountLimitTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "0"},

{{14,MplsLdpEntityTransportAddrKind}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityTransportAddrKindGet, MplsLdpEntityTransportAddrKindSet, MplsLdpEntityTransportAddrKindTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "2"},

{{14,MplsLdpEntityTargetPeer}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityTargetPeerGet, MplsLdpEntityTargetPeerSet, MplsLdpEntityTargetPeerTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "2"},

{{14,MplsLdpEntityTargetPeerAddrType}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityTargetPeerAddrTypeGet, MplsLdpEntityTargetPeerAddrTypeSet, MplsLdpEntityTargetPeerAddrTypeTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityTargetPeerAddr}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityTargetPeerAddrGet, MplsLdpEntityTargetPeerAddrSet, MplsLdpEntityTargetPeerAddrTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityLabelType}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityLabelTypeGet, MplsLdpEntityLabelTypeSet, MplsLdpEntityLabelTypeTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityDiscontinuityTime}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStorageType}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityStorageTypeGet, MplsLdpEntityStorageTypeSet, MplsLdpEntityStorageTypeTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 0, "3"},

{{14,MplsLdpEntityRowStatus}, GetNextIndexMplsLdpEntityTable, MplsLdpEntityRowStatusGet, MplsLdpEntityRowStatusSet, MplsLdpEntityRowStatusTest, MplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityTableINDEX, 2, 0, 1, NULL},

{{14,MplsLdpEntityStatsSessionAttempts}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsSessionAttemptsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsSessionRejectedNoHelloErrors}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsSessionRejectedNoHelloErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsSessionRejectedAdErrors}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsSessionRejectedAdErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsSessionRejectedMaxPduErrors}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsSessionRejectedMaxPduErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsSessionRejectedLRErrors}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsSessionRejectedLRErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsBadLdpIdentifierErrors}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsBadLdpIdentifierErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsBadPduLengthErrors}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsBadPduLengthErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsBadMessageLengthErrors}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsBadMessageLengthErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsBadTlvLengthErrors}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsBadTlvLengthErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsMalformedTlvValueErrors}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsMalformedTlvValueErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsKeepAliveTimerExpErrors}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsKeepAliveTimerExpErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsShutdownReceivedNotifications}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsShutdownReceivedNotificationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{14,MplsLdpEntityStatsShutdownSentNotifications}, GetNextIndexMplsLdpEntityStatsTable, MplsLdpEntityStatsShutdownSentNotificationsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpEntityStatsTableINDEX, 2, 0, 0, NULL},

{{12,MplsLdpPeerLastChange}, NULL, MplsLdpPeerLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,MplsLdpPeerLdpId}, GetNextIndexMplsLdpPeerTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsLdpPeerTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpPeerLabelDistMethod}, GetNextIndexMplsLdpPeerTable, MplsLdpPeerLabelDistMethodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsLdpPeerTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpPeerPathVectorLimit}, GetNextIndexMplsLdpPeerTable, MplsLdpPeerPathVectorLimitGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, MplsLdpPeerTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpPeerTransportAddrType}, GetNextIndexMplsLdpPeerTable, MplsLdpPeerTransportAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsLdpPeerTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpPeerTransportAddr}, GetNextIndexMplsLdpPeerTable, MplsLdpPeerTransportAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsLdpPeerTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpSessionStateLastChange}, GetNextIndexMplsLdpSessionTable, MplsLdpSessionStateLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsLdpSessionTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpSessionState}, GetNextIndexMplsLdpSessionTable, MplsLdpSessionStateGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsLdpSessionTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpSessionRole}, GetNextIndexMplsLdpSessionTable, MplsLdpSessionRoleGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsLdpSessionTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpSessionProtocolVersion}, GetNextIndexMplsLdpSessionTable, MplsLdpSessionProtocolVersionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsLdpSessionTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpSessionKeepAliveHoldTimeRem}, GetNextIndexMplsLdpSessionTable, MplsLdpSessionKeepAliveHoldTimeRemGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsLdpSessionTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpSessionKeepAliveTime}, GetNextIndexMplsLdpSessionTable, MplsLdpSessionKeepAliveTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsLdpSessionTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpSessionMaxPduLength}, GetNextIndexMplsLdpSessionTable, MplsLdpSessionMaxPduLengthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsLdpSessionTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpSessionDiscontinuityTime}, GetNextIndexMplsLdpSessionTable, MplsLdpSessionDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsLdpSessionTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpSessionStatsUnknownMesTypeErrors}, GetNextIndexMplsLdpSessionStatsTable, MplsLdpSessionStatsUnknownMesTypeErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpSessionStatsTableINDEX, 3, 0, 0, NULL},

{{14,MplsLdpSessionStatsUnknownTlvErrors}, GetNextIndexMplsLdpSessionStatsTable, MplsLdpSessionStatsUnknownTlvErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsLdpSessionStatsTableINDEX, 3, 0, 0, NULL},

{{15,MplsLdpHelloAdjacencyIndex}, GetNextIndexMplsLdpHelloAdjacencyTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsLdpHelloAdjacencyTableINDEX, 4, 0, 0, NULL},

{{15,MplsLdpHelloAdjacencyHoldTimeRem}, GetNextIndexMplsLdpHelloAdjacencyTable, MplsLdpHelloAdjacencyHoldTimeRemGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsLdpHelloAdjacencyTableINDEX, 4, 0, 0, NULL},

{{15,MplsLdpHelloAdjacencyHoldTime}, GetNextIndexMplsLdpHelloAdjacencyTable, MplsLdpHelloAdjacencyHoldTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsLdpHelloAdjacencyTableINDEX, 4, 0, 0, NULL},

{{15,MplsLdpHelloAdjacencyType}, GetNextIndexMplsLdpHelloAdjacencyTable, MplsLdpHelloAdjacencyTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsLdpHelloAdjacencyTableINDEX, 4, 0, 0, NULL},

{{14,MplsInSegmentLdpLspIndex}, GetNextIndexMplsInSegmentLdpLspTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsInSegmentLdpLspTableINDEX, 4, 0, 0, NULL},

{{14,MplsInSegmentLdpLspLabelType}, GetNextIndexMplsInSegmentLdpLspTable, MplsInSegmentLdpLspLabelTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsInSegmentLdpLspTableINDEX, 4, 0, 0, NULL},

{{14,MplsInSegmentLdpLspType}, GetNextIndexMplsInSegmentLdpLspTable, MplsInSegmentLdpLspTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsInSegmentLdpLspTableINDEX, 4, 0, 0, NULL},

{{14,MplsOutSegmentLdpLspIndex}, GetNextIndexMplsOutSegmentLdpLspTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsOutSegmentLdpLspTableINDEX, 4, 0, 0, NULL},

{{14,MplsOutSegmentLdpLspLabelType}, GetNextIndexMplsOutSegmentLdpLspTable, MplsOutSegmentLdpLspLabelTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsOutSegmentLdpLspTableINDEX, 4, 0, 0, NULL},

{{14,MplsOutSegmentLdpLspType}, GetNextIndexMplsOutSegmentLdpLspTable, MplsOutSegmentLdpLspTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsOutSegmentLdpLspTableINDEX, 4, 0, 0, NULL},

{{13,MplsFecLastChange}, NULL, MplsFecLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsFecIndexNext}, NULL, MplsFecIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{15,MplsFecIndex}, GetNextIndexMplsFecTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsFecTableINDEX, 1, 0, 0, NULL},

{{15,MplsFecType}, GetNextIndexMplsFecTable, MplsFecTypeGet, MplsFecTypeSet, MplsFecTypeTest, MplsFecTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsFecTableINDEX, 1, 0, 0, NULL},

{{15,MplsFecAddrPrefixLength}, GetNextIndexMplsFecTable, MplsFecAddrPrefixLengthGet, MplsFecAddrPrefixLengthSet, MplsFecAddrPrefixLengthTest, MplsFecTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsFecTableINDEX, 1, 0, 0, "0"},

{{15,MplsFecAddrType}, GetNextIndexMplsFecTable, MplsFecAddrTypeGet, MplsFecAddrTypeSet, MplsFecAddrTypeTest, MplsFecTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsFecTableINDEX, 1, 0, 0, NULL},

{{15,MplsFecAddr}, GetNextIndexMplsFecTable, MplsFecAddrGet, MplsFecAddrSet, MplsFecAddrTest, MplsFecTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsFecTableINDEX, 1, 0, 0, NULL},

{{15,MplsFecStorageType}, GetNextIndexMplsFecTable, MplsFecStorageTypeGet, MplsFecStorageTypeSet, MplsFecStorageTypeTest, MplsFecTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsFecTableINDEX, 1, 0, 0, "3"},

{{15,MplsFecRowStatus}, GetNextIndexMplsFecTable, MplsFecRowStatusGet, MplsFecRowStatusSet, MplsFecRowStatusTest, MplsFecTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsFecTableINDEX, 1, 0, 1, NULL},

{{12,MplsLdpLspFecLastChange}, NULL, MplsLdpLspFecLastChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{14,MplsLdpLspFecSegment}, GetNextIndexMplsLdpLspFecTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsLdpLspFecTableINDEX, 6, 0, 0, NULL},

{{14,MplsLdpLspFecSegmentIndex}, GetNextIndexMplsLdpLspFecTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsLdpLspFecTableINDEX, 6, 0, 0, NULL},

{{14,MplsLdpLspFecIndex}, GetNextIndexMplsLdpLspFecTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsLdpLspFecTableINDEX, 6, 0, 0, NULL},

{{14,MplsLdpLspFecStorageType}, GetNextIndexMplsLdpLspFecTable, MplsLdpLspFecStorageTypeGet, MplsLdpLspFecStorageTypeSet, MplsLdpLspFecStorageTypeTest, MplsLdpLspFecTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpLspFecTableINDEX, 6, 0, 0, "3"},

{{14,MplsLdpLspFecRowStatus}, GetNextIndexMplsLdpLspFecTable, MplsLdpLspFecRowStatusGet, MplsLdpLspFecRowStatusSet, MplsLdpLspFecRowStatusTest, MplsLdpLspFecTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpLspFecTableINDEX, 6, 0, 1, NULL},

{{14,MplsLdpSessionPeerAddrIndex}, GetNextIndexMplsLdpSessionPeerAddrTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsLdpSessionPeerAddrTableINDEX, 4, 0, 0, NULL},

{{14,MplsLdpSessionPeerNextHopAddrType}, GetNextIndexMplsLdpSessionPeerAddrTable, MplsLdpSessionPeerNextHopAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsLdpSessionPeerAddrTableINDEX, 4, 0, 0, NULL},

{{14,MplsLdpSessionPeerNextHopAddr}, GetNextIndexMplsLdpSessionPeerAddrTable, MplsLdpSessionPeerNextHopAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsLdpSessionPeerAddrTableINDEX, 4, 0, 0, NULL},
};
tMibData stdldpEntry = { 84, stdldpMibEntry };
#endif /* _STDLDPDB_H */

