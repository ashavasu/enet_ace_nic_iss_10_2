
 /* $Id: ldpextif.h,v 1.8 2014/11/08 11:59:52 siva Exp $*/
/********************************************************************
 *                                                                  *
 * $Id: ldpextif.h,v 1.8 2014/11/08 11:59:52 siva Exp $
 *                                                                  *
 * $Date: 2014/11/08 11:59:52 $                                     *
 *                                                                  *
 * $Revision: 1.8 $                                                 *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpextif.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains extern declarations of the external
 *                             interface routines.
 *---------------------------------------------------------------------------*/

#ifndef _LDPEXTIF_H
#define _LDPEXTIF_H

/* Prototypes for IP Interface Routines */
extern UINT4        gu4CrldpTableOffSet[CRLDP_TABLE_DEF_OFFSET];

/* Prototypes for  MPLS-FM Interface Routines */

extern tSNMP_OID_TYPE *SNMP_AGT_FormOid ARG_LIST ((UINT4 *, INT2));

extern INT4 L2VpnLdpEventHandler ARG_LIST ((VOID *pMsg, UINT4 u4Event));
#ifdef LDP_GR_WANTED
extern INT4
L2VpnGetNextLabelFromHwList(tL2VpnPwHwList  *pL2VpnPwHwListEntry,
						tL2VpnPwHwList  *pL2VpnPwHwListNextEntry,
						UINT4 * pu4InLabel);

#define tLdpL2VpnPwHwList       tL2VpnPwHwList
#endif
extern INT4 L2VpnLdpGetEventInfo (VOID **ppL2VpnQMsg);
extern INT4 L2VpnLdpCleanEventInfo (VOID *pL2VpnQMsg, BOOL1 b1CleanQMsg, BOOL1 b1CleanNodes);
extern INT4 L2VpnLdpSendEventInfo (VOID *pL2VpnQMsg, UINT4 u4Event);
extern tPwRedDataEvtPwFec * L2VpnLdpGetEventPwFec ARG_LIST ((VOID));
extern INT4 L2VpnLdpCleanEventPwFec (VOID *pEvtPwFec);
extern tPwRedDataEvtPwData  * L2VpnLdpGetEventPwData ARG_LIST ((VOID));
extern INT4 L2VpnLdpCleanEventPwData (VOID *pEvtPwData);

extern VOID L2vpUpdateLdpUpStatus ARG_LIST((VOID));

extern UINT4 gi4MplsSimulateFailure;
 
#endif /* _LDPEXTIF_H   */
/*---------------------------------------------------------------------------*/
/*                        End of file ldpextif.h                             */
/*---------------------------------------------------------------------------*/
