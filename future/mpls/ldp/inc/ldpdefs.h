
/********************************************************************
 *                                                                  *
 * $Id: ldpdefs.h,v 1.36 2015/02/28 12:19:38 siva Exp $ 
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpdefs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants used across sub modules
 *---------------------------------------------------------------------------*/
#ifndef _LDP_DEFS_H
#define _LDP_DEFS_H

/* Values used to validate an IP address */
#define LDP_MIN_NET_NUMBER       0x00000001
#define LDP_MAX_NET_NUMBER       0xffffffff
#define LDP_LOOPBACK_NET_ADDRESS 0x7F000000
#define LDP_HOST_NETMASK         0xffffffff
#define LDP_24BIT_NETMASK        0xffffff00
#define LDP_CSR_RESTORE_COMPLELTE   0x00400000
/* Udp related Default Paramters */
#define LDP_DEF_TOS              0
#define LDP_DEF_TTL              50
#define LDP_DEF_OPT_ID           0
#define LDP_DONT_FRAG            0
#define LDP_DEF_OPT_LEN          0
#define LDP_CHKSUM_FLAG          0
#define LDP_BASIC_HELLO_TTL      1
#define LDP_TARGET_HELLO_TTL     64
/* Tcp related Default Parameters */
#define LDP_DEF_TCP_TIMEOUT      100    /* This value MUST be > Max ssn hold time */
#define LDP_MAX_TCP_PEND_MSGS    30

/* Return Values */
#define LDP_OK                   1
#define LDP_NOT_OK               0
#define LDP_TRUE                 1
#define LDP_FALSE                0
#define LDP_EQUAL                0
#define LDP_NOT_EQUAL            1 
#define LDP_SMALLER              1
#define LDP_GREATER              2

#define LDP_ADMIN_UP             1
#define LDP_ADMIN_DOWN           2
#define LDP_ADMIN_TESTING        3

#define LDP_ADMIN_UP_IN_PROGRESS    3
#define LDP_ADMIN_DOWN_IN_PROGRESS  4


#define LDP_OPER_UP               1
#define LDP_OPER_DOWN             2
#define LDP_OPER_TSTNG            3
#define LDP_OPER_UNKWN            4
#define LDP_OPER_DRMNT            5
#define LDP_OPER_NOTPRES          6
#define LDP_OPER_LLDWN            7

/* OperStatus */
#define LDP_ON                   1
#define LDP_OFF                  0
#define LDP_RM_TRUE              1
#define LDP_RM_FALSE             2
#define LDP_ZERO                 0
#define LDP_NO_LOOPBACK          0

/* TruthValue as defined in RFC 1903 (Textual Convention - SNMPv2) */
#define LDP_SNMP_TRUE            1
#define LDP_SNMP_FALSE           2

#define LDP_SNMP_NO              0
#define LDP_SNMP_YES             1

/* Macros related to mplsLdpEntityOperStatus */
#define LDP_OPER_UNKNOWN         1
#define LDP_OPER_ENABLED         2
#define LDP_OPER_DISABLED        3

#define LDP_ONE 1
#define LDP_THREE 3

#define LDP_INSEG_FEC 1
#define LDP_OUTSEG_FEC 2

#define LDP_KEEPALIVE_DEFAULT_TIME 40
#define LDP_HELLOHOLDTIME_DEFAULT_TIME 0
#define LDP_DEFAULT_THERSHOLD 8
#define LDP_MIN_THERSHOLD 0
#define LDP_MAX_THERSHOLD  100



/* Types of Peer */
#define LDP_LOCAL_PEER           1
#define LDP_REMOTE_PEER          2

/* Types of Hellos */
#define LDP_BASIC_HELLOS         1
#define LDP_TRG_HELLOS           2

/* Admin Status */
#define LDP_ENABLED              1
#define LDP_DISABLED             2

/* Ldp policy */
#define LDP_POLICY_ENABLED       1
#define LDP_POLICY_DISABLED      2

/* Label Space Type */
#define LDP_UNKNOWN_LBL_SPACE    1
#define LDP_PER_IFACE            2
#define LDP_PER_PLATFORM         3


#define LDP_GEN_PER_IFACE        1
#define LDP_GEN_PER_PLATFORM     2

/* Label Retention Modes */
#define LDP_CONSERVATIVE_MODE    1
#define LDP_LIBERAL_MODE         2

/* Label Allocation modes */
#define LDP_ORDERED_MODE         1
#define LDP_INDEPENDENT_MODE     2

/* Label Distribution modes */
#define LDP_ON_DEMAND_MODE       1
#define LDP_UNSOLICITED_MODE     2

/* Loop Detection */
#define LDP_LOOP_DETECT_ENABLE   1
#define LDP_LOOP_DETECT_DISABLE  0

#define LDP_LOOP_DETECT_NOT_CAPABLE    1
#define LDP_LOOP_DETECT_CAPABLE_OTHER  2
#define LDP_LOOP_DETECT_CAPABLE_HC     3
#define LDP_LOOP_DETECT_CAPABLE_PV     4
#define LDP_LOOP_DETECT_CAPABLE_HCPV   5

/* Loop Prevention */
#define LDP_LOOP_PREV_ENABLE     1
#define LDP_LOOP_PREV_DISABLE    0

/* Lsr Roles in a Session */
#define LDP_UNKNOWN              1
#define LDP_ACTIVE               2
#define LDP_PASSIVE              3

/* Entity Types */
#define LDP_ATM_MODE             2
#define LDP_GEN_MODE             1

/* LSR Types */
#define LDP_EDGE_LSR             1
#define LDP_INTRM_LSR            2

/* Entity Merge Capability */
#define LDP_NO_MRG               0
#define LDP_VP_MRG               1
#define LDP_VC_MRG               2
#define LDP_VPVC_MRG             3
#define LDP_DU_MRG               4   /* Down stream default Mrg 
                                      * value */

/* VC Directionality */
#define LDP_BI_DIR               1
#define LDP_UNI_DIR              2

/* ATM LSR Connectivity */
#define LDP_ATM_DIR_CONN         1
#define LDP_ATM_INDIR_CONN       2

/* Storage Types */
#define LDP_STORAGE_OTHER        1
#define LDP_STORAGE_VOLATILE     2
#define LDP_STORAGE_NONVOLATILE  3
#define LDP_STORAGE_PERMANENT    4
#define LDP_STORAGE_READONLY     5

/* Ranges for Generic Label ie, for Platform based Label Space */
#define LDP_MIN_GEN_LBL               LDP_GEN_MIN_LBL 
#define LDP_MAX_GEN_LBL               LDP_GEN_MAX_LBL 
#define LDP_TARGET_ENTITY_MIN_GEN_LBL VPLS_GEN_MIN_LBL 
#define LDP_TARGET_ENTITY_MAX_GEN_LBL VPLS_GEN_MAX_LBL

#define LDP_VC_BI_DIR            0
#define LDP_VC_UNI_DIR           1

#define HOLD_PRIO_RANGE          8
#define LDP_MIN_PDU_LEN          (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN + 6)
#define LDP_DEF_MAX_PDU_LEN      4096
#define LDP_MIN_MSG_LEN          4
#define LDP_NET_ADDR_LEN         4    /* First 4 bytes of LdpId */
/* MPLS_IPv6 add start*/
#define LDP_NET_ADDR6_LEN        16
/* MPLS_IPv6 add end*/
#define LDP_MAX_INCARN           1
#define LDP_MIN_INCARN           1
#define LDP_MAX_RETRY_TIME       120  /* seconds */

/* Constants related to TCP Connection establishment */
#define LDP_CONN_ESTD_TIME        5    /* Specifies the interval at which LDP 
                                          tries to send an Init Msg assuming that
                                          the Active TCP connection has been 
                                          established by then */
#define LDP_MAX_CONN_ESTD_TIME   30    /* This specifies the max time allowed 
                                          for a tcp connection to get 
                                          established. */

#define LDP_CUR_INCARN           0

#define LDP_TNLNAME_LEN          32

#define LDP_DEF_DEST_ADDR        0x10000600    /* This could be made a
                                                  configurable variable also */
#define LDP_DEF_LSR_ID           0x00000000

#define LDP_ZERO_HTIME           0
#define LDP_INFINITE_HTIME       0xffff
#define LDP_DEF_RSSN_HTIME       60    /* For Session with Remote Peer */
#define LDP_DEF_LSSN_HTIME       60    /* For Session with Local Peer */
#define LDP_DEF_RHELLO_HTIME     45    /* DEFVAL : 45 Secs, For Remote Peer */
#define LDP_DEF_LHELLO_HTIME     15    /* DEFVAL : 15 Secs, For Local Peer */
#define LDP_RHELLO_HTIME_FACTOR  3    /* Note : Remote Hello Hold Time Factor 
                                         Can be modified based on the Target */
#define LDP_HELLO_FREQ           0.2    /* 0.2  of the HoldTime negotiated through
                                           exhange of Hello Messages */
#define LDP_KALIVE_MSG_FREQ      0.4    /* 0.4  of the HoldTime negotiated 
                                           through exhange of Ssn Init Msgs */
/* Related to BFD for LDP */
#define LDP_MAX_BFD_SESSIONS  100

/* For calculating the milliseconds input for Timer Start*/
#define LDP_CONVERT_MSEC_TO_STUPS(x) ((x * SYS_NUM_OF_TIME_UNITS_IN_A_SEC) / 1000)

#define LDP_MAX_POOLS_PER_INCRN  20
#define LDP_MAX_PASSIVE_PEERS    48
#define LDP_DEF_MAX_PEER_IF_ENTS 256
#define LDP_DEF_MAX_TRG_BLOCKS   16
/* Set this to appropriate value for load testing */
/*#define LDP_DEF_MAX_TRG_BLOCKS   20000*/
#define LDP_DEF_GAD_MAX_BLOCKS   16
#define LDP_DEF_HOP_CNT_LIM      0
#define LDP_DEF_PATH_VECT_LIM    32
#define LDP_DEF_LBL_RET_MODE     LDP_CONSERVATIVE_MODE
#define CRLDP_SIG_PROT           2

/* Since the VPI and VCI fiels are only 
 * 2 bytes long */
#define LDP_MAX_VC_MERGE_LIMIT  65535
#define LDP_MAX_VP_MERGE_LIMIT  65535

#define LDP_MAX_VP_MRG_COUNT 1
#define LDP_MAX_VC_MRG_COUNT 1  

/* Interface Speed related definitions
 * Interface speed in bits per second */
#define LDP_ETH_IF_SPEED         10000000
#define LDP_ATM_IF_SPEED         155000000

#define LDP_START_REROUTE  1
#define LDP_END_REROUTE    2


/* For LDP_TCP_MD5 support */

#define MAX_MD5_KEY_SIZE         100
#define LDP_MD5_ENABLED          1
#define LDP_MD5_DISABLED         0
#define TCP_MD5_OPTION           1

#define LDP_MAX_STATIC_LSPS      16 
/* The Static LSPs that can be          
   configured by the SNMP manager */
/*
 * The Lsp States that are defined in the LDP mib under 
 * Insegment table 
 */

#define LDP_LSP_STATE_UNKNOWN 1 /* unknown */
#define LDP_LSP_TERMINATES    2 /* terminatingLsp */
#define LDP_LSP_ORIGINATES    3 /* originatingLsp */
#define LDP_LSP_XCONNECT      4 /* crossConnectingLsp */

#define LDP_MAX_TRIE_NUM_ENTRIES 24

#define LDP_HASH_SIZE            128

#define LDP_PROTO_VER            1
#define LDP_ALL_ROUTERS_ADDR     0xe0000002
/* MPLS_IPv6 add start*/
#define LDP_ALL_ROUTERS_V6ADDR    "ff02::2"
/* MPLS_IPv6 add end*/
#define LDP_DEF_MTU              1500
#define LDP_MIN_MTU              1500
#define LDP_MAX_MTU              9180

#define LDP_OFF_MSG_ID           4    /* from the beginning of the message */
#define LDP_OFF_TLV              8    /* from the beginning of the message */
#define LDP_OFF_TLV_LEN          2    /* from the beginning of the TLV */
#define LDP_OFF_MSG_LEN          2    /* from the beginning of the message */

#define LDP_MSGID_LEN            4    /* LDP Message Id length */

#define LDP_MSG_TYPE_FLD         0x7fff
#define LDP_TLV_TYPE_FLD         0x3fff
#define LDP_MIN_TLV_VAL          0x8000

#define LDP_RSVD_FLD             0x0000
#define LDP_UINT1_SIZE           255
#define LDP_UINT2_SIZE           65535
#define LDP_UINT4_SIZE           0xffffffff

#define LDP_ALL_APPIDS           -1
#define LDP_OSPF_RT_INDEX        13

/* definitions for mib object ranges */

#define LDP_MIN_OF_MAX_ENTITIES           1
#define LDP_MAX_OF_MAX_ENTITIES           1024
#define LDP_MIN_OF_MAX_LOCAL_PEERS        1
#define LDP_MAX_OF_MAX_LOCAL_PEERS        1024
#define LDP_MIN_OF_MAX_REMOTE_PEERS       1
#define LDP_MAX_OF_MAX_REMOTE_PEERS       1024
#define LDP_MIN_NONSTD_PORT               1024    /* 1 - 1023 are reserved port 
                                                   * numbers */
#define LDP_MAX_NONSTD_PORT               5000
#define LDP_MIN_PVLIMIT                   0
#define LDP_MAX_PVLIMIT                   255
#define LDP_MIN_HCLIMIT                   0
#define LDP_MAX_HCLIMIT                   255

/* SNMP Traps */
#define LDP_SSN_RETRY_FAILURE    1
#define LDP_LD_ADMIN_MISMATCH    2

#define LDP_TLV_MSG_U_BIT        0x8000
#define LDP_TLV_F_BIT            0x4000
#define LDP_TLV_T_BIT            0x8000
#define LDP_TLV_R_BIT            0x4000
#define LDP_ATM_V_BITS           0x0000
#define LDP_ATM_VP_BITS          0x1000
#define LDP_ATM_RESV_BITS        0xc000
#define LDP_SSN_A_BIT            0x8000
#define LDP_SSN_D_BIT            0x4000
#define LDP_SSN_RESV_BITS        0x3f00
#define LDP_ALR_RESV_BITS        0xf000    /* ATM Label Range */

#define LDP_SSN_ATM_M_BITS       0xc0000000
#define LDP_SSN_ATM_N_BITS       0x3c000000
#define LDP_SSN_ATM_D_BIT        0x02000000
#define LDP_SSN_ATM_RESV_BITS    0x01ffffff
#define LDP_SSN_FRM_M_BITS       0xc0000000
#define LDP_SSN_FRM_N_BITS       0x3c000000
#define LDP_SSN_FRM_D_BIT        0x02000000
#define LDP_SSN_FRM_RESV_BITS    0x01ffffff
#define LDP_STAT_E_BIT           0x80000000
#define LDP_STAT_F_BIT           0x40000000
#define LDP_STAT_U_BIT           0x8000
#define LDP_FLR_RESV_BITS        0xfe000000  /* FRML Label Range */
#define LDP_FLR_DLCI_LEN         0x01800000
#define LDP_STATUS_DATA_MASK     0x3fffffff  /* Mask for getting StatusData */

#define LDP_SET_TLV_T_BIT        1
#define LDP_SET_TLV_R_BIT        1

#define LDP_ENQ_MSG_SIZE       4

#define LDP_INVALID_CONN_ID    65535
#define LDP_INVALID_HOP_COUNT  0xff
#define LDP_INVALID_POOL_ID    0xffffffff
#define LDP_INVALID_REQ_ID     0xffffffff
#define LDP_INVALID_LABEL      0xffffffff
#define LDP_INVALID_GWADDR     0xffffffff
#define LDP_INVALID_RSRC_GRPID 0xffffffff

#define LDP_MAX_HOP_COUNT      0xff
#define LDP_MIN_HOP_COUNT      0

/* Memory Pool related */
#define LDP_MSG_BLK_SIZE       LDP_DEF_MTU
#define LDP_MEM_TYPE           MEM_DEFAULT_MEMORY_TYPE

/* Task related */
#define LDP_TSK_STACK_SIZE     OSIX_DEFAULT_STACK_SIZE
#define LDP_TSK_START_ADDR     LdpTaskMain
#define LDP_TSK_START_ARGS     NULL
#define LDP_TSK_MODE           OSIX_DEFAULT_TASK_MODE

/* 
 * NOTE: This value LDP_TSK_PRIORITY indicates the priority at which the LDP
 * Task is currently created with. This value needs to be suitably assigned
 * based on the final target requirements. 
 */
#define LDP_TSK_PRIORITY       35

/* Queue related */
#define LDP_QNAME              "LDPQ"    /* Name of LDP's Msg Queue */

#define LDP_UDP_QNAME        "ULQ"    /* Name of LDP's UDP Msg Queue */
#define LDP_UDP_QDEPTH       64 

#define LDP_MSG_EVT     0x00000001
#define LDP_TMR_EVT     0x00000010
#define LDP_UDP_MSG_EVT 0x00002000
#define LDP_TCP_MSG_EVT 0x00020000
#define LDP_TMR_EXPR_EVENT          0x00200000
#define LDP_CSR_RESTORE_COMPLELTE   0x00400000

#define LDP_QMODE       OSIX_LOCAL
#define LDP_QFLAGS      OSIX_NO_WAIT
#define LDP_QTIMEOUT    0

/* TCP Related */
#define LDP_ACTIVE_OPEN        1
#define LDP_PASSIVE_OPEN       2

/* BUFFER related */
#define LDP_DEF_BUF_SIZE       16
#define LDP_DEF_OFFSET         0

/* MPLS_IPv6 start*/
#define LDP_INIT_ADDR   0x0
#define LDP_ADDR_TYPE_IPV4   0x01
#define LDP_ADDR_TYPE_IPV6   0x02
#ifdef MPLS_IPV6_WANTED
#define LDP_TCP6_MSG_EVT 0x02000000
#define LDP_V6TR_ADDR_LEN           16
#endif
#define LDP_UDP6_MSG_EVT 0x20000000
/* MPLS_IPv6 end*/
#define LDP_STD_PORT           646
#define LDP_IP_TOS             0
#define LDP_IP_TTL             0
#define LDP_IP_ID              0
#define LDP_IP_DF              LDP_FALSE
#define LDP_IP_OPT_LEN         0
#define LDP_IP_PORT            0
#define LDP_CKSUM_WANTED       LDP_TRUE

#define LDP_PDU_HDR_LEN           10
#define LDP_PDU_HDR_OFFSET        4
#define LDP_MSG_HDR_LEN           4
#define LDP_TLV_TYPE_LEN          2
#define LDP_TLV_HDR_LEN           4
#define LDP_RETURN_MSG_LEN        32
#define LDP_MSG_ID_LEN            4
#define LDP_MSG_TYPE_LEN          2
#define LDP_STATUS_CODE_LEN       4
#define LDP_HC_VALUE_LEN          1
#define LDP_LSR_ID_LEN            4
#define LDP_FEC_ELMNT_LEN         8
#define LDP_IPV6_FEC_ELMNT_LEN    20
#define LDP_FEC_ELMNT_HDRLEN      4
#define LDP_LABEL_LEN             4
#define LDP_FEC_TYPE_LEN          1
#define LDP_HELLO_HOLD_TIME_LEN   2
#define LDP_HELLO_T_R_RSVD_LEN    2
#define LDP_IPV4_TR_ADDR_LEN      4
#define LDP_IPV6_TR_ADDR_LEN      16
#define LDP_CS_NUM_LEN            4
#define LDP_PRTCL_VER_LEN         2
#define LDP_KA_HOLD_TIME_LEN      2
#define LDP_ADDR_FMLY_LEN         2
#define LDP_IPV4ADR_LEN           4
#define LDP_IPV6ADR_LEN           16
#define LDP_CMN_SSN_TLV_VAL_LEN   14
#define LDP_FT_SSN_TLV_VAL_LEN    12
#define LDP_UINT1_LEN             1
#define LDP_UINT2_LEN             2
#define LDP_ADDR_TLV_HDR_LEN      LDP_ADDR_FMLY_LEN + LDP_IPV4_TR_ADDR_LEN


#define LDP_IP_HDR_LEN            20
#define LDP_UDP_HDR_LEN           8
#define LDP_TCP_HDR_LEN           20

/* CR-LSP Tlv Lengths related definitions */
#define LDP_LSPID_TLV_LEN         8
#define LDP_RTPIN_TLV_LEN         4
#define LDP_RSRCLASS_TLV_LEN      4
#define LDP_PREMPT_TLV_LEN        4
#define LDP_IPV4_ERHOP_TLV_LEN    8
#define LDP_IPV6_ERHOP_TLV_LEN    20
#define LDP_ERHOP_AS_TLV_LEN      4
#define LDP_ERHOP_LSPID_TLV_LEN   8
#define LDP_TRF_PARAMS_TLV_LEN    24
#define CRLSP_FEC_ELMNT_LEN       1
#define LDP_TR_ADDR_LEN           4

/* Definition for specifying what different types of Cr-Lsps */
#define LDP_ER_CRLSP              0x8000    /* Explicit-route based Cr-Lsp */
#define LDP_TR_CRLSP              0x4000    /* Traffic Params based Cr-Lsp */
#define LDP_PN_CRLSP              0x2000    /* Route Pinning based Cr-Lsp */
#define LDP_RSRC_CRLSP            0x1000    /* Resource Class based Cr-Lsp */
#define LDP_PREMPT_CRLSP          0x0800    /* Pre-emption */

/* Definition for indicating the type of Modification */
#define CRLSP_ACT_FLAG            0x0001    /* Explicit-route based Cr-Lsp */

/* Definitions for CRLSP Tunnel directions */
#define CRLSP_TNL_IN              1
#define CRLSP_TNL_OUT             2
#define CRLSP_TNL_INOUT           3

/* Flags */
#define LDP_TARG_FLAG          0x8000
#define LDP_TX_REQ_FLAG        0x4000
#define LDP_NO_TX_TRGT_HELLOS  0
#define LDP_TX_TRGT_HELLOS     1
#define LDP_FT_TLV_FLAG        0x0001

/* FEC Types */
#define LDP_FEC_INVALID_TYPE   0x00
#define LDP_FEC_WILDCARD_TYPE  0x01
#define LDP_FEC_PREFIX_TYPE    0x02
#define LDP_FEC_HOSTADDR_TYPE  0x03
#define LDP_FEC_CRLSP_TYPE     0x04
#define LDP_FEC_PWVC_TYPE      0x80
#define LDP_FEC_GEN_PWVC_TYPE  0x81

/* Interface Type */
#define LDP_IFACE_ETHERNET_TYPE   0x01
#define LDP_IFACE_ATM_TYPE        0x02
#define LDP_IFACE_FR_TYPE         0x03

/* LSP Internal SETUP - FEC Types */
#define HST_ADDR_FEC_LSP_SETUP 0x01
#define IPPRFX_FEC_LSP_SETUP   0x02
#define NEXT_HOP_CHNG_LSP_SET_UP 0x03    /* LOC_RPR */

/* Message Types */
#define LDP_NOTIF_MSG          0x0001
#define LDP_HELLO_MSG          0x0100
#define LDP_INIT_MSG           0x0200
#define LDP_KEEP_ALIVE_MSG     0x0201
#define LDP_ADDR_MSG           0x0300
#define LDP_ADDR_WITHDRAW_MSG  0x0301
#define LDP_LBL_MAPPING_MSG    0x0400
#define LDP_LBL_REQUEST_MSG    0x0401
#define LDP_LBL_WITHDRAW_MSG   0x0402
#define LDP_LBL_RELEASE_MSG    0x0403
#define LDP_LBL_ABORT_REQ_MSG  0x0404
#define LDP_ICCP_RG_CONNECT_MSG         0x0700
#define LDP_ICCP_RG_DISCONNECT_MSG      0x0701
#define LDP_ICCP_RG_NOTIFICATION_MSG    0x0702
#define LDP_ICCP_RG_DATA_MSG            0x0703


/* TLV Types */
#define LDP_FEC_TLV                 0x0100
#define LDP_ADDRLIST_TLV            0x0101
#define LDP_HOP_COUNT_TLV           0x0103
#define LDP_PATH_VECTOR_TLV         0x0104
#define LDP_GEN_LABEL_TLV           0x0200
#define LDP_ATM_LABEL_TLV           0x0201
#define LDP_FR_LABEL_TLV            0x0202
#define LDP_STATUS_TLV              0x0300
#define LDP_EXTND_STATUS_TLV        0x0301
#define LDP_RETURNED_PDU_TLV        0x0302
#define LDP_RETURNED_MSG_TLV        0x0303
#define LDP_CMN_HELLO_PARM_TLV      0x0400
#define LDP_IPV4_TRANSPORT_ADDR_TLV 0x0401
#define LDP_CONF_SEQNUMB_TLV        0x0402
#define LDP_IPV6_TRANSPORT_ADDR_TLV 0x0403
#define LDP_MAC_LIST_TLV            0x0404
#define LDP_CMN_SSN_PARAM_TLV       0x0500
#define LDP_ATM_SSN_PARAM_TLV       0x0501
#define LDP_FR_SSN_PARAM            0x0502
#define LDP_FT_SSN_TLV              0x0503
#define LDP_LBL_REQ_MSGID_TLV       0x0600
#define LDP_PW_STATUS_TLV           (LDP_PWVC_SET_U_BIT_MASK | 0x096A)
#define LDP_PW_IF_PARAMS_TLV        0x096B
#define LDP_PW_GROUPING_ID_TLV      0x096C
#define LDP_OTHER_MSG_STATUS_TLV    0x8300
#define LDP_ICCP_RG_ID_TLV          0x0005
#define LDP_ICCP_PW_RED_CONFIG_TLV  0x0012
#define LDP_ICCP_SERVICE_NAME_TLV   0x0013
#define LDP_ICCP_PW_ID_TLV          0x0014
#define LDP_ICCP_GEN_PW_ID_TLV      0x0015
#define LDP_ICCP_PW_RED_STATE_TLV   0x0016
#define LDP_ICCP_PW_RED_SYNC_RQST_TLV       0x0017
#define LDP_ICCP_PW_RED_SYNC_DATA_TLV       0x0018

/*  Label Types */
#define LDP_ATM_LABEL          2
#define LDP_GEN_LABEL          1
#define LDP_FR_LABEL           3

/* Status Types */
#define LDP_MAX_STATUS_TYPES            36

#define LDP_STAT_TYPE_SUCCESS             0
#define LDP_STAT_TYPE_BAD_LDPID           1
#define LDP_STAT_TYPE_BAD_PROTO_VER       2
#define LDP_STAT_TYPE_BAD_PDU_LEN         3
#define LDP_STAT_TYPE_UNKNOWN_MSG_TYPE    4
#define LDP_STAT_TYPE_BAD_MSG_LEN         5
#define LDP_STAT_TYPE_UNKNOWN_TLV         6
#define LDP_STAT_TYPE_BAD_TLV_LEN         7
#define LDP_STAT_TYPE_CRPTD_TLV_VAL       8
#define LDP_STAT_TYPE_MALFORMED_TLV_VAL   LDP_STAT_TYPE_CRPTD_TLV_VAL
#define LDP_STAT_TYPE_HOLD_TMR_EXPRD      9
#define LDP_STAT_TYPE_SHUT_DOWN           10
#define LDP_STAT_TYPE_LOOP_DETECTED       11
#define LDP_STAT_TYPE_UNKNOWN_FEC         12
#define LDP_STAT_TYPE_NO_ROUTE            13
#define LDP_STAT_TYPE_NO_LBL_RSRC         14
#define LDP_STAT_TYPE_LBL_RSRC_AVBL       15
#define LDP_STAT_TYPE_NO_HELLO            16
#define LDP_STAT_TYPE_ADVRT_MODE_REJECT   17
#define LDP_STAT_TYPE_MAX_PDULEN_REJECT   18
#define LDP_STAT_TYPE_LBL_RANGE_REJECT    19
#define LDP_STAT_TYPE_KEEPALIVE_EXPRD     20
#define LDP_STAT_TYPE_LBL_REQ_ABRTD       21
#define LDP_STAT_TYPE_MISSING_MSG_PARAM   22
#define LDP_STAT_TYPE_UNSUPP_ADDR_FMLY    23
#define LDP_STAT_TYPE_BAD_KEEPALIVE_TMR   24
#define LDP_STAT_TYPE_INTERNAL_ERROR      25


/* The Error codes for the CRLSP Status is modified to accomodate the newly
 * included Status Error types */

/* CRLSP Status Error codes definitions  */
#define LDP_STAT_TYPE_BAD_EROUTE_TLV_ERR  26
#define LDP_STAT_TYPE_BAD_STRICT_NODE_ERR 27
#define LDP_STAT_TYPE_BAD_LOOSE_NODE_ERR  28
#define LDP_STAT_TYPE_BAD_INIT_ER_HOP_ERR 29
#define LDP_STAT_TYPE_RESOURCE_UNAVAIL    30
#define LDP_STAT_TYPE_TRAF_PARMS_UNAVL    31
#define LDP_STAT_TYPE_CRLSP_SETUP_ABORT   32
#define LDP_STAT_TYPE_CRLSP_MODIFY_NOTSUP 33
#define LDP_STAT_TYPE_CRLSP_PREEMPTED     34


/* The Error codes for the Fault Tolerance Session TLV Implementations */
#define LDP_STAT_TYPE_TEMPORARY_SHUT_DOWN 35


#define LDP_CRLSP_PDR_OFFSET_VAL          8
#define LDP_CRLSP_CDR_OFFSET_VAL          16

#define LDP_IGNORE_ERROR               100    /* this is used to signal invalid
                                                 Hello Msg Header */

/* Status Data Codes */
#define LDP_STAT_SUCCESS           0x00000000
#define LDP_STAT_BAD_LDPID         0x00000001
#define LDP_STAT_BAD_PROT_VER      0x00000002
#define LDP_STAT_BAD_PDU_LEN       0x00000003
#define LDP_STAT_UNKNOWN_MSG_TYPE  0x00000004
#define LDP_STAT_BAD_MSG_LEN       0x00000005
#define LDP_STAT_UNKNOWN_TLV       0x00000006
#define LDP_STAT_BAD_TLV_LEN       0x00000007
#define LDP_STAT_CRPTD_TLV_VAL     0x00000008
#define LDP_STAT_HOLD_TMR_EXPRD    0x00000009
#define LDP_STAT_SHUT_DOWN         0x0000000A
#define LDP_STAT_LOOP_DETECTED     0x0000000B
#define LDP_STAT_UNKNOWN_FEC       0x0000000C
#define LDP_STAT_NO_ROUTE          0x0000000D
#define LDP_STAT_NO_LBL_RSRC       0x0000000E
#define LDP_STAT_LBL_RSRC_AVBL     0x0000000F
#define LDP_STAT_NO_HELLO          0x00000010
#define LDP_STAT_ADVRT_MODE_REJECT 0x00000011
#define LDP_STAT_MAX_PDULEN_REJECT 0x00000012
#define LDP_STAT_LBL_RANGE_REJECT  0x00000013
#define LDP_STAT_KEEPALIVE_EXPRD   0x00000014
#define LDP_STAT_LBL_REQ_ABRTD     0x00000015
#define LDP_STAT_MISSING_MSG_PARAM 0x00000016
#define LDP_STAT_UNSUPP_ADDR_FMLY  0x00000017
#define LDP_STAT_BAD_KEEPALIVE_TMR 0x00000018
#define LDP_STAT_INTERNAL_ERROR    0x00000019
#define LDP_STAT_PW_STATUS        0x00000028  /* Pw Status Code */

/* CRLSP related Status Data definitions  */
#define LDP_STAT_BAD_EROUTE_TLV_ERR  0x04000001
#define LDP_STAT_BAD_STRICT_NODE_ERR 0x04000002
#define LDP_STAT_BAD_LOOSE_NODE_ERR  0x04000003
#define LDP_STAT_BAD_INIT_ER_HOP_ERR 0x04000004
#define LDP_STAT_RESOURCE_UNAVAIL    0x04000005
#define LDP_STAT_TRAF_PARMS_UNAVL    0x04000006
#define LDP_STAT_CRLSP_PREEMPTED     0x04000007
#define LDP_STAT_CRLSP_MODIFY_NOTSUP 0x04000008
#define LDP_STAT_CRLSP_SETUP_ABORT   0x04000015

/* Fault Tolerance related Status Data definitions */
#define LDP_STAT_TEMPORARY_SHUTDOWN  0x00000020

/* Notification Types */
#define LDP_FATAL_ERR_NOTIF        0x80000000
#define LDP_ADVISORY_NOTIF         0x00000000

/* Notification Types based on the Message type in the partial Message
 * returned.
 */
#define LDP_NOTIF_FOR_LBL_REQ         1
#define LDP_NOTIF_FOR_LBL_REQ_ABORT   2
#define LDP_NOTIF_FOR_LBL_MAP         3

#define LDP_HC_VALUE_AT_INGRESS    1
#define LDP_HC_VALUE_AT_EGRESS     1

#define LDP_PDU_LENGTH_500 500

/* Offset from the start of Init Pdu, for getting the receiver's ldp id
 * present in the Common Session Parameters Tlv */
#define RCVR_LDPID_OFFSET  (LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN +   LDP_TLV_HDR_LEN + LDP_PRTCL_VER_LEN + LDP_KA_HOLD_TIME_LEN + 4)

/* Following defintions are added to handle CRLSP related actions */
/* IP Address Length Definitions */

/* CRLDP related */
#define CRLDP_MIN_SET_PRIO   0
#define CRLDP_MAX_SET_PRIO   7
#define CRLDP_MIN_HOLD_PRIO  0
#define CRLDP_MAX_HOLD_PRIO  7

/* Traffic Params related . Check the values assigned for the below traffic 
 * paramters */
#define CRLDP_MIN_PD         10
#define CRLDP_MAX_PD         10000000    /* Max Peak Data Rate on an 
                                            Ethernet Interface */
#define CRLDP_MIN_PB         512
#define CRLDP_MAX_PB         1024
#define CRLDP_MIN_CD         5
#define CRLDP_MAX_CD         256
#define CRLDP_MIN_CB         256
#define CRLDP_MAX_CB         512
#define CRLDP_MIN_EB         64
#define CRLDP_MAX_EB         128

#define CRLDP_UNSPEC_FREQ    0
#define CRLDP_FREQ           1
#define CRLDP_VERY_FREQ      2

#define CRLDP_MIN_FREQ_VAL   0
#define CRLDP_MAX_FREQ_VAL   255

#define CRLDP_MIN_WGT_VAL    1
#define CRLDP_MAX_WGT_VAL    255

/* CRLSP related TLV Type Definitions */
#define CRLDP_EXPLROUTE_TLV 0x0800
#define CRLDP_ERHOP_TY_IPV4 0x0801
#define CRLDP_ERHOP_TY_IPV6 0x0802
#define CRLDP_ER_HOP_ASNUM  0x0803
#define CRLDP_ER_HOP_LSPID  0x0804
#define CRLDP_TRAFPARM_TLV  0x0810
#define CRLDP_PREEMPT_TLV   0x0820
#define CRLDP_LSPID_TLV     0x0821
#define CRLDP_RESCLS_TLV    0x0822
#define CRLDP_PINNING_TLV   0x0823
#define CRLDP_DIFFSER_TLV   0x0901

/* CRLSP ER HOP type Definitions */
#define CRLDP_ERHOP_IPV4    0x01
#define CRLDP_ERHOP_IPV6    0x02
#define CRLDP_ERHOP_ASNUM   0x03
#define CRLDP_ERHOP_LSPID   0x04

/* Traffic parameter Flag definitions */
#define TRAF_PARM_PDR_BMAP  0x01
#define TRAF_PARM_PBS_BMAP  0x02
#define TRAF_PARM_CDR_BMAP  0x04
#define TRAF_PARM_CBS_BMAP  0x08
#define TRAF_PARM_EBS_BMAP  0x10
#define TRAF_PARM_WGT_BMAP  0x20

/* Traffic paramters frequency Definitions */
#define UNSPECIFIED_FREQ    0
#define FREQUENT_FREQ       1
#define VERY_FREQUENT_FREQ  2

/* CRLSP Path type definitions  */
#define STRICT_CRLSP_PATH 0x00
#define LOOSE_CRLSP_PATH  0x80

/* CRLSP Path type definitions - snmp mib values */
#define CRLSP_STRICT_ER 1
#define CRLSP_LOOSE_ER  2

/* CRLSP Route Pinning definitions  */
#define CRLSP_ROUTE_PIN_DISABLE 0x00
#define CRLSP_ROUTE_PIN_ENABLE 0x80

#define CRLSP_DEF_MAX_NXT_HOPS    8
#define CRLSP_TNL_MAX_NXT_HOPS    16

/* CRLSP Configuration for the Resource pointer Definitions */
#define CRLDP_TABLE_DEF_OFFSET  15

/* CRLSP - ER Hop Processing error conditions */
#define NO_ER_HOP_PRESENT     1
#define BAD_ER_HOP_TLV        2
#define NO_ER_HOP_RESOURCE    3
#define UNSUPPRTD_ER_HOP_TLV  4

#define LDP_DSTR_ON_DEMAND       1
#define LDP_DSTR_UNSOLICIT       2
#define LDP_VPI_FOR_UNLBL_TRAFFIC   0
#define LDP_VCI_FOR_UNLBL_TRAFFIC   32

/* MIN/MAX of VPI/VCI - may be changed based on the Target */
#define LDP_ENTITY_MIN_VPI   0
#define LDP_ENTITY_MAX_VPI   100
#define LDP_ENTITY_MIN_VCI   32 
#define LDP_ENTITY_MAX_VCI   100
#define LDP_ENTITY_UNLBL_TRAF_MIN_VPI   0
#define LDP_ENTITY_UNLBL_TRAF_MAX_VPI   100
#define LDP_ENTITY_UNLBL_TRAF_MIN_VCI   32 
#define LDP_ENTITY_UNLBL_TRAF_MAX_VCI   100

#define LDP_MINVPI_MINVAL    0
#define LDP_MINVPI_MAXVAL    4095

#define LDP_MAXVPI_MINVAL    0
#define LDP_MAXVPI_MAXVAL    4095

#define LDP_MINVCI_MINVAL    0
#define LDP_MINVCI_MAXVAL    65535

#define LDP_MAXVCI_MINVAL    0
#define LDP_MAXVCI_MAXVAL    65535

/* Hold Timer MIN/MAX Range in seconds */
#define LDP_KALIVE_MIN_HTIME  1    /* as defined in RFC */
#define LDP_KALIVE_MAX_HTIME  65535    /* based on TCP time out */

/* Hold Timer MIN/MAX Range in seconds */
#define LDP_HELLO_MIN_HTIME   0
#define LDP_HELLO_MAX_HTIME   65535    /* based on RFC infinite value 0xffff */
/* Default CRLSP Tunnel field values. */
#define DEF_CRLSP_TNL_DIR     3    /* inOut */
#define DEF_CRLSP_SGNL_PRTCL  2    /* LDP */
#define DEF_CRLSP_SET_PRIO    4
#define DEF_CRLSP_HOLD_PRIO   4
#define DEF_CRLSP_ERHOP_COUNT 0

/* 
 * All the following CRLSP Traffic related parameters are assigned as
 * zero. They are to suitably defined as required for the target at the
 * time of porting.
 */
#define DEF_CRLSP_TRF_FREQ    0
#define DEF_CRLSP_TRF_WGT     0
#define DEF_CRLSP_TRF_PD      0
#define DEF_CRLSP_TRF_PB      0
#define DEF_CRLSP_TRF_CD      0
#define DEF_CRLSP_TRF_CB      0
#define DEF_CRLSP_TRF_EB      0

/* Definitions  in Pre Emption */
#define TNL_CRLSP_ZERO           0
#define TNL_CRLSP_ONE            1

/*  Maximum tunnel priority  for a CRLSP and a RSVP  tunnel */
#define CRLSP_TNL_MIN_PRIO 7

#define PM_CRLSP_SUCCESS         1
#define PM_CRLSP_FAILURE         0
#define PM_CRLSP_TRUE            1
#define PM_CRLSP_FALSE           0

/* TE related definitions */
#define LDP_DEF_PO_INDEX         1
#define LDP_TE_TNL_DELETE        1
#define LDP_TE_TNL_NOT_DELETE    2
#define LDP_TNL_DELETE           0
#define LDP_TNL_NOT_DELETE       1



#define CRLDP_DATA_TX_UP         1
#define CRLDP_DATA_TX_DOWN       2

#define LDP_LBIT_FLAG 0x8000
#define LDP_NOTIF_STATUS_MASK 0x3fffffff
#define LDP_V_BITS_MASK 0x3000
#define LDP_VPI_MASK 0x0fff
#define LDP_LOOSE_ERHOP_TYPE 0x8000
#define LDP_STRICT_ERHOP_TYPE 0x0000
#define CRLDP_ROUTE_PIN_REQ        0x80000000
#define CRLDP_ROUTE_PIN_NOREQ      0x00000000
#define CRLDP_MODIFY_REQ           0x00000001

#define LDP_BCAST_LSRID          0xffffffff
#define LDP_MCAST_NET_ADDR         0xe0000000
#define LDP_MCAST_NET_INV_ADDR     0xe00000ff
#define LDP_NULL_LSRID             0x00000000


/* Max number of states in the VC Merge Next Hop State machine */
#define LDP_MAX_MRG_NHOP_CHNG_STATES         3

/* Max number of states  & and evenrts in the VC Merge State machine */
#define LDP_MAX_MRG_DNSTR_EVENTS  8
#define LDP_MAX_MRG_DNSTR_STATES  3
#define LDP_MAX_MRG_UPSTR_EVENTS  9
#define LDP_MAX_MRG_UPSTR_STATES  4

/* Following are the states in the VC Merge Next Hop State machine */
#define LDP_MRG_NH_LSM_ST_IDLE               0
#define LDP_MRG_NH_LSM_ST_NEWNH_RETRY        1
#define LDP_MRG_NH_LSM_ST_RESP_AWAIT         2

/* Max number of events in the VC Merge Next Hop State machine */
#define LDP_MAX_MRG_NHOP_CHNG_EVENTS         5
/*Following are the events related to the VC Merge Next Hop State machine */
#define LDP_MRG_NH_LSM_EVT_INT_NEW_NH        0
#define LDP_MRG_NH_LSM_EVT_INT_RETRY_TMOUT   1
#define LDP_MRG_NH_LSM_EVT_INT_DSTR_MAP      2
#define LDP_MRG_NH_LSM_EVT_INT_LSP_NAK       3
#define LDP_MRG_NH_LSM_EVT_INT_DESTROY       4

/* The waiting time after which the LSP through new next hop starts
 * estabilshing.
 */
#define NHOP_MRG_RETRY_TMR_TIMEOUT           1
#define NHOP_MRG_RETRY_TMR_EXPIRED_EVENT     0x00000006

#define FEC_REROUTE_STABLE_TIME 5

#define LDP_TRAP_SESS_THOLD_EXCD        1
#define LDP_TRAP_PVL_MISMH              2
#define LDP_TRAP_SESS_UP                3
#define LDP_TRAP_SESS_DN                4



#define LDP_REGISTER           1
#define LDP_DEREGISTER         0

/* Definitions for 16 bits while performing bit shift operation */
#define LDP_2_BYTE_SHIFT    16
/*Deifinition for value of size of types UINT4 */
#define LDP_SIZE_OF_UINT4   4

#define LDP_DUMP_DIR_INOUT  3    /* inOut */

/*L2VPN Application Id */
#define LDP_APP_L2VPN                               0x1
#define LDP_APP_L2VPN_MASK                          0x00000001

#define LDP_STAT_PWVC_UNASS_TAI                     5
#define LDP_STAT_PWVC_GEN_MISCONFIG_ERR             4
#define LDP_STAT_PWVC_LBL_WR_NOTSUPP                3
#define LDP_STAT_PWVC_ILLEGAL_C_BIT                 2
#define LDP_STAT_PWVC_WRONG_C_BIT                   1
#define LDP_STAT_PWVC_CODE_NONE                     0

#define LDP_RMT_STATUS_NOT_APPL                     1
#define LDP_RMT_STATUS_NOT_YET_KNOWN                2
#define LDP_RMT_STATUS_CAPABLE                      3
#define LDP_RMT_STATUS_NOT_CAPABLE                  4

#define LDP_PWVC_C_BIT_MASK                         0xf000
#define LDP_PWVC_SET_U_BIT_MASK                     0x8000
#define LDP_PWVC_RESET_U_BIT_MASK                   0x7fff
#define LDP_PWVC_GET_TLV_TYPE_MASK                  0x7fff
#define LDP_PWVC_SET_C_BIT_MASK                     0x8000
#define LDP_PWVC_RESET_C_BIT_MASK                   0x7fff
#define LDP_PWVC_GET_PW_TYPE_MASK                   0x7fff

/* MPLS PseudoWire Type */
#define LDP_PWVC_TYPE_FR_DLCI_MARTINI_MODE          0x0001
#define LDP_PWVC_TYPE_ATM_AAL5_VCC_TRANS            0x0002
#define LDP_PWVC_TYPE_ATM_TRANSPARENT_CELL_TRANS    0x0003
#define LDP_PWVC_TYPE_ETHERNET_VLAN                 0x0004
#define LDP_PWVC_TYPE_ETHERNET                      0x0005
#define LDP_PWVC_TYPE_HDLC                          0x0006
#define LDP_PWVC_TYPE_PPP                           0x0007
#define LDP_PWVC_TYPE_CEM                           0x0008
#define LDP_PWVC_TYPE_ATM_VCC_CELL_TRANS            0x0009
#define LDP_PWVC_TYPE_ATM_VPC_CELL_TRANS            0x000a

/* Interface Parameter Sub-TLV Type */
#define LDP_IFPARAM_IF_MTU                          0x01
#define LDP_IFPARAM_MAX_CONC_ATM_CELLS              0x02
#define LDP_IFPARAM_IF_DESC_STR                     0x03
#define LDP_IFPARAM_CEM_PAYLOAD_BYTES               0x04
#define LDP_IFPARAM_CEP_OPTIONS                     0x05
#define LDP_IFPARAM_REQ_VLAN_ID                     0x06
#define LDP_IFPARAM_CEP_BIT_RATE                    0x07
#define LDP_IFPARAM_FR_DLCI_LEN                     0x08
#define LDP_IFPARAM_FRAG_IND                        0x09
#define LDP_IFPARAM_FCS_RETN_IND                    0x0a
#define LDP_IFPARAM_TDM_OPTIONS                     0x0b
#define LDP_IFPARAM_VCCV_PARAM                      0x0c /* VCCV parameters */

/* Attachment Identifier Type */
#define LDP_GEN_PWVC_AII_TYPE                       0x01
#define LDP_GEN_PWVC_AGI_TYPE                       0x01

/* PseudoWire Status Code */
#define LDP_PW_STATUS_FORWARDING                0x00000000
#define LDP_PW_STATUS_NOT_FORWARDING            0x00000001
#define LDP_PW_STATUS_LCL_AC_RX_FAULT            0x00000002
#define LDP_PW_STATUS_LCL_AC_TX_FAULT            0x00000004
#define LDP_PW_STATUS_LCL_PSN_FACING_RX_FAULT     0x00000008
#define LDP_PW_STATUS_LCL_PSN_FACING_TX_FAULT     0x00000010

/* Pseudowire Owner */
#define LDP_PWID_FEC_SIGNALING       2
#define LDP_GEN_FEC_SIGNALING       3

#define LDP_IFPARAM_IF_TYPE1_LEN                    2
#define LDP_IFPARAM_IF_TYPE2_LEN                    4
#define LDP_IFPARAM_IF_TYPE3_LEN                    6
#define LDP_IFPARAM_IF_TYPE4_LEN                    8
#define LDP_IFPARAM_IF_TYPE5_LEN                    10

/* Interface Parameter Sub-TLV - VCCV parameter TLV length. 
 * CC: 2bytes, CV: 2bytes */
#define LDP_IFPARAM_VCCV_PARAM_LEN                  4

#define LDP_IFPARAM_IF_MTU_LEN                      0x04
#define LDP_IFPARAM_REQ_VLAN_ID_LEN                 0x04
#define LDP_PWVC_INVALID_VLAN_ID                    4097

#define LDP_PWVC_FEC_ELEM_HDR_LEN                        4
#define LDP_PWVC_FEC_ELEM_GROUP_ID_LEN                   4
#define LDP_PWVC_FEC_ELEM_PWVCID_LEN                     4
#define LDP_PWVC_FEC_OFFSET_IF_PARAM                    12
#define LDP_PWVC_FEC_IF_PARAM_HDR_LEN                    2
#define LDP_GEN_PWVC_FEC_SUB_HDR_LEN           2
#define LDP_PW_GROUPING_ID_LEN                   4
#define LDP_PW_STATUS_CODE_LEN                   4

#define LDP_ICCP_FILL_SERVICE_NAME_TLV          1
#define LDP_ICCP_FILL_PW_ID_TLV                 2
#define LDP_ICCP_FILL_GEN_PW_ID_TLV             3

#define LDP_ICCP_RG_ID_LEN                      4
#define LDP_ICCP_ROID_LEN                       LDP_LSR_ID_LEN + LDP_PWVC_FEC_ELEM_PWVCID_LEN
#define LDP_ICCP_SYNC_RQST_TYPE_FIELD_LEN       2
#define LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN        2
#define LDP_ICCP_PW_PRIORITY_FIELD_LEN          2
#define LDP_ICCP_PW_RED_FLAG_FIELD_LEN          2
#define LDP_ICCP_PW_STATE_FIELD_LEN             4
#define LDP_ICCP_RG_ID_TLV_LEN                  LDP_TLV_HDR_LEN + LDP_ICCP_RG_ID_LEN
#define LDP_ICCP_PW_RED_REQUEST_TLV_HDR_LEN     LDP_TLV_HDR_LEN + LDP_ICCP_SYNC_RQST_TYPE_FIELD_LEN + \
    LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN
#define LDP_ICCP_PW_RED_RQST_DATA_TLV_HDR_LEN   LDP_TLV_HDR_LEN + LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN + \
    LDP_ICCP_PW_RED_FLAG_FIELD_LEN
#define LDP_ICCP_PW_RED_STATE_TLV_LEN           LDP_TLV_HDR_LEN + LDP_ICCP_ROID_LEN + \
    LDP_ICCP_PW_STATE_FIELD_LEN + LDP_ICCP_PW_STATE_FIELD_LEN
#define LDP_ICCP_PW_RED_CONFIG_TLV_HDR_LEN      LDP_TLV_HDR_LEN + LDP_ICCP_ROID_LEN + \
    LDP_ICCP_PW_PRIORITY_FIELD_LEN + LDP_ICCP_PW_RED_FLAG_FIELD_LEN 
#define LDP_ICCP_PW_ID_TLV_LEN                  LDP_TLV_HDR_LEN + LDP_LSR_ID_LEN + \
    LDP_PWVC_FEC_ELEM_GROUP_ID_LEN + LDP_PWVC_FEC_ELEM_PWVCID_LEN

#define LDP_ICCP_MAX_PW_IN_RG                   4

#define LDP_ICCP_SYNC_DATA_END                  0x01
#define LDP_ICCP_PW_SYNCHRONIZED                0x01
#define LDP_ICCP_PW_PURGED                      0x02

#define LDP_L2VPN_FEC_PWID_TYPE                 0x80
#define LDP_L2VPN_FEC_GEN_TYPE                  0x81

#define LDP_L2VPN_GEN_FEC_AII_TYPE_1            1
#define LDP_L2VPN_GEN_FEC_AII_TYPE_2            2


#define LDP_NORMAL_WITHDRAW                              1
#define LDP_WCARD_WITHDRAW                               2

#define INTERFACE_ADDRESS_TYPE 1
#define LOOPBACK_ADDRESS_TYPE  2

#define LDP_RELINQUISH_CNTR 10

/* GR Related Timers - Ranges in Seconds */

#define LDP_MIN_NBR_LIVENESS_TIME      5
#define LDP_MAX_NBR_LIVENESS_TIME      300
#define LDP_MIN_MAX_RECOVERY_TIME      15
#define LDP_MAX_MAX_RECOVERY_TIME      600
#define LDP_MIN_FWD_HOLDING_TIME       30
#define LDP_MAX_FWD_HOLDING_TIME       600 
#define LDP_DEF_MAX_RECOVERY_TIME      120
#define LDP_DEF_FWD_HOLDING_TIME       600
#define LDP_DEF_NBR_LIVENESS_TIME      120

#define  LDP_MAX_FRAGMENT 65535
#define LDP_PEND_MAX_FRAGMENT 65535

#define  LDP_MAX_UNKNOWN_TLV_LEN       100

#define LDP_MAX_SOCK_READ_COUNT        6000

#define LDP_RM_BLKUPDT_NOT_STARTED    1
#define LDP_RM_BLKUPDT_INPROGRESS     2
#define LDP_RM_BLKUPDT_COMPLETED      3
#define LDP_RM_BLKUPDT_ABORTED        4
#define LDP_HA_IS_SWITCHOVER    if(gRsvpTeGblInfo.rpteRmInfo.u4RpteRmGoActiveReason == RM_STANDBY_TO_ACTIVE_EVT_PROCESSED) (return TRUE)else (return FALSE)
#define LDP_ZERO                      0
#define LDP_ENABLED                   1
#define LDP_DISABLED                  2


#define LDP_IFTYPE_INVALID 0
#define LDP_IFTYPE_IPV4 1
#define LDP_IFTYPE_IPV6 2
#define LDP_IFTYPE_DUAL 3

#define LDP_SSN_TCP_NON_INIT 0
#define LDP_SSN_TCP_INIT      1

#ifdef MPLS_IPV6_WANTED
#define LDP_IPV6_SSN_PREF_TMR_NSTARTED 0
#define LDP_IPV6_SSN_PREF_TMR_STARTED 1
#define LDP_IPV6_SSN_PREF_TMR_EXPIRED 2
#define LDP_IPV6_SSN_PREF_TMR_STOPPED 3
#define LDP_IPV6_SSN_PREF_TIME 5
#endif

#endif /*_LDP_DEFS_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpdefs.h                              */
/*---------------------------------------------------------------------------*/
