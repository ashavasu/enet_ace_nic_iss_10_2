/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgnllw.h,v 1.4 2008/08/20 15:14:46 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for MplsLdpEntityGenericLRTable. */
INT1
nmhValidateIndexInstanceMplsLdpEntityGenericLRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsLdpEntityGenericLRTable  */

INT1
nmhGetFirstIndexMplsLdpEntityGenericLRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLdpEntityGenericLRTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLdpEntityGenericLabelSpace ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityGenericIfIndexOrZero ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityGenericLRStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsLdpEntityGenericLRRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsLdpEntityGenericLabelSpace ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityGenericIfIndexOrZero ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityGenericLRStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsLdpEntityGenericLRRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsLdpEntityGenericLabelSpace ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityGenericIfIndexOrZero ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityGenericLRStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLdpEntityGenericLRRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsLdpEntityGenericLRTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
