/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpvcnsm.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP (ADVT SUB-MODULE)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains Merge capable Next hop state
 *                             machine routines declarations.
 *---------------------------------------------------------------------------*/

#ifndef _LDP_VCNSM_H
#define _LDP_VCNSM_H

/* Max number of states in the VC Merge Next Hop State machine */
tMrgNhopChngFsmFuncPtr
    gLdpMrgNhopChngFsm[LDP_MAX_MRG_NHOP_CHNG_STATES]
    [LDP_MAX_MRG_NHOP_CHNG_EVENTS] =
{
    /* Idle State */
    {
        LdpMrgNhopChngIdlNewNh,          /* Internal new NH event */
        LdpMrgNhopChngInvStateEvt,       /* Internal Retry time out event */
        LdpMrgNhopChngInvStateEvt,       /* Internal LSP Up  event */
        LdpMrgNhopChngInvStateEvt,       /* Internal LSP NAK event */
        LdpMrgNhopChngInvStateEvt        /* Internal Destroy Event */
    }
    ,
        /* New NH Retry state */
    {
        LdpMrgNhopChngRetryNewNh,         /* Internal new NH event */
        LdpMrgNhopChngRetryRetryTmout,    /* Internal Retry time out event */
        LdpMrgNhopChngInvStateEvt,        /* Internal LSP Up  event */
        LdpMrgNhopChngInvStateEvt,        /* Internal LSP NAK event */
        LdpMrgNhopChngRetryDestroy        /* Internal Destroy Event */
    }
    ,
        /* NEW NH Response Await state */
    {
        LdpMrgNhopChngRspAwaitNewNh,      /* Internal new NH event */
        LdpMrgNhopChngInvStateEvt,        /* Internal Retry time out event */
        LdpMrgNhopChngRspAwaitDStrMap,    /* Internal DownStream Mapping */
        LdpMrgNhopChngRspAwaitNak,        /* Internal LSP NAK event */
        LdpMrgNhopChngRspAwaitDestroy     /* Internal Destroy Event */
    }
};


CONST CHR1              *au1LdpMrgNhEvts[LDP_MAX_MRG_NHOP_CHNG_EVENTS] = {
               "NEWNH",
               "RETRY-TMOUT",
               "DSTR_MAP",
               "LSPNAK",
               "LSP-DESTROY"
};

CONST CHR1              *au1LdpMrgNhStates[LDP_MAX_MRG_NHOP_CHNG_STATES] = {
               "IDLE",
               "NEWNH-RETRY",
               "NEWNH-RSPAWT"
};


#endif /*_LDP_VCNSM_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpnhsm2.h                             */
/*---------------------------------------------------------------------------*/
