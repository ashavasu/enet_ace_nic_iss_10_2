/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgnldb.h,v 1.4 2008/08/20 15:14:46 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDGNLDB_H
#define _STDGNLDB_H

UINT1 MplsLdpEntityGenericLRTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdgnl [] ={1,3,6,1,2,1,10,166,7};
tSNMP_OID_TYPE stdgnlOID = {9, stdgnl};


UINT4 MplsLdpEntityGenericLRMin [ ] ={1,3,6,1,2,1,10,166,7,1,1,1,1,1};
UINT4 MplsLdpEntityGenericLRMax [ ] ={1,3,6,1,2,1,10,166,7,1,1,1,1,2};
UINT4 MplsLdpEntityGenericLabelSpace [ ] ={1,3,6,1,2,1,10,166,7,1,1,1,1,3};
UINT4 MplsLdpEntityGenericIfIndexOrZero [ ] ={1,3,6,1,2,1,10,166,7,1,1,1,1,4};
UINT4 MplsLdpEntityGenericLRStorageType [ ] ={1,3,6,1,2,1,10,166,7,1,1,1,1,5};
UINT4 MplsLdpEntityGenericLRRowStatus [ ] ={1,3,6,1,2,1,10,166,7,1,1,1,1,6};


tMbDbEntry stdgnlMibEntry[]= {

{{14,MplsLdpEntityGenericLRMin}, GetNextIndexMplsLdpEntityGenericLRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsLdpEntityGenericLRTableINDEX, 4, 0, 0, NULL},

{{14,MplsLdpEntityGenericLRMax}, GetNextIndexMplsLdpEntityGenericLRTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsLdpEntityGenericLRTableINDEX, 4, 0, 0, NULL},

{{14,MplsLdpEntityGenericLabelSpace}, GetNextIndexMplsLdpEntityGenericLRTable, MplsLdpEntityGenericLabelSpaceGet, MplsLdpEntityGenericLabelSpaceSet, MplsLdpEntityGenericLabelSpaceTest, MplsLdpEntityGenericLRTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityGenericLRTableINDEX, 4, 0, 0, "1"},

{{14,MplsLdpEntityGenericIfIndexOrZero}, GetNextIndexMplsLdpEntityGenericLRTable, MplsLdpEntityGenericIfIndexOrZeroGet, MplsLdpEntityGenericIfIndexOrZeroSet, MplsLdpEntityGenericIfIndexOrZeroTest, MplsLdpEntityGenericLRTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityGenericLRTableINDEX, 4, 0, 0, NULL},

{{14,MplsLdpEntityGenericLRStorageType}, GetNextIndexMplsLdpEntityGenericLRTable, MplsLdpEntityGenericLRStorageTypeGet, MplsLdpEntityGenericLRStorageTypeSet, MplsLdpEntityGenericLRStorageTypeTest, MplsLdpEntityGenericLRTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityGenericLRTableINDEX, 4, 0, 0, "3"},

{{14,MplsLdpEntityGenericLRRowStatus}, GetNextIndexMplsLdpEntityGenericLRTable, MplsLdpEntityGenericLRRowStatusGet, MplsLdpEntityGenericLRRowStatusSet, MplsLdpEntityGenericLRRowStatusTest, MplsLdpEntityGenericLRTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLdpEntityGenericLRTableINDEX, 4, 0, 1, NULL},
};
tMibData stdgnlEntry = { 6, stdgnlMibEntry };
#endif /* _STDGNLDB_H */

