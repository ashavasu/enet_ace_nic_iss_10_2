
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpstevt.h,v 1.21 2016/03/10 13:14:53 siva Exp $
 *
 * Description:This file contains macros for states and events
 *
 *******************************************************************/





/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpstevt.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains macros for states and events
 *---------------------------------------------------------------------------*/

#ifndef _LDP_STEVT_H
#define _LDP_STEVT_H

/* Major Events */
#define LDP_MAX_MAJOR_EVENTS         13
#define LDP_SNMP_EVENT               0x00000001
#define LDP_UDP_EVENT                0x00000002
#define LDP_TCP_EVENT                0x00000003
#define LDP_IP_EVENT                 0x00000004
#define LDP_INTERNAL_EVENT           0x00000005
#define LDP_APP_EVENT                0x00000006
#define LDP_TE_EVENT                 0x00000007
#define LDP_OVER_RSVP_EVENT          0x00000008
#define LDP_RM_EVENT                 0x00000009
#define LDP_GR_LBL_REL_EVENT         0x0000000a
#ifdef MPLS_IPV6_WANTED
#define LDP_IPV6_EVENT               0x0000000b
#define LDP_UDP6_EVENT               0x0000000c
#endif

#ifdef MPLS_LDP_BFD_WANTED
#define LDP_BFD_EVENT                0x0000000d
#endif

#define LDP_MAJOR_EVENT_MASK         0x0000FFFF
#define LDP_MINOR_EVENT_MASK         0xFFFF0000

/* Minor Events */
/* Internal LDP Events */
#define LDP_MAX_LDP_INT_EVENTS       0x00000002
#define LDP_ESTB_LSPS_EVENT          0x00000001
#define LDP_LBL_RES_AVL_EVENT        0x00000002

#ifdef MPLS_LDP_BFD_WANTED
#define LDP_HANDLE_BFD_SESSION_DOWN_EVENT 0x00000003
#endif

/* Timer Events */
#define LDP_MAX_TIMER_EVENTS         10
#define LDP_HELLO_ADJ_EXPRD_EVENT    0x00000000
#define LDP_HELLO_TMR_EXPRD_EVENT    0x00000001
#define LDP_SSN_EXPRD_EVENT          0x00000002
#define LDP_KALIVE_TMR_EVENT         0x00000003
#define LDP_SSN_RETRY_EVENT          0x00000004
#define LDP_SSN_TX_INIT_MSG_EVENT    0x00000005
#define NHOP_RETRY_TMR_EXPIRED_EVENT 0x00000006    /* LOC_RPR */
#define LDP_PERST_EXPIRED_EVENT      0x00000007
#define LDP_TARDY_PEER_EXPIRED_EVENT 0x00000008
#define LDP_FEC_REROUTE_EXPIRED_EVT  0x00000010
#define LDP_GR_RECONNECT_TMR_EXP_EVT   0x00000011
#define LDP_GR_FWD_HOLDING_TMR_EXP_EVT 0x00000012
#define LDP_GR_RECOVERY_TMR_EXP_EVT    0x00000013
#define LDP_TCP_SOCK_READ_TMR_EVT      0x00000015
#define LDP_TCP_RECONNECT_TMR_EVT      0x00000017
#ifdef MPLS_IPV6_WANTED
#define LDP_FRAG_V6DATA_PRCS_TMR_EVT     0x00000018
#define LDP_TCP6_SOCK_READ_TMR_EVT      0x00000019
#define LDP_TCP6_SOCK_WRITE_TMR_EVT     0x00000020
#define LDP_IPV6_SSN_PREF_TMR_EVT                 0x00000021
#endif
/* SNMP Events */
#define LDP_MAX_SNMP_EVENTS          13
#define LDP_INCARN_UP                0x00000000
#define LDP_INCARN_DOWN              0x00000001
#define LDP_ENTITY_UP                0x00000002
#define LDP_ENTITY_DOWN              0x00000003
#define LDP_REMOTE_PEER_DOWN         0x00000004
#define CRLDP_TNL_UP_EVENT           0x00000005
#define CRLDP_TNL_DOWN_EVENT         0x00000006
#define LDP_ENTITY_NOTINSERVICE      0x00000007
#define STATIC_LSP_UP_EVENT          0x00000008
#define STATIC_LSP_DOWN_EVENT        0x00000009
#define STATIC_LSP_NOTINSERV_EVENT   0x00000010
#define LDP_INCARN_NOTINSERVICE      0x00000011
#define LDP_ROUTERID_CHANGE          0x00000012


/* UDP Events */
#define LDP_MAX_UDP_EVENTS           1
#define LDP_UDP_DATA_EVENT           0x00000000
/* MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED           
#define LDP_UDP6_DATA_EVENT        0x00000002
#endif
/* MPLS_IPv6 add end*/

/* TCP Events */
#define LDP_MAX_TCP_EVENTS           3

/* MPLS Events */
#define LDP_MAX_MPLS_EVENTS          1
#define LDP_MPLS_LBL_REQ_EVENT       0x00000000

/* IP Events */
#define LDP_MAX_IP_EVENTS            5
#define LDP_RT_NEW                   0x00000100
#define LDP_RT_NH_CHG                0x00000200
#define LDP_RT_IFACE_CHG             0x00000400
#define LDP_RT_METRIC_CHG            0x00000800
#define LDP_RT_DELETED               0x00001000
#define LDP_IPV6_ADDR_ADD            0x00002000
#define LDP_IPV6_ADDR_DEL            0x00004000
#define LDP_IF_OPER_ENABLE          1
#define LDP_IF_OPER_DISABLE         32

/* States and Events for Session State Machine */
#define LDP_MAX_SESSION_STATES     7
#define LDP_SSM_ST_INVALID         0
#define LDP_SSM_ST_NON_EXISTENT    1
#define LDP_SSM_ST_INITIALIZED     2
#define LDP_SSM_ST_OPEN_RECV       3
#define LDP_SSM_ST_OPEN_SENT       4
#define LDP_SSM_ST_OPERATIONAL     5
#define LDP_SSM_ST_RETRY           6

#define LDP_MAX_SESSION_EVENTS     10
#define LDP_SSM_EVT_INT_INIT_CON   0
#define LDP_SSM_EVT_INT_SEND_INIT  1
#define LDP_SSM_EVT_INT_DESTROY    2
#define LDP_SSM_EVT_RECV_INIT      3
#define LDP_SSM_EVT_KEEP_ALIVE_MSG 4
#define LDP_SSM_EVT_OTH_LDP_MSG    5
#define LDP_SSM_EVT_TIMEOUT        6
#define LDP_SSM_EVT_RECV_NOTIF_MSG 7
#define LDP_SSM_EVT_RETRY          8
#define LDP_SSM_EVT_ICCP_MSG       9
/* LOC_RPR */

#define NHOP_RETRY_TMR_TIMEOUT     1    /* The waiting time after which the
                                           LSP through new next hop starts
                                           estabilshing. */
#define LDP_PERS_TIME_PERD         1    /* The waiting time after which the
                                           retry of LSP takes place if in 
                                           any case establishment fails */
#define LDP_TARDY_TIME_PERD        30     /* The time for which the ingress    
                                           LSR waits for the response to the
                                           come and in case expires the label
                                           abort message is sent for that LCB
                                           and the LCB is deleted */

#define LDP_FRAG_DATA_PRCS_TIME    2     /* The time until which the LDP waits 
                                            for receiving the fragmented buffer.
                                            Upon expiry of this timer, it processess
                                            the partially received buffer */

#define LDP_TCP_SOCK_READ_TIME     2

#define LDP_TCP_RECONNECT_TIME     300    /* TCP Reconnect interval. If the TCP connection
                                            is not getting established till this time,
                                            then the tcp session should be deleted
                                            */

/* This definition is valid only for handling 
 * the label mapping messages in DU and liberal retention mode 
 * when route does not exist in RTM table. 
 * After the route has been installed in the RTM, LDP gets the notification, 
 * Upon this notification MPLS DB (FTN/ILM) programming is done. */ 
#define LDP_DU_DN_MLIB_UPD_NOT_DONE 1
#define LDP_DU_DN_MLIB_UPD_DONE 0

/*
 * States and Events for State Machine of Downstream-on-demand
 * Non-merge ATM LSR
 */
#define LDP_MAX_NON_MRG_STATES    4
#define LDP_LSM_ST_IDLE           0
#define LDP_LSM_ST_RSP_AWT        1
#define LDP_LSM_ST_EST            2
#define LDP_LSM_ST_REL_AWT        3

/* Sub-events for Idle State */
#define LDP_IDLE_REL              1
#define LDP_IDLE_NAKORMAP         2
#define LDP_IDLE_NAKORMAP_REL     3

#define LDP_MAX_NON_MRG_EVENTS    13
#define LDP_LSM_EVT_LBL_REQ       0
#define LDP_LSM_EVT_LBL_MAP       1
#define LDP_LSM_EVT_LBL_REL       2
#define LDP_LSM_EVT_LBL_WTH       3
#define LDP_LSM_EVT_LBL_ABRT      4
#define LDP_LSM_EVT_DN_NAK        5
#define LDP_LSM_EVT_UP_LOST       6
#define LDP_LSM_EVT_DN_LOST       7
#define LDP_LSM_EVT_INT_SETUP     8
#define LDP_LSM_EVT_INT_DSTR      9
#define LDP_LSM_EVT_INT_XCON      10
#define LDP_LSM_EVT_INT_NEW_NH    11
#define LDP_LSM_EVT_PREMPTED      12

/*
 * Events for Downstream State Machine of Downstream-on-demand
 * VC- merge ATM LSR
 */
#define LDP_DSM_EVT_INT_ADD_UPSTR 0
#define LDP_DSM_EVT_LBL_MAP       1
#define LDP_DSM_EVT_LBL_WDRAW     3
#define LDP_DSM_EVT_INT_DSTR_NAK  5
#define LDP_DSM_EVT_INT_DEL_UPSTR 6
#define LDP_DSM_EVT_DSTR_LOST     7

/*
 * Events for Upstream State Machine of Downstream-on-demand
 * VC- merge ATM LSR
 */
#define LDP_USM_EVT_INT_LDP_REQ  0
#define LDP_USM_EVT_INT_DSTR_MAP 1
#define LDP_USM_EVT_LDP_REL      2
#define LDP_USM_EVT_DSTR_WDRAW   3
#define LDP_USM_EVT_USTR_ABRT    4
#define LDP_USM_EVT_DSTR_NAK     5
#define LDP_USM_EVT_USTR_LOST    6
#define LDP_USM_EVT_INT_RE_X     7
#define LDP_USM_EVT_INT_NEW_NH   8

/*
 * States and Events for State Machine of Downstream-on-demand
 * Non-merge Next Hop Change (Local-repair) ATM LSR.
 */

/* LOC_RPR */

#define LDP_MAX_NON_MRG_NHOP_CHNG_STATES 3
#define LDP_NH_LSM_ST_IDLE               0
#define LDP_NH_LSM_ST_NEWNH_RETRY        1
#define LDP_NH_LSM_ST_RESP_AWAIT         2

#define LDP_MAX_NON_MRG_NHOP_CHNG_EVENTS 5
#define LDP_NH_LSM_EVT_INT_NEW_NH        0
#define LDP_NH_LSM_EVT_INT_RETRY_TMOUT   1
#define LDP_NH_LSM_EVT_INT_LSP_UP        2
#define LDP_NH_LSM_EVT_INT_LSP_NAK       3
#define LDP_NH_LSM_EVT_INT_DESTROY       4


/* DU */
#define LDP_MAX_DU_UP_STATES             5
#define LDP_DU_UP_LSM_ST_IDLE            0
#define LDP_DU_UP_LSM_ST_EST             2
#define LDP_DU_UP_LSM_ST_REL_AWT         3
#define LDP_DU_UP_LSM_ST_RSRC_AWT        4

#ifdef LDP_GR_WANTED
#define LDP_MAX_DU_UP_EVENTS             16
#else
#define LDP_MAX_DU_UP_EVENTS             15
#endif

/* The events are not in sequence, because the 
 * same #defines are retained for DOD, VC Merge and
 * DU
 */

#define LDP_DU_UP_LSM_EVT_LREQ           0
#define LDP_DU_UP_LSM_EVT_INT_DN_MAP     1
#define LDP_DU_UP_LSM_EVT_LDP_REL        2
#define LDP_DU_UP_LSM_EVT_INT_DN_WR      3
#define LDP_DU_UP_LSM_EVT_UP_LOST        6
#define LDP_DU_UP_LSM_EVT_DEL_FEC        13 
#define LDP_DU_UP_LSM_EVT_RSRC_AVL       14 
#ifdef LDP_GR_WANTED
#define LDP_DU_UP_LSM_EVT_INT_REFRESH    15
#endif


#define LDP_MAX_DU_DN_STATES             3
#define LDP_DU_DN_LSM_ST_IDLE            0
#define LDP_DU_DN_LSM_ST_EST             2


#define LDP_MAX_DU_DN_EVENTS             15

/* The events are not in sequence, because the 
 * same #defines are retained for DOD, VC Merge and
 * DU
 */
 
#define LDP_DU_DN_LSM_EVT_LDP_MAP        1
#define LDP_DU_DN_LSM_EVT_LDP_WR         3
#define LDP_DU_DN_LSM_EVT_DN_LOST        7
#define LDP_DU_DN_LSM_EVT_NH_CHG         11
#define LDP_DU_DN_LSM_EVT_DEL_FEC        13 

#endif /*_LDP_STEVT_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpstevt.h                             */
/*---------------------------------------------------------------------------*/
