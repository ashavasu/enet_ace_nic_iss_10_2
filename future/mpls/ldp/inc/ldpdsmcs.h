/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 * $Id: ldpdsmcs.h,v 1.6 2013/07/04 13:24:17 siva Exp $
 *
 *********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpdsmcs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : CRLDP-DIFFSERV 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains macros used throughout diffServ module
 *---------------------------------------------------------------------------*/
#ifndef _LDP_DSMCS_H
#define _LDP_DSMCS_H

/* tDiffServElspTlvPrefix macros */

#define LDP_DS_ELSP_TLV_PREFIX_TBIT(ElspTlvPrefix) \
                        ElspTlvPrefix.u1Tbit

#define LDP_DS_ELSP_TLV_PREFIX_RESERVED(ElspTlvPrefix) \
                        ElspTlvPrefix.u1Reserved

#define LDP_DS_ELSP_TLV_PREFIX_RESERVED1(ElspTlvPrefix) \
                        ElspTlvPrefix.u1Reserved1

#define LDP_DS_ELSP_TLV_PREFIX_NO_OF_MAPENTRIES(ElspTlvPrefix) \
                        ElspTlvPrefix.u1NoOfMapEntries
/* tDiffServElspMapEntry macros */

#define LDP_DS_ELSP_TLV_MAPENTRY_RESERVED(ElspTlvMapEntry)\
                        ElspTlvMapEntry.u1Reserved

#define LDP_DS_ELSP_TLV_MAPENTRY_EXP(ElspTlvMapEntry) \
                        ElspTlvMapEntry.u1Exp

#define LDP_DS_ELSP_TLV_MAPENTRY_PHBID(ElspTlvMapEntry) \
                        ElspTlvMapEntry.u2PhbId
/* tDiffServLlspTlv macros */

#define LDP_DS_LLSP_TLV_TBIT(LlspTlv) \
            LlspTlv.u1Tbit

#define LDP_DS_LLSP_TLV_RESERVED(LlspTlv) \
            LlspTlv.u1Reserved

#define LDP_DS_LLSP_TLV_PSC(LlspTlv) \
            LlspTlv.u2Psc

#define LDP_DS_NOOF_EXP_MAP_ENTRIES(pElspSigMapArray ,\
            u2NumberOfEntries, u1MapArrayIndex)\
        for (u1MapArrayIndex=0; u1MapArrayIndex < MAX_DS_EXP;\
                      u1MapArrayIndex++) { \
              if(TE_DS_ELSP_MAPENTRY_ISVALID\
                  (pElspSigMapArray,u1MapArrayIndex) == LDP_DS_ELSP_PHB_VALID) \
                  u2NumberOfEntries++;\
        }

#define LDP_DS_GET_NO_OF_ELSPTP_ENTRIES(pPerOATrfcProfilePointerArray ,\
                                         u1NoOfExpEntries, u1PscIndex)\
         for(u1PscIndex=0; u1PscIndex < LDP_DS_MAX_NO_PSCS; u1PscIndex++) {\
            if(*(pPerOATrfcProfilePointerArray + u1PscIndex) != NULL)\
                u1NoOfExpEntries++;\
         }

#define LDP_DS_GET_PHBID_14TH_BIT(u2PhbId,u1Bit14) \
        u1Bit14 =  (UINT1) ((u2PhbId & LDP_DS_BIT14_MASK) >> 1)

#define LDP_DS_DSCP_FOR_PHBID(u2Var1,u1Var2) \
                    u1Var2 = (UINT1)((u2Var1 & LDP_DS_DSCP_PHBID_MASK ) >> 10)

#define LDP_DS_DSCP_TO_LLSP_PHBID(u1Dscp,u2PhbId) \
        u2PhbId = u1Dscp;\
        u2PhbId = (UINT2)((u2PhbId << 10) | LDP_DS_DSCP_TO_LLSP_PHBID_MASK)

#define DSCP_TO_ELSP_PHBID(u1Dscp,u2PhbId) \
        u2PhbId = u1Dscp;\
        u2PhbId =(UINT2)((u2PhbId << 10) | LDP_DS_DSCP_TO_ELSP_PHBID_MASK)

#define LDP_DS_DSCP_TO_PSC(u1Dscp,u2Psc) \
        u2Psc = u1Dscp;\
        u2Psc = (UINT2)((u2Psc << 10) | LDP_DS_DSCP_TO_PSC_MASK)

/* PoolIds macros */

#define LDP_DS_IS_PRECONF_VALID(x) (gLdpInfo.LdpIncarn[x].DiffServGlobal.u1IsPreConfValid)
#define LDP_DS_PRECONF_ELSP_MAP(x) (gLdpInfo.LdpIncarn[x].DiffServGlobal.aPreConfMap)
#define LDP_DS_PRECONF_MAP_PHB_DSCP(x,y) (gLdpInfo.LdpIncarn[x].DiffServGlobal.aPreConfMap[y].u1ElspPhbDscp)

#define LDP_DS_ELSP_MAP_POOL_ID(u4Incarn) TE_DS_ELSP_MAP_POOL_ID 

/* tDiffServElspTPTlvPrefix macros */
#define LDP_DS_ELSPTP_TLV_PREFIX_RESERVED(ElspTPTlvPrefix)\
            ElspTPTlvPrefix.u2Reserved

#define LDP_DS_ELSPTP_TLV_PREFIX_NOOFTPENTRIES(ElspTPTlvPrefix)\
            ElspTPTlvPrefix.u2NoOfElspTPEntries

/* tLdpDiffServElspTPEntry macros*/
#define LDP_DS_ELSPTP_MAPENTRY_RESERVED2(ElspTPMapEntry)\
            ElspTPMapEntry.u2Reserved

#define LDP_DS_ELSPTP_MAPENTRY_PSC(ElspTPMapEntry)\
            ElspTPMapEntry.u2Psc

#define LDP_DS_ELSPTP_MAPENTRY_FLAGS(ElspTPMapEntry)\
            ElspTPMapEntry.u1Flags

#define LDP_DS_ELSPTP_MAPENTRY_FREQUENCY(ElspTPMapEntry)\
            ElspTPMapEntry.u1Frequency

#define LDP_DS_ELSPTP_MAPENTRY_RESERVED1(ElspTPMapEntry)\
            ElspTPMapEntry.u1Reserved

#define LDP_DS_ELSPTP_MAPENTRY_WEIGHT(ElspTPMapEntry)\
            ElspTPMapEntry.u1Weight

#define LDP_DS_ELSPTP_MAPENTRY_PDR(ElspTPMapEntry)\
            ElspTPMapEntry.u4PeakDataRate

#define LDP_DS_ELSPTP_MAPENTRY_PBS(ElspTPMapEntry)\
            ElspTPMapEntry.u4PeakBurstSize

#define LDP_DS_ELSPTP_MAPENTRY_CDR(ElspTPMapEntry)\
            ElspTPMapEntry.u4CommittedDataRate

#define LDP_DS_ELSPTP_MAPENTRY_CBS(ElspTPMapEntry)\
            ElspTPMapEntry.u4CommittedBurstSize

#define LDP_DS_ELSPTP_MAPENTRY_EBS(ElspTPMapEntry)\
            ElspTPMapEntry.u4ExcessBurstSize

#define LDP_DS_CRLSP_DIFFSERV_PARAMS(pCrlspTnlInfo) \
                LDP_TE_MPLS_DIFFSERV_TNL_INFO (LDP_DS_CRLSP_TE_TNLINFO(pCrlspTnlInfo))

#define LDP_DS_CRLSP_TE_TNLINFO(pCrlspTnlInfo) \
                pCrlspTnlInfo->pTeTnlInfo

/* tDiffServClassTypeTlv macros */

#define LDP_DS_CLASS_TYPE_TLV_RESERVED(ClassTypeTlv)\
            ClassTypeTlv.u2Reserved


#define LDP_DS_CLASS_TYPE_TLV_CLASSTYPE(ClassTypeTlv)\
            ClassTypeTlv.u2ClassType

/* Per OA basis Resource manager related macros */

#define LDP_DS_PER_OA_TRF_PD(CrlspBlk,x) \
                TE_CRLDP_TPARAM_PDR(LDP_DS_PEROATRFC_PARAMS(CrlspBlk,x))

#define LDP_DS_PER_OA_GRP_ID(CrlspBlk,x) \
                CrlspBlk->pPerOATrfcProfilePointerArray[x]->u4RsvTrfcParmPerOAGrpId

#define LDP_DS_GET_PER_OA_RSRC_AVAIL(CrlspBlk,x) \
                    CrlspBlk->pPerOATrfcProfilePointerArray[x]->u1IsRsrcAvail

#define LDP_DS_SET_PER_OA_RSRC_AVAIL(CrlspBlk,x) \
                    CrlspBlk->pPerOATrfcProfilePointerArray[x]->u1IsRsrcAvail= TRUE

#define LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW(pCrlspTnlInfo, x)\
            pCrlspTnlInfo->pPerOATrfcProfilePointerArray[x]

#define LDP_DS_PEROA_TRFC_PROFILE_PSC(pCrlspTnlInfo,x)\
            (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW(pCrlspTnlInfo,x))->u1PscDscp

#define LDP_DS_PEROATRFC_PARAMS(pCrlspTnlInfo, x)\
            (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW(pCrlspTnlInfo, x))->pPerOATrfcParms

/* define flags for the traffic params */

#define LDP_DS_GET_PDR_FLAG_BIT_VAL(CrlspBlk,x) \
            (TE_CRLDP_TPARAM_FLAGS(LDP_DS_PEROATRFC_PARAMS(CrlspBlk,x))\
            & (0x01))

#define LDP_DS_GET_PBS_FLAG_BIT_VAL(CrlspBlk,x) \
            (TE_CRLDP_TPARAM_FLAGS(LDP_DS_PEROATRFC_PARAMS(CrlspBlk,x))\
            & (0x02))

#define LDP_DS_GET_CDR_FLAG_BIT_VAL(CrlspBlk,x) \
            (TE_CRLDP_TPARAM_FLAGS(LDP_DS_PEROATRFC_PARAMS(CrlspBlk,x))\
            & (0x04))

#define LDP_DS_GET_CBS_FLAG_BIT_VAL(CrlspBlk,x) \
            (TE_CRLDP_TPARAM_FLAGS(LDP_DS_PEROATRFC_PARAMS(CrlspBlk,x))\
            & (0x08))

#define LDP_DS_GET_EBS_FLAG_BIT_VAL(CrlspBlk,x) \
            (TE_CRLDP_TPARAM_FLAGS(LDP_DS_PEROATRFC_PARAMS(CrlspBlk,x))\
            & (0x0f))

#define LDP_DS_GET_WHT_FLAG_BIT_VAL(CrlspBlk,x) \
            (TE_CRLDP_TPARAM_FLAGS(LDP_DS_PEROATRFC_PARAMS(CrlspBlk,x))\
            & (0x10))

/* Macro to check that the PHB is supported by the RM */

#define LDP_DS_IS_PHB_SUPPORTED(u1Phb, u1IsSupported, u1Count, u4OutIfIndex) \
                for(u1Count=0; u1Count<LDP_DS_MAX_NO_OF_PHBS; u1Count++) {\
                     if(RM_SUPPORTED_PHB(u4OutIfIndex, u1Count) == u1Phb){ \
                         u1IsSupported = 1; \
                         break;}\
                }\

/* Macro to check that the PSC is supported by the RM */

#define LDP_DS_IS_PSC_SUPPORTED(u1PSC, u1IsSupported, u1Count, u4OutIfindex) \
                 for( u1Count=0; u1Count<LDP_DS_MAX_NO_PSCS ; u1Count++) {\
                      if(RM_SUPPORTED_PSC(u4OutIfIndex, u1Count) == u1PSC) \
                          u1IsSupported = 1; \
                  }

/* Macro to check that the CLassType is supported by the RM */

#define LDP_DS_IS_CLASSTYPE_SUPPORTED(u1ClassType, u1IsSupported,\
                             u1Count, u4OutIfIndex)\
                  for( u1Count=0; u1Count<LDP_DS_MAX_NO_OF_CLASSTYPES ; u1Count++) {\
                      if(RM_SUPPORTED_CLASSTYPE(u4OutIfIndex, u1Count)\
                              == u1ClassType) \
                      u1IsSupported = 1; \
                  }

/* DiffServ memory related macros */

#define LDP_DS_INVALID_POOL_ID LDP_INVALID_POOL_ID


#define LDP_DS_TNL_HOLD_PRIO_LIST(x,y,z) \
        (tTMO_SLL*)(&(gLdpInfo.LdpIncarn[x].aIfaceEntry[y].aDSPerOAHoldPrio[z]))


#define LDP_DS_TUNNELING_MODEL gu1TnlModel 

#define LDP_DS_SUPRESS_WARNING(x) UNUSED_PARAM(x)

#define ldpteDiffServGetPhbPsc  TeSigDiffServGetPhbPsc

#define ldpDiffServGetNextAvailableIndex TeDiffServGetNextAvailableIndex

#define LDP_DS_PARAMS_CLASSTYPE_GRPID(pCrlspTnlInfo)\
             pCrlspTnlInfo->u4RsvClassTypeTrfcParamGrpId

#endif
/*---------------------------------------------------------------------------*/
/*                        End of file ldpdsmcs.h                             */
/*---------------------------------------------------------------------------*/
