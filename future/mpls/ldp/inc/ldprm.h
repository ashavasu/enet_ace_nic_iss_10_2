
/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: ldprm.h,v 1.2 2015/02/05 11:29:59 siva Exp $
 ***
 *** Description: Files contails HA implementation of ldp 
 *** NOTE: This file should included in release packaging.
 *** ***********************************************************************/

#ifndef _LDPRM_H
#define _LDPRM_H

VOID LdpProcessRmEvent(tLdpRmEvtInfo *pRmLdpMsg);
INT4 LdpRmRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen);
VOID  LdpRmProcessBulkReq (VOID);
INT4 LdpRmRegisterWithRM (VOID);
INT4 LdpRmDeRegisterWithRM(VOID);
UINT1 LdpRmGetStandbyNodeCount(VOID);
INT4 LdpRmSendEventToRm (tRmProtoEvt * pEvt);
INT4 LdpRmRelRmMsgMem(UINT1 *pu1Block);
UINT4 LdpRmGetRmNodeState (VOID);
VOID LdpRmApiSendProtoAckToRM (UINT4 u4SeqNum);
INT4 LdpRmSendMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen);
tRmMsg* LdpRmAllocForRmMsg(UINT2 u2Size);
INT4  LdpRmProcessILMHwListSync (tRmMsg * pRmMesg,UINT4 *pu4Offset);
INT4  LdpRmProcessFTNHwListSync (tRmMsg * pRmMsg,UINT4 *pOffset);
VOID LdpRmSendBulkFTNInfo(VOID);
VOID LdpRmProcessBulkILMGblInfo(tRmMsg * pMsg, UINT4 *pu4Offset);
VOID LdpRmProcessBulkFTNGblInfo(tRmMsg * pMsg, UINT4 *pu4Offset);
VOID LdpRmSendBulkAbort(UINT4 u4ErrCode);
VOID LdpRmSetBulkUpdateStatus (VOID);
VOID LdpRmSendBulkILMInfo(VOID);
VOID LdpRmInit (VOID);

enum {
    LDP_RED_BULK_UPDT_REQ_MSG  = RM_BULK_UPDT_REQ_MSG,
    LDP_RED_BULK_UPDATE_MSG ,
    LDP_RED_BULK_UPDT_TAIL_MSG ,
    LDP_RED_ILM_HW_LIST_SYNC_MSG,
    LDP_RED_FTN_HW_LIST_SYNC_MSG
};
enum {
    LDP_RM_INIT = 1,
    LDP_RM_ACTIVE_STANDBY_UP,
    LDP_RM_ACTIVE_STANDBY_DOWN,
    LDP_RM_STANDBY
};

#define LDP_RM_ILM_GBL_MOD     1
#define LDP_RM_FTN_GBL_MOD     2
#define LDP_RM_BULK_UPD_REQ_MSG_LEN 3
#define LDP_RM_MOD_COMPLETED       3
#define LDP_RM_MSG_HDR_SIZE  3
#define LDP_RM_BULK_TAIL_MSG_LEN     3
#define LDP_RM_BULK_LEN_OFFSET     1
#define LDP_RM_BULK_TYPE_OFFSET    1
#define LDP_RM_MAX_BULK_SIZE      1500

#define LDP_RM_PUT_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, u1Value); \
    u4Offset += 1; \
}

#define LDP_RM_PUT_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_DATA_ASSIGN_2_BYTE (pMsg, u4Offset, u2Value); \
    u4Offset += 2; \
}

#define LDP_RM_GET_1_BYTE(pMsg, u4Offset, u1Value) \
{\
    RM_GET_DATA_1_BYTE (pMsg, u4Offset, u1Value); \
    u4Offset += 1; \
}

#define LDP_RM_GET_2_BYTE(pMsg, u4Offset, u2Value) \
{\
    RM_GET_DATA_2_BYTE (pMsg, u4Offset, u2Value); \
    u4Offset += 2; \
}

#define LDP_RM_GET_N_BYTE(psrc, pdest, u4Offset, u4Size) \
{\
    RM_GET_DATA_N_BYTE (psrc, pdest, u4Offset, u4Size); \
    u4Offset +=u4Size; \
}


#define LDP_IS_STANDBY_UP() \
    ((gLdpInfo.ldpRmInfo.u4LdpRmState == LDP_RM_ACTIVE_STANDBY_UP) \
     ? LDP_RM_TRUE : LDP_RM_FALSE)

#define LDP_IS_NODE_ACTIVE()\
    ((gLdpInfo.ldpRmInfo.u4LdpRmState == LDP_RM_ACTIVE_STANDBY_UP || \
      gLdpInfo.ldpRmInfo.u4LdpRmState == LDP_RM_ACTIVE_STANDBY_DOWN) \
     ? LDP_RM_TRUE : LDP_RM_FALSE)

#define BUF_COPY_OVER_CHAIN(pMsgBuf, pu1Data, u4Offset, u4Size) \
    CRU_BUF_Copy_OverBufChain(pMsgBuf, (UINT1 *) pu1Data,u4Offset,u4Size)


#endif
