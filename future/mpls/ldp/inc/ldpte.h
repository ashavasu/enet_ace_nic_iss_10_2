
/********************************************************************
 *                                                                  *
 * $RCSfile: ldpte.h,v $
 *                                                                  *
 * $Date: 2009/04/21 13:23:17 $                                     *
 *                                                                  *
 * $Revision: 1.5 $                                             *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpte.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains macros that map on to the TE related
 *                             constants or function name or a macro in 
 *                             TE module.
 *---------------------------------------------------------------------------*/

#ifndef _LDP_TE_H
#define _LDP_TE_H

/* Events related constants */
#define LDP_TE_TNL_UP                 TE_TNL_UP
#define LDP_TE_TNL_DESTROY            TE_TNL_DESTROY
#define LDP_TE_TNL_DOWN               TE_TNL_DOWN
#define LDP_TE_DATA_TX_ENABLE         TE_DATA_TX_ENABLE
#define LDP_TE_DATA_TX_DISABLE        TE_DATA_TX_DISABLE
#define LDP_TE_GOING_DOWN             TE_GOING_DOWN

/* LDP Protocol Id maintained in TE */
#define LDP_TE_PROT_ID                TE_SIGPROTO_LDP
#define CRLDP_TE_TNL_OWNER_LDP        TE_TNL_OWNER_LDP

/* TE SUCCESS - FAILURE */
#define LDP_TE_SUCCESS                TE_SUCCESS
#define LDP_TE_FAILURE                TE_FAILURE

/* ErHop Address Type constants */ 
#define CRLSP_TE_ERHOP_IPV4_TYPE      TE_ERHOP_IPV4_TYPE
#define CRLSP_TE_ERHOP_IPV6_TYPE      TE_ERHOP_IPV6_TYPE
#define CRLSP_TE_ERHOP_LSPID_TYPE     TE_ERHOP_LSPID_TYPE
#define CRLSP_TE_ERHOP_ASNUM_TYPE     TE_ERHOP_ASNUM_TYPE

/* ErHop Type Constants */
#define CRLSP_TE_STRICT_ER            TE_STRICT_ER
#define CRLSP_TE_LOOSE_ER             TE_LOOSE_ER
#define CRLSP_TE_SSN_IS_PINNED_BIT    TE_SSN_IS_PINNED_BIT

/* Role of the LSR */
#define LDP_TE_INGRESS                TE_INGRESS
#define LDP_TE_INTER                  TE_INTERMEDIATE
#define LDP_TE_EGRESS                 TE_EGRESS

/* Tunnel related macros */
#define CRLSP_TE_TNL_INDEX            TE_TNL_TNL_INDEX
#define CRLSP_TE_TNL_INSTANCE         TE_TNL_TNL_INSTANCE
#define CRLSP_TE_TNL_INGRESS_RTR_ID   TE_TNL_INGRESS_LSRID
#define CRLSP_TE_TNL_EGRESS_RTR_ID    TE_TNL_EGRESS_LSRID
#define CRLSP_TE_TNL_SGNL_PRTCL       TE_TNL_SIGPROTO
#define CRLSP_TE_TNL_SET_PRIO         TE_TNL_SETUP_PRIO
#define CRLSP_TE_TNL_HOLD_PRIO        TE_TNL_HLDNG_PRIO
#define CRLSP_TE_TNL_SSN_ATTR         TE_TNL_SSN_ATTR
#define CRLSP_TE_TNL_HOP_LIST_INDEX   TE_TNL_HOP_TABLE_INDEX
#define CRLSP_TE_TNL_PATH_IN_USE      TE_TNL_PATH_IN_USE
#define CRLSP_TE_TNL_INC_ANY_AFFINITY TE_TNL_INC_ANY_AFFINITY
#define CRLSP_TE_TNL_ROLE             TE_TNL_ROLE
#define CRLSP_TE_TNL_TRFC_PARAM_INDEX TE_TNL_TRFC_PARAM_INDEX
#define CRLSP_TE_TNL_TRFC_PARAM       TE_TNL_TRFC_PARAM
#define CRLSP_TE_TNL_PATH_INFO        TE_TNL_PATH_INFO
#define CRLSP_TE_TNL_HOP_LIST_INFO    TE_TNL_PATH_LIST_INFO 
#define CRLSP_TE_TNL_OWNER            TE_TNL_OWNER 

#define CRLSP_TE_TNL_TPARAM_INDEX     TE_TNL_TPARAM_INDEX
#define CRLSP_TE_TNL_ROW_STATUS       TE_TNL_ROW_STATUS
#define CRLSP_TE_TNL_ADMIN_STATUS     TE_TNL_ADMIN_STATUS
#define CRLSP_TE_TNL_OPER_STATUS      TE_TNL_OPER_STATUS

#define CRLSP_TE_TNL_CTPARAM_PDR      TE_TNL_CTPARAM_PDR
#define CRLSP_TE_TNL_CTPARAM_PBS      TE_TNL_CTPARAM_PBS
#define CRLSP_TE_TNL_CTPARAM_CDR      TE_TNL_CTPARAM_CDR
#define CRLSP_TE_TNL_CTPARAM_CBS      TE_TNL_CTPARAM_CBS
#define CRLSP_TE_TNL_CTPARAM_EBS      TE_TNL_CTPARAM_EBS
#define CRLSP_TE_TNL_CTPARAM_FREQ     TE_TNL_CTPARAM_FREQ
#define CRLSP_TE_TNL_CTPARAM_WEIGHT   TE_TNL_CTPARAM_WEIGHT
#define CRLSP_TE_TNL_CTPARAM_FLAGS    TE_TNL_CTPARAM_FLAGS


#define CRLSP_TE_HOP_LIST             TE_HOP_LIST

/* Path List Structure Related */
#define CRLSP_TE_HOPLIST_INDEX        TE_HOPLIST_INDEX
#define CRLSP_TE_HOP_ROLE             TE_HOP_ROLE
#define CRLSP_TE_HOP_PO_LIST          TE_HOP_PO_LIST

/* PathOption Structure Related */
#define CRLSP_TE_PO_NUM_TUNNELS       TE_PO_NUM_TUNNELS
#define CRLSP_TE_PO_INDEX             TE_PO_INDEX
#define CRLSP_TE_PO_HOP_COUNT         TE_PO_HOP_COUNT

/* ErHop Related */
#define CRLSP_TE_ERHOP_NEXT_NODE      TE_ERHOP_NEXT_NODE
#define CRLSP_TE_ERHOP_INDEX          TE_ERHOP_INDEX
#define CRLSP_TE_ERHOP_ADDR_TYPE      TE_ERHOP_ADDR_TYPE
#define CRLSP_TE_ERHOP_IP_ADDR        TE_ERHOP_IP_ADDR
#define CRLSP_TE_ERHOP_IPV4_ADDR      TE_ERHOP_IPV4_ADDR
#define CRLSP_TE_ERHOP_ADDR_PRFX_LEN  TE_ERHOP_ADDR_PRFX_LEN
#define CRLSP_TE_ERHOP_LSPID          TE_ERHOP_LSPID
#define CRLSP_TE_ERHOP_AS_NUM         TE_ERHOP_AS_NUM
#define CRLSP_TE_ERHOP_TYPE           TE_ERHOP_TYPE
#define CRLSP_TE_ERHOP_ROW_STATUS     TE_ERHOP_ROW_STATUS

/* Traffic Parameter Table Related */
#define CRLSP_TE_TRFC_PARAM_INDEX     TE_TNLRSRC_INDEX
#define CRLSP_TE_TNLRSRC_ROW_STATUS   TE_TNLRSRC_ROW_STATUS
#define CRLSP_TE_TNLRSRC_STORAGE_TYPE TE_TNLRSRC_STORAGE_TYPE
#define CRLSP_TE_CRLDP_TRFC_PARAMS    TE_CRLDP_TRFC_PARAMS
#define CRLSP_TE_RSVPTE_TRFC_PARAMS   TE_RSVPTE_TRFC_PARAMS
#define CRLSP_TE_TRFC_PARAM_NUM_TNLS  TE_TNLRSRC_NUM_OF_TUNNELS
#define CRLSP_TE_TRFC_PARAM_ROLE      TE_TNLRSRC_ROLE


#define CRLSP_TE_CRLDP_TPARAM_PDR     TE_CRLDP_TPARAM_PDR
#define CRLSP_TE_CRLDP_TPARAM_PBS     TE_CRLDP_TPARAM_PBS
#define CRLSP_TE_CRLDP_TPARAM_CDR     TE_CRLDP_TPARAM_CDR
#define CRLSP_TE_CRLDP_TPARAM_CBS     TE_CRLDP_TPARAM_CBS
#define CRLSP_TE_CRLDP_TPARAM_EBS     TE_CRLDP_TPARAM_EBS
#define CRLSP_TE_CRLDP_TPARAM_FREQ    TE_CRLDP_TPARAM_FREQ
#define CRLSP_TE_CRLDP_TPARAM_WEIGHT  TE_CRLDP_TPARAM_WEIGHT
#define CRLSP_TE_CRLDP_TPARAM_FLAGS   TE_CRLDP_TPARAM_FLAGS


/* TE Data Structure related macros */
#define tLdpTeTnlInfo                 tTeTnlInfo
#define tLdpTePathListInfo            tTePathListInfo
#define tLdpTePathInfo                tTePathInfo
#define tLdpTeHopInfo                 tTeHopInfo
#define tLdpTeTrfcParams              tTeTrfcParams
#define tLdpTeCRLDPTrfcParams         tCRLDPTrfcParams

/* TE Function related macros */
#define ldpTeCreateNewTnl             TeSigCreateNewTnl
#define ldpTeDeleteTnlInfo            TeSigDeleteTnlInfo
#define ldpTeCreateTrfcParams         TeSigCreateTrfcParams
#define ldpTeDeleteTrfcParams         TeSigDeleteTrfcParams
#define ldpTeUpdateTrfcParams         TeSigUpdateTrfcParams
#define ldpTeCreateHopListInfo        TeSigCreateHopListInfo
#define ldpTeDeleteHopListInfo        TeSigDeleteHopListInfo
#define ldpTeDeleteHopInfo            TeSigDeleteHopInfo

#endif
/*---------------------------------------------------------------------------*/
/*                        End of file ldpte.h                                */
/*---------------------------------------------------------------------------*/
