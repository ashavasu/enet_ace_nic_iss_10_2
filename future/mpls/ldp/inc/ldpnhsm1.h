
/********************************************************************
 *                                                                  *
 * $RCSfile: ldpnhsm1.h,v $
 *                                                                  *
 * $Date: 2007/02/01 14:57:00 $                                     *
 *                                                                  *
 * $Revision: 1.3 $                                                 *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpnhsm1.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP (ADVT SUB-MODULE)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains Next hop state machine
 *                             routines declarations.
 *---------------------------------------------------------------------------*/

#ifndef _LDP_NHSM1_H
#define _LDP_NHSM1_H

tNonMrgNhopChngFsmFuncPtr
    gLdpNonMrgNhopChngFsm[LDP_MAX_NON_MRG_NHOP_CHNG_STATES]
    [LDP_MAX_NON_MRG_NHOP_CHNG_EVENTS] =
{

    /* Idle State */
    {
        LdpNonMrgNhopChngIdlNewNh,    /* Internal new NH event */
            LdpNonMrgNhopChngInvStateEvt,    /* Internal Retry time out event */
            LdpNonMrgNhopChngInvStateEvt,    /* Internal LSP Up  event */
            LdpNonMrgNhopChngInvStateEvt,    /* Internal LSP NAK event */
            LdpNonMrgNhopChngInvStateEvt    /* Internal Destroy Event */
    }
    ,
        /* New NH Retry state */
    {
        LdpNonMrgNhopChngRetryNewNh,    /* Internal new NH event */
            LdpNonMrgNhopChngRetryRetryTmout,    /* Internal Retry time out event */
            LdpNonMrgNhopChngInvStateEvt,    /* Internal LSP Up  event */
            LdpNonMrgNhopChngInvStateEvt,    /* Internal LSP NAK event */
            LdpNonMrgNhopChngRetryDestroy    /* Internal Destroy Event */
    }
    ,
        /* NEW NH Response Await state */
    {
        LdpNonMrgNhopChngRspAwaitNewNh,    /* Internal new NH event */
            LdpNonMrgNhopChngInvStateEvt,    /* Internal Retry time out event */
            LdpNonMrgNhopChngRspAwaitLspup,    /* Internal LSP Up  event */
            LdpNonMrgNhopChngRspAwaitNak,    /* Internal LSP NAK event */
            LdpNonMrgNhopChngRspAwaitDestroy    /* Internal Destroy Event */
    }
};

#endif /*_LDP_NHSM1_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpnhsm1.h                             */
/*---------------------------------------------------------------------------*/
