/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpclip.h,v 1.14 2014/11/08 11:41:01 siva Exp $
 *
 * Description:This file contains the prototypes of functions 
 *             present in ldpcli.c and ldpcli1.c
 *
 *******************************************************************/




/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Licensee Aricent Inc.ware, 1999-2000
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpclip.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1(Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the prototypes of functions 
 *                             present in ldpcli.c and ldpcli1.c
 *----------------------------------------------------------------------------*/

#ifndef __LDPCLIP_H__
#define __LDPCLIP_H__
#include "ldpincs.h"

PRIVATE 
INT4 LdpCliMplsLdpChangeMode ARG_LIST((INT4 i4MplsLdpMode, UINT4 u4ModeIndex));

PRIVATE
INT4 LdpCliCreateEntity ARG_LIST((tCliHandle CliHandle, UINT4 u4EntityIndex));

PRIVATE 
INT4 LdpCliDeleteEntity ARG_LIST((tCliHandle CliHandle, UINT4 u4EntityIndex));

PRIVATE INT4 LdpCliHelloHoldTime ARG_LIST((tCliHandle CliHandle, 
                                    UINT4 u4HelloHoldTime));

PRIVATE INT4 LdpCliKeepAliveHoldTime ARG_LIST((tCliHandle CliHandle, 
                                       UINT4 u4KeepAliveHoldTime));

PRIVATE INT4 LdpCliMaxHops ARG_LIST((tCliHandle CliHandle, UINT4 i4MaxHops));

PRIVATE INT4
LdpCliTargetedHello ARG_LIST ((tCliHandle CliHandle, INT4 i4TargetPeerFlag,
                                          tGenU4Addr inPeerAddr, UINT4 u4InTnlId, 
                                          UINT4 u4OutTnlId));

PRIVATE INT4 LdpCliLabelDistributionMode ARG_LIST((tCliHandle CliHandle, INT4 i4Mode));

PRIVATE 
INT4 LdpCliLabelRetentionMode ARG_LIST((tCliHandle CliHandle, INT4 i4Mode));

PRIVATE 
INT4 LdpCliLabelAllocationMode ARG_LIST((tCliHandle CliHandle, INT4 i4Mode));

PRIVATE 
INT4 LdpCliLsrId ARG_LIST((tCliHandle CliHandle, UINT4 u4IfType, 
                           UINT4 u4Value, INT4 i4LdpForceOption, 
                           INT4 i4Status));

PRIVATE
INT4 LdpCliTransportAddress ARG_LIST((tCliHandle CliHandle, UINT4 u4TransIfType,
                                      UINT4 u4IfIndex, INT4 i4CmdType));
#ifdef MPLS_IPV6_WANTED
PRIVATE INT4
LdpCliIpv6TransportAddress ARG_LIST((tCliHandle CliHandle, UINT4 u4Ipv6TransIfType,
                                      UINT4 u4IfIndex, INT4 i4CmdType));
#endif

PRIVATE INT4 LdpCliPhpMode ARG_LIST((tCliHandle CliHandle, INT4 i4Mode));

PRIVATE INT4 LdpCliEnableMplsLdpInterface ARG_LIST((tCliHandle CliHandle, 
                                             UINT4  u4MplsInterfaceIndex,
                                  UINT4 i4MinLabel,  UINT4 u4MaxLabel));

PRIVATE INT4 LdpCliDisableMplsLdpInterface ARG_LIST((tCliHandle CliHandle, 
                                        UINT4 u4MinLabel,
                                                 UINT4 u4MaxLabel));

PRIVATE INT4 LdpCliShowLabelRange ARG_LIST((tCliHandle CliHandle));

PRIVATE INT4 LdpCliShowDiscovery ARG_LIST((tCliHandle CliHandle, UINT4 u4Flag));


PRIVATE INT4 LdpCliShowNeighbor ARG_LIST((tCliHandle CliHandle, UINT4 u4Flag,
                                  UINT4 u4IpAddress, UINT4 u4IfIndex));

PRIVATE INT4 LdpCliShowParameters ARG_LIST((tCliHandle CliHandle));

PRIVATE 
INT4 LdpCliMplsLdpEntityChangeRowStatus ARG_LIST((tCliHandle CliHandle, INT4));

PRIVATE INT4 LdpGetInterfaceName ARG_LIST ((tSNMP_OCTET_STRING_TYPE 
                                              *pMplsLdpEntityLdpLsrId, 
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4 u4MinLabelVal,
                                          UINT4 u4MaxLabelVal, UINT1 *pu1IfName,
                                          UINT4 *pu4IpAddr));

PRIVATE INT4 LdpCliDbgResetMsgDumpAttr ARG_LIST ((tCliHandle CliHandle, INT4, INT4));

PRIVATE INT4 LdpCliDbgSetMsgDumpAttr ARG_LIST ((tCliHandle CliHandle, INT4, INT4));

PRIVATE VOID LdpCliDumpMessageDirection(tCliHandle CliHandle, INT4 i4Direction);


/* GR Related Functions */
PRIVATE INT4 LdpCliSetGrCapability ARG_LIST ((tCliHandle CliHandle, INT4 i4GrCapability));
PRIVATE INT4 LdpCliSetMaxRecoveryTime ARG_LIST ((tCliHandle CliHandle, INT4 i4RecoveryTime));
PRIVATE INT4 LdpCliSetFwdHoldTime ARG_LIST ((tCliHandle CliHandle, INT4 i4FwdHoldTime));
PRIVATE INT4 LdpCliSetNbrLivenessTime ARG_LIST ((tCliHandle CliHandle, INT4 i4NbrAliveTime));
PRIVATE VOID LdpCliGrShowRunningConfig ARG_LIST ((tCliHandle CliHandle));
PRIVATE VOID LdpCliDisplayGrConfigurations ARG_LIST ((tCliHandle CliHandle));
PRIVATE INT4 LdpCliDisplayPeerGrConfigurations ARG_LIST 
               ((tCliHandle CliHandle, 
                 tSNMP_OCTET_STRING_TYPE *pMplsLdpEntityLdpLsrId, 
                 UINT4 u4MplsLdpEntityIndex,
                 tSNMP_OCTET_STRING_TYPE *pMplsLdpEntityPeerId));
PRIVATE INT4 LdpCliSetConfigSeqTLVOption (tCliHandle CliHandle, INT4 i4ConfigSeqTlvOption);
PRIVATE INT4 LdpCliSetPathVectorLimit (tCliHandle CliHandle, INT4 i4PathVectorLimit);

/* BFD for LDP functions */
#ifdef MPLS_LDP_BFD_WANTED
PRIVATE INT4 LdpCliBfdStatus (tCliHandle CliHandle, INT4 i4BfdStatus);
#endif 

INT4 LdpCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel);
INT4 LdpCliGetDebugLevelVal(tCliHandle CliHandle, UINT4 *pu4ArgsOne);
#endif /*__LDPCLIP_H__*/
