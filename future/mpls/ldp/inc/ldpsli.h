
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpsli.h,v 1.16 2014/11/08 11:41:01 siva Exp $
 *              
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpsli.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP-TCP UDP Interface
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Definition of constants.
 *---------------------------------------------------------------------------*/

#ifndef _LDPSLI_H
#define _LDPSLI_H


#define LDP_MAX_TCP_UDP_SOCK    (sizeof (tLdpTcpUdpSockInfo) * MAX_LDP_TCP_UDP_SSN_SUPRTD)

/* TCP_LDP_CLIENT Task OS related defintions */
#define LDPTCP_QNAME              "LTCP"
#define LDP_TCP_QDEPTH            30

#define LDP_TCP_BACKLOG  5        /* 
                                 * no. conn. req. can be queued by the system
                                 * while it waits for the server to execute
                                 * accept call.
                                 */

/* Events handled by the LDP_TCP_UDP CLient Task */
#define     LDPTCP_QUE_ENQ_EVENT           0x00000010
#define     LDP_TCP_UDP_SOCK_EVENT         0x00000001
#define     LDP_TCP_UDP_POLL_REQ_EVENT     0x00000100

/* Events associated with TCP socket actions */
#define     LDP_TCP_OPEN_REQ_EVENT     2
#define     LDP_TCP_CLOSE_REQ_EVENT    3
#define     LDP_TCP_SEND_REQ_EVENT     4
#define     LDP_TCP_CON_IND_EVENT      5
#define     LDP_UDP_SOCK_REG_EVENT     6
#define     LDP_TCP_RECV_REQ_EVENT     7
/* MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED
#define     LDP_TCP6_CLOSE_REQ_EVENT    8
#define LDP_V6READ_FD  3
#define LDP_V6WRITE_FD 4
#define     LDP_TCP6_RECV_REQ_EVENT     9
#endif
/* MPLS_IPv6 add end*/

/* LDP_TCP_SOCKET Status */
#define SOCK_FREE        0
#define SOCK_UNCONNECTED 1
#define SOCK_CONNECTING  2
#define SOCK_ESTABLISHED 3
#define SOCK_SERVER      4

/* Miscellaneous Definitions */
#define     LDP_SOCK_MASK         0xffff
#define     NORMAL_DATA           1
#define     CONTROL_DATA          2
#define     LDP_TCP_UDP_POLL_REQ_TIME  0.1
#define     LDP_PDU_VER_OFFSET         2

/* TCP Event types indicated to LDP */
#define LDP_TCP_DATA_RX_EVENT        0x00000001
#define LDP_TCP_CONN_UP_EVENT        0x00000002
#define LDP_TCP_CONN_DOWN_EVENT      0x00000003
#define TCP_SRVR_OPEN_FAILURE        0x00000004
#define TCP_CLI_OPEN_FAILURE         0x00000005

/* Connection Opening Type */
#define TCP_CLI_OPEN     0x01
#define TCP_SERV_OPEN    0x02
#define SLI_UDP_OPEN     0x04

#define  CONNECT_SUCCESS      0x01
#define  CONNECT_INPROGRESS   0x02
#define  CONNECT_FAIL         0x03

/* Definitions required for LDP to interact with UDP using Sockets */
#define     LDP_INVALID_IFINDEX           65535
#define SERV_STD_PORT       646    /* Standard TCP Port for LDP functionality */

#define LDP_READ_FD  1
#define LDP_WRITE_FD 2

/* 
 * Structure defintion to hold socket connection information with respect
 * to the ldpTcpUdpIfaceTask.
 */
typedef struct _tLdpTcpUdpSockInfo
{
    tLdpTimer           SockReadTimer;
    /* MPLS_IPv6 mod start*/
    uGenAddr             DestAddr;
    uGenAddr             SrcAddr;
    UINT4               u4IfIndex;
    UINT1               au1VrfName [VCM_ALIAS_MAX_LEN];
    UINT1               u1SockStatus;
    UINT1               u1SockType; /* TCP/UDP Socket indication flag
                                     * 1 - UDP, 2 - TCP Active Open,
                                     * 4 - TCP Passive Open
                                     */
    UINT2               u2Port;
    UINT4               u4ConnId;
    INT4                i4SockDesc;
    INT4                i4PendWriteLen; /* Length of pending data to 
                                           be written
                                         */
    INT1               *pPendWriteBuf;  /* Pending data to be written */
    INT1               *pRcvBuf;    /*  Added the Rcv Buf and 
                                     *  u2ResidualDataLen for Capturing 
                                     *  Tcp Data when even less than PDU 
                                     *  Header Length 
                                     */
    UINT2               u2SrcPort;
    UINT2               u2ResidualDataLen;
    
    UINT1               u1AddrType;
    UINT1               u1DataFlag;
    UINT1               u1RowStatus;
    BOOL1               bTimerExpired;
    BOOL1               b1IsSockReadFdReqd;

    BOOL1               b1IsWaitingSockWriteReady;
    UINT1               au1Rsvd[2];
}
tLdpTcpUdpSockInfo;

/* This Structure is added for system sizing.
 *  It should not be used for any other purpose.
 */

typedef struct _LdpTcpUdpSockInfoSize
{
    UINT1   au1LdpTcpUdpSockInfo[LDP_MAX_TCP_UDP_SOCK];
}
tLdpTcpUdpSockInfoSize;

typedef struct _tLdpTcpUdpEnqMsgInfo
{
    INT4                i4SockFdOrConnId;
    UINT4               u4Event;
    UINT4               u4MsgType;
    /* MPLS_IPv6 mod start used in case of opening sockets*/
    UINT4               u4SsnTcpOpenIndex;
   /* MPLS_IPv6 mod end*/
    UINT1               *pu1MD5Info;
    /* MPLS_IPv6 mod start used in case of opening sockets*/
    UINT1                u1AddrType;    
    /* MPLS_IPv6 mod end*/
    UINT1               au1Pad[3];
}tLdpTcpUdpEnqMsgInfo;

#define     LDP_CLOSE_SOCKET              close
 /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
#define     IN6_ADDR_ANY                   IN6ADDR_ANY
#endif
 /* MPLS_IPv6 mod end*/ 
#define     IN_ADDR_ANY                   INADDR_ANY
#define     LDP_TCP_MD5_OPTION            TCP_MD5_OPTION

/* Global Variables used for LDP_TCP_UDP_INTERFACE */

#ifdef _LDP_SLI_C
UINT4               gu4CurSocketConns = 0;
tLdpTcpUdpSockInfo *gpLdpTcpUdpSockInfoTab;
#endif


#define LDP_TCP_UDP_SOCK_CLOSE(pLdpTcpUdpSockInfo) \
        LdpTcpUdpSockClose (pLdpTcpUdpSockInfo)

#define LDP_SOCK_EVENT_INDICATE(u4Event, pLdpTcpUdpSockInfo) \
          LdpTcpEventHandler (u4Event, 0, pLdpTcpUdpSockInfo->u4ConnId, NULL); \
          LDP_TCP_UDP_SOCK_CLOSE(pLdpTcpUdpSockInfo)


#ifndef SLI_WANTED
#define LDP_SLI_ERR_COULDNT_WRITE_DATA       -9
#else
#define LDP_SLI_ERR_COULDNT_WRITE_DATA       SLI_ERR_COULDNT_WRITE_DATA
#endif
   
/* LDP - SLI Interface function prototypes - Begin */

/*  Prototypes of routines present in ldpsli.c */
INT4 LdpTcpInitSockInfo ARG_LIST ((VOID));
VOID LdpTcpDeInitSockInfo ARG_LIST ((VOID));
VOID LdpProcessTcpMsg ARG_LIST((VOID));
VOID LdpTcpProcOpenReqEvent ARG_LIST ((tLdpTcpUdpEnqMsgInfo *pLdpTcpUdpEnqMsgInfo));
VOID LdpTcpProcCloseReqEvent ARG_LIST ((tLdpTcpUdpEnqMsgInfo *pLdpTcpUdpEnqMsgInfo));
VOID LdpTcpProcConnIndEvent ARG_LIST((INT4 i4SockDesc, UINT4 u4DestAddr,
                                      UINT2 u2Port, UINT1 u1SockType));
VOID LdpTcpProcRecvReqEvent ARG_LIST ((tLdpTcpUdpEnqMsgInfo *pLdpTcpUdpEnqMsgInfo));
tLdpTcpUdpSockInfo *LdpTcpUdpGetSocket ARG_LIST ((UINT4 u4ConnId,
                                                  INT4 i4SockInfo));
UINT4 LdpTcpUdpGetFreeSocket
ARG_LIST ((tLdpTcpUdpSockInfo ** ppLdpTcpUdpSockInfo));
tLdpTcpUdpSockInfo *
LdpTcpUdpGetSocketFrmDestAdd ARG_LIST ((uGenAddr DestAddr, UINT1 u1AddrType));

/*  Prototypes of routines(related SLI) present in ldpudp.c */
UINT1 udpRegisterSock ARG_LIST ((INT4 i4UdpSockDesc));

VOID  LdpProcessUdpMsgs ARG_LIST((VOID));

/* MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED
VOID LdpProcessIpv6UdpMsgs ARG_LIST((VOID));
VOID LdpTcp6PktInSocket ARG_LIST((INT4 i4SockFd));
VOID LdpTcp6ProcRecvReqEvent ARG_LIST ((tLdpTcpUdpEnqMsgInfo *pLdpTcpUdpEnqMsgInfo));
VOID LdpTcp6ProcConnIndEvent ARG_LIST((INT4 i4SockDesc, tIp6Addr DestAddr,tIp6Addr SrcAddr,
                                      UINT2 u2Port, UINT1 u1SockType));
VOID LdpTcp6PktOutSocket ARG_LIST((INT4 i4SockFd));
#endif
/* MPLS_IPv6 and end*/
UINT1 ldpUdpSockClose ARG_LIST ((INT4 i4SockDesc));
VOID LdpTcpPktInSocket ARG_LIST((INT4 i4SockFd));
VOID LdpTcpPktOutSocket ARG_LIST((INT4 i4SockFd));
INT4 LdpTcpEventSend ARG_LIST((INT4 i4SockFd,UINT4 u4Type));

VOID LdpCheckAndCloseTcpSocket ARG_LIST((tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo));
VOID LdpCheckAndClearSocketInfo ARG_LIST((tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo));
VOID LdpTcpUdpSockClose ARG_LIST((tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo));
UINT4 LdpGetConnIdFromDestIp ARG_LIST((UINT4 u4DestIp));
tLdpTcpUdpSockInfo *LdpGetSockInfoFromConnId ARG_LIST((UINT4 u4ConnId));
UINT4 LdpGetNextConnId ARG_LIST((UINT4 u4CurrConnId));
UINT4 LdpSetNewSockConnection ARG_LIST((UINT4 u4ConnId));
UINT4 LdpCloseSockConnection ARG_LIST((UINT4 u4ConnId));
UINT4 LdpActivateSockConnection ARG_LIST ((UINT4 u4ConnId));
INT4 LdpSendPktOverConnection ARG_LIST ((UINT4 u4ConnId, UINT1 *pMsg, 
                                         INT4 i4MsgLen));

/* Function Prototype - End */

#endif /* _LDPSLI_H */
/*---------------------------------------------------------------------------*/
/*                        End of file ldpsli.h                               */
/*---------------------------------------------------------------------------*/
