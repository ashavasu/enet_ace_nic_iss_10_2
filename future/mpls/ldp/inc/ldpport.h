
/********************************************************************
 *                                                                  *
 * $RCSfile: ldpport.h,v $
 * 
 * $Id: ldpport.h,v 1.9 2014/11/08 11:59:52 siva Exp $
 *                                                                  *
 * $Date: 2014/11/08 11:59:52 $                                     *
 *                                                                  *
 * $Revision: 1.9 $                                                 *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpport.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains macros related to UDP, TCP, SNMP etc.
 *---------------------------------------------------------------------------*/

#ifndef _LDP_PORT_H
#define _LDP_PORT_H

/* UDP related macros */ 
#define LDP_UDP_OPEN          udp_open
#define LDP_UDP_CLOSE         udp_close
#define tLdpUdpToAppParms     t_UDP_TO_APP_MSG_PARMS
#define tLdpAppToUdpParms     t_UDP_SEND_PARMS

#ifdef IP_WANTED
#define LDP_UDP_SEND          UdpSend
#endif /* IP_WANTED */

/* IP related macros */
#define LDP_IS_DEST_OUR_ADDR        LdpIpIsAddrLocal
#define LDP_REGISTER_WITH_IP        IpRegisterHigherLayerProtocol
#define LDP_DEREGISTER_WITH_IP      IpDeRegisterHigherLayerProtocol
#define LDP_GET_ROUTE_FOR_DEST(u4Dest, i4DestMask, u1Tos, pu2RtPort, pu4RtGw) \
                LdpIpUcastRouteQuery ( u4Dest, pu2RtPort, pu4RtGw)
#define LDP_IP_GET_PORT_FROM_IF_INDEX     IpGetPortFromIfIndex
#define LDP_IP_GET_IF_INDEX_FROM_PORT     IpGetIfIndexFromPort
#define LDP_IP_IF_VAL_INDEX               IpifValidateIfIndex
#define LDP_GET_EXACT_IPROUTE_FOR_DEST    LdpIpGetExactRoute
#define LDP_GET_EXACT_ROUTE_INFO_FOR_DEST IpGetExactRtInfo
#define LDP_GET_IFINDEX_FROM_IFID   IpGetIfIndexFromIfId
#define LDP_GET_IFID_FROM_IFINDEX   IpGetIfIdFromIfIndex
#define LDP_REL_UDP_TO_APP_MSG_PARMS(pParms)  \
          IP_PARMS_RELEASE_UDP_TO_APP_MSG_PARMS(pParms)
#define LDP_GET_UDP_TO_APP_MSG_PARMS(pBuf) \
          IP_PARMS_GET_UDP_TO_APP_MSG_PARMS(pBuf)
#define tLdpIpRtInfo                tRtInfo
#define LDP_ROUTE_LOCAL_ID          LOCAL_ID
#define LDP_ROUTE_LOCAL             FSIP_LOCAL
#define LDP_STATIC_RT_INDEX         STATIC_RT_INDEX
#define LDP_IP_INVALID_INDEX        IPIF_INVALID_INDEX

#define LDP_IP_SUCCESS               IP_SUCCESS
#define LDP_IP_FAILURE               IP_FAILURE
#define LDP_UDP_SUCCESS              IP_SUCCESS

#define LDP_IP_BIT_TOS     IP_BIT_TOS    /* Type of Service */
#define LDP_IP_BIT_NXTHOP  IP_BIT_NXTHOP    /* Next hop */
#define LDP_IP_BIT_IFACE   IP_BIT_INTRFAC    /* Interface */
#define LDP_IP_BIT_STATUS  IP_BIT_STATUS    /* Route status */
#define LDP_IP_BIT_METRIC  IP_BIT_METRIC    /* Metric       */
#define LDP_IP_BIT_ALL     IP_BIT_ALL    /* all parameters */
#define LDP_RT_NOT_IN_SERVICE IPFWD_NOT_IN_SERVICE    /* Specifes that row status 
                                                       of tRtInfo is not in service */
/* Cfa Related */
#define LDP_CFA_GET_IF_NAME_FRM_INDEX CfaGetInterfaceNameFromIndex

/* Trie Related */
#define tLdpTrieInParams        tInputParams
#define tLdpTrieOutParams       tScanOutParams
#define LDP_SCAN_IPROUTE_TABLE  TrieScan

/* RIP Related */
#define LDP_IPFWD_TABLE_ROOT    gpIpRtTblRoot
#define LDP_RIP_ID              gi1RipId

/* SNMP related macros */
#define LDP_SEND_SNMP_TRAP   SNMP_AGT_RIF_Notify_V1_Or_V2_Trap
#define LDP_GET_OID_FROM_STR SNMP_AGT_GetOidFromString
#define LDP_FORM_SNMP_VB     SNMP_AGT_FormVarBind

/* MPLS_FM related macros */
#define tLdpLspInfo      tLspInfo


#define ldpCreateLabelSpaceGroup     LblMgrCreateLabelSpaceGroup
#define ldpDeleteLabelSpaceGroup     LblMgrDeleteLabelSpaceGroup
#define ldpGetLblFromLblGroup        lblMgrGetLblFromLblGroup
#define ldpGetNextLblValFromLblGroup lblMgrGetNextLblValFromLblGroup
#define ldpRelLblToLblGroup          LblMgrRelLblToLblGroup
#define ldpCheckLblInLblGroup        LblMgrCheckLblInLblGroup
#define ldpAssignLblToLblGroup		 LblMgrAssignLblToLblGroup
#endif /*_LDP_PORT_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file ldpport.h                              */
/*---------------------------------------------------------------------------*/
