#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 05/11/2001                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureLDP          |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+


# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME         = FutureLDP
PROJECT_BASE_DIR     = ${BASE_DIR}/mpls/ldp
PROJECT_SOURCE_DIR   = ${PROJECT_BASE_DIR}/src
MPLS_INCL_DIR        = $(BASE_DIR)/mpls/mplsinc
MPLS_CMNDB_DIR       = $(BASE_DIR)/mpls/mplsdb
PROJECT_INCLUDE_DIR  = ${PROJECT_BASE_DIR}/inc
MPLS_INDEX_DIR       = $(BASE_DIR)/mpls/mplsrtr/inc
ISS_COMMON_SYS_DIR   = $(BASE_DIR)/ISS/common/system/inc
ISS_ENT_SYS_DIR      = $(BASE_DIR)/ISS/common/system/ent/inc
PROJECT_OBJECT_DIR   = ${PROJECT_BASE_DIR}/obj

PROJECT_COMPILATION_SWITCHES +=

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/ldpadsm1.h \
                $(PROJECT_INCLUDE_DIR)/ldpdbg.h \
                $(PROJECT_INCLUDE_DIR)/ldpdefs.h \
                $(PROJECT_INCLUDE_DIR)/ldpextif.h \
                $(PROJECT_INCLUDE_DIR)/ldpgbl.h \
                $(PROJECT_INCLUDE_DIR)/ldpgblex.h \
                $(PROJECT_INCLUDE_DIR)/ldpincs.h \
                $(PROJECT_INCLUDE_DIR)/ldpmacs.h \
                $(PROJECT_INCLUDE_DIR)/ldpnhsm1.h \
                $(PROJECT_INCLUDE_DIR)/ldpport.h \
                $(PROJECT_INCLUDE_DIR)/ldpprot.h \
                $(PROJECT_INCLUDE_DIR)/ldpsli.h \
                $(PROJECT_INCLUDE_DIR)/ldpstevt.h\
                $(PROJECT_INCLUDE_DIR)/ldptdfs.h\
                $(PROJECT_INCLUDE_DIR)/ldplwinc.h\
                $(PROJECT_INCLUDE_DIR)/ldpvcusm.h\
                $(PROJECT_INCLUDE_DIR)/ldpvcnsm.h\
                $(PROJECT_INCLUDE_DIR)/stdldalw.h\
                $(PROJECT_INCLUDE_DIR)/stdldawr.h\
                $(PROJECT_INCLUDE_DIR)/stdldadb.h \
                $(PROJECT_INCLUDE_DIR)/stdgnldb.h \
                $(PROJECT_INCLUDE_DIR)/stdgnllw.h \
                $(PROJECT_INCLUDE_DIR)/stdgnlwr.h \
                $(PROJECT_INCLUDE_DIR)/stdldplw.h \
                $(PROJECT_INCLUDE_DIR)/stdldpwr.h \
                $(PROJECT_INCLUDE_DIR)/stdldpdb.h \
                $(PROJECT_INCLUDE_DIR)/ldpvcdsm.h \
                $(PROJECT_INCLUDE_DIR)/ldpclip.h \
                $(PROJECT_INCLUDE_DIR)/ldpdudsm.h \
                $(PROJECT_INCLUDE_DIR)/ldpduusm.h \
                $(PROJECT_INCLUDE_DIR)/ldpdsgbl.h

PROJECT_FINAL_INCLUDES_DIRS    = -I$(PROJECT_INCLUDE_DIR) \
                                 -I$(MPLS_INDEX_DIR) \
                                 -I$(MPLS_INCL_DIR) \
                                 -I$(MPLS_CMNDB_DIR) \
                                 -I$(ISS_COMMON_SYS_DIR) \
                                 -I$(ISS_ENT_SYS_DIR)\
                                 $(COMMON_INCLUDE_DIRS)

PROJECT_FINAL_INCLUDE_FILES    = $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES    += $(COMMON_DEPENDENCIES) \
                          $(PROJECT_FINAL_INCLUDE_FILES) \
                          $(MPLS_BASE_DIR)/make.h \
                          $(PROJECT_BASE_DIR)/Makefile \
                          $(PROJECT_BASE_DIR)/make.h


