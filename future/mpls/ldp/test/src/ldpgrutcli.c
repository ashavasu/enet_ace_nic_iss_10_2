/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: ldpgrutcli.c,v 1.1 2014/08/25 14:30:07 siva Exp $
 ***
 *** Description: LDP GR  UT Test cases.
 *** NOTE: This file should not be included in release packaging.
 *** ***********************************************************************/

#include "ldpgrut.h"


UINT4  u4TotalPassCases = 0;
UINT4  u4TotalFailCases = 0;


INT4
cli_process_ldp_gr_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...)
{
    va_list            ap;
    UINT4             *args[MAX_ARGS];
    INT1               argno = 0;
    INT4               i4TestcaseNo ;
    va_start (ap, u4Command);
    UNUSED_PARAM (CliHandle);
    /* Walk through the arguements and store in args array. */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
           break;
    }

    va_end (ap);

    switch (u4Command)

    {
        case CLI_LDP_GR_UT:

            if (args[1] == NULL)
            {
                i4TestcaseNo = 0;
            }
            else
            {
                i4TestcaseNo = *args[1];
            }
            LdpGrUt (CliHandle,i4TestcaseNo);
            break;
    }
    return CLI_SUCCESS;
}


VOID
LdpGrUt (tCliHandle CliHandle, INT4 i4TestType)
{

    if (i4TestType == 0)
    {
        /* Call all the test cases */
        LdpGrExecAllUTCases(CliHandle);
    }
    else
    {
        LdpGrExecUnitTestCase (CliHandle, i4TestType);
    }
}


VOID
LdpGrExecAllUTCases(tCliHandle CliHandle)
{
    UINT4     u4Count = 0;

    for (u4Count = 1; u4Count <= LDP_GR_MAX_UT_CASES; u4Count++)
    {
        LdpGrExecUnitTestCase (CliHandle, u4Count);
    }
}

