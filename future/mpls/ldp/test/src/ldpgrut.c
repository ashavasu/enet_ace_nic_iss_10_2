/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: ldpgrut.c,v 1.1 2014/08/25 14:30:07 siva Exp $
 ***
 *** Description: LDP GR  UT Test cases.
 *** NOTE: This file should not be included in release packaging.
 *** ***********************************************************************/

#include "ldpgrut.h"
#include "mplshwlist.h"

static tFTNHwListEntry   *gpFTNHwListEntry[MAX_MPLSDB_HW_LIST_FTN_ENTRY];
static tILMHwListEntry   *gpILMHwListEntry[MAX_MPLSDB_HW_LIST_ILM_ENTRY];
static UINT1             *pLdpQMsg[MAX_LDP_QDEPTH];


extern struct rbtree *gpFTNHwListRBTree;
extern struct rbtree *gpILMHwListRBTree;

extern tMplsNodeStatus     gMplsNodeStatus;

VOID LdpGrExecUnitTestCase (tCliHandle CliHandle, INT4 i4TestId)
{

    UINT4    u4RetVal = OSIX_FAILURE;

    switch (i4TestId)
    {
        case 1:
        {
            /* Sunny day Case */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that Mpls Hw List "
                                  "Initialisation is successful\n", i4TestId);
            if (MplsHwListInit() == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;   
            }
            break;
        }
  
        case 2:
        {
            /* Sunny Day Case */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeFTNHwListCmpFunc when "
                                  "Address1 < Address2\n", i4TestId);

            tFTNHwListEntry       FTNHwListEntry1;
            tFTNHwListEntry       FTNHwListEntry2;
         
            MEMSET (&FTNHwListEntry1, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FTNHwListEntry2, 0, sizeof (tFTNHwListEntry));

            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry2)) = MPLS_IPV6_ADDR_TYPE;

            if (MplsHwListRbTreeFTNHwListCmpFunc ((tRBElem *) &FTNHwListEntry1, 
                                                  (tRBElem *) &FTNHwListEntry2) == -1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 3:
        {
             /* Sunny Day Case */
             CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeFTNHwListCmpFunc when "
                                  "Address1 > Address2\n", i4TestId);
 
            tFTNHwListEntry       FTNHwListEntry1;
            tFTNHwListEntry       FTNHwListEntry2;

            MEMSET (&FTNHwListEntry1, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FTNHwListEntry2, 0, sizeof (tFTNHwListEntry));

            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry1)) = MPLS_IPV6_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            if (MplsHwListRbTreeFTNHwListCmpFunc ((tRBElem *) &FTNHwListEntry1,
                                                  (tRBElem *) &FTNHwListEntry2) == 1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }
        case 4:
        {
            
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeFTNHwListCmpFunc  when PrefixLen1 > PrefixLen2\n", i4TestId);
            
            tFTNHwListEntry       FTNHwListEntry1;
            tFTNHwListEntry       FTNHwListEntry2;

            MEMSET (&FTNHwListEntry1, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FTNHwListEntry2, 0, sizeof (tFTNHwListEntry));

            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;
 
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry1)) = 32; /* Prefix Len1 = 32 */
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry2)) = 30; /* Prefix Len2 = 30 */

            if (MplsHwListRbTreeFTNHwListCmpFunc ((tRBElem *) &FTNHwListEntry1,
                                                  (tRBElem *) &FTNHwListEntry2) == 1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 5:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeFTNHwListCmpFunc  when PrefixLen1 < PrefixLen2\n", i4TestId);

            tFTNHwListEntry       FTNHwListEntry1;
            tFTNHwListEntry       FTNHwListEntry2;

            MEMSET (&FTNHwListEntry1, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FTNHwListEntry2, 0, sizeof (tFTNHwListEntry));

            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry1)) = 30; /* Prefix Len1 = 30 */
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry2)) = 32; /* Prefix Len2 = 32 */

            if (MplsHwListRbTreeFTNHwListCmpFunc ((tRBElem *) &FTNHwListEntry1,
                                                  (tRBElem *) &FTNHwListEntry2) == -1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 6:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeFTNHwListCmpFunc  when Prefix1 > Prefix2\n", i4TestId);

            tFTNHwListEntry       FTNHwListEntry1;
            tFTNHwListEntry       FTNHwListEntry2;

            UINT4                 u4Prefix1 = 0x02020202; /* IP Address = 1.1.1.1 */
            UINT4                 u4Prefix2 = 0x01010101; /* IP Address = 2.2.2.2 */
            
            MEMSET (&FTNHwListEntry1, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FTNHwListEntry2, 0, sizeof (tFTNHwListEntry));

            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry1)) = 32; /* Prefix Len1 = 32 */
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry2)) = 32; /* Prefix Len2 = 32 */
          

            MEMCPY (&FTNHwListEntry1.Fec.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
            MEMCPY (&FTNHwListEntry2.Fec.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH); 

            if (MplsHwListRbTreeFTNHwListCmpFunc ((tRBElem *) &FTNHwListEntry1,
                                                  (tRBElem *) &FTNHwListEntry2) == 1)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        }

        case 7:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeFTNHwListCmpFunc  when Prefix1 < Prefix2\n", i4TestId);

            tFTNHwListEntry       FTNHwListEntry1;
            tFTNHwListEntry       FTNHwListEntry2;

            UINT4                 u4Prefix1 = 0x01010101; /* IP Address = 1.1.1.1 */
            UINT4                 u4Prefix2 = 0x02020202; /* IP Address = 2.2.2.2 */

            MEMSET (&FTNHwListEntry1, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FTNHwListEntry2, 0, sizeof (tFTNHwListEntry));

            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry1)) = 32; /* Prefix Len1 = 32 */
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry2)) = 32; /* Prefix Len2 = 32 */


            MEMCPY (&FTNHwListEntry1.Fec.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
            MEMCPY (&FTNHwListEntry2.Fec.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);

            if (MplsHwListRbTreeFTNHwListCmpFunc ((tRBElem *) &FTNHwListEntry1,
                                                  (tRBElem *) &FTNHwListEntry2) == -1)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        }
 
        case 8:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeFTNHwListCmpFunc when address type is IPv6\n", i4TestId);

            tFTNHwListEntry       FTNHwListEntry1;
            tFTNHwListEntry       FTNHwListEntry2;

            MEMSET (&FTNHwListEntry1, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FTNHwListEntry2, 0, sizeof (tFTNHwListEntry));

            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry1)) = MPLS_IPV6_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry2)) = MPLS_IPV6_ADDR_TYPE;

            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry1)) = 32; /* Prefix Len1 = 32 */
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry2)) = 32; /* Prefix Len2 = 32 */

            if (MplsHwListRbTreeFTNHwListCmpFunc ((tRBElem *) &FTNHwListEntry1,
                                                  (tRBElem *) &FTNHwListEntry2) == 0)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 9:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeFTNHwListCmpFunc when prefix len, prefix and "
                                  "address type matches (All the keys match)\n", i4TestId);

            tFTNHwListEntry       FTNHwListEntry1;
            tFTNHwListEntry       FTNHwListEntry2;
           
            UINT4                 u4Prefix1 = 0x01010101; /* IP Address = 1.1.1.1 */
            UINT4                 u4Prefix2 = 0x01010101; /* IP Address = 1.1.1.1 */

            MEMSET (&FTNHwListEntry1, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FTNHwListEntry2, 0, sizeof (tFTNHwListEntry));

            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry1)) = 32; /* Prefix Len1 = 32 */
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry2)) = 32; /* Prefix Len2 = 32 */
            
            MEMCPY (&FTNHwListEntry1.Fec.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
            MEMCPY (&FTNHwListEntry2.Fec.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);

            if (MplsHwListRbTreeFTNHwListCmpFunc ((tRBElem *) &FTNHwListEntry1,
                                                  (tRBElem *) &FTNHwListEntry2) == 0)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 10:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeFTNHwListCmpFunc when Address Type is other "
                                  "than Ipv4/Ipv6\n", i4TestId);

            tFTNHwListEntry       FTNHwListEntry1;
            tFTNHwListEntry       FTNHwListEntry2;

            MEMSET (&FTNHwListEntry1, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FTNHwListEntry2, 0, sizeof (tFTNHwListEntry));

            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry1)) = 4; /* Address Type other than IPv4/IPv6 */
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry2)) = 4; /* Address Type other than IPv4/IPv6 */

            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry1)) = 32; /* Prefix Len1 = 32 */
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry2)) = 32; /* Prefix Len2 = 32 */


            if (MplsHwListRbTreeFTNHwListCmpFunc ((tRBElem *) &FTNHwListEntry1,
                                                  (tRBElem *) &FTNHwListEntry2) == 0)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        
        case 11:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when Address Type1 > Address Type2\n" ,i4TestId);
       
            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));
              
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV6_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                 (tRBElem *) &ILMHwListEntry2) == 1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }  
     
        case 12:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when Address Type1 < Address Type2\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV6_ADDR_TYPE;

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == -1)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        }

        case 13:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when PrefixLen1 > PrefixLen2\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 32;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 30;

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == 1)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        } 
   
        case 14:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when PrefixLen1 < PrefixLen2\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 30;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 32;

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == -1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 15:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when Prefix1 > Prefix2\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            UINT4                 u4Prefix1 = 0x02020202;
            UINT4                 u4Prefix2 = 0x01010101;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 32;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 32;

            MEMCPY (&ILMHwListEntry1.Fec.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
            MEMCPY (&ILMHwListEntry2.Fec.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == 1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 16:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when Prefix1 < Prefix2\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            UINT4                 u4Prefix1 = 0x01010101;
            UINT4                 u4Prefix2 = 0x02020202;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 32;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 32;

            MEMCPY (&ILMHwListEntry1.Fec.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
            MEMCPY (&ILMHwListEntry2.Fec.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == -1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }
 
        case 17:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when InInterface1 > InInterface2\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            UINT4                 u4Prefix1 = 0x01010101;
            UINT4                 u4Prefix2 = 0x01010101;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 32;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 32;

            MEMCPY (&ILMHwListEntry1.Fec.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
            MEMCPY (&ILMHwListEntry2.Fec.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);

            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry1)) = 10; /* In-interface1 */
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry2)) = 5;  /* In-interface2 */

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == 1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 18:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when InInterface1 < InInterface2\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            UINT4                 u4Prefix1 = 0x01010101;
            UINT4                 u4Prefix2 = 0x01010101;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 32;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 32;

            MEMCPY (&ILMHwListEntry1.Fec.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
            MEMCPY (&ILMHwListEntry2.Fec.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);

            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry1)) = 5; /* In-interface1 */
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry2)) = 10;  /* In-interface2 */

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == -1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 19:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when Ip address Type is IPv6\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;


            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV6_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV6_ADDR_TYPE;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 32;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 32;

            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry1)) = 5; /* In-interface1 */
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry2)) = 10;  /* In-interface2 */

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == -1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 20:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when InLabel1 > InLabel2\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            UINT4                 u4Prefix1 = 0x01010101;
            UINT4                 u4Prefix2 = 0x01010101;

            UINT4                 u4Label1 = 200;
            UINT4                 u4Label2 = 100;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 32;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 32;

            MEMCPY (&ILMHwListEntry1.Fec.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
            MEMCPY (&ILMHwListEntry2.Fec.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);

            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry1)) = 10; /* In-interface1 */
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry2)) = 10;  /* In-interface2 */
   
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry1)) = u4Label1;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry2)) = u4Label2;

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == 1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }        

        case 21:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when InLabel1 < InLabel2\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            UINT4                 u4Prefix1 = 0x01010101;
            UINT4                 u4Prefix2 = 0x01010101;

            UINT4                 u4Label1 = 100;
            UINT4                 u4Label2 = 200;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 32;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 32;

            MEMCPY (&ILMHwListEntry1.Fec.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
            MEMCPY (&ILMHwListEntry2.Fec.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);

            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry1)) = 10; /* In-interface1 */
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry2)) = 10;  /* In-interface2 */

            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry1)) = u4Label1;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry2)) = u4Label2;           

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == -1)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 22:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when all the keys of"
                                  "ILM Hw List Entry matches\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            UINT4                 u4Prefix1 = 0x01010101;
            UINT4                 u4Prefix2 = 0x01010101;

            UINT4                 u4Label1 = 200;
            UINT4                 u4Label2 = 200;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 32;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 32;

            MEMCPY (&ILMHwListEntry1.Fec.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
            MEMCPY (&ILMHwListEntry2.Fec.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);

            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry1)) = 10; /* In-interface1 */
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry2)) = 10;  /* In-interface2 */

            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry1)) = u4Label1;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry2)) = u4Label2;

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == 0)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }
   
        case 23:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the validity of "
                                  " the RBtree comparison function: "
                                  "MplsHwListRbTreeILMHwListCmpFunc when Addr type"
                                  "is other than IPv4/IPv6\n" ,i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (ILMHwListEntry2));

            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = 4;
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = 4;

            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = 32;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = 32;

            if (MplsHwListRbTreeILMHwListCmpFunc ((tRBElem *) &ILMHwListEntry1,
                                                  (tRBElem *) &ILMHwListEntry2) == 0)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 24:
        {
            /* Sunny Day */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the FTN Hw List Entry "
                                  "is created successfully\n", i4TestId);

            tFTNHwListEntry       *pFTNHwListEntry = NULL;
            pFTNHwListEntry = MplsHwListCreateFTNHwListEntry ();
            if (pFTNHwListEntry != NULL)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            MPLS_DEALLOC_FTN_HW_LIST_ENTRY (pFTNHwListEntry);
            break;
        }

        case 25:
        {
            /* Rainy Day */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the scenario when FTN Hw List Entry "
                                  "creation is not successful\n", i4TestId);

            tFTNHwListEntry     *pFTNHwListEntry = NULL;
            LdpGrUtExhaustFTNHwListMemPool ();
            
            
            
            pFTNHwListEntry = MplsHwListCreateFTNHwListEntry ();
            if (pFTNHwListEntry == NULL)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            LdpGrUtFreeFTNHwListMemPool ();
            break;
        }

        case 26:
        {
            /* Sunny Day */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the ILM Hw List Entry "
                                  "is created successfully\n", i4TestId);

            tILMHwListEntry       *pILMHwListEntry = NULL;
            pILMHwListEntry = MplsHwListCreateILMHwListEntry ();
            if (pILMHwListEntry != NULL)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            
            /* Clean-up */
            MPLS_DEALLOC_ILM_HW_LIST_ENTRY (pILMHwListEntry);
            break;
        }

        case 27:
        {
            /* Rainy Day */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the scenario when ILM Hw List Entry "
                                  "creation is not successful\n", i4TestId);

            tILMHwListEntry     *pILMHwListEntry = NULL;
            LdpGrUtExhaustILMHwListMemPool ();
            
            
            
            pILMHwListEntry = MplsHwListCreateILMHwListEntry ();
            if (pILMHwListEntry == NULL)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            LdpGrUtFreeILMHwListMemPool ();
            break;
        }

        case 28:
        {
            /* Sunny Day */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify whether the FTN Hw List Entry "
                                  "initialization is successful, when Owner is LDP\n", i4TestId);

            tFTNHwListEntry        FTNHwListEntry;
            tFtnEntry              FtnEntry;
            tXcEntry               XcEntry;
            tOutSegment            OutSegEntry;

            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FtnEntry, 0, sizeof (tFtnEntry));
            MEMSET (&XcEntry, 0, sizeof (tXcEntry));
            MEMSET (&OutSegEntry, 0, sizeof (tOutSegment));

            OutSegEntry.u1Owner = MPLS_OWNER_LDP;
            XcEntry.pOutIndex = &OutSegEntry;

            if (MplsHwListInitFTNHwListEntry (&FTNHwListEntry, &FtnEntry, &XcEntry, 1) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }
        
        case 29:
        {
            /* Sunny Day */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify whether the FTN Hw List Entry "
                                  "initialization is successful, when Owner is SNMP\n", i4TestId);

            tFTNHwListEntry        FTNHwListEntry;
            tFtnEntry              FtnEntry;
            tXcEntry               XcEntry;
            tOutSegment            OutSegEntry;

            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FtnEntry, 0, sizeof (tFtnEntry));
            MEMSET (&XcEntry, 0, sizeof (tXcEntry));
            MEMSET (&OutSegEntry, 0, sizeof (tOutSegment));

            OutSegEntry.u1Owner = MPLS_OWNER_SNMP;
            XcEntry.pOutIndex = &OutSegEntry;

            if (MplsHwListInitFTNHwListEntry (&FTNHwListEntry, &FtnEntry, &XcEntry, 1) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }
     
        case 30:
        {
            /* RAINY Day */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the scenario that FTN Hw List Entry "
                                  "initialization should FAIL, when OutSegment entry is NULL\n", i4TestId);

            tFTNHwListEntry        FTNHwListEntry;
            tFtnEntry              FtnEntry;
            tXcEntry               XcEntry;

            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FtnEntry, 0, sizeof (tFtnEntry));
            MEMSET (&XcEntry, 0, sizeof (tXcEntry));

            /* The XC Entry does not point to a Outsegment entry, Hence Init should Fail */

            if (MplsHwListInitFTNHwListEntry (&FTNHwListEntry, &FtnEntry, &XcEntry, 1) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }
   
        case 31:
        {
            /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the scenario that ILM Hw List Entry "
                                  "initialization is successful, when owner is LDP and Outparams are NULL\n", i4TestId);
        
            tILMHwListEntry            ILMHwListEntry;
            tInSegment                 InSegment;
            tXcEntry                   XcEntry;

            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&InSegment, 0, sizeof (tInSegment));
            MEMSET (&XcEntry, 0, sizeof (tXcEntry));

            InSegment.u1Owner = MPLS_OWNER_LDP;
 
            if (MplsHwListInitILMHwListEntry (&ILMHwListEntry, &InSegment, &XcEntry) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 32:
        {
            /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the scenario that ILM Hw List Entry "
                                  "initialization is successful, when owner is SNMP and Outparams are NULL\n", i4TestId);

            tILMHwListEntry            ILMHwListEntry;
            tInSegment                 InSegment;
            tXcEntry                   XcEntry;

            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&InSegment, 0, sizeof (tInSegment));
            MEMSET (&XcEntry, 0, sizeof (tXcEntry));

            InSegment.u1Owner = MPLS_OWNER_SNMP;

            if (MplsHwListInitILMHwListEntry (&ILMHwListEntry, &InSegment, &XcEntry) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 33:
        {
            /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the scenario that ILM Hw List Entry "
                                  "initialization is successful, when owner is SNMP and Outparams NOT NULL\n", i4TestId);

            tILMHwListEntry            ILMHwListEntry;
            tInSegment                 InSegment;
            tXcEntry                   XcEntry;
            tOutSegment                OutSegment;
            UINT4                      u4TnlIfIndex = 0;
            UINT4                      u4L3Intf = 1;

            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&InSegment, 0, sizeof (tInSegment));
            MEMSET (&XcEntry, 0, sizeof (tXcEntry));
            MEMSET (&OutSegment, 0, sizeof (tOutSegment));

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf); /* Create a L3 interface on gig 0/1 */
            if (CfaIfmCreateStackMplsTunnelInterface (u4L3Intf, &u4TnlIfIndex) == CFA_FAILURE)
            {
                break;
            }
            XcEntry.pOutIndex = &OutSegment;
            OutSegment.u4IfIndex = u4TnlIfIndex;
            InSegment.u1Owner = MPLS_OWNER_SNMP;

            if (MplsHwListInitILMHwListEntry (&ILMHwListEntry, &InSegment, &XcEntry) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            CfaIfmDeleteStackMplsTunnelInterface (u4L3Intf, u4TnlIfIndex);
            break;
        }
 
        case 34:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify the scenario that ILM Hw List Entry "
                                  "initialization should FAIL, when fetching valid L3 interface FAILS\n", i4TestId);

            tILMHwListEntry            ILMHwListEntry;
            tInSegment                 InSegment;
            tXcEntry                   XcEntry;
            tOutSegment                OutSegment;

            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&InSegment, 0, sizeof (tInSegment));
            MEMSET (&XcEntry, 0, sizeof (tXcEntry));
            MEMSET (&OutSegment, 0, sizeof (tOutSegment));

            XcEntry.pOutIndex = &OutSegment;
            InSegment.u1Owner = MPLS_OWNER_SNMP;

            if (MplsHwListInitILMHwListEntry (&ILMHwListEntry, &InSegment, &XcEntry) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        }

        case 35:
        {
            /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that New FTN Hw List Entry is adding  "
                                  "successfully to the FTN Hw List\n", i4TestId);
 
            tFTNHwListEntry       FTNHwListEntry;
            tFTNHwListEntry       *pFTNHwListEntry = NULL;
            UINT4                 u4Prefix = 0x01010101;
            tMplsIpAddress        Fec;

            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;            

            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            MEMCPY (&(FTNHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry)) = 32;
 
            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, 0) == MPLS_SUCCESS)
            {
                 u4RetVal = OSIX_SUCCESS;
            }
 
            pFTNHwListEntry = MplsHwListGetFTNHwListEntry(Fec, 32);
            if (pFTNHwListEntry != NULL)
            { 
                /* Clean Up */
                MplsHwListDelFTNHwListEntry (pFTNHwListEntry);
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;
        }
 
        case 36:
        {
             /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that Existing FTN Hw List Entry  "
                                  "is Updated successfully with provided NP bit mask\n", i4TestId);

            tFTNHwListEntry       FTNHwListEntry;
            tFTNHwListEntry       *pFTNHwListEntry = NULL;
            UINT4                 u4Prefix = 0x01010101;
            tMplsIpAddress        Fec;


            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            MEMCPY (&(FTNHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry)) = 32;

            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, 0) == MPLS_FAILURE)
            {
                 break;
            }

            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, 15) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
             
            pFTNHwListEntry = MplsHwListGetFTNHwListEntry (Fec, 32);
            if (pFTNHwListEntry != NULL)
            {
                /* Clean Up */
                MplsHwListDelFTNHwListEntry (pFTNHwListEntry);
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }

            break;

        }

        case 37:
        {
             /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that New ILM Hw List Entry  "
                                  "is added successfully to ILM Hw List\n", i4TestId);

            tILMHwListEntry       ILMHwListEntry;
            tILMHwListEntry       *pILMHwListEntry = NULL;
            UINT4                 u4Prefix = 0x01010101;
            UINT4                 u4Label = 100;
            tMplsIpAddress        Fec;
            uLabel                Label;

            MEMSET (&Label, 0, sizeof (uLabel));
            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));            


            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (&Label.u4GenLbl, &u4Label, sizeof (UINT4));

            MEMCPY (&(ILMHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = 32;
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = 1;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;

            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_SUCCESS)
            {
                 u4RetVal = OSIX_SUCCESS;
            }

            pILMHwListEntry = MplsHwListGetILMHwListEntry (Fec, 32, 1, Label);
            if (pILMHwListEntry != NULL)
            {
                /* Clean Up */
                MplsHwListDelILMHwListEntry (pILMHwListEntry);
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;

        }

        case 38:
        {
             /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that Existing ILM Hw List Entry  "
                                  "is updated successfully\n", i4TestId);

            tILMHwListEntry       ILMHwListEntry;
            tILMHwListEntry       *pILMHwListEntry = NULL;
            UINT4                 u4Prefix = 0x01010101;
            uLabel                Label;
            tMplsIpAddress        Fec;
            UINT4                 u4Label = 100;

            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMSET (&Label, 0, sizeof (uLabel));
           
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            Label.u4GenLbl = u4Label;

 
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMCPY (&(ILMHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = 32;
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = 1;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;

            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
            {
                 break;
            }

            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 3) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            pILMHwListEntry = MplsHwListGetILMHwListEntry (Fec, 32, 1, Label);
            if (pILMHwListEntry != NULL)
            {
                /* Clean Up */
                MplsHwListDelILMHwListEntry (pILMHwListEntry);
            }
            else 
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;
        }

        case 39:
        {
             /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that FTN Hw List Entry  "
                                  "is fetched succesfully based on the FEC and prefix Len\n", i4TestId);


            tFTNHwListEntry       FTNHwListEntry;
            UINT4                 u4Prefix = 0x01010101;
            UINT4                 u4PrefixLen = 32;
            tMplsIpAddress        Fec;
            tFTNHwListEntry       *pFTNHwListEntry = NULL;

            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
             

            MEMCPY (&(FTNHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry)) = u4PrefixLen;

            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, 0) == MPLS_FAILURE)
            {
                 break;
            }
           
            if ((pFTNHwListEntry = MplsHwListGetFTNHwListEntry (Fec, u4PrefixLen)) != NULL)
            {
                 u4RetVal = OSIX_SUCCESS;
            }
  
            if (pFTNHwListEntry != NULL)
            {
               /* Clean-up */
               MplsHwListDelFTNHwListEntry (pFTNHwListEntry);
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;
        }

        case 40:
        {
             /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that ILM Hw List Entry  "
                                  "is fetched succesfully based on the FEC, prefix Len, Ininterface and in label\n", i4TestId);


            tILMHwListEntry       *pILMHwListEntry = NULL;
            tILMHwListEntry       ILMHwListEntry;
            UINT4                 u4Prefix = 0x01010101;
            UINT4                 u4PrefixLen = 32;
            uLabel                Label;
            UINT4                 u4Label = 100;
            UINT4                 u4Interface = 1;
            tMplsIpAddress        Fec;


            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&Label, 0, sizeof (uLabel));
            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (&Label.u4GenLbl, &u4Label, sizeof (UINT4));

            MEMCPY (&(ILMHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u4PrefixLen;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4Interface; 


            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
            {
                 break;
            }

            if ((pILMHwListEntry = MplsHwListGetILMHwListEntry (Fec, u4PrefixLen, u4Interface, Label)) != NULL)
            {
                 u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            if (pILMHwListEntry != NULL)
            {
                MplsHwListDelILMHwListEntry (pILMHwListEntry);
            }
            else
            {
                 u4RetVal = OSIX_FAILURE;
            }
            break;
        }

        case 41:
        {
             /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that ILM Hw List Entry  "
                                  "is fetched succesfully based on the FEC and in interface\n", i4TestId);


            tILMHwListEntry       *pILMHwListEntry = NULL;
            tILMHwListEntry       ILMHwListEntry;
            UINT4                 u4Prefix = 0x01010101;
            UINT4                 u4PrefixLen = 32;
            UINT4                 u4Label = 100;
            UINT4                 u4Interface = 1;
            tMplsIpAddress        Fec;

            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;


            MEMCPY (&(ILMHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u4PrefixLen;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4Interface;


            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
            {
                 break;
            }

            if ((pILMHwListEntry = MplsGetILMHwListEntryFromFecAndInIntf (Fec, u4PrefixLen, u4Interface)) != NULL)
            {
                 u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            if (pILMHwListEntry != NULL)
            {
                MplsHwListDelILMHwListEntry (pILMHwListEntry);
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;
        }

        case 42:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsGetILMHwListEntryFromFecAndInIntf "
                                  "returns NULL when the fecthed entry has a different prefix\n", i4TestId);


            tILMHwListEntry       *pILMHwListEntry = NULL;
            tILMHwListEntry       ILMHwListEntry;
            UINT4                 u4Prefix = 0x02020202;
            UINT4                 u4SearchPrefix = 0x01010101;
            UINT4                 u4PrefixLen = 32;
            UINT4                 u4Label = 100;
            UINT4                 u4Interface = 1;
            tMplsIpAddress        Fec;

            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4SearchPrefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

            MEMCPY (&(ILMHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u4PrefixLen;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4Interface;


            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
            {
                 break;
            }

            if (MplsGetILMHwListEntryFromFecAndInIntf (Fec, u4PrefixLen, u4Interface) == NULL)
            {
                 u4RetVal = OSIX_SUCCESS;
            }

            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            /* Clean-up */
            if ((pILMHwListEntry = MplsGetILMHwListEntryFromFecAndInIntf (Fec, u4PrefixLen, u4Interface)) !=  NULL)
            {
                MplsHwListDelILMHwListEntry (pILMHwListEntry);
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;
        }

        case 43:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that ILM hw List entry is NULL "
                                  "when ILM Hw List is empty\n", i4TestId);

            tMplsIpAddress        Fec;
            UINT4                 u4Prefix = 0x02020202;

            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
 

            if (MplsGetILMHwListEntryFromFecAndInIntf (Fec, 32, 1) == NULL)
            {
                 u4RetVal = OSIX_SUCCESS;
            }

            break;
        }

        case 44:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsGetILMHwListEntryFromFecAndInIntf "
                                  "returns NULL when the fecthed entry has a different prefix len\n", i4TestId);

            tILMHwListEntry       *pILMHwListEntry = NULL;
            tILMHwListEntry       ILMHwListEntry;
            UINT4                 u4Prefix = 0x01010101;
            UINT4                 u4PrefixLen = 32;
            UINT4                 u4SearchPrefixLen = 30;
            UINT4                 u4Label = 100;
            UINT4                 u4Interface = 1;
            tMplsIpAddress        Fec;

            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;


            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMCPY (&(ILMHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u4PrefixLen;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4Interface;


            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
            {
                 break;
            }

            if (MplsGetILMHwListEntryFromFecAndInIntf (Fec, u4SearchPrefixLen, u4Interface) == NULL)
            {
                 u4RetVal = OSIX_SUCCESS;
            }

            pILMHwListEntry = MplsGetILMHwListEntryFromFecAndInIntf (Fec, u4PrefixLen, u4Interface);
            if (pILMHwListEntry != NULL)
            {
                /* Clean-up */
                MplsHwListDelILMHwListEntry (pILMHwListEntry);
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;
        }

        case 45:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that FTN H/w List Entry Deletion should FAIL "
                                  "when RBTree Remove fails\n", i4TestId);

            
            struct rbtree         *pTempFTNHwList = NULL;
            tFTNHwListEntry       FTNHwListEntry;
            
            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            pTempFTNHwList = gpFTNHwListRBTree;
 
            /* Simulate RBTree failure */
            gpFTNHwListRBTree = NULL;
            if (MplsHwListDelFTNHwListEntry (&FTNHwListEntry) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS; 
            }
 
            /* Clean-up */
            gpFTNHwListRBTree = pTempFTNHwList;
            break; 
        }

        case 46:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that ILM H/w List Entry Deletion should FAIL "
                                  "when RBTreeRemove fails\n", i4TestId);
                               
            tILMHwListEntry       ILMHwListEntry;  
            struct rbtree         *pTempILMHwList = NULL;

            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            pTempILMHwList = gpILMHwListRBTree;

            /* Simulate RB-Tree Failure */
            gpILMHwListRBTree = NULL;
            if (MplsHwListDelILMHwListEntry (&ILMHwListEntry) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            gpILMHwListRBTree = pTempILMHwList;
            break;
        }
       
        case 47:
        {
            /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that first FTN H/w list entry should be fetched "
                                  "successfully\n", i4TestId);

            tFTNHwListEntry       *pFTNHwListEntry = NULL;
            tFTNHwListEntry       FTNHwListEntry;
            UINT4                 u4Prefix = 0x01010101;
            UINT4                 u4PrefixLen = 32;


            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));

            MEMCPY (&(FTNHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry)) = u4PrefixLen;

            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, 0) == MPLS_FAILURE)
            {
                 break;
            }

            
            if ((pFTNHwListEntry = MplsHwListGetFirstFTNHwListEntry ()) != NULL)
            {
                u4RetVal = OSIX_SUCCESS;
            }
 
            if (pFTNHwListEntry != NULL)
            {
                /* Clean-up */
                MplsHwListDelFTNHwListEntry (pFTNHwListEntry);
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;
        }

        case 48:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that first FTN Hw list should be NULL"
                                  "if FTN hw list is empty\n", i4TestId);

            if (MplsHwListGetFirstFTNHwListEntry () == NULL)
            {
                 u4RetVal = OSIX_SUCCESS;
            }
            break;
        }
    
        case 49:   
        {
            /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that first ILM H/w list entry should be fetched "
                                  "successfully\n", i4TestId);

            tILMHwListEntry       *pILMHwListEntry = NULL;
            tILMHwListEntry       ILMHwListEntry;
            UINT4                 u4Prefix = 0x01010101;
            UINT4                 u4PrefixLen = 32;


            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            
            MEMCPY (&(ILMHwListEntry.Fec.IpAddress), &u4Prefix, IPV4_ADDR_LENGTH);
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u4PrefixLen;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = 100;
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = 1;

            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
            {
                 break;
            }

            if ((pILMHwListEntry = MplsHwListGetFirstILMHwListEntry ()) != NULL)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            if (pILMHwListEntry != NULL)
            {
                /* Clean-up */
                MplsHwListDelILMHwListEntry (pILMHwListEntry); 
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;
        }

        case 50:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that first ILM Hw list should be NULL"
                                  "if ILM hw list is empty\n", i4TestId);

            if (MplsHwListGetFirstILMHwListEntry () == NULL)
            {
                 u4RetVal = OSIX_SUCCESS;
            }
            break;
        }
 
        case 51:
        {
            /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that the next FTN Hw List Entry is fetched "
                                  "successfully from FTN hw list\n", i4TestId);

            tFTNHwListEntry       *pFTNHwListEntry1 = NULL;
            tFTNHwListEntry       *pFTNHwListEntry2 = NULL;
            tFTNHwListEntry        FTNHwListEntry1;
            tFTNHwListEntry        FTNHwListEntry2;

            UINT4                 u4Prefix1 = 0x01010101;
            UINT4                 u4PrefixLen1 = 32;
            UINT4                 u4Prefix2 = 0x02020202;
            UINT4                 u4PrefixLen2 = 32;

            MEMSET (&FTNHwListEntry1, 0, sizeof (tFTNHwListEntry));
            MEMSET (&FTNHwListEntry2, 0, sizeof (tFTNHwListEntry));


            MEMCPY (&(FTNHwListEntry1.Fec.IpAddress), &u4Prefix1, IPV4_ADDR_LENGTH);
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry1)) = u4PrefixLen1;
 
            MEMCPY (&(FTNHwListEntry2.Fec.IpAddress), &u4Prefix2, IPV4_ADDR_LENGTH);
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&FTNHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry2)) = u4PrefixLen2;


            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry1, 0) == MPLS_FAILURE)
            {
                 break;
            }
            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry2, 0) == MPLS_FAILURE)
            {
                 break;
            }

            pFTNHwListEntry1 = MplsHwListGetFirstFTNHwListEntry ();
            if (pFTNHwListEntry1 != NULL)
            {
                pFTNHwListEntry2 = MplsHwListGetNextFTNHwListEntry (pFTNHwListEntry1);
                if (pFTNHwListEntry2 != NULL)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            }

            if (pFTNHwListEntry1 != NULL && pFTNHwListEntry2 != NULL)
            {
                /* Clean-up */
                MplsHwListDelFTNHwListEntry (pFTNHwListEntry1);
                MplsHwListDelFTNHwListEntry (pFTNHwListEntry2);
            } 
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;
        }

        case 52:
        {
            /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that the next ILM Hw List Entry is fetched "
                                  "successfully from the ILM hw list\n", i4TestId);

            tILMHwListEntry       ILMHwListEntry1;
            tILMHwListEntry       ILMHwListEntry2;
            tILMHwListEntry       *pILMHwListEntry1 = NULL;
            UINT4                 u4Prefix1 = 0x01010101;
            UINT4                 u4PrefixLen1 = 32;
            tILMHwListEntry       *pILMHwListEntry2 = NULL;
            UINT4                 u4Prefix2 = 0x02020202;
            UINT4                 u4PrefixLen2 = 32;

            MEMSET (&ILMHwListEntry1, 0, sizeof (tILMHwListEntry));
            MEMSET (&ILMHwListEntry2, 0, sizeof (tILMHwListEntry));


            MEMCPY (&(ILMHwListEntry1.Fec.IpAddress), &u4Prefix1, IPV4_ADDR_LENGTH);
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry1)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry1)) = u4PrefixLen1;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry1)) = 100;
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry1)) = 1;

            MEMCPY (&(ILMHwListEntry2.Fec.IpAddress), &u4Prefix2, IPV4_ADDR_LENGTH);
            MPLS_FTN_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry2)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry2)) = u4PrefixLen2;
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry2)) = 200;
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry2)) = 1;

            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry1, 0) == MPLS_FAILURE)
            {
                 break;
            }

            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry2, 0) == MPLS_FAILURE)
            {
                 break;
            }

            pILMHwListEntry1 = MplsHwListGetFirstILMHwListEntry (); 

            if (pILMHwListEntry1 != NULL)
            {
                pILMHwListEntry2 = MplsHwListGetNextILMHwListEntry (pILMHwListEntry1);
                if (pILMHwListEntry2 !=  NULL)
                {
                    u4RetVal = OSIX_SUCCESS;
                }
            } 

            if (pILMHwListEntry1 != NULL && pILMHwListEntry2 != NULL)
            { 
                /* Clean-up */
            
                MplsHwListDelILMHwListEntry (pILMHwListEntry1);
                MplsHwListDelILMHwListEntry (pILMHwListEntry2);
            }
            else
            {
                u4RetVal = OSIX_FAILURE;
            }
            break;
        }
 
        case 53:
        {
            /* SUNNY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that FTN Hw Entry Validation should "
                                  "be successfull when label and Next Hop matches\n", i4TestId);
 
            tFTNHwListEntry           FTNHwListEntry;
            UINT4                     u4Label = 100;
            tMplsIpAddress            NextHop;
            UINT4                     u4NextHopIp = 0x01010101;
            uLabel                    Label;

            MEMSET (&NextHop, 0, sizeof (tMplsIpAddress));
            MEMSET (&Label, 0, sizeof (uLabel));
            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            
          
            MPLS_FTN_HW_LIST_LABEL ((&FTNHwListEntry)) = u4Label;
            FTNHwListEntry.NextHopIp.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (&FTNHwListEntry.NextHopIp.IpAddress, &u4NextHopIp, IPV4_ADDR_LENGTH);
 
            NextHop.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (&NextHop.IpAddress, &u4NextHopIp, IPV4_ADDR_LENGTH);
            MEMCPY (&Label.u4GenLbl, &u4Label, sizeof (UINT4));
           
            if (MplsHwListValidateFTNHwListEntry (&FTNHwListEntry, Label, NextHop) == MPLS_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 54:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that FTN Hw Entry Validation should "
                                  "FAIL when label mismatch ocuurs\n", i4TestId);

            tFTNHwListEntry           FTNHwListEntry;
            UINT4                     u4Label = 100;
            UINT4                     u4MisMatchLbl = 200;
            tMplsIpAddress            NextHop;
            UINT4                     u4NextHopIp = 0x01010101;
            uLabel                    Label;

            MEMSET (&NextHop, 0, sizeof (tMplsIpAddress));
            MEMSET (&Label, 0, sizeof (uLabel));
            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));


            MPLS_FTN_HW_LIST_LABEL ((&FTNHwListEntry)) = u4Label;
            FTNHwListEntry.NextHopIp.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (&FTNHwListEntry.NextHopIp.IpAddress, &u4NextHopIp, IPV4_ADDR_LENGTH);

            NextHop.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (&NextHop.IpAddress, &u4NextHopIp, IPV4_ADDR_LENGTH);
            MEMCPY (&Label.u4GenLbl, &u4MisMatchLbl, sizeof (UINT4));

            if (MplsHwListValidateFTNHwListEntry (&FTNHwListEntry, Label, NextHop) == MPLS_FALSE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 55:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that FTN Hw Entry Validation should "
                                  "FAIL when NextHop mismatch ocuurs\n", i4TestId);

            tFTNHwListEntry           FTNHwListEntry;
            UINT4                     u4Label = 100;
            tMplsIpAddress            NextHop;
            UINT4                     u4NextHopIp = 0x01010101;
            UINT4                     u4MismatchNextHop = 0x02020202;
            uLabel                    Label;

            MEMSET (&NextHop, 0, sizeof (tMplsIpAddress));
            MEMSET (&Label, 0, sizeof (uLabel));
            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));


            MPLS_FTN_HW_LIST_LABEL ((&FTNHwListEntry)) = u4Label;
            FTNHwListEntry.NextHopIp.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (&FTNHwListEntry.NextHopIp.IpAddress, &u4NextHopIp, IPV4_ADDR_LENGTH);

            NextHop.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (&NextHop.IpAddress, &u4MismatchNextHop, IPV4_ADDR_LENGTH);
            MEMCPY (&Label.u4GenLbl, &u4Label, sizeof (UINT4));

            if (MplsHwListValidateFTNHwListEntry (&FTNHwListEntry, Label, NextHop) == MPLS_FALSE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 56:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that ILM Hw Entry Validation should "
                                  "be successful\n", i4TestId);

            tILMHwListEntry               ILMHwListEntry;
            UINT4                         u4NextHop = 0x01010101;
            UINT4                         u4Label = 100;
            UINT4                         u4TnlIf = 1069;
            uLabel                        Label; 
            tMplsIpAddress                NextHopIp;


            MEMSET (&Label, 0, sizeof (uLabel));
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&NextHopIp, 0, sizeof (tMplsIpAddress)); 

            MEMCPY (&NextHopIp.IpAddress, &u4NextHop, IPV4_ADDR_LENGTH);
            NextHopIp.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
             
            MEMCPY (&Label.u4GenLbl, &u4Label, sizeof (UINT4));

            MPLS_ILM_HW_LIST_SWAP_LABEL ((&ILMHwListEntry)) = u4Label;
            MEMCPY (&ILMHwListEntry.NextHopIp.IpAddress, &u4NextHop, IPV4_ADDR_LENGTH);
            MPLS_ILM_HW_LIST_NEXT_HOP_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&ILMHwListEntry)) = u4TnlIf;

            if (MplsHwListValidateILMHwListEntry (&ILMHwListEntry, Label, NextHopIp, u4TnlIf) == MPLS_TRUE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }
        
        case 57:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that ILM Hw Entry Validation should "
                                  "FAIL, when out label mismatch\n", i4TestId);

            tILMHwListEntry               ILMHwListEntry;
            UINT4                         u4NextHop = 0x01010101;
            UINT4                         u4Label = 100;
            UINT4                         u4MisMatchLbl = 200;
            UINT4                         u4TnlIf = 1069;
            uLabel                        Label;
            tMplsIpAddress                NextHopIp;


            MEMSET (&Label, 0, sizeof (uLabel));
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&NextHopIp, 0, sizeof (tMplsIpAddress));

            MEMCPY (&NextHopIp.IpAddress, &u4NextHop, IPV4_ADDR_LENGTH);
            NextHopIp.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

            MEMCPY (&Label.u4GenLbl, &u4MisMatchLbl, sizeof (UINT4));

            MPLS_ILM_HW_LIST_SWAP_LABEL ((&ILMHwListEntry)) = u4Label;
            MEMCPY (&ILMHwListEntry.NextHopIp.IpAddress, &u4NextHop, IPV4_ADDR_LENGTH);
            MPLS_ILM_HW_LIST_NEXT_HOP_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&ILMHwListEntry)) = u4TnlIf;

            if (MplsHwListValidateILMHwListEntry (&ILMHwListEntry, Label, NextHopIp, u4TnlIf) == MPLS_FALSE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 58:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that ILM Hw Entry Validation should "
                                  "FAIL, when Next Hop mismatch occurs\n", i4TestId);

            tILMHwListEntry               ILMHwListEntry;
            UINT4                         u4NextHop = 0x01010101;
            UINT4                         u4MismatchNextHop = 0x02020202;
            UINT4                         u4Label = 100;
            UINT4                         u4TnlIf = 1069;
            uLabel                        Label;
            tMplsIpAddress                NextHopIp;


            MEMSET (&Label, 0, sizeof (uLabel));
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&NextHopIp, 0, sizeof (tMplsIpAddress));

            MEMCPY (&NextHopIp.IpAddress, &u4MismatchNextHop, IPV4_ADDR_LENGTH);
            NextHopIp.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

            MEMCPY (&Label.u4GenLbl, &u4Label, sizeof (UINT4));

            MPLS_ILM_HW_LIST_SWAP_LABEL ((&ILMHwListEntry)) = u4Label;
            MEMCPY (&ILMHwListEntry.NextHopIp.IpAddress, &u4NextHop, IPV4_ADDR_LENGTH);
            MPLS_ILM_HW_LIST_NEXT_HOP_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&ILMHwListEntry)) = u4TnlIf;

            if (MplsHwListValidateILMHwListEntry (&ILMHwListEntry, Label, NextHopIp, u4TnlIf) == MPLS_FALSE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }
  
        case 59:
        {
            /* RAINY DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that ILM Hw Entry Validation should "
                                  "FAIL, when TNL interface mismatch occurs\n", i4TestId);

            tILMHwListEntry               ILMHwListEntry;
            UINT4                         u4NextHop = 0x01010101;
            UINT4                         u4Label = 100;
            UINT4                         u4TnlIf = 1069;
            UINT4                         u4MismatchTnlIf = 1070;
            uLabel                        Label;
            tMplsIpAddress                NextHopIp;


            MEMSET (&Label, 0, sizeof (uLabel));
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMSET (&NextHopIp, 0, sizeof (tMplsIpAddress));

            MEMCPY (&NextHopIp.IpAddress, &u4NextHop, IPV4_ADDR_LENGTH);
            NextHopIp.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

            MEMCPY (&Label.u4GenLbl, &u4Label, sizeof (UINT4));

            MPLS_ILM_HW_LIST_SWAP_LABEL ((&ILMHwListEntry)) = u4Label;
            MEMCPY (&ILMHwListEntry.NextHopIp.IpAddress, &u4NextHop, IPV4_ADDR_LENGTH);
            MPLS_ILM_HW_LIST_NEXT_HOP_ADDR_TYPE ((&ILMHwListEntry)) = MPLS_IPV4_ADDR_TYPE;
            MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&ILMHwListEntry)) = u4TnlIf;

            if (MplsHwListValidateILMHwListEntry (&ILMHwListEntry, Label, NextHopIp, u4MismatchTnlIf) == MPLS_FALSE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 60:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListGetHwL3FTNParams "
                                  "should return L3FTN params sucessfully\n", i4TestId);

            UINT4             u4L3Intf = 1;
            UINT4             u4TnlIfIndex = 0;
            tFTNHwListEntry   FTNHwListEntry; 
            tMplsHwL3FTNInfo  MplsHwL3FTNInfo;
            UINT4             u4Prefix = 0;
            UINT4             u4NetMask = 0;
            tFsNpNextHopInfo  NextHop;

            MEMSET (&NextHop, 0, sizeof (tFsNpNextHopInfo));
            MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));
            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            if (CfaIfmCreateStackMplsTunnelInterface (u4L3Intf, &u4TnlIfIndex) == CFA_FAILURE)
            {
                break;
            }

            MPLS_FTN_HW_LIST_OUT_TNL_INTF ((&FTNHwListEntry)) = u4TnlIfIndex;

            if (MplsHwListGetHwL3FTNParams (&FTNHwListEntry, &MplsHwL3FTNInfo, 
                       &u4Prefix, &u4NetMask, &NextHop) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            CfaIfmDeleteStackMplsTunnelInterface (u4L3Intf, u4TnlIfIndex);
            break;     
        }

        case 61:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListGetHwL3FTNParams "
                                  "should FAIL if MPLS tunnel interface is invalid in the FTN hw list\n", i4TestId);

            tFTNHwListEntry   FTNHwListEntry;
            tMplsHwL3FTNInfo  MplsHwL3FTNInfo;
            UINT4             u4Prefix = 0;
            UINT4             u4NetMask = 0;
            tFsNpNextHopInfo  NextHop;

            MEMSET (&NextHop, 0, sizeof (tFsNpNextHopInfo));
            MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));
            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));

            MPLS_FTN_HW_LIST_OUT_TNL_INTF ((&FTNHwListEntry)) = 0;

            if (MplsHwListGetHwL3FTNParams (&FTNHwListEntry, &MplsHwL3FTNInfo,
                       &u4Prefix, &u4NetMask, &NextHop) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            break;
        }
        
        case 62:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListGetHwL3FTNParams "
                                  "should FAIL if CfaGetVlanId fails for the L3 interface in the FTN hw list\n", i4TestId);

            tFTNHwListEntry   FTNHwListEntry;
            tMplsHwL3FTNInfo  MplsHwL3FTNInfo;
            UINT4             u4Prefix = 0;
            UINT4             u4NetMask = 0;
            tFsNpNextHopInfo  NextHop;
            tCfaIfInfo        *pCfaIfInfo = NULL;
            UINT4             u4TnlIfIndex = 0;
            UINT4             u4L3Intf = 1;         

            MEMSET (&NextHop, 0, sizeof (tFsNpNextHopInfo));
            MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));
            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            if (CfaIfmCreateStackMplsTunnelInterface (u4L3Intf, &u4TnlIfIndex) == CFA_FAILURE)
            {
                break;
            }


            pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4L3Intf);
            pCfaIfInfo->i4Valid = CFA_FALSE;

            MPLS_FTN_HW_LIST_OUT_TNL_INTF ((&FTNHwListEntry)) = u4TnlIfIndex;

            if (MplsHwListGetHwL3FTNParams (&FTNHwListEntry, &MplsHwL3FTNInfo,
                       &u4Prefix, &u4NetMask, &NextHop) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            /* Clean-up */
            pCfaIfInfo->i4Valid = CFA_TRUE;
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            CfaIfmDeleteStackMplsTunnelInterface (u4L3Intf, u4TnlIfIndex);
            break;
        }

        case 63:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListGetHwILMParams "
                                  "should fetch ILM Hw Params successfully for normal L3 interface in ILM hw List\n", i4TestId);
 
            tILMHwListEntry         ILMHwListEntry;
            tMplsHwIlmInfo          MplsHwDelIlmParams;
            UINT4                   u4L3Intf = 1; 

            MEMSET (&MplsHwDelIlmParams, 0, sizeof (tMplsHwIlmInfo));
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
 
            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4L3Intf;
            
            if (MplsHwListGetHwILMParams (&ILMHwListEntry, &MplsHwDelIlmParams) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            /* Clean-up */
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            break;
        }

        case 64:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListGetHwILMParams "
                                  "should return FAILURE when L3 interface in the ILM Hw list in Invalid!\n", i4TestId);

            tILMHwListEntry         ILMHwListEntry;
            tMplsHwIlmInfo          MplsHwDelIlmParams;

            MEMSET (&MplsHwDelIlmParams, 0, sizeof (tMplsHwIlmInfo));
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));


            if (MplsHwListGetHwILMParams (&ILMHwListEntry, &MplsHwDelIlmParams) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 65:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListGetHwILMParams "
                                  "should fetch ILM hw params successfully if interface type MPLS tunnel\n", i4TestId);

            tILMHwListEntry         ILMHwListEntry;
            tMplsHwIlmInfo          MplsHwDelIlmParams;
            UINT4                   u4TnlIfIndex = 0;
            UINT4                   u4L3Intf = 1;

            MEMSET (&MplsHwDelIlmParams, 0, sizeof (tMplsHwIlmInfo));
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            if (CfaIfmCreateStackMplsTunnelInterface (u4L3Intf, &u4TnlIfIndex) == CFA_FAILURE)
            {
                break;
            }

            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4TnlIfIndex;
           
            if (MplsHwListGetHwILMParams (&ILMHwListEntry, &MplsHwDelIlmParams) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            CfaIfmDeleteStackMplsTunnelInterface (u4L3Intf, u4TnlIfIndex);
            break;
        }

        case 66:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListGetHwILMParams "
                                  "should fail if L3 interface fetching failed from Mpls Tunnel interface\n", i4TestId);

            tILMHwListEntry         ILMHwListEntry;
            tMplsHwIlmInfo          MplsHwDelIlmParams;
            UINT4                   u4TnlIfIndex = 0;
            UINT4                   u4L3Intf = 1;
            tCfaIfInfo              *pCfaIfInfo = NULL;

            MEMSET (&MplsHwDelIlmParams, 0, sizeof (tMplsHwIlmInfo));
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            if (CfaIfmCreateStackMplsTunnelInterface (u4L3Intf, &u4TnlIfIndex) == CFA_FAILURE)
            {
                break;
            }

            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4TnlIfIndex;

            /* Simulating the Failure */ 
            pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4TnlIfIndex);
            pCfaIfInfo->i4Valid = CFA_FALSE;

            if (MplsHwListGetHwILMParams (&ILMHwListEntry, &MplsHwDelIlmParams) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            pCfaIfInfo->i4Valid = CFA_TRUE;
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            CfaIfmDeleteStackMplsTunnelInterface (u4L3Intf, u4TnlIfIndex);
            break;
        }

        case 67:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListGetHwILMParams "
                                  "should fail if CfaGetVlanId fails for L3 interface in the ILM hw list\n", i4TestId);

            tILMHwListEntry         ILMHwListEntry;
            tMplsHwIlmInfo          MplsHwDelIlmParams;
            UINT4                   u4L3Intf = 1;
            tCfaIfInfo              *pCfaIfInfo = NULL;
  
            MEMSET (&MplsHwDelIlmParams, 0, sizeof (tMplsHwIlmInfo));
            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);

            /* Simulate the CFA failure */
            pCfaIfInfo = CfaIfUtlCdbGetIfDbEntry (u4L3Intf);
            pCfaIfInfo->i4Valid = CFA_FALSE;

            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4L3Intf; 

            if (MplsHwListGetHwILMParams (&ILMHwListEntry, &MplsHwDelIlmParams) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            pCfaIfInfo->i4Valid = CFA_TRUE;
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);

            break;
        }

        case 68:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListAddOrUpdateFTNHwListEntry "
                                  "should fail if RbTree add returns Failure\n", i4TestId);
            struct rbtree         *pTempFTNHwList = NULL;
            tFTNHwListEntry       FTNHwListEntry;

            pTempFTNHwList = gpFTNHwListRBTree;

            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));

            /* Simulate RBTree Add FAILURE */
            gpFTNHwListRBTree = NULL;
            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, 0) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
          
            /* Clean-up */
            gpFTNHwListRBTree = pTempFTNHwList;
            break;
        }

        case 69:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListAddOrUpdateILMHwListEntry "
                                  "should fail if RbTree add returns Failure\n", i4TestId);

            tILMHwListEntry        ILMHwListEntry;
            struct rbtree         *pTempILMHwList = NULL;
            tMplsIpAddress        Fec;
            UINT4                 u4Intf = 1;
            UINT4                 u4Label = 100;
            UINT1                 u1PrefixLen = 32;
            uLabel                Label;
            UINT4                 u4Prefix = 0x01010101;


            MEMSET (&Label, 0, sizeof (uLabel));  
            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            Label.u4GenLbl = u4Label;
            

            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            MEMCPY (&ILMHwListEntry.Fec, &Fec, sizeof (tMplsIpAddress));
            MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4Intf;
            MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u1PrefixLen;

            pTempILMHwList = gpILMHwListRBTree;

            /* Simulate RBTree Add FAILURE */
            gpILMHwListRBTree = NULL;
            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
           
            /* Clean-up */       
            gpILMHwListRBTree = pTempILMHwList;

            break;

        }

        case 70:
        {
            /* SUNNY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteILMEntryFromHw should "
                                  "successfully delete ILM Entry from the data plane\n", i4TestId);
            tMplsIpAddress        Fec;
            UINT4                 u4L3Intf = 1; 
            UINT4                 u4Label = 200020; 
            tILMHwListEntry       *pILMHwListEntry = NULL;
            uLabel                Label;

            MEMSET (&Label, 0, sizeof (uLabel));
            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateStaticILMByCLI ("1.1.1.1", 200010); /* The function creates ILM with label 200020 on interface gig 0/1*/
            MEMCPY (&Label.u4GenLbl, &u4Label, sizeof (UINT4));
            pILMHwListEntry = MplsHwListGetILMHwListEntry (Fec, 0, 1069, Label);
            if (pILMHwListEntry == NULL)
            {
                break;
            }

            if (MplsHwListDeleteILMEntryFromHw (pILMHwListEntry, NULL) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            LdpGrUtDeleteStaticILMbyCLI ("1.1.1.1", 200010);          
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);  
            break;
        }
   
        case 71: 
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteILMEntryFromHw should "
                                  "not delete entry from data plane if node is STANDBY\n", i4TestId);

            tILMHwListEntry       ILMHwListEntry;


            gMplsNodeStatus = MPLS_NODE_STANDBY;

            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));

            if (MplsHwListDeleteILMEntryFromHw (&ILMHwListEntry, NULL) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            gMplsNodeStatus = MPLS_NODE_ACTIVE;
            break;
        }

        case 72:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteILMEntryFromHw should "
                                  "FAIL if the ILM hw list entry has invalid L3 interface\n", i4TestId);

             tILMHwListEntry       ILMHwListEntry;

             MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
             MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = 0;

             if (MplsHwListDeleteILMEntryFromHw (&ILMHwListEntry, NULL) == MPLS_FAILURE)
             {
                  u4RetVal = OSIX_SUCCESS;
             }
             break;
        }
        case 73:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteILMEntryFromHw should "
                                  "be SUCCESS even though if the ILM entry is not present in the data plane\n", i4TestId);

             tILMHwListEntry       *pILMHwListEntry = NULL;
             tILMHwListEntry       ILMHwListEntry;
             UINT4                 u4L3Intf = 1; 
             tMplsIpAddress        Fec;
             UINT4                 u4Label = 100;
             UINT1                 u1PrefixLen = 32;
             UINT4                 u4Prefix = 0x01010101;
             uLabel                Label;
             

             MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));

             MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
             MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
             Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
             Label.u4GenLbl = u4Label;

             MEMCPY (&ILMHwListEntry.Fec.IpAddress, &Fec, sizeof (tMplsIpAddress));
             MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
             MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4L3Intf;            
             MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u1PrefixLen;

             LdpGrUtCreateL3InterfaceCLI (u4L3Intf);

             if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
             {
                 break;
             }
           
             pILMHwListEntry = MplsHwListGetILMHwListEntry (Fec, u1PrefixLen, u4L3Intf, Label); 
             if (pILMHwListEntry == NULL)
             {
                 break;
             }
             
             if (MplsHwListDeleteILMEntryFromHw (pILMHwListEntry, NULL) == MPLS_SUCCESS)
             {
                  u4RetVal = OSIX_SUCCESS;
             }
 
             /* Clean-up */
             LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
             break;
        }
        case 74:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteILMEntryFromHw should "
                                  "FAIL when ILM Hw List entry deletion fails from the RBtree\n", i4TestId);

             tILMHwListEntry       ILMHwListEntry;
             tILMHwListEntry       *pILMHwListEntry = NULL;
             UINT4                 u4L3Intf = 1;
             struct rbtree         *pTempILMHwList = NULL;
             tMplsIpAddress        Fec;
             UINT4                 u4Label = 100;
             UINT1                 u1PrefixLen = 32;
             UINT4                 u4Prefix = 0x01010101;
             uLabel                Label;


             MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));

             MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
             MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
             Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
             Label.u4GenLbl = u4Label;

             MEMCPY (&ILMHwListEntry.Fec.IpAddress, &Fec, sizeof (tMplsIpAddress));
             MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
             MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4L3Intf;
             MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u1PrefixLen;



             pTempILMHwList = gpILMHwListRBTree;
             LdpGrUtCreateL3InterfaceCLI (u4L3Intf);

             if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
             {
                 break;
             }

             pILMHwListEntry = MplsHwListGetILMHwListEntry (Fec, u1PrefixLen, u4L3Intf, Label);
             if (pILMHwListEntry == NULL)
             {
                 break;
             }

             /* Simulate the FAILURE */
             gpILMHwListRBTree = NULL;
             if (MplsHwListDeleteILMEntryFromHw (pILMHwListEntry, NULL) == MPLS_FAILURE)
             {
                  u4RetVal = OSIX_SUCCESS;
             }

             /* Clean-up */
             gpILMHwListRBTree = pTempILMHwList;
             MplsHwListDelILMHwListEntry (pILMHwListEntry);
             LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
             break;
        }

        case 75:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteFTNEntryFromHw should "
                                  "return Success when the MPLS route is not found in data plane\n", i4TestId);

            UINT4                   u4L3Intf = 1;
            UINT4                   u4TnlIfIndex = 0;
            tFTNHwListEntry         *pFTNHwListEntry = NULL;
            UINT4                   u4Prefix = 0x99999999;
            tFTNHwListEntry         FTNHwListEntry;
            tMplsIpAddress          Fec; 
            UINT1                   u1PrefixLen = 32;

            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
 
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;     
 

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            if (CfaIfmCreateStackMplsTunnelInterface (u4L3Intf, &u4TnlIfIndex) == CFA_FAILURE)
            {
                break;
            }

            MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&FTNHwListEntry)) = u4TnlIfIndex;
            MPLS_FTN_HW_LIST_OUT_L3_INTF ((&FTNHwListEntry)) = u4L3Intf;
            MEMCPY (&FTNHwListEntry.Fec, &Fec, sizeof (tMplsIpAddress));
            MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FTNHwListEntry)) = u1PrefixLen; 

            MPLS_FTN_HW_LIST_IS_STATIC ((&FTNHwListEntry)) = MPLS_TRUE;  

            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, 0) == MPLS_FAILURE)
            {
                break;
            }
 
            pFTNHwListEntry = MplsHwListGetFTNHwListEntry (Fec, u1PrefixLen);
            if (pFTNHwListEntry == NULL)
            {
                break;
            }
 
            if (MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry, NULL) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            CfaIfmDeleteStackMplsTunnelInterface (u4L3Intf, u4TnlIfIndex);
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            break;
        }


        case 76:
        {
            /* SUNNY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteFTNEntryFromHw should "
                                  "successfully delete FTN entry from the Hw\n", i4TestId);

            tFTNHwListEntry   *pFTNHwListEntry = NULL;
            UINT4             u4L3Intf = 1;
            tMplsIpAddress    Fec;
            UINT1             u1PrefixLen = 32;
            UINT4             u4Prefix = 0x02020202;

            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
            
            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateStaticFTNByCLI ("2.2.2.2", 200010);       
            pFTNHwListEntry = MplsHwListGetFTNHwListEntry (Fec, u1PrefixLen);
            if (pFTNHwListEntry == NULL)
            {
                break;
            }

            if (MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry, NULL) == MPLS_SUCCESS)
            {
                u4RetVal =  OSIX_SUCCESS;
            }

            /* Clean-up */
            LdpGrUtDeleteStaticFTNByCLI ("2.2.2.2", 200010); 
            break;
        }

        case 77:
        {
            /* SUNNY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteFTNEntryFromHw should "
                                  "return SUCCESS when the NP programming is FALSE\n", i4TestId);


            UINT4                   u4L3Intf = 1;
            UINT4                   u4TnlIfIndex = 0;
            tFTNHwListEntry         FTNHwListEntry;

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            if (CfaIfmCreateStackMplsTunnelInterface (u4L3Intf, &u4TnlIfIndex) == CFA_FAILURE)
            {
                break;
            }

            MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&FTNHwListEntry)) = u4TnlIfIndex;
            MPLS_FTN_HW_LIST_OUT_L3_INTF ((&FTNHwListEntry)) = u4L3Intf;
            MPLS_FTN_HW_LIST_IS_STATIC ((&FTNHwListEntry)) = MPLS_FALSE;

            gMplsNodeStatus = MPLS_NODE_STANDBY;

            if (MplsHwListDeleteFTNEntryFromHw (&FTNHwListEntry, NULL) == MPLS_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            
            /* Clean-up */
            gMplsNodeStatus = MPLS_NODE_ACTIVE;            
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            break;
        }

        case 78:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteFTNEntryFromHw should "
                                  "return FAIL if MplsHwListGetHwL3FTNParams returns FAILURE\n", i4TestId);

            tFTNHwListEntry         FTNHwListEntry;
            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));

            /* Simulate the failure by passing the 0 Mpls tunnel interface in the Hw list entry */
            if (MplsHwListDeleteFTNEntryFromHw (&FTNHwListEntry, NULL) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
            break;
        }

        case 79:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteFTNEntryFromHw should "
                                  "return FAIL if MPLS Tunnel interface deletion fails for non-static entry\n", i4TestId);

            UINT4                   u4L3Intf = 1;
            UINT4                   u4TnlIfIndex = 0;
            tFTNHwListEntry         FTNHwListEntry;
 

            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            if (CfaIfmCreateStackMplsTunnelInterface (u4L3Intf, &u4TnlIfIndex) == CFA_FAILURE)
            {
                break;
            }

            /* Simulate Failure by non initilaising the L3 interface on which Tunnel interface is stacked */
            MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&FTNHwListEntry)) = u4TnlIfIndex;
 
            MPLS_FTN_HW_LIST_IS_STATIC ((&FTNHwListEntry)) = MPLS_FALSE;
            gMplsNodeStatus = MPLS_NODE_STANDBY;

            if (MplsHwListDeleteFTNEntryFromHw (&FTNHwListEntry, NULL) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            gMplsNodeStatus = MPLS_NODE_ACTIVE;
            CfaIfmDeleteStackMplsTunnelInterface (u4L3Intf, u4TnlIfIndex);
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            break;
        }

        case 80:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListDeleteFTNEntryFromHw should "
                                  "return FAIL if FTN Hw List deletion fails\n", i4TestId);

            tFTNHwListEntry   *pFTNHwListEntry = NULL;
            UINT4             u4L3Intf = 1;
            tMplsIpAddress    Fec;
            UINT1             u1PrefixLen = 32;
            UINT4             u4Prefix = 0x04040404;
            struct rbtree     *pTempFTNHwList = NULL;


            pTempFTNHwList = gpFTNHwListRBTree;
            MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
            MEMCPY (&Fec.IpAddress, &u4Prefix, IPV4_ADDR_LENGTH);
            Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateStaticFTNByCLI ("4.4.4.4", 200012);
            pFTNHwListEntry = MplsHwListGetFTNHwListEntry (Fec, u1PrefixLen);
            if (pFTNHwListEntry == NULL)
            {
                break;
            }

            /* Simulate the FAILURE by making Rbtree pointer NULL */ 
            gpFTNHwListRBTree = NULL;
            if (MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry, NULL) == MPLS_FAILURE)
            {
                u4RetVal =  OSIX_SUCCESS;
            }

            /* Clean-up */
            gpFTNHwListRBTree = pTempFTNHwList;
            LdpGrUtDeleteStaticFTNByCLI ("4.4.4.4", 200012);
            MplsHwListDelFTNHwListEntry (pFTNHwListEntry);
            break;
        }

        case 81:
        {
             /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListAddOrUpdateFTNHwListEntry should "
                                  "return FAIL if MplsHwListCreateFTNHwListEntry returns NULL\n", i4TestId);

            tFTNHwListEntry          FTNHwListEntry;

            MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
            LdpGrUtExhaustFTNHwListMemPool ();

            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, 0) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }
 
            /* Clean up */
            LdpGrUtFreeFTNHwListMemPool ();
            break;
        }

        case 82:
        {
            /* RAINY-DAY */
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that MplsHwListAddOrUpdateILMHwListEntry should "
                                  "return FAIL if MplsHwListCreateILMHwListEntry returns NULL\n", i4TestId);

            tILMHwListEntry          ILMHwListEntry;

            MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
            LdpGrUtExhaustILMHwListMemPool ();

            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean up */
            LdpGrUtFreeILMHwListMemPool ();
            break;
        }

        case 83:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that LdpGrFreeLabel should "
                                  "Release the Label Successfully\n", i4TestId);
 
            UINT4        u4Label = 100;
            UINT4        u4L3Intf = 1;            

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateAndInitLdpEntityCLI ();

            if (LdpGrReserveLabel (u4Label) == LDP_FAILURE)
            {
                break;
            }

            if (LdpGrFreeLabel (u4Label) == LDP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean Up */
            LdpGrUtDeleteLdpEntityCLI ();
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            break; 
        }

        case 84:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that LdpGrFreeLabel should "
                                  "return FAILURE when the label is higher than all labels configured in the LDP entity\n", i4TestId);

            UINT4        u4Label = 100;
            UINT4        u4RelLabel = 200;
            UINT4        u4L3Intf = 1;

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateAndInitLdpEntityCLI ();

            if (LdpGrReserveLabel (u4Label) == LDP_FAILURE)
            {
                break;
            }

            if (LdpGrFreeLabel (u4RelLabel) == LDP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean Up */
            LdpGrUtDeleteLdpEntityCLI ();
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            LdpGrFreeLabel (u4Label);
            break;
        }

        case 85:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that LdpGrFreeLabel should "
                                  "return FAILURE when the label is smaller than all labels configured in the LDP entity\n", i4TestId);

            UINT4        u4Label = 100;
            UINT4        u4RelLabel = 50;
            UINT4        u4L3Intf = 1;

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateAndInitLdpEntityCLI ();

            if (LdpGrReserveLabel (u4Label) == LDP_FAILURE)
            {
                break;
            }

            if (LdpGrFreeLabel (u4RelLabel) == LDP_FAILURE)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean Up */
            LdpGrUtDeleteLdpEntityCLI ();
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            LdpGrFreeLabel (u4Label);
            break;
        }
        
        case 86:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that LdpProcessLblRelEvent should "
                                  "successfully release the provided Label in the event info\n", i4TestId);

            UINT4                 u4Label = 100;
            UINT4                 u4L3Intf = 1;
            tLdpGrLblRelEvtInfo   LdpGrLblRelEvtInfo;

            MEMSET (&LdpGrLblRelEvtInfo, 0, sizeof (tLdpGrLblRelEvtInfo));
            LdpGrLblRelEvtInfo.Label.u4GenLbl  = u4Label;

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateAndInitLdpEntityCLI ();
 
            if (LdpGrReserveLabel (u4Label) == LDP_FAILURE)
            {
                break;
            }
            LdpGrProcessLblRelEvent (&LdpGrLblRelEvtInfo);
            u4RetVal = OSIX_SUCCESS;            

            /* Clean Up */
            LdpGrUtDeleteLdpEntityCLI ();
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            break;
        }

        case 87:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that LdpProcessLblRelEvent "
                                  " FAILS if label is Invalid\n", i4TestId);

            UINT4                 u4Label = 100;
            UINT4                 u4RelLabel = 200; /* Label out of range from the entity 100-199 */
            UINT4                 u4L3Intf = 1;
            tLdpGrLblRelEvtInfo   LdpGrLblRelEvtInfo;

            MEMSET (&LdpGrLblRelEvtInfo, 0, sizeof (tLdpGrLblRelEvtInfo));
            LdpGrLblRelEvtInfo.Label.u4GenLbl  = u4RelLabel;

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateAndInitLdpEntityCLI ();

            if (LdpGrReserveLabel (u4Label) == LDP_FAILURE)
            {
                break;
            }
            LdpGrProcessLblRelEvent (&LdpGrLblRelEvtInfo);
            u4RetVal = OSIX_SUCCESS;

            /* Clean Up */
            LdpGrUtDeleteLdpEntityCLI ();
            LdpGrFreeLabel (u4Label);
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            break;
        }

        case 88:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that  "
                                  "LdpGrLblRelEventHandler should successfully post the event to LDP Queue\n", i4TestId);

            UINT4        u4Label = 200;
            uLabel       Label;

            MEMSET (&Label, 0, sizeof (uLabel));
            Label.u4GenLbl = u4Label;

            LdpGrLblRelEventHandler (Label);
            u4RetVal = OSIX_SUCCESS;

            break;
        }

        case 89:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that  "
                                  "LdpGrLblRelEventHandler FAILS when LdpEnqueueMsgToLdpQ returns FAILURE\n", i4TestId);

            UINT4       u4Label = 200;
            uLabel      Label;
 
            MEMSET (&Label, 0, sizeof (uLabel));
            Label.u4GenLbl = u4Label;

            LdpGrUtExhaustLdpQMemPool ();
            LdpGrLblRelEventHandler (Label);
            u4RetVal = OSIX_SUCCESS;

            /* Clean-up */
            LdpGrUtFreeLdpQMemPool ();
            break;
        }

        case 90:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that  "
                                  "LdpGrReserveLabel successfully reserves the provided Label\n", i4TestId);

            UINT4              u4Label = 100;
            UINT4              u4L3Intf = 1;          
   
            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateAndInitLdpEntityCLI ();            

            if (LdpGrReserveLabel (u4Label) == LDP_SUCCESS)
            {
                u4RetVal = OSIX_SUCCESS;
            }

            /* Clean-up */
            LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
            LdpGrUtDeleteLdpEntityCLI ();
            LdpGrFreeLabel (u4Label); 
            break;
        }

        case 91:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that  "
                                  "LdpGrReserveLabel should fail if Label is higher than max Label Range\n", i4TestId);

            UINT4              u4Label = 200;
            UINT4              u4L3Intf = 1;

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateAndInitLdpEntityCLI ();

           if (LdpGrReserveLabel (u4Label) == LDP_FAILURE)
           {
               u4RetVal = OSIX_SUCCESS;
           }

           /* Clean-up */
           LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
           LdpGrUtDeleteLdpEntityCLI ();
           break;
        }

        case 92:
        {
            CliPrintf (CliHandle, "TestCaseId: %d: To Verify that  "
                                  "LdpGrReserveLabel should fail if Label is less than minimum Label Range\n", i4TestId);

            UINT4              u4Label = 90;
            UINT4              u4L3Intf = 1;

            LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
            LdpGrUtCreateAndInitLdpEntityCLI ();

           if (LdpGrReserveLabel (u4Label) == LDP_FAILURE)
           {
               u4RetVal = OSIX_SUCCESS;
           }

           /* Clean-up */
           LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
           LdpGrUtDeleteLdpEntityCLI ();
 
           break;
        }

        case 93:
        {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpProcessFwdHoldTmrExpiry should process all stale FTN entries successfully\n", i4TestId);

           tMplsIpAddress          Fec1;
           tMplsIpAddress          Fec2;
           UINT4                   u4Prefix1 = 0x05050505;
           UINT4                   u4Prefix2 = 0x06060606;
           tFTNHwListEntry         *pFTNHwListEntry1 = NULL;
           tFTNHwListEntry         *pFTNHwListEntry2 = NULL;
           UINT4                   u4L3Intf = 1;

           MEMSET (&Fec1, 0, sizeof (tMplsIpAddress));
           MEMSET (&Fec2, 0, sizeof (tMplsIpAddress));
           
           MEMCPY (&Fec1.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
           MEMCPY (&Fec2.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);
           Fec1.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
           Fec2.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

           LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
           LdpGrUtCreateStaticFTNByCLI ("5.5.5.5", 200021);
           LdpGrUtCreateStaticFTNByCLI ("6.6.6.6", 200022);

           pFTNHwListEntry1 = MplsHwListGetFTNHwListEntry (Fec1, 32);
           pFTNHwListEntry2 = MplsHwListGetFTNHwListEntry (Fec2, 32);

           if (pFTNHwListEntry1 == NULL || pFTNHwListEntry2 == NULL)
           {
               break;
           }

           MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry1) = MPLS_TRUE;  
           MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry2) = MPLS_TRUE;
           LdpProcessFwdHoldTmrExpiry ();
          

           /* Clean-up */
           LdpGrUtDeleteStaticFTNByCLI ("5.5.5.5", 200021);
           LdpGrUtDeleteStaticFTNByCLI ("6.6.6.6", 200022);
           LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
           break;
           
        }
        case 94:
        {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpProcessFwdHoldTmrExpiry should not act on unstale FTN entries\n", i4TestId);

           tMplsIpAddress          Fec1;
           tMplsIpAddress          Fec2;
           UINT4                   u4Prefix1 = 0x12121212; /* 18.18.18.18 */
           UINT4                   u4Prefix2 = 0x13131313; /* 19.19.19.19 */
           tFTNHwListEntry         *pFTNHwListEntry1 = NULL;
           tFTNHwListEntry         *pFTNHwListEntry2 = NULL;
           UINT4                   u4L3Intf = 1;

           MEMSET (&Fec1, 0, sizeof (tMplsIpAddress));
           MEMSET (&Fec2, 0, sizeof (tMplsIpAddress));

           MEMCPY (&Fec1.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
           MEMCPY (&Fec2.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);
           Fec1.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
           Fec2.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

           LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
           LdpGrUtCreateStaticFTNByCLI ("18.18.18.18", 200023);
           LdpGrUtCreateStaticFTNByCLI ("19.19.19.19", 200024);

           pFTNHwListEntry1 = MplsHwListGetFTNHwListEntry (Fec1, 32);
           pFTNHwListEntry2 = MplsHwListGetFTNHwListEntry (Fec2, 32);

           if (pFTNHwListEntry1 == NULL || pFTNHwListEntry2 == NULL)
           {
               break;
           }

           MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry1) = MPLS_FALSE;
           MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry2) = MPLS_FALSE;
           LdpProcessFwdHoldTmrExpiry ();


           /* Clean-up */
           LdpGrUtDeleteStaticFTNByCLI ("18.18.18.18", 200023);
           LdpGrUtDeleteStaticFTNByCLI ("19.19.19.19", 200024);
           MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry1, NULL);
           MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry2, NULL);           

           LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
           break;
        }

        case 95:
        {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpProcessFwdHoldTmrExpiry should FAIL if MplsHwListDeleteFTNEntryFromHw FAILS\n", i4TestId);

           tMplsIpAddress          Fec1;
           tMplsIpAddress          Fec2;
           UINT4                   u4Prefix1 = 0x14141414; /*20.20.20.20 */
           tFTNHwListEntry         *pFTNHwListEntry1 = NULL;
           UINT4                   u4L3Intf = 1;
           UINT4                   u4TnlIntf = 0; 


           MEMSET (&Fec1, 0, sizeof (tMplsIpAddress));

           MEMCPY (&Fec1.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
           Fec1.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

           LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
           LdpGrUtCreateStaticFTNByCLI ("20.20.20.20", 200025);

           pFTNHwListEntry1 = MplsHwListGetFTNHwListEntry (Fec1, 32);

           if (pFTNHwListEntry1 == NULL)
           {
               break;
           }

           MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry1) = MPLS_TRUE;
 
           u4TnlIntf = MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry1);
           /* Simulate Failure */
           MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry1) = 0;
   
           LdpProcessFwdHoldTmrExpiry ();


           /* Clean-up */
           LdpGrUtDeleteStaticFTNByCLI ("20.20.20.20", 200025);
           LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
           MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry1) = u4TnlIntf;
           MplsHwListDelFTNHwListEntry (pFTNHwListEntry1);
           break;
       }


       case 96:
       {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpProcessFwdHoldTmrExpiry should successfully process stale ILM list entries\n", i4TestId);

           UINT4                   u4L3Intf = 1;
           UINT4                   u4Label1 = 200090;
           UINT4                   u4Label2 = 200091; 
           uLabel                  Label1;
           uLabel                  Label2; 
           tILMHwListEntry         *pILMHwListEntry1 = NULL;
           tILMHwListEntry         *pILMHwListEntry2 = NULL;
           tMplsIpAddress           Fec;
           UINT4                   u4TnlIntf1 = 1077;
           UINT4                   u4TnlIntf2 = 1078;


           MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
           MEMSET (&Label1, 0, sizeof (uLabel));
           MEMSET (&Label2, 0, sizeof (uLabel));

           Label1.u4GenLbl = u4Label1;
           Label2.u4GenLbl = u4Label2;

           LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
           LdpGrUtCreateStaticILMByCLI ("7.7.7.7", 200090);
           LdpGrUtCreateStaticILMByCLI ("8.8.8.8", 200091);

           MGMT_UNLOCK ();
           CliExecuteAppCmd ("show ilm hw list");
           MGMT_LOCK ();

           pILMHwListEntry1 = MplsHwListGetILMHwListEntry (Fec, 0, u4TnlIntf1, Label1);
           pILMHwListEntry2 = MplsHwListGetILMHwListEntry (Fec, 0, u4TnlIntf2, Label2); 

           if (pILMHwListEntry1 == NULL || pILMHwListEntry2 == NULL)
           {
               break;
           }

           MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry1) = MPLS_TRUE;
           MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry2) = MPLS_TRUE;

           LdpProcessFwdHoldTmrExpiry (); 
           
           /* Clean-up */
           LdpGrUtDeleteStaticILMbyCLI ("7.7.7.7", 200090);
           LdpGrUtDeleteStaticILMbyCLI ("8.8.8.8", 200091);
           LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
           break;
       }

       case 97:
       {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpProcessFwdHoldTmrExpiry should FAIL when MplsHwListDelILMEntryFromHw Fails\n", i4TestId);

          
           UINT4                   u4L3Intf = 1;
           UINT4                   u4Label1 = 200092;
           uLabel                  Label1;
           tILMHwListEntry         *pILMHwListEntry1 = NULL;
           tILMHwListEntry         *pILMHwListEntry2 = NULL;
           tMplsIpAddress           Fec;
           UINT4                   u4TnlIntf1 = 1077;
           UINT4                   u4SaveTnlInft = 0; 

           MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
           MEMSET (&Label1, 0, sizeof (uLabel));

           Label1.u4GenLbl = u4Label1;

           LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
           LdpGrUtCreateStaticILMByCLI ("9.9.9.9", 200092);

           MGMT_UNLOCK ();
           CliExecuteAppCmd ("show ilm hw list");
           MGMT_LOCK ();

           pILMHwListEntry1 = MplsHwListGetILMHwListEntry (Fec, 0, u4TnlIntf1, Label1);

           if (pILMHwListEntry1 == NULL)
           {
               break;
           }

           MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry1) = MPLS_TRUE;
           u4SaveTnlInft = MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry1);

           /* Simulate the FAILURE by making l3 intf = 0 */
           MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry1) = 0;
           LdpProcessFwdHoldTmrExpiry ();

           /* Clean-up */
           MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry1) = u4SaveTnlInft;
           LdpGrUtDeleteStaticILMbyCLI ("9.9.9.9", 200092);
           LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
           MplsHwListDeleteILMEntryFromHw (pILMHwListEntry1, NULL);
           break;
       }

       case 98:
       {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpGrReserveResources SUCESSFULLY marks the FTN hw list entries stale "
                                 "when Tunnel intf reservation flag is TRUE\n", i4TestId);

           tMplsIpAddress          Fec1;
           tMplsIpAddress          Fec2;
           UINT4                   u4Prefix1 = 0x0b0b0b0b;
           UINT4                   u4Prefix2 = 0x0c0c0c0c;
           tFTNHwListEntry         *pFTNHwListEntry1 = NULL;
           tFTNHwListEntry         *pFTNHwListEntry2 = NULL;
           UINT4                   u4L3Intf = 1;

           MEMSET (&Fec1, 0, sizeof (tMplsIpAddress));
           MEMSET (&Fec2, 0, sizeof (tMplsIpAddress));

           MEMCPY (&Fec1.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
           MEMCPY (&Fec2.IpAddress, &u4Prefix2, IPV4_ADDR_LENGTH);
           Fec1.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;
           Fec2.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

           LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
           LdpGrUtCreateStaticFTNByCLI ("11.11.11.11", 200026);
           LdpGrUtCreateStaticFTNByCLI ("12.12.12.12", 200027);

           pFTNHwListEntry1 = MplsHwListGetFTNHwListEntry (Fec1, 32);
           pFTNHwListEntry2 = MplsHwListGetFTNHwListEntry (Fec2, 32);
           
           MPLS_FTN_HW_LIST_IS_STATIC (pFTNHwListEntry1) = MPLS_FALSE;
           MPLS_FTN_HW_LIST_IS_STATIC (pFTNHwListEntry2) = MPLS_TRUE;
           if (pFTNHwListEntry1 == NULL || pFTNHwListEntry2 == NULL)
           {
               break;
           }

           if (LdpGrReserveResources (LDP_TRUE) == LDP_SUCCESS)
           {
               u4RetVal = OSIX_SUCCESS;
           }
           
          
           /* Clean Up */
           LdpGrUtDeleteStaticFTNByCLI ("11.11.11.11", 200026);
           LdpGrUtDeleteStaticFTNByCLI ("12.12.12.12", 200027);
           MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry1, NULL);
           MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry2, NULL);

           LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
           break;
       }

       case 99:
       {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpGrReserveResources SUCESSFULLY marks the FTN hw list entries stale "
                                 "when Tunnel intf reservation flag is FASLE\n", i4TestId);

           tMplsIpAddress          Fec1;
           UINT4                   u4Prefix1 = 0x0d0d0d0d;
           tFTNHwListEntry         *pFTNHwListEntry1 = NULL;
           UINT4                   u4L3Intf = 1;

           MEMSET (&Fec1, 0, sizeof (tMplsIpAddress));

           MEMCPY (&Fec1.IpAddress, &u4Prefix1, IPV4_ADDR_LENGTH);
           Fec1.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

           LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
           LdpGrUtCreateStaticFTNByCLI ("13.13.13.13", 200028);

           pFTNHwListEntry1 = MplsHwListGetFTNHwListEntry (Fec1, 32);


           if (pFTNHwListEntry1 == NULL)
           {
               break;
           }

           MPLS_FTN_HW_LIST_IS_STATIC (pFTNHwListEntry1) = MPLS_FALSE;
           if (LdpGrReserveResources (LDP_FALSE) == LDP_SUCCESS)
           {
               u4RetVal = OSIX_SUCCESS;
           }


           /* Clean Up */
           LdpGrUtDeleteStaticFTNByCLI ("13.13.13.13", 200028);
           MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry1, NULL);

           LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
           break;
       }

       case 100:
       {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpGrReserveResources SUCESSFULLY marks the ILM hw list entries stale\n", i4TestId);

           UINT4                   u4L3Intf = 1;
           UINT4                   u4Label1 = 200100;
           UINT4                   u4Label2 = 200101;
           uLabel                  Label1;
           uLabel                  Label2;
           tILMHwListEntry         *pILMHwListEntry1 = NULL;
           tILMHwListEntry         *pILMHwListEntry2 = NULL;
           tMplsIpAddress          Fec1;
           tMplsIpAddress          Fec2;
           UINT4                   u4TnlIntf1 = 1080;
           UINT4                   u4TnlIntf2 = 1081;


           MEMSET (&Fec1, 0, sizeof (tMplsIpAddress));
           MEMSET (&Fec2, 0, sizeof (tMplsIpAddress));
           MEMSET (&Label1, 0, sizeof (uLabel));
           MEMSET (&Label2, 0, sizeof (uLabel));

           Label1.u4GenLbl = u4Label1;
           Label2.u4GenLbl = u4Label2;

           LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
           LdpGrUtCreateStaticILMByCLI ("14.14.14.14", u4Label1);
           LdpGrUtCreateStaticILMByCLI ("15.15.15.15", u4Label2);

           MGMT_UNLOCK ();
           CliExecuteAppCmd ("show ilm hw list");
           MGMT_LOCK ();

           pILMHwListEntry1 = MplsHwListGetILMHwListEntry (Fec1, 0, u4TnlIntf1, Label1);
           pILMHwListEntry2 = MplsHwListGetILMHwListEntry (Fec2, 0, u4TnlIntf2, Label2);

           if (pILMHwListEntry1 == NULL || pILMHwListEntry2 == NULL)
           {
               break;
           }

           MPLS_ILM_HW_LIST_IS_STATIC (pILMHwListEntry1) = MPLS_FALSE;
           MPLS_ILM_HW_LIST_IS_STATIC (pILMHwListEntry2) = MPLS_TRUE;
           MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry1) = MPLS_TRUE;
           MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry2) = MPLS_FALSE;

           if (LdpGrReserveResources (LDP_FALSE) == LDP_SUCCESS)
           {
               u4RetVal = OSIX_SUCCESS;
           }

           /* Clean-up */
           LdpGrUtDeleteStaticILMbyCLI ("14.14.14.14", u4Label1);
           LdpGrUtDeleteStaticILMbyCLI ("15.15.15.15", u4Label2);
           LdpGrUtDeleteL3InterfaceCLI (u4L3Intf);
           MplsHwListDeleteILMEntryFromHw (pILMHwListEntry1, NULL);
           MplsHwListDeleteILMEntryFromHw (pILMHwListEntry2, NULL);
           break;
       }

       /* This case Requires the LDP Session with a PEER, test case can only work with A peer */
       case 101:
       {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpGrEstbLspForStaleILMHwListEntry should form LSP for SWAP based ILM entry\n", i4TestId);

           tILMHwListEntry             ILMHwListEntry;
           UINT4                       u4L3Intf = 1;
           UINT4                       u4Label = 200;
           UINT4                       u4RoutePrefix = 0x16161616; /* 22.22.22.22 */
           UINT4                       u4PrefixLen = 32;
           tMplsIpAddress              Fec;

           MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
           MEMCPY (&Fec.IpAddress, &u4RoutePrefix, IPV4_ADDR_LENGTH);
           Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

           LdpGrUtConfigureCLI ();
       
           sleep (5);
           MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
           MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4L3Intf;
           MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
           MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&ILMHwListEntry)) = 1; /* Dummy interface to make entry swap based*/
           MEMCPY (&ILMHwListEntry.Fec, &Fec, sizeof (tMplsIpAddress));
           MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u4PrefixLen;

           /*if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
           {
               break;
           }*/
        
           if (LdpGrEstbLspForStaleILMHwListEntry (&ILMHwListEntry) == LDP_SUCCESS)
           {
               u4RetVal = OSIX_SUCCESS;
           }
           /* Clean up */
           LdpGrUtCleanUpCLI ();
           break; 
       }

       case 102:
       {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpGrEstbLspForStaleILMHwListEntry should not form LSP for non-swap ILM hw list entry\n", i4TestId);

           tILMHwListEntry             ILMHwListEntry;
           UINT4                       u4L3Intf = 1;
           UINT4                       u4Label = 200;
           UINT4                       u4RoutePrefix = 0x16161616; /* 22.22.22.22 */
           UINT4                       u4PrefixLen = 32;
           tMplsIpAddress              Fec;

           MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
           MEMCPY (&Fec.IpAddress, &u4RoutePrefix, IPV4_ADDR_LENGTH);
           Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

           LdpGrUtConfigureCLI ();

           sleep (5);
           MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
           MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = u4L3Intf;
           MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
           MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&ILMHwListEntry)) = 0; /* Interface =0 to make entry non-swap based*/
           MEMCPY (&ILMHwListEntry.Fec, &Fec, sizeof (tMplsIpAddress));
           MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u4PrefixLen;

           /*if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
           {
               break;
           }*/

           if (LdpGrEstbLspForStaleILMHwListEntry (&ILMHwListEntry) == LDP_SUCCESS)
           {
               u4RetVal = OSIX_SUCCESS;
           }
           /* Clean up */
           LdpGrUtCleanUpCLI ();
           break;
       }

       case 103:
       {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpGrEstbLspForStaleILMHwListEntry should FAILURE if LDP session is not found", i4TestId);

           tILMHwListEntry             ILMHwListEntry;
           UINT4                       u4L3Intf = 1;
           UINT4                       u4Label = 200;
           UINT4                       u4RoutePrefix = 0x16161616; /* 22.22.22.22 */
           UINT4                       u4PrefixLen = 32;
           tMplsIpAddress              Fec;

           MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
           MEMCPY (&Fec.IpAddress, &u4RoutePrefix, IPV4_ADDR_LENGTH);
           Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

           LdpGrUtConfigureCLI ();

           sleep (5);
           MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
           MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = 0; /* Fill 0 to simulate session not found failure */
           MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
           MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&ILMHwListEntry)) = 1; /* Dummy to make entry swap based*/
           MEMCPY (&ILMHwListEntry.Fec, &Fec, sizeof (tMplsIpAddress));
           MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u4PrefixLen;

           /*if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
           {
               break;
           }*/

           if (LdpGrEstbLspForStaleILMHwListEntry (&ILMHwListEntry) == LDP_FAILURE)
           {
               u4RetVal = OSIX_SUCCESS;
           }
           /* Clean up */
           LdpGrUtCleanUpCLI ();
           break;
       }

       case 104:
       {
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpGrEstbLspForStaleILMHwListEntry should FAILURE Route is not found for the "
                                 "prefix which is there in the ILM Hw list entry\n", i4TestId);

           tILMHwListEntry             ILMHwListEntry;
           UINT4                       u4L3Intf = 1;
           UINT4                       u4Label = 200;
           UINT4                       u4RoutePrefix = 0x16161616; /* 22.22.22.22 */
           UINT4                       u4PrefixLen = 32;
           tMplsIpAddress              Fec;

           MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
           MEMCPY (&Fec.IpAddress, &u4RoutePrefix, IPV4_ADDR_LENGTH);
           Fec.i4IpAddrType = MPLS_IPV4_ADDR_TYPE;

           LdpGrUtConfigureCLI ();

           sleep (5);
           MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
           MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)) = 1; 
           MPLS_ILM_HW_LIST_IN_LABEL ((&ILMHwListEntry)) = u4Label;
           MPLS_ILM_HW_LIST_OUT_TNL_INTF ((&ILMHwListEntry)) = 1; /* Dummy to make entry swap based*/
           MEMCPY (&ILMHwListEntry.Fec, &Fec, sizeof (tMplsIpAddress));
           MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)) = u4PrefixLen;

          
           /* Simulate FAILURE by Deleting the route */
           MGMT_UNLOCK ();
           CliExecuteAppCmd ("c t");
           CliExecuteAppCmd ("no ip route 22.22.22.22 255.255.255.255 10.0.0.2");
           CliExecuteAppCmd ("en");
           MGMT_LOCK ();

           if (LdpGrEstbLspForStaleILMHwListEntry (&ILMHwListEntry) == LDP_FAILURE)
           {
               u4RetVal = OSIX_SUCCESS;
           }
           /* Clean up */
           LdpGrUtCleanUpCLI ();
           break;
       }

       case 105:
       {
           /* RAINY-DAY */
           CliPrintf (CliHandle, "TestCaseId: %d: To Verify that "
                                 "LdpGetSessionFromIfIndex should return NULL "
                                 "if no LDP session exists with the peer\n", i4TestId);

           UINT4         u4L3Intf = 1;
           tLdpSession   *pLdpSession = NULL;

           /* LdpGrUtConfigureCLI (); */
           LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
           
           MGMT_UNLOCK ();
           CliExecuteAppCmd ("c t");
           CliExecuteAppCmd ("mpls ldp");
           CliExecuteAppCmd ("en 2");
           CliExecuteAppCmd ("neighbor 22.22.22.22 targeted");
           CliExecuteAppCmd ("ldp label range min 160001 max 160100");
           CliExecuteAppCmd ("no sh");
           CliExecuteAppCmd ("en");
           MGMT_LOCK ();


           /* Simulate Failure */ 
           LdpDeleteLdpSession (pLdpSession, 1);
           if (LdpGetSessionFromIfIndex (u4L3Intf) == NULL)
           {
               u4RetVal = OSIX_SUCCESS;
           }

           /* Clean-up */
           LdpGrUtCreateL3InterfaceCLI (u4L3Intf);
           MGMT_UNLOCK ();
           CliExecuteAppCmd ("c t");
           CliExecuteAppCmd ("mpls ldp");
           CliExecuteAppCmd ("en 2");
           CliExecuteAppCmd ("sh");
           CliExecuteAppCmd ("ex");
           CliExecuteAppCmd ("no en 2");
           CliExecuteAppCmd ("en");
           MGMT_LOCK ();
           break;          
       }
    }

    if (u4RetVal == OSIX_FAILURE)
    {
        CliPrintf (CliHandle, "!!!!!!!!!!  UNIT TEST ID: %-2d %-2s  !!!!!!!!!!\r\n\n", i4TestId,
                "FAILED");
    }
    else
    {
        CliPrintf(CliHandle, "$$$$$$$$$$  UNIT TEST ID: %-2d %-2s  $$$$$$$$$$\r\n\n", i4TestId,
                "PASSED");
    }
}

VOID
LdpGrUtExhaustLdpQMemPool ()
{
    UINT4        u4Count = 0;
   
    for (u4Count = 0; u4Count < MAX_LDP_QDEPTH; u4Count++)
    {
        pLdpQMsg [u4Count] = (UINT1 *) MemAllocMemBlk (LDP_Q_POOL_ID);
    }
}

VOID
LdpGrUtFreeLdpQMemPool ()
{
    UINT4        u4Count = 0;

    for (u4Count = 0; u4Count < MAX_LDP_QDEPTH; u4Count++)
    {
        MemReleaseMemBlock (LDP_Q_POOL_ID, pLdpQMsg [u4Count]);
    }
}

VOID
LdpGrUtConfigureCLI ()
{
   MGMT_UNLOCK ();
   CliExecuteAppCmd ("c t");
   CliExecuteAppCmd ("i g 0/1");
   CliExecuteAppCmd ("no sw");
   CliExecuteAppCmd ("mpls ip");
   CliExecuteAppCmd ("ip address 10.0.0.1 255.0.0.0");
   CliExecuteAppCmd ("no sh");
   CliExecuteAppCmd ("en");

   CliExecuteAppCmd ("c t");
   CliExecuteAppCmd ("int loopback 0");
   CliExecuteAppCmd ("ip add 11.11.11.11 255.255.255.255");
   CliExecuteAppCmd ("no sh");
   CliExecuteAppCmd ("en");

   CliExecuteAppCmd ("c t");
   CliExecuteAppCmd ("ip route 22.22.22.22 255.255.255.255 10.0.0.2");
   CliExecuteAppCmd ("en");

   CliExecuteAppCmd ("c t");
   CliExecuteAppCmd ("mpls ldp");
   CliExecuteAppCmd ("mpls ldp graceful-restart full");
   CliExecuteAppCmd ("en");

   
   CliExecuteAppCmd ("configure terminal");
   CliExecuteAppCmd ("mpls ldp");
   CliExecuteAppCmd ("router-id loopback 0 force");
   CliExecuteAppCmd ("entity 1");
   CliExecuteAppCmd ("ldp label range min 100 max 199");
   CliExecuteAppCmd ("label distribution unsoli");
   CliExecuteAppCmd ("label retention liberal");
   CliExecuteAppCmd ("transport-address tlv loopback 0");
   CliExecuteAppCmd ("no shutdown");
   CliExecuteAppCmd ("end");
   MGMT_LOCK ();
}


VOID
LdpGrUtCleanUpCLI ()
{
    MGMT_UNLOCK ();

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls ldp");
    CliExecuteAppCmd ("en 1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("no en 1");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("i g 0/1");
    CliExecuteAppCmd ("no ip address");
    CliExecuteAppCmd ("no mpls ip");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("no i g 0/1");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("int loopback 0");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no ip add");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("no int loopback 0");
    CliExecuteAppCmd ("en");

    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("no ip route 22.22.22.22 255.255.255.255 10.0.0.2");
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
}

VOID
LdpGrUtCreateAndInitLdpEntityCLI ()
{
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls ldp");
    CliExecuteAppCmd ("entity 1");
    CliExecuteAppCmd ("ldp label range min 100 max 199");
    CliExecuteAppCmd ("label distribution unsolicited");
    CliExecuteAppCmd ("label retention liberal");
    CliExecuteAppCmd ("no shutdown");
    CliExecuteAppCmd ("end");
    MGMT_LOCK ();
}

VOID
LdpGrUtDeleteLdpEntityCLI ()
{
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("mpls ldp");
    CliExecuteAppCmd ("en 1");
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ("no en 1");
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
}

VOID
LdpGrUtCreateStaticILMByCLI (char *pIpAddress, UINT4 u4Label)
{
    UINT1       au1Ip[20];
    UINT1       au1ILMCli[100];

    MEMSET (au1Ip, 0, 10);
    MEMSET (au1ILMCli, 0, 100);
    MEMCPY (au1Ip, pIpAddress, STRLEN (pIpAddress));

    SPRINTF ((char *)au1ILMCli, "mpls static binding ipv4 %s 255.255.255.255 input gig 0/1 %d", au1Ip, u4Label);

    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ((const char *) au1ILMCli);
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
}

VOID
LdpGrUtDeleteStaticILMbyCLI (char *pIpAddress, UINT4 u4Label)
{
    UINT1       au1Ip[20];
    UINT1       au1ILMCli[100];

    MEMSET (au1Ip, 0, 10);
    MEMSET (au1ILMCli, 0, 100);
    MEMCPY (au1Ip, pIpAddress, STRLEN (pIpAddress));

    SPRINTF ((char *)au1ILMCli, "no mpls static binding ipv4 %s 255.255.255.255 input %d", au1Ip, u4Label);

    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ((const char *)au1ILMCli);
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
}

VOID
LdpGrUtCreateStaticFTNByCLI (char *pIpAddress, UINT4 u4Label)
{
    UINT1       au1Ip[20];
    UINT1       au1RouteCli[100];
    UINT1       au1FTNCli[100];    

    MEMSET (au1Ip, 0, 10);
    MEMSET (au1RouteCli, 0, 100);
    MEMSET (au1FTNCli, 0, 100);
    MEMCPY (au1Ip, pIpAddress, STRLEN (pIpAddress));
    
    SPRINTF ((char *)au1RouteCli, "ip route %s 255.255.255.255 10.0.0.2", au1Ip);
    SPRINTF ((char *)au1FTNCli, "mpls static binding ipv4 %s 255.255.255.255 output 10.0.0.2 %d", au1Ip, u4Label); 

    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ((const char *)au1RouteCli);
    CliExecuteAppCmd ("arp 10.0.0.2  00:01:02:03:04:02 gigabitethernet 0/1");
    CliExecuteAppCmd ((const char *)au1FTNCli);
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
}

VOID
LdpGrUtDeleteStaticFTNByCLI (char *pIpAddress, UINT4 u4Label)
{
    UINT1       au1Ip[20];
    UINT1       au1RouteCli[100];
    UINT1       au1FTNCli[100];

    MEMSET (au1Ip, 0, 10);
    MEMSET (au1RouteCli, 0, 100);
    MEMSET (au1FTNCli, 0, 100);
    MEMCPY (au1Ip, pIpAddress, STRLEN (pIpAddress));

    SPRINTF ((char *)au1RouteCli, "no ip route %s 255.255.255.255 10.0.0.2", au1Ip);
    SPRINTF ((char *)au1FTNCli, "no mpls static binding ipv4 %s 255.255.255.255 output 10.0.0.2 %d", au1Ip, u4Label);


    MGMT_UNLOCK ();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ((const char *)au1RouteCli);
    CliExecuteAppCmd ((const char *)au1FTNCli);
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
}

INT4
LdpGrUtExhaustFTNHwListMemPool ()
{
    UINT4        u4Count = 0;

    for (u4Count = 0; u4Count < MAX_MPLSDB_HW_LIST_FTN_ENTRY; u4Count++)
    {
        if ((gpFTNHwListEntry[u4Count] = MplsHwListCreateFTNHwListEntry ()) == NULL)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

VOID
LdpGrUtFreeFTNHwListMemPool ()
{
    UINT4        u4Count = 0;

    for (u4Count = 0; u4Count < MAX_MPLSDB_HW_LIST_FTN_ENTRY; u4Count++)
    {
         MPLS_DEALLOC_FTN_HW_LIST_ENTRY (gpFTNHwListEntry[u4Count]);
    }
}


INT4
LdpGrUtExhaustILMHwListMemPool ()
{
    UINT4        u4Count = 0;

    for (u4Count = 0; u4Count < MAX_MPLSDB_HW_LIST_ILM_ENTRY; u4Count++)
    {
        if ((gpILMHwListEntry[u4Count] = MplsHwListCreateILMHwListEntry ()) == NULL)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4
LdpGrExhaustFTNRbTreeMemPool (VOID)
{
    UINT4        u4Count = 0;

    for (u4Count = 0; u4Count < MAX_MPLSDB_HW_LIST_FTN_ENTRY; u4Count++)
    {
        if ((gpFTNHwListEntry[u4Count] = MemAllocMemBlk (gpFTNHwListRBTree->PoolId)) == NULL)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


VOID
LdpGrFreeFTNRbTreeMemPool (VOID)
{
    UINT4        u4Count = 0;

    for (u4Count = 0; u4Count < MAX_MPLSDB_HW_LIST_FTN_ENTRY; u4Count++)
    {
        MemReleaseMemBlock (gpFTNHwListRBTree->PoolId, (UINT1 *) gpFTNHwListEntry[u4Count]);
    }
}



VOID
LdpGrUtFreeILMHwListMemPool ()
{
    UINT4        u4Count = 0;

    for (u4Count = 0; u4Count < MAX_MPLSDB_HW_LIST_ILM_ENTRY; u4Count++)
    {
         MPLS_DEALLOC_ILM_HW_LIST_ENTRY (gpILMHwListEntry[u4Count]);
    }
}

VOID
LdpGrUtCreateL3InterfaceCLI (UINT4 u4IfIndex)
{
    char        au1Cli[100];


    MEMSET (au1Cli, 0, 100);
    SPRINTF (au1Cli,"int gig 0/%d", u4IfIndex);

    MGMT_UNLOCK(); 
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ((const char *)au1Cli);
    CliExecuteAppCmd ("sh");
    CliExecuteAppCmd ("no sw");
    CliExecuteAppCmd ("mpls ip");
    CliExecuteAppCmd ("ip add 10.0.0.1 255.0.0.0");
    CliExecuteAppCmd ("no sh");
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
}

VOID
LdpGrUtDeleteL3InterfaceCLI (UINT4 u4IfIndex)
{
    char        au1Cli[100];

    MEMSET (au1Cli, 0, 100);
    SPRINTF (au1Cli,"no int gig 0/%d", u4IfIndex);

    MGMT_UNLOCK();
    CliExecuteAppCmd ("c t");
    CliExecuteAppCmd ("i g 0/1");
    CliExecuteAppCmd ("no mpls ip");
    CliExecuteAppCmd ("no ip add");
    CliExecuteAppCmd ("ex");
    CliExecuteAppCmd ((const char *)au1Cli);
    CliExecuteAppCmd ("en");
    MGMT_LOCK ();
}
