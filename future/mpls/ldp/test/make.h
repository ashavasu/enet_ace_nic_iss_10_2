#********************************************************************
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#* $Id: make.h,v 1.1 2014/08/25 14:28:49 siva Exp $
#*******************************************************************

include ../../../LR/make.h
include ../../../LR/make.rule
include ../../make.h
include ../../make.rule
include ../make.h

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)

#project directories
MPLS_INCL_DIR           = $(BASE_DIR)/mpls/mplsinc
PROJECT_TEST_DIR        = $(BASE_DIR)/mpls/ldp/test
PROJECT_INCLUDE_DIR     = $(BASE_DIR)/mpls/ldp/inc
PROJECT_OBJECT_DIR      = $(BASE_DIR)/mpls/ldp/obj
MPLS_LDP_DIR          = $(BASE_DIR)/mpls/ldp/inc
MPLS_RTR_DIR            = $(BASE_DIR)/mpls/mplsrtr/inc
MPLS_DB_DIR             = $(BASE_DIR)/mpls/mplsdb


LDP_TEST_BASE_DIR  = ${BASE_DIR}/mpls/ldp/test
LDP_TEST_SRC_DIR   = ${LDP_TEST_BASE_DIR}/src
LDP_TEST_INC_DIR   = ${LDP_TEST_BASE_DIR}/inc
LDP_TEST_OBJ_DIR   = ${LDP_TEST_BASE_DIR}/obj

LDP_TEST_INCLUDES  = -I$(LDP_TEST_INC_DIR) \
                     -I$(PROJECT_INCLUDE_DIR) \
                     -I$(MPLS_INCL_DIR) \
                     -I$(MPLS_LDP_DIR) \
                     -I$(MPLS_RTR_DIR) \
		             -I$(MPLS_DB_DIR) \
                     $(COMMON_INCLUDE_DIRS)


##########################################################################
#                    End of file make.h                  #
##########################################################################
