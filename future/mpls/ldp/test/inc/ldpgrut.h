
/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: ldpgrut.h,v 1.1 2014/08/25 14:30:06 siva Exp $
 ***
 *** Description: LDP GR  UT Test cases.
 *** NOTE: This file should not be included in release packaging.
 *** ***********************************************************************/

#ifndef _LDP_GR_UT_H
#define _LDP_GR_UT_H

#include "lr.h"
#include "cli.h"
#include "mplscli.h"
#include "mpls.h"
#include "ldpincs.h"
#include "mplsutil.h"
#include "mplsdbsz.h"
#include "../../../mplsrtr/inc/mplsred.h"

#define LDP_GR_MAX_UT_CASES 110
#define MAX_ARGS 5

extern tCfaIfInfo* CfaIfUtlCdbGetIfDbEntry (UINT4);

VOID LdpGrUt (tCliHandle CliHandle, INT4 i4TestType);

VOID LdpGrExecAllUTCases(tCliHandle CliHandle);

VOID LdpGrExecUnitTestCase (tCliHandle CliHandle, INT4 i4TestId);

INT4 LdpGrUtExhaustFTNHwListMemPool(VOID);

VOID LdpGrUtFreeFTNHwListMemPool(VOID);

INT4 LdpGrUtExhaustILMHwListMemPool(VOID);

VOID LdpGrUtFreeILMHwListMemPool(VOID);

VOID LdpGrUtCreateL3InterfaceCLI (UINT4 u4IfIndex);

VOID LdpGrUtDeleteL3InterfaceCLI (UINT4 u4IfIndex);

INT4 LdpGrExhaustFTNRbTreeMemPool (VOID);

INT4 LdpGrExhaustILMRbTreeMemPool (VOID);

VOID LdpGrFreeFTNRbTreeMemPool (VOID);

VOID LdpGrFreeILMRbTreeMemPool (VOID);

VOID LdpGrUtCreateStaticILMByCLI (char *pIpAddress, UINT4 u4Label);

VOID LdpGrUtDeleteStaticILMbyCLI (char *pIpAddress, UINT4 u4Label);

VOID LdpGrUtCreateStaticFTNByCLI (char *pIpAddress, UINT4 u4Label);


VOID LdpGrUtDeleteStaticFTNByCLI (char *pIpAddress, UINT4 u4Label);

VOID LdpGrUtCreateAndInitLdpEntityCLI (VOID);

VOID LdpGrUtDeleteLdpEntityCLI (VOID);

VOID LdpGrUtExhaustLdpQMemPool (VOID);

VOID LdpGrUtFreeLdpQMemPool (VOID);

#endif
