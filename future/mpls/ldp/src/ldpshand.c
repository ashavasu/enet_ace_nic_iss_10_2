/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
* 
* $Id: ldpshand.c,v 1.20 2016/01/11 11:56:48 siva Exp $
*
********************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpshand.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : LDP (SESSION SUB-MODULE)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the Session related Address
 *                             message and Address Withdraw message processing
 *                             routines.
 *----------------------------------------------------------------------------*/

#include "ldpincs.h"

/*****************************************************************************/
/* Function Name : LdpHandleAddrMsg                                          */
/* Description   : This routine is called to handle the Address Message      */
/*                 received on a session.                                    */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session on which the        */
/*                                 Address message is received.              */
/*                 pMsg - Points to the Address message received on the      */
/*                        session.                                           */
/*                                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpHandleAddrMsg (tLdpSession * pSessionEntry, UINT1 *pMsg)
{
    UINT1              *pu1AddrList = NULL;
    UINT2               u2TlvType = 0;
    UINT2               u2AdrListLen = 0;
    UINT2               u2Len = 0;
    eGenAddrType        AddrFmly;

    /* default specifies the Ipv4 addr len */
    UINT1               u1AddrLen = LDP_IPV4ADR_LEN;
    UINT1               u1AdrPrsnt = LDP_FALSE;
    UINT4               u4HIndex = 0;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tPeerIfAdrNode     *pIfAddrTempNode = NULL;
    tTMO_SLL_NODE      *pTempNode = NULL;
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);
    UINT2               u2HLen = 0;
    UINT2               u2AddrHLen = 0;

#ifdef MPLS_IPV6_WANTED 
    UINT1               u1Ipv6HIndex = 0;
    tIp6Addr            TempIpv6ZeroAddr;
    tIp6Addr            TempIpv6Addr; 
#endif 


#ifdef MPLS_IPV6_WANTED
    MEMSET(&TempIpv6Addr,LDP_ZERO,sizeof(tIp6Addr));
    MEMSET(&TempIpv6ZeroAddr,LDP_ZERO,sizeof(tIp6Addr));
#endif

    /* Length of Addr Msg */
     LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    u2HLen -= (UINT2)(LDP_MSG_ID_LEN + LDP_TLV_TYPE_LEN);

    u2TlvType = MSG_GET_FIRST_TLVTYPE (pMsg);

    LDP_DBG4 (LDP_SSN_RX,
              "SESSION: Rcvd Address Msg from %d.%d.%d.%d\n",
            pSessionEntry->pLdpPeer->PeerLdpId[0],
            pSessionEntry->pLdpPeer->PeerLdpId[1],
            pSessionEntry->pLdpPeer->PeerLdpId[2],
            pSessionEntry->pLdpPeer->PeerLdpId[3]);

    /*Masking the U and F bits in the Tlv */
    if (u2TlvType == LDP_ADDRLIST_TLV)
    {
        /* Get Adr Family from Adr Msg */
        AddrFmly = (eGenAddrType) LDP_GET_ADDR_FMLY (pMsg);
        /* Gets the list of addresses from the Addr Msg */
        LDP_GET_ADDRLIST (pMsg, pu1AddrList);

        /* Gets the length of the Address List */
        LDP_GET_2_BYTES (((pMsg) + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN +
                          LDP_TLV_TYPE_LEN), u2AddrHLen);
        /* The TLV len for address messgae is supposed to be a minimum value of 6 .
         *  If the value is lesser than that we must throw malformed error notification*/
        if (u2AddrHLen < LDP_ADDR_TLV_HDR_LEN)
        {
            LDP_DBG (LDP_SSN_PRCS, "SSN: Rx AddrMsg with Bad AddrFamily\n");
            LdpSsmSendNotifCloseSsn (pSessionEntry,
                                     LDP_STAT_TYPE_MALFORMED_TLV_VAL, NULL,
                                     LDP_TRUE);

            return LDP_FAILURE;

        }

        if (!((AddrFmly == IPV4) || (AddrFmly == IPV6)))
        {
            /* AddrType is not IPv4. */
            LDP_DBG (LDP_SSN_PRCS, "SSN: Rx AddrMsg with Bad AddrFamily\n");
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_UNSUPP_ADDR_FMLY, NULL);
            return LDP_FAILURE;

        }
        u2AdrListLen = (UINT2) (u2AddrHLen - LDP_ADDR_FMLY_LEN);

        if ((u2AdrListLen + LDP_ADDR_FMLY_LEN) > (u2HLen))
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "SSN: Bad Tlv Len in Addr Msg. Tx NotifMsg\n");
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_BAD_TLV_LEN, NULL);
            return LDP_FAILURE;
        }
	if(AddrFmly == IPV4)
	{
                u1AddrLen=LDP_IPV4ADR_LEN;
		for (u2Len = 0; u2Len < u2AdrListLen; u2Len += (UINT2)u1AddrLen)
		{
			/* Check if the Iface Address is already present */
			u4HIndex =
				LDP_COMPUTE_HASH_INDEX (OSIX_NTOHL
						(*(UINT4 *) (VOID *) pu1AddrList));

			TMO_HASH_Scan_Bucket (LDP_PEER_IFADR_TABLE (u2IncarnId), u4HIndex,
					pIfAddrNode, tPeerIfAdrNode *)
			{
                if (pIfAddrNode->AddrType != MPLS_IPV4_ADDR_TYPE)
                {
                    continue;
                } 

				if (MEMCMP (pu1AddrList, (UINT1 *) &(pIfAddrNode->IfAddr),
							u1AddrLen) == 0)
				{
					/* Addr already present in table */
					u1AdrPrsnt = LDP_TRUE;
					break;
				}
			}
			if (u1AdrPrsnt == LDP_TRUE)
			{
				/* Go to next address in the list */
				pu1AddrList += u1AddrLen;
				/* Re-Init for next cycle of while loop */
				u1AdrPrsnt = LDP_FALSE;
				pIfAddrNode = NULL;
				pIfAddrTempNode = NULL;
				pTempNode = NULL;

				continue;
			}

			pIfAddrNode = (tPeerIfAdrNode *) MemAllocMemBlk (LDP_PIF_POOL_ID);

			if (pIfAddrNode == NULL)
			{
				LDP_DBG (LDP_SSN_MEM, "SSN: MemAlloc Failed for PeerIfNode\n");
				return LDP_FAILURE;
			}

			MEMSET (pIfAddrNode, 0, sizeof (tPeerIfAdrNode));

			TMO_HASH_INIT_NODE ((tTMO_HASH_NODE
						*) (&(pIfAddrNode->NextHashNode)));
			pIfAddrNode->AddrType = AddrFmly;
			MEMCPY ((UINT1 *) &(pIfAddrNode->IfAddr), pu1AddrList, u1AddrLen);
			pIfAddrNode->pSession = pSessionEntry;

			/* For the 32 Bit this Approach is fine but for 
			 * 64 bit we can not use address as Index.
			 * So if the node is first to be added to Link List
			 * then its index will be 1 for other node index
			 * will be last node index  + 1 . This will
			 * give us unique index for all the nodes */

			pTempNode = TMO_SLL_Last (SSN_GET_PEER_IFADR_LIST (pSessionEntry));

			if (pTempNode != NULL)
			{
				pIfAddrTempNode =
					((tPeerIfAdrNode *) (VOID *)
					 ((UINT1 *) (pTempNode) -
					  MPLS_OFFSET (tPeerIfAdrNode, NextSllNode)));

				pIfAddrNode->u4SessionPeerAddrIndex =
					pIfAddrTempNode->u4SessionPeerAddrIndex + LDP_ONE;
			}
			else
			{
				pIfAddrNode->u4SessionPeerAddrIndex = LDP_ONE;
			}
			LDP_DBG4 (LDP_SSN_PRCS, "SSN: Rcvd Addr Msg with Addr:%x:%x:%x:%x\n",pIfAddrNode->IfAddr.au1Ipv4Addr[0],pIfAddrNode->IfAddr.au1Ipv4Addr[1],pIfAddrNode->IfAddr.au1Ipv4Addr[2],pIfAddrNode->IfAddr.au1Ipv4Addr[3]);
			TMO_HASH_Add_Node (LDP_PEER_IFADR_TABLE (u2IncarnId),
					&(pIfAddrNode->NextHashNode), u4HIndex, NULL);
			TMO_SLL_Add (SSN_GET_PEER_IFADR_LIST (pSessionEntry),
					&(pIfAddrNode->NextSllNode));

			/* Go to next address in the list */
			pu1AddrList += u1AddrLen;
			u1AdrPrsnt = LDP_FALSE;
			pIfAddrNode = NULL;
			pIfAddrTempNode = NULL;
			pTempNode = NULL;

		}
	}
#ifdef MPLS_IPV6_WANTED
	if(AddrFmly == IPV6)
	{
		u1AddrLen=LDP_IPV6ADR_LEN;	

		for (u2Len = 0; u2Len < u2AdrListLen; u2Len += (UINT2)u1AddrLen)
		{
			MEMCPY (TempIpv6Addr.u1_addr, pu1AddrList,u1AddrLen);
			u1Ipv6HIndex =  Ip6AddrHash (&TempIpv6Addr);
                        if (MEMCMP (TempIpv6Addr.u1_addr,TempIpv6ZeroAddr.u1_addr, MPLS_IPV6_ADDR_LEN) == 0)
                        {
                            LDP_DBG (LDP_DBG_PRCS, "Rcvd Address Msg with 0 IPV6 Address\n");
                            continue;
                        }
                        
                        LDP_DBG3 ( LDP_SSN_PRCS,"%s IPV6 u1Ipv6HIndex = %d IPV6 Addr = %s\n",
                             __FUNCTION__, u1Ipv6HIndex, Ip6PrintAddr(&TempIpv6Addr));

			TMO_HASH_Scan_Bucket (LDP_PEER_IPV6_IFADR_TABLE(u2IncarnId),u1Ipv6HIndex,
					pIfAddrNode, tPeerIfAdrNode *)
			{
				pIfAddrNode = LDP_IPV6_OFFSET (pIfAddrNode);
                
                                if (pIfAddrNode->AddrType != MPLS_IPV6_ADDR_TYPE)
                                {
                                    continue;
                                } 
				if ((MEMCMP (&TempIpv6Addr,&pIfAddrNode->IfAddr.Ip6Addr,
								u1AddrLen) == 0))
				{
					/* Addr already present in table */
#if 0
                                        printf("Address is already there =%s\n",Ip6PrintAddr(&TempIpv6Addr));
#endif
					u1AdrPrsnt = LDP_TRUE;
					break;
				}
			}
			if (u1AdrPrsnt == LDP_TRUE)
			{
				/* Go to next address in the list */
				pu1AddrList += u1AddrLen;
				/* Re-Init for next cycle of while loop */
				u1AdrPrsnt = LDP_FALSE;
				pIfAddrNode = NULL;
				pIfAddrTempNode = NULL;
				pTempNode = NULL;

				continue;
			}

			pIfAddrNode = (tPeerIfAdrNode *) MemAllocMemBlk (LDP_PIF_POOL_ID);


			if (pIfAddrNode == NULL)
			{
				LDP_DBG (LDP_SSN_MEM, "SSN: MemAlloc Failed for PeerIfNode\n");
				return LDP_FAILURE;
			}

			MEMSET (pIfAddrNode, 0, sizeof (tPeerIfAdrNode));

			TMO_HASH_INIT_NODE ((tTMO_HASH_NODE
						*) (&(pIfAddrNode->NextIpv6HashNode)));
			pIfAddrNode->AddrType = AddrFmly;
			MEMCPY (&pIfAddrNode->IfAddr.Ip6Addr, pu1AddrList, u1AddrLen);
			pIfAddrNode->pSession = pSessionEntry;

			pTempNode = TMO_SLL_Last (SSN_GET_PEER_IFADR_LIST (pSessionEntry));

			if (pTempNode != NULL)
			{
				pIfAddrTempNode =
					((tPeerIfAdrNode *) (VOID *)
					 ((UINT1 *) (pTempNode) -
					  MPLS_OFFSET (tPeerIfAdrNode, NextSllNode)));

				pIfAddrNode->u4SessionPeerAddrIndex =
					pIfAddrTempNode->u4SessionPeerAddrIndex + LDP_ONE;
			}
			else
			{
				pIfAddrNode->u4SessionPeerAddrIndex = LDP_ONE;
			}

			TMO_HASH_Add_Node (LDP_PEER_IPV6_IFADR_TABLE (u2IncarnId),
					&(pIfAddrNode->NextIpv6HashNode),u1Ipv6HIndex, NULL);
			TMO_SLL_Add (SSN_GET_PEER_IFADR_LIST (pSessionEntry),
					&(pIfAddrNode->NextSllNode));

			/* Go to next address in the list */
			pu1AddrList += u1AddrLen;
			u1AdrPrsnt = LDP_FALSE;
			pIfAddrNode = NULL;
			pIfAddrTempNode = NULL;
			pTempNode = NULL;


		}
	}
#endif
	/* End of for loop */
        return LDP_SUCCESS;
    }
    /* Tlv Type is not Address List Tlv. Mandatory Parameter missing. */
    else
    {

        LDP_DBG (LDP_SSN_PRCS,
                 "SSN: Mandatory Param AddrList TLV is missing. "
                 "Sending Notif Msg.\n");
        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        return LDP_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : LdpHandleAddrWithdrawMsg                                  */
/* Description   : This routine is called to handle the Address Withdraw Msg */
/*                 received on a session.                                    */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session on which the        */
/*                                 Address Withdraw message is received.     */
/*                 pMsg - Points to the Address Withdraw msg received on the */
/*                        session.                                           */
/*                                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpHandleAddrWithdrawMsg (tLdpSession * pSessionEntry, UINT1 *pMsg)
{
    UINT1              *pu1AddrList = NULL;
    UINT2               u2TlvType = 0;
    UINT2               u2AdrListLen = 0;
    UINT2               u2Len = 0;
    eGenAddrType        AddrFmly;

    /* default specifies the Ipv4 addr len */
    UINT1               u1AddrLen = LDP_IPV4ADR_LEN;
    UINT1               u1AdrPrsnt = LDP_FALSE;
    UINT4               u4HIndex = 0;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tPeerIfAdrNode     *pTmpIfAddrNode = NULL;
    tPeerIfAdrNode     *pHTmpIfAddrNode = NULL;
    UINT2               u2HLen = 0;
    UINT2               u2AddrHLen = 0;
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);
    tFec                Fec;
    UINT1              *pu1MsgPtr = pMsg;
    UINT2               u2TlvLen = 0;
    UINT1               u1StatusType = 0;

#ifdef MPLS_IPV6_WANTED
    UINT1               u1Ipv6HIndex = 0;
    tIp6Addr            TempIpv6Addr;
#endif


#ifdef MPLS_IPV6_WANTED
    MEMSET(&TempIpv6Addr,LDP_ZERO,sizeof(tIp6Addr));
#endif
    MEMSET(&Fec,0,sizeof(tFec));


    /* Length of Addr Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    if (u2HLen >= (LDP_MSG_ID_LEN + LDP_TLV_TYPE_LEN))
    {
        u2HLen -= (UINT2)(LDP_MSG_ID_LEN + LDP_TLV_TYPE_LEN);
        u2TlvType = MSG_GET_FIRST_TLVTYPE (pMsg);
        LDP_GET_FEC_TLV_LEN (pMsg, u2TlvLen);
    }
    LDP_DBG4 (LDP_SSN_RX,
              "SESSION: Rcvd Address withdraw Msg from %d.%d.%d.%d\n",
              pSessionEntry->pLdpPeer->PeerLdpId[0],
              pSessionEntry->pLdpPeer->PeerLdpId[1],
              pSessionEntry->pLdpPeer->PeerLdpId[2],
              pSessionEntry->pLdpPeer->PeerLdpId[3]);

    /*Masking the U and F bits in the Tlv */
    if (u2TlvType == LDP_ADDRLIST_TLV)
    {
        /*Get Adr Family from Adr WDraw Msg */
        AddrFmly = (eGenAddrType) LDP_GET_ADDR_FMLY (pMsg);

        /* Gets the list of addrs to be withdrawn from Adr WDraw Msg */
        LDP_GET_ADDRLIST (pMsg, pu1AddrList);

        /* Gets the length of the Address List */
        LDP_GET_2_BYTES (((pMsg) + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN +
                          LDP_TLV_TYPE_LEN), u2AddrHLen);
        u2AdrListLen = (UINT2) (u2AddrHLen - LDP_ADDR_FMLY_LEN);

        /* The TLV len for address messgae is supposed to be a minimum value of 6 .
           ((i.e ) AddrType (2) + Address Len (4))
           *  If the value is lesser than that we must throw malformed error notification
           Section 3.5.5 RFC 5036 */
        if (u2AddrHLen < LDP_ADDR_TLV_HDR_LEN)
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "SSN: Rx AddrWDrawMsg with lesser address list length\n");
            LdpSsmSendNotifCloseSsn (pSessionEntry,
                                     LDP_STAT_TYPE_MALFORMED_TLV_VAL, NULL,
                                     LDP_TRUE);

            return LDP_FAILURE;
        }

        if (!((AddrFmly == IPV4) || (AddrFmly == IPV6)))
        {
            /* AddrType is not IPv4. */
            LDP_DBG (LDP_SSN_PRCS,
                     "SSN: Rx AddrWDrawMsg with Bad AddrFmaliy\n");
            LdpSendNotifMsg
                (pSessionEntry, pMsg, LDP_FALSE, LDP_STAT_TYPE_UNSUPP_ADDR_FMLY,
                 NULL);
            return LDP_FAILURE;

        }

        if ((u2AdrListLen + LDP_ADDR_FMLY_LEN) > (u2HLen))
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "SSN: Bad Tlv Len in AddrW Msg. Tx NotifMsg\n");
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_BAD_TLV_LEN, NULL);
            return LDP_FAILURE;
        }

	if(AddrFmly == IPV4)
	{
		u1AddrLen=LDP_IPV4ADR_LEN;

		for (u2Len = 0; u2Len < u2AdrListLen; u2Len += (UINT2)u1AddrLen)
		{
			/* Get the Iface Addr to be withdrawn, and delete it from
			 * PeerIfAddrTable(HashTable) maintained at Incarnation level and
			 * from IfaceAdrList(SLL) maintained at Peer level */
			u4HIndex =
				LDP_COMPUTE_HASH_INDEX (OSIX_NTOHL
						(*(UINT4 *) (VOID *) pu1AddrList));
			TMO_HASH_DYN_Scan_Bucket (LDP_PEER_IFADR_TABLE (u2IncarnId),
					u4HIndex, pIfAddrNode, pHTmpIfAddrNode,
					tPeerIfAdrNode *)
			{
                if (pIfAddrNode->AddrType != MPLS_IPV4_ADDR_TYPE)
                {
                    continue;
                } 

                LDP_DBG5 (LDP_SSN_PRCS, "%s Address Rcvd in Address Wdraw Msg: %d.%d.%d.%d\n", __func__, 
                                        pu1AddrList[0], pu1AddrList[1], 
                                        pu1AddrList[2], pu1AddrList[3]);
                LDP_DBG5 (LDP_SSN_PRCS, "%s Comparing this address with address in Hash Table:%d.%d.%d.%d\n", __func__, 
                                       pIfAddrNode->IfAddr.au1Ipv4Addr[0],
                                       pIfAddrNode->IfAddr.au1Ipv4Addr[1],
                                       pIfAddrNode->IfAddr.au1Ipv4Addr[2],
                                       pIfAddrNode->IfAddr.au1Ipv4Addr[3]);
				if (MEMCMP
						(pu1AddrList, (UINT1 *) &(pIfAddrNode->IfAddr),
						 u1AddrLen) == 0)
				{
                    LDP_DBG1 (LDP_SSN_PRCS,"%s: Address Matched\n", __func__);
					TMO_HASH_Delete_Node (LDP_PEER_IFADR_TABLE (u2IncarnId),
							&(pIfAddrNode->NextHashNode),
							u4HIndex);
					/* Delete this Adr Node from list maintained at 
					 * Peer level */
					TMO_DYN_SLL_Scan (SSN_GET_PEER_IFADR_LIST (pSessionEntry),
							pIfAddrNode, pTmpIfAddrNode,
							tPeerIfAdrNode *)
					{
                        LDP_DBG5 (LDP_SSN_PRCS, "%s, Address in the PEER If List: %d.%d.%d.%d\n",__func__, 
                                               (LDP_OFFSET (pIfAddrNode))->IfAddr.au1Ipv4Addr[0],
                                               (LDP_OFFSET (pIfAddrNode))->IfAddr.au1Ipv4Addr[1],
                                               (LDP_OFFSET (pIfAddrNode))->IfAddr.au1Ipv4Addr[2],
                                               (LDP_OFFSET (pIfAddrNode))->IfAddr.au1Ipv4Addr[3]);
                        LDP_DBG2 (LDP_SSN_PRCS, "%s, Address Type: %d\n",
                                   __func__, (LDP_OFFSET (pIfAddrNode))->AddrType);
                        if ((LDP_OFFSET (pIfAddrNode))->AddrType != MPLS_IPV4_ADDR_TYPE)
                        {
                            continue;
                        } 

						if (MEMCMP (pu1AddrList,
									(UINT1 *) &((LDP_OFFSET (pIfAddrNode))->
										IfAddr), u1AddrLen) == 0)
						{
							pIfAddrNode = LDP_OFFSET (pIfAddrNode);
                            LDP_DBG5 (LDP_SSN_PRCS, "%s Deleting the address Entry from the list: %d.%d.%d.%d\n", 
                                      __func__, 
                                      pIfAddrNode->IfAddr.au1Ipv4Addr[0],
                                      pIfAddrNode->IfAddr.au1Ipv4Addr[1],
                                      pIfAddrNode->IfAddr.au1Ipv4Addr[2],
                                      pIfAddrNode->IfAddr.au1Ipv4Addr[3]);                   
							TMO_SLL_Delete (SSN_GET_PEER_IFADR_LIST
									(pSessionEntry),
									&(pIfAddrNode->NextSllNode));
							MemReleaseMemBlock (LDP_PIF_POOL_ID,
									(UINT1 *) pIfAddrNode);
							u1AdrPrsnt = LDP_TRUE;
							break;
						}
					}
					break;
				}
			}

			if (u1AdrPrsnt != LDP_TRUE)
			{
				/* The address to be withdrawn is not found in Hash Table! 
				 * This cannot happen if Addr have previously been sent 
				 * in the AddrMsg. This is error on Peer's side. Just 
				 * ignoring this error. */
				LDP_DBG (LDP_SSN_PRCS,
						"SSN: PeerAddr to be WDrawn was not sent "
						"in AddrMsg before\n");
			}
			/* Go to next address in the list */
			pu1AddrList += u1AddrLen;
			u1AdrPrsnt = LDP_FALSE;
		}
	}                        /* end of for loop */
#ifdef MPLS_IPV6_WANTED
	if(AddrFmly == IPV6)
	{
		u1AddrLen=LDP_IPV6ADR_LEN;

		for (u2Len = 0; u2Len < u2AdrListLen; u2Len += (UINT2)u1AddrLen)
		{
			/* Get the Iface Addr to be withdrawn, and delete it from
			 * PeerIfAddrTable(HashTable) maintained at Incarnation level and
			 * from IfaceAdrList(SLL) maintained at Peer level */

			MEMCPY (TempIpv6Addr.u1_addr, pu1AddrList,u1AddrLen);
			u1Ipv6HIndex =  Ip6AddrHash (&TempIpv6Addr);

			TMO_HASH_DYN_Scan_Bucket (LDP_PEER_IPV6_IFADR_TABLE (u2IncarnId),
				 u1Ipv6HIndex, pIfAddrNode, pHTmpIfAddrNode,
					tPeerIfAdrNode *)
			{
                if (pIfAddrNode->AddrType != MPLS_IPV6_ADDR_TYPE)
                {
                    continue;
                } 

				pIfAddrNode = LDP_IPV6_OFFSET (pIfAddrNode);

				if ((MEMCMP(&TempIpv6Addr,&pIfAddrNode->IfAddr.Ip6Addr,
								u1AddrLen) == 0))
				{

					TMO_HASH_Delete_Node (LDP_PEER_IPV6_IFADR_TABLE (u2IncarnId),
							&(pIfAddrNode->NextIpv6HashNode),
							u1Ipv6HIndex);
					/* Delete this Adr Node from list maintained at 
					 * Peer level */
					TMO_DYN_SLL_Scan (SSN_GET_PEER_IFADR_LIST (pSessionEntry),
							pIfAddrNode, pTmpIfAddrNode,
							tPeerIfAdrNode *)
					{
                        if (LDP_OFFSET (pIfAddrNode)->AddrType != MPLS_IPV6_ADDR_TYPE)
                        {
                            continue;
                        } 

						if (MEMCMP (&TempIpv6Addr,
									(UINT1 *) &((LDP_OFFSET (pIfAddrNode))->
										IfAddr.Ip6Addr), u1AddrLen) == 0)
						{
							pIfAddrNode = LDP_OFFSET (pIfAddrNode);
#if 0
							printf("%s : %d Deleting Peer If Addr Node : %s\n",
									__FUNCTION__,__LINE__,
									Ip6PrintAddr(&pIfAddrNode->IfAddr.Ip6Addr));
#endif
							TMO_SLL_Delete (SSN_GET_PEER_IFADR_LIST
									(pSessionEntry),
									&(pIfAddrNode->NextSllNode));
							MemReleaseMemBlock (LDP_PIF_POOL_ID,
									(UINT1 *) pIfAddrNode);
							u1AdrPrsnt = LDP_TRUE;
							break;
						}
					}
					break;
				}
			}

			if (u1AdrPrsnt != LDP_TRUE)
			{
				/* The address to be withdrawn is not found in Hash Table! 
				 * This cannot happen if Addr have previously been sent 
				 * in the AddrMsg. This is error on Peer's side. Just 
				 * ignoring this error. */
				LDP_DBG (LDP_SSN_PRCS,
						"SSN: PeerAddr to be WDrawn was not sent "
						"in AddrMsg before\n");
			}
			/* Go to next address in the list */
			pu1AddrList += u1AddrLen;
			u1AdrPrsnt = LDP_FALSE;
		}

	}
#endif
	return LDP_SUCCESS;
    }
    else if (u2TlvType == LDP_FEC_TLV)
    {
        /* Now the pointer points to FEC Element TLV */
        pu1MsgPtr =
            pu1MsgPtr + (LDP_MSG_HDR_LEN + LDP_MSGID_LEN + LDP_TLV_HDR_LEN);

        /* FEC TLV Contents copied into local FEC Structure */

        if (LdpStrToFec (pu1MsgPtr, u2TlvLen, &Fec, &u1StatusType) ==
            LDP_FAILURE)
        {
            if (u1StatusType == LDP_STAT_TYPE_MALFORMED_TLV_VAL)
            {
                LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                         (UINT1 *) pMsg, LDP_TRUE);
            }
            /* The FEC TLV has unknown FEC type or unsupported address family.
               In that case just send the notification message */
            else if ((u1StatusType == LDP_STAT_TYPE_UNKNOWN_FEC) ||
                     (u1StatusType == LDP_STAT_TYPE_UNSUPP_ADDR_FMLY))
            {
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 u1StatusType, NULL);
            }
            return LDP_FAILURE;
        }

        if ((Fec.u1FecElmntType == LDP_FEC_PWVC_TYPE) ||
            (Fec.u1FecElmntType == LDP_FEC_GEN_PWVC_TYPE))
        {
            if (LdpRegStatus (Fec.u1FecElmntType) == LDP_REGISTER)
            {
                if (LdpHandleFecMsg (pSessionEntry, pMsg, LDP_ADDR_WITHDRAW_MSG,
                                     Fec.u1FecElmntType) != LDP_SUCCESS)
                {
                    return LDP_FAILURE;
                }
                return LDP_SUCCESS;
            }
            else
            {
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 LDP_STAT_TYPE_UNKNOWN_FEC, NULL);
                return LDP_FAILURE;
            }
        }
    }
    /* Tlv Type is not Address List Tlv or FEC Tlv. Mandatory Parameter missing. */
    LDP_DBG (LDP_SSN_PRCS,
             "SSN: Mandatory Param AddrList TLV or FEC TLV is missing. "
             "Sending Notif Msg.\n");
    LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                     LDP_STAT_MISSING_MSG_PARAM, NULL);
    return LDP_FAILURE;
}

#ifdef MPLS_IPV6_WANTED

BOOL1 LdpIfGetAddressIndex(tPeerIfAdrNode *pPeerIfAdrNode,UINT4 u4IfIndex) 
{ 
	tLdpAdjacency      *pLdpAdj = NULL;
	tLdpAdjacency      *pTempAdj = NULL;

	if(pPeerIfAdrNode->pSession==NULL)
	{
		return LDP_FALSE;
	}

	TMO_DYN_SLL_Scan (&(pPeerIfAdrNode->pSession->AdjacencyList),
			pLdpAdj, pTempAdj, tLdpAdjacency *)
	{
		if (pLdpAdj->u4IfIndex == u4IfIndex)
		{
                      return LDP_TRUE;
		}
	}

	return LDP_FALSE; 
}
#endif

/*---------------------------------------------------------------------------*/
/*                       End of file ldpshand.c                              */
/*---------------------------------------------------------------------------*/
