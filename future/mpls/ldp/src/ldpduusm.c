/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpduusm.c,v 1.40 2017/07/10 11:26:05 siva Exp $
 *
 * Description: This file contains DU UPstream state machine routines.
 ********************************************************************/

#include "ldpincs.h"
#include "ldpduusm.h"

/*****************************************************************************/
/* Function Name : LdpDuUpSm                                                 */
/* Description   : This Function is called by other internal modules to      */
/*                 perform the corresponding operations required by the      */
/*                 DU Upstream state machine                                 */
/* Input(s)      : pUsLspCtrlBlock  - Pointer to the LSP ctrl block stcture  */
/*                 u1Event        - The event to be passed to state machine  */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpSm (tUstrLspCtrlBlock * pUsLspCtrlBlock, UINT1 u1Event,
           tLdpMsgInfo * pMsg)
{
    LDP_DBG2 (LDP_ADVT_SEM,
              "Upstream Control Block Current State: %s Evt %s \n",
              au1LdpDuUpStates[pUsLspCtrlBlock->u1LspState],
              au1LdpDuUpEvents[u1Event]);

    LDP_DU_UP_FSM[LCB_STATE (pUsLspCtrlBlock)][u1Event] (pUsLspCtrlBlock, pMsg);

    LDP_DBG1 (LDP_ADVT_SEM,
              "Upstream Control Block Next  State: %s \n",
              au1LdpDuUpStates[pUsLspCtrlBlock->u1LspState]);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpIdLblReq                                           */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and Label Request Event received.              */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpIdLblReq (tUstrLspCtrlBlock * pLspUpCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    UINT1               u1RouteStatus = LDP_TRUE;
    UINT1               u1SsnState;
    tGenU4Addr          NextHopAddr;
    UINT4               u4IfIndex;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tLdpSession        *pLdpSessionEntry = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    UINT1               u1DnCtlBlockPresent = FALSE;
    tLdpSession        *pLdpSession = UPSTR_USSN (pLspUpCtrlBlock);
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pLdpSession);

    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));

    if (LdpIsLsrEgress (&UPSTR_FEC (pLspUpCtrlBlock), &u1RouteStatus,
                        &NextHopAddr.Addr, &u4IfIndex,
                        (UINT2) SSN_GET_INCRN_ID (pLdpSession)) == LDP_TRUE)
    {

#ifdef LDP_GR_WANTED
        /* Do not send the Label Map msg if the Upstream session is Stale */
        if (UPSTR_USSN (pLspUpCtrlBlock)->u1StaleStatus == LDP_TRUE)
        {
            return;
        }
        else
        {
            LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_DN_MAP,
                       pLdpMsgInfo);
        }
#else
        LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_DN_MAP, pLdpMsgInfo);
#endif
        return;
    }

    NextHopAddr.u2AddrType = UPSTR_FEC (pLspUpCtrlBlock).u2AddrFmly;

    if (u1RouteStatus == LDP_FALSE)
    {
        /*
         * Notification message sent to upstream that no route is present
         * and the LSP Control block is released.
         */
        LdpSendNotifMsg (UPSTR_USSN (pLspUpCtrlBlock), pLdpMsgInfo->pu1Msg,
                         LDP_FALSE, LDP_STAT_TYPE_NO_ROUTE, NULL);
        LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
        return;
    }

    /*  If it is a proxy egress */
    if (LdpGetPeerSession (&NextHopAddr, u4IfIndex,
                           &pLdpSessionEntry, u2IncarnId) == LDP_FAILURE)
    {
        if (gi4MplsSimulateFailure != LDP_SIM_FAILURE_DISABLE_PROXY_EGRESS)
        {
            LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_DN_MAP,
                       pLdpMsgInfo);

        }
        return;
    }
    else
    {
        /* As per RFC 5036 section A.1.1, when an LSR receives label request
         * message for a FEC whose msg source is the next hop,
         * it has to send a notification message with Loop Detected status code
         * */
        if (pLdpSessionEntry == UPSTR_USSN (pLspUpCtrlBlock))
        {
            LdpSendNotifMsg (UPSTR_USSN (pLspUpCtrlBlock), pLdpMsgInfo->pu1Msg,
                             LDP_FALSE, LDP_STAT_TYPE_LOOP_DETECTED, NULL);
            LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
            return;
        }

        SSN_GET_SSN_STATE (pLdpSessionEntry, u1SsnState);
        if ((u1SsnState == LDP_SSM_ST_OPERATIONAL) &&
            (SSN_ADVTYPE (pLdpSessionEntry) == LDP_DSTR_UNSOLICIT))
        {
            /* Scan the Downstream Lsp Control Block List */
            TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSessionEntry), pLdpSllNode,
                              pTempSllNode, tTMO_SLL_NODE *)
            {
                pLspCtrlBlock = SLL_TO_LCB (pLdpSllNode);
                if (pLspCtrlBlock != NULL)
                {
                    if ((MEMCMP (&LCB_FEC (pLspCtrlBlock),
                                 &UPSTR_FEC (pLspUpCtrlBlock),
                                 sizeof (tFec))) == 0)
                    {
                        u1DnCtlBlockPresent = TRUE;
                        break;
                    }
                }
            }
        }
    }
    /* A.1.1. Receive Label Request
     * LRq.9   Perform LSR Label Distribution procedure:
     * For Downstream Unsolicited Independent Control 
     * */

    LdpDuIndLblReqOrNewFecProcedure (u1DnCtlBlockPresent, pLspUpCtrlBlock,
                                     pLspCtrlBlock, pLdpMsgInfo,
                                     pLdpSessionEntry, &NextHopAddr);

    /* A.1.1. Receive Label Request
     * LRq.9   Perform LSR Label Distribution procedure:
     * For Downstream Unsolicited Ordered Control
     * */

    LdpDuOrderLblReqOrNewFecProcedure (u1DnCtlBlockPresent, pLspUpCtrlBlock,
                                       pLspCtrlBlock, pLdpMsgInfo,
                                       pLdpSessionEntry, &NextHopAddr);
    if (UPSTR_STATE (pLspUpCtrlBlock) == LDP_DU_UP_LSM_ST_RSRC_AWT)
    {
        LdpSendNotifMsg (UPSTR_USSN (pLspUpCtrlBlock), pLdpMsgInfo->pu1Msg,
                         LDP_FALSE, LDP_STAT_TYPE_NO_LBL_RSRC, NULL);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpIdLblAbrtReq                                       */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and Label Abort Request Event received.        */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpIdLblAbrtReq (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                     tLdpMsgInfo * pLdpMsgInfo)
{
    tLspCtrlBlock      *pLspCtrlBlock = NULL;

    pLspCtrlBlock = pLspUpCtrlBlock->pDnstrCtrlBlock;

    if (pLspCtrlBlock != NULL)
    {
        if (LCB_STATE (pLspCtrlBlock) != LDP_LSM_ST_EST)
        {
            /* Sending Abort Msg DStream and going to IDLE State */
            LdpSendLblAbortReqMsg (pLspCtrlBlock);
            LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_IDLE;
        }
        /* As per rfc 5036, section A.1.3 if an LSR receives a Label Mapping message
         * in response to a Label request message after it has sent a Label Abort Request
         * to abort the Label Request, the label in the Label Mapping message is
         * valid.  The LSR may choose to use the label or to release it with a
         * Label Release message*/

        else if (gi4MplsSimulateFailure ==
                 LDP_SIM_FAILURE_PROPAGATE_LBL_RELEASE)
        {
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);

        }
    }
    UNUSED_PARAM (pLdpMsgInfo);
}

/*****************************************************************************/
/* Function Name : LdpDuIndLblReqOrNewFecProcedure                           */
/* Description   : This routine handles Label Req message in Downstream      */
/*                 unsolicited and independent control as per algorithm      */
/*                 mentioned in RFC 5036 section A.1.1                       */
/* Input(s)      : u1DnCtlBlockPresent - dn ctrl block availablilty          */
/*                 pLspUpCtrlBlock - Upstream control block                  */
/*                 pLspCtrlBlock   - downstream control block                */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/*                 pLdpSessionEntry - Ldp Session                            */
/*                 u4NextHopAddr - Next Hop Address                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpDuIndLblReqOrNewFecProcedure (UINT1 u1DnCtlBlockPresent,
                                 tUstrLspCtrlBlock * pLspUpCtrlBlock,
                                 tLspCtrlBlock * pLspCtrlBlock,
                                 tLdpMsgInfo * pLdpMsgInfo,
                                 tLdpSession * pLdpSessionEntry,
                                 tGenU4Addr * pNextHopAddr)
{
    LDP_DBG (LDP_SSN_PRCS, "LdpDuIndLblReqOrNewFecProcedure ENTRY\n");

    if (SSN_GET_LBL_ALLOC_MODE (pLdpSessionEntry) != LDP_INDEPENDENT_MODE)
    {
        return;
    }

    /* Send Label Request message to downstream, if the LSR has not received 
     * Label mapping message for the FEC already and the Label Request procedure 
     * is not configured as Request Never
     * */
    if (((pLspCtrlBlock == NULL)
         || (LCB_STATE (pLspCtrlBlock) != LDP_DU_DN_LSM_ST_EST))
        && ((SSN_GET_ENTITY (pLdpSessionEntry)->u2LabelRet) ==
            LDP_CONSERVATIVE_MODE))
    {
        LdpDuSendLblReq (pLspUpCtrlBlock, pLspCtrlBlock, pLdpMsgInfo,
                         pLdpSessionEntry, pNextHopAddr);
    }

    LDP_MSG_HC (pLdpMsgInfo) = LDP_ZERO;
    LDP_MSG_PV_PTR (pLdpMsgInfo) = NULL;
    LDP_MSG_PV_COUNT (pLdpMsgInfo) = LDP_ZERO;

    /* If LSR has received label mapping message for the FEC already,
     * update the HOP count from downstream control block. 
     * Else set HOP count value as zero. 
     * */
    if ((u1DnCtlBlockPresent == TRUE) && (pLspCtrlBlock != NULL))
    {
        if (pLspUpCtrlBlock->pDnstrCtrlBlock == NULL)
        {
            TMO_SLL_Add ((tTMO_SLL *) (&(LCB_UPSTR_LIST (pLspCtrlBlock))),
                         (tTMO_SLL_NODE *) (&(pLspUpCtrlBlock->NextUpstrNode)));
            pLspUpCtrlBlock->pDnstrCtrlBlock = pLspCtrlBlock;
        }
        if (LCB_STATE (pLspCtrlBlock) == LDP_DU_DN_LSM_ST_EST)
        {
            LDP_MSG_HC (pLdpMsgInfo) = LCB_HCOUNT (pLspCtrlBlock);
            LDP_MSG_PV_PTR (pLdpMsgInfo) = pLspCtrlBlock->pu1PVTlv;
            LDP_MSG_PV_COUNT (pLdpMsgInfo) = pLspCtrlBlock->u1PVCount;
        }
    }

    /* Send Label Mapping message to upstream for both cases
     * 1. LSR has received Label mapping message already for the FEC
     * 2. LSR has not received Label mapping message already for the FEC
     * */
#ifdef LDP_GR_WANTED
    /* For Stale control block, separate event is created since reusing the 
     * existing internal map event, it would be difficult to differentiate
     * from which flow the internal map is called. Eg: Int Map is given 
     * when the Upstream control block is in POP mode and mapping is received
     * by the DLCB. In that case, it is required that ILM should be modified to
     * SWAP mode in the H/w. However from another flow, when the stale session has come
     * and Labels are given to upstream routers, ILM should not be modified
     * only it should be refreshed. Hence separate event is created */
    if (pLspUpCtrlBlock->u1StaleStatus == LDP_TRUE)
    {
        LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_REFRESH, pLdpMsgInfo);
    }
    else
    {
        LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_DN_MAP, pLdpMsgInfo);
    }
#else
    LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_DN_MAP, pLdpMsgInfo);
#endif

    LDP_DBG (LDP_SSN_PRCS, "LdpDuIndLblReqOrNewFecProcedure EXIT\n");
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuOrderLblReqOrNewFecProcedure                         */
/* Description   : This routine handles Label Req message in Downstream      */
/*                 unsolicited and ordered control as per algorithm          */
/*                 mentioned in RFC 5036 section A.1.1                       */
/* Input(s)      : u1DnCtlBlockPresent - dn ctrl block availablilty          */
/*                 pLspUpCtrlBlock - Upstream control block                  */
/*                 pLspCtrlBlock   - downstream control block                */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/*                 pLdpSessionEntry - Ldp Session                            */
/*                 u4NextHopAddr - Next Hop Address                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpDuOrderLblReqOrNewFecProcedure (UINT1 u1DnCtlBlockPresent,
                                   tUstrLspCtrlBlock * pLspUpCtrlBlock,
                                   tLspCtrlBlock * pLspCtrlBlock,
                                   tLdpMsgInfo * pLdpMsgInfo,
                                   tLdpSession * pLdpSessionEntry,
                                   tGenU4Addr * pNextHopAddr)
{
    LDP_DBG (LDP_SSN_PRCS, "LdpDuOrderLblReqOrNewFecProcedure ENTRY\n");

    if (SSN_GET_LBL_ALLOC_MODE (pLdpSessionEntry) != LDP_ORDERED_MODE)
    {
        return;
    }
    /* Send Label Mapping message to upstream if only LSR has received Label mapping 
     * message already for the FEC
     * */
    if ((u1DnCtlBlockPresent == TRUE) &&
        (pLspCtrlBlock != NULL) &&
        (LCB_STATE (pLspCtrlBlock) == LDP_DU_DN_LSM_ST_EST))
    {
        LDP_MSG_HC (pLdpMsgInfo) = LCB_HCOUNT (pLspCtrlBlock);
        LDP_MSG_PV_PTR (pLdpMsgInfo) = pLspCtrlBlock->pu1PVTlv;
        LDP_MSG_PV_COUNT (pLdpMsgInfo) = pLspCtrlBlock->u1PVCount;

        if (pLspUpCtrlBlock->pDnstrCtrlBlock == NULL)
        {
            TMO_SLL_Add ((tTMO_SLL *) (&(LCB_UPSTR_LIST (pLspCtrlBlock))),
                         (tTMO_SLL_NODE *) (&(pLspUpCtrlBlock->NextUpstrNode)));
            pLspUpCtrlBlock->pDnstrCtrlBlock = pLspCtrlBlock;
        }
#ifdef LDP_GR_WANTED
        if (pLspUpCtrlBlock->u1StaleStatus == LDP_TRUE)
        {
            LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_REFRESH,
                       pLdpMsgInfo);
        }
        else
        {
            LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_DN_MAP,
                       pLdpMsgInfo);
        }
#else
        LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_DN_MAP, pLdpMsgInfo);
#endif
    }

    /* Create downstream ctrl block and Send Label Request message to downstream, 
     * if the LSR has not received Label mapping message for the FEC already 
     * and the Label Request procedure is not configured as Request Never
     * */
    else if (((pLspCtrlBlock == NULL)
              || (LCB_STATE (pLspCtrlBlock) != LDP_DU_DN_LSM_ST_EST))
             && ((SSN_GET_ENTITY (pLdpSessionEntry)->u2LabelRet) ==
                 LDP_CONSERVATIVE_MODE))
    {
        LdpDuSendLblReq (pLspUpCtrlBlock, pLspCtrlBlock, pLdpMsgInfo,
                         pLdpSessionEntry, pNextHopAddr);
    }
    else
    {
        LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
    }

    LDP_DBG (LDP_SSN_PRCS, "LdpDuOrderLblReqOrNewFecProcedure EXIT\n");
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuSendLblReq                                           */
/* Description   : This routine sends Label Req message in Downstream        */
/*                 unsolicited mode                                          */
/* Input(s)      : pLspUpCtrlBlock - Upstream control block                  */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/*                 pLdpSessionEntry - Ldp Session                            */
/*                 u4NextHopAddr - Next Hop Address                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpDuSendLblReq (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                 tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo,
                 tLdpSession * pLdpSessionEntry, tGenU4Addr * pNextHopAddr)
{

#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = LDP_ZERO;
#endif
    UINT4               u4IfIndex = LDP_ZERO;

    /* If the LSR recognizes a FEC in its routing table in DU mode, it will 
     * send label req message for the FEC to its downstream (Intermediate case)
     * */
    if (pLspCtrlBlock == NULL)
    {
        if (LdpCreateDnstrCtrlBlock (&pLspCtrlBlock) == LDP_FAILURE)
        {
            LdpDeleteUpstrCtrlBlock (pLspUpCtrlBlock);
            return;
        }
        if ((LDP_ENTITY_LDP_OVER_RSVP_FLAG
             (SSN_GET_ENTITY (pLdpSessionEntry)) == LDP_SNMP_TRUE) &&
            (SSN_GET_ENTITY (pLdpSessionEntry)->OutStackTnlInfo.u4TnlId !=
             LDP_ZERO))
        {
            if (LdpGetL3IfaceFromLdpOverRsvpOutTnl
                (SSN_GET_ENTITY (pLdpSessionEntry), &u4IfIndex) == LDP_FAILURE)
            {
                MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlock);
                return;
            }
        }
        else
        {
            u4IfIndex =
                LdpSessionGetIfIndex (pLdpSessionEntry,
                                      UPSTR_FEC (pLspUpCtrlBlock).u2AddrFmly);
        }

#ifdef CFA_WANTED
        if (CfaIfmCreateStackMplsTunnelInterface (u4IfIndex, &u4MplsTnlIfIndex)
            == CFA_FAILURE)
        {

            MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlock);
            return;
        }

        pLspCtrlBlock->u4OutIfIndex = u4MplsTnlIfIndex;
#else
        /* Update L3 INF Index */
        pLspCtrlBlock->u4OutIfIndex =
            LdpSessionGetIfIndex (pLdpSessionEntry,
                                  UPSTR_FEC (pLspUpCtrlBlock).u2AddrFmly);
#endif
        /* Add the FEC, session, Next Hop Addr and IfIndex to the
         * down stream ctrl block
         */
        MEMCPY ((UINT1 *) &(pLspCtrlBlock->Fec),
                (UINT1 *) &(UPSTR_FEC (pLspUpCtrlBlock)), sizeof (tFec));

        LdpCopyAddr (&pLspCtrlBlock->NextHopAddr, &pNextHopAddr->Addr,
                     pNextHopAddr->u2AddrType);

        pLspCtrlBlock->u2AddrType = pNextHopAddr->u2AddrType;

        LCB_DSSN (pLspCtrlBlock) = pLdpSessionEntry;

        /*
         * The LSP control block is added to the list of down stream LSP
         * ctrlblock list with respect to the downstream session.
         *  */
        TMO_SLL_Insert (&SSN_DLCB (pLdpSessionEntry), NULL, (tTMO_SLL_NODE *)
                        & (pLspCtrlBlock->NextDSLspCtrlBlk));

    }
    /* As per RFC 5036 section 2.8.1, if LSR receives a Label Request message 
     * for a particular FEC and R has previously sent a Label Request message
     * for that FEC to its next hop and has not yet received a reply, 
     * and if LSR intends to merge the newly received Label Request with the existing
     * outstanding Label Request, then LSR does not propagate the Label
     * Request to the next hop
     *
     * If the LSR has received notification message with NO-ROUTE/LOOP DETECTED/
     * NO LABEL RESOURCE status code, it should not forward the label
     * request message to the downstream
     * */
    else if ((gi4MplsSimulateFailure == LDP_SIM_FAILURE_LBL_REQ_MERGE) ||
             (pLspCtrlBlock->u1LblReqFwdFlag == LDP_FALSE) ||
             (pLdpSessionEntry->u1StatusRecord == LDP_STAT_TYPE_NO_LBL_RSRC))
    {
        return;
    }

    LDP_MSG_CTRL_BLK_PTR (pLdpMsgInfo) = pLspUpCtrlBlock;

    LCB_USSN (pLspCtrlBlock) = UPSTR_USSN (pLspUpCtrlBlock);

    if (pLspUpCtrlBlock->pDnstrCtrlBlock == NULL)
    {
        TMO_SLL_Add ((tTMO_SLL *) (&(LCB_UPSTR_LIST (pLspCtrlBlock))),
                     (tTMO_SLL_NODE *) (&(pLspUpCtrlBlock->NextUpstrNode)));
        pLspUpCtrlBlock->pDnstrCtrlBlock = pLspCtrlBlock;
    }
    LdpSendLblReqMsg (pLspCtrlBlock, LDP_MSG_HC (pLdpMsgInfo),
                      (pLdpMsgInfo->pu1PVTlv + LDP_TLV_HDR_LEN),
                      LDP_MSG_PV_COUNT (pLdpMsgInfo),
                      &pLdpMsgInfo->au1UnknownTlv[0],
                      pLdpMsgInfo->u1UnknownTlvLen);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuSendLblReqForIngress                                 */
/* Description   : This routine sends Label Req message in Downstream        */
/*                 unsolicited mode for Ingress once it recognizes a FEC     */
/* Input(s)      : pLdpSessionEntry - Ldp Session                            */
/*                 u4NextHopAddr - Next Hop Address                          */
/*                 Fec           - Fec Info                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpDuSendLblReqForIngress (tLdpSession * pLdpSessionEntry, tFec Fec,
                           tGenU4Addr * pNextHopAddr)
{
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = LDP_ZERO;
#endif
    UINT4               u4IfIndex = LDP_ZERO;

    if (LdpCreateDnstrCtrlBlock (&pLspCtrlBlock) == LDP_FAILURE)
    {
        return;
    }
    if ((LDP_ENTITY_LDP_OVER_RSVP_FLAG
         (SSN_GET_ENTITY (pLdpSessionEntry)) == LDP_SNMP_TRUE) &&
        (SSN_GET_ENTITY (pLdpSessionEntry)->OutStackTnlInfo.u4TnlId !=
         LDP_ZERO))
    {
        if (LdpGetL3IfaceFromLdpOverRsvpOutTnl
            (SSN_GET_ENTITY (pLdpSessionEntry), &u4IfIndex) == LDP_FAILURE)
        {
            MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlock);
            return;
        }
    }
    else
    {
        u4IfIndex = LdpSessionGetIfIndex (pLdpSessionEntry, Fec.u2AddrFmly);
    }

#ifdef CFA_WANTED
    if (CfaIfmCreateStackMplsTunnelInterface (u4IfIndex, &u4MplsTnlIfIndex)
        == CFA_FAILURE)
    {

        MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlock);
        return;
    }

    pLspCtrlBlock->u4OutIfIndex = u4MplsTnlIfIndex;
#else
    /* Update L3 INF Index */
    pLspCtrlBlock->u4OutIfIndex = u4IfIndex;
#endif

    /* Add the FEC, session, Next Hop Addr and IfIndex to the
     * down stream ctrl block
     */
    MEMCPY ((UINT1 *) &(pLspCtrlBlock->Fec), (UINT1 *) &(Fec), sizeof (tFec));

    LdpCopyAddr (&pLspCtrlBlock->NextHopAddr, &pNextHopAddr->Addr,
                 pNextHopAddr->u2AddrType);
    pLspCtrlBlock->u2AddrType = pNextHopAddr->u2AddrType;

    LCB_DSSN (pLspCtrlBlock) = pLdpSessionEntry;

    /*
     * The LSP control block is added to the list of down stream LSP
     * ctrlblock list with respect to the downstream session.
     *  */
    TMO_SLL_Insert (&SSN_DLCB (pLdpSessionEntry), NULL, (tTMO_SLL_NODE *)
                    & (pLspCtrlBlock->NextDSLspCtrlBlk));

    LdpSendLblReqMsg (pLspCtrlBlock, 0, NULL, 0, NULL, 0);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpIdIntDnMap                                         */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and a Internal Downstream Map Event received.  */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpIdIntDnMap (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                   tLdpMsgInfo * pLdpMsgInfo)
{
    tLdpSession        *pLdpSession = UPSTR_USSN (pLspUpCtrlBlock);
    tLdpEntity         *pLdpEntity = NULL;
    uLabel              Label;
    UINT1              *pu1PvPtr = NULL;
    UINT4               u4SessIfIndex = LdpSessionGetIfIndex (pLdpSession,
                                                              UPSTR_FEC
                                                              (pLspUpCtrlBlock).
                                                              u2AddrFmly);
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pLdpSession);
    UINT2               u2LabelOperation;
#ifdef LDP_GR_WANTED
    tILMHwListEntry    *pILMHwListEntry = NULL;
    UINT4               u4AdjIfIndex = 0;
    tLdpAdjacency      *pLdpAdjacency = NULL;
    UINT1               u1IsLabelAllocReq = MPLS_TRUE;
    tMplsIpAddress      ILMHwListNextHop;
    tMplsIpAddress      ILMHwListFec;
    UINT4               u4OutTnlIfIndex = 0;
    uLabel              ILMHwListOutLabel;
    UINT1               u1ILMHwListPrefLen = 0;
#endif

#ifdef LDP_GR_WANTED
    MEMSET (&ILMHwListNextHop, 0, sizeof (tMplsIpAddress));
    MEMSET (&ILMHwListFec, 0, sizeof (tMplsIpAddress));
    MEMSET (&ILMHwListOutLabel, 0, sizeof (uLabel));

    /* Fetch the ILM Hw List entry for the FEC and session */
    ILMHwListFec.i4IpAddrType = UPSTR_FEC (pLspUpCtrlBlock).u2AddrFmly;

#ifdef MPLS_IPV6_WANTED
    if (ILMHwListFec.i4IpAddrType == LDP_ADDR_TYPE_IPV6)
    {
        MEMCPY (&ILMHwListFec.IpAddress.Ip6Addr,
                &(LDP_IPV6_U4_ADDR (UPSTR_FEC (pLspUpCtrlBlock).Prefix)),
                IPV6_ADDR_LENGTH);
    }
    else
#endif
    {
        MEMCPY (ILMHwListFec.IpAddress.au1Ipv4Addr,
                &(LDP_IPV4_U4_ADDR (UPSTR_FEC (pLspUpCtrlBlock).Prefix)),
                IPV4_ADDR_LENGTH);
    }

    u1ILMHwListPrefLen = UPSTR_FEC (pLspUpCtrlBlock).u1PreLen;

    TMO_SLL_Scan (SSN_GET_ADJ_LIST (pLdpSession), pLdpAdjacency,
                  tLdpAdjacency *)
    {
        u4AdjIfIndex = pLdpAdjacency->u4IfIndex;
        pILMHwListEntry = MplsGetILMHwListEntryFromFecAndInIntf (ILMHwListFec,
                                                                 u1ILMHwListPrefLen,
                                                                 u4AdjIfIndex);
        if (pILMHwListEntry != NULL)
        {
#ifdef MPLS_IPV6_WANTED
            if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
                MPLS_IPV6_ADDR_TYPE)
            {
                LDP_DBG1 (GRACEFUL_DEBUG,
                          "LdpDuUpIdIntDnMap: ILM Hw List Entry found for "
                          "FEC: %s and interface:\n",
                          Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                        (pILMHwListEntry)));
            }
            else
#endif
            {
                LDP_DBG5 (GRACEFUL_DEBUG,
                          "LdpDuUpIdIntDnMap: ILM Hw List Entry found for "
                          "FEC: %d.%d.%d.%d and interface: %d\n",
                          MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
                          MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
                          MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
                          MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3],
                          MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
            }
            /* ILM hw list entry found */
            break;
        }
    }
#endif

    /* If it is an intermediate node and entity is configured for ATM,
     * then the merge count is checked. If the merge count is
     * reached , then we get into resource awaited state
     */
    if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
        (LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock)) != NULL))
    {
        pLdpEntity =
            SSN_GET_ENTITY (LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock)));
        if (SSN_GET_LBL_TYPE (LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) ==
            LDP_ATM_MODE)
        {
            if (LDP_ENTITY_MRG_TYPE (pLdpEntity) == LDP_VP_MRG)
            {
                if (TMO_SLL_Count ((tTMO_SLL *) &
                                   (LCB_UPSTR_LIST
                                    (UPSTR_DSTR_PTR (pLspUpCtrlBlock)))) ==
                    LDP_MAX_ATM_VP_MRG_COUNT (u2IncarnId))
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT:Mrg Failed-Max VP MrgCnt "
                             "Reached-LdpDuUpIdIntDnMap\n");
                    LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_RSRC_AWT;
                    return;
                }
            }
            if (LDP_ENTITY_MRG_TYPE (pLdpEntity) == LDP_VC_MRG)
            {
                if (TMO_SLL_Count ((tTMO_SLL *) &
                                   (LCB_UPSTR_LIST
                                    (UPSTR_DSTR_PTR (pLspUpCtrlBlock)))) ==
                    LDP_MAX_ATM_VC_MRG_COUNT (u2IncarnId))
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT:Mrg Failed-Max VC MrgCnt "
                             "Reached-LdpDuUpIdIntDnMap\n");
                    LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_RSRC_AWT;
                    return;
                }
            }
        }
    }

    if ((pLspUpCtrlBlock->bIsConnectedRoute == LDP_TRUE) &&
        ((LDP_ENTITY_PHP_CONF (SSN_GET_ENTITY (pLdpSession))
          == MPLS_IMPLICIT_NULL_LABEL) ||
         (LDP_ENTITY_PHP_CONF (SSN_GET_ENTITY (pLdpSession))
          == MPLS_IPV4_EXPLICIT_NULL_LABEL)))
    {
        Label.u4GenLbl = LDP_ENTITY_PHP_CONF (SSN_GET_ENTITY (pLdpSession));
        LDP_DBG1 (LDP_ADVT_MISC,
                  "ADVT: PHP Configured with Label Value - %x \n",
                  Label.u4GenLbl);
        MEMCPY (&LCB_ULBL (pLspUpCtrlBlock), &Label, sizeof (uLabel));
#ifdef LDP_GR_WANTED
        if (pILMHwListEntry != NULL)
        {
            UPSTR_INCOM_IFINDX (pLspUpCtrlBlock) =
                MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry);
        }
#endif
        if (Label.u4GenLbl == MPLS_IPV4_EXPLICIT_NULL_LABEL)
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP,
                                   UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                   pLspUpCtrlBlock);
        }
        LdpMrgSendLblMappingMsg
            (pLspUpCtrlBlock, LDP_MSG_HC (pLdpMsgInfo),
             (LDP_MSG_PV_PTR (pLdpMsgInfo) + LDP_TLV_HDR_LEN),
             (UINT1) LDP_MSG_PV_COUNT (pLdpMsgInfo),
             &pLdpMsgInfo->au1UnknownTlv[0], pLdpMsgInfo->u1UnknownTlvLen);
        LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_EST;
        /* If PHP is configured there is no need to do an FM MLIB 
         *  update at the egress so function returns after sending 
         *  a label mapping message */
        return;
    }

#ifdef LDP_GR_WANTED
    if ((pILMHwListEntry != NULL)
        && (MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry) == MPLS_TRUE))
    {
        if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
            (LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock)) != NULL))
        {

            ILMHwListNextHop.i4IpAddrType =
                UPSTR_FEC (pLspUpCtrlBlock).u2AddrFmly;

#ifdef MPLS_IPV6_WANTED
            if (ILMHwListNextHop.i4IpAddrType == MPLS_LSR_ADDR_IPV6)
            {
                MEMCPY (&ILMHwListNextHop.IpAddress.Ip6Addr,
                        &(LDP_IPV6_U4_ADDR
                          (UPSTR_DSTR_PTR (pLspUpCtrlBlock)->NextHopAddr)),
                        IPV6_ADDR_LENGTH);
            }
            else
#endif
            {
                MEMCPY (ILMHwListNextHop.IpAddress.au1Ipv4Addr,
                        &(LDP_IPV4_U4_ADDR
                          (UPSTR_DSTR_PTR (pLspUpCtrlBlock)->NextHopAddr)),
                        LDP_IPV4ADR_LEN);
            }

            MEMCPY (&ILMHwListOutLabel,
                    &LCB_DLBL (UPSTR_DSTR_PTR (pLspUpCtrlBlock)),
                    sizeof (uLabel));

            /* For Implcit-NULL case, the Out Tunnel If Index would be 0 */
            if (ILMHwListOutLabel.u4GenLbl != MPLS_IMPLICIT_NULL_LABEL)
            {
                u4OutTnlIfIndex =
                    UPSTR_DSTR_PTR (pLspUpCtrlBlock)->u4OutIfIndex;
            }
        }

        /* For egress case, Outlabel, NextHop and TnlIf Index will be 0
         * Hence this validation should pass for the egress as well */
        if (MplsHwListValidateILMHwListEntry (pILMHwListEntry,
                                              ILMHwListOutLabel,
                                              ILMHwListNextHop,
                                              u4OutTnlIfIndex) == MPLS_TRUE)
        {
#ifdef MPLS_IPV6_WANTED
            if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
                MPLS_IPV6_ADDR_TYPE)
            {
                LDP_DBG2 (GRACEFUL_DEBUG,
                          "LdpDuUpIdIntDnMap: ILM Hw List Entry Validation Successful "
                          "for FEC: %s and interface: %d\n",
                          Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                        (pILMHwListEntry)),
                          MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
            }
            else
#endif
            {

                LDP_DBG5 (GRACEFUL_DEBUG,
                          "LdpDuUpIdIntDnMap: ILM Hw List Entry Validation Successful "
                          "for FEC: %d.%d.%d.%d and interface: %d\n",
                          MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
                          MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
                          MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
                          MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3],
                          MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));

            }

            u1IsLabelAllocReq = MPLS_FALSE;
            /* RE-use the Label and In Index from the ILM h/w list Entry */
            MEMCPY (&Label, &pILMHwListEntry->InLabel, sizeof (uLabel));
            UPSTR_INCOM_IFINDX (pLspUpCtrlBlock) =
                MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry);

            LDP_DBG2 (GRACEFUL_DEBUG,
                      "LdpDuUpIdIntDnMap: Re-using the In Label: %d, "
                      "and InInterface: %d\n",
                      MPLS_ILM_HW_LIST_IN_LABEL (pILMHwListEntry),
                      MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
        }
        else
        {
            /* The entry will be deleted from the Hw in the MplsILMHwAdd, since the
             * validation will fail over there also */
            LDP_DBG5 (GRACEFUL_DEBUG,
                      "LdpDuUpIdIntDnMap: Validation Failed for the ILM Hw List Entry "
                      "For FEC: %d.%d.%d.%d, interface: %d\n ",
                      MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
                      MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
                      MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
                      MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3],
                      MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
        }
    }
#endif

#ifdef LDP_GR_WANTED
    if (u1IsLabelAllocReq == MPLS_TRUE)
    {
#endif
        if (LdpGetLabel (SSN_GET_ENTITY (pLdpSession), &Label, u4SessIfIndex)
            != LDP_SUCCESS)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Alloc Label Failed - LdpDuUpIdIntDnMap\n");
            LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_RSRC_AWT;
            return;
        }
#ifdef LDP_GR_WANTED
    }
#endif

    MEMCPY (&LCB_ULBL (pLspUpCtrlBlock), &Label, sizeof (uLabel));

    /* Intermediate Node */
    if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
        ((LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) != NULL))
    {
        /* Allow programming the ILM operation only 
         * when the downstream MLIB operation has been completed */
        if (LCB_MLIB_UPD_STATUS (UPSTR_DSTR_PTR (pLspUpCtrlBlock))
            != LDP_DU_DN_MLIB_UPD_NOT_DONE)
        {
            u2LabelOperation = MPLS_OPR_POP_PUSH;
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, u2LabelOperation,
                                   UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                   pLspUpCtrlBlock);
        }
    }
    else                        /* Egress Node */
    {
        u2LabelOperation = MPLS_OPR_POP;

        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, u2LabelOperation,
                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                               pLspUpCtrlBlock);
    }
    if (LDP_MSG_PV_PTR (pLdpMsgInfo) == NULL)
    {
        pu1PvPtr = NULL;
    }
    else
    {
        pu1PvPtr = LDP_MSG_PV_PTR (pLdpMsgInfo) + LDP_TLV_HDR_LEN;
    }

    LdpMrgSendLblMappingMsg (pLspUpCtrlBlock,
                             LDP_MSG_HC (pLdpMsgInfo), pu1PvPtr,
                             LDP_MSG_PV_COUNT (pLdpMsgInfo),
                             &pLdpMsgInfo->au1UnknownTlv[0],
                             pLdpMsgInfo->u1UnknownTlvLen);
    LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_EST;
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpIdDelFec                                           */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and Delete FEC Event received.                 */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpIdDelFec (tUstrLspCtrlBlock * pLspUpCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
    LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpIdUpLost                                           */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and UpStream Lost Event received.              */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpIdUpLost (tUstrLspCtrlBlock * pLspUpCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
    LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpIdIntWR                                            */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and Internal Withdraw Event received.          */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpIdIntWR (tUstrLspCtrlBlock * pLspUpCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{

    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
    LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : ldpDuUpEstIntMap                                          */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and a Internal Downstream Map Event     */
/*                  received.                                                */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpEstIntDnMap (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                    tLdpMsgInfo * pLdpMsgInfo)
{
    UINT2               u2MlibOperation;

    if (UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL)
    {
        LDP_DBG (LDP_ADVT_MISC, "LdpDuUpEstIntDnMap: Updating Swap Entry\n");

        u2MlibOperation = MPLS_OPR_POP_PUSH;

        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, u2MlibOperation,
                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                               pLspUpCtrlBlock);

        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, u2MlibOperation,
                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                               pLspUpCtrlBlock);
    }
    else
    {
        /*In case of POP entry, no hardware operation required
         * as no parameter gets updated in upstream control block*/
        LDP_DBG (LDP_ADVT_MISC,
                 "LdpDuUpEstIntDnMap: Pop to Pop No Hardware action required\n");
    }

#ifdef LDP_GR_WANTED
    /* Do not send the Label Map msg if the Upstream session is Stale */
    if (UPSTR_USSN (pLspUpCtrlBlock)->u1StaleStatus == LDP_TRUE)
    {
        return;
    }
    else
    {
        LdpMrgSendLblMappingMsg (pLspUpCtrlBlock, LDP_MSG_HC (pLdpMsgInfo),
                                 (LDP_MSG_PV_PTR (pLdpMsgInfo) +
                                  LDP_TLV_HDR_LEN),
                                 (UINT1) LDP_MSG_PV_COUNT (pLdpMsgInfo),
                                 &pLdpMsgInfo->au1UnknownTlv[0],
                                 pLdpMsgInfo->u1UnknownTlvLen);

        LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_EST;
    }
#else
    LdpMrgSendLblMappingMsg (pLspUpCtrlBlock, LDP_MSG_HC (pLdpMsgInfo),
                             (LDP_MSG_PV_PTR (pLdpMsgInfo) +
                              LDP_TLV_HDR_LEN),
                             (UINT1) LDP_MSG_PV_COUNT (pLdpMsgInfo),
                             &pLdpMsgInfo->au1UnknownTlv[0],
                             pLdpMsgInfo->u1UnknownTlvLen);

    LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_EST;
    return;
#endif
}

/*****************************************************************************/
/* Function Name : LdpDuUpEstLdpRel                                          */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and LDP Release Event received.         */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpEstLdpRel (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                  tLdpMsgInfo * pLdpMsgInfo)
{
    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
    if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
        ((LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) != NULL))
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                               MPLS_OPR_POP_PUSH,
                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                               pLspUpCtrlBlock);

        /* Handle Here to send Label Release message to be sent to downstream,
         * if Label Release message is received from Upstream
         */
        if (gi4MplsSimulateFailure == LDP_SIM_FAILURE_PROPAGATE_LBL_RELEASE)
        {
            LdpSendLblRelMsg (&LCB_FEC (UPSTR_DSTR_PTR (pLspUpCtrlBlock)),
                              &LCB_DLBL (UPSTR_DSTR_PTR (pLspUpCtrlBlock)),
                              LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock)));
        }
    }
    else
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                               MPLS_OPR_POP,
                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                               pLspUpCtrlBlock);
    }

    LdpFreeLabel (SSN_GET_ENTITY (UPSTR_USSN (pLspUpCtrlBlock)),
                  &LCB_ULBL (pLspUpCtrlBlock),
                  LdpSessionGetIfIndex (UPSTR_USSN (pLspUpCtrlBlock),
                                        LCB_FEC (pLspUpCtrlBlock).u2AddrFmly));

    LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);

    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpEstIntWR                                           */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and a Internal WithDraw Event received. */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpEstIntWR (tUstrLspCtrlBlock * pLspUpCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{

#ifdef LDP_GR_WANTED
    UINT1               u1IsWdrawBlocked = LDP_FALSE;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    tLdpSession        *pUstrLdpSession = NULL;
    tLdpSession        *pDnstrLdpSession = NULL;
#endif

    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
    if (UPSTR_USSN (pLspUpCtrlBlock) != NULL)
    {

#ifdef LDP_GR_WANTED
        if ((gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus ==
             LDP_GR_SHUT_DOWN_IN_PROGRESS) &&
            (gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability ==
             LDP_GR_CAPABILITY_FULL))
        {

            pUstrLdpSession = UPSTR_USSN (pLspUpCtrlBlock);

            if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
                (LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock)) != NULL))
            {
                pDnstrLdpSession = LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock));

                if (pDnstrLdpSession->pLdpPeer->u1GrProgressState !=
                    LDP_PEER_GR_NOT_SUPPORTED
                    && pUstrLdpSession->pLdpPeer->u1GrProgressState !=
                    LDP_PEER_GR_NOT_SUPPORTED)
                {
                    u1IsWdrawBlocked = LDP_TRUE;
                }
            }
            else                /* This is the case of Egress */
            {
                if (pUstrLdpSession->pLdpPeer->u1GrProgressState !=
                    LDP_PEER_GR_NOT_SUPPORTED)
                {
                    u1IsWdrawBlocked = LDP_TRUE;
                }
            }
        }
        if (u1IsWdrawBlocked == LDP_FALSE)
        {
            LdpSendLblWithdrawMsg (&LCB_FEC (pLspUpCtrlBlock),
                                   &LCB_ULBL (pLspUpCtrlBlock),
                                   UPSTR_USSN (pLspUpCtrlBlock));
        }
        else
        {
            LDP_DBG1 (GRACEFUL_DEBUG,
                      "LdpDuUpEstIntWR: Label Wdraw Msg not sent for the FEC: %x,"
                      "Since the module shutdown is in progress and GR is Enabled\n",
                      LDP_IPV4_U4_ADDR (LCB_FEC (pLspUpCtrlBlock).Prefix));
        }
#else
        /* For all LSR's other than ingress */
        LdpSendLblWithdrawMsg (&LCB_FEC (pLspUpCtrlBlock),
                               &LCB_ULBL (pLspUpCtrlBlock),
                               UPSTR_USSN (pLspUpCtrlBlock));
#endif
        if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
            ((LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) != NULL))
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                   MPLS_OPR_POP_PUSH,
                                   UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                   pLspUpCtrlBlock);
        }
        else
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                   MPLS_OPR_POP,
                                   UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                   pLspUpCtrlBlock);
        }

        if (UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL)
        {

            TMO_SLL_Delete (&LCB_UPSTR_LIST (UPSTR_DSTR_PTR (pLspUpCtrlBlock)),
                            UPCB_TO_SLL (pLspUpCtrlBlock));
        }

        UPSTR_DSTR_PTR (pLspUpCtrlBlock) = NULL;

        LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_REL_AWT;
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpEstDelFec                                          */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and Delete FEC Event received.          */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpEstDelFec (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                  tLdpMsgInfo * pLdpMsgInfo)
{
    tLdpSession        *pUpstrSession = NULL;

    LDP_SUPPRESS_WARNING (pLdpMsgInfo);

    pUpstrSession = pLspUpCtrlBlock->pUpstrSession;

    if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
        ((LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) != NULL))
    {
        /* Not the Egress node */
        /*Release message  to Downstream has to be handled */

        /* As per RFC 5036 - section A.1.7, Label withdraw message has to be sent
         * for both conservative and liberal modes
         * */

        LdpSendLblWithdrawMsg (&LCB_FEC (pLspUpCtrlBlock),
                               &LCB_ULBL (pLspUpCtrlBlock), pUpstrSession);

        LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_REL_AWT;

        if (LCB_MLIB_UPD_STATUS (UPSTR_DSTR_PTR (pLspUpCtrlBlock))
            != LDP_DU_DN_MLIB_UPD_NOT_DONE)
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                   MPLS_OPR_POP_PUSH,
                                   UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                   pLspUpCtrlBlock);
        }

        TMO_SLL_Delete (&LCB_UPSTR_LIST (UPSTR_DSTR_PTR (pLspUpCtrlBlock)),
                        UPCB_TO_SLL (pLspUpCtrlBlock));
        UPSTR_DSTR_PTR (pLspUpCtrlBlock) = NULL;
    }
    else
    {
        /* Egress node */
        LdpSendLblWithdrawMsg (&LCB_FEC (pLspUpCtrlBlock),
                               &LCB_ULBL (pLspUpCtrlBlock), pUpstrSession);

        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                               MPLS_OPR_POP,
                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                               pLspUpCtrlBlock);

        LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_REL_AWT;
    }
    return;
}

/*****************************************************************************/
/* Function Name : ldpDuUpEstUpLostUp                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and UpStream Lost Event received.       */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpEstUpLost (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                  tLdpMsgInfo * pLdpMsgInfo)
{

    LDP_SUPPRESS_WARNING (pLdpMsgInfo);

    LdpFreeLabel (SSN_GET_ENTITY (UPSTR_USSN (pLspUpCtrlBlock)),
                  &LCB_ULBL (pLspUpCtrlBlock),
                  LdpSessionGetIfIndex (UPSTR_USSN (pLspUpCtrlBlock),
                                        LCB_FEC (pLspUpCtrlBlock).u2AddrFmly));

    if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
        ((LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) != NULL))
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                               MPLS_OPR_POP_PUSH,
                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                               pLspUpCtrlBlock);
    }
    else
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                               MPLS_OPR_POP,
                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                               pLspUpCtrlBlock);
    }

    LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);

    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpRelLdpRel                                          */
/* Description   : This routine is called when label state machine is in     */
/*                 RELEASE_AWAITED state and LDP Release Event received.     */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpRelLdpRel (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                  tLdpMsgInfo * pLdpMsgInfo)
{
    LDP_SUPPRESS_WARNING (pLdpMsgInfo);

    LdpFreeLabel (SSN_GET_ENTITY (UPSTR_USSN (pLspUpCtrlBlock)),
                  &LCB_ULBL (pLspUpCtrlBlock),
                  LdpSessionGetIfIndex (UPSTR_USSN (pLspUpCtrlBlock),
                                        LCB_FEC (pLspUpCtrlBlock).u2AddrFmly));

    LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpRelUpLost                                          */
/* Description   : This routine is called when label state machine is in     */
/*                 RELEASE_AWAITED state and UpStream Lost Event received.   */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpRelUpLost (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                  tLdpMsgInfo * pLdpMsgInfo)
{

    LDP_SUPPRESS_WARNING (pLdpMsgInfo);

    LdpFreeLabel (SSN_GET_ENTITY (UPSTR_USSN (pLspUpCtrlBlock)),
                  &LCB_ULBL (pLspUpCtrlBlock),
                  LdpSessionGetIfIndex (UPSTR_USSN (pLspUpCtrlBlock),
                                        LCB_FEC (pLspUpCtrlBlock).u2AddrFmly));

    LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);

    return;

}

/*****************************************************************************/
/* Function Name : LdpDuUpRsrcIntWR                                          */
/* Description   : This routine is called when label state machine is in     */
/*                 RESOURCES_AWAITED state and Internal WithDraw Event       */
/*                 received.                                                 */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpRsrcIntWR (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                  tLdpMsgInfo * pLdpMsgInfo)
{
    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
    LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpRsrcRsrcAvl                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 RESOURCES_AWAITED state and Resources Available Event     */
/*                 received.                                                 */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpRsrcRsrcAvl (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                    tLdpMsgInfo * pLdpMsgInfo)
{

    tLdpSession        *pLdpSession = UPSTR_USSN (pLspUpCtrlBlock);
    UINT4               u4SessIfIndex = LdpSessionGetIfIndex (pLdpSession,
                                                              LCB_FEC
                                                              (pLspUpCtrlBlock).
                                                              u2AddrFmly);
    uLabel              Label;
    UINT2               u2LabelOperation;
    LDP_SUPPRESS_WARNING (pLdpMsgInfo);

    if (LdpGetLabel (SSN_GET_ENTITY (pLdpSession), &Label, u4SessIfIndex)
        != LDP_SUCCESS)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT:Alloc Label Failed - LdpDuUpIdIntDnMap\n");
        LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_RSRC_AWT;
        return;
    }
    else
    {
        /* Label successfully allocated */
        MEMCPY (&LCB_ULBL (pLspUpCtrlBlock), &Label, sizeof (uLabel));
        if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
            ((LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) != NULL))
        {
            u2LabelOperation = MPLS_OPR_POP_PUSH;
        }
        else
        {
            u2LabelOperation = MPLS_OPR_POP;
        }
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, u2LabelOperation,
                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                               pLspUpCtrlBlock);
        if (UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL)
        {
            LdpMrgSendLblMappingMsg
                (pLspUpCtrlBlock, LCB_HCOUNT (UPSTR_DSTR_PTR (pLspUpCtrlBlock)),
                 NULL, LDP_ZERO, NULL, LDP_ZERO);
        }
        else
        {
            LdpMrgSendLblMappingMsg (pLspUpCtrlBlock, LDP_ZERO, NULL, LDP_ZERO,
                                     NULL, LDP_ZERO);
        }

        /*
         * As per RFC 5036, notification message with label resource available
         * status case code has to be sent to upstream
         */
        LdpSendNotifMsg (UPSTR_USSN (pLspUpCtrlBlock), NULL,
                         LDP_FALSE, LDP_STAT_TYPE_LBL_RSRC_AVBL, NULL);

        LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_EST;
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpRsrcDelFec                                         */
/* Description   : This routine is called when label state machine is in     */
/*                 RESOURCES_AWAITED state and Delete FEC Event received.    */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpRsrcDelFec (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                   tLdpMsgInfo * pLdpMsgInfo)
{
    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
    LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuUpRsrcUpLost                                         */
/* Description   : This routine is called when label state machine is in     */
/*                 RESOURCES_AWAITED state and Upstream Lost Event received. */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock,                                          */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpRsrcUpLost (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                   tLdpMsgInfo * pLdpMsgInfo)
{
    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
    LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgSendLblMappingMsg                                   */
/* Description   : This routine constructs and sends Label Mapping Message.  */
/*                 This routine is invoked in case of Egress LSR, as a result*/
/*                 of the processing of Label Request Message received from  */
/*                 upstream.                                                 */
/*                 In the case of other LSRs, this routine is invoked as a   */
/*                 result of the processing of Label Mapping messages        */
/*                 received from downstream.                                 */
/*                                                                           */
/* Input(s)      : pLspUpCtrlBlock - Points to the curr. Up Lsp Ctrl Block   */
/*                 u1HcValue - Hop count Value in the Label Mapping message  */
/*                             received from downstream. In case of Egress   */
/*                             LSR, value is one.                            */
/*                 pu1PvList - Points to a list of Path Vectors received in  */
/*                             the Label Mapping message. Points to NULL in  */
/*                             the case of Egress LSR.                       */
/*                 u1PvCount - Number of Path Vectors, pu1PvList is pointing */
/*                             to. In case the loop detection is not enabled,*/
/*                             this should be zero.                          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS   if Label Mapping Message is sent            */
/*                 successfully else LDP_FAILURE                             */
/*****************************************************************************/
UINT1
LdpMrgSendLblMappingMsg (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                         UINT1 u1HcValue, UINT1 *pu1PvList, UINT1 u1PvCount,
                         UINT1 *pu1UnknownTlv, UINT1 u1UnknownTlvLen)
{
    UINT1               u1HcPrsnt = LDP_FALSE;
    UINT1               u1LsrEgress = LDP_FALSE;
    UINT2               u2OptParamLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT4               u4Offset = 0;
    UINT1               u1PadPrefixLen =
        LDP_GET_PADDED_PRFX_LEN (pLspUpCtrlBlock->Fec.u1PreLen);

    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;

    tFec                FecElmnt;
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tTlvHdr             PVTlvHdr = { OSIX_HTONS (LDP_PATH_VECTOR_TLV), 0 };
    tHopCountTlv        HopCountTlv = { {OSIX_HTONS (LDP_HOP_COUNT_TLV),
                                         OSIX_HTONS (LDP_HC_VALUE_LEN)},
    0, {0, 0, 0}
    };
    tLblReqMsgIdTlv     ReqMsgIdTlv = { {OSIX_HTONS (LDP_LBL_REQ_MSGID_TLV),
                                         OSIX_HTONS (LDP_MSG_ID_LEN)}, 0
    };
    tGenLblTlv          GenLblTlv;
    tAtmLblTlv          AtmLblTlv;
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_MAPPING_MSG), 0, 0 };
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = UPSTR_DSTR_PTR (pLspUpCtrlBlock);
    tLdpId              PeerLdpId;
    tLdpSession        *pLdpSession = UPSTR_USSN (pLspUpCtrlBlock);
    /*MPLS_IPv6 add start */
    tGenAddr            ConnId;
    tGenU4Addr          LdpIfAddrZero;

    /*MPLS_IPv6 add end */
    FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PadPrefixLen));

#ifdef LDP_GR_WANTED
    if (pLdpSession->u1StaleStatus == LDP_TRUE)
    {
        LDP_DBG (GRACEFUL_DEBUG,
                 "LdpMrgSendLblMappingMsg: LBL Mapping Msg not sent "
                 "Since the Session is STALE\n");
        return LDP_SUCCESS;
    }
#endif
    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);
    MEMSET (&ConnId, LDP_ZERO, sizeof (tGenAddr));
    MEMSET (&LdpIfAddrZero, LDP_ZERO, sizeof (tGenU4Addr));
    if ((pLspCtrlBlock == NULL) ||
        ((pLspCtrlBlock != NULL) && (pLspCtrlBlock->pDStrSession == NULL)))
    {
        u1LsrEgress = LDP_TRUE;
        /* TBD chk if the condn holds good for VC mrg */
    }
    if (u1LsrEgress == LDP_TRUE)
    {

        /* LSR is Egress */
        /* As Per RFC 5036, A.2.8,
         * If the LSR is Egress and if 
         * 1. Hop Count is configured or
         * 2. Loop Detection is configured
         * include the Hop Count TLV with Hop Count as ONE.
         */

        if ((LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID (pLdpSession))
             == LDP_LOOP_DETECT_CAPABLE_HC)
            || (LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID (pLdpSession))
                == LDP_LOOP_DETECT_CAPABLE_HCPV)
            || (LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID (pLdpSession))
                == LDP_LOOP_DETECT_CAPABLE_PV))
        {
            HopCountTlv.u1HcValue = LDP_HC_VALUE_AT_EGRESS;
            u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN);
            u1HcPrsnt = LDP_TRUE;
        }
        else
        {
            HopCountTlv.u1HcValue = LDP_ZERO;
        }
    }
    else
    {
        /* Intermediate Lsr */

        /* As Per RFC 5036, A.2.8,
         * If an intermediate LSR receives Label Mapping message with
         * 1. Hop Count TLV or
         * 2. Hop Count in configured in the LSR or
         * 3. Loop Detection is configured in the LSR,
         * then
         * a. If the hop count TLV is there, increment the TLV
         * b. If the hop count TLV is not there, but Loop Detection (PV) is 
         *    configured or Hop Count is configured then,
         *    add Hop Count TLV with value ZERO
         */

        u1HcPrsnt = LDP_HC_TLV_PRSNT (u1HcValue);
        if ((LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID (pLdpSession))
             == LDP_LOOP_DETECT_CAPABLE_HC)
            || (LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID (pLdpSession))
                == LDP_LOOP_DETECT_CAPABLE_HCPV)
            || (LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID (pLdpSession))
                == LDP_LOOP_DETECT_CAPABLE_PV) || (u1HcPrsnt == LDP_TRUE))
        {
            if (u1HcPrsnt != LDP_TRUE)
            {
                HopCountTlv.u1HcValue = LDP_ZERO;
            }
            else
            {
                HopCountTlv.u1HcValue = LDP_INCREMENT_HC_VALUE (u1HcValue);
            }
            u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN);
            u1HcPrsnt = LDP_TRUE;
        }
        /* As Per RFC 5036, A.2.8, If an intermediate LSR receives
         * Label Map message without HOP count TLV, It should not send
         * Label map message with PATH VECTOR TLV
         * */

        if ((SSN_GET_LOOP_DETECT (pLspUpCtrlBlock->pUpstrSession) == LDP_TRUE)
            && (u1HcValue != LDP_INVALID_HOP_COUNT))
        {
            PVTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_LSR_ID_LEN * (u1PvCount + 1)));
            u2OptParamLen +=
                (LDP_TLV_HDR_LEN + (LDP_LSR_ID_LEN * (u1PvCount + 1)));
        }
    }

    if (UPSTR_UREQ_ID (pLspUpCtrlBlock) != LDP_INVALID_REQ_ID)
    {
        /* for Label Request Message Id Tlv */
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN);
    }

    u2MandParamLen +=
        (LDP_MSG_ID_LEN + 2 * LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
         u1PadPrefixLen + LDP_LABEL_LEN);

    if (u1UnknownTlvLen != LDP_ZERO)
    {
        u2OptParamLen += u1UnknownTlvLen;
    }

    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2OptParamLen + u2MandParamLen),
                                      LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buffer for Lbl Mapping Msg \n");
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2OptParamLen + u2MandParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLspUpCtrlBlock->pUpstrSession));
    MsgHdrAndId.u4MsgId =
        OSIX_HTONL (SSN_GET_MSGID (pLspUpCtrlBlock->pUpstrSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr and MsgId to buffer chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct Fec Element */
    FecElmnt.u1FecElmntType = pLspUpCtrlBlock->Fec.u1FecElmntType;
    FecElmnt.u2AddrFmly = OSIX_HTONS (pLspUpCtrlBlock->Fec.u2AddrFmly);
    if (pLspUpCtrlBlock->Fec.u1FecElmntType == LDP_FEC_HOSTADDR_TYPE)
    {
        /* When the Fec Element Type is HostAddress, the Address Length
         * should be in Octect Length. 
         * u1PrefLen has the length in bits.
         * It is divided by 8 (right shift 3 times), to get the length
         * in Octect Length.*/
        FecElmnt.u1PreLen = (UINT1) ((pLspUpCtrlBlock->Fec.u1PreLen) >> 3);
    }
    else
    {
        FecElmnt.u1PreLen = pLspUpCtrlBlock->Fec.u1PreLen;
    }

    LdpCopyAddr (&FecElmnt.Prefix, &pLspUpCtrlBlock->Fec.Prefix,
                 pLspUpCtrlBlock->Fec.u2AddrFmly);

    if (pLspUpCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV4)
    {
        LDP_IPV4_U4_ADDR (FecElmnt.Prefix) =
            OSIX_HTONL (LDP_IPV4_U4_ADDR (FecElmnt.Prefix));
    }

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying MsgHdr to buffer chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to buffer chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecElmnt.u2AddrFmly,
                                   u4Offset, LDP_UINT2_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement-PrefixLen to buffer chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    u4Offset += LDP_UINT2_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1PreLen, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to buffer chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    u4Offset += LDP_UINT1_LEN;

#ifdef MPLS_IPV6_WANTED
    if (pLspUpCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.Prefix.Ip6Addr, u4Offset,
             u1PadPrefixLen) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying FecElement-AddrPrefix Type to buffer chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }
    else
#endif
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.Prefix.u4Addr, u4Offset,
             LDP_IPV4ADR_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying FecElement-AddrPrefix Type to buffer chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }

    u4Offset += u1PadPrefixLen;

    if (pLspUpCtrlBlock->u1InLblType == LDP_ATM_LABEL)
    {
        AtmLblTlv.AtmLblTlvHdr.u2TlvType = OSIX_HTONS (LDP_ATM_LABEL_TLV);
        AtmLblTlv.AtmLblTlvHdr.u2TlvLen = OSIX_HTONS (LDP_LABEL_LEN);
        if (SSN_MRGTYPE (UPSTR_USSN (pLspUpCtrlBlock)) == LDP_VP_MRG)
        {
            AtmLblTlv.u2Vpi = OSIX_HTONS
                ((pLspUpCtrlBlock->UStrLabel.AtmLbl.u2Vpi | LDP_ATM_VP_BITS));
        }
        else
        {
            AtmLblTlv.u2Vpi = OSIX_HTONS
                ((pLspUpCtrlBlock->UStrLabel.AtmLbl.u2Vpi | LDP_ATM_V_BITS));
        }
        AtmLblTlv.u2Vci = OSIX_HTONS (pLspUpCtrlBlock->UStrLabel.AtmLbl.u2Vci);

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AtmLblTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Label Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }
    else if (pLspUpCtrlBlock->u1InLblType == LDP_GEN_LABEL)
    {
        /* Platform based */

        GenLblTlv.GenLblTlvHdr.u2TlvType = OSIX_HTONS (LDP_GEN_LABEL_TLV);
        GenLblTlv.GenLblTlvHdr.u2TlvLen = OSIX_HTONS (LDP_LABEL_LEN);
        GenLblTlv.u4GenLbl = OSIX_HTONL (pLspUpCtrlBlock->UStrLabel.u4GenLbl);

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &GenLblTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Label Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_LABEL_LEN);

    if (UPSTR_UREQ_ID (pLspUpCtrlBlock) != LDP_INVALID_REQ_ID)
    {
        /* Construct Label Request Msg Id Tlv */
        ReqMsgIdTlv.u4LblReqMsgId =
            OSIX_HTONL (UPSTR_UREQ_ID (pLspUpCtrlBlock));

        if (!((gi4MplsSimulateFailure == LDP_SIM_FAILURE_HOLD_TRANSACTION) ||
              (gi4MplsSimulateFailure ==
               LDP_SIM_FAILURE_PROPAGATE_LBL_RELEASE)))

        {
            UPSTR_UREQ_ID (pLspUpCtrlBlock) = LDP_INVALID_REQ_ID;
        }

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &ReqMsgIdTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying MsgHdr and MsgId to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN);
    }

    if (u1HcPrsnt == LDP_TRUE)
    {
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &HopCountTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying HC Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN);
    }

    /* As Per RFC 5036, A.2.8, If an intermediate LSR receives
     * Label Map message without HOP count TLV, It should not send
     * Label map message with PATH VECTOR TLV
     * */

    /* As per RFC 5036, section 2.8.2, If LSR is egress, the label mapping message
     * need not include a path vector TLV
     * */

    if ((u1LsrEgress == LDP_FALSE) &&
        (SSN_GET_LOOP_DETECT (pLspUpCtrlBlock->pUpstrSession) == LDP_TRUE) &&
        (u1HcValue != LDP_INVALID_HOP_COUNT))
    {
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &PVTlvHdr, u4Offset,
                                       LDP_TLV_HDR_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying PVTlv Hdr to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_TLV_HDR_LEN;

        if ((u1LsrEgress == LDP_FALSE) && (u1PvCount != LDP_ZERO) &&
            (pu1PvList != NULL))
        {
            /* LSR is Not Egress */

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) pu1PvList, u4Offset,
                                           (u1PvCount * LDP_LSR_ID_LEN)) ==
                FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying PV List to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += (u1PvCount * LDP_LSR_ID_LEN);
        }
        /* Copy Local LSR ID */
        if (CRU_BUF_Copy_OverBufChain (pMsg,
                                       (UINT1 *)
                                       &SSN_GET_LSR_ID (pLspUpCtrlBlock->
                                                        pUpstrSession),
                                       u4Offset, LDP_LSR_ID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Local LsrId to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_LSR_ID_LEN;
    }

    if (u1UnknownTlvLen != LDP_ZERO)
    {
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) pu1UnknownTlv, u4Offset,
                                       u1UnknownTlvLen) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += u1UnknownTlvLen;
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLspUpCtrlBlock->pUpstrSession);

#ifdef MPLS_IPV6_WANTED
    if (pLspUpCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        LDP_DBG2 (LDP_ADVT_TX,
                  "Label Mapping for the prefix %s/%d \n",
                  Ip6PrintAddr (&FecElmnt.Prefix.Ip6Addr), FecElmnt.u1PreLen);
    }
    else
#endif
    {
        u4Temp = OSIX_HTONL (LDP_IPV4_U4_ADDR (FecElmnt.Prefix));

        MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);

        LDP_DBG5 (LDP_ADVT_TX,
                  "Label Mapping Msg for the prefix %d.%d.%d.%d/%d \n",
                  au1IpAddr[3], au1IpAddr[2], au1IpAddr[1], au1IpAddr[0],
                  FecElmnt.u1PreLen);

    }

    LDP_DBG4 (LDP_ADVT_TX,
              "ADVT: Sending Label Mapping Msg To %d.%d.%d.%d \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);

    /*MPLS_IPv6 add start */
    MEMCPY (LDP_IPV4_ADDR (ConnId.Addr),
            &(pLspUpCtrlBlock->pUpstrSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg
        (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
         (pLspUpCtrlBlock->pUpstrSession->pLdpPeer->pLdpEntity), LDP_FALSE,
         ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Label Mapping Msg\n");
        return LDP_FAILURE;
    }

    if (u1LsrEgress == LDP_TRUE)
    {
        /* LSR is Egress */

        (SSN_GET_LBL_TYPE (UPSTR_USSN (pLspUpCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspUpCtrlBlock->u1InLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspUpCtrlBlock->u1InLblType) = LDP_IFACE_ETHERNET_TYPE);
    }
    else
    {
        /* LSR is Intermediate */

        if (UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL)
        {
            (SSN_GET_LBL_TYPE (LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock)))
             == LDP_ATM_MODE) ?
(((UPSTR_DSTR_PTR (pLspUpCtrlBlock))->u1OutLblType) =
LDP_IFACE_ATM_TYPE) : (((UPSTR_DSTR_PTR (pLspUpCtrlBlock))->u1OutLblType) = LDP_IFACE_ETHERNET_TYPE);
        }

        (SSN_GET_LBL_TYPE (UPSTR_USSN (pLspUpCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspUpCtrlBlock->u1InLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspUpCtrlBlock->u1InLblType) = LDP_IFACE_ETHERNET_TYPE);

        if (pLspCtrlBlock->u4CrossConnectIndex != 0)
        {
            LdpGetCrossConnectIndex ((UINT4 *)
                                     &pLspUpCtrlBlock->u4CrossConnectIndex);
        }
    }

    LDP_DBG1 (LDP_ADVT_TX, "ADVT: Label Mapping Msg sent with label %d\n",
              pLspUpCtrlBlock->UStrLabel.u4GenLbl);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDuDeleteUpLspCtrlBlock                                 */
/* Description   : This routine deletes UP LSP control block. Up             */
/*                 LSP control block list in session entry is also modified. */
/*                 Up Lsp control list in downstream control block is alos   */
/*                 modified                                                  */
/* Input(s)      : pLspUpCtrlBlock - pointer to up lsp control block         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpDuDeleteUpLspCtrlBlock (tUstrLspCtrlBlock * pLspUpCtrlBlock)
{
    tTMO_SLL           *pLcbList = NULL;
    tLdpSession        *pSessionEntry = UPSTR_USSN (pLspUpCtrlBlock);
    tUstrLspCtrlBlock  *pTempLspUpCtrlBlock = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tLdpMsgInfo         LdpMsgInfo;

    MEMSET (&LdpMsgInfo, 0, sizeof (tLdpMsgInfo));

    /* Delete this LSP control blcok from upstream session's upstream LSP
     * Control block list */
    pLcbList = &SSN_ULCB (UPSTR_USSN (pLspUpCtrlBlock));
    TMO_SLL_Delete (pLcbList, &(pLspUpCtrlBlock->NextUpstrCtrlBlk));

    /* 
     * Delete this LSP control blcok from downstream LSP
     * Control block's upstream control block list */
    if (UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL)
    {
        TMO_SLL_Delete (&LCB_UPSTR_LIST (UPSTR_DSTR_PTR (pLspUpCtrlBlock)),
                        UPCB_TO_SLL (pLspUpCtrlBlock));
        UPSTR_DSTR_PTR (pLspUpCtrlBlock) = NULL;
    }
    TMO_DYN_SLL_Scan (&SSN_ULCB (pSessionEntry), pLdpSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        pTempLspUpCtrlBlock = (tUstrLspCtrlBlock *) pLdpSllNode;
        if (UPSTR_STATE (pTempLspUpCtrlBlock) == LDP_DU_UP_LSM_ST_RSRC_AWT)
        {
            LdpDuUpSm (pTempLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_RSRC_AVL, NULL);
            break;
        }
    }

    LDP_DBG1 (LDP_ADVT_PRCS,
              "Upstream if index %d\n", pLspUpCtrlBlock->u4InComIfIndex);
    if (pLspUpCtrlBlock->u4InComIfIndex != 0)
    {
        pLspUpCtrlBlock->u4InComIfIndex = 0;
    }
    MemReleaseMemBlock (LDP_UPSTR_POOL_ID, (UINT1 *) pLspUpCtrlBlock);
}

/*****************************************************************************/
/* Function Name : LdpDuUpInvStEvt                                           */
/* Description   : This routine is called when DU UP state machine is in     */
/*                 ESTABLISHED/IDLE state and an invalid event is received   */
/* Input(s)      : pLspUpCtrlBlock - Points to up stream ctrl blk            */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpDuUpInvStEvt (tUstrLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    LDP_SUPPRESS_WARNING (pLspCtrlBlock);
    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
}

#ifdef LDP_GR_WANTED
/*****************************************************************************/
/* Function Name : LdpDuUpEstIntRefreshEvt                                   */
/* Description   : This routine is called when DU UP state machine is in     */
/*                 ESTABLISHED state and an internal refresh event is        */
/*                 received                                                */
/* Input(s)      : pLspUpCtrlBlock - Points to up stream ctrl blk            */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuUpEstIntRefreshEvt (tUstrLspCtrlBlock * pLspUpCtrlBlock,
                         tLdpMsgInfo * pLdpMsgInfo)
{

    /* Send the Label Mapping Msg to the Upstream router */
    LDP_DBG2 (GRACEFUL_DEBUG,
              "LdpDuUpEstIntRefreshEvt: Sending the LBL MAP MSG "
              "for the FEC:%x/%d\n",
              LDP_IPV4_U4_ADDR (UPSTR_FEC (pLspUpCtrlBlock).Prefix),
              UPSTR_FEC (pLspUpCtrlBlock).u1PreLen);

    LdpMrgSendLblMappingMsg (pLspUpCtrlBlock, LDP_MSG_HC (pLdpMsgInfo),
                             (LDP_MSG_PV_PTR (pLdpMsgInfo) +
                              LDP_TLV_HDR_LEN),
                             (UINT1) LDP_MSG_PV_COUNT (pLdpMsgInfo),
                             &pLdpMsgInfo->au1UnknownTlv[0],
                             pLdpMsgInfo->u1UnknownTlvLen);

    LDP_DBG (GRACEFUL_DEBUG,
             "LdpDuUpEstIntRefreshEvt: Marking the Upstream control block as UNSTALE\n");
    /* Mark the Control block as Unstale */
    pLspUpCtrlBlock->u1StaleStatus = LDP_FALSE;

}

#endif
