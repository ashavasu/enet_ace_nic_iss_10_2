/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpdsprc.c,v 1.12 2015/02/05 11:29:57 siva Exp $
 *
 ********************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpdsprc.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : CRLDP-DIFFSERV 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains functions that are
 *                             associeated with CRLDP-DIFFSERV.
 *----------------------------------------------------------------------------*/
#include "ldpincs.h"
/******************************************************************************
* Function Name : ldpDiffServProcHandleDsTlv                                 
* Description   : This routine is called when the mpls is diffServ 
*                 enabled and Label Request is received which may or 
*                 may not contain the DiffServ TLV.The routine processes 
*                 the DiffServ Tlv if present.It updates the DiffServ related
*                 information in DiffServParams.
*                  
*               : Reference - draft-ietf-mpls-diff-ext-09.txt                 
*               : Section - 6                                                 
* Input(s)      : pLspCtrlBlock - Pointer to the LspCtrlBlock                 
*               : pu1DiffServTlv - Pointer to the DiffServ Tlv present in 
*                                  the Msg. 
*               : pMsg - Pointer to the label request message received.       
* Output(s)     : None                                                     
* Return(s)     : LDP_SUCCESS or LDP_FAILURE                     
*******************************************************************************/
UINT1
LdpDiffServProcHandleDSTlv (tLspCtrlBlock * pLspCtrlBlock,
                            UINT1 *pu1DiffServTlv, UINT1 *pMsg,
                            UINT1 *pu1ClassTypeTlv)
{
    UINT1               u1ErrReceived = LDP_DS_FALSE;
    UINT1               u1Tbit;
    UINT1               u1ErrType;
    UINT1              *pu1DiffServTlvInfo = pu1DiffServTlv;
    UINT4               u4IncarnId;
    tCrlspTnlInfo      *pCrlspTnlInfo = LCB_TNLINFO (pLspCtrlBlock);
    tMplsDiffServTnlInfo DiffServTnlInfo;
    tMplsDiffServTnlInfo *pDiffServParams = &DiffServTnlInfo;
    tMplsDiffServTnlInfo *pTeDiffServTnlInfo = NULL;
    tTeTnlInfo         *pTeTnlInfo = CRLSP_TE_TNL_INFO (pCrlspTnlInfo);

    MEMSET (&DiffServTnlInfo, LDP_ZERO, sizeof (tMplsDiffServTnlInfo));
    LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo) = pDiffServParams;
    u4IncarnId = SSN_GET_INCRN_ID (LCB_USSN (pLspCtrlBlock));

    /* If DiffServ TLV is present in the label request message */
    if (pu1DiffServTlv != NULL)
    {
        /* Check the T-bit and call the appropriate function */
        pu1DiffServTlvInfo += LDP_TLV_HDR_LEN;
        LDP_EXT_1_BYTES (pu1DiffServTlvInfo, u1Tbit);

        /* Rightshift the bit by 7 */
        u1Tbit = (UINT1) (u1Tbit >> 7);

        /* If the diffServ TLV received is E-lsp tlv */
        if (u1Tbit == 0)
        {
            if (LdpDiffServUpdateElspInfo (pDiffServParams,
                                           pu1DiffServTlv,
                                           &u1ErrType, u4IncarnId,
                                           pLspCtrlBlock) == LDP_FAILURE)
            {

                LDP_DBG (LDP_DS_PRCS,
                         ":Error in Processing DIFFSERV-ELSP TLV\n ");
                u1ErrReceived = LDP_DS_TRUE;
            }
        }
        else if (u1Tbit == 1)
        {
            if (LdpDiffServUpdateLlspInfo (pDiffServParams,
                                           pu1DiffServTlv,
                                           &u1ErrType,
                                           pLspCtrlBlock) == LDP_FAILURE)
            {
                LDP_DBG (LDP_DS_PRCS,
                         ":Error in Processing DIFFSERV-LLSP TLV\n ");
                u1ErrReceived = LDP_DS_TRUE;
            }
        }

        else
        {
            return LDP_SUCCESS;
        }

        if (u1ErrReceived == LDP_DS_TRUE)
        {
            switch (u1ErrType)
            {
                case LDP_DS_INVALID_EXP_PHB_MAPPING:
                    LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock), NULL,
                                             NULL,
                                             LDP_DS_STAT_TYPE_INVALID_EXP_PHB_MAPPING,
                                             pLspCtrlBlock);

                    LDP_DBG (LDP_DS_PRCS,
                             ":Error- Invalid Exp to Phb mapping\n ");
                    return LDP_FAILURE;

                case LDP_DS_UNSUPPORTED_PHB:
                    LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock), NULL,
                                             NULL,
                                             LDP_DS_STAT_TYPE_UNSUPPORTED_PHB,
                                             pLspCtrlBlock);

                    LDP_DBG (LDP_DS_PRCS, ":Error- Unsupported PHB\n ");
                    return LDP_FAILURE;

                case LDP_DS_MEM_ALLOC_FAILURE:
                    LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                          pLspCtrlBlock,
                                          LDP_STAT_TYPE_NO_LBL_RSRC, pMsg);

                    LDP_DBG (LDP_DS_MEM,
                             ":Error- Memory allocation failure\n ");
                    return LDP_FAILURE;

                case LDP_DS_UNSUPPORTED_PSC:
                    LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock), NULL,
                                             NULL,
                                             LDP_DS_STAT_TYPE_UNSUPPORTED_PSC,
                                             pLspCtrlBlock);

                    LDP_DBG (LDP_DS_PRCS, ":Error- Unsupported PSC\n ");
                    return LDP_FAILURE;

                case LDP_DS_PER_LSP_CONTEXT_ALLOC_FAIL:
                    LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock), NULL,
                                             NULL,
                                             LDP_DS_STAT_TYPE_PER_LSPCONTEXT_ALLOCFAIL,
                                             pLspCtrlBlock);
                    LDP_DBG (LDP_DS_PRCS,
                             ":Error- Context allocation failure\n ");
                    return LDP_FAILURE;
            }
        }

        /* Set the flags to indicate that the TLV is present 
         * and the receive of the TLV is successful */
        /* Since some TLV is received, set the flags */
        CRLSP_PARM_FLAG (pCrlspTnlInfo) |= LDP_DIFFSERV_CRLSP;
    }
    else
    {
        /* Since no Tlv is present , the LSP should be preconfigurred E-LSP */
        LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServParams) = LDP_DS_ELSP;
        LDP_TE_DS_ELSP_TYPE (pDiffServParams) = LDP_DS_PRECONF_ELSP;
        LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServParams) = NULL;
    }
    /* Handle ClassType TLV Here */
    if (pu1ClassTypeTlv != NULL)
    {
        if (LdpDiffServHandleClassTypeTlv (pLspCtrlBlock, pu1ClassTypeTlv,
                                           pMsg) == LDP_FAILURE)
        {
            if (LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServParams) != NULL)
            {
                ldpTeDeleteDiffServElspList (LDP_TE_DS_PARAMS_ELSPLIST_PTR
                                             (pDiffServParams));
            }
            LDP_DBG (LDP_DS_PRCS,
                     ":Error in Processing DIFFSERV-CLASSTYPE TLV\n ");
            return LDP_FAILURE;
        }
    }
    LDP_TE_DS_PARAMS_ROWSTATUS (pDiffServParams) = ACTIVE;
    if (ldpTeCreateDiffServTnl (&pTeDiffServTnlInfo, pDiffServParams)
        != LDP_TE_SUCCESS)
    {
        if (LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServParams) != NULL)
        {
            ldpTeDeleteDiffServElspList (LDP_TE_DS_PARAMS_ELSPLIST_PTR
                                         (pDiffServParams));
        }
        LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                              LDP_STAT_TYPE_RESOURCE_UNAVAIL, pMsg);
        LDP_DBG (LDP_DS_MEM,
                 ":Error- ldpTeCreateDiffServTnl allocation failure\n ");
        return LDP_FAILURE;
    }
    LDP_TE_MPLS_DIFFSERV_TNL_INFO (pTeTnlInfo) = pTeDiffServTnlInfo;
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServUpdateElspInfo
* Description   : This routine is called to update the E-LSP
*                 related information in MplsDiffServParams.
*                                                             
*                 Reference - draft-ietf-mpls-diff-ext-09.txt
*                 Section - 6
* Input(s)      : pDiffServParams - Pointer to the DiffServParams in
*                                   TeTnlInfo where the E-LSPinformation
*                                   is to be updated
*               : pu1DiffServTlv - Pointer to the DiffServ Tlv present
*                                  in the Msg.
*               : pErrType - Pointer to the error received in processing 
*                            the diffServ TLV.It conyains the.
*                            error code value of the error.
* Output(s)     : None
* Return(s)     : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServUpdateElspInfo (tMplsDiffServTnlInfo * pDiffServTnlInfo,
                           UINT1 *pu1DiffServTlv, UINT1 *pu1ErrType,
                           UINT4 u4IncarnId, tLspCtrlBlock * pLspCtrlBlock)
{
    UINT1               u1PhbId14thBit;
    UINT1               u1Dscp;
    UINT1               u1SigMapArrayIndex;
    UINT1               u1Exp;
    UINT1               u1IsSupported = LDP_DS_FALSE;
    UINT1              *pu1DiffServInfo = pu1DiffServTlv;
    tDiffServElspTlvPrefix ElspTlvPrefix;
    tDiffServElspMapEntry ElspMapEntry;
    UINT1               u1Count;
    tCrlspTnlInfo      *pCrlspTnlInfo = LCB_TNLINFO (pLspCtrlBlock);
    UINT4               u4OutIfIndex =
        LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo)));
    UINT1               u1DuplicateError = LDP_FALSE;
    tMplsDiffServElspList *pMplsDiffServElspList = NULL;
    tTMO_SLL            ElspInfoList;

    tMplsDiffServElspInfo aMplsDiffServElspInfo[8];
    tMplsDiffServElspInfo *pTempElspInfo = NULL;

    MEMSET (aMplsDiffServElspInfo, LDP_ZERO,
            sizeof (tMplsDiffServElspInfo) * 8);
    TMO_SLL_Init (&ElspInfoList);

    LDP_TE_DS_PARAMS_ELSPLISTINDEX (pDiffServTnlInfo) = LDP_ZERO;

    LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServTnlInfo) = NULL;

    LDP_DS_SUPRESS_WARNING (u4IncarnId);
    pu1DiffServInfo += LDP_TLV_HDR_LEN;

    LDP_EXT_1_BYTES (pu1DiffServInfo,
                     LDP_DS_ELSP_TLV_PREFIX_TBIT (ElspTlvPrefix));

    LDP_EXT_1_BYTES (pu1DiffServInfo,
                     LDP_DS_ELSP_TLV_PREFIX_RESERVED (ElspTlvPrefix));

    LDP_EXT_1_BYTES (pu1DiffServInfo,
                     LDP_DS_ELSP_TLV_PREFIX_RESERVED1 (ElspTlvPrefix));

    LDP_EXT_1_BYTES (pu1DiffServInfo,
                     LDP_DS_ELSP_TLV_PREFIX_NO_OF_MAPENTRIES (ElspTlvPrefix));

    /* Check if the MAPnb feild in the DIffServ TLV is 
     * not within the range of 0 to 8 */
    if ((LDP_DS_ELSP_TLV_PREFIX_NO_OF_MAPENTRIES (ElspTlvPrefix) >
         MAX_DS_EXP)
        ||
        ((INT1) (LDP_DS_ELSP_TLV_PREFIX_NO_OF_MAPENTRIES (ElspTlvPrefix))
         < LDP_DS_ZERO))
    {
        /* Follwing error is sent according to the RFC */
        *pu1ErrType = LDP_DS_INVALID_EXP_PHB_MAPPING;
        LDP_DBG (LDP_DS_PRCS,
                 ":Error- ELSP TLV - Invalid Exp to Phb Mapping\n ");

        return LDP_FAILURE;
    }

    if (LDP_DS_ELSP_TLV_PREFIX_NO_OF_MAPENTRIES (ElspTlvPrefix) == LDP_DS_ZERO)
    {
        /* Mapping is preconfigured with diffServ TLV present 
         *according to the RFC */
        LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) = LDP_DS_ELSP;
        LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) = LDP_DS_PRECONF_ELSP;
        return LDP_SUCCESS;
    }

    for (u1SigMapArrayIndex = LDP_DS_ZERO;
         u1SigMapArrayIndex <
         LDP_DS_ELSP_TLV_PREFIX_NO_OF_MAPENTRIES (ElspTlvPrefix);
         u1SigMapArrayIndex++)
    {

        LDP_EXT_1_BYTES (pu1DiffServInfo,
                         LDP_DS_ELSP_TLV_MAPENTRY_RESERVED (ElspMapEntry));
        LDP_EXT_1_BYTES (pu1DiffServInfo,
                         LDP_DS_ELSP_TLV_MAPENTRY_EXP (ElspMapEntry));
        LDP_EXT_2_BYTES (pu1DiffServInfo,
                         LDP_DS_ELSP_TLV_MAPENTRY_PHBID (ElspMapEntry));

        u1Exp = LDP_DS_ELSP_TLV_MAPENTRY_EXP (ElspMapEntry);
        /* Check whether PhbId Valid, If 14 bit is 0 , it is valid */
        LDP_DS_GET_PHBID_14TH_BIT
            (LDP_DS_ELSP_TLV_MAPENTRY_PHBID (ElspMapEntry), u1PhbId14thBit);
        if (u1PhbId14thBit != 0)
        {
            *pu1ErrType = LDP_DS_INVALID_EXP_PHB_MAPPING;
            LDP_DBG (LDP_DS_PRCS,
                     ":Error- ELSP TLV - Invalid Exp to Phb Mapping\n ");

            return LDP_FAILURE;
        }

        LDP_DS_DSCP_FOR_PHBID (LDP_DS_ELSP_TLV_MAPENTRY_PHBID
                               (ElspMapEntry), u1Dscp);
        /*Check whether the DSCP is valid */
        if (LdpDiffServValidateDscp (u1Dscp) == LDP_FAILURE)
        {
            *pu1ErrType = LDP_DS_INVALID_EXP_PHB_MAPPING;
            return LDP_FAILURE;
        }

        /* Check has done to find whether PHB is supported by QoS or not */
        /* Check should not be done for the egress case */
        if (CRLSP_TNL_ROLE (pCrlspTnlInfo) != LDP_TE_EGRESS)
        {
            u1IsSupported = 0;
            LDP_DS_IS_PHB_SUPPORTED (u1Dscp, u1IsSupported, u1Count,
                                     u4OutIfIndex);
            if (u1IsSupported != LDP_DS_TRUE)
            {
                *pu1ErrType = LDP_DS_UNSUPPORTED_PHB;
                LDP_DBG (LDP_DS_PRCS, ":Error- ELSP TLV - Unsupported PHB\n ");
                return LDP_FAILURE;
            }
        }

        /* This check is to ignore duplicate values */

        TMO_SLL_Scan (&ElspInfoList, pTempElspInfo, tMplsDiffServElspInfo *)
        {
            if (LDP_TE_DS_ELSPINFO_INDEX (pTempElspInfo) == u1Exp + 1)
            {
                u1DuplicateError = LDP_TRUE;
                break;
            }
        }
        if (u1DuplicateError == LDP_TRUE)
        {
            continue;
        }
        TMO_SLL_Init_Node (&LDP_TE_DS_ELSPINFO_NEXT
                           (&(aMplsDiffServElspInfo[u1SigMapArrayIndex])));
        TMO_SLL_Add (&ElspInfoList,
                     &LDP_TE_DS_ELSPINFO_NEXT (&(aMplsDiffServElspInfo
                                                 [u1SigMapArrayIndex])));
        LDP_TE_DS_ELSPINFO_INDEX
            (&(aMplsDiffServElspInfo[u1SigMapArrayIndex])) =
            (UINT1) (u1Exp + 1);

        LDP_TE_DS_ELSPINFO_PHB_DSCP
            (&(aMplsDiffServElspInfo[u1SigMapArrayIndex])) = u1Dscp;

        LDP_TE_DS_ELSPINFO_ROW_STATUS
            (&aMplsDiffServElspInfo[u1SigMapArrayIndex]) = LDP_ACTIVE;

        LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS
            (&(aMplsDiffServElspInfo[u1SigMapArrayIndex])) = NULL;

    }

    /* Create the ElspList */
    if (ldpTeCreateDiffServElspList (&pMplsDiffServElspList, &ElspInfoList)
        == LDP_FAILURE)
    {
        *pu1ErrType = LDP_DS_MEM_ALLOC_FAILURE;
        LDP_DBG (LDP_DS_MEM,
                 ":Error- ELSP TLV - ldpTeCreateDiffServElspList "
                 "allocation failure\n ");

        return LDP_FAILURE;

    }

    LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) = LDP_DS_ELSP;
    LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) = LDP_DS_SIG_ELSP;
    LDP_TE_DS_PARAMS_ELSPLISTINDEX (pDiffServTnlInfo) =
        LDP_TE_DS_ELSPINFOLIST_INDEX (pMplsDiffServElspList);
    LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServTnlInfo) = pMplsDiffServElspList;
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServUpdateLlspInfo
* Description   : This routine is called to update the L-LSP related 
*                 information in MplsDiffServParams.
*
*                 Reference - draft-ietf-mpls-diff-ext-09.txt
*                 Section - 6
* Input(s)      : pDiffServParams - Pointer to the DiffServParams 
*                      in TeTnlInfo where the L-LSPinformation 
*                      is to be updated 
*               : pu1DiffServTlv - Pointer to the DiffServ 
*                      Tlv present in the Msg.
*               : pErrType - Pointer to the error received in processing 
*                      the diffServ TLV.It contains the error code 
*                      value of the error.
* Output(s)     : None
* Return(s)     : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServUpdateLlspInfo (tMplsDiffServTnlInfo * pDiffServTnlInfo,
                           UINT1 *pu1DiffServTlv, UINT1 *pu1ErrType,
                           tLspCtrlBlock * pLspCtrlBlock)
{
    UINT1               u1Dscp;
    UINT1               u1PhbId14thBit;
    UINT1               u1IsSupported = LDP_FALSE;
    UINT1               u1Count;
    UINT1              *pu1DiffServInfo = pu1DiffServTlv;
    tDiffServLlspTlv    LlspTlv;
    tCrlspTnlInfo      *pCrlspTnlInfo = LCB_TNLINFO (pLspCtrlBlock);
    UINT4               u4OutIfIndex =
        LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo)));

    /* offset done to get feild values */
    pu1DiffServInfo += LDP_TLV_HDR_LEN;

    LDP_EXT_1_BYTES (pu1DiffServInfo, LDP_DS_LLSP_TLV_TBIT (LlspTlv));
    LDP_EXT_1_BYTES (pu1DiffServInfo, LDP_DS_LLSP_TLV_RESERVED (LlspTlv));
    LDP_EXT_2_BYTES (pu1DiffServInfo, LDP_DS_LLSP_TLV_PSC (LlspTlv));

    /* Check whether PhbId Valid, If 14 bit is 1 , it is valid */
    LDP_DS_GET_PHBID_14TH_BIT (LDP_DS_LLSP_TLV_PSC (LlspTlv), u1PhbId14thBit);

    if (u1PhbId14thBit != LDP_DS_TRUE)

    {
        *pu1ErrType = LDP_DS_INVALID_EXP_PHB_MAPPING;
        LDP_DBG (LDP_DS_PRCS,
                 ":Error- LLSP TLV - Invalid Exp tp Phb mapping\n ");

        return LDP_FAILURE;
    }

    /* Get the DSCP from PHBID */
    LDP_DS_DSCP_FOR_PHBID (LDP_DS_LLSP_TLV_PSC (LlspTlv), u1Dscp);

    if (LdpDiffServValidateDscp (u1Dscp) == LDP_FAILURE)
    {
        *pu1ErrType = LDP_DS_INVALID_EXP_PHB_MAPPING;
        LDP_DBG (LDP_DS_PRCS,
                 ":Error- LLSP TLV - Invalid Exp tp Phb mapping\n ");

        return LDP_FAILURE;
    }

    /* Check has done to find whether PSC 
     *is supported br QoS or NOT */
    /* Check has done to find whether PHB is supported by QoS or not */
    /* Check should not be done for the egress case */
    if (CRLSP_TNL_ROLE (pCrlspTnlInfo) != LDP_TE_EGRESS)
    {

        LDP_DS_IS_PSC_SUPPORTED (u1Dscp, u1IsSupported, u1Count, u4OutIfIndex)
            if (u1IsSupported != LDP_DS_TRUE)
        {
            *pu1ErrType = LDP_DS_UNSUPPORTED_PSC;
            LDP_DBG (LDP_DS_PRCS, ":Error- LLSP TLV - Unsupported PSC\n ");

            return LDP_FAILURE;
        }
    }

    LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) = LDP_DS_LLSP;
    LDP_TE_DS_LLSP_DSCP (pDiffServTnlInfo) = u1Dscp;
    LDP_TE_DS_PARAMS_ELSPLISTINDEX (pDiffServTnlInfo) = LDP_ZERO;
    LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServTnlInfo) = NULL;

    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : ldpDiffServVadidateDscp
* Description   : This routine is called to validate whether 
*                 such DSCP exists or not or it is invalid. 
* Input(s)      : u1Dscp - Dscp value to be validated 
* Output(s)     : None                                                      
* Return(s)     : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServValidateDscp (UINT1 u1Dscp)
{

    switch (u1Dscp)
    {
        case LDP_DS_DF_DSCP:
        case LDP_DS_CS1_DSCP:
        case LDP_DS_CS2_DSCP:
        case LDP_DS_CS3_DSCP:
        case LDP_DS_CS4_DSCP:
        case LDP_DS_CS5_DSCP:
        case LDP_DS_CS6_DSCP:
        case LDP_DS_CS7_DSCP:
        case LDP_DS_EF_DSCP:
        case LDP_DS_AF11_DSCP:
        case LDP_DS_AF12_DSCP:
        case LDP_DS_AF13_DSCP:
        case LDP_DS_AF21_DSCP:
        case LDP_DS_AF22_DSCP:
        case LDP_DS_AF23_DSCP:
        case LDP_DS_AF31_DSCP:
        case LDP_DS_AF32_DSCP:
        case LDP_DS_AF33_DSCP:
        case LDP_DS_AF41_DSCP:
        case LDP_DS_AF42_DSCP:
        case LDP_DS_AF43_DSCP:
            return LDP_SUCCESS;

        default:
            LDP_DBG (LDP_DS_PRCS, ":Error- LdpDiffServValidateDscp failure\n ");

            return LDP_FAILURE;

    }
}

/*****************************************************************************
* Function Name : LdpSendDiffServNotifMsg                                     
* Description   : This routine constucts and sends the DiffServ 
*                 related Notification Message.
* Input(s)      : pSessionEntry - Points to the session entry on which 
*                                 this Notif Message has to be sent.
*                 pLspCtrlBlock - Points to the actual Lsp Control Block
*                                 which resulted in this Notification Msg
*                                 generation.                               
*                 u1DiffServStatusType  - Indicates the event being signalled.
*                 pu1RetPDU - pointer to the PDU to be returned.
*                 pu1Msg - Pointer to the messsage for which the notification 
*                          Msg is to be sent.
*                                                                           
* Output(s)     : None                                                      
* Return(s)     : LDP_SUCCESS  if Notif Message is constructed and sent 
*                 successfully, else LDP_FAILURE is returned.
*****************************************************************************/

UINT1
LdpSendDiffServNotifMsg (tLdpSession * pSessionEntry, UINT1 *pu1Msg,
                         UINT1 *pu1RetPDU, UINT1 u1DiffServStatusType,
                         tLspCtrlBlock * pLspCtrlBlock)
{
    UINT1               u1StatusTlvLen =
        (LDP_TLV_HDR_LEN + LDP_STATUS_CODE_LEN + LDP_MSG_ID_LEN +
         LDP_MSG_TYPE_LEN);

    UINT2               u2OptParamLen = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2MsgType;
    UINT2               u2MandParamLen =
        { (UINT2) (LDP_MSG_ID_LEN + u1StatusTlvLen) };
    UINT4               u4Offset = 0;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLdpMsgHdrAndId     MsgHdrAndId = { OSIX_HTONS (LDP_NOTIF_MSG), 0, 0 };
    tStatusTlv          StatusTlv = { {OSIX_HTONS (LDP_STATUS_TLV),
                                       OSIX_HTONS ((LDP_STATUS_CODE_LEN +
                                                    LDP_MSG_ID_LEN +
                                                    LDP_MSG_TYPE_LEN))}, 0, 0,
    0, 0
    };
    tTlvHdr             OptionalTlvHdr;
    tLdpId              PeerLdpId;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    LDP_DS_SUPRESS_WARNING (pu1Msg);
    MEMSET(&ConnId, LDP_ZERO, sizeof(tGenAddr));
    MEMSET(&LdpIfAddrZero, LDP_ZERO,sizeof(tGenU4Addr));
    /* Construct Optional TLV for Returned PDU/Message based on u1RetPDU */
    if (pu1RetPDU != NULL)
    {
        u2OptParamLen =
            (UINT2) (OSIX_HTONS (((tTlvHdr *) (VOID *) pu1RetPDU)->u2TlvLen) +
                     LDP_TLV_HDR_LEN);
        OptionalTlvHdr.u2TlvType = OSIX_HTONS (LDP_RETURNED_PDU_TLV);
    }

    u2MsgLen = (UINT2) (u2MandParamLen + u2OptParamLen);
    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((UINT4)(LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2MsgLen), (UINT4)LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS (u2MsgLen);
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pSessionEntry));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry));

    if (CRU_BUF_Copy_OverBufChain (pMsg,
                                   (UINT1 *) &MsgHdrAndId,
                                   u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_DS_MEM,
                 "NOTIF: Error in copying DiffServNotif MsgHdr and "
                 "MsgId to Buf chain \n ");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);
    StatusTlv.u4StatusCode =
        OSIX_HTONL ((*(au4DiffServStatusData + u1DiffServStatusType)));

    switch (u1DiffServStatusType)
    {
        case LDP_DS_STAT_TYPE_UNEXPECTED_DIFFSERV_TLV:
        case LDP_DS_STAT_TYPE_UNSUPPORTED_PHB:
        case LDP_DS_STAT_TYPE_INVALID_EXP_PHB_MAPPING:
        case LDP_DS_STAT_TYPE_UNSUPPORTED_PSC:
        case LDP_DS_STAT_TYPE_PER_LSPCONTEXT_ALLOCFAIL:
        case LDP_DS_STAT_TYPE_UNSUPPORTED_CLASS_TYPE_TLV:
        case LDP_DS_STAT_TYPE_INVALID_CLASS_TYPE_VALUE:
        case LDP_DS_STAT_TYPE_ELSPTP_UNSUPPORTED_PSC:
        case LDP_DS_STAT_TYPE_ELSPTP_UNAVL_OARSRC:
        case LDP_DS_STAT_TYPE_UNEXP_CLASS_TYPE_TLV:
        case LDP_DS_STAT_TYPE_UNSUPPORTED_ELSPTP_TLV:
            u2MsgType = LDP_LBL_REQUEST_MSG;
            StatusTlv.u2MsgType = OSIX_HTONS (u2MsgType);
            StatusTlv.u4MsgId = OSIX_HTONL (pLspCtrlBlock->u4UStrLblReqId);
            break;
        default:
            /* Assign MsgId and MsgType to default zero. */
            StatusTlv.u2MsgType = 0;
            StatusTlv.u4MsgId = 0;
            break;
    }

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &StatusTlv, u4Offset,
                                   u1StatusTlvLen) == CRU_FAILURE)
    {
        LDP_DBG (LDP_DS_MEM,
                 "NOTIF: Error while copying DiffServ Status Tlv to buffer\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    u4Offset += u1StatusTlvLen;
    /* Construct the optional TLV if present */
    if (u2OptParamLen != 0)
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &OptionalTlvHdr, u4Offset,
             LDP_TLV_HDR_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_DS_MEM,
                     "NOTIF: Error while copying DiffServ Optional Tlv "
                     "Hdr Notif to buffer \n ");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }

        /* Copy the optional value feilds */
        u4Offset += LDP_TLV_HDR_LEN;
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, pu1RetPDU, u4Offset,
             (UINT4)(u2OptParamLen - LDP_TLV_HDR_LEN)) == CRU_FAILURE)
        {
            LDP_DBG (LDP_DS_MEM,
                     "NOTIF: Error while copying DiffServ Returned Msg "
                     "PDU to buffer \n ");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }

        u4Offset += (UINT4)(u2OptParamLen - LDP_TLV_HDR_LEN);
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);
    SSN_GET_PEERID (PeerLdpId, pSessionEntry);
    LDP_DBG6 (LDP_DS_MISC,
              "NOTIF: Sending DiffServNotif Msg To   : %#x:%x:%x:%x:%x:%x \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2],
              PeerLdpId[3], PeerLdpId[4], PeerLdpId[5]);

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg
        (pMsg, u2MsgLen,
         SSN_GET_ENTITY (pSessionEntry), LDP_FALSE,
         ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        /*MPLS_IPv6 add end*/
        LDP_DBG (LDP_DS_TX, "NOTIF: Failed to send DiffServNotif Msg\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name :  LdpDiffServConstructCopyDSTlv
* Description   :  This routine is called to construct and copy 
*                  the DiffServ TLV in the label request message.
*                  Reference - draft-ietf-mpls-diff-ext-09.txt
*                  Section - 6
* Input(s)      :  pCrlspTnlInfo - Pointer to the CrlspTnlInfo
*               :  pMsg - Pointer to the label request message where 
*                        the TLV is to be copied.
*               :  pu4OffSet - Pointer to the offset in pMsg from where 
*                             the TLV is to be copied.
* Output(s)     :  None
* Return(s)     :  LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServConstructCopyDSTlv (tCrlspTnlInfo * pCrlspTnlInfo,
                               tCRU_BUF_CHAIN_HEADER * pMsg, UINT4 *pu4Offset)
{
    UINT2               u2DiffServElspValueLen;
    UINT2               u2NumberOfMapEntries = 0;
    UINT1               u1MapEntries;
    UINT2               u2PhbId;
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);
    tDiffServElspTlvPrefix ElspTlvPrefix;
    tDiffServElspMapEntry ElspTlvMapEntry;
    tDiffServLlspTlv    LlspTlv;
    tTlvHdr             DiffServTlvHdr = { OSIX_HTONS (CRLDP_DIFFSERV_TLV),
        0
    };
    tTMO_SLL           *pElspInfoList = NULL;
    tMplsDiffServElspInfo *pTempElspInfo = NULL;

    /* Check if the flag is set for DiffServ TLV.This flag decides 
     * whether the TLV has to be constructed or NOT */

    if ((CRLSP_PARM_FLAG (pCrlspTnlInfo) & LDP_DIFFSERV_CRLSP) ==
        LDP_DIFFSERV_CRLSP)
    {
        /* If It is E-lsp Tlv */
        if (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) == LDP_DS_ELSP)
        {
            /* Construct and copy DIFFSERV ELSP TLV header */

            if (LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) == LDP_DS_SIG_ELSP)
            {
                /* Count the number of entries */
                if (LDP_TE_DS_PARAMS_ELSPLISTINDEX (pDiffServTnlInfo)
                    != LDP_ZERO)
                {
                    pElspInfoList = (LDP_TE_DS_ELSPINFOLIST
                                     (LDP_TE_DS_PARAMS_ELSPLIST_PTR
                                      (pDiffServTnlInfo)));
                    u2NumberOfMapEntries =
                        (UINT2) TMO_SLL_Count (pElspInfoList);
                }
            }
            u2DiffServElspValueLen =
                (UINT2) ((u2NumberOfMapEntries *
                          sizeof (tDiffServElspMapEntry)) +
                         sizeof (tDiffServElspTlvPrefix));
            DiffServTlvHdr.u2TlvLen = OSIX_HTONS (u2DiffServElspValueLen);

            /* Copy for the DiffServ ELSP TLV Header */

            if (CRU_BUF_Copy_OverBufChain (pMsg,
                                           (UINT1 *) &DiffServTlvHdr,
                                           *pu4Offset,
                                           LDP_TLV_HDR_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_DS_MEM,
                         "ADVT: Error copying DiffServElspTlvHdr "
                         "to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }

            *pu4Offset += LDP_TLV_HDR_LEN;

            u1MapEntries = (UINT1) u2NumberOfMapEntries;

            /* Construct the value feild of DIffServ ELSP TLV */

            LDP_DS_ELSP_TLV_PREFIX_TBIT (ElspTlvPrefix) =
                (LDP_DS_ELSP_TBIT << 7);
            LDP_DS_ELSP_TLV_PREFIX_RESERVED (ElspTlvPrefix) =
                (UINT1) (LDP_DS_RESERVED);
            LDP_DS_ELSP_TLV_PREFIX_RESERVED1 (ElspTlvPrefix) =
                (UINT1) (LDP_DS_RESERVED);
            LDP_DS_ELSP_TLV_PREFIX_NO_OF_MAPENTRIES (ElspTlvPrefix) =
                (u1MapEntries);

            if (CRU_BUF_Copy_OverBufChain (pMsg,
                                           (UINT1 *) &ElspTlvPrefix,
                                           *pu4Offset,
                                           sizeof (tDiffServElspTlvPrefix)) ==
                CRU_FAILURE)
            {
                LDP_DBG (LDP_DS_MEM,
                         "ADVT: Error copying DiffServElspTlvHdr to "
                         "Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }

            *pu4Offset += sizeof (tDiffServElspTlvPrefix);

            /* This code is added for preconfigured at ingress only */

            if (LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) == LDP_DS_PRECONF_ELSP)
            {
                return LDP_SUCCESS;
            }

            /* Construct and copy the ELSP MAP ENTRIES */
            pElspInfoList = (LDP_TE_DS_ELSPINFOLIST
                             (LDP_TE_DS_PARAMS_ELSPLIST_PTR
                              (pDiffServTnlInfo)));

            TMO_SLL_Scan (pElspInfoList, pTempElspInfo, tMplsDiffServElspInfo *)
            {
                LDP_DS_ELSP_TLV_MAPENTRY_RESERVED (ElspTlvMapEntry) =
                    OSIX_HTONS (LDP_DS_RESERVED);

                LDP_DS_ELSP_TLV_MAPENTRY_EXP (ElspTlvMapEntry) =
                    (UINT1) (LDP_TE_DS_ELSPINFO_INDEX (pTempElspInfo) - 1);

                DSCP_TO_ELSP_PHBID (LDP_TE_DS_ELSPINFO_PHB_DSCP
                                    (pTempElspInfo), u2PhbId);

                LDP_DS_ELSP_TLV_MAPENTRY_PHBID (ElspTlvMapEntry) =
                    OSIX_HTONS (u2PhbId);

                if (CRU_BUF_Copy_OverBufChain (pMsg,
                                               (UINT1 *) &ElspTlvMapEntry,
                                               *pu4Offset,
                                               sizeof (tDiffServElspMapEntry))
                    == CRU_FAILURE)
                {
                    LDP_DBG (LDP_DS_MEM,
                             "ADVT: Error copying DiffServElspMapEntry "
                             "to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                *pu4Offset += sizeof (tDiffServElspMapEntry);
            }
            return LDP_SUCCESS;
        }
        else if (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) == LDP_DS_LLSP)
        {
            DiffServTlvHdr.u2TlvLen = OSIX_HTONS (LDP_DS_LLSP_TLV_LEN);

            /* Copy and construct DiffServ L-LSP TLV Header */

            if (CRU_BUF_Copy_OverBufChain (pMsg,
                                           (UINT1 *) &DiffServTlvHdr,
                                           *pu4Offset,
                                           LDP_TLV_HDR_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_DS_MEM,
                         "ADVT: Error copying DiffServLlspTlvHdr to "
                         "Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }

            *pu4Offset += LDP_TLV_HDR_LEN;

            LDP_DS_LLSP_TLV_TBIT (LlspTlv) = (UINT1) (LDP_DS_LLSP_TBIT << 7);

            LDP_DS_LLSP_TLV_RESERVED (LlspTlv) = (UINT1) (LDP_DS_RESERVED);

            /* Do conversion of PSC to PHBID */

            LDP_DS_DSCP_TO_LLSP_PHBID (LDP_TE_DS_LLSP_DSCP (pDiffServTnlInfo),
                                       u2PhbId);
            LDP_DS_LLSP_TLV_PSC (LlspTlv) = OSIX_HTONS (u2PhbId);

            /* Copy for the DiffServ L-LSP TLV Value */

            if (CRU_BUF_Copy_OverBufChain (pMsg,
                                           (UINT1 *) &LlspTlv,
                                           *pu4Offset,
                                           sizeof (tDiffServLlspTlv))
                == CRU_FAILURE)
            {

                LDP_DBG (LDP_DS_MEM,
                         "ADVT: Error copying DiffServLlspTlv Value to "
                         "Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            *pu4Offset += sizeof (tDiffServLlspTlv);
            return LDP_SUCCESS;
        }
    }
    /* The mapping is preconfigured at the intermediate without sending 
     * any diffServ TLV or the service type is NON_DIFFSERV_LSP
     */
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServGetOptionalDSTlvLength
* Description   : This routine is called to get the optional length 
*                 needed for allocating buffer if mpls is diffServ 
*                 enabled while sending DiffServ TLV in  the label 
*                 request message.
*
* Input(s)      : pCrlspTnlInfo - Pointer to the CrlspTnlInfo
*               : pu4OptionalLength - Pointer to the optional length in 
*                                    LdpSendCrlspLblMappingMsg
* Output(s)     : None
* Return(s)     : LDP_SUCCESS
*****************************************************************************/
UINT1
LdpDiffServGetOptionalDSTlvLength (tCrlspTnlInfo * pCrlspTnlInfo,
                                   UINT2 *pu2OptionalLength)
{
    UINT2               u2DiffServElspValueLen;
    UINT2               u2NumberOfMapEntries = 0;
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);

    tTMO_SLL           *pElspInfoList = NULL;

    if ((CRLSP_PARM_FLAG (pCrlspTnlInfo) & LDP_DIFFSERV_CRLSP) ==
        LDP_DIFFSERV_CRLSP)
    {
        /* This lines will be processed at ingress only */
        if ((LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) == LDP_DS_ELSP) &&
            (LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) == LDP_DS_PRECONF_ELSP))
        {
            *pu2OptionalLength += (UINT2)(LDP_TLV_HDR_LEN + LDP_DIFF_PRECONF_LEN);
            return LDP_SUCCESS;
        }
        /* If the TLV to be created is E-LSP TLV */
        if ((LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) == LDP_DS_ELSP) &&
            (LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) == LDP_DS_SIG_ELSP))
        {
            /* Get optional length for E-LSP header */
            /* Count the number of entries */

            if (LDP_TE_DS_PARAMS_ELSPLISTINDEX (pDiffServTnlInfo) != LDP_ZERO)
            {
                pElspInfoList = (LDP_TE_DS_ELSPINFOLIST
                                 (LDP_TE_DS_PARAMS_ELSPLIST_PTR
                                  (pDiffServTnlInfo)));
                u2NumberOfMapEntries = (UINT2) TMO_SLL_Count (pElspInfoList);
            }

            u2DiffServElspValueLen =
                (UINT2) (u2NumberOfMapEntries * sizeof (tDiffServElspMapEntry) +
                         sizeof (tDiffServElspTlvPrefix));
            *pu2OptionalLength += (UINT2)(LDP_TLV_HDR_LEN + u2DiffServElspValueLen);
        }
        /* Else the TLV to be created will be LLSP TLV */
        else if (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) == LDP_DS_LLSP)
        {
            *pu2OptionalLength += (UINT2)(LDP_TLV_HDR_LEN + LDP_DS_LLSP_TLV_LEN);
            return LDP_SUCCESS;
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServDeleteMem
* Description   : This routine is called to delete the diffServ related
*                 memories. 
* Input(s)      : pCrlspTnlInfo - Pointer to the CrlspTnlInfo
*               : u2IncarnId - The IncarnId 
* Output(s)     : None
* Return(s)     : LDP_SUCCESS
*****************************************************************************/
UINT1
LdpDiffServDeleteMem (tCrlspTnlInfo * pCrlspTnlInfo, UINT2 u2IncarnId)
{
    UINT2               u2PscIndex;

    UNUSED_PARAM (u2IncarnId);

    /* This deletes the ElspTP related mempools in the intermediate nodes.
       according to the traffic profile policies */

    for (u2PscIndex = 0; u2PscIndex < LDP_DS_MAX_NO_PSCS; u2PscIndex++)
    {
        if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW (pCrlspTnlInfo, u2PscIndex)
            != NULL)
        {
            MemReleaseMemBlock (LDP_DS_ELSP_TRFC_POOL_ID,
                                (UINT1 *)
                                (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                                 (pCrlspTnlInfo, u2PscIndex)));
            LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW (pCrlspTnlInfo, u2PscIndex) =
                NULL;
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServHandleElspTPTlv
*
* Description   : This routine is called if the mpls is diffServ enabled 
*                  to handle the ELSP-TP Tlv
*                  Reference - draft-ganti-mpls-diff-elsp-00.txt
*                  Section - 4
* Input(s)      : pLspCtrlBlock - Pointer to the LspCtrlBlock
*
*               : pElspTPTlv - Pointer to the Elsp-TP Tlv received in the
*                              label request message
*               : pMsg - Pointer to the label request message.
* Output(s)     : None
* Return(s)     : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServHandleElspTPTlv (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg,
                            UINT1 *pu1ElspTPTlv)
{

    UINT1               u1Count;
    UINT2               u2IncarnId;
    UINT1               u1Dscp;
    UINT1              *pu1ElspTPInfo = pu1ElspTPTlv;
    tTeTrfcParams      *pTeDSTrfcParms = NULL;
    tTeTrfcParams       TempTeTrfcParams;
    tTeTrfcParams      *pTempTeTrfcParams = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = LCB_TNLINFO (pLspCtrlBlock);
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);
    tDiffServElspTPTlvPrefix ElspTPTlvPrefix;
    tLdpDiffServElspTPEntry ElspTPMapEntry;
    tLdpTeCRLDPTrfcParams TmpLdpTeCRLDPTrfcParams;
    tTeTnlInfo         *pTeTnlInfo = CRLSP_TE_TNL_INFO (pCrlspTnlInfo);
    UINT1               u1PscFound = LDP_FALSE;
    tMplsDiffServElspInfo *pTempElspInfo = NULL;
    tTMO_SLL            ElspInfoList;
    tTMO_SLL           *pElspInfoList = NULL;
    tMplsDiffServElspInfo aMplsDiffServElspInfo[8];
    UINT1               u1PhbPsc;
    UINT1               u1PreConfIndex;
    UINT1               u1DuplicateError = LDP_FALSE;
    UINT1               u1TeTrfcParamsDelete = LDP_FALSE;
    tMplsDiffServElspList *pMplsDiffServElspList = NULL;

    TMO_SLL_Init (&ElspInfoList);
    MEMSET (&TmpLdpTeCRLDPTrfcParams, LDP_ZERO, sizeof (tLdpTeCRLDPTrfcParams));
    MEMSET (&TempTeTrfcParams, LDP_ZERO, sizeof (tTeTrfcParams));

    TMO_SLL_Init (&ElspInfoList);
    /* Check whether the manager is configured to do reservation 
     *for perOA basis */
    if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo) !=
        LDP_DS_PEROABASED_RESOURCES)
    {
        /* if the management type is not per OA based, 
         *then send the notification message instead of ignoring the TLV 
         *Just ignore the TLV */

        LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock), NULL,
                                 pu1ElspTPTlv,
                                 LDP_DS_STAT_TYPE_UNSUPPORTED_ELSPTP_TLV,
                                 pLspCtrlBlock);
        LDP_DBG (LDP_DS_PRCS, ":Error- Resource Manager is not perOA based\n ");

        return LDP_FAILURE;
    }

    if (pDiffServTnlInfo == NULL)
    {
        LDP_DBG (LDP_DS_PRCS,
                 ":Error- DiffServTnlInfo is not expected to be NULL\n ");

        return LDP_FAILURE;
    }

    /* Check whether LSP is Elsp configured, then only this TLV 
     *is valid, otherwise ignore the TLV */
    if (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) != LDP_DS_ELSP)
    {
        LDP_DBG (LDP_DS_PRCS, ":Msg- Ignoring the ELSP-TP Tlv\n ");

        return LDP_SUCCESS;
    }

    u2IncarnId = SSN_GET_INCRN_ID (LCB_USSN (pLspCtrlBlock));
    pu1ElspTPInfo += LDP_TLV_HDR_LEN;

    LDP_EXT_2_BYTES (pu1ElspTPInfo,
                     LDP_DS_ELSPTP_TLV_PREFIX_RESERVED (ElspTPTlvPrefix));
    LDP_EXT_2_BYTES (pu1ElspTPInfo,
                     LDP_DS_ELSPTP_TLV_PREFIX_NOOFTPENTRIES (ElspTPTlvPrefix));

    if (LDP_DS_ELSPTP_TLV_PREFIX_NOOFTPENTRIES (ElspTPTlvPrefix) <= 0)
    {
        LDP_DBG (LDP_DS_PRCS,
                 ":Msg- Number of ELSP-TP Tlvs in the TLV are less than or "
                 "equal to zero\n ");

        return LDP_SUCCESS;
    }

    for (u1Count = 0;
         u1Count < LDP_DS_ELSPTP_TLV_PREFIX_NOOFTPENTRIES (ElspTPTlvPrefix);
         u1Count++)
    {

        LDP_EXT_2_BYTES (pu1ElspTPInfo,
                         LDP_DS_ELSPTP_MAPENTRY_RESERVED2 (ElspTPMapEntry));
        LDP_EXT_2_BYTES (pu1ElspTPInfo,
                         LDP_DS_ELSPTP_MAPENTRY_PSC (ElspTPMapEntry));

        LDP_DS_DSCP_FOR_PHBID (LDP_DS_ELSPTP_MAPENTRY_PSC (ElspTPMapEntry),
                               u1Dscp);

        u1PscFound = LDP_FALSE;
        /* Scan the List if it is signalled to find where to add the Trfc
         * Paramas*/
        if (LDP_TE_DS_PARAMS_ELSPLISTINDEX (pDiffServTnlInfo) != LDP_ZERO)

        {

            pElspInfoList = (LDP_TE_DS_ELSPINFOLIST
                             (LDP_TE_DS_PARAMS_ELSPLIST_PTR
                              (pDiffServTnlInfo)));

            TMO_SLL_Scan (pElspInfoList, pTempElspInfo, tMplsDiffServElspInfo *)
            {

                u1PhbPsc =
                    ldpTeDiffServGetPhbPsc (LDP_TE_DS_ELSPINFO_PHB_DSCP
                                            (pTempElspInfo));
                if (u1PhbPsc == u1Dscp)
                {
                    u1PscFound = LDP_TRUE;
                    break;
                }

            }

            /* Check for duplicate values */
            if ((u1PscFound == LDP_TRUE) &&
                (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pTempElspInfo) != NULL))
            {
                /* Ignore the duplicate values */
                /* Four bytes are read */
                pu1ElspTPInfo += sizeof (tLdpDiffServElspTPEntry) - 4;
                continue;
            }
        }
        else                    /* The mapping is preconfigured */
        {
            /* Check whether the preconfigured mapping is present or not? */
            if ((LDP_DS_IS_PRECONF_VALID (u2IncarnId)) == LDP_FALSE)
            {
#ifndef NPAPI_WANTED
                if (ldpMplsGetPreConfMap (u2IncarnId,
                                          LDP_DS_PRECONF_ELSP_MAP (u2IncarnId))
                    != LDP_SUCCESS)
                {
                    /* Send Notification message */

                    LdpSendDiffServNotifMsg (LCB_USSN
                                             (pLspCtrlBlock), NULL,
                                             NULL,
                                             LDP_DS_ELSPTP_UNSUPPORTED_PSC,
                                             pLspCtrlBlock);

                    LDP_DBG (LDP_DS_PRCS,
                             ":Error- ELSP-TP TLV , Unsupported PSC\n ");

                    return LDP_FAILURE;

                }
                else
                {
                    LDP_DS_IS_PRECONF_VALID (u2IncarnId) = LDP_TRUE;
                }
#else

/* TODO:The above is fm call. so For diffserv same functionality should be added */

#endif
            }

            /* Check in the preconfigured array if the PSC is present */
            for (u1PreConfIndex = 0; u1PreConfIndex < MAX_DS_EXP;
                 u1PreConfIndex++)
            {
                u1PhbPsc =
                    ldpTeDiffServGetPhbPsc (LDP_DS_PRECONF_MAP_PHB_DSCP
                                            (u2IncarnId, u1PreConfIndex));
                if (u1PhbPsc == u1Dscp)
                {
                    u1PscFound = LDP_TRUE;
                    break;
                }
            }
            /* Allocate ElspNode for it */
            if (u1PscFound == LDP_TRUE)
            {

                /* Scan the list to check for duplicate values */
                TMO_SLL_Scan (&ElspInfoList, pTempElspInfo,
                              tMplsDiffServElspInfo *)
                {

                    if (LDP_TE_DS_ELSPINFO_PHB_DSCP (pTempElspInfo) == u1Dscp)
                    {
                        u1DuplicateError = LDP_TRUE;
                    }
                }
                if (u1DuplicateError == LDP_TRUE)
                {
                    /* Ignore the duplicate values */
                    /* Four bytes are read */
                    pu1ElspTPInfo += sizeof (tLdpDiffServElspTPEntry) - 4;
                    continue;

                }
                TMO_SLL_Init_Node (&LDP_TE_DS_ELSPINFO_NEXT
                                   (&(aMplsDiffServElspInfo[u1Count])));
                TMO_SLL_Add (&ElspInfoList,
                             &LDP_TE_DS_ELSPINFO_NEXT (&
                                                       (aMplsDiffServElspInfo
                                                        [u1Count])));
                pTempElspInfo = &(aMplsDiffServElspInfo[u1Count]);
            }

        }

        if (u1PscFound == LDP_FALSE)
        {
            /* Send notification message */
            LdpSendDiffServNotifMsg (LCB_USSN
                                     (pLspCtrlBlock), NULL,
                                     NULL,
                                     LDP_DS_ELSPTP_UNSUPPORTED_PSC,
                                     pLspCtrlBlock);
            LDP_DBG (LDP_DS_PRCS, ":Error- ELSP-TP TLV , Unsupported PSC\n ");

            u1TeTrfcParamsDelete = LDP_TRUE;
            break;
        }

        pTempTeTrfcParams = &TempTeTrfcParams;

        LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pTempElspInfo) = pTempTeTrfcParams;
        CRLSP_TE_CRLDP_TRFC_PARAMS ((pTempTeTrfcParams)) =
            &TmpLdpTeCRLDPTrfcParams;

        if (CRLSP_TE_TNL_ROLE (pTeTnlInfo) == LDP_TE_INTER)
        {
            CRLSP_TE_TRFC_PARAM_ROLE (pTempTeTrfcParams) = LDP_TE_INTER;
        }
        else
        {
            CRLSP_TE_TRFC_PARAM_ROLE (pTempTeTrfcParams) = LDP_TE_EGRESS;
        }

        CRLSP_TE_TNLRSRC_ROW_STATUS (pTempTeTrfcParams) = ACTIVE;
        CRLSP_TE_TRFC_PARAM_NUM_TNLS (pTempTeTrfcParams) = 1;

        LDP_EXT_1_BYTES (pu1ElspTPInfo,
                         CRLSP_TE_CRLDP_TPARAM_FLAGS (pTempTeTrfcParams));
        LDP_EXT_1_BYTES (pu1ElspTPInfo,
                         CRLSP_TE_CRLDP_TPARAM_FREQ (pTempTeTrfcParams));
        pu1ElspTPInfo += LDP_UINT1_LEN;
        LDP_EXT_1_BYTES (pu1ElspTPInfo,
                         CRLSP_TE_CRLDP_TPARAM_WEIGHT (pTempTeTrfcParams));
        LDP_EXT_4_BYTES (pu1ElspTPInfo,
                         CRLSP_TE_CRLDP_TPARAM_PDR (pTempTeTrfcParams));
        LDP_EXT_4_BYTES (pu1ElspTPInfo,
                         CRLSP_TE_CRLDP_TPARAM_PBS (pTempTeTrfcParams));
        LDP_EXT_4_BYTES (pu1ElspTPInfo,
                         CRLSP_TE_CRLDP_TPARAM_CDR (pTempTeTrfcParams));
        LDP_EXT_4_BYTES (pu1ElspTPInfo,
                         CRLSP_TE_CRLDP_TPARAM_CBS (pTempTeTrfcParams));
        LDP_EXT_4_BYTES (pu1ElspTPInfo,
                         CRLSP_TE_CRLDP_TPARAM_EBS (pTempTeTrfcParams));
        /* Check For PDR and CDR values */
        if (CRLSP_TE_CRLDP_TPARAM_CDR (pTempTeTrfcParams) >
            CRLSP_TE_CRLDP_TPARAM_PDR (pTempTeTrfcParams))
        {
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock),
                             pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_TRAF_PARMS_UNAVL, NULL);
            LDP_DBG (LDP_DS_PRCS, ":Error- ELSP-TP TLV , CDR > PDR\n ");

            u1TeTrfcParamsDelete = LDP_TRUE;
            break;
        }

        if (LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) == LDP_DS_PRECONF_ELSP)
        {
            /* Then  allocate memory for TeTrfcParams */
            if (ldpTeCreateTrfcParams
                (LDP_TE_PROT_ID, &pTeDSTrfcParms,
                 &TempTeTrfcParams) == LDP_TE_FAILURE)
            {
                LDP_DBG (LDP_DS_MEM,
                         ":Error- ldpTeCreateTrfcParams allocation failure\n ");

                u1TeTrfcParamsDelete = LDP_TRUE;
                LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock), NULL,
                                         pu1ElspTPTlv,
                                         LDP_DS_STAT_TYPE_RESOURCE_UNAVAIL,
                                         pLspCtrlBlock);

            }
            LDP_TE_DS_ELSPINFO_INDEX (pTempElspInfo) = u1Count;
            LDP_TE_DS_ELSPINFO_PHB_DSCP (pTempElspInfo) = u1Dscp;
            LDP_TE_DS_ELSPINFO_ROW_STATUS (pTempElspInfo) = LDP_ACTIVE;
            LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pTempElspInfo) = pTeDSTrfcParms;

        }
        else                    /* The mapping is signalled */
        {
            ldpTeUpdateTrfcParamsElspList (LDP_TE_DS_PARAMS_ELSPLIST_PTR
                                           (pDiffServTnlInfo),
                                           pTempTeTrfcParams,
                                           LDP_TE_DS_ELSPINFO_INDEX
                                           (pTempElspInfo), LDP_TE_TP_ADD);

        }
    }

    if ((LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) == LDP_DS_PRECONF_ELSP) &&
        (u1TeTrfcParamsDelete == LDP_TRUE))

    {
        if (pElspInfoList != NULL)
        {
            /* Scan the Sll List */
            TMO_SLL_Scan (pElspInfoList, pTempElspInfo, tMplsDiffServElspInfo *)
            {
                if (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pTempElspInfo) != NULL)
                {
                    /* Delete the Trfc params */
                    ldpTeDeleteTrfcParams
                        (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pTempElspInfo));

                }

            }
        }
        return LDP_FAILURE;
    }

    if (LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) == LDP_DS_PRECONF_ELSP)
    {
        /* Create the ElspList for Traffic Params alone */
        if (ldpTeCreateDiffServElspList (&pMplsDiffServElspList,
                                         &ElspInfoList) == LDP_FAILURE)
        {
            LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                  LDP_STAT_TYPE_RESOURCE_UNAVAIL, pMsg);

            return LDP_FAILURE;
        }
        LDP_TE_DS_PARAMS_ELSPLISTINDEX (pDiffServTnlInfo) =
            LDP_TE_DS_ELSPINFOLIST_INDEX (pMplsDiffServElspList);
        LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServTnlInfo) =
            pMplsDiffServElspList;

    }
    /* All the information is copied if no error has occured */
    CRLSP_PARM_FLAG (pCrlspTnlInfo) |= LDP_RSRC_PEROA_CRLSP;
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServElspTPForRM 
* Description   : This routine is called if the mpls is diffServ enabled 
*                 to store ELSP-TP pointers so that it can be easily accessed
* Input(s)      : pLspCtrlBlock - Pointer to the LspCtrlBlock
*               : pMsg - Pointer to the label request message.
* Output(s)     : None
* Return(s)     : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServElspTPForRM (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{

    UINT2               u2IncarnId;
    UINT2               u2PscIndex = 0;
    UINT1               u1Dscp;
    UINT1               u1Psc;
    tPerOATrfcProfileEntry *pElspTrfcProfileEntry = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = LCB_TNLINFO (pLspCtrlBlock);
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);
    tTMO_SLL           *pElspInfoList = NULL;
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    u2IncarnId = SSN_GET_INCRN_ID (LCB_USSN (pLspCtrlBlock));

    if ((CRLSP_PARM_FLAG (pCrlspTnlInfo) & LDP_RSRC_PEROA_CRLSP) ==
        LDP_RSRC_PEROA_CRLSP)
    {
        pElspInfoList = (LDP_TE_DS_ELSPINFOLIST
                         (LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServTnlInfo)));

        TMO_SLL_Scan (pElspInfoList, pElspInfoNode, tMplsDiffServElspInfo *)
        {
            if (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pElspInfoNode) != NULL)
            {
                /* Allocate memory for tPerOATrfcProfileEntry from the 
                 * memory pool */
                pElspTrfcProfileEntry = (tPerOATrfcProfileEntry *)
                    MemAllocMemBlk (LDP_DS_ELSP_TRFC_POOL_ID);

                if (pElspTrfcProfileEntry == NULL)
                {
                    LDP_DBG (LDP_DS_MEM,
                             ":Error- LDP_DS_ELSP_TRFC_POOL_ID Failure:\n ");

                    LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                          pLspCtrlBlock,
                                          LDP_STAT_TYPE_RESOURCE_UNAVAIL, pMsg);
                    LdpDiffServDeleteMem (pCrlspTnlInfo, u2IncarnId);

                    return LDP_FAILURE;
                }
                MEMSET (pElspTrfcProfileEntry, LDP_DS_ZERO,
                        sizeof (tPerOATrfcProfileEntry));

                u1Dscp = LDP_TE_DS_ELSPINFO_PHB_DSCP (pElspInfoNode);
                u1Psc = ldpTeDiffServGetPhbPsc (u1Dscp);

                /* Get Index for Dscp */
                LdpDiffServGetIndexForPsc (u1Psc, &u2PscIndex);

                LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW (pCrlspTnlInfo,
                                                        u2PscIndex) =
                    pElspTrfcProfileEntry;

                LDP_DS_PEROATRFC_PARAMS (pCrlspTnlInfo, u2PscIndex) =
                    LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pElspInfoNode);

                LDP_DS_PEROA_TRFC_PROFILE_PSC (pCrlspTnlInfo, u2PscIndex) =
                    u1Dscp;
                LDP_DS_GET_PER_OA_RSRC_AVAIL (pCrlspTnlInfo, u2PscIndex) =
                    LDP_DS_FALSE;
            }
        }
    }
    return LDP_SUCCESS;
}

/******************************************************************************
* Function Name : LdpDiffServConstructCopyElspTPTlv
* Description   : This routine is called if the mpls is diffServ enabled 
*                 to construct and copy  ELSP-TP Tlv in the label request
*                 message
*
*               : Reference - draft-ganti-mpls-diff-elsp-00.txt
*               : Section - 4
* Input(s)      : pCrlspTnlInfo - Pointer to the CrlspTnlInfo
*               : pMsg - Pointer to the label request message which is 
*                        being constructed.
*               : pu4OffSet - Pointer to offset in the pMsg from where
*                        the message is to be copied
* Output(s)     : None 
* Return(s)     : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServConstructCopyElspTPTlv (tCrlspTnlInfo * pCrlspTnlInfo,
                                   tCRU_BUF_CHAIN_HEADER * pMsg,
                                   UINT4 *pu4Offset)
{
    UINT2               u2NumberOfTPEntries = 0;
    UINT2               u2DiffServElspTPValueLen;
    UINT2               u2PhbId;
    tDiffServElspTPTlvPrefix ElspTPTlvPrefix;
    tLdpDiffServElspTPEntry ElspTPMapEntry;
    tTlvHdr             ElspTPTlvHdr = { OSIX_HTONS
            (CRLDP_DIFFSERV_ELSPTP_TLV), 0
    };
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);
    tTMO_SLL           *pElspInfoList = NULL;
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    if ((CRLSP_PARM_FLAG (pCrlspTnlInfo) & LDP_RSRC_PEROA_CRLSP) ==
        LDP_RSRC_PEROA_CRLSP)
    {
        pElspInfoList = (LDP_TE_DS_ELSPINFOLIST
                         (LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServTnlInfo)));

        TMO_SLL_Scan (pElspInfoList, pElspInfoNode, tMplsDiffServElspInfo *)
        {
            if (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pElspInfoNode) != NULL)
            {
                u2NumberOfTPEntries++;
            }
        }
        u2DiffServElspTPValueLen =
            (UINT2) ((u2NumberOfTPEntries * sizeof (tLdpDiffServElspTPEntry)) +
                     sizeof (tDiffServElspTPTlvPrefix));
        ElspTPTlvHdr.u2TlvLen = OSIX_HTONS (u2DiffServElspTPValueLen);

        /* Do copy for the DiffServ ELSP TLV Header */
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &ElspTPTlvHdr, *pu4Offset,
             LDP_TLV_HDR_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_DS_MEM,
                     "Error: copying DiffServElspTPTlvHdr to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        *pu4Offset += LDP_TLV_HDR_LEN;

        LDP_DS_ELSPTP_TLV_PREFIX_RESERVED (ElspTPTlvPrefix) =
            OSIX_HTONS (LDP_DS_RESERVED);

        LDP_DS_ELSPTP_TLV_PREFIX_NOOFTPENTRIES (ElspTPTlvPrefix) =
            OSIX_HTONS (u2NumberOfTPEntries);

        if (CRU_BUF_Copy_OverBufChain (pMsg,
                                       (UINT1 *) &ElspTPTlvPrefix,
                                       *pu4Offset,
                                       sizeof (tDiffServElspTPTlvPrefix)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_DS_MEM,
                     "Error: copying DiffServElspTlvHdr to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }

        *pu4Offset += sizeof (tDiffServElspTPTlvPrefix);
        /* Construct the value field */

        TMO_SLL_Scan (pElspInfoList, pElspInfoNode, tMplsDiffServElspInfo *)
        {
            if (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pElspInfoNode) != NULL)
            {
                LDP_DS_DSCP_TO_PSC ((ldpTeDiffServGetPhbPsc
                                     (LDP_TE_DS_ELSPINFO_PHB_DSCP
                                      (pElspInfoNode))), u2PhbId);

                LDP_DS_ELSPTP_MAPENTRY_RESERVED2 (ElspTPMapEntry) =
                    LDP_DS_RESERVED;

                LDP_DS_ELSPTP_MAPENTRY_PSC (ElspTPMapEntry) =
                    OSIX_HTONS (u2PhbId);

                LDP_DS_ELSPTP_MAPENTRY_FLAGS (ElspTPMapEntry) =
                    (CRLSP_TE_CRLDP_TPARAM_FLAGS
                     (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pElspInfoNode)));

                LDP_DS_ELSPTP_MAPENTRY_FREQUENCY (ElspTPMapEntry) =
                    (CRLSP_TE_CRLDP_TPARAM_FREQ
                     (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pElspInfoNode)));

                LDP_DS_ELSPTP_MAPENTRY_RESERVED1 (ElspTPMapEntry) =
                    LDP_DS_RESERVED;

                LDP_DS_ELSPTP_MAPENTRY_WEIGHT
                    (ElspTPMapEntry) =
                    (CRLSP_TE_CRLDP_TPARAM_WEIGHT
                     (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pElspInfoNode)));

                LDP_DS_ELSPTP_MAPENTRY_PDR (ElspTPMapEntry) =
                    OSIX_HTONL (CRLSP_TE_CRLDP_TPARAM_PDR
                                (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS
                                 (pElspInfoNode)));

                LDP_DS_ELSPTP_MAPENTRY_PBS (ElspTPMapEntry) =
                    OSIX_HTONL (CRLSP_TE_CRLDP_TPARAM_PBS
                                (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS
                                 (pElspInfoNode)));

                LDP_DS_ELSPTP_MAPENTRY_CDR (ElspTPMapEntry) =
                    OSIX_HTONL (CRLSP_TE_CRLDP_TPARAM_CDR
                                (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS
                                 (pElspInfoNode)));

                LDP_DS_ELSPTP_MAPENTRY_CBS (ElspTPMapEntry) =
                    OSIX_HTONL (CRLSP_TE_CRLDP_TPARAM_CBS
                                (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS
                                 (pElspInfoNode)));

                LDP_DS_ELSPTP_MAPENTRY_EBS (ElspTPMapEntry) =
                    OSIX_HTONL (CRLSP_TE_CRLDP_TPARAM_EBS
                                (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS
                                 (pElspInfoNode)));

                if (CRU_BUF_Copy_OverBufChain (pMsg,
                                               (UINT1 *) &ElspTPMapEntry,
                                               *pu4Offset,
                                               sizeof (tLdpDiffServElspTPEntry))
                    == CRU_FAILURE)
                {
                    LDP_DBG (LDP_DS_MEM,
                             "Error: copying ElspTPEntry to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                *pu4Offset += sizeof (tLdpDiffServElspTPEntry);
            }
        }
    }
    /* No need of creating the Tlv if the flags are not set */
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServHandleClassTypeTlv
*
* Description   : This routine is called if the mpls is diffServ enabled
*                 and  Label Request is received which contains the classType
*                 TLV.The routine processes the ClassType Tlv.
* 
*                 Reference -
*
* Input(s)     : pLspCtrlBlock - Pointer to the LspCtrlBlock
*              : pu1DiffServTlv - Pointer to the ClassType Tlv present in 
*                                 the Msg.
*              : pMsg - Pointer to the label request message received.
* Output(s)    : None
* Return(s)    : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServHandleClassTypeTlv (tLspCtrlBlock * pLspCtrlBlock,
                               UINT1 *pu1ClassTypeTlv, UINT1 *pMsg)
{
    UINT1               u1IsSupported = LDP_DS_FALSE;
    UINT1               u1Count;
    UINT1              *pu1ClassTypeTlvInfo = pu1ClassTypeTlv;
    UINT4               u4OutIfIndex;
    tDiffServClassTypeTlv ClassTypeTlv;
    tCrlspTnlInfo      *pCrlspTnlInfo = LCB_TNLINFO (pLspCtrlBlock);
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);

    pu1ClassTypeTlvInfo += LDP_TLV_HDR_LEN;

    /* Get the interface */
    u4OutIfIndex = LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo)));
    LDP_DS_SUPRESS_WARNING (pMsg);

    /* Check whether the resource manager is configured to support *
     * classtype resources */

    if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo) !=
        LDP_DS_CLASSTYPE_RESOURCES)
    {
        LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock), NULL, NULL,
                                 LDP_DS_STAT_TYPE_UNEXP_CLASS_TYPE_TLV,
                                 pLspCtrlBlock);
        LDP_DBG (LDP_DS_MISC,
                 "Error: CLASS-TYPE TLV: Resource Management is not of "
                 "ClassType\n");
        return LDP_FAILURE;
    }

    LDP_EXT_2_BYTES (pu1ClassTypeTlvInfo,
                     LDP_DS_CLASS_TYPE_TLV_RESERVED (ClassTypeTlv));
    LDP_EXT_2_BYTES (pu1ClassTypeTlvInfo,
                     LDP_DS_CLASS_TYPE_TLV_CLASSTYPE (ClassTypeTlv));

    if ((LDP_DS_CLASS_TYPE_TLV_CLASSTYPE (ClassTypeTlv) >
         LDP_DS_MAX_CLASS_TYPE) ||
        (LDP_DS_CLASS_TYPE_TLV_CLASSTYPE (ClassTypeTlv) <= LDP_ZERO))
    {
        /* If Invalid ClassType, Send the notification message */
        LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock), NULL, NULL,
                                 LDP_DS_STAT_TYPE_INVALID_CLASS_TYPE_VALUE,
                                 pLspCtrlBlock);
        LDP_DBG (LDP_DS_PRCS,
                 "Error: CLASS-TYPE TLV Resource Management is not of "
                 "ClassType\n");
        return LDP_FAILURE;
    }

    /* Check whether classType is supported by the QoS or RM */
    /* If RM supports this classType */

    LDP_DS_IS_CLASSTYPE_SUPPORTED (LDP_DS_CLASS_TYPE_TLV_CLASSTYPE
                                   (ClassTypeTlv), u1IsSupported, u1Count,
                                   u4OutIfIndex) if (u1IsSupported ==
                                                     LDP_DS_FALSE)
    {
        /* Send Notification message for unsupported classType error */
        LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock), NULL, NULL,
                                 LDP_DS_STAT_TYPE_UNSUPPORTED_CLASS_TYPE_TLV,
                                 pLspCtrlBlock);
        LDP_DBG (LDP_DS_PRCS,
                 "Error: CLASS-TYPE TLV :Unsupported ClassType TLV\n");
        return LDP_FAILURE;
    }
    CRLSP_PARM_FLAG (pCrlspTnlInfo) |= LDP_CLASSTYPE_CRLSP;
    LDP_TE_DS_CLASS_TYPE (pDiffServTnlInfo) =
        (UINT1) LDP_DS_CLASS_TYPE_TLV_CLASSTYPE (ClassTypeTlv);
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServConstructCopyClassTypeTlv
*
* Description   :  This routine is called when the classType Tlv is to 
*                  be constructed for the label mapping message or the 
*                  label request message.
*
* Input(s)      : pCrlspTnlInfo - Pointer to the LspCtrlBlock
*               : pMsg - Pointer to the label request message received.
*               : pu4OffSet - OffSet in the pMsg from where the TLV should
*                             be copied.
* Output(s)     : None
* Return(s)     : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServConstructCopyClassTypeTlv (tCrlspTnlInfo * pCrlspTnlInfo,
                                      tCRU_BUF_CHAIN_HEADER * pMsg,
                                      UINT4 *pu4Offset)
{
    tTlvHdr             ClassTypeTlvHdr =
        { OSIX_HTONS (CRLDP_DIFFSERV_CLASSTYPE_TLV),
        OSIX_HTONS (LDP_DS_CLASSTYPE_TLV_LEN)
    };
    tMplsDiffServTnlInfo *pDiffServParams =
        LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);
    tDiffServClassTypeTlv ClassTypeTlv;
    UINT2               u2ClassType = 0;

    if ((CRLSP_PARM_FLAG (pCrlspTnlInfo) & LDP_CLASSTYPE_CRLSP) ==
        LDP_CLASSTYPE_CRLSP)
    {
        /* Copy for the CLASSTYPE TLV Header */

        if (CRU_BUF_Copy_OverBufChain (pMsg,
                                       (UINT1 *) &ClassTypeTlvHdr,
                                       *pu4Offset,
                                       LDP_TLV_HDR_LEN) == CRU_FAILURE)
        {

            LDP_DBG (LDP_DS_MEM,
                     ":Error copying ClassTypeTlvHdr to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }

        *pu4Offset += LDP_TLV_HDR_LEN;

        LDP_DS_CLASS_TYPE_TLV_RESERVED (ClassTypeTlv) = LDP_DS_RESERVED;

        u2ClassType = (UINT2) (LDP_TE_DS_CLASS_TYPE (pDiffServParams));
        LDP_DS_CLASS_TYPE_TLV_CLASSTYPE (ClassTypeTlv) =
            OSIX_HTONS (u2ClassType);

        if (CRU_BUF_Copy_OverBufChain (pMsg,
                                       (UINT1 *) &ClassTypeTlv,
                                       *pu4Offset,
                                       sizeof (tDiffServClassTypeTlv)) ==
            CRU_FAILURE)
        {

            LDP_DBG (LDP_DS_MEM, ": Error copying ClassTypeTlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        *pu4Offset += sizeof (tDiffServClassTypeTlv);
    }
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServGetOptionalElspTPLength
* Description   : This routine is called to get the optional length needed
*                 for allocating buffer if mpls is diffServ enabled while
*                 sending Elsp TP Tlv in the label request message.
*
* Input(s)      : pCrlspTnlInfo - Pointer to the CrlspTnlInfo
*
*               : pu4OptionalLength - Pointer to the optional length in
*                   LdpSendCrlspLblMappingMsg
* Output(s)     : None
* Return(s)      : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServGetOptionalElspTPLength (tCrlspTnlInfo * pCrlspTnlInfo,
                                    UINT2 *pu2OptionalLength)
{
    UINT1               u1NumberOfTrfcProfileEntries = 0;
    UINT2               u2DiffServElspTPValueLen;
    tTMO_SLL           *pElspInfoList = NULL;
    tMplsDiffServTnlInfo *pDiffServTnlInfo = NULL;
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    if ((CRLSP_PARM_FLAG (pCrlspTnlInfo) & LDP_RSRC_PEROA_CRLSP) ==
        LDP_RSRC_PEROA_CRLSP)
    {
        pDiffServTnlInfo = LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);

        pElspInfoList = (LDP_TE_DS_ELSPINFOLIST
                         (LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServTnlInfo)));

        TMO_SLL_Scan (pElspInfoList, pElspInfoNode, tMplsDiffServElspInfo *)
        {
            if (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pElspInfoNode) != NULL)
            {
                u1NumberOfTrfcProfileEntries++;
            }
        }

        u2DiffServElspTPValueLen =
            (UINT2) ((u1NumberOfTrfcProfileEntries *
                      sizeof (tLdpDiffServElspTPEntry)) +
                     sizeof (tDiffServElspTPTlvPrefix));
        *pu2OptionalLength += (UINT2)(LDP_TLV_HDR_LEN + u2DiffServElspTPValueLen);

    }

    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServGetIndexForPsc
*
* Description   : This routine is called to get the index for the PSC 
*                 in pCrlspTnlInfo->pPerOATrfcProfilePointerArrray[].
*
* Input(s)      : u1Psc - Psc value for which the index is needed
* Output(s)     : u2Index - Index of the PSC
* Return(s)     : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServGetIndexForPsc (UINT1 u1Psc, UINT2 *pu2Index)
{
    switch (u1Psc)
    {

        case LDP_DS_DF_DSCP:
            *pu2Index = INDEX_DF;
            break;

        case LDP_DS_CS1_DSCP:
            *pu2Index = INDEX_CS1;
            break;

        case LDP_DS_CS2_DSCP:
            *pu2Index = INDEX_CS2;
            break;

        case LDP_DS_CS3_DSCP:
            *pu2Index = INDEX_CS3;
            break;

        case LDP_DS_CS4_DSCP:
            *pu2Index = INDEX_CS4;
            break;

        case LDP_DS_CS5_DSCP:
            *pu2Index = INDEX_CS5;
            break;

        case LDP_DS_CS6_DSCP:
            *pu2Index = INDEX_CS6;
            break;

        case LDP_DS_CS7_DSCP:
            *pu2Index = INDEX_CS7;
            break;

        case LDP_DS_EF_DSCP:
            *pu2Index = INDEX_EF;
            break;

        case LDP_DS_AF1_PSC_DSCP:
            *pu2Index = INDEX_AF1;
            break;

        case LDP_DS_AF2_PSC_DSCP:
            *pu2Index = INDEX_AF2;
            break;

        case LDP_DS_AF3_PSC_DSCP:
            *pu2Index = INDEX_AF3;
            break;

        case LDP_DS_AF4_PSC_DSCP:
            *pu2Index = INDEX_AF4;
            break;

        default:
            LDP_DBG (LDP_DS_PRCS, ":Error LdpDiffServGetIndexForPsc\n");

            return LDP_FAILURE;

    }
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : ldpDiffServTrfcRsrcForLLSP
* Description   : This routine is called by the ingress LSR. The 
*                 routine puts the traffic params pointer in the
*                 pPerOATrfcProfilePointerArrray for L-LSP if the resource
*                 management is on perOA basis.
*
*                pTeTnlInfo - Pointer to the tunnelInfo
*                pMsg - Pointer to the message if the LSR is the intermediate
* Output(s)     : None
* Return(s)     : LDP_SUCCESS or LDP_FAILURE
*****************************************************************************/
UINT1
LdpDiffServTrfcRsrcForLLsp (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    UINT2               u2PscIndex = 0;
    tPerOATrfcProfileEntry *pPerOATrfcProfileEntry = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = LCB_TNLINFO (pLspCtrlBlock);
    tTeTnlInfo         *pTeTnlInfo = CRLSP_TE_TNL_INFO (pCrlspTnlInfo);
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);

    if (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) != LDP_DS_LLSP)
    {
        return LDP_SUCCESS;
    }

    if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo) !=
        LDP_DS_PEROABASED_RESOURCES)
    {
        return LDP_SUCCESS;
    }

    if (CRLSP_TRF_PARMS (pCrlspTnlInfo) == NULL)
    {
        return LDP_SUCCESS;
    }

    /* else Get the index for the PSC */

    LdpDiffServGetIndexForPsc (LDP_TE_DS_LLSP_DSCP (pDiffServTnlInfo),
                               &u2PscIndex);

    /* Allocate memory for tPerOATrfcProfileEntry from the memory pool */

    pPerOATrfcProfileEntry = (tPerOATrfcProfileEntry *)
        MemAllocMemBlk (LDP_DS_ELSP_TRFC_POOL_ID);

    if (pPerOATrfcProfileEntry == NULL)
    {
        /* If the node is not the ingress LSR, then send the notification 
         * message */
        if (CRLSP_TE_TNL_ROLE (pTeTnlInfo) != LDP_TE_INGRESS)
        {
            LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                  pLspCtrlBlock,
                                  LDP_STAT_TYPE_NO_LBL_RSRC, pMsg);
            LDP_DBG (LDP_DS_MEM,
                     ":Error:LDP_DS_ELSP_TRFC_POOL_ID "
                     "Allocation Failure For L-LSP\n");
        }
        return LDP_FAILURE;
    }
    LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW (pCrlspTnlInfo, u2PscIndex) =
        pPerOATrfcProfileEntry;
    LDP_DS_PEROATRFC_PARAMS (pCrlspTnlInfo, u2PscIndex) =
        CRLSP_TE_TNL_TRFC_PARAM (pTeTnlInfo);
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : ldpDIffServhandlePerOARsrc
* Description   : This routine will be invoked by the LDP with diffServ
*                 feature. The This routine calls the function to do the
*                 admission control for ELSP-TP tlv receivedl
* Input(s)      : pCrlspTnlInfo  - Pointer to the LspCtrlBlock.
*                 pMsg - Pointer to the message containing requirement of
*                     the traffic profiles.
* Return(s)     : LDP_SUCCESS in case the resources are available for
*                 all OAs
*                 or  LDP_FAILURE otherwise
*****************************************************************************/
VOID
LdpDiffServHandlePerOARsrc (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tCrlspTnlInfo      *pCrlspTnlInfo = pLspCtrlBlock->pCrlspTnlInfo;
    tTMO_SLL            pPreEmpList;
    tTMO_SLL           *pCandidatePreemp = (tTMO_SLL *) & pPreEmpList;

    LDP_SUPPRESS_WARNING (pMsg);

    if ((CRLSP_STACK_TUNN (pCrlspTnlInfo) == LDP_FALSE) ||
        (CRLSP_IS_EGR_OF_TUNN (pCrlspTnlInfo) == LDP_TRUE))
    {

        if (LdpDiffServTcResvResources (pCrlspTnlInfo) != LDP_SUCCESS)
        {

            /* Try to get resources by preemption */

            if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
            {
                LdpDiffServModifyTnlPrioList (LCB_OUT_IFINDEX
                                              (CRLSP_LCB
                                               (CRLSP_INS_POIN
                                                (pCrlspTnlInfo))),
                                              CRLSP_HOLD_PRIO
                                              (CRLSP_INS_POIN
                                               (pCrlspTnlInfo)),
                                              LCB_OUT_IFINDEX
                                              (CRLSP_LCB
                                               ((pCrlspTnlInfo))),
                                              CRLSP_SET_PRIO
                                              ((pCrlspTnlInfo)),
                                              CRLSP_INS_POIN (pCrlspTnlInfo));
            }

            /* PreEmption Module is called for bumping if any lower
             * priority tunnels,with which the resources could be met. 
             * Only the Modified set of resources will be looked for
             * if it's a crlsp modify.
             */

            if (LdpDiffServCrlspGetTnlFromPrioList
                (pCrlspTnlInfo, pCandidatePreemp) != LDP_SUCCESS)
            {
                LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock),
                                         NULL,
                                         NULL,
                                         LDP_DS_STAT_TYPE_ELSPTP_UNAVL_OARSRC,
                                         pLspCtrlBlock);
                if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
                {
                    LdpDiffServModifyTnlPrioList (LCB_OUT_IFINDEX
                                                  (CRLSP_LCB ((pCrlspTnlInfo))),
                                                  CRLSP_SET_PRIO
                                                  ((pCrlspTnlInfo)),
                                                  LCB_OUT_IFINDEX
                                                  (CRLSP_LCB (CRLSP_INS_POIN
                                                              (pCrlspTnlInfo))),
                                                  CRLSP_HOLD_PRIO
                                                  (CRLSP_INS_POIN
                                                   (pCrlspTnlInfo)),
                                                  CRLSP_INS_POIN
                                                  (pCrlspTnlInfo));
                }
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return;
            }

            LdpDiffServPreEmptTunnels (pCandidatePreemp);
            /* Now the Resources will be available for reservation */
            LdpDiffServTcResvResources (pCrlspTnlInfo);

            if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
            {
                LdpDiffServModifyTnlPrioList (LCB_OUT_IFINDEX
                                              (CRLSP_LCB ((pCrlspTnlInfo))),
                                              CRLSP_SET_PRIO ((pCrlspTnlInfo)),
                                              LCB_OUT_IFINDEX
                                              (CRLSP_LCB (CRLSP_INS_POIN
                                                          (pCrlspTnlInfo))),
                                              CRLSP_HOLD_PRIO (CRLSP_INS_POIN
                                                               (pCrlspTnlInfo)),
                                              CRLSP_INS_POIN (pCrlspTnlInfo));
            }

            /* Add tunnel to the priority list */
            LdpDiffServAddTnlToPrioList (LCB_OUT_IFINDEX
                                         (CRLSP_LCB ((pCrlspTnlInfo))),
                                         CRLSP_HOLD_PRIO (pCrlspTnlInfo),
                                         pCrlspTnlInfo);
        }
        else
        {
            /* Resources were available add it to the priority list */
            LdpDiffServAddTnlToPrioList (LCB_OUT_IFINDEX
                                         (CRLSP_LCB ((pCrlspTnlInfo))),
                                         CRLSP_HOLD_PRIO (pCrlspTnlInfo),
                                         pCrlspTnlInfo);
        }
    }

    /* Lsr is Intermediate to the Cr-Lsp being estblished.
     * Forward Cr-Lsp Label Req Msg DownStream and set LCB 
     * state to ResponseAwait.
     */
    LdpSendCrlspLblReqMsg (pLspCtrlBlock);
    LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_RSP_AWT;
    return;
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpdsprc.c                              */
/*---------------------------------------------------------------------------*/
