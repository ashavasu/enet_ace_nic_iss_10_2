
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpdsmn.c,v 1.6 2013/06/26 11:30:20 siva Exp $
 *
 ********************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpdsmn.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : CRLDP-DIFFSERV 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains functions that are
 *                             associated with ingess of CRLDP-DIFFSERV.
 *----------------------------------------------------------------------------*/

#include "ldpincs.h"
/******************************************************************************
* Function Name : LdpDiffServHandleSigPhbs                                 
* Description   : This routine is called to check whether the PHBs are supported*                 by the resource manager or not. 
* Input(s)      : tTeTnlInfo - Pointer to the tTeTnlInfo                 
*               : u2IncarnId - IncarnId       
* Output(s)     : None                                                     
* Return(s)     : LDP_DS_SUCCESS or LDP_DS_FAILURE                     
*******************************************************************************/
UINT1
LdpDiffServHandleSigPhbs (tTeTnlInfo * pTeTnlInfo, UINT4 u4OutIfIndex)
{
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_TE_MPLS_DIFFSERV_TNL_INFO (pTeTnlInfo);
    UINT1               u1IsSupported;
    UINT1               u1Count;
    UINT1               u1Dscp;
    tMplsDiffServElspInfo *pElspInfoNode = NULL;
    tTMO_SLL           *pElspInfoList = NULL;

    pElspInfoList = (LDP_TE_DS_ELSPINFOLIST
                     (LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServTnlInfo)));

    if (pElspInfoList == NULL)
    {
        return LDP_FAILURE;
    }

    TMO_SLL_Scan (pElspInfoList, pElspInfoNode, tMplsDiffServElspInfo *)
    {
        u1Dscp = LDP_TE_DS_ELSPINFO_PHB_DSCP (pElspInfoNode);

        /* Check that the PHB is supported by the manager */

        u1IsSupported = 0;

        LDP_DS_IS_PHB_SUPPORTED (u1Dscp, u1IsSupported, u1Count, u4OutIfIndex)
            if (u1IsSupported != LDP_DS_TRUE)
        {
            LDP_DBG (LDP_DS_SNMP, "Error: UNSUPPORTED PHB. No Lsp estd\n");
            return LDP_DS_FAILURE;
        }
    }
    return LDP_DS_SUCCESS;
}

/******************************************************************************
* Function Name : LdpDiffServHandleDSIngressNode                           
* Description   : This routine is called to handle the DiffServ related 
*                 actions in the ingress node.
* Input(s)      : tLspCtrlBlock - Pointer to the tLspCtrlBlock                 
*               : u2IncarnId - IncarnId       
* Output(s)     : None                                                     
* Return(s)     : LDP_DS_SUCCESS or LDP_DS_FAILURE                     
*******************************************************************************/
UINT1
LdpDiffServHandleDSIngressNode (tLspCtrlBlock * pLspCtrlBlock, UINT2 u2IncarnId)
{
    UINT4               u4OutIfIndex;
    tCrlspTnlInfo      *pCrlspTnlInfo = LCB_TNLINFO (pLspCtrlBlock);
    tTeTnlInfo         *pTeTnlInfo = CRLSP_TE_TNL_INFO (pCrlspTnlInfo);
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_TE_MPLS_DIFFSERV_TNL_INFO (pTeTnlInfo);
    UINT1               u1IsSupported = 0;
    UINT1               u1Count = 0;
    UINT1               u1Dscp;

    u4OutIfIndex = LCB_OUT_IFINDEX (pLspCtrlBlock);

    u1Dscp = LDP_TE_DS_LLSP_DSCP (pDiffServTnlInfo);

    if ((LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) == LDP_DS_ELSP) &&
        (LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) == LDP_DS_SIG_ELSP))
    {
        if (LdpDiffServHandleSigPhbs (pTeTnlInfo, u4OutIfIndex) ==
            LDP_DS_FAILURE)
        {
            return LDP_DS_FAILURE;
        }
    }

    if (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) == LDP_DS_LLSP)
    {
        /* Check if the PSC is supported by Resource manager or not */
        LDP_DS_IS_PSC_SUPPORTED (u1Dscp, u1IsSupported, u1Count,
                                 u4OutIfIndex) if (u1IsSupported != LDP_DS_TRUE)
        {
            LDP_DBG (LDP_DS_SNMP, "Error: UNSUPPORTED PSC. No Lsp estd\n");
            return LDP_DS_FAILURE;
        }
    }

    /* Check if the flags are set for PER OA Resource */

    if ((CRLSP_PARM_FLAG (pCrlspTnlInfo) & LDP_RSRC_PEROA_CRLSP) ==
        LDP_RSRC_PEROA_CRLSP)
    {
        if (LdpDiffServHandlePerOATrfcParams (pCrlspTnlInfo, u2IncarnId)
            == LDP_DS_FAILURE)
        {
            return LDP_DS_FAILURE;
        }
    }

    if (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) == LDP_DS_LLSP)
    {
        if (LdpDiffServTrfcRsrcForLLsp (pLspCtrlBlock, NULL) != LDP_DS_SUCCESS)
        {
            return LDP_DS_FAILURE;
        }
    }
    /* Check the classType conditions */

    if ((CRLSP_PARM_FLAG (pCrlspTnlInfo) & LDP_CLASSTYPE_CRLSP) ==
        LDP_CLASSTYPE_CRLSP)
    {
        if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo) !=
            LDP_DS_CLASSTYPE_RESOURCES)
        {
            LDP_DBG (LDP_DS_SNMP,
                     "Error: ClassType Resouce Mgmt. not supported. "
                     "No Lsp estd\n");
            return LDP_DS_FAILURE;

        }
        if (LDP_TE_DS_CLASS_TYPE (pDiffServTnlInfo) > LDP_DS_MAX_CLASS_TYPE)
        {
            return LDP_DS_FAILURE;
        }

        LDP_DS_IS_CLASSTYPE_SUPPORTED (LDP_TE_DS_CLASS_TYPE (pDiffServTnlInfo),
                                       u1IsSupported, u1Count,
                                       u4OutIfIndex)
            if (u1IsSupported == LDP_DS_FALSE)
        {
            LDP_DBG (LDP_DS_SNMP,
                     "Error: UNSUPPORTED CLASSTYPE. No Lsp estd\n");
            return LDP_DS_FAILURE;
        }
    }
    return LDP_DS_SUCCESS;
}

/******************************************************************************
* Function Name : LdpDiffServHandlePerOATrfcParams
*                                 
* Description   : This routine is called to handle the PerOA traffic params 
*                 actions in the ingress node.
*
* Input(s)      : tCrlspTnlInfo - Pointer to the tCrlspTnlInfo                 
*               : u2IncarnId - IncarnId       
* Output(s)     : None                                                     
* Return(s)     : LDP_DS_SUCCESS or LDP_DS_FAILURE                     
*******************************************************************************/
UINT1
LdpDiffServHandlePerOATrfcParams (tCrlspTnlInfo * pCrlspTnlInfo,
                                  UINT2 u2IncarnId)
{
    tTeTnlInfo         *pTeTnlInfo = CRLSP_TE_TNL_INFO (pCrlspTnlInfo);
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_TE_MPLS_DIFFSERV_TNL_INFO (pTeTnlInfo);
    tMplsDiffServElspInfo *pElspInfoNode = NULL;
    tPerOATrfcProfileEntry *pElspTrfcProfileEntry = NULL;
    UINT2               u2PscIndex = 0;
    UINT1               u1PhbPsc;
    UINT1               u1PscFound = LDP_FALSE;
    UINT1               u1PreConfIndex;
    UINT1               u1Dscp;
    UINT1               u1Psc;
    tTMO_SLL           *pElspInfoList = NULL;

    pElspInfoList = (LDP_TE_DS_ELSPINFOLIST
                     (LDP_TE_DS_PARAMS_ELSPLIST_PTR (pDiffServTnlInfo)));

    if ((LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo) == LDP_DS_ELSP) &&
        (LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo) == LDP_DS_PRECONF_ELSP))
    {

        if ((LDP_DS_IS_PRECONF_VALID (u2IncarnId)) != LDP_TRUE)
        {
#ifndef NPAPI_WANTED
            if (ldpMplsGetPreConfMap (u2IncarnId,
                                      LDP_DS_PRECONF_ELSP_MAP (u2IncarnId)) !=
                LDP_SUCCESS)
            {
                LDP_DBG (LDP_DS_MISC,
                         "Error: Unable to Get preconfigured Map from FM. "
                         "No Lsp estd\n");
                return LDP_FAILURE;
            }
            else
            {
                LDP_DS_IS_PRECONF_VALID (u2IncarnId) = LDP_TRUE;
            }
#else

/*TODO:The above is fm call. so For diffserv same functionality should be added */

#endif

        }

        /* Check if the resources are with preconfigured E-LSP , then it must 
         * match the supported PHBS
         * */

        TMO_SLL_Scan (pElspInfoList, pElspInfoNode, tMplsDiffServElspInfo *)
        {
            /* Check in the preconfigured array if the PSC is present */
            u1PscFound = LDP_FALSE;

            u1PhbPsc = LDP_TE_DS_ELSPINFO_PHB_DSCP (pElspInfoNode);

            for (u1PreConfIndex = 0; u1PreConfIndex < MAX_DS_EXP;
                 u1PreConfIndex++)
            {
                u1Dscp =
                    LDP_DS_PRECONF_MAP_PHB_DSCP (u2IncarnId, u1PreConfIndex);

                if (u1PhbPsc == u1Dscp)
                {
                    u1PscFound = LDP_TRUE;
                    break;
                }
            }
            if (u1PscFound == LDP_FALSE)
            {
                LDP_DBG (LDP_DS_MISC, "Error: Unable to Match preconfigured ");
                LDP_DBG (LDP_DS_MISC, "Map with ElspTP. No Lsp estd\n");
                return LDP_FAILURE;
            }
        }
    }

    TMO_SLL_Scan (pElspInfoList, pElspInfoNode, tMplsDiffServElspInfo *)
    {
        if (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pElspInfoNode) != NULL)
        {
            if (TE_CRLDP_TRFC_PARAMS (LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS
                                      (pElspInfoNode)) == NULL)
            {
                LDP_DBG (LDP_DS_MISC, "Error: No CRLDP Trfc Params found\n");
                return LDP_FAILURE;
            }

            /* Allocate memory for tPerOATrfcProfileEntry from the 
             * memory pool */

            pElspTrfcProfileEntry = (tPerOATrfcProfileEntry *)
                MemAllocMemBlk (LDP_DS_ELSP_TRFC_POOL_ID);

            if (pElspTrfcProfileEntry == NULL)
            {
                LDP_DBG (LDP_DS_MEM,
                         "Error: LDP_DS_ELSP_TRFC_POOL_ID "
                         "Allocation Failure\n");
                return LDP_FAILURE;
            }
            /* Set the memory to zero */
            MEMSET (pElspTrfcProfileEntry, LDP_DS_ZERO,
                    sizeof (tPerOATrfcProfileEntry));

            u1Dscp = LDP_TE_DS_ELSPINFO_PHB_DSCP (pElspInfoNode);

            /* Get Index for Dscp */
            u1Psc = ldpTeDiffServGetPhbPsc (u1Dscp);

            LdpDiffServGetIndexForPsc (u1Psc, &u2PscIndex);

            LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW (pCrlspTnlInfo,
                                                    u2PscIndex) =
                pElspTrfcProfileEntry;

            LDP_DS_PEROATRFC_PARAMS (pCrlspTnlInfo, u2PscIndex) =
                LDP_TE_DS_ELSPINFO_PSC_TRFC_PARAMS (pElspInfoNode);

            /* Enter the Dscp value */

            LDP_DS_PEROA_TRFC_PROFILE_PSC (pCrlspTnlInfo, u2PscIndex) = u1Dscp;
            LDP_DS_GET_PER_OA_RSRC_AVAIL (pCrlspTnlInfo, u2PscIndex) =
                LDP_DS_FALSE;
        }
    }
    return LDP_DS_SUCCESS;
}

/******************************************************************************
* Function Name : LdpDiffServHandleRMPerOARsrc
* Description   : This routine is called to handle the resource 
*                 management of PerOA traffic params at the ingress node.
* Input(s)      : tLspCtrlBlock - Pointer to the tLspCtrlBlock                 
* Output(s)     : None                                                     
* Return(s)     : LDP_DS_SUCCESS or LDP_DS_FAILURE                     
*******************************************************************************/
UINT1
LdpDiffServHandleRMPerOARsrc (tLspCtrlBlock * pLspCtrlBlock)
{
    tCrlspTnlInfo      *pCrlspTnlInfo = pLspCtrlBlock->pCrlspTnlInfo;
    tTMO_SLL            pPreEmpList;
    tTMO_SLL           *pCandidatePreemp = (tTMO_SLL *) & pPreEmpList;
    UINT1               u1Event;

    if (LdpDiffServTcResvResources (pCrlspTnlInfo) != LDP_DS_SUCCESS)
    {
        /* Try to get resources by preemption */
        if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
        {
            LdpDiffServModifyTnlPrioList (LCB_OUT_IFINDEX
                                          (CRLSP_LCB
                                           (CRLSP_INS_POIN (pCrlspTnlInfo))),
                                          CRLSP_HOLD_PRIO (CRLSP_INS_POIN
                                                           (pCrlspTnlInfo)),
                                          LCB_OUT_IFINDEX (CRLSP_LCB
                                                           ((pCrlspTnlInfo))),
                                          CRLSP_SET_PRIO ((pCrlspTnlInfo)),
                                          CRLSP_INS_POIN (pCrlspTnlInfo));
        }

        /* PreEmption Module is called for bumping if any lower
         * priority tunnels,with which the resources could be met. 
         * Only the Modified set of resources will be looked for
         * if it's a crlsp modify.
         */
        if (LdpDiffServCrlspGetTnlFromPrioList
            (pCrlspTnlInfo, pCandidatePreemp) != LDP_DS_SUCCESS)
        {
            /*  Failed to get resources even by preemption. */

            if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
            {
                LdpDiffServModifyTnlPrioList (LCB_OUT_IFINDEX
                                              (CRLSP_LCB ((pCrlspTnlInfo))),
                                              CRLSP_SET_PRIO ((pCrlspTnlInfo)),
                                              LCB_OUT_IFINDEX
                                              (CRLSP_LCB
                                               (CRLSP_INS_POIN
                                                (pCrlspTnlInfo))),
                                              CRLSP_HOLD_PRIO (CRLSP_INS_POIN
                                                               (pCrlspTnlInfo)),
                                              CRLSP_INS_POIN (pCrlspTnlInfo));
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return LDP_DS_FAILURE;
        }

        /* Preemption is success */
        LdpDiffServPreEmptTunnels (pCandidatePreemp);

        /* Now the Resources will be available for reservation */
        LdpDiffServTcResvResources (pCrlspTnlInfo);

        if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
        {
            LdpDiffServModifyTnlPrioList (LCB_OUT_IFINDEX
                                          (CRLSP_LCB ((pCrlspTnlInfo))),
                                          CRLSP_SET_PRIO ((pCrlspTnlInfo)),
                                          LCB_OUT_IFINDEX (CRLSP_LCB
                                                           (CRLSP_INS_POIN
                                                            (pCrlspTnlInfo))),
                                          CRLSP_HOLD_PRIO (CRLSP_INS_POIN
                                                           (pCrlspTnlInfo)),
                                          CRLSP_INS_POIN (pCrlspTnlInfo));

            /* Add tunnel to the priority list */
            LdpDiffServAddTnlToPrioList (LCB_OUT_IFINDEX
                                         (CRLSP_LCB ((pCrlspTnlInfo))),
                                         CRLSP_HOLD_PRIO (pCrlspTnlInfo),
                                         pCrlspTnlInfo);
        }
    }
    else
    {
        /* Resources were available add it to the priority list */
        LdpDiffServAddTnlToPrioList (LCB_OUT_IFINDEX
                                     (CRLSP_LCB ((pCrlspTnlInfo))),
                                     CRLSP_HOLD_PRIO (pCrlspTnlInfo),
                                     pCrlspTnlInfo);

    }
    /* Invoke Advert State M/c for fwding, Cr-Lsp Label Request Msg */
    u1Event = LDP_LSM_EVT_INT_SETUP;
    LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlock)][u1Event] (pLspCtrlBlock, NULL);
    return LDP_DS_SUCCESS;
}

/******************************************************************************
* Function Name : LdpDiffServGetIndexForPsc                                 
* Description   : This routine is called to get the index for the PSC.
* Input(s)      : u1PhbDscp - Dscp value for which the index is needed
* Output(s)     : None                                                     
* Return(s)     : Index of the Dscp value                     
*******************************************************************************/
UINT1
LdpDiffServGetPhbPsc (UINT1 u1PhbDscp)
{
    switch (u1PhbDscp)
    {
        case LDP_DS_AF11_DSCP:
        case LDP_DS_AF12_DSCP:
        case LDP_DS_AF13_DSCP:
            return LDP_DS_AF11_DSCP;
        case LDP_DS_AF21_DSCP:
        case LDP_DS_AF22_DSCP:
        case LDP_DS_AF23_DSCP:
            return LDP_DS_AF21_DSCP;
        case LDP_DS_AF31_DSCP:
        case LDP_DS_AF32_DSCP:
        case LDP_DS_AF33_DSCP:
            return LDP_DS_AF31_DSCP;
        case LDP_DS_AF41_DSCP:
        case LDP_DS_AF42_DSCP:
        case LDP_DS_AF43_DSCP:
            return LDP_DS_AF41_DSCP;
        default:
            return u1PhbDscp;
    }
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpdsmn.c                               */
/*---------------------------------------------------------------------------*/
