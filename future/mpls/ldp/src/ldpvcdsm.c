/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ldpvcdsm.c,v 1.13 2014/11/08 11:40:55 siva Exp $
*
**********************************************************************/
/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*-----------------------------------------------------------------------------
*    FILE  NAME             : ldpvcdsm.c
*    PRINCIPAL AUTHOR       : Aricent Inc. 
*    SUBSYSTEM NAME         : MPLS   
*    MODULE NAME            : LDP (ADVT SUB-MODULE)
*    LANGUAGE               : ANSI-C
*    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
*    DATE OF FIRST RELEASE  :
*    DESCRIPTION            : This file contains VC Merge Downstream state
*                             machine routines.
*----------------------------------------------------------------------------*/
#include "ldpincs.h"
#include "ldpgblex.h"
#include "ldpvcdsm.h"

/*****************************************************************************/
/* Function Name : LdpMergeDnSm                                              */
/* Description   : This Function is called by other internal modules to      */
/*                 perform the corresponding operations required by the      */
/*                 VC merge downstream state machine                         */
/* Input(s)      : pLspCtrlBlock  - Pointer to the LSP ctrl block structure  */
/*                 pMsg           - Pointer to Message strcuture             */
/*                 u1Event        - The event to be passed to state machine  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMergeDnSm (tLspCtrlBlock * pLspCtrlBlock, UINT1 u1Event, tLdpMsgInfo * pMsg)
{
    if (pLspCtrlBlock == NULL)
    {
        return;
    }

    LDP_DBG2 (LDP_ADVT_SEM,
              "Downstream Control Block Current State: %s Evt %s \n",
              au1LdpVcDnStates[pLspCtrlBlock->u1LspState],
              au1LdpVcDnEvents[u1Event]);

    gLdpMrgDnFsm[LCB_STATE (pLspCtrlBlock)][u1Event] (pLspCtrlBlock, pMsg);

    LDP_DBG1 (LDP_ADVT_SEM,
              "Downstream Control Block Next  State: %s \n",
              au1LdpVcDnStates[pLspCtrlBlock->u1LspState]);
    return;
}

/*****************************************************************************/
/* Function Name :  LdpCreateDnstrCtrlBlock                                  */
/* Description   :  This function creates a downstream control block and     */
/*                  initializes it as required                               */
/* Input(s)      : None                                                      */
/* Output(s)     : ppDnstrCtrlBlock - Pointer to the Adress DownStream ctrl  */
/*                 block                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpCreateDnstrCtrlBlock (tLspCtrlBlock ** ppDnstrCtrlBlock)
{
    *ppDnstrCtrlBlock = (tLspCtrlBlock *) MemAllocMemBlk (LDP_LSP_POOL_ID);
    if (*ppDnstrCtrlBlock == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: MemAlloc Failed for LCB\n");
        return LDP_FAILURE;
    }
    /* This function should initialize the upstream LSP control block
     * to contain the newly added upstream pointer */
    LdpInitLspCtrlBlock (*ppDnstrCtrlBlock);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpMrgDnIdlIntAddUpstr                                    */
/* Description   : This function performs the actions required by the        */
/*                 downstream state machine in the IDLE state and an internal*/
/*                 Add upstream event is received.                           */
/* Input(s)      : pUpstrCtrlBlock - Pointer to the upStream ctrl block      */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnIdlIntAddUpstr (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;

    pUpstrCtrlBlock = LDP_MSG_CTRL_BLK_PTR (pMsg);

    TMO_SLL_Add ((tTMO_SLL *) (&(LCB_UPSTR_LIST (pDnstrCtrlBlock))),
                 (tTMO_SLL_NODE *) (VOID *) UPCB_TO_SLL (pUpstrCtrlBlock));

    /* Path Vector should not be sent in lbl req mesg in merge cases */
    LdpSendLblReqMsg (pDnstrCtrlBlock, LDP_MSG_HC (pMsg), NULL, 0, NULL, 0);
    /* The LSP -State machine state moves to RESPONSE AWAIT State */
    LCB_STATE (pDnstrCtrlBlock) = LDP_LSM_ST_RSP_AWT;

    /* The down stream control block is added into the session Dn list */
    TMO_SLL_Add ((tTMO_SLL *) (&(SSN_DLCB (LCB_DSSN (pDnstrCtrlBlock)))),
                 (tTMO_SLL_NODE *) (&(pDnstrCtrlBlock->NextDSLspCtrlBlk)));
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgDnRspAwtIntAddUpstr                                 */
/* Description   : This function performs the actions required by the        */
/*                 downstream state machine in the RESP-AWAITED state and an */
/*                 internal Add upstream event is received.                  */
/* Input(s)      : pLspCtrlBlock  -  Pointer to the LSP control block struct */
/*                 pMsg            - Pointer to Message strcuture            */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgDnRspAwtIntAddUpstr (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;

    pUpstrCtrlBlock = LDP_MSG_CTRL_BLK_PTR (pMsg);

    /* The node is added to the upstream control block list 
     * no further processing is required in the response awaited state */

    TMO_SLL_Add ((tTMO_SLL *) (&(LCB_UPSTR_LIST (pDnstrCtrlBlock))),
                 (tTMO_SLL_NODE *) UPCB_TO_SLL (pUpstrCtrlBlock));
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgDnRspAwtIntDelUpstr                                 */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in RESP-AWAITED state and an     */
/*                 Internal Delete upstream event is received.               */
/* Input(s)      : pLspCtrlBlock  -  Pointer to the LSP control block struct */
/*                 pMsg            - Pointer to Message strcuture            */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnRspAwtIntDelUpstr (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;

    pUpstrCtrlBlock = LDP_MSG_CTRL_BLK_PTR (pMsg);

    TMO_SLL_Delete ((tTMO_SLL *) (&(LCB_UPSTR_LIST (pDnstrCtrlBlock))),
                    UPCB_TO_SLL (pUpstrCtrlBlock));

    if (TMO_SLL_Count ((tTMO_SLL *) (&(LCB_UPSTR_LIST (pDnstrCtrlBlock)))) == 0)
    {
        LdpSendLblAbortReqMsg (pDnstrCtrlBlock);
        LdpDeleteLspCtrlBlock (pDnstrCtrlBlock);
    }
    return;
}

/*****************************************************************************/
/* Function Name :  LdpMrgDnRspAwtLblMap                                     */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in RESP-AWAITED state and an     */
/*                 Label mapping event is receieved                          */
/*                                                                           */
/* Input(s)      : pUpstrCtrlBlock - Pointer to the upStream ctrl block      */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnRspAwtLblMap (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    UINT2               u2PvTlvLen = 0;
    UINT1               u1HopCount = 0;
    UINT1               u1LsrEgrOrIngr = LDP_FALSE;
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;
    tTMO_SLL           *pUpstCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tGenU4Addr         NextHopAddr;

    MEMSET(&NextHopAddr,LDP_ZERO,sizeof(tGenU4Addr));

    pUpstCtrlBlkList = &LCB_UPSTR_LIST (pDnstrCtrlBlock);
    /* u1LsrEgrOrIngr is always FALSE in th down stream ctrl blk case */

    if (LdpIsLoopFound (LDP_MSG_HC_PTR (pMsg), LDP_MSG_PV_PTR (pMsg),
                        (&u2PvTlvLen), (&u1HopCount),
                        SSN_GET_INCRN_ID (LCB_DSSN (pDnstrCtrlBlock)),
                        LCB_DSSN (pDnstrCtrlBlock), u1LsrEgrOrIngr) == LDP_TRUE)
    {
        /* Label release message has to be sent with Status TLV (Loop detected status
         * code)
         * */
        pDnstrCtrlBlock->pDStrSession->u1StatusCode =
            LDP_STAT_TYPE_LOOP_DETECTED;

        /* Case of LSR being Ingress to the LSP */
        LdpSendLblRelMsg (&LCB_FEC (pDnstrCtrlBlock),
                          &LCB_DLBL (pDnstrCtrlBlock),
                          LCB_DSSN (pDnstrCtrlBlock));

        TMO_DYN_SLL_Scan (pUpstCtrlBlkList, pSllNode, pTempSllNode,
                          tTMO_SLL_NODE *)
        {
            pUpstrCtrlBlock = SLL_TO_UPCB (pSllNode);

            if (UPSTR_USSN (pUpstrCtrlBlock) != NULL)
            {
                LdpMergeUpSm (pUpstrCtrlBlock, LDP_USM_EVT_DSTR_NAK, pMsg);
            }
            else
            {
                LdpDeleteUpstrCtrlBlock (pUpstrCtrlBlock);
            }
        }
        return;
    }

    /* The hop count is updated */
    LCB_HCOUNT (pDnstrCtrlBlock) = u1HopCount;
    LDP_MSG_HC (pMsg) = u1HopCount;

    pDnstrCtrlBlock->pu1PVTlv = LDP_MSG_PV_PTR (pMsg);
    pDnstrCtrlBlock->u1PVCount = u2PvTlvLen;

    TMO_DYN_SLL_Scan (pUpstCtrlBlkList, pSllNode, pTempSllNode, tTMO_SLL_NODE *)
    {
        pUpstrCtrlBlock = SLL_TO_UPCB (pSllNode);

        /* Call the Upsream Control Block Function with internal 
         * downstream Map event with the appropriate control Block 
         * with the RESPONSE_AWAITED state */
        if (UPSTR_USSN (pUpstrCtrlBlock) != NULL)
        {
            if (UPSTR_NHOP (pUpstrCtrlBlock) != NULL)
            {
                LdpVcMergeNhopSm (UPSTR_NHOP (pUpstrCtrlBlock),
                                  LDP_NH_LSM_EVT_INT_LSP_UP, (UINT1 *) pMsg);
            }
            else
            {
                LdpMergeUpSm (pUpstrCtrlBlock, LDP_USM_EVT_INT_DSTR_MAP, pMsg);
            }
        }
        else
        {
            if ((SSN_GET_ENTITY (LCB_DSSN (pDnstrCtrlBlock))->u2LabelRet)
                == LDP_LIBERAL_MODE)
            {
                LDP_IPV4_U4_ADDR(NextHopAddr.Addr)=LDP_IPV4_U4_ADDR(pDnstrCtrlBlock->NextHopAddr);
                NextHopAddr.u2AddrType=LDP_ADDR_TYPE_IPV4;
                if ((LdpIsSsnForFec (&NextHopAddr,
                                     pDnstrCtrlBlock->u4OutIfIndex,
                                     LCB_DSSN (pDnstrCtrlBlock))) == LDP_TRUE)
                {
                    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE,
                                           MPLS_OPR_PUSH, pDnstrCtrlBlock,
                                           NULL);
                }
            }
            else
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE,
                                       MPLS_OPR_PUSH, pDnstrCtrlBlock, NULL);
            }
            if (UPSTR_NHOP (pUpstrCtrlBlock) != NULL)
            {
                LdpVcMergeNhopSm (UPSTR_NHOP (pUpstrCtrlBlock),
                                  LDP_NH_LSM_EVT_INT_LSP_UP, (UINT1 *) pMsg);
            }

            UPSTR_STATE (pUpstrCtrlBlock) = LDP_LSM_ST_EST;
        }

    }

    LCB_STATE (pDnstrCtrlBlock) = LDP_LSM_ST_EST;
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgDnRspAwtLblWdraw                                    */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in RESP-AWAITED state and an     */
/*                 Label withdraw event is receieved                         */
/*                                                                           */
/* Input(s)      : pDnstrCtrlBlock - Pointer to the downstream ctrl block    */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnRspAwtLblWdraw (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    /* Sending Label Release Msg DnStream */
    LDP_SUPPRESS_WARNING (pMsg);

    LdpSendLblRelMsg (&LCB_FEC (pDnstrCtrlBlock), NULL,
                      LCB_DSSN (pDnstrCtrlBlock));
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgDnRspAwtIntDstrNak                                  */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in RESP-AWAITED state and an     */
/*                 Internal Downstream NAK event is receieved                */
/* Input(s)      : pDnstrCtrlBlock - Pointer to the downstream ctrl block    */
/*                 pMsg            - Pointer to Message strcuture            */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnRspAwtIntDstrNak (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;
    tTMO_SLL           *pUpstCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;

    pUpstCtrlBlkList = &LCB_UPSTR_LIST (pDnstrCtrlBlock);

    TMO_DYN_SLL_Scan (pUpstCtrlBlkList, pSllNode, pTempSllNode, tTMO_SLL_NODE *)
    {
        pUpstrCtrlBlock = SLL_TO_UPCB (pSllNode);

        /* Call the Upsream Control Block Function with internal 
         * downstream NAK event with the appropriate control Block 
         * with the RESPONSE_AWAITED state */

        if (UPSTR_NHOP (pUpstrCtrlBlock) != NULL)
        {
            LdpVcMergeNhopSm (UPSTR_NHOP (pUpstrCtrlBlock),
                              LDP_NH_LSM_EVT_INT_LSP_NAK, (UINT1 *) pMsg);
        }
        else if (UPSTR_USSN (pUpstrCtrlBlock) != NULL)
        {
            LdpMergeUpSm (pUpstrCtrlBlock, LDP_USM_EVT_DSTR_NAK, pMsg);
        }
        else
        {
            LdpDeleteUpstrCtrlBlock (pUpstrCtrlBlock);
        }
    }

    /* deletion of the LSP control block would happen inside the
     * scan for the last node in the upstr list */
    return;

}

/*****************************************************************************/
/* Function Name :  LdpMrgDnRspAwtDstrLost                                   */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in RESP-AWAITED state and an     */
/*                 Internal Downstream Lost event is receieved               */
/* Input(s)      : pDnstrCtrlBlock - Pointer to the downstream ctrl block    */
/*                 pMsg            - Pointer to Message strcuture            */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnRspAwtDstrLost (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;
    tTMO_SLL           *pUpstCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;

    pUpstCtrlBlkList = &LCB_UPSTR_LIST (pDnstrCtrlBlock);

    TMO_DYN_SLL_Scan (pUpstCtrlBlkList, pSllNode, pTempSllNode, tTMO_SLL_NODE *)
    {
        pUpstrCtrlBlock = SLL_TO_UPCB (pSllNode);

        /* Call the Upsream Control Block Function with internal 
         * downstream NAK event with the appropriate control Block 
         * with the RESPONSE_AWAITED state */
        if (UPSTR_USSN (pUpstrCtrlBlock) != NULL)
        {
            LdpMergeUpSm (pUpstrCtrlBlock, LDP_USM_EVT_DSTR_NAK, pMsg);
        }
        else
        {
            LdpDeleteUpstrCtrlBlock (pUpstrCtrlBlock);
        }
    }

    /* deletion of the LSP control block would happen inside the
     * scan for the last node in the upstr list */
    return;

}

/*****************************************************************************/
/* Function Name :  LdpMrgDnEstIntAddUpstr                                   */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in ESTABLISHED  state and an     */
/*                 Internal Adds upstream   event is receieved               */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock  - Pointer to the LSP ctrl block structure  */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnEstIntAddUpstr (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;

    pUpstrCtrlBlock = LDP_MSG_CTRL_BLK_PTR (pMsg);

    TMO_SLL_Add ((tTMO_SLL *) (&(LCB_UPSTR_LIST (pDnstrCtrlBlock))),
                 (tTMO_SLL_NODE *) UPCB_TO_SLL (pUpstrCtrlBlock));
    return;

}

/*****************************************************************************/
/* Function Name : LdpMrgDnEstIntDelUpstr                                    */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in ESTABLISHED  state and an     */
/*                 Internal Del  upstream   event is receieved               */
/* Input(s)      : pLspCtrlBlock  - Pointer to the LSP ctrl block structure  */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnEstIntDelUpstr (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;
    tTMO_SLL           *pUpstCtrlBlkList = NULL;

    pUpstrCtrlBlock = LDP_MSG_CTRL_BLK_PTR (pMsg);
    pUpstCtrlBlkList = &LCB_UPSTR_LIST (pDnstrCtrlBlock);

    TMO_SLL_Delete (pUpstCtrlBlkList, UPCB_TO_SLL (pUpstrCtrlBlock));

    if (TMO_SLL_Count (pUpstCtrlBlkList) == 0)
    {
        LdpSendLblRelMsg (&LCB_FEC (pDnstrCtrlBlock),
                          &LCB_DLBL (pDnstrCtrlBlock),
                          LCB_DSSN (pDnstrCtrlBlock));

        LdpDeleteLspCtrlBlock (pDnstrCtrlBlock);
    }
    return;
}

/*****************************************************************************/
/* Function Name :  LdpMrgDnEstLblMap                                        */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in ESTABLISHED  state and an     */
/*                 Label Mapping event is received                           */
/* Input(s)      : pLspCtrlBlock  - Pointer to the LSP ctrl block structure  */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnEstLblMap (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;
    tTMO_SLL           *pUpstCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tGenU4Addr         NextHopAddr;

    MEMSET(&NextHopAddr,LDP_ZERO,sizeof(tGenU4Addr));

    pUpstCtrlBlkList = &LCB_UPSTR_LIST (pDnstrCtrlBlock);

    /* The label has to be updated into the DownStream Control Block  
     *  verification of the message ID has to be done for identifying 
     *  the appropriate control block, The comparison of the the labels
     *  is also done aprior */

    /* since the path vectors are not stored in control block
     * verification for change cannot be done */
    if ((LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID (LCB_DSSN (pDnstrCtrlBlock)))
         == LDP_LOOP_DETECT_CAPABLE_HCPV) ||
        (LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID (LCB_DSSN (pDnstrCtrlBlock)))
         == LDP_LOOP_DETECT_CAPABLE_PV))
    {
        if (LdpCheckMapAttributes (NULL, 0, pDnstrCtrlBlock, pMsg) == LDP_TRUE)
        {
            LCB_HCOUNT (pDnstrCtrlBlock) = LDP_MSG_HC (pMsg);

            pDnstrCtrlBlock->pu1PVTlv = LDP_MSG_PV_PTR (pMsg);
            pDnstrCtrlBlock->u1PVCount = LDP_MSG_PV_COUNT (pMsg);

            TMO_DYN_SLL_Scan (pUpstCtrlBlkList, pSllNode, pTempSllNode,
                              tTMO_SLL_NODE *)
            {

                pUpstrCtrlBlock = SLL_TO_UPCB (pSllNode);

                /* Call the Upsream Control Block Function with internal 
                 * downstream Map event with the appropriate control Block 
                 * in the ESTABLISHED state */
                if (UPSTR_USSN (pUpstrCtrlBlock) != NULL)
                {

                    if (UPSTR_NHOP (pUpstrCtrlBlock) != NULL)
                    {
                        LdpVcMergeNhopSm (UPSTR_NHOP (pUpstrCtrlBlock),
                                          LDP_NH_LSM_EVT_INT_LSP_UP,
                                          (UINT1 *) pMsg);
                    }
                    else
                    {

                        LdpMergeUpSm (pUpstrCtrlBlock, LDP_USM_EVT_INT_DSTR_MAP,
                                      pMsg);
                    }
                }
                else
                {
                    if ((SSN_GET_ENTITY (LCB_DSSN (pDnstrCtrlBlock))->
                         u2LabelRet) == LDP_LIBERAL_MODE)
                    {
                        LDP_IPV4_U4_ADDR(NextHopAddr.Addr)=LDP_IPV4_U4_ADDR(pDnstrCtrlBlock->NextHopAddr);
                        NextHopAddr.u2AddrType=LDP_ADDR_TYPE_IPV4;
                        if ((LdpIsSsnForFec (&NextHopAddr,
                                             pDnstrCtrlBlock->u4OutIfIndex,
                                             LCB_DSSN (pDnstrCtrlBlock)))
                            == LDP_TRUE)
                        {
                            LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_MODIFY,
                                                   MPLS_OPR_PUSH,
                                                   pDnstrCtrlBlock, NULL);
                        }
                    }
                    else
                    {
                        LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_MODIFY,
                                               MPLS_OPR_PUSH, pDnstrCtrlBlock,
                                               NULL);
                    }
                }
            }
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgDnEstLblWdraw                                       */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in ESTABLISHED  state and an     */
/*                 Label WithDraw event is received                          */
/* Input(s)      : pLspCtrlBlock  - Pointer to the LSP ctrl block structure  */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnEstLblWdraw (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;
    tTMO_SLL           *pUpstCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tGenU4Addr         NextHopAddr;

    LDP_SUPPRESS_WARNING (pMsg);
    
    MEMSET(&NextHopAddr,LDP_ZERO,sizeof(tGenU4Addr));
    pUpstCtrlBlkList = &LCB_UPSTR_LIST (pDnstrCtrlBlock);

    /* Sending Label Release Msg DnStream, and Deleting LCB */
    /* Release Messge will be sent during the del upstream */

    TMO_DYN_SLL_Scan (pUpstCtrlBlkList, pSllNode, pTempSllNode, tTMO_SLL_NODE *)
    {
        pUpstrCtrlBlock = SLL_TO_UPCB (pSllNode);
        /* Call the Upsream Control Block Function with internal 
         * downstream WithDraw event with the appropriate control Block 
         * with the RESPONSE_AWAITED state */
        if (UPSTR_USSN (pUpstrCtrlBlock) != NULL)
        {
            LdpMergeUpSm (pUpstrCtrlBlock, LDP_USM_EVT_DSTR_WDRAW, pMsg);
        }
        else
        {
            if ((SSN_GET_ENTITY (LCB_DSSN (pDnstrCtrlBlock))->u2LabelRet)
                == LDP_LIBERAL_MODE)
            {
                LDP_IPV4_U4_ADDR(NextHopAddr.Addr)=LDP_IPV4_U4_ADDR(pDnstrCtrlBlock->NextHopAddr);
                NextHopAddr.u2AddrType=LDP_ADDR_TYPE_IPV4;

                if ((LdpIsSsnForFec (&NextHopAddr,
                                     pDnstrCtrlBlock->u4OutIfIndex,
                                     LCB_DSSN (pDnstrCtrlBlock))) == LDP_TRUE)
                {
                    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                           MPLS_OPR_PUSH, pDnstrCtrlBlock,
                                           NULL);
                }
                LdpDeleteUpstrCtrlBlock (pUpstrCtrlBlock);
            }
            else
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                       MPLS_OPR_PUSH, pDnstrCtrlBlock, NULL);
                LdpDeleteUpstrCtrlBlock (pUpstrCtrlBlock);
            }
        }
    }

    /* deletion of the LSP control block would happen inside the
     * scan for the last node in the upstr list */

    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgDnEstIntDstrNak                                     */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in ESTABLISHED  state and an     */
/*                 Internal Downstream NAK is received                       */
/* Input(s)      : pLspCtrlBlock  - Pointer to the LSP ctrl block structure  */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnEstIntDstrNak (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    LDP_SUPPRESS_WARNING (pDnstrCtrlBlock);
    LDP_SUPPRESS_WARNING (pMsg);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgDnEstIntDstrLost                                    */
/* Description   : This function performs the operations required when the   */
/*                 downstream control block in ESTABLISHED  state and an     */
/*                 Internal Downstream LOST is received                      */
/* Input(s)      : pLspCtrlBlock  - Pointer to the LSP ctrl block structure  */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnEstIntDstrLost (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;
    tTMO_SLL           *pUpstCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;

    pUpstCtrlBlkList = &LCB_UPSTR_LIST (pDnstrCtrlBlock);

    TMO_DYN_SLL_Scan (pUpstCtrlBlkList, pSllNode, pTempSllNode, tTMO_SLL_NODE *)
    {
        pUpstrCtrlBlock = SLL_TO_UPCB (pSllNode);

        /* Call the Upsream Control Block Function with internal 
         * downstream NAK event with the appropriate control Block 
         * with the RESPONSE_AWAITED state */
        if (UPSTR_USSN (pUpstrCtrlBlock) != NULL)
        {
            LdpMergeUpSm (pUpstrCtrlBlock, LDP_USM_EVT_DSTR_NAK, pMsg);
        }
        else
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                   MPLS_OPR_PUSH, pDnstrCtrlBlock, NULL);

            LdpDeleteUpstrCtrlBlock (pUpstrCtrlBlock);
        }
    }

    /* deletion of the LSP control block would happen inside the
     * scan for the last node in the upstr list */
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgDnInvStateEvt                                       */
/* Description   : This routine is stub routine that's called on an occurence*/
/*                 of an invalid event in a state.                           */
/* Input(s)      : pLspCtrlBlock  - Pointer to the LSP ctrl block structure  */
/*                 pMsg           - Pointer to Message strcuture             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgDnInvStateEvt (tLspCtrlBlock * pDnstrCtrlBlock, tLdpMsgInfo * pMsg)
{
    LDP_SUPPRESS_WARNING (pDnstrCtrlBlock);
    LDP_SUPPRESS_WARNING (pMsg);
    return;
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpvcdsm.c                              */
/*---------------------------------------------------------------------------*/
