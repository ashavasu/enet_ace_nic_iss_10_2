
/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: ldprmtx.c,v 1.1 2014/08/25 12:23:31 siva Exp $
 ***
 *** Description: Files contails HA implementation of ldp 
 *** NOTE: This file should included in release packaging.
 *** ***********************************************************************/


#include "ldpincs.h"

/*******************************************************************/
/*  Function Name   :LdpRmSendILMHwListEntry                       */
/*  Description     :This function will be invoked from            */
/*                   MplsHwListAddOrUpdateILMHwListEntry and       */
/*                   MplsHwListDelILMHwListEntry  in active        */
/*                   node when it gets ILM Hwlist and send this    */
/*                   to standby by calling LdpSendMsgToRm          */
/*  Input(s)        :pILMHwList -The received HwList entry         */
/*                   u1OpType - Add or delete operation            */
/*  Output(s)       :None                                          */
/***************************************************************** */

VOID LdpRmSendILMHwListEntry (tILMHwListEntry *pILMHwList, UINT1 u1OpType)
{

    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = LDP_ZERO;
    UINT2               u2MsgLength = LDP_ZERO;
    UINT2               u2RmMsgLength = LDP_ZERO;

    /* Check If the node is active if not return */
    if (LDP_IS_NODE_ACTIVE () != LDP_RM_TRUE)
    {
        LDP_DBG(HA_DEBUG,"Node is Standby\n");
        return;
    }
    /* Check If istandby exist if not return */
    if (LDP_IS_STANDBY_UP () == LDP_RM_FALSE)
    {
        LDP_DBG(HA_DEBUG,"No Standby is existing\n");
        return;
    }
    if (NULL == pILMHwList)
    {
        LDP_DBG(HA_DEBUG,"No Entry in HW List\n");
        return;
    }

    /*Typcasting each to avoid Coverity Warning*/
    u2MsgLength = (UINT2)sizeof(tILMHwListEntry);
    u2RmMsgLength = (UINT2)((UINT2)LDP_RM_MSG_HDR_SIZE + u2MsgLength +(UINT2) sizeof (UINT1));

    LDP_DBG1(HA_DEBUG,"Total Msg Length :%d",u2RmMsgLength);
    pMsg = CRU_BUF_Allocate_MsgBufChain(u2RmMsgLength, LDP_ZERO);

    if(pMsg == NULL)
    {
        LDP_DBG(HA_DEBUG,"Memory Allocation Failed\n");
        return;
    }
    /* Fill the Message Type */
    RM_DATA_ASSIGN_1_BYTE(pMsg, u4Offset, LDP_RED_ILM_HW_LIST_SYNC_MSG);
    u4Offset = u4Offset + sizeof (UINT1);
    /* Fill the Message  Length */
    RM_DATA_ASSIGN_2_BYTE(pMsg, u4Offset, u2RmMsgLength);
    u4Offset = u4Offset + sizeof (UINT2);
    /*Fill RBTree ADD/DELETE type */
    RM_DATA_ASSIGN_1_BYTE(pMsg, u4Offset, u1OpType);
    u4Offset = u4Offset + sizeof (UINT1);

    BUF_COPY_OVER_CHAIN(pMsg,pILMHwList, u4Offset, sizeof(tILMHwListEntry));
    u4Offset = u4Offset + sizeof(tILMHwListEntry);
    if (LdpRmSendMsgToRm(pMsg, u2RmMsgLength) != LDP_SUCCESS )
    {
        LDP_DBG(HA_DEBUG,"Sending Message to RM is failed\n");
        return;
    }
    return;
}

/***************************************************************/
/*  Function Name   :LdpRmSendFTNHwListEntry                   */
/*  Description     :This function will be invoked from        */
/*                   MplsHwListAddOrUpdateFTNHwListEntry       */
/*                   and MplsHwListDelFTNHwListEntry           */
/*                   in active node when it gets FTN Hwlist    */
/*                   and send this to standby by calling       */
/*                   LdpSendMsgToRm                            */ 
/*  Input(s)        :pFTNHwList -The received HwList entry     */
/*                   u1OpType - Add or Delete Type             */
/*  Output(s)       :None                                      */
/***************************************************************/

VOID LdpRmSendFTNHwListEntry (tFTNHwListEntry *pFTNHwList, UINT1 u1OpType)
{

    tRmMsg             *pMsg = NULL;
    UINT4               u4Offset = LDP_ZERO;
    UINT2               u2MsgLength = LDP_ZERO;
    UINT2               u2RmMsgLength = LDP_ZERO;

    /* Check If the node is active if not return */
    if (LDP_IS_NODE_ACTIVE () != LDP_RM_TRUE)
    {
        LDP_DBG(HA_DEBUG,"Node is Standby\n");
        return;
    }

    /* Check If istandby exist if not return */
    if (LDP_IS_STANDBY_UP () == LDP_RM_FALSE)
    {
        LDP_DBG (HA_DEBUG,"No Standby is existing\n");
        return;
    }

    if (NULL == pFTNHwList)
    {
        LDP_DBG (HA_DEBUG,"No Entry in HW List\n");
        return;
    }

    u2MsgLength = (UINT2)sizeof(tFTNHwListEntry);
    u2RmMsgLength = (UINT2)((UINT2)LDP_RM_MSG_HDR_SIZE + u2MsgLength + (UINT2)sizeof (UINT1));

    LDP_DBG1 (HA_DEBUG,"Total Msg Length :%d", u2RmMsgLength);
    pMsg = CRU_BUF_Allocate_MsgBufChain(u2RmMsgLength, LDP_ZERO);

    if(pMsg == NULL)
    {
        LDP_DBG (HA_DEBUG,"Memory Allocation Failed\n");
        return;
    }
    /* Fill the Message Type */
    RM_DATA_ASSIGN_1_BYTE (pMsg, u4Offset, LDP_RED_FTN_HW_LIST_SYNC_MSG);
    u4Offset = u4Offset + sizeof (UINT1);

    /* Fill the Message  Length */
    RM_DATA_ASSIGN_2_BYTE(pMsg, u4Offset, u2RmMsgLength);
    u4Offset = u4Offset + sizeof (UINT2);

    RM_DATA_ASSIGN_1_BYTE(pMsg,u4Offset,u1OpType);
    u4Offset = u4Offset + sizeof (UINT1);

    BUF_COPY_OVER_CHAIN(pMsg,pFTNHwList,u4Offset,sizeof(tFTNHwListEntry));
    u4Offset = u4Offset + sizeof(tFTNHwListEntry);

    if ( LdpRmSendMsgToRm(pMsg, u2RmMsgLength) != LDP_SUCCESS )
    {
        LDP_DBG(HA_DEBUG,"Sending Message to RM is failed\n");
        return;
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpRmSendBulkILMInfo                                      */
/* Description   : This function is invoked from RM in active                */
/*                 node when RM gets the bulk update request                 */
/*                 and sends out the bulk update packet for ILM Entry        */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID LdpRmSendBulkILMInfo()
{
    tRmMsg              *pBuf = NULL;
    tILMHwListEntry     *pILMHwListEntry = NULL;
    UINT4               u4Offset = LDP_ZERO;
    UINT2               u2MaxILMEntries = LDP_ZERO;
    UINT2               u2Count = LDP_ZERO ;

     /* Calculate Max no. of hw list entry can be sent to RM
        after dividing total RM message length  by sizeof tILMHwListEntry structure */
    u2MaxILMEntries = (LDP_RM_MAX_BULK_SIZE - (LDP_RM_MSG_HDR_SIZE + 
                        sizeof (UINT1)+ sizeof (UINT2)))/sizeof(tILMHwListEntry);


    pBuf = CRU_BUF_Allocate_MsgBufChain (LDP_RM_MAX_BULK_SIZE, LDP_ZERO);
    if(NULL == pBuf)
    {
        LDP_DBG(HA_DEBUG,"Memory allocation Failed\n");
        LdpRmSendBulkAbort (RM_MEMALLOC_FAIL);
        gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_ABORTED;
        return;
    }
    /* Fill message type */
    LDP_RM_PUT_1_BYTE (pBuf, u4Offset, LDP_RED_BULK_UPDATE_MSG);
    /* Location for Data_Lenght, which will be filled later in the code below 
     * when data lengnth is available. For now, move the offset*/
    u4Offset = u4Offset + sizeof (UINT2);

    LDP_RM_PUT_1_BYTE (pBuf, u4Offset, LDP_RM_ILM_GBL_MOD);
    u4Offset = u4Offset + sizeof (UINT2);


    pILMHwListEntry = MplsHwListGetFirstILMHwListEntry();

    while (NULL != pILMHwListEntry)
    {

        BUF_COPY_OVER_CHAIN (pBuf, pILMHwListEntry, u4Offset, sizeof(tILMHwListEntry));
        u4Offset = u4Offset + sizeof(tILMHwListEntry);
        u2Count++;

        if(u2Count == u2MaxILMEntries)
        {
            RM_DATA_ASSIGN_2_BYTE (pBuf, LDP_RM_BULK_LEN_OFFSET, (UINT2)u4Offset);
            RM_DATA_ASSIGN_2_BYTE (pBuf, (LDP_RM_MSG_HDR_SIZE + LDP_RM_BULK_TYPE_OFFSET), u2Count);

             /* pBuf is freed by LdpRmSendMsgToRm, if it fails*/
            if (LdpRmSendMsgToRm (pBuf, (UINT2)u4Offset) == LDP_FAILURE)
            {
                LDP_DBG (HA_DEBUG,"sending Msg to RM failed\n");

                gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_ABORTED;
                return ;
            }
            else /* If RM_Send is success, Allocate new buffer for next set of data*/
            {   
                pBuf = CRU_BUF_Allocate_MsgBufChain (LDP_RM_MAX_BULK_SIZE, LDP_ZERO);

                if(NULL == pBuf)
                {
                    LDP_DBG(HA_DEBUG,"Memory allocation Failed\n");
                    LdpRmSendBulkAbort (RM_MEMALLOC_FAIL);
                    gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_ABORTED;
                    return;
                }

                u4Offset = LDP_ZERO;
                LDP_RM_PUT_1_BYTE (pBuf, u4Offset, LDP_RED_BULK_UPDATE_MSG);

                /* Increment offset to pass the total packet length which 
                 * will be filled later */
                u4Offset = u4Offset + sizeof (UINT2);

                LDP_RM_PUT_1_BYTE (pBuf, u4Offset, LDP_RM_ILM_GBL_MOD);
                u4Offset = u4Offset + sizeof (UINT2);

                /* Reset the count*/
                u2Count = LDP_ZERO;

                LDP_DBG (HA_DEBUG,"send Next bulk update from Hw List\n");
            }
        }
        pILMHwListEntry = MplsHwListGetNextILMHwListEntry (pILMHwListEntry);
    }

    /*Need to send Msg if Count has not reached Max, hence not sent already*/ 
    if (u2Count > 0)
    {
        RM_DATA_ASSIGN_2_BYTE (pBuf, LDP_RM_BULK_LEN_OFFSET, (UINT2)u4Offset);
        RM_DATA_ASSIGN_2_BYTE (pBuf, (LDP_RM_MSG_HDR_SIZE + LDP_RM_BULK_TYPE_OFFSET), u2Count);

        /* pBuf is freed by LdpRmSendMsgToRm, if it fails */
        if(LdpRmSendMsgToRm (pBuf, (UINT2)u4Offset) == LDP_FAILURE)
        {
            LDP_DBG(HA_DEBUG,"sending Msg to RM failed\n");
            gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_ABORTED;
            return ;
        }
    }
    else /*No Need to send any message, free the buffer*/
    {
        RM_FREE (pBuf);
    }
    return ;
}

/*****************************************************************************/
/* Function Name : LdpRmSendBulkFTNInfo                                      */
/* Description   : This function is invoked from RM in active                */
/*                 node when RM gets the bulk update request                 */
/*                 and sends out the bulk update packet for FTN Entry        */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID LdpRmSendBulkFTNInfo()
{

    tRmMsg              *pBuf = NULL;
    tFTNHwListEntry     *pFTNHwListEntry = NULL;
    UINT4               u4Offset = LDP_ZERO;
    UINT2               u2MaxFTNEntries = LDP_ZERO;
    UINT2               u2Count =LDP_ZERO ;

    /* Calculate Max no. of hw list entry can be sent to RM
       after dividing total RM message length  by sizeof tFTNHwListEntry structure */

    u2MaxFTNEntries = (LDP_RM_MAX_BULK_SIZE - (LDP_RM_MSG_HDR_SIZE + 
                         sizeof (UINT1) + sizeof (UINT2)))/sizeof(tFTNHwListEntry);

    pBuf = CRU_BUF_Allocate_MsgBufChain (LDP_RM_MAX_BULK_SIZE, LDP_ZERO);
    if(pBuf == NULL)
    {
        LDP_DBG(HA_DEBUG,"Memory allocation Failed\n");
        LdpRmSendBulkAbort (RM_MEMALLOC_FAIL);
        gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_ABORTED;
        return;
    }

    LDP_RM_PUT_1_BYTE (pBuf, u4Offset, LDP_RED_BULK_UPDATE_MSG);
    /* Increment offset to pass the total packet length which
     * will be filled later */
    u4Offset = u4Offset + sizeof (UINT2);

    LDP_RM_PUT_1_BYTE (pBuf, u4Offset, LDP_RM_FTN_GBL_MOD);
    /* The offset will set be later, when the Buf will be sent out */
    u4Offset = u4Offset + sizeof (UINT2);

    pFTNHwListEntry = MplsHwListGetFirstFTNHwListEntry();
    while (NULL != pFTNHwListEntry)
    {
        BUF_COPY_OVER_CHAIN (pBuf, pFTNHwListEntry, u4Offset, sizeof(tFTNHwListEntry));
        u4Offset = u4Offset + sizeof(tFTNHwListEntry);
        u2Count++;

        if(u2Count == u2MaxFTNEntries)
        {
            RM_DATA_ASSIGN_2_BYTE (pBuf, LDP_RM_BULK_LEN_OFFSET, (UINT2)u4Offset);
            RM_DATA_ASSIGN_2_BYTE (pBuf, (LDP_RM_MSG_HDR_SIZE + LDP_RM_BULK_TYPE_OFFSET), u2Count);

             /* pBuf is freed by LdpRmSendMsgToRm, if it fails*/

            if (LdpRmSendMsgToRm (pBuf,(UINT2)u4Offset) == LDP_FAILURE)
            {
                LDP_DBG(HA_DEBUG,"sending Msg to RM failed\n");
                gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_ABORTED;
                return ;
            }
            else /* If RM_Send is success, Allocate new buffer for next set of data*/
            {
                pBuf = CRU_BUF_Allocate_MsgBufChain (LDP_RM_MAX_BULK_SIZE, LDP_ZERO);
                if (pBuf == NULL)
                {
                    LDP_DBG (HA_DEBUG, "FAILURE: Memory allocation failed\n");
                    LdpRmSendBulkAbort (RM_MEMALLOC_FAIL);
                    gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_ABORTED;
                    return;
                }

                u4Offset = LDP_ZERO;
                LDP_RM_PUT_1_BYTE (pBuf, u4Offset, LDP_RED_BULK_UPDATE_MSG);

                /* Increment offset to pass the total packet length which
                 *  will be filled later */       
                u4Offset = u4Offset + sizeof (UINT2);
                LDP_RM_PUT_1_BYTE (pBuf, u4Offset, LDP_RM_FTN_GBL_MOD);

                /* The offset will set be later, when the Buf will be sent out */
                u4Offset = u4Offset + sizeof (UINT2);

                /* Reset the count*/
                u2Count = LDP_ZERO;
                LDP_DBG (HA_DEBUG,"send Next bulk update from Hw List\n");
            }

        }
        pFTNHwListEntry =  MplsHwListGetNextFTNHwListEntry (pFTNHwListEntry);
    }

    /*Need to send Msg if Count has not reached Max, hence not sent already*/
    if(u2Count > 0)
    {
        RM_DATA_ASSIGN_2_BYTE (pBuf, LDP_RM_BULK_LEN_OFFSET, (UINT2)u4Offset);
        RM_DATA_ASSIGN_2_BYTE (pBuf, (LDP_RM_MSG_HDR_SIZE + LDP_RM_BULK_TYPE_OFFSET), u2Count);
        /* pBuf is freed by LdpRmSendMsgToRm, if it fails*/
        if(LdpRmSendMsgToRm (pBuf, (UINT2)u4Offset) == LDP_FAILURE)
        {
            LDP_DBG(HA_DEBUG,"sending Msg to RM failed\n");
            gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_ABORTED;
            return ;
        }
    }
    else /*No Need to send any message, free the buffer*/
    {
        RM_FREE (pBuf);
    }
    return ;
}














