/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdldpwr.c,v 1.5 2012/10/24 10:44:08 siva Exp $
*
* Description: Protocol Wrapper Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "stdldplw.h"
# include  "stdldpwr.h"
# include  "stdldpdb.h"
# include  "ldpincs.h"
VOID
RegisterSTDLDP ()
{
    SNMPRegisterMibWithLock (&stdldpOID, &stdldpEntry, LdpLock, LdpUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdldpOID, (const UINT1 *) "stdldp");
}

VOID
UnRegisterSTDLDP ()
{
    SNMPUnRegisterMib (&stdldpOID, &stdldpEntry);
    SNMPDelSysorEntry (&stdldpOID, (const UINT1 *) "stdldp");
}

INT4
MplsLdpLsrIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsLdpLsrId (pMultiData->pOctetStrValue));
}

INT4
MplsLdpLsrLoopDetectionCapableGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsLdpLsrLoopDetectionCapable
            (&(pMultiData->i4_SLongValue)));
}

INT4
MplsLdpEntityLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsLdpEntityLastChange (&(pMultiData->u4_ULongValue)));
}

INT4
MplsLdpEntityIndexNextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsLdpEntityIndexNext (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexMplsLdpEntityTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpEntityTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpEntityTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpEntityLdpIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsLdpEntityIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
MplsLdpEntityProtocolVersionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityProtocolVersion
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAdminStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityOperStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityTcpPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityTcpPort (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityUdpDscPortGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityUdpDscPort
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityMaxPduLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityMaxPduLength
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityKeepAliveHoldTimerGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityKeepAliveHoldTimer
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityHelloHoldTimerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityHelloHoldTimer
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityInitSessionThresholdGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityInitSessionThreshold
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityLabelDistMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityLabelDistMethod
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityLabelRetentionModeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityLabelRetentionMode
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityPathVectorLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityPathVectorLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityHopCountLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityHopCountLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityTransportAddrKindGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityTransportAddrKind
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityTargetPeerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityTargetPeer
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityTargetPeerAddrTypeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityTargetPeerAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityTargetPeerAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityTargetPeerAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
MplsLdpEntityLabelTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityLabelType (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityDiscontinuityTimeGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityDiscontinuityTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityProtocolVersionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityProtocolVersion
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAdminStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityTcpPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityTcpPort (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].u4_ULongValue,
                                        pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityUdpDscPortSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityUdpDscPort
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityMaxPduLengthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityMaxPduLength
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityKeepAliveHoldTimerSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityKeepAliveHoldTimer
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityHelloHoldTimerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityHelloHoldTimer
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityInitSessionThresholdSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityInitSessionThreshold
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityLabelDistMethodSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityLabelDistMethod
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityLabelRetentionModeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityLabelRetentionMode
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityPathVectorLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityPathVectorLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityHopCountLimitSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityHopCountLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityTransportAddrKindSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityTransportAddrKind
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityTargetPeerSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityTargetPeer
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityTargetPeerAddrTypeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityTargetPeerAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityTargetPeerAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityTargetPeerAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
MplsLdpEntityLabelTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityLabelType (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityProtocolVersionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityProtocolVersion (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAdminStatus (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityTcpPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityTcpPort (pu4Error,
                                           pMultiIndex->pIndex[0].
                                           pOctetStrValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityUdpDscPortTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityUdpDscPort (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityMaxPduLengthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityMaxPduLength (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityKeepAliveHoldTimerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityKeepAliveHoldTimer (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      u4_ULongValue));

}

INT4
MplsLdpEntityHelloHoldTimerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityHelloHoldTimer (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue));

}

INT4
MplsLdpEntityInitSessionThresholdTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityInitSessionThreshold (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        pOctetStrValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
MplsLdpEntityLabelDistMethodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityLabelDistMethod (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityLabelRetentionModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityLabelRetentionMode (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
MplsLdpEntityPathVectorLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityPathVectorLimit (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityHopCountLimitTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityHopCountLimit (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityTransportAddrKindTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityTransportAddrKind (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     i4_SLongValue));

}

INT4
MplsLdpEntityTargetPeerTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityTargetPeer (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityTargetPeerAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityTargetPeerAddrType (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
MplsLdpEntityTargetPeerAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityTargetPeerAddr (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->pOctetStrValue));

}

INT4
MplsLdpEntityLabelTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityLabelType (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityStorageType (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsLdpEntityTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMplsLdpEntityStatsTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpEntityStatsTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpEntityStatsTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpEntityStatsSessionAttemptsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsSessionAttempts
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsSessionRejectedNoHelloErrorsGet (tSnmpIndex * pMultiIndex,
                                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsSessionRejectedNoHelloErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsSessionRejectedAdErrorsGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsSessionRejectedAdErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsSessionRejectedMaxPduErrorsGet (tSnmpIndex * pMultiIndex,
                                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsSessionRejectedMaxPduErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsSessionRejectedLRErrorsGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsSessionRejectedLRErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsBadLdpIdentifierErrorsGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsBadLdpIdentifierErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsBadPduLengthErrorsGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsBadPduLengthErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsBadMessageLengthErrorsGet (tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsBadMessageLengthErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsBadTlvLengthErrorsGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsBadTlvLengthErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsMalformedTlvValueErrorsGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsMalformedTlvValueErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsKeepAliveTimerExpErrorsGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsKeepAliveTimerExpErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsShutdownReceivedNotificationsGet (tSnmpIndex * pMultiIndex,
                                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsShutdownReceivedNotifications
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityStatsShutdownSentNotificationsGet (tSnmpIndex * pMultiIndex,
                                                tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityStatsShutdownSentNotifications
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpPeerLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsLdpPeerLastChange (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexMplsLdpPeerTable (tSnmpIndex * pFirstMultiIndex,
                              tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpPeerTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpPeerTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpPeerLdpIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpPeerTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[2].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsLdpPeerLabelDistMethodGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpPeerTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpPeerLabelDistMethod
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpPeerPathVectorLimitGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpPeerTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpPeerPathVectorLimit
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpPeerTransportAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpPeerTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpPeerTransportAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpPeerTransportAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpPeerTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpPeerTransportAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
GetNextIndexMplsLdpSessionTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpSessionTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpSessionTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpSessionStateLastChangeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionStateLastChange
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpSessionStateGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionState (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpSessionRoleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionRole (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpSessionProtocolVersionGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionProtocolVersion
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpSessionKeepAliveHoldTimeRemGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionKeepAliveHoldTimeRem
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpSessionKeepAliveTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionKeepAliveTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpSessionMaxPduLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionMaxPduLength
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpSessionDiscontinuityTimeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionDiscontinuityTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexMplsLdpSessionStatsTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpSessionStatsTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpSessionStatsTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpSessionStatsUnknownMesTypeErrorsGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionStatsUnknownMesTypeErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpSessionStatsUnknownTlvErrorsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionStatsTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionStatsUnknownTlvErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
GetNextIndexMplsLdpHelloAdjacencyTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpHelloAdjacencyTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpHelloAdjacencyTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpHelloAdjacencyIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpHelloAdjacencyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[3].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
MplsLdpHelloAdjacencyHoldTimeRemGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpHelloAdjacencyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpHelloAdjacencyHoldTimeRem
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpHelloAdjacencyHoldTimeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpHelloAdjacencyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpHelloAdjacencyHoldTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpHelloAdjacencyTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpHelloAdjacencyTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpHelloAdjacencyType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexMplsInSegmentLdpLspTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsInSegmentLdpLspTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsInSegmentLdpLspTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsInSegmentLdpLspIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentLdpLspTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[3].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[3].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[3].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsInSegmentLdpLspLabelTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentLdpLspTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentLdpLspLabelType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsInSegmentLdpLspTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentLdpLspTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentLdpLspType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
GetNextIndexMplsOutSegmentLdpLspTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsOutSegmentLdpLspTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsOutSegmentLdpLspTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].pOctetStrValue,
             pNextMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsOutSegmentLdpLspIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentLdpLspTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[3].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[3].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[3].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsOutSegmentLdpLspLabelTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentLdpLspTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentLdpLspLabelType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsOutSegmentLdpLspTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentLdpLspTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentLdpLspType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsFecLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsFecLastChange (&(pMultiData->u4_ULongValue)));
}

INT4
MplsFecIndexNextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsFecIndexNext (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexMplsFecTable (tSnmpIndex * pFirstMultiIndex,
                          tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsFecTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsFecTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsFecIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsFecTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[0].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
MplsFecTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsFecTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsFecType (pMultiIndex->pIndex[0].u4_ULongValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
MplsFecAddrTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsFecTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsFecAddrType (pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
MplsFecAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsFecTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsFecAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->pOctetStrValue));

}

INT4
MplsFecAddrPrefixLengthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsFecTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsFecAddrPrefixLength (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
MplsFecStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsFecTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsFecStorageType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
MplsFecRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsFecTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsFecRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
MplsFecTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsFecType (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->i4_SLongValue));

}

INT4
MplsFecAddrTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsFecAddrType (pMultiIndex->pIndex[0].u4_ULongValue,
                                   pMultiData->i4_SLongValue));

}

INT4
MplsFecAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsFecAddr (pMultiIndex->pIndex[0].u4_ULongValue,
                               pMultiData->pOctetStrValue));

}

INT4
MplsFecAddrPrefixLengthSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsFecAddrPrefixLength (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue));

}

INT4
MplsFecStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsFecStorageType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
MplsFecRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsFecRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue));

}

INT4
MplsFecTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsFecType (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->i4_SLongValue));

}

INT4
MplsFecAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2MplsFecAddrType (pu4Error,
                                      pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->i4_SLongValue));

}

INT4
MplsFecAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsFecAddr (pu4Error,
                                  pMultiIndex->pIndex[0].u4_ULongValue,
                                  pMultiData->pOctetStrValue));

}

INT4
MplsFecAddrPrefixLengthTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2MplsFecAddrPrefixLength (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue));

}

INT4
MplsFecStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2MplsFecStorageType (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue));

}

INT4
MplsFecRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    return (nmhTestv2MplsFecRowStatus (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue));

}

INT4
MplsLdpLspFecLastChangeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsLdpLspFecLastChange (&(pMultiData->u4_ULongValue)));
}

INT4
MplsFecTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsFecTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMplsLdpLspFecTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpLspFecTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpLspFecTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue,
             pFirstMultiIndex->pIndex[5].u4_ULongValue,
             &(pNextMultiIndex->pIndex[5].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpLspFecSegmentGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpLspFecTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[3].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
MplsLdpLspFecSegmentIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpLspFecTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[4].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[4].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[4].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsLdpLspFecIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpLspFecTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[5].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
MplsLdpLspFecStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpLspFecTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpLspFecStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             pMultiIndex->pIndex[5].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpLspFecRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpLspFecTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].pOctetStrValue,
         pMultiIndex->pIndex[5].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpLspFecRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiIndex->pIndex[4].pOctetStrValue,
                                          pMultiIndex->pIndex[5].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpLspFecStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpLspFecStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             pMultiIndex->pIndex[5].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpLspFecRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpLspFecRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiIndex->pIndex[2].pOctetStrValue,
                                          pMultiIndex->pIndex[3].i4_SLongValue,
                                          pMultiIndex->pIndex[4].pOctetStrValue,
                                          pMultiIndex->pIndex[5].u4_ULongValue,
                                          pMultiData->i4_SLongValue));

}

INT4
MplsLdpLspFecStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpLspFecStorageType (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[3].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[4].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[5].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
MplsLdpLspFecRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpLspFecRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiIndex->pIndex[2].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[3].
                                             i4_SLongValue,
                                             pMultiIndex->pIndex[4].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[5].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue));

}

INT4
MplsLdpLspFecTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsLdpLspFecTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMplsLdpSessionPeerAddrTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpSessionPeerAddrTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpSessionPeerAddrTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpSessionPeerAddrIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionPeerAddrTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[3].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
MplsLdpSessionPeerNextHopAddrTypeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionPeerAddrTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionPeerNextHopAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpSessionPeerNextHopAddrGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpSessionPeerAddrTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionPeerNextHopAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->pOctetStrValue));

}
