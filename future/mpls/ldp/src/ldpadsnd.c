/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *  
 * $Id: ldpadsnd.c,v 1.28 2016/03/02 11:39:53 siva Exp $
 *   
 *********************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpadsnd.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : LDP (ADVERT SUB-MODULE)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file consists of the routines for sending
 *                             Advertisement realted Messages like Label Request
 *                             Label Mapping, Label Release, Label Withdraw, and
 *                             Label Req Abort etc.
 *----------------------------------------------------------------------------*/

#include "ldpincs.h"

/*****************************************************************************/
/* Function Name : LdpSendLblReqMsg                                          */
/* Description   : This routine constructs and sends the Label Request       */
/*                 Message.                                                  */
/*                 This routine is invoked as a result of the processing of  */
/*                 Label Request Message received from upstream or internal  */
/*                 event in case of Ingress LSR.                             */
/* Input(s)      : pLspCtrlBlock - Points to the current Lsp Control Block.  */
/*                 u1HcValue - Hop count Value in the Label Request message  */
/*                           received from upstream. In case of Ingress LSR, */
/*                           value is zero.                                  */
/*                 pu1PvList - Points to a list of Path Vectors received in  */
/*                             the Label Request  message from upstream.     */
/*                             Points to NULL, in the case of Ingress LSR.   */
/*                 u1PvCount - Number of Path Vectors, pu1PvList is pointing */
/*                             to. In case the loop detection is not enabled,*/
/*                             this should be zero.                          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS if Label Request Message is sent successfully */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpSendLblReqMsg (tLspCtrlBlock * pLspCtrlBlock, UINT1 u1HcValue,
                  UINT1 *pu1PvList, UINT1 u1PvCount,
                  UINT1 *pu1UnknownTlv, UINT1 u1UnknownTlvLen)
{
    UINT1               u1HcPrsnt = LDP_FALSE;
    UINT2               u2OptParamLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT4               u4Offset = 0;
    UINT1               u1PadPrefixLen =
        (UINT1) LDP_GET_PADDED_PRFX_LEN (pLspCtrlBlock->Fec.u1PreLen);

    tFec                FecElmnt;
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tTlvHdr             PVTlvHdr = { OSIX_HTONS (LDP_PATH_VECTOR_TLV), 0 };
    tHopCountTlv        HopCountTlv = { {OSIX_HTONS (LDP_HOP_COUNT_TLV),
                                         OSIX_HTONS (LDP_HC_VALUE_LEN)},
    0, {0, 0, 0}
    };
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_REQUEST_MSG), 0, 0 };
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLdpId              PeerLdpId;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;

    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);
    MEMSET(&FecElmnt,0,sizeof(tFec));

    /*MPLS_IPv6 add start*/
    LdpIfAddrZero.Addr.u4Addr = LDP_ZERO;
    /*MPLS_IPv6 add end*/
    FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PadPrefixLen));
    u2MandParamLen += LDP_MSG_ID_LEN;

    if (pLspCtrlBlock->pUStrSession == NULL)
    {
        /*LSR is ingress */
        /* By default copying the Hop Count Value to buffer */

        /* As Per RFC 5036, A.2.7,
         * If the LSR is Ingress and if 
         * 1. Hop Count is configured or
         * 2. Loop Detection is configured
         * include the Hop Count TLV with Hop Count as ONE.
         */
        if ((LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID
                              (pLspCtrlBlock->pDStrSession))
             == LDP_LOOP_DETECT_CAPABLE_HC)
            || (LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID
                                 (pLspCtrlBlock->pDStrSession))
                == LDP_LOOP_DETECT_CAPABLE_HCPV)
            || (LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID
                                 (pLspCtrlBlock->pDStrSession))
                == LDP_LOOP_DETECT_CAPABLE_PV))
        {
            HopCountTlv.u1HcValue = LDP_HC_VALUE_AT_INGRESS;
            u1HcValue = HopCountTlv.u1HcValue;
            u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN);
            u1HcPrsnt = LDP_TRUE;
        }
        else
        {
            HopCountTlv.u1HcValue = LDP_ZERO;
        }
        /* As Per RFC 5036, A.2.8, If an LSR configured as merge capable, 
         * It should not send Label req message with PATH VECTOR TLV
         * */

        if ((SSN_GET_LOOP_DETECT (pLspCtrlBlock->pDStrSession) == LDP_TRUE) &&
            (gi4MplsSimulateFailure != LDP_SIM_FAILURE_LBL_MERGE_CAPABLE))
        {
            PVTlvHdr.u2TlvLen = OSIX_HTONS (LDP_LSR_ID_LEN);
            u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_LSR_ID_LEN);
        }
    }
    else
    {
        /* Intermediate Lsr */

        /* As Per RFC 5036, A.2.7,
         * If an intermediate LSR receives Label Req message with
         * 1. Hop Count TLV or
         * 2. Loop Detection is configured in the LSR,
         * then
         * a. If the hop count TLV is there, increment the TLV
         * b. If the hop count TLV is not there, but Loop Detection (PV) is 
         *    configured, add Hop Count TLV with value ZERO
         */

        u1HcPrsnt = LDP_HC_TLV_PRSNT (u1HcValue);
        if ((LDP_LOOP_DETECT
             ((UINT2) SSN_GET_INCRN_ID (pLspCtrlBlock->pDStrSession)) ==
             LDP_LOOP_DETECT_CAPABLE_HCPV) ||
            (LDP_LOOP_DETECT
             ((UINT2) SSN_GET_INCRN_ID (pLspCtrlBlock->pDStrSession)) ==
             LDP_LOOP_DETECT_CAPABLE_PV) ||
            (LDP_LOOP_DETECT
             ((UINT2) SSN_GET_INCRN_ID (pLspCtrlBlock->pDStrSession)) ==
             LDP_LOOP_DETECT_CAPABLE_HC) || (u1HcPrsnt == LDP_TRUE))
        {
            if (u1HcPrsnt != LDP_TRUE)
            {
                HopCountTlv.u1HcValue = LDP_ZERO;
            }
            else
            {
                HopCountTlv.u1HcValue = LDP_INCREMENT_HC_VALUE (u1HcValue);
            }
            u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN);
            u1HcPrsnt = LDP_TRUE;
        }
        /* As Per RFC 5036, A.2.7, If an LSR configured as merge capable, 
         * It should not send Label req message with PATH VECTOR TLV
         * */
        if ((SSN_GET_LOOP_DETECT (pLspCtrlBlock->pDStrSession) == LDP_TRUE) &&
            (gi4MplsSimulateFailure != LDP_SIM_FAILURE_LBL_MERGE_CAPABLE))
        {
            PVTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_LSR_ID_LEN * (u1PvCount + 1)));
            u2OptParamLen +=
                (LDP_TLV_HDR_LEN + (LDP_LSR_ID_LEN * (u1PvCount + 1)));
        }
    }

    u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN + u1PadPrefixLen);

    if (u1UnknownTlvLen != LDP_ZERO)
    {
        u2OptParamLen += u1UnknownTlvLen;
    }

    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2OptParamLen + u2MandParamLen),
                                      LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Can't Allocate Buf for Lbl Req Msg \n");
        return LDP_FAILURE;
    }

    /* Get the Msg Id for this LblReqMsg and store it in Lsp Control Block */
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLspCtrlBlock->pDStrSession));
    pLspCtrlBlock->u4DStrLblReqId = SSN_GET_MSGID (pLspCtrlBlock->pDStrSession);

    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2OptParamLen + u2MandParamLen));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (pLspCtrlBlock->u4DStrLblReqId);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying LblReqMsgHdr and MsgId to Buf-chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct Fec Element */
    FecElmnt.u1FecElmntType = pLspCtrlBlock->Fec.u1FecElmntType;
    FecElmnt.u2AddrFmly = OSIX_HTONS (pLspCtrlBlock->Fec.u2AddrFmly);
    if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_HOSTADDR_TYPE)
    {
        /* When the Fec Element Type is HostAddress, the Address Length
         * should be in Octect Length. 
         * u1PrefLen has the length in bits.
         * It is divided by 8 (right shift 3 times), to get the length
         * in Octect Length.*/
        FecElmnt.u1PreLen = (UINT1) ((pLspCtrlBlock->Fec.u1PreLen) >> 3);
    }
    else
    {
	   FecElmnt.u1PreLen = pLspCtrlBlock->Fec.u1PreLen;
    }

    LdpCopyAddr(&FecElmnt.Prefix,&pLspCtrlBlock->Fec.Prefix,pLspCtrlBlock->Fec.u2AddrFmly);

    if(pLspCtrlBlock->Fec.u2AddrFmly==LDP_ADDR_TYPE_IPV4)
    {
	    LDP_IPV4_U4_ADDR(FecElmnt.Prefix)=OSIX_HTONL(LDP_IPV4_U4_ADDR(FecElmnt.Prefix));
    }

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
			    LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecElmnt.u2AddrFmly,
                                   u4Offset, LDP_UINT2_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement-AddrFamily to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT2_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1PreLen, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement-PrefixLen to Buff chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

#ifdef MPLS_IPV6_WANTED
    if(pLspCtrlBlock->Fec.u2AddrFmly==LDP_ADDR_TYPE_IPV6)
    {
	    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecElmnt.Prefix.Ip6Addr, u4Offset,
				    u1PadPrefixLen) == CRU_FAILURE)
	    {
		    LDP_DBG (LDP_ADVT_MEM,
				    "ADVT: Error copying FecElement-AddrPrefix to Buf chain\n");
		    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
		    return LDP_FAILURE;
	    }	    
    }
    else
#endif
    {
	    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecElmnt.Prefix.u4Addr, u4Offset,
				    LDP_IPV4ADR_LEN) == CRU_FAILURE)
	    {
		    LDP_DBG (LDP_ADVT_MEM,
				    "ADVT: Error copying FecElement-AddrPrefix to Buf chain\n");
		    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
		    return LDP_FAILURE;
	    }
    }

    u4Offset += u1PadPrefixLen;

    if (u1HcPrsnt == LDP_TRUE)
    {
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &HopCountTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying HC Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN);
    }

    /* As Per RFC 5036, A.2.8, If an LSR configured as merge capable, 
     * It should not send Label req message with PATH VECTOR TLV
     * */

    if ((SSN_GET_LOOP_DETECT (pLspCtrlBlock->pDStrSession) == LDP_TRUE) &&
        (gi4MplsSimulateFailure != LDP_SIM_FAILURE_LBL_MERGE_CAPABLE))
    {
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &PVTlvHdr, u4Offset,
                                       LDP_TLV_HDR_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying PVTlv Hdr to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_TLV_HDR_LEN;

        if (pLspCtrlBlock->pUStrSession != NULL)
        {
            /* LSR is Not Ingress */

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) pu1PvList, u4Offset,
                                           (u1PvCount * LDP_LSR_ID_LEN)) ==
                CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying PV List to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += (u1PvCount * LDP_LSR_ID_LEN);
        }

        /* Copy local Lsr-Id */
        if (CRU_BUF_Copy_OverBufChain (pMsg,
                                       (UINT1 *)
                                       &SSN_GET_LSR_ID (pLspCtrlBlock->
                                                        pDStrSession), u4Offset,
                                       LDP_LSR_ID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Local LsrId to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_LSR_ID_LEN;
    }

    if (u1UnknownTlvLen != LDP_ZERO)
    {
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) pu1UnknownTlv, u4Offset,
                                       u1UnknownTlvLen) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += u1UnknownTlvLen;
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLspCtrlBlock->pDStrSession);


#ifdef MPLS_IPV6_WANTED
    if(pLspCtrlBlock->Fec.u2AddrFmly==LDP_ADDR_TYPE_IPV6)
    {
	    LDP_DBG2 (LDP_ADVT_TX,
			    "Label Req Msg for the prefix %s/%d \n",
			    Ip6PrintAddr(&FecElmnt.Prefix.Ip6Addr),
			    FecElmnt.u1PreLen);
    }
    else
#endif
    {
	    u4Temp = OSIX_HTONL (FecElmnt.Prefix.u4Addr);
	    MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);
	    LDP_DBG5 (LDP_ADVT_TX,
			    "Label Req Msg for the prefix %d.%d.%d.%d/%d \n",
			    au1IpAddr[3], au1IpAddr[2], au1IpAddr[1], au1IpAddr[0],
			    FecElmnt.u1PreLen);
    }

    LDP_DBG4 (LDP_ADVT_TX,
		    "ADVT: Sending Lbl Request Msg To %d.%d.%d.%d \n",
		    PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);
    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLspCtrlBlock->pDStrSession->u4TcpConnId), LDP_IPV4ADR_LEN);

    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLspCtrlBlock->pDStrSession->pLdpPeer->pLdpEntity),
                    LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        /*MPLS_IPv6 add end*/
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Lbl Request Msg\n");
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_ADVT_TX, "ADVT: Lbl Request Msg Sent\n");

    if (pLspCtrlBlock->pUStrSession == NULL)
    {
        /* LSR is Ingress */

        (SSN_GET_LBL_TYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ETHERNET_TYPE);

        (pLspCtrlBlock->u4InComIfIndex) = LDP_ZERO;

    }
    else
    {
        /* LSR is Intermediate */

        (SSN_GET_LBL_TYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ETHERNET_TYPE);

        (SSN_GET_LBL_TYPE (LCB_USSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ETHERNET_TYPE);
    }

    if (gu4LdpLspPersist == FALSE)
    {
        if ((gi4MplsSimulateFailure == LDP_SIM_FAILURE_PROPAGATE_LBL_ABORT_REQ)
            || (pLspCtrlBlock->pTrigCtrlBlk != NULL))
        {
            /* In case of Ingress Lsr, start a timer for a long period
             * (sufficient enough for the response to come back) after 
             * sending a label request 
             * This helps in handling the Tardy peers who generate Hello
             * messages and Keep Alive messages regularly, but any kind
             * of response is not sent back where by this LCB can be
             * reused for establishing some other LSP/CRLSP.
             */

            pLspCtrlBlock->TardyTimer.u4Event = LDP_TARDY_PEER_EXPIRED_EVENT;
            pLspCtrlBlock->TardyTimer.pu1EventInfo = (UINT1 *) pLspCtrlBlock;
            if (TmrStartTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *)
                               & (pLspCtrlBlock->TardyTimer.AppTimer),
                               (LDP_TARDY_TIME_PERD *
                                SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
            {
                LDP_DBG (LDP_ADVT_TMR, "ADVT: Failed to start the timer.\n");
            }
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendLblMappingMsg                                      */
/* Description   : This routine constructs and sends Label Mapping Message.  */
/*                 This routine is invoked in case of Egress LSR, as a result*/
/*                 of the processing of Label Request Message received from  */
/*                 upstream.                                                 */
/*                 In the case of other LSRs, this routine is invoked as a   */
/*                 result of the processing of Label Mapping messages        */
/*                 received from downstream.                                 */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock - Points to the current Lsp Control Block   */
/*                 u1HcValue - Hop count Value in the Label Mapping message  */
/*                             received from downstream. In case of Egress   */
/*                             LSR, value is one.                            */
/*                 pu1PvList - Points to a list of Path Vectors received in  */
/*                             the Label Mapping message. Points to NULL in  */
/*                             the case of Egress LSR.                       */
/*                 u1PvCount - Number of Path Vectors, pu1PvList is pointing */
/*                             to. In case the loop detection is not enabled,*/
/*                             this should be zero.                          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS   if Label Mapping Message is sent            */
/*                 successfully else LDP_FAILURE                             */
/*****************************************************************************/

UINT1
LdpSendLblMappingMsg (tLspCtrlBlock * pLspCtrlBlock, UINT1 u1HcValue,
                      UINT1 *pu1PvList, UINT1 u1PvCount)
{
    UINT1               u1HcPrsnt = LDP_FALSE;
    UINT2               u2OptParamLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT4               u4Offset = 0;
    UINT1               u1PadPrefixLen =
        LDP_GET_PADDED_PRFX_LEN (pLspCtrlBlock->Fec.u1PreLen);

    tFec                FecElmnt;
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tTlvHdr             PVTlvHdr = { OSIX_HTONS (LDP_PATH_VECTOR_TLV), 0 };
    tHopCountTlv        HopCountTlv = { {OSIX_HTONS (LDP_HOP_COUNT_TLV),
                                         OSIX_HTONS (LDP_HC_VALUE_LEN)},
    0, {0, 0, 0}
    };
    tLblReqMsgIdTlv     ReqMsgIdTlv = { {OSIX_HTONS (LDP_LBL_REQ_MSGID_TLV),
                                         OSIX_HTONS (LDP_MSG_ID_LEN)}, 0
    };
        /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    tGenLblTlv          GenLblTlv;
    tAtmLblTlv          AtmLblTlv;
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_MAPPING_MSG), 0, 0 };
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLdpId              PeerLdpId;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;

    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);
    /*MPLS_IPv6 add start*/
    LdpIfAddrZero.Addr.u4Addr = LDP_ZERO;
    /*MPLS_IPv6 add end*/
    FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PadPrefixLen));

    if (pLspCtrlBlock->pUStrSession == NULL)
    {
        LDP_DBG (LDP_ADVT_TX,
                 "Label Mapping message not sent - Upstream session not present\n");
        return LDP_FAILURE;
    }

    if (pLspCtrlBlock->pDStrSession == NULL)
    {
        /* LSR is Egress */
        /* As Per RFC 5036, A.2.8,
         * If the LSR is Egress and if 
         * 1. Hop Count is configured or
         * 2. Loop Detection is configured
         * include the Hop Count TLV with Hop Count as ONE.
         */

        if ((LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID
                              (pLspCtrlBlock->pUStrSession))
             == LDP_LOOP_DETECT_CAPABLE_HC)
            || (LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID
                                 (pLspCtrlBlock->pUStrSession))
                == LDP_LOOP_DETECT_CAPABLE_HCPV)
            || (LDP_LOOP_DETECT ((UINT2) SSN_GET_INCRN_ID
                                 (pLspCtrlBlock->pUStrSession))
                == LDP_LOOP_DETECT_CAPABLE_PV))
        {
            HopCountTlv.u1HcValue = LDP_HC_VALUE_AT_EGRESS;
            u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN);
            u1HcPrsnt = LDP_TRUE;
            u1HcValue = HopCountTlv.u1HcValue;

        }
        else
        {
            HopCountTlv.u1HcValue = LDP_ZERO;
        }
    }
    else
    {
        /* Intermediate Lsr */

        /* As Per RFC 5036, A.2.8,
         * If an intermediate LSR receives Label Mapping message with
         * 1. Hop Count TLV or
         * 2. Hop Count in configured in the LSR or
         * 3. Loop Detection is configured in the LSR,
         * then
         * a. If the hop count TLV is there, increment the TLV
         * b. If the hop count TLV is not there, but Loop Detection (PV) is 
         *    configured or Hop Count is configured then,
         *    add Hop Count TLV with value ZERO
         */

        u1HcPrsnt = LDP_HC_TLV_PRSNT (u1HcValue);
        if ((LDP_LOOP_DETECT
             ((UINT2) SSN_GET_INCRN_ID (pLspCtrlBlock->pUStrSession)) ==
             LDP_LOOP_DETECT_CAPABLE_HC)
            ||
            (LDP_LOOP_DETECT
             ((UINT2) SSN_GET_INCRN_ID (pLspCtrlBlock->pUStrSession)) ==
             LDP_LOOP_DETECT_CAPABLE_HCPV)
            ||
            (LDP_LOOP_DETECT
             ((UINT2) SSN_GET_INCRN_ID (pLspCtrlBlock->pUStrSession)) ==
             LDP_LOOP_DETECT_CAPABLE_PV) || (u1HcPrsnt == LDP_TRUE))
        {
            if (u1HcPrsnt != LDP_TRUE)
            {
                HopCountTlv.u1HcValue = LDP_ZERO;
            }
            else
            {
                HopCountTlv.u1HcValue = LDP_INCREMENT_HC_VALUE (u1HcValue);
            }
            u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN);
            u1HcPrsnt = LDP_TRUE;
        }

        /* As Per RFC 5036, A.2.8, If an intermediate LSR receives
         * Label Map message without HOP count TLV, It should not send
         * Label map message with PATH VECTOR TLV
         * */
        if ((SSN_GET_LOOP_DETECT (pLspCtrlBlock->pUStrSession) == LDP_TRUE) &&
            (u1HcValue != LDP_INVALID_HOP_COUNT))
        {
            PVTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_LSR_ID_LEN * (u1PvCount + 1)));
            u2OptParamLen +=
                (LDP_TLV_HDR_LEN + (LDP_LSR_ID_LEN * (u1PvCount + 1)));
        }
    }

    /* for Label Request Message Id Tlv */
    u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN);

    u2MandParamLen +=
        (LDP_MSG_ID_LEN + 2 * LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
         u1PadPrefixLen + LDP_LABEL_LEN);

    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2OptParamLen + u2MandParamLen),
                                      LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buffer for Lbl Mapping Msg \n");
        return LDP_FAILURE;
    }
    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2OptParamLen + u2MandParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLspCtrlBlock->pUStrSession));
    MsgHdrAndId.u4MsgId =
        OSIX_HTONL (SSN_GET_MSGID (pLspCtrlBlock->pUStrSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct Fec Element */
    FecElmnt.u1FecElmntType = pLspCtrlBlock->Fec.u1FecElmntType;
    FecElmnt.u2AddrFmly = OSIX_HTONS (pLspCtrlBlock->Fec.u2AddrFmly);
    if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_HOSTADDR_TYPE)
    {
        /* When the Fec Element Type is HostAddress, the Address Length
         * should be in Octect Length. 
         * u1PrefLen has the length in bits.
         * It is divided by 8 (right shift 3 times), to get the length
         * in Octect Length.*/
        FecElmnt.u1PreLen = (UINT1) ((pLspCtrlBlock->Fec.u1PreLen) >> 3);
    }
    else
    {
        FecElmnt.u1PreLen = pLspCtrlBlock->Fec.u1PreLen;
    }
   
    LdpCopyAddr(&FecElmnt.Prefix,&pLspCtrlBlock->Fec.Prefix,pLspCtrlBlock->Fec.u2AddrFmly);
    
    if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV4)
    {
        FecElmnt.Prefix.u4Addr = OSIX_HTONL (FecElmnt.Prefix.u4Addr);
    }

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecElmnt.u2AddrFmly,
                                   u4Offset, LDP_UINT2_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement-AddrFamily to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT2_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1PreLen, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement-PrefixLen to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

#ifdef MPLS_IPV6_WANTED
    if(pLspCtrlBlock->Fec.u2AddrFmly==LDP_ADDR_TYPE_IPV6)
    {
	    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) LDP_IPV6_U4_ADDR(FecElmnt.Prefix), u4Offset,
				    LDP_IPV6ADR_LEN) == CRU_FAILURE)
	    {
		    LDP_DBG (LDP_ADVT_MEM,
				    "ADVT: Error copying FecElement-AddrPrefix to Buf chain\n");
		    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
		    return LDP_FAILURE;
	    }
    }
    else
#endif
    {
	    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecElmnt.Prefix.u4Addr, u4Offset,
				    LDP_IPV4ADR_LEN) == CRU_FAILURE)
	    {
		    LDP_DBG (LDP_ADVT_MEM,
				    "ADVT: Error copying FecElement-AddrPrefix to Buf chain\n");
		    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
		    return LDP_FAILURE;
	    }
    }
    u4Offset += u1PadPrefixLen;

    if (pLspCtrlBlock->u1LblType == LDP_ATM_LABEL)
    {
        AtmLblTlv.AtmLblTlvHdr.u2TlvType = OSIX_HTONS (LDP_ATM_LABEL_TLV);
        AtmLblTlv.AtmLblTlvHdr.u2TlvLen = OSIX_HTONS (LDP_LABEL_LEN);
        AtmLblTlv.u2Vpi = OSIX_HTONS ((pLspCtrlBlock->UStrLabel.AtmLbl.u2Vpi |
                                       LDP_ATM_V_BITS));
        AtmLblTlv.u2Vci = OSIX_HTONS (pLspCtrlBlock->UStrLabel.AtmLbl.u2Vci);

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AtmLblTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Label Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }
    else if (pLspCtrlBlock->u1LblType == LDP_GEN_LABEL)
    {
        /* Platform based */

        GenLblTlv.GenLblTlvHdr.u2TlvType = OSIX_HTONS (LDP_GEN_LABEL_TLV);
        GenLblTlv.GenLblTlvHdr.u2TlvLen = OSIX_HTONS (LDP_LABEL_LEN);
        GenLblTlv.u4GenLbl = OSIX_HTONL (pLspCtrlBlock->UStrLabel.u4GenLbl);

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &GenLblTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying Label Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_LABEL_LEN);

    /* Construct Label Requeast Msg Id Tlv */
    ReqMsgIdTlv.u4LblReqMsgId = OSIX_HTONL (pLspCtrlBlock->u4UStrLblReqId);

    LDP_DBG4(LDP_ADVT_PRCS, "ADVT: %s:%d (%x)->u4UStrLblReqId=%x\n",__FUNCTION__,__LINE__,
		    pLspCtrlBlock,LCB_UREQ_ID (pLspCtrlBlock));

    if (LDP_LBL_DISTR_TYPE(SSN_GET_ENTITY (pLspCtrlBlock->pUStrSession)) == LDP_DSTR_UNSOLICIT)
    {
	    if (!((gi4MplsSimulateFailure == LDP_SIM_FAILURE_HOLD_TRANSACTION) ||
				    (gi4MplsSimulateFailure == LDP_SIM_FAILURE_PROPAGATE_LBL_RELEASE)))
	    {
		    pLspCtrlBlock->u4UStrLblReqId = LDP_INVALID_REQ_ID;
	    }
    }

    LDP_DBG4(LDP_ADVT_PRCS, "ADVT: %s:%d (%x)->u4UStrLblReqId=%x\n",__FUNCTION__,__LINE__,
                    pLspCtrlBlock,LCB_UREQ_ID (pLspCtrlBlock));


    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &ReqMsgIdTlv, u4Offset,
                            (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN)) ==
                    CRU_FAILURE)
    {
            LDP_DBG (LDP_ADVT_MEM,
                            "ADVT: Failed copying ReqMsgId Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
    }

    LDP_DBG4(LDP_ADVT_PRCS, "ADVT: %s:%d (%x)->u4UStrLblReqId=%x\n",__FUNCTION__,__LINE__,
                    pLspCtrlBlock,LCB_UREQ_ID (pLspCtrlBlock));



    u4Offset += (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN);

    if (u1HcPrsnt == LDP_TRUE)
    {
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &HopCountTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying HC Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_HC_VALUE_LEN);
    }
    /* As Per RFC 5036, A.2.8, If an intermediate LSR receives
     * Label Map message without HOP count TLV, It should not send
     * Label map message with PATH VECTOR TLV
     * */

    /* As per RFC 5036, section 2.8.2, If LSR is egress, the label mapping message
     * need not include a path vector TLV
     * */

    if ((pLspCtrlBlock->pDStrSession != NULL) &&
        (SSN_GET_LOOP_DETECT (pLspCtrlBlock->pUStrSession) == LDP_TRUE) &&
        (u1HcValue != LDP_INVALID_HOP_COUNT))
    {
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &PVTlvHdr, u4Offset,
                                       LDP_TLV_HDR_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying PVTlv Hdr to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_TLV_HDR_LEN;

        if ((pLspCtrlBlock->pDStrSession != NULL) && (u1PvCount != LDP_ZERO))
        {
            /* LSR is Not Egress */

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) pu1PvList, u4Offset,
                                           (u1PvCount * LDP_LSR_ID_LEN)) ==
                CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying PV List to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += (u1PvCount * LDP_LSR_ID_LEN);
        }
        /* Copy Local LSR ID */
        if (CRU_BUF_Copy_OverBufChain (pMsg,
                                       (UINT1 *)
                                       &SSN_GET_LSR_ID (pLspCtrlBlock->
                                                        pUStrSession), u4Offset,
                                       LDP_LSR_ID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Local LsrId to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLspCtrlBlock->pUStrSession);

#ifdef MPLS_IPV6_WANTED
    if(pLspCtrlBlock->Fec.u2AddrFmly==LDP_ADDR_TYPE_IPV6)
    {
	    LDP_DBG2 (LDP_ADVT_TX,
			    "Label Mapping Msg for the prefix %s/%d \n",
			    Ip6PrintAddr(&FecElmnt.Prefix.Ip6Addr),
			    FecElmnt.u1PreLen);
    }
    else
#endif
    {

	    u4Temp = OSIX_HTONL (FecElmnt.Prefix.u4Addr);
	    MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);
	    LDP_DBG5 (LDP_ADVT_TX,
			    "Label Mapping Msg for the prefix %d.%d.%d.%d/%d \n",
			    au1IpAddr[3], au1IpAddr[2], au1IpAddr[1], au1IpAddr[0],
			    FecElmnt.u1PreLen);
    }

    LDP_DBG4 (LDP_ADVT_TX,
              "ADVT: Sending Label Mapping Msg To %d.%d.%d.%d \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);
        /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLspCtrlBlock->pUStrSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLspCtrlBlock->pUStrSession->pLdpPeer->pLdpEntity),
                    LDP_FALSE,
                    ConnId,
                    LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        /*MPLS_IPv6 add end*/
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Label Mapping Msg\n");
        return LDP_FAILURE;
    }
    if (pLspCtrlBlock->pDStrSession == NULL)
    {
        /* LSR is Egress */

        (SSN_GET_LBL_TYPE (LCB_USSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ETHERNET_TYPE);

        (pLspCtrlBlock->u4OutIfIndex) = LDP_ZERO;
    }
    else
    {
        /* LSR is Intermediate */

        (SSN_GET_LBL_TYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ETHERNET_TYPE);

        (SSN_GET_LBL_TYPE (LCB_USSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ETHERNET_TYPE);
    }
    LDP_DBG1 (LDP_ADVT_TX, "ADVT: Label Mapping Msg sent with label %d\n",
              pLspCtrlBlock->UStrLabel.u4GenLbl);

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendLblAbortReqMsg                                     */
/* Description   : This routine constructs and sends the Label Abort Request */
/*                 Message.                                                  */
/*                 Routine is called, whenever an LSR received a Label Abort */
/*                 Request from upstream or when the next hop for FEC has    */
/*                 changed.                                                  */
/* Input(s)      : pLspCtrlBlock - Points to the current Lsp Control Block.  */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS     if Message is sent successfully else      */
/*                 LDP_FAILURE                                               */
/*****************************************************************************/

UINT1
LdpSendLblAbortReqMsg (tLspCtrlBlock * pLspCtrlBlock)
{
    tFec                FecElmnt;
    UINT1               u1PadPrefixLen =
        LDP_GET_PADDED_PRFX_LEN (pLspCtrlBlock->Fec.u1PreLen);
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tLblReqMsgIdTlv     LblReqMsgIdTlv = { {OSIX_HTONS (LDP_LBL_REQ_MSGID_TLV),
                                            OSIX_HTONS (LDP_MSG_ID_LEN)}, 0
    };
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_ABORT_REQ_MSG), 0, 0 };
    UINT2               u2MandParamLen = 0;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    UINT4               u4Offset = 0;
    tLdpId              PeerLdpId;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;

    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);
    MEMSET (&FecElmnt, 0, sizeof (tFec));
    /*MPLS_IPv6 add start*/
    LdpIfAddrZero.Addr.u4Addr = LDP_ZERO;
    /*MPLS_IPv6 add end*/
    if (pLspCtrlBlock->pDStrSession == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't send Lbl Abort Req Msg if Downstream Session"
                 "not available\n");
        return LDP_FAILURE;
    }

    FecElmnt.u1FecElmntType = pLspCtrlBlock->Fec.u1FecElmntType;
    if (FecElmnt.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
    {
        FecTlvHdr.u2TlvLen = OSIX_HTONS (CRLSP_FEC_ELMNT_LEN);
        u2MandParamLen += (2 * LDP_MSG_ID_LEN + LDP_TLV_HDR_LEN +
                           LDP_FEC_ELMNT_HDRLEN + CRLSP_FEC_ELMNT_LEN);
    }
    else
    {
        FecElmnt.u2AddrFmly = OSIX_HTONS (pLspCtrlBlock->Fec.u2AddrFmly);
        if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_HOSTADDR_TYPE)
        {
            /* When the Fec Element Type is HostAddress, the Address Length
             * should be in Octect Length. 
             * u1PrefLen has the length in bits.
             * It is divided by 8 (right shift 3 times), to get the length
             * in Octect Length.*/
            FecElmnt.u1PreLen = (UINT1) ((pLspCtrlBlock->Fec.u1PreLen) >> 3);
        }
        else
        {
            FecElmnt.u1PreLen = pLspCtrlBlock->Fec.u1PreLen;
	}

	LdpCopyAddr(&FecElmnt.Prefix,&pLspCtrlBlock->Fec.Prefix,pLspCtrlBlock->Fec.u2AddrFmly);

	if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV4)
	{
		FecElmnt.Prefix.u4Addr = OSIX_HTONL (FecElmnt.Prefix.u4Addr);
	}



	FecTlvHdr.u2TlvLen =
            OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PadPrefixLen));
        u2MandParamLen +=
            (2 * LDP_MSG_ID_LEN + 2 * LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
             u1PadPrefixLen);
    }

    pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2MandParamLen), LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buf for Lbl Abort Req Msg \n");
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS (u2MandParamLen);
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLspCtrlBlock->pDStrSession));
    MsgHdrAndId.u4MsgId =
        OSIX_HTONL (SSN_GET_MSGID (pLspCtrlBlock->pDStrSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    if (FecElmnt.u1FecElmntType != LDP_FEC_CRLSP_TYPE)
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.u2AddrFmly, u4Offset,
             LDP_UINT2_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying FecElement-AddrFamily to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT2_LEN;

        if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1PreLen, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying FecElement-PrefixLen to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
	u4Offset += LDP_UINT1_LEN;

#ifdef MPLS_IPV6_WANTED
	if(pLspCtrlBlock->Fec.u2AddrFmly==LDP_ADDR_TYPE_IPV6)
	{
		if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) LDP_IPV6_U4_ADDR(FecElmnt.Prefix), u4Offset,
					LDP_IPV6ADR_LEN) == CRU_FAILURE)
		{
			LDP_DBG (LDP_ADVT_MEM,
					"ADVT: Error copying FecElement-AddrPrefix to Buf chain\n");
			CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
			return LDP_FAILURE;
		}
	}
	else
#endif
	{
		if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecElmnt.Prefix.u4Addr, u4Offset,
					LDP_IPV4ADR_LEN) == CRU_FAILURE)
		{
			LDP_DBG (LDP_ADVT_MEM,
					"ADVT: Error copying FecElement-AddrPrefix to Buf chain\n");
			CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
			return LDP_FAILURE;
		}
	}

	u4Offset += u1PadPrefixLen;
    }

    LblReqMsgIdTlv.u4LblReqMsgId = OSIX_HTONL (pLspCtrlBlock->u4DStrLblReqId);
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &LblReqMsgIdTlv,
                                   u4Offset,
                                   (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying LblReqMsgId Tlv to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLspCtrlBlock->pDStrSession);

#ifdef MPLS_IPV6_WANTED
    if(pLspCtrlBlock->Fec.u2AddrFmly==LDP_ADDR_TYPE_IPV6)
    {
	    LDP_DBG2 (LDP_ADVT_TX,
			    "Label Abort Msg for the prefix %s/%d \n",
			    Ip6PrintAddr(&FecElmnt.Prefix.Ip6Addr),
			    FecElmnt.u1PreLen);
    }
    else
#endif
    {
	    u4Temp = OSIX_HTONL (FecElmnt.Prefix.u4Addr);
	    MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);
	    LDP_DBG5 (LDP_ADVT_TX,
			    "Label Abort Msg for the prefix %d.%d.%d.%d/%d \n",
			    au1IpAddr[3], au1IpAddr[2], au1IpAddr[1], au1IpAddr[0],
			    FecElmnt.u1PreLen);
    }

    LDP_DBG4 (LDP_ADVT_TX,
		    "ADVT: Sending Label Abort Msg To %d.%d.%d.%d \n",
		    PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLspCtrlBlock->pDStrSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLspCtrlBlock->pDStrSession->pLdpPeer->pLdpEntity),
                    LDP_FALSE,
                    ConnId,
                    LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        /*MPLS_IPv6 add end*/
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Label Abort Request Msg\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_ADVT_TX, "ADVT: Abort Req Msg sent\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendLblRelMsg                                          */
/* Description   : This routine constructs and sends the Label Release       */
/*                 Message.                                                  */
/*                                                                           */
/* Input(s)        pFecElement - Points to the FEC for which this Label      */
/*                               Release Message is being generated. This    */
/*                               Fec could be of type,Wild Card or Prefix or */
/*                               Host Address.                               */
/*                 pLabel - Points to the label to be released. If this value*/
/*                          is NULL then all the labels associated with the  */
/*                          FEC mentioned, are to be released.               */
/*                 pSessionEntry  - Points to the session on which the Label */
/*                                  Release Message has to be sent over.     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS     if Message is sent successfully else      */
/*                 LDP_FAILURE                                               */
/*****************************************************************************/

UINT1
LdpSendLblRelMsg (tFec * pFecElement, uLabel * pLabel,
                  tLdpSession * pSessionEntry)
{
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_RELEASE_MSG), 0, 0 };
    tFec                FecElmnt;
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tGenLblTlv          GenLabelTlv = { {OSIX_HTONS (LDP_GEN_LABEL_TLV),
                                         OSIX_HTONS (LDP_LABEL_LEN)}, 0
    };
    tAtmLblTlv          AtmLabelTlv = { {OSIX_HTONS (LDP_ATM_LABEL_TLV),
                                         OSIX_HTONS (LDP_LABEL_LEN)}, 0, 0
    };
    tStatusTlv          StatusTlv = { {OSIX_HTONS (LDP_OTHER_MSG_STATUS_TLV),
                                       OSIX_HTONS ((LDP_STATUS_CODE_LEN +
                                                    LDP_MSG_ID_LEN +
                                                    LDP_MSG_TYPE_LEN))}, 0, 0,
    0, 0
    };
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    /* This is in case of Wild card Fec */
    UINT2               u2FecElmntLen = LDP_FEC_TYPE_LEN;
    UINT2               u2MandParamLen = 0;
    UINT2               u2OptParamLen = 0;
    UINT1               u1PadPrefixLen =
        LDP_GET_PADDED_PRFX_LEN (pFecElement->u1PreLen);
    UINT1               u1IsAtmMode;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    UINT4               u4Offset = 0;
    tLdpId              PeerLdpId;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;

    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);
    MEMSET (&FecElmnt, 0, sizeof (tFec));


    /*MPLS_IPv6 add start*/
    LdpIfAddrZero.Addr.u4Addr = LDP_ZERO;
    /*MPLS_IPv6 add end*/
    u2MandParamLen += LDP_MSG_ID_LEN;
    u1IsAtmMode = LDP_IS_ENTITY_TYPE_ATM (pSessionEntry);

    /* As per RFC 5036 section 3.4.1, Inclusion of wild card FEC in
     * Label Release message
     * */

    if (gi4MplsSimulateFailure == LDP_SIM_FAILURE_WILDCARD_FEC)
    {
        FecElmnt.u1FecElmntType = LDP_FEC_WILDCARD_TYPE;
    }
    else
    {
        FecElmnt.u1FecElmntType = pFecElement->u1FecElmntType;
    }

    if (FecElmnt.u1FecElmntType != LDP_FEC_WILDCARD_TYPE)
    {
        if (FecElmnt.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
        {
            u2FecElmntLen = CRLSP_FEC_ELMNT_LEN;
        }
        else
        {
            FecElmnt.u2AddrFmly = OSIX_HTONS (pFecElement->u2AddrFmly);
            if (FecElmnt.u1FecElmntType == LDP_FEC_HOSTADDR_TYPE)
            {
                /* When the Fec Element Type is HostAddress, the Address Length
                 * should be in Octect Length. 
                 * u1PrefLen has the length in bits.
                 * It is divided by 8 (right shift 3 times), to get the length
                 * in Octect Length.*/
                FecElmnt.u1PreLen = (UINT1) ((pFecElement->u1PreLen) >> 3);
            }
            else
            {
                FecElmnt.u1PreLen = pFecElement->u1PreLen;
            }
#ifdef MPLS_IPV6_WANTED
	    if(pFecElement->u2AddrFmly==LDP_ADDR_TYPE_IPV6)
	    {

		    MEMCPY(&FecElmnt.Prefix.Ip6Addr,&pFecElement->Prefix.Ip6Addr,LDP_IPV6ADR_LEN);

	    }
	    else
#endif
	    {
		    LDP_IPV4_U4_ADDR(FecElmnt.Prefix) = OSIX_HTONL (LDP_IPV4_U4_ADDR(pFecElement->Prefix));
	    }


            u2FecElmntLen = (UINT2) (LDP_FEC_ELMNT_HDRLEN + u1PadPrefixLen);
        }
    }
    FecTlvHdr.u2TlvLen = OSIX_HTONS (u2FecElmntLen);
    u2MandParamLen += (LDP_TLV_HDR_LEN + u2FecElmntLen);

    if (pLabel != NULL)
    {
        if (u1IsAtmMode == LDP_TRUE)
        {
            AtmLabelTlv.u2Vpi = OSIX_HTONS (pLabel->AtmLbl.u2Vpi);
            AtmLabelTlv.u2Vci = OSIX_HTONS (pLabel->AtmLbl.u2Vci);
        }
        else
        {
            /* Generic Label */
            GenLabelTlv.u4GenLbl = OSIX_HTONL (pLabel->u4GenLbl);
        }
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_LABEL_LEN);
    }

    if (pSessionEntry->u1StatusCode != LDP_STAT_TYPE_SUCCESS)
    {
        StatusTlv.u4StatusCode = OSIX_HTONL
            (LdpConvertLocalValToTLVVal (pSessionEntry->u1StatusCode));
        StatusTlv.u2MsgType = 0;
        StatusTlv.u4MsgId = 0;
        u2OptParamLen +=
            (LDP_TLV_HDR_LEN + LDP_STATUS_CODE_LEN + LDP_MSG_ID_LEN +
             LDP_MSG_TYPE_LEN);
    }

    pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2OptParamLen + u2MandParamLen),
                                         LDP_PDU_HDR_LEN);

    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buf for Lbl Release Msg \n");
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2MandParamLen + u2OptParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pSessionEntry));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(FecTlvHdr), u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    if (FecElmnt.u1FecElmntType != LDP_FEC_WILDCARD_TYPE)
    {
        if (FecElmnt.u1FecElmntType != LDP_FEC_CRLSP_TYPE)
        {
            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &FecElmnt.u2AddrFmly, u4Offset,
                 LDP_UINT2_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying FecElement-AddrFamily "
                         "to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT2_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1PreLen,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying FecElement-PrefixLen"
                         " to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
	    u4Offset += LDP_UINT1_LEN;

#ifdef MPLS_IPV6_WANTED
	    if(pFecElement->u2AddrFmly==LDP_ADDR_TYPE_IPV6)
	    {
		    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) LDP_IPV6_U4_ADDR(FecElmnt.Prefix), u4Offset,
					    LDP_IPV6ADR_LEN) == CRU_FAILURE)
		    {
			    LDP_DBG (LDP_ADVT_MEM,
					    "ADVT: Error copying FecElement-AddrPrefix to Buf chain\n");
			    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
			    return LDP_FAILURE;
		    }
	    }
	    else
#endif
	    {
		    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecElmnt.Prefix.u4Addr, u4Offset,
					    LDP_IPV4ADR_LEN) == CRU_FAILURE)
		    {
			    LDP_DBG (LDP_ADVT_MEM,
					    "ADVT: Error copying FecElement-AddrPrefix to Buf chain\n");
			    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
			    return LDP_FAILURE;
		    }
	    }

	    u4Offset += u1PadPrefixLen;
	}
    }

    if (pLabel != NULL)
    {
        if (u1IsAtmMode == LDP_TRUE)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AtmLabelTlv,
                                           u4Offset,
                                           (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
                CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Atm Label to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
        }
        else
        {
            /* Copy the generic label */
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &GenLabelTlv,
                                           u4Offset,
                                           (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
                CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Generic Lbl to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
        }
        u4Offset += (LDP_MSG_HDR_LEN + LDP_LABEL_LEN);
    }

    if (pSessionEntry->u1StatusCode != LDP_STAT_TYPE_SUCCESS)
    {
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &StatusTlv, u4Offset,
                                       LDP_TLV_HDR_LEN + LDP_STATUS_CODE_LEN +
                                       LDP_MSG_ID_LEN + LDP_MSG_TYPE_LEN) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_NOTIF_MEM,
                     "NOTIF: Error while copying Status Tlv to buffer\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pSessionEntry);

#ifdef MPLS_IPV6_WANTED
    if(pFecElement->u2AddrFmly==LDP_ADDR_TYPE_IPV6)
    {
	    LDP_DBG2 (LDP_ADVT_TX,
			    "Label Req Msg for the prefix %s/%d \n",
			    Ip6PrintAddr(&FecElmnt.Prefix.Ip6Addr),
			    FecElmnt.u1PreLen);
    }
    else
#endif
    {
	    u4Temp = OSIX_HTONL (FecElmnt.Prefix.u4Addr);
	    MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);
	    LDP_DBG5 (LDP_ADVT_TX,
			    "Label Req Msg for the prefix %d.%d.%d.%d/%d \n",
			    au1IpAddr[3], au1IpAddr[2], au1IpAddr[1], au1IpAddr[0],
			    FecElmnt.u1PreLen);
    }

    if (pLabel != NULL)
    {
        LDP_DBG5 (LDP_ADVT_TX,
                  "ADVT: Sending Label Release Msg To %d.%d.%d.%d to release %d\n",
                  PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3],
                  pLabel->u4GenLbl);
    }

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pSessionEntry->pLdpPeer->pLdpEntity), LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
            /*MPLS_IPv6 add end*/
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Lbl Release Msg\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    pSessionEntry->u1StatusCode = LDP_STAT_TYPE_SUCCESS;

    LDP_DBG (LDP_ADVT_TX, "ADVT: Label Release Msg sent\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendLblWithdrawMsg                                     */
/* Description   : This routine constructs and sends the Label Withdraw      */
/*                 Message.                                                  */
/* Input(s)        pFecElement - Points to the FEC for which this Label      */
/*                              Withdraw Message is being generated. This    */
/*                               Fec could be of type,Wild Card or Prefix or */
/*                               Host Address.                               */
/*                 pLabel - Points to the label to be withdrawn.If this value*/
/*                          is NULL then all the labels associated with the  */
/*                          FEC mentioned, are to be withdrawn.              */
/*                 pSessionEntry  - Points to the session on which the Label */
/*                                 Withdraw Message has to be sent over.     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS     if Message is sent successfully else      */
/*                 LDP_FAILURE                                               */
/*****************************************************************************/

UINT1
LdpSendLblWithdrawMsg (tFec * pFecElement, uLabel * pLabel,
                       tLdpSession * pSessionEntry)
{

    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_WITHDRAW_MSG), 0, 0 };
    tFec                FecElmnt;

    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tGenLblTlv          GenLabelTlv = { {OSIX_HTONS (LDP_GEN_LABEL_TLV),
                                         OSIX_HTONS (LDP_LABEL_LEN)}, 0
    };
    tAtmLblTlv          AtmLabelTlv = { {OSIX_HTONS (LDP_ATM_LABEL_TLV),
                                         OSIX_HTONS (LDP_LABEL_LEN)}, 0, 0
    };
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    /* This is in case of Wild card Fec */
    UINT2               u2FecElmntLen = LDP_FEC_TYPE_LEN;
    UINT2               u2MandParamLen = 0;
    UINT2               u2OptParamLen = 0;
    UINT1               u1PadPrefixLen =
        LDP_GET_PADDED_PRFX_LEN (pFecElement->u1PreLen);
    UINT1               u1IsAtmMode = 0;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    UINT4               u4Offset = 0;
    tLdpId              PeerLdpId;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;

    u2MandParamLen += LDP_MSG_ID_LEN;
    u1IsAtmMode = LDP_IS_ENTITY_TYPE_ATM (pSessionEntry);
    MEMSET (&FecElmnt, 0, sizeof (tFec));
#ifdef LDP_GR_WANTED
    if (pSessionEntry->u1StaleStatus == LDP_TRUE)
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpSendLblWithdrawMsg: LBL WDRAW Msg not sent "
                                 "Since the Session is STALE\n");
        return LDP_SUCCESS;
    }
#endif
    /*MPLS_IPv6 add start*/
    LdpIfAddrZero.Addr.u4Addr = LDP_ZERO;
    /*MPLS_IPv6 add end*/
	
    /* As per RFC 5036 section 3.4.1, Inclusion of wild card FEC in
     * Label Withdraw  message
     * */

    if (gi4MplsSimulateFailure == LDP_SIM_FAILURE_WILDCARD_FEC)
    {
        FecElmnt.u1FecElmntType = LDP_FEC_WILDCARD_TYPE;
    }
    else
    {
        FecElmnt.u1FecElmntType = pFecElement->u1FecElmntType;
    }

    if (FecElmnt.u1FecElmntType != LDP_FEC_WILDCARD_TYPE)
    {
        if (FecElmnt.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
        {
            u2FecElmntLen = CRLSP_FEC_ELMNT_LEN;
        }
        else
        {
            FecElmnt.u2AddrFmly = OSIX_HTONS (pFecElement->u2AddrFmly);
            if (FecElmnt.u1FecElmntType == LDP_FEC_HOSTADDR_TYPE)
            {
                /* When the Fec Element Type is HostAddress, the Address Length
                 * should be in Octect Length. 
                 * u1PrefLen has the length in bits.
                 * It is divided by 8 (right shift 3 times), to get the length
                 * in Octect Length.*/
                FecElmnt.u1PreLen = (UINT1) ((pFecElement->u1PreLen) >> 3);
            }
            else
            {
                FecElmnt.u1PreLen = pFecElement->u1PreLen;
            }

#ifdef MPLS_IPV6_WANTED
	    if(pFecElement->u2AddrFmly==LDP_ADDR_TYPE_IPV6)
	    {
		    MEMCPY(&FecElmnt.Prefix.Ip6Addr,&pFecElement->Prefix.Ip6Addr,LDP_IPV6ADR_LEN);
	    }
	    else
#endif
	    {
		    LDP_IPV4_U4_ADDR(FecElmnt.Prefix) = OSIX_HTONL (LDP_IPV4_U4_ADDR(pFecElement->Prefix));
	    }
	    u2FecElmntLen = (UINT2) (LDP_FEC_ELMNT_HDRLEN + u1PadPrefixLen);
        }
    }

    FecTlvHdr.u2TlvLen = OSIX_HTONS (u2FecElmntLen);
    u2MandParamLen += (LDP_TLV_HDR_LEN + u2FecElmntLen);

    if (pLabel != NULL)
    {
        if (u1IsAtmMode == LDP_TRUE)
        {
            AtmLabelTlv.u2Vpi = OSIX_HTONS (pLabel->AtmLbl.u2Vpi);
            AtmLabelTlv.u2Vci = OSIX_HTONS (pLabel->AtmLbl.u2Vci);
        }
        else
        {
            /* Generic Label */
            GenLabelTlv.u4GenLbl = OSIX_HTONL (pLabel->u4GenLbl);
        }
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_LABEL_LEN);
    }

    pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2OptParamLen + u2MandParamLen),
                                         LDP_PDU_HDR_LEN);

    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buf for Lbl Wthdraw Msg\n");
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2MandParamLen + u2OptParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pSessionEntry));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(FecTlvHdr), u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    if (FecElmnt.u1FecElmntType != LDP_FEC_WILDCARD_TYPE)
    {
        if (FecElmnt.u1FecElmntType != LDP_FEC_CRLSP_TYPE)
        {
            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &FecElmnt.u2AddrFmly, u4Offset,
                 LDP_UINT2_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying FecElement-AddrFamily"
                         " to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT2_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, &FecElmnt.u1PreLen,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying FecElement-PrefixLen to"
                         " Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
	    u4Offset += LDP_UINT1_LEN;

#ifdef MPLS_IPV6_WANTED
	    if(pFecElement->u2AddrFmly==LDP_ADDR_TYPE_IPV6)
	    {
		    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) LDP_IPV6_U4_ADDR(FecElmnt.Prefix), u4Offset,
					    LDP_IPV6ADR_LEN) == CRU_FAILURE)
		    {
			    LDP_DBG (LDP_ADVT_MEM,
					    "ADVT: Error copying FecElement-AddrPrefix to Buf chain\n");
			    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
			    return LDP_FAILURE;
		    }
	    }
	    else
#endif
	    {
		    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecElmnt.Prefix.u4Addr, u4Offset,
					    LDP_IPV4ADR_LEN) == CRU_FAILURE)
		    {
			    LDP_DBG (LDP_ADVT_MEM,
					    "ADVT: Error copying FecElement-AddrPrefix to Buf chain\n");
			    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
			    return LDP_FAILURE;
		    }
	    }

            u4Offset += u1PadPrefixLen;
        }
    }

    if (pLabel != NULL)
    {
        if (u1IsAtmMode == LDP_TRUE)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AtmLabelTlv,
                                           u4Offset,
                                           (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
                CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Atm Label to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
        }
        else
        {
            /* Copy the generic label */
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &GenLabelTlv,
                                           u4Offset,
                                           (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
                CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Generic Lbl to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
        }
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pSessionEntry);

#ifdef MPLS_IPV6_WANTED
    if(pFecElement->u2AddrFmly==LDP_ADDR_TYPE_IPV6)
    {
	    LDP_DBG2 (LDP_ADVT_TX,
			    "Label Req Msg for the prefix %s/%d \n",
			    Ip6PrintAddr(&FecElmnt.Prefix.Ip6Addr),
			    FecElmnt.u1PreLen);
    }
    else
#endif
    {
	    u4Temp = OSIX_HTONL (FecElmnt.Prefix.u4Addr);
	    MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);
	    LDP_DBG5 (LDP_ADVT_TX,
			    "Label Req Msg for the prefix %d.%d.%d.%d/%d \n",
			    au1IpAddr[3], au1IpAddr[2], au1IpAddr[1], au1IpAddr[0],
			    FecElmnt.u1PreLen);
    }

    if (pLabel != NULL)
    {
        LDP_DBG5 (LDP_ADVT_TX,
                  "ADVT: Sending Label Withdraw Msg to %d.%d.%d.%d to withdraw label %d\n",
                  PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3],
                  pLabel->u4GenLbl);
    }

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pSessionEntry->pLdpPeer->pLdpEntity), LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
            /*MPLS_IPv6 add end*/
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Lbl Wthdraw Msg \n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_ADVT_TX, "ADVT: Label Withdraw Msg sent\n");
    return LDP_SUCCESS;
}

/* Routines for sending the CR-LDP related Request and Mapping Messages */

/*****************************************************************************/
/* Function Name : LdpSendCrlspLblReqMsg                                     */
/* Description   : This routine constructs and sends the Label Request       */
/*                 Message when the LSP to be created is CRLSP in nature.    */
/*                 This routine is invoked as a result of the processing of  */
/*                 Label Request Message received from upstream for an CRLSP */
/*                 setup or internal CRLSP tunnel setup event in case of     */
/*                 Ingress LSR.                                              */
/* Input(s)      : pLspCtrlBlock - Points to the current Lsp Control Block.  */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS if Label Request Message is sent successfully */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpSendCrlspLblReqMsg (tLspCtrlBlock * pLspCtrlBlock)
{
    UINT2               u2OptParamLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT2               u2ErTlvParamLen = 0;
    UINT4               u4Offset = 0;
    UINT2               u2EHTlvLen = 0;
    UINT1              *pu1ErHopTlv = NULL;
    tLdpTeHopInfo      *pNHInfo = NULL;

    tCrlspLspIdTlv      LspIdTlv;
    tTlvHdr             ErTlvHdr = { OSIX_HTONS (CRLDP_EXPLROUTE_TLV), 0 };
    tErHopIpv4Tlv       ErHopIpv4Tlv;
    tErHopAsTlv         ErHopAsTlv;
    tErHopLspidTlv      ErHopLspidTlv;
    tCrlspTrfcParmsTlv  TrfParamsTlv;
    tRoutePinningTlv    RtPinTlv;
    tCrlspResClsTlv     RsrcClsTlv = { {OSIX_HTONS (CRLDP_RESCLS_TLV),
                                        OSIX_HTONS (LDP_RSRCLASS_TLV_LEN)}, 0
    };
    tCrlspPreemptTlv    PreEmptTlv = { {OSIX_HTONS (CRLDP_PREEMPT_TLV),
                                        OSIX_HTONS (LDP_PREMPT_TLV_LEN)}, 0, 0,
    0
    };
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_REQUEST_MSG), 0, 0 };
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tCrlspTnlInfo      *pCrLsp = pLspCtrlBlock->pCrlspTnlInfo;
    tTlvHdr             FecTlvHdr;
    tFec                CrLspFecElmnt;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    UINT1               u1PadPrefixLen;
    tLdpId              PeerLdpId;

    /*MPLS_IPv6 add start*/
    LdpIfAddrZero.Addr.u4Addr = LDP_ZERO;
    /*MPLS_IPv6 add end*/
	
    LspIdTlv.TlvHdr.u2TlvType = OSIX_HTONS (CRLDP_LSPID_TLV);
    LspIdTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_LSPID_TLV_LEN);
    ErHopIpv4Tlv.TlvHdr.u2TlvType = OSIX_HTONS (CRLDP_ERHOP_TY_IPV4);
    ErHopIpv4Tlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_IPV4_ERHOP_TLV_LEN);
    ErHopAsTlv.TlvHdr.u2TlvType = OSIX_HTONS (CRLDP_ER_HOP_ASNUM);
    ErHopAsTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_ERHOP_AS_TLV_LEN);
    ErHopLspidTlv.TlvHdr.u2TlvType = OSIX_HTONS (CRLDP_ER_HOP_LSPID);
    ErHopLspidTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_ERHOP_LSPID_TLV_LEN);
    TrfParamsTlv.TlvHdr.u2TlvType = OSIX_HTONS (CRLDP_TRAFPARM_TLV);
    TrfParamsTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_TRF_PARAMS_TLV_LEN);
    RtPinTlv.TlvHdr.u2TlvType = OSIX_HTONS (CRLDP_PINNING_TLV);
    RtPinTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_RTPIN_TLV_LEN);

    if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
    {
        FecTlvHdr.u2TlvType = OSIX_HTONS (LDP_FEC_TLV);
        FecTlvHdr.u2TlvLen = OSIX_HTONS (CRLSP_FEC_ELMNT_LEN);

        /* Assigning the Fec Element type to CR-LSP */
        CrLspFecElmnt.u1FecElmntType = LDP_FEC_CRLSP_TYPE;
    }
    else
    {
        u1PadPrefixLen = LDP_GET_PADDED_PRFX_LEN (pLspCtrlBlock->Fec.u1PreLen);
        FecTlvHdr.u2TlvType = OSIX_HTONS (LDP_FEC_TLV);
        FecTlvHdr.u2TlvLen =
            OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PadPrefixLen));
    }

    u2MandParamLen += LDP_MSG_ID_LEN;

    /* Updating for the mandatory 'Fec Tlv' length */
    u2MandParamLen += (LDP_TLV_HDR_LEN + CRLSP_FEC_ELMNT_LEN);

    /* Updating for the mandatory 'LspId Tlv' length */
    u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_LSPID_TLV_LEN);

    if ((CRLSP_PARM_FLAG (LCB_TNLINFO (pLspCtrlBlock)) & LDP_DIFFSERV_CRLSP)
        == LDP_DIFFSERV_CRLSP)
    {
        LdpDiffServGetOptionalDSTlvLength (LCB_TNLINFO (pLspCtrlBlock),
                                           &u2OptParamLen);
    }

    if ((CRLSP_PARM_FLAG (LCB_TNLINFO (pLspCtrlBlock)) & LDP_RSRC_PEROA_CRLSP)
        == LDP_RSRC_PEROA_CRLSP)
    {
        LdpDiffServGetOptionalElspTPLength (LCB_TNLINFO (pLspCtrlBlock),
                                            &u2OptParamLen);
    }

    if ((CRLSP_PARM_FLAG (LCB_TNLINFO (pLspCtrlBlock)) & LDP_CLASSTYPE_CRLSP)
        == LDP_CLASSTYPE_CRLSP)
    {
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_DS_CLASSTYPE_TLV_LEN);
    }

    /* Checking if flag is set for Er-Hop Tlv. */
    if (pCrLsp->u2Flag & LDP_ER_CRLSP)
    {
        TMO_SLL_Scan (&CRLSP_ERHOP_LIST (pCrLsp), pNHInfo, tLdpTeHopInfo *)
        {
            /* Incrementing the length basing on the Er-Hop Type */
            switch (CRLSP_TE_ERHOP_ADDR_TYPE (pNHInfo))
            {
                case CRLSP_TE_ERHOP_IPV4_TYPE:
                    u2ErTlvParamLen +=
                        (LDP_TLV_HDR_LEN + LDP_IPV4_ERHOP_TLV_LEN);
                    break;

                case CRLSP_TE_ERHOP_IPV6_TYPE:
                    u2ErTlvParamLen +=
                        (LDP_TLV_HDR_LEN + LDP_IPV6_ERHOP_TLV_LEN);
                    break;

                case CRLSP_TE_ERHOP_ASNUM_TYPE:
                    u2ErTlvParamLen += (LDP_TLV_HDR_LEN + LDP_ERHOP_AS_TLV_LEN);
                    break;

                case CRLSP_TE_ERHOP_LSPID_TYPE:
                    u2ErTlvParamLen +=
                        (LDP_TLV_HDR_LEN + LDP_ERHOP_LSPID_TLV_LEN);
                    break;
            }
        }
        /* The length of the total ER Hop TLVs updated in ER TLV Header */
        ErTlvHdr.u2TlvLen = OSIX_HTONS (u2ErTlvParamLen);

        /* Increment done to take care of ER-TLV Header */
        u2OptParamLen += LDP_TLV_HDR_LEN + u2ErTlvParamLen;
    }

    /* Checking if flag is set for Traffic Params Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_TR_CRLSP) == LDP_TR_CRLSP)
    {
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_TRF_PARAMS_TLV_LEN);
    }

    /* Checking if flag is set for Route Pinning Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_PN_CRLSP) == LDP_PN_CRLSP)
    {
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_RTPIN_TLV_LEN);
    }

    /* Checking if flag is set for Resource Class Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_RSRC_CRLSP) == LDP_RSRC_CRLSP)
    {
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_RSRCLASS_TLV_LEN);
    }

    /* Checking if flag is set for Pre-Emption Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_PREMPT_CRLSP) == LDP_PREMPT_CRLSP)
    {
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_PREMPT_TLV_LEN);
    }

    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2OptParamLen + u2MandParamLen),
                                      LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Can't Allocate Buf for Lbl Req Msg \n");
        return LDP_FAILURE;
    }

    /* Get the Msg Id for this LblReqMsg and store it in Lsp Control Block */
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLspCtrlBlock->pDStrSession));
    pLspCtrlBlock->u4DStrLblReqId = SSN_GET_MSGID (pLspCtrlBlock->pDStrSession);

    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2OptParamLen + u2MandParamLen));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (pLspCtrlBlock->u4DStrLblReqId);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying LblReqMsgHdr & MsgId to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Copying the Fec Tlv Header */
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    /* Copying the CR-LSP Fec ELement */
    if (CRU_BUF_Copy_OverBufChain (pMsg, &CrLspFecElmnt.u1FecElmntType,
                                   u4Offset,
                                   CRLSP_FEC_ELMNT_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Cr-Lsp FecElement to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += CRLSP_FEC_ELMNT_LEN;

    /* Constructing and copying LspId Tlv. Check whether it's a 
     * modification of a tunnel.If so Action flag is set. 
     */
    if (CRLSP_IS_MOD_OF_TUNN (pCrLsp) == LDP_TRUE)
    {
        LspIdTlv.u2Reserved = (LDP_RSVD_FLD | CRLSP_ACT_FLAG);
    }
    else
    {
        LspIdTlv.u2Reserved = 0;
    }

    LspIdTlv.u2Reserved = OSIX_HTONS (LspIdTlv.u2Reserved);
    LspIdTlv.u2LocalLspid = OSIX_HTONS (CRLSP_LSPID (pCrLsp));
    MEMCPY (LspIdTlv.IngressLsrRtrId, CRLSP_INGRESS_LSRID (pCrLsp),
            LDP_IPV4ADR_LEN);
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &LspIdTlv, u4Offset,
                                   (LDP_TLV_HDR_LEN + LDP_LSPID_TLV_LEN))
        == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying LspId Tlv to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_LSPID_TLV_LEN);

    /* Constructing and copying Explicit Route Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_ER_CRLSP) == LDP_ER_CRLSP)
    {
        /* Copying the Explicit Route Tlv Header */
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &ErTlvHdr, u4Offset,
                                       LDP_TLV_HDR_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Explicit Route Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_TLV_HDR_LEN;

        TMO_SLL_Scan (&CRLSP_ERHOP_LIST (pCrLsp), pNHInfo, tLdpTeHopInfo *)
        {
            switch (CRLSP_TE_ERHOP_ADDR_TYPE (pNHInfo))
            {
                case CRLSP_TE_ERHOP_IPV4_TYPE:
                    ErHopIpv4Tlv.u1CrlspPathType =
                        (CRLSP_TE_ERHOP_TYPE (pNHInfo) ==
                         CRLSP_TE_STRICT_ER)
                        ? STRICT_CRLSP_PATH : LOOSE_CRLSP_PATH;
                    ErHopIpv4Tlv.u1Reserved1 = 0;
                    ErHopIpv4Tlv.u1Reserved2 = 0;
                    ErHopIpv4Tlv.u1PrefLen =
                        CRLSP_TE_ERHOP_ADDR_PRFX_LEN (pNHInfo);
                    MEMCPY (ErHopIpv4Tlv.Ipv4Address,
                            (UINT1 *) &CRLSP_TE_ERHOP_IP_ADDR (pNHInfo),
                            LDP_IPV4ADR_LEN);

                    pu1ErHopTlv = (UINT1 *) &ErHopIpv4Tlv;
                    u2EHTlvLen = (LDP_TLV_HDR_LEN + LDP_IPV4_ERHOP_TLV_LEN);
                    break;

                    /*case CRLSP_TE_ERHOP_IPV6_TYPE:
                       ErHopIpv6Tlv.u1CrlspPathType =
                       (CRLSP_TE_ERHOP_TYPE (pNHInfo) ==
                       CRLSP_TE_STRICT_ER)
                       ? STRICT_CRLSP_PATH : LOOSE_CRLSP_PATH;
                       ErHopIpv6Tlv.u1Reserved1 = 0;
                       ErHopIpv6Tlv.u1Reserved2 = 0;
                       ErHopIpv6Tlv.u1PrefLen =
                       CRLSP_TE_ERHOP_ADDR_PRFX_LEN (pNHInfo);
                       MEMCPY (ErHopIpv6Tlv.Ipv6Address,
                       (UINT1 *) &CRLSP_TE_ERHOP_IP_ADDR (pNHInfo),
                       LDP_IPV6ADR_LEN);

                       pu1ErHopTlv = (UINT1 *) &ErHopIpv6Tlv;
                       u2EHTlvLen = (LDP_TLV_HDR_LEN + LDP_IPV6_ERHOP_TLV_LEN);
                       break; */

                case CRLSP_TE_ERHOP_ASNUM_TYPE:
                    ErHopAsTlv.u1CrlspPathType =
                        (CRLSP_TE_ERHOP_TYPE (pNHInfo) ==
                         CRLSP_TE_STRICT_ER)
                        ? STRICT_CRLSP_PATH : LOOSE_CRLSP_PATH;
                    ErHopAsTlv.u1Reserved = 0;
                    ErHopAsTlv.u2AsNumber =
                        OSIX_HTONS (CRLSP_TE_ERHOP_AS_NUM (pNHInfo));

                    pu1ErHopTlv = (UINT1 *) &ErHopAsTlv;
                    u2EHTlvLen = (LDP_TLV_HDR_LEN + LDP_ERHOP_AS_TLV_LEN);
                    break;

                case CRLSP_TE_ERHOP_LSPID_TYPE:
                    ErHopLspidTlv.u1CrlspPathType =
                        (CRLSP_TE_ERHOP_TYPE (pNHInfo) ==
                         CRLSP_TE_STRICT_ER)
                        ? STRICT_CRLSP_PATH : LOOSE_CRLSP_PATH;
                    ErHopLspidTlv.u1Reserved = 0;
                    ErHopLspidTlv.u2LocalLspid =
                        OSIX_HTONS ((UINT2) (CRLSP_TE_ERHOP_LSPID (pNHInfo)));
                    MEMCPY (ErHopLspidTlv.IngressLsrRtrId,
                            (UINT1 *) &CRLSP_TE_ERHOP_IP_ADDR (pNHInfo),
                            ROUTER_ID_LENGTH);

                    pu1ErHopTlv = (UINT1 *) &ErHopLspidTlv;
                    u2EHTlvLen = (LDP_TLV_HDR_LEN + LDP_ERHOP_LSPID_TLV_LEN);
                    break;
            }
            if (pu1ErHopTlv == NULL)
            {
                LDP_DBG (LDP_ADVT_MEM, "ADVT: pu1ErHopTlv is NULL\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            if (CRU_BUF_Copy_OverBufChain
                (pMsg, pu1ErHopTlv, u4Offset, u2EHTlvLen) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying LspId Tlv to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += u2EHTlvLen;
        }
    }

    /* Constructing Traffic Params Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_TR_CRLSP) == LDP_TR_CRLSP)
    {
        TrfParamsTlv.u1Flags = CRLSP_TRF_FLAGS (pCrLsp);
        TrfParamsTlv.u1Frequency = CRLSP_TRF_FREQ (pCrLsp);
        TrfParamsTlv.u1Reserved = 0;
        TrfParamsTlv.u1Weight = CRLSP_TRF_WGT (pCrLsp);
        TrfParamsTlv.u4PeakDataRate = OSIX_HTONL (CRLSP_TRF_PD (pCrLsp));
        TrfParamsTlv.u4PeakBurstSize = OSIX_HTONL (CRLSP_TRF_PB (pCrLsp));
        TrfParamsTlv.u4CommittedDataRate = OSIX_HTONL (CRLSP_TRF_CD (pCrLsp));
        TrfParamsTlv.u4ExcessBurstSize = OSIX_HTONL (CRLSP_TRF_EB (pCrLsp));
        TrfParamsTlv.u4CommittedBurstSize = OSIX_HTONL (CRLSP_TRF_CB (pCrLsp));

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &TrfParamsTlv,
                                       u4Offset,
                                       (LDP_TLV_HDR_LEN +
                                        LDP_TRF_PARAMS_TLV_LEN)) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Traffic params Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_TRF_PARAMS_TLV_LEN);
    }

    /* Constructing Route Pinning Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_PN_CRLSP) == LDP_PN_CRLSP)
    {
        RtPinTlv.u1RoutePinningType =
            (CRLSP_RTPIN_TYPE (pCrLsp) == LDP_TRUE)
            ? CRLSP_ROUTE_PIN_ENABLE : CRLSP_ROUTE_PIN_DISABLE;
        RtPinTlv.u1Reserved = 0;
        RtPinTlv.u2Reserved = 0;
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &RtPinTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_RTPIN_TLV_LEN))
            == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Route Pinning Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_RTPIN_TLV_LEN);
    }

    /* Constructing Resource Class Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_RSRC_CRLSP) == LDP_RSRC_CRLSP)
    {
        RsrcClsTlv.u4ResourceClass = OSIX_HTONL
            (CRLSP_TE_TNL_INC_ANY_AFFINITY (CRLSP_TE_TNL_INFO (pCrLsp)));
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &RsrcClsTlv,
                                       u4Offset,
                                       (LDP_TLV_HDR_LEN +
                                        LDP_RSRCLASS_TLV_LEN)) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Resource class Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_RSRCLASS_TLV_LEN);
    }

    /* Constructing Pre-Emption Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_PREMPT_CRLSP) == LDP_PREMPT_CRLSP)
    {
        PreEmptTlv.u1SetPrio = CRLSP_SET_PRIO (pCrLsp);
        PreEmptTlv.u1HoldPrio = CRLSP_HOLD_PRIO (pCrLsp);
        PreEmptTlv.u2Reserved = 0;
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &PreEmptTlv,
                                       u4Offset,
                                       (LDP_TLV_HDR_LEN +
                                        LDP_PREMPT_TLV_LEN)) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Pre-emptive Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_PREMPT_TLV_LEN);
    }

    if ((CRLSP_PARM_FLAG (LCB_TNLINFO (pLspCtrlBlock)) & LDP_DIFFSERV_CRLSP)
        == LDP_DIFFSERV_CRLSP)
    {
        if (LdpDiffServConstructCopyDSTlv (LCB_TNLINFO (pLspCtrlBlock),
                                           pMsg, &u4Offset) == LDP_DS_FAILURE)
        {
            return LDP_FAILURE;
        }
    }

    if ((CRLSP_PARM_FLAG (LCB_TNLINFO (pLspCtrlBlock)) & (LDP_RSRC_PEROA_CRLSP))
        == LDP_RSRC_PEROA_CRLSP)
    {
        if (LdpDiffServConstructCopyElspTPTlv (LCB_TNLINFO (pLspCtrlBlock),
                                               pMsg,
                                               &u4Offset) == LDP_DS_FAILURE)
        {
            return LDP_FAILURE;
        }
    }

    if ((CRLSP_PARM_FLAG (LCB_TNLINFO (pLspCtrlBlock)) & LDP_CLASSTYPE_CRLSP)
        == LDP_CLASSTYPE_CRLSP)
    {
        if (LdpDiffServConstructCopyClassTypeTlv (LCB_TNLINFO (pLspCtrlBlock),
                                                  pMsg,
                                                  &u4Offset) == LDP_DS_FAILURE)
        {
            return LDP_FAILURE;
        }
    }

    /* Moving the offset to PDU Header start */

    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLspCtrlBlock->pDStrSession);
    LDP_DBG6 (LDP_ADVT_TX,
              "ADVT: Sending Label Request Msg To   : %d.%d.%d.%d.%d:%d \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2],
              PeerLdpId[3], PeerLdpId[4], PeerLdpId[5]);
    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLspCtrlBlock->pDStrSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLspCtrlBlock->pDStrSession->pLdpPeer->pLdpEntity),
                    LDP_FALSE,
                    ConnId,
                    LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
            /*MPLS_IPv6 add end*/
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Label Request Msg\n");
        return LDP_FAILURE;
    }

    /* Updating the LspCtrlBlock values. 
     * These values are updated for Supporting Insegment Table,Out Segment 
     * Tables defined as per LDP_MIB.
     */

    if (pLspCtrlBlock->pUStrSession == NULL)
    {
        /* LSR is Ingress */
        (SSN_GET_LBL_TYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ETHERNET_TYPE);

        (pLspCtrlBlock->u4InComIfIndex) = LDP_ZERO;
    }
    else
    {
        /* LSR is Intermediate */
        (SSN_GET_LBL_TYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ETHERNET_TYPE);

        (SSN_GET_LBL_TYPE (LCB_USSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ETHERNET_TYPE);
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendCrlspLblMappingMsg                                 */
/* Description   : This routine constructs and sends Label Mapping Message   */
/*                 when the LSP to be created is CRLSP in nature.            */
/*                 This routine is invoked in case of Egress LSR, as a result*/
/*                 of the processing of CRLSP Label Request Message received */
/*                 from upstream.                                            */
/*                 In the case of other LSRs, this routine is invoked as a   */
/*                 result of the processing of CRLSP Label Mapping messages  */
/*                 received from downstream.                                 */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock - Points to the current Lsp Control Block   */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS   if Label Mapping Message is sent            */
/*                 successfully else LDP_FAILURE                             */
/*****************************************************************************/

UINT1
LdpSendCrlspLblMappingMsg (tLspCtrlBlock * pLspCtrlBlock)
{
    UINT2               u2OptParamLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT4               u4Offset = 0;

    tGenLblTlv          GenLblTlv;
    tAtmLblTlv          AtmLblTlv;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;

    tLblReqMsgIdTlv     ReqMsgIdTlv = { {OSIX_HTONS (LDP_LBL_REQ_MSGID_TLV),
                                         OSIX_HTONS (LDP_MSG_ID_LEN)}, 0
    };
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_MAPPING_MSG), 0, 0 };
    tCrlspLspIdTlv      LspIdTlv;
    tCrlspTrfcParmsTlv  TrfParamsTlv;
    tCrlspTnlInfo      *pCrLsp = pLspCtrlBlock->pCrlspTnlInfo;
    tTlvHdr             FecTlvHdr;
    tFec                CrLspFecElmnt;
    UINT1               u1PadPrefixLen;
    tLdpId              PeerLdpId;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    LdpIfAddrZero.Addr.u4Addr = LDP_ZERO;
    /*MPLS_IPv6 add end*/
	
    LspIdTlv.TlvHdr.u2TlvType = OSIX_HTONS (CRLDP_LSPID_TLV);
    LspIdTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_LSPID_TLV_LEN);
    TrfParamsTlv.TlvHdr.u2TlvType = OSIX_HTONS (CRLDP_TRAFPARM_TLV);
    TrfParamsTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_TRF_PARAMS_TLV_LEN);

    if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
    {
        FecTlvHdr.u2TlvType = OSIX_HTONS (LDP_FEC_TLV);
        FecTlvHdr.u2TlvLen = OSIX_HTONS (CRLSP_FEC_ELMNT_LEN);
        /* Assigning the Fec Element type to CR-LSP */
        CrLspFecElmnt.u1FecElmntType = LDP_FEC_CRLSP_TYPE;
    }
    else
    {
        u1PadPrefixLen = LDP_GET_PADDED_PRFX_LEN (pLspCtrlBlock->Fec.u1PreLen);
        FecTlvHdr.u2TlvType = OSIX_HTONS (LDP_FEC_TLV);
        FecTlvHdr.u2TlvLen =
            OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PadPrefixLen));
    }

    u2MandParamLen += LDP_MSG_ID_LEN;

    /* Updating for the mandatory 'Fec Tlv' length */
    u2MandParamLen += (LDP_TLV_HDR_LEN + CRLSP_FEC_ELMNT_LEN);

    /* Updating for the mandatory Label Tlv length */
    u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_LABEL_LEN);

    /* Updating for the mandatory Request MsgId Tlv' length */
    u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN);

    /* Optional LspId Tlv shall, by default be sent. */
    u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_LSPID_TLV_LEN);

    if ((CRLSP_PARM_FLAG (LCB_TNLINFO (pLspCtrlBlock)) & LDP_RSRC_PEROA_CRLSP)
        == LDP_RSRC_PEROA_CRLSP)
    {
        LdpDiffServGetOptionalElspTPLength (LCB_TNLINFO (pLspCtrlBlock),
                                            &u2OptParamLen);
    }

    /* Checking if flag is set for Optional Traffic Params Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_TR_CRLSP) == LDP_TR_CRLSP)
    {
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_TRF_PARAMS_TLV_LEN);
    }

    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2OptParamLen + u2MandParamLen),
                                      LDP_PDU_HDR_LEN);

    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buf for Lbl Mapping Msg \n");
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2OptParamLen + u2MandParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLspCtrlBlock->pUStrSession));
    MsgHdrAndId.u4MsgId =
        OSIX_HTONL (SSN_GET_MSGID (pLspCtrlBlock->pUStrSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Copying the Fec Tlv Header */
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    /* Copying the CR-LSP Fec ELement */
    if (CRU_BUF_Copy_OverBufChain (pMsg, &CrLspFecElmnt.u1FecElmntType,
                                   u4Offset,
                                   CRLSP_FEC_ELMNT_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Cr-Lsp FecElement to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += CRLSP_FEC_ELMNT_LEN;

    /* Constructing and copying Label Tlv */
    if (pLspCtrlBlock->u1LblType == LDP_ATM_LABEL)
    {
        AtmLblTlv.AtmLblTlvHdr.u2TlvType = OSIX_HTONS (LDP_ATM_LABEL_TLV);
        AtmLblTlv.AtmLblTlvHdr.u2TlvLen = OSIX_HTONS (LDP_LABEL_LEN);
        AtmLblTlv.u2Vpi =
            OSIX_HTONS ((pLspCtrlBlock->UStrLabel.AtmLbl.u2Vpi |
                         LDP_ATM_V_BITS));
        AtmLblTlv.u2Vci = OSIX_HTONS (pLspCtrlBlock->UStrLabel.AtmLbl.u2Vci);

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AtmLblTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying Label Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }
    else
    {
        /* Non-Atm */
        GenLblTlv.GenLblTlvHdr.u2TlvType = OSIX_HTONS (LDP_GEN_LABEL_TLV);
        GenLblTlv.GenLblTlvHdr.u2TlvLen = OSIX_HTONS (LDP_LABEL_LEN);
        GenLblTlv.u4GenLbl = OSIX_HTONL (pLspCtrlBlock->UStrLabel.u4GenLbl);

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &GenLblTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying Label Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_LABEL_LEN);

    /* Constructing and copying Label Request Msg Id Tlv */
    ReqMsgIdTlv.u4LblReqMsgId = OSIX_HTONL (pLspCtrlBlock->u4UStrLblReqId);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &ReqMsgIdTlv, u4Offset,
                                   (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Failed copying ReqMsgId Tlv to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN);

    /* Constructing and copying LspId Tlv. This optional tlv shall, 
     * by default be sent to the peer. */
    if (CRLSP_IS_MOD_OF_TUNN (pCrLsp) == LDP_TRUE)
    {
        LspIdTlv.u2Reserved = (LDP_RSVD_FLD | CRLSP_ACT_FLAG);
    }
    else
    {
        LspIdTlv.u2Reserved = 0;
    }

    LspIdTlv.u2LocalLspid = OSIX_HTONS (CRLSP_LSPID (pCrLsp));
    MEMCPY (LspIdTlv.IngressLsrRtrId, CRLSP_INGRESS_LSRID (pCrLsp),
            LDP_IPV4ADR_LEN);
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &LspIdTlv, u4Offset,
                                   (LDP_TLV_HDR_LEN + LDP_LSPID_TLV_LEN))
        == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying LspId Tlv to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_LSPID_TLV_LEN);

    /* Constructing Traffic Params Tlv. */
    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_TR_CRLSP) == LDP_TR_CRLSP)
    {
        TrfParamsTlv.u1Flags = CRLSP_TRF_FLAGS (pCrLsp);
        TrfParamsTlv.u1Frequency = CRLSP_TRF_FREQ (pCrLsp);
        TrfParamsTlv.u1Reserved = 0;
        TrfParamsTlv.u1Weight = CRLSP_TRF_WGT (pCrLsp);
        TrfParamsTlv.u4PeakDataRate = OSIX_HTONL (CRLSP_TRF_PD (pCrLsp));
        TrfParamsTlv.u4PeakBurstSize = OSIX_HTONL (CRLSP_TRF_PB (pCrLsp));
        TrfParamsTlv.u4CommittedDataRate = OSIX_HTONL (CRLSP_TRF_CD (pCrLsp));
        TrfParamsTlv.u4ExcessBurstSize = OSIX_HTONL (CRLSP_TRF_EB (pCrLsp));
        TrfParamsTlv.u4CommittedBurstSize = OSIX_HTONL (CRLSP_TRF_CB (pCrLsp));

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &TrfParamsTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN +
                                        LDP_TRF_PARAMS_TLV_LEN)) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT:Error copying Traffic params Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_TRF_PARAMS_TLV_LEN);
    }

    if ((CRLSP_PARM_FLAG (pCrLsp) & LDP_RSRC_PEROA_CRLSP)
        == LDP_RSRC_PEROA_CRLSP)
    {
        if (LdpDiffServConstructCopyElspTPTlv (pCrLsp, pMsg, &u4Offset)
            == LDP_DS_FAILURE)
        {
            return LDP_FAILURE;
        }
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLspCtrlBlock->pUStrSession);
    LDP_DBG6 (LDP_ADVT_TX,
              "ADVT: Sending Label Mapping Msg To   : %d.%d.%d.%d.%d:%d \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2],
              PeerLdpId[3], PeerLdpId[4], PeerLdpId[5]);

        /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLspCtrlBlock->pUStrSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLspCtrlBlock->pUStrSession->pLdpPeer->pLdpEntity),
                    LDP_FALSE,
                    ConnId,
                    LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
                /*MPLS_IPv6 add start*/
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Label Mapping Msg\n");
        return LDP_FAILURE;
    }

    if (pLspCtrlBlock->pDStrSession == NULL)
    {
        /* LSR is Egress */
        (SSN_GET_LBL_TYPE (LCB_USSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ETHERNET_TYPE);

        (pLspCtrlBlock->u4OutIfIndex) = LDP_ZERO;
    }
    else
    {
        /* LSR is Intermediate */
        (SSN_GET_LBL_TYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1OutLblType) = LDP_IFACE_ETHERNET_TYPE);

        (SSN_GET_LBL_TYPE (LCB_USSN (pLspCtrlBlock)) == LDP_ATM_MODE) ?
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ATM_TYPE) :
            ((pLspCtrlBlock->u1InLblType) = LDP_IFACE_ETHERNET_TYPE);
    }
    return LDP_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpadsnd.c                              */
/*---------------------------------------------------------------------------*/
