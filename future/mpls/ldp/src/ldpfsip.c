
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: ldpfsip.c,v 1.60 2017/07/10 11:26:05 siva Exp $
 *                                                                  *
 *******************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpfsip.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP (IFACE SUB-MODULE)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file call back routines provided to external
 *                             modules (Timer, SNMP, UDP, TCP, IP, MPLS-FM)
 *----------------------------------------------------------------------------*/

#include "ldpincs.h"
#include "mplsutil.h"
#include "rtm.h"
#include "mplsprot.h"
UINT4               gLdpTimeStart;
UINT4               gLdpTimeEnd;

/*****************************************************************************/
/* Function Name : LdpSNMPEventHandler                                       */
/* Description   : This routine is called by low level routines.             */
/*                 Corresponding event is posted to LDP task along           */
/*                 with necessary arguments.                                 */
/* Input(s)      : u4Event, u4SnmpParms                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

UINT1
LdpSNMPEventHandler (UINT4 u4Event, VOID *pSnmpParms, UINT2 u2IncarnId)
{
    tLdpIfMsg           LdpIfMsg;

    LdpIfMsg.u4MsgType = LDP_SNMP_EVENT;
    LdpIfMsg.u.SnmpEvt.pParams = pSnmpParms;
    LdpIfMsg.u.SnmpEvt.u4IncarnId = u2IncarnId;
    LdpIfMsg.u.SnmpEvt.u4SubEvt = u4Event;

    if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Failed to EnQ SNMP event to LDP Task\n");
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpIfStChgEventHandler                                    */
/* Description   : This routine is called by If module, for informing the    */
/*                 the State changes.                                        */
/*                 Corresponding event is posted to LDP task along           */
/*                 with necessary parameters.                                */
/* Input(s)      : u4IfIndex - Interface If Index                            */
/*                 u1OperState - Interface Status                            */
/*                 u4IpAddr - IP address of the u4IfIndex                    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpIfStChgEventHandler (UINT4 u4IfIndex, UINT1 u1OperState)
{
    tLdpIfMsg           LdpIfMsg;
    UINT4               u4IpAddr = 0;

#ifdef MPLS_IPV6_WANTED
    tIp6Addr            LnklocalIpv6Addr;
    tIp6Addr            SitelocalIpv6Addr;
    tIp6Addr            GlbUniqueIpv6Addr;

    MEMSET (&LnklocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&SitelocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&GlbUniqueIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
#endif

    /* Ignore the Notification if Incarn is not UP Administratively */
    if (LDP_INCARN_STATUS (LDP_CUR_INCARN) != ACTIVE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Failed to EnQ If Event to LDP Task, "
                 "Incarnation is down\n");
        return;
    }
    MEMSET (&LdpIfMsg, 0, sizeof (LdpIfMsg));

    LdpIfMsg.u.IpEvt.u4IfIndex = u4IfIndex;

    if (u1OperState == CFA_IF_UP)
    {
        LdpIfMsg.u.IpEvt.u4BitMap = LDP_IF_OPER_ENABLE;
    }
    else
    {
        LdpIfMsg.u.IpEvt.u4BitMap = LDP_IF_OPER_DISABLE;
    }

    CfaGetIfIpAddr (u4IfIndex, &u4IpAddr);

#ifdef MPLS_IPV6_WANTED
    LdpGetIpv6IfAllAddr ((INT4) u4IfIndex, &LnklocalIpv6Addr,
                         &SitelocalIpv6Addr, &GlbUniqueIpv6Addr);

    MEMCPY (&LdpIfMsg.u.IpEvt.LnklocalIpv6Addr, &LnklocalIpv6Addr,
            LDP_IPV6ADR_LEN);

    MEMCPY (&LdpIfMsg.u.IpEvt.SitelocalIpv6Addr, &SitelocalIpv6Addr,
            LDP_IPV6ADR_LEN);

    MEMCPY (&LdpIfMsg.u.IpEvt.GlbUniqueIpv6Addr, &GlbUniqueIpv6Addr,
            LDP_IPV6ADR_LEN);

#endif

    LdpIfMsg.u4MsgType = LDP_IP_EVENT;
    LdpIfMsg.u.IpEvt.u4IpAddr = u4IpAddr;
    LdpIfMsg.u.IpEvt.u4IncarnId = LDP_CUR_INCARN;
    if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Failed to EnQ If Event to LDP Task\n");
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpIpRtChgEventHandler                                    */
/* Description   : This routine is called by IP module, for informing the    */
/*                 the Route changes.                                        */
/*                 Corresponding event is posted to LDP task along           */
/*                 with necessary parameters.                                */
/* Input(s)      : tNetIpv4RtInfo - Route info  as given by IP               */
/*                 ! Do not free pNetIpv4RtInfo,                             */
/*                 as it points to Trie Route Entry !                        */
/*                 u1CmdType - Species type of route change - NewEntry,      */
/*                            NextHop change, Interface change etc.,         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpIpRtChgEventHandler (tNetIpv4RtInfo * pNetIpv4RtInfo, UINT1 u1CmdType)
{
    UINT2               u2IncarnId;
    tLdpRouteEntryInfo *pRouteInfo = NULL;
    tLdpIfMsg           LdpIfMsg;

    u2IncarnId = LDP_CUR_INCARN;

    /* Ignore the Notification if Incarn is not UP Administratively */
    if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
    {
        return;
    }

    if (pNetIpv4RtInfo != NULL)
    {
        pRouteInfo = (tLdpRouteEntryInfo *) MemAllocMemBlk (LDP_IP_RT_POOL_ID);

        if (pRouteInfo == NULL)
        {
            LDP_DBG (LDP_IF_MEM,
                     "IF: Memory Allocation for Ip Route Info failed \n");
            return;
        }

        /* Copying information to Ldp's strucure */
        LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr) = pNetIpv4RtInfo->u4DestNet;
        LDP_IPV4_U4_ADDR (pRouteInfo->DestMask) = pNetIpv4RtInfo->u4DestMask;
        LDP_IPV4_U4_ADDR (pRouteInfo->NextHop) = pNetIpv4RtInfo->u4NextHop;
        pRouteInfo->u4RtIfIndx = pNetIpv4RtInfo->u4RtIfIndx;
        pRouteInfo->u4RtNxtHopAS = pNetIpv4RtInfo->u4RtNxtHopAs;
        pRouteInfo->i4Metric1 = pNetIpv4RtInfo->i4Metric1;
        pRouteInfo->u4RowStatus = pNetIpv4RtInfo->u4RowStatus;
        pRouteInfo->u2AddrType = LDP_ADDR_TYPE_IPV4;
        pRouteInfo->u4Flag = pNetIpv4RtInfo->u4Flag;
    }
    else
    {
        LDP_DBG (LDP_IF_MISC, "IF: No Route Information passed by IP\n");
        return;
    }
    LdpIfMsg.u4MsgType = LDP_IP_EVENT;
    if (NetIpv4GetCfaIfIndexFromPort (pRouteInfo->u4RtIfIndx,
                                      &LdpIfMsg.u.IpEvt.u4IfIndex) ==
        NETIPV4_FAILURE)
    {
        LDP_DBG (LDP_IF_MISC, "IF: No Port to IfIndex Mapping\n");
        MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
        return;
    }
    LdpIfMsg.u.IpEvt.u4IncarnId = u2IncarnId;
    if (u1CmdType == NETIPV4_ADD_ROUTE)
    {
        LdpIfMsg.u.IpEvt.u4BitMap = LDP_RT_NEW;
    }
    else if (u1CmdType == NETIPV4_DELETE_ROUTE)
    {
        LdpIfMsg.u.IpEvt.u4BitMap = LDP_RT_DELETED;
    }
    else if (u1CmdType == NETIPV4_MODIFY_ROUTE)
    {
        /* From NetIP only Route Change will come so,
         * Just Mapped to Next Hop Change. We do not know
         * existing route info */
        LdpIfMsg.u.IpEvt.u4BitMap = LDP_RT_NH_CHG;
    }
    LdpIfMsg.u.IpEvt.pRouteInfo = (UINT1 *) pRouteInfo;

    if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Failed to EnQ IP Event to LDP Task\n");
        MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpEnqueueMsgToLdpQ                                       */
/* Description   : Enqueues the message to LDP Message Queue and generates   */
/*                 respective event.                                         */
/* Input(s)      : u1EvtType - Specifies whether Timer or Queue Eventi       */
/*                 pIfMsg - Message/Timer Information                        */
/* Output(s)     : NONE                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

UINT1
LdpEnqueueMsgToLdpQ (UINT1 u1EvtType, tLdpIfMsg * pIfMsg)
{
    UINT1              *pu1TmpMsg = NULL;

    LDP_DBG1 (LDP_IF_MISC, "IF: EnQing event %s to LDPQ\n",
              aEventName[pIfMsg->u4MsgType - 1]);
    if (LDP_INITIALISED != TRUE)
    {
        LDP_DBG (LDP_IF_MEM, "IF: LDP module is in shutdown\n");
        return LDP_FAILURE;
    }

    pu1TmpMsg = (UINT1 *) MemAllocMemBlk (LDP_Q_POOL_ID);

    if (pu1TmpMsg == NULL)
    {
        LDP_DBG (LDP_IF_MEM, "Memory Allocation failed for If Message\n");
        return LDP_FAILURE;
    }

    MEMCPY (pu1TmpMsg, (UINT1 *) pIfMsg, sizeof (tLdpIfMsg));

    if (OsixQueSend (LDP_QID, (UINT1 *) (&pu1TmpMsg), OSIX_DEF_MSG_LEN)
        != OSIX_SUCCESS)
    {
        LDP_DBG (LDP_IF_MEM, "IF: Failed to EnQ the Msg to LDP Q\n");

        MemReleaseMemBlock (LDP_Q_POOL_ID, pu1TmpMsg);
        return LDP_FAILURE;
    }

    if (OsixEvtSend (LDP_TSK_ID, u1EvtType) == OSIX_FAILURE)
    {
        MemReleaseMemBlock (LDP_Q_POOL_ID, pu1TmpMsg);

        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendMplsMlibUpdate                                     */
/* Description   : This routine prepares the necessary structure/information */
/*                 and invokes the interface routine provided by MPLS-FM     */
/*                 module to update the MLIB maintained by the MPLS-FM.      */
/* Input(s)      : u2MlibOperation - This field indicates the Operation to be*/
/*                 performed in the MLIB. It takes on any one of the follwing*/
/*                 eight values                                              */
/*                 MPLS_MLIB_ILM_CREATE, MPLS_MLIB_ILM_DELETE,               */
/*                 MPLS_MLIB_ILM_MODIFY, MPLS_MLIB_FTN_CREATE,               */
/*                 MPLS_MLIB_FTN_DELETE, MPLS_MLIB_TNL_CREATE,               */
/*                 MPLS_MLIB_TNL_DELETE, MPLS_MLIB_TNL_MODIFY.               */
/*                 u2LabelOperation - The operation to be performed using    */
/*                 the label information on the received packet by the MPLS. */
/*                 If the u2MlibOperation is MPLS_MLIB_TNL_MODIFY, then      */
/*                 u2LabelOperation will contain the Modification operation  */
/*                 say, DataTxEnable or DataTxDisable.                       */
/*                 pLspCtrlBlock - Pointer to the LSP Control whose label    */
/*                 related information is to be updated in the MLIB.         */
/* Output(s)     : None.                                                     */
/* Return(s)     : LDP_SUCCESS / LDP_FAILURE                                 */
/*****************************************************************************/
UINT1
LdpSendMplsMlibUpdate (UINT2 u2MlibOperation,
                       UINT2 u2LabelOperation,
                       tLspCtrlBlock * pLspCtrlBlock,
                       tUstrLspCtrlBlock * pUstrCtrlBlock)
{
    tLspInfo            LspInfo;

    MEMSET (&LspInfo, LDP_ZERO, sizeof (tLspInfo));

    if (LdpMplsMlibUpdate (u2MlibOperation, u2LabelOperation, pLspCtrlBlock,
                           pUstrCtrlBlock, &LspInfo) == LDP_SUCCESS)
    {
        return LDP_SUCCESS;
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpMplsMlibUpdate                                     */
/* Description   : This routine prepares the necessary structure/information */
/*                 and invokes the interface routine provided by MPLS-FM     */
/*                 module to update the MLIB maintained by the MPLS-FM.      */
/* Input(s)      : u2MlibOperation - This field indicates the Operation to be*/
/*                 performed in the MLIB. It takes on any one of the follwing*/
/*                 eight values                                              */
/*                 MPLS_MLIB_ILM_CREATE, MPLS_MLIB_ILM_DELETE,               */
/*                 MPLS_MLIB_ILM_MODIFY, MPLS_MLIB_FTN_CREATE,               */
/*                 MPLS_MLIB_FTN_DELETE, MPLS_MLIB_TNL_CREATE,               */
/*                 MPLS_MLIB_TNL_DELETE, MPLS_MLIB_TNL_MODIFY.               */
/*                 u2LabelOperation - The operation to be performed using    */
/*                 the label information on the received packet by the MPLS. */
/*                 If the u2MlibOperation is MPLS_MLIB_TNL_MODIFY, then      */
/*                 u2LabelOperation will contain the Modification operation  */
/*                 say, DataTxEnable or DataTxDisable.                       */
/*                 pLspCtrlBlock - Pointer to the LSP Control whose label    */
/*                 related information is to be updated in the MLIB.         */
/* Output(s)     : None.                                                     */
/* Return(s)     : LDP_SUCCESS / LDP_FAILURE                                 */
/*****************************************************************************/
UINT1
LdpMplsMlibUpdate (UINT2 u2MlibOperation,
                   UINT2 u2LabelOperation,
                   tLspCtrlBlock * pLspCtrlBlock,
                   tUstrLspCtrlBlock * pUstrCtrlBlock, tLspInfo * pLspInfo)
{
    uLabel             *pLabel = NULL;
    tNHLFE              Nhlfe;
    tLdpSession        *pLdpSession = NULL;
    tLdpSession        *pDStrLdpSession = NULL;
    tLdpSession        *pUStrLdpSession = NULL;
    tLdpSession        *pTmpUpStrSession = NULL;
    tLdpIfTableEntry   *pIfTableEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tMplsDiffServTnlInfo *pDiffServTnlInfo = NULL;
    tMplsDiffServElspInfo *pTempElspInfo = NULL;
    tMplsDiffServParams DiffServParams;
    tMplsDiffServParams *pDiffServParams = &DiffServParams;
    tTMO_SLL           *pElspInfoList = NULL;
    tElspMapRow         ElspMapRow;
    UINT1               u1Exp;
    tLspCtrlBlock       TempCtrlBlock;
    tCrlspTnlInfo      *pStackTnlInfo = NULL;
#ifdef LDP_GR_WANTED
    UINT4               u1PeerGrCapability = MPLS_FALSE;
#endif

    MEMSET (&TempCtrlBlock, 0, sizeof (tLspCtrlBlock));
    MEMSET (&Nhlfe, 0, sizeof (tNHLFE));

#ifdef LDP_GR_WANTED
    pLspInfo->u1IsNPBlock = MPLS_FALSE;
#endif
    MEMSET (pDiffServParams, LDP_ZERO, sizeof (tMplsDiffServParams));

    if ((u2MlibOperation == MPLS_MLIB_FTN_DELETE) &&
        (pLspCtrlBlock != NULL) &&
        (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) == LDP_DU_DN_MLIB_UPD_NOT_DONE))
    {
        /* Please don't delete the FTN entry which is not 
         * created by this LSP */
#ifdef MPLS_IPV6_WANTED
        if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_IPV6ADR_LEN)
        {
            LDP_DBG1 (LDP_DBG_PRCS,
                      "FTN Delete for FEC %s skipped\n",
                      Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.Ip6Addr));
        }
        else
#endif
        {
            LDP_DBG1 (LDP_DBG_PRCS,
                      "FTN Delete for FEC %x skipped\n",
                      LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));
        }
        return LDP_SUCCESS;
    }

    if (pUstrCtrlBlock != NULL)
    {
        LdpInitLspCtrlBlock (&TempCtrlBlock);

        if (pLspCtrlBlock != NULL)    /* Egress Case check */
        {
            MEMCPY ((UINT1 *) &TempCtrlBlock, (UINT1 *) pLspCtrlBlock,
                    sizeof (tLspCtrlBlock));
        }
        else
        {
            MEMCPY ((UINT1 *) &LCB_FEC (&TempCtrlBlock),
                    (UINT1 *) &UPSTR_FEC (pUstrCtrlBlock), sizeof (tFec));
        }

        MEMCPY ((UINT1 *) &LCB_ULBL (&TempCtrlBlock),
                (UINT1 *) &(LCB_ULBL (pUstrCtrlBlock)), sizeof (uLabel));
        LCB_USSN (&TempCtrlBlock) = UPSTR_USSN (pUstrCtrlBlock);

        pLspCtrlBlock = &TempCtrlBlock;
        pLspCtrlBlock->u4InComIfIndex = pUstrCtrlBlock->u4InComIfIndex;
    }
    if (pLspCtrlBlock == NULL)
    {
        return LDP_FAILURE;
    }
    switch (u2MlibOperation)
    {
        case MPLS_MLIB_ILM_CREATE:
        case MPLS_MLIB_ILM_DELETE:
        case MPLS_MLIB_ILM_MODIFY:

            /* Assignment of incoming label for the LSP */
            pLabel = &(LCB_ULBL (pLspCtrlBlock));

            /* 
             * In case of ATM interface, the interface index associated
             * with the entity is asssigned as the incoming ifindex.
             */

            pLdpSession = LCB_USSN (pLspCtrlBlock);
            if (pLdpSession == NULL)
            {
                return LDP_FAILURE;
            }
            if (SSN_GET_LBL_TYPE (pLdpSession) == LDP_ATM_MODE)
            {
                pIfTableEntry = (tLdpIfTableEntry *)
                    (TMO_SLL_First (SSN_GET_ENTITY_IFLIST (pLdpSession)));
                if (pIfTableEntry != NULL)
                {
                    pLspInfo->u4IfIndex = (pIfTableEntry->u4IfIndex);

                    /* Label assigned as the ATM Label and in terms of 
                     * VPI and VCI */
                    pLspInfo->u4InTopLabel = pLabel->AtmLbl.u2Vci;
                    pLspInfo->u4InTopLabel = pLspInfo->u4InTopLabel << 16;
                    pLspInfo->u4InTopLabel |= pLabel->AtmLbl.u2Vpi;
                }

            }
            else if (SSN_GET_LBL_TYPE (pLdpSession) == LDP_GEN_MODE)
            {
                pLspInfo->u4IfIndex = pLspCtrlBlock->u4InComIfIndex;
                /* Label assigned as the Generic Label */
                pLspInfo->u4InTopLabel = pLabel->u4GenLbl;
            }
            break;

        default:
            break;
    }

    switch (u2MlibOperation)
    {
        case MPLS_MLIB_ILM_CREATE:
            pTmpUpStrSession = LCB_USSN (pLspCtrlBlock);
            if (pTmpUpStrSession == NULL)
            {
                return LDP_FAILURE;
            }

            if (LCB_DSSN (pLspCtrlBlock) == NULL)
            {
                if (pLspInfo->u4InTopLabel == MPLS_IPV4_EXPLICIT_NULL_LABEL)
                {
                    if (pLspInfo->u4IfIndex != 0)
                    {
                        pTmpUpStrSession->u4ExpNullCount++;

#ifdef MPLS_IPV6_WANTED
                        if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
                        {
                            LDP_DBG3 (LDP_DBG_PRCS,
                                      "ILM Create - Value of Exp Null Count: %d, InInterface: %u Fec: %s\n",
                                      pTmpUpStrSession->u4ExpNullCount,
                                      pLspInfo->u4IfIndex,
                                      Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.
                                                    Ip6Addr));
                        }
                        else
#endif
                        {
                            LDP_DBG3 (LDP_DBG_PRCS,
                                      "ILM Create - Value of Exp Null Count: %d InInterface: %u, Fec: %x\n",
                                      pTmpUpStrSession->u4ExpNullCount,
                                      pLspInfo->u4IfIndex,
                                      LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.
                                                        Prefix));
                        }
                        if (pTmpUpStrSession->u4ExpNullCount > LDP_ONE)
                        {
                            LDP_DBG (LDP_DBG_PRCS,
                                     "ILM Create - Prog skipped\n");
                            return LDP_SUCCESS;
                        }
                    }
                }
            }
            break;

        case MPLS_MLIB_ILM_DELETE:
            pTmpUpStrSession = LCB_USSN (pLspCtrlBlock);
            if (pTmpUpStrSession == NULL)
            {
                return LDP_FAILURE;
            }

            /* Update the local varibale for ILM deletion with the Peer GR capability */
#ifdef LDP_GR_WANTED
            /* Transit Case */
            if (LCB_DSSN (pLspCtrlBlock) != NULL)
            {
                if ((pTmpUpStrSession->pLdpPeer->u1GrProgressState !=
                     LDP_PEER_GR_NOT_SUPPORTED)
                    && (LCB_DSSN (pLspCtrlBlock)->pLdpPeer->u1GrProgressState !=
                        LDP_PEER_GR_NOT_SUPPORTED))
                {
                    u1PeerGrCapability = MPLS_TRUE;
                }
            }
            else                /* Egress case */
            {
                if (pTmpUpStrSession->pLdpPeer->u1GrProgressState !=
                    LDP_PEER_GR_NOT_SUPPORTED)
                {
                    u1PeerGrCapability = MPLS_TRUE;
                }
            }
#endif

            if (LCB_DSSN (pLspCtrlBlock) == NULL)
            {
                if (pLspInfo->u4InTopLabel == MPLS_IPV4_EXPLICIT_NULL_LABEL)
                {
                    pTmpUpStrSession->u4ExpNullCount--;

#ifdef MPLS_IPV6_WANTED
                    if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
                    {
                        LDP_DBG2 (LDP_DBG_PRCS,
                                  "ILM Delete - Value of Exp Null Count: %d Fec: %s\n",
                                  pTmpUpStrSession->u4ExpNullCount,
                                  Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.
                                                Ip6Addr));
                    }
                    else
#endif
                    {
                        LDP_DBG2 (LDP_DBG_PRCS,
                                  "ILM Delete - Value of Exp Null Count: %d Fec: %x\n",
                                  pTmpUpStrSession->u4ExpNullCount,
                                  LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));
                    }

                    if (pTmpUpStrSession->u4ExpNullCount >= LDP_ONE)
                    {
                        LDP_DBG (LDP_DBG_PRCS, "ILM Delete - Prog skipped\n");
                        return LDP_SUCCESS;
                    }
                }
            }
            break;
        default:
            break;
    }

    pLspInfo->FecParams.u1FecType = pLspCtrlBlock->Fec.u1FecElmntType;

    LdpCopyAddr (&pLspInfo->FecParams.DestAddrPrefix,
                 &pLspCtrlBlock->Fec.Prefix, pLspCtrlBlock->Fec.u2AddrFmly);
    pLspInfo->FecParams.u2AddrType = pLspCtrlBlock->Fec.u2AddrFmly;

#ifdef MPLS_IPV6_WANTED
    if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        MplsGetIPV6Subnetmask (pLspCtrlBlock->Fec.u1PreLen,
                               LDP_IPV6_U4_ADDR (pLspInfo->FecParams.DestMask));
    }
    else
#endif
    {
        LDP_GET_MASK_FROM_PRFX_LEN (pLspCtrlBlock->Fec.u1PreLen,
                                    LDP_IPV4_U4_ADDR (pLspInfo->FecParams.
                                                      DestMask));
    }

    if (pLspCtrlBlock->pCrlspTnlInfo != NULL)
    {
        if (pLspCtrlBlock->pCrlspTnlInfo->pTeTnlInfo != NULL)
        {
            if ((CRLSP_TRF_PARMS (pLspCtrlBlock->pCrlspTnlInfo) != NULL) &&
                (TE_CRLDP_TRFC_PARAMS
                 (CRLSP_TRF_PARMS (pLspCtrlBlock->pCrlspTnlInfo)) != NULL) &&
                (CRLSP_TE_TNL_ROLE
                 (CRLSP_TE_TNL_INFO (pLspCtrlBlock->pCrlspTnlInfo)) !=
                 LDP_TE_EGRESS))
            {
                pLspInfo->FecParams.MplsTrfcParms.u4PeakDataRate =
                    CRLSP_TRF_PD (pLspCtrlBlock->pCrlspTnlInfo);
                pLspInfo->FecParams.MplsTrfcParms.u4PeakBurstSize =
                    CRLSP_TRF_PB (pLspCtrlBlock->pCrlspTnlInfo);
                pLspInfo->FecParams.MplsTrfcParms.u4CommittedBurstSize =
                    CRLSP_TRF_CB (pLspCtrlBlock->pCrlspTnlInfo);
                pLspInfo->FecParams.MplsTrfcParms.u4CommittedDataRate =
                    CRLSP_TRF_CD (pLspCtrlBlock->pCrlspTnlInfo);
                pLspInfo->FecParams.MplsTrfcParms.u4ExcessBurstSize =
                    CRLSP_TRF_EB (pLspCtrlBlock->pCrlspTnlInfo);
            }

            pLspInfo->FecParams.u4TnlId =
                pLspCtrlBlock->pCrlspTnlInfo->u2LocalLspid;
            pLspInfo->FecParams.u4TnlInstance =
                (UINT2) CRLSP_TNL_INSTANCE (LCB_TNLINFO (pLspCtrlBlock));
            CONVERT_TO_INTEGER (CRLSP_INGRESS_LSRID
                                (pLspCtrlBlock->pCrlspTnlInfo),
                                pLspInfo->FecParams.u4IngressId);
            pLspInfo->FecParams.u4IngressId =
                OSIX_NTOHL (pLspInfo->FecParams.u4IngressId);
            CONVERT_TO_INTEGER (CRLSP_EGRESS_LSRID
                                (pLspCtrlBlock->pCrlspTnlInfo),
                                pLspInfo->FecParams.u4EgressId);
            pLspInfo->FecParams.u4EgressId =
                OSIX_NTOHL (pLspInfo->FecParams.u4EgressId);
        }
    }

    Nhlfe.u1Operation = (UINT1) u2LabelOperation;
    pLabel = &(LCB_DLBL (pLspCtrlBlock));

    if (pLspCtrlBlock->pCrlspTnlInfo != NULL)
    {
        if (((tCrlspTnlInfo *) (VOID *) pLspCtrlBlock->pCrlspTnlInfo)->
            pStackTnlHead != NULL)
        {
            Nhlfe.StackTnlInfo.u1StackTnlBit = TRUE;
            pStackTnlInfo = (tCrlspTnlInfo *) (VOID *) (((UINT1 *)
                                                         (((tCrlspTnlInfo
                                                            *) (VOID
                                                                *)
                                                           (pLspCtrlBlock->
                                                            pCrlspTnlInfo))->
                                                          pStackTnlHead)) -
                                                        VAR_OFFSET
                                                        (tCrlspTnlInfo,
                                                         StackTnlList));
            if (pStackTnlInfo != NULL)
            {
                Nhlfe.StackTnlInfo.u4TnlId = pStackTnlInfo->u2LocalLspid;
                Nhlfe.StackTnlInfo.u4TnlInstance =
                    (UINT2) CRLSP_TNL_INSTANCE (pStackTnlInfo);
                CONVERT_TO_INTEGER (CRLSP_INGRESS_LSRID (pStackTnlInfo),
                                    Nhlfe.StackTnlInfo.u4IngressId);
                Nhlfe.StackTnlInfo.u4IngressId =
                    OSIX_NTOHL (Nhlfe.StackTnlInfo.u4IngressId);
                CONVERT_TO_INTEGER (CRLSP_EGRESS_LSRID (pStackTnlInfo),
                                    Nhlfe.StackTnlInfo.u4EgressId);
                Nhlfe.StackTnlInfo.u4EgressId =
                    OSIX_NTOHL (Nhlfe.StackTnlInfo.u4EgressId);
            }
        }
        if (u2MlibOperation == MPLS_MLIB_TNL_MODIFY)
        {
            if (u2LabelOperation == CRLDP_DATA_TX_UP)
            {
                /* NOTE : When the AdminStatus is added to FM, this
                 * piece of code needs to be properly ported. */
                Nhlfe.u1OperStatus = LDP_OPER_UP;
            }
            else
            {
                Nhlfe.u1OperStatus = LDP_OPER_DOWN;
            }
        }
        else if (u2MlibOperation == MPLS_MLIB_TNL_CREATE)
        {
            if (pLspCtrlBlock->pCrlspTnlInfo->pTeTnlInfo == NULL)
            {
                return LDP_FAILURE;
            }
            if (CRLSP_ADMIN_STATUS (LCB_TNLINFO (pLspCtrlBlock))
                == LDP_ADMIN_UP)
            {
                Nhlfe.u1OperStatus = LDP_OPER_UP;
            }
            else
            {
                Nhlfe.u1OperStatus = LDP_OPER_DOWN;
            }
        }
        else if (u2MlibOperation == MPLS_MLIB_FTN_DELETE)
        {
            u2MlibOperation = MPLS_MLIB_TNL_DELETE;
        }
    }

    pLdpSession = LCB_DSSN (pLspCtrlBlock);
    if (pLdpSession != NULL)
    {
        if (SSN_GET_LBL_TYPE (pLdpSession) == LDP_ATM_MODE)
        {
            /* The label value is to be stored back as a u4 value and hence
             * the access of UINT2 values and assigned here */
            Nhlfe.u4OutLabel = pLabel->AtmLbl.u2Vci;
            Nhlfe.u4OutLabel = Nhlfe.u4OutLabel << 16;
            Nhlfe.u4OutLabel |= pLabel->AtmLbl.u2Vpi;
        }
        else if (SSN_GET_LBL_TYPE (pLdpSession) == LDP_GEN_MODE)
        {
            Nhlfe.u4OutLabel = pLabel->u4GenLbl;
        }
#ifdef LDP_GR_WANTED
        if (u2MlibOperation == MPLS_MLIB_FTN_DELETE)
        {
            if (pLdpSession->pLdpPeer->u1GrProgressState !=
                LDP_PEER_GR_NOT_SUPPORTED)
            {
                u1PeerGrCapability = MPLS_TRUE;
            }
        }
#endif
    }
    else
    {
        Nhlfe.u4OutLabel = pLabel->u4GenLbl;
    }
    if (Nhlfe.u4OutLabel == LDP_INVALID_LABEL)
    {
        Nhlfe.u4OutLabel = LDP_ZERO;
    }

    LdpCopyAddr (&Nhlfe.NextHopAddr, &pLspCtrlBlock->NextHopAddr,
                 pLspCtrlBlock->Fec.u2AddrFmly);
    Nhlfe.u1NHAddrType = (UINT1) pLspCtrlBlock->u2AddrType;
    Nhlfe.u4OutIfIndex = (UINT4) (pLspCtrlBlock->u4OutIfIndex);
    Nhlfe.u1OperStatus = MPLS_STATUS_UP;

    /* LDP LSP over RSVP LSP handling starts */
    if (pLspCtrlBlock->pCrlspTnlInfo == NULL)
    {
        pDStrLdpSession = LCB_DSSN (pLspCtrlBlock);
        pUStrLdpSession = LCB_USSN (pLspCtrlBlock);
        if ((pDStrLdpSession != NULL) &&
            (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pDStrLdpSession))
             == LDP_TRUE) && (Nhlfe.u4OutLabel != LDP_INVALID_LABEL))
        {
            if (SSN_GET_ENTITY (pDStrLdpSession)->OutStackTnlInfo.u4TnlId ==
                LDP_ZERO)
            {
                return LDP_FAILURE;
            }
            /* Find out the tunnel that goes upto Nexthop */
            Nhlfe.StackTnlInfo.u1StackTnlBit = TRUE;

            Nhlfe.StackTnlInfo.u4TnlId =
                SSN_GET_ENTITY (pDStrLdpSession)->OutStackTnlInfo.u4TnlId;
            Nhlfe.StackTnlInfo.u4TnlInstance =
                SSN_GET_ENTITY (pDStrLdpSession)->OutStackTnlInfo.u4TnlInstance;
            Nhlfe.StackTnlInfo.u4IngressId =
                SSN_GET_ENTITY (pDStrLdpSession)->OutStackTnlInfo.u4IngressId;
            Nhlfe.StackTnlInfo.u4EgressId =
                SSN_GET_ENTITY (pDStrLdpSession)->OutStackTnlInfo.u4EgressId;
        }
        if ((pUStrLdpSession != NULL) &&
            (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pUStrLdpSession))
             == LDP_TRUE) && (pLspInfo->u4InTopLabel != LDP_INVALID_LABEL))
        {
            if (SSN_GET_ENTITY (pUStrLdpSession)->InStackTnlInfo.u4TnlId ==
                LDP_ZERO)
            {
                return LDP_FAILURE;
            }

            /* Find out the tunnel that goes upto Nexthop */
            pLspInfo->StackTnlInfo.u1StackTnlBit = TRUE;

            pLspInfo->StackTnlInfo.u4TnlId =
                SSN_GET_ENTITY (pUStrLdpSession)->InStackTnlInfo.u4TnlId;
            pLspInfo->StackTnlInfo.u4TnlInstance =
                SSN_GET_ENTITY (pUStrLdpSession)->InStackTnlInfo.u4TnlInstance;
            pLspInfo->StackTnlInfo.u4IngressId =
                SSN_GET_ENTITY (pUStrLdpSession)->InStackTnlInfo.u4IngressId;
            pLspInfo->StackTnlInfo.u4EgressId =
                SSN_GET_ENTITY (pUStrLdpSession)->InStackTnlInfo.u4EgressId;
            if (pLspInfo->u4TnlLabel == LDP_ZERO)
            {
                pLspInfo->u4TnlLabel =
                    SSN_GET_ENTITY (pUStrLdpSession)->u4InTnlLabel;
            }
        }
    }
    /* LDP LSP over RSVP LSP handling ends */

    pLdpSession = LCB_DSSN (pLspCtrlBlock);

    if ((pLdpSession != NULL) && (pLspCtrlBlock->pCrlspTnlInfo == NULL))
    {
        if (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
            == LDP_TRUE)
        {
            Nhlfe.u1RemoteBit = MPLS_TRUE;
        }
    }

    Nhlfe.u1NumDecTtl =
        (UINT1) ((LCB_HCOUNT (pLspCtrlBlock) == LDP_INVALID_HOP_COUNT ||
                  (LCB_HCOUNT (pLspCtrlBlock) == LDP_ZERO)) ? 1 :
                 (LCB_HCOUNT (pLspCtrlBlock) - 1));

    if ((pLdpSession != NULL) && ((SSN_MRGTYPE (pLdpSession) != LDP_NO_MRG)))
    {
        Nhlfe.u1MergeFlag = MPLS_TRUE;
    }

    if (pLspCtrlBlock->pCrlspTnlInfo != NULL)
    {
        if (pLspCtrlBlock->pCrlspTnlInfo->pTeTnlInfo != NULL)
        {
            pTeTnlInfo = pLspCtrlBlock->pCrlspTnlInfo->pTeTnlInfo;
            if (LDP_TE_MPLS_DIFFSERV_TNL_INFO (pTeTnlInfo) != NULL)
            {
                pDiffServTnlInfo = LDP_TE_MPLS_DIFFSERV_TNL_INFO (pTeTnlInfo);
                if (pDiffServTnlInfo != NULL)
                {
                    LDP_FM_DS_LSP_SERVICE_TYPE (pDiffServParams) =
                        LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServTnlInfo);
                    LDP_FM_DS_CREATER (pDiffServParams) = MPLS_CTRL_PROTO;
                    if (LDP_FM_DS_LSP_SERVICE_TYPE (pDiffServParams) ==
                        MPLS_DIFFSERV_LLSP)
                    {
                        LDP_FM_DS_LLSP_DSCP (pDiffServParams) =
                            LDP_TE_DS_LLSP_DSCP (pDiffServTnlInfo);
                    }
                    else
                    {
                        LDP_FM_DS_ELSP_TYPE (pDiffServParams) =
                            LDP_TE_DS_ELSP_TYPE (pDiffServTnlInfo);
                        if (LDP_FM_DS_ELSP_TYPE (pDiffServParams)
                            == MPLS_DIFFSERV_PRECONF_ELSP)
                        {
                            LDP_FM_DS_SIG_ELSP (pDiffServParams) = NULL;
                        }
                        else
                        {
                            ElspMapRow.u1Creator = MPLS_CTRL_PROTO;
                            for (u1Exp = 0; u1Exp < 8; u1Exp++)
                            {
                                ElspMapRow.aElspExpPhbMapArray[u1Exp].
                                    u1IsExpValid = MPLS_FALSE;
                            }
                            pElspInfoList = (LDP_TE_DS_ELSPINFOLIST
                                             (LDP_TE_DS_PARAMS_ELSPLIST_PTR
                                              (pDiffServTnlInfo)));

                            TMO_SLL_Scan (pElspInfoList, pTempElspInfo,
                                          tMplsDiffServElspInfo *)
                            {
                                u1Exp =
                                    (UINT1) (LDP_TE_DS_ELSPINFO_INDEX
                                             (pTempElspInfo) - 1);

                                ElspMapRow.aElspExpPhbMapArray[u1Exp].
                                    u1IsExpValid = MPLS_TRUE;
                                ElspMapRow.aElspExpPhbMapArray[u1Exp].
                                    u1ElspPhbDscp =
                                    LDP_TE_DS_ELSPINFO_PHB_DSCP (pTempElspInfo);
                            }
                            LDP_FM_DS_SIG_ELSP (pDiffServParams) = &ElspMapRow;
                        }
                    }
                    Nhlfe.pDiffServParams = pDiffServParams;
                }

            }
        }
    }
    pLspInfo->pNhlfe = (tNHLFE *) (&Nhlfe);

    /* 
     * MPLS_FM API invoked to update the MLIB - In case of third party
     * MPLS-FM this has to be appropriately ported 
     */
    pLspInfo->u1Owner = MPLS_OWNER_LDP;

    /* Setting the direction of the LSP as forward. */
    /* This needs to be updated appropriately. */
    pLspInfo->Direction = MPLS_DIRECTION_FORWARD;

#ifdef LDP_GR_WANTED
    /* Since the function is getting called from the LDP context
     * No need to take LDP LOCK before accesing the global variable */

    /* If the node supports retaining the MPLS Fwd entries and the PEER
     * node supports GR and the node is in process of SHUTDOWN and Mlib operation 
     * is either FTN Delete or ILM Delete, NPAPI should be Blocked */

    LDP_DBG5 (LDP_DBG_PRCS,
              "%s: gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrProgressStatus: %u,"
              "gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrCapability: %u, "
              "u1PeerGrCapability: %u, u2MlibOperation: %u\n", __func__,
              gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrProgressStatus,
              gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrCapability,
              u1PeerGrCapability, u2MlibOperation);
    if (((gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrProgressStatus ==
          LDP_GR_SHUT_DOWN_IN_PROGRESS)
         && (gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrCapability ==
             LDP_GR_CAPABILITY_FULL) && u1PeerGrCapability == MPLS_TRUE)
        && ((u2MlibOperation == MPLS_MLIB_FTN_DELETE)
            || (u2MlibOperation == MPLS_MLIB_ILM_DELETE)))
    {
        LDP_DBG1 (LDP_DBG_PRCS, "%s Marking pLspInfo->u1IsNPBlock as TRUE\n",
                  __func__);
        pLspInfo->u1IsNPBlock = MPLS_TRUE;
    }
#endif
    if (MplsMlibUpdate (u2MlibOperation, pLspInfo) == LDP_SUCCESS)
    {
        if ((pLspCtrlBlock != NULL)
            && (u2MlibOperation == MPLS_MLIB_FTN_CREATE))
        {
            LCB_MLIB_UPD_STATUS (pLspCtrlBlock) = 0;
            LDP_DBG1 (LDP_DBG_PRCS,
                      "Updating the DLCB MLIB STATUS to Programmed for %x\n",
                      pLspCtrlBlock);
        }
        return LDP_SUCCESS;
    }
    else
    {
        if ((pLspCtrlBlock != NULL)
            && (u2MlibOperation == MPLS_MLIB_FTN_CREATE))
        {
            LCB_MLIB_UPD_STATUS (pLspCtrlBlock) = LDP_DU_DN_MLIB_UPD_NOT_DONE;
            LDP_DBG1 (LDP_DBG_PRCS,
                      "Updating the DLCB MLIB STATUS to Not Programmed for %x\n",
                      pLspCtrlBlock);
        }

    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpEstbTargetLspForEgpRouteEntry                          */
/* Description   : This routine Triggeres Targetted lsp creation for the     */
/*                 destination, mask, nexthop address, and                   */
/*                 interface index.                                          */
/* Input(s)      : u4DestNet     -   Destination Net                         */
/*               : u4DestMask    -   Destination Mask                        */
/*               : u4NextHop     -   Next hop Address                        */
/*               : u4RtIfIndx    -   Outgoing Interface Index                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

UINT1
LdpEstbTargetLspForEgpRouteEntry (UINT4 u4DestNet, UINT4 u4DestMask,
                                  UINT4 u4NextHop, UINT4 u4RtIfIndx)
{
    UINT1               u1SsnState;
    UINT4               u4HSessIndex = 0;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tTMO_HASH_NODE     *pSsnHashNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1               u1PrefLen = 0;
    tLdpIpRtInfo        InRtInfo;
    UINT2               u2IncarnId = 0;
    tIpv4Addr           NetworkAddr;
    tIpv4Addr           NextHopAddr;

    /* Get the prefix length from the Destination Mask */
    LDP_GET_PRFX_LEN (u4DestMask, u1PrefLen);

    u4DestNet = OSIX_HTONL (u4DestNet);
    u4NextHop = OSIX_HTONL (u4NextHop);
    MEMSET (&InRtInfo, 0, sizeof (tLdpIpRtInfo));
    MEMCPY (&NetworkAddr, (UINT1 *) &u4DestNet, sizeof (tIpv4Addr));
    MEMCPY (&NextHopAddr, (UINT1 *) &u4NextHop, sizeof (tIpv4Addr));
    InRtInfo.u4DestNet = OSIX_NTOHL (u4DestNet);
    InRtInfo.u4DestMask = u4DestMask;
    InRtInfo.u4NextHop = OSIX_NTOHL (u4NextHop);
    InRtInfo.u4RtIfIndx = u4RtIfIndx;

    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

    TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HSessIndex)
    {
        TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HSessIndex,
                              pSsnHashNode, tTMO_HASH_NODE *)
        {
            pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);

            SSN_GET_SSN_STATE (pLdpSession, u1SsnState);
            if ((u1SsnState == LDP_SSM_ST_OPERATIONAL)
                && (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
                    == LDP_TRUE))
            {
                if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
                    (SSN_MRGTYPE (pLdpSession) == LDP_NO_MRG))
                {
                    LdpNonMrgIntLspSetReq (u2IncarnId, NetworkAddr,
                                           u1PrefLen, NextHopAddr,
                                           u4RtIfIndx, NULL);
                    return LDP_SUCCESS;
                }

                if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
                    ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
                     (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG)))
                {
                    LdpEstbLspsForDoDMrgSsns (&InRtInfo, pLdpSession);
                }
                else if (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT)
                {
                    LdpEstbLspsForDuSsns (pLdpSession, &InRtInfo);
                }
            }
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDelTargetLspForRouteEntry                              */
/* Description   : This routine Triggeres Targetted lsp deletion for the     */
/*                 destination, mask, nexthop address, and                   */
/*                 interface index.                                          */
/* Input(s)      : u4DestNet     -   Destination Net                         */
/*               : u4DestMask    -   Destination Mask                        */
/*               : u4NextHop     -   Next hop Address                        */
/*               : u4RtIfIndx    -   Outgoing Interface Index                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

UINT1
LdpDelTargetLspForRouteEntry (UINT4 u4DestNet, UINT4 u4DestMask,
                              UINT4 u4NextHop, UINT4 u4RtIfIndx)
{

    UINT4               u4HIndex = 0;
    UINT1               u1PrefLen = 0;
    tLdpRouteEntryInfo *pRouteInfo = NULL;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT2               u2IncarnId = 0;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;

    pRouteInfo = (tLdpRouteEntryInfo *) MemAllocMemBlk (LDP_IP_RT_POOL_ID);

    if (pRouteInfo == NULL)
    {
        LDP_DBG (LDP_IF_MEM,
                 "IF: Memory Allocation for Ip Route Info failed \n");
        return LDP_FAILURE;
    }

    MEMSET (pRouteInfo, 0, sizeof (tLdpRouteEntryInfo));
    LDP_GET_PRFX_LEN (u4DestMask, u1PrefLen);

    LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr) = u4DestNet;
    LDP_IPV4_U4_ADDR (pRouteInfo->DestMask) = u4DestMask;
    LDP_IPV4_U4_ADDR (pRouteInfo->NextHop) = u4NextHop;
    pRouteInfo->u4RtIfIndx = u4RtIfIndx;

    /* 
     * Get the session that is associated with the NextHopAddr & 
     * ifIndex. The peer that has advertised the NextHopAddr in 
     * its LDP Address msg is located. If no such peer exists, 
     * implies no Lsps associated.
     * If the peer exists, the session associated with the peer 
     * is accessed for further processing.
     */

    u4HIndex = LDP_COMPUTE_HASH_INDEX (LDP_IPV4_U4_ADDR (pRouteInfo->NextHop));

    TMO_HASH_Scan_Bucket (LDP_PEER_IFADR_TABLE (u2IncarnId),
                          u4HIndex, pIfAddrNode, tPeerIfAdrNode *)
    {
        if (pIfAddrNode->AddrType != MPLS_IPV4_ADDR_TYPE)
        {
            continue;
        }

        if (LDP_IPV4_U4_ADDR (pRouteInfo->NextHop) ==
            OSIX_NTOHL (*((UINT4 *) (VOID *) (&(pIfAddrNode->IfAddr)))))
        {
            pLdpSession = pIfAddrNode->pSession;
            break;
        }
    }

    if (pLdpSession == NULL)
    {
        /* 
         * No Session exists with given Next Hop Address => Lsps 
         * effected by the deletion of this route entry are NONE.
         */
        /* 
         * Route info allocated when the IP event was indicated is 
         * released. 
         */
        MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
        return LDP_SUCCESS;
    }

    /* 
     * Scanning all the DownStream LSP control Blocks to get 
     * the Lsps that are effected by the Deletion of this route 
     * entry. 
     */
    TMO_SLL_Scan (&SSN_DLCB (pLdpSession), pSllNode, tTMO_SLL_NODE *)
    {
        pLspCtrlBlk = SLL_TO_LCB (pSllNode);
        if ((u4DestNet == LDP_IPV4_U4_ADDR (pLspCtrlBlk->Fec.Prefix)) &&
            (u1PrefLen == pLspCtrlBlk->Fec.u1PreLen))
        {
            /* 
             * Destroying the Lsp, that was established using 
             * the Deleted Route Entry.
             */

            LdpTriggerRtChgEvent (u2IncarnId, LDP_RT_DELETED,
                                  pRouteInfo, pLspCtrlBlk, pLdpSession);
        }
    }
    MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpEstbLspsForAllRouteEntries                            */
/* Description   : This routine gets each entry present in the IP Routing    */
/*                 Table, and establishes LSP for the destination for which  */
/*                 the NextHop is reachable on a given Session Entry.        */
/*                 This routine is called initially when LDP receives an     */
/*                 Internal Event(SESSION_UP Event). The Session Up Event is */
/*                 generated when an Ldp Session becomes operational.        */
/* Input(s)      : pSessionEntry - Points to the session Entry               */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpEstbLspsForAllRouteEntries (tLdpSession * pLdpSession)
{
    tLdpIpRtInfo        InRtInfo;
    tNetIpv4RtInfo      NetIpRtInfo, NextNetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      BestNetIpRtInfo;

    InRtInfo.u4DestNet = 0;
    InRtInfo.u4DestMask = 0;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&NextNetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));

    NetIpRtInfo.u4DestNet = 0;
    NetIpRtInfo.u4DestMask = 0;

    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (&BestNetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));

    while ((NetIpv4GetNextFwdTableRouteEntry (&NetIpRtInfo, &NextNetIpRtInfo))
           == NETIPV4_SUCCESS)
    {
        if (LDP_SESSION_STATE (pLdpSession) != LDP_SSM_ST_OPERATIONAL)
        {
            LDP_DBG (LDP_IF_MISC,
                     "LdpEstbLspsForAllRouteEntries: session is not operationally up\n");
            break;
        }
        if (NetIpv4GetCfaIfIndexFromPort (NextNetIpRtInfo.u4RtIfIndx,
                                          &InRtInfo.u4RtIfIndx) ==
            NETIPV4_FAILURE)
        {
            MEMCPY (&NetIpRtInfo, &NextNetIpRtInfo, sizeof (tNetIpv4RtInfo));
            continue;
        }
        InRtInfo.u4DestNet = NextNetIpRtInfo.u4DestNet;
        InRtInfo.u4DestMask = NextNetIpRtInfo.u4DestMask;
        InRtInfo.u4Tos = NextNetIpRtInfo.u4Tos;
        InRtInfo.u4NextHop = NextNetIpRtInfo.u4NextHop;
        InRtInfo.u4RtAge = NextNetIpRtInfo.u4RtAge;
        InRtInfo.i4Metric1 = NextNetIpRtInfo.i4Metric1;
        InRtInfo.u4RowStatus = NextNetIpRtInfo.u4RowStatus;
        InRtInfo.u4RouteTag = NextNetIpRtInfo.u4RouteTag;
        InRtInfo.u2RtProto = NextNetIpRtInfo.u2RtProto;
        InRtInfo.u2RtType = NextNetIpRtInfo.u2RtType;

        RtQuery.u4DestinationIpAddress = InRtInfo.u4DestNet;
        RtQuery.u4DestinationSubnetMask = InRtInfo.u4DestMask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        if ((SSN_GET_ENTITY (pLdpSession)->u2LabelRet ==
             LDP_CONSERVATIVE_MODE) &&
            (NetIpv4GetRoute (&RtQuery, &BestNetIpRtInfo) ==
             NETIPV4_SUCCESS) &&
            (BestNetIpRtInfo.u4NextHop != InRtInfo.u4NextHop))
        {
            MEMCPY (&NetIpRtInfo, &NextNetIpRtInfo, sizeof (tNetIpv4RtInfo));
            continue;
        }

        LDP_DBG2 (LDP_IF_MISC,
                  "LdpEstbLspsForAllRouteEntries FEC %x NextHop %x\n",
                  InRtInfo.u4DestNet, InRtInfo.u4NextHop);

        if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
            (SSN_MRGTYPE (pLdpSession) == LDP_NO_MRG))
        {
            LdpGetLspRouteInfo (&InRtInfo, pLdpSession);
        }
        else if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
                 ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
                  (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG)))
        {
            LdpEstbLspsForDoDMrgSsns (&InRtInfo, pLdpSession);

        }
        else if (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT)
        {
            LdpEstbLspsForDuSsns (pLdpSession, &InRtInfo);
        }
        MEMCPY (&NetIpRtInfo, &NextNetIpRtInfo, sizeof (tNetIpv4RtInfo));
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpEstbLspsForDoDMrgSsns                                 */
/* Description   : This routine gets each entry present in the IP Routing    */
/*                 Table, and establishes LSP for DoD Mrg Sessions           */
/* Input(s)      : pInRtInfo, pLdpSession                                    */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
VOID
LdpEstbLspsForDoDMrgSsns (tLdpIpRtInfo * pInRtInfo, tLdpSession * pLdpSession)
{
    UINT1               u1PrefLen;
    tTMO_SLL           *pPeerAddrList = NULL;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tPeerIfAdrNode     *pTmpIfAddrNode = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlock = NULL;
    tLdpMsgInfo         MsgInfo;

    MEMSET (&MsgInfo, 0, sizeof (tLdpMsgInfo));

    /* Getting the Peer's Interface Addresses' List */
    pPeerAddrList = SSN_GET_PEER_IFADR_LIST (pLdpSession);

    /* Getting the Prefix length from the obtained Destination Mask */
    LDP_GET_PRFX_LEN (pInRtInfo->u4DestMask, u1PrefLen);

    /*
     * Scanning through the Peer's addresses and establishing Lsp, if the
     * Next Hop Address through which the Destination is reachable, belongs
     * to the LdpPeer associated with the present LdpSession.
     */

    TMO_SLL_Scan (pPeerAddrList, pTmpIfAddrNode, tPeerIfAdrNode *)
    {
        pIfAddrNode = LDP_OFFSET (pTmpIfAddrNode);

        if (pIfAddrNode->AddrType != MPLS_IPV4_ADDR_TYPE)
        {
            continue;
        }

        if (((SSN_GET_ENTITY (pLdpSession)->u2LabelRet) ==
             LDP_CONSERVATIVE_MODE))
        {
            if (OSIX_NTOHL (*(UINT4 *) (VOID *) &(pIfAddrNode->IfAddr)) ==
                pInRtInfo->u4NextHop)
            {
                pUstrCtrlBlock = NULL;
                LdpCreateUpstrCtrlBlock (&pUstrCtrlBlock);
                if (pUstrCtrlBlock == NULL)
                {
                    return;
                }
                pUstrCtrlBlock->Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
                pUstrCtrlBlock->Fec.u1PreLen = u1PrefLen;
                pUstrCtrlBlock->Fec.u2AddrFmly = MPLS_IPV4_ADDR_TYPE;
                LDP_IPV4_U4_ADDR (pUstrCtrlBlock->Fec.Prefix) =
                    pInRtInfo->u4DestNet;
                UPSTR_STATE (pUstrCtrlBlock) = LDP_LSM_ST_IDLE;
                LdpMergeUpSm (pUstrCtrlBlock, LDP_USM_EVT_INT_LDP_REQ,
                              &MsgInfo);
                break;
            }
        }
        else if (((SSN_GET_ENTITY (pLdpSession)->u2LabelRet) ==
                  LDP_LIBERAL_MODE))
        {
            if (LdpIsLsrPartOfIpv4ErHopOrFec (pInRtInfo->u4DestNet,
                                              pInRtInfo->u4DestMask) ==
                LDP_FALSE)
            {
                pUstrCtrlBlock = NULL;
                LdpCreateUpstrCtrlBlock (&pUstrCtrlBlock);
                if (pUstrCtrlBlock == NULL)
                {
                    return;
                }
                pUstrCtrlBlock->Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
                pUstrCtrlBlock->Fec.u1PreLen = u1PrefLen;
                pUstrCtrlBlock->Fec.u2AddrFmly = IPV4;
                LDP_IPV4_U4_ADDR (pUstrCtrlBlock->Fec.Prefix) =
                    pInRtInfo->u4DestNet;
                UPSTR_STATE (pUstrCtrlBlock) = LDP_LSM_ST_IDLE;
                LdpMergeUpSm (pUstrCtrlBlock,
                              LDP_USM_EVT_INT_LDP_REQ, &MsgInfo);
                break;
            }
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpEstbLspsForDuSsns                                      */
/* Description   : This routine gets each entry present in the IP Routing    */
/*                 Table, and establishes LSP for the destination for which  */
/*                 the NextHop is reachable on a given Session Entry.        */
/*                 This routine is called initially when LDP receives an     */
/*                 Internal Event(SESSION_UP Event). The Session Up Event is */
/*                 generated when an Ldp Session becomes operational.        */
/* Input(s)      : pSessionEntry - Points to the session Entry               */
/*               : InRtInfo- Contains the Route Info                         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

VOID
LdpEstbLspsForDuSsns (tLdpSession * pLdpSession, tLdpIpRtInfo * pInRtInfo)
{
    tFec                Fec;
    UINT1               u1PrefLen;
    UINT1               u1SsnState;
    UINT1               u1DnCtrlBlockPresent = FALSE;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pTempLspUpCtrlBlock = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tLspCtrlBlock      *pTempLspCtrlBlock = NULL;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tLdpSession        *pLdpSessionEntry = NULL;
    tLdpSession        *pTempLdpSessionEntry = NULL;
    tLdpMsgInfo         LdpMsgInfo;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL           *pPeerIfList = NULL;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tPeerIfAdrNode     *pTempSllNode = NULL;
    tTMO_SLL           *pEntityList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pLdpSession);
    UINT2               u2RtPort = 0;
    UINT4               u4RtGw = 0;
#ifdef LDP_GR_WANTED
    tLdpAdjacency      *pLdpAdjacency = NULL;
    UINT4               u4AdjIfIndex = 0;
    tILMHwListEntry    *pILMHwListEntry = NULL;
    tMplsIpAddress      ILMHwListFec;
    UINT1               u1IsUpstrAllocReq = LDP_TRUE;
    UINT4               u4RecoveryTimeRem = 0;
#endif
    tGenU4Addr          NextHop;
    tGenU4Addr          TempNextHop;
    tGenU4Addr          RtGw;
    uGenU4Addr          TempAddr;
    tGenU4Addr          NextHopAddr;
    tFtnEntry          *pFtnEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tLdpIpRtInfo        TempInRtInfo;
    tLdpIpRtInfo       *pRt = NULL;
    tLdpIpRtInfo       *pTmpRt = NULL;
    UINT4               u4OutIndex = LDP_ZERO;
    UINT4               u4NextHopAddr = LDP_ZERO;
    UINT1               u1SsnFound = LDP_FALSE;

#ifdef LDP_GR_WANTED
    MEMSET (&ILMHwListFec, 0, sizeof (tMplsIpAddress));
#endif
    MEMSET (&TempAddr, LDP_ZERO, sizeof (uGenU4Addr));
    MEMSET (&NextHop, LDP_ZERO, sizeof (tGenU4Addr));
    MEMSET (&RtGw, LDP_ZERO, sizeof (tGenU4Addr));
    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));
    MEMSET (&TempInRtInfo, LDP_ZERO, sizeof (tLdpIpRtInfo));

    if ((LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
         == LDP_TRUE) &&
        (SSN_GET_ENTITY (pLdpSession)->InStackTnlInfo.u4TnlId == LDP_ZERO))
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "LdpEstbLspsForDuSsns: Labels can't be "
                 "negotiated when out-tunnel is not "
                 "associated with LDP targeted session\n");
        return;
    }

    LDP_DBG3 (LDP_SSN_PRCS,
              "LdpEstbLspsForDuSsns for FEC %x Nexthop %x RtIfIndex %x\n",
              pInRtInfo->u4DestNet, pInRtInfo->u4NextHop,
              pInRtInfo->u4RtIfIndx);
    LDP_DBG5 (LDP_SSN_PRCS, "%s Establishing LSP with Peer: %d.%d.%d.%d\n",
              __func__, pLdpSession->pLdpPeer->PeerLdpId[0],
              pLdpSession->pLdpPeer->PeerLdpId[1],
              pLdpSession->pLdpPeer->PeerLdpId[2],
              pLdpSession->pLdpPeer->PeerLdpId[3]);
    MEMSET (&LdpMsgInfo, 0, sizeof (tLdpMsgInfo));
    LDP_GET_PRFX_LEN (pInRtInfo->u4DestMask, u1PrefLen);

    MEMSET (&Fec, 0, sizeof (tFec));

    Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
    Fec.u2AddrFmly = IPV4;
    Fec.u1PreLen = u1PrefLen;
    LDP_IPV4_U4_ADDR (Fec.Prefix) = pInRtInfo->u4DestNet;

    /* Check for downstream session existence 
     * For LDP targeted session case, there may be a session with a remote peer, 
     * not with a directly connected one. */

    LDP_IPV4_U4_ADDR (NextHop.Addr) = pInRtInfo->u4NextHop;
    NextHop.u2AddrType = LDP_ADDR_TYPE_IPV4;

    if (LdpIsSsnForFec (&NextHop, pInRtInfo->u4RtIfIndx,
                        pLdpSession) == LDP_FALSE)
    {

        /* This section applicable for 
         * 1. Egress route case, nexthop will be zero, 
         * so LDP label mapping message has to be sent to upstream.
         * 2. Intermediate router with valid nexthop case, 
         * if label allocation mode is INDEPENDENT 
         * only the label message will be sent upstream, 
         * otherwise ORDERED label allocation mode, 
         * we expect the downstream router to 
         * send the label mapping message for the corresponding FECs.
         * 3. Intermediate router with valid nexthop case, 
         * here route table will not give you the remote nexthop, 
         * so we have to find out the targeted session by 
         * scanning all the downstream sessions. 
         * if Label allocation mode is ORDERED, 
         * then we have to find out the targeted sessions */
        pPeerIfList = SSN_GET_PEER_IFADR_LIST (pLdpSession);
        TMO_DYN_SLL_Scan (pPeerIfList, pIfAddrNode,
                          pTempSllNode, tPeerIfAdrNode *)
        {
            pIfAddrNode = LDP_OFFSET (pIfAddrNode);

            if (pIfAddrNode->AddrType != MPLS_IPV4_ADDR_TYPE)
            {
                continue;
            }

            if (((OSIX_NTOHL (*(UINT4 *) (VOID *) &(pIfAddrNode->IfAddr)))
                 & (pInRtInfo->u4DestMask)) ==
                ((LDP_IPV4_U4_ADDR (Fec.Prefix)) & (pInRtInfo->u4DestMask)))
            {
                /* The peer is part of this route and hence the
                 * map message or Label is not updated to the 
                 * peer
                 */
                LDP_DBG ((LDP_ADVT_PRCS | LDP_SSN_PRCS),
                         "IF: The peer is part of this route and hence"
                         " the map message or Label is not updated to the peer\n");
                return;
            }
        }

        TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession), pSllNode,
                          pTempLdpSllNode, tTMO_SLL_NODE *)
        {
            pTempLspUpCtrlBlock = (tUstrLspCtrlBlock *) (pSllNode);

            LDP_IPV4_U4_ADDR (TempAddr) = pInRtInfo->u4DestNet;
            if ((LdpIsAddressMatchWithAddrType
                 (&TempAddr, &pTempLspUpCtrlBlock->Fec.Prefix,
                  LDP_ADDR_TYPE_IPV4,
                  pTempLspUpCtrlBlock->Fec.u2AddrFmly) == LDP_TRUE)
                && (u1PrefLen == pTempLspUpCtrlBlock->Fec.u1PreLen))
            {
#ifdef LDP_GR_WANTED
                /* If upstream control block is found to be stale,
                 * then it will be in the ESTABLISHED state, as all other
                 * control blocks are already deleted in helper mode */
                if (pTempLspUpCtrlBlock->u1StaleStatus == LDP_TRUE)
                {
                    LDP_DBG2 (GRACEFUL_DEBUG,
                              "LdpEstbLspsForDuSsns: Upstream control exists in STALE state "
                              "for the Prefix: %x, Control block state = %d\n",
                              LDP_IPV4_U4_ADDR (pTempLspUpCtrlBlock->Fec.
                                                Prefix),
                              pTempLspUpCtrlBlock->u1LspState);
                    u1IsUpstrAllocReq = LDP_FALSE;
                    break;
                }
#endif
                /* For this prefix, LSP is already available in this session */
                /* MPLS IP forwarding looping will occur if the 
                 * same LSP is created on both Upstream and 
                 * downstream LSP sessions */
                if (pTempLspUpCtrlBlock->u1LspState == LDP_DU_UP_LSM_ST_REL_AWT)
                {
                    LDP_DBG1 ((LDP_ADVT_PRCS | LDP_SSN_PRCS),
                              "LSP for prefix %x is in Release await state\n",
                              LDP_IPV4_U4_ADDR (pTempLspUpCtrlBlock->Fec.
                                                Prefix));
                    break;
                }
                else
                {
                    LDP_DBG3 ((LDP_ADVT_PRCS | LDP_SSN_PRCS),
                              "For this prefix %x %x, LSP is already "
                              "available in this session:%d\n",
                              pInRtInfo->u4DestNet, pInRtInfo->u4DestMask,
                              pTempLspUpCtrlBlock->u1LspState);
                    return;
                }
            }
        }

#ifdef LDP_GR_WANTED
        /* At this point of time, the Session has already come up,
         * Thus GR timer, if present (running) will be recovery timer
         * and not the reconnect timer */
        if (TmrGetRemainingTime (LDP_TIMER_LIST_ID,
                                 &(pLdpSession->pLdpPeer->GrTimer.AppTimer),
                                 &u4RecoveryTimeRem) == TMR_FAILURE)
        {
            LDP_DBG (GRACEFUL_DEBUG,
                     "LdpEstbLspsForDuSsns: Error in Getting the "
                     "Remaining Time for the GR Recovery Timer\n");
            return;
        }

        /* If the Recovery timer is not running=>
         * This implies this is not the Helper node */
        if (u4RecoveryTimeRem == 0)
        {
            /* Fetch the ILM Hw List entry for session */
            ILMHwListFec.i4IpAddrType = MPLS_LSR_ADDR_IPV4;
            MEMCPY (ILMHwListFec.IpAddress.au1Ipv4Addr,
                    &(pInRtInfo->u4DestNet), IPV4_ADDR_LENGTH);

            TMO_SLL_Scan (SSN_GET_ADJ_LIST (pLdpSession), pLdpAdjacency,
                          tLdpAdjacency *)
            {
                u4AdjIfIndex = pLdpAdjacency->u4IfIndex;
                pILMHwListEntry =
                    MplsGetILMHwListEntryFromFecAndInIntf (ILMHwListFec,
                                                           u1PrefLen,
                                                           u4AdjIfIndex);
                if (pILMHwListEntry != NULL)
                {
                    LDP_DBG2 (GRACEFUL_DEBUG,
                              "LdpEstbLspsForDuSsns: ILM Hw List Entry found for "
                              "the prefix: %x and interface: %d\n",
                              pInRtInfo->u4DestNet, u4AdjIfIndex);
                    /* ILM hw list entry found */
                    break;
                }
            }
            if (pILMHwListEntry != NULL)
            {
                /* Check if the ILM hw list entry is SWAP based and Label is 
                   rcvd from DOWNSTTREAM router */
                if ((MPLS_ILM_HW_LIST_OUT_L3_INTF (pILMHwListEntry) != 0) &&
                    (LdpIsLblRcvdFromDownstreamRtr
                     (pLdpSession, Fec, &NextHop,
                      pInRtInfo->u4RtIfIndx) == LDP_FALSE))
                {
                    /* The processing of such entries will be done when LBL_MAP
                     * will arrive from the downstream router */
                    LDP_DBG1 (GRACEFUL_DEBUG,
                              "LdpEstbLspsForDuSsns: For the Prefix %x, ILM Hw List Entry "
                              "with SWAP operation exists, Hene skipping the processing here\n",
                              pInRtInfo->u4DestNet);
                    return;
                }
            }
        }
#endif
        LDP_DBG2 (LDP_ADVT_PRCS, "Label distribution for FEC %x Nexthop %x \n",
                  pInRtInfo->u4DestNet, pInRtInfo->u4NextHop);
#ifdef LDP_GR_WANTED
        if (u1IsUpstrAllocReq == LDP_TRUE)
        {
            /* Create up ctrl blk */
            /* Add to the dn ctrl block */
            /* initialise the us ctl block */
            pLspUpCtrlBlock = NULL;
            LdpCreateUpstrCtrlBlock (&pLspUpCtrlBlock);
            if (pLspUpCtrlBlock == NULL)
            {
                LDP_DBG (GRACEFUL_DEBUG,
                         "Memory Allocation FAILED for Upstream Control Block\n");
                return;
            }
            pLspUpCtrlBlock->u1InLblType = SSN_GET_LBL_TYPE (pLdpSession);
            UPSTR_INCOM_IFINDX (pLspUpCtrlBlock) =
                LdpSessionGetIfIndex (pLdpSession, Fec.u2AddrFmly);
            pLspUpCtrlBlock->pUpstrSession = pLdpSession;
            pLspUpCtrlBlock->u1LspState = LDP_DU_UP_LSM_ST_IDLE;
            UPSTR_UREQ_ID (pLspUpCtrlBlock) = LDP_INVALID_REQ_ID;
            if (pInRtInfo->u4NextHop == LDP_ZERO)
            {
                pLspUpCtrlBlock->bIsConnectedRoute = LDP_TRUE;
            }
            MEMCPY (&LCB_FEC (pLspUpCtrlBlock), &Fec, sizeof (tFec));
            TMO_SLL_Add (&SSN_ULCB (pLdpSession),
                         (tTMO_SLL_NODE
                          *) (&(pLspUpCtrlBlock->NextUpstrCtrlBlk)));
        }
        else
        {
            pLspUpCtrlBlock = pTempLspUpCtrlBlock;
            if (pLspUpCtrlBlock == NULL)
            {
                LDP_DBG (GRACEFUL_DEBUG,
                         "Memory Allocation FAILED for Upstream Control Block\n");
                return;
            }

        }
#else
        pLspUpCtrlBlock = NULL;
        LdpCreateUpstrCtrlBlock (&pLspUpCtrlBlock);
        if (pLspUpCtrlBlock == NULL)
        {
            LDP_DBG (GRACEFUL_DEBUG,
                     "Memory Allocation FAILED for Upstream Control Block\n");
            return;
        }

        pLspUpCtrlBlock->u1InLblType = SSN_GET_LBL_TYPE (pLdpSession);
        MEMCPY (&LCB_FEC (pLspUpCtrlBlock), &Fec, sizeof (tFec));
        UPSTR_INCOM_IFINDX (pLspUpCtrlBlock) =
            LdpSessionGetIfIndex (pLdpSession, Fec.u2AddrFmly);
        pLspUpCtrlBlock->pUpstrSession = pLdpSession;
        pLspUpCtrlBlock->u1LspState = LDP_DU_UP_LSM_ST_IDLE;
        UPSTR_UREQ_ID (pLspUpCtrlBlock) = LDP_INVALID_REQ_ID;
        if (pInRtInfo->u4NextHop == LDP_ZERO)
        {
            pLspUpCtrlBlock->bIsConnectedRoute = LDP_TRUE;
        }
        TMO_SLL_Add (&SSN_ULCB (pLdpSession),
                     (tTMO_SLL_NODE *) (&(pLspUpCtrlBlock->NextUpstrCtrlBlk)));
#endif

        pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
        TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
        {
            TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
            {
                if ((pLdpPeer->pLdpSession) == NULL)
                {
                    continue;
                }
                /* Target sessions also we need to 
                 * consider for LDP over RSVP case */
                pLdpSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;

                if ((LDP_ENTITY_IS_TARGET_TYPE
                     (SSN_GET_ENTITY (pLdpSessionEntry)) == LDP_TRUE)
                    && (SSN_GET_ENTITY (pLdpSessionEntry)->OutStackTnlInfo.
                        u4TnlId == LDP_ZERO))
                {
                    LDP_DBG (LDP_ADVT_PRCS,
                             "\n LdpEstbLspsForDuSsns: Labels can't be negotiated when "
                             "out-tunnel is not associated with LDP targeted session\n");
                    continue;
                }

                /* if dn ctl block exists chk if the dn ssn of dn ctl 
                 * block same as curr ssn */
                if (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
                    == LDP_FALSE)
                {
                    if (pLdpSessionEntry == pLdpSession)
                    {
                        continue;
                    }
                }
                SSN_GET_SSN_STATE (pLdpSessionEntry, u1SsnState);
#ifdef LDP_GR_WANTED
                /* If on the transit node, both sessions are stale (both peers restarted)
                 * then the DLCB which existed on the stale session (on stale sessions
                 * only Established DLCB's exist) should be used. If this check is not applied
                 * it will skip the stale session and the ESTABLISHED DLCB on stale session
                 * will not be used */
                if (pLdpSessionEntry->u1StaleStatus == LDP_FALSE)
                {
                    if (!((u1SsnState == LDP_SSM_ST_OPERATIONAL) &&
                          (SSN_ADVTYPE (pLdpSessionEntry) ==
                           LDP_DSTR_UNSOLICIT)))
                    {
                        continue;
                    }
                }
#else
                /*Session state should be Operational & ADV. Type should be Unsolicited */
                if (!((u1SsnState == LDP_SSM_ST_OPERATIONAL) &&
                      (SSN_ADVTYPE (pLdpSessionEntry) == LDP_DSTR_UNSOLICIT)))
                {
                    continue;
                }
#endif

                /* Scan the Downstream Lsp Control Block List */
                TMO_SLL_Scan (&SSN_DLCB (pLdpSessionEntry), pLdpSllNode,
                              tTMO_SLL_NODE *)
                {
                    pLspCtrlBlock = SLL_TO_LCB (pLdpSllNode);
                    if ((MEMCMP (&pLspCtrlBlock->Fec, &(Fec),
                                 sizeof (tFec))) != 0)
                    {
                        continue;
                    }

                    if (LdpGetNonTeLspOutSegmentFromPrefix
                        ((uGenU4Addr *) & (LCB_FEC (pLspCtrlBlock).Prefix),
                         &u4OutIndex) == MPLS_SUCCESS)
                    {
                        pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
                    }

                    if ((pOutSegment != NULL)
                        && (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) ==
                            LDP_DU_DN_MLIB_UPD_NOT_DONE))
                    {
                        continue;
                    }

                    /* Check has been created if Dwnstrm CntrlBlk exist in state of other than Establish
                     * 'If' condition will take care of ECMP path and elseif condition will take care
                     * of non ECMP path*/
                    if ((pOutSegment != NULL)
                        && (LCB_STATE (pLspCtrlBlock) != LDP_LSM_ST_EST))
                    {
                        continue;
                    }
                    else if (LCB_STATE (pLspCtrlBlock) != LDP_LSM_ST_EST)
                    {
                        LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
                        return;
                    }

                    if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) ==
                        LDP_DU_DN_MLIB_UPD_NOT_DONE)
                    {
                        /* For remote targeted session i.e not directly 
                         * connected router
                         * case, there will not be a sesion for a 
                         * route table's nexthop address. 
                         * So, here we do the FTN and ILM programming */

                        if (LDP_ENTITY_IS_TARGET_TYPE
                            (SSN_GET_ENTITY (pLdpSessionEntry)) == LDP_TRUE)
                        {
                            SSN_GET_PEER_TRANSADDR (LDP_IPV4_U4_ADDR
                                                    (pLspCtrlBlock->
                                                     NextHopAddr),
                                                    LCB_DSSN (pLspCtrlBlock));

                            /* Route nexthop and the targeted session's 
                             * transport 
                             * address should be in the same network, 
                             * otherwise continue */
                            if (LdpIpUcastRouteQuery
                                (LDP_IPV4_U4_ADDR (pLspCtrlBlock->NextHopAddr),
                                 &u2RtPort, &u4RtGw) == LDP_IP_SUCCESS)
                            {
                                if (u4RtGw != pInRtInfo->u4NextHop)
                                {
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            LDP_DBG4 (LDP_SSN_PRCS,
                                      "LdpEstbLspsForDuSsns pLdpSessionEntry PeerIp=%d.%d.%d.%d\n",
                                      pLdpSession->pLdpPeer->PeerLdpId[0],
                                      pLdpSession->pLdpPeer->PeerLdpId[1],
                                      pLdpSession->pLdpPeer->PeerLdpId[2],
                                      pLdpSession->pLdpPeer->PeerLdpId[3]);

                            LDP_DBG2 (LDP_SSN_PRCS,
                                      "LdpEstbLspsForDuSsns pInRtInfo->u4DestNet=%x u4RtGw=%x"
                                      " & LDP_DU_DN_MLIB_UPD_NOT_DONE",
                                      pInRtInfo->u4DestNet, u4RtGw);

                            MEMSET (&RtGw, LDP_ZERO, sizeof (tGenU4Addr));
                            LDP_IPV4_U4_ADDR (RtGw.Addr) = pInRtInfo->u4NextHop;
                            RtGw.u2AddrType = LDP_ADDR_TYPE_IPV4;
                            if (LdpIsSsnForFec
                                (&RtGw, pInRtInfo->u4RtIfIndx,
                                 pLdpSessionEntry) == LDP_FALSE)
                            {
                                LDP_DBG1 (LDP_ADVT_PRCS,
                                          "FEC %x Skipped \n",
                                          pInRtInfo->u4DestNet);
                                continue;
                            }

                            if (LdpGetNonTeLspOutSegmentFromPrefix
                                ((uGenU4Addr *) &
                                 (LCB_FEC (pLspCtrlBlock).Prefix),
                                 &u4OutIndex) == MPLS_SUCCESS)
                            {
                                pOutSegment =
                                    MplsGetOutSegmentTableEntry (u4OutIndex);
                            }

                            /*Case 1: If FTN is already present, donot recreate FTN
                             * Create ILM (Swap) with this FTN*/
                            if (pOutSegment != NULL)
                            {
                                LDP_DBG2 (LDP_SSN_PRCS,
                                          "LdpEstbLspsForDuSsns FTN FOUND for Fec = %x NH = %x\n",
                                          pInRtInfo->u4DestNet,
                                          pOutSegment->NHAddr.u4_addr[0]);

                                LDP_IPV4_U4_ADDR (TempNextHop.Addr) =
                                    pOutSegment->NHAddr.u4_addr[0];
                                TempNextHop.u2AddrType = LDP_ADDR_TYPE_IPV4;

                                if (LdpGetPeerSession
                                    (&TempNextHop, pInRtInfo->u4RtIfIndx,
                                     &pTempLdpSessionEntry,
                                     u2IncarnId) == LDP_SUCCESS)
                                {
                                    /* Scan the Downstream Lsp Control Block List */
                                    TMO_SLL_Scan (&SSN_DLCB
                                                  (pTempLdpSessionEntry),
                                                  pLdpSllNode, tTMO_SLL_NODE *)
                                    {
                                        pTempLspCtrlBlock =
                                            SLL_TO_LCB (pLdpSllNode);
                                        if ((MEMCMP
                                             (&pTempLspCtrlBlock->Fec, &(Fec),
                                              sizeof (tFec))) != 0)
                                        {
                                            continue;
                                        }
                                        if (LCB_MLIB_UPD_STATUS
                                            (pTempLspCtrlBlock) ==
                                            LDP_DU_DN_MLIB_UPD_DONE)
                                        {
                                            /* A.1.6. Recognize New FEC
                                             * For Downstream Unsolicited Independent Control
                                             * */

                                            LdpDuIndLblReqOrNewFecProcedure
                                                (TRUE, pLspUpCtrlBlock,
                                                 pTempLspCtrlBlock, &LdpMsgInfo,
                                                 pTempLdpSessionEntry,
                                                 &NextHop);

                                            /* A.1.1. Recognize New FEC
                                             * For Downstream Unsolicited ordered Control
                                             * */
                                            LdpDuOrderLblReqOrNewFecProcedure
                                                (TRUE, pLspUpCtrlBlock,
                                                 pTempLspCtrlBlock, &LdpMsgInfo,
                                                 pTempLdpSessionEntry,
                                                 &NextHop);

                                            LDP_DBG2 (LDP_SSN_PRCS,
                                                      "Upstr list-Dn Node %p FEC %x\n",
                                                      &UPSTR_DSTR_LIST_NODE
                                                      (pLspUpCtrlBlock),
                                                      LDP_IPV4_U4_ADDR (LCB_FEC
                                                                        (pLspUpCtrlBlock).
                                                                        Prefix));
                                            return;
                                        }
                                    }
                                }
                            }
                            /*Case 2: If No FTN is present, Create FTN & respective ILM (Swap) */
                            else
                            {
                                LDP_DBG1 (LDP_SSN_PRCS,
                                          "No FTN found for FEC %x\n",
                                          LDP_IPV4_U4_ADDR (Fec.Prefix));

                                TempInRtInfo.u4DestNet = pInRtInfo->u4DestNet;
                                TempInRtInfo.u4DestMask = pInRtInfo->u4DestMask;

                                RtmApiGetBestRouteEntryInCxt (u2IncarnId,
                                                              TempInRtInfo,
                                                              &pRt);
                                pTmpRt = pRt;

                                for (;
                                     ((pTmpRt != NULL)
                                      && (pTmpRt->i4Metric1 == pRt->i4Metric1));
                                     pTmpRt = pTmpRt->pNextAlternatepath)
                                {
                                    /*If Next Hop recieved is Best Route/Best ECMP Route, Program FTN
                                       with it */
                                    LDP_IPV4_U4_ADDR (NextHopAddr.Addr) =
                                        pTmpRt->u4NextHop;
                                    NextHopAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;

                                    if (LdpIsSsnForFec
                                        (&NextHopAddr,
                                         pLspCtrlBlock->u4OutIfIndex,
                                         LCB_DSSN (pLspCtrlBlock)) == LDP_TRUE)
                                    {
                                        u1SsnFound = LDP_TRUE;
                                        LDP_DBG1 (LDP_ADVT_PRCS,
                                                  "Session Found with NH = %x\n",
                                                  u4NextHopAddr);
                                        break;
                                    }
                                    LDP_DBG1 (LDP_ADVT_PRCS,
                                              "Session Not Found = %x \n",
                                              pTmpRt->u4NextHop);
                                }
                                pRt = NULL;
                                pTmpRt = NULL;
                                MEMSET (&TempInRtInfo, LDP_ZERO,
                                        sizeof (tLdpIpRtInfo));

                                /*No Rt/Best Rt found */
                                if (u1SsnFound == LDP_FALSE)
                                {
                                    LDP_DBG1 (LDP_ADVT_PRCS,
                                              "Session Not Found with NH = %x \n",
                                              pTmpRt->u4NextHop);
                                    continue;
                                }
                                LDP_IPV4_U4_ADDR (pLspCtrlBlock->NextHopAddr) =
                                    LDP_IPV4_U4_ADDR (NextHopAddr.Addr);
                                pLspCtrlBlock->u2AddrType = LDP_ADDR_TYPE_IPV4;
                            }
                        }

                        /* Create FTN, as its not created and update all the upstream with New ILM */
                        /* create ftn entry */

                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "LdpEstbLspsForDuSsns Creating FTN for Fec %x with Next Hop %x\n",
                                  pInRtInfo->u4DestNet,
                                  LDP_IPV4_U4_ADDR (NextHopAddr.Addr));

                        LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE,
                                               MPLS_OPR_PUSH, pLspCtrlBlock,
                                               NULL);
                        LCB_MLIB_UPD_STATUS (pLspCtrlBlock) =
                            LDP_DU_DN_MLIB_UPD_DONE;
                    }

                    /* We have to return from here, 
                     * to avoid sending the label mapping message to upstream */
                    if ((LDP_ENTITY_IS_TARGET_TYPE
                         (SSN_GET_ENTITY (pLdpSession)) == LDP_TRUE)
                        && (pLdpSessionEntry == pLdpSession))
                    {
                        LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
                        return;
                    }

                    if (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY
                                                   (pLdpSessionEntry)) ==
                        LDP_TRUE)
                    {
                        /* A.1.6. Recognize New FEC
                         * For Downstream Unsolicited Independent Control
                         * */

                        LDP_IPV4_U4_ADDR (NextHopAddr.Addr) =
                            pInRtInfo->u4NextHop;
                        NextHopAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;

                        LdpDuIndLblReqOrNewFecProcedure (TRUE, pLspUpCtrlBlock,
                                                         pLspCtrlBlock,
                                                         &LdpMsgInfo,
                                                         pLdpSessionEntry,
                                                         &NextHopAddr);

                        /* A.1.1. Receive Label Request
                         * For Downstream Unsolicited ordered Control
                         * */
                        LdpDuOrderLblReqOrNewFecProcedure (TRUE,
                                                           pLspUpCtrlBlock,
                                                           pLspCtrlBlock,
                                                           &LdpMsgInfo,
                                                           pLdpSessionEntry,
                                                           &NextHopAddr);
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "Upstr list-Dn Node %p FEC %x\n",
                                  &UPSTR_DSTR_LIST_NODE (pLspUpCtrlBlock),
                                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspUpCtrlBlock).
                                                    Prefix));
                        return;
                    }

                    MEMSET (&NextHop, LDP_ZERO, sizeof (tGenU4Addr));
                    LDP_IPV4_U4_ADDR (NextHop.Addr) = pInRtInfo->u4NextHop;
                    NextHop.u2AddrType = LDP_ADDR_TYPE_IPV4;

                    /* ECMP Case: FTN is already programmed with LdpIsSsnForFec == False
                     * Create ILM with the Programmed FTN
                     * */
                    if ((LdpIsSsnForFec
                         (&NextHop, pInRtInfo->u4RtIfIndx,
                          pLdpSessionEntry) == LDP_TRUE)
                        ||
                        (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) ==
                         LDP_DU_DN_MLIB_UPD_DONE))
                    {
                        /* A.1.6. Recognize New FEC
                         * For Downstream Unsolicited Independent Control
                         * */

                        LdpDuIndLblReqOrNewFecProcedure (TRUE, pLspUpCtrlBlock,
                                                         pLspCtrlBlock,
                                                         &LdpMsgInfo,
                                                         pLdpSessionEntry,
                                                         &NextHop);

                        /* A.1.1. Recognize New FEC
                         * For Downstream Unsolicited ordered Control
                         * */
                        LdpDuOrderLblReqOrNewFecProcedure (TRUE,
                                                           pLspUpCtrlBlock,
                                                           pLspCtrlBlock,
                                                           &LdpMsgInfo,
                                                           pLdpSessionEntry,
                                                           &NextHop);
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "Upstr list-Dn Node %p FEC %x\n",
                                  &UPSTR_DSTR_LIST_NODE (pLspUpCtrlBlock),
                                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspUpCtrlBlock).
                                                    Prefix));
                        return;
                    }
                }
            }
        }
        /* If the policy is enabled, skip the label mapping message
         * for not connected routes. */
        if ((gLdpInfo.u1Policy == LDP_POLICY_ENABLED) &&
            (pInRtInfo->u4NextHop != LDP_ZERO))
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "LdpEstbLspsForDuSsns: Policy enabled to advertise "
                     "only the connected routes.Not connected routed are not "
                     "advertised to the peer \n");
            LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
            return;
        }

        LDP_IPV4_U4_ADDR (NextHop.Addr) = pInRtInfo->u4NextHop;
        NextHop.u2AddrType = LDP_ADDR_TYPE_IPV4;

        if (pInRtInfo->u4NextHop == LDP_ZERO)
        {
            /* Egress case - Since the label distribution mode is unsolicited,
             * send label mapping message for both ordered and independent mode
             * */
            LDP_DBG2 (LDP_SSN_PRCS, "%s : %d Egress Case\n",
                      __FUNCTION__, __LINE__);
#ifdef LDP_GR_WANTED
            if (pLspUpCtrlBlock->u1StaleStatus == LDP_TRUE)
            {
                LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_REFRESH,
                           &LdpMsgInfo);
            }
            else
            {
                LdpDuUpIdIntDnMap (pLspUpCtrlBlock, &LdpMsgInfo);
            }
#else
            LdpDuUpIdIntDnMap (pLspUpCtrlBlock, &LdpMsgInfo);
#endif
            return;
        }
        else if (LdpGetPeerSession (&NextHop, pInRtInfo->u4RtIfIndx,
                                    &pLdpSessionEntry,
                                    u2IncarnId) == LDP_FAILURE)
        {
            if (gi4MplsSimulateFailure != LDP_SIM_FAILURE_DISABLE_PROXY_EGRESS)
            {
                /* Proxy egress case - Since the label distribution mode is unsolicited,
                 * send label mapping message for both ordered and independent mode
                 * */
                LDP_DBG2 (LDP_SSN_PRCS, "%s : %d Proxy Egress Case\n",
                          __FUNCTION__, __LINE__);
#ifdef LDP_GR_WANTED
                if (pLspUpCtrlBlock->u1StaleStatus == LDP_TRUE)
                {
                    LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_REFRESH,
                               &LdpMsgInfo);
                }
                else
                {
                    LdpDuUpIdIntDnMap (pLspUpCtrlBlock, &LdpMsgInfo);
                }
#else
                LdpDuUpIdIntDnMap (pLspUpCtrlBlock, &LdpMsgInfo);
#endif
            }
            return;
        }

        /* A.1.6. Recognize New FEC
         * For Downstream Unsolicited Independent Control
         * */

        LdpDuIndLblReqOrNewFecProcedure (FALSE, pLspUpCtrlBlock, NULL,
                                         &LdpMsgInfo, pLdpSessionEntry,
                                         &NextHop);

        /* Intermediate case */

        /* A.1.1. Recognize New FEC
         * For Downstream Unsolicited ordered Control
         * */
        LdpDuOrderLblReqOrNewFecProcedure (FALSE, pLspUpCtrlBlock, NULL,
                                           &LdpMsgInfo, pLdpSessionEntry,
                                           &NextHop);
    }
    else
    {
        LDP_DBG2 (LDP_SSN_PRCS, "Route issue for FEC %x Nexthop %x \n",
                  pInRtInfo->u4DestNet, pInRtInfo->u4NextHop);

        MEMSET (&RtGw, LDP_ZERO, sizeof (tGenU4Addr));
        LDP_IPV4_U4_ADDR (RtGw.Addr) = pInRtInfo->u4NextHop;
        RtGw.u2AddrType = LDP_ADDR_TYPE_IPV4;

        TMO_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode, tTMO_SLL_NODE *)
        {
            pLspCtrlBlock = SLL_TO_LCB (pLdpSllNode);
            if ((MEMCMP (&pLspCtrlBlock->Fec, &(Fec), sizeof (tFec))) != 0)
            {
                continue;
            }

            if ((LCB_MLIB_UPD_STATUS (pLspCtrlBlock) ==
                 LDP_DU_DN_MLIB_UPD_NOT_DONE)
                && (LCB_STATE (pLspCtrlBlock) == LDP_DU_DN_LSM_ST_EST))
            {
                u1DnCtrlBlockPresent = TRUE;

                pFtnEntry = MplsSignalGetFtnTableEntry (pInRtInfo->u4DestNet);
                if (pFtnEntry != NULL)
                {
                    LDP_DBG1 (LDP_SSN_PRCS,
                              "LdpEstbLspsForDuSsns FTN FOUND for  Fec %x\n",
                              pInRtInfo->u4DestNet);
                    break;
                }
                if (LdpGetNonTeLspOutSegmentFromPrefix
                    ((uGenU4Addr *) & (LCB_FEC (pLspCtrlBlock).Prefix),
                     &u4OutIndex) == MPLS_SUCCESS)
                {
                    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
                }
                if (pOutSegment != NULL)
                {
                    LDP_DBG1 (LDP_SSN_PRCS,
                              "LdpEstbLspsForDuSsns OutSegment FOUND for  Fec %x\n",
                              pInRtInfo->u4DestNet);
                    break;
                }

                LDP_DBG1 (LDP_SSN_PRCS,
                          "MLIB status not Programmed for DLCB: %x\n",
                          pLspCtrlBlock);

                LDP_IPV4_U4_ADDR (pLspCtrlBlock->NextHopAddr) =
                    pInRtInfo->u4NextHop;
                pLspCtrlBlock->u2AddrType = LDP_ADDR_TYPE_IPV4;

                LDP_DBG2 (LDP_SSN_PRCS,
                          "Rt added DB prg to be done for Prefix %x NH %x\n",
                          LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix),
                          LDP_IPV4_U4_ADDR (pLspCtrlBlock->NextHopAddr));

                /*Create FTN */
                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE, MPLS_OPR_PUSH,
                                       pLspCtrlBlock, NULL);

                LCB_MLIB_UPD_STATUS (pLspCtrlBlock) = LDP_DU_DN_MLIB_UPD_DONE;

                LDP_DBG1 (LDP_SSN_PRCS,
                          "FTN Created, MLIB status for Fec: %x\n",
                          pInRtInfo->u4DestNet);
            }

            break;
        }

        if (u1DnCtrlBlockPresent == TRUE
            &&
            (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) == LDP_DU_DN_MLIB_UPD_DONE)
            && (LCB_STATE (pLspCtrlBlock) == LDP_DU_DN_LSM_ST_EST))
        {
            /** Create swap entry for other ILMs */
            pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
            TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
            {
                TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
                {
                    if ((pLdpPeer->pLdpSession) == NULL)
                    {
                        continue;
                    }
                    /* Target sessions also we need to consider for LDP over RSVP case */
                    pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

                    if ((LDP_ENTITY_IS_TARGET_TYPE
                         (SSN_GET_ENTITY (pLdpSession)) == LDP_TRUE)
                        && (SSN_GET_ENTITY (pLdpSession)->OutStackTnlInfo.
                            u4TnlId == LDP_ZERO))
                    {
                        LDP_DBG (LDP_ADVT_PRCS,
                                 "\n LdpDuDnEstNHChg: Labels can't be negotiated "
                                 " when out-tunnel is not "
                                 "associated with LDP targeted session\n");
                        continue;
                    }

                    SSN_GET_SSN_STATE (pLdpSession, u1SsnState);

                    if ((u1SsnState != LDP_SSM_ST_OPERATIONAL) ||
                        (SSN_ADVTYPE (pLdpSession) != LDP_DSTR_UNSOLICIT))
                    {
                        continue;
                    }

                    if (pLdpSession == LCB_DSSN (pLspCtrlBlock))
                    {
                        continue;
                    }

                    LDP_DBG1 (LDP_SSN_PRCS,
                              "LdpEstbLspsForDuSsns Scanning ULCBs for LCB_FEC = %x\n",
                              LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                Prefix));

                    TMO_SLL_Scan (&SSN_ULCB (pLdpSession),
                                  pTempLspUpCtrlBlock, tUstrLspCtrlBlock *)
                    {
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "LdpEstbLspsForDuSsns Comparing UPSTR_FEC %x & LCB_FEC %x are EQUAL\n",
                                  LDP_IPV4_U4_ADDR (UPSTR_FEC
                                                    (pTempLspUpCtrlBlock).
                                                    Prefix),
                                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                    Prefix));

                        if (LdpCompareFec (&UPSTR_FEC (pTempLspUpCtrlBlock),
                                           &LCB_FEC (pLspCtrlBlock)) ==
                            LDP_EQUAL)
                        {
                            LDP_DBG2 (LDP_SSN_PRCS,
                                      "LdpEstbLspsForDuSsns UPSTR_FEC %x & LCB_FEC %x are EQUAL\n",
                                      LDP_IPV4_U4_ADDR (UPSTR_FEC
                                                        (pTempLspUpCtrlBlock).
                                                        Prefix),
                                      LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                        Prefix));

                            if ((UPSTR_DSTR_PTR (pTempLspUpCtrlBlock) == NULL))
                            {
                                /* Delete the pop entry */
                                LDP_DBG1 (LDP_SSN_PRCS,
                                          "LdpEstbLspsForDuSsns Delete ILM (Pop) for Fec %x\n",
                                          LDP_IPV4_U4_ADDR (LCB_FEC
                                                            (pLspCtrlBlock).
                                                            Prefix));

                                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                                       MPLS_OPR_POP,
                                                       NULL,
                                                       pTempLspUpCtrlBlock);

                                /*Add Downstream CtrlBlk to Upstream CtrlBlk */
                                UPSTR_DSTR_PTR (pTempLspUpCtrlBlock) =
                                    pLspCtrlBlock;

                                /* Create the swap entry */
                                LDP_DBG (LDP_SSN_PRCS,
                                         "LdpEstbLspsForDuSsns Create ILM (SWAP)\n");

                                LdpSendMplsMlibUpdate
                                    (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP_PUSH,
                                     pLspCtrlBlock, pTempLspUpCtrlBlock);

                                LDP_DBG (LDP_SSN_PRCS, "Swap Created\n");

                                /*Add Upstream CtrlBlk to Upstream CtrlBlk List of Downstream Ctrl Blk */
                                TMO_SLL_Add ((tTMO_SLL
                                              *) (&(LCB_UPSTR_LIST
                                                    (pLspCtrlBlock))),
                                             (tTMO_SLL_NODE
                                              *) (&UPSTR_DSTR_LIST_NODE
                                                  (pTempLspUpCtrlBlock)));
                            }
                        }
                    }
                    LDP_DBG1 (LDP_SSN_PRCS,
                              "LdpEstbLspsForDuSsns Exit Loop Scanning ULCBs for LCB_FEC = %x\n",
                              LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                Prefix));
                }
            }
        }

        /* If the LSR is an ingress and it has not received Label mapping message
         * for the FEC already, it will send Label request message
         * */

        if ((u1DnCtrlBlockPresent == FALSE) &&
            ((SSN_GET_ENTITY (pLdpSession)->u2LabelRet) ==
             LDP_CONSERVATIVE_MODE))
        {
            LDP_DBG2 (LDP_SSN_PRCS,
                      "Sending Lbl Req Msg for Ingress FEC %x NextHop %x\n",
                      Fec, NextHop);
            LdpDuSendLblReqForIngress (pLdpSession, Fec, &NextHop);
        }
    }
    return;
}

/****************************************************************************/
/* Function Name : LdpMlibDeleteForEstbLsps                                 */
/* Description   : This routine deletes the LSP information from MPLS-DB    */
/*                 as expected IP route does not exist                      */
/* Inputs (s)    : pLspCtrlBlock - LSP control block                        */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/
VOID
LdpMlibDeleteForEstbLsps (tLspCtrlBlock * pLspCtrlBlock)
{
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tTMO_SLL           *pUpstCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLspInfo            LspInfo;
    UINT2               u2LabelOperation = LDP_ZERO;

    if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) == LDP_DU_DN_MLIB_UPD_NOT_DONE)
    {
        return;
    }

    MEMSET (&LspInfo, LDP_ZERO, sizeof (tLspInfo));
    /* Delete the FTN and ILM from MPLS DB but maintain the LSP info.
     * in the LDP as it is liberal label retention mode */
    LspInfo.bIsLspDestroy = LDP_TRUE;
    LdpMplsMlibUpdate (MPLS_MLIB_FTN_DELETE, MPLS_OPR_PUSH, pLspCtrlBlock,
                       NULL, &LspInfo);
    LCB_MLIB_UPD_STATUS (pLspCtrlBlock) = LDP_DU_DN_MLIB_UPD_NOT_DONE;

#ifdef MPLS_IPV6_WANTED
    if (LCB_FEC (pLspCtrlBlock).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        LDP_DBG1 (LDP_SSN_PRCS,
                  "FTN deleted FEC %s\n",
                  Ip6PrintAddr (&LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr));
    }
    else
#endif
    {
        LDP_DBG1 (LDP_SSN_PRCS,
                  "FTN deleted FEC %x\n",
                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix));
    }

    pUpstCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlock);
    TMO_SLL_Scan (pUpstCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
    {
        pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);
        /* Intermediate Node */
        if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
            ((LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) != NULL))
        {
            u2LabelOperation = MPLS_OPR_POP_PUSH;
        }
        else
        {
            /* Egress Node is not possible here,
             * so continue*/
            continue;
        }

        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                               u2LabelOperation, pLspCtrlBlock,
                               pLspUpCtrlBlock);
    }
    return;
}

/****************************************************************************/
/* Function Name : LdpMlibUpdateForEstbLsps                                 */
/* Description   : This routine updates the MPLS-DB once IP route exist for */
/*                 the LSP.                                                 */
/* Inputs (s)    : pLspCtrlBlock - LSP control block                        */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/
VOID
LdpMlibUpdateForEstbLsps (tLspCtrlBlock * pLspCtrlBlock)
{
    tLspCtrlBlock      *pTmpLspCtrlBlock = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tTMO_SLL           *pEntityList = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLdpSession        *pLdpSessionEntry = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT1               u1SsnState;

    /* create ftn entry */
    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE, MPLS_OPR_PUSH,
                           pLspCtrlBlock, NULL);
    LCB_MLIB_UPD_STATUS (pLspCtrlBlock) = 0;

    /* Remove the MLIB update done on other Downstream session, as the downstream 
     * is changed for the FEC */
    pLdpSession = pLspCtrlBlock->pDStrSession;

    pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
        {
            if ((pLdpPeer->pLdpSession) == NULL)
            {
                continue;
            }
            /* Target sessions also we need to 
             * consider for LDP over RSVP case */
            pLdpSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;

            /* if dn ctl block exists chk if the dn ssn of dn ctl 
             * block same as curr ssn */
            if (pLdpSessionEntry == pLdpSession)
            {
                continue;
            }

            SSN_GET_SSN_STATE (pLdpSessionEntry, u1SsnState);
            if (!((u1SsnState == LDP_SSM_ST_OPERATIONAL) &&
                  (SSN_ADVTYPE (pLdpSessionEntry) == LDP_DSTR_UNSOLICIT)))
            {
                continue;
            }

            /* Scan the Downstream Lsp Control Block List */
            TMO_SLL_Scan (&SSN_DLCB (pLdpSessionEntry), pLdpSllNode,
                          tTMO_SLL_NODE *)
            {
                pTmpLspCtrlBlock = SLL_TO_LCB (pLdpSllNode);
                if ((MEMCMP (&pLspCtrlBlock->Fec, &pTmpLspCtrlBlock->Fec,
                             sizeof (tFec))) != 0)
                {
                    continue;
                }
                if (LCB_MLIB_UPD_STATUS (pTmpLspCtrlBlock) == 0)
                {
                    LCB_MLIB_UPD_STATUS (pTmpLspCtrlBlock) =
                        LDP_DU_DN_MLIB_UPD_NOT_DONE;
                    LDP_DBG (LDP_ADVT_PRCS,
                             "\n LDP_DU_DN_MLIB_UPD_NOT_DONE "
                             "reset is done \n");
                }
            }
        }
    }
}

/****************************************************************************/
/* Function Name : LdpGetLspRouteInfo                                       */
/* Description   : This routine gets the Information needed to set up       */
/*                 the LSP.                                                 */
/* Inputs (s)    : InRtInfo - Pointer to the Route Info                     */
/*               : pLdpSession - Pointer to the Ldp Session                 */
/* Output(s)     : None                                                     */
/* Return(s)     : None                                                     */
/****************************************************************************/
VOID
LdpGetLspRouteInfo (tLdpIpRtInfo * pIpRtInfo, tLdpSession * pLdpSession)
{
    UINT1               u1PrefLen;
    tTMO_SLL           *pPeerAddrList = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tPeerIfAdrNode     *pTmpIfAddrNode = NULL;
    tIntLspSetupInfo   *pIntLspSetup = NULL;

    pIntLspSetup = (tIntLspSetupInfo *)
        MemAllocMemBlk (LDP_INT_LSPSETUP_POOL_ID);

    if (pIntLspSetup == NULL)
    {
        LDP_DBG ((LDP_IF_MEM | LDP_MAIN_MEM),
                 "Memory Allocation failed for LSP Setup Pool\r\n");
        /* No Lsps can be established as stack memory is not left */
        return;
    }

    /* Getting the Peer's Interface Addresses' List */
    pPeerAddrList = SSN_GET_PEER_IFADR_LIST (pLdpSession);

    /* Getting the Prefix length from the obtained Destination Mask */
    LDP_GET_PRFX_LEN (pIpRtInfo->u4DestMask, u1PrefLen);

    /*
     * Scanning through the Peer's addresses and establishing Lsp, if the
     * Next Hop Address through which the Destination is reachable, belongs
     * to the LdpPeer associated with the present LdpSession.
     */

    TMO_SLL_Scan (pPeerAddrList, pTmpIfAddrNode, tPeerIfAdrNode *)
    {
        pIfAddrNode = LDP_OFFSET (pTmpIfAddrNode);

        if (pIfAddrNode->AddrType != MPLS_IPV4_ADDR_TYPE)
        {
            continue;
        }
        if (OSIX_NTOHL (*(UINT4 *) (VOID *) &(pIfAddrNode->IfAddr)) !=
            pIpRtInfo->u4NextHop)
        {
            continue;
        }

        if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
            ((SSN_GET_ENTITY (pLdpSession)->u2LabelRet) ==
             LDP_CONSERVATIVE_MODE))
        {
            pIntLspSetup->u2IncarnId = u2IncarnId;
            LDP_IPV4_U4_ADDR (pIntLspSetup->Dest) = pIpRtInfo->u4DestNet;
            pIntLspSetup->u1PrefixLen = u1PrefLen;
            LDP_IPV4_U4_ADDR (pIntLspSetup->NextHop) = pIpRtInfo->u4NextHop;
            pIntLspSetup->u4IfIndex = pIpRtInfo->u4RtIfIndx;
            pIntLspSetup->pLdpSession = pLdpSession;
            pIntLspSetup->u2AddrType = LDP_ADDR_TYPE_IPV4;
            if (LdpCheckIfDnCtrlBlkExistForFec (pLdpSession,
                                                (&pIntLspSetup->Dest)) !=
                LDP_TRUE)
            {
                LdpIntLspSetReq (pIntLspSetup);
            }
            break;
        }
        else if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
                 ((SSN_GET_ENTITY (pLdpSession)->u2LabelRet) ==
                  LDP_LIBERAL_MODE))
        {

            if (LdpIsLsrPartOfIpv4ErHopOrFec (pIpRtInfo->u4DestNet,
                                              pIpRtInfo->u4DestMask) ==
                LDP_FALSE)
            {
                pIntLspSetup->u2IncarnId = u2IncarnId;
                LDP_IPV4_U4_ADDR (pIntLspSetup->Dest) = pIpRtInfo->u4DestNet;
                pIntLspSetup->u1PrefixLen = u1PrefLen;
                LDP_IPV4_U4_ADDR (pIntLspSetup->NextHop) = pIpRtInfo->u4NextHop;
                pIntLspSetup->u4IfIndex = pIpRtInfo->u4RtIfIndx;
                pIntLspSetup->pLdpSession = pLdpSession;
                pIntLspSetup->u2AddrType = LDP_ADDR_TYPE_IPV4;
                if (LdpCheckIfDnCtrlBlkExistForFec (pLdpSession,
                                                    (&pIntLspSetup->Dest)) !=
                    LDP_TRUE)
                {
                    LdpIntLspSetReq (pIntLspSetup);
                }
                break;
            }
        }
    }
    MemReleaseMemBlock (LDP_INT_LSPSETUP_POOL_ID, (UINT1 *) pIntLspSetup);
    return;
}

/*************************************************************************/
/* Function Name : LdpSetupLspForAllRt                                   */
/* Description   : This routine establishes Lsp for destinations whose   */
/*                 next hop is reachable via this session entry          */
/* Inputs (s)    : pLdpSession - Pointer to the Ldp Session.             */
/* Output(s)     : None                                                  */
/* Return(s)     : LDP_IP_SUCCESS                                        */
/*************************************************************************/

VOID
LdpSetupLspForAllRt (tLdpSession * pLdpSession)
{
    tIntLspSetupInfo   *pIntLspSetupEntry = NULL;

    pIntLspSetupEntry = (tIntLspSetupInfo *) TMO_SLL_First
        (&(pLdpSession->IntLspSetupList));
    while (pIntLspSetupEntry != NULL)
    {
        LdpIntLspSetReq (pIntLspSetupEntry);
        TMO_SLL_Delete (&(pLdpSession->IntLspSetupList),
                        &(pIntLspSetupEntry->NextSetupNode));
        MemReleaseMemBlock (LDP_INT_LSPSETUP_POOL_ID,
                            (UINT1 *) pIntLspSetupEntry);
        pIntLspSetupEntry = (tIntLspSetupInfo *) TMO_SLL_First
            (&(pLdpSession->IntLspSetupList));
    }
    TMO_SLL_Init (&(pLdpSession->IntLspSetupList));
}

/*************************************************************************/
/* Function Name   : LdpIpIsAddrLocal                                    */
/* Description     : This function verifies whether the address          */
/*                   matches any of the local interface adresses         */
/* Input (s)       : u4DestAddr - The Address to verify                  */
/* Output (s)      : None                                                */
/* Returns         : LDP_TRUE or LDP_FALSE                               */
/*************************************************************************/

UINT1
LdpIpIsAddrLocal (UINT4 u4DestAddr)
{
    if (NetIpv4IfIsOurAddress (u4DestAddr) == NETIPV4_SUCCESS)
    {

        LDP_DBG (LDP_IF_PRCS, "IF: Success in finding IP is Local or Not\n");
        return LDP_TRUE;
    }

    LDP_DBG (LDP_IF_PRCS, "IF: Failed to find whether IP is Local or Not\n");
    return LDP_FALSE;
}

/******************************************************************************/
/* Function Name : ldpUtlUcastRouteQuery                                      */
/* Description   : This routine is used to do a route query for a particular  */
/*                 destination                                                */
/* Input(s)      : u4Addr      - The destination address                      */
/*                 pu2RtPort   - pointer to the outgoing interface            */
/*                 pu4RtGw     - pointer to the gateway                       */
/* Output(s)     : pu2RtPort - This is updated with the outgoing interface    */
/*                             index                                          */
/*                 pu4RtGw   - This is updated with the corresponding gateway */
/*                             for the destination                            */
/* Return(s)     : LDP_IP_SUCCESS or LDP_IP_FAILURE                           */
/******************************************************************************/
INT1
LdpIpUcastRouteQuery (UINT4 u4Addr, UINT2 *pu2RtPort, UINT4 *pu4RtGw)
{
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4Mask = 0xffffffff;
    UINT4               u4IfIndex = 0;
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    RtQuery.u4DestinationIpAddress = u4Addr;
    RtQuery.u4DestinationSubnetMask = u4Mask;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {

        if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                          &u4IfIndex) == NETIPV4_SUCCESS)
        {
            *pu2RtPort = (UINT2) u4IfIndex;
            *pu4RtGw = (NetIpRtInfo.u4NextHop == 0) ? u4Addr :
                NetIpRtInfo.u4NextHop;
            LDP_DBG (LDP_IF_PRCS, "IF: Success in geting route\n");
            return LDP_IP_SUCCESS;
        }
    }

    LDP_DBG (LDP_IF_PRCS, "IF: Failed in geting route\n");
    return LDP_IP_FAILURE;
}

/******************************************************************************/
/* Function Name : LdpIpGetExactRoute                                         */
/* Description   : This routine is used to get the exact route for a          */
/*                 particular destination                                     */
/* Input(s)      : u4Addr      - The destination address                      */
/*                 pu4RtPort   - pointer to the outgoing interface            */
/*                 pu4RtGw     - pointer to the gateway                       */
/* Output(s)     : pu2RtPort - This is updated with the outgoing interface    */
/*                             index                                          */
/*                 pu4RtGw   - This is updated with the corresponding gateway */
/*                             for the destination                            */
/* Return(s)     : LDP_IP_SUCCESS or LDP_IP_FAILURE                           */
/******************************************************************************/
INT1
LdpIpGetExactRoute (UINT4 u4Addr, UINT4 u4DestMask,
                    UINT4 *pu4RtPort, UINT4 *pu4RtGw)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4IfIndex = 0;
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (RtQuery));

    RtQuery.u4DestinationIpAddress = u4Addr;
    RtQuery.u4DestinationSubnetMask = u4DestMask;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                          &u4IfIndex) == NETIPV4_SUCCESS)
        {
            *pu4RtPort = u4IfIndex;
            *pu4RtGw = (NetIpRtInfo.u4NextHop == 0) ? u4Addr :
                NetIpRtInfo.u4NextHop;
            LDP_DBG (LDP_IF_PRCS, "IF: Success in geting exact route\n");
            return LDP_IP_SUCCESS;
        }
    }
    LDP_DBG (LDP_IF_PRCS, "IF: Failed in geting exact route\n");
    return LDP_IP_FAILURE;
}

/****************************************************************************/
/* Function Name : LdpIsLsrPartOfIpv4ErHopOrFec                             */
/* Description   : This routine is called to check whether an ErHopAddr or  */
/*                 FEC is part of a particular LSR                          */
/* Input(s)      : UINT4 u4ErHopAddr                                        */
/*                 UINT4 u4ErHopMask                                        */
/*                                                                          */
/* Output(s)     : None                                                     */
/* Return(s)     : LDP_TRUE/LDP_FALSE.                                      */
/****************************************************************************/
UINT1
LdpIsLsrPartOfIpv4ErHopOrFec (UINT4 u4ErHopAddr, UINT4 u4ErHopMask)
{
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    if (LDP_HOST_NETMASK == u4ErHopMask)
    {
        if (NetIpv4IfIsOurAddress (u4ErHopAddr) == NETIPV4_SUCCESS)
        {
            LDP_DBG (LDP_IF_PRCS, "IF:Success in NetIpv4IfIsOurAddress\n");
            return LDP_TRUE;
        }
    }
    else
    {
        MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
        MEMSET (&RtQuery, 0, sizeof (RtQuery));
        RtQuery.u4DestinationIpAddress = u4ErHopAddr;
        RtQuery.u4DestinationSubnetMask = u4ErHopMask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
        {
            if (NetIpRtInfo.u2RtProto == CIDR_LOCAL_ID)
            {
                LDP_DBG (LDP_IF_PRCS, "IF:Success in NetIpv4GetRoute\n");
                return LDP_TRUE;
            }
        }
    }
    LDP_DBG (LDP_IF_PRCS, "IF: Failed in LdpIsLsrPartOfIpv4ErHopOrFec\n");
    return LDP_FALSE;
    /*
     * Scanning through the Ifaces configured in Ip.
     */
}

/*****************************************************************************/
/* Function Name : LdpIpGetIfAddr                                               */
/* Description   : This function is used to find out the address of the       */
/*                 interface from the ifIndex                                 */
/* Input(s)      : i4IfIndex - The interface index                            */
/*                 *pu4Addr  - pointer to interface address                   */
/* Output(s)     : pu4Addr -The pu4Addr is udpated with the interface address */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                 */
/******************************************************************************/
UINT1
LdpIpGetIfAddr (INT4 i4IfIndex, UINT4 *pu4Addr)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4Port = 0;
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    if (NetIpv4GetPortFromIfIndex ((UINT4) i4IfIndex, &u4Port) ==
        NETIPV4_SUCCESS)
    {
        if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_SUCCESS)
        {
            *pu4Addr = NetIpIfInfo.u4Addr;
            return LDP_SUCCESS;
        }
    }
    LDP_DBG (LDP_IF_PRCS, "IF: Failed in Getting Interface Address\n");
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpReRouteTime                                             */
/* Description   : This function is used to calculate the                     */
/*                 reroute time during the reroute                            */
/* Input(s)      : UINT1 u1StartOrEnd                                         */
/* Output(s)     : Displays the difference in start and End time              */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                 */
/******************************************************************************/
UINT1
LdpReRouteTime (UINT1 u1StartOrEnd)
{

    if (u1StartOrEnd == LDP_START_REROUTE)
    {
        OsixGetSysTime ((tOsixSysTime *) & (gLdpTimeStart));
    }
    else if (u1StartOrEnd == LDP_END_REROUTE)
    {
        OsixGetSysTime ((tOsixSysTime *) & (gLdpTimeEnd));

        LDP_DBG1 (LDP_RERTIM_DUMP,
                  "LDP-IF: ReRoute : Time in Sec : %d \n",
                  gLdpTimeEnd - gLdpTimeStart);

    }
    return LDP_SUCCESS;
}

#ifdef LDP_GR_WANTED
UINT1
LdpIsLblRcvdFromDownstreamRtr (tLdpSession * pUpstrLdpSession,
                               tFec Fec,
                               tGenU4Addr * pNextHop, UINT4 u4RtIfIndx)
{
    tLdpSession        *pLdpSessionEntry = NULL;
    tTMO_SLL           *pEntityList = NULL;
    UINT1               u1SsnState = 0;
    tLdpEntity         *pLdpEntity = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;

    pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
        {
            pLdpSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;
            if (pLdpSessionEntry == NULL)
            {
                continue;
            }
            if (pLdpSessionEntry == pUpstrLdpSession)
            {
                continue;
            }
            SSN_GET_SSN_STATE (pLdpSessionEntry, u1SsnState);

            if (!((u1SsnState == LDP_SSM_ST_OPERATIONAL) &&
                  (SSN_ADVTYPE (pLdpSessionEntry) == LDP_DSTR_UNSOLICIT)))
            {
                continue;
            }

            TMO_SLL_Scan (&SSN_DLCB (pLdpSessionEntry), pLdpSllNode,
                          tTMO_SLL_NODE *)
            {
                pLspCtrlBlock = SLL_TO_LCB (pLdpSllNode);
                if ((MEMCMP (&pLspCtrlBlock->Fec, &(Fec), sizeof (tFec))) != 0)
                {
                    continue;
                }
                if (LdpIsSsnForFec (pNextHop,
                                    u4RtIfIndx, pLdpSessionEntry) == LDP_TRUE)
                {
                    LDP_DBG1 (GRACEFUL_DEBUG,
                              "LdpIsLblRcvdFromDownstreamRtr: Label is Already Rcvd for the FEC: %x "
                              "from the Downstream Router\n",
                              LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                Prefix));
                    return LDP_TRUE;
                }
            }
        }
    }
    return LDP_FALSE;
}
#endif
/*---------------------------------------------------------------------------*/
/*                       End of file ldpfsip.c                               */
/*---------------------------------------------------------------------------*/
