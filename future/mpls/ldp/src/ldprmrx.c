
/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: ldprmrx.c,v 1.1 2014/08/25 12:23:31 siva Exp $
 ***
 *** Description: Files contails HA implementation of ldp 
 *** NOTE: This file should included in release packaging.
 *** ***********************************************************************/


#include "ldpincs.h"
#include "mplshwlist.h"


/*************************************************************
 *  Function Name   :LdpRmProcessILMHwListSync                             
 *  Description     :This function is invoked at standby node    
 *                   To Process Dynamic sync 
 *                                                                
 *  Input(s)        :pRmMesg - The message received from RM       
 *  Output(s)       :None                                      

 *  Returns         :None                                      
 ************************************************************/
INT4 LdpRmProcessILMHwListSync (tRmMsg * pRmMesg, UINT4 *pu4Offset)
{
    UINT4              u4Offset = *pu4Offset ;
    UINT1              u1OpType = LDP_ZERO ;
    tILMHwListEntry    ILMHwListEntry;
    tILMHwListEntry    *pILMHwListEntry = NULL;
    tMplsIpAddress     IlmFec;
    UINT1              u1PrefixLen = 0;
    uLabel             Label;
    UINT4              u4L3Intf = 0;


    MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
    if (LDP_IS_NODE_ACTIVE () == LDP_RM_TRUE)
    {
        LDP_DBG (HA_DEBUG,"Received  syn At active \n");  
        return LDP_FAILURE;
    }

    
    LDP_RM_GET_1_BYTE (pRmMesg, u4Offset,u1OpType);
  
    LDP_RM_GET_N_BYTE (pRmMesg, &ILMHwListEntry, u4Offset, sizeof(tILMHwListEntry));
      
    LDP_DBG(HA_DEBUG,"Add to RBTree\n");


    switch (u1OpType)
    {
        case ADD_ILM:
            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) == MPLS_FAILURE)
            {
                LDP_DBG (HA_DEBUG, "Failed to Add ILM Hw List Entry\n");
                return LDP_FAILURE;
            }
            break;

        case UPDATE_ILM:
            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 
                              ILMHwListEntry.u4ILMEntryStatus) == MPLS_FAILURE)
            {
                LDP_DBG (HA_DEBUG, "Failed to update ILM hw List Entry\n");
                return LDP_FAILURE;
            }
            break;

        case DEL_ILM:
            
            MEMCPY (&IlmFec, &ILMHwListEntry.Fec, sizeof (tMplsIpAddress));
            MEMCPY (&Label, &ILMHwListEntry.InLabel, sizeof (uLabel));
            u4L3Intf = MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry));
            u1PrefixLen = MPLS_ILM_HW_LIST_FEC_PREFIX_LEN ((&ILMHwListEntry)); 

            pILMHwListEntry = MplsHwListGetILMHwListEntry (IlmFec, u1PrefixLen, u4L3Intf, Label);
            if (pILMHwListEntry != NULL)
            {
                if (MplsHwListDelILMHwListEntry (pILMHwListEntry) == MPLS_FAILURE)
                {
                    LDP_DBG (HA_DEBUG, "Failed to delete ILM hw List Entry\n");
                    return LDP_FAILURE;
                }
            }
            else
            {
                LDP_DBG (HA_DEBUG, "ILM Hw List Entry NOT FOUND in Hw List\n");
            }
            break;

        default:
           LDP_DBG (HA_DEBUG, "Inavlid Operation Type\n");
    }

    return LDP_SUCCESS;
}      


/****************************************************************
 *  *  Function Name   :LdpRmProcessFTNHwListSync
 *     Description     :This function is invoked at standby node
 *                      To Process Dynamic sync
 *     Input(s)        :pRmMsg - The message received from RM
 *     Output(s)       :None
 *     Returns         :None
 * **************************************************************/
INT4 LdpRmProcessFTNHwListSync (tRmMsg * pRmMsg, UINT4 *pOffset)
{
    UINT4                   u4Offset = *pOffset;
    UINT1                   u1OpType = LDP_ZERO ;
    tFTNHwListEntry         FtnHwListEntry;
    tMplsIpAddress          FTNFec;
    UINT1                   u1PrefixLen = 0;
    tFTNHwListEntry         *pFTNHwListEntry = NULL;

    MEMSET (&FTNFec, 0, sizeof (tMplsIpAddress));
    MEMSET (&FtnHwListEntry, 0, sizeof (tFTNHwListEntry));
    if (LDP_IS_NODE_ACTIVE () == LDP_RM_TRUE)
    {
        LDP_DBG (HA_DEBUG,"Received Bulk syn At active \n");
        return LDP_FAILURE;
    }

    LDP_RM_GET_1_BYTE (pRmMsg, u4Offset, u1OpType);
    LDP_RM_GET_N_BYTE (pRmMsg, &FtnHwListEntry, u4Offset, sizeof(tFTNHwListEntry));

    switch (u1OpType)
    {
        case ADD_FTN:
            if (MplsHwListAddOrUpdateFTNHwListEntry (&FtnHwListEntry, 0) ==
                MPLS_FAILURE)
            {
                LDP_DBG (HA_DEBUG, "Error in Adding the FTN HW List Entry\n");
            }
            break;

        case UPDATE_FTN:
            if (MplsHwListAddOrUpdateFTNHwListEntry (&FtnHwListEntry, 
                                FtnHwListEntry.u4FtnEntryStatus) == MPLS_FAILURE)
            {
                LDP_DBG (HA_DEBUG, "Error in Updating the FTN HW List Entry\n");
            }
            break;

        case DEL_FTN:
            MEMCPY (&FTNFec, &FtnHwListEntry.Fec, sizeof (tMplsIpAddress));
            u1PrefixLen = MPLS_FTN_HW_LIST_FEC_PREFIX_LEN ((&FtnHwListEntry));
            pFTNHwListEntry  = MplsHwListGetFTNHwListEntry (FTNFec, u1PrefixLen);

            if (pFTNHwListEntry != NULL)
            {
                if (MplsHwListDelFTNHwListEntry (pFTNHwListEntry) == MPLS_FAILURE)
                {
                    LDP_DBG (HA_DEBUG, "Error in Deleting the FTN HW List Entry\n");
                }
            }
            else
            {
                LDP_DBG (HA_DEBUG, "FTN Hw List Entry NOT FOUND in FTN hw list\n");
            }
            break;

        default:
             LDP_DBG (HA_DEBUG, "Invalid operation type\n");
    }
    return LDP_SUCCESS;
}



/*************************************************************
 *  Function Name   :LdpRmProcessBulkILMGblInfo
 *  Description     :This function is invoked at standby node
 *                   To Process bulk update 
 *  Input(s)        :pMsg- The message received from RM
 *  Output(s)       :None
 *  Returns         :None
 * ************************************************************/

VOID LdpRmProcessBulkILMGblInfo(tRmMsg * pMsg, UINT4 *pu4Offset)
{

    UINT2                    u2Count = LDP_ZERO;
    UINT4                    u4Offset = *pu4Offset;
    tILMHwListEntry          ILMHwListEntry;
    UINT1                    u1Range = LDP_ZERO;

    if (LDP_IS_NODE_ACTIVE () == LDP_RM_TRUE)
    {
        LDP_DBG(HA_DEBUG,"Received Bulk syn At active \n");
        return;
    }

    LDP_RM_GET_2_BYTE (pMsg, u4Offset, u2Count);
    
    LDP_DBG1 (HA_DEBUG, "u2Count= %d\n", u2Count);
    LDP_DBG (HA_DEBUG,"Add to RBTree\n");

    for(u1Range = LDP_ZERO; u1Range < u2Count; u1Range++)
    {
        MEMSET (&ILMHwListEntry , LDP_ZERO, sizeof(tILMHwListEntry));
        LDP_RM_GET_N_BYTE (pMsg, &ILMHwListEntry, u4Offset, sizeof(tILMHwListEntry));
        MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, MPLS_ILM_HW_LIST_NP_STATUS((&ILMHwListEntry)));
    }

}

/*************************************************************
 *     Function Name   :LdpRmProcessBulkFTNGblInfo
 *     Description     :This function is invoked at standby node
 *                      To Process bulk update
 *     
 *     Input(s)        :pMsg- The message received from RM
 *     Output(s)       :None
 *       
 *     
 *     Returns         :None
 * ************************************************************/

VOID LdpRmProcessBulkFTNGblInfo(tRmMsg * pMsg, UINT4 *pu4Offset)
{

    UINT2                    u2Count = LDP_ZERO;
    UINT4                    u4Offset = *pu4Offset;
    UINT1                    u1Range = LDP_ZERO;
    tFTNHwListEntry          FTNHwListEntry;

    if (LDP_IS_NODE_ACTIVE () == LDP_RM_TRUE)
    { 
        LDP_DBG (HA_DEBUG,"Received Bulk syn At active \n");
        return;
    }

    LDP_RM_GET_2_BYTE (pMsg, u4Offset, u2Count);
    LDP_DBG1 (HA_DEBUG, "u2Count= %d\n", u2Count);
    LDP_DBG (HA_DEBUG,"Add to RBTree\n");
    for (u1Range = LDP_ZERO ; u1Range < u2Count; u1Range++)
    {
        MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
        LDP_RM_GET_N_BYTE (pMsg, &FTNHwListEntry, u4Offset, sizeof(tFTNHwListEntry)); 
        MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, MPLS_FTN_HW_LIST_NP_STATUS((&FTNHwListEntry)));
    }
}   

