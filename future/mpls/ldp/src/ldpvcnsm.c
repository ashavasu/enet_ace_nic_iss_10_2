/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpvcnsm.c,v 1.11 2014/11/08 11:40:57 siva Exp $
 *
 * Description: This file contains VC Merge Next hop state machine routines.
 ********************************************************************/

#include "ldpincs.h"
#include "ldpgblex.h"
#include "ldpvcnsm.h"
/*****************************************************************************/
/* Function Name : ldpVcMrgNhopSm                                            */
/* Description   : This F'n is called defines a Merge Next Hop State machine */
/* Input(s)      : pNHCtrlBlock, u1Event                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpVcMergeNhopSm (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 u1Event, UINT1 *pMsg)
{
    LDP_DBG2 (LDP_ADVT_SEM,
              "Next Hop Control Block Current State: %s Evt %s \n",
              au1LdpMrgNhStates[pNHCtrlBlock->u1NhLspState],
              au1LdpMrgNhEvts[u1Event]);

    gLdpMrgNhopChngFsm[NH_TRGCB_ST (pNHCtrlBlock)][u1Event]
        (pNHCtrlBlock, pMsg);

    LDP_DBG1 (LDP_ADVT_SEM,
              "Next Hop Control Block Next  State: %s \n",
              au1LdpMrgNhStates[pNHCtrlBlock->u1NhLspState]);
}

/*****************************************************************************/
/* Function Name : LdpMrgNhopChngIdlNewNh                                    */
/* Description   : This F'n is called when the state machine is in Idle      */
/*                 state and New_Next_hop event is received.                 */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgNhopChngIdlNewNh (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
    tLdpRouteEntryInfo *pRtInfo = NULL;

    pRtInfo = (tLdpRouteEntryInfo *) (VOID *) (pMsg);
    LDP_IPV4_U4_ADDR(NH_ADDR (pNHCtrlBlock)) = LDP_IPV4_U4_ADDR(pRtInfo->NextHop);
    NH_OUT_IFINDEX (pNHCtrlBlock) = pRtInfo->u4RtIfIndx;
    /* Start the retry Timer */
    pNHCtrlBlock->retryTimer.u4Event = NHOP_MRG_RETRY_TMR_EXPIRED_EVENT;
    pNHCtrlBlock->retryTimer.pu1EventInfo = (UINT1 *) pNHCtrlBlock;
    NH_TRGCB_ST (pNHCtrlBlock) = LDP_MRG_NH_LSM_ST_NEWNH_RETRY;
    if (TmrStartTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *)
                       & (pNHCtrlBlock->retryTimer.AppTimer),
                       (NHOP_MRG_RETRY_TMR_TIMEOUT *
                        SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
    {
        LDP_DBG1 (LDP_ADVT_TMR,
                  "ADVT: Failed to start timer inside the function %s \n",
                  __func__);
    }
}

/************************************************************************/
/* Function Name :  LdpMrgNhopChngInvStateEvt                           */
/* Description   : This F'n is called, on the occurrence of an invalid  */
/*                 event, in a state.                                   */
/* Input(s)      : pNHCtrlBlock, pMsg                                   */
/* Output(s)     : None                                                 */
/* Return(s)     : None                                                 */
/************************************************************************/

VOID
LdpMrgNhopChngInvStateEvt (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
    LDP_SUPPRESS_WARNING (pMsg);
    pNHCtrlBlock = (tLspTrigCtrlBlock *) pNHCtrlBlock;
    LDP_DBG (LDP_ADVT_MISC, "ADVT_MRG_NH: Mrg NH st m/c Invalid State \n");
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgNhopChngRetryNewNh                                  */
/* Description   : This F'n is called when the state machine is in           */
/*                  New_NH_retry state and New_Next_hop event is received.   */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgNhopChngRetryNewNh (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
    tLdpRouteEntryInfo *pRtInfo = NULL;

    /* Get the new Next hop address */
    pRtInfo = (tLdpRouteEntryInfo *) (VOID *) (pMsg);
    LDP_IPV4_U4_ADDR(NH_ADDR (pNHCtrlBlock)) = LDP_IPV4_U4_ADDR(pRtInfo->NextHop);
    NH_OUT_IFINDEX (pNHCtrlBlock) = pRtInfo->u4RtIfIndx;
    /*  Restart the retry Timer */
    if (TmrStopTimer (LDP_TIMER_LIST_ID,
                      (tTmrAppTimer *) & (pNHCtrlBlock->retryTimer.AppTimer)) ==
        TMR_FAILURE)
    {
        LDP_DBG1 (LDP_ADVT_TMR,
                  "ADVT: Failed to stop timer inside the function %s \n",
                  __func__);
    }
    if (TmrStartTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *)
                       & (pNHCtrlBlock->retryTimer.AppTimer),
                       (NHOP_MRG_RETRY_TMR_TIMEOUT
                        * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
    {
        LDP_DBG1 (LDP_ADVT_TMR,
                  "ADVT: Failed to start timer inside the function %s \n",
                  __func__);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgNhopChngRetryRetryTmout                             */
/* Description   : This F'n is called when the state machine is in           */
/*                 New_NH_retry state and Internal_Retry_timeout event is    */
/*                 received.                                                 */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgNhopChngRetryRetryTmout (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
    tTMO_SLL           *pLspCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLspCtrlBlock      *pDnstrCtrlBlk = NULL;
    tUstrLspCtrlBlock  *pUpstrCtrlBlk = NULL;
    UINT1               u1Flag = LDP_FALSE;
    tLdpSession        *pDssn = NULL;
    tLdpMsgInfo         LdpMsg;
    tGenU4Addr          NextHopAddr;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
#endif
    LDP_SUPPRESS_WARNING (pMsg);

    MEMSET (&LdpMsg, LDP_ZERO, sizeof (tLdpMsgInfo));
    MEMSET (&NextHopAddr,LDP_ZERO,sizeof(NextHopAddr));

    if ((LCB_COMP_NHOPS (LDP_IPV4_U4_ADDR((NH_TRGCB_OLSP (pNHCtrlBlock))->NextHopAddr),
                         LDP_IPV4_U4_ADDR(NH_ADDR (pNHCtrlBlock)), IPV4_ADDR_LENGTH)) == LDP_ZERO)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT_MRG_NH:New NH same as old.Deleteing Trgg Blk\n");
        LdpDeleteTrigCtrlBlk (pNHCtrlBlock);
        return;
    }
    /* Search for the Dstream control block for the given FEC 
     * and if not found create a new one and associate it with
     * the new Next Hop control block. */

    LDP_IPV4_U4_ADDR(NextHopAddr.Addr)=LDP_IPV4_U4_ADDR(NH_ADDR (pNHCtrlBlock));
    NextHopAddr.u2AddrType=LDP_ADDR_TYPE_IPV4;
    if (LdpGetPeerSession (&NextHopAddr,
                           NH_OUT_IFINDEX (pNHCtrlBlock),
                           &pDssn,
                           SSN_GET_INCRN_ID (LCB_DSSN
                                             (NH_TRGCB_OLSP (pNHCtrlBlock)))) ==
        LDP_FAILURE)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT_MRG_NH:No Session returned. Deleteing Trgg Blk\n");
        LdpDeleteTrigCtrlBlk (pNHCtrlBlock);
        return;
    }

    pLspCtrlBlkList = &SSN_DLCB (pDssn);

    TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
    {
        pDnstrCtrlBlk = SLL_TO_LCB (pSllNode);

        if (LdpCompareFec(&LCB_FEC (pDnstrCtrlBlk),
                           &UPSTR_FEC ((tUstrLspCtrlBlock *)
                                      NH_NEW_UPSTR_PTR (pNHCtrlBlock))) ==
            LDP_EQUAL)
        {
            u1Flag = LDP_TRUE;
            NH_TRGCB_NLSP (pNHCtrlBlock) = pDnstrCtrlBlk;
            break;
        }
    }

    if (u1Flag == LDP_FALSE)
    {
        if (LdpCreateDnstrCtrlBlock (&pDnstrCtrlBlk) == LDP_FAILURE)
        {
            LdpDeleteTrigCtrlBlk (pNHCtrlBlock);
            return;
        }
#ifdef CFA_WANTED
        /* Create a MPLS Tunnel interface at CFA  and Stack over the MPLS
         * l3 interface (which is stacked over an L3IPVLAN interface,
         * and use the interface index  returned as OutIfIndex
         */
	if (CfaIfmCreateStackMplsTunnelInterface
			(LdpSessionGetIfIndex(pDssn,UPSTR_FEC ((tUstrLspCtrlBlock *)NH_NEW_UPSTR_PTR (pNHCtrlBlock)).u2AddrFmly),
			 &u4MplsTnlIfIndex) == CFA_FAILURE)
	{

            LDP_DBG (LDP_ADVT_MEM,
                     " LdpMrgNhopChngRetryRetryTmout - ADVT: Mplstunnel "
                     " IF creation failed for DownStream Cntl Block\n");
            MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pDnstrCtrlBlk);
            return;
        }
        pDnstrCtrlBlk->u4OutIfIndex = u4MplsTnlIfIndex;
#else
        pDnstrCtrlBlk->u4OutIfIndex = LdpSessionGetIfIndex(pDssn,UPSTR_FEC ((tUstrLspCtrlBlock *)NH_NEW_UPSTR_PTR (pNHCtrlBlock)).u2AddrFmly);
#endif
        /* Add the FEC, session, Next Hop Addr and IfIndex to the
         * down stream ctrl block
         */

        MEMCPY (&(LCB_FEC (pDnstrCtrlBlk)),
                &(UPSTR_FEC ((tUstrLspCtrlBlock *)
                             NH_NEW_UPSTR_PTR (pNHCtrlBlock))), sizeof (tFec));
        MEMCPY ((UINT1 *) &LDP_IPV4_U4_ADDR(pDnstrCtrlBlk->NextHopAddr),
                (UINT1 *) &LDP_IPV4_U4_ADDR(NH_ADDR (pNHCtrlBlock)), IPV4_ADDR_LENGTH);
        LCB_OUT_IFINDEX (pDnstrCtrlBlk) = NH_OUT_IFINDEX (pNHCtrlBlock);
        LCB_DSSN (pDnstrCtrlBlk) = pDssn;
        NH_TRGCB_NLSP (pNHCtrlBlock) = pDnstrCtrlBlk;
    }

    LDP_DBG (LDP_ADVT_SEM,
             "ADVT_MRG_NH: Send Int-add upstream event to DStr Mrg St m/c\n ");
    /* Change made for the new upstream control block field added to the
     * upstream control block structure.
     */

    pUpstrCtrlBlk = (tUstrLspCtrlBlock *) NH_NEW_UPSTR_PTR (pNHCtrlBlock);

    UPSTR_DSTR_PTR (pUpstrCtrlBlk) = pDnstrCtrlBlk;
    LDP_MSG_CTRL_BLK_PTR (&LdpMsg) = NH_NEW_UPSTR_PTR (pNHCtrlBlock);

    LdpMergeDnSm (pDnstrCtrlBlk, LDP_DSM_EVT_INT_ADD_UPSTR, &LdpMsg);

    NH_TRGCB_ST (pNHCtrlBlock) = LDP_MRG_NH_LSM_ST_RESP_AWAIT;
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgNhopChngRetryDestroy                                */
/* Description   : This F'n is called when the state machine is in           */
/*                 New_NH_retry state and Internal_Destroy event is          */
/*                 received.                                                 */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgNhopChngRetryDestroy (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
    LDP_SUPPRESS_WARNING (pMsg);
    TmrStopTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *)
                  & (pNHCtrlBlock->retryTimer.AppTimer));
    LDP_DBG (LDP_ADVT_TMR, "ADVT_MRG_NH: Nhop Retry Tmr stopped \n");
    LdpDeleteTrigCtrlBlk (pNHCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgNhopChngRspAwaitNewNh                               */
/* Description   : This routine is called when the state machine is in       */
/*                 New_NH_Resp_Await state and New_NH event is  received.    */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgNhopChngRspAwaitNewNh (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
    tLdpRouteEntryInfo *pRtInfo = NULL;

    /* Get the new Next hop address */
    pRtInfo = (tLdpRouteEntryInfo *) (VOID *) (pMsg);
    LDP_IPV4_U4_ADDR(NH_ADDR (pNHCtrlBlock)) = LDP_IPV4_U4_ADDR(pRtInfo->NextHop);
    NH_OUT_IFINDEX (pNHCtrlBlock) = pRtInfo->u4RtIfIndx;
    /* Restart the retry Timer */
    if (TmrStopTimer (LDP_TIMER_LIST_ID,
                      (tTmrAppTimer *) & (pNHCtrlBlock->retryTimer.AppTimer)) ==
        TMR_FAILURE)
    {
        LDP_DBG1 (LDP_ADVT_TMR,
                  "ADVT: Failed to stop timer inside the function %s \n",
                  __func__);
    }
    if (TmrStartTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *)
                       & (pNHCtrlBlock->retryTimer.AppTimer),
                       (NHOP_RETRY_TMR_TIMEOUT *
                        SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
    {
        LDP_DBG1 (LDP_ADVT_TMR,
                  "ADVT: Failed to start timer inside the function %s \n",
                  __func__);
    }
    LDP_DBG (LDP_ADVT_TMR, "ADVT_MRG_NH: Nhop retry tmr restarted \n");
    /* Send Internal_Destroy to the newly estabilshing LSP in the Next
     * Hop VC merge state machine.
     */
    LdpVcMergeNhopSm (pNHCtrlBlock, LDP_MRG_NH_LSM_EVT_INT_DESTROY, pMsg);
    NH_TRGCB_ST (pNHCtrlBlock) = LDP_MRG_NH_LSM_ST_NEWNH_RETRY;
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgNhopChngRspAwaitDStrMap                             */
/* Description   : This F'n is called when the state machine is in           */
/*                 New_NH_Resp_Await state and LSP-DESTROY event is          */
/*                 received.                                                 */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgNhopChngRspAwaitDStrMap (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pLdpSession = NULL;

    /* Pass the Internal Re-Cross-Connect Event to the the Upstream
     * Ctrl Blk State machine.
     */

    pLdpSession = UPSTR_USSN (NH_USTR_CTRL_BLK (pNHCtrlBlock));
    /* If it is an intermediate node */
    if (pLdpSession != NULL)
    {
        LdpMergeUpSm (NH_NEW_UPSTR_PTR (pNHCtrlBlock), LDP_USM_EVT_INT_RE_X,
                      (tLdpMsgInfo *) (VOID *) pMsg);
        TMO_SLL_Add (&SSN_ULCB (pLdpSession),
                     &UPSTR_DSSN_LIST_NODE (NH_NEW_UPSTR_PTR (pNHCtrlBlock)));
    }

    UPSTR_NHOP (NH_NEW_UPSTR_PTR (pNHCtrlBlock)) = NULL;

    NH_NEW_UPSTR_PTR (pNHCtrlBlock) = NULL;

    LdpDeleteUpstrCtrlBlock (NH_USTR_CTRL_BLK (pNHCtrlBlock));
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgNhopChngRspAwaitNak                                 */
/* Description   : This routine is called when the state machine is in       */
/*                 New_NH_Resp_Await state and LSP_NAK event is  received.   */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgNhopChngRspAwaitNak (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
    tUstrLspCtrlBlock  *pNewUsCtrlBlk = NULL;

    LDP_SUPPRESS_WARNING (pMsg);
    pNewUsCtrlBlk = NH_NEW_UPSTR_PTR (pNHCtrlBlock);
    LdpMergeUpSm (NH_USTR_CTRL_BLK (pNHCtrlBlock), LDP_USM_EVT_DSTR_NAK,
                  (tLdpMsgInfo *) (VOID *) pMsg);
    UPSTR_NHOP (pNewUsCtrlBlk) = NULL;
    LdpDeleteUpstrCtrlBlock (NH_NEW_UPSTR_PTR (pNHCtrlBlock));
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgNhopChngRspAwaitDestroy                             */
/* Description   : This routine is called when the state machine is in       */
/*                 New_NH_Resp_Await state and Internal_Destroy event is     */
/*                 received.                                                 */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgNhopChngRspAwaitDestroy (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
    tLdpMsgInfo         LdpMsg;
    /* Destroy the Newly Estabilsing LSP */
    LDP_SUPPRESS_WARNING (pMsg);
    MEMSET (&LdpMsg, LDP_ZERO, sizeof (tLdpMsgInfo));
    LDP_MSG_CTRL_BLK_PTR (&LdpMsg) = NH_USTR_CTRL_BLK (pNHCtrlBlock);
    LdpMergeDnSm (UPSTR_DSTR_PTR (NH_USTR_CTRL_BLK (pNHCtrlBlock)),
                  LDP_DSM_EVT_INT_DEL_UPSTR, &LdpMsg);
    LDP_DBG (LDP_ADVT_MISC, "ADVT_MRG_NH: New LSP CBlk Deleted \n");
    NH_TRGCB_ST (pNHCtrlBlock) = LDP_MRG_NH_LSM_ST_IDLE;
    return;
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpvcnsm.c                              */
/*---------------------------------------------------------------------------*/
