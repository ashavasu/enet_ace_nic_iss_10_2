/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldppdprc.c,v 1.59 2017/06/13 13:19:23 siva Exp $
 *
 * Description: Contains routines required for processing the PDU received.
 *              Also contains routine for sending the PDUs over TCP/UDP.
 ********************************************************************/

#include "ldpincs.h"
#include "mplslsr.h"

/* NOTE: Routine calling LdpSendMsg() Needs to take care of adding trace 
 * message for Peer LdpId before calling this routine */
/*****************************************************************************/
/* Function Name : LdpSendMsg                                                */
/* Description   : Fills up the LDP PDU Header and then calls the relevant   */
/*                 TCP/UDP routines for sending the LDP PDU.                 */
/*                                                                           */
/* Input(s)      : pBuf - Buffer containing the Message to be sent.          */
/*                 u2MsgLen    - Length of the message. This doesn't include */
/*                               the Message Header Length.                  */
/*                 pLdpEntity  - Points to the Ldp Entity which is sending   */
/*                               the message.                                */
/*                 u1IsHello   - Value 'LDP_TRUE', specifies that the        */
/*                               message being passed is Hello message. This */
/*                               field is used to interpret what u4PeerInfo */
/*                               points to.                                  */
/*                 u4PeerInfo -  Contains  the peer information. Dest Addr   */
/*                               in case of Hello messages and TCP connection*/
/*                               id in case of other messages.               */
/*                 u4IfAddr   -  Contains the Interface address. This is     */
/*                               used when sending LDP Basic Hello on        */
/*                               a particular interface.                     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS     if Message is sent Successfully else      */
/*                 LDP_FAILURE                                               */
/*****************************************************************************/

UINT1
LdpSendMsg (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2MsgLen,
            tLdpEntity * pLdpEntity, UINT1 u1IsHello, tGenAddr PeerInfo,
            tGenU4Addr IfAddr, UINT4 u4IfIndex)
{
    UINT2               u2PduLen = 0;
    INT4                i4DbgMsgDir = 0;
    tLdpPduHdr          PduHdr;
    /*MPLS_IPv6 add star */
    UINT4               u4DestAddr = 0;
    UINT4               u4ConnId = 0;
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            TempAllRouteAddr;
    MEMSET (&TempAllRouteAddr, 0, sizeof (tIp6Addr));
#endif
    /*MPLS_IPv6 add star */
    u2PduLen = (UINT2) (LDP_MAX_LDPID_LEN + LDP_MSG_HDR_LEN + u2MsgLen);
    /* Construct LDP PDU Header */
    PduHdr.u2Version = OSIX_HTONS (LDP_PROTO_VER);
    PduHdr.u2PduLen = OSIX_HTONS (u2PduLen);
    MEMCPY (PduHdr.LdpId, pLdpEntity->LdpId, LDP_MAX_LDPID_LEN);

    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &PduHdr, 0, LDP_PDU_HDR_LEN)
        != CRU_SUCCESS)
    {
        LDP_DBG (LDP_PRCS_MEM,
                 "PRCS: Error in Copying PduHdr to Buffer Chain\n");
        return LDP_FAILURE;
    }

    /* Check is to confirm whether any message dumps has been set  */
    if (((gu4LdpDumpType & LDP_DUMP_ALL) != LDP_ZERO) &&
        ((gu4LdpDumpDir == DUMP_DIR_OUT) || (gu4LdpDumpDir == DUMP_DIR_INOUT)))
    {
        i4DbgMsgDir = DUMP_DIR_OUT;
        LdpDumpLdpPdu (pBuf, i4DbgMsgDir);
    }

    if (u1IsHello != LDP_TRUE)
    {
        MEMCPY (&u4ConnId, LDP_IPV4_ADDR (PeerInfo.Addr), sizeof (u4ConnId));
        if (LdpTcpSend (u4ConnId, pBuf,
                        (UINT2) (u2PduLen + LDP_PDU_HDR_OFFSET)) != LDP_SUCCESS)
        {
            LDP_DBG (LDP_PRCS_TX, "PRCS: Failed to send TCP packet\n");
            return LDP_FAILURE;
        }
        LDP_DBG (LDP_PRCS_TX, "PRCS: Successful Tx of LDP PDU over Tcp\n");
        return LDP_SUCCESS;
    }
    else
    {
#ifdef MPLS_IPV6_WANTED
        if (LDP_ADDR_TYPE_IPV6 == IfAddr.u2AddrType)
        {
            /* If msg being sent is Basic Discovery Message */
            if (LDP_FAILURE !=
                INET_ATON6 (LDP_ALL_ROUTERS_V6ADDR, &TempAllRouteAddr))
            {
                if (LDP_ZERO ==
                    MEMCMP (LDP_IPV6_ADDR (PeerInfo.Addr),
                            TempAllRouteAddr.u1_addr, LDP_IPV6ADR_LEN))
                {
                    /* Hello packets are being sent on ldp enabled interfaces only */
                    if (LdpIpv6UdpSend
                        (LDP_STD_PORT, PeerInfo.Addr.Ip6Addr, LDP_STD_PORT,
                         pBuf, (UINT2) (u2PduLen + LDP_PDU_HDR_OFFSET),
                         IfAddr.Addr.Ip6Addr, u4IfIndex) != LDP_SUCCESS)
                    {
                        LDP_DBG (LDP_PRCS_TX,
                                 "PRCS: Failed to Tx Basic Hello Pkt\n");
                        return LDP_FAILURE;
                    }
                    LDP_DBG1 (LDP_IF_MISC,
                              "IF: IPV6: Success Tx Basic Hello Pkt\n",
                              __LINE__);
                    return LDP_SUCCESS;
                }
                /* if msg to be sent is Remote Discovery Message */
                else
                {
                    /* this is a unicast (hello)packet targeted at particular remote peer */
                    if (LdpIpv6UdpSend
                        (LDP_STD_PORT, PeerInfo.Addr.Ip6Addr, LDP_STD_PORT,
                         pBuf, (UINT2) (u2PduLen + LDP_PDU_HDR_OFFSET),
                         IfAddr.Addr.Ip6Addr, u4IfIndex) != LDP_SUCCESS)
                    {
                        LDP_DBG (LDP_PRCS_TX,
                                 "PRCS: Failed to Tx Remote Hello Pkt\n");
                        return LDP_FAILURE;
                    }
                    LDP_DBG1 (LDP_IF_MISC,
                              "IF: IPV6: Success Targeted V6  Hello Pkt Sent\n",
                              __LINE__);
                    return LDP_SUCCESS;
                }
            }
            else
            {
                LDP_DBG (LDP_IF_MISC,
                         "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
                return LDP_FAILURE;
            }
        }
        else
        {
#endif
            /*To remove warning */
            UNUSED_PARAM (u4IfIndex);
            /* If msg being sent is Basic Discovery Message */
            MEMCPY (&u4DestAddr, LDP_IPV4_ADDR (PeerInfo.Addr),
                    LDP_IPV4ADR_LEN);
            if (u4DestAddr == LDP_ALL_ROUTERS_ADDR)
            {
                /* Hello packets are being sent on ldp enabled interfaces only */
                if (LdpUdpSend (LDP_STD_PORT, u4DestAddr, LDP_STD_PORT,
                                pBuf,
                                (UINT2) (u2PduLen + LDP_PDU_HDR_OFFSET),
                                IfAddr.Addr.u4Addr) != LDP_SUCCESS)
                {
                    LDP_DBG (LDP_PRCS_TX,
                             "PRCS: Failed to Tx Basic Hello Pkt\n");
                    return LDP_FAILURE;
                }
                return LDP_SUCCESS;
            }
            /* if msg to be sent is Remote Discovery Message */
            else
            {
                /* this is a unicast (hello)packet targeted at particular remote peer */
                u4DestAddr = OSIX_NTOHL (u4DestAddr);
                if (LdpUdpSend (LDP_STD_PORT, u4DestAddr, LDP_STD_PORT, pBuf,
                                (UINT2) (u2PduLen + LDP_PDU_HDR_OFFSET),
                                LDP_IP_INVALID_INDEX) != LDP_SUCCESS)
                {
                    LDP_DBG (LDP_PRCS_TX,
                             "PRCS: Failed to Tx Remote Hello Pkt\n");
                    return LDP_FAILURE;
                }
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleHelloMsg                                         */
/* Description   : Called to handle the hello messges received from the peer */
/*                 LSRs                                                      */
/* Input(s)      : pMsg    - Points to Ldp Hello Message that is received    */
/*                           from the peer LSR.                              */
/*                 PeerLdpId - Ldp Id of the peer that sent the Hello Msg.   */
/*                 u4PeerAddr - Source Addr of Peer from which the Hello Pkt */
/*                              is Rx. This Addr is assigned to Peer's Ntw   */
/*                              Addr, when TrAddrTlv is not present in Hello.*/
/*                 u4IfIndex - Interface Index over which the Hello Msg is   */
/*                             received.                                     */
/*                 u2IncarnId - Incarnation Id.                              */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

/* MPLS_IPv6 mod start*/
UINT1
LdpHandleHelloMsg (UINT1 *pMsg, tLdpId PeerLdpId, tGenAddr PeerAddr,
                   UINT4 u4IfIndex, UINT2 u2IncarnId, UINT1 u1DiscardV6LHello)
{
    /* MPLS_IPv6 mod end */
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    UINT4               u4TempLocalAddr = LDP_ZERO;
    UINT4               u4SsnL3IfIndex = LDP_ZERO;
#ifdef LDP_GR_WANTED
    UINT4               u4PeerAddr;
#endif
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pSessionEntry = NULL;
    tLdpAdjacency      *pLdpAdj = NULL;
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    /* MPLS_IPv6 mod start */
    tGenAddr            TempPeerAddr;
    tGenAddr            TempPeerAddr1;
    tIp6Addr            Ipv6TrAddr;
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            Ipv6TempLocalAddr;
    tIp6Addr            V6TempZeroAddr;
#endif
    /* MPLS_IPv6 mod end */
    UINT1               u1MTlvPrcd = LDP_FALSE;    /* Flag indicating whether 
                                                 * Mandatory Tlv is
                                                 * processed or not */
    UINT1               u1RemoteFlag = LDP_FALSE;
    UINT1               u1PeerPresent = LDP_FALSE;
    UINT1               u1SsnAbsent = LDP_FALSE;
    UINT1               u1SsnRole = LDP_PASSIVE;
    UINT1               u1CSNumPresent = LDP_FALSE;    /* flag indicating if 
                                                       Configuration sequence
                                                       number is present in 
                                                       the Hello Msg 
                                                       received */
    UINT1               u1TrAddrPresent = LDP_FALSE;    /* Specifies if 
                                                           Transport Address 
                                                           Tlv is present 
                                                           in the Hello Msg 
                                                           received */
    /* MPLS_IPv6  start */
    UINT1               u1TrAddr6Present = LDP_FALSE;
    /* MPLS_IPv6  end */
    UINT4               u4TrAddr = 0;
    UINT4               u4TempNetAddr = 0;
    UINT4               u4CSNumber = 0;
    UINT4               u4InLabel = 0;
    UINT4               u4AssocIf = 0;
    UINT1               au4TempPeerId[4] = { 0, 0, 0, 0 };

    UINT2               u2Len = 0;
    UINT2               u2TmpLen = 0;
    UINT2               u2HLen = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2HoldTime = 0;
    UINT2               u2LabelOperation = 0;
    UINT2               u2TRField = 0;    /* Present in Cmn Hello Params Tlv */
    UINT1               u1Flag1 = LDP_FAILURE;
    UINT1               u1Flag2 = LDP_FAILURE;
    /* MPLS_IPv6 add start */
    UINT1               u1IPV6HelloRcvd = LDP_FALSE;
    MEMSET (&TempPeerAddr, 0, sizeof (tGenAddr));
    MEMSET (&TempPeerAddr1, 0, sizeof (tGenAddr));

#ifdef LDP_HA_WANTED
    if (LDP_RM_STANDBY == gLdpInfo.ldpRmInfo.u4LdpRmState)
    {
        LDP_DBG (LDP_SSN_PRCS, "LdpHandleHelloMsg: Standby Node "
                 "LDP Hello Message not processed\r\n");
        return LDP_FAILURE;
    }
#endif

#ifdef LDP_GR_WANTED
    if ((gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus ==
         LDP_GR_SHUT_DOWN_IN_PROGRESS) &&
        (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) == LDP_GR_CAPABILITY_FULL))
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpHandleHelloMsg: GR Restore is in Progress "
                 "LDP Hello Message not processed\r\n");
        return LDP_FAILURE;
    }
#endif

    if (MsrGetRestorationStatus () == ISS_TRUE)
    {
        LDP_DBG (LDP_SSN_PRCS,
                 "SESSION: MSR Restoration in progress - "
                 "LDP Hello Message not processed\r\n");
        return LDP_FAILURE;
    }

#ifdef MPLS_IPV6_WANTED
    if (LDP_ADDR_TYPE_IPV6 == PeerAddr.u2AddrType)
    {
        MEMSET (&Ipv6TrAddr, 0, sizeof (tIp6Addr));
        MEMSET (&Ipv6TempLocalAddr, 0, sizeof (tIp6Addr));
        MEMSET (&V6TempZeroAddr, 0, sizeof (tIp6Addr));
        u1IPV6HelloRcvd = LDP_TRUE;
        LDP_DBG1 (LDP_PRCS_RX,
                  "PRCS: IPV6 Hello Message received on the interface %d\n",
                  u4IfIndex);
    }
    else
    {
#endif
        UNUSED_PARAM (u1DiscardV6LHello);
        LDP_DBG1 (LDP_PRCS_RX,
                  "PRCS: IPV4 Hello Message received on the interface %d\n",
                  u4IfIndex);
#ifdef MPLS_IPV6_WANTED
    }
#endif
    /* MPLS_IPv6 add end */

    /* Decode the Hello Msg and store the HoldTime, CSNUM and TrAddr */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);    /* Length of Hello 
                                                               Msg */
    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);
    u2HLen -= (UINT2) LDP_MSG_ID_LEN;    /* This length specifies the total 
                                           length of all the tlvs together 
                                           contained in this hello Msg */
    if (u2HLen == LDP_ZERO)
    {
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: No TLV present in the message");
        return LDP_FAILURE;
    }

    while (u2Len < u2HLen)
    {
        LDP_EXT_2_BYTES (pMsg, u2TlvType);    /* Extract the Tlv Type */
        LDP_EXT_2_BYTES (pMsg, u2TmpLen);    /* Extract the Tlv Len */
        /* Check for the bad Tlv length is done here */
        if ((u2TmpLen < LDP_TLV_HDR_LEN) || ((u2Len + u2TmpLen) > u2HLen))
        {
            LDP_DBG (LDP_PRCS_PRCS, "PRCS: Bad Tlv Len in the Rx Hello Msg\n");
            return LDP_FAILURE;
        }

        switch (u2TlvType)
        {
            case LDP_CMN_HELLO_PARM_TLV:
                LDP_EXT_2_BYTES (pMsg, u2HoldTime);    /* Get the Hold Time from
                                                       the Tlv */
                LDP_EXT_2_BYTES (pMsg, u2TRField);    /* Get the T & R fields
                                                       from the Tlv */
                u1MTlvPrcd = LDP_TRUE;
#ifdef MPLS_IPV6_WANTED
                if ((LDP_TRUE == u1IPV6HelloRcvd)
                    && (LDP_TARG_FLAG != (u2TRField & LDP_TARG_FLAG))
                    && (LDP_FALSE != u1DiscardV6LHello))
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "SSN_PRCS:Discarding V6 Link Hello HopLimit OR Dest Addr (LDP_V6_ALL_Router_Addr) Not Correct \n");
                    return LDP_FAILURE;
                }
#endif
                break;

            case LDP_IPV4_TRANSPORT_ADDR_TLV:
                /* MPLS_IPv6 mod start */
                if (LDP_FALSE == u1TrAddrPresent)
                {
                    LDP_EXT_4_BYTES (pMsg, u4TrAddr);    /* Check Here. Addr is 
                                                           being read as 4 bytes */
                    u1TrAddrPresent = LDP_TRUE;
                }
                else
                {
                    pMsg += 4;
                }
                /* MPLS_IPv6 mod end */
                break;

            case LDP_CONF_SEQNUMB_TLV:
                LDP_EXT_4_BYTES (pMsg, u4CSNumber);
                u1CSNumPresent = LDP_TRUE;
                break;

            case LDP_IPV6_TRANSPORT_ADDR_TLV:
                /* MPLS_IPv6 mod start */
#ifdef MPLS_IPV6_WANTED
                if (LDP_FALSE == u1TrAddr6Present)
                {
                    LDP_EXT_16_BYTES (pMsg, Ipv6TrAddr.u1_addr)
                        LDP_DBG (LDP_PRCS_PRCS,
                                 "PRCS: IPV6 Transport Address TLV received in Hello Msg\n");
                    u1TrAddr6Present = LDP_TRUE;
                }
                else
                {
                    pMsg += 16;
                }
                break;
#else /*MPLS_IPV6_WANTED Not Defined */
                LDP_DBG (LDP_PRCS_PRCS,
                         "PRCS: IPV6 Transport Address TLV Not Supoorted \n");
                return LDP_FAILURE;
#endif
                /* MPLS_IPv6 mod end */
            default:
                LDP_DBG (LDP_PRCS_PRCS,
                         "PRCS: Invalid TlvType Rx in Hello Msg\n");
                /* As per RFC 5036 section 3.3, If both U bit is set
                 * the unknown TLV is silently ignored and the rest of the message
                 * is processed
                 * */
                if (!(u2TlvType & LDP_TLV_MSG_U_BIT))
                {
                    LDP_DBG (LDP_PRCS_PRCS,
                             "PRCS: Invalid TlvType Rx in Hello Msg\n");
                    return LDP_FAILURE;
                }
                pMsg += u2TmpLen;
                break;

        }

        /* Check if Mand Tlv is processed or not */
        if (u1MTlvPrcd != LDP_TRUE)
        {
            LDP_DBG (LDP_PRCS_PRCS,
                     "PRCS: CmnHelloParams Tlv(Mandatory) is not present\n");
            return LDP_FAILURE;
        }

        u2Len += (UINT2) (u2TmpLen + LDP_TLV_HDR_LEN);    /* Increment by the msg length 
                                                           processed */
    }                            /* End of the While loop for decoding 
                                   the Tlvs in Hello Msg */
    /* MPLS_IPv6 mod start */
#ifdef MPLS_IPV6_WANTED
    if (LDP_TRUE == u1IPV6HelloRcvd)
    {
        LDP_DBG2 (LDP_PRCS_RX, "PRCS: IPV6 Hello Message received on the "
                  "interface %d From Peer Addr %s\n", u4IfIndex,
                  Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)));
        if (LDP_FALSE == u1TrAddr6Present)
        {
            MEMCPY (LDP_IPV6_ADDR (TempPeerAddr.Addr),
                    LDP_IPV6_ADDR (PeerAddr.Addr), LDP_IPV6ADR_LEN);
            LDP_DBG1 (LDP_PRCS_RX,
                      "PRCS V6: Tr Addr not present Fetching Peer Source Addr %s\n",
                      Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)));
        }
        else
        {
            MEMCPY (TempPeerAddr.Addr.Ip6Addr.u1_addr, Ipv6TrAddr.u1_addr,
                    LDP_NET_ADDR6_LEN);
            LDP_DBG1 (LDP_PRCS_RX, "PRCS V6: Tr Addr present %s\n",
                      Ip6PrintAddr (&(Ipv6TrAddr)));
        }
        TempPeerAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
    }
    else
    {
#endif
        LDP_DBG5 (LDP_PRCS_RX, "PRCS: IPV4  Hello Message received on the "
                  "interface %d from %x:%x:%x:%x\n", u4IfIndex,
                  PeerAddr.Addr.au1Ipv4Addr[0], PeerAddr.Addr.au1Ipv4Addr[1],
                  PeerAddr.Addr.au1Ipv4Addr[2], PeerAddr.Addr.au1Ipv4Addr[3]);
        if (u1TrAddrPresent == LDP_FALSE)
        {
            MEMCPY (LDP_IPV4_ADDR (TempPeerAddr.Addr),
                    LDP_IPV4_ADDR (PeerAddr.Addr), LDP_IPV4ADR_LEN);
            LDP_DBG4 (LDP_PRCS_RX,
                      "PRCS: IPV4  Hello Tr Addr not Present Fetching Peer Source Addr %x:%x:%x:%x\n",
                      TempPeerAddr.Addr.au1Ipv4Addr[0],
                      TempPeerAddr.Addr.au1Ipv4Addr[1],
                      TempPeerAddr.Addr.au1Ipv4Addr[2],
                      TempPeerAddr.Addr.au1Ipv4Addr[3]);
        }
        else
        {
            u4TrAddr = OSIX_HTONL (u4TrAddr);
            MEMCPY (&(TempPeerAddr.Addr.au1Ipv4Addr), (UINT1 *) &(u4TrAddr),
                    LDP_NET_ADDR_LEN);
            LDP_DBG4 (LDP_PRCS_RX,
                      "PRCS: IPV4  Hello Tr Addr Present %x:%x:%x:%x\n",
                      TempPeerAddr.Addr.au1Ipv4Addr[0],
                      TempPeerAddr.Addr.au1Ipv4Addr[1],
                      TempPeerAddr.Addr.au1Ipv4Addr[2],
                      TempPeerAddr.Addr.au1Ipv4Addr[3]);
        }
        TempPeerAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
#ifdef MPLS_IPV6_WANTED
    }
#endif
    /* MPLS_IPv6 mod end */
    u1RemoteFlag = LDP_IS_TFLAG_SET (u2TRField);
    if (u1RemoteFlag == LDP_TRUE)
    {
        u1Flag1 = LdpGetEntityFromIfIndex (u2IncarnId, u4IfIndex, LDP_FALSE,
                                           &pLdpEntity, PeerAddr,
                                           &pLdpPeer, PeerLdpId);
#ifdef MPLS_IPV6_WANTED
        if (LDP_TRUE == u1IPV6HelloRcvd)
        {
            if (u1TrAddr6Present != LDP_FALSE)
            {
                /*If Targeted Hello is received with TRANS ADDR other than Gloabl Unicast, Then Discard Hello */
                if (ADDR6_SCOPE_GLOBAL != Ip6GetAddrScope (&Ipv6TrAddr))
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "PRCS: Targeted Hello Rcvd With Transport Addr not a Global Unicast Address. Discarding HEllo\n");
                    return LDP_FAILURE;
                }
                u1Flag2 =
                    LdpGetEntityFromIfIndex (u2IncarnId, u4IfIndex, LDP_FALSE,
                                             &pLdpEntity, TempPeerAddr,
                                             &pLdpPeer, PeerLdpId);
            }
        }
        else
        {
#endif
            if (u1TrAddrPresent != LDP_FALSE)
            {
                u1Flag2 =
                    LdpGetEntityFromIfIndex (u2IncarnId, u4IfIndex, LDP_FALSE,
                                             &pLdpEntity, TempPeerAddr,
                                             &pLdpPeer, PeerLdpId);
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
        /* MPLS_IPv6 mod end */
        if ((u1Flag1 == LDP_FAILURE) && (u1Flag2 == LDP_FAILURE))
        {
            LDP_DBG1 (LDP_SSN_PRCS,
                      "PRCS: Targeted Ldp Entity for IfIndex %d Not Found\n",
                      u4IfIndex);
            return LDP_FAILURE;
        }
        /* MPLS_IPv6 mod start */
#ifdef MPLS_IPV6_WANTED
        if (LDP_TRUE == u1IPV6HelloRcvd)
        {
            LDP_DBG1 (LDP_SSN_PRCS,
                      "SESSION: LDP Entity %d found for Targeted "
                      "IPV6 Hello received \n", pLdpEntity->u4EntityIndex);
            if ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) == TRUE) &&
                (LDP_ENTITY_IPV6TRANSPORT_ADDRKIND (pLdpEntity) ==
                 LOOPBACK_ADDRESS_TYPE)
                && (pLdpEntity->bIsIpv6TransAddrIntfUp == LDP_FALSE))
            {
                LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Transport Interface to recieve Targeted IPV6 LDP Hello %x\n"
                          " is DOWN", pLdpEntity->u4Ipv6TransIfIndex);
                return LDP_FAILURE;
            }
        }
        else
        {
#endif
            LDP_DBG5 (LDP_SSN_PRCS,
                      "SESSION: LDP Entity %d found for Targeted "
                      "IPV4 Hello received with from %x:%x:%x:%x \n",
                      pLdpEntity->u4EntityIndex, PeerAddr.Addr.au1Ipv4Addr[0],
                      PeerAddr.Addr.au1Ipv4Addr[1],
                      PeerAddr.Addr.au1Ipv4Addr[2],
                      PeerAddr.Addr.au1Ipv4Addr[3]);
            if ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) == TRUE)
                && (LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) ==
                    LOOPBACK_ADDRESS_TYPE)
                && (pLdpEntity->bIsTransAddrIntfUp == LDP_FALSE))
            {
                LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Transport Interface to recieve Targeted IPV4 LDP Hello %x\n"
                          " is DOWN", pLdpEntity->u4TransIfIndex);
                return LDP_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
        /* MPLS_IPv6 mod end */
        u1PeerPresent = LDP_TRUE;
        if (MEMCMP ((UINT1 *) &pLdpPeer->PeerLdpId,
                    (UINT1 *) &au4TempPeerId, LDP_NET_ADDR_LEN) == LDP_ZERO)
        {
            LDP_DBG4 (LDP_SSN_RX,
                      "SESSION: Targeted Hello received from the Peer with"
                      "LDP ID %d.%d.%d.%d for the first time\n",
                      PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);

            /* Check tunnel's existance, 
             * * it tunnels not exist then defer the LDP session. */
            if ((LDP_ENTITY_LDP_OVER_RSVP_FLAG (pLdpEntity) == LDP_SNMP_TRUE) &&
                (pLdpEntity->OutStackTnlInfo.u4TnlId != LDP_ZERO))
            {
                if (TeIsTnlOperStatusUp
                    (pLdpEntity->OutStackTnlInfo.u4TnlId,
                     pLdpEntity->OutStackTnlInfo.u4TnlInstance,
                     pLdpEntity->OutStackTnlInfo.u4IngressId,
                     pLdpEntity->OutStackTnlInfo.u4EgressId) != TRUE)
                {
                    LDP_DBG5 (LDP_SSN_PRCS,
                              "SESSION: Out tunnel %d %d %x %x is not up for "
                              "handling remote peers for the entity %d\n",
                              pLdpEntity->OutStackTnlInfo.u4TnlId,
                              pLdpEntity->OutStackTnlInfo.u4TnlInstance,
                              pLdpEntity->OutStackTnlInfo.u4IngressId,
                              pLdpEntity->OutStackTnlInfo.u4EgressId,
                              pLdpEntity->u4EntityIndex);
                    return LDP_FAILURE;
                }
            }

            if ((LDP_ENTITY_LDP_OVER_RSVP_FLAG (pLdpEntity) == LDP_SNMP_TRUE) &&
                (pLdpEntity->InStackTnlInfo.u4TnlId != LDP_ZERO))
            {
                MPLS_CMN_LOCK ();

                pTeTnlInfo =
                    TeGetTunnelInfo (pLdpEntity->InStackTnlInfo.u4TnlId,
                                     pLdpEntity->InStackTnlInfo.u4TnlInstance,
                                     pLdpEntity->InStackTnlInfo.u4IngressId,
                                     pLdpEntity->InStackTnlInfo.u4EgressId);
                if (pTeTnlInfo == NULL)
                {
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "SESSION: In Tunnel %d is not up for handling "
                              "remote peers for the entity %d\n",
                              pLdpEntity->InStackTnlInfo.u4TnlId,
                              pLdpEntity->u4EntityIndex);
                    MPLS_CMN_UNLOCK ();
                    return LDP_FAILURE;
                }

                /* Incoming tunnel label is stored here for ILM delete
                 * when we don't have the in tunnel */
                pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                      MPLS_DEF_DIRECTION);
                if ((pXcEntry != NULL) && (pXcEntry->pInIndex != NULL))
                {
                    pInSegment = pXcEntry->pInIndex;
                    u4InLabel = pInSegment->u4Label;
                }
                pLdpEntity->u4InTnlLabel = u4InLabel;

                TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo) =
                    pLdpEntity->u4EntityIndex;

                LDP_DBG2 (LDP_SSN_PRCS,
                          "SESSION: Entity %d registered with In Tunnel %d "
                          "for LDP over RSVP Case\n",
                          pLdpEntity->u4EntityIndex,
                          pLdpEntity->InStackTnlInfo.u4TnlId);
                MPLS_CMN_UNLOCK ();
            }

            /* received targeted hello from peer for the first time
             * get peer's Ldp Id and store in peer list */
            MEMCPY (pLdpPeer->PeerLdpId, PeerLdpId, LDP_MAX_LDPID_LEN);
        }
        else if (MEMCMP (pLdpPeer->PeerLdpId, PeerLdpId,
                         LDP_MAX_LDPID_LEN) != LDP_ZERO)
        {
            MEMSET (pLdpPeer->PeerLdpId, LDP_ZERO, LDP_MAX_LDPID_LEN);
        }

        LDP_GET_TX_REQ_FLAG (u2TRField, pLdpPeer->u1TrgtHelloTxReqFlag);
    }
    else
    {                            /* Hello is Local */
        /* Scanning through the Entity List to find the matching Entity from
         * IfIndex. */
        /*MPLS_IPv6 mod start */
        /*Arguement 5 is changed as Func Def is accepting tGenAddr, Although PeerAddr is not */
        /*used in definition of function in case of Basic Hello */
        if (LdpGetEntityFromIfIndex (u2IncarnId, u4IfIndex, LDP_TRUE,
                                     &pLdpEntity, PeerAddr, &pLdpPeer,
                                     PeerLdpId) != LDP_SUCCESS)
        {
            /*MPLS_IPv6 mod end */
            LDP_DBG1 (LDP_SSN_PRCS,
                      "PRCS: Ldp Entity for IfIndex %d Not Found \n",
                      u4IfIndex);
            return LDP_FAILURE;
        }

#ifdef MPLS_IPV6_WANTED
        if (LDP_TRUE == u1IPV6HelloRcvd)
        {
            LDP_DBG2 (LDP_SSN_PRCS,
                      "SESSION: LDP Entity %d found for V6 Link Hello received from "
                      "%s\n", pLdpEntity->u4EntityIndex,
                      Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)));
            if ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) == TRUE)
                && (LDP_ENTITY_IPV6TRANSPORT_ADDRKIND (pLdpEntity) ==
                    LOOPBACK_ADDRESS_TYPE)
                && (pLdpEntity->bIsIpv6TransAddrIntfUp == LDP_FALSE))
            {
                LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Transport Interface to recieve Basic IPV6 LDP Hello %x\n"
                          " is DOWN", pLdpEntity->u4Ipv6TransIfIndex);
                return LDP_FAILURE;
            }
        }
        else
        {
#endif
            LDP_DBG5 (LDP_SSN_PRCS,
                      "SESSION: LDP Entity %d found for V4 Link Hello received from "
                      "%x:%x:%x:%x\n", pLdpEntity->u4EntityIndex,
                      PeerAddr.Addr.au1Ipv4Addr[0],
                      PeerAddr.Addr.au1Ipv4Addr[1],
                      PeerAddr.Addr.au1Ipv4Addr[2],
                      PeerAddr.Addr.au1Ipv4Addr[3]);
            if ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) == TRUE)
                && (LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) ==
                    LOOPBACK_ADDRESS_TYPE)
                && (pLdpEntity->bIsTransAddrIntfUp == LDP_FALSE))
            {
                LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Transport Interface to recieve Basic IPV4 LDP Hello %x\n"
                          " is DOWN", pLdpEntity->u4TransIfIndex);
                return LDP_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
        /*MPLS_IPv6 MOD start */
        TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
        {
#ifdef MPLS_IPV6_WANTED
            if (LDP_TRUE == u1IPV6HelloRcvd)
            {
                MEMCPY (LDP_IPV6_ADDR (TempPeerAddr1.Addr),
                        LDP_IPV6_ADDR (pLdpPeer->NetAddr.Addr),
                        LDP_IPV6ADR_LEN);
                if ((LDP_ZERO ==
                     MEMCMP (TempPeerAddr1.Addr.Ip6Addr.u1_addr,
                             TempPeerAddr.Addr.Ip6Addr.u1_addr,
                             LDP_IPV6ADR_LEN))
                    && (LDP_ZERO !=
                        MEMCMP (pLdpPeer->PeerLdpId, PeerLdpId,
                                LDP_MAX_LDPID_LEN)))
                {
                    MEMCPY (pLdpPeer->PeerLdpId, PeerLdpId, LDP_MAX_LDPID_LEN);
                    u1PeerPresent = LDP_TRUE;
                    break;
                }
                if (MEMCMP (pLdpPeer->PeerLdpId, PeerLdpId, LDP_MAX_LDPID_LEN)
                    == 0)
                {
                    LDP_DBG4 (LDP_SSN_PRCS,
                              "SESSION: Peer Found for Basic IPV6 Hello Received "
                              "with PEER LDP ID %d.%d.%d.%d\n", PeerLdpId[0],
                              PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);
                    u1PeerPresent = LDP_TRUE;
                    break;
                }
            }
            else
            {
#endif
                MEMCPY (LDP_IPV4_ADDR (TempPeerAddr1.Addr),
                        LDP_IPV4_ADDR (pLdpPeer->NetAddr.Addr),
                        LDP_NET_ADDR_LEN);

                if ((MEMCMP
                     (TempPeerAddr1.Addr.au1Ipv4Addr,
                      TempPeerAddr.Addr.au1Ipv4Addr, LDP_NET_ADDR_LEN) == 0)
                    &&
                    (MEMCMP (pLdpPeer->PeerLdpId, PeerLdpId, LDP_MAX_LDPID_LEN)
                     != LDP_ZERO))
                {
                    MEMCPY (pLdpPeer->PeerLdpId, PeerLdpId, LDP_MAX_LDPID_LEN);
                    u1PeerPresent = LDP_TRUE;
                    break;
                }

                if (MEMCMP (pLdpPeer->PeerLdpId, PeerLdpId, LDP_MAX_LDPID_LEN)
                    == 0)
                {
                    LDP_DBG4 (LDP_SSN_PRCS,
                              "SESSION: Peer Found for Basic IPV4 Hello Received "
                              "from %d.%d.%d.%d\n", PeerLdpId[0], PeerLdpId[1],
                              PeerLdpId[2], PeerLdpId[3]);
                    u1PeerPresent = LDP_TRUE;
                    break;
                }
#ifdef MPLS_IPV6_WANTED
            }
#endif
        }
        /*MPLS_IPv6 MOD end */
    }

    if (u1PeerPresent == LDP_FALSE)
    {
        LDP_DBG5 (LDP_SSN_RX,
                  "SESSION: Link Hello received from PEER LDP ID %d.%d.%d.%d for the "
                  "LDP Entity %d for the first time\n",
                  PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3],
                  pLdpEntity->u4EntityIndex);

        /* Peer is Absent. Hence Creating a Peer Entry for the Local Peer */
#ifdef MPLS_IPV6_WANTED
        if (LDP_TRUE == u1IPV6HelloRcvd)
        {
            if (LdpCreateLdpPeer
                (pLdpEntity, PeerLdpId, u1RemoteFlag, &pLdpPeer,
                 LDP_ADDR_TYPE_IPV6) == LDP_FAILURE)
            {
                LDP_DBG (LDP_SSN_PRCS, "PRCS: Failed to Create V6 Ldp Peer\n");
                return LDP_FAILURE;
            }
            LDP_DBG2 (LDP_SSN_PRCS,
                      "PRCS: Peer Entry created for the peer %s in the "
                      "Entity %d\n", Ip6PrintAddr (&(PeerAddr.Addr.Ip6Addr)),
                      pLdpEntity->u4EntityIndex);
        }
        else
        {
#endif
            if (LdpCreateLdpPeer
                (pLdpEntity, PeerLdpId, u1RemoteFlag, &pLdpPeer,
                 LDP_ADDR_TYPE_IPV4) == LDP_FAILURE)
            {
                LDP_DBG (LDP_SSN_PRCS, "PRCS: Failed to Create V4 Ldp Peer\n");
                return LDP_FAILURE;
            }
            LDP_DBG5 (LDP_SSN_PRCS,
                      "PRCS: Peer Entry created for the peer %x:%x:%x:%x in the "
                      "Entity %d\n", PeerAddr.Addr.au1Ipv4Addr[0],
                      PeerAddr.Addr.au1Ipv4Addr[1],
                      PeerAddr.Addr.au1Ipv4Addr[2],
                      PeerAddr.Addr.au1Ipv4Addr[3], pLdpEntity->u4EntityIndex);
#ifdef MPLS_IPV6_WANTED
        }
#endif
        if (u1CSNumPresent == LDP_TRUE)
        {
            /* Received hello msg's CS num updated in the peer */
            pLdpPeer->u4ConfSeqNum = u4CSNumber;
        }
    }
    if (LDP_TRUE == u1IPV6HelloRcvd)
    {
        LDP_DBG2 (LDP_SSN_PRCS, " V6NetAddr %s   \n IPv6TrAddr Recvd %s   \n",
                  Ip6PrintAddr (&(pLdpPeer->NetAddr.Addr.Ip6Addr)),
                  Ip6PrintAddr (&(Ipv6TrAddr)));
    }
    else
    {
        LDP_DBG2 (LDP_SSN_PRCS, " TempNetAddr %d \n TrADDR %d \n",
                  u4TempNetAddr, u4TrAddr);
    }
    if (LDP_ADDR_TYPE_IPV4 == pLdpPeer->NetAddr.u2AddrType)
    {
        MEMCPY (&u4TempNetAddr, LDP_IPV4_ADDR (pLdpPeer->NetAddr.Addr),
                LDP_IPV4ADR_LEN);
    }
    /* Peer is present. Check for Session and Adjacecny */
    if (pLdpPeer->pLdpSession == NULL)
    {
        /* Session not present => Adjacencies can not be present */
        /* Adj is created after creating the session */

        u1SsnAbsent = LDP_TRUE;
    }
    /* session present and this Adj is not present */
    else if (LdpIsAdjEntryPresent
             ((tLdpSession *) pLdpPeer->pLdpSession, u4IfIndex,
              u1IPV6HelloRcvd) != LDP_TRUE)
    {
        /* MPLS_IPv6 mod start */
        /* check for change in Conf Seq number */
        if (((u1CSNumPresent == LDP_TRUE) && (pLdpPeer->u4ConfSeqNum != 0)
             && (u4CSNumber != pLdpPeer->u4ConfSeqNum)) ||
            ((u1TrAddrPresent == LDP_TRUE)
             && (LDP_ADDR_TYPE_IPV4 == pLdpPeer->NetAddr.u2AddrType)
             && (u4TrAddr != u4TempNetAddr)) || ((u1TrAddr6Present == LDP_TRUE)
                                                 && (LDP_ADDR_TYPE_IPV6 ==
                                                     pLdpPeer->NetAddr.
                                                     u2AddrType)
                                                 &&
                                                 (MEMCMP
                                                  (&
                                                   (pLdpPeer->NetAddr.Addr.
                                                    Ip6Addr.u1_addr),
                                                   Ipv6TrAddr.u1_addr,
                                                   LDP_NET_ADDR6_LEN) != 0)))
        {
            /* MPLS_IPv6 mod end */
            /* Received hello msg's modified CS num updated in the peer */
            pLdpPeer->u4ConfSeqNum = u4CSNumber;

            /* Re-Establish the session */
            LDP_DBG (LDP_PRCS_PRCS,
                     "PRCS: Deleting Session CsNum Changed or TrAddr gets changed\n");
            LdpDeleteLdpSession ((tLdpSession *) pLdpPeer->pLdpSession,
                                 LDP_STAT_TYPE_SHUT_DOWN);
            u1SsnAbsent = LDP_TRUE;

        }
        else
        {
            u4SsnL3IfIndex =
                LdpSessionGetIfIndex (pLdpPeer->pLdpSession,
                                      pLdpPeer->NetAddr.u2AddrType);
            /* Create Adj Entry */
            /* MPLS_IPv6 mod start */
            if (LdpCreateAdj ((tLdpSession *) pLdpPeer->pLdpSession,
                              u4IfIndex, u2HoldTime, PeerLdpId,
                              pLdpEntity->u2IncarnId, PeerAddr) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_PRCS_PRCS,
                         "PRCS: Failed to Create LdpAdjacency\n");
                /* Not returning Failure. Ignoring the Error */
            }
            /* MPLS_IPv6 mod end */
            else if ((LDP_ENTITY_IS_TARGET_TYPE
                      (SSN_GET_ENTITY (pLdpPeer->pLdpSession)) == LDP_TRUE)
                     && (SSN_GET_ENTITY (pLdpPeer->pLdpSession)->InStackTnlInfo.
                         u4TnlId != LDP_ZERO))
            {
                /* New adjacency has been formed for the 
                 * existing targeted session, so delete all the originated LSPs'
                 * L3 interface index */
                /* Scan the upstream Lsp Control Block List */
                TMO_SLL_Scan (&SSN_ULCB (pLdpPeer->pLdpSession), pLdpSllNode,
                              tTMO_SLL_NODE *)
                {
                    pLspUpCtrlBlock = (tUstrLspCtrlBlock *) (pLdpSllNode);
                    if (pLspUpCtrlBlock->u4InComIfIndex != 0)
                    {
                        /* Intermediate Node */
                        if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
                            ((LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) !=
                             NULL))
                        {
                            /* Allow programming the ILM operation only
                             * when the downstream MLIB operation has been completed */
                            if (LCB_MLIB_UPD_STATUS
                                (UPSTR_DSTR_PTR (pLspUpCtrlBlock)) !=
                                LDP_DU_DN_MLIB_UPD_NOT_DONE)
                            {
                                u2LabelOperation = MPLS_OPR_POP_PUSH;
                                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                                       u2LabelOperation,
                                                       UPSTR_DSTR_PTR
                                                       (pLspUpCtrlBlock),
                                                       pLspUpCtrlBlock);
                            }
                        }
                        else    /* Egress Node */
                        {
                            u2LabelOperation = MPLS_OPR_POP;

                            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                                   u2LabelOperation,
                                                   UPSTR_DSTR_PTR
                                                   (pLspUpCtrlBlock),
                                                   pLspUpCtrlBlock);
                        }

                        pLspUpCtrlBlock->u4InComIfIndex = u4SsnL3IfIndex;

                        /* Intermediate Node */
                        if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
                            ((LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) !=
                             NULL))
                        {
                            /* Allow programming the ILM operation only
                             * when the downstream MLIB operation has been completed */
                            if (LCB_MLIB_UPD_STATUS
                                (UPSTR_DSTR_PTR (pLspUpCtrlBlock)) !=
                                LDP_DU_DN_MLIB_UPD_NOT_DONE)
                            {
                                u2LabelOperation = MPLS_OPR_POP_PUSH;
                                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE,
                                                       u2LabelOperation,
                                                       UPSTR_DSTR_PTR
                                                       (pLspUpCtrlBlock),
                                                       pLspUpCtrlBlock);
                            }
                        }
                        else    /* Egress Node */
                        {
                            u2LabelOperation = MPLS_OPR_POP;

                            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE,
                                                   u2LabelOperation,
                                                   UPSTR_DSTR_PTR
                                                   (pLspUpCtrlBlock),
                                                   pLspUpCtrlBlock);
                        }

                    }
                }
            }
        }
    }
    else
    {                            /* Session present and entry for this Adj is also present  */

#ifdef LDP_GR_WANTED
        if (pLdpPeer->pLdpSession->u1StaleStatus == LDP_TRUE)
        {
#ifdef MPLS_IPV6_WANTED
            if ((LDP_TRUE == u1IPV6HelloRcvd) &&
                (pLdpPeer->pLdpSession->SrcTransAddr.u2AddrType ==
                 LDP_ADDR_TYPE_IPV6))
            {
                if (LdpRefreshStaleIpv6LdpSession
                    (pLdpPeer->pLdpSession, pLdpEntity,
                     &TempPeerAddr.Addr.Ip6Addr, u4IfIndex,
                     u1RemoteFlag) == LDP_FAILURE)
                {
                    LDP_DBG (GRACEFUL_DEBUG,
                             "LdpHandleHelloMsg: Failed to Refresh "
                             "the STALE session\n");
                }
            }
            if ((LDP_FALSE == u1IPV6HelloRcvd) &&
                (pLdpPeer->pLdpSession->SrcTransAddr.u2AddrType ==
                 LDP_ADDR_TYPE_IPV4))
#endif
            {
                MEMCPY (&u4PeerAddr, PeerAddr.Addr.au1Ipv4Addr,
                        LDP_NET_ADDR_LEN);
                u4PeerAddr = OSIX_NTOHL (u4PeerAddr);
                if (LdpRefreshStaleLdpSession
                    (pLdpPeer->pLdpSession, pLdpEntity, u1TrAddrPresent,
                     u4TrAddr, u4PeerAddr, u4IfIndex) == LDP_FAILURE)
                {
                    LDP_DBG (GRACEFUL_DEBUG,
                             "LdpHandleHelloMsg: Failed to Refresh "
                             "the STALE session\n");
                }
            }
        }
        else
        {
            /* check for change in Conf Seq number */
            if (((u1CSNumPresent == LDP_TRUE) && (pLdpPeer->u4ConfSeqNum != 0)
                 && (u4CSNumber != pLdpPeer->u4ConfSeqNum)) ||
                ((u1TrAddrPresent == LDP_TRUE)
                 && (LDP_ADDR_TYPE_IPV4 == pLdpPeer->NetAddr.u2AddrType)
                 && (u4TrAddr != u4TempNetAddr))
                || ((u1TrAddr6Present == LDP_TRUE)
                    && (LDP_ADDR_TYPE_IPV6 == pLdpPeer->NetAddr.u2AddrType)
                    &&
                    (MEMCMP
                     (&(pLdpPeer->NetAddr.Addr.Ip6Addr.u1_addr),
                      Ipv6TrAddr.u1_addr, LDP_NET_ADDR6_LEN) != 0)))
            {

                /* Received hello msg's modified CS num updated in the peer */
                pLdpPeer->u4ConfSeqNum = u4CSNumber;

                /* Re-Establish the session */
                LdpDeleteLdpSession ((tLdpSession *) pLdpPeer->pLdpSession,
                                     LDP_STAT_TYPE_SHUT_DOWN);
                u1SsnAbsent = LDP_TRUE;
            }
            /*This code is added to take care the below scenario:
               If hello is received with different TCP ADDRESS (source address, if Transport Addr Tlv is not enabled) than the pre-saved ip address
               in Peer's Netaddr when first Hello was received from the peer.
               Case: In IPV6 if Static Link Local < Auto Link Local, Then session may be initiated with Auto Link Local before LDP receives the Addr_Del 
               for Auto Link Local. In this case Passive LSR will have wrong entry about the Peer List as interface Address would have been changed to Static
               Link Local at active LSR after Addr_Del for Auto Link Local is received. */

            else if ((((tLdpSession *) pLdpPeer->pLdpSession)->
                      u4SsnTcpIfIndex == u4IfIndex)
                     &&
                     (((LDP_ADDR_TYPE_IPV6 == pLdpPeer->NetAddr.u2AddrType)
                       && (LDP_TRUE == u1IPV6HelloRcvd)
                       &&
                       (MEMCMP
                        (&(pLdpPeer->NetAddr.Addr.Ip6Addr.u1_addr),
                         LDP_IPV6_ADDR (TempPeerAddr.Addr),
                         LDP_IPV6ADR_LEN) != 0))
                      || ((LDP_ADDR_TYPE_IPV4 == pLdpPeer->NetAddr.u2AddrType)
                          && (LDP_FALSE == u1IPV6HelloRcvd)
                          &&
                          (MEMCMP
                           (LDP_IPV4_ADDR (pLdpPeer->NetAddr.Addr),
                            LDP_IPV4_ADDR (TempPeerAddr.Addr),
                            LDP_IPV4ADR_LEN) != 0))))
            {
                if (u1IPV6HelloRcvd)
                {
                    LDP_DBG2 (LDP_DBG_PRCS,
                              "PRCS: Deleting Session as received IPv6 Hello with different TCP-IP (V6) Address  TCP_IP_ADDR %s  TCP_IP_ADDR_CHNAGED %s\n",
                              Ip6PrintAddr (&(pLdpPeer->NetAddr.Addr.Ip6Addr)),
                              Ip6PrintAddr (&(TempPeerAddr.Addr.Ip6Addr)));
                }
                else
                {
                    LDP_DBG8 (LDP_DBG_PRCS,
                              "PRCS: Deleting Session as received IPv4 Hello with different TCP-IP (V4) Address  TCP_IP_ADDR %x:%x:%x:%x  TCP_IP_ADDR_CHNAGED %x:%x:%x:%x\n",
                              pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                              pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                              pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                              pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3],
                              TempPeerAddr.Addr.au1Ipv4Addr[0],
                              TempPeerAddr.Addr.au1Ipv4Addr[1],
                              TempPeerAddr.Addr.au1Ipv4Addr[2],
                              TempPeerAddr.Addr.au1Ipv4Addr[3]);
                }
                LdpDeleteLdpSession ((tLdpSession *) pLdpPeer->pLdpSession,
                                     LDP_STAT_TYPE_SHUT_DOWN);
                u1SsnAbsent = LDP_TRUE;
            }
            else
            {
                /* if no change in Configuration Sequence number, then just
                 * ReStart the PeerAdjacency's HoldTimer */
                pSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;

                /* Get the Adjacency */
                TMO_SLL_Scan (&(pSessionEntry->AdjacencyList), pLdpAdj,
                              tLdpAdjacency *)
                {
                    if (pLdpAdj->u4IfIndex == u4IfIndex
                        && pLdpAdj->u1AddrType == PeerAddr.u2AddrType)
                    {
                        break;
                    }
                }
                if ((pLdpAdj != NULL)
                    && (pLdpAdj->u4HoldTimeRem != LDP_INFINITE_HTIME))
                {

                    /* Restart PeerAdjacency's HelloHold Timer */
                    TmrStopTimer (LDP_TIMER_LIST_ID,
                                  (tTmrAppTimer *) & (pLdpAdj->PeerAdjTimer.
                                                      AppTimer));
                    TmrStartTimer (LDP_TIMER_LIST_ID,
                                   (tTmrAppTimer *) & (pLdpAdj->PeerAdjTimer.
                                                       AppTimer),
                                   (pLdpAdj->u4HoldTimeRem *
                                    SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
                }
            }
        }
#else
        /* check for change in Conf Seq number */
        /* MPLS_IPv6 mod start */
        if (((u1CSNumPresent == LDP_TRUE) && (pLdpPeer->u4ConfSeqNum != 0)
             && (u4CSNumber != pLdpPeer->u4ConfSeqNum)) ||
            ((u1TrAddrPresent == LDP_TRUE)
             && (LDP_ADDR_TYPE_IPV4 == pLdpPeer->NetAddr.u2AddrType)
             && (u4TrAddr != u4TempNetAddr)) || ((u1TrAddr6Present == LDP_TRUE)
                                                 && (LDP_ADDR_TYPE_IPV6 ==
                                                     pLdpPeer->NetAddr.
                                                     u2AddrType)
                                                 &&
                                                 (MEMCMP
                                                  (&
                                                   (pLdpPeer->NetAddr.Addr.
                                                    Ip6Addr.u1_addr),
                                                   Ipv6TrAddr.u1_addr,
                                                   LDP_NET_ADDR6_LEN) != 0)))
        {
            /* MPLS_IPv6 mod end */
            /* Received hello msg's modified CS num updated in the peer */
            pLdpPeer->u4ConfSeqNum = u4CSNumber;

            /* Re-Establish the session */
            LdpDeleteLdpSession ((tLdpSession *) pLdpPeer->pLdpSession,
                                 LDP_STAT_TYPE_SHUT_DOWN);
            u1SsnAbsent = LDP_TRUE;
        }
        else
        {
            /* if no change in Configuration Sequence number, then just
             * ReStart the PeerAdjacency's HoldTimer */
            pSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;

            /* Get the Adjacency */
            TMO_SLL_Scan (&(pSessionEntry->AdjacencyList), pLdpAdj,
                          tLdpAdjacency *)
            {
                if (pLdpAdj->u4IfIndex == u4IfIndex
                    && pLdpAdj->u1AddrType == PeerAddr.u2AddrType)
                {
                    break;
                }
            }
            if (pLdpAdj != NULL)
            {
                /* Restart PeerAdjacency's HelloHold Timer */
                if (TmrStopTimer (LDP_TIMER_LIST_ID,
                                  (tTmrAppTimer *) & (pLdpAdj->PeerAdjTimer.
                                                      AppTimer)) == TMR_FAILURE)
                {
                    LDP_DBG1 (LDP_PRCS_ALL,
                              "PRCS: Failed to stop the timer inside function %s\n",
                              __func__);
                }
                if (TmrStartTimer (LDP_TIMER_LIST_ID,
                                   (tTmrAppTimer *) & (pLdpAdj->PeerAdjTimer.
                                                       AppTimer),
                                   (pLdpAdj->u4HoldTimeRem *
                                    SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) ==
                    TMR_FAILURE)
                {
                    LDP_DBG1 (LDP_PRCS_ALL,
                              "PRCS: Failed to start the timer inside function %s\n",
                              __func__);;
                }
            }
        }
#endif
    }
    /* Increment the Hellos(Targetted/Basic) received */
    LDP_UPDATE_HELLOS_RCVD (pLdpEntity, u1RemoteFlag);

    if (u1SsnAbsent == LDP_TRUE)
    {
        if (LdpIsPeerExist (u2IncarnId, u4IfIndex, u1RemoteFlag,
                            TempPeerAddr) == LDP_TRUE)
        {
            if (u1RemoteFlag)
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "PRCS: Link Session already exists for this peer\n");
            }
            else
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "PRCS: Targeted Session already exists for this peer\n");
            }
            return LDP_FAILURE;
        }
        if (u1RemoteFlag == LDP_TRUE)
        {
            LDP_DBG (LDP_SSN_PRCS, "PRCS: Targeted session created\n");
        }
        else
        {
            LDP_DBG (LDP_SSN_PRCS, "PRCS: Link session created\n");
        }
        /* Create session With Passive Role Later It's updated */
        if (LdpCreateSession (pLdpPeer, pLdpEntity, u1SsnRole,
                              pLdpEntity->u2IncarnId) != LDP_SUCCESS)
        {
            LDP_DBG (LDP_PRCS_PRCS, "PRCS: Failed to Create New Active Ssn\n");
            return LDP_FAILURE;
        }
        /* Create Adj Entry */
        pSessionEntry = (tLdpSession *) (pLdpPeer->pLdpSession);
        if (LdpCreateAdj (pSessionEntry, u4IfIndex, u2HoldTime, PeerLdpId,
                          pLdpEntity->u2IncarnId, PeerAddr) != LDP_SUCCESS)
        {
            /* Freeing the Session block allocated previously */
            pSessionEntry->pLdpPeer->pLdpSession = NULL;
            MemReleaseMemBlock (LDP_SSN_POOL_ID, (UINT1 *) pSessionEntry);
            LDP_DBG (LDP_PRCS_PRCS, "PRCS: Failed to Create LdpAdjacency\n");
            return LDP_FAILURE;
        }
    }
    else
    {
        LDP_DBG (LDP_PRCS_PRCS,
                 "PRCS: Session Entry Already Created Updating Session Pointer \n");
        pSessionEntry = pLdpPeer->pLdpSession;
    }
    if (LDP_SSN_TCP_NON_INIT == pSessionEntry->u1SessionInitStatus)
    {
        LDP_DBG1 (LDP_SSN_PRCS, " LdpHH TransAddrIntfUp (0 Down:: 1 UP): %x",
                  pLdpEntity->bIsTransAddrIntfUp);
#ifdef MPLS_IPV6_WANTED
        if (LDP_TRUE == u1IPV6HelloRcvd)
        {
            if ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) != LDP_TRUE)
                || (pLdpEntity->bIsIpv6TransAddrIntfUp == LDP_FALSE)
                ||
                ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) ==
                  LDP_TRUE)
                 && (LDP_ENTITY_IPV6_TRANSPORT_ADDRKIND (pLdpEntity) ==
                     INTERFACE_ADDRESS_TYPE)))
            {
                if (LdpGetLdpEntityIfEntry (LDP_CUR_INCARN, pLdpEntity,
                                            u4IfIndex,
                                            &pLdpIfTableEntry) == LDP_FAILURE)
                {
                    LDP_DBG (LDP_PRCS_PRCS,
                             "Trgd Hello Rcvd in Wrong Interface \n");
                    return LDP_FAILURE;
                }
                MEMCPY (Ipv6TempLocalAddr.u1_addr,
                        pLdpIfTableEntry->LnklocalIpv6Addr.u1_addr,
                        LDP_IPV6ADR_LEN);
                LDP_DBG (LDP_SSN_PRCS,
                         "V6 Hello Rcvd: Link Local Addr configured may be used as SrcTransAddress \n");
                if ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) ==
                     LDP_TRUE)
                    && (LDP_ENTITY_IPV6TRANSPORT_ADDRKIND (pLdpEntity) ==
                        INTERFACE_ADDRESS_TYPE))
                {
                    /* In Basic Entity, If TransAddr as INTERFACE is enabled, 
                     * Session should be created with Link Local IfAddr */
                }

                if (u1RemoteFlag)
                {
                    /* Target Hello Rcvd: Tcp Connection should be established bw  Glbl Unicast Addresses */
                    if (LDP_ZERO ==
                        MEMCMP (pLdpIfTableEntry->GlbUniqueIpv6Addr.u1_addr,
                                V6TempZeroAddr.u1_addr, LDP_IPV6ADR_LEN))
                    {
                        LDP_DBG (LDP_SSN_PRCS,
                                 "Tgt V6 Hello Rcvd: Glbl Unicast Addr is not configured in IfTableEntry Hello Discarded\n");
                        return LDP_FAILURE;
                    }
                    MEMCPY (Ipv6TempLocalAddr.u1_addr,
                            pLdpIfTableEntry->GlbUniqueIpv6Addr.u1_addr,
                            LDP_IPV6ADR_LEN);
                }

                if (LDP_ZERO ==
                    MEMCMP (Ipv6TempLocalAddr.u1_addr, V6TempZeroAddr.u1_addr,
                            LDP_IPV6ADR_LEN))
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "Case: Unnumbered Interfaces Not Supported forIPV6 Return Handle Hello\n");
                    return LDP_FAILURE;
                }
            }
            else
            {
                MEMCPY (Ipv6TempLocalAddr.u1_addr,
                        LDP_ENTITY_V6TRANSPORT_ADDRESS (pLdpEntity),
                        LDP_IPV6ADR_LEN);
            }
            u1SsnRole =
                LdpGetV6SsnRole (pLdpPeer, TempPeerAddr, Ipv6TempLocalAddr,
                                 u1RemoteFlag);
            pSessionEntry->u1SessionRole = u1SsnRole;
            MEMCPY (LDP_IPV6_ADDR (pSessionEntry->SrcTransAddr.Addr),
                    Ipv6TempLocalAddr.u1_addr, LDP_IPV6ADR_LEN);
            pSessionEntry->SrcTransAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
            pSessionEntry->u4SsnTcpIfIndex = u4IfIndex;
            if (LDP_IPV6_SSN_PREF_TMR_STARTED == pLdpPeer->u1SsnPrefTmrStatus)
            {
                TmrStopTimer (LDP_TIMER_LIST_ID,
                              (tTmrAppTimer *) & (pLdpPeer->Ipv6SessPrefTimer.
                                                  AppTimer));
                pLdpPeer->u1SsnPrefTmrStatus = LDP_IPV6_SSN_PREF_TMR_STOPPED;
                LDP_DBG (LDP_SSN_PRCS,
                         "PRCS: IPV6 Pref Timer Stopped on receiving IPV6 Hello\n");
            }
            LDP_DBG1 (LDP_SSN_PRCS, "PRCS: Source T Addr updated: %s\n",
                      Ip6PrintAddr (&
                                    (pSessionEntry->SrcTransAddr.Addr.
                                     Ip6Addr)));
            LDP_DBG1 (LDP_SSN_PRCS, "PRCS: Peer T Addr updated: %s\n",
                      Ip6PrintAddr (&(pLdpPeer->TransAddr.Addr.Ip6Addr)));
            if (LDP_FAILURE ==
                LdpNeInitTcpConn (pSessionEntry, u1SsnRole, u2IncarnId))
            {
                LDP_DBG (LDP_SSN_PRCS, "PRCS: LdpNeInitTcpConn Failed\n");
                return LDP_FAILURE;
            }
        }
        else
        {
            if (LDP_TRUE == IsEntityDualStack (pLdpEntity)
                && LDP_TRUE != u1RemoteFlag)
            {
                if (LDP_IPV6_SSN_PREF_TMR_NSTARTED ==
                    pLdpPeer->u1SsnPrefTmrStatus)
                {
                    if ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) !=
                         LDP_TRUE)
                        || (pLdpEntity->bIsTransAddrIntfUp == LDP_FALSE)
                        ||
                        ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) ==
                          LDP_TRUE)
                         && (LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) ==
                             INTERFACE_ADDRESS_TYPE)))
                    {
                        if (LdpGetLdpEntityIfEntry (LDP_CUR_INCARN, pLdpEntity,
                                                    u4IfIndex,
                                                    &pLdpIfTableEntry) ==
                            LDP_FAILURE)
                        {
                            LDP_DBG (LDP_SSN_PRCS,
                                     "Trgd Hello Rcvd in Wrong Interface \n");
                            return LDP_FAILURE;
                        }
                        CONVERT_TO_INTEGER (pLdpIfTableEntry->IfAddr,
                                            u4TempLocalAddr);
                        u4TempLocalAddr = OSIX_NTOHL (u4TempLocalAddr);
                        if (u4TempLocalAddr == LDP_ZERO)
                        {
                            CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4AssocIf);
                            CfaGetIfIpAddr ((INT4) u4AssocIf, &u4TempLocalAddr);
                        }
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "LdpHHMsg: TRANS ADD Not PRESENT TempLocalAddr %x at %d\n",
                                  u4TempLocalAddr, __LINE__);
                    }
                    else
                    {
                        u4TempLocalAddr =
                            LDP_ENTITY_TRANSPORT_ADDRESS (pLdpEntity);
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "LdpHHMsg: TRANS ADD PRESENT  TempLocalAddr: %x at %d\n",
                                  u4TempLocalAddr, __LINE__);
                    }
                    u1SsnRole =
                        LdpGetV4SsnRole (pLdpPeer, TempPeerAddr,
                                         u4TempLocalAddr, u1RemoteFlag);
                    pSessionEntry->u1SessionRole = u1SsnRole;
                    u4TempLocalAddr = OSIX_NTOHL (u4TempLocalAddr);
                    MEMCPY (LDP_IPV4_ADDR (pSessionEntry->SrcTransAddr.Addr),
                            &u4TempLocalAddr, LDP_IPV4ADR_LEN);
                    pSessionEntry->SrcTransAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
                    LDP_DBG5 (LDP_SSN_PRCS,
                              "LdpHHMsg SOURCE TRANSPORT ADDRES: %x:%x:%x:%x at %d",
                              pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[0],
                              pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[1],
                              pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[2],
                              pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[3],
                              __LINE__);
                    pLdpPeer->Ipv6SessPrefTimer.u4Event =
                        LDP_IPV6_SSN_PREF_TMR_EVT;
                    pLdpPeer->Ipv6SessPrefTimer.pu1EventInfo =
                        (UINT1 *) pLdpPeer;
                    TmrStartTimer (LDP_TIMER_LIST_ID,
                                   (tTmrAppTimer *) & (pLdpPeer->
                                                       Ipv6SessPrefTimer.
                                                       AppTimer),
                                   (LDP_IPV6_SSN_PREF_TIME *
                                    SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
                    pLdpPeer->u1SsnPrefTmrStatus =
                        LDP_IPV6_SSN_PREF_TMR_STARTED;
                    LDP_DBG1 (LDP_SSN_PRCS,
                              "PRCS: Dual Stack Entity IPV6 Pref Timer Started on receiving IPV4 Hello with SSN ROLE: %d\n",
                              u1SsnRole);
                }
                /*Case: V6 Ssn Pref Tmr has already started once for this peer, So not Starting timer again */
                if (LDP_IPV6_SSN_PREF_TMR_EXPIRED ==
                    pLdpPeer->u1SsnPrefTmrStatus
                    || LDP_IPV6_SSN_PREF_TMR_STOPPED ==
                    pLdpPeer->u1SsnPrefTmrStatus)
                {
                    if ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) !=
                         LDP_TRUE)
                        || (pLdpEntity->bIsTransAddrIntfUp == LDP_FALSE)
                        ||
                        ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) ==
                          LDP_TRUE)
                         && (LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) ==
                             INTERFACE_ADDRESS_TYPE)))
                    {
                        if (LdpGetLdpEntityIfEntry (LDP_CUR_INCARN, pLdpEntity,
                                                    u4IfIndex,
                                                    &pLdpIfTableEntry) ==
                            LDP_FAILURE)
                        {
                            LDP_DBG (LDP_SSN_PRCS,
                                     "Trgd Hello Rcvd in Wrong Interface \n");
                            return LDP_FAILURE;
                        }
                        CONVERT_TO_INTEGER (pLdpIfTableEntry->IfAddr,
                                            u4TempLocalAddr);
                        u4TempLocalAddr = OSIX_NTOHL (u4TempLocalAddr);
                        if (u4TempLocalAddr == LDP_ZERO)
                        {
                            CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4AssocIf);
                            CfaGetIfIpAddr ((INT4) u4AssocIf, &u4TempLocalAddr);
                        }
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "LdpHHMsg: TRANS ADD Not PRESENT TempLocalAddr %x at %d\n",
                                  u4TempLocalAddr, __LINE__);
                    }
                    else
                    {
                        u4TempLocalAddr =
                            LDP_ENTITY_TRANSPORT_ADDRESS (pLdpEntity);
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "LdpHHMsg: TRANS ADD PRESENT  TempLocalAddr: %x at %d\n",
                                  u4TempLocalAddr, __LINE__);
                    }
                    u1SsnRole =
                        LdpGetV4SsnRole (pLdpPeer, TempPeerAddr,
                                         u4TempLocalAddr, u1RemoteFlag);
                    pSessionEntry->u1SessionRole = u1SsnRole;
                    u4TempLocalAddr = OSIX_NTOHL (u4TempLocalAddr);
                    MEMCPY (LDP_IPV4_ADDR (pSessionEntry->SrcTransAddr.Addr),
                            &u4TempLocalAddr, LDP_IPV4ADR_LEN);
                    pSessionEntry->SrcTransAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
                    LDP_DBG5 (LDP_SSN_PRCS,
                              "LdpHHMsg SOURCE TRANSPORT ADDRES: %x:%x:%x:%x at %d",
                              pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[0],
                              pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[1],
                              pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[2],
                              pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[3],
                              __LINE__);
                    LDP_DBG1 (LDP_SSN_PRCS,
                              "PRCS: Dual Stack Entity IPV6 Pref Timer Not Started on receiving IPV4 Hello with SSN ROLE: %d\n",
                              u1SsnRole);
                    if (LDP_FAILURE ==
                        LdpNeInitTcpConn (pSessionEntry, u1SsnRole, u2IncarnId))
                    {
                        LDP_DBG (LDP_SSN_PRCS,
                                 "PRCS: LdpNeInitTcpConn Failed\n");
                        return LDP_FAILURE;
                    }
                }
            }
            else
            {
#endif
                if ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) != LDP_TRUE)
                    || (pLdpEntity->bIsTransAddrIntfUp == LDP_FALSE)
                    ||
                    ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) == LDP_TRUE)
                     && (LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) ==
                         INTERFACE_ADDRESS_TYPE)))
                {
                    if (LdpGetLdpEntityIfEntry (LDP_CUR_INCARN, pLdpEntity,
                                                u4IfIndex,
                                                &pLdpIfTableEntry) ==
                        LDP_FAILURE)
                    {
                        LDP_DBG (LDP_PRCS_PRCS,
                                 "Trgd Hello Rcvd in Wrong Interface \n");
                        return LDP_FAILURE;
                    }
                    CONVERT_TO_INTEGER (pLdpIfTableEntry->IfAddr,
                                        u4TempLocalAddr);
                    u4TempLocalAddr = OSIX_NTOHL (u4TempLocalAddr);
                    if (u4TempLocalAddr == LDP_ZERO)
                    {
                        CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4AssocIf);
                        CfaGetIfIpAddr ((INT4) u4AssocIf, &u4TempLocalAddr);
                    }
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "LdpHHMsg: TRANS ADD Not PRESENT TempLocalAddr %x at %d\n",
                              u4TempLocalAddr, __LINE__);
                }
                else
                {
                    u4TempLocalAddr = LDP_ENTITY_TRANSPORT_ADDRESS (pLdpEntity);
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "LdpHHMsg: TRANS ADD PRESENT  TempLocalAddr: %x at %d\n",
                              u4TempLocalAddr, __LINE__);
                }
                u1SsnRole =
                    LdpGetV4SsnRole (pLdpPeer, TempPeerAddr, u4TempLocalAddr,
                                     u1RemoteFlag);
                pSessionEntry->u1SessionRole = u1SsnRole;
                u4TempLocalAddr = OSIX_NTOHL (u4TempLocalAddr);
                MEMCPY (LDP_IPV4_ADDR (pSessionEntry->SrcTransAddr.Addr),
                        &u4TempLocalAddr, LDP_IPV4ADR_LEN);
                pSessionEntry->SrcTransAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
                pSessionEntry->u4SsnTcpIfIndex = u4IfIndex;
                LDP_DBG5 (LDP_SSN_PRCS,
                          "LdpHHMsg SOURCE TRANSPORT ADDRES: %x:%x:%x:%x at %d",
                          pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[0],
                          pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[1],
                          pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[2],
                          pSessionEntry->SrcTransAddr.Addr.au1Ipv4Addr[3],
                          __LINE__);
                LDP_DBG1 (LDP_SSN_PRCS,
                          "Entity is not Dual Stack InitTcpConn Called with session role %x\n",
                          u1SsnRole);
                if (LDP_FAILURE ==
                    LdpNeInitTcpConn (pSessionEntry, u1SsnRole, u2IncarnId))
                {
                    LDP_DBG (LDP_SSN_PRCS, "PRCS: LdpNeInitTcpConn Failed\n");
                    return LDP_FAILURE;
                }
#ifdef MPLS_IPV6_WANTED
            }
        }
#endif
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleUdpPkt                                           */
/* Description   : Routine to handle LDP PDU.                                */
/*                 This routine is called whenever the Interface Module      */
/*                 receives UDP Packet Arrival Event, from UDP.              */
/* Input(s)      : pPDU       - Points to the Ldp Pdu that is received.      */
/*                 u4IfIndex  - Iface Index over which Ldp Pdu is received.  */
/*                 u4PeerAddr - Source Addr of Peer from which the Hello Pkt */
/*                              is Rx. This Addr is assigned to Peer's Ntw   */
/*                              Addr, when TrAddrTlv is not present in Hello.*/
/*                 u2IncarnId - Incarnation Id                               */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

UINT1
LdpHandleUdpPkt (tCRU_BUF_CHAIN_HEADER * pPDU, UINT4 u4Port,
                 UINT4 u4PeerAddr, UINT2 u2IncarnId)
{
    UINT1              *pMsg = NULL;
    UINT1               u1StatusType = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2Offset = 0;
    UINT2               u2MsgType = 0;
    UINT2               u2MsgLen = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4DbgMsgDir = 0;
    tLdpPduHdr          PduHdr;
    tLdpMsgHdrAndId     MsgHdr;    /* will be accessing only the msg hdr part */
    /* MPLS_IPv6 mod start */
    tGenAddr            PeerAddr;
    MEMSET (&PeerAddr, 0, sizeof (tGenAddr));
    PeerAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
    u4PeerAddr = OSIX_NTOHL (u4PeerAddr);
    MEMCPY (PeerAddr.Addr.au1Ipv4Addr, (UINT1 *) &u4PeerAddr, LDP_NET_ADDR_LEN);
    /* MPLS_IPv6 mod end */
    u2BufSize = (UINT2) CRU_BUF_Get_ChainValidByteCount (pPDU);
    MEMSET (&PduHdr, 0, sizeof (tLdpPduHdr));
    MEMSET (&MsgHdr, 0, sizeof (tLdpMsgHdrAndId));

    if (((gu4LdpDumpType & LDP_DUMP_HELLO) == LDP_DUMP_HELLO) &&
        ((gu4LdpDumpDir == DUMP_DIR_INOUT)
         || (gu4LdpDumpDir == LDP_DUMP_DIR_IN)))
    {
        i4DbgMsgDir = LDP_DUMP_DIR_IN;

        LdpDumpLdpPdu (pPDU, i4DbgMsgDir);
    }

    /* LDP Table Entries are created using CFA Index But recvmsg
     * will give IP index so, we are getting CFA Index from IP Index */
    if (NetIpv4GetCfaIfIndexFromPort (u4Port, &u4IfIndex) == NETIPV4_FAILURE)
    {
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "PRCS: Port Index Mapping Failed Port =%d \n", u4Port);
        return LDP_FAILURE;

    }

    /* Copy the Pdu Hdr to PduHdr Structure */
    if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &PduHdr, 0, LDP_PDU_HDR_LEN)
        != LDP_PDU_HDR_LEN)
    {
        LDP_DBG (LDP_PRCS_MEM,
                 "PRCS: Error while copying Pdu Hdr from chain Buf\n");
        return LDP_FAILURE;
    }

    if (LdpValidatePduHdr (&PduHdr, NULL, u2BufSize, &u1StatusType) !=
        LDP_SUCCESS)
    {
        /* Error: Received PDU with invalid Header */
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: Rx Pdu with Invalid Header \n");
        return LDP_FAILURE;
    }

    u2BufSize -= (UINT2) LDP_PDU_HDR_LEN;
    u2Offset = LDP_PDU_HDR_LEN;

    /* Copy the Msg hdr from buffer */
    if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &MsgHdr, u2Offset,
                                   (UINT4) LDP_MSG_HDR_LEN) != LDP_MSG_HDR_LEN)
    {
        LDP_DBG (LDP_PRCS_MEM,
                 "PRCS: Error copying Msg Hdr from the chain Buf\n");
        return LDP_FAILURE;
    }

    u2MsgType = OSIX_NTOHS (MsgHdr.u2MsgType);
    u2MsgLen = OSIX_NTOHS (MsgHdr.u2MsgLen);

    if (u2MsgType != LDP_HELLO_MSG)
    {
        LDP_DBG (LDP_PRCS_PRCS,
                 "PRCS: Unknown Msg in Udp Pkt. Stop Processing \n");
        return LDP_FAILURE;
    }

    if (LdpValidateMsgHdr (u2BufSize, u2MsgLen, u2MsgType,
                           &u1StatusType) != LDP_SUCCESS)
    {
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: Rx Pdu with invalid Msg header \n ");
        return LDP_FAILURE;
    }

    pMsg = (UINT1 *) MemAllocMemBlk (LDP_MSG_POOL_ID);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_PRCS_MEM, "PRCS: Mem Alloc from Msg MemPool Failed\n");
        return LDP_FAILURE;
    }
    /* Copy one msg from the chain to linear buffer */
    if (CRU_BUF_Copy_FromBufChain (pPDU, pMsg, u2Offset,
                                   (UINT4) (u2MsgLen + LDP_MSG_HDR_LEN)) !=
        (u2MsgLen + LDP_MSG_HDR_LEN))
    {
        LDP_DBG (LDP_PRCS_MEM,
                 "PRCS: Error copying from chain Buf to linear Blk\n");
        MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
        return LDP_FAILURE;
    }

    LdpHandleHelloMsg (pMsg, PduHdr.LdpId, PeerAddr, u4IfIndex, u2IncarnId,
                       LDP_FALSE);
    MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleTcpPkt                                           */
/* Description   : Routine to handle LDP PDU.                                */
/*                 This routine is called whenever the Interface Module      */
/*                 receives a TCP Packet Arrival Event, from TCP.            */
/*                 It extracts all the messages (normally one message) that  */
/*                 are present in the LDP PDU, examines the message type of  */
/*                 each message and calls the appropriate Session Handling   */
/*                 routines.                                                 */
/* Input(s)      : pPDU       - Points to the Ldp Pdu that is received.      */
/*                 u4TcpConnId - Tcp connection Id on which this Ldp Pdu is  */
/*                               received.                                   */
/*                 u2IncarnId - Incarnatin Id                                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpHandleTcpPkt (tCRU_BUF_CHAIN_HEADER * pPDU, UINT4 u4TcpConnId,
                 UINT4 u4PeerAddr, UINT2 u2IncarnId)
{
    UINT1              *pMsg = NULL;
    UINT1               u1StatusType = 0;
    UINT1               u1SsnState = 0;
    UINT1               u1SsnFsmEvent = LDP_MAX_SESSION_EVENTS;    /* Valid 
                                                                   FsmEvent vals
                                                                   are upto
                                                                   LDP_MAX_SESSI
                                                                   ON_EVENTS - 1
                                                                 */
    UINT2               u2Len = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2Offset = 0;
    UINT2               u2MsgType = 0;
    UINT2               u2InitMsgType = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2PduLen = 0;
    UINT1               u1InitMsgFlag = LDP_FALSE;
    UINT1               u1CmnSsnTlvFlag = LDP_FALSE;
    UINT1               u1PeerIndex;
    INT4                i4DbgMsgDir = 0;
    tLdpId              RcvrLdpId;
    tLdpPduHdr          PduHdr;
    tLdpMsgHdrAndId     MsgHdr;    /* will be accessing only the msg hdr part */
    tLdpSession        *pSessionEntry = NULL;
    tLdpPeer           *pTmpPeer = NULL;
    UINT4               u4RelinquishCtr = 0;
    UINT4               u4TempPeerAddr = LDP_ZERO;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    MEMSET (&MsgHdr, 0, sizeof (tLdpMsgHdrAndId));
    MEMSET (&PduHdr, 0, sizeof (tLdpPduHdr));

    if ((gu4LdpDumpType & ~(UINT4) (LDP_DUMP_HELLO)) &&
        ((gu4LdpDumpDir == DUMP_DIR_INOUT)
         || (gu4LdpDumpDir == LDP_DUMP_DIR_IN)))
    {
        i4DbgMsgDir = LDP_DUMP_DIR_IN;
        LdpDumpLdpPdu (pPDU, i4DbgMsgDir);
    }

    u2BufSize = (UINT2) CRU_BUF_Get_ChainValidByteCount (pPDU);
    LDP_DBG (LDP_PRCS_PRCS, "PRCS: Processing LDP PDU Rx on TCP.\n");

    /* 
     * Invoked to dump LDP_DEF_MAX_PDU_LEN Length.  Dump should contain the
     * rcvd LDP PDU contents, which will be less than the LDP_DEF_MAX_PDU_LEN
     * length.
     */
    /* Copy the Pdu Hdr to PduHdr Structure */
    if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &PduHdr, 0, LDP_PDU_HDR_LEN)
        != LDP_PDU_HDR_LEN)
    {
        LDP_DBG (LDP_PRCS_MEM,
                 "PRCS: Error copying Pdu Hdr from chain buffer\n");
        return LDP_FAILURE;
    }
    pSessionEntry = LdpGetSsnFromTcpConnTbl (u2IncarnId, u4TcpConnId);
    if (pSessionEntry == NULL)
    {
        if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &u2InitMsgType,
                                       LDP_PDU_HDR_LEN, LDP_MSG_TYPE_LEN) ==
            LDP_MSG_TYPE_LEN)
        {
            if (OSIX_NTOHS (u2InitMsgType) == LDP_INIT_MSG)
            {
                u1InitMsgFlag = LDP_TRUE;
                MEMCPY (au1IpAddr, (UINT1 *) &u4PeerAddr, ROUTER_ID_LENGTH);

                LDP_DBG4 (LDP_SSN_RX,
                          "SESSION: Rcvd Init Msg from %d.%d.%d.%d \n",
                          au1IpAddr[0], au1IpAddr[1], au1IpAddr[2],
                          au1IpAddr[3]);
            }
        }
        else
        {
            LDP_DBG (LDP_PRCS_MEM,
                     "PRCS: Error in Copying InitMsgType From Buffer\n");
            return LDP_FAILURE;
        }

        if (u1InitMsgFlag == LDP_TRUE)
        {
            u2PduLen = OSIX_NTOHS (PduHdr.u2PduLen);
            if ((u2BufSize < LDP_MIN_PDU_LEN)
                || (u2BufSize > LDP_DEF_MAX_PDU_LEN) || (u2PduLen == 0)
                || (u2PduLen < LDP_MIN_PDU_LEN)
                || (u2PduLen > LDP_DEF_MAX_PDU_LEN))
            {
                u1StatusType = LDP_STAT_TYPE_BAD_PDU_LEN;
            }

            /* Check if recvd msg contains mandatory Cmn Ssn Params Tlv */
            if ((u1StatusType == LDP_ZERO) &&
                (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &u2TlvType,
                                            (LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                             LDP_MSG_ID_LEN),
                                            LDP_TLV_TYPE_LEN) ==
                 LDP_TLV_TYPE_LEN))
            {
                if (OSIX_NTOHS (u2TlvType) == LDP_CMN_SSN_PARAM_TLV)
                {
                    u1CmnSsnTlvFlag = LDP_TRUE;
                }
            }

            /* Get Receiver's LdpId from the received PDU carrying init msg */
            if ((CRU_BUF_Copy_FromBufChain (pPDU, RcvrLdpId, RCVR_LDPID_OFFSET,
                                            LDP_MAX_LDPID_LEN) !=
                 LDP_MAX_LDPID_LEN) &&
                (u1StatusType == LDP_ZERO) && (u1CmnSsnTlvFlag == LDP_TRUE))
            {
                u1StatusType = LDP_STAT_TYPE_MALFORMED_TLV_VAL;
                LDP_DBG (LDP_PRCS_MEM,
                         "PRCS: Error in Copying RcvrLdpId From Buffer. TLV is malformed\n");
            }

            for (u1PeerIndex = 0; u1PeerIndex < LDP_MAX_PASSIVE_PEERS;
                 u1PeerIndex++)
            {
                pTmpPeer = LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex);
                if ((pTmpPeer == NULL) || (pTmpPeer->pLdpSession == NULL))
                {
                    continue;
                }

                pSessionEntry = (tLdpSession *) pTmpPeer->pLdpSession;

                MEMCPY ((UINT1 *) &u4TempPeerAddr,
                        (UINT1 *) &LDP_PEER_TRANS_ADDR (pTmpPeer),
                        LDP_NET_ADDR_LEN);
                if ((u1StatusType != LDP_ZERO) &&
                    (u4TempPeerAddr == u4PeerAddr))
                {
                    /* Status type is already set as MALFORMED_TLV or
                     * BAD_PDU_LEN. So, No need to handle anything here */
                }
                else if ((u1CmnSsnTlvFlag != LDP_TRUE) &&
                         (u4TempPeerAddr == u4PeerAddr) &&
                         !(MEMCMP (pTmpPeer->PeerLdpId, PduHdr.LdpId,
                                   LDP_MAX_LDPID_LEN)))
                {
                    /* Common session params is missing in Init msg */
                    u1StatusType = LDP_STAT_MISSING_MSG_PARAM;
                    LDP_DBG (LDP_PRCS_PRCS,
                             "PRCS: In PassiveMode, Rx InitMsg with No"
                             " CmnSsnParamsTlv\n");
                }
                else if ((u4TempPeerAddr == u4PeerAddr) &&
                         (MEMCMP (pTmpPeer->PeerLdpId,
                                  PduHdr.LdpId, LDP_MAX_LDPID_LEN)))
                {
                    u1StatusType = LDP_STAT_TYPE_BAD_LDPID;
                    LDP_DBG (LDP_PRCS_PRCS, "PRCS: Bad LDP Identifier \n");
                }
                else if ((u4TempPeerAddr == u4PeerAddr) &&
                         (MEMCMP (RcvrLdpId, pTmpPeer->pLdpEntity->LdpId,
                                  LDP_MAX_LDPID_LEN)))
                {
                    /* the Ldp id received in Hello is different than the one 
                       received in INIT Header msg */
                    u1StatusType = LDP_STAT_TYPE_NO_HELLO;
                    LDP_DBG (LDP_PRCS_PRCS,
                             "PRCS: No Hello received for this LDP Identifieri \n");
                }

                if (u1StatusType != 0)
                {
                    pSessionEntry->u4TcpConnId = u4TcpConnId;

                    if (u1StatusType == LDP_STAT_MISSING_MSG_PARAM)
                    {
                        LdpSendNotifMsg (pSessionEntry,
                                         (UINT1 *) &PduHdr, LDP_FALSE,
                                         u1StatusType, NULL);
                        LDP_DBG (LDP_PRCS_PRCS,
                                 "PRCS: CmnSsnParams TLV missing\n");
                    }
                    else if (LdpSsmSendNotifCloseSsn (pSessionEntry,
                                                      u1StatusType,
                                                      (UINT1 *) &PduHdr,
                                                      LDP_TRUE) != LDP_SUCCESS)
                    {
                        LDP_DBG (LDP_SSN_PRCS,
                                 "SESSION: Failed to send Notif Msg & close "
                                 "Session.\n");
                    }

                    return LDP_FAILURE;
                }
                else if (!(MEMCMP (pTmpPeer->PeerLdpId,
                                   PduHdr.LdpId, LDP_MAX_LDPID_LEN)) &&
                         !(MEMCMP (RcvrLdpId,
                                   pTmpPeer->pLdpEntity->LdpId,
                                   LDP_MAX_LDPID_LEN))
                         && (u4TempPeerAddr == u4PeerAddr))
                {
                    pSessionEntry->u1SessionState = LDP_SSM_ST_INITIALIZED;
                    pSessionEntry->u4TcpConnId = u4TcpConnId;
                    MplsLdpSessionUpdateSysTime (pSessionEntry);
                    LdpAddSsnToTcpConnTbl (pSessionEntry);
                    /* Removing the PassivePeer Entry from the Incarn */
                    LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex) = NULL;

                    break;
                }
            }

            if ((u1PeerIndex == LDP_MAX_PASSIVE_PEERS) &&
                (TMO_SLL_Count (&LDP_ENTITY_LIST (u2IncarnId)) != 0))
            {
                /* Socket should not be closed when ISS MPLS LDP is used as a
                 * test framework. If it is used as test framework, no entities are
                 * created. That's why the above check is added */
                LDP_DBG (LDP_SSN_PRCS,
                         "PRCS:Session not found. InitMsg not processed \n ");
                SockClose (u4TcpConnId);
                return LDP_FAILURE;
            }
        }
        else if (TMO_SLL_Count (&LDP_ENTITY_LIST (u2IncarnId)) != 0)
        {
            /* Socket should not be closed when ISS MPLS LDP is used as a
             * test framework. If it is used as test framework, no entities are
             * created. That's why the above check is added */
            SockClose (u4TcpConnId);
        }
    }
    if (pSessionEntry == NULL)
    {
        LDP_DBG (LDP_SSN_PRCS, "PRCS: Session not found.Pdu not processed \n ");
        return LDP_FAILURE;
    }

    if (LdpValidatePduHdr (&PduHdr, pSessionEntry, u2BufSize, &u1StatusType) !=
        LDP_SUCCESS)
    {
        /* Error: Received PDU with invalid Header */
        if (LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                     (UINT1 *) &PduHdr,
                                     LDP_TRUE) != LDP_SUCCESS)
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Failed to send Notif Msg & close " "Session.\n");

        }

        return LDP_FAILURE;
    }

    u2BufSize -= (UINT2) LDP_PDU_HDR_LEN;
    u2Offset = (UINT2) LDP_PDU_HDR_LEN;
    /* Extract Messages from the Buffer and store them on Linear buffers */
    for (u2Len = 0; u2Len < u2BufSize; u2Offset += (u2MsgLen + LDP_MSG_HDR_LEN))
    {
        /* Copy the Msg hdr from buffer */
        if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &MsgHdr, u2Offset,
                                       LDP_MSG_HDR_LEN) != LDP_MSG_HDR_LEN)
        {
            LDP_DBG (LDP_PRCS_MEM, "PRCS: Error copying MsgHdr from Buffer\n");
            return LDP_FAILURE;
        }

        u2MsgType = OSIX_NTOHS (MsgHdr.u2MsgType);
        u2MsgLen = OSIX_NTOHS (MsgHdr.u2MsgLen);
        if (LdpValidateMsgHdr ((UINT2) (u2BufSize - u2Len), u2MsgLen, u2MsgType,
                               &u1StatusType) != LDP_SUCCESS)
        {
            if (u1StatusType == LDP_STAT_TYPE_BAD_MSG_LEN)
            {
                /* Error: Received message with invalid Header */
                LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                         NULL, LDP_TRUE);
                return LDP_FAILURE;
            }
            else if (u1StatusType == LDP_STAT_TYPE_UNKNOWN_MSG_TYPE)
            {
                LdpSendNotifMsg (pSessionEntry, NULL, LDP_FALSE,
                                 u1StatusType, NULL);
                break;            /* Stop processing of PDU */
            }
            else if (u1StatusType == LDP_IGNORE_ERROR)
            {
                break;
            }
            else
            {
                u2Len += (UINT2) (u2MsgLen + LDP_MSG_HDR_LEN);    /* Go to next msg 
                                                                   in the buffer */
                continue;
            }
        }

        pMsg = (UINT1 *) MemAllocMemBlk (LDP_MSG_POOL_ID);
        if (pMsg == NULL)
        {
            LDP_DBG (LDP_PRCS_MEM, "PRCS: Mem Alloc from Msg MemPool Failed\n");
            return LDP_FAILURE;
        }

        /* Copy one msg from the chain to linear buffer */
        if (CRU_BUF_Copy_FromBufChain (pPDU, pMsg, u2Offset,
                                       (UINT4) (u2MsgLen + LDP_MSG_HDR_LEN)) !=
            (u2MsgLen + LDP_MSG_HDR_LEN))
        {
            LDP_DBG (LDP_PRCS_MEM,
                     "PRCS: Error copying Msg from Buf to LinearBlk\n");
            MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
            return LDP_FAILURE;
        }

        if (LdpValidateTlvHdr (pMsg, u2MsgLen, &u1StatusType) != LDP_SUCCESS)
        {
            if (u1StatusType == LDP_STAT_TYPE_UNKNOWN_TLV)
            {
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 u1StatusType, NULL);
            }
            else if (u1StatusType == LDP_STAT_BAD_TLV_LEN)
            {
                LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                         NULL, LDP_TRUE);
            }
            MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
            return LDP_FAILURE;
        }

        SSN_GET_SSN_STATE (pSessionEntry, u1SsnState);
        LdpGetSsnStateEvent (u2MsgType, &u1SsnFsmEvent);
        if (u1SsnFsmEvent < LDP_MAX_SESSION_EVENTS)
        {
            LDP_DBG2 (LDP_PRCS_SEM, "PRCS: SsnState  %s   Event  %s \n",
                      au1SsnState[u1SsnState], au1SsnEvents[u1SsnFsmEvent]);
        }
        LDP_SESSION_FSM[u1SsnState] (pSessionEntry, u1SsnFsmEvent, pMsg);
        MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
        u2Len += (UINT2) (u2MsgLen + LDP_MSG_HDR_LEN);    /* Go to next msg in 
                                                           the buffer */
        u4RelinquishCtr++;

        if (u4RelinquishCtr > LDP_RELINQUISH_CNTR)
        {
            LDP_DBG1 (LDP_IF_RX,
                      "LDPTCP: More than %d messages processed\n",
                      LDP_RELINQUISH_CNTR);

            u4RelinquishCtr = 0;

            /*Process the Timer Expiry Events */
            LdpProcessTimerEvents ();

            /*Process the Udp Messages */
            LdpProcessUdpMsgs ();
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpValidatePduHdr                                         */
/* Description   : Validates the Header of the received PDU for              */
/*                     - Version                                             */
/*                     - LDP-ID                                              */
/*                     - PDU Length                                          */
/*                                                                           */
/* Input(s)      : pPduHdr - points to the received LDP PDU Header.          */
/*                 pSessionEntry - Points to the session on which this PDU   */
/*                                 is received.                              */
/*                 u2BufSize - Size of the buffer containing the LDP PDU.    */
/* Outputs   : pu1StatusType - Status is stored in this field, in case of    */
/*                             invalid Pdu Header. Holds                     */
/*         LDP_STAT_TYPE_BAD_LDPID - if the LDPID in the header is not known */
/*                                   or it is known but is not the one that  */
/*                                   is associated with this session.        */
/*         LDP_STAT_TYPE_BAD_PROTO_VER   - if the Protocol version is not    */
/*                                         a valid one, or not the one       */
/*                                         negotiated during Initialisation. */
/*          LDP_STAT_TYPE_BAD_PDU_LEN- if the actual size of the buffer and  */
/*                                     PDU length are mismatching.           */
/*         LDP_STAT_TYPE_BAD_LDPID - if the LDPID in the header is not known */
/* Return(s)     : LDP_SUCCESS  -  if the Header passed is a valid one, else */
/*                 LDP_FAILURE                                               */
/*****************************************************************************/

UINT1
LdpValidatePduHdr (tLdpPduHdr * pPduHdr, tLdpSession * pSessionEntry,
                   UINT2 u2BufSize, UINT1 *pu1StatusType)
{
    UINT2               u2Version = OSIX_NTOHS (pPduHdr->u2Version);
    UINT2               u2PduLen = OSIX_NTOHS (pPduHdr->u2PduLen);

    UINT2               u2CmnVersion = 0;
    UINT2               u2CmnMaxPduLen = 0;

    /* LSR-ID Validation - compare based on the net address present in the 
     * first four bytes of the LdpId. 
     * Note : Here Incarnation used is Default Incarn.  If we have
     * different Incarnation then we need to modify this part accordingly.*/

    if (MEMCMP (pPduHdr->LdpId, (UINT1 *) &(LDP_LSR_ID (LDP_CUR_INCARN)),
                LDP_NET_ADDR_LEN) == 0)
    {
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: Validate PduHdr Failed - Bad LSR-ID\n");
        return LDP_FAILURE;
    }

    /* Considering default version and pdu length in case of Session not
     * present or session not in operational state */
    if ((pSessionEntry == NULL) || (pSessionEntry->u1SessionState !=
                                    LDP_SSM_ST_OPERATIONAL))
    {
        u2CmnVersion = LDP_PROTO_VER;
        u2CmnMaxPduLen = LDP_DEF_MAX_PDU_LEN;
    }
    else
    {                            /* When session is in Operational State */
        u2CmnVersion = pSessionEntry->CmnSsnParams.u2ProtocolVer;
        u2CmnMaxPduLen = pSessionEntry->CmnSsnParams.u2MaxPduLen;
    }

    if (pSessionEntry == NULL)
    {                            /* packet recvd is Hello */
        /* As the u2Bufsize is the size of whole packet, at sometime there are some chance 
           where the packet length may differ from the buffer size. Hence the PDU length also
           needs to be validated for min pdu length */
        if ((u2Version != u2CmnVersion) || (u2BufSize < LDP_MIN_PDU_LEN) ||
            (u2BufSize > u2CmnMaxPduLen) || (u2PduLen == 0)
            || (u2PduLen < LDP_MIN_PDU_LEN) || (u2PduLen > u2CmnMaxPduLen))
        {
            /* No event is generated as Errors in PDUs carrying Hello Msgs are
             * silently ingnored */
            pu1StatusType = NULL;
            LDP_DBG (LDP_PRCS_PRCS,
                     "PRCS: Validate PduHdr Failed for Hello Msg\n");
            return LDP_FAILURE;
        }
        LDP_DBG (LDP_PRCS_PRCS,
                 "PRCS: Validate PduHdr Success for Hello Msg\n");
        return LDP_SUCCESS;
    }

    /* Validation for packets other than Hello */

    /* Ldp Protocol Version validation */
    if (u2Version != u2CmnVersion)
    {
        *pu1StatusType = LDP_STAT_TYPE_BAD_PROTO_VER;
        LDP_DBG (LDP_PRCS_PRCS,
                 "PRCS: Validate PduHdr Failed - Bad Protocol Version\n");
        return LDP_FAILURE;
    }

    /* Pdu Length validation */
    if ((u2BufSize < LDP_MIN_PDU_LEN) || (u2BufSize > u2CmnMaxPduLen) ||
        (u2PduLen == 0) || (u2PduLen < LDP_MIN_PDU_LEN) ||
        (u2PduLen > u2CmnMaxPduLen))
    {
        *pu1StatusType = LDP_STAT_TYPE_BAD_PDU_LEN;
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: Validate PduHdr Failed - Bad PDU Len\n");
        return LDP_FAILURE;
    }

    /* Peer LdpId validation */
    if (MEMCMP (pPduHdr->LdpId, pSessionEntry->pLdpPeer->PeerLdpId,
                LDP_MAX_LDPID_LEN) != 0)
    {
        *pu1StatusType = LDP_STAT_TYPE_BAD_LDPID;
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: Validate PduHdr Failed - Bad LDP-ID\n");
        return LDP_FAILURE;
    }

    return (LDP_SUCCESS);
}

/*****************************************************************************/
/* Function Name : LdpValidateMsgHdr                                         */
/* Description   : This routine validates the Message header. Validation is  */
/*                 done on Message type and Message length.                  */
/*                                                                           */
/* Input(s)      : u2BufLen - Length of the message buffer. This includes    */
/*                            the message header also.                       */
/*                 u2MsgLen - Length of the message as encoded in the pMsg.  */
/*                 u2MsgType - Type of the message                           */
/* Output        : pu1StatusType - If the message header is invalid, this    */
/*                            field specifies the type of error.             */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpValidateMsgHdr (UINT2 u2BufLen, UINT2 u2MsgLen, UINT2 u2MsgType,
                   UINT1 *pu1StatusType)
{

    switch (u2MsgType)
    {
        case LDP_NOTIF_MSG:
        case LDP_HELLO_MSG:
        case LDP_INIT_MSG:
        case LDP_KEEP_ALIVE_MSG:
        case LDP_ADDR_MSG:
        case LDP_ADDR_WITHDRAW_MSG:
        case LDP_LBL_MAPPING_MSG:
        case LDP_LBL_REQUEST_MSG:
        case LDP_LBL_WITHDRAW_MSG:
        case LDP_LBL_RELEASE_MSG:
        case LDP_LBL_ABORT_REQ_MSG:
        case LDP_ICCP_RG_DATA_MSG:
        case LDP_ICCP_RG_NOTIFICATION_MSG:
        case LDP_ICCP_RG_DISCONNECT_MSG:
        case LDP_ICCP_RG_CONNECT_MSG:
            /* Valid Msg Type */
            break;

        default:                /* Invalid MsgType */
            if (u2MsgType < LDP_TLV_MSG_U_BIT)
            {
                *pu1StatusType = LDP_STAT_TYPE_UNKNOWN_MSG_TYPE;
            }
            else
            {                    /* (u2MsgType > LDP_TLV_MSG_U_BIT) */
                *pu1StatusType = LDP_IGNORE_ERROR;
            }
            LDP_DBG (LDP_PRCS_PRCS,
                     "PRCS: Validate MsgHdr Failed - Invalid MsgType\n");
            return LDP_FAILURE;
    }

    if ((u2MsgLen > (u2BufLen - LDP_MSG_HDR_LEN)) ||
        (u2MsgLen < LDP_MIN_MSG_LEN) || (u2MsgLen > LDP_DEF_MAX_PDU_LEN))
    {
        /* Msg extends beyond the end of the LDP PDU */
        *pu1StatusType = LDP_STAT_TYPE_BAD_MSG_LEN;
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: Validate MsgHdr Failed - Bad MsgLen\n");
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpValidateTlvHdr                                         */
/* Description   : This routine validates the TLV header. Validation is      */
/*                 done on TLV type                                          */
/*                                                                           */
/* Input(s)      : pMsg - Pointer to the entire msg                          */
/*                 u2MsgLen - Length of the message                          */
/* Output        : pu1StatusType - If the TLV header is invalid, this        */
/*                            field specifies the type of error.             */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpValidateTlvHdr (UINT1 *pMsg, UINT2 u2MsgLen, UINT1 *pu1StatusType)
{
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLen = 0;
    UINT1              *pu1MsgPtr = pMsg;

    *pu1StatusType = 0;

    pu1MsgPtr = pu1MsgPtr + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN;

    while (pu1MsgPtr < pMsg + u2MsgLen)
    {
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        /*Check for invalid TLV Length */
        if ((u2TlvLen > LDP_DEF_MAX_PDU_LEN) || (u2TlvLen > u2MsgLen))
        {
            *pu1StatusType = LDP_STAT_BAD_TLV_LEN;
            return LDP_FAILURE;
        }
        switch (u2TlvType)
        {
            case LDP_FEC_TLV:
            case LDP_ADDRLIST_TLV:
            case LDP_HOP_COUNT_TLV:
            case LDP_PATH_VECTOR_TLV:
            case LDP_GEN_LABEL_TLV:
            case LDP_ATM_LABEL_TLV:
            case LDP_FR_LABEL_TLV:
            case LDP_STATUS_TLV:
            case LDP_EXTND_STATUS_TLV:
            case LDP_RETURNED_PDU_TLV:
            case LDP_RETURNED_MSG_TLV:
            case LDP_CMN_HELLO_PARM_TLV:
            case LDP_IPV4_TRANSPORT_ADDR_TLV:
            case LDP_CONF_SEQNUMB_TLV:
            case LDP_IPV6_TRANSPORT_ADDR_TLV:
            case LDP_CMN_SSN_PARAM_TLV:
            case LDP_ATM_SSN_PARAM_TLV:
            case LDP_FR_SSN_PARAM:
            case LDP_LBL_REQ_MSGID_TLV:
                /* PW related TLVs */
            case LDP_PW_STATUS_TLV:
            case LDP_PW_IF_PARAMS_TLV:
            case LDP_PW_GROUPING_ID_TLV:
                /* CRLDP related TLVs */
            case CRLDP_EXPLROUTE_TLV:
            case CRLDP_ERHOP_TY_IPV4:
            case CRLDP_ERHOP_TY_IPV6:
            case CRLDP_ER_HOP_ASNUM:
            case CRLDP_ER_HOP_LSPID:
            case CRLDP_TRAFPARM_TLV:
            case CRLDP_PREEMPT_TLV:
            case CRLDP_LSPID_TLV:
            case CRLDP_RESCLS_TLV:
            case CRLDP_PINNING_TLV:
            case CRLDP_DIFFSER_TLV:
            case CRLDP_DIFFSERV_ELSPTP_TLV:
            case CRLDP_DIFFSERV_CLASSTYPE_TLV:
            case LDP_FT_SSN_TLV:
            case LDP_ICCP_RG_ID_TLV:
            case LDP_ICCP_PW_RED_CONFIG_TLV:
            case LDP_ICCP_SERVICE_NAME_TLV:
            case LDP_ICCP_PW_ID_TLV:
            case LDP_ICCP_GEN_PW_ID_TLV:
            case LDP_ICCP_PW_RED_STATE_TLV:
            case LDP_ICCP_PW_RED_SYNC_RQST_TLV:
            case LDP_ICCP_PW_RED_SYNC_DATA_TLV:
                /* Valid Msg Type */
                break;

            default:            /* Invalid MsgType */
                if (u2TlvType < LDP_MIN_TLV_VAL)
                {
                    *pu1StatusType = LDP_STAT_TYPE_UNKNOWN_TLV;
                }
                else
                {                /* (u2MsgType > LDP_TLV_MSG_U_BIT) */
                    *pu1StatusType = LDP_IGNORE_ERROR;
                    break;
                }

                LDP_DBG (LDP_PRCS_PRCS,
                         "PRCS: Validate TlvHdr Failed - Invalid TlvType\n");
                return LDP_FAILURE;
        }
        pu1MsgPtr += u2TlvLen;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpGetSsnFromConnIdOrPeerLdpId                            */
/* Description   : This routine is called to get the session entry from the  */
/*                 given Tcp connection Id or given Peer's LdpId.            */
/*                                                                           */
/* Input(s)      : ppSessionEntry - Output of the search is stored in this    */
/*                                 field ie, the Session Entry.              */
/*                 pLdpEntity - Points to the Ldp Entity to which the msg    */
/*                              is sent.                                     */
/*                 u4TcpConnId - Value of the Tcp connection Id for the      */
/*                               session to be searched.                     */
/*                 u1SrchConnId - Specifies based on which the search has to */
/*                               be made. If LDP_TRUE, search has to be made */
/*                               based on Tcp ConnId, else based on PeerLdpID*/
/* Output(s)     : ppSessionEntry - Session Entry                            */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpGetSsnFromConnIdOrPeerLdpId (tLdpSession ** ppSessionEntry,
                                tLdpEntity * pLdpEntity, UINT4 u4TcpConnId,
                                UINT1 *pu1PeerLdpId, UINT1 u1SrchConnId)
{
    tLdpPeer           *pLdpPeer = NULL;

    if ((u1SrchConnId == LDP_TRUE) && (u4TcpConnId == LDP_INVALID_CONN_ID))
    {
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "PRCS: Failed to Get Ssn. Invalid ConnId passed - %d\n",
                  u4TcpConnId);
        return LDP_FAILURE;
    }
    else if ((u1SrchConnId != LDP_TRUE) && (pu1PeerLdpId == NULL))
    {
        /* if search has to be done based on PeerLdpId and valid 
         * LdpId not passed */
        LDP_DBG (LDP_PRCS_PRCS,
                 "PRCS: Failed to Get Ssn. PeerLdpId is not passed\n");
        return LDP_FAILURE;
    }

    /* Search sessions belonging to this Ldp Entity */
    /* Search Sessions with Local Peers */
    TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
    {
        if (u1SrchConnId == LDP_TRUE)
        {                        /* Search based on ConnId */
            *ppSessionEntry = (tLdpSession *) (pLdpPeer->pLdpSession);

            if (*ppSessionEntry != NULL)
            {
                if ((*ppSessionEntry)->u4TcpConnId == u4TcpConnId)
                {
                    return LDP_SUCCESS;
                }
            }
        }
        else
        {                        /* search based on LdpId */
            if (MEMCMP (pLdpPeer->PeerLdpId, pu1PeerLdpId, LDP_MAX_LDPID_LEN)
                == 0)
            {
                *ppSessionEntry = (tLdpSession *) (pLdpPeer->pLdpSession);
                if (*ppSessionEntry == NULL)
                {
                    return LDP_FAILURE;
                }
                else
                {
                    return LDP_SUCCESS;
                }
            }
        }
    }

    /* Search Sessions with Remote Peers */
    TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
    {
        if (u1SrchConnId == LDP_TRUE)
        {                        /* Search based on ConnId */
            *ppSessionEntry = (tLdpSession *) (pLdpPeer->pLdpSession);

            if (*ppSessionEntry != NULL)
            {
                if ((*ppSessionEntry)->u4TcpConnId == u4TcpConnId)
                {
                    return LDP_SUCCESS;
                }
            }
        }
        else
        {                        /* search based on LdpId */
            if (MEMCMP (pLdpPeer->PeerLdpId, pu1PeerLdpId, LDP_MAX_LDPID_LEN)
                == 0)
            {
                *ppSessionEntry = (tLdpSession *) (pLdpPeer->pLdpSession);

                if (*ppSessionEntry == NULL)
                {
                    LDP_DBG (LDP_PRCS_PRCS,
                             "PRCS: Failed to get Ssn. No Ssn with given Peer\n");
                    return LDP_FAILURE;
                }
                else
                {
                    return LDP_SUCCESS;
                }
            }
        }
    }

    /* No session found with matching TcpConnId/PeerLdpId. */
    *ppSessionEntry = NULL;
    LDP_DBG (LDP_PRCS_PRCS, "PRCS: Failed to get Ssn from ConnId/PeerLdpId\n");
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetEntityFromIfIndex                                   */
/* Description   : This routine gets the Ldp Entity corresponding to the     */
/*                 Interface Index passed.                                   */
/*                                                                           */
/* Input(s)      : u4IncarnId - Incarnation ID                               */
/*                 u4IfIndex - Interface Index for which the corresponding   */
/*                             Ldp Entity has to be found.                   */
/*               : u1LocalFlag - If LDP_TRUE Scan for Local Ldp Entities     */
/*                               If LDP_FALSE Scan for Targeted LDP Entities */
/*                 u4PeerAddr  - Target Peer Address. It is used to match    */
/*                               the targeted entity for the address given   */
/*                               This value is given only when the flag      */
/*                               u1LocalFlag is LDP_FALSE.                   */
/*                               If u1LocalFlag is LDP_TRUE u4PeerAddr       */
/*                               should be zero. u4PeerAddr should be given  */
/*                               in Network Order.                           */
/* OUTPUT        : ppLdpEntity - Output LDP Entity Value                     */
/*                 ppLdpPeer   - Output LDP Peer Value. Filled only for      */
/*                               Targeted Entity case.                       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpGetEntityFromIfIndex (UINT4 u4IncarnId, UINT4 u4IfIndex,
                         UINT1 u1LocalFlag, tLdpEntity ** ppLdpEntity,
                         tGenAddr PeerAddr, tLdpPeer ** ppLdpPeer,
                         tLdpId PeerLdpId)
{
    tLdpIfTableEntry   *pIfEntry = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT4               u4TempPeerAddr = 0;
    UINT4               u4TempNetAddr = 0;

    MEMSET (&PeerLdpId, 0, sizeof (tLdpId));

    TMO_SLL_Scan (&(LDP_ENTITY_LIST (u4IncarnId)), pLdpEntity, tLdpEntity *)
    {
        if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE) ||
            (LDP_ENTITY_ADMIN_STATUS (pLdpEntity) != LDP_ADMIN_UP) ||
            (pLdpEntity->u1OperStatus != LDP_OPER_ENABLED))

        {
            continue;
        }

        if (u1LocalFlag == LDP_TRUE)
        {
            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
            {
                continue;
            }
        }
        else
        {
            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_FALSE)
            {
                continue;
            }

            pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));

            if (pLdpPeer == NULL)
            {
                continue;
            }

            /* MPLS_IPv6 mod start */
            /*In DUAL STACK interface as Peer LDPID will remain same for both V4 and V6 Targeted, 
               To fetch the right entity Addr Type should also be matched */
            if (pLdpPeer->NetAddr.u2AddrType != PeerAddr.u2AddrType)
            {
                continue;
            }

#ifdef MPLS_IPV6_WANTED
            if (LDP_ADDR_TYPE_IPV6 == PeerAddr.u2AddrType)
            {
                if (MEMCMP (pLdpPeer->NetAddr.Addr.Ip6Addr.u1_addr,
                            PeerAddr.Addr.Ip6Addr.u1_addr,
                            LDP_NET_ADDR6_LEN) != LDP_ZERO)
                {
                    continue;
                }
            }
            else
            {
#endif
                MEMCPY (&u4TempPeerAddr, LDP_IPV4_ADDR (PeerAddr.Addr),
                        LDP_IPV4ADR_LEN);
                MEMCPY (&u4TempNetAddr, LDP_IPV4_ADDR (pLdpPeer->NetAddr.Addr),
                        LDP_IPV4ADR_LEN);

                /* Targeted session will not come up till the locally configured neighbor-address
                 * matches the transport-address-TLV configured on peer; Hence Comparison for
                 * LSRID match has been removed as per As per RFC 5036, Section “3.5.2.1 Point 1*/

                if (u4TempPeerAddr != u4TempNetAddr)
                {
                    continue;
                }
#ifdef MPLS_IPV6_WANTED
            }
#endif
            /* MPLS_IPv6 mod end */
        }

        if (pLdpEntity->bIsIntfMapped == LDP_FALSE)
        {
            *ppLdpEntity = pLdpEntity;
            *ppLdpPeer = pLdpPeer;
            return LDP_SUCCESS;
        }

        TMO_SLL_Scan (&(pLdpEntity->IfList), pIfEntry, tLdpIfTableEntry *)
        {
            if ((pIfEntry->u4IfIndex == u4IfIndex) &&
                (pIfEntry->u1OperStatus == LDP_IF_OPER_ENABLE))
            {
                *ppLdpEntity = pLdpEntity;
                *ppLdpPeer = pLdpPeer;
                return LDP_SUCCESS;
            }
        }
    }

    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpIsPeerExist                                            */
/* Description   : This routine checks whether the peer exists or not        */
/*                                                                           */
/* Input(s)      : u4IncarnId - Incarnation ID                               */
/*                 u4IfIndex - Interface Index for which the corresponding   */
/*                             Ldp Entity has to be found.                   */
/*               : u1RemoteFlag - If LDP_TRUE Scan for Local Ldp Entities     */
/*                               If LDP_FALSE Scan for Targeted LDP Entities */
/*                 u4PeerAddr  - Target Peer Address. It is used to match    */
/* OUTPUT        : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_TRUE/LDP_FALSE                                        */
/*****************************************************************************/

/* MPLS_IPv6 mod start*/
BOOL1
LdpIsPeerExist (UINT4 u4IncarnId, UINT4 u4IfIndex, UINT1 u1RemoteFlag,
                tGenAddr PeerAddr)
{
    /* MPLS_IPv6 mod end */
    tLdpIfTableEntry   *pIfEntry = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpId              LdpId;
    BOOL1               bIsLinkSsnPresent = LDP_FALSE;
    /* MPLS_IPv6 mod start */
    UINT1               u1IpAddr[4];
#ifdef MPLS_IPV6_WANTED
    UINT1               u1IsPeerAddr6 = LDP_FALSE;
    tIp6Addr            IpAddr;
    MEMSET (&IpAddr, 0, sizeof (tIp6Addr));
    if (LDP_ADDR_TYPE_IPV6 == PeerAddr.u2AddrType)
    {
        u1IsPeerAddr6 = LDP_TRUE;
    }
#endif
    MEMSET (u1IpAddr, 0, sizeof (u1IpAddr));
    /* MPLS_IPv6 mod end */
    MEMSET (LdpId, LDP_ZERO, LDP_MAX_LDPID_LEN);

    TMO_SLL_Scan (&(LDP_ENTITY_LIST (u4IncarnId)), pLdpEntity, tLdpEntity *)
    {
        if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE) ||
            (LDP_ENTITY_ADMIN_STATUS (pLdpEntity) != LDP_ADMIN_UP) ||
            (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE))
        {
            continue;
        }

        /* Preferred session is link session always */
        TMO_SLL_Scan (&((pLdpEntity)->PeerList), pLdpPeer, tLdpPeer *)
        {
            /* MPLS_IPv6 mod start */
#ifdef MPLS_IPV6_WANTED
            if (LDP_TRUE == u1IsPeerAddr6)
            {
                MEMCPY (IpAddr.u1_addr, LDP_IPV6_ADDR (pLdpPeer->NetAddr.Addr),
                        LDP_NET_ADDR6_LEN);
                if (LDP_ZERO ==
                    MEMCMP (LDP_IPV6_ADDR (PeerAddr.Addr), IpAddr.u1_addr,
                            LDP_NET_ADDR6_LEN))
                {
                    bIsLinkSsnPresent = LDP_TRUE;
                }
            }
            else
            {
#endif
                MEMCPY (u1IpAddr, pLdpPeer->NetAddr.Addr.au1Ipv4Addr,
                        LDP_NET_ADDR_LEN);
                if (LDP_ZERO ==
                    MEMCMP (PeerAddr.Addr.au1Ipv4Addr, u1IpAddr,
                            LDP_NET_ADDR_LEN))
                {

                    bIsLinkSsnPresent = LDP_TRUE;
                }
#ifdef MPLS_IPV6_WANTED
            }
#endif
            /* MPLS_IPv6 mod end */
        }
    }
    TMO_SLL_Scan (&(LDP_ENTITY_LIST (u4IncarnId)), pLdpEntity, tLdpEntity *)
    {
        if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE) ||
            (LDP_ENTITY_ADMIN_STATUS (pLdpEntity) != LDP_ADMIN_UP))
        {
            continue;
        }
        if (u1RemoteFlag == LDP_TRUE)
        {
            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
            {
                continue;
            }
            TMO_SLL_Scan (&((pLdpEntity)->PeerList), pLdpPeer, tLdpPeer *)
            {
                /* MPLS_IPv6 mod start */
#ifdef MPLS_IPV6_WANTED
                if (LDP_TRUE == u1IsPeerAddr6)
                {
                    MEMCPY (IpAddr.u1_addr,
                            LDP_IPV6_ADDR (pLdpPeer->NetAddr.Addr),
                            LDP_NET_ADDR6_LEN);
                    if (LDP_ZERO ==
                        MEMCMP (LDP_IPV6_ADDR (PeerAddr.Addr), IpAddr.u1_addr,
                                LDP_NET_ADDR6_LEN))
                    {
                        break;
                    }
                }
                else
                {
#endif
                    MEMCPY (u1IpAddr, pLdpPeer->NetAddr.Addr.au1Ipv4Addr,
                            LDP_NET_ADDR_LEN);
                    if (LDP_ZERO ==
                        MEMCMP (PeerAddr.Addr.au1Ipv4Addr, u1IpAddr,
                                LDP_NET_ADDR_LEN))
                    {
                        break;
                    }
#ifdef MPLS_IPV6_WANTED
                }
#endif
                /* MPLS_IPv6 mod end */
            }
        }
        else
        {
            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_FALSE)
            {
                continue;
            }

            pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));

            if (pLdpPeer == NULL)
            {
                continue;
            }
#ifdef MPLS_IPV6_WANTED
            if (LDP_TRUE == u1IsPeerAddr6)
            {
                if (MEMCMP (pLdpPeer->NetAddr.Addr.Ip6Addr.u1_addr,
                            PeerAddr.Addr.Ip6Addr.u1_addr,
                            LDP_NET_ADDR6_LEN) != LDP_ZERO)
                {
                    continue;
                }
            }
            else
            {
#endif
                if (MEMCMP (pLdpPeer->NetAddr.Addr.au1Ipv4Addr,
                            PeerAddr.Addr.au1Ipv4Addr,
                            LDP_NET_ADDR_LEN) != LDP_ZERO)
                {
                    continue;
                }
#ifdef MPLS_IPV6_WANTED
            }
#endif
        }
        if ((pLdpPeer == NULL) || (pLdpPeer->pLdpSession == NULL))
        {
            continue;
        }

        if ((bIsLinkSsnPresent == LDP_TRUE) &&
            (u1RemoteFlag == LDP_FALSE) && (pLdpPeer->pLdpSession != NULL))
        {
            return LDP_TRUE;
        }

        TMO_SLL_Scan (&(pLdpEntity->IfList), pIfEntry, tLdpIfTableEntry *)
        {
            if ((pIfEntry->u4IfIndex == u4IfIndex) &&
                (pIfEntry->u1OperStatus == LDP_IF_OPER_ENABLE))
            {
                return LDP_TRUE;
            }
        }
    }
    if ((bIsLinkSsnPresent == LDP_TRUE) && (u1RemoteFlag == LDP_TRUE))
    {
        return LDP_TRUE;
    }
    return LDP_FALSE;
}

/*****************************************************************************/
/* Function Name : LdpGetTargetedEntity                                      */
/* Description   : This routine gets the Ldp TargetedEntity                  */
/*                                                                           */
/* Input(s)      : pEntityList - Points to the Ldp Entity list, in which the */
/*                               search has to be done.                      */
/*                 u4IfIndex - Interface Index for which the corresponding   */
/*                             Ldp Entity has to be found.                   */
/*               : u4PeerAddr - Targeted Peer address                        */
/* OUTPUT        : ppLdpEntity - The output of the search is stored in this  */
/*                               field.                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpGetTargetedEntity (tTMO_SLL * pEntityList, UINT4 u4IfIndex,
                      UINT4 u4PeerAddr, tLdpEntity ** ppLdpEntity)
{
    tLdpIfTableEntry   *pIfEntry = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT4               u4IpAddr;

    TMO_SLL_Scan (pEntityList, *ppLdpEntity, tLdpEntity *)
    {
        if (LDP_ENTITY_IS_TARGET_TYPE (*ppLdpEntity) == LDP_TRUE)
        {
            TMO_SLL_Scan (&((*ppLdpEntity)->IfList), pIfEntry,
                          tLdpIfTableEntry *)
            {
                if (pIfEntry->u4IfIndex == u4IfIndex)
                {
                    pLdpPeer =
                        (tLdpPeer *) TMO_SLL_First
                        (&(((tLdpEntity *) * ppLdpEntity)->PeerList));
                    if (pLdpPeer == NULL)
                    {
                        return LDP_FAILURE;
                    }

                    MEMCPY (&u4IpAddr, pLdpPeer->NetAddr.Addr.au1Ipv4Addr,
                            IPV4_ADDR_LENGTH);
                    u4IpAddr = OSIX_NTOHL (u4IpAddr);

                    if (u4PeerAddr == u4IpAddr)
                    {
                        LDP_DBG2 (LDP_PRCS_PRCS,
                                  "PRCS: Targetted Entity  found. IfIndex/PeerAddr: %d/%x\n",
                                  u4IfIndex, u4PeerAddr);
                        return LDP_SUCCESS;
                    }
                }
            }
        }
    }

    /* No entity found with matching Interface Index */
    LDP_DBG2 (LDP_PRCS_PRCS,
              "PRCS: Targetted Entity Search Failed IfIndex/PeerAddr: %d/%x\n",
              u4IfIndex, u4PeerAddr);
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpCreateLdpBfdSession                                    */
/* Description   : Creates a new LdpBfdSession                               */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLdpEntity - Points to the LdpEntity, to which the Session*/
/*                              has to be linked to.                         */
/*                 pLdpSession  - Pointer to LdpSession                      */
/*                 u4IfIndex  - ifindex                                      */
/*                                                                           */
/*                                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
#ifdef MPLS_LDP_BFD_WANTED
UINT1
LdpCreateBfdSession (tLdpEntity * pLdpEntity, tLdpSession * pLdpSession,
                     UINT4 u4IfIndex, UINT1 u1AddrType)
{

    tLdpBfdSession     *pLdpBfdSession = NULL;

    /* Scan through the Bfd Ldp List  */
    if ((NULL == pLdpEntity) || (NULL == pLdpSession))
    {
        LDP_DBG (LDP_SSN_PRCS, "PRCS: pLdpSession is NULL \n");
        return LDP_FAILURE;
    }

    TMO_SLL_Scan (&(pLdpEntity->BfdSessList), pLdpBfdSession, tLdpBfdSession *)
    {
        if (NULL == pLdpBfdSession->pLdpSession)
        {
            if (pLdpBfdSession->u1AddrType == LDP_ADDR_TYPE_IPV4)
            {
                if ((MEMCMP (&pLdpBfdSession->DestAddr.Addr.au1Ipv4Addr,
                             &LDP_PEER_TRANS_ADDR (pLdpSession->pLdpPeer).Addr.
                             au1Ipv4Addr, LDP_IPV4ADR_LEN) == LDP_ZERO)
                    &&
                    (MEMCMP
                     (&pLdpBfdSession->SrcAddr.Addr.au1Ipv4Addr,
                      &pLdpSession->SrcTransAddr.Addr.au1Ipv4Addr,
                      LDP_IPV4ADR_LEN) == LDP_ZERO)
                    && (pLdpBfdSession->u4IfIndex == u4IfIndex))
                {
                    pLdpBfdSession->pLdpSession = pLdpSession;
                    return LDP_SUCCESS;
                }
            }
            else
            {
                if ((MEMCMP (&pLdpBfdSession->DestAddr.Addr.Ip6Addr,
                             &LDP_PEER_TRANS_ADDR (pLdpSession->pLdpPeer).Addr.
                             Ip6Addr, LDP_IPV6ADR_LEN) == LDP_ZERO)
                    &&
                    (MEMCMP
                     (&pLdpBfdSession->SrcAddr.Addr.Ip6Addr,
                      &pLdpSession->SrcTransAddr.Addr.Ip6Addr,
                      LDP_IPV6ADR_LEN) == LDP_ZERO)
                    && (pLdpBfdSession->u4IfIndex == u4IfIndex))
                {
                    pLdpBfdSession->pLdpSession = pLdpSession;
                    return LDP_SUCCESS;
                }
            }

        }
    }

    /* Allocating Mem for LdpBfdsession */
    pLdpBfdSession = (tLdpBfdSession *) MemAllocMemBlk (LDP_BFD_SSN_POOL_ID);
    if (pLdpBfdSession == NULL)
    {
        LDP_DBG (LDP_PRCS_MEM, "PRCS: MemAlloc Failed for LdpBfdSession \n");
        return LDP_FAILURE;
    }

    /* Initialising the LdpBfdSession's MemBlock to Zero */
    MEMSET (pLdpBfdSession, LDP_ZERO, sizeof (tLdpBfdSession));
    TMO_SLL_Init_Node (&(pLdpBfdSession->NextBfdSess));

#ifdef MPLS_IPV6_WANTED
    if (u1AddrType == LDP_ADDR_TYPE_IPV4)
    {
#endif
        MEMCPY (&pLdpBfdSession->DestAddr.Addr.au1Ipv4Addr,
                &LDP_PEER_TRANS_ADDR (pLdpSession->pLdpPeer).Addr.au1Ipv4Addr,
                LDP_NET_ADDR_LEN);
        MEMCPY (&pLdpBfdSession->SrcAddr.Addr.au1Ipv4Addr,
                &pLdpSession->SrcTransAddr.Addr.au1Ipv4Addr, LDP_NET_ADDR_LEN);

#ifdef MPLS_IPV6_WANTED
    }
    else
    {
        MEMCPY (&pLdpBfdSession->DestAddr.Addr.Ip6Addr,
                &LDP_PEER_TRANS_ADDR (pLdpSession->pLdpPeer).Addr.Ip6Addr,
                LDP_NET_ADDR6_LEN);
        MEMCPY (&pLdpBfdSession->SrcAddr.Addr.Ip6Addr,
                &pLdpSession->SrcTransAddr.Addr.Ip6Addr, LDP_NET_ADDR6_LEN);
    }
#endif

    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpSession->pLdpPeer->pLdpEntity) ==
        LDP_TRUE)
    {
        pLdpBfdSession->u1SessionType =
            BFD_SESS_TYPE_MULTIHOP_UNIDIRECTIONAL_LINKS;
    }
    else
    {
        pLdpBfdSession->u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;
    }

    pLdpBfdSession->u1AddrType = u1AddrType;
    pLdpBfdSession->u4IfIndex = u4IfIndex;
    pLdpBfdSession->pLdpSession = pLdpSession;
    TMO_SLL_Insert (&(pLdpEntity->BfdSessList), NULL,
                    &(pLdpBfdSession->NextBfdSess));
    return LDP_SUCCESS;

}
#endif
/*****************************************************************************/
/* Function Name : LdpCreateLdpPeer                                          */
/* Description   : Creates a new LdpPeer, Remote/Local, based on the Remote  */
/*                 Flag.                                                     */
/*                                                                           */
/* Input(s)      : pLdpEntity - Points to the LdpEntity, to which the peer   */
/*                              has to be linked to.                         */
/*                 PeerLdpId  - Peer's LdpId Rx in the Hello Msg             */
/*                 u1RemoteFlag - Peer to be created is Remote, if Flag is   */
/*                                set to LDP_TRUE, else Local.               */
/*                                                                           */
/* Output(s)     : ppLdpPeer  - Peer Created and Initialised.                */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpCreateLdpPeer (tLdpEntity * pLdpEntity, tLdpId PeerLdpId,
                  UINT1 u1RemoteFlag, tLdpPeer ** ppLdpPeer,
                  UINT1 u1PeerAddrType)
{
    tLdpPeer           *pLdpPeer = NULL;
    /* Validation for Local and Remote Peer boundaries */
    if (((u1RemoteFlag == LDP_TRUE) &&
         (LDP_ENTITY_ESTB_REMOTE_PEERS (pLdpEntity) ==
          LDP_MAX_REMOTE_PEERS (pLdpEntity->u2IncarnId))) ||
        ((u1RemoteFlag == LDP_FALSE) &&
         (LDP_ENTITY_ESTB_LOCAL_PEERS (pLdpEntity) ==
          LDP_MAX_LOCAL_PEERS (pLdpEntity->u2IncarnId))))
    {
        LDP_DBG (LDP_PRCS_MEM, "PRCS: All Peers Allocated are utilized\n");
        return LDP_FAILURE;
    }

    /* Allocating Mem for Peer */
    pLdpPeer = (tLdpPeer *) MemAllocMemBlk (LDP_PEER_POOL_ID);
    if (pLdpPeer == NULL)
    {
        LDP_DBG (LDP_PRCS_MEM, "PRCS: MemAlloc Failed for LdpPeer\n");
        return LDP_FAILURE;
    }

    /* Initialising the LdpPeer's MemBlock to Zero */
    MEMSET ((VOID *) pLdpPeer, LDP_ZERO, sizeof (tLdpPeer));

    /* Initialising the Peer */
    TMO_SLL_Init_Node (&(pLdpPeer->NextLdpPeer));
    TMO_SLL_Init (&pLdpPeer->IfAdrList);
    TMO_SLL_Init (&pLdpPeer->AtmLblRangeList);

    pLdpPeer->u4PeerIndex = LDP_ONE;
    pLdpPeer->u1LabelDistrType = LDP_DSTR_ON_DEMAND;
    pLdpPeer->u4DefMtu = LDP_DEF_MTU;
    pLdpPeer->u4ConfSeqNum = LDP_ZERO;
    pLdpPeer->pLdpEntity = pLdpEntity;
    pLdpPeer->u1GrProgressState = LDP_PEER_GR_NOT_SUPPORTED;
    pLdpPeer->u2GrReconnectTime = LDP_ZERO;
    pLdpPeer->u2GrRecoveryTime = LDP_ZERO;

    if (u1RemoteFlag == LDP_TRUE)
    {
        /* MPLS_IPv6 mod start */

#ifdef MPLS_IPV6_WANTED
        pLdpPeer->u1SsnPrefTmrStatus = LDP_IPV6_SSN_PREF_TMR_NSTARTED;
        if (LDP_ADDR_TYPE_IPV6 == u1PeerAddrType)
        {
            MEMSET (LDP_IPV6_ADDR (pLdpPeer->NetAddr.Addr), LDP_ZERO,
                    LDP_IPV6ADR_LEN);
            pLdpPeer->NetAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
        }
        else
        {
#endif
            UNUSED_PARAM (u1PeerAddrType);
            MEMSET (LDP_IPV4_ADDR (pLdpPeer->NetAddr.Addr), LDP_ZERO,
                    LDP_IPV4ADR_LEN);
            pLdpPeer->NetAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
#ifdef MPLS_IPV6_WANTED
        }
#endif
        /* MPLS_IPv6 mod end */
        pLdpPeer->u4KeepAliveHoldTimer = LDP_DEF_RSSN_HTIME;
        pLdpPeer->u1RemoteFlag = LDP_REMOTE_PEER;
        pLdpPeer->u1TrgtHelloTxReqFlag = LDP_TX_TRGT_HELLOS;
        LDP_ENTITY_ESTB_REMOTE_PEERS (pLdpPeer->pLdpEntity)++;
    }
    else
    {
        /* First 4 Bytes of Peer LdpId */
        MEMCPY ((UINT1 *) &(pLdpPeer->NetAddr), PeerLdpId, LDP_NET_ADDR_LEN);
        pLdpPeer->u4KeepAliveHoldTimer = LDP_DEF_LSSN_HTIME;
        pLdpPeer->u1RemoteFlag = LDP_LOCAL_PEER;
        LDP_ENTITY_ESTB_LOCAL_PEERS (pLdpPeer->pLdpEntity)++;
    }
    MEMCPY (pLdpPeer->PeerLdpId, PeerLdpId, LDP_MAX_LDPID_LEN);
    /* Linking Peer to the Peer List */
    TMO_SLL_Insert (&(pLdpEntity->PeerList), NULL, &(pLdpPeer->NextLdpPeer));

    *ppLdpPeer = pLdpPeer;
    MplsLdpPeerUpdateSysTime ();
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpCreateAdj                                              */
/* Description   : Creates a new Adjacency entry in the Adjacency list,      */
/*                 maintained by the session.                                */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session entry. Adj Entry    */
/*                                 has to be made to the Adj list maintained */
/*                                 by this session.                          */
/*                 u4IfIndex - Iface Index over which Hello Msg is received. */
/*                 u2HoldTime - Hold Time for the Adjacency;                 */
/*                 PeerLdpId - LdpId of the Peer.                            */
/*                 u2IncarnId - Incarnation Id                               */
/*                                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpCreateAdj (tLdpSession * pSessionEntry, UINT4 u4IfIndex,
              UINT2 u2HoldTime, tLdpId PeerLdpId, UINT2 u2IncarnId,
              tGenAddr PeerAddr)
{
    UINT2               u2DefVal = LDP_DEF_LHELLO_HTIME;    /* Hello HTime 
                                                               for Local 
                                                               Hellos */
    tLdpEntity         *pLdpEntity = SSN_GET_ENTITY (pSessionEntry);
    tLdpAdjacency      *pAdjEntry = NULL;
    tLdpAdjacency      *pTempAdjEntry = NULL;
#ifdef MPLS_IPV6_WANTED
    UINT1               u1LdpSendHelloReq = LDP_THREE;
    tLdpAdjacency      *pLdpAdj = NULL;
    tLdpAdjacency      *pTempAdj = NULL;
#endif

    UNUSED_PARAM (u2IncarnId);
    UNUSED_PARAM (PeerLdpId);

    pAdjEntry = (tLdpAdjacency *) MemAllocMemBlk (LDP_ADJ_POOL_ID);

    if (pAdjEntry == NULL)
    {
        LDP_DBG (LDP_PRCS_MEM, "PRCS: MemAlloc Failed for Ldp Adjacency\n");
        return LDP_FAILURE;
    }

    /* Initialising the Adjacency's MemBlock to Zero */
    MEMSET ((VOID *) pAdjEntry, LDP_ZERO, sizeof (tLdpAdjacency));

    /* Initialise Adj Entry */
    TMO_SLL_Init_Node (&(pAdjEntry->NextAdjacency));

    pTempAdjEntry =
        (tLdpAdjacency *) TMO_SLL_Last (&(pSessionEntry->AdjacencyList));

    if (pTempAdjEntry != NULL)
    {
        pAdjEntry->u4Index = pTempAdjEntry->u4Index + LDP_ONE;
    }
    else
    {
        pAdjEntry->u4Index = LDP_ONE;
    }
    /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
    if (LDP_ADDR_TYPE_IPV6 == PeerAddr.u2AddrType)
    {
        pAdjEntry->u1AddrType = (UINT1) LDP_ADDR_TYPE_IPV6;
        MEMCPY (LDP_IPV6_ADDR (pAdjEntry->PeerIpAddr),
                LDP_IPV6_ADDR (PeerAddr.Addr), LDP_IPV6ADR_LEN);
        LDP_DBG1 (LDP_SSN_PRCS, "PRCS: IPV6 Adj Created For PeerAddr %s\n",
                  Ip6PrintAddr (&(pAdjEntry->PeerIpAddr.Ip6Addr)));
    }
    else
    {
#endif
        MEMCPY (LDP_IPV4_ADDR (pAdjEntry->PeerIpAddr),
                LDP_IPV4_ADDR (PeerAddr.Addr), LDP_IPV4ADR_LEN);
        pAdjEntry->u1AddrType = LDP_ADDR_TYPE_IPV4;
        LDP_DBG4 (LDP_SSN_PRCS,
                  "PRCS: IPV4 Adj Created for PeerAddr %x:%x:%x:%x\n",
                  pAdjEntry->PeerIpAddr.au1Ipv4Addr[0],
                  pAdjEntry->PeerIpAddr.au1Ipv4Addr[1],
                  pAdjEntry->PeerIpAddr.au1Ipv4Addr[2],
                  pAdjEntry->PeerIpAddr.au1Ipv4Addr[3]);
#ifdef MPLS_IPV6_WANTED
    }
#endif
    /* MPLS_IPv6 add end */
    pAdjEntry->u4IfIndex = u4IfIndex;
    pAdjEntry->pSession = pSessionEntry;
    pAdjEntry->pPeer = pSessionEntry->pLdpPeer;

    pAdjEntry->PeerAdjTimer.u4Event = LDP_HELLO_ADJ_EXPRD_EVENT;

    /* Assigning the addr of the adj. to which this timer relates to */
    pAdjEntry->PeerAdjTimer.pu1EventInfo = (UINT1 *) pAdjEntry;

#ifdef LDP_GR_WANTED
    /* For the first adjacency the variable would be set to
       LDP_GR_RECONNECT_IN_PROGRESS and for subsequent adjacencies
       the variable would be LDP_GR_RECOVERY_IN_PROGRESS. Once th
       the FWD hold timer expires this would be set to LDP_GR_COMPLETED */
#if 0
    printf ("--------gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus = %d\n",
            gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus);

    printf ("-------pAdjEntry->pPeer->u1GrProgressState = %d\n",
            pAdjEntry->pPeer->u1GrProgressState);
    if (((gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus ==
          LDP_GR_RECONNECT_IN_PROGRESS)
         || (gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus ==
             LDP_GR_RECOVERY_IN_PROGRESS))
        && (pAdjEntry->pPeer->u1GrProgressState != LDP_PEER_GR_NOT_SUPPORTED))
    {
        pAdjEntry->pPeer->u1GrProgressState = LDP_PEER_GR_RECOVERY_IN_PROGRESS;

        LDP_DBG4 (GRACEFUL_DEBUG, "LdpCreateAdj: The Ldp Peer %d.%d.%d.%d "
                  "is set to GR Recovery in Progress State\n",
                  pAdjEntry->pPeer->NetAddr.au1Ipv4Addr[0],
                  pAdjEntry->pPeer->NetAddr.au1Ipv4Addr[1],
                  pAdjEntry->pPeer->NetAddr.au1Ipv4Addr[2],
                  pAdjEntry->pPeer->NetAddr.au1Ipv4Addr[3]);
    }
#endif
#endif

    if (IS_REMOTE_ADJ (pAdjEntry) == LDP_TRUE)
    {
        u2DefVal = LDP_DEF_RHELLO_HTIME;

#ifndef LDP_GR_WANTED
        if ((pAdjEntry->pPeer->u1GrProgressState ==
             LDP_PEER_GR_RECONNECT_IN_PROGRESS) ||
            (gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus ==
             LDP_GR_RECONNECT_IN_PROGRESS))
        {
            pAdjEntry->pPeer->u1GrProgressState =
                LDP_PEER_GR_RECOVERY_IN_PROGRESS;

            LDP_DBG4 (GRACEFUL_DEBUG, "LdpCreateAdj: The Ldp Peer %d.%d.%d.%d "
                      "is set to GR Recovery in Progress State\n",
                      pAdjEntry->pPeer->NetAddr.Addr.au1Ipv4Addr[0],
                      pAdjEntry->pPeer->NetAddr.Addr.au1Ipv4Addr[1],
                      pAdjEntry->pPeer->NetAddr.Addr.au1Ipv4Addr[2],
                      pAdjEntry->pPeer->NetAddr.Addr.au1Ipv4Addr[3]);
        }
#endif
    }
    if (u2HoldTime != LDP_INFINITE_HTIME)
    {

        if (u2HoldTime == LDP_ZERO)
        {
            pAdjEntry->u4HoldTimeRem = (UINT4) LDP_MIN (u2DefVal,
                                                        (pSessionEntry)->
                                                        pLdpPeer->pLdpEntity->
                                                        u2HelloConfHoldTime);
        }
        else
        {
            pAdjEntry->u4HoldTimeRem = (UINT4) LDP_MIN (u2HoldTime,
                                                        (pSessionEntry)->
                                                        pLdpPeer->pLdpEntity->
                                                        u2HelloConfHoldTime);
        }

        /*
         * Modifying Entity's HelloHoldTime Value to the one ie negotiatied.
         * Sending Hello Msgs and restarting the timer for sending Hello Msgs as
         * per the new value ie negotiated.
         * Note: Timer restarting is done within LdpSendHelloMsg.
         */
        SSN_GET_HELLO_HTIME (pSessionEntry) =
            (UINT2) (pAdjEntry->u4HoldTimeRem);

        TmrStopTimer (LDP_TIMER_LIST_ID,
                      (tTmrAppTimer *) & (pLdpEntity->HelloTimer.AppTimer));
        LdpSendHelloMsg (pLdpEntity);

        /* Starting Peer's AdjHold Timer */
        if (TmrStartTimer (LDP_TIMER_LIST_ID,
                           (tTmrAppTimer *) & (pAdjEntry->PeerAdjTimer.
                                               AppTimer),
                           (pAdjEntry->u4HoldTimeRem *
                            SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
        {
            LDP_DBG1 (LDP_PRCS_ALL,
                      "PRCS: Failed to start the timer inside function %s\n",
                      __func__);
        }
    }
    else
    {
        if (SSN_GET_HELLO_HTIME (pSessionEntry) != LDP_INFINITE_HTIME)
        {
            pAdjEntry->u4HoldTimeRem = (UINT4) LDP_MIN (u2HoldTime,
                                                        SSN_GET_HELLO_HTIME
                                                        (pSessionEntry));
            if (pLdpEntity->u2HelloConfHoldTime == LDP_INFINITE_HTIME)
            {
                pAdjEntry->u4HoldTimeRem = u2HoldTime;
                SSN_GET_HELLO_HTIME (pSessionEntry) = u2HoldTime;
                TmrStopTimer (LDP_TIMER_LIST_ID,
                              (tTmrAppTimer *) & (pLdpEntity->HelloTimer.
                                                  AppTimer));
                LdpSendHelloMsg (pLdpEntity);
            }
            else
            {
                if (TmrStartTimer (LDP_TIMER_LIST_ID,
                                   (tTmrAppTimer *) & (pAdjEntry->PeerAdjTimer.
                                                       AppTimer),
                                   (pAdjEntry->u4HoldTimeRem *
                                    SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) ==
                    TMR_FAILURE)
                {
                    LDP_DBG1 (LDP_PRCS_ALL,
                              "PRCS: Failed to start the timer inside function %s\n",
                              __func__);
                }
            }
        }
        else
        {
            pAdjEntry->u4HoldTimeRem = (UINT4) u2HoldTime;
            SSN_GET_HELLO_HTIME (pSessionEntry) = u2HoldTime;

            TmrStopTimer (LDP_TIMER_LIST_ID,
                          (tTmrAppTimer *) & (pLdpEntity->HelloTimer.AppTimer));
            LdpSendHelloMsg (pLdpEntity);

        }
    }
#ifdef MPLS_IPV6_WANTED
    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_FALSE)
    {
        TMO_DYN_SLL_Scan (&(pSessionEntry->AdjacencyList), pLdpAdj, pTempAdj,
                          tLdpAdjacency *)
        {
            if (pSessionEntry->SrcTransAddr.u2AddrType == LDP_ADDR_TYPE_IPV6)
            {
                if (pLdpAdj->u1AddrType == (UINT1) LDP_ADDR_TYPE_IPV4)
                {
                    LDP_DBG1 (LDP_DBG_PRCS,
                              "LdpCreateAdj: SessionType->V6 V4Adj Found with IfIndex->:%d \n",
                              pLdpAdj->u4IfIndex);
                    u1LdpSendHelloReq = LDP_FALSE;
                    break;
                }
                LDP_DBG1 (LDP_DBG_PRCS,
                          "LdpCreateAdj: SessionType->V6 First V4Adj came up with IfIndex->:%d \n",
                          pLdpAdj->u4IfIndex);
                u1LdpSendHelloReq = LDP_TRUE;
            }
            else
            {
                if (pLdpAdj->u1AddrType == (UINT1) LDP_ADDR_TYPE_IPV6)
                {
                    LDP_DBG1 (LDP_DBG_PRCS, "LdpCreateAdj: Dual interface"
                              "came up: SessionType->V4 V6Adj Found with IfIndex->:%d \n",
                              pLdpAdj->u4IfIndex);
                    u1LdpSendHelloReq = LDP_FALSE;
                    break;
                }
                LDP_DBG1 (LDP_DBG_PRCS,
                          "LdpCreateAdj: SessionType->V4 First V6Adj came up with IfIndex->:%d \n",
                          pLdpAdj->u4IfIndex);

                u1LdpSendHelloReq = LDP_TRUE;
            }
        }

    }
#endif
    /* Link the Adj Entry to Adj List */
    TMO_SLL_Add (&(pSessionEntry->AdjacencyList), &(pAdjEntry->NextAdjacency));
#ifdef MPLS_IPV6_WANTED
    if ((pSessionEntry->u1SessionState == LDP_SSM_ST_OPERATIONAL) &&
        (LDP_TRUE == u1LdpSendHelloReq))
    {
        if (pSessionEntry->SrcTransAddr.u2AddrType == LDP_ADDR_TYPE_IPV4)
        {
            LDP_DBG (LDP_DBG_PRCS,
                     "LdpCreateAdj: Establishing Ipv6 LSP for all prefixes\n");
            LdpEstbLspsForAllIpv6RouteEntries (pSessionEntry);
        }
        else
        {
            LDP_DBG (LDP_DBG_PRCS,
                     "LdpCreateAdj: Establishing IPV4 LSP for all prefixes\n");
            LdpEstbLspsForAllRouteEntries (pSessionEntry);
        }
    }
#endif

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpCreateSession                                          */
/* Description   : Creates a new Session entry and links to the Session list */
/*                 maintained by the Ldp Peer. Session state is initialised  */
/*                 to NON_EXISTENT state.                                    */
/*                                                                           */
/* Input(s)      : pLdpPeer - LdpPeer with which the session is being        */
/*                            established.                                   */
/*                 pLdpEntity - Ldp Entity associated to this session.       */
/*                 u1SessionRole - Specifies the role of Ldp Entity in this  */
/*                                 session ie, Active or Passive.            */
/*                 u2IncarnId - Incarnation Id                               */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */

UINT1
LdpCreateSession (tLdpPeer * pLdpPeer, tLdpEntity * pLdpEntity,
                  UINT1 u1SessionRole, UINT2 u2IncarnId)
{
    tLdpSession        *pSessionEntry = NULL;

    UNUSED_PARAM (u2IncarnId);
    LDP_DBG2 (LDP_PRCS_PRCS, "%s %d ENTRY\n", __func__, __LINE__);

    /* Create the Session Entry */
    pSessionEntry = (tLdpSession *) MemAllocMemBlk (LDP_SSN_POOL_ID);
    if (pSessionEntry == NULL)
    {
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: MemAlloc Failed for Ldp Session\n");
        return LDP_FAILURE;
    }

    /* Initialising the Session's MemBlock to Zero */
    MEMSET ((VOID *) pSessionEntry, LDP_ZERO, sizeof (tLdpSession));

    /* Initialise the Session Entry */
    TMO_HASH_INIT_NODE ((tTMO_HASH_NODE *) (&(pSessionEntry->NextHashNode)));
    TMO_SLL_Init (&pSessionEntry->AdjacencyList);
    TMO_SLL_Init (&pSessionEntry->USLspCtrlBlkList);
    TMO_SLL_Init (&pSessionEntry->DSLspCtrlBlkList);
    TMO_SLL_Init (&pSessionEntry->IntAtmLblRngList);

    pSessionEntry->u4Index = LDP_ONE;
    /* Addr acting as the unique Index */
    pSessionEntry->u4SsnTcpIfIndex = LDP_INVALID_IFINDEX;
    MEMCPY (pSessionEntry->SessionId, pLdpEntity->LdpId, LDP_MAX_LDPID_LEN);
    pSessionEntry->u1SessionRole = u1SessionRole;
    pSessionEntry->u1SessionState = LDP_SSM_ST_NON_EXISTENT;
    pSessionEntry->u1SessionInitStatus = LDP_SSN_TCP_NON_INIT;
    MplsLdpSessionUpdateSysTime (pSessionEntry);
    pSessionEntry->pLdpPeer = pLdpPeer;
    pSessionEntry->u4TcpConnId = LDP_INVALID_CONN_ID;
    pSessionEntry->u1StatusRecord = LDP_STAT_TYPE_LBL_RSRC_AVBL;

#ifdef MPLS_LDP_BFD_WANTED
    pSessionEntry->i4SessBfdStatus = LDP_ZERO;
#endif
    pSessionEntry->CmnSsnParams.u2ProtocolVer =
        (SSN_GET_ENTITY (pSessionEntry)->u2ProtVersion);

    pSessionEntry->u1StatusCode = LDP_STAT_TYPE_SUCCESS;
#ifdef LDP_GR_WANTED
    pSessionEntry->u1StaleStatus = LDP_FALSE;
#endif

    if ((LDP_ENTITY_LDP_OVER_RSVP_FLAG (pLdpEntity) == LDP_SNMP_TRUE) &&
        (pLdpEntity->OutStackTnlInfo.u4TnlId != LDP_ZERO))
    {
        if (LdpGetL3IfaceFromLdpOverRsvpOutTnl (pLdpEntity,
                                                &pSessionEntry->u4L3Intf)
            == LDP_FAILURE)

        {
            MEMSET ((UINT1 *) pSessionEntry, 0, sizeof (tLdpSession));
            MemReleaseMemBlock (LDP_SSN_POOL_ID, (UINT1 *) pSessionEntry);
            return LDP_FAILURE;
        }
    }

    /* Link the Session Entry to Peer */
    pLdpPeer->pLdpSession = pSessionEntry;
    LDP_ENTITY_UPDATE_ATTMPT_SESSION (pSessionEntry);
    LDP_DBG (LDP_PRCS_PRCS, "PRCS: Created New Ssn\n");
    MplsLdpPeerUpdateSysTime ();
    return LDP_SUCCESS;

}

/*****************************************************************************/
/* Function Name : LdpGetSsnStateEvent                                       */
/* Description   : This routine maps the Message arrived, to the appropriate */
/*                 Session State Event.                                      */
/*                                                                           */
/* Input(s)      : u2MsgType - Type of the Message received.                 */
/*                 pu1SsnFsmEvent - Resultant Session State Event is stored  */
/*                                  in this field.                           */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpGetSsnStateEvent (UINT2 u2MsgType, UINT1 *pu1SsnFsmEvent)
{
    switch (u2MsgType)
    {
        case LDP_INIT_MSG:
            *pu1SsnFsmEvent = LDP_SSM_EVT_RECV_INIT;
            break;

        case LDP_KEEP_ALIVE_MSG:
            *pu1SsnFsmEvent = LDP_SSM_EVT_KEEP_ALIVE_MSG;
            break;

        case LDP_NOTIF_MSG:
            *pu1SsnFsmEvent = LDP_SSM_EVT_RECV_NOTIF_MSG;
            break;

        case LDP_ADDR_MSG:
        case LDP_ADDR_WITHDRAW_MSG:
        case LDP_LBL_MAPPING_MSG:
        case LDP_LBL_REQUEST_MSG:
        case LDP_LBL_WITHDRAW_MSG:
        case LDP_LBL_RELEASE_MSG:
        case LDP_LBL_ABORT_REQ_MSG:
            *pu1SsnFsmEvent = LDP_SSM_EVT_OTH_LDP_MSG;
            break;
        case LDP_ICCP_RG_CONNECT_MSG:
        case LDP_ICCP_RG_DISCONNECT_MSG:
        case LDP_ICCP_RG_NOTIFICATION_MSG:
        case LDP_ICCP_RG_DATA_MSG:
            *pu1SsnFsmEvent = LDP_SSM_EVT_ICCP_MSG;
            break;
    }
}

/*****************************************************************************/
/* Function Name : LdpIsAdjEntryPresent                                      */
/* Description   : This routine checks if, for the given Session Entry, there*/
/*                 exists an Adjacency Entry, with a given u4IfIndex.        */
/*                                                                           */
/* Input(s)      : pSsnEntry - Points to the session whose adjacency list    */
/*                             needs to be searched.                         */
/*                 u4IfIndex - Interface Index over which the Hello pkt is   */
/*                             received.                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_TRUE or LDP_FALSE                                     */
/*****************************************************************************/

UINT1
LdpIsAdjEntryPresent (tLdpSession * pSsnEntry, UINT4 u4IfIndex,
                      UINT1 u1IPV6HelloRcvd)
{
    UINT1               u1AdjType;
    tLdpAdjacency      *pAdjEntry = NULL;
    LDP_DBG1 (LDP_PRCS_PRCS,
              " LdpIsAdjEntryPresent with Hello Type(V6:1 V4:0) %x \n",
              u1IPV6HelloRcvd);
#ifdef MPLS_IPV6_WANTED
    if (LDP_TRUE == u1IPV6HelloRcvd)
    {
        u1AdjType = LDP_ADDR_TYPE_IPV6;
    }
    else
    {
#endif
        u1AdjType = LDP_ADDR_TYPE_IPV4;
#ifdef MPLS_IPV6_WANTED
    }
#endif
    TMO_SLL_Scan (&(pSsnEntry->AdjacencyList), pAdjEntry, tLdpAdjacency *)
    {
        if (pAdjEntry->u4IfIndex == u4IfIndex
            && pAdjEntry->u1AddrType == u1AdjType)
        {
            return LDP_TRUE;    /* Adj found */
        }
    }
    return LDP_FALSE;            /* No Adj Entry with matching u4IfIndex */
}

#ifdef LDP_GR_WANTED
/*****************************************************************************/
/* Function Name : LdpRefreshStaleLdpSession                                 */
/* Description   : This routine does the following things:                   */
/*                 1. Decide the role of the session (ACTIVE or PASSIVE)     */
/*                 2. Initiate the socket call for ACTIVE role               */
/*                 3. Start the timer for all adjacencies in the stale       */
/*                    session                                                */
/*                                                                           */
/* Input(s)      : pLdpSession - Points to the session which is stale        */
/*                 pLdpEntity - Entity for the session                       */
/*                 u1TrAddrPresent- Flag to indicate the presence of TrAdd   */
/*                 TLV                                                       */
/*                 u4TrAddr- Transport Address of the peer                   */
/*                 u4PeerAddr- Peer Address                                  */
/*                 u4IfIndex- If Index on which Hello is received            */
/*                                                                           */
/*                 u4IfIndex - Interface Index over which the Hello pkt is   */
/*                             received.                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpRefreshStaleLdpSession (tLdpSession * pLdpSession, tLdpEntity * pLdpEntity,
                           UINT1 u1TrAddrPresent, UINT4 u4TrAddr,
                           UINT4 u4PeerAddr, UINT4 u4IfIndex)
{
    tLdpAdjacency      *pLdpAdj = NULL;
    UINT4               u4TempTrAddr = 0;
    UINT4               u4TempLocalAddr = 0;
    UINT1               u2IncarnId = MPLS_DEF_INCARN;
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    UINT1               u1SsnRole = 0;
    UINT4               u4AssocIf = 0;
    UINT1               u1PeerIndex = 0;
    UINT1               u1IsPassivePeerPre = LDP_FALSE;

    if (u1TrAddrPresent == LDP_TRUE)
    {
        u4TempTrAddr = OSIX_HTONL (u4TrAddr);
    }
    else
    {
        u4TempTrAddr = OSIX_HTONL (u4PeerAddr);
    }

    if ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) == LDP_TRUE) &&
        (LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) == INTERFACE_ADDRESS_TYPE))
    {
        if (LdpGetLdpEntityIfEntry (u2IncarnId, pLdpEntity,
                                    u4IfIndex,
                                    &pLdpIfTableEntry) == LDP_FAILURE)
        {
            LDP_DBG (GRACEFUL_DEBUG,
                     "LdpRefreshStaleLdpSession: Error in Getting the LdpIfEntry \n");
            return LDP_FAILURE;
        }

        CONVERT_TO_INTEGER (pLdpIfTableEntry->IfAddr, u4TempLocalAddr);
        u4TempLocalAddr = OSIX_NTOHL (u4TempLocalAddr);

        if (u4TempLocalAddr == LDP_ZERO)
        {
            CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4AssocIf);
            CfaGetIfIpAddr ((INT4) u4AssocIf, &u4TempLocalAddr);
        }
    }
    else
    {
        u4TempLocalAddr = LDP_ENTITY_TRANSPORT_ADDRESS (pLdpEntity);
    }

    if (u4TempLocalAddr > u4TempTrAddr)
    {
        LDP_DBG (GRACEFUL_DEBUG,
                 "LdpRefreshStaleLdpSession: Role for Session is ACTIVE\n");
        u1SsnRole = LDP_ACTIVE;
    }
    else
    {
        LDP_DBG (GRACEFUL_DEBUG,
                 "LdpRefreshStaleLdpSession: Role for Session is PASSIVE\n");
        u1SsnRole = LDP_PASSIVE;
    }

    u4TempTrAddr = OSIX_NTOHL (u4TempTrAddr);
    MEMCPY ((UINT1 *) &(pLdpSession->pLdpPeer->TransAddr),
            (UINT1 *) &(u4TempTrAddr), LDP_NET_ADDR_LEN);

    u4TempLocalAddr = OSIX_HTONL (u4TempLocalAddr);
    MEMCPY (pLdpSession->SrcTransAddr.Addr.au1Ipv4Addr, &u4TempLocalAddr,
            LDP_IPV4ADR_LEN);
    pLdpSession->SrcTransAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;

    if (pLdpSession->pLdpPeer->u1GrProgressState ==
        LDP_PEER_GR_RECONNECT_IN_PROGRESS)
    {
        pLdpSession->pLdpPeer->u1GrProgressState =
            LDP_PEER_GR_RECOVERY_IN_PROGRESS;

        LDP_DBG4 (GRACEFUL_DEBUG,
                  "LdpRefreshStaleLdpSession: The Ldp Peer %d.%d.%d.%d "
                  "is set to GR Recovery in Progress State\n",
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);
    }

    if (u1SsnRole == LDP_ACTIVE)
    {
        if (pLdpSession->u4TcpConnId == LDP_INVALID_CONN_ID)
        {
            LDP_DBG (GRACEFUL_DEBUG,
                     "LdpRefreshStaleLdpSession: Ssn is being Estd with LdpPeer\n");
            LDP_SESSION_FSM[LDP_SSM_ST_NON_EXISTENT] (pLdpSession,
                                                      LDP_SSM_EVT_INT_INIT_CON,
                                                      NULL);
        }
    }
    else
    {

        /* Entity is acting passive. Adding PeerEntry to PassivePeers list
           maintained at IncarnLevel. */

        for (u1PeerIndex = 0; u1PeerIndex < LDP_MAX_PASSIVE_PEERS;
             u1PeerIndex++)
        {
            if (LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex) ==
                pLdpSession->pLdpPeer)
            {
                u1IsPassivePeerPre = LDP_TRUE;
                break;
            }
        }

        if (u1IsPassivePeerPre == LDP_FALSE)
        {
            LDP_DBG (GRACEFUL_DEBUG,
                     "LdpRefreshStaleLdpSession: Adding Peer to Passive Peer List\n");

            for (u1PeerIndex = 0; u1PeerIndex < LDP_MAX_PASSIVE_PEERS;
                 u1PeerIndex++)
            {
                if (LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex) == NULL)
                {
                    LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex) =
                        pLdpSession->pLdpPeer;
                    break;
                }
            }
            if (u1PeerIndex == LDP_MAX_PASSIVE_PEERS)
            {
                LDP_DBG (GRACEFUL_DEBUG,
                         "LdpRefreshStaleLdpSession: PassivePeerList at IncarnLevel is Full.\n");
                return LDP_FAILURE;
            }
        }
    }

    /* Marking the Session as LDP_SSN_TCP_INIT */
    /* pLdpSession->u1SessionInitStatus = LDP_SSN_TCP_INIT; */

    LDP_DBG (GRACEFUL_DEBUG,
             "LdpRefreshStaleLdpSession: Starting the Adjacency Timers for "
             "all the Adjacencies in the STALE session\n");

    /* Start the Adjaceny Timers for all the adjacencies */
    TMO_SLL_Scan (&(pLdpSession->AdjacencyList), pLdpAdj, tLdpAdjacency *)
    {
        TmrStartTimer (LDP_TIMER_LIST_ID,
                       (tTmrAppTimer *) & (pLdpAdj->PeerAdjTimer.AppTimer),
                       (pLdpAdj->u4HoldTimeRem *
                        SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
    }

    /* Explicitly not marked the session as unstale here */
    /* Session will be marked as unstale when the session will 
     * become operational. This will avoid any state of the session
     * such as the session has become unstale and yet to become operational.
     * Session will become operational only after it has become Operational */
    LDP_DBG (GRACEFUL_DEBUG, "LdpRefreshStaleLdpSession: EXIT\n");
    return LDP_SUCCESS;
}

#ifdef MPLS_IPV6_WANTED
UINT1
LdpRefreshStaleIpv6LdpSession (tLdpSession * pLdpSession,
                               tLdpEntity * pLdpEntity, tIp6Addr * pPeerAddr,
                               UINT4 u4IfIndex, UINT1 u1RemoteFlag)
{
    tLdpAdjacency      *pLdpAdj = NULL;
    UINT1               u2IncarnId = MPLS_DEF_INCARN;
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    tIp6Addr            Ipv6TempLocalAddr;
    tIp6Addr            V6TempZeroAddr;
    UINT1               u1PeerIndex = 0;
    UINT1               u1SsnRole = LDP_PASSIVE;
    UINT4               u1IsPassivePeerPre = LDP_FALSE;

    MEMSET (&Ipv6TempLocalAddr, 0, sizeof (tIp6Addr));
    MEMSET (&V6TempZeroAddr, 0, sizeof (tIp6Addr));

    if ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) != LDP_TRUE) ||
        (LDP_ZERO ==
         MEMCMP (LDP_ENTITY_V6TRANSPORT_ADDRESS (pLdpEntity),
                 V6TempZeroAddr.u1_addr, LDP_IPV6ADR_LEN)))
    {
        if (LdpGetLdpEntityIfEntry (LDP_CUR_INCARN, pLdpEntity,
                                    u4IfIndex,
                                    &pLdpIfTableEntry) == LDP_FAILURE)
        {
            LDP_DBG (LDP_PRCS_PRCS, "Trgd Hello Rcvd in Wrong Interface \n");
            return LDP_FAILURE;
        }
        MEMCPY (Ipv6TempLocalAddr.u1_addr,
                pLdpIfTableEntry->LnklocalIpv6Addr.u1_addr, LDP_IPV6ADR_LEN);
        LDP_DBG (LDP_SSN_PRCS,
                 "V6 Hello Rcvd: Link Local Addr configured may be used as SrcTransAddress \n");
        if ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) == LDP_TRUE)
            && (LDP_ENTITY_IPV6TRANSPORT_ADDRKIND (pLdpEntity) ==
                INTERFACE_ADDRESS_TYPE))
        {
            /* In Basic Entity, If TransAddr as INTERFACE is enabled,
             *                          * Session should be created with Link Local IfAddr */
        }

        if (u1RemoteFlag)
        {
            /* Target Hello Rcvd: Tcp Connection should be established bw  Glbl Unicast Addresses */
            if (LDP_ZERO ==
                MEMCMP (pLdpIfTableEntry->GlbUniqueIpv6Addr.u1_addr,
                        V6TempZeroAddr.u1_addr, LDP_IPV6ADR_LEN))
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "Tgt V6 Hello Rcvd: Glbl Unicast Addr is not configured in IfTableEntry Hello Discarded\n");
                return LDP_FAILURE;
            }
            MEMCPY (Ipv6TempLocalAddr.u1_addr,
                    pLdpIfTableEntry->GlbUniqueIpv6Addr.u1_addr,
                    LDP_IPV6ADR_LEN);
        }

        if (LDP_ZERO ==
            MEMCMP (Ipv6TempLocalAddr.u1_addr, V6TempZeroAddr.u1_addr,
                    LDP_IPV6ADR_LEN))
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "Case: Unnumbered Interfaces Not Supported forIPV6 Return Handle Hello\n");
            return LDP_FAILURE;
        }
    }
    else
    {
        MEMCPY (Ipv6TempLocalAddr.u1_addr,
                LDP_ENTITY_V6TRANSPORT_ADDRESS (pLdpEntity), LDP_IPV6ADR_LEN);
    }

    MEMCPY (LDP_IPV6_ADDR (pLdpSession->SrcTransAddr.Addr),
            Ipv6TempLocalAddr.u1_addr, LDP_IPV6ADR_LEN);
    pLdpSession->SrcTransAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
    pLdpSession->u4SsnTcpIfIndex = u4IfIndex;

    if (pLdpSession->pLdpPeer->u1GrProgressState ==
        LDP_PEER_GR_RECONNECT_IN_PROGRESS)
    {
        pLdpSession->pLdpPeer->u1GrProgressState =
            LDP_PEER_GR_RECOVERY_IN_PROGRESS;

        LDP_DBG4 (GRACEFUL_DEBUG,
                  "LdpRefreshStaleIpv6LdpSession: The Ldp Peer %d.%d.%d.%d "
                  "is set to GR Recovery in Progress State\n",
                  pLdpSession->pLdpPeer->PeerLdpId[0],
                  pLdpSession->pLdpPeer->PeerLdpId[1],
                  pLdpSession->pLdpPeer->PeerLdpId[2],
                  pLdpSession->pLdpPeer->PeerLdpId[3]);
    }

    if (MEMCMP (Ipv6TempLocalAddr.u1_addr, pPeerAddr->u1_addr, LDP_IPV6ADR_LEN)
        > LDP_ZERO)
    {
        u1SsnRole = LDP_ACTIVE;
    }

    if (u1SsnRole == LDP_ACTIVE)
    {
        if (pLdpSession->u4TcpConnId == LDP_INVALID_CONN_ID)
        {
            LDP_DBG (GRACEFUL_DEBUG,
                     "LdpRefreshStaleIpv6LdpSession: Ssn is being Estd with LdpPeer\n");
            LDP_SESSION_FSM[LDP_SSM_ST_NON_EXISTENT] (pLdpSession,
                                                      LDP_SSM_EVT_INT_INIT_CON,
                                                      NULL);
        }
    }
    else
    {

        /* Entity is acting passive. Adding PeerEntry to PassivePeers list
           maintained at IncarnLevel. */
        for (u1PeerIndex = 0; u1PeerIndex < LDP_MAX_PASSIVE_PEERS;
             u1PeerIndex++)
        {
            if (LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex) ==
                pLdpSession->pLdpPeer)
            {
                u1IsPassivePeerPre = LDP_TRUE;
                break;
            }
        }

        if (u1IsPassivePeerPre == LDP_FALSE)
        {
            LDP_DBG (GRACEFUL_DEBUG,
                     "LdpRefreshStaleIpv6LdpSession: Adding Peer to Passive Peer List\n");

            for (u1PeerIndex = 0; u1PeerIndex < LDP_MAX_PASSIVE_PEERS;
                 u1PeerIndex++)
            {
                if (LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex) == NULL)
                {
                    LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex) =
                        pLdpSession->pLdpPeer;
                    break;
                }
            }

            if (u1PeerIndex == LDP_MAX_PASSIVE_PEERS)
            {
                LDP_DBG (GRACEFUL_DEBUG,
                         "LdpRefreshStaleIpv6LdpSession: PassivePeerList at IncarnLevel is Full.\n");
                return LDP_FAILURE;
            }
        }
    }
    /* pLdpSession->u1SessionInitStatus = LDP_SSN_TCP_INIT; */

    LDP_DBG (GRACEFUL_DEBUG,
             "LdpRefreshStaleIpv6LdpSession: Starting the Adjacency Timers for "
             "all the Adjacencies in the STALE session\n");

    /* Start the Adjaceny Timers for all the adjacencies */
    TMO_SLL_Scan (&(pLdpSession->AdjacencyList), pLdpAdj, tLdpAdjacency *)
    {
        TmrStartTimer (LDP_TIMER_LIST_ID,
                       (tTmrAppTimer *) & (pLdpAdj->PeerAdjTimer.AppTimer),
                       (pLdpAdj->u4HoldTimeRem *
                        SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
    }

    return LDP_SUCCESS;
}
#endif
#endif

/* MPLS_IPv6 add start*/
/*****************************************************************************/
/* Function Name : LdpNeInitTcpConn                                           */
/* Description   : Routine to initiate LDP Session State Machine with event*/
/*..................... INIT CONN if session role is Active else adding the Peer */
/*..................... in Passive Entry */
/* Input(s)      : pSessionEntry       - Points to the Session Entry.      */
/*                     u1SsnRole  - SessionRole (Active / Passive).  */
/*                     u2IncarnId - Current Incarnation Id */
/*                     u2IncarnId - Incarnation Id                               */
/* Output(s)    : None                                                      */
/* Return(s)    : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

UINT1
LdpNeInitTcpConn (tLdpSession * pSessionEntry, UINT1 u1SsnRole,
                  UINT2 u2IncarnId)
{
    UINT1               u1PeerIndex;
    /* Initiate Session Estab, in case Entity is acting in Acitve Mode. */
    if (u1SsnRole == LDP_ACTIVE)
    {
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "LdpNeInitTcpConn: SsnRole Active  Addr Fam: %x \n",
                  pSessionEntry->pLdpPeer->TransAddr.u2AddrType);
        LDP_SESSION_FSM[LDP_SSM_ST_NON_EXISTENT] (pSessionEntry,
                                                  LDP_SSM_EVT_INT_INIT_CON,
                                                  NULL);
    }
    else
    {                            /* Entity is acting passive. Adding 
                                   PeerEntry to PassivePeers list 
                                   maintained at IncarnLevel. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "LdpNeInitTcpConn: SsnRole Passive  Addr Fam: %x \n",
                  pSessionEntry->pLdpPeer->TransAddr.u2AddrType);
        for (u1PeerIndex = 0; u1PeerIndex < LDP_MAX_PASSIVE_PEERS;
             u1PeerIndex++)
        {
            if (LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex) == NULL)
            {
                LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex) =
                    pSessionEntry->pLdpPeer;
                break;
            }
        }
        if (u1PeerIndex == LDP_MAX_PASSIVE_PEERS)
        {
            LDP_DBG (LDP_PRCS_PRCS,
                     "PRCS: PassivePeerList at IncarnLevel is Full.\n");
            return LDP_FAILURE;
        }
    }
    pSessionEntry->u1SessionInitStatus = LDP_SSN_TCP_INIT;
    return LDP_SUCCESS;

}

UINT1
LdpGetV4SsnRole (tLdpPeer * pLdpPeer, tGenAddr PeerAddr, UINT4 u4TempLocalAddr,
                 UINT1 u1RemoteFlag)
{
    UINT4               u4TempTrAddr = 0;
    UINT1               u1SsnRole = LDP_PASSIVE;
    MEMCPY (&u4TempTrAddr, LDP_IPV4_ADDR (PeerAddr.Addr), LDP_IPV4ADR_LEN);
    u4TempTrAddr = OSIX_NTOHL (u4TempTrAddr);
    if (u1RemoteFlag == LDP_FALSE)
    {
        MEMCPY (LDP_IPV4_ADDR (pLdpPeer->NetAddr.Addr),
                LDP_IPV4_ADDR (PeerAddr.Addr), LDP_NET_ADDR_LEN);
        pLdpPeer->NetAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
        LDP_DBG4 (LDP_SSN_PRCS,
                  "LdpGetV4SsnRole PLDPPEER NETADDR %x:%x:%x:%x\n",
                  pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                  pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                  pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                  pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);
    }
    MEMCPY (LDP_IPV4_ADDR (pLdpPeer->TransAddr.Addr),
            LDP_IPV4_ADDR (PeerAddr.Addr), LDP_NET_ADDR_LEN);
    pLdpPeer->TransAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
    LDP_DBG4 (LDP_SSN_PRCS,
              "LdpGetV4SsnRole: (DEST ADD) PEER-TRADDR: %x:%x:%x:%x\n",
              pLdpPeer->TransAddr.Addr.au1Ipv4Addr[0],
              pLdpPeer->TransAddr.Addr.au1Ipv4Addr[1],
              pLdpPeer->TransAddr.Addr.au1Ipv4Addr[2],
              pLdpPeer->TransAddr.Addr.au1Ipv4Addr[3]);
    LDP_DBG2 (LDP_SSN_PRCS,
              "Compared Peer Tr Address: %x and Source Tr Address:%x \n",
              u4TempTrAddr, u4TempLocalAddr);
    if (u4TempLocalAddr > u4TempTrAddr)
    {
        u1SsnRole = LDP_ACTIVE;
    }
    return u1SsnRole;
}

/*****************************************************************************/
/* Function Name : LdpHandleIpv6UdpPkt                                           */
/* Description   : Routine to handle LDP PDU.                                */
/*                 This routine is called whenever the Interface Module      */
/*                 receives UDP Packet Arrival Event, from UDP.              */
/* Input(s)      : pPDU       - Points to the Ldp Pdu that is received.      */
/*                 u4IfIndex  - Iface Index over which Ldp Pdu is received.  */
/*                 PeerAddress - Source Addr of IPV6 Peer from which the Hello Pkt */
/*                              is Rx. This Addr is assigned to Peer's Ntw   */
/*                              Addr, when TrAddrTlv is not present in Hello.*/
/*                 u2IncarnId - Incarnation Id                               */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
#if 0
VOID
LdpPrintTime ()
{
    UINT1               au1TimeString[9];
    UINT4               u4TimeTicks = 0;

    MEMSET (au1TimeString, 0, 9);

    OsixGetSysTime (&u4TimeTicks);

    CliL3VpnConvertTimetoString (u4TimeTicks, au1TimeString);
    printf ("Time: %s\n", au1TimeString);
    return;
}
#endif

#ifdef MPLS_IPV6_WANTED
UINT1
LdpHandleIpv6UdpPkt (tCRU_BUF_CHAIN_HEADER * pPDU, UINT4 u4Port,
                     tIpAddr PeerAddress, UINT2 u2IncarnId,
                     UINT1 u1DiscardV6LHello)
{
    UINT1              *pMsg = NULL;
    UINT1               u1StatusType = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2Offset = 0;
    UINT2               u2MsgType = 0;
    UINT2               u2MsgLen = 0;
    UINT4               u4IfIndex = 0;
    INT4                i4DbgMsgDir = 0;
    tLdpPduHdr          PduHdr;
    tLdpMsgHdrAndId     MsgHdr;    /* will be accessing only the msg hdr part */
    /* MPLS_IPv6 mod start */
    tGenAddr            PeerAddr;
    MEMSET (&PeerAddr, 0, sizeof (tGenAddr));
    MEMSET (&PduHdr, LDP_ZERO, sizeof (tLdpPduHdr));
    MEMSET (&MsgHdr, LDP_ZERO, sizeof (tLdpMsgHdrAndId));
    PeerAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
    MEMCPY (LDP_IPV6_ADDR (PeerAddr.Addr), PeerAddress.u1_addr,
            LDP_IPV6ADR_LEN);
    /* MPLS_IPv6 mod end */
    u2BufSize = (UINT2) CRU_BUF_Get_ChainValidByteCount (pPDU);

    if (((gu4LdpDumpType & LDP_DUMP_HELLO) == LDP_DUMP_HELLO) &&
        ((gu4LdpDumpDir == DUMP_DIR_INOUT)
         || (gu4LdpDumpDir == LDP_DUMP_DIR_IN)))
    {
        i4DbgMsgDir = LDP_DUMP_DIR_IN;

        LdpDumpLdpPdu (pPDU, i4DbgMsgDir);
    }

    /* LDP Table Entries are created using CFA Index But recvmsg
     * will give IP index so, we are getting CFA Index from IP Index */
    if (NetIpv6GetCfaIfIndexFromPort (u4Port, &u4IfIndex) == NETIPV4_FAILURE)
    {
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "PRCS: Port V6 Index Mapping Failed Port =%d \n", u4Port);
        return LDP_FAILURE;

    }

    /* Copy the Pdu Hdr to PduHdr Structure */
    if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &PduHdr, 0, LDP_PDU_HDR_LEN)
        != LDP_PDU_HDR_LEN)
    {
        LDP_DBG (LDP_PRCS_MEM,
                 "PRCS: Error while copying IPV6 Pdu Hdr from chain Buf\n");
        return LDP_FAILURE;
    }

    if (LdpValidatePduHdr (&PduHdr, NULL, u2BufSize, &u1StatusType) !=
        LDP_SUCCESS)
    {
        /* Error: Received PDU with invalid Header */
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: Rx IPV6 Pdu with Invalid Header \n");
        return LDP_FAILURE;
    }

    u2BufSize -= (UINT2) LDP_PDU_HDR_LEN;
    u2Offset = LDP_PDU_HDR_LEN;

    /* Copy the Msg hdr from buffer */
    if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &MsgHdr, u2Offset,
                                   LDP_MSG_HDR_LEN) != LDP_MSG_HDR_LEN)
    {
        LDP_DBG (LDP_PRCS_MEM,
                 "PRCS: Error copying Msg Hdr from the chain Buf\n");
        return LDP_FAILURE;
    }

    u2MsgType = OSIX_NTOHS (MsgHdr.u2MsgType);
    u2MsgLen = OSIX_NTOHS (MsgHdr.u2MsgLen);

    if (u2MsgType != LDP_HELLO_MSG)
    {
        LDP_DBG (LDP_PRCS_PRCS,
                 "PRCS: Unknown Msg in IPV6 Udp Pkt. Stop Processing \n");
        return LDP_FAILURE;
    }

    if (LdpValidateMsgHdr (u2BufSize, u2MsgLen, u2MsgType,
                           &u1StatusType) != LDP_SUCCESS)
    {
        LDP_DBG (LDP_PRCS_PRCS,
                 "PRCS: Rx IPV6 Pdu with invalid Msg header \n ");
        return LDP_FAILURE;
    }

    pMsg = (UINT1 *) MemAllocMemBlk (LDP_MSG_POOL_ID);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_PRCS_MEM, "PRCS: Mem Alloc from Msg MemPool Failed\n");
        return LDP_FAILURE;
    }
    /* Copy one msg from the chain to linear buffer */
    if (CRU_BUF_Copy_FromBufChain (pPDU, pMsg, u2Offset,
                                   (UINT4) (u2MsgLen + LDP_MSG_HDR_LEN)) !=
        (u2MsgLen + LDP_MSG_HDR_LEN))
    {
        LDP_DBG (LDP_PRCS_MEM,
                 "PRCS: Error copying from chain Buf to linear Blk\n");
        MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
        return LDP_FAILURE;
    }
    /* MPLS_IPv6 mod start */
    LdpHandleHelloMsg (pMsg, PduHdr.LdpId, PeerAddr, u4IfIndex, u2IncarnId,
                       u1DiscardV6LHello);
    /* MPLS_IPv6 mod start */
    MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleTcp6Pkt                                           */
/* Description   : Routine to handle LDP PDU.                                */
/*                 This routine is called whenever the Interface Module      */
/*                 receives a TCP Packet Arrival Event, from TCP.            */
/*                 It extracts all the messages (normally one message) that  */
/*                 are present in the LDP PDU, examines the message type of  */
/*                 each message and calls the appropriate Session Handling   */
/*                 routines.                                                 */
/* Input(s)      : pPDU       - Points to the Ldp Pdu that is received.      */
/*                 u4TcpConnId - Tcp connection Id on which this Ldp Pdu is  */
/*                               received.                                   */
/*                 u2IncarnId - Incarnatin Id                                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpHandleTcp6Pkt (tCRU_BUF_CHAIN_HEADER * pPDU, UINT4 u4TcpConnId,
                  tIp6Addr PeerAddr, UINT2 u2IncarnId)
{
    UINT1              *pMsg = NULL;
    UINT1               u1StatusType = 0;
    UINT1               u1SsnState = 0;
    UINT1               u1SsnFsmEvent = LDP_MAX_SESSION_EVENTS;    /* Valid 
                                                                   FsmEvent vals
                                                                   are upto
                                                                   LDP_MAX_SESSI
                                                                   ON_EVENTS - 1
                                                                 */
    UINT2               u2Len = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2Offset = 0;
    UINT2               u2MsgType = 0;
    UINT2               u2InitMsgType = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2PduLen = 0;
    UINT1               u1InitMsgFlag = LDP_FALSE;
    UINT1               u1CmnSsnTlvFlag = LDP_FALSE;
    UINT1               u1PeerIndex;
    INT4                i4DbgMsgDir = 0;
    tLdpId              RcvrLdpId;
    tLdpPduHdr          PduHdr;
    tLdpMsgHdrAndId     MsgHdr;    /* will be accessing only the msg hdr part */
    tLdpSession        *pSessionEntry = NULL;
    tIp6Addr            Ipv6Addr;
    tIp6Addr            Ipv6PeerAddr;
    tLdpPeer           *pTmpPeer = NULL;
    UINT4               u4RelinquishCtr = 0;

    MEMSET (&Ipv6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&Ipv6PeerAddr, 0, sizeof (tIp6Addr));
    MEMSET (&PduHdr, LDP_ZERO, sizeof (tLdpPduHdr));
    MEMSET (&MsgHdr, LDP_ZERO, sizeof (tLdpMsgHdrAndId));
    if ((gu4LdpDumpType & ~(UINT4) (LDP_DUMP_HELLO)) &&
        ((gu4LdpDumpDir == DUMP_DIR_INOUT)
         || (gu4LdpDumpDir == LDP_DUMP_DIR_IN)))
    {
        i4DbgMsgDir = LDP_DUMP_DIR_IN;
        LdpDumpLdpPdu (pPDU, i4DbgMsgDir);
    }

    u2BufSize = (UINT2) CRU_BUF_Get_ChainValidByteCount (pPDU);
    LDP_DBG (LDP_PRCS_PRCS, "PRCS: Processing LDP PDU Rx on TCP.\n");

    /* 
     * Invoked to dump LDP_DEF_MAX_PDU_LEN Length.  Dump should contain the
     * rcvd LDP PDU contents, which will be less than the LDP_DEF_MAX_PDU_LEN
     * length.
     */
    /* Copy the Pdu Hdr to PduHdr Structure */
    if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &PduHdr, 0, LDP_PDU_HDR_LEN)
        != LDP_PDU_HDR_LEN)
    {
        LDP_DBG (LDP_PRCS_MEM,
                 "PRCS: Error copying IPV6 Pdu Hdr from chain buffer\n");
        return LDP_FAILURE;
    }
    pSessionEntry = LdpGetSsnFromTcpConnTbl (u2IncarnId, u4TcpConnId);
    if (pSessionEntry == NULL)
    {
        if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &u2InitMsgType,
                                       LDP_PDU_HDR_LEN, LDP_MSG_TYPE_LEN) ==
            LDP_MSG_TYPE_LEN)
        {
            if (OSIX_NTOHS (u2InitMsgType) == LDP_INIT_MSG)
            {
                u1InitMsgFlag = LDP_TRUE;
                MEMCPY (Ipv6Addr.u1_addr, PeerAddr.u1_addr, LDP_IPV6ADR_LEN);
                LDP_DBG1 (LDP_PRCS_MEM, "PRCS: Recvd Init Msg from %s\n",
                          Ip6PrintAddr (&(Ipv6Addr)));
            }
        }
        else
        {
            LDP_DBG (LDP_PRCS_MEM,
                     "PRCS: Error in Copying InitMsgType From Buffer\n");
            return LDP_FAILURE;
        }

        if (u1InitMsgFlag == LDP_TRUE)
        {
            u2PduLen = OSIX_NTOHS (PduHdr.u2PduLen);
            if ((u2BufSize < LDP_MIN_PDU_LEN)
                || (u2BufSize > LDP_DEF_MAX_PDU_LEN) || (u2PduLen == 0)
                || (u2PduLen < LDP_MIN_PDU_LEN)
                || (u2PduLen > LDP_DEF_MAX_PDU_LEN))
            {
                u1StatusType = LDP_STAT_TYPE_BAD_PDU_LEN;
            }

            /* Check if recvd msg contains mandatory Cmn Ssn Params Tlv */
            if ((u1StatusType == LDP_ZERO) &&
                (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &u2TlvType,
                                            (LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                             LDP_MSG_ID_LEN),
                                            LDP_TLV_TYPE_LEN) ==
                 LDP_TLV_TYPE_LEN))
            {
                if (OSIX_NTOHS (u2TlvType) == LDP_CMN_SSN_PARAM_TLV)
                {
                    u1CmnSsnTlvFlag = LDP_TRUE;
                }
            }

            /* Get Receiver's LdpId from the received PDU carrying init msg */
            if ((CRU_BUF_Copy_FromBufChain (pPDU, RcvrLdpId, RCVR_LDPID_OFFSET,
                                            LDP_MAX_LDPID_LEN) !=
                 LDP_MAX_LDPID_LEN) &&
                (u1StatusType == LDP_ZERO) && (u1CmnSsnTlvFlag == LDP_TRUE))
            {
                u1StatusType = LDP_STAT_TYPE_MALFORMED_TLV_VAL;
                LDP_DBG (LDP_PRCS_MEM,
                         "PRCS: Error in Copying RcvrLdpId From Buffer. TLV is malformed\n");
            }

            for (u1PeerIndex = 0; u1PeerIndex < LDP_MAX_PASSIVE_PEERS;
                 u1PeerIndex++)
            {
                pTmpPeer = LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex);
                if ((pTmpPeer == NULL) || (pTmpPeer->pLdpSession == NULL))
                {
                    continue;
                }

                pSessionEntry = (tLdpSession *) pTmpPeer->pLdpSession;
                MEMCPY (Ipv6PeerAddr.u1_addr,
                        LDP_IPV6_ADDR (pTmpPeer->TransAddr.Addr),
                        LDP_IPV6ADR_LEN);
                if ((u1StatusType != LDP_ZERO)
                    &&
                    (MEMCMP
                     (Ipv6PeerAddr.u1_addr, PeerAddr.u1_addr,
                      LDP_IPV6ADR_LEN) == LDP_ZERO))
                {
                    /* Status type is already set as MALFORMED_TLV or
                     * BAD_PDU_LEN. So, No need to handle anything here */
                }
                else if ((u1CmnSsnTlvFlag != LDP_TRUE) &&
                         (MEMCMP
                          (Ipv6PeerAddr.u1_addr, PeerAddr.u1_addr,
                           LDP_IPV6ADR_LEN) == LDP_ZERO)
                         &&
                         !(MEMCMP
                           (pTmpPeer->PeerLdpId, PduHdr.LdpId,
                            LDP_MAX_LDPID_LEN)))
                {
                    /* Common session params is missing in Init msg */
                    u1StatusType = LDP_STAT_MISSING_MSG_PARAM;
                    LDP_DBG (LDP_PRCS_PRCS,
                             "PRCS: In PassiveMode, Rx InitMsg with No"
                             " CmnSsnParamsTlv\n");
                }
                else if ((MEMCMP
                          (Ipv6PeerAddr.u1_addr, PeerAddr.u1_addr,
                           LDP_IPV6ADR_LEN) == LDP_ZERO)
                         &&
                         (MEMCMP
                          (pTmpPeer->PeerLdpId, PduHdr.LdpId,
                           LDP_MAX_LDPID_LEN)))
                {
                    u1StatusType = LDP_STAT_TYPE_BAD_LDPID;
                    LDP_DBG (LDP_PRCS_PRCS, "PRCS: Bad LDP Identifier \n");
                }
                else if ((MEMCMP
                          (Ipv6PeerAddr.u1_addr, PeerAddr.u1_addr,
                           LDP_IPV6ADR_LEN) == LDP_ZERO)
                         &&
                         (MEMCMP
                          (RcvrLdpId, pTmpPeer->pLdpEntity->LdpId,
                           LDP_MAX_LDPID_LEN)))
                {
                    /* the Ldp id received in Hello is different than the one 
                       received in INIT Header msg */
                    u1StatusType = LDP_STAT_TYPE_NO_HELLO;
                    LDP_DBG (LDP_PRCS_PRCS,
                             "PRCS: No Hello received for this LDP Identifieri \n");
                }

                if (u1StatusType != 0)
                {
                    pSessionEntry->u4TcpConnId = u4TcpConnId;

                    if (u1StatusType == LDP_STAT_MISSING_MSG_PARAM)
                    {
                        LdpSendNotifMsg (pSessionEntry,
                                         (UINT1 *) &PduHdr, LDP_FALSE,
                                         u1StatusType, NULL);
                        LDP_DBG (LDP_PRCS_PRCS,
                                 "PRCS: CmnSsnParams TLV missing\n");
                    }
                    else if (LdpSsmSendNotifCloseSsn (pSessionEntry,
                                                      u1StatusType,
                                                      (UINT1 *) &PduHdr,
                                                      LDP_TRUE) != LDP_SUCCESS)
                    {
                        LDP_DBG (LDP_SSN_PRCS,
                                 "SESSION: Failed to send Notif Msg & close "
                                 "Session.\n");
                    }

                    return LDP_FAILURE;
                }
                else if (!(MEMCMP (pTmpPeer->PeerLdpId,
                                   PduHdr.LdpId, LDP_MAX_LDPID_LEN)) &&
                         !(MEMCMP (RcvrLdpId,
                                   pTmpPeer->pLdpEntity->LdpId,
                                   LDP_MAX_LDPID_LEN))
                         &&
                         (MEMCMP
                          (Ipv6PeerAddr.u1_addr, PeerAddr.u1_addr,
                           LDP_IPV6ADR_LEN) == LDP_ZERO))
                {
                    pSessionEntry->u1SessionState = LDP_SSM_ST_INITIALIZED;
                    pSessionEntry->u4TcpConnId = u4TcpConnId;
                    MplsLdpSessionUpdateSysTime (pSessionEntry);
                    LdpAddSsnToTcpConnTbl (pSessionEntry);
                    /* Removing the PassivePeer Entry from the Incarn */
                    LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex) = NULL;

                    break;
                }
            }

            if ((u1PeerIndex == LDP_MAX_PASSIVE_PEERS) &&
                (TMO_SLL_Count (&LDP_ENTITY_LIST (u2IncarnId)) != 0))
            {
                /* Socket should not be closed when ISS MPLS LDP is used as a
                 * test framework. If it is used as test framework, no entities are
                 * created. That's why the above check is added */
                LDP_DBG (LDP_SSN_PRCS,
                         "PRCS:Session not found. InitMsg not processed \n ");
                SockClose (u4TcpConnId);
                return LDP_FAILURE;
            }
        }
        else if (TMO_SLL_Count (&LDP_ENTITY_LIST (u2IncarnId)) != 0)
        {
            /* Socket should not be closed when ISS MPLS LDP is used as a
             * test framework. If it is used as test framework, no entities are
             * created. That's why the above check is added */
            SockClose (u4TcpConnId);
        }
    }
    if (pSessionEntry == NULL)
    {
        LDP_DBG (LDP_SSN_PRCS, "PRCS: Session not found.Pdu not processed \n ");
        return LDP_FAILURE;
    }

    if (LdpValidatePduHdr (&PduHdr, pSessionEntry, u2BufSize, &u1StatusType) !=
        LDP_SUCCESS)
    {
        /* Error: Received PDU with invalid Header */
        if (LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                     (UINT1 *) &PduHdr,
                                     LDP_TRUE) != LDP_SUCCESS)
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Failed to send Notif Msg & close " "Session.\n");
        }

        return LDP_FAILURE;
    }

    u2BufSize -= (UINT2) LDP_PDU_HDR_LEN;
    u2Offset = LDP_PDU_HDR_LEN;
    /* Extract Messages from the Buffer and store them on Linear buffers */
    for (u2Len = 0; u2Len < u2BufSize;
         u2Offset += (UINT2) (u2MsgLen + LDP_MSG_HDR_LEN))
    {
        /* Copy the Msg hdr from buffer */
        if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &MsgHdr, u2Offset,
                                       LDP_MSG_HDR_LEN) != LDP_MSG_HDR_LEN)
        {
            LDP_DBG (LDP_PRCS_MEM, "PRCS: Error copying MsgHdr from Buffer\n");
            return LDP_FAILURE;
        }

        u2MsgType = OSIX_NTOHS (MsgHdr.u2MsgType);
        u2MsgLen = OSIX_NTOHS (MsgHdr.u2MsgLen);
        if (LdpValidateMsgHdr ((UINT2) (u2BufSize - u2Len), u2MsgLen, u2MsgType,
                               &u1StatusType) != LDP_SUCCESS)
        {
            if (u1StatusType == LDP_STAT_TYPE_BAD_MSG_LEN)
            {
                /* Error: Received message with invalid Header */
                LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                         NULL, LDP_TRUE);
                return LDP_FAILURE;
            }
            else if (u1StatusType == LDP_STAT_TYPE_UNKNOWN_MSG_TYPE)
            {
                LdpSendNotifMsg (pSessionEntry, NULL, LDP_FALSE,
                                 u1StatusType, NULL);
                break;            /* Stop processing of PDU */
            }
            else if (u1StatusType == LDP_IGNORE_ERROR)
            {
                break;
            }
            else
            {
                u2Len += (UINT2) (u2MsgLen + LDP_MSG_HDR_LEN);    /* Go to next msg 
                                                                   in the buffer */
                continue;
            }
        }

        pMsg = (UINT1 *) MemAllocMemBlk (LDP_MSG_POOL_ID);
        if (pMsg == NULL)
        {
            LDP_DBG (LDP_PRCS_MEM, "PRCS: Mem Alloc from Msg MemPool Failed\n");
            return LDP_FAILURE;
        }

        /* Copy one msg from the chain to linear buffer */
        if (CRU_BUF_Copy_FromBufChain (pPDU, pMsg, u2Offset,
                                       (UINT4) (u2MsgLen + LDP_MSG_HDR_LEN)) !=
            (u2MsgLen + LDP_MSG_HDR_LEN))
        {
            LDP_DBG (LDP_PRCS_MEM,
                     "PRCS: Error copying Msg from Buf to LinearBlk\n");
            MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
            return LDP_FAILURE;
        }

        if (LdpValidateTlvHdr (pMsg, u2MsgLen, &u1StatusType) != LDP_SUCCESS)
        {
            if (u1StatusType == LDP_STAT_TYPE_UNKNOWN_TLV)
            {
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 u1StatusType, NULL);
            }
            else if (u1StatusType == LDP_STAT_BAD_TLV_LEN)
            {
                LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                         NULL, LDP_TRUE);
            }
            MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
            return LDP_FAILURE;
        }

        SSN_GET_SSN_STATE (pSessionEntry, u1SsnState);
        LdpGetSsnStateEvent (u2MsgType, &u1SsnFsmEvent);
        if (u1SsnFsmEvent < LDP_MAX_SESSION_EVENTS)
        {
            LDP_DBG2 (LDP_PRCS_SEM, "PRCS: SsnState  %s   Event  %s \n",
                      au1SsnState[u1SsnState], au1SsnEvents[u1SsnFsmEvent]);
        }
        LDP_SESSION_FSM[u1SsnState] (pSessionEntry, u1SsnFsmEvent, pMsg);
        MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
        u2Len += (UINT2) (u2MsgLen + LDP_MSG_HDR_LEN);    /* Go to next msg in 
                                                           the buffer */
        u4RelinquishCtr++;

        if (u4RelinquishCtr > LDP_RELINQUISH_CNTR)
        {
            LDP_DBG1 (LDP_IF_RX,
                      "LDPTCP: More than %d messages processed\n",
                      LDP_RELINQUISH_CNTR);

            u4RelinquishCtr = 0;

            /*Process the Timer Expiry Events */
            LdpProcessTimerEvents ();

            /*Process the Udp Messages */
            LdpProcessIpv6UdpMsgs ();
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : IsEntityDualStack                                           */
/* Description   : Routine to check if Entity is Having V6 Interface                                */
/* Input(s)      : pLdpEntity       - Entity to be checked for Dual Stack      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS     If Dual Stack                                          */
/*                      or LDP_FAILURE   If Not Dual Stack                                         */
/*****************************************************************************/

UINT1
IsEntityDualStack (tLdpEntity * pLdpEntity)
{
    tLdpIfTableEntry   *pLdpIfEntry;
    TMO_SLL_Scan (&(pLdpEntity->IfList), pLdpIfEntry, tLdpIfTableEntry *)
    {
        if (LDP_IFTYPE_DUAL == pLdpIfEntry->u1InterfaceType
            || LDP_IFTYPE_IPV6 == pLdpIfEntry->u1InterfaceType)
        {
            return LDP_SUCCESS;
            break;
        }
    }
    return LDP_FAILURE;
}

UINT1
LdpGetV6SsnRole (tLdpPeer * pLdpPeer, tGenAddr PeerAddr,
                 tIp6Addr Ipv6TempLocalAddr, UINT1 u1RemoteFlag)
{
    tIp6Addr            TempIpv6TrAddr;
    UINT1               u1SsnRole = LDP_PASSIVE;

    MEMSET (&TempIpv6TrAddr, 0, sizeof (tIp6Addr));
    MEMCPY (TempIpv6TrAddr.u1_addr, LDP_IPV6_ADDR (PeerAddr.Addr),
            LDP_IPV6ADR_LEN);
    if (u1RemoteFlag == LDP_FALSE)
    {
        MEMCPY (LDP_IPV6_ADDR (pLdpPeer->NetAddr.Addr), TempIpv6TrAddr.u1_addr,
                LDP_IPV6ADR_LEN);
        pLdpPeer->NetAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
    }
    MEMCPY (LDP_IPV6_ADDR (pLdpPeer->TransAddr.Addr), (TempIpv6TrAddr.u1_addr),
            LDP_IPV6ADR_LEN);
    pLdpPeer->TransAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
    LDP_DBG1 (LDP_SSN_PRCS, "PRCS: Source Add Compared for SSN ROLE: %s\n",
              Ip6PrintAddr (&Ipv6TempLocalAddr));
    LDP_DBG1 (LDP_SSN_PRCS, "PRCS: Peer Add Compared for SSN ROLE: %s\n",
              Ip6PrintAddr (&TempIpv6TrAddr));
    if (MEMCMP
        (Ipv6TempLocalAddr.u1_addr, TempIpv6TrAddr.u1_addr,
         LDP_IPV6ADR_LEN) > LDP_ZERO)
    {
        u1SsnRole = LDP_ACTIVE;
    }
    LDP_DBG1 (LDP_SSN_PRCS, "Session Role (Activ:2    Passive:3) is %x",
              u1SsnRole);
    return u1SsnRole;
}
#endif

/* MPLS_IPv6 add end*/
/*---------------------------------------------------------------------------*/
/*                       End of file ldppdprc.c                              */
/*---------------------------------------------------------------------------*/
