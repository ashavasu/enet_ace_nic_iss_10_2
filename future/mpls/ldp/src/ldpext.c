/*---------------------------------------------------------------------------*/
 /* Copyright (C) 2009 Aricent Inc . All Rights Reserved
  * $Id: ldpext.c,v 1.13 2014/11/08 11:40:55 siva Exp $
  *-----------------------------------------------------------------------------
  *    FILE  NAME             : ldpext.c
  *    PRINCIPAL AUTHOR       : Aricent Inc. 
  *    SUBSYSTEM NAME         : MPLS   
  *    MODULE NAME            : LDP 
  *    LANGUAGE               : ANSI-C
  *    TARGET ENVIRONMENT     : Linux (Portable)                         
  *    DATE OF FIRST RELEASE  :
  *    DESCRIPTION            : 
  *----------------------------------------------------------------------------*/

#include "ldpincs.h"
#include "mplslsr.h"

/*****************************************************************************/
/* Function Name : LdpTnlOperUpDnHdlForLdpOverRsvp                           */
/* Description   : This routine used for making the entity down and up       */
/*                 whenever the associated tunnel is operationally up/down   */
/* Input(s)      : u4TnlIndex      - Tunnel Index                            */
/*                 u4TnlInstance   - Tunnel Instance                         */
/*                 u4IngressId     - Source Address of the Tunnel            */
/*                 u4EgressId      - Destination Address of the Tunnel       */
/*                 u4LdpEntIndex   - LDP Entity Index                        */
/*                 u4XcOrIfIndex   - Tunnels' interface or XC index          */
/*                 u1EvtType      - Event to notify LDP                      */
/*                 u1TnlRole       - Tunnel Role                             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpTnlOperUpDnHdlForLdpOverRsvp (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                                 UINT4 u4IngressId, UINT4 u4EgressId,
                                 UINT4 u4LdpEntIndex, UINT4 u4XcOrIfIndex,
                                 UINT1 u1EvtType, UINT1 u1TnlRole)
{
    tLdpIfMsg           LdpIfMsg;
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tLblEntry          *pLblEntry = NULL;

    if ((u4TnlInstance >= TE_DETOUR_TNL_INSTANCE) ||
        (u4TnlInstance == MPLS_ZERO) || (u1TnlRole == MPLS_TE_INTERMEDIATE))
    {
        /* Backup tunnel's status change should not be 
         * notified to the applications */
        LDP_DBG4 (LDP_IF_PRCS, "EXTN : TE event posting to LDP over RSVP not "
                  "required for backup tunnels, Instance 0 tunnels, "
                  "intermediate tunnels "
                  "TE TnlId=%d TnlInstance=%d TnlIngressId=%x "
                  "TnlEgressId=%x and Event=%d\n",
                  u4TnlIndex, u4TnlInstance, u4IngressId, u4EgressId);

        return LDP_SUCCESS;
    }

    MEMSET (&LdpIfMsg, LDP_ZERO, sizeof (tLdpIfMsg));

    LdpIfMsg.u4MsgType = LDP_OVER_RSVP_EVENT;
    LdpIfMsg.u.LdpOverRsvpEvt.u4LdpEntIndex = u4LdpEntIndex;
    LdpIfMsg.u.LdpOverRsvpEvt.u4TnlIndex = u4TnlIndex;
    LdpIfMsg.u.LdpOverRsvpEvt.u4TnlInstance = u4TnlInstance;
    LdpIfMsg.u.LdpOverRsvpEvt.u4IngressId = u4IngressId;
    LdpIfMsg.u.LdpOverRsvpEvt.u4EgressId = u4EgressId;
    LdpIfMsg.u.LdpOverRsvpEvt.u4XcOrIfIndex = u4XcOrIfIndex;

    if ((u1EvtType == TE_FRR_TNL_MP_DEL) || (u1EvtType == TE_OPER_UP))
    {
        pXcEntry = MplsGetXCEntryByDirection (u4XcOrIfIndex,
                                              MPLS_DEF_DIRECTION);
        if ((pXcEntry != NULL) && (pXcEntry->pInIndex != NULL))
        {
            pInSegment = pXcEntry->pInIndex;
            /* Fill the incoming tunnel label */
            if (pInSegment->i4NPop == MPLS_ONE)
            {
                LdpIfMsg.u.LdpOverRsvpEvt.u4TnlLabel = pInSegment->u4Label;
            }
            else if ((pInSegment->u4Label == 0) &&
                     (pInSegment->mplsLabelIndex != 0)
                     && (pInSegment->i4NPop == 2))
            {
                if (TMO_SLL_Count (&(pInSegment->mplsLabelIndex->LblList)) != 2)
                {
                    return LDP_FAILURE;
                }
                pLblEntry = (tLblEntry *)
                    TMO_SLL_First (&(pInSegment->mplsLabelIndex->LblList));
                if (pLblEntry != NULL)
                {
                    LdpIfMsg.u.LdpOverRsvpEvt.u4TnlLabel = pLblEntry->u4Label;
                }
            }
            LDP_DBG1 (LDP_IF_PRCS,
                      "Tunnel label %d during oper. up\n", pInSegment->u4Label);
        }
    }

    LdpIfMsg.u.LdpOverRsvpEvt.u1EvtType = u1EvtType;

    if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Failed to EnQ TE Event to LDP Task\n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessLdpOverRsvpEvt                                  */
/* Description   : It processes the event related with the                   */
/*                 LDP over RSVP operation                                   */
/* Input(s)      :  tLdpOverRsvpEvtInfo - Entity index                       */
/*                  and tunnel oper status information                       */
/* Output(s)     : NONE.                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE.                                  */
/*****************************************************************************/
UINT1
LdpProcessLdpOverRsvpEvt (tLdpOverRsvpEvtInfo * pLdpOverRsvpEvtInfo)
{
    tLdpId              LdpEntityId;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSessionEntry = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tTMO_SLL_NODE      *pLdpUpSllNode = NULL;
    tTMO_SLL_NODE      *pLdpDnSllNode = NULL;
    tLspInfo            LspInfo;
    UINT4               u4IncarnId = MPLS_DEF_INCARN;
    UINT4               u4L3Intf = LDP_ZERO;
    UINT4               u4MplsTnlIfIndex = LDP_ZERO;

    MEMSET (&LspInfo, LDP_ZERO, sizeof (tLspInfo));
    MEMSET (LdpEntityId, LDP_ZERO, sizeof (tLdpId));
    MEMCPY (LdpEntityId, LDP_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr,
            LDP_IPV4ADR_LEN);

    if (pLdpOverRsvpEvtInfo->u4LdpEntIndex != LDP_ZERO)
    {
        /* Entity Index is not zero, Get the LDP Entity. Down/Up the 
         * LDP Entity based on event received. */
        if (LdpGetLdpEntity (MPLS_DEF_INCARN, LdpEntityId,
                             (pLdpOverRsvpEvtInfo->u4LdpEntIndex),
                             &pLdpEntity) != LDP_SUCCESS)
        {
            return LDP_FAILURE;
        }

        switch (pLdpOverRsvpEvtInfo->u1EvtType)
        {
            case TE_OPER_DOWN:
            {
                LDP_DBG4 ((LDP_IF_PRCS | LDP_IF_ALL),
                          "Entity with known index is made down for tunnel "
                          "%d %d %x %x\n",
                          pLdpOverRsvpEvtInfo->u4TnlIndex,
                          pLdpOverRsvpEvtInfo->u4TnlInstance,
                          pLdpOverRsvpEvtInfo->u4IngressId,
                          pLdpOverRsvpEvtInfo->u4EgressId);

                LdpDownLdpEntity (pLdpEntity);
                break;
            }
            case TE_OPER_UP:
            {
                LDP_DBG4 ((LDP_IF_PRCS | LDP_IF_ALL),
                          "Entity with known index is made up for tunnel "
                          "%d %d %x %x\n",
                          pLdpOverRsvpEvtInfo->u4TnlIndex,
                          pLdpOverRsvpEvtInfo->u4TnlInstance,
                          pLdpOverRsvpEvtInfo->u4IngressId,
                          pLdpOverRsvpEvtInfo->u4EgressId);

                if (pLdpEntity->InStackTnlInfo.u4TnlId ==
                    pLdpOverRsvpEvtInfo->u4TnlIndex)
                {
                    pLdpEntity->u4InTnlLabel = pLdpOverRsvpEvtInfo->u4TnlLabel;
                }

                if (pLdpEntity->OutStackTnlInfo.u4TnlId ==
                    pLdpOverRsvpEvtInfo->u4TnlIndex)
                {
                    pLdpEntity->OutStackTnlInfo.u4TnlInstance
                        = pLdpOverRsvpEvtInfo->u4TnlInstance;
                }

                LdpUpLdpEntity (pLdpEntity);
                break;
            }

                /* L2VPN over LDP over RSVP scenarios handling */
                /* LDP over RSVP PLR case handling */
            case TE_FRR_TNL_PLR_DEL:
            case TE_FRR_TNL_PLR_ADD:
                TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
            {
                if ((pLdpPeer->pLdpSession) == NULL)
                {
                    continue;
                }
                pLdpSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;

                /* Scan the Downstream Lsp Control Block List */
                TMO_SLL_Scan (&SSN_DLCB (pLdpSessionEntry), pLdpDnSllNode,
                              tTMO_SLL_NODE *)
                {
                    pLspCtrlBlock = SLL_TO_LCB (pLdpDnSllNode);
                    if (pLspCtrlBlock->u1LspState != LDP_DU_DN_LSM_ST_EST)
                    {
                        continue;
                    }
                    if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) ==
                        LDP_DU_DN_MLIB_UPD_NOT_DONE)
                    {
                        continue;
                    }

                    MEMSET (&LspInfo, LDP_ZERO, sizeof (tLspInfo));
                    LspInfo.bIsUsedByL2vpn = pLspCtrlBlock->u1IsLspUsedByL2VPN;

                    /* Filter only the LDP over RSVP targeted sessions */
                    if ((LDP_ENTITY_IS_TARGET_TYPE
                         (SSN_GET_ENTITY (pLdpSessionEntry)) == LDP_TRUE)
                        && (SSN_GET_ENTITY (pLdpSessionEntry)->OutStackTnlInfo.
                            u4TnlId != LDP_ZERO))
                    {
                        if (pLdpOverRsvpEvtInfo->u1EvtType ==
                            TE_FRR_TNL_PLR_DEL)
                        {
                            /* Handle the FRR tnl down event */
                            LdpMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                               MPLS_OPR_PUSH,
                                               pLspCtrlBlock, NULL, &LspInfo);
                        }
                        else
                        {
                            if (pLspCtrlBlock->u4OrgOutIfIndex == LDP_ZERO)
                            {
                                /* Fetch backup tunnel if index */
                                if (MplsGetL3Intf
                                    (pLdpOverRsvpEvtInfo->u4XcOrIfIndex,
                                     &u4L3Intf) == MPLS_FAILURE)
                                {
                                    return LDP_FAILURE;
                                }
                                if (CfaIfmCreateStackMplsTunnelInterface
                                    (u4L3Intf,
                                     &u4MplsTnlIfIndex) == CFA_FAILURE)
                                {
                                    return LDP_FAILURE;
                                }

                                LDP_DBG6 (LDP_IF_PRCS,
                                          "LdpProcessLdpOverRsvpEvt: OutInt %d "
                                          "created for FEC %x with Peer "
                                          "%d.%d.%d.%d\n",
                                          u4MplsTnlIfIndex,
                                          LDP_IPV4_U4_ADDR(pLspCtrlBlock->Fec.Prefix),
                                          pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                                          pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                                          pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                                          pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);

                                pLspCtrlBlock->u4OrgOutIfIndex =
                                    pLspCtrlBlock->u4OutIfIndex;
                                pLspCtrlBlock->u4OutIfIndex = u4MplsTnlIfIndex;
                            }
                            else
                            {
                                u4MplsTnlIfIndex = pLspCtrlBlock->u4OutIfIndex;
                                pLspCtrlBlock->u4OutIfIndex =
                                    pLspCtrlBlock->u4OrgOutIfIndex;
                                pLspCtrlBlock->u4OrgOutIfIndex =
                                    u4MplsTnlIfIndex;
                            }

                            /* Handle the FRR tnl up event */
                            LdpMplsMlibUpdate (MPLS_MLIB_FTN_CREATE,
                                               MPLS_OPR_PUSH,
                                               pLspCtrlBlock, NULL, &LspInfo);
                        }
                    }
                    TMO_SLL_Scan (&LCB_UPSTR_LIST (pLspCtrlBlock),
                                  pLdpUpSllNode, tTMO_SLL_NODE *)
                    {
                        pLspUpCtrlBlock = SLL_TO_UPCB (pLdpUpSllNode);
                        if (pLspUpCtrlBlock->u1LspState != LDP_DU_UP_LSM_ST_EST)
                        {
                            continue;
                        }
                        if (((pLdpSessionEntry->pLdpPeer->
                              pLdpEntity->u1TargetFlag != LDP_TRUE) &&
                             (pLdpSessionEntry->pLdpPeer->pLdpEntity->
                              OutStackTnlInfo.u4TnlId == LDP_ZERO)) &&
                            ((pLspUpCtrlBlock->pUpstrSession->pLdpPeer->
                              pLdpEntity->u1TargetFlag != LDP_TRUE) &&
                             (pLspUpCtrlBlock->pUpstrSession->pLdpPeer->
                              pLdpEntity->OutStackTnlInfo.u4TnlId == LDP_ZERO)))
                        {
                            continue;
                        }

                        if (UPSTR_USSN (pLspUpCtrlBlock) != NULL)
                        {
                            MEMSET (&LspInfo, LDP_ZERO, sizeof (tLspInfo));
                            LspInfo.bIsUsedByL2vpn =
                                pLspCtrlBlock->u1IsLspUsedByL2VPN;
                            if (pLdpOverRsvpEvtInfo->u1EvtType ==
                                TE_FRR_TNL_PLR_DEL)
                            {
                                LdpMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                                   MPLS_OPR_POP_PUSH,
                                                   pLspCtrlBlock,
                                                   pLspUpCtrlBlock, &LspInfo);
                            }
                            else
                            {
                                LdpMplsMlibUpdate (MPLS_MLIB_ILM_CREATE,
                                                   MPLS_OPR_POP_PUSH,
                                                   pLspCtrlBlock,
                                                   pLspUpCtrlBlock, &LspInfo);
                            }
                        }
                    }
                }
            }
                break;
                /* LDP over RSVP case ILM merge point from 
                 * backup path handling */
            case TE_FRR_TNL_MP_ADD:
            case TE_FRR_TNL_MP_DEL:
                LspInfo.u4BkpXcIndex = pLdpOverRsvpEvtInfo->u4XcOrIfIndex;
                LspInfo.u4TnlLabel = pLdpOverRsvpEvtInfo->u4TnlLabel;
                LDP_DBG2 (LDP_IF_PRCS,
                          "LDP Bkp XC index=%d Tnl label=%d\n",
                          LspInfo.u4BkpXcIndex, LspInfo.u4TnlLabel);

                TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
                {
                    if ((pLdpPeer->pLdpSession) == NULL)
                    {
                        continue;
                    }
                    pLdpSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;

                    /* Filter only the LDP over RSVP targeted sessions */
                    if ((LDP_ENTITY_IS_TARGET_TYPE
                         (SSN_GET_ENTITY (pLdpSessionEntry)) != LDP_TRUE)
                        || (SSN_GET_ENTITY (pLdpSessionEntry)->InStackTnlInfo.
                            u4TnlId == LDP_ZERO))
                    {
                        continue;
                    }
                    /* Scan the upstream Lsp Control Block List */
                    TMO_SLL_Scan (&SSN_ULCB (pLdpSessionEntry), pLdpUpSllNode,
                                  tTMO_SLL_NODE *)
                    {
                        pLspUpCtrlBlock = (tUstrLspCtrlBlock *) (pLdpUpSllNode);

                        MEMSET (&LspInfo, LDP_ZERO, sizeof (tLspInfo));
                        LspInfo.u4BkpXcIndex =
                            pLdpOverRsvpEvtInfo->u4XcOrIfIndex;
                        LspInfo.u4TnlLabel = pLdpOverRsvpEvtInfo->u4TnlLabel;

                        if (pLdpOverRsvpEvtInfo->u1EvtType == TE_FRR_TNL_MP_ADD)
                        {
                            LdpMplsMlibUpdate (MPLS_MLIB_ILM_CREATE,
                                               MPLS_OPR_POP_PUSH,
                                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                               pLspUpCtrlBlock, &LspInfo);
                        }
                        else
                        {
                            LdpMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                               MPLS_OPR_POP_PUSH,
                                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                               pLspUpCtrlBlock, &LspInfo);
                        }
                    }
                }
                break;
            default:
                break;
        }
    }
    else
    {
        /* Will handle only oper up case if Entity Index is Zero. */
        if (pLdpOverRsvpEvtInfo->u1EvtType != TE_OPER_UP)
        {
            return LDP_SUCCESS;
        }

        TMO_SLL_Scan (&LDP_ENTITY_LIST (u4IncarnId), pLdpEntity, tLdpEntity *)
        {
            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
            {
                continue;
            }

            if (LDP_ENTITY_LDP_OVER_RSVP_FLAG (pLdpEntity) != LDP_SNMP_TRUE)
            {
                continue;
            }

            if ((pLdpEntity->InStackTnlInfo.u4TnlId !=
                 pLdpOverRsvpEvtInfo->u4TnlIndex) &&
                (pLdpEntity->OutStackTnlInfo.u4TnlId !=
                 pLdpOverRsvpEvtInfo->u4TnlIndex))
            {
                continue;
            }

            /* In Stack Tunnel Instance has been changed. So, make the entity
             * down and then up. */

            LDP_DBG4 ((LDP_IF_PRCS | LDP_IF_ALL),
                      "Entity with unknown index is made down for tunnel "
                      "%d %d %x %x\n",
                      pLdpOverRsvpEvtInfo->u4TnlIndex,
                      pLdpOverRsvpEvtInfo->u4TnlInstance,
                      pLdpOverRsvpEvtInfo->u4IngressId,
                      pLdpOverRsvpEvtInfo->u4EgressId);

            LdpDownLdpEntity (pLdpEntity);

            if (pLdpEntity->InStackTnlInfo.u4TnlId ==
                pLdpOverRsvpEvtInfo->u4TnlIndex)
            {
                pLdpEntity->InStackTnlInfo.u4TnlInstance =
                    pLdpOverRsvpEvtInfo->u4TnlInstance;
                pLdpEntity->InStackTnlInfo.u4IngressId =
                    pLdpOverRsvpEvtInfo->u4IngressId;
                pLdpEntity->InStackTnlInfo.u4EgressId =
                    pLdpOverRsvpEvtInfo->u4EgressId;

                pLdpEntity->u4InTnlLabel = pLdpOverRsvpEvtInfo->u4TnlLabel;
            }
            else if (pLdpEntity->OutStackTnlInfo.u4TnlId ==
                     pLdpOverRsvpEvtInfo->u4TnlIndex)
            {
                pLdpEntity->OutStackTnlInfo.u4TnlInstance =
                    pLdpOverRsvpEvtInfo->u4TnlInstance;
            }

            LDP_DBG4 ((LDP_IF_PRCS | LDP_IF_ALL),
                      "Entity with unknown index is made up for tunnel "
                      "%d %d %x %x\n",
                      pLdpOverRsvpEvtInfo->u4TnlIndex,
                      pLdpOverRsvpEvtInfo->u4TnlInstance,
                      pLdpOverRsvpEvtInfo->u4IngressId,
                      pLdpOverRsvpEvtInfo->u4EgressId);

            LdpUpLdpEntity (pLdpEntity);
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleLdpUpDownEntityForLdpOverRsvp                    */
/* Description   : This Routine takes care setting/resetting of Entity Index */
/*                 in Tunnel in TE Module if LDP over RSVP is enabled.       */
/* Input(s)      : pLdpEntity - Pointer to LDP Entity Information            */
/*                 u4Status   - Current status of LDP Entity.                */
/*                              LDP_ENTITY_UP/LDP_ENTITY_DOWN                */
/* Output(s)     : NONE.                                                     */
/* Return(s)     : NONE.                                                     */
/*****************************************************************************/

VOID
LdpHandleLdpUpDownEntityForLdpOverRsvp (tLdpEntity * pLdpEntity, UINT4 u4Status)
{
    UINT4               u4EntityIndex = LDP_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    if (LDP_ENTITY_LDP_OVER_RSVP_FLAG (pLdpEntity) != LDP_SNMP_TRUE)
    {
        LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL),
                  "LDP over RSVP not enabled on this Entity %d\n",
                  pLdpEntity->u4EntityIndex);
        return;
    }

    LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL),
              "Handling Entity Index change for Entity %d in Tunnel Info "
              "for LDP over RSVP case\n", pLdpEntity->u4EntityIndex);

    if ((pLdpEntity->InStackTnlInfo.u4TnlId == LDP_ZERO) &&
        (pLdpEntity->OutStackTnlInfo.u4TnlId) == LDP_ZERO)
    {
        LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL),
                  "InStack and OutStack TunnelInfo not present for this "
                  "Entity  %d\n", pLdpEntity->u4EntityIndex);
        return;
    }

    if (u4Status == LDP_ENTITY_UP)
    {
        LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL),
                  "Handling Entity Index change for Entity %d in Tunnel Info "
                  "for LDP over RSVP case during Entity Up Case\n",
                  pLdpEntity->u4EntityIndex);

        u4EntityIndex = pLdpEntity->u4EntityIndex;
    }
    else
    {
        LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL),
                  "Handling Entity Index change for Entity %d in Tunnel Info "
                  "for LDP over RSVP case during Entity Down Case\n",
                  pLdpEntity->u4EntityIndex);
    }

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (pLdpEntity->OutStackTnlInfo.u4TnlId,
                                  pLdpEntity->OutStackTnlInfo.u4TnlInstance,
                                  pLdpEntity->OutStackTnlInfo.u4IngressId,
                                  pLdpEntity->OutStackTnlInfo.u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        LDP_DBG2 ((LDP_IF_PRCS | LDP_IF_ALL),
                  "Out Stack Tunnel Info not present for Tunnel %d in TE Module"
                  "for this Entity %d\n",
                  pLdpEntity->InStackTnlInfo.u4TnlId,
                  pLdpEntity->u4EntityIndex);
        MPLS_CMN_UNLOCK ();
        return;
    }

    if (pLdpEntity->OutStackTnlInfo.u4TnlInstance == MPLS_ZERO)
    {
        pTeTnlInfo = TeGetTunnelInfo (pLdpEntity->OutStackTnlInfo.u4TnlId,
                                      pTeTnlInfo->u4TnlPrimaryInstance,
                                      pLdpEntity->OutStackTnlInfo.u4IngressId,
                                      pLdpEntity->OutStackTnlInfo.u4EgressId);
        if (pTeTnlInfo == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return;
        }
    }

    TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo) = u4EntityIndex;

    MPLS_CMN_UNLOCK ();

    if (pLdpEntity->InStackTnlInfo.u4TnlId == LDP_ZERO)
    {
        LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL),
                  "In Stack Tunnel Index is zero for LDP Entity %d\n",
                  pLdpEntity->u4EntityIndex);
        return;
    }

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (pLdpEntity->InStackTnlInfo.u4TnlId,
                                  pLdpEntity->InStackTnlInfo.u4TnlInstance,
                                  pLdpEntity->InStackTnlInfo.u4IngressId,
                                  pLdpEntity->InStackTnlInfo.u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        LDP_DBG2 ((LDP_IF_PRCS | LDP_IF_ALL),
                  "In Stack Tunnel Info not present for Tunnel %d in TE Module"
                  "for this Entity %d\n",
                  pLdpEntity->InStackTnlInfo.u4TnlId,
                  pLdpEntity->u4EntityIndex);
        MPLS_CMN_UNLOCK ();
        return;
    }
    TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo) = u4EntityIndex;
    MPLS_CMN_UNLOCK ();

    LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL),
              "Entity Index changed for Entity %d in Out Tunnel Info "
              "for LDP over RSVP case\n", pLdpEntity->u4EntityIndex);
    return;
}

/*****************************************************************************/
/* Function Name : LdpGetL3IfaceFromLdpOverRsvpOutTnl                        */
/* Description   : This Routine gets the L3 interface from MPLS tnl          */
/*                 interface for LDP entity                                  */
/*                 in Tunnel in TE Module if LDP over RSVP is enabled.       */
/* Input(s)      : pLdpEntity - Pointer to LDP Entity Information            */
/* Output(s)     : pu4L3Intf - Pointer to the tnl's L3 interface             */
/* Return(s)     : NONE.                                                     */
/*****************************************************************************/
UINT1
LdpGetL3IfaceFromLdpOverRsvpOutTnl (tLdpEntity * pLdpEntity, UINT4 *pu4L3Intf)
{
    UINT4               u4TnlIfIndex = LDP_ZERO;

    if (TeSigGetTnlIfIndex (pLdpEntity->OutStackTnlInfo.u4TnlId,
                            pLdpEntity->OutStackTnlInfo.u4TnlInstance,
                            pLdpEntity->OutStackTnlInfo.u4IngressId,
                            pLdpEntity->OutStackTnlInfo.u4EgressId,
                            &u4TnlIfIndex) == TE_FAILURE)
    {
        return LDP_FAILURE;
    }

    if (u4TnlIfIndex == LDP_ZERO)
    {
        return LDP_FAILURE;
    }

    if (MplsGetL3Intf (u4TnlIfIndex, pu4L3Intf) == MPLS_FAILURE)
    {
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}
