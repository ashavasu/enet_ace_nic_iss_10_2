
/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: ldprmport.c,v 1.1 2014/08/25 12:23:31 siva Exp $
 ***
 *** Description: Files contails HA implementation of ldp 
 *** NOTE: This file should included in release packaging.
 *** ***********************************************************************/


#include "ldpincs.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmRcvPktFromRm                                            */
/* Description  : This API constructs a message containing the               */
/*                given RM event and RM message. And posts it to the         */
/*                LDP RM queue                                               */
/*                                                                           */
/* Input        : u1Event   - Event type given by RM module                  */
/*              : pData     - RM Message to enqueue                          */
/*              : u2DataLen - Length of the message                          */
/*                                                                           */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

INT4 LdpRmRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tLdpIfMsg           *pRmLdpMsg = NULL;

    if ((u1Event != RM_MESSAGE) && (u1Event != GO_ACTIVE) &&
            (u1Event != GO_STANDBY) && (u1Event != RM_PEER_UP) &&
            (u1Event != RM_PEER_DOWN) &&
            (u1Event != L2_INITIATE_BULK_UPDATES) &&
            (u1Event != RM_CONFIG_RESTORE_COMPLETE))
    {
        LDP_DBG (HA_DEBUG,"Invalid event received from RM\n");
        return OSIX_FAILURE ;

    }

    if ((u1Event == RM_MESSAGE) ||
            (u1Event == RM_PEER_UP) || (u1Event == RM_PEER_DOWN))

    {
        if(pData == NULL)
        {
            LDP_DBG (HA_DEBUG,"NULL pointer message is received\n");
            return OSIX_FAILURE;
        }

        if (u1Event == RM_MESSAGE)
        {
            if (u2DataLen < LDP_RM_MSG_HDR_SIZE)
            {
                LDP_DBG (HA_DEBUG,"Received invalid RM Message\n");
                RM_FREE (pData);
                return OSIX_FAILURE;
            }
        }

        else
        {
            if (u2DataLen != RM_NODE_COUNT_MSG_SIZE)
            {
                LdpRmRelRmMsgMem ((UINT1 *) pData);
                LDP_DBG (HA_DEBUG,"Received invalid standby node information \n");
                return OSIX_FAILURE;
            }

        }
    }

    pRmLdpMsg =  MemAllocMemBlk (LDP_Q_POOL_ID);
    if(pRmLdpMsg == NULL)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_PEER_UP) || (u1Event == RM_PEER_DOWN))
        {
            LdpRmRelRmMsgMem ((UINT1 *) pData);
        }
        LDP_DBG (HA_DEBUG, "Memory allocation failed for message to post LDP Queue\n");
        return OSIX_FAILURE;
    }

    MEMSET (pRmLdpMsg, 0, sizeof(tLdpIfMsg));
    pRmLdpMsg->u4MsgType = LDP_RM_EVENT;
    pRmLdpMsg->u.LdpRmEvt.pBuff = pData;
    pRmLdpMsg->u.LdpRmEvt.u1Event = u1Event;
    pRmLdpMsg->u.LdpRmEvt.u2Length = u2DataLen;

    if(LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, pRmLdpMsg) == LDP_FAILURE)
    {
        MemReleaseMemBlock (LDP_Q_POOL_ID, (UINT1 *) pRmLdpMsg);	

        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }

        else if ((u1Event == RM_PEER_UP) || (u1Event == RM_PEER_DOWN))
        {
            LdpRmRelRmMsgMem ((UINT1 *) pData);
        }
        LDP_DBG (HA_DEBUG, "Sending RM message to LDP queue failed\n");
        return OSIX_FAILURE;      

    }

    MemReleaseMemBlock (LDP_Q_POOL_ID, (UINT1 *)pRmLdpMsg);	
    return OSIX_SUCCESS;      

}


/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmRegisterWithRM                                        */
/*                                                                           */
/* Description  : This function calls the Redundancy Module API to register  */
/*                with the redundancy module.                                */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : LDP_SUCCESS on Successfull registration.                  */
/*                else returns LDP_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/

INT4 LdpRmRegisterWithRM (VOID) 
{
#ifdef LDP_HA_WANTED
    UINT4               u4RetVal = RM_FAILURE;
    tRmRegParams LdpRmRegParams = {RM_LDP_APP_ID, LdpRmRcvPktFromRm};  
    u4RetVal = RmRegisterProtocols (&LdpRmRegParams);

    if (u4RetVal != RM_SUCCESS)
    {
        LDP_DBG (HA_DEBUG, "LDP registration with RM failed\n");
        return LDP_FAILURE;
    }
    gLdpInfo.ldpRmInfo.u1AdminState = LDP_ENABLED;
#else
    gLdpInfo.ldpRmInfo.u4LdpRmState = LDP_RM_ACTIVE_STANDBY_DOWN;
    gLdpInfo.ldpRmInfo.u1BulkUpdModuleStatus = LDP_RM_MOD_COMPLETED;
#endif
    LDP_DBG (HA_DEBUG, "LDP registration with RM Success\n");
    return LDP_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmDeRegisterWithRM                                      */
/*                                                                           */
/* Description  : This function calls the Redundancy Module API to register  */
/*                with the redundancy module.                                */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : LDP_SUCCESS on Successfull registration.                  */
/*                else returns LDP_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/
INT4 LdpRmDeRegisterWithRM (VOID)
{

    UINT4               u4RetVal = RM_FAILURE;
    u4RetVal = RmDeRegisterProtocols (RM_LDP_APP_ID);

    if (u4RetVal != RM_SUCCESS)
    {
        LDP_DBG (HA_DEBUG," LDP de-registration with RM failed\n");
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmGetStandbyNodeCount                                   */
/*                                                                           */
/* Description  : This function calls the RM API to get the standby node     */
/*                count.                                                     */
/*                                                                           */
/* Input        : None                                                       */
/*                None                                                       */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : Standby node count                                         */
/*                                                                           */
/*****************************************************************************/

UINT1 LdpRmGetStandbyNodeCount()
{
    UINT1               u1StandbyCount = 0;


    u1StandbyCount = RmGetStandbyNodeCount();
    return u1StandbyCount;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmSendEventToRm                                         */
/*                                                                           */
/* Description  : This function calls the RM API to post the event           */
/* Input        : u1Event - Event to be sent to RM. And the event can be     */
/*                any of the following                                       */
/*                RM_PROTOCOL_BULK_UPDT_COMPLETION                           */
/*                RM_BULK_UPDT_ABORT                                         */
/*                RM_STANDBY_TO_ACTIVE_EVT_PROCESSED                         */
/*                RM_IDLE_TO_ACTIVE_EVT_PROCESSED                            */
/*                RM_STANDBY_EVT_PROCESSED                                   */
/*                                                                           */
/*                u1Error - In case the event is RM_BULK_UPDATE_ABORT,       */
/*                the reason for the failure is send in the error. And       */
/*                the error can be any of the following,                     */
/*                RM_MEMALLOC_FAIL                                           */
/*                RM_SENTO_FAIL                                              */
/*                RM_PROCESS_FAIL                                            */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : LDP_SUCCESS on Success                                    */
/*                else returns LDP_FAILURE.                                 */
/*                                                                           */
/*****************************************************************************/

INT4 LdpRmSendEventToRm (tRmProtoEvt * pEvt)
{

    INT4                i4RetVal = RM_FAILURE;
    i4RetVal = RmApiHandleProtocolEvent (pEvt);

    if (i4RetVal != RM_SUCCESS)
    {
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmRelRmMsgMem                                           */
/*                                                                           */
/* Description  : This function calls the RM API to release the memory       */
/*                allocated for RM message.                                  */
/*                                                                           */
/* Input        : pu1Block - Memory block to be released                     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : LDP_SUCCESS on Successfull release         .               */
/*                else returns LDP_FAILURE.                                  */
/*                                                                           */
/*****************************************************************************/

INT4 LdpRmRelRmMsgMem (UINT1 *pu1Block)  
{


    UINT4               u4RetVal = RM_FAILURE;
    u4RetVal = RmReleaseMemoryForMsg (pu1Block);

    if (u4RetVal != RM_SUCCESS)
    {
        LDP_DBG (HA_DEBUG,"Releasing the RM message memory failed \n");
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmGetRmNodeState                                        */
/*                                                                           */
/* Description  : This function calls the RM API to get the node state.      */
/*                                                                           */
/* Input        : None                                                       */
/* Output       : None.                                                      */
/* Returns      : Node state.                                                */
/*                                                                           */
/*****************************************************************************/

UINT4 LdpRmGetRmNodeState ()
{
    UINT4  u4RmState = RM_ACTIVE;

    u4RmState = RmGetNodeState();

    return u4RmState;
}



/******************************************************************************
 *   Function           : LdpRmApiSendProtoAckToRM
 *   Input(s)           : u4SeqNum - Sequence number of the RM message for
 *                        which this ACK is generated.
 *   Output(s)          : None.
 *   Returns            : NONE
 *   Action             : This is the function used by LDP  task
 *                        to send acknowledgement to RM after processing the
 *                        sync-up message.
 ********************************************************************************/

PUBLIC VOID LdpRmApiSendProtoAckToRM (UINT4 u4SeqNum)
{

    tRmProtoAck         ProtoAck;

    MEMSET (&ProtoAck, 0, sizeof(tRmProtoAck));
    ProtoAck.u4AppId = RM_LDP_APP_ID;
    ProtoAck.u4SeqNumber = u4SeqNum;
    RmApiSendProtoAckToRM (&ProtoAck);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmAllocForRmMsg                                         */
/*                                                                           */
/* Description  : This function allocates memory for the RM messages.        */
/*                If allocation fails the send the event to RM.              */
/*                                                                           */
/* Input        : u2Size - required memory size in bytes.                    */
/*                                                                           */
/* Output       : pRmMsg - pointer to the allocated memory.                  */
/*                                                                           */
/* Returns      : LDP_SUCCESS on Successfull release.                        */
/*                else returns LDP_FAILURE.                                  */
/*                                                                           */
/*****************************************************************************/

tRmMsg* LdpRmAllocForRmMsg (UINT2 u2Size)
{
    tRmProtoEvt         ProtoEvt;
    tRmMsg              *pRmMsg = NULL;

    MEMSET (&ProtoEvt, 0, sizeof(tRmProtoEvt));
#ifdef L2RED_WANTED
    pRmMsg = RM_ALLOC_TX_BUF ((UINT4)u2Size);
#else
    UNUSED_PARAM (u2Size);
#endif

    if(pRmMsg == NULL)
    {
        LDP_DBG (HA_DEBUG, "Memory allocation failed\n");
        gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_ABORTED;
        LdpRmSendBulkAbort (RM_MEMALLOC_FAIL);
    }

    return pRmMsg;

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmSendMsgToRm                                           */
/*                                                                           */
/* Description  : This function calls the RM API to en-queue the             */
/*                message from LDP to RM task.                               */
/*                                                                           */
/* Input        : pRmMsg - Message from LDP                                  */
/*                u2DataLen - Length of message                              */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : LDP_SUCCESS on Successfull en-queue                        */
/*                else returns LDP_FAILURE.                                  */
/*                                                                           */
/*****************************************************************************/

INT4 LdpRmSendMsgToRm (tRmMsg * pRmMsg, UINT2 u2DataLen)
{

    UINT4               u4SrcEntId = (UINT4) RM_LDP_APP_ID;
    UINT4               u4DestEntId = (UINT4) RM_LDP_APP_ID;
    UINT4               u4RetVal = RM_FAILURE;

    u4RetVal = RmEnqMsgToRmFromAppl (pRmMsg, u2DataLen,
            u4SrcEntId, u4DestEntId);
    if (u4RetVal != RM_SUCCESS)
    {
        /* Memory allocated for pRmMsg is freed here, only in failure case.
           In success case RM will free the memory */

        RM_FREE (pRmMsg);
        return LDP_FAILURE;
    }


    return LDP_SUCCESS;
}

/*****************************************************************************
 *  
 *   FUNCTION NAME      : LdpRmSetBulkUpdateStatus 
 *    
 *   DESCRIPTION        : This function sets LDP bulk update status in RM
 *     
 *   INPUT              : NONE
 *        
 *   OUTPUT             : NONE
 *          
 *    RETURNS           : NONE
 *           
 * ****************************************************************************/

VOID LdpRmSetBulkUpdateStatus (VOID)
{

    RmSetBulkUpdatesStatus (RM_LDP_APP_ID);

}






