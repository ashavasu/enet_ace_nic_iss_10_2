/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpsz.c,v 1.4 2013/11/29 11:04:14 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _LDPSZ_C
#include "ldpincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
LdpSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < LDP_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal = MemCreateMemPool (FsLDPSizingParams[i4SizingId].u4StructSize,
                                     FsLDPSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(LDPMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            LdpSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
LdpSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsLDPSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, LDPMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
LdpSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < LDP_MAX_SIZING_ID; i4SizingId++)
    {
        if (LDPMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (LDPMemPoolIds[i4SizingId]);
            LDPMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
