/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpgr.c,v 1.13 2014/11/28 02:13:10 siva Exp $
 *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldpgr.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : RSVP-TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains routines for the GR functionalities.
 *
 *---------------------------------------------------------------------------*/



#include "ldpincs.h"
#include "mplscli.h"
#ifdef LDP_GR_WANTED
#include "rtm.h"
#endif

/*****************************************************************************/
/* Function Name : LdpGrShutDownProcess                                      */
/* Description   : This function handles the GR-shutdown process             */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

PUBLIC VOID
LdpGrShutDownProcess (VOID)
{
    LDP_DBG (GRACEFUL_DEBUG, "LdpGrShutDownProcess: "
             "LDP Graceful Restart Procedure Initiated. \n");

    if (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) == LDP_GR_CAPABILITY_FULL)
    {
#ifdef CLI_WANTED
        IssCsrSaveCli ((UINT1 *) "LDP");
#endif
   }

    LdpShutDownProcess ();
    IssSetModuleSystemControl (LDP_MODULE_ID, MODULE_SHUTDOWN);

    LDP_DBG (GRACEFUL_DEBUG, "LdpGrShutDownProcess: "
             "LDP Component is shutdown.\n");
    return;
}

/*****************************************************************************/
/* Function Name : LdpStartLdpTask                                           */
/* Description   : This function Starts the LDP Task                         */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : OSIX_FAILURE / OSIX_SUCCESS                               */
/*****************************************************************************/

PUBLIC INT4
LdpStartLdpTask (VOID)
{
    INT4                i4RetVal = OSIX_FAILURE;

    LDP_DBG (GRACEFUL_DEBUG, "LdpStartLdpTask: " "LDP Component Started.\n");

    i4RetVal = OsixTskCrt ((UINT1 *) "LDPT", 150 | OSIX_SCHED_RR,
                           (UINT4) (3 * OSIX_DEFAULT_STACK_SIZE),
                           (OsixTskEntry) LdpTaskMain, 0, &LDP_TSK_ID);

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name : LdpNotifyCsrCompleted                                     */
/* Description   : This function is called once the CSR Restoration is       */
/*                 Complete                                                  */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpNotifyCsrCompleted (VOID)
{
    if (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) != LDP_GR_CAPABILITY_FULL)
    {
		 gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus =
                LDP_GR_NOT_STARTED;			
         return;
    }

#ifdef LDP_GR_WANTED
    /* Post an event to LDP that CSR restoreation is complete */
    if (OsixEvtSend (LDP_TSK_ID, LDP_CSR_RESTORE_COMPLELTE) == OSIX_FAILURE)
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpNotifyCsrCompleted: Failed to post "
              "LDP_CSR_RESTORE_COMPLELTE Event to LDP task\n");
        return;
    }
#endif
    MPLS_LDP_LOCK ();

#ifdef LDP_GR_WANTED
    /* Post an event to LDP that CSR restoreation is complete */
    if (OsixEvtSend (LDP_TSK_ID, LDP_CSR_RESTORE_COMPLELTE) == OSIX_FAILURE)
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpNotifyCsrCompleted: Failed to post "
              "LDP_CSR_RESTORE_COMPLELTE Event to LDP task\n");
        return;
    }
#endif
    LDP_DBG (GRACEFUL_DEBUG, "LdpNotifyCsrCompleted: "
             "RTR Starting .\n");

    MPLS_LDP_UNLOCK ();
    return;
}
#ifdef LDP_GR_WANTED
/*****************************************************************************/
/* Function Name : LdpGrStartHelperProcess                                   */
/* Description   : This function Starts the Functions done by helper         */
/* Input(s)      : pSessionEntry  - Pointer to Ldp Session Entry             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpGrStartHelperProcess (tLdpSession *pSessionEntry)
{

    UINT4               u4ReconnectTime = LDP_ZERO;
    UINT2               u2IncarnId = LDP_ZERO;
    tLdpAdjacency       *pLdpAdjacency = NULL;
    tTMO_SLL_NODE       *pSllNode = NULL;
    tUstrLspCtrlBlock   *pUpstrCtrlBlock = NULL;
    tLspCtrlBlock       *pDnstrCtrlBlock = NULL;
    tTMO_SLL_NODE       *pTempSllNode = NULL;
    UINT4               u4RemainingTime = 0;


     if (TmrGetRemainingTime (LDP_TIMER_LIST_ID,
                             &(pSessionEntry->pLdpPeer->GrTimer.AppTimer),
                             &u4RemainingTime) == TMR_FAILURE)
     {
         LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: Failed to get the "
                                  "Remaining Time for the GR Timer\n");
         return;
     }

    /* This is the case when the Recovery timer is running and Peer node
     * restarts again, Do the following:
       1. Delete all the stale entries
       2. Stop the recovery timer
       3. Mark already recovered entries as Stale */
    if(pSessionEntry->pLdpPeer->GrTimer.u4Event == LDP_GR_RECOVERY_TMR_EXP_EVT)
    {
        if (u4RemainingTime > 0)
        {
           /* Delete the Stale Entries */
           LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: Peer node Restarted again while "
                                    "Recovery Timer is running.\n");
           LdpGrDeleteStaleCtrlBlockEntries (pSessionEntry->pLdpPeer);
           LdpSendDeleteStaleNotification
                    (pSessionEntry->pLdpPeer, L2VPN_LDP_PWVC_GR_DELETE_STALE_ENTRIES);

           pSessionEntry->pLdpPeer->u1GrProgressState = LDP_PEER_GR_ABORTED;
 
          
           /* Stop the Recovery timer */
           TmrStopTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *) & (pSessionEntry->pLdpPeer->GrTimer.
                                             AppTimer));

           u4RemainingTime = 0; /* Make the Remaining Time = 0 to start reconnect timer again */
        }
     
    }

    u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);
    u4ReconnectTime = LDP_MIN (pSessionEntry->pLdpPeer->u2GrReconnectTime,
                               gLdpInfo.LdpIncarn[u2IncarnId].u2NbrLivenessTime);

    /* Start the Reconnect Timer only if reconnect timer is not running currently */
    if (u4RemainingTime == 0) 
    {
        pSessionEntry->pLdpPeer->GrTimer.u4Event = LDP_GR_RECONNECT_TMR_EXP_EVT;
        pSessionEntry->pLdpPeer->GrTimer.pu1EventInfo = 
                                      (UINT1 *)pSessionEntry->pLdpPeer;

        LDP_DBG1 (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: Starting the Reconnect Timer for %d secs\n",
                                  (u4ReconnectTime));
        if (TmrStartTimer (LDP_TIMER_LIST_ID,
                           (tTmrAppTimer *) & (pSessionEntry->pLdpPeer->GrTimer),
                           (u4ReconnectTime * SYS_NUM_OF_TIME_UNITS_IN_A_SEC))
           == TMR_FAILURE)
        {
            LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: "
                     "Starting MPLS LDP Reconnect Timer Failed.\n");
        }
    }
    /* Send event to L2VPN */
    LdpSsnStatusChngNotif (pSessionEntry, L2VPN_LDP_PWVC_GR_HELPER_MARK_STALE);

    pSessionEntry->u1SessionState = LDP_SSM_ST_NON_EXISTENT;
    LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: Marking the Session as Stale\n");

    /* Mark the session as stale */
    pSessionEntry->u1StaleStatus = LDP_TRUE;

    /* Close the TCP Socket */
    if (LdpTcpClose (pSessionEntry->u4TcpConnId) != LDP_SUCCESS)
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: "
                                 " Failed to close the Tcp Connection\n");
    }


    LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: Stopping the SSN Hold Timer\n");

    /* Stop the SSN Hold Timer */
    TmrStopTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *) & (pSessionEntry->SsnHoldTimer.AppTimer));

    LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: Stopping the Keep ALive Timer\n");
    /* Stop the Keep Alive Timer */
    TmrStopTimer (LDP_TIMER_LIST_ID,
                  (tTmrAppTimer *) & (pSessionEntry->KeepAliveTimer.AppTimer));

    /* Stop the  Adjacency Timer associated with this session */
    TMO_SLL_Scan (SSN_GET_ADJ_LIST(pSessionEntry), pLdpAdjacency, tLdpAdjacency *)
    {
        /* Stop the Adj Expiry Timer */
        TmrStopTimer (LDP_TIMER_LIST_ID,
                     (tTmrAppTimer *) & (pLdpAdjacency->PeerAdjTimer.AppTimer));
    }

     /* Delete the session from the Hash Table */ 
    LdpDelSsnFromTcpConnTbl (pSessionEntry); 
    pSessionEntry->u4TcpConnId = LDP_INVALID_CONN_ID;

    /* Mark the Upstream and downstream control blocks for this session as stale */
	TMO_DYN_SLL_Scan (&SSN_ULCB (pSessionEntry), pSllNode, pTempSllNode, tTMO_SLL_NODE *) 
	{
		pUpstrCtrlBlock = (tUstrLspCtrlBlock *) (pSllNode);

		/* Delete the control blocks which are not in the established state */
		if (UPSTR_STATE (pUpstrCtrlBlock) != LDP_DU_UP_LSM_ST_EST)
		{
			if (UPSTR_STATE (pUpstrCtrlBlock) == LDP_DU_UP_LSM_ST_REL_AWT)
			{
				LdpFreeLabel (SSN_GET_ENTITY (UPSTR_USSN(pUpstrCtrlBlock)),
						&LCB_ULBL (pUpstrCtrlBlock),
						UPSTR_INCOM_IFINDX (pUpstrCtrlBlock));
			}    
			LdpDeleteUpstrCtrlBlock (pUpstrCtrlBlock); 
		} 
		else /* Mark all the established Upstream control blocks as stale */
		{
			pUpstrCtrlBlock->u1StaleStatus = LDP_TRUE; 
		}
	}
    pSllNode = NULL;
	pTempSllNode = NULL; 
	TMO_DYN_SLL_Scan (&SSN_DLCB (pSessionEntry), pSllNode,
			pTempSllNode, tTMO_SLL_NODE *)
	{
		pDnstrCtrlBlock = SLL_TO_LCB (pSllNode);
		/* Delete the control block which are not in ESTB state */
		if (LCB_STATE (pDnstrCtrlBlock) != LDP_DU_DN_LSM_ST_EST)
		{
			LDP_DBG2 (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: Deleting the "
					"DLCB for FEC: %x STATE = %d\n", 
					LDP_IPV4_U4_ADDR(LCB_FEC (pDnstrCtrlBlock).Prefix), LCB_STATE (pDnstrCtrlBlock));
			LdpDeleteLspCtrlBlock (pDnstrCtrlBlock);
		}
		else
		{  
			pDnstrCtrlBlock->u1StaleStatus = LDP_TRUE;
		}
	}
}
#else
/*****************************************************************************/
/* Function Name : LdpGrStartHelperProcess                                   */
/* Description   : This function Starts the Functions done by helper         */
/* Input(s)      : pSessionEntry  - Pointer to Ldp Session Entry             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
        

VOID
LdpGrStartHelperProcess (tLdpSession * pSessionEntry)
{
    UINT4               u4ReconnectTime = LDP_ZERO;
    UINT2               u2IncarnId = LDP_ZERO;

    u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);
    u4ReconnectTime = LDP_MIN (pSessionEntry->pLdpPeer->u2GrReconnectTime,
                               gLdpInfo.LdpIncarn[u2IncarnId].
                               u2NbrLivenessTime);

    /* Start the Reconnect Timer */
    pSessionEntry->pLdpPeer->GrTimer.u4Event = LDP_GR_RECONNECT_TMR_EXP_EVT;
    pSessionEntry->pLdpPeer->GrTimer.pu1EventInfo = (UINT1 *)
        pSessionEntry->pLdpPeer;

    if (TmrStartTimer (LDP_TIMER_LIST_ID,
                       (tTmrAppTimer *) & (pSessionEntry->pLdpPeer->GrTimer),
                       (u4ReconnectTime * SYS_NUM_OF_TIME_UNITS_IN_A_SEC))
        == TMR_FAILURE)
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: "
                 "Starting MPLS LDP Reconnect Timer Failed.\n");
    }

    LDP_DBG5 (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: "
              "MPLS LDP Reconnect Timer Started for th Peer %d.%d.%d.%d "
              "Timer will run for %d seconds.",
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3], u4ReconnectTime);

    /* Mark all the PW associated with this Session as Stale.
     * For this post a Event */
    LdpSsnStatusChngNotif (pSessionEntry, L2VPN_LDP_PWVC_GR_HELPER_MARK_STALE);

    LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartHelperProcess: "
             "L2vpn is Notified to mark Pseudowire Entries as Stale.\n");

    return;
}
#endif
/*****************************************************************************/
/* Function Name : LdpSendDeleteStaleNotification                            */
/* Description   : This function send notifications to l2vpn module to Delete*/
/*                 Stale Entries of Pseduowire                               */
/* Input(s)      : pLdpPeer    - Pointer to the Ldp Peer for which the       */
/*                               message is received                         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpSendDeleteStaleNotification (tLdpPeer * pLdpPeer, UINT4 u4Event)
{
    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;
    tLdpEntity         *pLdpEntity = NULL;
    UINT2               u2IncarnId = LDP_ZERO;
    tTMO_SLL           *pList = NULL;

    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

    if (u4Event == L2VPN_LDP_PWVC_GR_DELETE_STALE_ENTRIES)
    {
        pLdpEntity = pLdpPeer->pLdpEntity;
		LDP_L2VPN_EVT_SSN_ADDR_TYPE(&PwVcEvtInfo) = MPLS_IPV4_ADDR_TYPE; 
        MEMCPY ((UINT1 *) &(LDP_L2VPN_EVT_SSN_PEER_ADDR_IPV4 (&PwVcEvtInfo)),
                (UINT1 *) &(pLdpPeer->NetAddr), LDP_LSR_ID_LEN);

        MEMCPY (&LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&PwVcEvtInfo),
                &pLdpEntity->LdpId, IPV4_ADDR_LENGTH);
        LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&PwVcEvtInfo) =
            OSIX_NTOHL (LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&PwVcEvtInfo));
        LDP_L2VPN_EVT_SSN_LCL_ENT_INDEX (&PwVcEvtInfo) =
            pLdpEntity->u4EntityIndex;
    }
    else
    {
        for (u2IncarnId = 0; u2IncarnId < LDP_MAX_INCARN; ++u2IncarnId)
        {
            pList = &LDP_ENTITY_LIST (u2IncarnId);
            pLdpEntity = (tLdpEntity *) TMO_SLL_First (pList);

            if (pLdpEntity == NULL)
            {
                continue;
            }

            TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
            {
                TMO_SLL_Scan (&pLdpEntity->PeerList, pLdpPeer, tLdpPeer *)
                {
                    /* For the Peers who do not support the GR, Gr Progress state
                     * will be LDP_PEER_GR_NOT_SUPPORTED, For those who support 
                     * GR, the state will be LDP_PEER_GR_NOT_STARTED */
                    if (pLdpPeer->u1GrProgressState == LDP_PEER_GR_RECOVERY_IN_PROGRESS)
                    {
                        pLdpPeer->u1GrProgressState = LDP_PEER_GR_COMPLETED;
                        LDP_DBG4 (GRACEFUL_DEBUG, "LdpSendDeleteStaleNotification: "
                              "The Ldp Peer %d.%d.%d.%d is set to GR Completed "
                              "State.\n", pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                              pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                              pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                              pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);

                    }
                }
            }
        }
    }


    LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = u4Event;

    if (LdpL2VpnPostEvent (&PwVcEvtInfo) == LDP_FAILURE)
    {
        LDP_DBG ((LDP_PRCS_PRCS | GRACEFUL_DEBUG),
                 "LdpSendDeleteStaleNotification: Delete Stale Entries "
                 "Event Posted To Application Failed.\n");
        return;
    }

    LDP_DBG (GRACEFUL_DEBUG, "LdpSendDeleteStaleNotification: Delete "
             "Stale Entries Event Posted to Application.\n");
    return;
}

/*****************************************************************************/
/* Function Name : LdpStartFwdHoldingTimer                                   */
/* Description   : This function starts the Forwarding State Holding Timer   */
/*                 upon the restart of LDP Component.                        */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpStartFwdHoldingTimer ()
{
    UINT4               u4FwdHoldingTimer = LDP_ZERO;

    if (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) != LDP_GR_CAPABILITY_FULL)
    {
        return;
    }

    u4FwdHoldingTimer = (UINT4) LDP_GR_FWD_HOLDING_TIME (MPLS_DEF_INCARN);

    if (u4FwdHoldingTimer == LDP_ZERO)
    {
        return;
    }

    /* Start the Forward Holding Timer */
    gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].FwdHoldingTmr.
        u4Event = LDP_GR_FWD_HOLDING_TMR_EXP_EVT;
    gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].FwdHoldingTmr.pu1EventInfo = NULL;

    if (TmrStartTimer (LDP_TIMER_LIST_ID,
                       (tTmrAppTimer *) & (gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].
                                           FwdHoldingTmr),
                       (u4FwdHoldingTimer * SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) ==
        TMR_FAILURE)
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpStartFwdHoldingTimer: "
                 "Starting MPLS Forwarding Hold Timer Failed.\n");

        return;
    }

    LDP_DBG1 (GRACEFUL_DEBUG, "LdpStartFwdHoldingTimer: "
              "MPLS Forwarding Hold Timer Started. Timer will run for "
              "%d seconds.\n", u4FwdHoldingTimer);
    return;
}

/*****************************************************************************/
/* Function Name : LdpGrStartRecoveryTimer                                   */
/* Description   : This function Starts the GR Reconnect Timer and Starts    */
/*                 the Recovery Timer.                                       */
/*                 This function is called by the node that is currently     */
/*                 Helping.                                                  */
/* Input(s)      : pSessionEntry  - Pointer to Ldp Session Entry             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpGrStartRecoveryTimer (tLdpSession * pSessionEntry)
{
    UINT4               u4RecoveryTime = LDP_ZERO;
    UINT4               u4RemainingTime = LDP_ZERO;
    UINT2               u2IncarnId = LDP_ZERO;
    

    /* Get the Remaining Time of Reconnect Timer.
     */

    
    if (TmrGetRemainingTime (LDP_TIMER_LIST_ID,
                             &(pSessionEntry->pLdpPeer->GrTimer.AppTimer),
                             &u4RemainingTime) == TMR_FAILURE)
    {
        return;
    }

    if (u4RemainingTime == LDP_ZERO)
    {
        /* Reconnect Timer Already Expired */
        return;
    }

    u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);

    /* Stop the Reconnect Timer and Start Recovery Timer */
    TmrStopTimer (LDP_TIMER_LIST_ID,
                  (tTmrAppTimer *) & (pSessionEntry->pLdpPeer->GrTimer.
                                      AppTimer));

    LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartRecoveryTimer: "
             "Start Recovery Timer.\n");

    u4RecoveryTime = LDP_MIN (pSessionEntry->pLdpPeer->u2GrRecoveryTime,
                              gLdpInfo.LdpIncarn[u2IncarnId].u2MaxRecoveryTime);

    pSessionEntry->pLdpPeer->GrTimer.u4Event = LDP_GR_RECOVERY_TMR_EXP_EVT;
    pSessionEntry->pLdpPeer->GrTimer.pu1EventInfo = (UINT1 *)
        pSessionEntry->pLdpPeer;

    if (TmrStartTimer (LDP_TIMER_LIST_ID,
                       (tTmrAppTimer *) & (pSessionEntry->pLdpPeer->GrTimer),
                       (u4RecoveryTime * SYS_NUM_OF_TIME_UNITS_IN_A_SEC))
        == TMR_FAILURE)
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpGrStartRecoveryTimer: "
                 "Starting MPLS LDP Recovery Timer Failed.\n");
        return;
    }

         LDP_DBG5 (GRACEFUL_DEBUG, "LdpGrStartRecoveryTimer: "
              "MPLS LDP Recovery Timer Started for the Peer %d.%d.%d.%d "
              " Timer will run for %d seconds.\n",
              pSessionEntry->pLdpPeer->PeerLdpId[0],
              pSessionEntry->pLdpPeer->PeerLdpId[1],
              pSessionEntry->pLdpPeer->PeerLdpId[2],
              pSessionEntry->pLdpPeer->PeerLdpId[3], u4RecoveryTime);
    
    
    return;
}

/*****************************************************************************/
/* Function Name : LdpGrProcessRecoveryTimerEvent                              */
/* Description   : This routine processes recovery timer event               */
/*                 from the L2VPN and stop the recovery timer                */
/* Input(s)      : pSsnEvtInfo- Pointer to info containing Entity Id and Peer*/
/*                 Address to create the session                             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpGrProcessRecoveryTimerEvent (tPwVcSsnEvtInfo * pSsnEvtInfo)
{
    UINT4               u2IncarnId = LDP_CUR_INCARN;
    tLdpSession        *pPeerSession = NULL;
    uGenU4Addr          PeerAddr;

    MEMSET(&PeerAddr,0,sizeof(uGenU4Addr));

    LDP_IPV4_U4_ADDR(PeerAddr)=LDP_IPV4_U4_ADDR(pSsnEvtInfo->PeerAddr);
    if (LdpGetSessionWithPeer (u2IncarnId,
			    pSsnEvtInfo->u4LocalLdpEntityID,
			    &PeerAddr,
			    LDP_ADDR_TYPE_IPV4,
			    LDP_DSTR_UNSOLICIT, LDP_TRUE,
			    &pPeerSession) == LDP_FAILURE)
    {
        LDP_DBG (GRACEFUL_DEBUG, "MAIN: Entity not configured\n");
        return LDP_FAILURE;
    }
    if (pPeerSession->pLdpPeer == NULL)
    {
        LDP_DBG (GRACEFUL_DEBUG, "LDP Peer is NULL \n");
        return (LDP_FAILURE);
    }
    /* Stop the Recovery Timer */
    TmrStopTimer (LDP_TIMER_LIST_ID,
                  (tTmrAppTimer *) & (pPeerSession->pLdpPeer->GrTimer.
                                      AppTimer));

    LDP_DBG (GRACEFUL_DEBUG, "LdpGrProcessRecoveryTimerEvent: "
             "Recovery Timer Stopped.\n");
    return LDP_SUCCESS;
}

#ifdef LDP_GR_WANTED
/*****************************************************************************/
/* Function Name : LdpGrInitProcess                                            */
/* Description  : This routine is used Start Gr process at CSR_COMPLETE        */
/*                event                                                        */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpGrInitProcess()
{
    LDP_DBG (GRACEFUL_DEBUG, "LdpGrInitProcess: Entry.\n");
    LdpSendGrStartEventToL2Vpn();
    LdpStartFwdHoldingTimer ();
    LDP_DBG (GRACEFUL_DEBUG, "LdpGrInitProcess: Exit.\n");
}
/*****************************************************************************/
/* Function Name : LdpSendGrStartEventToL2Vpn                                */
/* Description   : This routine is used to send an L2VPN_LDP_PWVC_GR_START   */
/*                 event to L2VPN                                              */
/*                    1. when LDP thread will come up in case of GR            */
/*                  2. At GO_ACTIVE Event in case of switchover                */
/*                  So, that L2VPN will read the enries from HW List and   */
/*                  mark the entries as stale                                  */
/* Input(s)      : None                                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                        */
/*****************************************************************************/
VOID
LdpSendGrStartEventToL2Vpn()
{
	tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;

	LDP_DBG (GRACEFUL_DEBUG, "LdpSendGrStartEventToL2Vpn: Entry.\n");

	MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

	if (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) == LDP_GR_CAPABILITY_FULL)
	{
		LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = L2VPN_LDP_PWVC_GR_START;

		if (LdpL2VpnPostEvent (&PwVcEvtInfo) == LDP_FAILURE)
		{
			LDP_DBG ((LDP_PRCS_PRCS | GRACEFUL_DEBUG),
								"LdpSendGrStartEventToL2Vpn: "
								"Event Posted To Application Failed.\n");
			return;
		}
	}
	else
	{
			return;
	}
	LDP_DBG (GRACEFUL_DEBUG, "LdpSendGrStartEventToL2Vpn: Exit.\n");
}
/*****************************************************************************/
/* Function Name : LdpReserveLabelToLabelManager                             */
/* Description   : This routine is used to get InLabel from L2VPN and        */
/*                    reserve in Label Manager                                 */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpReserveLabelToLabelManager()
{
	tLdpL2VpnPwHwList   LdpL2VpnPwHwListEntry;
	UINT4               u4InLabel = LDP_ZERO;
	tLdpEthParams       *pLdpEthParams = NULL;
	UINT4               u4Label1 = LDP_ZERO;
	UINT4               u4Label2 = LDP_ZERO;
	UINT2               u2LabelRangeId = LDP_ZERO;
	tTMO_SLL           *pList = NULL;
	tLdpEntity         *pLdpEntity = NULL;

	LDP_DBG (GRACEFUL_DEBUG, "LdpReserveLabelToLabelManager: Entry.\n");

	MEMSET(&LdpL2VpnPwHwListEntry,LDP_ZERO,sizeof(tLdpL2VpnPwHwList));
	while(L2VpnGetNextLabelFromHwList(&LdpL2VpnPwHwListEntry,
							&LdpL2VpnPwHwListEntry,
							&u4InLabel) == L2VPN_VALID)
	{
		pList = &LDP_ENTITY_LIST (LDP_CUR_INCARN);
		TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
		{
			if (pLdpEntity->u4EntityIndex ==
									LdpL2VpnPwHwListEntry.u4LocalLdpEntityIndex)
			{
				break;
			}
		}
		if (pLdpEntity == NULL)
		{
			return;
		}
		if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
		{
			TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity), pLdpEthParams,
									tLdpEthParams *)
			{
				if ((u4InLabel >= pLdpEthParams->u4MinLabelVal) &&
											(u4InLabel <= pLdpEthParams->u4MaxLabelVal))
				{
					u2LabelRangeId = pLdpEthParams->u2EthLblRangeId;
					u4Label1 = 0;
					u4Label2 = u4InLabel;

					if(ldpAssignLblToLblGroup(u2LabelRangeId,u4Label1,u4Label2)
														!= LDP_SUCCESS)
					{
						LDP_DBG (GRACEFUL_DEBUG, "LdpReserveLabelToLabelManager:"
															"Label reservation is failed\n");
						return;

					}
				}
			}
		}
	}

	LDP_DBG (GRACEFUL_DEBUG, "LdpReserveLabelToLabelManager: Exit.\n");
}	
/*****************************************************************************/
/* Function Name : LdpProcessFwdHoldTmrExpiry                                */
/* Description   : This function is called when MPLS FWD hold timer expires. */
/*                 This deletes all the stale entries from FTN and ILM Hw    */
/*                 List.                                                     */
/* Input(s)      :                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpProcessFwdHoldTmrExpiry(VOID)
{
    tFTNHwListEntry     *pFTNHwListEntry = NULL;
    tILMHwListEntry     *pILMHwListEntry = NULL;
    tILMHwListEntry      TempILMHwListEntry;


    LDP_DBG (GRACEFUL_DEBUG, "LdpProcessFwdHoldTmrExpiry: Entry\n");
    MEMSET (&TempILMHwListEntry, 0, sizeof (tILMHwListEntry));


    /* Read the FTN Hw List Entries */
    pFTNHwListEntry = MplsHwListGetFirstFTNHwListEntry();

    while (pFTNHwListEntry != NULL)
    {
        /* Delete Stale Entry from the H/w */
        if (MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry) == MPLS_TRUE)
        {
#ifdef MPLS_IPV6_WANTED
            if(MPLS_FTN_HW_LIST_FEC_ADDR_TYPE(pFTNHwListEntry)==LDP_ADDR_TYPE_IPV6)
            {
                LDP_DBG3 (GRACEFUL_DEBUG, "LdpProcessFwdHoldTmrExpiry: FTN Hw List Entry is found STALE:"
                        "FEC: %s, Tunnel Interface:%d, OutLabel: %d\n",
                        Ip6PrintAddr(&MPLS_IPV6_FTN_HW_LIST_FEC(pFTNHwListEntry)),
                        MPLS_FTN_HW_LIST_OUT_L3_INTF (pFTNHwListEntry),
                        MPLS_FTN_HW_LIST_LABEL (pFTNHwListEntry));
            }
            else
#endif
            {
                LDP_DBG6 (GRACEFUL_DEBUG, "LdpProcessFwdHoldTmrExpiry: FTN Hw List Entry is found STALE:"
                        "FEC: %d.%d.%d.%d, Tunnel Interface:%d, OutLabel: %d\n",
                        MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry)[0],
                        MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry)[1],
                        MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry)[2],
                        MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry)[3],
                        MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry),
                        MPLS_FTN_HW_LIST_LABEL (pFTNHwListEntry));
            }

            if (MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry, NULL) == MPLS_FAILURE)
            {
                /* Failure not returned explicitly */
                LDP_DBG (GRACEFUL_DEBUG, "Error in Deleting the STALE FTN Hw List Entry\n");
            }

            if ( MPLS_FTN_HW_LIST_IS_STATIC (pFTNHwListEntry) == MPLS_FALSE)
            {
                if (CfaIfmDeleteStackMplsTunnelInterface (MPLS_FTN_HW_LIST_OUT_L3_INTF (pFTNHwListEntry),
                            MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry)) == CFA_FAILURE)
                {
                    /* Failure not returned explicitly */
                    CMNDB_DBG1 (DEBUG_ERROR_LEVEL, "MplsHwListDeleteFTNEntryFromHw: Failed to Delete the "
                            "TUNNEL interface: %d\n",
                            MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry));
                }
            }

        }

        pFTNHwListEntry = MplsHwListGetNextFTNHwListEntry (pFTNHwListEntry);
    }

    pILMHwListEntry = MplsHwListGetFirstILMHwListEntry ();
    while (pILMHwListEntry != NULL)
    {

        /* Delete Stale entry from the H/w */
        if (MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry) == MPLS_TRUE)
        {

#ifdef MPLS_IPV6_WANTED
		if(MPLS_ILM_HW_LIST_FEC_ADDR_TYPE(pILMHwListEntry)==MPLS_IPV6_ADDR_TYPE)
		{
			LDP_DBG5 (GRACEFUL_DEBUG, "LdpProcessFwdHoldTmrExpiry: ILM Hw List Entry is found STALE:"
					"FEC: %s, InInterface: %d, InLabel: %d, OutInterface: %d, "
					"OutLabel: %d\n",
					Ip6PrintAddr(&MPLS_IPV6_ILM_HW_LIST_FEC(pILMHwListEntry)),
					MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry),
					MPLS_ILM_HW_LIST_IN_LABEL (pILMHwListEntry),
					MPLS_ILM_HW_LIST_OUT_TNL_INTF (pILMHwListEntry),
					MPLS_ILM_HW_LIST_SWAP_LABEL (pILMHwListEntry));
		}
		else
#endif
		{

			LDP_DBG8 (GRACEFUL_DEBUG, "LdpProcessFwdHoldTmrExpiry: ILM Hw List Entry is found STALE:"
					"FEC: %d.%d.%d.%d, InInterface: %d, InLabel: %d, OutInterface: %d, "
					"OutLabel: %d\n",
					MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
					MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
					MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
					MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3],
					MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry),
					MPLS_ILM_HW_LIST_IN_LABEL (pILMHwListEntry),
					MPLS_ILM_HW_LIST_OUT_TNL_INTF (pILMHwListEntry),
					MPLS_ILM_HW_LIST_SWAP_LABEL (pILMHwListEntry));
		}

		MEMCPY (&TempILMHwListEntry, pILMHwListEntry, sizeof (tILMHwListEntry));

            if (MplsHwListDeleteILMEntryFromHw (pILMHwListEntry, NULL) == MPLS_SUCCESS)
            {
                if (LdpGrEstbLspForStaleILMHwListEntry (&TempILMHwListEntry) == LDP_FAILURE)
                {
                    /* Failure not returned explicitly */
                    LDP_DBG (GRACEFUL_DEBUG, "LdpProcessFwdHoldTmrExpiry: Failed to Establish LSP for STALE "
                                             "ILM Hw List Entry\n");
                }
            }
            else
            {
                /* Failure not returned explicitly */
                LDP_DBG (GRACEFUL_DEBUG, "LdpProcessFwdHoldTmrExpiry: Error in Deleting the STALE ILM "
                                         "Entry\n");
            }
        }

        pILMHwListEntry = MplsHwListGetNextILMHwListEntry (pILMHwListEntry);
    }
    LDP_DBG (GRACEFUL_DEBUG, "LdpProcessFwdHoldTmrExpiry: Exit\n");
}

/*****************************************************************************/
/* Function Name : LdpGrDeleteStaleCtrlBlockEntries                          */
/* Description   : This function deletes the stale control block on the
                   Non-restarting node after the Recovery timer expires      */
/* Input(s)      : pLdpPeer                                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpGrDeleteStaleCtrlBlockEntries (tLdpPeer *pLdpPeer)
{
    tLdpSession        *pLdpSession = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tLdpMsgInfo         LdpMsgInfo;

    pLdpSession = pLdpPeer->pLdpSession;
    LDP_DBG (GRACEFUL_DEBUG, "LdpGrDeleteStaleCtrlBlockEntries: Entry\n");
    if (pLdpSession != NULL)
    {

        MEMSET (&LdpMsgInfo, 0, sizeof (tLdpMsgInfo));

        LDP_DBG4 (GRACEFUL_DEBUG, "LdpGrDeleteStaleCtrlBlockEntries: Entry: "
                "for the Peer: %d.%d.%d.%d\n",
                pLdpPeer->PeerLdpId[0],
                pLdpPeer->PeerLdpId[1],
                pLdpPeer->PeerLdpId[2],
                pLdpPeer->PeerLdpId[3]);

        /* Send the Downstream lost Event to all the STALE DLCB's */
        TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pSllNode,
                pTempSllNode, tTMO_SLL_NODE *)
        {
            pLspCtrlBlock = SLL_TO_LCB (pSllNode);

            if (pLspCtrlBlock->u1StaleStatus == LDP_TRUE)
            {
                LDP_DBG1 (GRACEFUL_DEBUG, "LdpGrDeleteStaleCtrlBlockEntries: Downstream control Block for FEC: "
                        "%x, remained STALE, sending DOWNSTREAM LOST\n",
                        LDP_IPV4_U4_ADDR(LCB_FEC (pLspCtrlBlock).Prefix));

                LdpInvCorrStMh (pLdpSession, (tLspCtrlBlock *) pLspCtrlBlock,
                        LDP_LSM_EVT_DN_LOST, &LdpMsgInfo, LDP_FALSE);
            }
        }


        /* Send the Event UPSTREAM_LOST to all the STALE ULCB's */
        TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession), pSllNode,
                pTempSllNode, tTMO_SLL_NODE *)
        {
            pLspUpCtrlBlock = (tUstrLspCtrlBlock *) pSllNode;

            if (pLspUpCtrlBlock->u1StaleStatus == LDP_TRUE)
            {
                LDP_DBG1 (GRACEFUL_DEBUG, "LdpGrDeleteStaleCtrlBlockEntries: UPSTREAM control Block for FEC: "
                        "%x, remained STALE, sending UPSTREAM  LOST\n",
                        LDP_IPV4_U4_ADDR(LCB_FEC (pLspUpCtrlBlock).Prefix));

                LdpInvCorrStMh (pLdpSession, pLspUpCtrlBlock,
                        LDP_LSM_EVT_UP_LOST, &LdpMsgInfo, LDP_TRUE);
            }
        }
    }
    else
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpGrDeleteStaleCtrlBlockEntries: LDP Session is NULL\n");
    }

    LDP_DBG (GRACEFUL_DEBUG, "LdpGrDeleteStaleCtrlBlockEntries: Exit\n");
}

/*****************************************************************************/
/* Function Name : LdpGrEstbLspForStaleILMHwListEntry                        */
/* Description   : This function Establish LSP's for the those ILM Hw List   */
/*                 entries which were in SWAP mode and and after the restart */
/*                 no Label mapping is rcvd from the Dowstream router for    */
/*                 such entries.                                             */
/*                 If the route exists then LSP will be established.         */
/* Input(s)      : pILMHwListEntry                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpGrEstbLspForStaleILMHwListEntry (tILMHwListEntry *pILMHwListEntry)
{
    tLdpSession           *pSessionEntry = NULL;
    UINT4                 u4Prefix = 0;
    UINT4                 u4DestMask = 0;
    UINT4                 u4RtPort = 0;
    tNetIpv4RtInfo        NetIpRtInfo;
    tRtInfoQueryMsg       RtQuery;
    tLdpIpRtInfo          InRtInfo;
    tGenU4Addr            NextHopAddr;

#ifdef MPLS_IPV6_WANTED
    tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
    tNetIpv6RtInfo         NetIpv6RtInfo;
#endif

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (&InRtInfo, 0, sizeof (tLdpIpRtInfo));
    MEMSET(&NextHopAddr, 0, sizeof(tGenU4Addr));

#ifdef MPLS_IPV6_WANTED
    MEMSET(&NetIpv6RtInfoQueryMsg,0,sizeof(tNetIpv6RtInfoQueryMsg));
    MEMSET(&NetIpv6RtInfo,0,sizeof(tNetIpv6RtInfo));
#endif

    LDP_DBG (GRACEFUL_DEBUG, "LdpGrEstbLspForStaleILMHwListEntry: Entry\n");

    if (MPLS_ILM_HW_LIST_OUT_TNL_INTF (pILMHwListEntry) != 0)
    {

        /* Get the LDP Session from the Adjacency Index */
        pSessionEntry = LdpGetSessionFromIfIndex
                       (MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));

	if (pSessionEntry != NULL)
	{
#ifdef MPLS_IPV6_WANTED
		if(MPLS_ILM_HW_LIST_FEC_ADDR_TYPE(pILMHwListEntry)==MPLS_IPV6_ADDR_TYPE)
		{
			LDP_GET_MASK_FROM_PRFX_LEN (MPLS_ILM_HW_LIST_FEC_PREFIX_LEN (pILMHwListEntry), u4DestMask);
			NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
			NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
			MEMCPY (&NetIpv6RtInfo.Ip6Dst,&MPLS_IPV6_ILM_HW_LIST_FEC(pILMHwListEntry), IPV6_ADDR_LENGTH);
			NetIpv6RtInfo.u1Prefixlen=(UINT1)u4DestMask;

			if(NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
						&NetIpv6RtInfo)!=NETIPV6_SUCCESS)
			{
				LDP_DBG2 (GRACEFUL_DEBUG, "LdpGrEstbLspForStaleILMHwListEntry: Route Not found for the "
						"Prefix: %s, PrefixLen: %d\n", u4Prefix,
						MPLS_ILM_HW_LIST_FEC_PREFIX_LEN (pILMHwListEntry));
			}
			MEMCPY(&NextHopAddr.Addr.Ip6Addr,&NetIpv6RtInfo.NextHop,IPV6_ADDR_LENGTH);
			NextHopAddr.u2AddrType = MPLS_IPV6_ADDR_TYPE;
			if (LdpIsSsnForFec (&NextHopAddr, u4RtPort, pSessionEntry) == LDP_FALSE)
			{
				LDP_DBG2 (GRACEFUL_DEBUG, "LdpGrEstbLspForStaleILMHwListEntry: Establishing LSP "
						"for the Prefix: %s, Prefix Len: %d\n", 
						Ip6PrintAddr(&MPLS_IPV6_ILM_HW_LIST_FEC(pILMHwListEntry)),
						MPLS_ILM_HW_LIST_FEC_PREFIX_LEN (pILMHwListEntry));

				LdpEstbIpv6LspsForDuSsns (pSessionEntry, &NetIpv6RtInfo);
			}
		}
		else
#endif
		{
			MEMCPY (&u4Prefix, MPLS_ILM_HW_LIST_FEC (pILMHwListEntry), IPV4_ADDR_LENGTH);
			LDP_GET_MASK_FROM_PRFX_LEN (MPLS_ILM_HW_LIST_FEC_PREFIX_LEN (pILMHwListEntry), u4DestMask);


			RtQuery.u4DestinationIpAddress = u4Prefix;
			RtQuery.u4DestinationSubnetMask = u4DestMask;
			RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

			if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
			{
				LDP_DBG2 (GRACEFUL_DEBUG, "LdpGrEstbLspForStaleILMHwListEntry: Route Not found for the "
						"Prefix: %x, PrefixLen: %d\n", u4Prefix,
						MPLS_ILM_HW_LIST_FEC_PREFIX_LEN (pILMHwListEntry));
				return LDP_FAILURE;
			}
			InRtInfo.u4DestNet = NetIpRtInfo.u4DestNet;
			InRtInfo.u4DestMask = NetIpRtInfo.u4DestMask;
			InRtInfo.u4Tos = NetIpRtInfo.u4Tos;
			InRtInfo.u4NextHop = NetIpRtInfo.u4NextHop;
			InRtInfo.u4RtAge = NetIpRtInfo.u4RtAge;
			InRtInfo.i4Metric1 = NetIpRtInfo.i4Metric1;
			InRtInfo.u4RowStatus = NetIpRtInfo.u4RowStatus;
			InRtInfo.u4RouteTag = NetIpRtInfo.u4RouteTag;
			InRtInfo.u2RtProto = NetIpRtInfo.u2RtProto;
			InRtInfo.u2RtType = NetIpRtInfo.u2RtType;

			NextHopAddr.Addr.u4Addr = NetIpRtInfo.u4NextHop;
			NextHopAddr.u2AddrType = MPLS_IPV4_ADDR_TYPE;
			if (LdpIsSsnForFec (&NextHopAddr, u4RtPort, pSessionEntry) == LDP_FALSE)
			{
				LDP_DBG2 (GRACEFUL_DEBUG, "LdpGrEstbLspForStaleILMHwListEntry: Establishing LSP "
						"for the Prefix: %x, Prefix Len: %d\n", u4Prefix,
						MPLS_ILM_HW_LIST_FEC_PREFIX_LEN (pILMHwListEntry));

				LdpEstbLspsForDuSsns (pSessionEntry, &InRtInfo);
			}
		}
	}
	else
	{
		LDP_DBG1 (GRACEFUL_DEBUG, "LdpGrEstbLspForStaleILMHwListEntry: LDP Session is NULL for interface: %d\n",
				MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
		return LDP_FAILURE;
	}
    }
    else
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpGrEstbLspForStaleILMHwListEntry: ILM HwList Entry is not in SWAP mode,"
                                 "Hence LSP not formed here\n");
    }
    LDP_DBG (GRACEFUL_DEBUG, "LdpGrEstbLspForStaleILMHwListEntry: Exit\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpGetSessionFromIfIndex                                  */
/* Description   : This function returns the LDP session from the Adjacency  */
/*                 If index                                                  */
/* Input(s)      : u4IfIndex                                                 */
/* Output(s)     : NONE                                                      */
/* Return(s)     : pLdpSession                                               */
/*****************************************************************************/
tLdpSession *
LdpGetSessionFromIfIndex (UINT4 u4IfIndex)
{

    tTMO_SLL              *pEntityList = NULL;
    tLdpEntity            *pLdpEntity = NULL;
    tLdpPeer              *pLdpPeer = NULL;
    tLdpSession           *pSessionEntry = NULL;
    tLdpAdjacency         *pLdpAdjacency = NULL;


    pEntityList = &LDP_ENTITY_LIST (LDP_CUR_INCARN);
    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
        {
            pSessionEntry = pLdpPeer->pLdpSession;
            if (pSessionEntry == NULL)
            {
                continue;
            }
            TMO_SLL_Scan (SSN_GET_ADJ_LIST(pSessionEntry), pLdpAdjacency, tLdpAdjacency *)
            {
                if (pLdpAdjacency->u4IfIndex == u4IfIndex)
                {
                    return pSessionEntry;
                }
            }
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name : LdpGrProcessLblRelEvent                                   */
/* Description   : This function process the GR Label Release Event          */
/* Input(s)      : pLdpGrLblRelEvtInfo                                       */
/* Output(s)     : NONE                                                      */
/* Return(s)     : NONE                                                      */
/*****************************************************************************/
VOID
LdpGrProcessLblRelEvent (tLdpGrLblRelEvtInfo *pLdpGrLblRelEvtInfo)
{
   uLabel            Label;

   MEMSET (&Label, 0, sizeof (uLabel));
   MEMCPY (&Label, &pLdpGrLblRelEvtInfo->Label, sizeof (uLabel));
   if (LdpGrFreeLabel (Label.u4GenLbl) == LDP_FAILURE)
   {
       LDP_DBG1 (GRACEFUL_DEBUG, "LdpGrProcessLblRelEvent: Failed to Release the Label: %d\n", Label.u4GenLbl);
   }
   else
   {
        LDP_DBG1 (GRACEFUL_DEBUG, "LdpGrProcessLblRelEvent: Label Released Successfully: %d\n", Label.u4GenLbl);
   }
}


/*****************************************************************************/
/* Function Name : LdpGrLblRelEventHandler                                   */
/* Description   : This function post the event LDP_GR_LBL_REL_EVENT to the  */
/*                 LDP msg queue                                             */
/* Input(s)      : pLdpGrLblRelEvtInfo                                       */
/* Output(s)     : NONE                                                      */
/* Return(s)     : NONE                                                      */
/*****************************************************************************/
VOID
LdpGrLblRelEventHandler (uLabel Label)
{
    tLdpIfMsg           LdpIfMsg;


    MEMSET (&LdpIfMsg, 0, sizeof (tLdpIfMsg));
    LdpIfMsg.u4MsgType = LDP_GR_LBL_REL_EVENT;
    MEMCPY (&LdpIfMsg.u.LdpGrLblRelEvt.Label, &Label, sizeof (uLabel));

    if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Failed to EnQ TCP Event to LDP Task\n");
    }
}


/*****************************************************************************/
/* Function Name : LdpGrReserveResources                                     */
/* Description   : This function Reserves the Labels from the ILM  hw list   */
/*                 and CFA tunnel interfaces based on the flag from the FTN  */
/*                 hw List and mark the ILM and FTN Hw List Entries Stale    */
/* Input(s)      : u1TnlIntfFlag                                             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpGrReserveResources (UINT1 u1TnlIntfReservationFlag)
{
    tFTNHwListEntry     *pFTNHwListEntry = NULL;
    tILMHwListEntry     *pILMHwListEntry = NULL;
    UINT4               u4TnlIntf = 0;
    UINT4               u4L3Intf = 0;
    UINT4               u4InLabel = 0;

    pFTNHwListEntry = MplsHwListGetFirstFTNHwListEntry ();
    while (pFTNHwListEntry != NULL)
    {
        /* Mark the dynamic FTN hw list entry as STALE */
        if (MPLS_FTN_HW_LIST_IS_STATIC (pFTNHwListEntry) == MPLS_FALSE)
        {
            MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry) = MPLS_TRUE;

            if (u1TnlIntfReservationFlag == LDP_TRUE)
            {
                u4TnlIntf = MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry);

                u4L3Intf = MPLS_FTN_HW_LIST_OUT_L3_INTF (pFTNHwListEntry);

                if (CfaIfmCreateStackMplsTnlIntfFromTnlIfIndex (u4L3Intf,
                                     u4TnlIntf) == CFA_FAILURE)
                {
                    LDP_DBG1 (GRACEFUL_DEBUG, "LdpGrReserveResources: Failed to Reserve "
                                             "MPLS TUNNEL interface: %d for FTN Hw List Entry\n",
                                              u4TnlIntf);
                    /* Failure not returned explicitly */
                }
                else
                {
                    LDP_DBG1 (GRACEFUL_DEBUG, "LdpGrReserveResources: MPLS TUNNEL interface: %d "
                                              "reservation SUCCESS for FTN Hw List Entry\n",
                                              u4TnlIntf);
                }
            }
        }
        pFTNHwListEntry = MplsHwListGetNextFTNHwListEntry (pFTNHwListEntry);
    }
    /* Read the ILM hw list */
    pILMHwListEntry = MplsHwListGetFirstILMHwListEntry ();
    while (pILMHwListEntry != NULL)
    {
        /* Mark the dynamic ILM hw list entry as stale */
        if (MPLS_ILM_HW_LIST_IS_STATIC (pILMHwListEntry) == MPLS_FALSE)
        {
            MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry) = MPLS_TRUE;

            /* Fetch the Label from the ILM hw List Entry and reserve it */
            u4InLabel = MPLS_ILM_HW_LIST_IN_LABEL (pILMHwListEntry);
            if(LdpGrReserveLabel (u4InLabel) == LDP_FAILURE)
            {
                /* Failure not returned Explicitly */
                LDP_DBG1 (GRACEFUL_DEBUG, "LdpGrReserveResources Error: Label Reservation "
                        "failed for the Label: %d\n", u4InLabel);
            }
            else
            {
                LDP_DBG1 (GRACEFUL_DEBUG, "LdpGrReserveResources: Label Reserved Successfully: %d "
                        "for the ILM Hw List entry\n", u4InLabel);
            }
        }
        pILMHwListEntry = MplsHwListGetNextILMHwListEntry (pILMHwListEntry);
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpGrReserveLabel                                         */
/* Description   : This function Reserves the given label for LDP GR purpose */
/* Input(s)      : u4Label - Label to be reserved                            */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpGrReserveLabel (UINT4 u4Label)
{
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT4              u4GroupId = 0;
    tLdpEntity         *pLdpEntity = NULL;

    TMO_SLL_Scan (&(LDP_ENTITY_LIST (MPLS_DEF_INCARN)), pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity), pLdpEthParams,
                tLdpEthParams *)
        {
            if ((u4Label >= pLdpEthParams->u4MinLabelVal) &&
                    (u4Label <= pLdpEthParams->u4MaxLabelVal))
            {
                u4GroupId = pLdpEthParams->u2EthLblRangeId;
                break;
            }
        }
    }
    if (u4GroupId > 0)
    {
        if (LblMgrAssignLblToLblGroup((UINT2)u4GroupId, 0, u4Label) == LBL_SUCCESS)
        {
            return LDP_SUCCESS;
        }
    }
    return LDP_FAILURE;
}


/*****************************************************************************/
/* Function Name : LdpGrFreeLabel                                            */
/* Description   : This function Free the given label for LDP GR purpose     */
/* Input(s)      : u4Label - Label to be freed                               */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpGrFreeLabel (UINT4 u4Label)
{
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT4              u4GroupId = 0;
    tLdpEntity         *pLdpEntity = NULL;


    TMO_SLL_Scan (&(LDP_ENTITY_LIST (LDP_CUR_INCARN)), pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity), pLdpEthParams,
                tLdpEthParams *)
        {
            if ((u4Label >= pLdpEthParams->u4MinLabelVal) &&
                    (u4Label <= pLdpEthParams->u4MaxLabelVal))
            {
                u4GroupId = pLdpEthParams->u2EthLblRangeId;
                break;
            }
        }
    }

    /* If Group Id is 0, No such label found in the any Label Groups */
    if (u4GroupId > 0)
    {
        if (ldpRelLblToLblGroup ((UINT2)u4GroupId, 0, u4Label) == LBL_SUCCESS)
        {
            LDP_DBG3 (GRACEFUL_DEBUG, "%s, %d Label: %d Released Successfully\n",
                                       __func__, __LINE__, u4Label);

            return LDP_SUCCESS;
        }
    }
    LDP_DBG3 (GRACEFUL_DEBUG, "%s, %d: Label: %d does not belong "
                             "any LABEL GROUP \n", __func__, __LINE__, u4Label);
    return LDP_FAILURE;
}


VOID
LdpEntityDownAtGoStandBy()
{

	UINT2               u2Incarn;
	tLdpEntity         *pLdpEntity = NULL;
	tTMO_SLL_NODE      *pSllNode = NULL;
	tTMO_SLL_NODE      *pTempSllNode = NULL;

    if ( gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrCapability == LDP_GR_CAPABILITY_FULL )
    {
        gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrProgressStatus =
                                        LDP_GR_SHUT_DOWN_IN_PROGRESS;
    }
	for (u2Incarn = 0; u2Incarn < LDP_MAX_INCARN; u2Incarn++)
	{
		if (LDP_INCARN_STATUS (u2Incarn) == ACTIVE)
		{
			TMO_DYN_SLL_Scan (&LDP_ENTITY_LIST (u2Incarn), pSllNode,
					pTempSllNode, tTMO_SLL_NODE *)
			{
				pLdpEntity = (tLdpEntity *) pSllNode;
				LdpDownLdpEntity (pLdpEntity);
			}
		}
	}
   if ( gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrCapability == LDP_GR_CAPABILITY_FULL )
     {
         gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrProgressStatus = LDP_GR_NOT_STARTED;
     }

}

VOID
LdpEntityUpAtGoStandBy()
{

	UINT2               u2Incarn;
	tLdpEntity         *pLdpEntity = NULL;
	tTMO_SLL_NODE      *pSllNode = NULL;
	tTMO_SLL_NODE      *pTempSllNode = NULL;

	for (u2Incarn = 0; u2Incarn < LDP_MAX_INCARN; u2Incarn++)
	{
		if (LDP_INCARN_STATUS (u2Incarn) == ACTIVE)
		{
			TMO_DYN_SLL_Scan (&LDP_ENTITY_LIST (u2Incarn), pSllNode,
					pTempSllNode, tTMO_SLL_NODE *)
			{
				pLdpEntity = (tLdpEntity *) pSllNode;
				LdpUpLdpEntity (pLdpEntity);
			}
		}
	}
}


#endif
