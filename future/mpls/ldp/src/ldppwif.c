/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: ldppwif.c,v 1.19 2016/07/14 13:00:09 siva Exp $
 *
 * Description: This file contains the code for interface from LDP
 *              module with L2VPN Module.
 *******************************************************************/

#include "ldpincs.h"

/*****************************************************************************/
/* Function Name : LdpSendPwVcLblMapMsg                                      */
/* Description   : This function sends the Label Map Msg with PwVc FEC TLV   */
/*                 or Gen FEC TLV to the peer identified by session          */
/*                 pLdpPeerSession and with label pLabel.                    */
/* Input(s)      : pLdpPeerSession - Pointer to the session to which the     */
/*                                   message  is to be sent                  */
/*                 pFecInfo        - Pointer to PwVc Fec Information         */
/*                 pLabel          - Pointer to Label Information            */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpSendPwVcLblMapMsg (tLdpSession * pLdpPeerSession, tPwVcLblMsgInfo * pFecInfo,
                      uLabel * pLabel)
{
    UINT2               u2OptParamLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT2               u2MtuValue = 0;
    UINT4               u4Offset = 0;
    UINT1               u1FecElmntType = 0;
    UINT2               u2PwVcTypeAndCBit;
    tPWIfParam          IfParams = { 0, 0, 0 };
    tPWIfParam          IfParams1 = { 0, 0, 0 };
    tPWIfParam          IfParams2 = { 0, 0, 0 };
    tVccvParamTlv       VccvParamTlv = { 0, 0, 0, 0 };
    tPWFec              FecElmnt;
    UINT1               u1AgiType = LDP_GEN_PWVC_AGI_TYPE;
    UINT1               u1AiiType = LDP_GEN_PWVC_AII_TYPE;
    UINT2               u2ReqVlanId = 0;
    tPwStatusTlv        PwStatusTlv = { {OSIX_HTONS (LDP_PW_STATUS_TLV),
                                         OSIX_HTONS (LDP_PW_STATUS_CODE_LEN)},
    0
    };
    tTlvHdr             PwIfParamTlvHdr =
        { OSIX_HTONS (LDP_PW_IF_PARAMS_TLV), 0 };
    tPwGroupingIdTlv    PwGroupingId = { {OSIX_HTONS (LDP_PW_GROUPING_ID_TLV),
                                          OSIX_HTONS (LDP_PW_GROUPING_ID_LEN)},
    0
    };
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tLblReqMsgIdTlv     ReqMsgIdTlv = { {OSIX_HTONS (LDP_LBL_REQ_MSGID_TLV),
                                         OSIX_HTONS (LDP_MSG_ID_LEN)}, 0
    };
    tGenLblTlv          GenLblTlv;
    tLdpId              PeerLdpId;
    UINT1               u1PwVcInfoLen = 0;
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_MAPPING_MSG), 0, 0 };
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;

    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    IfParams.u1ParamID = LDP_IFPARAM_IF_MTU;
    IfParams.u1Length = LDP_IFPARAM_IF_MTU_LEN;
    u2PwVcTypeAndCBit = pFecInfo->i1PwType;
    MEMSET (&ConnId, LDP_ZERO, sizeof (tGenAddr));
    MEMSET (&LdpIfAddrZero, LDP_ZERO, sizeof (tGenU4Addr));

    if ((pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET) ||
        (pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN))
    {
        if (pFecInfo->pi1LocalIfString != NULL)
        {
            /* make the if params for descriptor */
            IfParams1.u1ParamID = LDP_IFPARAM_IF_DESC_STR;
            IfParams1.u1Length = (UINT1) (LDP_PWVC_FEC_IF_PARAM_HDR_LEN +
                                          STRLEN (pFecInfo->pi1LocalIfString));
        }
    }

    if (pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN)
    {
        if ((pFecInfo->u2ReqVlanId != LDP_PWVC_INVALID_VLAN_ID) &&
            (pFecInfo->u2ReqVlanId != LDP_ZERO))
        {
            IfParams2.u1ParamID = LDP_IFPARAM_REQ_VLAN_ID;
            IfParams2.u1Length = LDP_IFPARAM_REQ_VLAN_ID_LEN;
            u2ReqVlanId = OSIX_HTONS ((UINT2) pFecInfo->u2ReqVlanId);
        }
    }

    LDP_SET_PWVC_C_BIT (u2PwVcTypeAndCBit, pFecInfo->i1ControlWord);

    if (pFecInfo->u1LocalCapabAdvert & L2VPN_PW_VCCV_CAPABLE)
    {
        /*Constructing VCCV Param Tlv */
        VccvParamTlv.u1ParamID = LDP_IFPARAM_VCCV_PARAM;
        VccvParamTlv.u1Length = LDP_IFPARAM_VCCV_PARAM_LEN;

        VccvParamTlv.u1CCTypes = pFecInfo->u1LocalCCAdvert;
        VccvParamTlv.u1CVTypes = pFecInfo->u1LocalCVAdvert;
    }

    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        u1PwVcInfoLen =
            (UINT1) (LDP_PWVC_FEC_ELEM_PWVCID_LEN + IfParams.u1Length +
                     IfParams1.u1Length + IfParams2.u1Length +
                     VccvParamTlv.u1Length);

        FecElmnt.u4GroupID = OSIX_HTONL (pFecInfo->u4GroupID);
        FecElmnt.u4PWID = OSIX_HTONL (pFecInfo->u4PwVcID);
        FecElmnt.u2PwType = pFecInfo->i1PwType;
        FecElmnt.u1CBit = pFecInfo->i1ControlWord;
        FecElmnt.u1VcInfoLen = u1PwVcInfoLen;

        FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PwVcInfoLen
                                          + LDP_PWVC_FEC_ELEM_GROUP_ID_LEN));
        u2MandParamLen += LDP_MSG_ID_LEN;

        /* Updating for the mandatory 'Fec Tlv' length */
        u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                           LDP_PWVC_FEC_ELEM_GROUP_ID_LEN + u1PwVcInfoLen);
        u1FecElmntType = LDP_FEC_PWVC_TYPE;
    }

    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        u1PwVcInfoLen = (UINT1) (pFecInfo->u1AgiLen + pFecInfo->u1SaiiLen +
                                 pFecInfo->u1TaiiLen +
                                 (3 * LDP_GEN_PWVC_FEC_SUB_HDR_LEN));

        /* Length of AGI, SAII and TAII sub hdr */
        FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN +
                                          u1PwVcInfoLen));

        PwIfParamTlvHdr.u2TlvLen = OSIX_HTONS ((IfParams.u1Length +
                                                IfParams1.u1Length +
                                                IfParams2.u1Length +
                                                VccvParamTlv.u1Length));

        u2MandParamLen += LDP_MSG_ID_LEN;

        /* Updating for the mandatory 'PW Gen Fec Tlv' length */
        u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                           u1PwVcInfoLen);

        /* Updating for the 'PW Interface Param Tlv' length */
        u2OptParamLen += (LDP_TLV_HDR_LEN + IfParams.u1Length +
                          IfParams1.u1Length + IfParams2.u1Length +
                          VccvParamTlv.u1Length);

        /* Updating for the 'PW Grouping ID Tlv' length */
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);

        u1FecElmntType = LDP_FEC_GEN_PWVC_TYPE;
    }

    /* Updating for the mandatory Generic Label Tlv length */
    u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_LABEL_LEN);

    /* Add PW Status Tlv only when the peer supports it */
    if (pFecInfo->u1LocalCapabAdvert & L2VPN_PW_STATUS_INDICATION)
    {
        /* Updating for the PW Status Tlv length */
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_PW_STATUS_CODE_LEN);
    }

    if (pFecInfo->u4LblReqId != LDP_INVALID_REQ_ID)
    {
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN);
    }

    /* Allocating the buffer for Label Mapping Message */
    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2OptParamLen + u2MandParamLen),
                                      LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buffer for Lbl Mapping Msg \n");
        return LDP_FAILURE;
    }

    /* Construct Message Hdr and ID */
    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2OptParamLen + u2MandParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLdpPeerSession));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pLdpPeerSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct Fec Hdr Tlv */
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    /* Construct common FEC Element fields */
    /* Construct FEC Element Type */
    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    /* Construct PW Type and C Bit */
    u2PwVcTypeAndCBit = OSIX_HTONS (u2PwVcTypeAndCBit);
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &u2PwVcTypeAndCBit, u4Offset,
                                   LDP_UINT2_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw VcType and C Bit to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT2_LEN;

    /* Construct PW Info Length */
    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1PwVcInfoLen, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw Vc info length to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    /* Construct FEC 128 fields */
    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.u4GroupID, u4Offset,
             LDP_PWVC_FEC_ELEM_GROUP_ID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Pw Vc Group ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_PWVC_FEC_ELEM_GROUP_ID_LEN;

        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.u4PWID, u4Offset,
             LDP_PWVC_FEC_ELEM_PWVCID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Pw Vc ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_PWVC_FEC_ELEM_PWVCID_LEN;

        /* Construct Interface Parameters sub-TLVs */
        /* Construct Interface MTU Params */
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams.u1ParamID,
                                       u4Offset, LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams.u1Length,
                                       u4Offset, LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params Length to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        u2MtuValue = OSIX_HTONS (pFecInfo->u2IfMtu);
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &u2MtuValue, u4Offset,
                                       LDP_UINT2_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params MTU Value to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT2_LEN;

        /* Construct Interface Description Param */
        if (((pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET) ||
             (pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN)) &&
            (pFecInfo->pi1LocalIfString != NULL))
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams1.u1ParamID,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams1.u1Length,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg,
                                           (UINT1 *) pFecInfo->pi1LocalIfString,
                                           u4Offset,
                                           STRLEN (pFecInfo->pi1LocalIfString))
                == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += STRLEN (pFecInfo->pi1LocalIfString);
        }

        /* Construct Interface Requested VLAN ID Params */
        if ((pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN) &&
            ((pFecInfo->u2ReqVlanId != LDP_ZERO) &&
             (pFecInfo->u2ReqVlanId != LDP_PWVC_INVALID_VLAN_ID)))
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams2.u1ParamID,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams2.u1Length,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &u2ReqVlanId, u4Offset,
                 LDP_UINT2_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT2_LEN;
        }
        if (pFecInfo->u1LocalCapabAdvert & L2VPN_PW_VCCV_CAPABLE)
        {
            /*VCCV parameter Tlv */
            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &VccvParamTlv, u4Offset,
                 LDP_IFPARAM_VCCV_PARAM_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Pw Vc VCCV parameter ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_IFPARAM_VCCV_PARAM_LEN;
        }
    }

    /* Construct FEC 129 fields */
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        u1AgiType = pFecInfo->u1AgiType;
        if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AgiType, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying AGI Type to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (CRU_BUF_Copy_OverBufChain
            (pMsg, &pFecInfo->u1AgiLen, u4Offset, LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying AGI Length to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (pFecInfo->u1AgiLen > LDP_ZERO)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, pFecInfo->au1Agi, u4Offset,
                                           pFecInfo->u1AgiLen) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying AGI Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += pFecInfo->u1AgiLen;
        }

        u1AiiType = pFecInfo->u1SaiiType;
        if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Source AII Type to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1SaiiLen, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying SAII Length to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (pFecInfo->u1SaiiLen > LDP_ZERO)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, pFecInfo->au1Saii, u4Offset,
                                           pFecInfo->u1SaiiLen) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying SAII Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += pFecInfo->u1SaiiLen;
        }
        u1AiiType = pFecInfo->u1TaiiType;
        if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Target AII Type to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1TaiiLen, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying TAII Length to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (pFecInfo->u1TaiiLen > LDP_ZERO)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, pFecInfo->au1Taii, u4Offset,
                                           pFecInfo->u1TaiiLen) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying TAII Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += pFecInfo->u1TaiiLen;
        }
    }
    /* Construct Generic Label Tlv */
    GenLblTlv.GenLblTlvHdr.u2TlvType = OSIX_HTONS (LDP_GEN_LABEL_TLV);
    GenLblTlv.GenLblTlvHdr.u2TlvLen = OSIX_HTONS (LDP_LABEL_LEN);

    GenLblTlv.u4GenLbl = OSIX_HTONL (pLabel->u4GenLbl);
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &GenLblTlv, u4Offset,
                                   (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Failed copying Label Tlv to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_LABEL_LEN);

    /* Construct FEC 129 fields */
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        /* contruct PW Interface Parameters TLV */
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &PwIfParamTlvHdr, u4Offset,
             LDP_TLV_HDR_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying PW If Param TLV Hdr to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_TLV_HDR_LEN;

        /* Construct Interface Parameters sub-TLVs */
        /* Construct Interface MTU Params */
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams.u1ParamID,
                                       u4Offset, LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams.u1Length,
                                       u4Offset, LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params Length to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        u2MtuValue = OSIX_HTONS (pFecInfo->u2IfMtu);
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &u2MtuValue, u4Offset,
                                       LDP_UINT2_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params MTU Value to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT2_LEN;

        /* Construct Interface Description Param */
        if (((pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET) ||
             (pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN)) &&
            (pFecInfo->pi1LocalIfString != NULL))
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams1.u1ParamID,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams1.u1Length,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg,
                                           (UINT1 *) pFecInfo->pi1LocalIfString,
                                           u4Offset,
                                           STRLEN (pFecInfo->
                                                   pi1LocalIfString)) ==
                CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += STRLEN (pFecInfo->pi1LocalIfString);
        }

        /* Construct Interface Requested VLAN ID Params */
        if ((pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN) &&
            ((pFecInfo->u2ReqVlanId != LDP_ZERO) &&
             (pFecInfo->u2ReqVlanId != LDP_PWVC_INVALID_VLAN_ID)))
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams2.u1ParamID,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams2.u1Length,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &u2ReqVlanId, u4Offset,
                 LDP_UINT2_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT2_LEN;
        }

        if (pFecInfo->u1LocalCapabAdvert & L2VPN_PW_VCCV_CAPABLE)
        {
            /*Constructing VCCV param Tlv for FEC129 */
            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &VccvParamTlv, u4Offset,
                 LDP_IFPARAM_VCCV_PARAM_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Pw Vc VCCV parameter ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_IFPARAM_VCCV_PARAM_LEN;
        }
        /* Construct PW Grouping ID TLV */
        if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
        {
            PwGroupingId.u4GrpId = OSIX_HTONL (pFecInfo->u4GroupID);

            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &PwGroupingId, u4Offset,
                 (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN)) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Failed copying Pw Grouping ID Tlv to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);
        }
    }

    /* Construct PW Status Tlv only when the peer supports it */
    if (pFecInfo->u1LocalCapabAdvert & L2VPN_PW_STATUS_INDICATION)
    {
        /* Construct PW Status TLV */

        /*Update the Local status */
        PwStatusTlv.u4PwStatusCode = OSIX_HTONL (pFecInfo->u4PwStatusCode);
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &PwStatusTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN
                                        + LDP_PW_STATUS_CODE_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying Pw Status Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_PW_STATUS_CODE_LEN);
    }

    /* Construct Optional Param TLV */
    if (pFecInfo->u4LblReqId != LDP_INVALID_REQ_ID)
    {
        /* Construct Label Request Msg Id Tlv */
        ReqMsgIdTlv.u4LblReqMsgId = OSIX_HTONL (pFecInfo->u4LblReqId);

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &ReqMsgIdTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying ReqMsgId Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN);
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLdpPeerSession);
    LDP_DBG6 (LDP_ADVT_MISC,
              "ADVT: Sending Lbl Map Msg To   : %#x:%x:%x:%x:%x:%x \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2],
              PeerLdpId[3], PeerLdpId[4], PeerLdpId[5]);

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLdpPeerSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLdpPeerSession->pLdpPeer->pLdpEntity),
                    LDP_FALSE, ConnId,
                    LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Label Mapping Msg\n");
        return LDP_FAILURE;
    }
    LDP_DBG (LDP_ADVT_MISC, "ADVT: Successfuly sent Label Mapping Msg\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendPwVcLblReqMsg                                      */
/* Description   : This function sends the Label Req Msg with PwVc FEC TLV   */
/*                 or Gen FEC TLV to the peer identified by session          */
/*                 pLdpPeerSession and with label pLabel.                    */
/* Input(s)      : pLdpPeerSession - Pointer to the session to which the     */
/*                                   message  is to be sent                  */
/*                 pFecInfo        - Pointer to PwVc Fec Information         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpSendPwVcLblReqMsg (tLdpSession * pLdpPeerSession, tPwVcLblMsgInfo * pFecInfo)
{
    UINT2               u2OptParamLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT2               u2MtuValue = 0;
    UINT4               u4Offset = 0;
    UINT1               u1FecElmntType = 0;
    UINT2               u2PwVcTypeAndCBit;
    tPWIfParam          IfParams = { 0, 0, 0 };
    tPWIfParam          IfParams1 = { 0, 0, 0 };
    tPWIfParam          IfParams2 = { 0, 0, 0 };
    tPWFec              FecElmnt;
    UINT1               u1AgiType = LDP_GEN_PWVC_AGI_TYPE;
    UINT1               u1AiiType = LDP_GEN_PWVC_AII_TYPE;
    UINT2               u2ReqVlanId = 0;
    tTlvHdr             PwIfParamTlvHdr =
        { OSIX_HTONS (LDP_PW_IF_PARAMS_TLV), 0 };
    tPwGroupingIdTlv    PwGroupingId = { {OSIX_HTONS (LDP_PW_GROUPING_ID_TLV),
                                          OSIX_HTONS (LDP_PW_GROUPING_ID_LEN)},
    0
    };
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tLdpId              PeerLdpId;
    UINT1               u1PwVcInfoLen = 0;
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_REQUEST_MSG), 0, 0 };
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;

    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    LdpIfAddrZero.Addr.u4Addr = LDP_ZERO;
    /*MPLS_IPv6 add end*/
    IfParams.u1ParamID = LDP_IFPARAM_IF_MTU;
    IfParams.u1Length = LDP_IFPARAM_IF_MTU_LEN;
    u2PwVcTypeAndCBit = pFecInfo->i1PwType;
    MEMSET (&ConnId, LDP_ZERO, sizeof (tGenAddr));
    MEMSET (&LdpIfAddrZero, LDP_ZERO, sizeof (tGenU4Addr));

    if ((pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET) ||
        (pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN))
    {
        if (pFecInfo->pi1LocalIfString != NULL)
        {
            /*  make the if params for descriptor */
            IfParams1.u1ParamID = LDP_IFPARAM_IF_DESC_STR;
            IfParams1.u1Length = (UINT1) (LDP_PWVC_FEC_IF_PARAM_HDR_LEN +
                                          STRLEN (pFecInfo->pi1LocalIfString));
        }
    }

    if (pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN)
    {
        if ((pFecInfo->u2ReqVlanId != LDP_PWVC_INVALID_VLAN_ID) &&
            (pFecInfo->u2ReqVlanId != LDP_ZERO))
        {
            /* Requested VLANID If params */
            IfParams2.u1ParamID = LDP_IFPARAM_REQ_VLAN_ID;
            IfParams2.u1Length = LDP_PWVC_FEC_IF_PARAM_HDR_LEN;
            u2ReqVlanId = OSIX_HTONS ((UINT2) pFecInfo->u2ReqVlanId);
        }
    }

    LDP_SET_PWVC_C_BIT (u2PwVcTypeAndCBit, pFecInfo->i1ControlWord);

    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        u1PwVcInfoLen =
            (UINT1) (LDP_PWVC_FEC_ELEM_PWVCID_LEN + IfParams.u1Length +
                     IfParams1.u1Length + IfParams2.u1Length);
        FecElmnt.u4GroupID = OSIX_HTONL (pFecInfo->u4GroupID);
        FecElmnt.u4PWID = OSIX_HTONL (pFecInfo->u4PwVcID);

        FecElmnt.u2PwType = pFecInfo->i1PwType;
        FecElmnt.u1CBit = pFecInfo->i1ControlWord;
        FecElmnt.u1VcInfoLen = u1PwVcInfoLen;
        FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PwVcInfoLen
                                          + LDP_PWVC_FEC_ELEM_GROUP_ID_LEN));
        u2MandParamLen += LDP_MSG_ID_LEN;

        /* Updating for the mandatory 'Fec Tlv' length */
        u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                           LDP_PWVC_FEC_ELEM_GROUP_ID_LEN + u1PwVcInfoLen);
        u1FecElmntType = LDP_FEC_PWVC_TYPE;
    }
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        u1PwVcInfoLen = (UINT1) (pFecInfo->u1AgiLen + pFecInfo->u1SaiiLen +
                                 pFecInfo->u1TaiiLen +
                                 (3 * LDP_GEN_PWVC_FEC_SUB_HDR_LEN));

        /* Length of AGI, SAII and TAII sub hdr */
        FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN +
                                          u1PwVcInfoLen));

        PwIfParamTlvHdr.u2TlvLen = OSIX_HTONS ((IfParams.u1Length +
                                                IfParams1.u1Length +
                                                IfParams2.u1Length));

        u2MandParamLen += LDP_MSG_ID_LEN;

        /* Updating for the mandatory 'PW Gen Fec Tlv' length */
        u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                           u1PwVcInfoLen);

        /* Updating for the 'PW Interface Param Tlv' length */
        u2OptParamLen += (LDP_TLV_HDR_LEN + IfParams.u1Length +
                          IfParams1.u1Length + IfParams2.u1Length);

        /* Updating for the 'PW Grouping ID Tlv' length */
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);

        u1FecElmntType = LDP_FEC_GEN_PWVC_TYPE;
    }
    /* Allocating the buffer for Label Request Message */
    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2OptParamLen + u2MandParamLen),
                                      LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buffer for Lbl Request Msg \n");
        return LDP_FAILURE;
    }

    /* Construct Message Hdr and ID */
    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2OptParamLen + u2MandParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLdpPeerSession));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pLdpPeerSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct FEC Hdr Tlv */
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    /* Construct FEC Element */
    /* Construct FEC Element Type */
    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    /* Construct PW Type and C Bit */
    u2PwVcTypeAndCBit = OSIX_HTONS (u2PwVcTypeAndCBit);
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &u2PwVcTypeAndCBit, u4Offset,
                                   LDP_UINT2_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw VcType and C Bit to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT2_LEN;

    /* Construct PW Info Length */
    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1PwVcInfoLen, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw Vc info length to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    /* Construct FEC 128 fields */
    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        /* Construct Group ID */
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.u4GroupID, u4Offset,
             LDP_PWVC_FEC_ELEM_GROUP_ID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Pw Vc Group ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_PWVC_FEC_ELEM_GROUP_ID_LEN;

        /* Construct PW ID */
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.u4PWID, u4Offset,
             LDP_PWVC_FEC_ELEM_PWVCID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Pw Vc ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_PWVC_FEC_ELEM_PWVCID_LEN;

        /* Construct Interface Parameters sub-TLVs */
        /* Construct Interface MTU Params */
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams.u1ParamID,
                                       u4Offset, LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams.u1Length,
                                       u4Offset, LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params Length to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        u2MtuValue = OSIX_HTONS (pFecInfo->u2IfMtu);
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &u2MtuValue, u4Offset,
                                       LDP_UINT2_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params MTU Value to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT2_LEN;

        /* Construct Interface Description Param */
        if (((pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET) ||
             (pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN)) &&
            (pFecInfo->pi1LocalIfString != NULL))
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams1.u1ParamID,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams1.u1Length,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg,
                                           (UINT1 *) pFecInfo->pi1LocalIfString,
                                           u4Offset,
                                           STRLEN (pFecInfo->pi1LocalIfString))
                == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += STRLEN (pFecInfo->pi1LocalIfString);
        }

        /* Construct Interface Requested VLAN ID Params */
        if ((pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN) &&
            ((pFecInfo->u2ReqVlanId != LDP_ZERO) &&
             (pFecInfo->u2ReqVlanId != LDP_PWVC_INVALID_VLAN_ID)))
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams2.u1ParamID,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams2.u1Length,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &u2ReqVlanId, u4Offset,
                 LDP_UINT2_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT2_LEN;
        }
    }
    /* Construct FEC 129 fields */
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        u1AgiType = pFecInfo->u1AgiType;
        if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AgiType, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying AGI Type to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (CRU_BUF_Copy_OverBufChain
            (pMsg, &pFecInfo->u1AgiLen, u4Offset, LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying AGI Length to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (pFecInfo->u1AgiLen > LDP_ZERO)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, pFecInfo->au1Agi, u4Offset,
                                           pFecInfo->u1AgiLen) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying AGI Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += pFecInfo->u1AgiLen;
        }

        u1AiiType = pFecInfo->u1SaiiType;
        if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Source AII Type to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1SaiiLen, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying SAII Length to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (pFecInfo->u1SaiiLen > LDP_ZERO)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, pFecInfo->au1Saii, u4Offset,
                                           pFecInfo->u1SaiiLen) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying SAII Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += pFecInfo->u1SaiiLen;
        }
        u1AiiType = pFecInfo->u1TaiiType;
        if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Target AII Type to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1TaiiLen, u4Offset,
                                       LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying TAII Length to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (pFecInfo->u1TaiiLen > LDP_ZERO)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, pFecInfo->au1Taii, u4Offset,
                                           pFecInfo->u1TaiiLen) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying TAII Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += pFecInfo->u1TaiiLen;
        }

        /* contruct PW Interface Parameters TLV */
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &PwIfParamTlvHdr, u4Offset,
             LDP_TLV_HDR_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying PW If Param TLV Hdr to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_TLV_HDR_LEN;

        /* Construct Interface Parameters sub-TLVs */
        /* Construct Interface MTU Params */
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams.u1ParamID,
                                       u4Offset, LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams.u1Length,
                                       u4Offset, LDP_UINT1_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params Length to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT1_LEN;

        u2MtuValue = OSIX_HTONS (pFecInfo->u2IfMtu);
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &u2MtuValue, u4Offset,
                                       LDP_UINT2_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Interface Params MTU Value to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_UINT2_LEN;

        /* Construct Interface Description Param */
        if (((pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET) ||
             (pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN)) &&
            (pFecInfo->pi1LocalIfString != NULL))
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams1.u1ParamID,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams1.u1Length,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg,
                                           (UINT1 *) pFecInfo->pi1LocalIfString,
                                           u4Offset,
                                           STRLEN (pFecInfo->pi1LocalIfString))
                == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += STRLEN (pFecInfo->pi1LocalIfString);
        }

        /* Construct Interface Requested VLAN ID Params */
        if ((pFecInfo->i1PwType == LDP_PWVC_TYPE_ETHERNET_VLAN) &&
            ((pFecInfo->u2ReqVlanId != LDP_ZERO) &&
             (pFecInfo->u2ReqVlanId != LDP_PWVC_INVALID_VLAN_ID)))
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams2.u1ParamID,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &IfParams2.u1Length,
                                           u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &u2ReqVlanId, u4Offset,
                 LDP_UINT2_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Interface Params Value to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT2_LEN;
        }

        /* Construct PW Grouping ID TLV */
        PwGroupingId.u4GrpId = OSIX_HTONL (pFecInfo->u4GroupID);

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &PwGroupingId, u4Offset,
                                       (LDP_TLV_HDR_LEN
                                        + LDP_PW_GROUPING_ID_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying Pw Grouping ID Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLdpPeerSession);
    LDP_DBG6 (LDP_ADVT_MISC,
              "ADVT: Sending Lbl Req Msg To   : %#x:%x:%x:%x:%x:%x \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2],
              PeerLdpId[3], PeerLdpId[4], PeerLdpId[5]);

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLdpPeerSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLdpPeerSession->pLdpPeer->pLdpEntity),
                    LDP_FALSE, ConnId,
                    LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Label Request Msg\n");
        return LDP_FAILURE;
    }
    LDP_DBG (LDP_ADVT_MISC, "ADVT: Successfuly sent Label Request Msg\n");

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendPwVcLblWdrawMsg                                    */
/* Description   : This function sends the Label Wdraw Msg with PwVc FEC TLV */
/*                 or Gen FEC TLV to the peer identified by session          */
/*                 pLdpPeerSession.                             */
/* Input(s)      : pLdpPeerSession - Pointer to the session to which the     */
/*                                   message  is to be sent                  */
/*                 pFecInfo        - Pointer to PwVc Fec Information         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpSendPwVcLblWdrawMsg (tLdpSession * pLdpPeerSession,
                        tPwVcLblMsgInfo * pFecInfo)
{
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_WITHDRAW_MSG), 0, 0 };
    tPWFec              FecElmnt;
    UINT1               u1AgiType = LDP_GEN_PWVC_AGI_TYPE;
    UINT1               u1AiiType = LDP_GEN_PWVC_AII_TYPE;
    tPwGroupingIdTlv    PwGroupingId = { {OSIX_HTONS (LDP_PW_GROUPING_ID_TLV),
                                          OSIX_HTONS (LDP_PW_GROUPING_ID_LEN)},
    0
    };
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tGenLblTlv          GenLabelTlv = { {OSIX_HTONS (LDP_GEN_LABEL_TLV),
                                         OSIX_HTONS (LDP_LABEL_LEN)}, 0
    };
    UINT1               u1StatusTlvLen =
        (LDP_TLV_HDR_LEN + LDP_STATUS_CODE_LEN + LDP_MSG_ID_LEN +
         LDP_MSG_TYPE_LEN);
    tStatusTlv          StatusTlv = { {OSIX_HTONS ((LDP_STATUS_TLV
                                                    | LDP_PWVC_SET_U_BIT_MASK)),
                                       OSIX_HTONS ((LDP_STATUS_CODE_LEN +
                                                    LDP_MSG_ID_LEN +
                                                    LDP_MSG_TYPE_LEN))}, 0, 0,
    0, 0
    };                            /* set the U Bit to one */
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    UINT1               u1FecElmntType = 0;
    UINT2               u2MandParamLen = 0;
    UINT2               u2OptParamLen = 0;
    UINT4               u4StatusCode = LDP_ZERO;
    UINT2               u2PwVcTypeAndCBit;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    UINT4               u4Offset = 0;
    tLdpId              PeerLdpId;
    UINT1               u1PwVcInfoLen = 0;
    u2MandParamLen += LDP_MSG_ID_LEN;
    MEMSET (&ConnId, LDP_ZERO, sizeof( tGenAddr));
    MEMSET (&LdpIfAddrZero, LDP_ZERO, sizeof (tGenU4Addr));
    u2PwVcTypeAndCBit = pFecInfo->i1PwType;
    LDP_SET_PWVC_C_BIT (u2PwVcTypeAndCBit, pFecInfo->i1ControlWord);

    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        FecElmnt.u4GroupID = OSIX_HTONL (pFecInfo->u4GroupID);
        FecElmnt.u4PWID = OSIX_HTONL (pFecInfo->u4PwVcID);

        FecElmnt.u2PwType = pFecInfo->i1PwType;
        FecElmnt.u1CBit = pFecInfo->i1ControlWord;

        if ((FecElmnt.u4PWID != LDP_ZERO) &&
            (gi4MplsSimulateFailure != LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC))
        {
            /* not a Wild Card Withdraw */
            u1PwVcInfoLen = LDP_PWVC_FEC_ELEM_PWVCID_LEN;
        }
        FecElmnt.u1VcInfoLen = u1PwVcInfoLen;

        FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PwVcInfoLen
                                          + LDP_PWVC_FEC_ELEM_GROUP_ID_LEN));

        /* Updating for the mandatory 'Fec Tlv' length */
        u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                           LDP_PWVC_FEC_ELEM_GROUP_ID_LEN + u1PwVcInfoLen);

        u1FecElmntType = LDP_FEC_PWVC_TYPE;
    }

    else if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        if (((pFecInfo->u1AgiLen != LDP_ZERO) ||
             (pFecInfo->u1SaiiLen != LDP_ZERO) ||
             (pFecInfo->u1TaiiLen != LDP_ZERO)) &&
            (gi4MplsSimulateFailure != LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC))
        {
            /* not a Wild Card Withdraw */
            u1PwVcInfoLen = (UINT1) (pFecInfo->u1AgiLen + pFecInfo->u1SaiiLen +
                                     pFecInfo->u1TaiiLen +
                                     (3 * LDP_GEN_PWVC_FEC_SUB_HDR_LEN));
        }

        if (u1PwVcInfoLen != LDP_ZERO)
        {
            FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN +
                                              u1PwVcInfoLen));

            /* Updating for the mandatory 'PW Gen Fec Tlv' length */
            u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                               u1PwVcInfoLen);
        }
        else
        {
            FecTlvHdr.u2TlvLen = OSIX_HTONS (LDP_FEC_ELMNT_HDRLEN);

            /* Updating for the mandatory 'PW Gen Fec Tlv' length */
            u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN);
        }

        /* Updating for the optional 'PW Grouping ID Tlv' length */
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);

        u1FecElmntType = LDP_FEC_GEN_PWVC_TYPE;
    }

    if (pFecInfo->u4Label != LDP_INVALID_LABEL)
    {
        /* Generic Label */
        GenLabelTlv.u4GenLbl = OSIX_HTONL (pFecInfo->u4Label);
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_LABEL_LEN);
    }

    if ((pFecInfo->i1StatusCode == LDP_STAT_PWVC_WRONG_C_BIT) ||
        (pFecInfo->i1StatusCode == LDP_STAT_PWVC_ILLEGAL_C_BIT))
    {
        /* Status Tlv */
        u2OptParamLen += u1StatusTlvLen;
        if (pFecInfo->i1StatusCode == LDP_STAT_PWVC_WRONG_C_BIT)
        {
            u4StatusCode = LDP_STAT_PWVC_WRONG_C_BIT_VAL;
            if (gi4MplsSimulateFailure ==
                LDP_SIM_FAILURE_WRONG_C_BIT_VAL_RFC_4906)
            {
                u4StatusCode = LDP_STAT_PWVC_WRONG_C_BIT_VAL_RFC_4906;
            }

        }
        if (pFecInfo->i1StatusCode == LDP_STAT_PWVC_ILLEGAL_C_BIT)
        {
            u4StatusCode = LDP_STAT_PWVC_ILLEGAL_C_BIT_VAL;
        }
        u4StatusCode = OSIX_HTONL (u4StatusCode);

        StatusTlv.u4StatusCode = u4StatusCode;
    }

    /* Allocate Buffer for Label Withdraw message */
    pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2OptParamLen + u2MandParamLen),
                                         LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buf for Lbl Wthdraw Msg\n");
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2MandParamLen + u2OptParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLdpPeerSession));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pLdpPeerSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(FecTlvHdr), u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    u2PwVcTypeAndCBit = OSIX_HTONS (u2PwVcTypeAndCBit);
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &u2PwVcTypeAndCBit, u4Offset,
                                   LDP_UINT2_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw VcType and C Bit to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT2_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1PwVcInfoLen, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw Vc info length to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    /* contruct FEC 128 fields */
    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.u4GroupID, u4Offset,
             LDP_PWVC_FEC_ELEM_GROUP_ID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Pw Vc Group ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_PWVC_FEC_ELEM_GROUP_ID_LEN;
        if ((FecElmnt.u4PWID != LDP_ZERO) &&
            (gi4MplsSimulateFailure != LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC))
        {
            /* not a Wild Card Withdraw  so add PWID */
            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &FecElmnt.u4PWID, u4Offset,
                 LDP_PWVC_FEC_ELEM_PWVCID_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Pw Vc ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_PWVC_FEC_ELEM_PWVCID_LEN;
        }
    }
    /* Construct FEC 129 fields */
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        if (u1PwVcInfoLen != LDP_ZERO)
        {
            u1AgiType = pFecInfo->u1AgiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AgiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying AGI Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain
                (pMsg, &pFecInfo->u1AgiLen, u4Offset,
                 LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying AGI Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1AgiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain (pMsg, pFecInfo->au1Agi, u4Offset,
                                               pFecInfo->u1AgiLen) ==
                    CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying AGI Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1AgiLen;
            }

            u1AiiType = pFecInfo->u1SaiiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Source AII Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1SaiiLen, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying SAII Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1SaiiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg, pFecInfo->au1Saii, u4Offset,
                     pFecInfo->u1SaiiLen) == CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying SAII Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1SaiiLen;
            }
            u1AiiType = pFecInfo->u1TaiiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Target AII Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1TaiiLen, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying TAII Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1TaiiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg, pFecInfo->au1Taii, u4Offset,
                     pFecInfo->u1TaiiLen) == CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying TAII Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1TaiiLen;
            }
        }
    }
    /* Construct Optional Label TLV */
    if (pFecInfo->u4Label != LDP_INVALID_LABEL)
    {
        /* Copy the generic label */
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &GenLabelTlv,
                                       u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Generic Lbl to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_TLV_HDR_LEN + LDP_LABEL_LEN;
    }

    /* Construct Optional Status TLV */
    if ((pFecInfo->i1StatusCode == LDP_STAT_PWVC_WRONG_C_BIT) ||
        (pFecInfo->i1StatusCode == LDP_STAT_PWVC_ILLEGAL_C_BIT))
    {
        /* Status Tlv */
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &StatusTlv, u4Offset,
                                       u1StatusTlvLen) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "NOTIF: Error while copying Status Tlv to buffer\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += u1StatusTlvLen;
    }

    /* Construct PW Grouping ID TLV */
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        PwGroupingId.u4GrpId = OSIX_HTONL (pFecInfo->u4GroupID);

        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &PwGroupingId, u4Offset,
             (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN)) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying Pw Grouping ID Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLdpPeerSession);
    LDP_DBG6 (LDP_ADVT_MISC,
              "ADVT: Sending Lbl Wthdraw Msg To   : %#x:%x:%x:%x:%x:%x \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2],
              PeerLdpId[3], PeerLdpId[4], PeerLdpId[5]);
    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLdpPeerSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLdpPeerSession->pLdpPeer->pLdpEntity), LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Lbl Wthdraw Msg \n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_ADVT_MISC, "ADVT: Successfuly sent lbl Wthdraw Msg\n");

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendPwVcLblRelMsg                                      */
/* Description   : This function sends the Label Rel Msg with PwVc FEC TLV   */
/*                 or Gen FEC TLV to the peer identified by session          */
/*           pLdpPeerSession.                             */
/* Input(s)      : pLdpPeerSession - Pointer to the session to which the     */
/*                                   message  is to be sent                  */
/*                 pFecInfo        - Pointer to PwVc Fec Information         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpSendPwVcLblRelMsg (tLdpSession * pLdpPeerSession, tPwVcLblMsgInfo * pFecInfo)
{
    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_LBL_RELEASE_MSG), 0, 0 };
    tPWFec              FecElmnt;
    UINT1               u1AgiType = LDP_GEN_PWVC_AGI_TYPE;
    UINT1               u1AiiType = LDP_GEN_PWVC_AII_TYPE;
    tPwGroupingIdTlv    PwGroupingId = { {OSIX_HTONS (LDP_PW_GROUPING_ID_TLV),
                                          OSIX_HTONS (LDP_PW_GROUPING_ID_LEN)},
    0
    };
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tGenLblTlv          GenLabelTlv = { {OSIX_HTONS (LDP_GEN_LABEL_TLV),
                                         OSIX_HTONS (LDP_LABEL_LEN)}, 0
    };
    UINT1               u1StatusTlvLen =
        (LDP_TLV_HDR_LEN + LDP_STATUS_CODE_LEN + LDP_MSG_ID_LEN +
         LDP_MSG_TYPE_LEN);
    tStatusTlv          StatusTlv = { {OSIX_HTONS ((LDP_STATUS_TLV
                                                    | LDP_PWVC_SET_U_BIT_MASK)),
                                       OSIX_HTONS ((LDP_STATUS_CODE_LEN +
                                                    LDP_MSG_ID_LEN +
                                                    LDP_MSG_TYPE_LEN))}, 0, 0,
    0, 0
    };                            /* set the U Bit to one */
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    UINT1               u1FecElmntType = 0;
    UINT1               u1PwVcInfoLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT4               u4StatusCode = LDP_ZERO;
    UINT2               u2OptParamLen = 0;
    UINT2               u2PwVcTypeAndCBit;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    UINT4               u4Offset = 0;
    tLdpId              PeerLdpId;
    u2MandParamLen += LDP_MSG_ID_LEN;

    MEMSET (&ConnId, LDP_ZERO, sizeof( tGenAddr));
    MEMSET (&LdpIfAddrZero, LDP_ZERO, sizeof (tGenU4Addr));
    u2PwVcTypeAndCBit = pFecInfo->i1PwType;
    LDP_SET_PWVC_C_BIT (u2PwVcTypeAndCBit, pFecInfo->i1ControlWord);

    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        FecElmnt.u4GroupID = OSIX_HTONL (pFecInfo->u4GroupID);
        FecElmnt.u4PWID = OSIX_HTONL (pFecInfo->u4PwVcID);

        FecElmnt.u2PwType = pFecInfo->i1PwType;
        FecElmnt.u1CBit = pFecInfo->i1ControlWord;

        if ((FecElmnt.u4PWID != LDP_ZERO) &&
            (gi4MplsSimulateFailure != LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC))
        {
            /* not a Wild Card Label Release */
            u1PwVcInfoLen = LDP_PWVC_FEC_ELEM_PWVCID_LEN;
        }
        FecElmnt.u1VcInfoLen = u1PwVcInfoLen;

        FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PwVcInfoLen
                                          + LDP_PWVC_FEC_ELEM_GROUP_ID_LEN));

        /* Updating for the mandatory 'Fec Tlv' length */
        u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                           LDP_PWVC_FEC_ELEM_GROUP_ID_LEN + u1PwVcInfoLen);
        u1FecElmntType = LDP_FEC_PWVC_TYPE;
    }

    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        if (((pFecInfo->u1AgiLen != LDP_ZERO) ||
             (pFecInfo->u1SaiiLen != LDP_ZERO) ||
             (pFecInfo->u1TaiiLen != LDP_ZERO)) &&
            (gi4MplsSimulateFailure != LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC))
        {
            /* not a Wild Card Label Release */
            u1PwVcInfoLen = (UINT1) (pFecInfo->u1AgiLen
                                     + pFecInfo->u1SaiiLen +
                                     pFecInfo->u1TaiiLen +
                                     (3 * LDP_GEN_PWVC_FEC_SUB_HDR_LEN));
        }
        if (u1PwVcInfoLen != LDP_ZERO)
        {
            FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN +
                                              u1PwVcInfoLen));

            /* Updating for the mandatory 'PW Gen Fec Tlv' length */
            u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                               u1PwVcInfoLen);
        }
        else
        {
            FecTlvHdr.u2TlvLen = OSIX_HTONS (LDP_FEC_ELMNT_HDRLEN);

            /* Updating for the mandatory 'PW Gen Fec Tlv' length */
            u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN);
        }

        /* Updating for the Optional 'PW Grouping ID Tlv' length */
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);

        u1FecElmntType = LDP_FEC_GEN_PWVC_TYPE;
    }

    if (pFecInfo->u4Label != LDP_INVALID_LABEL)
    {
        /* Generic Label */
        GenLabelTlv.u4GenLbl = OSIX_HTONL (pFecInfo->u4Label);
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_LABEL_LEN);
    }

    if ((pFecInfo->i1StatusCode == LDP_STAT_PWVC_WRONG_C_BIT) ||
        (pFecInfo->i1StatusCode == LDP_STAT_PWVC_ILLEGAL_C_BIT) ||
        (pFecInfo->i1StatusCode == LDP_STAT_PWVC_LBL_WR_NOTSUPP) ||
        (pFecInfo->i1StatusCode == LDP_STAT_PWVC_GEN_MISCONFIG_ERR) ||
        (pFecInfo->i1StatusCode == LDP_STAT_PWVC_UNASS_TAI))
    {
        /* Status Tlv */
        u2OptParamLen += u1StatusTlvLen;
        if (pFecInfo->i1StatusCode == LDP_STAT_PWVC_WRONG_C_BIT)
        {
            u4StatusCode = LDP_STAT_PWVC_WRONG_C_BIT_VAL;
        }
        if (pFecInfo->i1StatusCode == LDP_STAT_PWVC_ILLEGAL_C_BIT)
        {
            u4StatusCode = LDP_STAT_PWVC_ILLEGAL_C_BIT_VAL;
        }
        if (pFecInfo->i1StatusCode == LDP_STAT_PWVC_LBL_WR_NOTSUPP)
        {
            u4StatusCode = LDP_STAT_PWVC_LBL_WDRAW_UNSUPPORTED;
        }
        if (pFecInfo->i1StatusCode == LDP_STAT_PWVC_GEN_MISCONFIG_ERR)
        {
            u4StatusCode = LDP_STAT_PWVC_GEN_MISCONF_ERR;
        }
        if (pFecInfo->i1StatusCode == LDP_STAT_PWVC_UNASS_TAI)
        {
            u4StatusCode = LDP_STAT_PWVC_UNASSIGNED_TAI;
        }

        u4StatusCode = OSIX_HTONL (u4StatusCode);
        StatusTlv.u4StatusCode = u4StatusCode;
    }

    /* Allocate Buffer for Label Release msg */
    pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2OptParamLen + u2MandParamLen),
                                         LDP_PDU_HDR_LEN);

    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buf for Lbl Release Msg\n");
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2MandParamLen + u2OptParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLdpPeerSession));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pLdpPeerSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    u2PwVcTypeAndCBit = OSIX_HTONS (u2PwVcTypeAndCBit);
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &u2PwVcTypeAndCBit, u4Offset,
                                   LDP_UINT2_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw VcType and C Bit to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT2_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1PwVcInfoLen, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw Vc info length to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    /* contruct FEC 128 fields */
    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.u4GroupID, u4Offset,
             LDP_PWVC_FEC_ELEM_GROUP_ID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Pw Vc Group ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_PWVC_FEC_ELEM_GROUP_ID_LEN;
        if ((FecElmnt.u4PWID != LDP_ZERO) &&
            (gi4MplsSimulateFailure != LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC))
        {
            /* not a Wild Card Withdraw  so add PWID */
            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &FecElmnt.u4PWID, u4Offset,
                 LDP_PWVC_FEC_ELEM_PWVCID_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Pw Vc ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_PWVC_FEC_ELEM_PWVCID_LEN;
        }
    }
    /* Construct FEC 129 fields */
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        if (u1PwVcInfoLen != LDP_ZERO)
        {
            u1AgiType = pFecInfo->u1AgiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AgiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying AGI Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain
                (pMsg, &pFecInfo->u1AgiLen, u4Offset,
                 LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying AGI Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1AgiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain (pMsg, pFecInfo->au1Agi, u4Offset,
                                               pFecInfo->u1AgiLen) ==
                    CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying AGI Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1AgiLen;
            }

            u1AiiType = pFecInfo->u1SaiiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Source AII Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1SaiiLen, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying SAII Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1SaiiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg, pFecInfo->au1Saii, u4Offset,
                     pFecInfo->u1SaiiLen) == CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying SAII Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1SaiiLen;
            }
            u1AiiType = pFecInfo->u1TaiiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Target AII Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1TaiiLen, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying TAII Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1TaiiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg, pFecInfo->au1Taii, u4Offset,
                     pFecInfo->u1TaiiLen) == CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying TAII Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1TaiiLen;
            }
        }
    }

    /* Construct Optional Label TLV */
    if (pFecInfo->u4Label != LDP_INVALID_LABEL)
    {
        /* Copy the generic label */
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &GenLabelTlv,
                                       u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_LABEL_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Generic Lbl to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_TLV_HDR_LEN + LDP_LABEL_LEN;
    }

    /* Construct Optional Status TLV */
    if ((pFecInfo->i1StatusCode == LDP_STAT_PWVC_WRONG_C_BIT) ||
        (pFecInfo->i1StatusCode == LDP_STAT_PWVC_ILLEGAL_C_BIT) ||
        (pFecInfo->i1StatusCode == LDP_STAT_PWVC_LBL_WR_NOTSUPP) ||
        (pFecInfo->i1StatusCode == LDP_STAT_PWVC_GEN_MISCONFIG_ERR) ||
        (pFecInfo->i1StatusCode == LDP_STAT_PWVC_UNASS_TAI))
    {
        /* Status Tlv */
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &StatusTlv, u4Offset,
                                       u1StatusTlvLen) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "NOTIF: Error while copying Status Tlv to buffer\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += u1StatusTlvLen;
    }

    /* Construct optional PW Grouping ID TLV */
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        PwGroupingId.u4GrpId = OSIX_HTONL (pFecInfo->u4GroupID);

        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &PwGroupingId, u4Offset,
             (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN)) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying Pw Grouping ID Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLdpPeerSession);
    LDP_DBG6 (LDP_ADVT_MISC,
              "ADVT: Sending Lbl Release Msg To   : %#x:%x:%x:%x:%x:%x \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2],
              PeerLdpId[3], PeerLdpId[4], PeerLdpId[5]);

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLdpPeerSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLdpPeerSession->pLdpPeer->pLdpEntity), LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Lbl Release Msg\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_ADVT_MISC, "ADVT: Successfuly sent Lbl Rel Msg\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendPwVcNotifMsg                                       */
/* Description   : This function sends the Notification Msg with PwVc FEC    */
/*                 TLV or Gen FEC TLV to the peer identified by session      */
/*                 pLdpPeerSession.                             */
/* Input(s)      : pLdpPeerSession - Pointer to the session to which the     */
/*                                   message  is to be sent                  */
/*                 pFecInfo        - Pointer to PwVc Fec Information         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpSendPwVcNotifMsg (tLdpSession * pLdpPeerSession,
                     tPwVcNotifEvtInfo * pFecInfo)
{

    tLdpMsgHdrAndId     MsgHdrAndId = { OSIX_HTONS (LDP_NOTIF_MSG), 0, 0 };
    tPWFec              FecElmnt;
    UINT1               u1AgiType = LDP_GEN_PWVC_AGI_TYPE;
    UINT1               u1AiiType = LDP_GEN_PWVC_AII_TYPE;
    tPwGroupingIdTlv    PwGroupingId = { {OSIX_HTONS (LDP_PW_GROUPING_ID_TLV),
                                          OSIX_HTONS (LDP_PW_GROUPING_ID_LEN)},
    0
    };
    tPwStatusTlv        PwStatusTlv = {
        {OSIX_HTONS (LDP_PW_STATUS_TLV),
         OSIX_HTONS (LDP_PW_STATUS_CODE_LEN)}, 0
    };                            /* set the U bit to ONE */
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tStatusTlv          StatusTlv = { {OSIX_HTONS (LDP_STATUS_TLV),
                                       OSIX_HTONS ((LDP_STATUS_CODE_LEN +
                                                    LDP_MSG_ID_LEN +
                                                    LDP_MSG_TYPE_LEN))},
    OSIX_HTONL (LDP_STAT_PW_STATUS), 0, 0, 0
    };

    UINT1               u1StatusTlvLen = 0;
    UINT1               u1PwStatusTlvLen = 0;
    UINT1               u1FecElmntType = 0;
    UINT1               u1PwVcInfoLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT2               u2OptParamLen = 0;
    UINT2               u2PwVcTypeAndCBit;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    UINT4               u4Offset = 0;
    UINT4               u4StatusCode = 0;
    tLdpId              PeerLdpId;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    MEMSET (&ConnId, LDP_ZERO, sizeof (tGenAddr));
    MEMSET (&LdpIfAddrZero, LDP_ZERO, sizeof (tGenU4Addr));
    u2PwVcTypeAndCBit = pFecInfo->u1VcType;
    LDP_SET_PWVC_C_BIT (u2PwVcTypeAndCBit, pFecInfo->i1ControlWord);

    u1StatusTlvLen = (LDP_TLV_HDR_LEN + LDP_STATUS_CODE_LEN + LDP_MSG_ID_LEN +
                      LDP_MSG_TYPE_LEN);
    u1PwStatusTlvLen = (LDP_TLV_HDR_LEN + LDP_PW_STATUS_CODE_LEN);

    /* Updating for the mandatory 'Message ID' length */
    u2MandParamLen += LDP_MSG_ID_LEN;

    /* Updating for the mandatory 'Status Tlv' length */
    u2MandParamLen += u1StatusTlvLen;

    /* Updating for the mandatory 'PW Status Tlv' length */
    u2MandParamLen += u1PwStatusTlvLen;

    u4StatusCode = (UINT4) (pFecInfo->u1PwStatusCode);

    PwStatusTlv.u4PwStatusCode = OSIX_HTONL (u4StatusCode);

    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        FecElmnt.u4GroupID = OSIX_HTONL (pFecInfo->u4GroupID);
        FecElmnt.u4PWID = OSIX_HTONL (pFecInfo->u4VcID);

        FecElmnt.u2PwType = pFecInfo->u1VcType;
        FecElmnt.u1CBit = pFecInfo->i1ControlWord;

        if ((FecElmnt.u4PWID != LDP_ZERO) &&
            (gi4MplsSimulateFailure != LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC))
        {
            /* not a Wild Card Status Notification */
            u1PwVcInfoLen = LDP_PWVC_FEC_ELEM_PWVCID_LEN;
        }
        FecElmnt.u1VcInfoLen = u1PwVcInfoLen;

        FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PwVcInfoLen
                                          + LDP_PWVC_FEC_ELEM_GROUP_ID_LEN));

        /* Updating for the mandatory 'Fec Tlv' length */
        u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                           LDP_PWVC_FEC_ELEM_GROUP_ID_LEN + u1PwVcInfoLen);
        u1FecElmntType = LDP_FEC_PWVC_TYPE;
    }

    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        if (((pFecInfo->u1AgiLen != LDP_ZERO) ||
             (pFecInfo->u1SaiiLen != LDP_ZERO) ||
             (pFecInfo->u1TaiiLen != LDP_ZERO)) &&
            (gi4MplsSimulateFailure != LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC))
        {
            /* not a Wild Card Status Notification */
            u1PwVcInfoLen = (UINT1) (pFecInfo->u1AgiLen
                                     + pFecInfo->u1SaiiLen +
                                     pFecInfo->u1TaiiLen +
                                     (3 * LDP_GEN_PWVC_FEC_SUB_HDR_LEN));
        }

        if (u1PwVcInfoLen != LDP_ZERO)
        {
            FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN +
                                              u1PwVcInfoLen));

            /* Updating for the mandatory 'PW Gen Fec Tlv' length */
            u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                               u1PwVcInfoLen);
        }
        else
        {
            FecTlvHdr.u2TlvLen = OSIX_HTONS (LDP_FEC_ELMNT_HDRLEN);

            /* Updating for the mandatory 'PW Gen Fec Tlv' length */
            u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN);
        }

        /* Updating for the mandatory 'PW Grouping ID Tlv' length */
        u2MandParamLen += (UINT2) (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);

        u1FecElmntType = LDP_FEC_GEN_PWVC_TYPE;
    }

    /* Allocate Buffer for Notification Msg */
    pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2OptParamLen + u2MandParamLen),
                                         LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buf for Notification Msg\n");
        return LDP_FAILURE;
    }

    /* Construct Message Hdr and ID */
    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2MandParamLen + u2OptParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLdpPeerSession));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pLdpPeerSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct Status Tlv */
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &StatusTlv.StatusTlvHdr,
                                   u4Offset, LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "NOTIF: Error while copying Status Tlv Hdr to buffer\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &StatusTlv.u4StatusCode,
                                   u4Offset, LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "NOTIF: Error while copying Status Tlv Code to buffer\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &StatusTlv.u4MsgId,
                                   u4Offset, LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "NOTIF: Error while copying Status Tlv Msg ID to buffer\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &StatusTlv.u2MsgType,
                                   u4Offset, LDP_UINT2_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "NOTIF: Error while copying Status Tlv Msg Type to buffer\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT2_LEN;

    /* Construct PW Status Tlv */
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &PwStatusTlv, u4Offset,
                                   u1PwStatusTlvLen) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "NOTIF: Error while copying PW Status Tlv to buffer\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += u1PwStatusTlvLen;

    /* Construct FEC Tlv */
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    /* Construct FEC Element Type */
    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    /* Construct PW Type and C Bit */
    u2PwVcTypeAndCBit = OSIX_HTONS (u2PwVcTypeAndCBit);
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &u2PwVcTypeAndCBit, u4Offset,
                                   LDP_UINT2_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw VcType and C Bit to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT2_LEN;

    /* Construct PW Info Length */
    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1PwVcInfoLen, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw Vc info length to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    /* Construct FEC 128 fields */
    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.u4GroupID, u4Offset,
             LDP_PWVC_FEC_ELEM_GROUP_ID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Pw Vc Group ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_PWVC_FEC_ELEM_GROUP_ID_LEN;

        if ((FecElmnt.u4PWID != LDP_ZERO) &&
            (gi4MplsSimulateFailure != LDP_L2VPN_SIM_FAILURE_WILDCARD_FEC))
        {
            /* not a Wild Card Withdraw so add PWID */
            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &FecElmnt.u4PWID, u4Offset,
                 LDP_PWVC_FEC_ELEM_PWVCID_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Pw Vc ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_PWVC_FEC_ELEM_PWVCID_LEN;
        }
    }

    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        /* Construct FEC 129 fields */
        if (u1PwVcInfoLen != LDP_ZERO)
        {
            u1AgiType = pFecInfo->u1AgiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AgiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying AGI Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain
                (pMsg, &pFecInfo->u1AgiLen, u4Offset,
                 LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying AGI Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1AgiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain (pMsg, pFecInfo->au1Agi, u4Offset,
                                               pFecInfo->u1AgiLen) ==
                    CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying AGI Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1AgiLen;
            }

            u1AiiType = pFecInfo->u1SaiiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Source AII Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1SaiiLen, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying SAII Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1SaiiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg, pFecInfo->au1Saii, u4Offset,
                     pFecInfo->u1SaiiLen) == CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying SAII Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1SaiiLen;
            }

            u1AiiType = pFecInfo->u1TaiiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Target AII Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1TaiiLen, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying TAII Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1TaiiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg, pFecInfo->au1Taii, u4Offset,
                     pFecInfo->u1TaiiLen) == CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying TAII Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1TaiiLen;
            }
        }

        /* Construct PW Grouping ID TLV */
        PwGroupingId.u4GrpId = OSIX_HTONL (pFecInfo->u4GroupID);

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &PwGroupingId, u4Offset,
                                       (LDP_TLV_HDR_LEN
                                        + LDP_PW_GROUPING_ID_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying Pw Grouping ID Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLdpPeerSession);
    LDP_DBG6 (LDP_ADVT_MISC,
              "ADVT: Sending Notification Msg To   : %#x:%x:%x:%x:%x:%x \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2],
              PeerLdpId[3], PeerLdpId[4], PeerLdpId[5]);

    MEMSET (&ConnId, LDP_ZERO, sizeof( tGenAddr));
    MEMSET (&LdpIfAddrZero, LDP_ZERO, sizeof (tGenU4Addr));
    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLdpPeerSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLdpPeerSession->pLdpPeer->pLdpEntity), LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Notification Msg\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_ADVT_MISC, "ADVT: Successfuly sent Notification Msg\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendPwVcAddrWdrawMsg                                   */
/* Description   : This function sends the Address Withdraw Msg with PwVc    */
/*                 FEC TLV to the peer identified by session pLdpPeerSession.*/
/* Input(s)      : pLdpPeerSession - Pointer to the session to which the     */
/*                                   message  is to be sent                  */
/*                 pFecInfo        - Pointer to PwVc Fec Information         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpSendPwVcAddrWdrawMsg (tLdpSession * pLdpPeerSession,
                         tPwVcNotifEvtInfo * pFecInfo)
{

    tLdpMsgHdrAndId     MsgHdrAndId =
        { OSIX_HTONS (LDP_ADDR_WITHDRAW_MSG), 0, 0 };
    tPWFec              FecElmnt;
    UINT1               u1AgiType = LDP_GEN_PWVC_AGI_TYPE;
    UINT1               u1AiiType = LDP_GEN_PWVC_AII_TYPE;
    tPwGroupingIdTlv    PwGroupingId = { {OSIX_HTONS (LDP_PW_GROUPING_ID_TLV),
                                          OSIX_HTONS (LDP_PW_GROUPING_ID_LEN)},
    0
    };
    tTlvHdr             FecTlvHdr = { OSIX_HTONS (LDP_FEC_TLV), 0 };
    tTlvHdr             MacListTlvHdr = {
        OSIX_HTONS ((LDP_PWVC_SET_U_BIT_MASK | LDP_MAC_LIST_TLV)), 0
    };

    UINT1               u1FecElmntType = 0;
    UINT1               u1PwVcInfoLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT2               u2OptParamLen = 0;
    UINT2               u2PwVcTypeAndCBit;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    UINT4               u4Offset = 0;
    tLdpId              PeerLdpId;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    u2MandParamLen += LDP_MSG_ID_LEN;

    u2PwVcTypeAndCBit = pFecInfo->u1VcType;
    MEMSET (&ConnId, LDP_ZERO, sizeof(tGenAddr));
    MEMSET (&LdpIfAddrZero, LDP_ZERO,sizeof(tGenU4Addr));

    LDP_SET_PWVC_C_BIT (u2PwVcTypeAndCBit, pFecInfo->i1ControlWord);

    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        FecElmnt.u4GroupID = OSIX_HTONL (pFecInfo->u4GroupID);
        FecElmnt.u4PWID = OSIX_HTONL (pFecInfo->u4VcID);

        FecElmnt.u2PwType = pFecInfo->u1VcType;
        FecElmnt.u1CBit = pFecInfo->i1ControlWord;

        if (FecElmnt.u4PWID != LDP_ZERO)
        {
            /* not a Wild Card Address Withdraw */
            u1PwVcInfoLen = LDP_PWVC_FEC_ELEM_PWVCID_LEN;
        }
        FecElmnt.u1VcInfoLen = u1PwVcInfoLen;

        FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN + u1PwVcInfoLen
                                          + LDP_PWVC_FEC_ELEM_GROUP_ID_LEN));

        /* Updating for the mandatory 'Fec Tlv' length */
        u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                           LDP_PWVC_FEC_ELEM_GROUP_ID_LEN + u1PwVcInfoLen);

        u1FecElmntType = LDP_FEC_PWVC_TYPE;
    }
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        if ((pFecInfo->u1AgiLen != LDP_ZERO) ||
            (pFecInfo->u1SaiiLen != LDP_ZERO) ||
            (pFecInfo->u1TaiiLen != LDP_ZERO))
        {
            /* not a Wild Card Address Withdraw */
            u1PwVcInfoLen = (UINT1) (pFecInfo->u1AgiLen
                                     + pFecInfo->u1SaiiLen +
                                     pFecInfo->u1TaiiLen +
                                     (3 * LDP_GEN_PWVC_FEC_SUB_HDR_LEN));
        }
        if (u1PwVcInfoLen != LDP_ZERO)
        {
            FecTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_FEC_ELMNT_HDRLEN +
                                              u1PwVcInfoLen));

            /* Updating for the mandatory 'PW Gen Fec Tlv' length */
            u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN +
                               u1PwVcInfoLen);
        }
        else
        {
            FecTlvHdr.u2TlvLen = OSIX_HTONS (LDP_FEC_ELMNT_HDRLEN);

            /* Updating for the mandatory 'PW Gen Fec Tlv' length */
            u2MandParamLen += (LDP_TLV_HDR_LEN + LDP_FEC_ELMNT_HDRLEN);
        }

        /* Updating for the 'PW Grouping ID Tlv' length */
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);

        u1FecElmntType = LDP_FEC_GEN_PWVC_TYPE;
    }

    /* Updating for the mandatory 'Mac List Tlv' length */
    /* Empty Mac List */
    u2MandParamLen += (LDP_TLV_HDR_LEN);

    /* Allocate Buffer for Address Withdraw Message */
    pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2OptParamLen + u2MandParamLen),
                                         LDP_PDU_HDR_LEN);

    if (pMsg == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Buf for Addr Wthdraw Msg\n");
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2MandParamLen + u2OptParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLdpPeerSession));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pLdpPeerSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FecTlvHdr, u4Offset,
                                   LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Error copying FecTlvHdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1FecElmntType, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying FecElement Type to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    u2PwVcTypeAndCBit = OSIX_HTONS (u2PwVcTypeAndCBit);
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &u2PwVcTypeAndCBit, u4Offset,
                                   LDP_UINT2_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw VcType and C Bit to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT2_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, &u1PwVcInfoLen, u4Offset,
                                   LDP_UINT1_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Pw Vc info length to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_UINT1_LEN;

    /* contruct FEC 128 fields */
    if (pFecInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &FecElmnt.u4GroupID, u4Offset,
             LDP_PWVC_FEC_ELEM_GROUP_ID_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Error copying Pw Vc Group ID to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_PWVC_FEC_ELEM_GROUP_ID_LEN;
        if (FecElmnt.u4PWID != LDP_ZERO)
        {
            /* not a Wild Card Withdraw  so add PWID */
            if (CRU_BUF_Copy_OverBufChain
                (pMsg, (UINT1 *) &FecElmnt.u4PWID, u4Offset,
                 LDP_PWVC_FEC_ELEM_PWVCID_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Pw Vc ID to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_PWVC_FEC_ELEM_PWVCID_LEN;
        }
    }

    /* Construct FEC 129 fields */
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        if (u1PwVcInfoLen != LDP_ZERO)
        {
            u1AgiType = pFecInfo->u1AgiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AgiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying AGI Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain
                (pMsg, &pFecInfo->u1AgiLen, u4Offset,
                 LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying AGI Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1AgiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain (pMsg, pFecInfo->au1Agi, u4Offset,
                                               pFecInfo->u1AgiLen) ==
                    CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying AGI Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1AgiLen;
            }

            u1AiiType = pFecInfo->u1SaiiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Source AII Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1SaiiLen, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying SAII Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1SaiiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg, pFecInfo->au1Saii, u4Offset,
                     pFecInfo->u1SaiiLen) == CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying SAII Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1SaiiLen;
            }
            u1AiiType = pFecInfo->u1TaiiType;
            if (CRU_BUF_Copy_OverBufChain (pMsg, &u1AiiType, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying Target AII Type to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (CRU_BUF_Copy_OverBufChain (pMsg, &pFecInfo->u1TaiiLen, u4Offset,
                                           LDP_UINT1_LEN) == CRU_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Error copying TAII Length to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += LDP_UINT1_LEN;

            if (pFecInfo->u1TaiiLen > LDP_ZERO)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg, pFecInfo->au1Taii, u4Offset,
                     pFecInfo->u1TaiiLen) == CRU_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Error copying TAII Value to Buf chain\n");
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                u4Offset += pFecInfo->u1TaiiLen;
            }
        }
    }

    /* Construct Empty Mac List Tlv */
    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MacListTlvHdr,
                                   u4Offset, LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Error copying Empty Mac List Tlv to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += LDP_TLV_HDR_LEN;

    /* Construct optional PW Grouping ID TLV */
    if (pFecInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        PwGroupingId.u4GrpId = OSIX_HTONL (pFecInfo->u4GroupID);

        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &PwGroupingId, u4Offset,
             (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN)) == CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying Pw Grouping ID Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_PW_GROUPING_ID_LEN);
    }
    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pLdpPeerSession);
    LDP_DBG6 (LDP_ADVT_MISC,
              "ADVT: Sending Address Withdraw Msg To   : %#x:%x:%x:%x:%x:%x \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2],
              PeerLdpId[3], PeerLdpId[4], PeerLdpId[5]);

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLdpPeerSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLdpPeerSession->pLdpPeer->pLdpEntity), LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        LDP_DBG (LDP_ADVT_TX, "ADVT: Failed to send Address Withdraw Msg\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_ADVT_MISC, "ADVT: Successfuly sent Address Withdraw Msg\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpL2VpnPostEvent                                         */
/* Description   : Send data and event info to L2VPN  Application            */
/* Input(s)      : pPwVcEvtInfo  - pointer to event and data to be send to   */
/*                                 L2VPN Application.                        */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
/* ldppwif.c  */
UINT1
LdpL2VpnPostEvent (tL2VpnLdpPwVcEvtInfo * pPwVcEvtInfo)
{
    UINT4               u4Event = 0;

    if (L2VpnLdpEventHandler (pPwVcEvtInfo, u4Event) == L2VPN_FAILURE)
    {
        return (LDP_FAILURE);
    }
    return (LDP_SUCCESS);
}
