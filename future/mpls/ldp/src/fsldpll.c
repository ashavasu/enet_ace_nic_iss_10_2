
/********************************************************************
 *  Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * 
 *  $Id: fsldpll.c,v 1.35 2017/06/16 13:35:37 siva Exp $
 * 
 * ********************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : fsldpll.c 
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP (LDP - Low Level Routines)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains Low Level Get routines
 *                             for the LDP Module.
 *----------------------------------------------------------------------------*/
#include "ldpincs.h"
#include "ldplwinc.h"
#include "stdldplw.h"
#include "rtm.h"
#include "rtm6.h"
#include "mplscli.h"

static UINT4        gu4LsrIpAddr = 0x0;
/****************************************************************************
 Function    :  nmhGetFsMplsLsrLabelAllocationMethod
 Input       :  The Indices

                The Object 
                retValFsMplsLsrLabelAllocationMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLsrLabelAllocationMethod (INT4
                                      *pi4RetValFsMplsLsrLabelAllocationMethod)
{
    *pi4RetValFsMplsLsrLabelAllocationMethod
        = (INT4) LDP_LABEL_ALLOC_METHOD (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLsrLabelAllocationMethod
 Input       :  The Indices

                The Object 
                testValFsMplsLsrLabelAllocationMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLsrLabelAllocationMethod (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsMplsLsrLabelAllocationMethod)
{
    if ((i4TestValFsMplsLsrLabelAllocationMethod == LDP_ORDERED_MODE) ||
        (i4TestValFsMplsLsrLabelAllocationMethod == LDP_INDEPENDENT_MODE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLsrLabelAllocationMethod
 Input       :  The Indices
                FsMplsLdpLsrIncarnId

                The Object 
                setValFsMplsLsrLabelAllocationMethod
 Output      :  The Set Low Lev Routine Take the Indices &
               Sets the Value accordingly. 
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLsrLabelAllocationMethod (INT4
                                      i4SetValFsMplsLsrLabelAllocationMethod)
{
    LDP_LABEL_ALLOC_METHOD (MPLS_DEF_INCARN) =
        (UINT1) i4SetValFsMplsLsrLabelAllocationMethod;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLsrMaxLdpEntities
 Input       :  The Indices

                The Object 
                retValFsMplsLsrMaxLdpEntities
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLsrMaxLdpEntities (INT4 *pi4RetValFsMplsLsrMaxLdpEntities)
{
    *pi4RetValFsMplsLsrMaxLdpEntities =
        (INT4) LDP_MAX_ENTITIES (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLsrMaxLocalPeers
 Input       :  The Indices

                The Object 
                retValFsMplsLsrMaxLocalPeers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLsrMaxLocalPeers (INT4 *pi4RetValFsMplsLsrMaxLocalPeers)
{
    *pi4RetValFsMplsLsrMaxLocalPeers =
        (INT4) LDP_MAX_LOCAL_PEERS (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLsrMaxRemotePeers
 Input       :  The Indices

                The Object 
                retValFsMplsLsrMaxRemotePeers
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLsrMaxRemotePeers (INT4 *pi4RetValFsMplsLsrMaxRemotePeers)
{
    *pi4RetValFsMplsLsrMaxRemotePeers =
        (INT4) LDP_MAX_REMOTE_PEERS (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLsrMaxIfaces
 Input       :  The Indices

                The Object 
                retValFsMplsLsrMaxIfaces
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLsrMaxIfaces (INT4 *pi4RetValFsMplsLsrMaxIfaces)
{
    *pi4RetValFsMplsLsrMaxIfaces = (INT4) LDP_MAX_INTERFACES (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLsrMaxLsps
 Input       :  The Indices

                The Object 
                retValFsMplsLsrMaxLsps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLsrMaxLsps (INT4 *pi4RetValFsMplsLsrMaxLsps)
{
    *pi4RetValFsMplsLsrMaxLsps = (INT4) LDP_MAX_LSPS (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLsrMaxVcMergeCount
 Input       :  The Indices

                The Object 
                retValFsMplsLsrMaxVcMergeCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLsrMaxVcMergeCount (INT4 *pi4RetValFsMplsLsrMaxVcMergeCount)
{
    *pi4RetValFsMplsLsrMaxVcMergeCount =
        (INT4) LDP_MAX_ATM_VC_MRG_COUNT (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLsrMaxVpMergeCount
 Input       :  The Indices

                The Object 
                retValFsMplsLsrMaxVpMergeCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLsrMaxVpMergeCount (INT4 *pi4RetValFsMplsLsrMaxVpMergeCount)
{
    *pi4RetValFsMplsLsrMaxVpMergeCount =
        (INT4) LDP_MAX_ATM_VP_MRG_COUNT (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMplsLsrMaxCrlspTnls
 Input       :  The Indices

                The Object 
                retValFsMplsLsrMaxCrlspTnls
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLsrMaxCrlspTnls (INT4 *pi4RetValFsMplsLsrMaxCrlspTnls)
{
    *pi4RetValFsMplsLsrMaxCrlspTnls =
        (INT4) LDP_MAX_CRLSP_TNLS (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsCrlspDebugLevel
 Input       :  The Indices

                The Object 
                retValFsMplsCrlspDebugLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsCrlspDebugLevel (INT4 *pi4RetValFsMplsCrlspDebugLevel)
{
    *pi4RetValFsMplsCrlspDebugLevel = (INT4) gu4LdpDbg;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsCrlspDebugLevel
 Input       :  The Indices

                The Object 
                testValFsMplsCrlspDebugLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsCrlspDebugLevel (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsMplsCrlspDebugLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMplsCrlspDebugLevel);
    /* i4TestValFsMplsLdpCrlspDisplayDebugs is a 4byte value */

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsCrlspDebugLevel
 Input       :  The Indices

                The Object 
                setValFsMplsCrlspDebugLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsCrlspDebugLevel (INT4 i4SetValFsMplsCrlspDebugLevel)
{
    gu4LdpDbg = i4SetValFsMplsCrlspDebugLevel;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsCrlspDumpType
 Input       :  The Indices

                The Object
                retValFsMplsCrlspDumpType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsCrlspDumpType (INT4 *pi4RetValFsMplsCrlspDumpType)
{
    *pi4RetValFsMplsCrlspDumpType = (INT4) gu4LdpDumpType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsCrlspDumpType
 Input       :  The Indices

                The Object 
                testValFsMplsCrlspDumpType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsCrlspDumpType (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsMplsCrlspDumpType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMplsCrlspDumpType);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsCrlspDumpType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsCrlspDumpType (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsCrlspDumpType
 Input       :  The Indices

                The Object 
                setValFsMplsCrlspDumpType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsCrlspDumpType (INT4 i4SetValFsMplsCrlspDumpType)
{
    if ((UINT4) i4SetValFsMplsCrlspDumpType > LDP_DUMP_ALL)
    {
        i4SetValFsMplsCrlspDumpType = LDP_DUMP_ALL;
    }
    gu4LdpDumpType = i4SetValFsMplsCrlspDumpType;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsCrlspTnlTable
 Input       :  The Indices
                FsMplsCrlspTnlIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMplsCrlspTnlTable (UINT4 u4FsMplsCrlspTnlIndex)
{
    /* Validate the Tunnel Index and the Tunnel Instance */
    if ((u4FsMplsCrlspTnlIndex > 0) && (u4FsMplsCrlspTnlIndex < LDP_UINT2_SIZE))
    {
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsCrlspTnlTable
 Input       :  The Indices
                FsMplsCrlspTnlIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexFsMplsCrlspTnlTable (UINT4 *pu4FsMplsCrlspTnlIndex)
{
    UINT4               u4IncarnId;
    UINT1               u1FirstLoopEntry = LDP_TRUE;
    tTMO_SLL           *pList = NULL;
    tTnlIncarnNode     *pTnlIncarnNode = NULL;

    *pu4FsMplsCrlspTnlIndex = LDP_ZERO;

    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }
    for (u4IncarnId = 0; u4IncarnId < LDP_MAX_INCARN; ++u4IncarnId)
    {
        pList = &LDP_TNL_INCARN_LIST (u4IncarnId);
        pTnlIncarnNode = (tTnlIncarnNode *) TMO_SLL_First (pList);
        if (pTnlIncarnNode == NULL)
        {
            continue;
        }

        /* Loop to get the minimum Tunnel Index. */

        TMO_SLL_Scan (pList, pTnlIncarnNode, tTnlIncarnNode *)
        {
            if (u1FirstLoopEntry == LDP_TRUE)
            {
                *pu4FsMplsCrlspTnlIndex =
                    (INT4) (LDP_TNL_INCARN_TNL_INDEX (pTnlIncarnNode));
                u1FirstLoopEntry = LDP_FALSE;
                continue;
            }
            if (LDP_TNL_INCARN_TNL_INDEX (pTnlIncarnNode)
                < (UINT4) (*pu4FsMplsCrlspTnlIndex))
            {
                *pu4FsMplsCrlspTnlIndex =
                    (INT4) (LDP_TNL_INCARN_TNL_INDEX (pTnlIncarnNode));
            }
        }

        if (u1FirstLoopEntry == LDP_FALSE)
        {
            return SNMP_SUCCESS;
        }
    }                            /* End of FOR loop */
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsCrlspTnlTable
 Input       :  The Indices
                FsMplsCrlspTnlIndex
                nextFsMplsCrlspTnlIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsCrlspTnlTable (UINT4 u4FsMplsCrlspTnlIndex,
                                    UINT4 *pu4NextFsMplsCrlspTnlIndex)
{
    UINT1               u1FirstLoopEntry = LDP_TRUE;
    tTMO_SLL           *pList = NULL;
    tTnlIncarnNode     *pTnlIncarnNode = NULL;

    *pu4NextFsMplsCrlspTnlIndex = LDP_ZERO;

    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    pList = &LDP_TNL_INCARN_LIST (MPLS_DEF_INCARN);

    /* loop for finding the next tunnel Index in the Incarnation. */
    TMO_SLL_Scan (pList, pTnlIncarnNode, tTnlIncarnNode *)
    {
        if (LDP_TNL_INCARN_TNL_INDEX (pTnlIncarnNode)
            > (UINT4) u4FsMplsCrlspTnlIndex)
        {
            if (u1FirstLoopEntry == LDP_TRUE)
            {
                *pu4NextFsMplsCrlspTnlIndex =
                    (INT4) LDP_TNL_INCARN_TNL_INDEX (pTnlIncarnNode);
                u1FirstLoopEntry = LDP_FALSE;
                continue;
            }
            if (LDP_TNL_INCARN_TNL_INDEX (pTnlIncarnNode)
                < (UINT4) (*pu4NextFsMplsCrlspTnlIndex))
            {
                *pu4NextFsMplsCrlspTnlIndex =
                    (INT4) LDP_TNL_INCARN_TNL_INDEX (pTnlIncarnNode);
            }
        }
    }

    if (u1FirstLoopEntry == LDP_FALSE)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsCrlspTnlRowStatus
 Input       :  The Indices
                FsMplsCrlspTnlIndex

                The Object 
                retValFsMplsCrlspTnlRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsCrlspTnlRowStatus (UINT4 u4FsMplsCrlspTnlIndex,
                               INT4 *pi4RetValFsMplsCrlspTnlRowStatus)
{
    tTnlIncarnNode     *pTnlIncarnNode = NULL;

    if (LdpGetTnlIncarnNode ((UINT2) (MPLS_DEF_INCARN),
                             (UINT4) u4FsMplsCrlspTnlIndex,
                             &pTnlIncarnNode) == LDP_SUCCESS)
    {
        *pi4RetValFsMplsCrlspTnlRowStatus =
            (INT4) (LDP_TNL_INCARN_ROW_STATUS (pTnlIncarnNode));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsCrlspTnlRowStatus
 Input       :  The Indices
                FsMplsCrlspTnlIndex

                The Object 
                testValFsMplsCrlspTnlRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsCrlspTnlRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMplsCrlspTnlIndex,
                                  INT4 i4TestValFsMplsCrlspTnlRowStatus)
{
    /* Added to suppress warning */
    UNUSED_PARAM (u4FsMplsCrlspTnlIndex);

    if (LDP_INCARN_ADMIN_STATUS (MPLS_DEF_INCARN) == LDP_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsCrlspTnlRowStatus)
    {
        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsCrlspTnlRowStatus
 Input       :  The Indices
                FsMplsCrlspTnlIndex

                The Object 
                setValFsMplsCrlspTnlRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsCrlspTnlRowStatus (UINT4 u4FsMplsCrlspTnlIndex,
                               INT4 i4SetValFsMplsCrlspTnlRowStatus)
{
    tTnlIncarnNode     *pTnlIncarnNode = NULL;

    switch (i4SetValFsMplsCrlspTnlRowStatus)
    {
        case CREATE_AND_WAIT:
        case CREATE_AND_GO:

            if (LdpGetTnlIncarnNode ((UINT2) (MPLS_DEF_INCARN),
                                     (UINT4) u4FsMplsCrlspTnlIndex,
                                     &pTnlIncarnNode) == LDP_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            pTnlIncarnNode = (tTnlIncarnNode *)
                MemAllocMemBlk (LDP_TNL_INCARN_POOL_ID);

            if (pTnlIncarnNode == NULL)
            {
                LDP_DBG (LDP_MAIN_MEM, "MAIN: MemAlloc Failed for TnlIncarn\n");
                return SNMP_FAILURE;
            }
            TMO_SLL_Init_Node (&(pTnlIncarnNode->NextTnlIndex));
            LDP_TNL_INCARN_TNL_INDEX (pTnlIncarnNode) =
                (UINT4) u4FsMplsCrlspTnlIndex;
            LDP_TNL_INCARN_ROW_STATUS (pTnlIncarnNode) = NOT_READY;
            TMO_SLL_Add (&LDP_TNL_INCARN_LIST (MPLS_DEF_INCARN),
                         &(pTnlIncarnNode->NextTnlIndex));
            return SNMP_SUCCESS;

        case ACTIVE:

            if (LdpGetTnlIncarnNode ((UINT2) (MPLS_DEF_INCARN),
                                     (UINT4) u4FsMplsCrlspTnlIndex,
                                     &pTnlIncarnNode) == LDP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            LDP_TNL_INCARN_ROW_STATUS (pTnlIncarnNode) =
                (UINT2) i4SetValFsMplsCrlspTnlRowStatus;
            return SNMP_SUCCESS;

        case NOT_IN_SERVICE:

            if (LdpGetTnlIncarnNode ((UINT2) (MPLS_DEF_INCARN),
                                     (UINT4) u4FsMplsCrlspTnlIndex,
                                     &pTnlIncarnNode) == LDP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            /* If the Row Status is going to be NOT_IN_SERVICE, then the 
             * present Row status must be ACTIVE. Or the Status should be in 
             * NOT_IN_SERVICE. */
            if ((LDP_TNL_INCARN_ROW_STATUS (pTnlIncarnNode) == ACTIVE) ||
                (LDP_TNL_INCARN_ROW_STATUS (pTnlIncarnNode) == NOT_IN_SERVICE))
            {
                LDP_TNL_INCARN_ROW_STATUS (pTnlIncarnNode) = NOT_IN_SERVICE;
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;

        case DESTROY:

            if (LdpGetTnlIncarnNode ((UINT2) (MPLS_DEF_INCARN),
                                     (UINT4) u4FsMplsCrlspTnlIndex,
                                     &pTnlIncarnNode) == LDP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            TMO_SLL_Delete (&LDP_TNL_INCARN_LIST (MPLS_DEF_INCARN),
                            &(pTnlIncarnNode->NextTnlIndex));
            MemReleaseMemBlock (LDP_TNL_INCARN_POOL_ID,
                                (UINT1 *) pTnlIncarnNode);
            return SNMP_SUCCESS;

        default:

            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhGetFsMplsCrlspTnlStorageType
 Input       :  The Indices
                FsMplsCrlspTnlIndex

                The Object 
                retValFsMplsCrlspTnlStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsCrlspTnlStorageType (UINT4 u4FsMplsCrlspTnlIndex,
                                 INT4 *pi4RetValFsMplsCrlspTnlStorageType)
{
    tTnlIncarnNode     *pTnlIncarnNode = NULL;

    if (LdpGetTnlIncarnNode ((UINT2) (MPLS_DEF_INCARN),
                             (UINT4) u4FsMplsCrlspTnlIndex,
                             &pTnlIncarnNode) == LDP_SUCCESS)
    {
        *pi4RetValFsMplsCrlspTnlStorageType =
            (INT4) (LDP_TNL_INCARN_STORAGE_TYPE (pTnlIncarnNode));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsCrlspTnlStorageType
 Input       :  The Indices
                FsMplsCrlspTnlIndex

                The Object 
                setValFsMplsCrlspTnlStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsCrlspTnlStorageType (UINT4 u4FsMplsCrlspTnlIndex,
                                 INT4 i4SetValFsMplsCrlspTnlStorageType)
{
    tTnlIncarnNode     *pTnlIncarnNode = NULL;

    if (LdpGetTnlIncarnNode ((UINT2) (MPLS_DEF_INCARN),
                             (UINT4) u4FsMplsCrlspTnlIndex,
                             &pTnlIncarnNode) == LDP_SUCCESS)
    {
        if (LDP_TNL_INCARN_ROW_STATUS (pTnlIncarnNode) == NOT_READY)
        {
            LDP_TNL_INCARN_STORAGE_TYPE (pTnlIncarnNode) =
                (UINT2) i4SetValFsMplsCrlspTnlStorageType;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsCrlspTnlStorageType
 Input       :  The Indices
                FsMplsCrlspTnlIndex

                The Object 
                testValFsMplsCrlspTnlStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsCrlspTnlStorageType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMplsCrlspTnlIndex,
                                    INT4 i4TestValFsMplsCrlspTnlStorageType)
{
    /* Added to suppress warning */
    UNUSED_PARAM (u4FsMplsCrlspTnlIndex);

    switch (i4TestValFsMplsCrlspTnlStorageType)
    {
        case LDP_STORAGE_VOLATILE:
        case LDP_STORAGE_NONVOLATILE:
        case LDP_STORAGE_OTHER:
        case LDP_STORAGE_PERMANENT:
        case LDP_STORAGE_READONLY:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsCrlspTnlTable
 Input       :  The Indices
                FsMplsCrlspTnlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsCrlspTnlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsCrlspDumpDirection
 Input       :  The Indices

                The Object
                retValFsMplsCrlspDumpDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsCrlspDumpDirection (INT4 *pi4RetValFsMplsCrlspDumpDirection)
{
    *pi4RetValFsMplsCrlspDumpDirection = (INT4) gu4LdpDumpDir;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsCrlspDumpDirection
 Input       :  The Indices

                The Object 
                testValFsMplsCrlspDumpDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsCrlspDumpDirection (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsMplsCrlspDumpDirection)
{

    if (i4TestValFsMplsCrlspDumpDirection == LDP_DUMP_DIR_INOUT)
    {
        return SNMP_SUCCESS;
    }
    if (i4TestValFsMplsCrlspDumpDirection < LDP_DUMP_DIR_INOUT
        && i4TestValFsMplsCrlspDumpDirection > 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsCrlspDumpDirection
 Input       :  The Indices

                The Object 
                setValFsMplsCrlspDumpDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsCrlspDumpDirection (INT4 i4SetValFsMplsCrlspDumpDirection)
{
    gu4LdpDumpDir = i4SetValFsMplsCrlspDumpDirection;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsCrlspPersistance
 Input       :  The Indices

                The Object
                retValFsMplsCrlspPersistance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsCrlspPersistance (INT4 *pi4RetValFsMplsCrlspPersistance)
{
    *pi4RetValFsMplsCrlspPersistance = (INT4) gu4LdpLspPersist;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsCrlspPersistance
 Input       :  The Indices

                The Object 
                testValFsMplsCrlspPersistance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsCrlspPersistance (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMplsCrlspPersistance)
{
    if (i4TestValFsMplsCrlspPersistance > 1
        || i4TestValFsMplsCrlspPersistance < 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsCrlspDumpDirection
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsCrlspDumpDirection (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsCrlspPersistance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsCrlspPersistance (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsCrlspPersistance
 Input       :  The Indices

                The Object 
                setValFsMplsCrlspPersistance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsCrlspPersistance (INT4 i4SetValFsMplsCrlspPersistance)
{
    gu4LdpLspPersist = i4SetValFsMplsCrlspPersistance;

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsLdpEntityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsLdpEntityTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsLdpEntityTable (tSNMP_OCTET_STRING_TYPE *
                                              pMplsLdpEntityLdpId,
                                              UINT4 u4MplsLdpEntityIndex)
{
    return (nmhValidateIndexInstanceMplsLdpEntityTable
            (pMplsLdpEntityLdpId, u4MplsLdpEntityIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsLdpEntityTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsLdpEntityTable (tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 *pu4MplsLdpEntityIndex)
{
    return (nmhGetFirstIndexMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                                pu4MplsLdpEntityIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsLdpEntityTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsLdpEntityTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     UINT4 *pu4NextMplsLdpEntityIndex)
{
    return (nmhGetNextIndexMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                               pNextMplsLdpEntityLdpId,
                                               u4MplsLdpEntityIndex,
                                               pu4NextMplsLdpEntityIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityPHPRequestMethod
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityPHPRequestMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityPHPRequestMethod (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       *pi4RetValFsMplsLdpEntityPHPRequestMethod)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMplsLdpEntityPHPRequestMethod =
        (INT4) LDP_ENTITY_PHP_CONF (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityTransAddrTlvEnable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityTransAddrTlvEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityTransAddrTlvEnable (tSNMP_OCTET_STRING_TYPE
                                         * pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         *pi4RetValFsMplsLdpEntityTransAddrTlvEnable)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValFsMplsLdpEntityTransAddrTlvEnable =
        (INT4) LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityTransportAddress
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityTransportAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityTransportAddress (tSNMP_OCTET_STRING_TYPE
                                       * pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       *pu4RetValFsMplsLdpEntityTransportAddress)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValFsMplsLdpEntityTransportAddress =
        LDP_ENTITY_TRANSPORT_ADDRESS (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityLdpOverRsvpEnable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityLdpOverRsvpEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityLdpOverRsvpEnable (tSNMP_OCTET_STRING_TYPE
                                        * pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        INT4
                                        *pi4RetValFsMplsLdpEntityLdpOverRsvpEnable)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsLdpEntityLdpOverRsvpEnable =
        LDP_ENTITY_LDP_OVER_RSVP_FLAG (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityOutTunnelIndex
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityOutTunnelIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityOutTunnelIndex (tSNMP_OCTET_STRING_TYPE
                                     * pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     UINT4
                                     *pu4RetValFsMplsLdpEntityOutTunnelIndex)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsLdpEntityOutTunnelIndex =
        pLdpEntity->OutStackTnlInfo.u4TnlId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityOutTunnelInstance
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityOutTunnelInstance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityOutTunnelInstance (tSNMP_OCTET_STRING_TYPE
                                        * pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        UINT4
                                        *pu4RetValFsMplsLdpEntityOutTunnelInstance)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsLdpEntityOutTunnelInstance =
        pLdpEntity->OutStackTnlInfo.u4TnlInstance;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityOutTunnelIngressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityOutTunnelIngressLSRId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityOutTunnelIngressLSRId (tSNMP_OCTET_STRING_TYPE
                                            * pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            UINT4
                                            *pu4RetValFsMplsLdpEntityOutTunnelIngressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsLdpEntityOutTunnelIngressLSRId =
        pLdpEntity->OutStackTnlInfo.u4IngressId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityOutTunnelEgressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityOutTunnelEgressLSRId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityOutTunnelEgressLSRId (tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4
                                           *pu4RetValFsMplsLdpEntityOutTunnelEgressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsLdpEntityOutTunnelEgressLSRId =
        pLdpEntity->OutStackTnlInfo.u4EgressId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityInTunnelIndex
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityInTunnelIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityInTunnelIndex (tSNMP_OCTET_STRING_TYPE
                                    * pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    UINT4
                                    *pu4RetValFsMplsLdpEntityInTunnelIndex)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsLdpEntityInTunnelIndex = pLdpEntity->InStackTnlInfo.u4TnlId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityInTunnelInstance
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityInTunnelInstance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityInTunnelInstance (tSNMP_OCTET_STRING_TYPE
                                       * pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       *pu4RetValFsMplsLdpEntityInTunnelInstance)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsLdpEntityInTunnelInstance =
        pLdpEntity->InStackTnlInfo.u4TnlInstance;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityInTunnelIngressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityInTunnelIngressLSRId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityInTunnelIngressLSRId (tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4
                                           *pu4RetValFsMplsLdpEntityInTunnelIngressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsLdpEntityInTunnelIngressLSRId =
        pLdpEntity->InStackTnlInfo.u4IngressId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityInTunnelEgressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityInTunnelEgressLSRId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityInTunnelEgressLSRId (tSNMP_OCTET_STRING_TYPE
                                          * pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4
                                          *pu4RetValFsMplsLdpEntityInTunnelEgressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsLdpEntityInTunnelEgressLSRId =
        pLdpEntity->InStackTnlInfo.u4EgressId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityIpv6TransportAddress
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                The Object
                retValFsMplsLdpEntityIpv6TransportAddress
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityIpv6TransportAddress (tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pRetValFsMplsLdpEntityIpv6TransportAddress)
{
#ifdef MPLS_IPV6_WANTED
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValFsMplsLdpEntityIpv6TransportAddress->pu1_OctetList,
            pLdpEntity->Ipv6TransportAddress.u1_addr, LDP_IPV6ADR_LEN);
    pRetValFsMplsLdpEntityIpv6TransportAddress->i4_Length = LDP_IPV6ADR_LEN;
#else
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pRetValFsMplsLdpEntityIpv6TransportAddress);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityIpv6TransAddrTlvEnable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object
                retValFsMplsLdpEntityIpv6TransAddrTlvEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityIpv6TransAddrTlvEnable (tSNMP_OCTET_STRING_TYPE
                                             * pMplsLdpEntityLdpId,
                                             UINT4 u4MplsLdpEntityIndex,
                                             INT4
                                             *pi4RetValFsMplsLdpEntityIpv6TransAddrTlvEnable)
{
#ifdef MPLS_IPV6_WANTED
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsLdpEntityIpv6TransAddrTlvEnable =
        pLdpEntity->u1Ipv6TransAddrTlvEnable;
#else

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pi4RetValFsMplsLdpEntityIpv6TransAddrTlvEnable);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityIpv6TransportAddrKind
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object
                retValFsMplsLdpEntityIpv6TransportAddrKind
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityIpv6TransportAddrKind (tSNMP_OCTET_STRING_TYPE
                                            * pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            INT4
                                            *pi4RetValFsMplsLdpEntityIpv6TransportAddrKind)
{
#ifdef MPLS_IPV6_WANTED
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsLdpEntityIpv6TransportAddrKind =
        pLdpEntity->u1Ipv6TransportAddrKind;
#else
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pi4RetValFsMplsLdpEntityIpv6TransportAddrKind);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpEntityBfdStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValFsMplsLdpEntityBfdStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpEntityBfdStatus (tSNMP_OCTET_STRING_TYPE
                                * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 *pi4RetValFsMplsLdpEntityBfdStatus)
{
#ifdef MPLS_LDP_BFD_WANTED
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        CLI_SET_ERR (LDP_CLI_INVALID_ENTITY);
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsLdpEntityBfdStatus = pLdpEntity->i4BfdStatus;
#else
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pi4RetValFsMplsLdpEntityBfdStatus);
#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityPHPRequestMethod
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityPHPRequestMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityPHPRequestMethod (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       i4SetValFsMplsLdpEntityPHPRequestMethod)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {

        LDP_ENTITY_PHP_CONF (pLdpEntity) =
            (INT1) i4SetValFsMplsLdpEntityPHPRequestMethod;

        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         * NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityTransAddrTlvEnable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityTransAddrTlvEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityTransAddrTlvEnable (tSNMP_OCTET_STRING_TYPE
                                         * pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         i4SetValFsMplsLdpEntityTransAddrTlvEnable)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (i4SetValFsMplsLdpEntityTransAddrTlvEnable == LDP_SNMP_FALSE)
    {
        i4SetValFsMplsLdpEntityTransAddrTlvEnable = LDP_FALSE;
    }

    LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) =
        (UINT1) i4SetValFsMplsLdpEntityTransAddrTlvEnable;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityTransportAddress
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityTransportAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityTransportAddress (tSNMP_OCTET_STRING_TYPE
                                       * pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       u4SetValFsMplsLdpEntityTransportAddress)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT4               u4CfaIfIndex = LDP_ZERO;
    UINT4               u4IpPort = LDP_ZERO;
    UINT1               u1OperStatus = CFA_IF_DOWN;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Initialising the value to zero.
     * Transport Address in tLdpEntity is meaningful only if TransportAddr Kind
     * is LOOPBACK_ADDRESS_TYPE. */
    LDP_ENTITY_TRANSPORT_ADDRESS (pLdpEntity) = LDP_ZERO;

    if (u4SetValFsMplsLdpEntityTransportAddress == LDP_ZERO)
    {
        return SNMP_SUCCESS;
    }

    if (LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) ==
        (UINT1) LOOPBACK_ADDRESS_TYPE)
    {
        if (NetIpv4GetIfIndexFromAddr
            (u4SetValFsMplsLdpEntityTransportAddress, &u4IpPort)
            == NETIPV4_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (NetIpv4GetCfaIfIndexFromPort (u4IpPort,
                                          &u4CfaIfIndex) == NETIPV4_FAILURE)
        {
            return SNMP_FAILURE;
        }

        LDP_ENTITY_TRANSPORT_ADDRESS (pLdpEntity) =
            u4SetValFsMplsLdpEntityTransportAddress;

        pLdpEntity->u4TransIfIndex = u4CfaIfIndex;

        CfaGetIfOperStatus (u4CfaIfIndex, &u1OperStatus);
        if (u1OperStatus == CFA_IF_UP)
        {
            pLdpEntity->bIsTransAddrIntfUp = LDP_TRUE;
        }
        else
        {
            pLdpEntity->bIsTransAddrIntfUp = LDP_FALSE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityLdpOverRsvpEnable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityLdpOverRsvpEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityLdpOverRsvpEnable (tSNMP_OCTET_STRING_TYPE
                                        * pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        INT4
                                        i4SetValFsMplsLdpEntityLdpOverRsvpEnable)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    LDP_ENTITY_LDP_OVER_RSVP_FLAG (pLdpEntity) =
        (UINT1) i4SetValFsMplsLdpEntityLdpOverRsvpEnable;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityOutTunnelIndex
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityOutTunnelIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityOutTunnelIndex (tSNMP_OCTET_STRING_TYPE
                                     * pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     UINT4
                                     u4SetValFsMplsLdpEntityOutTunnelIndex)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpEntity->OutStackTnlInfo.u4TnlId = u4SetValFsMplsLdpEntityOutTunnelIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityOutTunnelInstance
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityOutTunnelInstance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityOutTunnelInstance (tSNMP_OCTET_STRING_TYPE
                                        * pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        UINT4
                                        u4SetValFsMplsLdpEntityOutTunnelInstance)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpEntity->OutStackTnlInfo.u4TnlInstance =
        u4SetValFsMplsLdpEntityOutTunnelInstance;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityOutTunnelIngressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityOutTunnelIngressLSRId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityOutTunnelIngressLSRId (tSNMP_OCTET_STRING_TYPE
                                            * pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            UINT4
                                            u4SetValFsMplsLdpEntityOutTunnelIngressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpEntity->OutStackTnlInfo.u4IngressId =
        u4SetValFsMplsLdpEntityOutTunnelIngressLSRId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityOutTunnelEgressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityOutTunnelEgressLSRId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityOutTunnelEgressLSRId (tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4
                                           u4SetValFsMplsLdpEntityOutTunnelEgressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpEntity->OutStackTnlInfo.u4EgressId =
        u4SetValFsMplsLdpEntityOutTunnelEgressLSRId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityInTunnelIndex
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityInTunnelIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityInTunnelIndex (tSNMP_OCTET_STRING_TYPE
                                    * pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    UINT4 u4SetValFsMplsLdpEntityInTunnelIndex)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpEntity->InStackTnlInfo.u4TnlId = u4SetValFsMplsLdpEntityInTunnelIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityInTunnelInstance
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityInTunnelInstance
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityInTunnelInstance (tSNMP_OCTET_STRING_TYPE
                                       * pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       u4SetValFsMplsLdpEntityInTunnelInstance)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpEntity->InStackTnlInfo.u4TnlInstance =
        u4SetValFsMplsLdpEntityInTunnelInstance;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityInTunnelIngressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityInTunnelIngressLSRId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityInTunnelIngressLSRId (tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4
                                           u4SetValFsMplsLdpEntityInTunnelIngressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpEntity->InStackTnlInfo.u4IngressId =
        u4SetValFsMplsLdpEntityInTunnelIngressLSRId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityInTunnelEgressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityInTunnelEgressLSRId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityInTunnelEgressLSRId (tSNMP_OCTET_STRING_TYPE
                                          * pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4
                                          u4SetValFsMplsLdpEntityInTunnelEgressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpEntity->InStackTnlInfo.u4EgressId =
        u4SetValFsMplsLdpEntityInTunnelEgressLSRId;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityIpv6TransportAddress
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object
                setValFsMplsLdpEntityIpv6TransportAddress
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityIpv6TransportAddress (tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pSetValFsMplsLdpEntityIpv6TransportAddress)
{
#ifdef MPLS_IPV6_WANTED
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT4               u4CfaIfIndex = LDP_ZERO;
    UINT4               u4IpPort = LDP_ZERO;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    UINT1               u1IfFound = FALSE;
    tNetIpv6IfInfo      NetIpIfInfo;
    tCfaIfInfo          CfaIfInfo;
    tIp6Addr            LnklocalIpv6Addr;
    tIp6Addr            SitelocalIpv6Addr;
    tIp6Addr            GlbUniqueIpv6Addr;

    MEMSET (&LnklocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&SitelocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&GlbUniqueIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&NetIpIfInfo, LDP_ZERO, sizeof (tNetIpv6IfInfo));
    MEMSET (&CfaIfInfo, LDP_ZERO, sizeof (tCfaIfInfo));

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Initialising the value to zero.
     * Transport Address in tLdpEntity is meaningful only if TransportAddr Kind
     * is LOOPBACK_ADDRESS_TYPE. */

    MEMSET (&(pLdpEntity->Ipv6TransportAddress), LDP_ZERO, sizeof (tIp6Addr));

    /* In Case of "no transport-address-ipv6" 
     * (u1Ipv6TransAddrTlvEnable = False) , set Ipv6 Address to 0*/
    if (pLdpEntity->u1Ipv6TransAddrTlvEnable == LDP_SNMP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (LDP_ENTITY_IPV6_TRANSPORT_ADDRKIND (pLdpEntity) ==
        (UINT1) LOOPBACK_ADDRESS_TYPE)
    {
        if (pLdpEntity->u1Ipv6TransAddrTlvEnable == LDP_SNMP_FALSE)
        {
            MEMSET (&(pLdpEntity->Ipv6TransportAddress), LDP_ZERO,
                    sizeof (tIp6Addr));
        }
        else
        {
            /* Find IfIndex for Loopback Interface with given Ipv6 Address */
            if (NetIpv6GetFirstIfInfo (&NetIpIfInfo) == NETIPV6_SUCCESS)
            {
                do
                {
                    u4IpPort = NetIpIfInfo.u4IfIndex;

                    if (NetIpv6GetCfaIfIndexFromPort (u4IpPort,
                                                      &u4CfaIfIndex)
                        == NETIPV6_FAILURE)
                    {
                        continue;
                    }
                    if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
                    {
                        continue;
                    }
                    if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
                    {
                        continue;
                    }

                    /* Loopback Interface found, check for IPv6 address match */
                    LdpGetIpv6IfAllAddr ((INT4) u4CfaIfIndex, &LnklocalIpv6Addr,
                                         &SitelocalIpv6Addr,
                                         &GlbUniqueIpv6Addr);

                    if (MEMCMP
                        (pSetValFsMplsLdpEntityIpv6TransportAddress->
                         pu1_OctetList, GlbUniqueIpv6Addr.u1_addr,
                         LDP_IPV6ADR_LEN) == LDP_ZERO)
                    {
                        u1IfFound = TRUE;
                        break;
                    }
                    else if (MEMCMP
                             (pSetValFsMplsLdpEntityIpv6TransportAddress->
                              pu1_OctetList, SitelocalIpv6Addr.u1_addr,
                              LDP_IPV6ADR_LEN) == LDP_ZERO)
                    {
                        u1IfFound = TRUE;
                        break;
                    }
                    else if (MEMCMP
                             (pSetValFsMplsLdpEntityIpv6TransportAddress->
                              pu1_OctetList, LnklocalIpv6Addr.u1_addr,
                              LDP_IPV6ADR_LEN) == LDP_ZERO)
                    {
                        u1IfFound = TRUE;
                        break;
                    }

                    MEMSET ((UINT1 *) &NetIpIfInfo, LDP_ZERO,
                            sizeof (tNetIpv6IfInfo));
                }
                while (NetIpv6GetNextIfInfo (u4IpPort, &NetIpIfInfo)
                       == NETIPV6_SUCCESS);
            }

            MEMCPY (&(pLdpEntity->Ipv6TransportAddress.u1_addr),
                    pSetValFsMplsLdpEntityIpv6TransportAddress->pu1_OctetList,
                    LDP_IPV6ADR_LEN);

            if (u1IfFound == FALSE)
            {
                LDP_DBG (LDP_MAIN_ALL,
                         "\n nmhSetFsMplsLdpEntityIpv6TransportAddress Failure: Interface Not Found \n");
                return SNMP_FAILURE;
            }

            pLdpEntity->u4Ipv6TransIfIndex = u4CfaIfIndex;

            CfaGetIfOperStatus (u4CfaIfIndex, &u1OperStatus);
            if (u1OperStatus == CFA_IF_UP)
            {
                pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_TRUE;
            }
            else
            {
                pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_FALSE;
            }
        }
    }
#else
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pSetValFsMplsLdpEntityIpv6TransportAddress);

#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityIpv6TransAddrTlvEnable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object
                setValFsMplsLdpEntityIpv6TransAddrTlvEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityIpv6TransAddrTlvEnable (tSNMP_OCTET_STRING_TYPE
                                             * pMplsLdpEntityLdpId,
                                             UINT4 u4MplsLdpEntityIndex,
                                             INT4
                                             i4SetValFsMplsLdpEntityIpv6TransAddrTlvEnable)
{
#ifdef MPLS_IPV6_WANTED
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpEntity->u1Ipv6TransAddrTlvEnable =
        (UINT1) i4SetValFsMplsLdpEntityIpv6TransAddrTlvEnable;
#else
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4SetValFsMplsLdpEntityIpv6TransAddrTlvEnable);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityIpv6TransportAddrKind
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object
                setValFsMplsLdpEntityIpv6TransportAddrKind
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityIpv6TransportAddrKind (tSNMP_OCTET_STRING_TYPE
                                            * pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            INT4
                                            i4SetValFsMplsLdpEntityIpv6TransportAddrKind)
{
#ifdef MPLS_IPV6_WANTED
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpEntity->u1Ipv6TransportAddrKind =
        (UINT1) i4SetValFsMplsLdpEntityIpv6TransportAddrKind;
#else
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4SetValFsMplsLdpEntityIpv6TransportAddrKind);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpEntityBfdStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValFsMplsLdpEntityBfdStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpEntityBfdStatus (tSNMP_OCTET_STRING_TYPE
                                * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 i4SetValFsMplsLdpEntityBfdStatus)
{
#ifdef MPLS_LDP_BFD_WANTED

    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1               u1SsnState;
    tLdpBfdSession     *pLdpBfdSession = NULL;

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        CLI_SET_ERR (LDP_CLI_INVALID_ENTITY);
        return SNMP_FAILURE;
    }

    if (pLdpEntity->i4BfdStatus == i4SetValFsMplsLdpEntityBfdStatus)
    {
        return SNMP_SUCCESS;
    }

    pLdpEntity->i4BfdStatus = i4SetValFsMplsLdpEntityBfdStatus;

    TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
    {
        pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

        if (NULL == pLdpSession)
        {
            CLI_SET_ERR (LDP_CLI_INVALID_SESSION);
            continue;
        }
        SSN_GET_SSN_STATE (pLdpSession, u1SsnState);
        if (u1SsnState == LDP_SSM_ST_OPERATIONAL)
        {
            if (i4SetValFsMplsLdpEntityBfdStatus == LDP_BFD_ENABLED)
            {
                LDP_DBG1 (LDP_MAIN_ALL,
                          "\n MAIN: nmhSetFsMplsLdpEntityBfdStatus :: register LDP session index %d with bfd\n",
                          pLdpSession->u4Index);

                if (LdpRegisterWithBfd (pLdpSession) != LDP_SUCCESS)
                {
                    LDP_DBG1 (LDP_MAIN_ALL,
                              "MAIN: Unable to register LDP session index %d with bfd\n",
                              pLdpSession->u4Index);
                }
            }
            else
            {
                LDP_DBG1 (LDP_MAIN_ALL,
                          "\n MAIN: nmhSetFsMplsLdpEntityBfdStatus :: deregister LDP session index %d with bfd\n",
                          pLdpSession->u4Index);

                if (LdpDeRegisterWithBfd (pLdpSession) != LDP_SUCCESS)
                {
                    LDP_DBG1 (LDP_MAIN_ALL,
                              "MAIN: Unable to deregister LDP session index %d with bfd\n",
                              pLdpSession->u4Index);
                }
            }
        }
    }

    /* clean all the disabled bfd sessions for the entity */
    if (i4SetValFsMplsLdpEntityBfdStatus == LDP_BFD_DISABLED)
    {
        pLdpBfdSession =
            (tLdpBfdSession *) TMO_SLL_First (&(pLdpEntity->BfdSessList));
        while (pLdpBfdSession != NULL)
        {
            LdpDeRegisterDisabledBfdSess (pLdpBfdSession);
            TMO_SLL_Delete (&(pLdpEntity->BfdSessList),
                            &(pLdpBfdSession->NextBfdSess));
            MemReleaseMemBlock (LDP_BFD_SSN_POOL_ID, (UINT1 *) pLdpBfdSession);
            pLdpBfdSession =
                (tLdpBfdSession *) TMO_SLL_First (&(pLdpEntity->BfdSessList));
        }
        TMO_SLL_Init (&(pLdpEntity->BfdSessList));
    }

    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4SetValFsMplsLdpEntityBfdStatus);
    return SNMP_FAILURE;
#endif
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityPHPRequestMethod
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityPHPRequestMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityPHPRequestMethod (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          INT4
                                          i4TestValFsMplsLdpEntityPHPRequestMethod)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsLdpEntityPHPRequestMethod)
    {
        case MPLS_IPV4_EXPLICIT_NULL_LABEL:
        case MPLS_IMPLICIT_NULL_LABEL:
        case MPLS_PHP_DISABLED:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityTransAddrTlvEnable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityTransAddrTlvEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityTransAddrTlvEnable (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE
                                            * pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            INT4
                                            i4TestValFsMplsLdpEntityTransAddrTlvEnable)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if ((i4TestValFsMplsLdpEntityTransAddrTlvEnable != LDP_SNMP_FALSE) &&
        (i4TestValFsMplsLdpEntityTransAddrTlvEnable != LDP_SNMP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityTransportAddress
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityTransportAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityTransportAddress (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE
                                          * pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4
                                          u4TestValFsMplsLdpEntityTransportAddress)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4Mask = 0xffffffff;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Transport Address to be set must be present in any of the local
     * interface */
    if (NetIpv4IpIsLocalNet (u4TestValFsMplsLdpEntityTransportAddress)
        == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    RtQuery.u4DestinationIpAddress = u4TestValFsMplsLdpEntityTransportAddress;
    RtQuery.u4DestinationSubnetMask = u4Mask;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    /* Now, Validating whether the Interface of the IP Address to be
     * configured as Transport Address is UP or DOWN. If it is down,
     * NetIpv4GetRoute will return FAILURE. */

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityLdpOverRsvpEnable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityLdpOverRsvpEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityLdpOverRsvpEnable (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           INT4
                                           i4TestValFsMplsLdpEntityLdpOverRsvpEnable)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if ((i4TestValFsMplsLdpEntityLdpOverRsvpEnable != LDP_SNMP_FALSE) &&
        (i4TestValFsMplsLdpEntityLdpOverRsvpEnable != LDP_SNMP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsLdpEntityLdpOverRsvpEnable == LDP_SNMP_TRUE) &&
        (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityOutTunnelIndex
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityOutTunnelIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityOutTunnelIndex (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE
                                        * pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        UINT4
                                        u4TestValFsMplsLdpEntityOutTunnelIndex)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsLdpEntityOutTunnelIndex != LDP_ZERO) &&
        (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityOutTunnelInstance
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityOutTunnelInstance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityOutTunnelInstance (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4
                                           u4TestValFsMplsLdpEntityOutTunnelInstance)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsLdpEntityOutTunnelInstance != LDP_ZERO) &&
        (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityOutTunnelIngressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityOutTunnelIngressLSRId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityOutTunnelIngressLSRId (UINT4 *pu4ErrorCode,
                                               tSNMP_OCTET_STRING_TYPE
                                               * pMplsLdpEntityLdpId,
                                               UINT4 u4MplsLdpEntityIndex,
                                               UINT4
                                               u4TestValFsMplsLdpEntityOutTunnelIngressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    UNUSED_PARAM (u4TestValFsMplsLdpEntityOutTunnelIngressLSRId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsLdpEntityOutTunnelIngressLSRId != LDP_ZERO) &&
        (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityOutTunnelEgressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityOutTunnelEgressLSRId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityOutTunnelEgressLSRId (UINT4 *pu4ErrorCode,
                                              tSNMP_OCTET_STRING_TYPE
                                              * pMplsLdpEntityLdpId,
                                              UINT4 u4MplsLdpEntityIndex,
                                              UINT4
                                              u4TestValFsMplsLdpEntityOutTunnelEgressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    UNUSED_PARAM (u4TestValFsMplsLdpEntityOutTunnelEgressLSRId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsLdpEntityOutTunnelEgressLSRId != LDP_ZERO) &&
        (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityInTunnelIndex
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityInTunnelIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityInTunnelIndex (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       u4TestValFsMplsLdpEntityInTunnelIndex)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsLdpEntityInTunnelIndex != LDP_ZERO) &&
        (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityInTunnelInstance
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityInTunnelInstance
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityInTunnelInstance (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE
                                          * pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4
                                          u4TestValFsMplsLdpEntityInTunnelInstance)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsLdpEntityInTunnelInstance != LDP_ZERO) &&
        (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityInTunnelIngressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityInTunnelIngressLSRId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityInTunnelIngressLSRId (UINT4 *pu4ErrorCode,
                                              tSNMP_OCTET_STRING_TYPE
                                              * pMplsLdpEntityLdpId,
                                              UINT4 u4MplsLdpEntityIndex,
                                              UINT4
                                              u4TestValFsMplsLdpEntityInTunnelIngressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsLdpEntityInTunnelIngressLSRId != LDP_ZERO) &&
        (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityInTunnelEgressLSRId
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityInTunnelEgressLSRId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityInTunnelEgressLSRId (UINT4 *pu4ErrorCode,
                                             tSNMP_OCTET_STRING_TYPE
                                             * pMplsLdpEntityLdpId,
                                             UINT4 u4MplsLdpEntityIndex,
                                             UINT4
                                             u4TestValFsMplsLdpEntityInTunnelEgressLSRId)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsLdpEntityInTunnelEgressLSRId != LDP_ZERO) &&
        (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2FsMplsLdpEntityIpv6TransportAddress
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object
                testValFsMplsLdpEntityIpv6TransportAddress
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityIpv6TransportAddress (UINT4 *pu4ErrorCode,
                                              tSNMP_OCTET_STRING_TYPE
                                              * pMplsLdpEntityLdpId,
                                              UINT4 u4MplsLdpEntityIndex,
                                              tSNMP_OCTET_STRING_TYPE
                                              *
                                              pTestValFsMplsLdpEntityIpv6TransportAddress)
{
#ifdef MPLS_IPV6_WANTED
    UINT1              *pu1LdpEntityId = NULL;
    UINT1               u1Scope = 0;
    UINT4               u4IfIndex = 0;
    tLdpEntity         *pLdpEntity = NULL;
    tNetIpv6RtInfoQueryMsg RtQuery;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tIp6Addr            Ipv6TransAddr;

    MEMSET (&Ipv6TransAddr, 0, sizeof (tIp6Addr));
    MEMCPY (&Ipv6TransAddr.u1_addr,
            pTestValFsMplsLdpEntityIpv6TransportAddress->pu1_OctetList,
            IP6_ADDR_SIZE);

    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MplsValidateIPv6Addr (pu4ErrorCode,
                              pTestValFsMplsLdpEntityIpv6TransportAddress) ==
        SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "nmhTestv2FsMplsLdpEntityIpv6TransportAddress"
                   " Failed: MplsValidateIpv6Addr Failure \t\n");
        return SNMP_FAILURE;
    }

    /* Transport Address to be set must be present in any of the local
     * interface */
    if (NetIpv6IsOurAddress (&Ipv6TransAddr, &u4IfIndex) == NETIPV6_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* "To DO" If Address is Link Local, then Find the Ifindex from IP6 Addr
     *& check the IF Oper status else use NetIpv6GetRoute for SLA & GUA*/

    u1Scope = Ip6GetAddrScope (&Ipv6TransAddr);

    if (u1Scope == ADDR6_SCOPE_GLOBAL || u1Scope == ADDR6_SCOPE_SITELOCAL)
    {
        RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
        RtQuery.u4ContextId = VCM_DEFAULT_CONTEXT;

        Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst, &Ipv6TransAddr);
        NetIpv6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

        /* Now, Validating whether the Interface of the IP Address to be
         * configured as Transport Address is UP or DOWN. If it is down,
         * NetIpv6GetRoute will return FAILURE. */
        if (NetIpv6GetRoute (&RtQuery, &NetIpv6RtInfo) == NETIPV6_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pTestValFsMplsLdpEntityIpv6TransportAddress);
#endif

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityIpv6TransAddrTlvEnable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object
                testValFsMplsLdpEntityIpv6TransAddrTlvEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityIpv6TransAddrTlvEnable (UINT4 *pu4ErrorCode,
                                                tSNMP_OCTET_STRING_TYPE
                                                * pMplsLdpEntityLdpId,
                                                UINT4 u4MplsLdpEntityIndex,
                                                INT4
                                                i4TestValFsMplsLdpEntityIpv6TransAddrTlvEnable)
{
#ifdef MPLS_IPV6_WANTED
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if ((i4TestValFsMplsLdpEntityIpv6TransAddrTlvEnable != LDP_SNMP_FALSE) &&
        (i4TestValFsMplsLdpEntityIpv6TransAddrTlvEnable != LDP_SNMP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4TestValFsMplsLdpEntityIpv6TransAddrTlvEnable);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityIpv6TransportAddrKind
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object
                testValFsMplsLdpEntityIpv6TransportAddrKind
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityIpv6TransportAddrKind (UINT4 *pu4ErrorCode,
                                               tSNMP_OCTET_STRING_TYPE
                                               * pMplsLdpEntityLdpId,
                                               UINT4 u4MplsLdpEntityIndex,
                                               INT4
                                               i4TestValFsMplsLdpEntityIpv6TransportAddrKind)
{
#ifdef MPLS_IPV6_WANTED
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsLdpEntityIpv6TransportAddrKind !=
         INTERFACE_ADDRESS_TYPE)
        && (i4TestValFsMplsLdpEntityIpv6TransportAddrKind !=
            LOOPBACK_ADDRESS_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4TestValFsMplsLdpEntityIpv6TransportAddrKind);
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpEntityBfdStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValFsMplsLdpEntityBfdStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpEntityBfdStatus (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   INT4 i4TestValFsMplsLdpEntityBfdStatus)
{
#ifdef MPLS_LDP_BFD_WANTED
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        CLI_SET_ERR (LDP_CLI_INVALID_ENTITY);
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsLdpEntityBfdStatus != LDP_BFD_ENABLED) &&
        (i4TestValFsMplsLdpEntityBfdStatus != LDP_BFD_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (LDP_CLI_INVALID_BFD_STATUS_VALUE);
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
#else
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4TestValFsMplsLdpEntityBfdStatus);
    return SNMP_FAILURE;
#endif
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsLdpEntityTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLdpEntityTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsLdpLsrId
 Input       :  The Indices

                The Object 
                retValFsMplsLdpLsrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpLsrId (tSNMP_OCTET_STRING_TYPE * pRetValFsMplsLdpLsrId)
{
    UINT1               au1IpAddr[LDP_LSR_ID_LEN];

    MEMSET (au1IpAddr, LDP_ZERO, LDP_LSR_ID_LEN);

    if (MEMCMP ((UINT1 *) LDP_NEW_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr,
                (UINT1 *) au1IpAddr, LDP_LSR_ID_LEN) == LDP_ZERO)
    {
        /* If the temporary LSR Id is ZERO, return the original LSR Id. */
        MEMCPY (pRetValFsMplsLdpLsrId->pu1_OctetList,
                LDP_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr, LDP_LSR_ID_LEN);
        pRetValFsMplsLdpLsrId->i4_Length = LDP_LSR_ID_LEN;

        return SNMP_SUCCESS;
    }

    MEMCPY ((UINT1 *) pRetValFsMplsLdpLsrId->pu1_OctetList,
            (UINT1 *) LDP_NEW_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr,
            LDP_LSR_ID_LEN);
    pRetValFsMplsLdpLsrId->i4_Length = LDP_LSR_ID_LEN;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpForceOption
 Input       :  The Indices

                The Object 
                retValFsMplsLdpForceOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpForceOption (INT4 *pi4RetValFsMplsLdpForceOption)
{
    *pi4RetValFsMplsLdpForceOption = (INT4) LDP_FORCE_OPTION (MPLS_DEF_INCARN);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsLdpLsrId
 Input       :  The Indices

                The Object 
                setValFsMplsLdpLsrId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpLsrId (tSNMP_OCTET_STRING_TYPE * pSetValFsMplsLdpLsrId)
{
    UINT4               u4IpAddress = LDP_ZERO;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4TempIpAddress = LDP_ZERO;
    UINT4               u4Mask = 0xffffffff;
    tLdpEntity         *pLdpEntity = NULL;

    if (pSetValFsMplsLdpLsrId->i4_Length != LDP_ZERO)
    {
        MEMCPY ((UINT1 *) &u4IpAddress, pSetValFsMplsLdpLsrId->pu1_OctetList,
                LDP_LSR_ID_LEN);
    }
    else
    {
        /* This is to tackle the no form of the CLI Command 'router-id'.
         * If the length is zero, we need to choose the Highest Ip Address
         * available on the system. */
        NetIpv4GetHighestIpAddr (&u4IpAddress);
        u4IpAddress = OSIX_NTOHL (u4IpAddress);
    }
    if (LDP_FORCE_OPTION (MPLS_DEF_INCARN) == LDP_SNMP_TRUE)
    {
        if (pSetValFsMplsLdpLsrId->i4_Length == LDP_ZERO)
        {
            MEMSET ((UINT1 *) (LDP_NEW_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr),
                    LDP_ZERO, LDP_LSR_ID_LEN);
        }
        else
        {
            MEMCPY ((UINT1 *) (LDP_NEW_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr),
                    (UINT1 *) &u4IpAddress, LDP_LSR_ID_LEN);
        }
    }
    if (MEMCMP (&u4IpAddress,
                (UINT1 *) (LDP_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr),
                LDP_LSR_ID_LEN) == 0
        && pSetValFsMplsLdpLsrId->i4_Length != LDP_ZERO)
    {
        MEMCPY ((UINT1 *) (LDP_NEW_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr),
                (UINT1 *) &u4IpAddress, LDP_LSR_ID_LEN);
        /* New LSR id and the current LSR id sare same, 
         * so skip the LSR id change*/
        return SNMP_SUCCESS;
    }

    /* If no LDP entity is created, copy the new LSR_ID immediately, 
     * So that new entity will create on Newly configured LSR_ID.
     * If Entity is already created, post it to LDP queue, 
     * so that first it make all Entity down and then configure the new one */
    pLdpEntity =
        (tLdpEntity *) TMO_SLL_First (&LDP_ENTITY_LIST (MPLS_DEF_INCARN));
    if (pLdpEntity == NULL)
    {
        if (MsrGetRestorationStatus () != ISS_TRUE)
        {
            MEMCPY ((UINT1 *) (LDP_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr),
                    (UINT1 *) &u4IpAddress, LDP_LSR_ID_LEN);
        }
    }

    if (LDP_FORCE_OPTION (MPLS_DEF_INCARN) == LDP_SNMP_FALSE)
    {
        if (pSetValFsMplsLdpLsrId->i4_Length == LDP_ZERO)
        {
            MEMSET ((UINT1 *) (LDP_NEW_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr),
                    LDP_ZERO, LDP_LSR_ID_LEN);
        }
        else
        {
            /* Since, force option is not set we are not retriggering the
             * LDP Sessions. Instead copying the Router ID passed by the user
             * into a temporary variable. Router ID will come into effect once
             * the interface of the current active Router ID goes down during
             * which retriggering of LDP Sessions will happen. */
            MEMCPY ((UINT1 *) (LDP_NEW_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr),
                    (UINT1 *) &u4IpAddress, LDP_LSR_ID_LEN);

            if (MsrGetRestorationStatus () == ISS_TRUE)
            {
                MEMCPY ((UINT1 *) &u4TempIpAddress,
                        (LDP_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr),
                        LDP_LSR_ID_LEN);
                MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
                MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
                RtQuery.u4DestinationIpAddress = OSIX_NTOHL (u4TempIpAddress);
                RtQuery.u4DestinationSubnetMask = u4Mask;
                RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
                if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) != NETIPV4_SUCCESS)
                {
                    LdpRetriggerEntitySessions (u4IpAddress, LDP_ZERO,
                                                u4TempIpAddress,
                                                LDP_IF_OPER_DISABLE);
                }
            }
        }
        return SNMP_SUCCESS;
    }

    gu4LsrIpAddr = u4IpAddress;

    if (LdpSNMPEventHandler (LDP_ROUTERID_CHANGE, &gu4LsrIpAddr,
                             MPLS_DEF_INCARN) == LDP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpForceOption
 Input       :  The Indices

                The Object 
                setValFsMplsLdpForceOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpForceOption (INT4 i4SetValFsMplsLdpForceOption)
{
    LDP_FORCE_OPTION (MPLS_DEF_INCARN) = (UINT1) i4SetValFsMplsLdpForceOption;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpLsrId
 Input       :  The Indices

                The Object 
                testValFsMplsLdpLsrId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpLsrId (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsMplsLdpLsrId)
{
    UINT4               u4IpAddress = LDP_ZERO;
    UINT4               u4NvRamIpAddr = LDP_ZERO;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    tLdpEntity         *pLdpEntity = NULL;
    UINT4               u4Mask = 0xffffffff;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    MEMCPY ((UINT1 *) &u4IpAddress, pTestValFsMplsLdpLsrId->pu1_OctetList,
            LDP_LSR_ID_LEN);

    u4IpAddress = OSIX_NTOHL (u4IpAddress);

    if (u4IpAddress == LDP_ZERO)
    {
        /* This is to reset the configured FsMplsLdpLsrId */

        /* "no router-id command is not allowed if Current router-id is of NVRAM
         * as this router is not given thru command hence cannot be removed
         *  to command (with ot without force)*/
        u4NvRamIpAddr = IssGetIpAddrFromNvRam ();
        u4NvRamIpAddr = OSIX_HTONL (u4NvRamIpAddr);

        if (MEMCMP (&u4NvRamIpAddr,
                    (UINT1 *) (LDP_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr),
                    LDP_LSR_ID_LEN) == LDP_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        /* "no router-id" command not allowed (without force) when there is already
         * an entity using that router as its LSR-ID*/
        /* This code is added to not to set LDP_NEW_LSR_ID */

        if (LDP_FORCE_OPTION (MPLS_DEF_INCARN) == LDP_SNMP_FALSE)
        {
            pLdpEntity =
                (tLdpEntity *)
                TMO_SLL_First (&LDP_ENTITY_LIST (MPLS_DEF_INCARN));
            if (pLdpEntity != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
                return SNMP_FAILURE;
            }
        }

        return SNMP_SUCCESS;
    }

    if (LdpGetIsIpv4ActiveInterface () == LDP_FALSE)
    {
        return SNMP_SUCCESS;
    }

    if (!((MPLS_IS_ADDR_CLASS_A (u4IpAddress)) ||
          (MPLS_IS_ADDR_CLASS_B (u4IpAddress)) ||
          (MPLS_IS_ADDR_CLASS_C (u4IpAddress))))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((MPLS_IS_ADDR_LOOPBACK (u4IpAddress)) ||
        (MPLS_IS_BROADCAST_ADDR (u4IpAddress)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* Ldp LSR Identifier to be set need not to be present in any of the local
     * interface  If Present over any interface then check routability of that IP*/
    if (NetIpv4IfIsOurAddress (u4IpAddress) == NETIPV4_SUCCESS)
    {
        RtQuery.u4DestinationIpAddress = u4IpAddress;
        RtQuery.u4DestinationSubnetMask = u4Mask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
        /* Now, Validating whether the Interface of the IP Address to be
         * configured as Router ID is UP or DOWN. If it is down,
         * NetIpv4GetRoute will return FAILURE. */
        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        if (NetIpRtInfo.u2RtProto != CIDR_LOCAL_ID)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    /* router-id command not allowed (without force) when there is already
     * an entity using that router as its LSR-ID*/
    /* This code is added to not to set LDP_NEW_LSR_ID */
    if (LDP_FORCE_OPTION (MPLS_DEF_INCARN) == LDP_SNMP_FALSE)
    {
        pLdpEntity =
            (tLdpEntity *) TMO_SLL_First (&LDP_ENTITY_LIST (MPLS_DEF_INCARN));
        if (pLdpEntity != NULL)
        {
            *pu4ErrorCode = SNMP_ERR_BAD_VALUE;
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpForceOption
 Input       :  The Indices

                The Object 
                testValFsMplsLdpForceOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpForceOption (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsMplsLdpForceOption)
{
    if ((i4TestValFsMplsLdpForceOption != LDP_SNMP_TRUE) &&
        (i4TestValFsMplsLdpForceOption != LDP_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsLdpLsrId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLdpLsrId (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsLdpForceOption
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLdpForceOption (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpGrCapability
 Input       :  The Indices

                The Object 
                retValFsMplsLdpGrCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpGrCapability (INT4 *pi4RetValFsMplsLdpGrCapability)
{
    *pi4RetValFsMplsLdpGrCapability =
        (INT4) LDP_GR_CAPABILITY (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpGrForwardEntryHoldTime
 Input       :  The Indices

                The Object 
                retValFsMplsLdpGrForwardEntryHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpGrForwardEntryHoldTime (INT4
                                       *pi4RetValFsMplsLdpGrForwardEntryHoldTime)
{
    *pi4RetValFsMplsLdpGrForwardEntryHoldTime =
        (INT4) LDP_GR_FWD_HOLDING_TIME (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpGrMaxRecoveryTime
 Input       :  The Indices

                The Object 
                retValFsMplsLdpGrMaxRecoveryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpGrMaxRecoveryTime (INT4 *pi4RetValFsMplsLdpGrMaxRecoveryTime)
{
    *pi4RetValFsMplsLdpGrMaxRecoveryTime =
        (INT4) LDP_GR_MAX_RECOVERY_TIME (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpGrNeighborLivenessTime
 Input       :  The Indices

                The Object 
                retValFsMplsLdpGrNeighborLivenessTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpGrNeighborLivenessTime (INT4
                                       *pi4RetValFsMplsLdpGrNeighborLivenessTime)
{
    *pi4RetValFsMplsLdpGrNeighborLivenessTime =
        (INT4) LDP_GR_NBR_LIVENESS_TIME (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpGrProgressStatus
 Input       :  The Indices

                The Object 
                retValFsMplsLdpGrProgressStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpGrProgressStatus (INT4 *pi4RetValFsMplsLdpGrProgressStatus)
{
    *pi4RetValFsMplsLdpGrProgressStatus =
        (INT4) LDP_GR_PROGRESS_STATUS (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsLdpGrCapability
 Input       :  The Indices

                The Object 
                setValFsMplsLdpGrCapability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpGrCapability (INT4 i4SetValFsMplsLdpGrCapability)
{
    LDP_GR_CAPABILITY (MPLS_DEF_INCARN) = (UINT1) i4SetValFsMplsLdpGrCapability;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpGrForwardEntryHoldTime
 Input       :  The Indices

                The Object 
                setValFsMplsLdpGrForwardEntryHoldTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpGrForwardEntryHoldTime (INT4
                                       i4SetValFsMplsLdpGrForwardEntryHoldTime)
{
    LDP_GR_FWD_HOLDING_TIME (MPLS_DEF_INCARN) = (UINT2)
        i4SetValFsMplsLdpGrForwardEntryHoldTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpGrMaxRecoveryTime
 Input       :  The Indices

                The Object 
                setValFsMplsLdpGrMaxRecoveryTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpGrMaxRecoveryTime (INT4 i4SetValFsMplsLdpGrMaxRecoveryTime)
{
    LDP_GR_MAX_RECOVERY_TIME (MPLS_DEF_INCARN) = (UINT2)
        i4SetValFsMplsLdpGrMaxRecoveryTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpGrNeighborLivenessTime
 Input       :  The Indices

                The Object 
                setValFsMplsLdpGrNeighborLivenessTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpGrNeighborLivenessTime (INT4
                                       i4SetValFsMplsLdpGrNeighborLivenessTime)
{
    LDP_GR_NBR_LIVENESS_TIME (MPLS_DEF_INCARN) = (UINT2)
        i4SetValFsMplsLdpGrNeighborLivenessTime;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpGrCapability
 Input       :  The Indices

                The Object 
                testValFsMplsLdpGrCapability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpGrCapability (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsMplsLdpGrCapability)
{

    if (LDP_INCARN_ADMIN_STATUS (MPLS_DEF_INCARN) == LDP_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsLdpGrCapability < LDP_GR_CAPABILITY_NONE) ||
        (i4TestValFsMplsLdpGrCapability > LDP_GR_CAPABILITY_HELPER))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpGrForwardEntryHoldTime
 Input       :  The Indices

                The Object 
                testValFsMplsLdpGrForwardEntryHoldTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpGrForwardEntryHoldTime (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4TestValFsMplsLdpGrForwardEntryHoldTime)
{
    if ((LDP_INCARN_ADMIN_STATUS (MPLS_DEF_INCARN) == LDP_ADMIN_DOWN) ||
        (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) == LDP_GR_CAPABILITY_NONE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsLdpGrForwardEntryHoldTime < LDP_MIN_FWD_HOLDING_TIME) ||
        (i4TestValFsMplsLdpGrForwardEntryHoldTime > LDP_MAX_FWD_HOLDING_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpGrMaxRecoveryTime
 Input       :  The Indices

                The Object 
                testValFsMplsLdpGrMaxRecoveryTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpGrMaxRecoveryTime (UINT4 *pu4ErrorCode,
                                     INT4 i4TestValFsMplsLdpGrMaxRecoveryTime)
{
    if ((LDP_INCARN_ADMIN_STATUS (MPLS_DEF_INCARN) == LDP_ADMIN_DOWN) ||
        (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) == LDP_GR_CAPABILITY_NONE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsLdpGrMaxRecoveryTime < LDP_MIN_MAX_RECOVERY_TIME) ||
        (i4TestValFsMplsLdpGrMaxRecoveryTime > LDP_MAX_MAX_RECOVERY_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpGrNeighborLivenessTime
 Input       :  The Indices

                The Object 
                testValFsMplsLdpGrNeighborLivenessTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpGrNeighborLivenessTime (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4TestValFsMplsLdpGrNeighborLivenessTime)
{
    if ((LDP_INCARN_ADMIN_STATUS (MPLS_DEF_INCARN) == LDP_ADMIN_DOWN) ||
        (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) == LDP_GR_CAPABILITY_NONE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsLdpGrNeighborLivenessTime < LDP_MIN_NBR_LIVENESS_TIME)
        || (i4TestValFsMplsLdpGrNeighborLivenessTime >
            LDP_MAX_NBR_LIVENESS_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsLdpGrCapability
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLdpGrCapability (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsLdpGrForwardEntryHoldTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLdpGrForwardEntryHoldTime (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsLdpGrMaxRecoveryTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLdpGrMaxRecoveryTime (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsLdpGrNeighborLivenessTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLdpGrNeighborLivenessTime (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsLdpPeerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsLdpPeerTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsLdpPeerTable (tSNMP_OCTET_STRING_TYPE
                                            * pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            tSNMP_OCTET_STRING_TYPE
                                            * pMplsLdpPeerLdpId)
{
    return (nmhValidateIndexInstanceMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                                      u4MplsLdpEntityIndex,
                                                      pMplsLdpPeerLdpId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsLdpPeerTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsLdpPeerTable (tSNMP_OCTET_STRING_TYPE
                                    * pMplsLdpEntityLdpId,
                                    UINT4 *pu4MplsLdpEntityIndex,
                                    tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId)
{
    return (nmhGetFirstIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                              pu4MplsLdpEntityIndex,
                                              pMplsLdpPeerLdpId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsLdpPeerTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpPeerLdpId
                nextMplsLdpPeerLdpId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsLdpPeerTable (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   UINT4 *pu4NextMplsLdpEntityIndex,
                                   tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextMplsLdpPeerLdpId)
{
    return (nmhGetNextIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                             pNextMplsLdpEntityLdpId,
                                             u4MplsLdpEntityIndex,
                                             pu4NextMplsLdpEntityIndex,
                                             pMplsLdpPeerLdpId,
                                             pNextMplsLdpPeerLdpId));

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsLdpPeerGrReconnectTime
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValFsMplsLdpPeerGrReconnectTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpPeerGrReconnectTime (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                    INT4 *pi4RetValFsMplsLdpPeerGrReconnectTime)
{
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsLdpPeerGrReconnectTime = (INT4) pLdpPeer->u2GrReconnectTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpPeerGrRecoveryTime
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValFsMplsLdpPeerGrRecoveryTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpPeerGrRecoveryTime (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                   INT4 *pi4RetValFsMplsLdpPeerGrRecoveryTime)
{
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsLdpPeerGrRecoveryTime = (INT4) pLdpPeer->u2GrRecoveryTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpPeerGrProgressStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValFsMplsLdpPeerGrProgressStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpPeerGrProgressStatus (tSNMP_OCTET_STRING_TYPE
                                     * pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpPeerLdpId,
                                     INT4
                                     *pi4RetValFsMplsLdpPeerGrProgressStatus)
{
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsLdpPeerGrProgressStatus = (INT4)
        pLdpPeer->u1GrProgressState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpConfigurationSequenceTLVEnable
 Input       :  The Indices

                The Object
                retValFsMplsLdpConfigurationSequenceTLVEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpConfigurationSequenceTLVEnable (INT4
                                               *pi4RetValFsMplsLdpConfigurationSequenceTLVEnable)
{
    *pi4RetValFsMplsLdpConfigurationSequenceTLVEnable =
        (INT4) gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1ConfigSeqTlvOption;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsLdpConfigurationSequenceTLVEnable
 Input       :  The Indices

                The Object
                setValFsMplsLdpConfigurationSequenceTLVEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpConfigurationSequenceTLVEnable (INT4
                                               i4SetValFsMplsLdpConfigurationSequenceTLVEnable)
{
    gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1ConfigSeqTlvOption =
        (UINT1) i4SetValFsMplsLdpConfigurationSequenceTLVEnable;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpConfigurationSequenceTLVEnable
 Input       :  The Indices

                The Object
                testValFsMplsLdpConfigurationSequenceTLVEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpConfigurationSequenceTLVEnable (UINT4 *pu4ErrorCode,
                                                  INT4
                                                  i4TestValFsMplsLdpConfigurationSequenceTLVEnable)
{
    if ((i4TestValFsMplsLdpConfigurationSequenceTLVEnable == LDP_SNMP_TRUE) ||
        (i4TestValFsMplsLdpConfigurationSequenceTLVEnable == LDP_SNMP_FALSE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsLdpConfigurationSequenceTLVEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLdpConfigurationSequenceTLVEnable (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
