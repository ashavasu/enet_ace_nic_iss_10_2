#ifndef __LDPECCLI_C__
#define __LDPECCLI_C__

/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpcli.c,v 1.91 2017/06/20 12:17:44 siva Exp $
 *
 * Description:Contains Action routines for CLI LDP commands. 
 *
 *******************************************************************/

#include "ldpincs.h"
#include "mplscli.h"
#include "ldpclip.h"
#include "stdldplw.h"
#include "stdgnllw.h"
#include "stdftnlw.h"
#include "stdlsrlw.h"
#include "stdldpwr.h"
#include "stdgnlwr.h"
#include "fsmplswr.h"
#include "stdgnlcli.h"
#include "stdldpcli.h"
#include "fsmplscli.h"
#include "mplslsr.h"
#include "rtm.h"
#include "ldplwinc.h"

typedef struct _LdpDummy
{
    tSNMP_OCTET_STRING_TYPE LdpEntityLdpId;
    tSNMP_OCTET_STRING_TYPE PeerLdpId;
    tCliHandle          CliHandle;
    UINT4               u4MplsLdpEntityIndex;
    UINT4               u4HelloAdjacencyIndex;
    UINT4               u4Flag;
}
tLdpDummy1;

typedef struct _LdpNeighborDummy
{
    tCliHandle          CliHandle;
    UINT4               u4MplsLdpEntityIndex;
    UINT4               u4HelloAdjacencyIndex;
    UINT4               u4Flag;
}
tLdpNeighborDummy1;

PRIVATE INT4 LdpDisplayDiscovery ARG_LIST ((tLdpDummy1 * LdpDummy,
                                            UINT1 *pu1PeerLdpId,
                                            INT4 i4AdjacencyType,
                                            UINT1 *pu1IpAddr));

PRIVATE INT4 LdpDisplayNeighbor ARG_LIST ((tLdpNeighborDummy1 * LdpDummy,
                                           tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpPeerLdpId,
                                           UINT1 *au1IfName));
PRIVATE INT4 LdpCliShowDatabase ARG_LIST ((tCliHandle CliHandle));

PRIVATE VOID
    LdpCliPrintNormUstrCtrlBlk ARG_LIST ((tCliHandle CliHandle,
                                          tUstrLspCtrlBlock * pUstrCtrlBlk));
PRIVATE VOID
    LdpCliPrintNormLCB ARG_LIST ((tCliHandle CliHandle,
                                  tLspCtrlBlock * pLspCtrlBlk));
PRIVATE VOID LdpCliPrintDatabaseHeader ARG_LIST ((tCliHandle CliHandle));
PRIVATE VOID LdpCliMplsLdpPolicy ARG_LIST ((tCliHandle CliHandle, UINT4));

#ifdef LDP_GR_WANTED
PRIVATE VOID
       LdpCliShowStaleDatabase (tCliHandle CliHandle, UINT2 *pu2NumActiveLsp);
#endif

INT4
cli_process_ldp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    UINT4              *pu4Args[LDP_CLI_MAX_ARGS];
    INT1                i1ArgNo = LDP_ZERO;
    va_list             ap;
    INT4                i4RetStatus = LDP_ZERO;
    INT4                i4LabelDistMode = LDP_ZERO;
    INT4                i4LabelRetMode = LDP_ZERO;
    INT4                i4LabelAllocMode = LDP_ZERO;
    INT4                i4PhpMode = LDP_ZERO;
    UINT4               u4EntityIndex = LDP_ZERO;
    UINT4               u4IpAddr = LDP_ZERO;
    UINT4               u4InTnlId = LDP_ZERO;
    UINT4               u4OutTnlId = LDP_ZERO;
    UINT4               u4Flag = LDP_ZERO;
    INT4                i4DbgFlag = LDP_ZERO;
    INT4                i4Dbg = LDP_ZERO;
    INT4                i4NbrTime = LDP_ZERO;
    INT4                i4FwdHoldTime = LDP_ZERO;
    INT4                i4RecoveryTime = LDP_ZERO;
    INT4                i4GrCapability = LDP_ZERO;
    INT4                i4ConfigSeqTlvOption = LDP_ZERO;
    INT4                i4PathVectorLimit = LDP_ZERO;
#ifdef MPLS_LDP_BFD_WANTED
    INT4                i4BfdStatus = LDP_ZERO;
#endif
    UINT4               u4ErrCode = LDP_ZERO;
    tGenU4Addr          CLIAddr;

    MEMSET (&CLIAddr, 0, sizeof (tGenU4Addr));
    if (LDP_INITIALISED != TRUE)
    {
        CliPrintf (CliHandle, "\r %%LDP module is shutdown\n");
        return CLI_FAILURE;
    }
    va_start (ap, u4Command);

    /* Collect all arguments in the array */
    while (LDP_ONE)
    {
        pu4Args[i1ArgNo++] = va_arg (ap, UINT4 *);
        if (i1ArgNo == LDP_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    CliRegisterLock (CliHandle, LdpLock, LdpUnLock);
    LdpLock ();
    /* Call Corresponding functions based on Command given */
    switch (u4Command)
    {
        case CLI_MPLS_LDP:
        {
            i4RetStatus = LdpCliMplsLdpChangeMode (CliHandle, u4EntityIndex);
            break;
        }
        case CLI_MPLS_LDP_ENTITY:
        {
            i4RetStatus = LdpCliCreateEntity (CliHandle, *pu4Args[0]);
            break;
        }
        case CLI_MPLS_LDP_NO_ENTITY:
        {
            i4RetStatus = LdpCliDeleteEntity (CliHandle, *pu4Args[0]);
            break;
        }
        case CLI_MPLS_LDP_HELLO:
        {
            i4RetStatus = LdpCliHelloHoldTime (CliHandle, *pu4Args[0]);
            break;
        }
        case CLI_MPLS_LDP_NO_HELLO:
        {
            i4RetStatus = LdpCliHelloHoldTime (CliHandle,
                                               (UINT4)
                                               LDP_HELLOHOLDTIME_DEFAULT_TIME);
            break;
        }
        case CLI_MPLS_LDP_KEEPALIVE:
        {
            i4RetStatus = LdpCliKeepAliveHoldTime (CliHandle, *pu4Args[0]);
            break;
        }
        case CLI_MPLS_LDP_NO_KEEPALIVE:
        {
            i4RetStatus = LdpCliKeepAliveHoldTime (CliHandle,
                                                   (UINT4)
                                                   LDP_KEEPALIVE_DEFAULT_TIME);
            break;
        }
        case CLI_MPLS_LDP_MAX_HOPS:
        {
            i4RetStatus = LdpCliMaxHops (CliHandle, *pu4Args[0]);
            break;
        }
        case CLI_MPLS_LDP_NO_MAX_HOPS:
        {
            i4RetStatus = LdpCliMaxHops (CliHandle,
                                         (UINT4) LDP_DEF_HOP_CNT_LIM);
            break;
        }
        case CLI_MPLS_LDP_TARGETED_HELLO:
        {
            if (pu4Args[1] != NULL)
            {
                u4InTnlId = *pu4Args[1];
            }
            if (pu4Args[2] != NULL)
            {
                u4OutTnlId = *pu4Args[2];
            }
            /*Check & get IPV4 or IPV6 Next Hop Address */
            MplsGetIpv4Ipv6AddrfromCliStr (CliHandle, (CONST CHR1 *)
                                           pu4Args[0], &CLIAddr);
            i4RetStatus = LdpCliTargetedHello (CliHandle,
                                               LDP_SNMP_TRUE,
                                               CLIAddr, u4InTnlId, u4OutTnlId);
            break;
        }
        case CLI_MPLS_LDP_NO_TARGETED_HELLO:
        {
            /*Check & get IPV4 or IPV6 Next Hop Address */

            MplsGetIpv4Ipv6AddrfromCliStr (CliHandle, (CONST CHR1 *)
                                           pu4Args[0], &CLIAddr);
            i4RetStatus = LdpCliTargetedHello (CliHandle,
                                               LDP_SNMP_FALSE,
                                               CLIAddr, LDP_ZERO, LDP_ZERO);
            break;
        }
        case CLI_MPLS_LDP_LABEL_DIST_METHOD:
        {
            i4LabelDistMode = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus = LdpCliLabelDistributionMode (CliHandle,
                                                       i4LabelDistMode);
            break;
        }
        case CLI_MPLS_LDP_NO_LABEL_DIST_METHOD:
        {
            i4RetStatus = LdpCliLabelDistributionMode (CliHandle,
                                                       LDP_DSTR_ON_DEMAND);
            break;
        }
        case CLI_MPLS_LDP_LABEL_RETENTION_MODE:
        {
            i4LabelRetMode = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus = LdpCliLabelRetentionMode (CliHandle, i4LabelRetMode);
            break;
        }
        case CLI_MPLS_LDP_NO_LABEL_RETENTION_MODE:
        {
            i4RetStatus = LdpCliLabelRetentionMode (CliHandle,
                                                    LDP_DEF_LBL_RET_MODE);
            break;
        }
        case CLI_MPLS_LDP_TRANSPORT_ADDRESS:
        {
            /* pu4Args[0] - Transport Address If Type 
             * pu4Args[1] - Transport Address If Index
             * pu4Args[2] - Command Type - Normal or No form
             */
            i4RetStatus = LdpCliTransportAddress (CliHandle,
                                                  (CLI_PTR_TO_U4
                                                   (pu4Args[0])),
                                                  (CLI_PTR_TO_U4
                                                   (pu4Args[1])),
                                                  (CLI_PTR_TO_I4 (pu4Args[2])));
            break;
        }
#ifdef MPLS_IPV6_WANTED
        case CLI_MPLS_LDP_IPV6_TRANSPORT_ADDRESS:
        {
            /* pu4Args[0] - Transport Address If Type
             * pu4Args[1] - Transport Address If Index
             * pu4Args[2] - Command Type - Normal or No form
             */
            i4RetStatus = LdpCliIpv6TransportAddress (CliHandle,
                                                      (CLI_PTR_TO_U4
                                                       (pu4Args[0])),
                                                      (CLI_PTR_TO_U4
                                                       (pu4Args[1])),
                                                      (CLI_PTR_TO_I4
                                                       (pu4Args[2])));
            break;
        }
#endif
        case CLI_MPLS_LDP_LABEL_ALLOCATION_MODE:
        {
            i4LabelAllocMode = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus = LdpCliLabelAllocationMode (CliHandle,
                                                     i4LabelAllocMode);
            break;
        }
        case CLI_MPLS_LDP_NO_LABEL_ALLOCATION_MODE:
        {
            i4RetStatus = LdpCliLabelAllocationMode (CliHandle,
                                                     LDP_ORDERED_MODE);
            break;
        }
        case CLI_MPLS_LDP_LSR_ID:
        {
            /* pu4Args[0] - IfType 
             * pu4Args[1] - If Index if IfType is valid 
             *              or Ip Address if IfType is CFA_NONE
             * pu4Args[2] - LDP Force Option 
             * pu4Args[3] - Command Type - Normal or No form
             */
            if (pu4Args[0] == CFA_NONE)
            {
                i4RetStatus = LdpCliLsrId (CliHandle,
                                           (CLI_PTR_TO_U4 (pu4Args[0])),
                                           (*(pu4Args[1])),
                                           (CLI_PTR_TO_I4 (pu4Args[2])),
                                           (CLI_PTR_TO_I4 (pu4Args[3])));

            }
            else
            {
                i4RetStatus = LdpCliLsrId (CliHandle,
                                           (CLI_PTR_TO_U4 (pu4Args[0])),
                                           (CLI_PTR_TO_U4 (pu4Args[1])),
                                           (CLI_PTR_TO_I4 (pu4Args[2])),
                                           (CLI_PTR_TO_I4 (pu4Args[3])));

            }

            break;
        }
        case CLI_MPLS_LDP_PHP_MODE:
        {
            i4PhpMode = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus = LdpCliPhpMode (CliHandle, i4PhpMode);
            break;
        }
        case CLI_MPLS_LDP_NO_PHP_MODE:
        {
            i4RetStatus = LdpCliPhpMode (CliHandle, MPLS_PHP_DISABLED);
            break;
        }
        case CLI_MPLS_LDP_INTERFACE_ENABLE:
        {

            i4RetStatus = LdpCliEnableMplsLdpInterface (CliHandle,
                                                        CLI_PTR_TO_U4
                                                        (pu4Args[0]),
                                                        CLI_PTR_TO_U4
                                                        (pu4Args[1]),
                                                        CLI_PTR_TO_U4
                                                        (pu4Args[2]));
            break;
        }
        case CLI_MPLS_LDP_INTERFACE_DISABLE:
        {
            i4RetStatus = LdpCliDisableMplsLdpInterface (CliHandle,
                                                         CLI_PTR_TO_U4
                                                         (pu4Args[0]),
                                                         CLI_PTR_TO_U4
                                                         (pu4Args[1]));
            break;
        }
        case CLI_MPLS_LDP_LABEL_RANGE_SHOW:
        {
            i4RetStatus = LdpCliShowLabelRange (CliHandle);
            break;
        }
        case CLI_MPLS_LDP_DISCOVERY_SHOW:
        {
            u4Flag = CLI_PTR_TO_U4 (pu4Args[0]);
            i4RetStatus = LdpCliShowDiscovery (CliHandle, u4Flag);
            break;
        }
        case CLI_MPLS_LDP_NEIGHBOR_SHOW:
        {
            u4Flag = CLI_PTR_TO_U4 (pu4Args[0]);
            if (pu4Args[1] != NULL)
            {
                u4IpAddr = *pu4Args[1];
            }
            i4RetStatus = LdpCliShowNeighbor (CliHandle,
                                              u4Flag,
                                              u4IpAddr, CLI_PTR_TO_U4
                                              (pu4Args[2]));
            break;
        }
        case CLI_MPLS_LDP_DATABASE_SHOW:
        {
            i4RetStatus = LdpCliShowDatabase (CliHandle);
            break;
        }
        case CLI_MPLS_LDP_PARAM_SHOW:
        {
            i4RetStatus = LdpCliShowParameters (CliHandle);
            break;
        }
        case CLI_MPLS_LDP_ENTITY_SHUT:
        {
            i4RetStatus = LdpCliMplsLdpEntityChangeRowStatus (CliHandle,
                                                              NOT_IN_SERVICE);
            break;
        }
        case CLI_MPLS_LDP_ENTITY_NO_SHUT:
        {
            i4RetStatus = LdpCliMplsLdpEntityChangeRowStatus (CliHandle,
                                                              ACTIVE);
            break;
        }
        case CLI_MPLS_LDP_DBG_ADVT:
        {
            LdpCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            i4Dbg = (CLI_PTR_TO_I4 (pu4Args[0]) == CLI_ENABLE) ?
                (LDP_DBG_FLAG |= LDP_ADVT_ALL) :
                (LDP_DBG_FLAG &= ~(LDP_ADVT_ALL));
            nmhSetFsMplsCrlspDebugLevel (i4Dbg);
            break;
        }
        case CLI_MPLS_LDP_DBG_INTERFACE:
        {
            LdpCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            i4Dbg = (CLI_PTR_TO_I4 (pu4Args[0]) == CLI_ENABLE) ?
                (LDP_DBG_FLAG |= LDP_IF_ALL) : (LDP_DBG_FLAG &= ~(LDP_IF_ALL));
            nmhSetFsMplsCrlspDebugLevel (i4Dbg);
            break;
        }
        case CLI_MPLS_LDP_DBG_MEM:
        {
            LdpCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            i4Dbg = (CLI_PTR_TO_I4 (pu4Args[0]) == CLI_ENABLE) ?
                (LDP_DBG_FLAG |= (LDP_MAIN_MEM | LDP_IF_MEM |
                                  LDP_ADVT_MEM | LDP_SSN_MEM |
                                  LDP_PRCS_MEM | LDP_NOTIF_MEM) |
                 (LDP_ADVT_SEM | LDP_SSN_SEM |
                  LDP_PRCS_SEM)) :
                (LDP_DBG_FLAG &= ~(LDP_DBG_MEM | LDP_DBG_SEM));
            nmhSetFsMplsCrlspDebugLevel (i4Dbg);
            break;
        }
        case CLI_MPLS_LDP_DBG_PRCS:
        {
            LdpCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            i4Dbg = (CLI_PTR_TO_I4 (pu4Args[0]) == CLI_ENABLE) ?
                (LDP_DBG_FLAG |= LDP_MAIN_ALL) :
                (LDP_DBG_FLAG &= ~(LDP_MAIN_ALL));
            nmhSetFsMplsCrlspDebugLevel (i4Dbg);
            break;
        }
        case CLI_MPLS_LDP_DBG_SESSION:
        {
            LdpCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            i4Dbg = (CLI_PTR_TO_I4 (pu4Args[0]) == CLI_ENABLE) ?
                (LDP_DBG_FLAG |= LDP_SSN_ALL) :
                (LDP_DBG_FLAG &= ~(LDP_SSN_ALL));
            nmhSetFsMplsCrlspDebugLevel (i4Dbg);
            break;
        }
        case CLI_MPLS_LDP_DBG_TIMER:
        {
            LdpCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            i4Dbg = (CLI_PTR_TO_I4 (pu4Args[0]) == CLI_ENABLE) ?
                (LDP_DBG_FLAG |= (LDP_IF_TMR | LDP_ADVT_TMR | LDP_SSN_TMR |
                                  LDP_NOTIF_TMR)) :
                (LDP_DBG_FLAG &= ~(LDP_DBG_TIMER));
            nmhSetFsMplsCrlspDebugLevel (i4Dbg);
            break;
        }
        case CLI_MPLS_LDP_DBG_GRACEFUL_RESTART:
        {
            LdpCliGetDebugLevelVal (CliHandle, pu4Args[1]);

            i4Dbg = (CLI_PTR_TO_I4 (pu4Args[0]) == CLI_ENABLE) ?
                (LDP_DBG_FLAG |= GRACEFUL_DEBUG) :
                (LDP_DBG_FLAG &= ~(GRACEFUL_DEBUG));
            nmhSetFsMplsCrlspDebugLevel (i4Dbg);
            break;
        }

        case CLI_MPLS_LDP_DBG_MESSAGE:
        {
            LdpCliGetDebugLevelVal (CliHandle, pu4Args[2]);

            if (CLI_PTR_TO_U4 (pu4Args[1]) == CLI_MPLS_LDP_ONE)
            {
                i4Dbg = (CLI_PTR_TO_I4 (pu4Args[0]) == CLI_ENABLE) ?
                    (LDP_DBG_FLAG |= (LDP_IF_TX | LDP_ADVT_TX | LDP_SSN_TX
                                      | LDP_PRCS_TX | LDP_NOTIF_TX)) :
                    (LDP_DBG_FLAG &= ~(LDP_DBG_TX));
            }
            else
            {
                i4Dbg = (CLI_PTR_TO_I4 (pu4Args[0]) == CLI_ENABLE) ?
                    (LDP_DBG_FLAG |= (LDP_IF_RX | LDP_ADVT_RX | LDP_SSN_RX
                                      | LDP_PRCS_RX | LDP_NOTIF_RX)) :
                    (LDP_DBG_FLAG &= ~(LDP_DBG_RX));
            }
            nmhSetFsMplsCrlspDebugLevel (i4Dbg);
            break;
        }
        case CLI_MPLS_LDP_DBG_DUMP_MSGS:
        {
            LdpCliGetDebugLevelVal (CliHandle, pu4Args[3]);

            i4Dbg = CLI_PTR_TO_I4 (pu4Args[0]);
            if (i4Dbg == CLI_ENABLE)
            {
                i4RetStatus = LdpCliDbgSetMsgDumpAttr (CliHandle,
                                                       CLI_PTR_TO_I4
                                                       (pu4Args[1]),
                                                       CLI_PTR_TO_I4
                                                       (pu4Args[2]));

                nmhGetFsMplsCrlspDebugLevel (&i4DbgFlag);
                i4DbgFlag |= LDP_PDU_DUMP;

            }
            else
            {
                i4RetStatus = LdpCliDbgResetMsgDumpAttr (CliHandle,
                                                         CLI_PTR_TO_I4
                                                         (pu4Args[1]),
                                                         CLI_PTR_TO_I4
                                                         (pu4Args[2]));

                nmhGetFsMplsCrlspDebugLevel (&i4DbgFlag);
                i4DbgFlag &= ~LDP_PDU_DUMP;

            }
            nmhSetFsMplsCrlspDebugLevel (i4DbgFlag);
            break;
        }
        case CLI_MPLS_LDP_POLICY:
        {
            LdpCliMplsLdpPolicy (CliHandle, CLI_PTR_TO_U4 (pu4Args[0]));
            break;
        }
        case CLI_MPLS_LDP_GR_MODE:
            /*Intentional Fall through */
        case CLI_MPLS_LDP_GR_NO_MODE:
        {
            i4GrCapability = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus = LdpCliSetGrCapability (CliHandle, i4GrCapability);
            break;
        }
        case CLI_MPLS_LDP_NBR_LIVENESS_TIMER:
            /*Intentional Fall through */
        case CLI_MPLS_LDP_NO_NBR_LIVENESS_TIMER:
        {
            i4NbrTime = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus = LdpCliSetNbrLivenessTime (CliHandle, i4NbrTime);
            break;
        }

        case CLI_MPLS_LDP_FWD_HOLDING_TIMER:
            /*Intentional Fall through */
        case CLI_MPLS_LDP_NO_FWD_HOLDING_TIMER:
        {
            i4FwdHoldTime = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus = LdpCliSetFwdHoldTime (CliHandle, i4FwdHoldTime);
            break;
        }
        case CLI_MPLS_LDP_RECOVERY_TIMER:
            /*Intentional Fall through */
        case CLI_MPLS_LDP_NO_RECOVERY_TIMER:
        {
            i4RecoveryTime = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus = LdpCliSetMaxRecoveryTime (CliHandle, i4RecoveryTime);
            break;
        }

        case CLI_MPLS_LDP_GR_SHOW:
        {
            LdpCliDisplayGrConfigurations (CliHandle);
            break;
        }

        case CLI_MPLS_CONFIG_SEQ_TLV:
        {
            i4ConfigSeqTlvOption = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus =
                LdpCliSetConfigSeqTLVOption (CliHandle, i4ConfigSeqTlvOption);
            break;
        }

        case CLI_MPLS_LDP_PATH_VECTOR:
            i4PathVectorLimit = CLI_PTR_TO_I4 (pu4Args[0]);
            i4RetStatus =
                LdpCliSetPathVectorLimit (CliHandle, i4PathVectorLimit);
            break;

#ifdef MPLS_LDP_BFD_WANTED

        case CLI_MPLS_LDP_BFD_STATUS:
            i4BfdStatus = CLI_PTR_TO_I4 (pu4Args[1]);
            i4RetStatus = LdpCliBfdStatus (CliHandle, i4BfdStatus);
            break;
#endif
        default:
        {
            CliPrintf (CliHandle, "\r %%No such Command\n");
            i4RetStatus = CLI_FAILURE;
            break;
        }
    }
    if ((i4RetStatus == CLI_FAILURE) &&
        (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_MPLS_LDP)
            && (u4ErrCode < LDP_CLI_MAX_ERROR_MSGS))
        {
            CliPrintf (CliHandle, "%s",
                       LDP_MPLS_CLI_ERROR_MSGS[CLI_ERR_OFFSET_MPLS_LDP
                                               (u4ErrCode)]);
            CLI_SET_CMD_STATUS (CLI_FAILURE);
            u4ErrCode = 0;
        }
        CLI_SET_ERR (0);
    }

    LdpUnLock ();
    CliUnRegisterLock (CliHandle);
    return i4RetStatus;
}

/******************************************************************************
 *  Function Name :  LdpCliMplsLdpPolicy
 *  Description   :  This routine configures the policy to distribute 
                     only the connected routes or both connected/non 
                     connected routes.
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   u1Policy - Enable/Disable
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PRIVATE VOID
LdpCliMplsLdpPolicy (tCliHandle CliHandle, UINT4 u4Policy)
{

    UNUSED_PARAM (CliHandle);

    gLdpInfo.u1Policy = (UINT1) u4Policy;

}

/******************************************************************************
 *  Function Name :  LdpCliCreateEntity
 *  Description   :  This routine calls nmh to create LDP entity
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   u4EntityIndex - Entity Index
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliCreateEntity (tCliHandle CliHandle, UINT4 u4EntityIndex)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpId              LdpEntityId;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT2               u2IncarnId = MPLS_DEF_INCARN;

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    MEMCPY (LdpEntityId, MplsLdpEntityLdpLsrId.pu1_OctetList,
            LDP_MAX_LDPID_LEN);

    /* Check the entity index range */
    if ((u4EntityIndex >
         FsLDPSizingParams[MAX_LDP_ENTITIES_SIZING_ID].u4PreAllocatedUnits) ||
        (u4EntityIndex < LDP_MIN_OF_MAX_ENTITIES))
    {
        CliPrintf (CliHandle, "\r%% Entity range is %d - %d\n",
                   LDP_MIN_OF_MAX_ENTITIES,
                   FsLDPSizingParams[MAX_LDP_ENTITIES_SIZING_ID].
                   u4PreAllocatedUnits);
        return CLI_FAILURE;
    }

    /* Check whether the entity index is already allotted or not */
    if (LdpGetLdpEntity (u2IncarnId, LdpEntityId,
                         u4EntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        /* Create the ldp entity in create and wait mode */
        if (nmhSetMplsLdpEntityRowStatus (&MplsLdpEntityLdpLsrId,
                                          u4EntityIndex,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to create LDP Entity\n");
            return CLI_FAILURE;
        }

        /* Set the Label Type as Generic */
        if (nmhSetMplsLdpEntityLabelType (&MplsLdpEntityLdpLsrId,
                                          u4EntityIndex,
                                          LDP_GEN_LABEL) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set the label type\n");
            LdpCliDeleteEntity (CliHandle, u4EntityIndex);
            return CLI_FAILURE;
        }
    }

    /* Entity already up, so only changing the mode  */
    if (LdpCliMplsLdpChangeMode (CLI_MPLS_LDP_ENTITY,
                                 u4EntityIndex) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to enter mpls ldp entity mode\n");
        LdpCliDeleteEntity (CliHandle, u4EntityIndex);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliDeleteEntity
 *  Description   :  This routine calls nmh to delete a LDP entity
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   u4EntityIndex - EntityIndex
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
LdpCliDeleteEntity (tCliHandle CliHandle, UINT4 u4EntityIndex)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    tLdpEntity         *pLdpEntity = NULL;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT2               u2IncarnId = MPLS_DEF_INCARN;
    UINT1              *pu1LdpEntityId = NULL;

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    pu1LdpEntityId = MplsLdpEntityLdpLsrId.pu1_OctetList;

    /* Check the entity index range */
    if ((u4EntityIndex >
         FsLDPSizingParams[MAX_LDP_ENTITIES_SIZING_ID].u4PreAllocatedUnits) ||
        (u4EntityIndex < LDP_MIN_OF_MAX_ENTITIES))
    {
        CliPrintf (CliHandle, "\r%% Entity range is %d - %d\n",
                   LDP_MIN_OF_MAX_ENTITIES,
                   FsLDPSizingParams[MAX_LDP_ENTITIES_SIZING_ID].
                   u4PreAllocatedUnits);
        return CLI_FAILURE;
    }

    /* Check whether the entity exists or not */
    if (LdpGetLdpEntity (u2IncarnId, pu1LdpEntityId, u4EntityIndex,
                         &pLdpEntity) == LDP_FAILURE)
    {
        /* Entity does not exist */
        CliPrintf (CliHandle, "\r%%Entity does not exist\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2MplsLdpEntityRowStatus (&u4ErrorCode, &MplsLdpEntityLdpLsrId,
                                         u4EntityIndex,
                                         DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Remove the L2VPN association if any.\n");
        return CLI_FAILURE;
    }

    /* Destroy the entity */
    if (nmhSetMplsLdpEntityRowStatus (&MplsLdpEntityLdpLsrId,
                                      u4EntityIndex, DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to destroy LDP entity\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliHelloHoldTime
 *  Description   :  This routine calls nmh to set or reset the Hello 
 *                   hold time for Basic hello discovery process
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   u4HelloHoldTime - Basic Hello Discovery Hold time
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliHelloHoldTime (tCliHandle CliHandle, UINT4 u4HelloHoldTime)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT4               u4IpAddr = LDP_ZERO;

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the Entity index and LDP Id */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();
    MPLS_OCTETSTRING_TO_INTEGER ((&MplsLdpEntityLdpLsrId), u4IpAddr);

    /* Test the hello timer given */
    if (nmhTestv2MplsLdpEntityHelloHoldTimer (&u4ErrorCode,
                                              &MplsLdpEntityLdpLsrId,
                                              u4MplsLdpEntityIndex,
                                              u4HelloHoldTime) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Wrong Hello Hold time value"
                   " Range is 0- %d\n", LDP_HELLO_MAX_HTIME);
        return CLI_FAILURE;
    }

    /* Set the hello hold time */
    if (nmhSetMplsLdpEntityHelloHoldTimer (&MplsLdpEntityLdpLsrId,
                                           u4MplsLdpEntityIndex,
                                           u4HelloHoldTime) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set hello hold time\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliKeepAliveHoldTime
 *  Description   :  This routine calls nmh to set or reset the Keep alive 
 *                   hold time
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   u4KeepAliveHoldTime - Keep Alive Hold time
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliKeepAliveHoldTime (tCliHandle CliHandle, UINT4 u4KeepAliveHoldTime)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4ErrorCode = LDP_ZERO;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the Entity Index and LDP LSR ID */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();

    /* Test the keep alive hold timer given */
    if (nmhTestv2MplsLdpEntityKeepAliveHoldTimer (&u4ErrorCode,
                                                  &MplsLdpEntityLdpLsrId,
                                                  u4MplsLdpEntityIndex,
                                                  u4KeepAliveHoldTime)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Wrong Keep alive Hold time value"
                   " Range is 1 - %d\n", LDP_KALIVE_MAX_HTIME);
        return CLI_FAILURE;
    }

    /* Set the keep alive hold timer given */
    if (nmhSetMplsLdpEntityKeepAliveHoldTimer (&MplsLdpEntityLdpLsrId,
                                               u4MplsLdpEntityIndex,
                                               u4KeepAliveHoldTime)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set the keep alive hold time\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  LdpCliGetDebugLevelVal
* Description :
* Input       :  CliHandle, pu4ArgsOne 
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
LdpCliGetDebugLevelVal (tCliHandle CliHandle, UINT4 *pu4ArgsOne)
{
    INT4                i4Args = 0;

    if (pu4ArgsOne != NULL)
    {
        i4Args = CLI_PTR_TO_I4 (pu4ArgsOne);
        if (i4Args < DEBUG_DEF_LVL_FLAG)
        {
            LdpCliSetDebugLevel (CliHandle, i4Args);
        }
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  LdpCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
LdpCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    LDP_LVL_FLAG = 0;

    UNUSED_PARAM (CliHandle);

    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        LDP_LVL_FLAG = MGMT_TRC | BUFFER_TRC | INIT_SHUT_TRC | ALL_FAILURE_TRC |
            OS_RESOURCE_TRC | CONTROL_PLANE_TRC | DATA_PATH_TRC | DUMP_TRC;

    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        LDP_LVL_FLAG = CONTROL_PLANE_TRC | DATA_PATH_TRC | DUMP_TRC
            | INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        LDP_LVL_FLAG = INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        LDP_LVL_FLAG = INIT_SHUT_TRC | ALL_FAILURE_TRC | OS_RESOURCE_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        LDP_LVL_FLAG = INIT_SHUT_TRC | ALL_FAILURE_TRC;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        LDP_LVL_FLAG = INIT_SHUT_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliMaxHops 
 *  Description   :  This routine calls nmh to set or reset maximum no. of hops
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   u4MaxHops - Maximum No. of Hops
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliMaxHops (tCliHandle CliHandle, UINT4 u4MaxHops)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4ErrorCode = LDP_ZERO;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the Entity Index and LDP LSR ID */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();

    /* Test the max hops given */
    if (nmhTestv2MplsLdpEntityHopCountLimit (&u4ErrorCode,
                                             &MplsLdpEntityLdpLsrId,
                                             u4MplsLdpEntityIndex,
                                             u4MaxHops) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set the Max Hop value."
                   "Make sure Entity is shutdown or Hop Count value is within range.\n");
        return CLI_FAILURE;
    }

    /* Set the max hops given */
    if (nmhSetMplsLdpEntityHopCountLimit (&MplsLdpEntityLdpLsrId,
                                          u4MplsLdpEntityIndex,
                                          u4MaxHops) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set the Max Hop value."
                   "Make sure Entity is shutdown or Hop Count value is within range.\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliTargetedHello
 *  Description   :  This routine calls nmh to set or reset hello hold time for 
 *                   targeted discovery.
 *  Input(s)      :  CliHandle - Handle to CLI.
 *                   i4TargetedHelloFlag - Set or reset flag
 *                   u4PeerAddr - Peer Ip address
 *                   u4TnlId - Tunnel id for LDP targeted session
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliTargetedHello (tCliHandle CliHandle, INT4 i4TargetPeerFlag,
                     tGenU4Addr inPeerAddr, UINT4 u4InTnlId, UINT4 u4OutTnlId)
{
    tSNMP_OCTET_STRING_TYPE LdpEntityLdpId;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE GetPeerAddr;
    static UINT1        au1PeerAddr[LDP_LSR_ID_LEN] = { LDP_ZERO };
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1GetPeerAddr[IPV6_ADDR_LENGTH] = { LDP_ZERO };
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4GetPeerAddr = LDP_ZERO;
    INT4                i4TargetPeerAddrType = LDP_ZERO;
    INT4                i4LdpOverRsvpFlag = LDP_SNMP_FALSE;
    UINT4               u4OutTunnelIndex = 0;
    UINT4               u4OutTunnelInstance = 0;
    UINT4               u4InTunnelInstance = 0;
    UINT4               u4OutTnlIngressId = 0;
    UINT4               u4OutTnlEgressId = 0;
    UINT4               u4InTnlIngressId = 0;
    UINT4               u4InTnlEgressId = 0;
    UINT4               u4InLabel = 0;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpId              LdpEntityId;
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;

#ifdef MPLS_IPV6_WANTED
    INT4                i4CmpRet = 0;
    /*  UINT1      u1Scope = 0; */

    tSNMP_OCTET_STRING_TYPE Ipv6PeerAddr;
    tSNMP_OCTET_STRING_TYPE GetIpv6PeerAddr;

    static UINT1        au1Ipv6PeerAddr[LDP_IPV6ADR_LEN] = { LDP_ZERO };
    static UINT1        au1GetIpv6PeerAddr[LDP_IPV6ADR_LEN] = { LDP_ZERO };

    Ipv6PeerAddr.pu1_OctetList = au1Ipv6PeerAddr;

    GetIpv6PeerAddr.pu1_OctetList = au1GetIpv6PeerAddr;
    GetIpv6PeerAddr.i4_Length = LDP_IPV6ADR_LEN;
#endif
    LdpEntityLdpId.pu1_OctetList = au1LdpLsrId;
    GetPeerAddr.pu1_OctetList = au1GetPeerAddr;
    GetPeerAddr.i4_Length = LDP_LSR_ID_LEN;
#ifdef MPLS_IPV6_WANTED
    if (inPeerAddr.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        /* Check whether peer address is self address */
        if (LdpIpv6IsAddrLocal (&(inPeerAddr.Addr.Ip6Addr)) == LDP_TRUE)
        {
            CliPrintf (CliHandle,
                       "\r%%Peer ipv6 address is same as self-ip address\n");
            return CLI_FAILURE;
        }

        MEMCPY (Ipv6PeerAddr.pu1_OctetList,
                LDP_IPV6_U4_ADDR (inPeerAddr.Addr), LDP_IPV6ADR_LEN);

        Ipv6PeerAddr.i4_Length = LDP_IPV6ADR_LEN;

        i4TargetPeerAddrType = MPLS_IPV6_ADDR_TYPE;
    }
    else
    {
#endif
        /* Check whether peer address is self address */
        if (NetIpv4IfIsOurAddress (LDP_IPV4_U4_ADDR (inPeerAddr.Addr))
            == NETIPV4_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r%%Peer ipv4 address is same as self-ip address\n");
            return CLI_FAILURE;
        }

        /* Convert the peer address from integer to octet string for IPV4 */
        PeerAddr.pu1_OctetList = au1PeerAddr;
        PeerAddr.i4_Length = LDP_LSR_ID_LEN;

        i4TargetPeerAddrType = MPLS_IPV4_ADDR_TYPE;

        MPLS_INTEGER_TO_OCTETSTRING (LDP_IPV4_U4_ADDR (inPeerAddr.Addr),
                                     (&PeerAddr));
#ifdef MPLS_IPV6_WANTED
    }
#endif

    /* Get Mpls Ldp Entity Index and Ldp Lsr Id */
    nmhGetMplsLdpLsrId (&LdpEntityLdpId);
    LdpEntityLdpId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = CLI_GET_MPLS_LDP_ENTITY_MODE ();

    MEMCPY (LdpEntityId, LdpEntityLdpId.pu1_OctetList, LDP_MAX_LDPID_LEN);
    if (LdpGetLdpEntity (MPLS_DEF_INCARN, LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        /* Entity does not exist */
        CliPrintf (CliHandle, "\r%%Entity does not exist\n");
        return CLI_FAILURE;
    }

    /* For 'no neighbor' command, first check whether any neighbor exists
     * Second, check whether existing neighbor matches the given neighbor to be 
     * deleted                                                                */
    if (i4TargetPeerFlag == LDP_SNMP_FALSE)
    {
#ifdef MPLS_IPV6_WANTED
        if (inPeerAddr.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            if (nmhGetMplsLdpEntityTargetPeerAddr (&LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   &GetIpv6PeerAddr) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to get the Target Peer Address\n");
                return CLI_FAILURE;
            }

            if (GetIpv6PeerAddr.i4_Length == LDP_ZERO)
            {
                CliPrintf (CliHandle, "\r%%No Such Ipv6 Peer Exists\n");
                return CLI_FAILURE;
            }

            i4CmpRet =
                MEMCMP (GetIpv6PeerAddr.pu1_OctetList,
                        LDP_IPV6_U4_ADDR (inPeerAddr.Addr), LDP_IPV6ADR_LEN);

            if (i4CmpRet != 0)
            {
                CliPrintf (CliHandle, "\r%%No Such Ipv6 Peer Exists\n");
                return CLI_FAILURE;
            }
        }
        else
        {
#endif
            if (nmhGetMplsLdpEntityTargetPeerAddr (&LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   &GetPeerAddr) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to get Ipv4 Target Peer Address\n");
                return CLI_FAILURE;
            }
            if (GetPeerAddr.i4_Length == LDP_ZERO)
            {
                CliPrintf (CliHandle, "\r%%No Such Ipv4 Peer Exists\n");
                return CLI_FAILURE;
            }

            MPLS_OCTETSTRING_TO_INTEGER ((&GetPeerAddr), u4GetPeerAddr);

            if (u4GetPeerAddr != LDP_IPV4_U4_ADDR (inPeerAddr.Addr))
            {
                CliPrintf (CliHandle, "\r%%No Such Ipv4 Peer Exists\n");
                return CLI_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }                            /*end forever */

    /* Check for the Target Peer is configured with correct lables */
    if (nmhTestv2MplsLdpEntityTargetPeer (&u4ErrorCode,
                                          &LdpEntityLdpId,
                                          u4MplsLdpEntityIndex,
                                          i4TargetPeerFlag) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Label Range should be "
                   "%d - %d for Targeted Hello Or Entity not in DOWN State\n",
                   gSystemSize.MplsSystemSize.u4MinL2VpnLblRange,
                   gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange);
        return CLI_FAILURE;
    }

    /* Set the Target Peer Flag */
    if (nmhSetMplsLdpEntityTargetPeer (&LdpEntityLdpId,
                                       u4MplsLdpEntityIndex,
                                       i4TargetPeerFlag) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set target peer flag\n");
        return CLI_FAILURE;
    }

    if ((u4InTnlId != LDP_ZERO) || (u4OutTnlId != LDP_ZERO))
    {
        i4LdpOverRsvpFlag = LDP_SNMP_TRUE;
    }

    if (nmhTestv2FsMplsLdpEntityLdpOverRsvpEnable (&u4ErrorCode,
                                                   &LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   i4LdpOverRsvpFlag)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Cannot set LDP over RSVP Flag\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityLdpOverRsvpEnable (&LdpEntityLdpId,
                                                u4MplsLdpEntityIndex,
                                                i4LdpOverRsvpFlag)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set LDP Over RSVP Flag\n");
        return CLI_FAILURE;
    }

    if (i4TargetPeerFlag == LDP_SNMP_TRUE)
    {
        /* Set the Target Peer Address Type */
        if (nmhSetMplsLdpEntityTargetPeerAddrType (&LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   i4TargetPeerAddrType)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to Set target Peer Addr Type\n");
            return CLI_FAILURE;
        }
#ifdef MPLS_IPV6_WANTED
        if (inPeerAddr.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            /* Validate the octet string of Ipv6 Peer Address */
            if (nmhTestv2MplsLdpEntityTargetPeerAddr (&u4ErrorCode,
                                                      &LdpEntityLdpId,
                                                      u4MplsLdpEntityIndex,
                                                      &Ipv6PeerAddr) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to set Ipv6 target peer addr\n");
                return CLI_FAILURE;
            }
            /* Set the Ipv6 Target Peer Addr */
            if (nmhSetMplsLdpEntityTargetPeerAddr (&LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   &Ipv6PeerAddr) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to set Ipv6 target peer addr\n");
                return CLI_FAILURE;
            }
        }
        else
        {
#endif
            /* Validate the octet string of Ipv4 Peer Ip Address */
            if (nmhTestv2MplsLdpEntityTargetPeerAddr (&u4ErrorCode,
                                                      &LdpEntityLdpId,
                                                      u4MplsLdpEntityIndex,
                                                      &PeerAddr) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* Set the Ipv4 Target Peer Addr */
            if (nmhSetMplsLdpEntityTargetPeerAddr (&LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   &PeerAddr) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to set Ipv4 target peer addr\n");
                return CLI_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }
    else
    {
        PeerAddr.i4_Length = LDP_ZERO;
#ifdef MPLS_IPV6_WANTED
        if (inPeerAddr.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            Ipv6PeerAddr.i4_Length = LDP_IPV6ADR_LEN;

            MEMSET (Ipv6PeerAddr.pu1_OctetList, LDP_ZERO, LDP_IPV6ADR_LEN);

            /* Set the Target Peer Addr */
            if (nmhSetMplsLdpEntityTargetPeerAddr (&LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   &Ipv6PeerAddr) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%%Unable to set Ipv6 target peer addr\n");
                return CLI_FAILURE;
            }
        }
        else
        {
#endif
            PeerAddr.i4_Length = LDP_ZERO;

            MPLS_INTEGER_TO_OCTETSTRING (LDP_ZERO, (&PeerAddr));

            /* Set the Target Peer Addr */
            if (nmhSetMplsLdpEntityTargetPeerAddr (&LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   &PeerAddr) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%%Unable to set target peer addr\n");
                return CLI_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }

    if (u4InTnlId != LDP_ZERO)
    {
        MPLS_CMN_LOCK ();

        pTeTnlInfo = TeGetLatestTunnelInfoFromTunnelId (u4InTnlId,
                                                        LDP_ZERO, TE_EGRESS);

        if (pTeTnlInfo != NULL)
        {
            u4InTunnelInstance = TE_TNL_TNL_INSTANCE (pTeTnlInfo);
            MEMCPY ((UINT1 *) (&u4InTnlIngressId),
                    TE_TNL_INGRESS_LSRID (pTeTnlInfo), ROUTER_ID_LENGTH);
            u4InTnlIngressId = OSIX_HTONL (u4InTnlIngressId);
            MEMCPY ((UINT1 *) (&u4InTnlEgressId),
                    TE_TNL_EGRESS_LSRID (pTeTnlInfo), ROUTER_ID_LENGTH);
            u4InTnlEgressId = OSIX_HTONL (u4InTnlEgressId);
            TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo) = u4MplsLdpEntityIndex;

            /* Incoming tunnel label is stored here for ILM delete
             * when we don't have the in tunnel */
            pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                  MPLS_DEF_DIRECTION);
            if ((pXcEntry != NULL) && (pXcEntry->pInIndex != NULL))
            {
                pInSegment = pXcEntry->pInIndex;
                u4InLabel = pInSegment->u4Label;
            }
            pLdpEntity->u4InTnlLabel = u4InLabel;
        }
        MPLS_CMN_UNLOCK ();
    }

    /* No Need for all the validations done for Out Tunnel Info, since
     * Incoming Tunnel may not exist at this point of time. So, just
     * associate the value passed in InStackTnlInfo. */
    if (nmhTestv2FsMplsLdpEntityInTunnelIndex (&u4ErrorCode,
                                               &LdpEntityLdpId,
                                               u4MplsLdpEntityIndex,
                                               u4InTnlId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Cannot set In Tunnel Id for LDP over RSVP"
                   " Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityInTunnelIndex (&LdpEntityLdpId,
                                            u4MplsLdpEntityIndex,
                                            u4InTnlId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set In Tunnel Id for LDP over RSVP"
                   " Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsLdpEntityInTunnelInstance (&u4ErrorCode,
                                                  &LdpEntityLdpId,
                                                  u4MplsLdpEntityIndex,
                                                  u4InTunnelInstance)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Cannot set In Tunnel Instance for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityInTunnelInstance (&LdpEntityLdpId,
                                               u4MplsLdpEntityIndex,
                                               u4InTunnelInstance)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set In Tunnel Instance for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsLdpEntityInTunnelIngressLSRId (&u4ErrorCode,
                                                      &LdpEntityLdpId,
                                                      u4MplsLdpEntityIndex,
                                                      u4InTnlIngressId)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Cannot set In Tunnel Ingress for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityInTunnelIngressLSRId (&LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   u4InTnlIngressId)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set In Tunnel Ingress for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsLdpEntityInTunnelEgressLSRId (&u4ErrorCode,
                                                     &LdpEntityLdpId,
                                                     u4MplsLdpEntityIndex,
                                                     u4InTnlEgressId)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Cannot set In Tunnel Egress for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityInTunnelEgressLSRId (&LdpEntityLdpId,
                                                  u4MplsLdpEntityIndex,
                                                  u4InTnlEgressId) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set In Tunnel Egress for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }
    if (u4OutTnlId != LDP_ZERO)
    {
        /* We expect the out tunnel to be present before establishing
         * LDP over RSVP session. */
        MPLS_CMN_LOCK ();
        pTeTnlInfo = TeGetTnlInfoByOwnerAndRole (u4OutTnlId, 0, 0, 0,
                                                 TE_TNL_OWNER_SNMP, TE_INGRESS);
        if (pTeTnlInfo == NULL)
        {
            CliPrintf (CliHandle,
                       "\r%%Create the tunnel prior to associate with "
                       "the ldp targeted session \n");
            MPLS_CMN_UNLOCK ();
            return CLI_FAILURE;
        }
        u4OutTunnelIndex = TE_TNL_TNL_INDEX (pTeTnlInfo);
        u4OutTunnelInstance = TE_TNL_TNL_INSTANCE (pTeTnlInfo);
        MEMCPY ((UINT1 *) (&u4OutTnlIngressId),
                TE_TNL_INGRESS_LSRID (pTeTnlInfo), ROUTER_ID_LENGTH);
        u4OutTnlIngressId = OSIX_HTONL (u4OutTnlIngressId);
        MEMCPY ((UINT1 *) (&u4OutTnlEgressId),
                TE_TNL_EGRESS_LSRID (pTeTnlInfo), ROUTER_ID_LENGTH);
        u4OutTnlEgressId = OSIX_HTONL (u4OutTnlEgressId);
        if (inPeerAddr.LDP_IPV4_U4_ADDR (Addr) != u4OutTnlEgressId)
        {
            CliPrintf (CliHandle, "\r%%LDP targeted session and Tunnel "
                       "are conflict in destination address\n");
            MPLS_CMN_UNLOCK ();
            return CLI_FAILURE;
        }

        if (u4OutTunnelInstance == MPLS_ZERO)
        {
            u4OutTunnelInstance = pTeTnlInfo->u4TnlPrimaryInstance;
        }
        MPLS_CMN_UNLOCK ();
    }
    if (nmhTestv2FsMplsLdpEntityOutTunnelIndex (&u4ErrorCode,
                                                &LdpEntityLdpId,
                                                u4MplsLdpEntityIndex,
                                                u4OutTunnelIndex) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Cannot set Out Tunnel Index for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityOutTunnelIndex (&LdpEntityLdpId,
                                             u4MplsLdpEntityIndex,
                                             u4OutTunnelIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set Out Tunnel Index for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsLdpEntityOutTunnelInstance (&u4ErrorCode,
                                                   &LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   u4OutTunnelInstance)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Cannot set Out Tunnel Instance for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityOutTunnelInstance (&LdpEntityLdpId,
                                                u4MplsLdpEntityIndex,
                                                u4OutTunnelInstance)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set Out Tunnel Instance for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsLdpEntityOutTunnelIngressLSRId (&u4ErrorCode,
                                                       &LdpEntityLdpId,
                                                       u4MplsLdpEntityIndex,
                                                       u4OutTnlIngressId)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Cannot set Out Tunnel Ingress for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityOutTunnelIngressLSRId (&LdpEntityLdpId,
                                                    u4MplsLdpEntityIndex,
                                                    u4OutTnlIngressId)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set Out Tunnel Ingress for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsLdpEntityOutTunnelEgressLSRId (&u4ErrorCode,
                                                      &LdpEntityLdpId,
                                                      u4MplsLdpEntityIndex,
                                                      u4OutTnlEgressId)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Cannot set Out Tunnel Egress for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityOutTunnelEgressLSRId (&LdpEntityLdpId,
                                                   u4MplsLdpEntityIndex,
                                                   u4OutTnlEgressId) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set Out Tunnel Egress for "
                   "LDP Over RSVP Targeted Session\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliLabelDistributionMode 
 *  Description   :  This routine calls another routine to set the mode based 
 *                   on the flag passed
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   i4LabelDistMode - DOD or DU
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliLabelDistributionMode (tCliHandle CliHandle, INT4 i4LabelDistMode)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the Entity Index and LDP LSR ID */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();

    /* Test the flag */
    if (nmhTestv2MplsLdpEntityLabelDistMethod (&u4ErrorCode,
                                               &MplsLdpEntityLdpLsrId,
                                               u4MplsLdpEntityIndex,
                                               i4LabelDistMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Wrong Label distribution value\n");
        return CLI_FAILURE;
    }

    /* Set the dist mode */
    if (nmhSetMplsLdpEntityLabelDistMethod (&MplsLdpEntityLdpLsrId,
                                            u4MplsLdpEntityIndex,
                                            i4LabelDistMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set label distribution mode\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliLabelRetentionMode 
 *  Description   :  This routine calls another routine to set the mode based 
 *                   on the flag passed
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   i4LabelRetention Mode - Liberal or conservative
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliLabelRetentionMode (tCliHandle CliHandle, INT4 i4LblRetMode)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the entity index and LDP LSR ID */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();

    /* Test the retention mode flag */
    if (nmhTestv2MplsLdpEntityLabelRetentionMode (&u4ErrorCode,
                                                  &MplsLdpEntityLdpLsrId,
                                                  u4MplsLdpEntityIndex,
                                                  i4LblRetMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Wrong Label retention value\n");
        return CLI_FAILURE;
    }

    /* Set the retention mode */
    if (nmhSetMplsLdpEntityLabelRetentionMode (&MplsLdpEntityLdpLsrId,
                                               u4MplsLdpEntityIndex,
                                               i4LblRetMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set label retention mode\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliTransportAddress
 *  Description   :  This routine sets or resets the transport address TLV 
 *                   option in LDP Hello Option. It configures the type
 *                   of Transport Address to be carried in the Transport address
 *                   TLV in LDP Hello Message. It configures an Transport
 *                   address if the transport address type is loopback.
 *  Input(s)      :  CliHandle      - Handle to CLI
 *                   u4TransIfType  - Transport Address Type
 *                   u4IfIndex      - Transport Address If Index
 *                   i4CmdType      - i4CmdType
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
LdpCliTransportAddress (tCliHandle CliHandle, UINT4 u4TransIfType,
                        UINT4 u4IfIndex, INT4 i4CmdType)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT1               au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4TransportAddress = LDP_ZERO;
    UINT4               u4Port = LDP_ZERO;

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the entity index and LDP LSR ID */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();

    if (nmhTestv2FsMplsLdpEntityTransAddrTlvEnable (&u4ErrorCode,
                                                    &MplsLdpEntityLdpLsrId,
                                                    u4MplsLdpEntityIndex,
                                                    i4CmdType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Wrong Transport Address Tlv Option or "
                   " Make sure Entity is Shut down\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2MplsLdpEntityTransportAddrKind (&u4ErrorCode,
                                                 &MplsLdpEntityLdpLsrId,
                                                 u4MplsLdpEntityIndex,
                                                 u4TransIfType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Wrong Transport Address Kind or "
                   " Make sure Entity is Shut down\n");
        return CLI_FAILURE;
    }

    if ((i4CmdType == CLI_ENABLE) && (u4TransIfType == LOOPBACK_ADDRESS_TYPE))
    {
        if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4Port) == NETIPV4_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to get the Port from If"
                       " Index\n");
            return CLI_FAILURE;
        }

        if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to get IP Address from If"
                       " Index\n");
            return CLI_FAILURE;
        }

        u4TransportAddress = NetIpIfInfo.u4Addr;

        if (nmhTestv2FsMplsLdpEntityTransportAddress (&u4ErrorCode,
                                                      &MplsLdpEntityLdpLsrId,
                                                      u4MplsLdpEntityIndex,
                                                      u4TransportAddress)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Wrong Transport Address Or Make "
                       " sure the interface of the transport address is UP\n");
            return CLI_FAILURE;
        }
    }

    if (nmhSetFsMplsLdpEntityTransAddrTlvEnable (&MplsLdpEntityLdpLsrId,
                                                 u4MplsLdpEntityIndex,
                                                 i4CmdType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set Transport Address Tlv "
                   "Option\n");
        return CLI_FAILURE;
    }

    if (nmhSetMplsLdpEntityTransportAddrKind (&MplsLdpEntityLdpLsrId,
                                              u4MplsLdpEntityIndex,
                                              u4TransIfType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set Transport Address Kind\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityTransportAddress (&MplsLdpEntityLdpLsrId,
                                               u4MplsLdpEntityIndex,
                                               u4TransportAddress)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set Transport Address\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliLabelAllocationMode
 *  Description   :  This routine calls another routine to set the mode based 
 *                   on the flag passed
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   i4LblAllocMode - ordered or independent
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliLabelAllocationMode (tCliHandle CliHandle, INT4 i4LblAllocMode)
{
    UINT4               u4ErrorCode = LDP_ZERO;

    /* Test the allocation mode */
    if (nmhTestv2FsMplsLsrLabelAllocationMethod (&u4ErrorCode,
                                                 i4LblAllocMode) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Wrong Label allocation value\n");
        return CLI_FAILURE;
    }

    /* Set the allocation mode */
    if (nmhSetFsMplsLsrLabelAllocationMethod (i4LblAllocMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set Label Allocation mode\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliLsrId
 *  Description   :  This routine sets the LDP LSR Identifier.
 *  Input(s)      :  CliHandle          - Handle to CLI
 *                   u4IfType           - Interface Type
 *                   u4Value            - Interface Index or 
 *                                        IP Address.
 *                   i4LdpForceOption   - Force Option allows to retrigger the
 *                                        LDP Entity Sessions immediately.
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliLsrId (tCliHandle CliHandle, UINT4 u4IfType, UINT4 u4Value,
             INT4 i4LdpForceOption, INT4 i4Status)
{
    tSNMP_OCTET_STRING_TYPE LdpLsrId;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT1               au1LdpLsrId[LDP_LSR_ID_LEN] = { LDP_ZERO };
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT4               u4Port = LDP_ZERO;

    LdpLsrId.pu1_OctetList = au1LdpLsrId;

    if (i4Status == CLI_ENABLE)
    {
        if (u4IfType != CFA_NONE)
        {
            if (NetIpv4GetPortFromIfIndex (u4Value, &u4Port) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to get the Port from If"
                           " Index\n");
                return CLI_FAILURE;
            }

            if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to get IP Address from If"
                           " Index\n");
                return CLI_FAILURE;
            }

            if (NetIpIfInfo.u4Addr == LDP_ZERO)
            {
                CliPrintf (CliHandle,
                           "\r%% LSR ID should be valid and not zero\n");
                return CLI_FAILURE;
            }

            u4Value = NetIpIfInfo.u4Addr;
        }
        LdpLsrId.i4_Length = LDP_LSR_ID_LEN;
        MPLS_INTEGER_TO_OCTETSTRING (u4Value, (&LdpLsrId));
    }
    else
    {
        /* For the command "no router-id", Length of the LdpLsrId is set as
         * zero. This will ensure that the highest IP Address available in the
         * system is choosen as LDP LSR Identifier. */
        LdpLsrId.i4_Length = LDP_ZERO;
    }

    if (i4LdpForceOption == LDP_ZERO)
    {
        i4LdpForceOption = LDP_SNMP_FALSE;
    }

    if (nmhTestv2FsMplsLdpForceOption (&u4ErrorCode,
                                       i4LdpForceOption) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Wrong Force Option \n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpForceOption (i4LdpForceOption) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set Force Option \n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsLdpLsrId (&u4ErrorCode, &LdpLsrId) == SNMP_FAILURE)
    {
        if (u4ErrorCode == SNMP_ERR_BAD_VALUE)
        {
            CliPrintf (CliHandle, "\r%% Router-ID being used in LDP Entity."
                       " Use Force option to override. \n");
        }
        else
        {
            CliPrintf (CliHandle, "\r%% Wrong LDP LSR ID \n");
        }
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpLsrId (&LdpLsrId) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% LDP LSR ID Configuration Failed \n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliPhpMode 
 *  Description   :  This routine calls another routine to set the mode based 
 *                   on the flag passed
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   i4PhpMode - explicit or implicit null
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliPhpMode (tCliHandle CliHandle, INT4 i4PhpMode)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the Entity Index and LDP LSR ID */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();

    /* Test the flag */
    if (nmhTestv2FsMplsLdpEntityPHPRequestMethod (&u4ErrorCode,
                                                  &MplsLdpEntityLdpLsrId,
                                                  u4MplsLdpEntityIndex,
                                                  i4PhpMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Make the entity shutdown to set the PHP method\r\n");
        return CLI_FAILURE;
    }

    /* Set the mode for php */
    if (nmhSetFsMplsLdpEntityPHPRequestMethod (&MplsLdpEntityLdpLsrId,
                                               u4MplsLdpEntityIndex,
                                               i4PhpMode) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set the PHP Mode\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliEnableMplsLdpInterface
 *  Description   :  This routine calls nmh to enable LDP protocol 
 *                   in interface configuration
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   u4MplsLdpInterfaceIndex - MPLS Interface Index
 *                   u4MinLabel - MinLabel
 *                   u4MaxLabel - MaxLabel 
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
*******************************************************************************/

PRIVATE INT4
LdpCliEnableMplsLdpInterface (tCliHandle CliHandle,
                              UINT4 u4MplsLdpInterfaceIndex, UINT4 u4MinLabel,
                              UINT4 u4MaxLabel)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpId              LdpEntityId;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4ErrorCode = LDP_ZERO;
    INT4                i4MplsLabelSpace = LDP_GEN_PER_PLATFORM;
    UINT2               u2IncarnId = MPLS_DEF_INCARN;

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the ldp lsr id - Used to test the label range also */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();
    MEMCPY (LdpEntityId, MplsLdpEntityLdpLsrId.pu1_OctetList,
            LDP_MAX_LDPID_LEN);

    /* Check whether the entity exists or not - not required 
     * But used to get the LdpEntity structure to test the label range */
    if (LdpGetLdpEntity (u2IncarnId, LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No Ldp Entity\n");
        return CLI_FAILURE;
    }

    /* Check the label range */
    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
    {
        /* Setting default label values for the targeted entity */
        if (u4MinLabel == CLI_MPLS_LDP_ZERO && u4MaxLabel == CLI_MPLS_LDP_ZERO)
        {
            u4MinLabel = gSystemSize.MplsSystemSize.u4MinL2VpnLblRange;
            u4MaxLabel = gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange;
        }
    }
    else
    {
        /* Setting default label values for the basic discovery entity */
        if (u4MinLabel == CLI_MPLS_LDP_ZERO && u4MaxLabel == CLI_MPLS_LDP_ZERO)
        {
            u4MinLabel = gSystemSize.MplsSystemSize.u4MinLdpLblRange;
            u4MaxLabel = gSystemSize.MplsSystemSize.u4MaxLdpLblRange;
        }
    }

    if (nmhTestv2MplsLdpEntityGenericLRRowStatus (&u4ErrorCode,
                                                  &MplsLdpEntityLdpLsrId,
                                                  u4MplsLdpEntityIndex,
                                                  u4MinLabel, u4MaxLabel,
                                                  CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to create the Generic Table. "
                   "Check Label Range or Entity is ACTIVE or Labels are already associated to some entity\n");
        return CLI_FAILURE;
    }

    /* Set LDP entity generic to create and wait */
    if (nmhSetMplsLdpEntityGenericLRRowStatus (&MplsLdpEntityLdpLsrId,
                                               u4MplsLdpEntityIndex, u4MinLabel,
                                               u4MaxLabel,
                                               CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to create generic entity\n");
        return CLI_FAILURE;
    }

    /* Test the range of the Labels */
    if (nmhTestv2MplsLdpEntityGenericLabelSpace (&u4ErrorCode,
                                                 &MplsLdpEntityLdpLsrId,
                                                 u4MplsLdpEntityIndex,
                                                 u4MinLabel, u4MaxLabel,
                                                 i4MplsLabelSpace)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Wrong Label Space Value\n");
        LdpCliDisableMplsLdpInterface (CliHandle, u4MinLabel, u4MaxLabel);
        return CLI_FAILURE;
    }

    if (u4MplsLdpInterfaceIndex != LDP_ZERO)
    {
        /* Test whether mpls interface is stacked over any L3 Interface */
        if (nmhTestv2MplsLdpEntityGenericIfIndexOrZero (&u4ErrorCode,
                                                        &MplsLdpEntityLdpLsrId,
                                                        u4MplsLdpEntityIndex,
                                                        u4MinLabel, u4MaxLabel,
                                                        u4MplsLdpInterfaceIndex)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%%Mpls Interface Not stacked over any L3If. "
                       "Or this interface is used by another Generic Entity\n");
            LdpCliDisableMplsLdpInterface (CliHandle, u4MinLabel, u4MaxLabel);
            return CLI_FAILURE;
        }

        /* Set the interface index */
        if (nmhSetMplsLdpEntityGenericIfIndexOrZero (&MplsLdpEntityLdpLsrId,
                                                     u4MplsLdpEntityIndex,
                                                     u4MinLabel, u4MaxLabel,
                                                     u4MplsLdpInterfaceIndex)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set the interfaceindex\n");
            LdpCliDisableMplsLdpInterface (CliHandle, u4MinLabel, u4MaxLabel);
            return CLI_FAILURE;
        }
    }
    if (nmhSetMplsLdpEntityGenericLabelSpace (&MplsLdpEntityLdpLsrId,
                                              u4MplsLdpEntityIndex,
                                              u4MinLabel, u4MaxLabel,
                                              i4MplsLabelSpace) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set the Label Space\n");
        LdpCliDisableMplsLdpInterface (CliHandle, u4MinLabel, u4MaxLabel);
        return CLI_FAILURE;
    }

    if (nmhTestv2MplsLdpEntityGenericLRRowStatus (&u4ErrorCode,
                                                  &MplsLdpEntityLdpLsrId,
                                                  u4MplsLdpEntityIndex,
                                                  u4MinLabel, u4MaxLabel,
                                                  ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Cannot set Generic LR Table to Active. "
                   "Verify parameters\n");
        LdpCliDisableMplsLdpInterface (CliHandle, u4MinLabel, u4MaxLabel);
        return CLI_FAILURE;
    }
    /* Set the entity to active */
    if (nmhSetMplsLdpEntityGenericLRRowStatus (&MplsLdpEntityLdpLsrId,
                                               u4MplsLdpEntityIndex,
                                               u4MinLabel, u4MaxLabel,
                                               ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set ldp generic entity"
                   " to active\n");
        LdpCliDisableMplsLdpInterface (CliHandle, u4MinLabel, u4MaxLabel);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliDisableMplsLdpInterface
 *  Description   :  This routine calls nmh to disable the label distribution
 *                   protocol in interface mode
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   u4MinLabel - Minimum label value
 *                   u4MaxLabel - Maximum label value
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliDisableMplsLdpInterface (tCliHandle CliHandle, UINT4 u4MinLabel,
                               UINT4 u4MaxLabel)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpId              LdpEntityId;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT2               u2IncarnId = MPLS_DEF_INCARN;

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the ldp lsr id - Used to test the label range also */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();
    MEMCPY (LdpEntityId, MplsLdpEntityLdpLsrId.pu1_OctetList,
            LDP_MAX_LDPID_LEN);

    /* Check whether the entity exists or not - not required 
     * But used to get the LdpEntity structure to test the label range */
    if (LdpGetLdpEntity (u2IncarnId, LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% No Ldp Entity\n");
        return CLI_FAILURE;
    }

    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
    {
        /* Setting default label values for the targeted entity */
        if ((u4MinLabel == CLI_MPLS_LDP_ZERO)
            && (u4MaxLabel == CLI_MPLS_LDP_ZERO))
        {
            u4MinLabel = gSystemSize.MplsSystemSize.u4MinL2VpnLblRange;
            u4MaxLabel = gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange;
        }
    }
    else
    {
        /* Setting default label values for the basic discovery entity */
        if ((u4MinLabel == CLI_MPLS_LDP_ZERO)
            && (u4MaxLabel == CLI_MPLS_LDP_ZERO))
        {
            u4MinLabel = gSystemSize.MplsSystemSize.u4MinLdpLblRange;
            u4MaxLabel = gSystemSize.MplsSystemSize.u4MaxLdpLblRange;
        }
    }

    if (nmhTestv2MplsLdpEntityGenericLRRowStatus (&u4ErrorCode,
                                                  &MplsLdpEntityLdpLsrId,
                                                  u4MplsLdpEntityIndex,
                                                  u4MinLabel, u4MaxLabel,
                                                  DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to destroy LDP Generic Entity\n");

        if (u4ErrorCode == SNMP_ERR_INCONSISTENT_VALUE)
        {
            CliPrintf (CliHandle, "\r%%Make LDP entity admin down\n");
        }
        return CLI_FAILURE;
    }

    /*Set the Generic LR RowStatus to destroy */
    if (nmhSetMplsLdpEntityGenericLRRowStatus (&MplsLdpEntityLdpLsrId,
                                               u4MplsLdpEntityIndex, u4MinLabel,
                                               u4MaxLabel,
                                               DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to destroy LDP Generic Entity\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliShowLabelRange 
 *  Description   :  This routine displays the label range values 
 *  Input(s)      :  CliHandle - Handle to CLI
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
LdpCliShowLabelRange (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityFirstLdpLsrId;
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityNextLdpLsrId;
    UINT4               u4MplsFirstLdpEntityIndex = LDP_ZERO;
    UINT4               u4MplsNextLdpEntityIndex = LDP_ZERO;
    UINT4               u4FirstMinLbl = LDP_ZERO;
    UINT4               u4NextMinLbl = LDP_ZERO;
    UINT4               u4FirstMaxLbl = LDP_ZERO;
    UINT4               u4NextMaxLbl = LDP_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               au1Temp[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4TransIpAddr = LDP_ZERO;
    static UINT1        au1FirstId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1NextId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    INT4                i4LabelType = LDP_ZERO;

    MplsLdpEntityFirstLdpLsrId.pu1_OctetList = au1FirstId;
    MplsLdpEntityNextLdpLsrId.pu1_OctetList = au1NextId;
    MplsLdpEntityFirstLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    MplsLdpEntityNextLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;

    MEMSET (au1IfName, LDP_ZERO, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (au1Temp, LDP_ZERO, CFA_MAX_PORT_NAME_LENGTH);

    /* Get First Generic Ldp Entity with the label */
    if (nmhGetFirstIndexMplsLdpEntityGenericLRTable
        (&MplsLdpEntityFirstLdpLsrId, &u4MplsFirstLdpEntityIndex,
         &u4FirstMinLbl, &u4FirstMaxLbl) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    while (LDP_ONE)
    {
        /* Get the Interface Name for the interface index */
        if (LdpGetInterfaceName (&MplsLdpEntityFirstLdpLsrId,
                                 u4MplsFirstLdpEntityIndex,
                                 u4FirstMinLbl, u4FirstMaxLbl,
                                 au1IfName, &u4TransIpAddr) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* Get the label type */
        if (nmhGetMplsLdpEntityLabelType (&MplsLdpEntityFirstLdpLsrId,
                                          u4MplsFirstLdpEntityIndex,
                                          &i4LabelType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* To Display each interface only once */
        if (STRCMP (au1Temp, au1IfName) != LDP_ZERO)
        {
            STRNCPY (au1Temp, au1IfName, (sizeof (au1Temp) - 1));
            au1Temp[sizeof (au1Temp) - 1] = '\0';
            CliPrintf (CliHandle, "\rInterface: %s", (INT1 *) au1IfName);
        }

        CliPrintf (CliHandle, "\n\r\tEntity Index: %d"
                   "\n\r\t\tDownstream Label Pool"
                   "\n\r\t\tLabel Type: %s "
                   " Min/Max Label: %d/%d\n\n",
                   u4MplsFirstLdpEntityIndex,
                   CLI_MPLS_LDP_LABEL (i4LabelType), u4FirstMinLbl,
                   u4FirstMaxLbl);

        /* Get the next generic ldp entity */
        if (nmhGetNextIndexMplsLdpEntityGenericLRTable
            (&MplsLdpEntityFirstLdpLsrId, &MplsLdpEntityNextLdpLsrId,
             u4MplsFirstLdpEntityIndex, &u4MplsNextLdpEntityIndex,
             u4FirstMinLbl, &u4NextMinLbl, u4FirstMaxLbl,
             &u4NextMaxLbl) == SNMP_FAILURE)
        {
            break;
        }

        MEMCPY (MplsLdpEntityFirstLdpLsrId.pu1_OctetList,
                MplsLdpEntityNextLdpLsrId.pu1_OctetList,
                MplsLdpEntityNextLdpLsrId.i4_Length);
        MplsLdpEntityFirstLdpLsrId.i4_Length =
            MplsLdpEntityNextLdpLsrId.i4_Length;
        u4MplsFirstLdpEntityIndex = u4MplsNextLdpEntityIndex;
        u4FirstMinLbl = u4NextMinLbl;
        u4FirstMaxLbl = u4NextMaxLbl;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliShowDiscovery 
 *  Description   :  This routine shows the status of discovery process
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   u4Flag
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
LdpCliShowDiscovery (tCliHandle CliHandle, UINT4 u4Flag)
{
    tSNMP_OCTET_STRING_TYPE EntityLsrId;
    tSNMP_OCTET_STRING_TYPE EntityPeerId;
    UINT4               u4EntityIndex = LDP_ZERO;
    UINT4               u4TempEntityIndex = LDP_ZERO;
    UINT4               u4HelloAdjIndex = LDP_ZERO;
    UINT4               u4TempLdpId = LDP_ZERO;
    UINT4               u4TransIpAddr = LDP_ZERO;
    UINT4               u4IncarnId = MPLS_DEF_INCARN;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    INT1               *piIfName;

    static UINT1        au1LdpId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1PeerId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1DummyLdpId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1DummyPeerLdpId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    INT4                i4AdjacencyType = LDP_LOCAL_PEER;
    CHR1               *pc1LdpId = NULL;
    CHR1               *pc1Temp = NULL;
    UINT1               au1PeerLdpId[LDP_DEF_BUF_SIZE] = { LDP_ZERO };
    UINT1               au1IpAddr[LDP_DEF_BUF_SIZE] = { LDP_ZERO };
    tLdpDummy1          LdpDummy;
    tLdpAdjacency      *pLdpAdj = NULL;
    piIfName = (INT1 *) (&au1IfName[0]);

    EntityLsrId.pu1_OctetList = au1LdpId;
    EntityPeerId.pu1_OctetList = au1PeerId;
    LdpDummy.LdpEntityLdpId.pu1_OctetList = au1DummyLdpId;
    LdpDummy.PeerLdpId.pu1_OctetList = au1DummyPeerLdpId;

    EntityLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    EntityPeerId.i4_Length = LDP_MAX_LDPID_LEN;
    LdpDummy.LdpEntityLdpId.i4_Length = LDP_MAX_LDPID_LEN;
    LdpDummy.PeerLdpId.i4_Length = LDP_MAX_LDPID_LEN;

    MEMSET (au1IfName, LDP_ZERO, CFA_MAX_PORT_NAME_LENGTH);

    if (nmhGetFirstIndexMplsLdpHelloAdjacencyTable (&EntityLsrId,
                                                    &u4EntityIndex,
                                                    &EntityPeerId,
                                                    &u4HelloAdjIndex)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*  Get First generic ldp entity */
    do
    {
        if (LdpGetLdpAdjacency (u4IncarnId, EntityLsrId.pu1_OctetList,
                                u4EntityIndex, EntityPeerId.pu1_OctetList,
                                u4HelloAdjIndex, &pLdpAdj) == LDP_FAILURE)
        {
            continue;
        }
        /* Get the bridge status for router ports. */
        CfaGetIfBridgedIfaceStatus (pLdpAdj->u4IfIndex, &u1BridgedIfaceStatus);

        if ((u1BridgedIfaceStatus == CFA_DISABLED)
            || (CfaIsL3IpVlanInterface (pLdpAdj->u4IfIndex) == CFA_SUCCESS))
        {
            /* Get interface name */
            if (CfaCliGetIfName (pLdpAdj->u4IfIndex, piIfName) == CFA_FAILURE)
            {
                continue;
            }
        }
        else
        {
            /* Get interface name */
            if (CfaGetInterfaceNameFromIndex (pLdpAdj->u4IfIndex, au1IfName)
                == (UINT4) CFA_FAILURE)
            {
                continue;
            }
        }
        if (u4TempEntityIndex != u4EntityIndex)
        {
            MPLS_OCTETSTRING_TO_LDPID ((&EntityLsrId), u4TempLdpId);
            CLI_CONVERT_IPADDR_TO_STR (pc1LdpId, u4TempLdpId);
            CliPrintf (CliHandle, "\n\rLocal Ldp Identifier:\n\r    %s,%d\n",
                       pc1LdpId, u4EntityIndex);
            CliPrintf (CliHandle, "\n\rDiscovery Sources:");
        }
        u4TempEntityIndex = u4EntityIndex;

        CliPrintf (CliHandle, "\n\r    Interfaces:");
        CliPrintf (CliHandle, "\n\r        %s:", (INT1 *) au1IfName);

        MEMCPY (LdpDummy.LdpEntityLdpId.pu1_OctetList,
                EntityLsrId.pu1_OctetList, EntityLsrId.i4_Length);
        LdpDummy.LdpEntityLdpId.i4_Length = EntityLsrId.i4_Length;

        MEMCPY (LdpDummy.PeerLdpId.pu1_OctetList,
                EntityPeerId.pu1_OctetList, EntityPeerId.i4_Length);
        LdpDummy.PeerLdpId.i4_Length = EntityPeerId.i4_Length;
        LdpDummy.CliHandle = CliHandle;
        LdpDummy.u4MplsLdpEntityIndex = u4EntityIndex;
        LdpDummy.u4HelloAdjacencyIndex = u4HelloAdjIndex;
        LdpDummy.u4Flag = u4Flag;

        if (nmhGetMplsLdpHelloAdjacencyType (&EntityLsrId, u4EntityIndex,
                                             &EntityPeerId, u4HelloAdjIndex,
                                             &i4AdjacencyType) == SNMP_FAILURE)
        {
            continue;
        }

        MPLS_OCTETSTRING_TO_LDPID ((&EntityPeerId), u4TempLdpId);
        CLI_CONVERT_IPADDR_TO_STR (pc1Temp, u4TempLdpId);
        MEMCPY ((UINT1 *) au1PeerLdpId, (UINT1 *) pc1Temp, LDP_DEF_BUF_SIZE);

        CLI_CONVERT_IPADDR_TO_STR (pc1Temp, u4TransIpAddr);
        MEMCPY ((UINT1 *) au1IpAddr, (UINT1 *) pc1Temp, LDP_DEF_BUF_SIZE);
        if (LdpDisplayDiscovery (&LdpDummy, au1PeerLdpId,
                                 i4AdjacencyType, au1IpAddr) == CLI_FAILURE)
        {
            continue;
        }
    }
    while (nmhGetNextIndexMplsLdpHelloAdjacencyTable
           (&EntityLsrId, &EntityLsrId, u4EntityIndex, &u4EntityIndex,
            &EntityPeerId, &EntityPeerId, u4HelloAdjIndex, &u4HelloAdjIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliShowNeighbor
 *  Description   :  This routine shows the status of ldp sessions
 *  Input(s)      :  CliHandle - Handle to CLI
 *                   u4Flag - Display flag
 *                   u4IpAddr
 *                   i4MplsInIfIndex - Incoming LDP enabled MPLS Interface
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
LdpCliShowNeighbor (tCliHandle CliHandle, UINT4 u4Flag, UINT4 u4IpAddress,
                    UINT4 u4IfIndex)
{
    tSNMP_OCTET_STRING_TYPE EntityLsrId;
    tSNMP_OCTET_STRING_TYPE EntityPeerId;
    UINT4               u4EntityIndex = LDP_ZERO;
    UINT4               u4HelloAdjIndex = LDP_ZERO;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];
    UINT4               u4PeerAddr = LDP_ZERO;
    UINT4               u4IncarnId = MPLS_DEF_INCARN;
    UINT1               u1BridgedIfaceStatus = CFA_DISABLED;
    INT1               *piIfName;

    static UINT1        au1LdpId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1PeerId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    tLdpNeighborDummy1  LdpDummy;
    tLdpAdjacency      *pLdpAdj = NULL;
    piIfName = (INT1 *) (&au1IfName[0]);

    EntityLsrId.pu1_OctetList = au1LdpId;
    EntityPeerId.pu1_OctetList = au1PeerId;

    EntityLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    EntityPeerId.i4_Length = LDP_MAX_LDPID_LEN;

    MEMSET (au1IfName, LDP_ZERO, CFA_MAX_PORT_NAME_LENGTH);

    if (nmhGetFirstIndexMplsLdpHelloAdjacencyTable (&EntityLsrId,
                                                    &u4EntityIndex,
                                                    &EntityPeerId,
                                                    &u4HelloAdjIndex)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    do
    {
        if (LdpGetLdpAdjacency (u4IncarnId, EntityLsrId.pu1_OctetList,
                                u4EntityIndex, EntityPeerId.pu1_OctetList,
                                u4HelloAdjIndex, &pLdpAdj) == LDP_FAILURE)
        {
            continue;
        }

        /* Check for the interface match */
        if (((u4Flag & CLI_MPLS_LDP_TWO) == CLI_MPLS_LDP_TWO)
            && (u4IfIndex != pLdpAdj->u4IfIndex))
        {
            continue;
        }

        /* Get the bridge status for router ports. */
        CfaGetIfBridgedIfaceStatus (pLdpAdj->u4IfIndex, &u1BridgedIfaceStatus);

        if ((u1BridgedIfaceStatus == CFA_DISABLED)
            || (CfaIsL3IpVlanInterface (pLdpAdj->u4IfIndex) == CFA_SUCCESS))
        {
            /* Get interface name */
            if (CfaCliGetIfName (pLdpAdj->u4IfIndex, piIfName) == CFA_FAILURE)
            {
                continue;
            }
        }
        else
        {
            /* Get interface name */
            if (CfaGetInterfaceNameFromIndex (pLdpAdj->u4IfIndex, au1IfName)
                == (UINT4) CFA_FAILURE)
            {
                continue;
            }
        }

        MPLS_OCTETSTRING_TO_LDPID ((&EntityPeerId), u4PeerAddr);

        /* Check for neighbor address match */
        if (((u4Flag & CLI_MPLS_LDP_FOUR) == CLI_MPLS_LDP_FOUR) &&
            (u4IpAddress != u4PeerAddr))
        {
            continue;
        }

        /* Display the neighbor information */
        LdpDummy.CliHandle = CliHandle;
        LdpDummy.u4MplsLdpEntityIndex = u4EntityIndex;
        LdpDummy.u4HelloAdjacencyIndex = u4HelloAdjIndex;
        LdpDummy.u4Flag = u4Flag;

        if (LdpDisplayNeighbor (&LdpDummy, &EntityLsrId, &EntityPeerId,
                                au1IfName) == CLI_FAILURE)
        {
            continue;
        }
    }
    while (nmhGetNextIndexMplsLdpHelloAdjacencyTable
           (&EntityLsrId, &EntityLsrId, u4EntityIndex, &u4EntityIndex,
            &EntityPeerId, &EntityPeerId, u4HelloAdjIndex, &u4HelloAdjIndex)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliShowDatabase
 *  Description   :  This routine shows the LDP parameters used
 *  Input(s)      :  CliHandle - Handle to CLI
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
LdpCliShowDatabase (tCliHandle CliHandle)
{
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT2               u2NumActiveLsp = 0;
    UINT4               u4HIndex = 0;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tTMO_SLL_NODE      *pLdpSllUpstrNode = NULL;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tTMO_SLL_NODE      *pTempLdpSllUpstrNode = NULL;
    tTMO_HASH_NODE     *pSsnHashNode = NULL;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlk = NULL;

    if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
    {
        return CLI_FAILURE;
    }

    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

    TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
    {
        TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex, pSsnHashNode,
                              tTMO_HASH_NODE *)
        {
            pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);
            if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
            {
                TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession), pLdpSllNode,
                                  pTempLdpSllNode, tTMO_SLL_NODE *)
                {
                    pUstrCtrlBlk = (tUstrLspCtrlBlock *) (pLdpSllNode);
                    if (UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL)
                    {
                        if (UPSTR_STATE (pUstrCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            if (u2NumActiveLsp == LDP_ZERO)
                            {
                                LdpCliPrintDatabaseHeader (CliHandle);
                            }
                            u2NumActiveLsp++;
                            LdpCliPrintNormUstrCtrlBlk (CliHandle,
                                                        (tUstrLspCtrlBlock *)
                                                        pUstrCtrlBlk);
                        }
                    }
                    else
                    {
                        if (UPSTR_STATE (pUstrCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            if (u2NumActiveLsp == LDP_ZERO)
                            {
                                LdpCliPrintDatabaseHeader (CliHandle);
                            }

                            u2NumActiveLsp++;
                            LdpCliPrintNormUstrCtrlBlk (CliHandle,
                                                        (tUstrLspCtrlBlock *)
                                                        pUstrCtrlBlk);
                        }
                    }
                }
                if (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG)
                {
                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                      pTempLdpSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                        if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                        {
                            continue;
                        }

                        if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            if (u2NumActiveLsp == LDP_ZERO)
                            {
                                LdpCliPrintDatabaseHeader (CliHandle);
                            }

                            u2NumActiveLsp++;
                            LdpCliPrintNormLCB (CliHandle,
                                                (tLspCtrlBlock *) pLspCtrlBlk);
                        }
                    }
                }
            }
            else
            {
                TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession), pLdpSllNode,
                                  pTempLdpSllNode, tTMO_SLL_NODE *)
                {
                    pLspCtrlBlk = (tLspCtrlBlock *) (pLdpSllNode);
                    if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                    {
                        continue;
                    }
                    if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                    {
                        if (u2NumActiveLsp == LDP_ZERO)
                        {
                            LdpCliPrintDatabaseHeader (CliHandle);
                        }

                        u2NumActiveLsp++;
                        LdpCliPrintNormLCB (CliHandle,
                                            (tLspCtrlBlock *) pLspCtrlBlk);
                    }
                }

                TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                  pTempLdpSllNode, tTMO_SLL_NODE *)
                {
                    pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                    if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                    {
                        continue;
                    }

                    if (pLspCtrlBlk->pUStrSession != NULL)
                    {
                        continue;
                    }

                    if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                    {
                        if (u2NumActiveLsp == LDP_ZERO)
                        {
                            LdpCliPrintDatabaseHeader (CliHandle);
                        }

                        u2NumActiveLsp++;
                        LdpCliPrintNormLCB (CliHandle,
                                            (tLspCtrlBlock *) pLspCtrlBlk);
                    }
                }
            }

        }
    }

    /* When the Lsr is acting as an ingress to the LSP */
    TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
    {
        TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex, pSsnHashNode,
                              tTMO_HASH_NODE *)
        {
            pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);
            if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
            {
                if (TMO_SLL_Count (&SSN_DLCB (pLdpSession)) != LDP_ZERO)
                {
                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                      pTempLdpSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                        if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                        {
                            continue;
                        }

                        if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            TMO_DYN_SLL_Scan (&LCB_UPSTR_LIST (pLspCtrlBlk),
                                              pLdpSllUpstrNode,
                                              pTempLdpSllUpstrNode,
                                              tTMO_SLL_NODE *)
                            {
                                pUstrCtrlBlk = SLL_TO_UPCB (pLdpSllUpstrNode);
                                if ((UPSTR_STATE (pUstrCtrlBlk) ==
                                     LDP_LSM_ST_EST) &&
                                    (UPSTR_USSN (pUstrCtrlBlk) == NULL))
                                {
                                    if (u2NumActiveLsp == LDP_ZERO)
                                    {
                                        LdpCliPrintDatabaseHeader (CliHandle);
                                    }

                                    u2NumActiveLsp++;
                                    LdpCliPrintNormUstrCtrlBlk (CliHandle,
                                                                (tUstrLspCtrlBlock
                                                                 *)
                                                                pUstrCtrlBlk);
                                }
                            }
                        }
                    }            /*End of Scan */
                }
            }
        }
    }
#ifdef LDP_GR_WANTED
    LdpCliShowStaleDatabase (CliHandle, &u2NumActiveLsp);
#endif

    if (u2NumActiveLsp)
    {
        CliPrintf (CliHandle, "\r--------------------------------------------"
                   "-------------------------------");
        CliPrintf (CliHandle, "\r\nNormal Lsps currently at this node = %d\n",
                   u2NumActiveLsp);
    }
    return CLI_SUCCESS;
}

#ifdef LDP_GR_WANTED
PRIVATE VOID
LdpCliShowStaleDatabase (tCliHandle CliHandle, UINT2 *pu2NumActiveLsp)
{

    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tTMO_SLL_NODE      *pLdpSllUpstrNode = NULL;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tTMO_SLL_NODE      *pTempLdpSllUpstrNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlk = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    tTMO_SLL           *pEntityList = NULL;

    pEntityList = &LDP_ENTITY_LIST (u2IncarnId);

    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
        {
            pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

            if ((pLdpSession == NULL) ||
                (pLdpSession->u1StaleStatus == LDP_FALSE))
            {
                continue;
            }
            if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
            {
                TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession), pLdpSllNode,
                                  pTempLdpSllNode, tTMO_SLL_NODE *)
                {
                    pUstrCtrlBlk = (tUstrLspCtrlBlock *) (pLdpSllNode);
                    if (UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL)
                    {
                        if (UPSTR_STATE (pUstrCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            if (*pu2NumActiveLsp == LDP_ZERO)
                            {
                                LdpCliPrintDatabaseHeader (CliHandle);
                            }
                            (*pu2NumActiveLsp)++;
                            LdpCliPrintNormUstrCtrlBlk (CliHandle,
                                                        (tUstrLspCtrlBlock *)
                                                        pUstrCtrlBlk);
                        }
                    }
                    else
                    {
                        if (UPSTR_STATE (pUstrCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            if (*pu2NumActiveLsp == LDP_ZERO)
                            {
                                LdpCliPrintDatabaseHeader (CliHandle);
                            }

                            (*pu2NumActiveLsp)++;
                            LdpCliPrintNormUstrCtrlBlk (CliHandle,
                                                        (tUstrLspCtrlBlock *)
                                                        pUstrCtrlBlk);
                        }
                    }
                }

                if (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG)
                {
                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                      pTempLdpSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                        if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                        {
                            continue;
                        }

                        if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            if ((*pu2NumActiveLsp) == LDP_ZERO)
                            {
                                LdpCliPrintDatabaseHeader (CliHandle);
                            }

                            (*pu2NumActiveLsp)++;
                            LdpCliPrintNormLCB (CliHandle,
                                                (tLspCtrlBlock *) pLspCtrlBlk);
                        }
                    }
                }
            }
            else
            {
                TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession), pLdpSllNode,
                                  pTempLdpSllNode, tTMO_SLL_NODE *)
                {
                    pLspCtrlBlk = (tLspCtrlBlock *) (pLdpSllNode);
                    if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                    {
                        continue;
                    }
                    if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                    {
                        if (*pu2NumActiveLsp == LDP_ZERO)
                        {
                            LdpCliPrintDatabaseHeader (CliHandle);
                        }

                        (*pu2NumActiveLsp)++;
                        LdpCliPrintNormLCB (CliHandle,
                                            (tLspCtrlBlock *) pLspCtrlBlk);
                    }
                }

                TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                  pTempLdpSllNode, tTMO_SLL_NODE *)
                {
                    pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                    if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                    {
                        continue;
                    }

                    if (pLspCtrlBlk->pUStrSession != NULL)
                    {
                        continue;
                    }

                    if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                    {
                        if (*pu2NumActiveLsp == LDP_ZERO)
                        {
                            LdpCliPrintDatabaseHeader (CliHandle);
                        }

                        (*pu2NumActiveLsp)++;
                        LdpCliPrintNormLCB (CliHandle,
                                            (tLspCtrlBlock *) pLspCtrlBlk);
                    }

                }
            }
        }
    }

    /* When the Lsr is acting as an ingress to the LSP */
    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
        {
            pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

            if ((pLdpSession == NULL) ||
                (pLdpSession->u1StaleStatus == LDP_FALSE))
            {
                continue;
            }

            if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
            {
                if (TMO_SLL_Count (&SSN_DLCB (pLdpSession)) != LDP_ZERO)
                {
                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                      pTempLdpSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                        if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                        {
                            continue;
                        }

                        if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            TMO_DYN_SLL_Scan (&LCB_UPSTR_LIST (pLspCtrlBlk),
                                              pLdpSllUpstrNode,
                                              pTempLdpSllUpstrNode,
                                              tTMO_SLL_NODE *)
                            {
                                pUstrCtrlBlk = SLL_TO_UPCB (pLdpSllUpstrNode);
                                if ((UPSTR_STATE (pUstrCtrlBlk) ==
                                     LDP_LSM_ST_EST) &&
                                    (UPSTR_USSN (pUstrCtrlBlk) == NULL))
                                {
                                    if (*pu2NumActiveLsp == LDP_ZERO)
                                    {
                                        LdpCliPrintDatabaseHeader (CliHandle);
                                    }

                                    (*pu2NumActiveLsp)++;
                                    LdpCliPrintNormUstrCtrlBlk (CliHandle,
                                                                (tUstrLspCtrlBlock
                                                                 *)
                                                                pUstrCtrlBlk);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}

#endif

/* ---------------------------------------------------------------------------
 * Function Name : LdpCliPrintDatabaseHeader                                 
 * Description   : This routine prints the Header of the database display
 * Input(s)      : CliHandle 
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
PRIVATE VOID
LdpCliPrintDatabaseHeader (tCliHandle CliHandle)
{
    CliPrintf (CliHandle,
               "\r\n--------------------------------------------------"
               "-------------------------");
    CliPrintf (CliHandle,
               "\r\n                  |           Upstream        |"
               "         Downstream        |");
    CliPrintf (CliHandle,
               "\r\n  Network Prefix  |---------------------------+"
               "---------------------------");
    CliPrintf (CliHandle,
               "\r\n                  |       LSR-ID     |  Label |"
               "       LSR-ID     | Label  |");
    CliPrintf (CliHandle,
               "\r\n------------------+------------------+--------+"
               "------------------+---------\r\n");
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpCliPrintNormLCB                                        
 * Description   : This routine prints the main identifier info of a normal 
 *                 LSP Control block.
 * Input(s)      : u2LspNum,pLspCtrlBlk  
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
PRIVATE VOID
LdpCliPrintNormLCB (tCliHandle CliHandle, tLspCtrlBlock * pLspCtrlBlk)
{
    tLdpId              ULdpId;
    tLdpId              DLdpId;
    CHR1                c1Line[MAX_COLUMN_LENGTH + LDP_ONE];
    CHR1               *pc1Aux = NULL;
    tUtlInAddr          Prefix;
    UINT2               u2ULdpId = 0;
    UINT2               u2DLdpId = 0;
    pc1Aux = c1Line;

    /* Printing the FEC Network Prefix */
#ifdef MPLS_IPV6_WANTED
    if (pLspCtrlBlk->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH,
                  "%15s/%-2d|", Ip6PrintAddr (&pLspCtrlBlk->Fec.Prefix.Ip6Addr),
                  pLspCtrlBlk->Fec.u1PreLen);
    }
    else
#endif
    {
        Prefix.u4Addr = OSIX_NTOHL (LDP_IPV4_U4_ADDR (pLspCtrlBlk->Fec.Prefix));
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH,
                  "%15s/%-2d|", INET_NTOA (Prefix), pLspCtrlBlk->Fec.u1PreLen);
    }
    pc1Aux = c1Line + STRLEN (c1Line);

    /* Printing the LSRID of the UpStream LSR */
    if (pLspCtrlBlk->pUStrSession != NULL
        && pLspCtrlBlk->UStrLabel.u4GenLbl != LDP_INVALID_LABEL)
    {
        LDP_DBG3 (LDP_MAIN_ALL,
                  "%s : %d pLspCtrlBlk->pUStrSession != NULL Printing Upstream LSR Id, pLspCtrlBlk->pUStrSession %x\n",
                  __FUNCTION__, __LINE__, pLspCtrlBlk->pUStrSession);

        MEMCPY (ULdpId, pLspCtrlBlk->pUStrSession->pLdpPeer->PeerLdpId,
                sizeof (tLdpId));
        MEMCPY ((UINT1 *) &(Prefix.u4Addr), (UINT1 *) &ULdpId, LDP_IPV4ADR_LEN);
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%15s:", INET_NTOA (Prefix));
        pc1Aux = c1Line + STRLEN (c1Line);
        MEMCPY (&u2ULdpId, &ULdpId[4], sizeof (UINT2));
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%-2d|", u2ULdpId);
        pc1Aux = c1Line + STRLEN (c1Line);
    }
    else
    {
        LDP_DBG2 (LDP_MAIN_ALL,
                  "%s : %d pLspCtrlBlk->pUStrSession == NULL Printing Upstream LSR Id\n",
                  __FUNCTION__, __LINE__);
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "         -        |");
        pc1Aux = c1Line + STRLEN (c1Line);
    }

    /* Printing the UpStream Label */
    if (pLspCtrlBlk->pUStrSession != NULL)
    {
        if (pLspCtrlBlk->UStrLabel.u4GenLbl != LDP_INVALID_LABEL)
        {
            SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%8u|",
                      pLspCtrlBlk->UStrLabel.u4GenLbl);
        }
        else
        {
            SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "    -   |");
        }

        pc1Aux = c1Line + STRLEN (c1Line);
    }
    else
    {
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "    -   |");
        pc1Aux = c1Line + STRLEN (c1Line);
    }

    /* Printing the LSRID of the DownStream LSR */
    if (pLspCtrlBlk->pDStrSession != NULL)
    {
        LDP_DBG2 (LDP_MAIN_ALL,
                  "%s : %d pLspCtrlBlk->pDStrSession != NULL Printing Downstream LSR Id\n",
                  __FUNCTION__, __LINE__);

        MEMCPY (DLdpId, pLspCtrlBlk->pDStrSession->pLdpPeer->PeerLdpId,
                sizeof (tLdpId));
        MEMCPY ((UINT1 *) &(Prefix.u4Addr), (UINT1 *) &DLdpId, LDP_IPV4ADR_LEN);
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%15s:", INET_NTOA (Prefix));
        pc1Aux = c1Line + STRLEN (c1Line);
        MEMCPY (&u2DLdpId, &DLdpId[4], sizeof (UINT2));
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%-2d|", u2DLdpId);
        pc1Aux = c1Line + STRLEN (c1Line);
    }
    else
    {
        LDP_DBG2 (LDP_MAIN_ALL,
                  "%s : %d pLspCtrlBlk->pDStrSession == NULL Printing Downstream LSR Id\n",
                  __FUNCTION__, __LINE__);
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "         -        |");
        pc1Aux = c1Line + STRLEN (c1Line);
    }

    /* Printing the DownStream Label */
    if (pLspCtrlBlk->pDStrSession != NULL)
    {
        if (pLspCtrlBlk->DStrLabel.u4GenLbl != LDP_INVALID_LABEL)
        {
            SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH,
                      "%8u|\r\n", pLspCtrlBlk->DStrLabel.u4GenLbl);
        }
        else
        {
            SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "    -   |\r\n");
        }
        pc1Aux = c1Line + STRLEN (c1Line);
    }
    else
    {
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "    -   |\r\n");
        pc1Aux = c1Line + STRLEN (c1Line);
    }

    CliPrintf (CliHandle, "%s", c1Line);
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpCliPrintNormUstrCtrlBlk                                      
 * Description   : This routine prints the main identifier info of a normal 
 *                 LSP Control block.
 * Input(s)      : u2LspNum,pLspCtrlBlk  
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
PRIVATE VOID
LdpCliPrintNormUstrCtrlBlk (tCliHandle CliHandle,
                            tUstrLspCtrlBlock * pUstrCtrlBlk)
{
    tLdpId              ULdpId;
    tLdpId              DLdpId;
    CHR1                c1Line[81];
    CHR1               *pc1Aux = NULL;
    tUtlInAddr          Prefix;
    UINT2               u2ULdpId = 0;
    UINT2               u2DLdpId = 0;

    pc1Aux = c1Line;

    LDP_DBG3 (LDP_MAIN_ALL, "%s : %d pUstrCtrlBlk = %x\n", __FUNCTION__,
              __LINE__, pUstrCtrlBlk);

    /* Printing the FEC Network Prefix */
#ifdef MPLS_IPV6_WANTED
    if (pUstrCtrlBlk->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH,
                  "%15s/%-2d|",
                  Ip6PrintAddr (&pUstrCtrlBlk->Fec.Prefix.Ip6Addr),
                  pUstrCtrlBlk->Fec.u1PreLen);
    }
    else
#endif
    {
        Prefix.u4Addr =
            OSIX_NTOHL (LDP_IPV4_U4_ADDR (pUstrCtrlBlk->Fec.Prefix));
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%15s/%-2d|", INET_NTOA (Prefix),
                  pUstrCtrlBlk->Fec.u1PreLen);
    }
    pc1Aux = c1Line + STRLEN (c1Line);

    /* Printing the LSRID of the UpStream LSR */
    if (pUstrCtrlBlk->pUpstrSession != NULL)
    {
        LDP_DBG2 (LDP_MAIN_ALL,
                  "%s : %d pUstrCtrlBlk->pUpstrSession != NULL, Printing UpStream LSR Id\n",
                  __FUNCTION__, __LINE__);
        MEMCPY (ULdpId, pUstrCtrlBlk->pUpstrSession->pLdpPeer->PeerLdpId,
                sizeof (tLdpId));
        MEMCPY ((UINT1 *) &(Prefix.u4Addr), (UINT1 *) &ULdpId, LDP_IPV4ADR_LEN);
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%15s:", INET_NTOA (Prefix));
        pc1Aux = c1Line + STRLEN (c1Line);
        MEMCPY (&u2ULdpId, &ULdpId[4], sizeof (UINT2));
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%-2d|", u2ULdpId);
        pc1Aux = c1Line + STRLEN (c1Line);
    }
    else
    {
        LDP_DBG2 (LDP_MAIN_ALL,
                  "%s : %d pUstrCtrlBlk->pUpstrSession = NULL Printing UpStream LSR Id\n",
                  __FUNCTION__, __LINE__);
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "         -          |");
        pc1Aux = c1Line + STRLEN (c1Line);
    }

    /* Printing the UpStream Label */
    if (pUstrCtrlBlk->pUpstrSession != NULL)
    {
        if (SSN_MRGTYPE (pUstrCtrlBlk->pUpstrSession) == LDP_ZERO)
        {
            if (pUstrCtrlBlk->UStrLabel.u4GenLbl != LDP_INVALID_LABEL)
            {
                SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%8u|",
                          pUstrCtrlBlk->UStrLabel.u4GenLbl);
            }
            else
            {
                SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "    -   |");
            }
            pc1Aux = c1Line + STRLEN (c1Line);
            if (UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL)
            {
                if (pUstrCtrlBlk->UStrLabel.u4GenLbl != LDP_INVALID_LABEL)
                {
                    SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH,
                              "%8u|         -           |    -   \r\n",
                              pUstrCtrlBlk->UStrLabel.u4GenLbl);
                }
                else
                {
                    SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH,
                              "    -   |         -           |    -   \r\n");
                }

                pc1Aux = c1Line + STRLEN (c1Line);
            }
            else
            {
                SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH,
                          "    -   |         -           |    -  \r\n");
                pc1Aux = c1Line + STRLEN (c1Line);
            }
        }
    }
    if (pUstrCtrlBlk->pUpstrSession != NULL)
    {
        if (LDP_IS_ENTITY_TYPE_ATM (pUstrCtrlBlk->pUpstrSession) == LDP_FALSE)
        {
            if (pUstrCtrlBlk->UStrLabel.u4GenLbl != LDP_INVALID_LABEL)
            {
                SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH,
                          "%8u|", pUstrCtrlBlk->UStrLabel.u4GenLbl);
            }
            else
            {
                SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "    -   |");
            }
            pc1Aux = c1Line + STRLEN (c1Line);
        }
        else
        {
            SNPRINTF (pc1Aux,
                      MAX_COLUMN_LENGTH, "%4d,%4d|",
                      pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vpi,
                      pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vci);
            pc1Aux = c1Line + STRLEN (c1Line);
        }
    }
    else
    {
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "    -   |");
        pc1Aux = c1Line + STRLEN (c1Line);
    }

    /* Printing the LSRID of the DownStream LSR */
    if (UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL)
    {

        LDP_DBG2 (LDP_MAIN_ALL,
                  "%s : %d UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL Printing Downstream LSR Id\n",
                  __FUNCTION__, __LINE__);
        MEMCPY (DLdpId,
                (UPSTR_DSTR_PTR (pUstrCtrlBlk))->pDStrSession->pLdpPeer->
                PeerLdpId, sizeof (tLdpId));
        MEMCPY ((UINT1 *) &(Prefix.u4Addr), (UINT1 *) &DLdpId, LDP_IPV4ADR_LEN);
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%15s:", INET_NTOA (Prefix));
        pc1Aux = c1Line + STRLEN (c1Line);
        MEMCPY (&u2DLdpId, &DLdpId[4], sizeof (UINT2));
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%-2d|", u2DLdpId);
        pc1Aux = c1Line + STRLEN (c1Line);
    }
    else
    {
        LDP_DBG2 (LDP_MAIN_ALL,
                  "%s : %d UPSTR_DSTR_PTR (pUstrCtrlBlk) == NULL Printing Downstream LSR Id\n",
                  __FUNCTION__, __LINE__);
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "         -        |");
        pc1Aux = c1Line + STRLEN (c1Line);
    }

    /* Printing the DownStream Label */
    if (UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL)
    {
        LDP_DBG2 (LDP_MAIN_ALL,
                  "%s : %d UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL Printing Downstream Label\n",
                  __FUNCTION__, __LINE__);
        if (LDP_IS_ENTITY_TYPE_ATM
            ((UPSTR_DSTR_PTR (pUstrCtrlBlk))->pDStrSession) == LDP_FALSE)
        {
            if ((UPSTR_DSTR_PTR (pUstrCtrlBlk))->DStrLabel.u4GenLbl !=
                LDP_INVALID_LABEL)
            {
                SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "%8u|\r\n",
                          (UPSTR_DSTR_PTR (pUstrCtrlBlk))->DStrLabel.u4GenLbl);
            }
            else
            {
                SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "   -   |\r\n");
            }

            pc1Aux = c1Line + STRLEN (c1Line);
        }
        else
        {
            SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH,
                      "%4d,%4d|\r\n",
                      (UPSTR_DSTR_PTR (pUstrCtrlBlk))->DStrLabel.AtmLbl.u2Vpi,
                      (UPSTR_DSTR_PTR (pUstrCtrlBlk))->DStrLabel.AtmLbl.u2Vci);
            pc1Aux = c1Line + STRLEN (c1Line);
        }
    }
    else
    {
        LDP_DBG2 (LDP_MAIN_ALL,
                  "%s : %d UPSTR_DSTR_PTR (pUstrCtrlBlk) == NULL Printing Downstream Label\n",
                  __FUNCTION__, __LINE__);
        SNPRINTF (pc1Aux, MAX_COLUMN_LENGTH, "    -   |\r\n");
        pc1Aux = c1Line + STRLEN (c1Line);
    }

    CliPrintf (CliHandle, "%s", c1Line);
}

/******************************************************************************
 *  Function Name :  LdpCliShowParameters
 *  Description   :  This routine shows the LDP parameters used
 *  Input(s)      :  CliHandle - Handle to CLI
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
LdpCliShowParameters (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityFirstLdpLsrId;
    tSNMP_OCTET_STRING_TYPE RouterId;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityNextLdpLsrId;
    UINT4               u4MplsFirstLdpEntityIndex = LDP_ZERO;
    UINT4               u4MplsNextLdpEntityIndex = LDP_ZERO;
    UINT4               u4ProtVersion = LDP_ZERO;
    UINT4               u4IpAddr = LDP_ZERO;
    UINT4               u4HelloHoldTime = LDP_ZERO;
    UINT4               u4HelloInterval = LDP_ZERO;
    UINT4               u4KeepAliveInterval = LDP_ZERO;
    UINT4               u4KeepAliveHoldTime = LDP_ZERO;

    INT4                i4AllocMode = LDP_ZERO;
    INT4                i4PhpMode = MPLS_PHP_DISABLED;
    INT4                i4LabelDistMode = LDP_ZERO;
    INT4                i4LabelRetMode = LDP_ZERO;
    INT4                i4TargetHFlag = LDP_ZERO;
    INT4                i4AdminStatus = LDP_ZERO;
    INT4                i4LoopDetectCapable = LDP_ZERO;
    INT4                i4HCLimit = LDP_ZERO;
    INT4                i4PVLimit = LDP_ZERO;
#ifdef MPLS_LDP_BFD_WANTED
    INT4                i4BfdStatus = LDP_ZERO;
#endif
    BOOL1               b1Loop = FALSE;

    static UINT1        au1FirstId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1RouterId[LDP_LSR_ID_LEN] = { LDP_ZERO };
    static UINT1        au1NextId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1PeerAddr[IPV6_ADDR_LENGTH] = { LDP_ZERO };
    CHR1               *pc1Prefix = NULL;

    MplsLdpEntityFirstLdpLsrId.pu1_OctetList = au1FirstId;
    MplsLdpEntityNextLdpLsrId.pu1_OctetList = au1NextId;
    PeerAddr.pu1_OctetList = au1PeerAddr;
    RouterId.pu1_OctetList = au1RouterId;

    MplsLdpEntityFirstLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    MplsLdpEntityNextLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    PeerAddr.i4_Length = LDP_LSR_ID_LEN;

    LdpCliDisplayGrConfigurations (CliHandle);

    /* Fetch Router Id, Convert this in octet string to string */
    nmhGetMplsLdpLsrId (&RouterId);
    RouterId.i4_Length = LDP_LSR_ID_LEN;
    MPLS_OCTETSTRING_TO_INTEGER ((&RouterId), u4IpAddr);
    CLI_CONVERT_IPADDR_TO_STR (pc1Prefix, u4IpAddr);

    /* Get global Parameters from ldp MIB */
    nmhGetFsMplsLsrLabelAllocationMethod (&i4AllocMode);

    CliPrintf (CliHandle, "\n\rRouter Id: %s\n", pc1Prefix);

    CliPrintf (CliHandle, " \rLabel Allocation Mode: %s\n\n",
               CLI_MPLS_LDP_ALLOC (i4AllocMode));

    /* Get First Ldp Entity */
    if (nmhGetFirstIndexMplsLdpEntityTable (&MplsLdpEntityFirstLdpLsrId,
                                            &u4MplsFirstLdpEntityIndex)
        == SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }

    while (LDP_ONE)
    {
        if (b1Loop == TRUE)
        {
            /* Get Next Entity, Not Executed Time */
            if (nmhGetNextIndexMplsLdpEntityTable (&MplsLdpEntityFirstLdpLsrId,
                                                   &MplsLdpEntityNextLdpLsrId,
                                                   u4MplsFirstLdpEntityIndex,
                                                   &u4MplsNextLdpEntityIndex)
                == SNMP_FAILURE)
            {
                break;
            }

            MEMCPY (MplsLdpEntityFirstLdpLsrId.pu1_OctetList,
                    MplsLdpEntityNextLdpLsrId.pu1_OctetList,
                    MplsLdpEntityNextLdpLsrId.i4_Length);
            MplsLdpEntityFirstLdpLsrId.i4_Length =
                MplsLdpEntityNextLdpLsrId.i4_Length;
            u4MplsFirstLdpEntityIndex = u4MplsNextLdpEntityIndex;
        }

        b1Loop = TRUE;
        /* Get entity related parameters */
        if (nmhGetMplsLdpEntityProtocolVersion (&MplsLdpEntityFirstLdpLsrId,
                                                u4MplsFirstLdpEntityIndex,
                                                &u4ProtVersion) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetFsMplsLdpEntityPHPRequestMethod (&MplsLdpEntityFirstLdpLsrId,
                                                   u4MplsFirstLdpEntityIndex,
                                                   &i4PhpMode) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsLdpEntityHelloHoldTimer (&MplsLdpEntityFirstLdpLsrId,
                                               u4MplsFirstLdpEntityIndex,
                                               &u4HelloHoldTime)
            == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsLdpEntityTargetPeer (&MplsLdpEntityFirstLdpLsrId,
                                           u4MplsFirstLdpEntityIndex,
                                           &i4TargetHFlag) == SNMP_FAILURE)
        {
            continue;
        }

        if (u4HelloHoldTime == LDP_ZERO)
        {
            u4HelloHoldTime = (i4TargetHFlag == LDP_TRUE) ?
                LDP_DEF_RHELLO_HTIME : LDP_DEF_LHELLO_HTIME;
        }

        u4HelloInterval = u4HelloHoldTime;
        LDP_GET_HELLO_FREQ (u4HelloInterval);

        if (nmhGetMplsLdpEntityKeepAliveHoldTimer (&MplsLdpEntityFirstLdpLsrId,
                                                   u4MplsFirstLdpEntityIndex,
                                                   &u4KeepAliveHoldTime)
            == SNMP_FAILURE)
        {
            continue;
        }
        u4KeepAliveInterval = (UINT4) ((FLT4)
                                       u4KeepAliveHoldTime *
                                       LDP_KALIVE_MSG_FREQ);

        if (nmhGetMplsLdpEntityLabelDistMethod (&MplsLdpEntityFirstLdpLsrId,
                                                u4MplsFirstLdpEntityIndex,
                                                &i4LabelDistMode)
            == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsLdpEntityLabelRetentionMode (&MplsLdpEntityFirstLdpLsrId,
                                                   u4MplsFirstLdpEntityIndex,
                                                   &i4LabelRetMode)
            == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsLdpEntityTargetPeerAddr (&MplsLdpEntityFirstLdpLsrId,
                                               u4MplsFirstLdpEntityIndex,
                                               &PeerAddr) == SNMP_FAILURE)
        {
            continue;
        }
        MPLS_OCTETSTRING_TO_INTEGER ((&PeerAddr), u4IpAddr);
        CLI_CONVERT_IPADDR_TO_STR (pc1Prefix, u4IpAddr);

        if (nmhGetMplsLdpEntityAdminStatus (&MplsLdpEntityFirstLdpLsrId,
                                            u4MplsFirstLdpEntityIndex,
                                            &i4AdminStatus) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsLdpLsrLoopDetectionCapable (&i4LoopDetectCapable) ==
            SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsLdpEntityPathVectorLimit (&MplsLdpEntityFirstLdpLsrId,
                                                u4MplsFirstLdpEntityIndex,
                                                &i4PVLimit) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsLdpEntityHopCountLimit (&MplsLdpEntityFirstLdpLsrId,
                                              u4MplsFirstLdpEntityIndex,
                                              &i4HCLimit) == SNMP_FAILURE)
        {
            continue;
        }
#ifdef MPLS_LDP_BFD_WANTED
        if (nmhGetFsMplsLdpEntityBfdStatus (&MplsLdpEntityFirstLdpLsrId,
                                            u4MplsFirstLdpEntityIndex,
                                            &i4BfdStatus) == SNMP_FAILURE)
        {
            continue;
        }
#endif
        CliPrintf (CliHandle, "\rEntity Index: %d\n",
                   u4MplsFirstLdpEntityIndex);
        CliPrintf (CliHandle, "\rLdp Protocol Version: %d\n", u4ProtVersion);
        if (u4IpAddr != 0)
        {
            CliPrintf (CliHandle, "\n\r\tTargeted Peer Addr: %s", pc1Prefix);
        }
        if (i4PhpMode != MPLS_PHP_DISABLED)
        {
            CliPrintf (CliHandle, "\n\r\tPHP Mode: %s",
                       CLI_MPLS_LDP_PHP (i4PhpMode));
        }
        CliPrintf (CliHandle, "\n\r\tHelloHoldTime: %d sec Interval: %d msec"
                   "\n\r\tKeepAliveTime: %d Interval: %d"
                   "\n\r\tLabel Distribution Mode: %s"
                   "\n\r\tLabel Retention Mode: %s"
                   "\n\r\tAdmin Status: %s",
                   u4HelloHoldTime, u4HelloInterval,
                   u4KeepAliveHoldTime, u4KeepAliveInterval,
                   CLI_MPLS_LDP_DIST (i4LabelDistMode),
                   CLI_MPLS_LDP_RET (i4LabelRetMode),
                   CLI_MPLS_LDP_SHOW_ADMIN_STATUS (i4AdminStatus));

        if (i4LoopDetectCapable == MPLS_LDP_LOOP_DETECT_NOT_CAPABLE)
        {
            CliPrintf (CliHandle, "\n\r\tLoop Detection: Disabled");
        }
        else
        {
            CliPrintf (CliHandle, "\n\r\tLoop Detection: Enabled");
        }
        if ((i4LoopDetectCapable == MPLS_LDP_LOOP_DETECT_CAPABLE_HC) ||
            (i4LoopDetectCapable == MPLS_LDP_LOOP_DETECT_CAPABLE_HCPV))
        {
            CliPrintf (CliHandle, "\n\r\t    Hop Count Limit: %d", i4HCLimit);
        }
        if ((i4LoopDetectCapable == MPLS_LDP_LOOP_DETECT_CAPABLE_PV) ||
            (i4LoopDetectCapable == MPLS_LDP_LOOP_DETECT_CAPABLE_HCPV))
        {
            CliPrintf (CliHandle, "\n\r\t    Path Vector Limit: %d", i4PVLimit);
        }
#ifdef MPLS_LDP_BFD_WANTED

        if (i4BfdStatus == LDP_BFD_ENABLED)
        {
            CliPrintf (CliHandle, "\n\r\tBfd Status: enabled");
        }
        else
        {
            CliPrintf (CliHandle, "\n\r\tBfd Status: Disabled");
        }
#endif
        CliPrintf (CliHandle, "\n\n");
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliMplsLdpChangeMode
 *  Description   :  This routine changes the mode
 *  Input(s)      :  i4MplsLdpMode
 *                   u4ModeIndex
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliMplsLdpChangeMode (INT4 i4MplsLdpMode, UINT4 u4ModeIndex)
{
    CHR1                ac1Cmd[MAX_PROMPT_LEN];

    MEMSET (ac1Cmd, LDP_ZERO, MAX_PROMPT_LEN);
    if (i4MplsLdpMode == CLI_MPLS_LDP)
    {
        SNPRINTF (ac1Cmd, MAX_PROMPT_LEN, "%s", LDP_MODE);
    }
    else
    {
        SNPRINTF (ac1Cmd, MAX_PROMPT_LEN, "%s%d", LDP_ENTITY_MODE, u4ModeIndex);
    }

    /* Change the mode */
    if (CliChangePath (ac1Cmd) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name :  LdpCliMplsLdpEntityChangeRowStatus
 *  Description   :  This routine calls nmh to set the LDP entity in 
 *                   NOT_IN_SERVICE 
 *  Input(s)      :  CliHandle - Handle to the CLI Interface
 *                   i4LdpEntityRowStatus - LDP Entity Row status
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

PRIVATE INT4
LdpCliMplsLdpEntityChangeRowStatus (tCliHandle CliHandle,
                                    INT4 i4LdpEntityRowStatus)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN];
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpId = NULL;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT2               u2IncarnId = MPLS_DEF_INCARN;

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the MPLS LDP LSR ID and entity index */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();

    /* Get the LDP Entity */
    pu1LdpId = MplsLdpEntityLdpLsrId.pu1_OctetList;
    if (LdpGetLdpEntity (u2IncarnId, pu1LdpId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Ldp Entity Not Found\n");
        return CLI_FAILURE;
    }

    if (i4LdpEntityRowStatus == ACTIVE)
    {
        if (pLdpEntity->u1TargetFlag == LDP_TRUE)
        {
            if ((pLdpEntity->i1PhpConf != MPLS_PHP_DISABLED)
                || (pLdpEntity->u2HopVecLimit != LDP_DEF_HOP_CNT_LIM)
                || (pLdpEntity->u1PathVecLimit != LDP_ZERO))
            {
                CLI_SET_ERR (LDP_CLI_INVALID_TLDP_CONFIG);
                return CLI_FAILURE;
            }
        }
        if (nmhTestv2MplsLdpEntityAdminStatus
            (&u4ErrorCode, &MplsLdpEntityLdpLsrId, u4MplsLdpEntityIndex,
             LDP_ADMIN_UP) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set Entity Admin-staus Up \n");
            return CLI_FAILURE;
        }

        if (nmhTestv2MplsLdpEntityRowStatus
            (&u4ErrorCode, &MplsLdpEntityLdpLsrId, u4MplsLdpEntityIndex,
             ACTIVE) == SNMP_FAILURE)
        {
            if (u4ErrorCode == SNMP_ERR_WRONG_TYPE)
            {
                CliPrintf (CliHandle, "\r%%Unable to set LDP Entity active -"
                           " Target Address & Transport Address should be of same Address Family\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "\r%%Unable to set LDP Entity active -"
                           " Check label range or interface association\r\n");
            }
            return CLI_FAILURE;
        }

        /* Make the Entity to ADMIN UP */
        if (nmhSetMplsLdpEntityAdminStatus (&MplsLdpEntityLdpLsrId,
                                            u4MplsLdpEntityIndex,
                                            LDP_ADMIN_UP) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set Entity Admin-status Up\n");
            return CLI_FAILURE;
        }

        /* Set the LDP Entity to ACTIVE */
        if (nmhSetMplsLdpEntityRowStatus (&MplsLdpEntityLdpLsrId,
                                          u4MplsLdpEntityIndex,
                                          ACTIVE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set LDP Entity active\n");
            return CLI_FAILURE;
        }

    }
    else
    {
        if (nmhTestv2MplsLdpEntityAdminStatus
            (&u4ErrorCode, &MplsLdpEntityLdpLsrId, u4MplsLdpEntityIndex,
             LDP_ADMIN_DOWN) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set Admin-status Down\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2MplsLdpEntityRowStatus
            (&u4ErrorCode, &MplsLdpEntityLdpLsrId, u4MplsLdpEntityIndex,
             NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%%Unable to set LDP Entity Not in Service -"
                       " Check label range\r\n");
            return CLI_FAILURE;
        }

        /* Make the Entity to ADMIN DOWN */
        if (nmhSetMplsLdpEntityAdminStatus (&MplsLdpEntityLdpLsrId,
                                            u4MplsLdpEntityIndex,
                                            LDP_ADMIN_DOWN) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set Admin-status Down\n");
            return CLI_FAILURE;
        }

        if (nmhSetMplsLdpEntityRowStatus (&MplsLdpEntityLdpLsrId,
                                          u4MplsLdpEntityIndex,
                                          NOT_IN_SERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to set entity Not in service\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

 /*************************************************************************
 *  Funtion Name :  MplsGetMplsLdpCfgPrompt
 *  Description   : This routine is returns the prompt to be displayed.
 *  Input(s)      : pi1ModeName - Mode to be configured
 *  Output(s)     : pi1DispStr  - Prompt to be displayed.
 *  Return(s)     : TRUE or FALSE.
 ************************************************************************/

INT1
MplsGetMplsLdpCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (LDP_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, LDP_MODE, u4Len) != LDP_ZERO)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    SNPRINTF (ac1PromptDisplayName, sizeof (ac1PromptDisplayName), "%s",
              "(config-mpls-ldp)#");
    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    pi1DispStr[MAX_PROMPT_LEN - 1] = '\0';
    return TRUE;
}

 /*************************************************************************
 *  Funtion Name :  MplsGetMplsLdpEntityCfgPrompt
 *  Description   : This routine is returns the prompt to be displayed.
 *  Input(s)      : pi1ModeName - Mode to be configured
 *  Output(s)     : pi1DispStr  - Prompt to be displayed.
 *  Return(s)     : TRUE or FALSE.
 ************************************************************************/

INT1
MplsGetMplsLdpEntityCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Index = LDP_ZERO;
    UINT4               u4Len = STRLEN (LDP_ENTITY_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, LDP_ENTITY_MODE, u4Len) != LDP_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;
    u4Index = CLI_ATOI (pi1ModeName);
    CLI_SET_MPLS_LDP_ENTITY_MODE (u4Index);

    SNPRINTF (ac1PromptDisplayName, sizeof (ac1PromptDisplayName), "%s%d%s",
              "(config-mpls-ldp-entity-", u4Index, ")#");
    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    pi1DispStr[MAX_PROMPT_LEN - 1] = '\0';
    return TRUE;
}

 /*************************************************************************
 *  Funtion Name :  LdpGetInterfaceName
 *  Description   : This routine returns mpls interface name.
 *  Input(s)      : pMplsLdpEntityLdpLsrId,
 *                  u4MplsLdpEntityIndex,
 *                  u4MinLabelVal,
 *                  u4MaxLabelVal,                  
 *  Output(s)     : pu1InterfaceName
 *                  pu4IpAddr
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE.
 ************************************************************************/

PRIVATE INT4
LdpGetInterfaceName (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpLsrId,
                     UINT4 u4MplsLdpEntityIndex, UINT4 u4MinLabelVal,
                     UINT4 u4MaxLabelVal, UINT1 *pu1IfName, UINT4 *pu4IpAddr)
{
#ifdef CFA_WANTED
    tIpConfigInfo       IpIfInfo;
    UINT4               u4IfIndex = LDP_ZERO;
#endif
    INT4                i4MplsIfIndex = LDP_ZERO;

    /* Get the mpls interface index - value will be 181.... */
    if (nmhGetMplsLdpEntityGenericIfIndexOrZero (pMplsLdpEntityLdpLsrId,
                                                 u4MplsLdpEntityIndex,
                                                 u4MinLabelVal,
                                                 u4MaxLabelVal,
                                                 &i4MplsIfIndex) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Even if Interface Index is not configured, return SUCCESS. */
    if (i4MplsIfIndex == LDP_ZERO)
    {
        return CLI_SUCCESS;
    }

#ifdef CFA_WANTED
    /* Get the l3 interface index */
    if (CfaUtilGetL3IfFromMplsIf ((UINT4) i4MplsIfIndex, &u4IfIndex, TRUE) ==
        CFA_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Get the interface name */
    if (CfaGetInterfaceNameFromIndex (u4IfIndex,
                                      pu1IfName) == (UINT4) CFA_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Get the interface ip address - not used for all cases */
    if (CfaIpIfGetIfInfo (u4IfIndex, &IpIfInfo) == CFA_FAILURE)
    {
        return CLI_FAILURE;
    }
    *pu4IpAddr = IpIfInfo.u4Addr;
#else
    *pu4IpAddr = 0;
    STRCPY (pu1IfName, "");
#endif
    return CLI_SUCCESS;
}

/*************************************************************************
 *  Funtion Name :  LdpDisplayNeighbor
 *  Description   : This routine displays neighbor info in normal.
 *  Input(s)      : LdpDummy
 *                  pMplsLdpEntityLdpLsrId,
 *                  pMplsLdpEntityPeerId,
 *                  pu1InterfaceName,
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE.
 ************************************************************************/

PRIVATE INT4
LdpDisplayNeighbor (tLdpNeighborDummy1 * LdpDummy,
                    tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpLsrId,
                    tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityPeerId,
                    UINT1 *pu1IfName)
{
    tCliHandle          CliHandle;
    tSNMP_OCTET_STRING_TYPE PeerTransAddr;
#ifdef MPLS_IPV6_WANTED
    tSNMP_OCTET_STRING_TYPE Ip6LocalTransAddr;
    tSNMP_OCTET_STRING_TYPE Ip6PeerTransAddr;
    tIp6Addr            LPeerAddr, LDPLOCLAddr, LOCLAddr, TempIpv6Addr;
    UINT1               au1Ip6LocalTransAddr[LDP_IPV6_TR_ADDR_LEN] =
        { LDP_ZERO };
    UINT1               au1Ip6PeerTransAddr[LDP_IPV6_TR_ADDR_LEN] =
        { LDP_ZERO };
    INT4                i4CmpRet;
#endif
    UINT1              *pu1LdpPeerId = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    INT4                i4SessionState = LDP_ZERO;
    INT4                i4SessionRole = LDP_ZERO;
    INT4                i4PeerSessionRole = LDP_ZERO;
    INT4                i4PeerLabelDistMode = LDP_ZERO;
    INT4                i4TransAddrTlv = LDP_ZERO;
    INT4                i4TransAddrKind = LDP_ZERO;
    INT4                i4PeerKATimeRem = LDP_ZERO;
    UINT4               u4KeepAliveTime = LDP_ZERO;
    UINT4               u4KeepAliveInterval = LDP_ZERO;
    UINT4               u4PeerKATime = LDP_ZERO;
    UINT4               u4Value = LDP_ZERO;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4LocalTransAddr = LDP_ZERO;
    UINT4               u4Flag = LDP_ZERO;
    CHR1                ac1LocalLdpId[LDP_DEF_BUF_SIZE] = { '0' };
    CHR1                ac1PeerLdpId[LDP_DEF_BUF_SIZE] = { '0' };
    CHR1                ac1LocalTransAddr[LDP_DEF_BUF_SIZE] = { '0' };
    CHR1                ac1PeerTransAddr[LDP_DEF_BUF_SIZE] = { '0' };
    CHR1               *pc1Temp = NULL;
    UINT1               au1PeerTransAddr[IPV6_ADDR_LENGTH] = { LDP_ZERO };
    UINT4               u4IfIpIndex = LDP_ZERO;
    UINT1               u1OperStatus = CFA_IF_DOWN;

    PeerTransAddr.pu1_OctetList = au1PeerTransAddr;
    PeerTransAddr.i4_Length = LDP_LSR_ID_LEN;

    u4MplsLdpEntityIndex = LdpDummy->u4MplsLdpEntityIndex;
    u4Flag = LdpDummy->u4Flag;
    CliHandle = LdpDummy->CliHandle;

    MPLS_OCTETSTRING_TO_LDPID ((pMplsLdpEntityPeerId), u4Value);
    CLI_CONVERT_IPADDR_TO_STR (pc1Temp, u4Value);
    MEMCPY ((UINT1 *) ac1PeerLdpId, pc1Temp, LDP_DEF_BUF_SIZE);

    MPLS_OCTETSTRING_TO_LDPID ((pMplsLdpEntityLdpLsrId), u4Value);
    CLI_CONVERT_IPADDR_TO_STR (pc1Temp, u4Value);
    MEMCPY ((UINT1 *) ac1LocalLdpId, pc1Temp, LDP_DEF_BUF_SIZE);

#ifdef MPLS_IPV6_WANTED
    MEMSET (&(TempIpv6Addr), 0, sizeof (tIp6Addr));
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpLsrId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityPeerId);

    Ip6PeerTransAddr.pu1_OctetList = au1Ip6PeerTransAddr;
    Ip6PeerTransAddr.i4_Length = LDP_IPV6_TR_ADDR_LEN;

    Ip6LocalTransAddr.pu1_OctetList = au1Ip6LocalTransAddr;
    Ip6LocalTransAddr.i4_Length = LDP_IPV6_TR_ADDR_LEN;

    if (LDP_FAILURE == LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                                      u4MplsLdpEntityIndex,
                                      pu1LdpPeerId, &pLdpPeer))
    {
        LDP_DBG (LDP_MAIN_ALL, "MAIN: Unable to get ldp peer \n");
    }

    if ((pLdpPeer->pLdpSession->SrcTransAddr.u2AddrType) == MPLS_IPV6_ADDR_TYPE)
    {
        MEMSET (&(LPeerAddr), 0, sizeof (tIp6Addr));
        MEMSET (&(LDPLOCLAddr), 0, sizeof (tIp6Addr));
        MEMSET (&(LOCLAddr), 0, sizeof (tIp6Addr));

        if (nmhGetFsMplsLdpEntityIpv6TransAddrTlvEnable (pMplsLdpEntityLdpLsrId,
                                                         u4MplsLdpEntityIndex,
                                                         &i4TransAddrTlv)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsMplsLdpEntityIpv6TransportAddrKind (pMplsLdpEntityLdpLsrId,
                                                        u4MplsLdpEntityIndex,
                                                        &i4TransAddrKind)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhGetFsMplsLdpEntityIpv6TransportAddress (pMplsLdpEntityLdpLsrId,
                                                       u4MplsLdpEntityIndex,
                                                       &Ip6LocalTransAddr)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhGetMplsLdpPeerTransportAddr (pMplsLdpEntityLdpLsrId,
                                            u4MplsLdpEntityIndex,
                                            pMplsLdpEntityPeerId,
                                            &Ip6PeerTransAddr) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        MEMCPY (LPeerAddr.u1_addr, Ip6PeerTransAddr.pu1_OctetList,
                LDP_IPV6_TR_ADDR_LEN);
        MEMCPY (LOCLAddr.u1_addr, Ip6LocalTransAddr.pu1_OctetList,
                LDP_IPV6_TR_ADDR_LEN);

        i4CmpRet =
            MEMCMP (LOCLAddr.u1_addr, TempIpv6Addr.u1_addr,
                    LDP_IPV6_TR_ADDR_LEN);

        /*If Session is made without Transport Address i.e on Hello Src Addr */
        if ((i4TransAddrTlv == LDP_FALSE) ||
            (i4TransAddrKind == INTERFACE_ADDRESS_TYPE) || (i4CmpRet == 0))
        {
            MEMCPY (LDPLOCLAddr.u1_addr,
                    pLdpPeer->pLdpSession->SrcTransAddr.Addr.Ip6Addr.u1_addr,
                    LDP_IPV6_TR_ADDR_LEN);
        }
        /*If Session is made on Transport Address */
        else
        {
            MEMCPY (LDPLOCLAddr.u1_addr, LOCLAddr.u1_addr,
                    LDP_IPV6_TR_ADDR_LEN);
        }
    }
    else
    {
#endif
        if (nmhGetFsMplsLdpEntityTransAddrTlvEnable (pMplsLdpEntityLdpLsrId,
                                                     u4MplsLdpEntityIndex,
                                                     &i4TransAddrTlv)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetMplsLdpEntityTransportAddrKind (pMplsLdpEntityLdpLsrId,
                                                  u4MplsLdpEntityIndex,
                                                  &i4TransAddrKind) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsMplsLdpEntityTransportAddress (pMplsLdpEntityLdpLsrId,
                                                   u4MplsLdpEntityIndex,
                                                   &u4LocalTransAddr)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetMplsLdpPeerTransportAddr (pMplsLdpEntityLdpLsrId,
                                            u4MplsLdpEntityIndex,
                                            pMplsLdpEntityPeerId,
                                            &PeerTransAddr) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        MPLS_OCTETSTRING_TO_INTEGER ((&PeerTransAddr), u4Value);
        CLI_CONVERT_IPADDR_TO_STR (pc1Temp, u4Value);
        MEMCPY ((UINT1 *) ac1PeerTransAddr, pc1Temp, LDP_DEF_BUF_SIZE);

        CfaIpIfGetIfIndexFromIpAddress (u4LocalTransAddr, &u4IfIpIndex);
        CfaGetIfOperStatus (u4IfIpIndex, &u1OperStatus);
        if ((i4TransAddrTlv == LDP_FALSE) ||
            (i4TransAddrKind == INTERFACE_ADDRESS_TYPE) ||
            (u4LocalTransAddr == LDP_ZERO) || (u1OperStatus == CFA_IF_DOWN))
        {
            if (NetIpv4GetSrcAddrToUseForDest (u4Value, &u4LocalTransAddr)
                == NETIPV4_FAILURE)
            {
                return CLI_FAILURE;
            }
        }

        CLI_CONVERT_IPADDR_TO_STR (pc1Temp, u4LocalTransAddr);
        MEMCPY ((UINT1 *) ac1LocalTransAddr, pc1Temp, LDP_DEF_BUF_SIZE);
#ifdef MPLS_IPV6_WANTED
    }
#endif
    /* Get the session state for the entity-peer relationship */
    if (nmhGetMplsLdpSessionState (pMplsLdpEntityLdpLsrId,
                                   u4MplsLdpEntityIndex, pMplsLdpEntityPeerId,
                                   &i4SessionState) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Get the Session Role */
    if (nmhGetMplsLdpSessionRole (pMplsLdpEntityLdpLsrId,
                                  u4MplsLdpEntityIndex, pMplsLdpEntityPeerId,
                                  &i4SessionRole) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4SessionRole == LDP_ACTIVE)
    {
        i4PeerSessionRole = LDP_PASSIVE;
    }
    else if (i4SessionRole == LDP_PASSIVE)
    {
        i4PeerSessionRole = LDP_ACTIVE;
    }

    /* Get the peer label distribution method */
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpLsrId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityPeerId);
    if (LDP_FAILURE == LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                                      u4MplsLdpEntityIndex,
                                      pu1LdpPeerId, &pLdpPeer))

    {
        LDP_DBG (LDP_MAIN_ALL, "MAIN: Unable to get ldp peer \n");
        return CLI_FAILURE;
    }

    i4PeerLabelDistMode = pLdpPeer->pLdpSession->CmnSsnParams.u1LabelAdvertMode;

    /* Get the entity keep alive timer */
    if (nmhGetMplsLdpEntityKeepAliveHoldTimer (pMplsLdpEntityLdpLsrId,
                                               u4MplsLdpEntityIndex,
                                               &u4KeepAliveTime)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    u4KeepAliveInterval = ((UINT4)
                           (((FLT4) (u4KeepAliveTime)) * LDP_KALIVE_MSG_FREQ));

    /* Display the information collected */
    if (nmhGetMplsLdpSessionKeepAliveTime (pMplsLdpEntityLdpLsrId,
                                           u4MplsLdpEntityIndex,
                                           pMplsLdpEntityPeerId,
                                           &u4PeerKATime) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsLdpSessionKeepAliveHoldTimeRem (pMplsLdpEntityLdpLsrId,
                                                  u4MplsLdpEntityIndex,
                                                  pMplsLdpEntityPeerId,
                                                  &i4PeerKATimeRem)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (!((i4SessionRole == LDP_PASSIVE) &&
          (i4SessionState == LDP_SSM_ST_NON_EXISTENT)))
    {
#ifdef MPLS_IPV6_WANTED
        if ((pLdpPeer->pLdpSession->SrcTransAddr.u2AddrType) ==
            MPLS_IPV6_ADDR_TYPE)
        {
            /* Display the ipv6 neighbor information in detail for  */
            if ((u4Flag & CLI_MPLS_LDP_EIGHT) == CLI_MPLS_LDP_EIGHT)
            {
                CliPrintf (CliHandle, "\n\rPeer Ldp Id: %s Local Ldp Id: %s,%d"
                           "\n\r        TCP Connection: %s - %s, %s - %s "
                           "\n\r        State: %s; %s"
                           "\n\r        LDP Discovery Sources:\n"
                           "\r             %s\n"
                           "\r             KeepAlive Holdtime: %d s,"
                           " KA Interval: %d s"
                           "\n             Peer KeepAlive Holdtime: %d s,"
                           " KA Time Remaining: %d s\n\n",
                           ac1PeerLdpId, ac1LocalLdpId, u4MplsLdpEntityIndex,
                           Ip6PrintAddr (&LPeerAddr),
                           Ip6PrintAddr (&LDPLOCLAddr),
                           CLI_MPLS_LDP_SESSION_ROLE (i4PeerSessionRole),
                           CLI_MPLS_LDP_SESSION_ROLE (i4SessionRole),
                           CLI_MPLS_SESSION_STATE (i4SessionState),
                           CLI_MPLS_LDP_DIST (i4PeerLabelDistMode),
                           (INT1 *) pu1IfName, u4KeepAliveTime,
                           u4KeepAliveInterval, u4PeerKATime, i4PeerKATimeRem);
            }
            else if ((u4Flag & CLI_MPLS_LDP_ONE) == CLI_MPLS_LDP_ONE)
            {
                /* Display the ipv6 neighbor information in concise manner */
                CliPrintf (CliHandle, "\n\rPeer Ldp Id: %s Local Ldp Id: %s,"
                           "%d\n\r        TCP Connection: %s - %s, %s - %s"
                           "\n\r        State: %s; %s\n"
                           "\r          LDP Discovery Sources:\n"
                           "\r             %s\n\n",
                           ac1PeerLdpId, ac1LocalLdpId, u4MplsLdpEntityIndex,
                           Ip6PrintAddr (&LPeerAddr),
                           Ip6PrintAddr (&LDPLOCLAddr),
                           CLI_MPLS_LDP_SESSION_ROLE (i4PeerSessionRole),
                           CLI_MPLS_LDP_SESSION_ROLE (i4SessionRole),
                           CLI_MPLS_SESSION_STATE (i4SessionState),
                           CLI_MPLS_LDP_DIST (i4PeerLabelDistMode),
                           (INT1 *) pu1IfName);
            }

        }
        else
        {
#endif
            /* Display the neighbor information in detail */
            if ((u4Flag & CLI_MPLS_LDP_EIGHT) == CLI_MPLS_LDP_EIGHT)
            {
                CliPrintf (CliHandle, "\n\rPeer Ldp Id: %s Local Ldp Id: %s,%d"
                           "\n\r        TCP Connection: %s - %s, %s - %s"
                           "\n\r        State: %s; %s"
                           "\n\r        LDP Discovery Sources:\n"
                           "\r             %s\n"
                           "\r             KeepAlive Holdtime: %d s,"
                           " KA Interval: %d s"
                           "\n             Peer KeepAlive Holdtime: %d s,"
                           " KA Time Remaining: %d s\n\n",
                           ac1PeerLdpId, ac1LocalLdpId, u4MplsLdpEntityIndex,
                           ac1PeerTransAddr, ac1LocalTransAddr,
                           CLI_MPLS_LDP_SESSION_ROLE (i4PeerSessionRole),
                           CLI_MPLS_LDP_SESSION_ROLE (i4SessionRole),
                           CLI_MPLS_SESSION_STATE (i4SessionState),
                           CLI_MPLS_LDP_DIST (i4PeerLabelDistMode),
                           (INT1 *) pu1IfName, u4KeepAliveTime,
                           u4KeepAliveInterval, u4PeerKATime, i4PeerKATimeRem);
            }
            else if ((u4Flag & CLI_MPLS_LDP_ONE) == CLI_MPLS_LDP_ONE)
            {
                /* Display the neighbor information in concise manner */
                CliPrintf (CliHandle, "\n\rPeer Ldp Id: %s Local Ldp Id: %s,"
                           "%d\n\r        TCP Connection: %s - %s, %s - %s"
                           "\n\r        State: %s; %s\n"
                           "\r          LDP Discovery Sources:\n"
                           "\r             %s\n\n",
                           ac1PeerLdpId, ac1LocalLdpId, u4MplsLdpEntityIndex,
                           ac1PeerTransAddr, ac1LocalTransAddr,
                           CLI_MPLS_LDP_SESSION_ROLE (i4PeerSessionRole),
                           CLI_MPLS_LDP_SESSION_ROLE (i4SessionRole),
                           CLI_MPLS_SESSION_STATE (i4SessionState),
                           CLI_MPLS_LDP_DIST (i4PeerLabelDistMode),
                           (INT1 *) pu1IfName);
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }                            /*end forever */
    if (LdpCliDisplayPeerGrConfigurations (CliHandle,
                                           pMplsLdpEntityLdpLsrId,
                                           u4MplsLdpEntityIndex,
                                           pMplsLdpEntityPeerId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*************************************************************************
 *  Funtion Name :  LdpDisplayDiscovery
 *  Description   : This routine displays neighbor info in normal.
 *  Input(s)      : pLdpDummy      
 *                  pc1LdpId
 *                  pc1PeerLdpId
 *                  i4AdjacencyType
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE.
 ************************************************************************/

PRIVATE INT4
LdpDisplayDiscovery (tLdpDummy1 * LdpDummy, UINT1 *pu1PeerLdpId,
                     INT4 i4AdjacencyType, UINT1 *pu1IpAddr)
{
    tSNMP_OCTET_STRING_TYPE LdpEntityLdpId;
    tSNMP_OCTET_STRING_TYPE PeerLdpId;
    tSNMP_OCTET_STRING_TYPE PeerTransportAddr;
#ifdef MPLS_IPV6_WANTED
    tSNMP_OCTET_STRING_TYPE *pMplsLdpEntityLdpLsrId = NULL;
    tSNMP_OCTET_STRING_TYPE *pu1TempPeerId = NULL;
    tSNMP_OCTET_STRING_TYPE Ip6PeerTransAddr;
    tSNMP_OCTET_STRING_TYPE Ip6LocalTransAddr;
    tIp6Addr            LPeerAddr;
    tIp6Addr            LDPLOCLAddr;
    tIp6Addr            LOCLAddr;
    tIp6Addr            TempIpv6Addr;
    tLdpPeer           *pLdpPeer = NULL;
    tNetIpv6RtInfo      NetIpv6RtInfo;
#endif
    tCliHandle          CliHandle;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4HelloAdjacencyIndex = LDP_ZERO;
    UINT4               u4HelloHoldTime = LDP_ZERO;
    UINT4               u4HelloInterval = LDP_ZERO;
    UINT4               u4PeerHoldTime = LDP_ZERO;
    UINT4               u4IpAddr = LDP_ZERO;
    UINT4               u4Mask = 0xffffffff;
    UINT1               au1PeerTransAddr[LDP_DEF_BUF_SIZE] = { LDP_ZERO };
    UINT1               au1LocalTransAddr[LDP_DEF_BUF_SIZE] = { LDP_ZERO };
#ifdef MPLS_IPV6_WANTED
    tNetIpv6RtInfoQueryMsg Ipv6RtQuery;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpId = NULL;
    UINT1               au1Ip6PeerTransAddr[LDP_IPV6_TR_ADDR_LEN] =
        { LDP_ZERO };
    UINT1               au1Ip6LocalTransAddr[LDP_IPV6_TR_ADDR_LEN] =
        { LDP_ZERO };
    INT4                i4CmpRet;
#endif
    UINT4               u4Flag = LDP_ZERO;
    UINT4               u4LocalTransAddr = LDP_ZERO;
    INT4                i4TargetHFlag = LDP_ZERO;
    INT4                i4TransAddrTlv = LDP_ZERO;
    INT4                i4TransAddrKind = LDP_ZERO;
    INT4                i4PeerHoldTimeRem = LDP_ZERO;
    CHR1               *pc1Temp = NULL;
    BOOL1               b1IsRouteFound = FALSE;
    static UINT1        au1LdpId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1PeerLdpId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1PeerAddr[IPV6_ADDR_LENGTH] = { LDP_ZERO };
    UINT4               u4IfIpIndex = LDP_ZERO;
    UINT1               u1OperStatus = CFA_DISABLED;

    UNUSED_PARAM (pu1IpAddr);
    MEMSET (&RtQuery, LDP_ZERO, sizeof (tRtInfoQueryMsg));

#ifdef MPLS_IPV6_WANTED
    MEMSET (&(LPeerAddr), 0, sizeof (tIp6Addr));
    MEMSET (&(LDPLOCLAddr), 0, sizeof (tIp6Addr));
    MEMSET (&(LOCLAddr), 0, sizeof (tIp6Addr));
    MEMSET (&(TempIpv6Addr), 0, sizeof (tIp6Addr));
    MEMSET (&(NetIpv6RtInfo), 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&(Ipv6RtQuery), 0, sizeof (tNetIpv6RtInfoQueryMsg));

    Ip6PeerTransAddr.pu1_OctetList = au1Ip6PeerTransAddr;
    Ip6PeerTransAddr.i4_Length = LDP_IPV6_TR_ADDR_LEN;

    Ip6LocalTransAddr.pu1_OctetList = au1Ip6LocalTransAddr;
    Ip6LocalTransAddr.i4_Length = LDP_IPV6_TR_ADDR_LEN;
#endif

    LdpEntityLdpId.pu1_OctetList = &au1LdpId[LDP_ZERO];
    PeerLdpId.pu1_OctetList = &au1PeerLdpId[LDP_ZERO];
    PeerTransportAddr.pu1_OctetList = &au1PeerAddr[LDP_ZERO];

    MEMCPY (LdpEntityLdpId.pu1_OctetList,
            LdpDummy->LdpEntityLdpId.pu1_OctetList,
            LdpDummy->LdpEntityLdpId.i4_Length);
    LdpEntityLdpId.i4_Length = LdpDummy->LdpEntityLdpId.i4_Length;
    MEMCPY (PeerLdpId.pu1_OctetList,
            LdpDummy->PeerLdpId.pu1_OctetList, LdpDummy->PeerLdpId.i4_Length);
    PeerLdpId.i4_Length = LdpDummy->PeerLdpId.i4_Length;
    CliHandle = LdpDummy->CliHandle;
    u4MplsLdpEntityIndex = LdpDummy->u4MplsLdpEntityIndex;
    u4HelloAdjacencyIndex = LdpDummy->u4HelloAdjacencyIndex;
    u4Flag = LdpDummy->u4Flag;

    if (nmhGetFsMplsLdpEntityTransAddrTlvEnable (&LdpEntityLdpId,
                                                 u4MplsLdpEntityIndex,
                                                 &i4TransAddrTlv)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsLdpEntityTransportAddrKind (&LdpEntityLdpId,
                                              u4MplsLdpEntityIndex,
                                              &i4TransAddrKind) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsMplsLdpEntityTransportAddress (&LdpEntityLdpId,
                                               u4MplsLdpEntityIndex,
                                               &u4LocalTransAddr)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Get the Entity related parameters */
    if (nmhGetMplsLdpEntityHelloHoldTimer (&LdpEntityLdpId,
                                           u4MplsLdpEntityIndex,
                                           &u4HelloHoldTime) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Get the peer related parameters */
    if (nmhGetMplsLdpHelloAdjacencyHoldTime (&LdpEntityLdpId,
                                             u4MplsLdpEntityIndex,
                                             &PeerLdpId,
                                             u4HelloAdjacencyIndex,
                                             &u4PeerHoldTime) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsLdpHelloAdjacencyHoldTimeRem (&LdpEntityLdpId,
                                                u4MplsLdpEntityIndex,
                                                &PeerLdpId,
                                                u4HelloAdjacencyIndex,
                                                &i4PeerHoldTimeRem)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsLdpPeerTransportAddr (&LdpEntityLdpId,
                                        u4MplsLdpEntityIndex,
                                        &PeerLdpId,
                                        &PeerTransportAddr) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetMplsLdpEntityTargetPeer (&LdpEntityLdpId,
                                       u4MplsLdpEntityIndex,
                                       &i4TargetHFlag) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (u4HelloHoldTime == LDP_ZERO)
    {
        u4HelloHoldTime = (i4TargetHFlag == LDP_SNMP_TRUE) ?
            LDP_DEF_RHELLO_HTIME : LDP_DEF_LHELLO_HTIME;
    }

    u4HelloInterval = u4HelloHoldTime;
    LDP_GET_HELLO_FREQ (u4HelloInterval);

    MPLS_OCTETSTRING_TO_INTEGER ((&PeerTransportAddr), u4IpAddr);

    RtQuery.u4DestinationIpAddress = u4IpAddr;
    RtQuery.u4DestinationSubnetMask = u4Mask;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        b1IsRouteFound = TRUE;
    }

    /* CLI_CONVERT_IPADDR_TO_STR (pc1Temp, u4IpAddr);
       MEMCPY ((UINT1 *) au1PeerTransAddr, (UINT1 *) pc1Temp, LDP_DEF_BUF_SIZE);

       CfaIpIfGetIfIndexFromIpAddress (u4LocalTransAddr, &u4IfIpIndex);
       CfaGetIfOperStatus (u4IfIpIndex, &u1OperStatus); */

#ifdef MPLS_IPV6_WANTED
    pMplsLdpEntityLdpLsrId = &LdpEntityLdpId;
    pu1TempPeerId = &PeerLdpId;
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpLsrId);
    pu1LdpId = SNMP_OCTET_STRING_LIST (pu1TempPeerId);

    Ipv6RtQuery.u4ContextId = VCM_DEFAULT_CONTEXT;
    Ipv6RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;

    if (LDP_FAILURE == LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                                      u4MplsLdpEntityIndex, pu1LdpId,
                                      &pLdpPeer))
    {
        LDP_DBG (LDP_MAIN_ALL, "MAIN: Unable to get ldp peer \n");
    }

    if ((pLdpPeer->pLdpSession->SrcTransAddr.u2AddrType) == MPLS_IPV6_ADDR_TYPE)
    {

        if (nmhGetFsMplsLdpEntityIpv6TransAddrTlvEnable (pMplsLdpEntityLdpLsrId,
                                                         u4MplsLdpEntityIndex,
                                                         &i4TransAddrTlv) ==
            SNMP_FAILURE)
        {

            return CLI_FAILURE;
        }

        if (nmhGetFsMplsLdpEntityIpv6TransportAddrKind (pMplsLdpEntityLdpLsrId,
                                                        u4MplsLdpEntityIndex,
                                                        &i4TransAddrKind) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetFsMplsLdpEntityIpv6TransportAddress (pMplsLdpEntityLdpLsrId,
                                                       u4MplsLdpEntityIndex,
                                                       &Ip6LocalTransAddr) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetMplsLdpPeerTransportAddr (pMplsLdpEntityLdpLsrId,
                                            u4MplsLdpEntityIndex, pu1TempPeerId,
                                            &Ip6PeerTransAddr) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        MEMCPY (LPeerAddr.u1_addr, Ip6PeerTransAddr.pu1_OctetList,
                LDP_IPV6_TR_ADDR_LEN);
        MEMCPY (LOCLAddr.u1_addr, Ip6LocalTransAddr.pu1_OctetList,
                LDP_IPV6_TR_ADDR_LEN);

        Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst, &LPeerAddr);
        NetIpv6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

        if (NetIpv6GetRoute (&Ipv6RtQuery, &NetIpv6RtInfo) == NETIPV6_SUCCESS)
        {
            b1IsRouteFound = TRUE;
        }

        i4CmpRet =
            MEMCMP (LOCLAddr.u1_addr, TempIpv6Addr.u1_addr,
                    LDP_IPV6_TR_ADDR_LEN);

        if ((i4TransAddrTlv == LDP_FALSE) ||
            (i4TransAddrKind == INTERFACE_ADDRESS_TYPE) || (i4CmpRet == 0))
        {
            MEMCPY (LDPLOCLAddr.u1_addr,
                    pLdpPeer->pLdpSession->SrcTransAddr.Addr.Ip6Addr.u1_addr,
                    LDP_IPV6_TR_ADDR_LEN);
        }
        else
        {
            MEMCPY (LDPLOCLAddr.u1_addr, LOCLAddr.u1_addr,
                    LDP_IPV6_TR_ADDR_LEN);
        }

        if (u4Flag == CLI_MPLS_LDP_ONE)
        {
            /* Display the Discovery information in detail */
            CliPrintf (CliHandle,
                       "\n\r          Ldp Id: %s Adjacency Type : %s",
                       (INT1 *) pu1PeerLdpId,
                       CLI_MPLS_LDP_ADJACENCY (i4AdjacencyType));

            CliPrintf (CliHandle, "\n\r              Hello Interval: %d ms;"
                       " Transport Addr: %s"
                       "\n\r              Peer Hello time : %d s; "
                       "Transport Addr: %s",
                       u4HelloInterval, Ip6PrintAddr (&LDPLOCLAddr),
                       i4PeerHoldTimeRem, Ip6PrintAddr (&LPeerAddr));

            if (b1IsRouteFound == FALSE)
            {
                CliPrintf (CliHandle, ";no route");
            }

            CliPrintf (CliHandle,
                       "\n\r              Proposed local/peer : %d/%d s\n",
                       u4HelloHoldTime, u4PeerHoldTime);
        }
        else
        {
            /* Display the discovery information in short */
            CliPrintf (CliHandle,
                       "\n\r          Ldp Id: %s Adjacency Type : %s\n",
                       (INT1 *) pu1PeerLdpId,
                       CLI_MPLS_LDP_ADJACENCY (i4AdjacencyType));
        }
    }
    else
#endif
    {
        CLI_CONVERT_IPADDR_TO_STR (pc1Temp, u4IpAddr);
        MEMCPY ((UINT1 *) au1PeerTransAddr, (UINT1 *) pc1Temp,
                LDP_DEF_BUF_SIZE);

        CfaIpIfGetIfIndexFromIpAddress (u4LocalTransAddr, &u4IfIpIndex);
        CfaGetIfOperStatus (u4IfIpIndex, &u1OperStatus);

        if ((i4TransAddrTlv == LDP_FALSE) ||
            (i4TransAddrKind == INTERFACE_ADDRESS_TYPE) ||
            (u4LocalTransAddr == LDP_ZERO) || (u1OperStatus == CFA_IF_DOWN))
        {
            if (NetIpv4GetSrcAddrToUseForDest (u4IpAddr, &u4LocalTransAddr)
                == NETIPV4_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        CLI_CONVERT_IPADDR_TO_STR (pc1Temp, u4LocalTransAddr);
        MEMCPY ((UINT1 *) au1LocalTransAddr, pc1Temp, LDP_DEF_BUF_SIZE);

        if (u4Flag == CLI_MPLS_LDP_ONE)
        {
            /* Display the Discovery information in detail */
            CliPrintf (CliHandle,
                       "\n\r          Ldp Id: %s Adjacency Type : %s",
                       (INT1 *) pu1PeerLdpId,
                       CLI_MPLS_LDP_ADJACENCY (i4AdjacencyType));

            if ((u4HelloHoldTime == LDP_INFINITE_HTIME)
                && (u4PeerHoldTime == LDP_INFINITE_HTIME))
            {
                CliPrintf (CliHandle,
                           "\n\r              Hello Interval: Infinite;"
                           " Transport Addr: %s"
                           "\n\r              Peer Hello time : Infinite; "
                           "Transport Addr: %s", (INT1 *) au1LocalTransAddr,
                           (INT1 *) au1PeerTransAddr);
            }
            else
            {
                CliPrintf (CliHandle, "\n\r              Hello Interval: %d ms;"
                           " Transport Addr: %s"
                           "\n\r              Peer Hello time : %d s; "
                           "Transport Addr: %s",
                           u4HelloInterval, (INT1 *) au1LocalTransAddr,
                           i4PeerHoldTimeRem, (INT1 *) au1PeerTransAddr);

            }
            if (b1IsRouteFound == FALSE)
            {
                CliPrintf (CliHandle, ";no route");
            }
            CliPrintf (CliHandle,
                       "\n\r              Proposed local/peer : %d/%d s\n",
                       u4HelloHoldTime, u4PeerHoldTime);
        }
        else
        {
            /* Display the discovery information in short */
            CliPrintf (CliHandle,
                       "\n\r          Ldp Id: %s Adjacency Type : %s\n",
                       (INT1 *) pu1PeerLdpId,
                       CLI_MPLS_LDP_ADJACENCY (i4AdjacencyType));
        }
    }
    return CLI_SUCCESS;
}

/*************************************************************************
 *  Funtion Name :  LdpCliDbgSetMsgDumpAttr
 *  Description   : This routine sets the dumping of various messages 
 *  Input(s)      : CliHandle - Handle to cli     
 *                  i4DbgMsgType - Dumping message type
 *                  i4DbgMsgDir - Direction of message dumping
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE.
 ************************************************************************/

INT4
LdpCliDbgSetMsgDumpAttr (tCliHandle CliHandle, INT4 i4DbgMsgType,
                         INT4 i4DbgMsgDir)
{
    INT4                i4DbgMsgOldType = LDP_ZERO;
    UNUSED_PARAM (CliHandle);

    nmhSetFsMplsCrlspDumpDirection (i4DbgMsgDir);

    switch (i4DbgMsgType)
    {
        case CLI_MPLS_LDP_DUMP_ADDRESS:
        {
            i4DbgMsgType = LDP_DUMP_ADDR;
            break;
        }
        case CLI_MPLS_LDP_DUMP_HELLO:
        {
            i4DbgMsgType = LDP_DUMP_HELLO;
            break;
        }
        case CLI_MPLS_LDP_DUMP_INIT:
        {
            i4DbgMsgType = LDP_DUMP_INIT;
            break;
        }
        case CLI_MPLS_LDP_DUMP_KA:
        {
            i4DbgMsgType = LDP_DUMP_KEEPALIVE;
            break;
        }
        case CLI_MPLS_LDP_DUMP_MAP:
        {
            i4DbgMsgType = LDP_DUMP_LDP_MAP;
            break;
        }
        case CLI_MPLS_LDP_DUMP_NOTIF:
        {
            i4DbgMsgType = LDP_DUMP_LDP_NOTIF;
            break;
        }
        case CLI_MPLS_LDP_DUMP_REL:
        {
            i4DbgMsgType = LDP_DUMP_LDP_REL;
            break;
        }
        case CLI_MPLS_LDP_DUMP_RQ:
        {
            i4DbgMsgType = LDP_DUMP_LDP_RQ;
            break;
        }
        case CLI_MPLS_LDP_DUMP_WITHDRAW:
        {
            i4DbgMsgType = LDP_DUMP_LDP_WDRAW;
            break;
        }
        default:
        {
            i4DbgMsgType = LDP_DUMP_ALL;
            break;
        }
    }

    nmhGetFsMplsCrlspDumpType (&i4DbgMsgOldType);
    i4DbgMsgType |= i4DbgMsgOldType;
    nmhSetFsMplsCrlspDumpType (i4DbgMsgType);
    return CLI_SUCCESS;
}

/*************************************************************************
 *  Funtion Name :  LdpCliDbgResetMsgDumpAttr
 *  Description   : This routine sets the dumping of various messages 
 *  Input(s)      : CliHandle - Handle to cli     
 *                  i4DbgMsgType - Dumping message type
 *                  i4DbgMsgDir - Direction of message dumping
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE.
 ************************************************************************/
INT4
LdpCliDbgResetMsgDumpAttr (tCliHandle CliHandle, INT4 i4DbgMsgType,
                           INT4 i4DbgMsgDir)
{
    INT4                i4DbgMsgOldType = LDP_ZERO;
    UNUSED_PARAM (CliHandle);

    if (i4DbgMsgDir == MPLS_CLI_NONE)
    {
        nmhSetFsMplsCrlspDumpDirection (LDP_DUMP_DIRN_NONE);
    }

    switch (i4DbgMsgType)
    {
        case CLI_MPLS_LDP_DUMP_ADDRESS:
        {
            i4DbgMsgType = LDP_DUMP_ADDR;
            break;
        }
        case CLI_MPLS_LDP_DUMP_HELLO:
        {
            i4DbgMsgType = LDP_DUMP_HELLO;
            break;
        }
        case CLI_MPLS_LDP_DUMP_INIT:
        {
            i4DbgMsgType = LDP_DUMP_INIT;
            break;
        }
        case CLI_MPLS_LDP_DUMP_KA:
        {
            i4DbgMsgType = LDP_DUMP_KEEPALIVE;
            break;
        }
        case CLI_MPLS_LDP_DUMP_MAP:
        {
            i4DbgMsgType = LDP_DUMP_LDP_MAP;
            break;
        }
        case CLI_MPLS_LDP_DUMP_NOTIF:
        {
            i4DbgMsgType = LDP_DUMP_LDP_NOTIF;
            break;
        }
        case CLI_MPLS_LDP_DUMP_REL:
        {
            i4DbgMsgType = LDP_DUMP_LDP_REL;
            break;
        }
        case CLI_MPLS_LDP_DUMP_RQ:
        {
            i4DbgMsgType = LDP_DUMP_LDP_RQ;
            break;
        }
        case CLI_MPLS_LDP_DUMP_WITHDRAW:
        {
            i4DbgMsgType = LDP_DUMP_LDP_WDRAW;
            break;
        }
        case CLI_MPLS_LDP_DUMP_ALL:
        {
            i4DbgMsgType = LDP_DUMP_ALL;
            break;
        }
        default:
        {
            i4DbgMsgType = LDP_DUMP_NONE;
            break;
        }
    }

    nmhGetFsMplsCrlspDumpType (&i4DbgMsgOldType);
    i4DbgMsgType &= ~(i4DbgMsgOldType);
    nmhSetFsMplsCrlspDumpType (i4DbgMsgType);

    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : LdpCliShowRunningConfig
 * * Description   : This routine displays the ldp configuration
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * ******************************************************************************/
INT4
LdpCliShowRunningConfig (tCliHandle CliHandle)
{

    /* Ldp Entity Configuration */
    LdpCliEntityShowRunningConfig (CliHandle);

    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : LdpCliFsMplsShowRunningConfig
 * * Description   : This routine displays the Ldp configuration
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * ******************************************************************************/
INT4
LdpCliFsMplsShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE RouterId;
    UINT4               u4RouterId = LDP_ZERO;
    UINT4               u4Port = LDP_ZERO;
    UINT4               u4L3IfIndex = LDP_ZERO;
    UINT4               u4IpAddr = LDP_ZERO;
    INT4                i4AllocMethod = LDP_ZERO;
    INT4                i4ForceOption = LDP_ZERO;
    INT4                i4ConfigSeqTlvOption = LDP_ZERO;
    CHR1               *pc1String = NULL;
    INT1               *pi1IfName = NULL;
    UINT1               u1IfType = LDP_ZERO;
    UINT1               au1IpId[CFA_MAX_PORT_NAME_LENGTH] = { LDP_ZERO };
    UINT1               au1LdpRouterId[LDP_LSR_ID_LEN] = { LDP_ZERO };
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { LDP_ZERO };
    UINT1               au1tempRouterId[LDP_LSR_ID_LEN] = { LDP_ZERO };

    pi1IfName = (INT1 *) &au1IfName[0];
    RouterId.pu1_OctetList = au1LdpRouterId;
    RouterId.i4_Length = LDP_LSR_ID_LEN;
    pc1String = (CHR1 *) & au1IpId[0];

    /*To retrieve the Router-id */

#ifdef ISS_WANTED
    u4IpAddr = IssGetIpAddrFromNvRam ();
#else
    NetIpv4GetHighestIpAddr (&u4IpAddr);
#endif

    if (MEMCMP
        (LDP_NEW_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr, au1tempRouterId,
         LDP_LSR_ID_LEN) != LDP_ZERO)
    {
        nmhGetFsMplsLdpLsrId (&RouterId);
        MPLS_OCTETSTRING_TO_INTEGER ((&RouterId), u4RouterId);
        if (u4IpAddr != u4RouterId)
        {
            if (NETIPV4_SUCCESS == NetIpv4GetIfIndexFromAddr (u4RouterId,
                                                              &u4Port))
            {
                if (NETIPV4_SUCCESS ==
                    NetIpv4GetCfaIfIndexFromPort (u4Port, &u4L3IfIndex))
                {
                    if (CFA_SUCCESS == CfaGetIfaceType (u4L3IfIndex, &u1IfType))
                    {
                        if (CFA_NONE == u1IfType)
                        {
                            CLI_CONVERT_IPADDR_TO_STR (pc1String, u4RouterId);
                            CliPrintf (CliHandle, "\nrouter-id %s", pc1String);
                            FilePrintf (CliHandle, "\nrouter-id %s", pc1String);
                        }
                        else
                        {
                            if (CFA_SUCCESS ==
                                CfaCliConfGetIfName (u4L3IfIndex, pi1IfName))
                            {
                                CliPrintf (CliHandle, "\nrouter-id %s ",
                                           pi1IfName);
                                FilePrintf (CliHandle, "\nrouter-id %s ",
                                            pi1IfName);
                            }
                        }
                    }
                }
            }
            else
            {
                /* This is the case to handle the scenario when 
                 * the IP given in the router-id command is not assigned to any other 
                 * interface in the system. In SRC, then IP will be shown instead of interface */
                CliPrintf (CliHandle, "\nrouter-id %d.%d.%d.%d ",
                           au1LdpRouterId[0], au1LdpRouterId[1],
                           au1LdpRouterId[2], au1LdpRouterId[3]);
                FilePrintf (CliHandle, "\nrouter-id %d.%d.%d.%d ",
                            au1LdpRouterId[0], au1LdpRouterId[1],
                            au1LdpRouterId[2], au1LdpRouterId[3]);
            }
            nmhGetFsMplsLdpForceOption (&i4ForceOption);
            if (i4ForceOption == LDP_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "force");
                FilePrintf (CliHandle, "force");
            }
        }
    }

    /* To retrieve the Label Allocation Method */

    nmhGetFsMplsLsrLabelAllocationMethod (&i4AllocMethod);
    if (MPLS_LDP_ORDERED_MODE != i4AllocMethod)
    {
        CliPrintf (CliHandle, "\nlabel allocation independent");
        FilePrintf (CliHandle, "\nlabel allocation independent");
    }

    nmhGetFsMplsLdpConfigurationSequenceTLVEnable (&i4ConfigSeqTlvOption);

    if (LDP_SNMP_TRUE == i4ConfigSeqTlvOption)
    {
        CliPrintf (CliHandle, "\nconfiguration-sequence-tlv enable");
        FilePrintf (CliHandle, "\nconfiguration-sequence-tlv enable");
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : LdpCliEntityShowRunningConfig
 * * Description   : This routine displays the ldp entity configuration
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * ***************************************************************************** */
INT4
LdpCliEntityShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE LdpEntityLdpId;
    tSNMP_OCTET_STRING_TYPE prevLdpEntityLdpId;
    tSNMP_OCTET_STRING_TYPE PeerAddr;
    UINT4               u4LdpEntityIndex = LDP_ZERO;
    UINT4               u4prevLdpEntityIndex = LDP_ZERO;
    UINT4               u4HelloHoldTime = LDP_ZERO;
    UINT4               u4KeepAliveHoldTime = LDP_ZERO;
    UINT4               u4PeerAddr = LDP_ZERO;
    INT4                i4HopCount = LDP_ZERO;
    INT4                i4LabelDistMode = LDP_ZERO;
    INT4                i4LabelRetentionMode = LDP_ZERO;
#ifdef MPLS_LDP_BFD_WANTED
    INT4                i4BfdStatus = LDP_ZERO;
#endif
    INT4                i4PhpMode = LDP_ONE;
    INT4                i4TransIfType = LOOPBACK_ADDRESS_TYPE;
    INT4                i4TargetFlag = LDP_SNMP_FALSE;
    INT4                i4CmdType = CLI_DISABLE;
    INT4                i4AdminStatus = LDP_ADMIN_DOWN;
    INT4                i4RowStatus = DESTROY;
    BOOL1               bloop = FALSE;
    BOOL1               bpolicy = FALSE;
    CHR1               *pc1String = NULL;
    INT1               *pi1IfName = NULL;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    static UINT1        au1LdpLsrId1[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT1               au1PeerAddr[IPV6_ADDR_LENGTH] = { LDP_ZERO };
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { LDP_ZERO };
    UINT1               au1IpId[CFA_MAX_PORT_NAME_LENGTH] = { LDP_ZERO };
    UINT4               u4TransportAddr = LDP_ZERO;
    UINT4               u4Port = LDP_ZERO;
    UINT4               u4L3IfIndex = LDP_ZERO;
    INT4                i4LdpOverRsvpEnable = LDP_ZERO;
    UINT4               u4InTunnelIndex = LDP_ZERO;
    UINT4               u4OutTunnelIndex = LDP_ZERO;
    INT4                i4PathVectorLimit = 0;
    tLdpEntity         *pLdpEntity = NULL;

#ifdef MPLS_IPV6_WANTED
    tIp6Addr            Ipv6TargetPeerAddr;
    MEMSET (&Ipv6TargetPeerAddr, 0, sizeof (tIp6Addr));
#endif

    UNUSED_PARAM (u4L3IfIndex);
    UNUSED_PARAM (u4Port);
    UNUSED_PARAM (u4TransportAddr);
    pc1String = (CHR1 *) & au1IpId[0];
    pi1IfName = (INT1 *) &au1IfName[0];
    LdpEntityLdpId.pu1_OctetList = au1LdpLsrId;
    LdpEntityLdpId.i4_Length = LDP_MAX_LDPID_LEN;
    prevLdpEntityLdpId.pu1_OctetList = au1LdpLsrId1;
    prevLdpEntityLdpId.i4_Length = LDP_MAX_LDPID_LEN;
    PeerAddr.pu1_OctetList = au1PeerAddr;
    PeerAddr.i4_Length = LDP_LSR_ID_LEN;

    LdpCliGrShowRunningConfig (CliHandle);

    if (CLI_ENABLE == gLdpInfo.u1Policy)
    {
        CliPrintf (CliHandle, "\n!");
        FilePrintf (CliHandle, "\n!");
        CliPrintf (CliHandle, "\nmpls ldp advertise-labels connected-only");
        FilePrintf (CliHandle, "\nmpls ldp advertise-labels connected-only");
        bpolicy = TRUE;
    }

    while (1)
    {
        if ((TRUE == bloop) &&
            (SNMP_FAILURE ==
             nmhGetNextIndexMplsLdpEntityTable (&prevLdpEntityLdpId,
                                                &LdpEntityLdpId,
                                                u4prevLdpEntityIndex,
                                                &u4LdpEntityIndex)))
        {
            break;
        }
        if ((FALSE == bloop) &&
            (SNMP_FAILURE ==
             nmhGetFirstIndexMplsLdpEntityTable (&LdpEntityLdpId,
                                                 &u4LdpEntityIndex)))
        {
            break;
        }

        if (FALSE == bpolicy)
        {
            CliPrintf (CliHandle, "\n!");
            FilePrintf (CliHandle, "\n!");
            CliPrintf (CliHandle, "\nmpls ldp");
            FilePrintf (CliHandle, "\nmpls ldp");
        }

        /* To Retrieve the router-id configuration */
        if (bloop == FALSE)
        {
            LdpCliFsMplsShowRunningConfig (CliHandle);
        }

        bloop = TRUE;
        u4prevLdpEntityIndex = u4LdpEntityIndex;
        MEMCPY (prevLdpEntityLdpId.pu1_OctetList,
                LdpEntityLdpId.pu1_OctetList, LDP_MAX_LDPID_LEN);

        CliPrintf (CliHandle, "\nentity %d", u4LdpEntityIndex);
        FilePrintf (CliHandle, "\nentity %d", u4LdpEntityIndex);
        /* To retrieve Keep Alive Hold Time */

        if (SNMP_SUCCESS ==
            nmhGetMplsLdpEntityKeepAliveHoldTimer (&LdpEntityLdpId,
                                                   u4LdpEntityIndex,
                                                   &u4KeepAliveHoldTime))
        {
            if (LDP_KEEPALIVE_DEFAULT_TIME != u4KeepAliveHoldTime)
            {
                CliPrintf (CliHandle, "\nholdtime %d", u4KeepAliveHoldTime);
                FilePrintf (CliHandle, "\nholdtime %d", u4KeepAliveHoldTime);
            }

        }

        /* To retrieve Hop Count Limit */
        /*Do not print deafult max-hop value 0 */
        if (LDP_LOOP_DETECT (MPLS_DEF_INCARN) != LDP_LOOP_DETECT_NOT_CAPABLE)
        {

            if (SNMP_SUCCESS ==
                nmhGetMplsLdpEntityHopCountLimit (&LdpEntityLdpId,
                                                  u4LdpEntityIndex,
                                                  &i4HopCount))
            {
                CliPrintf (CliHandle, "\nmax-hops %d", i4HopCount);
                FilePrintf (CliHandle, "\nmax-hops %d", i4HopCount);
            }
        }

        /* To retrieve Label Distribution Method */

        if (SNMP_SUCCESS ==
            nmhGetMplsLdpEntityLabelDistMethod (&LdpEntityLdpId,
                                                u4LdpEntityIndex,
                                                &i4LabelDistMode))
        {
            if (MPLS_LDP_ON_DEMAND_MODE != i4LabelDistMode)
            {
                CliPrintf (CliHandle, "\nlabel distribution unsolicited");
                FilePrintf (CliHandle, "\nlabel distribution unsolicited");
            }

        }

        /* To retriev Label Retention Mode */

        if (SNMP_SUCCESS ==
            nmhGetMplsLdpEntityLabelRetentionMode (&LdpEntityLdpId,
                                                   u4LdpEntityIndex,
                                                   &i4LabelRetentionMode))
        {
            if (LDP_CONSERVATIVE_MODE != i4LabelRetentionMode)
            {
                CliPrintf (CliHandle, "\nlabel retention liberal");
                FilePrintf (CliHandle, "\nlabel retention liberal");
            }
        }

        /* To retrieve Php Mode */

        if (SNMP_SUCCESS ==
            nmhGetFsMplsLdpEntityPHPRequestMethod (&LdpEntityLdpId,
                                                   u4LdpEntityIndex,
                                                   &i4PhpMode))
        {
            if (LDP_ONE != i4PhpMode)
            {
                if (MPLS_LDP_EXPLICIT_NULL_LABEL == i4PhpMode)
                {
                    CliPrintf (CliHandle, "\nencapsulate explicit-null");
                    FilePrintf (CliHandle, "\nencapsulate explicit-null");
                }
                if (MPLS_LDP_IMPLICIT_NULL_LABEL == i4PhpMode)
                {
                    CliPrintf (CliHandle, "\nencapsulate implicit-null");
                    FilePrintf (CliHandle, "\nencapsulate implicit-null");
                }
            }

        }
#ifdef MPLS_LDP_BFD_WANTED
        /* To retrieve Bfd Status */
        if (SNMP_SUCCESS ==
            nmhGetFsMplsLdpEntityBfdStatus (&LdpEntityLdpId,
                                            u4LdpEntityIndex, &i4BfdStatus))
        {
            if (LDP_BFD_ENABLED == i4BfdStatus)
            {
                CliPrintf (CliHandle, "\nenable bfd");
                FilePrintf (CliHandle, "\nenable bfd");
            }
        }
#endif
        /* To retrieve the Targeted neighbor */

        if (SNMP_SUCCESS ==
            nmhGetMplsLdpEntityTargetPeer (&LdpEntityLdpId,
                                           u4LdpEntityIndex, &i4TargetFlag))
        {
            if (LDP_SNMP_TRUE == i4TargetFlag)
            {

                if (SNMP_SUCCESS ==
                    nmhGetMplsLdpEntityTargetPeerAddr (&LdpEntityLdpId,
                                                       u4LdpEntityIndex,
                                                       &PeerAddr))
                {
#ifdef MPLS_IPV6_WANTED
                    if (PeerAddr.i4_Length == IPV6_ADDR_LENGTH)
                    {
                        MEMCPY (Ipv6TargetPeerAddr.u1_addr,
                                PeerAddr.pu1_OctetList, PeerAddr.i4_Length);

                        CliPrintf (CliHandle, "\nneighbor %s targeted",
                                   Ip6PrintAddr (&Ipv6TargetPeerAddr));
                        FilePrintf (CliHandle, "\nneighbor %s targeted",
                                    Ip6PrintAddr (&Ipv6TargetPeerAddr));
                    }
                    else
                    {
#endif
                        MPLS_OCTETSTRING_TO_INTEGER ((&PeerAddr), u4PeerAddr);
                        CLI_CONVERT_IPADDR_TO_STR (pc1String, u4PeerAddr);
                        if (u4PeerAddr != LDP_ZERO)
                        {
                            CliPrintf (CliHandle, "\nneighbor %s targeted",
                                       pc1String);
                            FilePrintf (CliHandle, "\nneighbor %s targeted",
                                        pc1String);
                        }
#ifdef MPLS_IPV6_WANTED
                    }
#endif
                }

            }

            if (SNMP_SUCCESS ==
                nmhGetFsMplsLdpEntityLdpOverRsvpEnable (&LdpEntityLdpId,
                                                        u4LdpEntityIndex,
                                                        &i4LdpOverRsvpEnable))
            {
                if (i4LdpOverRsvpEnable == LDP_SNMP_TRUE)
                {

                    CliPrintf (CliHandle, " te");
                    FilePrintf (CliHandle, " te");

                    nmhGetFsMplsLdpEntityInTunnelIndex (&LdpEntityLdpId,
                                                        u4LdpEntityIndex,
                                                        &u4InTunnelIndex);
                    if (u4InTunnelIndex != LDP_ZERO)
                    {
                        CliPrintf (CliHandle, " in %d", u4InTunnelIndex);
                        FilePrintf (CliHandle, " in %d", u4InTunnelIndex);
                    }

                    nmhGetFsMplsLdpEntityOutTunnelIndex (&LdpEntityLdpId,
                                                         u4LdpEntityIndex,
                                                         &u4OutTunnelIndex);
                    if (u4OutTunnelIndex != LDP_ZERO)
                    {
                        CliPrintf (CliHandle, " out %d", u4OutTunnelIndex);
                        FilePrintf (CliHandle, " out %d", u4OutTunnelIndex);
                    }
                }
            }

        }

        /* To retrieve Label Range */

        LdpCliEntityGLRShowRunningConfig (CliHandle,
                                          &LdpEntityLdpId, u4LdpEntityIndex);

        /* To retrieve the Ipv4 Transport Address tlv */

        if (SNMP_SUCCESS ==
            nmhGetFsMplsLdpEntityTransAddrTlvEnable (&LdpEntityLdpId,
                                                     u4LdpEntityIndex,
                                                     &i4CmdType))
        {

            if (CLI_ENABLE == i4CmdType)
            {
                if (SNMP_SUCCESS ==
                    nmhGetMplsLdpEntityTransportAddrKind (&LdpEntityLdpId,
                                                          u4LdpEntityIndex,
                                                          &i4TransIfType))
                {

                    if (MPLS_LDP_INTERFACE_ADDRESS_TYPE == i4TransIfType)
                    {
                        CliPrintf (CliHandle, "\ntransport-address tlv "
                                   "interface ");
                        FilePrintf (CliHandle, "\ntransport-address tlv "
                                    "interface ");
                    }
                    else
                    {
                        if (LdpGetLdpEntity
                            (MPLS_DEF_INCARN, LdpEntityLdpId.pu1_OctetList,
                             u4LdpEntityIndex, &pLdpEntity) == LDP_SUCCESS)
                        {

                            if (pLdpEntity->u4TransIfIndex != LDP_ZERO)
                            {

                                if (CFA_SUCCESS ==
                                    CfaCliConfGetIfName (pLdpEntity->
                                                         u4TransIfIndex,
                                                         pi1IfName))
                                {
                                    CliPrintf (CliHandle,
                                               "\ntransport-address tlv "
                                               "%s", pi1IfName);
                                    FilePrintf (CliHandle,
                                                "\ntransport-address tlv "
                                                "%s", pi1IfName);
                                }
                            }
                        }
                    }
                }
            }
        }

#ifdef MPLS_IPV6_WANTED
        /* To retrieve the Ipv6 Transport Address tlv */

        if (SNMP_SUCCESS ==
            nmhGetFsMplsLdpEntityIpv6TransAddrTlvEnable (&LdpEntityLdpId,
                                                         u4LdpEntityIndex,
                                                         &i4CmdType))
        {

            if (CLI_ENABLE == i4CmdType)
            {
                if (SNMP_SUCCESS ==
                    nmhGetFsMplsLdpEntityIpv6TransportAddrKind (&LdpEntityLdpId,
                                                                u4LdpEntityIndex,
                                                                &i4TransIfType))
                {

                    if (MPLS_LDP_INTERFACE_ADDRESS_TYPE == i4TransIfType)
                    {
                        CliPrintf (CliHandle, "\ntransport-address-ipv6 tlv "
                                   "interface ");
                        FilePrintf (CliHandle, "\ntransport-address-ipv6 tlv "
                                    "interface ");
                    }
                    else
                    {
                        if (LdpGetLdpEntity
                            (MPLS_DEF_INCARN, LdpEntityLdpId.pu1_OctetList,
                             u4LdpEntityIndex, &pLdpEntity) == LDP_SUCCESS)
                        {

                            if (pLdpEntity->u4Ipv6TransIfIndex != LDP_ZERO)
                            {

                                if (CFA_SUCCESS ==
                                    CfaCliConfGetIfName (pLdpEntity->
                                                         u4Ipv6TransIfIndex,
                                                         pi1IfName))
                                {
                                    CliPrintf (CliHandle,
                                               "\ntransport-address-ipv6 tlv "
                                               "%s", pi1IfName);
                                    FilePrintf (CliHandle,
                                                "\ntransport-address-ipv6 tlv "
                                                "%s", pi1IfName);
                                }
                            }
                        }
                    }
                }
            }
        }
#endif
        /* To retrieve Discovery Hello Hold Time */

        if (SNMP_SUCCESS ==
            nmhGetMplsLdpEntityHelloHoldTimer (&LdpEntityLdpId,
                                               u4LdpEntityIndex,
                                               &u4HelloHoldTime))
        {
            if (LDP_SNMP_TRUE == i4TargetFlag)
            {
                if ((LDP_DEF_RHELLO_HTIME != u4HelloHoldTime) &&
                    (LDP_ZERO != u4HelloHoldTime))
                {
                    CliPrintf (CliHandle, "\ndiscovery hello holdtime %d",
                               u4HelloHoldTime);
                    FilePrintf (CliHandle, "\ndiscovery hello holdtime %d",
                                u4HelloHoldTime);
                }
            }
            else
            {
                if ((LDP_DEF_LHELLO_HTIME != u4HelloHoldTime) &&
                    (LDP_ZERO != u4HelloHoldTime))
                {
                    CliPrintf (CliHandle, "\ndiscovery hello holdtime %d",
                               u4HelloHoldTime);
                    FilePrintf (CliHandle, "\ndiscovery hello holdtime %d",
                                u4HelloHoldTime);
                }
            }
        }

        /* To retrivr the path vector limit */

        if (SNMP_SUCCESS ==
            nmhGetMplsLdpEntityPathVectorLimit (&LdpEntityLdpId,
                                                u4LdpEntityIndex,
                                                &i4PathVectorLimit)
            && (i4PathVectorLimit != 0))
        {
            CliPrintf (CliHandle, "\nmpls ldp path-vector maxlength %d",
                       i4PathVectorLimit);
            FilePrintf (CliHandle, "\nmpls ldp path-vector maxlength %d",
                        i4PathVectorLimit);
        }

        /* To retrieve no shutdown */

        if (SNMP_SUCCESS ==
            nmhGetMplsLdpEntityAdminStatus (&LdpEntityLdpId,
                                            u4LdpEntityIndex, &i4AdminStatus))
        {
            if (SNMP_SUCCESS ==
                nmhGetMplsLdpEntityRowStatus (&LdpEntityLdpId,
                                              u4LdpEntityIndex, &i4RowStatus))
            {
                if ((LDP_ADMIN_UP == i4AdminStatus) && (ACTIVE == i4RowStatus))
                {
                    CliPrintf (CliHandle, "\nno shutdown ");
                    FilePrintf (CliHandle, "\nno shutdown ");
                }
                else if ((LDP_ADMIN_DOWN == i4AdminStatus)
                         && (NOT_IN_SERVICE == i4RowStatus))
                {
                    CliPrintf (CliHandle, "\nshutdown ");
                    FilePrintf (CliHandle, "\nshutdown ");
                }
            }
        }
        CliPrintf (CliHandle, "\n\rexit\n\r");
        FilePrintf (CliHandle, "\n\rexit\n\r");
        CliPrintf (CliHandle, "!\n\r");
        FilePrintf (CliHandle, "!\n\r");
    }

    return CLI_SUCCESS;
}

/******************************************************************************  
 * * Function Name : LdpCliEntityGLRShowRunningConfig 
 * * Description   : This routine displays the ldp Generic Label Range 
 * *                 configuration
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************  */
INT4
LdpCliEntityGLRShowRunningConfig (tCliHandle CliHandle,
                                  tSNMP_OCTET_STRING_TYPE * pLdpEntityLdpId,
                                  UINT4 u4LdpEntityIndex)
{
    UINT4               u4LdpEntityGenericLRMin = LDP_ZERO;
    UINT4               u4LdpEntityGenericLRMax = LDP_ZERO;
    UINT4               u4PrevLdpEntityGenericLRMin = LDP_ZERO;
    UINT4               u4PrevLdpEntityGenericLRMax = LDP_ZERO;
    UINT4               u4IfIndex = LDP_ZERO;
    INT4                i4MplsIfIndex = LDP_ZERO;
    UINT4               u4LdpCurrEntityIndex = u4LdpEntityIndex;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { LDP_ZERO };
    INT1               *pi1IfName = NULL;
    tSNMP_OCTET_STRING_TYPE *pLdpCurrEntityLdpId = pLdpEntityLdpId;

    pi1IfName = (INT1 *) &au1IfName[0];

    while (SNMP_SUCCESS ==
           nmhGetNextIndexMplsLdpEntityGenericLRTable (pLdpCurrEntityLdpId,
                                                       pLdpEntityLdpId,
                                                       u4LdpCurrEntityIndex,
                                                       &u4LdpEntityIndex,
                                                       u4PrevLdpEntityGenericLRMin,
                                                       &u4LdpEntityGenericLRMin,
                                                       u4PrevLdpEntityGenericLRMax,
                                                       &u4LdpEntityGenericLRMax))
    {
        if (u4LdpEntityIndex != u4LdpCurrEntityIndex)
        {
            break;
        }
        if (SNMP_SUCCESS ==
            nmhGetMplsLdpEntityGenericIfIndexOrZero (pLdpEntityLdpId,
                                                     u4LdpEntityIndex,
                                                     u4LdpEntityGenericLRMin,
                                                     u4LdpEntityGenericLRMax,
                                                     &i4MplsIfIndex))
        {
            CliPrintf (CliHandle,
                       "\nldp label range min %d max %d",
                       u4LdpEntityGenericLRMin, u4LdpEntityGenericLRMax);
            FilePrintf (CliHandle, "\nldp label range min %d max %d",
                        u4LdpEntityGenericLRMin, u4LdpEntityGenericLRMax);

            if (CFA_SUCCESS ==
                CfaUtilGetL3IfFromMplsIf (i4MplsIfIndex, &u4IfIndex, TRUE))
            {
                if (CFA_SUCCESS == CfaCliConfGetIfName (u4IfIndex, pi1IfName))
                {
                    CliPrintf (CliHandle, " interface %s ", pi1IfName);
                    FilePrintf (CliHandle, " interface %s ", pi1IfName);
                }
            }
        }
        u4PrevLdpEntityGenericLRMin = u4LdpEntityGenericLRMin;
        u4PrevLdpEntityGenericLRMax = u4LdpEntityGenericLRMax;
    }
    return LDP_SUCCESS;
}

/******************************************************************************  
 * * Function Name : LdpCliSetGrCapability 
 * * Description   : This routine Sets the Graceful Restart Capability of the
 * *                 Router
 * * Input(s)      : CliHandle - Cli Context Handle
 *                   i4GrCapability - Configuration done by user
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * *****************************************************************************/
INT4
LdpCliSetGrCapability (tCliHandle CliHandle, INT4 i4GrCapability)
{
    UINT4               u4Errcode = LDP_ZERO;

    if (nmhTestv2FsMplsLdpGrCapability (&u4Errcode, i4GrCapability) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r %%Unable to Set GR Capability\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsMplsLdpGrCapability (i4GrCapability);

    return CLI_SUCCESS;
}

/******************************************************************************  
 * * Function Name : LdpCliSetNbrLivenessTime 
 * * Description   : This routine Sets the Neighbour Aliveness Time for a Router
 * * Input(s)      : CliHandle - Cli Context Handle
 *                   u1GrCapability - Configuration done by user
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * *****************************************************************************/
INT4
LdpCliSetNbrLivenessTime (tCliHandle CliHandle, INT4 i4NbrAliveTime)
{
    UINT4               u4Errcode = LDP_ZERO;

    if (i4NbrAliveTime == -1)
    {
        i4NbrAliveTime = LDP_DEF_NBR_LIVENESS_TIME;
    }

    if (nmhTestv2FsMplsLdpGrNeighborLivenessTime (&u4Errcode,
                                                  i4NbrAliveTime) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r %%Unable to Set NeigborAliveness Time\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsMplsLdpGrNeighborLivenessTime (i4NbrAliveTime);

    return CLI_SUCCESS;
}

/******************************************************************************  
 * * Function Name : LdpCliSetFwdHoldTime
 * * Description   : This routine Sets the Forward Holding Time for a Router 
 * * Input(s)      : CliHandle - Cli Context Handle
 *                   i4FwdHoldTime - Configuration done by user
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * *****************************************************************************/
INT4
LdpCliSetFwdHoldTime (tCliHandle CliHandle, INT4 i4FwdHoldTime)
{
    UINT4               u4Errcode = LDP_ZERO;

    if (i4FwdHoldTime == -1)
    {
        i4FwdHoldTime = LDP_DEF_FWD_HOLDING_TIME;
    }

    if (nmhTestv2FsMplsLdpGrForwardEntryHoldTime (&u4Errcode,
                                                  i4FwdHoldTime) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r %%Unable to Set Forward Holding Time\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsMplsLdpGrForwardEntryHoldTime (i4FwdHoldTime);

    return CLI_SUCCESS;
}

/******************************************************************************  
 * * Function Name : LdpCliSetMaxRecoveryTime
 * * Description   : This routine Sets the Forward Holding Time for a Router 
 * * Input(s)      : CliHandle - Cli Context Handle
 *                   i4RecoveryTime - Configuration done by user
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * *****************************************************************************/
INT4
LdpCliSetMaxRecoveryTime (tCliHandle CliHandle, INT4 i4RecoveryTime)
{
    UINT4               u4Errcode = LDP_ZERO;

    if (i4RecoveryTime == -1)
    {
        i4RecoveryTime = LDP_DEF_MAX_RECOVERY_TIME;
    }

    if (nmhTestv2FsMplsLdpGrMaxRecoveryTime (&u4Errcode, i4RecoveryTime) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r %%Unable to Set Max Recovery Time\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsMplsLdpGrMaxRecoveryTime (i4RecoveryTime);

    return CLI_SUCCESS;
}

/******************************************************************************  
 * * Function Name : LdpCliGrShowRunningConfig
 * * Description   : This routine Displays the Configurations of GR parameters
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : NONE
 * *****************************************************************************/
VOID
LdpCliGrShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4NbrTime = LDP_ZERO;
    INT4                i4FwdHoldTime = LDP_ZERO;
    INT4                i4RecoveryTime = LDP_ZERO;
    INT4                i4GrCapability = LDP_GR_CAPABILITY_NONE;

    nmhGetFsMplsLdpGrCapability (&i4GrCapability);
    if (i4GrCapability == LDP_GR_CAPABILITY_NONE)
    {
        return;
    }

    CliPrintf (CliHandle, "\n!");
    FilePrintf (CliHandle, "\n!");
    CliPrintf (CliHandle, "\nmpls ldp");
    FilePrintf (CliHandle, "\nmpls ldp");

    /* Get the GR Related Configurations */
    nmhGetFsMplsLdpGrForwardEntryHoldTime (&i4FwdHoldTime);
    nmhGetFsMplsLdpGrMaxRecoveryTime (&i4RecoveryTime);
    nmhGetFsMplsLdpGrNeighborLivenessTime (&i4NbrTime);

    if (i4GrCapability != LDP_GR_CAPABILITY_NONE)
    {
        CliPrintf (CliHandle, "\nmpls ldp graceful-restart ");
        FilePrintf (CliHandle, "\nmpls ldp graceful-restart ");
        if (i4GrCapability == LDP_GR_CAPABILITY_FULL)
        {
            CliPrintf (CliHandle, "full");
            FilePrintf (CliHandle, "full");
        }
        else
        {
            CliPrintf (CliHandle, "help-neighbour");
            FilePrintf (CliHandle, "help-neighbour");
        }
    }

    if (i4FwdHoldTime != LDP_DEF_FWD_HOLDING_TIME)
    {
        CliPrintf (CliHandle, "\nmpls ldp graceful-restart timers "
                   "forwarding-holding %d", i4FwdHoldTime);
        FilePrintf (CliHandle, "\nmpls ldp graceful-restart timers "
                    "forwarding-holding %d", i4FwdHoldTime);
    }

    if (i4RecoveryTime != LDP_DEF_MAX_RECOVERY_TIME)
    {
        CliPrintf (CliHandle, "\nmpls ldp graceful-restart timers "
                   "max-recovery %d", i4RecoveryTime);
        FilePrintf (CliHandle, "\nmpls ldp graceful-restart timers "
                    "max-recovery %d", i4RecoveryTime);
    }

    if (i4NbrTime != LDP_DEF_NBR_LIVENESS_TIME)
    {
        CliPrintf (CliHandle, "\nmpls ldp graceful-restart timers "
                   "neighbor-liveness %d", i4NbrTime);
        FilePrintf (CliHandle, "\nmpls ldp graceful-restart timers "
                    "neighbor-liveness %d", i4NbrTime);
    }

    return;

}

/******************************************************************************  
 * * Function Name : LdpCliDisplayGrConfigurations
 * * Description   : This routine Displays the Configurations of GR parameters
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : NONE
 * *****************************************************************************/
VOID
LdpCliDisplayGrConfigurations (tCliHandle CliHandle)
{
    INT4                i4NbrTime = LDP_ZERO;
    INT4                i4FwdHoldTime = LDP_ZERO;
    INT4                i4RecoveryTime = LDP_ZERO;
    INT4                i4GrCapability = LDP_ZERO;

    /* Get the GR Related Configurations */
    nmhGetFsMplsLdpGrCapability (&i4GrCapability);
    nmhGetFsMplsLdpGrForwardEntryHoldTime (&i4FwdHoldTime);
    nmhGetFsMplsLdpGrMaxRecoveryTime (&i4RecoveryTime);
    nmhGetFsMplsLdpGrNeighborLivenessTime (&i4NbrTime);

    CliPrintf (CliHandle, "\r\nLDP Graceful Restart is ");

    if (i4GrCapability == LDP_GR_CAPABILITY_NONE)
    {
        CliPrintf (CliHandle, "disabled\n");
        return;
    }
    else
    {
        if (i4GrCapability == LDP_GR_CAPABILITY_FULL)
        {
            CliPrintf (CliHandle, "enabled: Configured as Full\n");
            CliPrintf (CliHandle, "\rForwarding Entry Hold Time: %d seconds\n",
                       i4FwdHoldTime);
        }
        else
        {
            CliPrintf (CliHandle, "enabled: Configured as help-neighbor\n");
        }
    }

    CliPrintf (CliHandle, "\rNeighbor Liveness Timer: %d seconds\n", i4NbrTime);
    CliPrintf (CliHandle, "\rMax Recovery Time: %d seconds\n", i4RecoveryTime);

    return;
}

/******************************************************************************  
 * * Function Name : LdpCliDisplayPeerGrConfigurations
 * * Description   : This routine Displays the Configurations of GR parameters
 *                   exchanged by the neighbour
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 * *****************************************************************************/
INT4
LdpCliDisplayPeerGrConfigurations (tCliHandle CliHandle,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpLsrId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityPeerId)
{
    INT4                i4PeerReconnectTime = LDP_ZERO;
    INT4                i4GrProgressStatus = LDP_ZERO;
    INT4                i4PeerRecoveryTime = LDP_ZERO;
    INT4                i4GrCapability = LDP_ZERO;

    nmhGetFsMplsLdpGrCapability (&i4GrCapability);

    if (i4GrCapability == LDP_GR_CAPABILITY_NONE)
    {
        return CLI_SUCCESS;
    }

    nmhGetFsMplsLdpPeerGrProgressStatus (pMplsLdpEntityLdpLsrId,
                                         u4MplsLdpEntityIndex,
                                         pMplsLdpEntityPeerId,
                                         &i4GrProgressStatus);

    if (i4GrProgressStatus == LDP_PEER_GR_NOT_SUPPORTED)
    {
        CliPrintf (CliHandle,
                   "\r         Peer is Configured as GR InCapable\n");
        return CLI_SUCCESS;
    }

    nmhGetFsMplsLdpPeerGrReconnectTime (pMplsLdpEntityLdpLsrId,
                                        u4MplsLdpEntityIndex,
                                        pMplsLdpEntityPeerId,
                                        &i4PeerReconnectTime);

    if (i4PeerReconnectTime == LDP_ZERO)
    {
        CliPrintf (CliHandle, "\r         Peer is Configured as GR Helper\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r         Peer is Configured as GR Full\n");
        CliPrintf (CliHandle, "\r         Peer Reconnect Time: %d seconds\n",
                   i4PeerReconnectTime);
    }

    nmhGetFsMplsLdpPeerGrRecoveryTime (pMplsLdpEntityLdpLsrId,
                                       u4MplsLdpEntityIndex,
                                       pMplsLdpEntityPeerId,
                                       &i4PeerRecoveryTime);

    CliPrintf (CliHandle, "\r         Peer Recovery Time: %d seconds\n",
               i4PeerRecoveryTime);

    CliPrintf (CliHandle, "\r         Peer GR Progress Status:");

    switch (i4GrProgressStatus)
    {
        case LDP_PEER_GR_NOT_STARTED:
            CliPrintf (CliHandle, " GR Not Started\n");
            break;

        case LDP_PEER_GR_RECONNECT_IN_PROGRESS:
            CliPrintf (CliHandle, " GR Reconnect in Progress\n");
            break;

        case LDP_PEER_GR_ABORTED:
            CliPrintf (CliHandle, " GR Aborted\n");
            break;

        case LDP_PEER_GR_RECOVERY_IN_PROGRESS:
            CliPrintf (CliHandle, " GR Recovery in Progress\n");
            break;

        case LDP_PEER_GR_COMPLETED:
            CliPrintf (CliHandle, " GR Recovery Completed\n");
            break;

        default:
            CliPrintf (CliHandle, "\n");
            break;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : LdpCliSetConfigSeqTLVOption
 * * Description   : This routine enables Configuration sequence TLV option
 * * Input(s)      : CliHandle - Cli Context Handle
 *                   i4ConfigSeqTlvEnable - Configuration done by user
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * *****************************************************************************/

INT4
LdpCliSetConfigSeqTLVOption (tCliHandle CliHandle, INT4 i4ConfigSeqTlvOption)
{
    UINT4               u4Errcode = LDP_ZERO;

    if (nmhTestv2FsMplsLdpConfigurationSequenceTLVEnable
        (&u4Errcode, i4ConfigSeqTlvOption) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r %%Unable to Set Config Seq TLV option\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsMplsLdpConfigurationSequenceTLVEnable (i4ConfigSeqTlvOption);

    return CLI_SUCCESS;
}

/******************************************************************************  
 * * Function Name : LdpCliSetPathVectorLimit 
 * * Description   : This routine Sets the Path Vector Limit for a LDP Entity
 * * Input(s)      : CliHandle - Cli Context Handle
 *                   i4PathVectorLimit - Configuration done by user
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * *****************************************************************************/
INT4
LdpCliSetPathVectorLimit (tCliHandle CliHandle, INT4 i4PathVectorLimit)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4ErrorCode = LDP_ZERO;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };

    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the Entity Index and LDP LSR ID */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();

    /* Test the max hops given */
    if (nmhTestv2MplsLdpEntityPathVectorLimit (&u4ErrorCode,
                                               &MplsLdpEntityLdpLsrId,
                                               u4MplsLdpEntityIndex,
                                               i4PathVectorLimit) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set the Path Vector Limit."
                   "Make sure Entity is shutdown or Path Vector Limit is within range.\n");
        return CLI_FAILURE;
    }

    if (nmhSetMplsLdpEntityPathVectorLimit (&MplsLdpEntityLdpLsrId,
                                            u4MplsLdpEntityIndex,
                                            i4PathVectorLimit) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to set the Path Vector Limit."
                   "Make sure Entity is shutdown or Path Vector Limit is within range.\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

 /******************************************************************************
 *  Function Name :  LdpCliBfdStatus
 *  Description   :  This routine sets or resets the BFD status.
 *  Input(s)      :  CliHandle      - Handle to CLI
 *                         - i4CmdType
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
#ifdef MPLS_LDP_BFD_WANTED
INT4
LdpCliBfdStatus (tCliHandle CliHandle, INT4 i4BfdStatus)
{

    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    static UINT1        au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;

    UNUSED_PARAM (CliHandle);
    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;

    /* Get the Entity Index and LDP LSR ID */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();

    /* Checks whether the passed parameter value falls within the mib range */
    if (nmhTestv2FsMplsLdpEntityBfdStatus (&u4ErrorCode,
                                           &MplsLdpEntityLdpLsrId,
                                           u4MplsLdpEntityIndex,
                                           i4BfdStatus) == SNMP_FAILURE)
    {

        LDP_DBG (LDP_MAIN_ALL, "MAIN: Wrong BFD status value\n");
        return CLI_FAILURE;
    }

    /* Calls the SNMP set routine to set the value    */
    if (nmhSetFsMplsLdpEntityBfdStatus (&MplsLdpEntityLdpLsrId,
                                        u4MplsLdpEntityIndex,
                                        i4BfdStatus) == SNMP_FAILURE)
    {
        LDP_DBG (LDP_MAIN_ALL, "MAIN: Unable to set bfd status\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}
#endif
/***************************************************************************/
/*                                                                         */
/*     Function Name : IssMplsLdpShowDebugging                             */
/*                                                                         */
/*     Description   : This function is used to display debug level for    */
/*                     MPLS LDP module                                     */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : None                                                */
/*                                                                         */
/***************************************************************************/

VOID
IssMplsLdpShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DebugLevel = 0;
    UINT4               u4TmpDbgLvl = 0;
    INT4                i4DumpType = 0;
    INT4                i4direction = 0;

    CliRegisterLock (CliHandle, LdpLock, LdpUnLock);
    LdpLock ();

    nmhGetFsMplsCrlspDebugLevel (&i4DebugLevel);

    nmhGetFsMplsCrlspDumpDirection (&i4direction);

    nmhGetFsMplsCrlspDumpType (&i4DumpType);

    LdpUnLock ();
    CliUnRegisterLock (CliHandle);

    if (i4DebugLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rMPLS LDP :");
    if ((i4DebugLevel & LDP_ADVT_ALL) == LDP_ADVT_ALL)
    {
        CliPrintf (CliHandle,
                   "\r\n  MPLS ldp all advertisement debugging is on");
    }
    if ((i4DebugLevel & LDP_IF_ALL) == LDP_IF_ALL)
    {
        CliPrintf (CliHandle, "\r\n  MPLS ldp all interface debugging is on");
    }
    if ((i4DebugLevel & LDP_MAIN_ALL) == LDP_MAIN_ALL)
    {
        CliPrintf (CliHandle, "\r\n  MPLS ldp all process debugging is on");
    }
    if ((i4DebugLevel & LDP_SSN_ALL) == LDP_SSN_ALL)
    {
        CliPrintf (CliHandle, "\r\n  MPLS ldp all session debugging is on");
    }

    u4TmpDbgLvl = (LDP_MAIN_MEM | LDP_IF_MEM | LDP_ADVT_MEM | LDP_SSN_MEM |
                   LDP_PRCS_MEM | LDP_NOTIF_MEM | LDP_ADVT_SEM | LDP_SSN_SEM |
                   LDP_PRCS_SEM);

    if (((UINT4) i4DebugLevel & u4TmpDbgLvl) == u4TmpDbgLvl)
    {
        CliPrintf (CliHandle, "\r\n  MPLS ldp memory debugging is on");
    }

    u4TmpDbgLvl = (LDP_IF_TMR | LDP_ADVT_TMR | LDP_SSN_TMR | LDP_NOTIF_TMR);
    if (((UINT4) i4DebugLevel & u4TmpDbgLvl) == u4TmpDbgLvl)
    {
        CliPrintf (CliHandle, "\r\n  MPLS ldp timer debugging is on");
    }
    if ((i4DebugLevel & GRACEFUL_DEBUG) == GRACEFUL_DEBUG)
    {
        CliPrintf (CliHandle,
                   "\r\n  MPLS ldp graceful restart debugging is on");
    }
    if ((i4DebugLevel & LDP_PDU_DUMP) == LDP_PDU_DUMP)
    {
        if ((i4DumpType & LDP_DUMP_ADDR) != 0)
        {
            LdpCliDumpMessageDirection (CliHandle, i4direction);
            CliPrintf (CliHandle, "address message debugging is on");
        }
        if ((i4DumpType & LDP_DUMP_HELLO) != 0)
        {
            LdpCliDumpMessageDirection (CliHandle, i4direction);
            CliPrintf (CliHandle, "hello message debugging is on");
        }
        if ((i4DumpType & LDP_DUMP_INIT) != 0)
        {
            LdpCliDumpMessageDirection (CliHandle, i4direction);
            CliPrintf (CliHandle, "init message debugging is on");
        }
        if ((i4DumpType & LDP_DUMP_KEEPALIVE) != 0)
        {
            LdpCliDumpMessageDirection (CliHandle, i4direction);
            CliPrintf (CliHandle, "keep alive message debugging is on");
        }
        if ((i4DumpType & LDP_DUMP_LDP_MAP) != 0)
        {
            LdpCliDumpMessageDirection (CliHandle, i4direction);
            CliPrintf (CliHandle, "map message debugging is on");
        }
        if ((i4DumpType & LDP_DUMP_LDP_NOTIF) != 0)
        {
            LdpCliDumpMessageDirection (CliHandle, i4direction);
            CliPrintf (CliHandle, "notif message debugging is on");
        }
        if ((i4DumpType & LDP_DUMP_LDP_REL) != 0)
        {
            LdpCliDumpMessageDirection (CliHandle, i4direction);
            CliPrintf (CliHandle, "release message debugging is on");
        }
        if ((i4DumpType & LDP_DUMP_LDP_RQ) != 0)
        {
            LdpCliDumpMessageDirection (CliHandle, i4direction);
            CliPrintf (CliHandle, "request message debugging is on");
        }
        if ((i4DumpType & LDP_DUMP_LDP_WDRAW) != 0)
        {
            LdpCliDumpMessageDirection (CliHandle, i4direction);
            CliPrintf (CliHandle, "withdraw message debugging is on");
        }
    }

    u4TmpDbgLvl = (LDP_IF_TX | LDP_ADVT_TX | LDP_SSN_TX | LDP_PRCS_TX |
                   LDP_NOTIF_TX);
    if (((UINT4) i4DebugLevel & u4TmpDbgLvl) == u4TmpDbgLvl)
    {
        CliPrintf (CliHandle, "\r\n  MPLS ldp sent messages debugging is on");
    }

    u4TmpDbgLvl = (LDP_IF_RX | LDP_ADVT_RX | LDP_SSN_RX | LDP_PRCS_RX |
                   LDP_NOTIF_RX);

    if (((UINT4) i4DebugLevel & u4TmpDbgLvl) == u4TmpDbgLvl)
    {
        CliPrintf (CliHandle,
                   "\r\n  MPLS ldp received messages debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");

    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : LdpCliDumpMessageDirection                             */
/*                                                                         */
/*     Description   : This function is used to dump message direction     */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : None                                                */
/*                                                                         */
/***************************************************************************/

VOID
LdpCliDumpMessageDirection (tCliHandle CliHandle, INT4 i4Dumpdirection)
{

    CliPrintf (CliHandle, "\r\n  MPLS ldp dump ");
    switch (i4Dumpdirection)
    {
        case MPLS_CLI_IN:
            CliPrintf (CliHandle, "incoming ");
            break;

        case MPLS_CLI_OUT:
            CliPrintf (CliHandle, "outgoing ");
            break;

        case MPLS_CLI_ALL:
            CliPrintf (CliHandle, "both direction ");
            break;

        default:
            return;
    }
}

#ifdef MPLS_IPV6_WANTED
/******************************************************************************
 *  Function Name :  LdpCliIpv6TransportAddress
 *  Description   :  This routine sets or resets the IPv6 transport address TLV
 *                   option in IPv6 LDP Hello Option. It configures the type
 *                   of IPv6 Transport Address to be carried in the Transport
 *                   address TLV in IPv6 LDP Hello Message. It configures an
 *                   Transport address if the transport address type is loopback.
 *  Input(s)      :  CliHandle      - Handle to CLI
 *                   u4Ipv6TransIfType  - Transport Address Type
 *                   u4IfIndex      - Transport Address If Index
 *                   i4CmdType      - i4CmdType
 *  Output(s)     :  None
 *  Return(s)     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/
PRIVATE INT4
LdpCliIpv6TransportAddress (tCliHandle CliHandle, UINT4 u4Ipv6TransIfType,
                            UINT4 u4IfIndex, INT4 i4CmdType)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityLdpLsrId;
    tSNMP_OCTET_STRING_TYPE Ipv6TransportAddress;
    tNetIpv6IfInfo      NetIpv6IfInfo;
    UINT1               au1LdpLsrId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    UINT1               au1Ipv6Addr[IPV6_ADDR_LENGTH] = { LDP_ZERO };
    UINT4               u4ErrorCode = LDP_ZERO;
    UINT4               u4MplsLdpEntityIndex = LDP_ZERO;
    UINT4               u4Port = LDP_ZERO;
    tIp6Addr           *pPrefAddr = NULL;
    tIp6Addr            Ipv6Zero;
    tIp6Addr            LinkLocalAddr;
    tIp6Addr            SiteLocalAddr;
    tIp6Addr            GlobalUniqAddr;
    tLdpEntity         *pLdpEntity = NULL;
    MEMSET (&Ipv6Zero, 0, sizeof (tIp6Addr));
    MEMSET (&LinkLocalAddr, 0, sizeof (tIp6Addr));
    MEMSET (&SiteLocalAddr, 0, sizeof (tIp6Addr));
    MEMSET (&GlobalUniqAddr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6IfInfo, 0, sizeof (tNetIpv6IfInfo));
    MEMSET (&Ipv6TransportAddress, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MplsLdpEntityLdpLsrId.pu1_OctetList = au1LdpLsrId;
    Ipv6TransportAddress.pu1_OctetList = au1Ipv6Addr;

    /* Get the entity index and LDP LSR ID */
    nmhGetMplsLdpLsrId (&MplsLdpEntityLdpLsrId);
    MplsLdpEntityLdpLsrId.i4_Length = LDP_MAX_LDPID_LEN;
    Ipv6TransportAddress.i4_Length = IPV6_ADDR_LENGTH;
    u4MplsLdpEntityIndex = (UINT4) CLI_GET_MPLS_LDP_ENTITY_MODE ();

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, MplsLdpEntityLdpLsrId.pu1_OctetList,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%% Make sure Entity is Shut down\n");
        return SNMP_FAILURE;
    }

    if (nmhTestv2FsMplsLdpEntityIpv6TransAddrTlvEnable (&u4ErrorCode,
                                                        &MplsLdpEntityLdpLsrId,
                                                        u4MplsLdpEntityIndex,
                                                        i4CmdType) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Wrong IPv6Transport Address Tlv Option"
                   "(Enable/Disable) or Make sure Entity" " is Shut down\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsLdpEntityIpv6TransportAddrKind (&u4ErrorCode,
                                                       &MplsLdpEntityLdpLsrId,
                                                       u4MplsLdpEntityIndex,
                                                       (INT4) u4Ipv6TransIfType)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Wrong Transport Address Kind or "
                   " Make sure Entity is Shut down\n");
        return CLI_FAILURE;
    }

    if ((i4CmdType == CLI_ENABLE)
        && (u4Ipv6TransIfType == LOOPBACK_ADDRESS_TYPE))
    {
        if (NetIpv6GetPortFromCfaIfIndex (u4IfIndex, &u4Port) ==
            NETIPV6_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to get the Port from If"
                       " Index\n");
            return CLI_FAILURE;
        }

        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
        {
            /* Get Site Local & Globally Unique Address */
            if ((LdpGetIpv6IfSiteLocalGlbUniqAddr (u4Port,
                                                   &SiteLocalAddr,
                                                   &GlobalUniqAddr) !=
                 LDP_FAILURE))
            {
                if (MEMCMP (&GlobalUniqAddr.u1_addr, &Ipv6Zero,
                            IPV6_ADDR_LENGTH) != LDP_ZERO)
                {
                    MEMCPY (Ipv6TransportAddress.pu1_OctetList,
                            &GlobalUniqAddr.u1_addr, IPV6_ADDR_LENGTH);

                }
                else if (MEMCMP (&SiteLocalAddr.u1_addr, &Ipv6Zero,
                                 IPV6_ADDR_LENGTH) != LDP_ZERO)
                {
                    MEMCPY (Ipv6TransportAddress.pu1_OctetList,
                            &SiteLocalAddr.u1_addr, IPV6_ADDR_LENGTH);
                }
            }
            else if (NetIpv6GetIfInfo (u4Port, &NetIpv6IfInfo) !=
                     NETIPV4_FAILURE)
            {
                MEMCPY (Ipv6TransportAddress.pu1_OctetList,
                        &NetIpv6IfInfo.Ip6Addr.u1_addr, IPV6_ADDR_LENGTH);

            }
        }
        /* Basic Entity Case */
        else
        {
            /* If LLA, SLA & GUA are configured, GUA is prefered over
             *               SLA and LLA ("GUA"-'SLA'-LLA) */
            /*Get Site Local & Globally Unique Address */
            LdpGetIpv6IfSiteLocalGlbUniqAddr (u4Port,
                                              &SiteLocalAddr, &GlobalUniqAddr);

            /*Get LinkLocal Address */
            NetIpv6GetIfInfo (u4Port, &NetIpv6IfInfo);
            MEMCPY (&LinkLocalAddr.u1_addr, NetIpv6IfInfo.Ip6Addr.u1_addr,
                    IPV6_ADDR_LENGTH);

            pPrefAddr = LdpGetBasicIpv6TransAddrPref (&LinkLocalAddr,
                                                      &SiteLocalAddr,
                                                      &GlobalUniqAddr);

            MEMCPY (Ipv6TransportAddress.pu1_OctetList,
                    pPrefAddr->u1_addr, IPV6_ADDR_LENGTH);
        }
        if (nmhTestv2FsMplsLdpEntityIpv6TransportAddress (&u4ErrorCode,
                                                          &MplsLdpEntityLdpLsrId,
                                                          u4MplsLdpEntityIndex,
                                                          &Ipv6TransportAddress)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Wrong Transport Address Or Make "
                       " sure the interface of the transport "
                       " address is UP\n");
            return CLI_FAILURE;
        }
    }

    if (nmhSetFsMplsLdpEntityIpv6TransAddrTlvEnable (&MplsLdpEntityLdpLsrId,
                                                     u4MplsLdpEntityIndex,
                                                     i4CmdType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set IPv6 Transport Address Tlv "
                   "Option\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityIpv6TransportAddrKind (&MplsLdpEntityLdpLsrId,
                                                    u4MplsLdpEntityIndex,
                                                    u4Ipv6TransIfType)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set IPv6 Transport Address Kind\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLdpEntityIpv6TransportAddress (&MplsLdpEntityLdpLsrId,
                                                   u4MplsLdpEntityIndex,
                                                   &Ipv6TransportAddress)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set IPv6 Transport Address\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#endif

#endif
