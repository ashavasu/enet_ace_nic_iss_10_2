/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpfsipv6.c,v 1.3 2015/01/21 11:04:09 siva Exp $
 *
 ********************************************************************/

#include "ldpincs.h"
#include "mplsutil.h"
#include "rtm6.h"
#include "mplsprot.h"



#ifdef MPLS_IPV6_WANTED
/*************************************************************************/
/* Function Name   : LdpIpv6IsAddrLocal                                  */
/* Description     : This function verifies whether the address          */
/*                   matches any of the local interface adresses         */
/* Input (s)       : u4DestAddr - The Address to verify                  */
/* Output (s)      : None                                                */
/* Returns         : LDP_TRUE or LDP_FALSE                               */
/*************************************************************************/

UINT1
LdpIpv6IsAddrLocal (tIp6Addr *pDestAddr)
{
	UINT4 u4IfIndex=0;

	if(NetIpv6IsOurAddress (pDestAddr,&u4IfIndex)==NETIPV6_SUCCESS)
	{
		LDP_DBG (LDP_IF_PRCS, "IF: Success in finding IPV6 is Local or Not\n");
		return LDP_TRUE;
	}

	LDP_DBG (LDP_IF_PRCS, "IF: Failed to find whether IPV6 is Local or Not\n");
	return LDP_FALSE;
}


/******************************************************************************/
/* Function Name : ldpUtlUcastIpv6RouteQuery                                  */
/* Description   : This routine is used to do a route query for a particular  */
/*                 destination                                                */
/* Input(s)      : u4Addr      - The destination address                      */
/*                 pu2RtPort   - pointer to the outgoing interface            */
/*                 pu4RtGw     - pointer to the gateway                       */
/* Output(s)     : pu2RtPort - This is updated with the outgoing interface    */
/*                             index                                          */
/*                 pu4RtGw   - This is updated with the corresponding gateway */
/*                             for the destination                            */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                 */
/******************************************************************************/
INT1
LdpIpUcastIpv6RouteQuery (tIp6Addr *pDestAddr, UINT4 *pu4RtPort, tIp6Addr *pRtGw)
{
	tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
	tNetIpv6RtInfo         NetIpv6RtInfo;
        tIp6Addr               TempAddr;

        MEMSET(&TempAddr,0,sizeof(tIp6Addr));
	MEMSET(&NetIpv6RtInfoQueryMsg,0,sizeof(tNetIpv6RtInfoQueryMsg));
	MEMSET(&NetIpv6RtInfo,0,sizeof(tNetIpv6RtInfo));

	NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
	NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
	Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst,pDestAddr);
	NetIpv6RtInfo.u1Prefixlen = IP6_ADDR_MAX_PREFIX;

	if(NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
				&NetIpv6RtInfo)==NETIPV6_SUCCESS)
	{
		/**vishal_1 : To check if we require this Index or not */
		*pu4RtPort=NetIpv6RtInfo.u4Index;
		if(MEMCMP(&NetIpv6RtInfo.NextHop,&TempAddr,IPV6_ADDR_LENGTH)==LDP_ZERO)
		{
			MEMCPY(pRtGw,pDestAddr,IPV6_ADDR_LENGTH);
		}
		else
		{
			MEMCPY(pRtGw,&NetIpv6RtInfo.NextHop,IPV6_ADDR_LENGTH);
		}
		LDP_DBG (LDP_IF_PRCS, "IF: Success in geting IPV6 route\n");
		return LDP_SUCCESS;
	}

	LDP_DBG (LDP_IF_PRCS, "IF: Failed in geting IPV6 route\n");

	return LDP_FAILURE;
}

/******************************************************************************/
/* Function Name : LdpIpGetExactRoute                                         */
/* Description   : This routine is used to get the exact route for a          */
/*                 particular destination                                     */
/* Input(s)      : u4Addr      - The destination address                      */
/*                 pu4RtPort   - pointer to the outgoing interface            */
/*                 pu4RtGw     - pointer to the gateway                       */
/* Output(s)     : pu2RtPort - This is updated with the outgoing interface    */
/*                             index                                          */
/*                 pu4RtGw   - This is updated with the corresponding gateway */
/*                             for the destination                            */
/* Return(s)     : LDP_IP_SUCCESS or LDP_IP_FAILURE                           */
/******************************************************************************/
INT1
LdpIpv6GetExactRoute (tIp6Addr *pDestAddr, UINT1 u1DestPrefixLen, UINT4 *pu4RtPort, tIp6Addr *pRtGw)
{

	tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
	tNetIpv6RtInfo         NetIpv6RtInfo;
	tIp6Addr               TempAddr;

	MEMSET(&TempAddr,0,sizeof(tIp6Addr));
	MEMSET(&NetIpv6RtInfoQueryMsg,0,sizeof(tNetIpv6RtInfoQueryMsg));
	MEMSET(&NetIpv6RtInfo,0,sizeof(tNetIpv6RtInfo));

	NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
	NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
	Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst,pDestAddr);
	NetIpv6RtInfo.u1Prefixlen = u1DestPrefixLen;

	if(NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
				&NetIpv6RtInfo)==NETIPV6_SUCCESS)
	{
		/**vishal_1 : To check if we require this Index or not */
		*pu4RtPort=NetIpv6RtInfo.u4Index;
		if(MEMCMP(&NetIpv6RtInfo.NextHop,&TempAddr,IPV6_ADDR_LENGTH)==LDP_ZERO)
		{
			MEMCPY(pRtGw,pDestAddr,IPV6_ADDR_LENGTH);
		}
		else
		{
			MEMCPY(pRtGw,&NetIpv6RtInfo.NextHop,IPV6_ADDR_LENGTH);
		}

		LDP_DBG (LDP_IF_PRCS, "IF: Success in geting IPV6 route\n");
		return LDP_SUCCESS;
	}

	LDP_DBG (LDP_IF_PRCS, "IF: Failed in geting IPV6 route\n");

	return LDP_FAILURE;
}
/******************************************************************************/
/* Function Name : LdpGetIpv6IfAddr                                           */
/* Description   : This function is used to find out the address of the       */
/*                 interface from the ifIndex                                 */
/* Input(s)      : i4IfIndex - The interface index                            */
/*                 *pu4Addr  - pointer to interface address                   */
/* Output(s)     : pu4Addr -The pu4Addr is udpated with the interface address */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                 */
/******************************************************************************/
UINT1
LdpGetIpv6IfAddr (INT4 i4IfIndex,tIp6Addr *pAddr)
{
	tNetIpv6IfInfo NetIpv6IfInfo;

	MEMSET(&NetIpv6IfInfo,0,sizeof(tNetIpv6IfInfo));
	if(NetIpv6GetIfInfo ((UINT4)i4IfIndex,&NetIpv6IfInfo)==NETIPV6_FAILURE)
	{
		LDP_DBG (LDP_IF_PRCS, "IF: Failure in Getting Interface Address\n");
		return LDP_FAILURE;
	}
	MEMCPY(pAddr,&NetIpv6IfInfo.Ip6Addr,IPV6_ADDR_LENGTH);
	LDP_DBG (LDP_IF_PRCS, "IF: Success in Getting Interface Address\n");
	return LDP_SUCCESS;
}

/****************************************************************************/
/* Function Name : LdpIsLsrPartOfIpv6ErHopOrFec                             */
/* Description   : This routine is called to check whether an ErHopAddr or  */
/*                 FEC is part of a particular LSR                          */
/* Input(s)      : UINT4 u4ErHopAddr                                        */
/*                 UINT4 u4ErHopMask                                        */
/*                                                                          */
/* Output(s)     : None                                                     */
/* Return(s)     : LDP_TRUE/LDP_FALSE.                                      */
/****************************************************************************/
UINT1
LdpIsLsrPartOfIpv6ErHopOrFec (tIp6Addr *pErHopAddr,UINT1 u1ErHopPrefixLength)
{
	tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
	tNetIpv6RtInfo         NetIpv6RtInfo;
	UINT4                  u4IfIndex=0;

	MEMSET(&NetIpv6RtInfoQueryMsg,0,sizeof(tNetIpv6RtInfoQueryMsg));
	MEMSET(&NetIpv6RtInfo,0,sizeof(tNetIpv6RtInfo));


	if (IP6_ADDR_MAX_PREFIX == u1ErHopPrefixLength)
	{
		if(NetIpv6IsOurAddress (pErHopAddr,&u4IfIndex)==NETIPV6_SUCCESS)
		{
			LDP_DBG (LDP_IF_PRCS, "IF: Success in finding IPV6 is Local or Not\n");
			return LDP_TRUE;
		}
	}
	else
	{
		NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
		NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
		Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst,pErHopAddr);
		NetIpv6RtInfo.u1Prefixlen = u1ErHopPrefixLength;

		if(NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
					&NetIpv6RtInfo)==NETIPV6_SUCCESS)
		{	
			if (NetIpv6RtInfo.i1Proto == IP6_LOCAL_PROTOID)
			{
				LDP_DBG (LDP_IF_PRCS, "IF:Success in NetIpv6GetRoute\n");
				return LDP_TRUE;
			}

		}
	}
	LDP_DBG (LDP_IF_PRCS, "IF: Failed in LdpIsLsrPartOfIpv6ErHopOrFec\n");
	return LDP_FALSE;
}


/*****************************************************************************/
/* Function Name : LdpIpv6RtChgEventHandler                                  */
/* Description   : This routine is called by IP module, for informing the    */
/*                 the Route changes.                                        */
/*                 Corresponding event is posted to LDP task along           */
/*                 with necessary parameters.                                */
/* Input(s)      : tNetIpv4RtInfo - Route info  as given by IP               */
/*                 ! Do not free pNetIpv4RtInfo,                             */
/*                 as it points to Trie Route Entry !                        */
/*                 u1CmdType - Species type of route change - NewEntry,      */
/*                            NextHop change, Interface change etc.,         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpIpv6RtChgEventHandler (tNetIpv6HliParams * pNetIpv6HlParams)
{
	UINT2               u2IncarnId;
	tLdpRouteEntryInfo *pRouteInfo = NULL;
	tLdpIfMsg           LdpIfMsg;
	uGenAddr DestMask;
	UINT1               u1Scope=0;
        UINT4               u4Type=0;
        UINT2               u2BitNxtHop=0;
        UINT2               u2BitRtType=0;
        UINT2               u2BitStatus=0;
        

        MEMSET(&LdpIfMsg,LDP_ZERO,sizeof(tLdpIfMsg));
        MEMSET(&DestMask,LDP_ZERO,sizeof(uGenAddr));

	u2IncarnId = LDP_CUR_INCARN;
#if 0
	printf("%s : %d command=%x\n",__FUNCTION__,__LINE__,pNetIpv6HlParams->u4Command);
	if(pNetIpv6HlParams->u4Command==NETIPV6_ROUTE_CHANGE)
	{
		printf("%s : %d RowStatus=%d ChangeBit=%d\n",__FUNCTION__,__LINE__,
				pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus,
				pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit);
	}
#endif

	/* Ignore the Notification if Incarn is not UP Administratively */
	if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
	{
		return;
	}

	if (pNetIpv6HlParams == NULL)
	{
		LDP_DBG (LDP_IF_MISC, "IF: No Information passed by IPV6\n");
		return;
	}
	switch(pNetIpv6HlParams->u4Command)
	{
		case NETIPV6_ADDRESS_CHANGE:
			switch(pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Mask)
			{
				case NETIPV6_ADDRESS_ADD:
					LDP_DBG1 (LDP_IF_PRCS, "IF:  NETIPV6_ADDRESS_ADD Event Recieved for Addr=%s\n",
							Ip6PrintAddr(&pNetIpv6HlParams->unIpv6HlCmdType.
								AddrChange.Ipv6AddrInfo.Ip6Addr));
					LdpIfMsg.u.IpEvt.u4BitMap = LDP_IPV6_ADDR_ADD;
					break;
				case NETIPV6_ADDRESS_DELETE:
					LDP_DBG1 (LDP_IF_PRCS, "IF:  NETIPV6_ADDRESS_DELETE Event Recieved for Addr=%s\n",
							Ip6PrintAddr(&pNetIpv6HlParams->unIpv6HlCmdType.
								AddrChange.Ipv6AddrInfo.Ip6Addr));
					LdpIfMsg.u.IpEvt.u4BitMap = LDP_IPV6_ADDR_DEL;
					break;
            };
			LdpIfMsg.u.IpEvt.u4IfIndex=pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.u4Index;
			u4Type=pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.u4Type;
			switch(u4Type)
			{
				case ADDR6_UNICAST:
				case ADDR6_LLOCAL:
					u1Scope=Ip6GetAddrScope(&pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr);
					switch(u1Scope)
					{
						case ADDR6_SCOPE_LLOCAL:
						case ADDR6_SCOPE_SITELOCAL:
						case ADDR6_SCOPE_GLOBAL:
							MEMCPY(&LdpIfMsg.u.IpEvt.LnklocalIpv6Addr,
									&pNetIpv6HlParams->unIpv6HlCmdType.AddrChange.Ipv6AddrInfo.Ip6Addr,
									sizeof(tIp6Addr));
							break;
						default:
							return;
							break;
					};
					break;
				default:
					return;
					break;
			};
			break;
		case NETIPV6_ROUTE_CHANGE:
			pRouteInfo = (tLdpRouteEntryInfo *) MemAllocMemBlk (LDP_IP_RT_POOL_ID);
			if (pRouteInfo == NULL)
			{
				LDP_DBG (LDP_IF_MEM,
						"IF: Memory Allocation for Ipv6 Route Info failed \n");
				return;
			}
                        LDP_DBG3(LDP_IF_PRCS,
                            "LdpIpv6RtChgEventHandler (V6RouteChange): Address: %s ,  ChgBit:%d, RowStatus:%d\n",
                        Ip6PrintAddr(&(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.Ip6Dst)),
                        pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit, 
                        pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus);
  
			MEMCPY(LDP_IPV6_U4_ADDR(pRouteInfo->DestAddr),
					pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.Ip6Dst.u1_addr,
					LDP_IPV6ADR_LEN);
			MplsGetIPV6Subnetmask(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u1Prefixlen,
					LDP_IPV6_ADDR(DestMask));
			MEMCPY(LDP_IPV6_U4_ADDR(pRouteInfo->DestMask),
					LDP_IPV6_ADDR(DestMask),LDP_IPV6ADR_LEN);
			MEMCPY(LDP_IPV6_U4_ADDR(pRouteInfo->NextHop),
					pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.NextHop.u1_addr,
					LDP_IPV6ADR_LEN);
			pRouteInfo->u4RtIfIndx = pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4Index;
			pRouteInfo->i4Metric1 = (INT4)pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4Metric;
			pRouteInfo->u4RowStatus = pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus;
			pRouteInfo->u2AddrType=LDP_ADDR_TYPE_IPV6;
			LdpIfMsg.u.IpEvt.u4IfIndex=pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4Index;
			u2BitNxtHop=pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit;
			u2BitRtType=pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit;
			u2BitStatus=pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit;

			IPV6_GET_BIT_FROM_CHANGED_PARAM(u2BitNxtHop,IP6_BIT_NXTHOP);
			IPV6_GET_BIT_FROM_CHANGED_PARAM(u2BitRtType,IP6_BIT_RT_TYPE);
			IPV6_GET_BIT_FROM_CHANGED_PARAM(u2BitStatus,IP6_BIT_STATUS);

			/** Route add **/
			if((pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit==IP6_BIT_ALL) ||
					(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit==IP6_BIT_METRIC))
			{
#if 0
				printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif
				if(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus == IP6FWD_ACTIVE)
				{
#if 0
					printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif

					LdpIfMsg.u.IpEvt.u4BitMap = LDP_RT_NEW;
					LDP_DBG4 (LDP_IF_MISC,
							"IF: Ipv6 Route Add event for Dest Addr=%s PrefixLen=%d PrefixMask=%s NextHop=%s\n",
							Ip6PrintAddr(&pRouteInfo->DestAddr.Ip6Addr),
							pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u1Prefixlen,
							Ip6PrintAddr(&pRouteInfo->DestMask.Ip6Addr),
							Ip6PrintAddr(&pRouteInfo->NextHop.Ip6Addr));
				}
				else
				{
#if 0
					printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif
					MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
					pRouteInfo=NULL;
					return;
				}
			}

			/** Route Delete **/
			else if(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit==IP6_BIT_STATUS) 
			{
#if 0
				printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif
				if(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus == IP6FWD_DESTROY)
				{
#if 0
					printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif
					LDP_DBG4 (LDP_IF_MISC,
							"IF: Ipv6 Route Delete event for Dest Addr=%s PrefixLen=%d PrefixMask=%s NextHop=%s\n",
							Ip6PrintAddr(&pRouteInfo->DestAddr.Ip6Addr),
							pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u1Prefixlen,
							Ip6PrintAddr(&pRouteInfo->DestMask.Ip6Addr),
							Ip6PrintAddr(&pRouteInfo->NextHop.Ip6Addr));
					LdpIfMsg.u.IpEvt.u4BitMap = LDP_RT_DELETED;


				}
				else
				{
#if 0
					printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif
					MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
					pRouteInfo=NULL;
					return;
				}
			}

			/** route Change **/
		        else if(u2BitNxtHop && u2BitRtType && u2BitStatus)
			{
#if 0
				printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif
				if(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus == IP6FWD_ACTIVE)
				{
#if 0
					printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif

					LDP_DBG4 (LDP_IF_MISC,
							"IF: Ipv6 Route change event for Dest Addr=%s PrefixLen=%d PrefixMask=%s NextHop=%s\n",
							Ip6PrintAddr(&pRouteInfo->DestAddr.Ip6Addr),
							pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u1Prefixlen,
							Ip6PrintAddr(&pRouteInfo->DestMask.Ip6Addr),
							Ip6PrintAddr(&pRouteInfo->NextHop.Ip6Addr));
					LdpIfMsg.u.IpEvt.u4BitMap=LDP_RT_NH_CHG;
				}
				else
				{
#if 0
					printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif
					MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
					pRouteInfo=NULL;
					return;
				}
			}
			else
			{
#if 0
				printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif
				MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
				pRouteInfo=NULL;
				return;
			}
			break;
		default:
#if 0
			printf("%s : %d \n",__FUNCTION__,__LINE__);
#endif
			LDP_DBG1 (LDP_IF_PRCS, 
					"IF: IPV6 unknown event with default Failure Case %d\n",
					pNetIpv6HlParams->u4Command);
			return;
			break;
	};
	LdpIfMsg.u4MsgType = LDP_IPV6_EVENT;
	LdpIfMsg.u.IpEvt.u4IncarnId = u2IncarnId;
	LdpIfMsg.u.IpEvt.pRouteInfo = (UINT1 *) pRouteInfo;

	if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
	{
		LDP_DBG (LDP_IF_PRCS, "IF: Failed to EnQ IPV6 Event to LDP Task\n");
		if(pRouteInfo!=NULL)
		{
			MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
		}
	}
	return;
}

UINT1
LdpGetIpv6IfSiteLocalGlbUniqAddr (UINT4 u4IfIndex,tIp6Addr *pSiteLocalAddr,tIp6Addr *pGlobalUniqAddr)
{
	tNetIpv6AddrInfo    netIp6FirstAddrInfo;
	tNetIpv6AddrInfo    netIp6NextAddrInfo;
	UINT1		    u1Scope=0;
	INT4                i4Status=0;

	MEMSET (&netIp6FirstAddrInfo, 0, sizeof (tNetIpv6AddrInfo));
	MEMSET (&netIp6NextAddrInfo, 0, sizeof (tNetIpv6AddrInfo));

	i4Status = NetIpv6GetFirstIfAddr (u4IfIndex, &netIp6FirstAddrInfo);

	if (i4Status != NETIPV6_FAILURE)
	{
		do
		{
			u1Scope=Ip6GetAddrScope (&netIp6FirstAddrInfo.Ip6Addr);
			switch(u1Scope)
			{
				case ADDR6_SCOPE_SITELOCAL:
					MEMCPY (pSiteLocalAddr,&netIp6FirstAddrInfo.Ip6Addr, sizeof (tIp6Addr));
				break;
				case ADDR6_SCOPE_GLOBAL:
					MEMCPY (pGlobalUniqAddr,&netIp6FirstAddrInfo.Ip6Addr, sizeof (tIp6Addr));
				break;
				default:
				break;
			}
			i4Status = NetIpv6GetNextIfAddr (u4IfIndex, &netIp6FirstAddrInfo,
					&netIp6NextAddrInfo);
			if (i4Status != NETIPV6_FAILURE)
			{
				MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
						sizeof (tNetIpv6AddrInfo));
			}
		}
		while (i4Status != NETIPV6_FAILURE);
		return LDP_SUCCESS;
	}
	LDP_DBG (LDP_IF_PRCS, "IF: Failed to get siteLocal and Global Unique address\n");
	return LDP_FAILURE;
}


UINT1
LdpGetIpv6IfGlbUniqAddr (UINT4 u4IfIndex,tNetIpv6AddrInfo *pGlobalUniqAddr)
{
	tNetIpv6AddrInfo    netIp6FirstAddrInfo;
	tNetIpv6AddrInfo    netIp6NextAddrInfo;
	UINT1		    u1Scope=0;
	INT4                i4Status=0;

	MEMSET (&netIp6FirstAddrInfo, 0, sizeof (tNetIpv6AddrInfo));
	MEMSET (&netIp6NextAddrInfo, 0, sizeof (tNetIpv6AddrInfo));

	i4Status = NetIpv6GetFirstIfAddr (u4IfIndex, &netIp6FirstAddrInfo);

	if (i4Status != NETIPV6_FAILURE)
	{
		do
		{
			u1Scope=Ip6GetAddrScope (&netIp6FirstAddrInfo.Ip6Addr);
			switch(u1Scope)
			{
				case ADDR6_SCOPE_GLOBAL:
					MEMCPY (pGlobalUniqAddr,&netIp6FirstAddrInfo, sizeof (tNetIpv6AddrInfo));
				break;
				default:
				break;
			}
			i4Status = NetIpv6GetNextIfAddr (u4IfIndex, &netIp6FirstAddrInfo,
					&netIp6NextAddrInfo);
			if (i4Status != NETIPV6_FAILURE)
			{
				MEMCPY (&netIp6FirstAddrInfo, &netIp6NextAddrInfo,
						sizeof (tNetIpv6AddrInfo));
			}
		}
		while (i4Status != NETIPV6_FAILURE);
		return LDP_SUCCESS;
	}
	LDP_DBG (LDP_IF_PRCS, "IF: Failed to get Global Unique address\n");
	return LDP_FAILURE;
}

UINT1
LdpGetIpv6IfAllAddr (INT4 i4IfIndex,tIp6Addr *pLnklocalIpv6Addr,tIp6Addr *pSiteLocalAddr,tIp6Addr *pGlobalUniqAddr)
{
        UINT1 u1RetType;

	/**return type explicitally not check, as it contains the interfaces with IPV4 Addr only **/
	u1RetType=LdpGetIpv6IfAddr (i4IfIndex,pLnklocalIpv6Addr);

	if (u1RetType == LDP_FAILURE)
	{
		LDP_DBG (LDP_IF_PRCS, "Failed to get LdpGetIpv6IfAddr \n");
	}

	u1RetType=LdpGetIpv6IfSiteLocalGlbUniqAddr((UINT4)i4IfIndex,pSiteLocalAddr,pGlobalUniqAddr);
 
        if (u1RetType == LDP_FAILURE)
        {
                LDP_DBG (LDP_IF_PRCS, "Failed to get LdpGetIpv6IfSiteLocalGlbUniqAddr \n");
        }

	return LDP_SUCCESS;
}



UINT1
LdpEstbLspsForAllIpv6RouteEntries(tLdpSession * pLdpSession)
{
	tNetIpv6RtInfo NetIpv6RtInfo;
	tNetIpv6RtInfo NextNetIpv6RtInfo;
	tNetIpv6RtInfoQueryMsg NetIpv6RtQuery;
	tNetIpv6RtInfo TempNetIpv6RtInfo;
        tNetIpv6RtInfo InIpv6RtInfo;
        INT4 i4Status=0;


	MEMSET(&NetIpv6RtInfo,0,sizeof(tNetIpv6RtInfo));
	MEMSET(&NextNetIpv6RtInfo,0,sizeof(tNetIpv6RtInfo));
	MEMSET(&InIpv6RtInfo,0,sizeof(tNetIpv6RtInfo));

	LDP_DBG (LDP_IF_MISC,
					"Entered: LdpEstbLspsForAllIpv6RouteEntries\n");
	if(NetIpv6GetFirstFwdTableRouteEntry(&NetIpv6RtInfo)!=NETIPV6_SUCCESS)
	{
        LDP_DBG (LDP_IF_MISC,
                    "LdpEstbLspsForAllIpv6RouteEntries: NetIpv6GetFirstFwdTableRouteEntry Failure\n");
		return LDP_FAILURE;
	}

	do
	{
		if (LDP_SESSION_STATE (pLdpSession) != LDP_SSM_ST_OPERATIONAL)
		{
			LDP_DBG (LDP_IF_MISC,
					"LdpEstbLspsForAllIpv6RouteEntries: session is not operationally up\n");
			break;
		}
		MEMCPY(&InIpv6RtInfo,&NetIpv6RtInfo,sizeof(tNetIpv6RtInfo));
		MEMSET(&TempNetIpv6RtInfo,0,sizeof(tNetIpv6RtInfo));
		MEMSET(&NetIpv6RtQuery,0,sizeof(tNetIpv6RtInfoQueryMsg));

		NetIpv6RtQuery.u4ContextId = VCM_DEFAULT_CONTEXT;
		NetIpv6RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
		Ip6AddrCopy (&TempNetIpv6RtInfo.Ip6Dst,&NetIpv6RtInfo.Ip6Dst);
		TempNetIpv6RtInfo.u1Prefixlen = NetIpv6RtInfo.u1Prefixlen;

		if ((SSN_GET_ENTITY (pLdpSession)->u2LabelRet ==
					LDP_CONSERVATIVE_MODE) &&
				(NetIpv6GetRoute (&NetIpv6RtQuery,&TempNetIpv6RtInfo)==NETIPV6_SUCCESS)&&
				(MEMCMP(&TempNetIpv6RtInfo.NextHop,&NetIpv6RtInfo.NextHop,LDP_IPV6ADR_LEN)!=LDP_ZERO))
		{
            LDP_DBG2 (LDP_IF_MISC,
                            "LdpEstbLspsForAllIpv6RouteEntries: if (\nLbl_ret = Conservative \n or NetIpv6GetRoute FAILURE\nor MEMCMP %s , %s \n",
                            Ip6PrintAddr (&TempNetIpv6RtInfo.NextHop), Ip6PrintAddr (&NetIpv6RtInfo.NextHop));

			MEMCPY(&InIpv6RtInfo.NextHop,&TempNetIpv6RtInfo.NextHop,LDP_IPV6ADR_LEN);
		}


		LDP_DBG2 (LDP_IF_MISC,
				"LdpEstbLspsForAllIpv6RouteEntries FEC %s NextHop %s\n",
				Ip6PrintAddr (&InIpv6RtInfo.Ip6Dst),Ip6PrintAddr (&InIpv6RtInfo.NextHop));

        LDP_DBG2 (LDP_IF_MISC,
                    "LdpEstbLspsForAllIpv6RouteEntries: \nSSN_ADVTYPE (pLdpSession)= %d\n(SSN_MRGTYPE (pLdpSession)= %d\n",
                     SSN_ADVTYPE (pLdpSession),SSN_MRGTYPE (pLdpSession));

		if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
				(SSN_MRGTYPE (pLdpSession) == LDP_NO_MRG))
		{
			LdpGetLspIpv6RouteInfo (&InIpv6RtInfo, pLdpSession);
		}
		else if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
				((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
				 (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG)))
		{
#if 0
			LdpEstbIpv6LspsForDoDMrgSsns (&InIpv6RtInfo, pLdpSession);
#endif

		}
		else if (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT)
		{
			LdpEstbIpv6LspsForDuSsns (pLdpSession, &InIpv6RtInfo);
		}

		i4Status=NetIpv6GetNextFwdTableRouteEntry (&NetIpv6RtInfo,
				&NextNetIpv6RtInfo);

		if(i4Status==NETIPV6_SUCCESS)
		{
            LDP_DBG (LDP_IF_MISC,"LdpEstbLspsForAllIpv6RouteEntries i4Status = NETIPV6_SUCCESS\n");
			MEMCPY (&NetIpv6RtInfo, &NextNetIpv6RtInfo, sizeof (tNetIpv6RtInfo));
		}
        else
        {
            LDP_DBG (LDP_IF_MISC,"LdpEstbLspsForAllIpv6RouteEntries i4Status = NETIPV6_FAILURE\n");
        }

	}while(i4Status!=NETIPV6_FAILURE);

	return LDP_SUCCESS;
}

VOID
LdpEstbIpv6LspsForDuSsns(tLdpSession * pLdpSession,tNetIpv6RtInfo * pNetIpv6RtInfo)
{
	tFec                Fec;
	UINT1               u1PrefLen;
	UINT1               u1SsnState;
	UINT1               u1IsIngress = TRUE;
	UINT1               u1DnCtrlBlockPresent = FALSE;
	tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
	tUstrLspCtrlBlock  *pTempLspUpCtrlBlock = NULL;
	tLspCtrlBlock      *pLspCtrlBlock = NULL;
	tLdpSession        *pLdpSessionEntry = NULL;
	tLdpMsgInfo         LdpMsgInfo;
	tTMO_SLL_NODE      *pLdpSllNode = NULL;
	tTMO_SLL_NODE      *pSllNode = NULL;
	tTMO_SLL           *pPeerIfList = NULL;
	tPeerIfAdrNode     *pIfAddrNode = NULL;
	tPeerIfAdrNode     *pTempSllNode = NULL;
	tTMO_SLL           *pEntityList = NULL;
	tLdpEntity         *pLdpEntity = NULL;
	tLdpPeer           *pLdpPeer = NULL;
	UINT2               u2IncarnId = SSN_GET_INCRN_ID (pLdpSession);
	UINT4               u4RtPort = 0;
	tIp6Addr            RtGw;
	tIp6Addr            TempAddr;
	tGenU4Addr          TempNextNode;
    uGenU4Addr          TempDestAddr;
    uGenU4Addr          DestMask;
    uGenU4Addr          AddrListPrefix;
    uGenU4Addr          FecPrefix;
#ifdef LDP_GR_WANTED
    tLdpAdjacency      *pLdpAdjacency = NULL;
    UINT4              u4AdjIfIndex = 0;
    tILMHwListEntry    *pILMHwListEntry = NULL;
    tMplsIpAddress     ILMHwListFec;
    UINT1              u1IsUpstrAllocReq = LDP_TRUE;
    UINT4              u4RecoveryTimeRem = 0;
#endif


	MEMSET(&TempNextNode,LDP_ZERO,sizeof(tGenU4Addr));
	MEMSET(&RtGw,LDP_ZERO,sizeof(tIp6Addr));
	MEMSET(&TempAddr,LDP_ZERO,sizeof(tIp6Addr));
        MEMSET(&DestMask,LDP_ZERO,sizeof(uGenU4Addr));
        MEMSET(&TempDestAddr,LDP_ZERO,sizeof(uGenU4Addr));
#ifdef LDP_GR_WANTED
    MEMSET (&ILMHwListFec, 0, sizeof (tMplsIpAddress));
#endif


	if ((LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
				== LDP_TRUE) &&
			(SSN_GET_ENTITY (pLdpSession)->InStackTnlInfo.u4TnlId == LDP_ZERO))
	{
		LDP_DBG (LDP_ADVT_PRCS,
				"LdpEstbIpv6LspsForDuSsns: Labels can't be "
				"negotiated when out-tunnel is not "
				"associated with LDP targeted session\n");
		return;
	}

	LDP_DBG2 (LDP_SSN_PRCS, "LdpEstbIpv6LspsForDuSsns for FEC %s Nexthop %s \n",
			Ip6PrintAddr(&pNetIpv6RtInfo->Ip6Dst),
			Ip6PrintAddr(&pNetIpv6RtInfo->NextHop));

	MEMSET (&LdpMsgInfo, 0, sizeof (tLdpMsgInfo));

	u1PrefLen=pNetIpv6RtInfo->u1Prefixlen;
        MplsGetIPV6Subnetmask(u1PrefLen,LDP_IPV6_U4_ADDR(DestMask));


	MEMSET (&Fec, 0, sizeof (tFec));

	Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
	Fec.u2AddrFmly = LDP_ADDR_TYPE_IPV6;
	Fec.u1PreLen = u1PrefLen;

	MEMCPY(&Fec.Prefix.Ip6Addr,&pNetIpv6RtInfo->Ip6Dst,
			LDP_IPV6ADR_LEN);


	/* Check for downstream session existence 
	 * For LDP targeted session case, there may be a session with a remote peer, 
	 * not with a directly connected one. */

	MEMCPY(&TempNextNode.Addr.Ip6Addr,
			pNetIpv6RtInfo->NextHop.u1_addr,
			LDP_IPV6ADR_LEN);
	TempNextNode.u2AddrType=LDP_ADDR_TYPE_IPV6;

	if (LdpIsSsnForFec (&TempNextNode, pNetIpv6RtInfo->u4Index,
				pLdpSession) == LDP_FALSE)
	{

		/* This section applicable for 
		 * 1. Egress route case, nexthop will be zero, 
		 * so LDP label mapping message has to be sent to upstream.
		 * 2. Intermediate router with valid nexthop case, 
		 * if label allocation mode is INDEPENDENT 
		 * only the label message will be sent upstream, 
		 * otherwise ORDERED label allocation mode, 
		 * we expect the downstream router to 
		 * send the label mapping message for the corresponding FECs.
		 * 3. Intermediate router with valid nexthop case, 
		 * here route table will not give you the remote nexthop, 
		 * so we have to find out the targeted session by 
		 * scanning all the downstream sessions. 
		 * if Label allocation mode is ORDERED, 
		 * then we have to find out the targeted sessions */
		pPeerIfList = SSN_GET_PEER_IFADR_LIST (pLdpSession);
		TMO_DYN_SLL_Scan (pPeerIfList, pIfAddrNode,
				pTempSllNode, tPeerIfAdrNode *)
		{
			pIfAddrNode = LDP_OFFSET (pIfAddrNode);

            if (pIfAddrNode->AddrType != MPLS_IPV6_ADDR_TYPE)
            {
                continue;
            }

			MEMSET(&AddrListPrefix,LDP_ZERO,sizeof(uGenU4Addr));
			MEMSET(&FecPrefix,LDP_ZERO,sizeof(uGenU4Addr));
			MplsGetPrefix((uGenU4Addr*)&pIfAddrNode->IfAddr,&DestMask,LDP_ADDR_TYPE_IPV6,&AddrListPrefix);
            MplsGetPrefix(&Fec.Prefix,&DestMask,LDP_ADDR_TYPE_IPV6,&FecPrefix);

            if(MEMCMP(&AddrListPrefix,&FecPrefix,LDP_IPV6ADR_LEN)==LDP_ZERO)
			{
				/* The peer is part of this route and hence the
				 * map message or Label is not updated to the 
				 * peer
				 */
				LDP_DBG ((LDP_ADVT_PRCS | LDP_SSN_PRCS),
						"IF: The peer is part of this route and hence"
						" the map message or Label is not updated to the peer\n");
				return;
			}
		}

		TMO_SLL_Scan (&SSN_ULCB (pLdpSession), pSllNode, tTMO_SLL_NODE *)
		{
			pTempLspUpCtrlBlock = (tUstrLspCtrlBlock *) (pSllNode);

                        MEMCPY(&TempDestAddr.Ip6Addr,&pNetIpv6RtInfo->Ip6Dst,LDP_IPV6ADR_LEN);
			if ((LdpIsAddressMatchWithAddrType(&TempDestAddr,
							&pTempLspUpCtrlBlock->Fec.Prefix,LDP_ADDR_TYPE_IPV6,
							pTempLspUpCtrlBlock->Fec.u2AddrFmly)==LDP_TRUE) &&
					(u1PrefLen == pTempLspUpCtrlBlock->Fec.u1PreLen))

			{
#ifdef LDP_GR_WANTED
				/* If upstream control block is found to be stale,
				 *                  * then it will be in the ESTABLISHED state, as all other
				 *                                   * control blocks are already deleted in helper mode */
				if (pTempLspUpCtrlBlock->u1StaleStatus == LDP_TRUE)
				{
					LDP_DBG2 (GRACEFUL_DEBUG, "LdpEstbLspsForDuSsns: Upstream control exists in STALE state "
							"for the Prefix: %s, Control block state = %d\n",
							Ip6PrintAddr(&pTempLspUpCtrlBlock->Fec.Prefix.Ip6Addr), 
							pTempLspUpCtrlBlock->u1LspState);
					u1IsUpstrAllocReq = LDP_FALSE;
					break;
				}
#endif

				/* For this prefix, LSP is already available in this session */
				/* MPLS IP forwarding looping will occur if the 
				 *                  * same LSP is created on both Upstream and 
				 *                                   * downstream LSP sessions */
				if (pTempLspUpCtrlBlock->u1LspState == LDP_DU_UP_LSM_ST_REL_AWT)
				{
					LDP_DBG1 ((LDP_ADVT_PRCS | LDP_SSN_PRCS),
							"LSP for prefix %s is in Release await state\n",
							Ip6PrintAddr(&pTempLspUpCtrlBlock->Fec.Prefix.Ip6Addr));

					LDP_DBG ((LDP_ADVT_PRCS | LDP_SSN_PRCS),
							"Freeing the label and deleting Upstr cntl blk\n");

					LdpFreeLabel (SSN_GET_ENTITY (UPSTR_USSN
								(pTempLspUpCtrlBlock)),
							&LCB_ULBL (pTempLspUpCtrlBlock),
							LdpSessionGetIfIndex(UPSTR_USSN
								(pTempLspUpCtrlBlock),pTempLspUpCtrlBlock->Fec.u2AddrFmly));
					LdpDuDeleteUpLspCtrlBlock (pTempLspUpCtrlBlock);
					break;
				}
				else
				{
					LDP_DBG2 ((LDP_ADVT_PRCS | LDP_SSN_PRCS),
							"For this prefix %s %x, LSP is already "
							"available in this session\n",
							Ip6PrintAddr(&pNetIpv6RtInfo->Ip6Dst), 
							pNetIpv6RtInfo->u1Prefixlen);
					return;
				}
			}
		}
#ifdef LDP_GR_WANTED
		/* At this point of time, the Session has already come up,
		 *          * Thus GR timer, if present (running) will be recovery timer
		 *                   * and not the reconnect timer */
		if (TmrGetRemainingTime (LDP_TIMER_LIST_ID,
					&(pLdpSession->pLdpPeer->GrTimer.AppTimer),
					&u4RecoveryTimeRem) == TMR_FAILURE)
		{
			LDP_DBG (GRACEFUL_DEBUG, "LdpEstbLspsForDuSsns: Error in Getting the "
					"Remaining Time for the GR Recovery Timer\n");
			return;
		}

		/* If the Recovery timer is not running=>
		 *          * This implies this is not the Helper node */
		if (u4RecoveryTimeRem == 0)
		{
			/* Fetch the ILM Hw List entry for session */
			ILMHwListFec.i4IpAddrType = MPLS_LSR_ADDR_IPV6;
			MEMCPY (&ILMHwListFec.IpAddress.Ip6Addr,
					&(pNetIpv6RtInfo->Ip6Dst),IPV6_ADDR_LENGTH);

			TMO_SLL_Scan (SSN_GET_ADJ_LIST(pLdpSession), pLdpAdjacency, tLdpAdjacency *)
			{
				u4AdjIfIndex = pLdpAdjacency->u4IfIndex;
				pILMHwListEntry = MplsGetILMHwListEntryFromFecAndInIntf (ILMHwListFec,
						u1PrefLen,
						u4AdjIfIndex);
				if (pILMHwListEntry != NULL)
				{
					LDP_DBG2 (GRACEFUL_DEBUG, "LdpEstbLspsForDuSsns: ILM Hw List Entry found for "
							"the prefix: %s and interface: %d\n",
							Ip6PrintAddr(&(pNetIpv6RtInfo->Ip6Dst)), u4AdjIfIndex);
					/* ILM hw list entry found */
					break;
				}
			}
			if (pILMHwListEntry != NULL)
			{
				/* Check if the ILM hw list entry is SWAP based and Label is
				 *                    rcvd from DOWNSTTREAM router */
				if ((MPLS_ILM_HW_LIST_OUT_TNL_INTF (pILMHwListEntry) != 0) &&
						(LdpIsLblRcvdFromDownstreamRtr (pLdpSession, Fec,&TempNextNode,
										pNetIpv6RtInfo->u4Index) == LDP_FALSE))
				{
					/* The processing of such entries will be done when LBL_MAP
					 *                      * will arrive from the downstream router */
					LDP_DBG1 (GRACEFUL_DEBUG, "LdpEstbLspsForDuSsns: For the Prefix %s, ILM Hw List Entry "
							"with SWAP operation exists, Hene skipping the processing here\n",
							 Ip6PrintAddr(&(pNetIpv6RtInfo->Ip6Dst)));
					return;
				}
			}
		}
#endif


		LDP_DBG2 (LDP_ADVT_PRCS, "Label distribution for FEC %s Nexthop %s \n",
				Ip6PrintAddr(&pNetIpv6RtInfo->Ip6Dst), 
				Ip6PrintAddr(&pNetIpv6RtInfo->NextHop));
#ifdef LDP_GR_WANTED
        if (u1IsUpstrAllocReq == LDP_TRUE)
        {
            /* Create up ctrl blk */
            /* Add to the dn ctrl block */
            /* initialise the us ctl block */
            pLspUpCtrlBlock = NULL;
            LdpCreateUpstrCtrlBlock (&pLspUpCtrlBlock);
            if (pLspUpCtrlBlock == NULL)
            {
             LDP_DBG (GRACEFUL_DEBUG, "Memory Allocation FAILED for Upstream Control Block\n");
             return;
            }
            pLspUpCtrlBlock->u1InLblType = SSN_GET_LBL_TYPE (pLdpSession);
            UPSTR_INCOM_IFINDX (pLspUpCtrlBlock) = LdpSessionGetIfIndex(pLdpSession,Fec.u2AddrFmly);
            pLspUpCtrlBlock->pUpstrSession = pLdpSession;
            pLspUpCtrlBlock->u1LspState = LDP_DU_UP_LSM_ST_IDLE;
	    UPSTR_UREQ_ID (pLspUpCtrlBlock) = LDP_INVALID_REQ_ID;

	    if (MEMCMP(&pNetIpv6RtInfo->NextHop,&TempAddr,
				    LDP_IPV6ADR_LEN) == LDP_ZERO)

	    {
		    pLspUpCtrlBlock->bIsConnectedRoute = LDP_TRUE;
	    }
	    MEMCPY (&LCB_FEC (pLspUpCtrlBlock), &Fec, sizeof (tFec));
            TMO_SLL_Add (&SSN_ULCB (pLdpSession),
                     (tTMO_SLL_NODE *) (&(pLspUpCtrlBlock->NextUpstrCtrlBlk)));
        }
        else
        {
            pLspUpCtrlBlock = pTempLspUpCtrlBlock;
            if (pLspUpCtrlBlock == NULL)
            {
              LDP_DBG (GRACEFUL_DEBUG, "Memory Allocation FAILED for Upstream Control Block\n");
             return;
            }

        }
#else

		/* Create up ctrl blk */
		/* Add to the dn ctrl block */
		/* initialise the us ctl block */
		pLspUpCtrlBlock = NULL;
		LdpCreateUpstrCtrlBlock (&pLspUpCtrlBlock);
		if (pLspUpCtrlBlock == NULL)
		{
			return;
		}
		pLspUpCtrlBlock->u1InLblType = SSN_GET_LBL_TYPE (pLdpSession);
		UPSTR_INCOM_IFINDX (pLspUpCtrlBlock) = LdpSessionGetIfIndex(pLdpSession,Fec.u2AddrFmly);
		pLspUpCtrlBlock->pUpstrSession = pLdpSession;
		pLspUpCtrlBlock->u1LspState = LDP_DU_UP_LSM_ST_IDLE;
		UPSTR_UREQ_ID (pLspUpCtrlBlock) = LDP_INVALID_REQ_ID;

		MEMSET(&TempAddr,LDP_ZERO,sizeof(tIp6Addr));
		if (MEMCMP(&pNetIpv6RtInfo->NextHop,&TempAddr,
					LDP_IPV6ADR_LEN) == LDP_ZERO)
		{
			pLspUpCtrlBlock->bIsConnectedRoute = LDP_TRUE;
		}
		MEMCPY (&LCB_FEC (pLspUpCtrlBlock), &Fec, sizeof (tFec));
		TMO_SLL_Add (&SSN_ULCB (pLdpSession),
				(tTMO_SLL_NODE *) (&(pLspUpCtrlBlock->NextUpstrCtrlBlk)));
#endif
		pEntityList = &LDP_ENTITY_LIST (u2IncarnId);


		TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
		{
			TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
			{
				if ((pLdpPeer->pLdpSession) == NULL)
				{
					continue;
				}
				/* Target sessions also we need to 
				 *                  * consider for LDP over RSVP case */
				pLdpSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;

				if ((LDP_ENTITY_IS_TARGET_TYPE
							(SSN_GET_ENTITY (pLdpSessionEntry)) == LDP_TRUE)
						&& (SSN_GET_ENTITY (pLdpSessionEntry)->OutStackTnlInfo.
							u4TnlId == LDP_ZERO))
				{
					LDP_DBG (LDP_ADVT_PRCS,
							"\n LdpEstbIpv6LspsForDuSsns: Labels can't be negotiated when "
							"out-tunnel is not associated with LDP targeted session\n");
					continue;
				}

				/* if dn ctl block exists chk if the dn ssn of dn ctl 
				 *                  * block same as curr ssn */
				if (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
						== LDP_FALSE)
				{
					if (pLdpSessionEntry == pLdpSession)
					{
						continue;
					}
				}
				SSN_GET_SSN_STATE (pLdpSessionEntry, u1SsnState);
#ifdef LDP_GR_WANTED
				/* If on the transit node, both sessions are stale (both peers restarted)
				 * then the DLCB which existed on the stale session (on stale sessions
				 * only Established DLCB's exist) should be used. If this check is not applied
				 * it will skip the stale session and the ESTABLISHED DLCB on stale session
				 * will not be used */
				if (pLdpSessionEntry->u1StaleStatus == LDP_FALSE)
				{
					if (!((u1SsnState == LDP_SSM_ST_OPERATIONAL) &&
								(SSN_ADVTYPE (pLdpSessionEntry) == LDP_DSTR_UNSOLICIT)))
					{
						continue;
					}
				}
#else

				if (!((u1SsnState == LDP_SSM_ST_OPERATIONAL) &&
							(SSN_ADVTYPE (pLdpSessionEntry) == LDP_DSTR_UNSOLICIT)))
				{
					continue;
				}

#endif
				/* Scan the Downstream Lsp Control Block List */
				TMO_SLL_Scan (&SSN_DLCB (pLdpSessionEntry), pLdpSllNode,
						tTMO_SLL_NODE *)
				{
					pLspCtrlBlock = SLL_TO_LCB (pLdpSllNode);


					if ((MEMCMP (&pLspCtrlBlock->Fec, &(Fec),
									sizeof (tFec))) != 0)
					{
						continue;
					}
					if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) ==
							LDP_DU_DN_MLIB_UPD_NOT_DONE)
					{
						/* For remote targeted session i.e not directly 
						 * connected router
						 * case, there will not be a sesion for a 
						 * route table's nexthop address. 
						 * So, here we do the FTN and ILM programming */
						if (LDP_ENTITY_IS_TARGET_TYPE
								(SSN_GET_ENTITY (pLdpSessionEntry)) == LDP_TRUE)
						{

							MEMCPY(LDP_IPV6_U4_ADDR(pLspCtrlBlock->NextHopAddr),
									&LCB_DSSN (pLspCtrlBlock)->pLdpPeer->TransAddr.Addr.Ip6Addr, 
									LDP_IPV6ADR_LEN);

							pLspCtrlBlock->u2AddrType=LDP_ADDR_TYPE_IPV6;
							/* Route nexthop and the targeted session's 
							 * transport 
							 * address should be in the same network, 
							 * otherwise continue */

							if(LdpIpUcastIpv6RouteQuery(&pLspCtrlBlock->NextHopAddr.Ip6Addr,
										&u4RtPort,
										&RtGw)==LDP_IP_SUCCESS)
							{
								if (MEMCMP(&RtGw,&pNetIpv6RtInfo->NextHop,
											LDP_IPV6ADR_LEN)!= LDP_ZERO)
								{
									continue;
								}
							}
						}
						else
						{
							if (LdpIpUcastIpv6RouteQuery
									(&pNetIpv6RtInfo->Ip6Dst, &u4RtPort,
									 &RtGw) == LDP_IP_FAILURE)
							{
								continue;
							}

							MEMSET(&TempNextNode,LDP_ZERO,sizeof(tGenU4Addr));
							MEMCPY(LDP_IPV6_U4_ADDR(TempNextNode.Addr),
									RtGw.u1_addr,
									LDP_IPV6ADR_LEN);
							TempNextNode.u2AddrType=LDP_ADDR_TYPE_IPV6;
							if (LdpIsSsnForFec
									(&TempNextNode,pNetIpv6RtInfo->u4Index,
									 pLdpSessionEntry) == LDP_FALSE)
							{
								LDP_DBG1 (LDP_ADVT_PRCS,
										"FEC %s Skipped \n",
										Ip6PrintAddr(&pNetIpv6RtInfo->Ip6Dst));
								continue;
							}
							MEMCPY(&pLspCtrlBlock->NextHopAddr.Ip6Addr,
									&pNetIpv6RtInfo->NextHop,
									LDP_IPV6ADR_LEN);
							pLspCtrlBlock->u2AddrType=LDP_ADDR_TYPE_IPV6;
						}

						LdpMlibUpdateForEstbLsps (pLspCtrlBlock);
					}
					/* We have to return from here, 
					 * to avoid sending the label mapping message to upstream */
					if ((LDP_ENTITY_IS_TARGET_TYPE
								(SSN_GET_ENTITY (pLdpSession)) == LDP_TRUE)
							&& (pLdpSessionEntry == pLdpSession))
					{
						LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
						return;
					}

					if (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY
								(pLdpSessionEntry)) ==
							LDP_TRUE)
					{
						/* A.1.6. Recognize New FEC
						 * For Downstream Unsolicited Independent Control
						 * */

						MEMSET(&TempNextNode,LDP_ZERO,sizeof(tGenU4Addr));
						MEMCPY(LDP_IPV6_U4_ADDR(TempNextNode.Addr),
								pNetIpv6RtInfo->NextHop.u1_addr,
								LDP_IPV6ADR_LEN);
						TempNextNode.u2AddrType=LDP_ADDR_TYPE_IPV6;

						LdpDuIndLblReqOrNewFecProcedure (TRUE, pLspUpCtrlBlock,
								pLspCtrlBlock,
								&LdpMsgInfo,
								pLdpSessionEntry,
								&TempNextNode);

						/* A.1.1. Receive Label Request
						 * For Downstream Unsolicited ordered Control
						 * */
						LdpDuOrderLblReqOrNewFecProcedure (TRUE,
								pLspUpCtrlBlock,
								pLspCtrlBlock,
								&LdpMsgInfo,
								pLdpSessionEntry,
								&TempNextNode);
						LDP_DBG2 (LDP_SSN_PRCS,
								"Upstr list-Dn Node %p FEC %s\n",
								&UPSTR_DSTR_LIST_NODE (pLspUpCtrlBlock),
								Ip6PrintAddr(&LCB_FEC (pLspUpCtrlBlock).Prefix.Ip6Addr));
						return;
					}


					MEMSET(&TempNextNode,LDP_ZERO,sizeof(tGenU4Addr));
					MEMCPY(LDP_IPV6_U4_ADDR(TempNextNode.Addr),
							pNetIpv6RtInfo->NextHop.u1_addr,
							LDP_IPV6ADR_LEN);
					TempNextNode.u2AddrType=LDP_ADDR_TYPE_IPV6;

					if (LdpIsSsnForFec
							(&TempNextNode,pNetIpv6RtInfo->u4Index,
							 pLdpSessionEntry) == LDP_TRUE)
					{
						/* A.1.6. Recognize New FEC
						 * For Downstream Unsolicited Independent Control
						 * */

						LdpDuIndLblReqOrNewFecProcedure (TRUE, pLspUpCtrlBlock,
								pLspCtrlBlock,
								&LdpMsgInfo,
								pLdpSessionEntry,
								&TempNextNode);

						/* A.1.1. Recognize New FEC
						 * For Downstream Unsolicited ordered Control
						 * */
						LdpDuOrderLblReqOrNewFecProcedure (TRUE,
								pLspUpCtrlBlock,
								pLspCtrlBlock,
								&LdpMsgInfo,
								pLdpSessionEntry,
								&TempNextNode);
						LDP_DBG2 (LDP_SSN_PRCS,
								"Upstr list-Dn Node %p FEC %s\n",
								&UPSTR_DSTR_LIST_NODE (pLspUpCtrlBlock),
								Ip6PrintAddr(&LCB_FEC (pLspUpCtrlBlock).Prefix.Ip6Addr));
						return;
					}
				}
			}
		}

		/* If the policy is enabled, skip the label mapping message
		 * for not connected routes. */
		MEMSET(&TempAddr,LDP_ZERO,sizeof(tIp6Addr));

		MEMSET(&TempNextNode,LDP_ZERO,sizeof(tGenU4Addr));
		MEMCPY(LDP_IPV6_U4_ADDR(TempNextNode.Addr),
				pNetIpv6RtInfo->NextHop.u1_addr,
				LDP_IPV6ADR_LEN);
		TempNextNode.u2AddrType=LDP_ADDR_TYPE_IPV6;

		if ((gLdpInfo.u1Policy == LDP_POLICY_ENABLED) &&
				(MEMCMP(&pNetIpv6RtInfo->NextHop,&TempAddr,
					LDP_IPV6ADR_LEN)!= LDP_ZERO))
		{
			LDP_DBG (LDP_ADVT_PRCS,
					"LdpEstbIpv6LspsForDuSsns: Policy enabled to advertise "
					"only the connected routes.Not connected routed are not "
					"advertised to the peer \n");
			LdpDuDeleteUpLspCtrlBlock (pLspUpCtrlBlock);
		}


		if (MEMCMP(&pNetIpv6RtInfo->NextHop,
					&TempAddr,LDP_IPV6ADR_LEN) == LDP_ZERO)
		{
			/* Egress case - Since the label distribution mode is unsolicited,
			 * send label mapping message for both ordered and independent mode
			 * */
#ifdef LDP_GR_WANTED
			if (pLspUpCtrlBlock->u1StaleStatus == LDP_TRUE)
			{
				LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_REFRESH, &LdpMsgInfo);
			}
			else
			{
				LdpDuUpIdIntDnMap (pLspUpCtrlBlock, &LdpMsgInfo);
			}
#else

			/* Egress case - Since the label distribution mode is unsolicited,
			 * send label mapping message for both ordered and independent mode
			 * */
			LdpDuUpIdIntDnMap (pLspUpCtrlBlock, &LdpMsgInfo);
#endif
			return;
		}
		else if (LdpGetPeerSession (&TempNextNode,pNetIpv6RtInfo->u4Index,
					&pLdpSessionEntry,
					u2IncarnId) == LDP_FAILURE)
		{
			if (gi4MplsSimulateFailure != LDP_SIM_FAILURE_DISABLE_PROXY_EGRESS)
			{
				/* Proxy egress case - Since the label distribution mode is unsolicited,
				 * send label mapping message for both ordered and independent mode
				 * */
#ifdef LDP_GR_WANTED
				if (pLspUpCtrlBlock->u1StaleStatus == LDP_TRUE)
				{
					LdpDuUpSm (pLspUpCtrlBlock, LDP_DU_UP_LSM_EVT_INT_REFRESH, &LdpMsgInfo);
				}
				else
				{
					LdpDuUpIdIntDnMap (pLspUpCtrlBlock, &LdpMsgInfo);
				}
#else

				LdpDuUpIdIntDnMap (pLspUpCtrlBlock, &LdpMsgInfo);
#endif
			}
			return;
		}

		/* A.1.6. Recognize New FEC
		 * For Downstream Unsolicited Independent Control
		 * */

		LdpDuIndLblReqOrNewFecProcedure (FALSE, pLspUpCtrlBlock, NULL,
				&LdpMsgInfo, pLdpSessionEntry,
				&TempNextNode);

		/* Intermediate case */

		/* A.1.1. Recognize New FEC
		 * For Downstream Unsolicited ordered Control
		 * */
		LdpDuOrderLblReqOrNewFecProcedure (FALSE, pLspUpCtrlBlock, NULL,
				&LdpMsgInfo, pLdpSessionEntry,
				&TempNextNode);
	}
	else
	{
		LDP_DBG2 (LDP_SSN_PRCS, "Route issue for FEC %s Nexthop %s \n",
				Ip6PrintAddr(&pNetIpv6RtInfo->Ip6Dst),
				Ip6PrintAddr(&pNetIpv6RtInfo->NextHop));

		if (LdpIpUcastIpv6RouteQuery
				(&pNetIpv6RtInfo->Ip6Dst, &u4RtPort, &RtGw) == LDP_IP_FAILURE)
		{
			return;
		}

		MEMSET(&TempNextNode,LDP_ZERO,sizeof(tGenU4Addr));
		MEMCPY(LDP_IPV6_U4_ADDR(TempNextNode.Addr),
				RtGw.u1_addr,
				LDP_IPV6ADR_LEN);
		TempNextNode.u2AddrType=LDP_ADDR_TYPE_IPV6;
		if (LdpIsSsnForFec
				(&TempNextNode,pNetIpv6RtInfo->u4Index,pLdpSession) == LDP_FALSE)
		{
			LDP_DBG1 (LDP_SSN_PRCS, "FEC %s skipped\n",  Ip6PrintAddr(&pNetIpv6RtInfo->Ip6Dst));
			return;
		}

		/* Check whether LSR has a session with another peer.
		 * If not, the LSR is an ingress for the FEC
		 * */
		pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
		TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
		{
			TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
			{
				if ((pLdpPeer->pLdpSession) == NULL)
				{
					continue;
				}
				pLdpSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;

				if (pLdpSessionEntry != pLdpSession)
				{
					u1IsIngress = FALSE;
				}
			}
		}

		TMO_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode, tTMO_SLL_NODE *)
		{
			pLspCtrlBlock = SLL_TO_LCB (pLdpSllNode);
			if ((MEMCMP (&pLspCtrlBlock->Fec, &(Fec), sizeof (tFec))) != 0)
			{
				continue;
			}

			if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) ==
					LDP_DU_DN_MLIB_UPD_NOT_DONE)
			{ 
				MEMCPY(&pLspCtrlBlock->NextHopAddr.Ip6Addr,&pNetIpv6RtInfo->NextHop,LDP_IPV6ADR_LEN);
                                pLspCtrlBlock->u2AddrType=LDP_ADDR_TYPE_IPV6;
				LDP_DBG2 (LDP_SSN_PRCS,
						"Rt added DB prg to be done for Prefix %s NH %s\n",
						Ip6PrintAddr(&LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr),
						Ip6PrintAddr(&pLspCtrlBlock->NextHopAddr.Ip6Addr));

                if (LCB_STATE (pLspCtrlBlock) == LDP_DU_DN_LSM_ST_EST)
                {
                    LDP_DBG1 (LDP_SSN_PRCS, "Downstream Control Block for Prefix: %s is "
                                            "in ESTABLISHED State\n", 
                                            Ip6PrintAddr(&LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr));

                    LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_IDLE;
                    MEMSET (&LdpMsgInfo, 0, sizeof (tLdpMsgInfo));

                    LDP_DBG (LDP_SSN_PRCS, "Invoking State Machine Function\n");
                    LdpInvCorrStMh (pLdpSession, pLspCtrlBlock,
                                    LDP_LSM_EVT_LBL_MAP, &LdpMsgInfo, LDP_FALSE);                    
                }
				LdpMlibUpdateForEstbLsps (pLspCtrlBlock);
			}
			/* LSR has sent Label mapping message already */
			if (LCB_STATE (pLspCtrlBlock) == LDP_DU_DN_LSM_ST_EST)
			{
				u1DnCtrlBlockPresent = TRUE;
			}
		}

		/* If the LSR is an ingress and it has not received Label mapping message
		 * for the FEC already, it will send Label request message
		 * */

		if ((u1IsIngress == TRUE) && (u1DnCtrlBlockPresent == FALSE) &&
				((SSN_GET_ENTITY (pLdpSession)->u2LabelRet) ==
				 LDP_CONSERVATIVE_MODE))
		{

			MEMSET(&TempNextNode,LDP_ZERO,sizeof(tGenU4Addr));
			MEMCPY(&TempNextNode.Addr.Ip6Addr,
					&pNetIpv6RtInfo->NextHop,
					LDP_IPV6ADR_LEN);
			TempNextNode.u2AddrType=LDP_ADDR_TYPE_IPV6;
			LdpDuSendLblReqForIngress (pLdpSession,Fec,&TempNextNode);
		}
	}
	return;
}

#if 0

VOID
LdpEstbIpv6LspsForDoDMrgSsns (tNetIpv6RtInfo * tNetIpv6RtInfo, tLdpSession * pLdpSession)
{
	UINT1               u1PrefLen;
	tTMO_SLL           *pPeerAddrList = NULL;
	tPeerIfIpv6AdrNode     *pIfIpv6AddrNode = NULL;
	tPeerIfIpv6AdrNode     *pTmpIfIpv6AddrNode = NULL;
	tUstrLspCtrlBlock  *pUstrCtrlBlock = NULL;
	tLdpMsgInfo         MsgInfo;

	MEMSET (&MsgInfo, 0, sizeof (tLdpMsgInfo));

	/* Getting the Peer's Interface Addresses' List */
	pPeerAddrList = SSN_GET_PEER_IFIPV6ADR_LIST (pLdpSession);

	/* Getting the Prefix length from the obtained Destination Mask */
	u1PrefLen=pNetIpv6RtInfo->u1Prefixlen;

	/*
	 ** Scanning through the Peer's addresses and establishing Lsp, if the
	 ** Next Hop Address through which the Destination is reachable, belongs
	 ** to the LdpPeer associated with the present LdpSession.
	 **/

	TMO_SLL_Scan (pPeerAddrList, pTmpIfIpv6AddrNode, tPeerIfIpv6AdrNode *)
	{
		pIfIpv6AddrNode = LDP_OFFSET (pTmpIfIpv6AddrNode);

		if (((SSN_GET_ENTITY (pLdpSession)->u2LabelRet) ==
					LDP_CONSERVATIVE_MODE))
		{
			if (MEMCMP(&pIfIpv6AddrNode->IfAddr,
						&pNetIpv6RtInfo->NextHop,
						LDP_IPV6ADR_LEN)==LDP_ZERO)
			{
				pUstrCtrlBlock = NULL;
				LdpCreateUpstrCtrlBlock (&pUstrCtrlBlock);
				if (pUstrCtrlBlock == NULL)
				{
					return;
				}
				pUstrCtrlBlock->Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
				pUstrCtrlBlock->Fec.u1PreLen = u1PrefLen;
				pUstrCtrlBlock->Fec.u2AddrFmly = LDP_ADDR_TYPE_IPV6;
#if 0
/*** vishal_1 : update for IPV6 FEC */
				pUstrCtrlBlock->Fec.u4Prefix = pInRtInfo->u4DestNet;
#endif
				UPSTR_STATE (pUstrCtrlBlock) = LDP_LSM_ST_IDLE;
				LdpMergeUpSm (pUstrCtrlBlock, LDP_USM_EVT_INT_LDP_REQ,
						&MsgInfo);
				break;
			}
		}
		else if (((SSN_GET_ENTITY (pLdpSession)->u2LabelRet) ==
					LDP_LIBERAL_MODE))
		{
			if (LdpIsLsrPartOfIpv6ErHopOrFec (&pNetIpv6RtInfo->Ip6Dst,
						pNetIpv6RtInfo->u1Prefixlen) ==
					LDP_FALSE)
			{
				pUstrCtrlBlock = NULL;
				LdpCreateUpstrCtrlBlock (&pUstrCtrlBlock);
				if (pUstrCtrlBlock == NULL)
				{
					return;
				}
				pUstrCtrlBlock->Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
				pUstrCtrlBlock->Fec.u1PreLen = u1PrefLen;
				pUstrCtrlBlock->Fec.u2AddrFmly = LDP_ADDR_TYPE_IPV6;
#if 0
/** vishal_1 : update it for FEC */
				pUstrCtrlBlock->Fec.u4Prefix = pInRtInfo->u4DestNet;
#endif
				UPSTR_STATE (pUstrCtrlBlock) = LDP_LSM_ST_IDLE;
				LdpMergeUpSm (pUstrCtrlBlock,
						LDP_USM_EVT_INT_LDP_REQ, &MsgInfo);
				break;
			}
		}
	}
	return;
}
#endif



VOID
LdpGetLspIpv6RouteInfo (tNetIpv6RtInfo * pNetIpv6RtInfo, tLdpSession * pLdpSession)
{
    UINT1               u1PrefLen;
    tTMO_SLL           *pPeerAddrList = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tPeerIfAdrNode     *pTmpIfAddrNode = NULL;
    tIntLspSetupInfo   *pIntLspSetup = NULL;

    pIntLspSetup = (tIntLspSetupInfo *)
	    MemAllocMemBlk (LDP_INT_LSPSETUP_POOL_ID);

    if (pIntLspSetup == NULL)
    {
	    LDP_DBG ((LDP_IF_MEM | LDP_MAIN_MEM),
			    "Memory Allocation failed for LSP Setup Pool\r\n");
	    /* No Lsps can be established as stack memory is not left */
	    return;
    }

    /* Getting the Peer's Interface Addresses' List */
    pPeerAddrList = SSN_GET_PEER_IFADR_LIST (pLdpSession);

    /* Getting the Prefix length from the obtained Destination Mask */
    u1PrefLen=pNetIpv6RtInfo->u1Prefixlen;

    /*
     * Scanning through the Peer's addresses and establishing Lsp, if the
     * Next Hop Address through which the Destination is reachable, belongs
     * to the LdpPeer associated with the present LdpSession.
     **/

    TMO_SLL_Scan (pPeerAddrList, pTmpIfAddrNode, tPeerIfAdrNode *)
    {
	    pIfAddrNode = LDP_OFFSET (pTmpIfAddrNode);

        if (pIfAddrNode->AddrType != MPLS_IPV6_ADDR_TYPE)
        {
            continue;
        } 

	    if (MEMCMP(&pIfAddrNode->IfAddr,&pNetIpv6RtInfo->NextHop,LDP_IPV6ADR_LEN)
			    !=LDP_ZERO)
	    {
		    continue;
	    }

	    if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
			    ((SSN_GET_ENTITY (pLdpSession)->u2LabelRet) ==
			     LDP_CONSERVATIVE_MODE))
	    {
		    pIntLspSetup->u2IncarnId = u2IncarnId;
		    MEMCPY(&pIntLspSetup->Dest.Ip6Addr,&pNetIpv6RtInfo->Ip6Dst,
				    LDP_IPV6ADR_LEN);
		    pIntLspSetup->u1PrefixLen = u1PrefLen;
		    MEMCPY(&pIntLspSetup->NextHop.Ip6Addr,&pNetIpv6RtInfo->NextHop,
				    LDP_IPV6ADR_LEN);
		    pIntLspSetup->u4IfIndex = pNetIpv6RtInfo->u4Index;
		    pIntLspSetup->pLdpSession = pLdpSession;
                    pIntLspSetup->u2AddrType  = LDP_ADDR_TYPE_IPV6;
		    LdpIntLspSetReq (pIntLspSetup);
		    break;
	    }
	    else if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
			    ((SSN_GET_ENTITY (pLdpSession)->u2LabelRet) ==
			     LDP_LIBERAL_MODE))
	    {

		    if (LdpIsLsrPartOfIpv6ErHopOrFec (&pNetIpv6RtInfo->Ip6Dst,
					    pNetIpv6RtInfo->u1Prefixlen) ==
				    LDP_FALSE)
		    {
			    pIntLspSetup->u2IncarnId = u2IncarnId;	    
			    MEMCPY(&pIntLspSetup->Dest.Ip6Addr,&pNetIpv6RtInfo->Ip6Dst,
					    LDP_IPV6ADR_LEN);
			    pIntLspSetup->u1PrefixLen = u1PrefLen;	    
			    MEMCPY(&pIntLspSetup->NextHop.Ip6Addr,&pNetIpv6RtInfo->NextHop,
					    LDP_IPV6ADR_LEN);
			    pIntLspSetup->u4IfIndex = pNetIpv6RtInfo->u4Index;
			    pIntLspSetup->pLdpSession = pLdpSession;
			    pIntLspSetup->u2AddrType  = LDP_ADDR_TYPE_IPV6;
			    LdpIntLspSetReq (pIntLspSetup);
			    break;
		    }
	    }
    }
    MemReleaseMemBlock (LDP_INT_LSPSETUP_POOL_ID, (UINT1 *) pIntLspSetup);
    return;
}
#endif
