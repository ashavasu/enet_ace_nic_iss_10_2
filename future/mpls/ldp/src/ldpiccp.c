/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpiccp.c,v 1.4 2015/02/05 11:29:57 siva Exp $
 *
 * Description: This file contains the ICCP related message handling
 *              and TLV formation
 *******************************************************************/

#include "ldpincs.h"
/*****************************************************************************/
/* Function Name : LdpProcessPwVcIccpMsgEvent                                */
/* Description   : This routine processes events received from L2VPN         */
/* Input(s)      : pIccpmsgInfo- info about ICCP events and data received    */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpProcessPwVcIccpMsgEvent (tL2VpnLdpIccpEvtInfo * pIccpMsgInfo)
{
    tLdpSession        *pPeerSession = NULL;
    UINT4               u2IncarnId = LDP_CUR_INCARN;
    UINT4               u4PeerAddr = LDP_ZERO;
    UINT1               u1RetVal = LDP_FAILURE;
    uGenU4Addr          PeerAddr;

    MEMSET(&PeerAddr,LDP_ZERO,sizeof(uGenU4Addr));

    LDP_DBG (LDP_MAIN_MISC, "ENTRY\n");

    switch (pIccpMsgInfo->u2MsgType)
    {
        case L2VPN_LDP_ICCP_RG_DATA_MSG:

		MEMCPY (&u4PeerAddr, &pIccpMsgInfo->PeerAddr, sizeof (UINT4));
		LDP_IPV4_U4_ADDR(PeerAddr)= u4PeerAddr;
		if (LdpGetSessionWithPeer (u2IncarnId,
					pIccpMsgInfo->u4LocalLdpEntityId,
					&PeerAddr,LDP_ADDR_TYPE_IPV4,LDP_DSTR_UNSOLICIT,
					LDP_TRUE, &pPeerSession) == LDP_FAILURE)
		{
                LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 1. LDP Entity not"
                         " configured\n");
                return LDP_FAILURE;
            }
            if ((LdpSendPwRedAppDataMsg (pPeerSession, pIccpMsgInfo))
                == LDP_FAILURE)
            {
                LDP_DBG (LDP_MAIN_MISC, "Failed to send PW-RED Application"
                         " Data Msg\n");
            }
            break;
        case L2VPN_LDP_ICCP_RG_CONNECT_MSG:
        case L2VPN_LDP_ICCP_RG_DISCONNECT_MSG:
        case L2VPN_LDP_ICCP_RG_NOTIFICATION_MSG:
            LDP_DBG (LDP_MAIN_MISC, "Unsupported ICCP Message type "
                     "received from L2VPN\n");
            break;
        default:
            LDP_DBG (LDP_MAIN_MISC, "Rcvd Invalid ICCP message type "
                     "from L2VPN\n");
            break;
    }

    LDP_DBG (LDP_MAIN_MISC, "EXIT 2\n");
    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : LdpSendPwRedAppDataMsg                                    */
/* Description   : This function sends the PW-RED Application Data Message   */
/*                 to the peer identified by session pLdpPeerSession.        */
/* Input(s)      : pLdpPeerSession - Pointer to the session to which the     */
/*                                   message  is to be sent                  */
/*                 pIccpMsgInfo     - Pointer to the ICCP messge Information */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpSendPwRedAppDataMsg (tLdpSession * pLdpPeerSession,
                        tL2VpnLdpIccpEvtInfo * pIccpMsgInfo)
{
    tLdpMsgHdrAndId     MsgHdrAndId;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tRgIdTlv            RgIdTlv;
    tPwRedSyncRqstTlv   PwRedSyncRqstTlv;
    tPwRedSyncDataTlv   PwRedSyncDataTlv;
    UINT4               u4OffSet = 0;
    UINT2               u2OptParamLen = 0;
    UINT2               u2MandParamLen = 0;
    UINT1               u1Index = 0;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    LDP_DBG (LDP_MAIN_MISC, "ENTRY\n");

    /* Re-Align SLLs */
    TMO_SLL_Copy_Adjust (&pIccpMsgInfo->u.PwData.DataList);
    TMO_SLL_Copy_Adjust (&pIccpMsgInfo->u.PwData.RequestList);

    MEMSET (&MsgHdrAndId, 0, sizeof (tLdpMsgHdrAndId));
    MEMSET (&RgIdTlv, 0, sizeof (tRgIdTlv));
    MEMSET (&PwRedSyncRqstTlv, 0, sizeof (tPwRedSyncRqstTlv));
    MEMSET (&PwRedSyncDataTlv, 0, sizeof (tPwRedSyncDataTlv));
    MEMSET (&ConnId, LDP_ZERO, sizeof (tGenAddr));
    MEMSET (&LdpIfAddrZero, LDP_ZERO, sizeof(tGenU4Addr));
    u2MandParamLen = (UINT2) (u2MandParamLen + LDP_MSG_ID_LEN);
    u2MandParamLen = (UINT2) (u2MandParamLen + LDP_ICCP_RG_ID_TLV_LEN);

    if ((pIccpMsgInfo->u.PwData.u2RequestNum == 0) &&
        (TMO_SLL_Count (&pIccpMsgInfo->u.PwData.DataList) == 0))
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 1: "
                 "Nothing has to be sent in RG Application Data Message\n");
        return LDP_FAILURE;
    }

    /* If request number is not equal to ZERO, then PW-RED Synchronization
     * Request TLV should be sent*/
    if (pIccpMsgInfo->u.PwData.u2RequestNum != 0)
    {
        LdpFillPwRedSyncRqstTlv (&(pIccpMsgInfo->u.PwData),
                                 &u2OptParamLen, &PwRedSyncRqstTlv);
    }

    /* If the number of nodes in Data List is not equal to ZERO, then
     * PW RED Synchronous Data TLV must be sent */
    if (TMO_SLL_Count (&pIccpMsgInfo->u.PwData.DataList) > 0)
    {
        LdpFillPwRedSyncDataTlv (&(pIccpMsgInfo->u.PwData),
                                 &u2OptParamLen, &PwRedSyncDataTlv);
    }
    /* Allocate Buffer for Notification Msg */
    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((UINT4)
                                      (LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2OptParamLen + u2MandParamLen),
                                      LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 2: "
                 "Can't Allocate Buffer for PW-RED Appllication Data Msg \n");
        return LDP_FAILURE;
    }

    /* Construct Message Hdr and ID */
    MsgHdrAndId.u2MsgType = OSIX_HTONS (LDP_ICCP_RG_DATA_MSG);
    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2OptParamLen + u2MandParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pLdpPeerSession));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pLdpPeerSession));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4OffSet,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 3: "
                 "Error copying MsgHdr & Id to Buf chain\n");
        return LDP_FAILURE;
    }
    u4OffSet += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* RG ID TLV */
    RgIdTlv.TlvHdr.u2TlvType = OSIX_HTONS (LDP_ICCP_RG_ID_TLV);
    RgIdTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_ICCP_RG_ID_LEN);
    RgIdTlv.u4RgId = OSIX_HTONL (pIccpMsgInfo->u4GroupId);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &RgIdTlv, u4OffSet,
                                   LDP_ICCP_RG_ID_TLV_LEN) == CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 4: "
                 "Error copying RG ID TLV Buf chain\n");
        return LDP_FAILURE;
    }
    u4OffSet += LDP_ICCP_RG_ID_TLV_LEN;

    if ((PwRedSyncDataTlv.u1ConfigTlvCount != 0) ||
        (PwRedSyncDataTlv.u1StateTlvCount != 0))
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &(PwRedSyncDataTlv.SyncDataTlvHdr[0]), u4OffSet,
             LDP_ICCP_PW_RED_RQST_DATA_TLV_HDR_LEN) == CRU_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 5: "
                     "Error copying PW-RED Synchronization Data Start TLV Header to Buf chain\n");
            return LDP_FAILURE;
        }
        u4OffSet += LDP_ICCP_PW_RED_RQST_DATA_TLV_HDR_LEN;

        for (u1Index = 0; u1Index < PwRedSyncDataTlv.u1ConfigTlvCount;
             u1Index++)
        {
            if (CRU_BUF_Copy_OverBufChain
                (pMsg,
                 (UINT1 *) &(PwRedSyncDataTlv.PwRedConfigTlv[u1Index].
                             PwRedConfigTlvHdr), u4OffSet,
                 LDP_ICCP_PW_RED_CONFIG_TLV_HDR_LEN) == CRU_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 6: "
                         "Error copying Config TLV Header in PW RED Data TLV to Buf chain\n");
                return LDP_FAILURE;
            }
            u4OffSet += LDP_ICCP_PW_RED_CONFIG_TLV_HDR_LEN;

            if (CRU_BUF_Copy_OverBufChain
                (pMsg,
                 (UINT1 *) &(PwRedSyncDataTlv.PwRedConfigTlv[u1Index].
                             ServiceNameTlv), u4OffSet,
                 (UINT4) (LDP_TLV_HDR_LEN +
                          OSIX_NTOHS (PwRedSyncDataTlv.PwRedConfigTlv[u1Index].
                                      ServiceNameTlv.TlvHdr.u2TlvLen))) ==
                CRU_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 7: "
                         "Error copying Service name TLV in PW RED Synchronization "
                         "Data TLV to Buf chain\n");
                return LDP_FAILURE;
            }
            u4OffSet +=
                (UINT4) (LDP_TLV_HDR_LEN +
                         OSIX_NTOHS (PwRedSyncDataTlv.PwRedConfigTlv[u1Index].
                                     ServiceNameTlv.TlvHdr.u2TlvLen));

            if (PwRedSyncDataTlv.PwRedConfigTlv[u1Index].u1FecType ==
                LDP_ICCP_FILL_PW_ID_TLV)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg,
                     (UINT1 *) &(PwRedSyncDataTlv.PwRedConfigTlv[u1Index].
                                 PwIdOrGenPwIdTlv.PwIdTlv), u4OffSet,
                     LDP_ICCP_PW_ID_TLV_LEN) == CRU_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 8: "
                             "Error copying PW ID TLV in PW-RED Synchronization Data "
                             "TLV to Buf chain\n");
                    return LDP_FAILURE;
                }
                u4OffSet += LDP_ICCP_PW_ID_TLV_LEN;
            }
            else if (PwRedSyncDataTlv.PwRedConfigTlv[u1Index].u1FecType ==
                     LDP_ICCP_FILL_GEN_PW_ID_TLV)
            {
                if (LdpCopyGenPwIdTlvToBuf
                    (pMsg,
                     &(PwRedSyncDataTlv.PwRedConfigTlv[u1Index].
                       PwIdOrGenPwIdTlv.GenPwIdTlv), &u4OffSet) == LDP_FAILURE)
                {
                    LDP_DBG (LDP_MAIN_MISC,
                             "ICCP : Error copying Gen PW ID TLV in PW-RED Synchronization Data TLV\n");
                }
            }
        }

        for (u1Index = 0; u1Index < PwRedSyncDataTlv.u1StateTlvCount; u1Index++)
        {
            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(PwRedSyncDataTlv.
                                                             PwRedStateTlv
                                                             [u1Index]),
                                           u4OffSet,
                                           LDP_ICCP_PW_RED_STATE_TLV_LEN) ==
                CRU_FAILURE)
            {
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 9: "
                         "Error copying PW-RED State TLV in PW-RED Synchronization "
                         "Data TLV to Buf chain\n");
                return LDP_FAILURE;
            }
            u4OffSet += LDP_ICCP_PW_RED_STATE_TLV_LEN;
        }

        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &(PwRedSyncDataTlv.SyncDataTlvHdr[1]), u4OffSet,
             LDP_ICCP_PW_RED_RQST_DATA_TLV_HDR_LEN) == CRU_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 10: "
                     "Error copying PW-RED Synchronization Data End TLV Header to Buf chain\n");
            return LDP_FAILURE;
        }
        u4OffSet += LDP_ICCP_PW_RED_RQST_DATA_TLV_HDR_LEN;
    }

    if (pIccpMsgInfo->u.PwData.u2RequestNum != 0)
    {
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &(PwRedSyncRqstTlv.RqstTlvHdr), u4OffSet,
             LDP_ICCP_PW_RED_REQUEST_TLV_HDR_LEN) == CRU_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 11: "
                     "Error copying PW-RED Synchronization Request TLV Header to Buf chain\n");
            return LDP_FAILURE;
        }
        u4OffSet += LDP_ICCP_PW_RED_REQUEST_TLV_HDR_LEN;

        for (u1Index = 0; u1Index < PwRedSyncRqstTlv.u1Count; u1Index++)
        {
            if (PwRedSyncRqstTlv.u1Type == LDP_ICCP_FILL_SERVICE_NAME_TLV)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg,
                     (UINT1 *) &(PwRedSyncRqstTlv.RqstOptTlv[u1Index].
                                 ServiceNameTlv), u4OffSet,
                     (UINT4) (LDP_TLV_HDR_LEN +
                              OSIX_NTOHS (PwRedSyncRqstTlv.RqstOptTlv[u1Index].
                                          ServiceNameTlv.TlvHdr.u2TlvLen))) ==
                    CRU_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 12: "
                             "Error copying Service name TLV in PW-RED Synchronization "
                             "Request TLV to Buf chain\n");
                    return LDP_FAILURE;
                }
                u4OffSet +=
                    (UINT4) (LDP_TLV_HDR_LEN +
                             OSIX_NTOHS (PwRedSyncRqstTlv.RqstOptTlv[u1Index].
                                         ServiceNameTlv.TlvHdr.u2TlvLen));
            }
            else if (PwRedSyncRqstTlv.u1Type == LDP_ICCP_FILL_PW_ID_TLV)
            {
                if (CRU_BUF_Copy_OverBufChain
                    (pMsg,
                     (UINT1 *) &(PwRedSyncRqstTlv.RqstOptTlv[u1Index].
                                 PwIdOrGenPwIdTlv.PwIdTlv), u4OffSet,
                     LDP_ICCP_PW_ID_TLV_LEN) == CRU_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 13: "
                             "Error copying PW ID TLV in PW-RED Synchronization "
                             "Request TLV to Buf chain\n");
                    return LDP_FAILURE;
                }
                u4OffSet += LDP_ICCP_PW_ID_TLV_LEN;
            }
            else if (PwRedSyncRqstTlv.u1Type == LDP_ICCP_FILL_GEN_PW_ID_TLV)
            {
                if (LdpCopyGenPwIdTlvToBuf (pMsg, &(PwRedSyncRqstTlv.RqstOptTlv
                                                    [u1Index].PwIdOrGenPwIdTlv.
                                                    GenPwIdTlv),
                                            &u4OffSet) == LDP_FAILURE)
                {
                    LDP_DBG (LDP_MAIN_MISC,
                             "ICCP : Error copying Gen PW ID TLV in PW-RED Synchronization Request TLV\n");
                }
            }
        }
    }
    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

        /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pLdpPeerSession->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    (pLdpPeerSession->pLdpPeer->pLdpEntity),
                    LDP_FALSE, ConnId,
                    LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
                /*MPLS_IPv6 add end*/
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 14: "
                 "Failed to send PW RED Application Data Msg\n");
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_MAIN_MISC, "ICCP: EXIT 15: Successfuly sent PW RED "
             "Application Data Msg\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpFillPwRedSyncRqstTlv                                   */
/* Description   : This function fill the synchronization request TLV to be  */
/*                 added in the PW-RED App Data Message                      */
/* Input(s)      : pPwRedData - Pointer to the PW redundancy data            */
/* Output(s)     : pu2Len - pointer to the optional parameter TLV length in  */
/*                          PW-RED App Data Message                          */
/*                 pPwRedSyncRqstTlv - pointer to the filled synchronization */
/*                                     request tlv info                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpFillPwRedSyncRqstTlv (tPwRedDataEvtArgs * pPwRedData, UINT2 *pu2Len,
                         tPwRedSyncRqstTlv * pPwRedSyncRqstTlv)
{
    tPwRedDataEvtPwFec *pPwFecData = NULL;
    tPwRedDataEvtPwFec *pTemp = NULL;
    VOID               *pSllNode = NULL;
    UINT2               u2RqstType = 0;
    UINT2               u2Len = 0;

    LDP_DBG1 (LDP_MAIN_MISC, "ENTRY. pu2Len(%u)\n", *pu2Len);

    *pu2Len = (UINT2) (*pu2Len + LDP_TLV_HDR_LEN);

    u2Len = (UINT2) (u2Len + LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN);
    u2Len = (UINT2) (u2Len + LDP_ICCP_SYNC_RQST_TYPE_FIELD_LEN);

    pPwRedSyncRqstTlv->RqstTlvHdr.TlvHdr.u2TlvType =
        OSIX_HTONS (LDP_ICCP_PW_RED_SYNC_RQST_TLV);
    pPwRedSyncRqstTlv->RqstTlvHdr.u2RqstId =
        OSIX_HTONS (pPwRedData->u2RequestNum);

    /* 'C' bit in Synchronization request TLV */
    if (pPwRedData->u1RequestType & L2VPNRED_ICCP_REQ_CONFIG)
    {
        u2RqstType |= L2VPNRED_ICCP_REQ_PW_CONFIG;
    }
    /* 'S' bit in Synchronization request TLV */
    if (pPwRedData->u1RequestType & L2VPNRED_ICCP_REQ_STATE)
    {
        u2RqstType |= L2VPNRED_ICCP_REQ_PW_STATE;
    }

    if (pPwRedData->u1RequestType & L2VPNRED_ICCP_REQ_ALL)
    {
        /* If the request is for all DATA, then no sub TLVs are needed
         * So return after updating the length and rqst type field*/
        u2RqstType |= L2VPNRED_ICCP_REQ_PW_TYPE_MASK;
        pPwRedSyncRqstTlv->RqstTlvHdr.u2RqstType = OSIX_HTONS (u2RqstType);
        pPwRedSyncRqstTlv->RqstTlvHdr.TlvHdr.u2TlvLen =
            OSIX_HTONS ((LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN +
                         LDP_ICCP_SYNC_RQST_TYPE_FIELD_LEN));
        *pu2Len = (UINT2) (*pu2Len + u2Len);
        LDP_DBG1 (LDP_MAIN_MISC, "EXIT 1. pu2Len(%u)\n", *pu2Len);
        return;
    }

    if (pPwRedData->u1RequestType & L2VPNRED_ICCP_REQ_SERVICE_NAME)
    {
        u2RqstType |= L2VPNRED_ICCP_REQ_PW_SERVICE_NAME;
    }
    /* The value for Specific PWs request is 0x00. So nothing needs to be updated
     * in the u2RqstType, if the request is for Specific PWs*/

    /* Update the request type field in synchronization request tlv header */
    pPwRedSyncRqstTlv->RqstTlvHdr.u2RqstType = OSIX_HTONS (u2RqstType);

    /* Scan the request list and fill the required TLVs.
     * it is required only if the request type is for specific PWs or specifc service name */

    TMO_DYN_SLL_Scan (&(pPwRedData->RequestList), pSllNode, pTemp,
                      tPwRedDataEvtPwFec *)
    {
        pPwFecData = CONTAINER_OF (pSllNode, tPwRedDataEvtPwFec, ListNode);

        if (pPwRedData->u1RequestType & L2VPNRED_ICCP_REQ_SERVICE_NAME)
        {
            /* If L2VPN requests info about the pseudowires belongs to specific
             * service name, then it may contain one or more Service Name TLV*/
            u2Len = (UINT2) (u2Len + LDP_TLV_HDR_LEN);
            u2Len = (UINT2) (u2Len + STRLEN (pPwFecData->u.Name));
            pPwRedSyncRqstTlv->u1Type = LDP_ICCP_FILL_SERVICE_NAME_TLV;

            pPwRedSyncRqstTlv->RqstOptTlv[pPwRedSyncRqstTlv->u1Count].
                ServiceNameTlv.TlvHdr.u2TlvType =
                OSIX_HTONS (LDP_ICCP_SERVICE_NAME_TLV);
            pPwRedSyncRqstTlv->RqstOptTlv[pPwRedSyncRqstTlv->u1Count].
                ServiceNameTlv.TlvHdr.u2TlvLen =
                OSIX_HTONS ((STRLEN (pPwFecData->u.Name)));
            MEMCPY (pPwRedSyncRqstTlv->RqstOptTlv[pPwRedSyncRqstTlv->u1Count].
                    ServiceNameTlv.PwServiceName, pPwFecData->u.Name,
                    STRLEN (pPwFecData->u.Name));

            pPwRedSyncRqstTlv->u1Count++;
        }
        else
        {
            /* If L2VPN requests info about the specific pseudowires, then it may
             * contain one or more PW ID/Generalized PW ID TLV*/

            LdpFillIccpPwIdTlv (&pPwFecData->u.Fec,
                                &(pPwRedSyncRqstTlv->
                                  RqstOptTlv[pPwRedSyncRqstTlv->u1Count].
                                  PwIdOrGenPwIdTlv), &u2Len,
                                &pPwRedSyncRqstTlv->u1Type);

            pPwRedSyncRqstTlv->u1Count++;
        }
        TMO_SLL_Delete (&(pPwRedData->RequestList),
                        (tTMO_SLL_NODE *) pPwFecData);
        LdpL2VpnCleanEventPwFec ((VOID *) pPwFecData);
    }
    *pu2Len += u2Len;
    pPwRedSyncRqstTlv->RqstTlvHdr.TlvHdr.u2TlvLen = OSIX_HTONS (u2Len);

    LDP_DBG1 (LDP_MAIN_MISC, "EXIT 2. pu2Len(%u)\n", *pu2Len);
    return;
}

/*****************************************************************************/
/* Function Name : LdpFillPwRedSyncDataTlv                                   */
/* Description   : This function fill the synchronization request TLV to be  */
/*                 added in the PW-RED App Data Message                      */
/* Input(s)      : pPwRedData - Pointer to the PW redundancy data            */
/* Output(s)     : pu2Len - pointer to the optional parameter TLV length in  */
/*                          PW-RED App Data Message                          */
/*                 pPwRedSyncDataTlv - pointer to the filled synchronization */
/*                                     Data tlv info                         */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpFillPwRedSyncDataTlv (tPwRedDataEvtArgs * pPwRedData, UINT2 *pu2Len,
                         tPwRedSyncDataTlv * pPwRedSyncDataTlv)
{
    tPwRedDataEvtPwData *pPwIccpInfo = NULL;
    tPwRedDataEvtPwData *pTemp = NULL;
    VOID               *pSllNode = NULL;
    UINT2               u2Len = 0;
    UINT2               u2ConfigTlvLen = 0;

    LDP_DBG1 (LDP_MAIN_MISC, "ENTRY. pu2Len(%u)\n", *pu2Len);

    *pu2Len = (UINT2) (*pu2Len + LDP_TLV_HDR_LEN);

    /*Fill Synchronization Data Start Header */
    pPwRedSyncDataTlv->SyncDataTlvHdr[0].TlvHdr.u2TlvType =
        OSIX_HTONS (LDP_ICCP_PW_RED_SYNC_DATA_TLV);
    pPwRedSyncDataTlv->SyncDataTlvHdr[0].u2RqstId =
        OSIX_HTONS (pPwRedData->u2ResponseNum);

    u2Len =
        (UINT2) (u2Len + LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN +
                 LDP_ICCP_PW_RED_FLAG_FIELD_LEN);

    TMO_DYN_SLL_Scan (&(pPwRedData->DataList), pSllNode, pTemp,
                      tPwRedDataEvtPwData *)
    {
        pPwIccpInfo = CONTAINER_OF (pSllNode, tPwRedDataEvtPwData, ListNode);

        if (pPwIccpInfo->u1Type & L2VPNRED_ICCP_DATA_CONFIG)
        {
            /* Fill config TLV header */
            u2Len = (UINT2) (u2Len + LDP_TLV_HDR_LEN);
            u2ConfigTlvLen =
                (LDP_ICCP_ROID_LEN + LDP_ICCP_PW_PRIORITY_FIELD_LEN +
                 LDP_ICCP_PW_RED_FLAG_FIELD_LEN);

            pPwRedSyncDataTlv->PwRedConfigTlv[pPwRedSyncDataTlv->
                                              u1ConfigTlvCount].
                PwRedConfigTlvHdr.TlvHdr.u2TlvType =
                OSIX_HTONS (LDP_ICCP_PW_RED_CONFIG_TLV);
            MEMCPY (&
                    (pPwRedSyncDataTlv->
                     PwRedConfigTlv[pPwRedSyncDataTlv->u1ConfigTlvCount].
                     PwRedConfigTlvHdr.RoId), &(pPwIccpInfo->Data.RoId),
                    sizeof (tL2vpnIccpRoId));

            pPwRedSyncDataTlv->PwRedConfigTlv[pPwRedSyncDataTlv->
                                              u1ConfigTlvCount].
                PwRedConfigTlvHdr.u2PwPriority =
                OSIX_HTONS (pPwIccpInfo->Data.u2Priority);

            if (pPwIccpInfo->u1Type & L2VPNRED_ICCP_DATA_SYNCHRONIZED)
            {
                pPwRedSyncDataTlv->PwRedConfigTlv[pPwRedSyncDataTlv->
                                                  u1ConfigTlvCount].
                    PwRedConfigTlvHdr.u2Flags =
                    OSIX_HTONS (LDP_ICCP_PW_SYNCHRONIZED);
            }
            else if (pPwIccpInfo->u1Type & L2VPNRED_ICCP_DATA_PURGED)
            {
                pPwRedSyncDataTlv->PwRedConfigTlv[pPwRedSyncDataTlv->
                                                  u1ConfigTlvCount].
                    PwRedConfigTlvHdr.u2Flags = OSIX_HTONS (LDP_ICCP_PW_PURGED);
            }

            /* Fill Service name tlv */
            u2ConfigTlvLen = (UINT2) (u2ConfigTlvLen + LDP_TLV_HDR_LEN);
            u2ConfigTlvLen =
                (UINT2) (u2ConfigTlvLen + STRLEN (pPwIccpInfo->Data.Name));

            pPwRedSyncDataTlv->PwRedConfigTlv[pPwRedSyncDataTlv->
                                              u1ConfigTlvCount].ServiceNameTlv.
                TlvHdr.u2TlvType = OSIX_HTONS (LDP_ICCP_SERVICE_NAME_TLV);
            pPwRedSyncDataTlv->PwRedConfigTlv[pPwRedSyncDataTlv->
                                              u1ConfigTlvCount].ServiceNameTlv.
                TlvHdr.u2TlvLen =
                OSIX_HTONS ((STRLEN (pPwIccpInfo->Data.Name)));
            MEMCPY (pPwRedSyncDataTlv->
                    PwRedConfigTlv[pPwRedSyncDataTlv->u1ConfigTlvCount].
                    ServiceNameTlv.PwServiceName, pPwIccpInfo->Data.Name,
                    STRLEN (pPwIccpInfo->Data.Name));

            /* Fill PW ID TLV/GEN PW ID TLV */
            LdpFillIccpPwIdTlv (&(pPwIccpInfo->Data.Fec),
                                &(pPwRedSyncDataTlv->
                                  PwRedConfigTlv[pPwRedSyncDataTlv->
                                                 u1ConfigTlvCount].
                                  PwIdOrGenPwIdTlv), &u2ConfigTlvLen,
                                &(pPwRedSyncDataTlv->
                                  PwRedConfigTlv[pPwRedSyncDataTlv->
                                                 u1ConfigTlvCount].u1FecType));

            pPwRedSyncDataTlv->PwRedConfigTlv[pPwRedSyncDataTlv->
                                              u1ConfigTlvCount].
                PwRedConfigTlvHdr.TlvHdr.u2TlvLen = OSIX_HTONS (u2ConfigTlvLen);

            pPwRedSyncDataTlv->u1ConfigTlvCount++;

            u2Len += u2ConfigTlvLen;
        }
        /* End of Config TLV */

        /* Fill State TLV */
        if (pPwIccpInfo->u1Type & L2VPNRED_ICCP_DATA_STATE)
        {
            u2Len = (UINT2) (u2Len + LDP_ICCP_PW_RED_STATE_TLV_LEN);
            pPwRedSyncDataTlv->PwRedStateTlv[pPwRedSyncDataTlv->
                                             u1StateTlvCount].TlvHdr.u2TlvType =
                OSIX_HTONS (LDP_ICCP_PW_RED_STATE_TLV);
            pPwRedSyncDataTlv->PwRedStateTlv[pPwRedSyncDataTlv->
                                             u1StateTlvCount].TlvHdr.u2TlvLen =
                OSIX_HTONS ((LDP_ICCP_ROID_LEN + LDP_ICCP_PW_STATE_FIELD_LEN +
                             LDP_ICCP_PW_STATE_FIELD_LEN));
            MEMCPY (&
                    (pPwRedSyncDataTlv->
                     PwRedStateTlv[pPwRedSyncDataTlv->u1StateTlvCount].RoId),
                    &(pPwIccpInfo->Data.RoId), sizeof (tL2vpnIccpRoId));
            pPwRedSyncDataTlv->PwRedStateTlv[pPwRedSyncDataTlv->
                                             u1StateTlvCount].u4RemotePwState =
                OSIX_HTONL (pPwIccpInfo->Data.u4RemoteStatus);
            pPwRedSyncDataTlv->PwRedStateTlv[pPwRedSyncDataTlv->
                                             u1StateTlvCount].u4LocalPwState =
                OSIX_HTONL (pPwIccpInfo->Data.u4LocalStatus);
            pPwRedSyncDataTlv->u1StateTlvCount++;
        }
        TMO_SLL_Delete (&(pPwRedData->DataList), (tTMO_SLL_NODE *) pPwIccpInfo);
        LdpL2VpnCleanEventPwData ((VOID *) pPwIccpInfo);
    }

    *pu2Len += u2Len;
    pPwRedSyncDataTlv->SyncDataTlvHdr[0].TlvHdr.u2TlvLen = OSIX_HTONS (u2Len);

    /* Fill Synchronization Data End Header */
    pPwRedSyncDataTlv->SyncDataTlvHdr[1].TlvHdr.u2TlvType =
        OSIX_HTONS (LDP_ICCP_PW_RED_SYNC_DATA_TLV);
    pPwRedSyncDataTlv->SyncDataTlvHdr[1].TlvHdr.u2TlvLen =
        OSIX_HTONS ((LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN +
                     LDP_ICCP_PW_RED_FLAG_FIELD_LEN));
    pPwRedSyncDataTlv->SyncDataTlvHdr[1].u2RqstId =
        OSIX_HTONS (pPwRedData->u2ResponseNum);
    pPwRedSyncDataTlv->SyncDataTlvHdr[1].u2Flags =
        OSIX_HTONS (LDP_ICCP_SYNC_DATA_END);

    *pu2Len = (UINT2) (*pu2Len + LDP_ICCP_PW_RED_RQST_DATA_TLV_HDR_LEN);

    LDP_DBG1 (LDP_MAIN_MISC, "EXIT 1. pu2Len(%u)\n", *pu2Len);
    return;
}

/*****************************************************************************/
/* Function Name : LdpFillIccpPwIdTlv                                        */
/* Description   : This function fills the PW ID TLV/Generalized PW ID TLV   */
/*                 to be added in PW Synchronization Data TLV                */
/* Input(s)      : pPwFec - Pointer to the PW FEC info                       */
/* Output(s)     : pu2Len - pointer to the optional parameter TLV length     */
/*                 pPwIdOrGenPwIdTlv - pointer to the filled PW ID TLV or    */
/*                                     Generalized PW ID TLV                 */
/*                 pu1Type - Type of the PW (PW ID FEC/GEN PW ID FEC)        */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpFillIccpPwIdTlv (tL2vpnPwFec * pPwFec, tPwIdOrGenPwIdTlv * pPwIdOrGenPwIdTlv,
                    UINT2 *pu2Len, UINT1 *pu1Type)
{
    UINT2               u2Len = 0;

    LDP_DBG (LDP_MAIN_MISC, "ENTRY\n");

    /* Fill PW ID TLV */
    if (pPwFec->u1Type == LDP_L2VPN_FEC_PWID_TYPE)
    {
        *pu1Type = LDP_ICCP_FILL_PW_ID_TLV;
        *pu2Len = (UINT2) (*pu2Len + LDP_ICCP_PW_ID_TLV_LEN);
        pPwIdOrGenPwIdTlv->PwIdTlv.TlvHdr.u2TlvType =
            OSIX_HTONS (LDP_ICCP_PW_ID_TLV);
        pPwIdOrGenPwIdTlv->PwIdTlv.TlvHdr.u2TlvLen =
            OSIX_HTONS ((LDP_LSR_ID_LEN + LDP_PWVC_FEC_ELEM_GROUP_ID_LEN +
                         LDP_PWVC_FEC_ELEM_PWVCID_LEN));
        MEMCPY (pPwIdOrGenPwIdTlv->PwIdTlv.PeerRouterId,
                pPwFec->u.Fec128.au1PeerId, ROUTER_ID_LENGTH);
        MEMCPY (&pPwIdOrGenPwIdTlv->PwIdTlv.u4PwGroupId,
                pPwFec->u.Fec128.au1GroupId, LDP_PWVC_FEC_ELEM_GROUP_ID_LEN);
        MEMCPY (&pPwIdOrGenPwIdTlv->PwIdTlv.u4PwId, pPwFec->u.Fec128.au1PwId,
                LDP_PWVC_FEC_ELEM_PWVCID_LEN);
    }
    /* Fill generalized PW ID TLV */
    else if (pPwFec->u1Type == LDP_L2VPN_FEC_GEN_TYPE)
    {
        *pu1Type = LDP_ICCP_FILL_GEN_PW_ID_TLV;
        *pu2Len = (UINT2) (*pu2Len + LDP_TLV_HDR_LEN);

        pPwIdOrGenPwIdTlv->GenPwIdTlv.TlvHdr.u2TlvType =
            OSIX_HTONS (LDP_ICCP_GEN_PW_ID_TLV);

        u2Len = (UINT2) (u2Len + LDP_GEN_PWVC_FEC_SUB_HDR_LEN);
        u2Len = (UINT2) (u2Len + L2VPN_PWVC_AGI1_LEN);
        /* Fill AGI info */
        pPwIdOrGenPwIdTlv->GenPwIdTlv.u1AgiType = 1;
        pPwIdOrGenPwIdTlv->GenPwIdTlv.u1AgiLen = L2VPN_PWVC_AGI1_LEN;
        MEMCPY (pPwIdOrGenPwIdTlv->GenPwIdTlv.au1Agi,
                pPwFec->u.Fec129.Agi.u.au1Agi1, L2VPN_PWVC_AGI1_LEN);

        /* Fill SAII info */
        if (pPwFec->u.Fec129.SAii.u1Type == LDP_L2VPN_GEN_FEC_AII_TYPE_1)
        {
            u2Len = (UINT2) (u2Len + LDP_GEN_PWVC_FEC_SUB_HDR_LEN);
            pPwIdOrGenPwIdTlv->GenPwIdTlv.u1SaiiType = 1;
            pPwIdOrGenPwIdTlv->GenPwIdTlv.u1SaiiLen = L2VPN_PWVC_AII1_LEN;
            MEMCPY (pPwIdOrGenPwIdTlv->GenPwIdTlv.au1Saii,
                    pPwFec->u.Fec129.SAii.u.au1Aii1, L2VPN_PWVC_AII1_LEN);
            u2Len = (UINT2) (u2Len + L2VPN_PWVC_AII1_LEN);
        }
        else
        {
            u2Len = (UINT2) (u2Len + LDP_GEN_PWVC_FEC_SUB_HDR_LEN);
            pPwIdOrGenPwIdTlv->GenPwIdTlv.u1SaiiType = 2;
            pPwIdOrGenPwIdTlv->GenPwIdTlv.u1SaiiLen = L2VPN_PWVC_AII2_LEN;
            MEMCPY (pPwIdOrGenPwIdTlv->GenPwIdTlv.au1Saii,
                    pPwFec->u.Fec129.SAii.u.au1Aii2, L2VPN_PWVC_AII2_LEN);
            u2Len = (UINT2) (u2Len + L2VPN_PWVC_AII2_LEN);
        }

        /* Fill TAII info */
        if (pPwFec->u.Fec129.TAii.u1Type == LDP_L2VPN_GEN_FEC_AII_TYPE_1)
        {
            u2Len = (UINT2) (u2Len + LDP_GEN_PWVC_FEC_SUB_HDR_LEN);
            pPwIdOrGenPwIdTlv->GenPwIdTlv.u1TaiiType = 1;
            pPwIdOrGenPwIdTlv->GenPwIdTlv.u1TaiiLen = L2VPN_PWVC_AII1_LEN;
            MEMCPY (pPwIdOrGenPwIdTlv->GenPwIdTlv.au1Taii,
                    pPwFec->u.Fec129.TAii.u.au1Aii1, L2VPN_PWVC_AII1_LEN);
            u2Len = (UINT2) (u2Len + L2VPN_PWVC_AII1_LEN);
        }
        else
        {
            u2Len = (UINT2) (u2Len + LDP_GEN_PWVC_FEC_SUB_HDR_LEN);
            pPwIdOrGenPwIdTlv->GenPwIdTlv.u1TaiiType = 2;
            pPwIdOrGenPwIdTlv->GenPwIdTlv.u1TaiiLen = L2VPN_PWVC_AII2_LEN;
            MEMCPY (pPwIdOrGenPwIdTlv->GenPwIdTlv.au1Taii,
                    pPwFec->u.Fec129.TAii.u.au1Aii2, L2VPN_PWVC_AII2_LEN);
            u2Len = (UINT2) (u2Len + L2VPN_PWVC_AII2_LEN);
        }

        *pu2Len += u2Len;
        pPwIdOrGenPwIdTlv->GenPwIdTlv.TlvHdr.u2TlvLen = OSIX_HTONS (u2Len);
    }

    LDP_DBG (LDP_MAIN_MISC, "EXIT 1\n");
    return;
}

/*****************************************************************************/
/* Function Name : LdpCopyGenPwIdTlvToBuf                                    */
/* Description   : This function copies the Gen PW ID TLV into CRU buffer    */
/* Input(s)      : pMsg - Pointer to the CRU buffer                          */
/*                 pGenPwIdTlv - pointer to the Gen PW ID TLV data           */
/*                 pu4OffSet - pointer to the CRU buffer offset              */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpCopyGenPwIdTlvToBuf (tCRU_BUF_CHAIN_HEADER * pMsg, tGenPwIdTlv * pGenPwIdTlv,
                        UINT4 *pu4OffSet)
{
    LDP_DBG (LDP_MAIN_MISC, "ENTRY\n");

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(pGenPwIdTlv->TlvHdr),
                                   *pu4OffSet, LDP_TLV_HDR_LEN) == CRU_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 1: "
                 "Error copying GEN PW ID TLV Header in PW-RED Synchronization Request "
                 "TLV to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    *pu4OffSet += LDP_TLV_HDR_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(pGenPwIdTlv->u1AgiType),
                                   *pu4OffSet, 1) == CRU_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 2: "
                 "Error copying Agi Type in GEN PW ID TLV to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    *pu4OffSet += 1;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(pGenPwIdTlv->u1AgiLen),
                                   *pu4OffSet, 1) == CRU_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 3: "
                 "Error copying Agi Length in GEN PW ID TLV to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    *pu4OffSet += 1;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(pGenPwIdTlv->au1Agi),
                                   *pu4OffSet, L2VPN_PWVC_AGI1_LEN) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 4: "
                 "Error copying Agi value in GEN PW ID TLV to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    *pu4OffSet += L2VPN_PWVC_AGI1_LEN;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(pGenPwIdTlv->u1SaiiType),
                                   *pu4OffSet, 1) == CRU_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 5: "
                 "Error copying Saii type in GEN PW ID TLV to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    *pu4OffSet += 1;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(pGenPwIdTlv->u1SaiiLen),
                                   *pu4OffSet, 1) == CRU_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 6: "
                 "Error copying Saii length in GEN PW ID TLV to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    *pu4OffSet += 1;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(pGenPwIdTlv->au1Saii),
                                   *pu4OffSet, pGenPwIdTlv->u1SaiiLen) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 7: "
                 "Error copying Saii value in GEN PW ID TLV to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    *pu4OffSet += pGenPwIdTlv->u1SaiiLen;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(pGenPwIdTlv->u1TaiiType),
                                   *pu4OffSet, 1) == CRU_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 8: "
                 "Error copying Taii type in GEN PW ID TLV to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    *pu4OffSet += 1;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(pGenPwIdTlv->u1TaiiLen),
                                   *pu4OffSet, 1) == CRU_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 9: "
                 "Error copying Taii length in GEN PW ID TLV to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    *pu4OffSet += 1;

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &(pGenPwIdTlv->au1Taii),
                                   *pu4OffSet, pGenPwIdTlv->u1TaiiLen) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 10: "
                 "Error copying Taii value in GEN PW ID TLV to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    *pu4OffSet += pGenPwIdTlv->u1TaiiLen;

    LDP_DBG (LDP_MAIN_MISC, "EXIT 11\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleIccpMsg                                          */
/* Description   : This routine is called to handle the ICCP message         */
/* Input(s)      : pMsg - Points to the RG message received                  */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpHandleIccpMsg (tLdpSession * pSession, UINT1 *pMsg)
{
    UINT2               u2MsgType = 0;
    UINT1               u1RetVal = LDP_FAILURE;

    LDP_GET_2_BYTES ((pMsg), u2MsgType);

    LDP_DBG1 (LDP_MAIN_MISC, "ENTRY. u2MsgType(%#X)\n", u2MsgType);

    switch (u2MsgType)
    {
        case LDP_ICCP_RG_DATA_MSG:
            u1RetVal = LdpHandleIccpRgDataMsg (pSession, pMsg);
            break;
        case LDP_ICCP_RG_CONNECT_MSG:
        case LDP_ICCP_RG_DISCONNECT_MSG:
        case LDP_ICCP_RG_NOTIFICATION_MSG:
            LDP_DBG (LDP_MAIN_MISC,
                     "ICCP : Unsupported ICCP message received\n");
            break;
        default:
            LDP_DBG (LDP_MAIN_MISC, "ICCP : Invalid message received\n");
            break;
    }

    LDP_DBG1 (LDP_MAIN_MISC, "EXIT 1. u1RetVal(%u)\n", u1RetVal);
    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : LdpHandleIccpRgDataMsg                                    */
/* Description   : This routine is called to handle the ICCP RG Application  */
/*                 Data message                                              */
/* Input(s)      : pMsg - Points to the RG Data message received             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpHandleIccpRgDataMsg (tLdpSession * pSession, UINT1 *pMsg)
{
    UINT4               u4RgId = 0;
    UINT4               u4MessageId = 0;
    UINT2               u2OffSet = 0;
    UINT2               u2MsgLen = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLen = 0;
    tL2VpnLdpPwVcEvtInfo LdpPwEvtInfo;

    LDP_DBG (LDP_MAIN_MISC, "ENTRY\n");

    MEMSET (&LdpPwEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

    /* Length of RG Application Data Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2MsgLen);
    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MessageId);
    u2TlvType = MSG_GET_FIRST_TLVTYPE (pMsg);
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN +
                      LDP_TLV_TYPE_LEN), u2TlvLen);

    if (u2TlvType != LDP_ICCP_RG_ID_TLV)
    {
        LDP_DBG (LDP_MAIN_MISC,
                 "[ICCP] : RG ID TLV missing in the RG App Data Msg.\n");
        return LDP_FAILURE;
    }
    if (u2TlvLen != LDP_ICCP_RG_ID_LEN)
    {
        LDP_DBG (LDP_MAIN_MISC,
                 "[ICCP] : RG ID TLV Len is invalid in RG App Data Msg.\n");
        return LDP_FAILURE;
    }

    /* Get the RG ID */
    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN +
                      LDP_TLV_HDR_LEN), u4RgId)
        u2OffSet = (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN + LDP_ICCP_RG_ID_TLV_LEN);

    LdpPwEvtInfo.u4EvtType = L2VPN_LDP_ICCP_RG_EVT;
    LdpPwEvtInfo.unEvtInfo.IccpInfo.u2MsgType = L2VPN_LDP_ICCP_RG_DATA_MSG;
    LdpPwEvtInfo.unEvtInfo.IccpInfo.u4MessageId = u4MessageId;
    LdpPwEvtInfo.unEvtInfo.IccpInfo.u4GroupId = u4RgId;

    MEMCPY (&LdpPwEvtInfo.unEvtInfo.IccpInfo.u4LocalLdpEntityId,
            pSession->pLdpPeer->pLdpEntity->LdpId, sizeof (UINT4));
    LdpPwEvtInfo.unEvtInfo.IccpInfo.u4LocalLdpEntityIndex =
        pSession->pLdpPeer->pLdpEntity->u4EntityIndex;

    MEMCPY (&LdpPwEvtInfo.unEvtInfo.IccpInfo.u4PeerLdpId,
            pSession->pLdpPeer->PeerLdpId, sizeof (UINT4));
    LdpPwEvtInfo.unEvtInfo.IccpInfo.PeerAddr = pSession->pLdpPeer->NetAddr.Addr;

    TMO_SLL_Init (&(LdpPwEvtInfo.unEvtInfo.IccpInfo.u.PwData.RequestList));
    TMO_SLL_Init (&(LdpPwEvtInfo.unEvtInfo.IccpInfo.u.PwData.DataList));

    /* Parse the received buffer and process it till the message length received */
    while ((u2OffSet - LDP_MSG_HDR_LEN) < u2MsgLen)
    {
        /* Initializing the TLV type and length */
        u2TlvType = 0;
        u2TlvLen = 0;

        /* Get the Next TLV type */
        LDP_GET_2_BYTES ((pMsg + u2OffSet), u2TlvType);
        u2OffSet = (UINT2) (u2OffSet + LDP_TLV_TYPE_LEN);

        /* The main TLV must be Config/state/request/data TLV */
        switch (u2TlvType)
        {
            case LDP_ICCP_PW_RED_CONFIG_TLV:
                if (LdpIccpExtractPwConfigTlv (pMsg,
                                               &(LdpPwEvtInfo.unEvtInfo.
                                                 IccpInfo.u.PwData),
                                               &u2OffSet, &u2TlvLen) ==
                    LDP_FAILURE)
                {
                    LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 4\n");
                    return LDP_FAILURE;
                }
                break;

            case LDP_ICCP_PW_RED_STATE_TLV:
                if (LdpIccpExtractPwStateTlv (pMsg,
                                              &(LdpPwEvtInfo.unEvtInfo.
                                                IccpInfo.u.PwData),
                                              &u2OffSet, &u2TlvLen) ==
                    LDP_FAILURE)
                {
                    LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 5\n");
                    return LDP_FAILURE;
                }
                break;

            case LDP_ICCP_PW_RED_SYNC_RQST_TLV:
                if (LdpProcessPwRedSyncRqstTlv (pMsg, &u2OffSet,
                                                &(LdpPwEvtInfo.unEvtInfo.
                                                  IccpInfo.u.PwData)) !=
                    LDP_SUCCESS)
                {
                    LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 6\n");
                    return LDP_FAILURE;
                }
                break;

            case LDP_ICCP_PW_RED_SYNC_DATA_TLV:
                if (LdpProcessPwRedSyncDataTlv (pMsg, &u2OffSet,
                                                &(LdpPwEvtInfo.unEvtInfo.
                                                  IccpInfo.u.PwData)) !=
                    LDP_SUCCESS)
                {
                    LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 7\n");
                    return LDP_FAILURE;
                }
                break;

            default:
                LDP_DBG1 (LDP_MAIN_MISC, "ICCP: INTMD-EXIT 8: "
                          "Invalid Main TLV type(%#X) in RG App Data Msg.\n",
                          u2TlvType);

                return LDP_FAILURE;
        }
    }                            /* End of while loop for parsing the RG Data message */

    LdpL2VpnPostEvent (&LdpPwEvtInfo);

    LDP_DBG (LDP_MAIN_MISC, "ICCP : Succesfully Passed the ICCP Data "
             "to L2VPN module for processing\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessPwRedSyncRqstTlv                                */
/* Description   : This routine is to process the received Synchronization   */
/*                 request message and fill in the infos to notify L2VPN     */
/* Input(s)      : pMsg - Points to the ICCPmessage received                 */
/*                 pu2OffSet - pointer to the offset value used to retrieve  */
/*                             data from the buffer                          */
/* Output(s)     : pPwRedData - pointer to the updated PW-RED information    */
/*                                   which needs to be sent to L2VPN for     */
/*                                   processing                              */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpProcessPwRedSyncRqstTlv (UINT1 *pMsg, UINT2 *pu2OffSet,
                            tPwRedDataEvtArgs * pPwRedData)
{
    tPwRedDataEvtPwFec *pPwRedRqstPwFec = NULL;
    UINT2               u2RqstTlvLen = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2RqstType = 0;
    UINT2               u2SubTlvsLen = 0;
    UINT2               u2TmpLen = 0;
    BOOL1               bIsValid = FALSE;

    LDP_DBG (LDP_MAIN_MISC, "ENTRY\n");

    /* Fetch the Synchronization request TLV length and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2RqstTlvLen);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);

    /* Fetch the request number and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), pPwRedData->u2RequestNum);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN);

    if (pPwRedData->u2RequestNum == 0)
    {
        LDP_DBG (LDP_MAIN_MISC,
                 "[ICCP] : Request number is ZERO in PW-RED Synchronization Request TLV\n");
        return LDP_FAILURE;
    }
    /* Fetch the request type and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2RqstType);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_SYNC_RQST_TYPE_FIELD_LEN);

    if (u2RqstType & L2VPNRED_ICCP_REQ_PW_CONFIG)
    {
        pPwRedData->u1RequestType |= L2VPNRED_ICCP_REQ_CONFIG;
        bIsValid = TRUE;
    }
    if (u2RqstType & L2VPNRED_ICCP_REQ_PW_STATE)
    {
        pPwRedData->u1RequestType |= L2VPNRED_ICCP_REQ_STATE;
        bIsValid = TRUE;
    }

    if (bIsValid == FALSE)
    {
        LDP_DBG (LDP_MAIN_MISC,
                 "[ICCP] : 'C' bit and 'S' bit both are clear in PW-RED Synchronization "
                 "Request TLV\n");
        return LDP_FAILURE;
    }
    if (u2RqstType & L2VPNRED_ICCP_REQ_PW_TYPE_MASK)
    {
        /* If the request is for all PWs, then no sub-TLVs will be
         * present in the Synchronization request TLV.
         * So return and process the next main TLV*/
        pPwRedData->u1RequestType |= L2VPNRED_ICCP_REQ_ALL;
        LDP_DBG (LDP_MAIN_MISC, "EXIT 1\n");
        return LDP_SUCCESS;
    }

    /* If the request is for PW informations with specified service name,
     * fill the service name and pass it to L2VPn module for processing */
    if (u2RqstType & L2VPNRED_ICCP_REQ_PW_SERVICE_NAME)
    {
        pPwRedData->u1RequestType |= L2VPNRED_ICCP_REQ_SERVICE_NAME;

        /* Parse the buffer and fetch the service name TLVs,
         * till the Synchronization Request TLV length*/
        while ((u2SubTlvsLen + LDP_ICCP_SYNC_RQST_TYPE_FIELD_LEN +
                LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN) < u2RqstTlvLen)
        {
            u2TlvType = 0;
            /* Fetch the sub-TLV type and increment the offset */
            LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2TlvType);
            u2SubTlvsLen = (UINT2) (u2SubTlvsLen + LDP_TLV_TYPE_LEN);
            *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);

            /* The Sub-TLV must be service name TLV, since the request type
             * is for PW with specified service name*/
            if (u2TlvType != LDP_ICCP_SERVICE_NAME_TLV)
            {
                LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 2\n");
                return LDP_FAILURE;
            }

            pPwRedRqstPwFec = L2VpnLdpGetEventPwFec ();
            if (pPwRedRqstPwFec == NULL)
            {
                LDP_DBG (LDP_MAIN_MISC, "PRCS: INTMD-EXIT 3: "
                         "Mem Alloc from Msg MemPool Failed for tPwRedDataEvtPwFec\n");

                return LDP_FAILURE;
            }
            TMO_SLL_Init_Node (&(pPwRedRqstPwFec->ListNode));

            /* Fetch the Service name TLV length and increment the offset */
            LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2TmpLen);
            u2SubTlvsLen = (UINT2) (u2SubTlvsLen + LDP_TLV_TYPE_LEN);
            *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);

            /* Service name TLV length must be less than or equal to 80 */
            if (u2TmpLen > L2VPN_PWVC_ICCP_NAME_MAX_LEN)
            {
                LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 4\n");
                L2VpnLdpCleanEventPwFec (pPwRedRqstPwFec);
                return LDP_FAILURE;
            }
            else
            {
                MEMCPY (pPwRedRqstPwFec->u.Name, (pMsg + *pu2OffSet), u2TmpLen);
                u2SubTlvsLen += u2TmpLen;
                *pu2OffSet += u2TmpLen;
            }
            /* Add the SLL node to the request list */
            TMO_SLL_Add (&(pPwRedData->RequestList),
                         &(pPwRedRqstPwFec->ListNode));

        }                        /* End of while loop for parsing "Service name TLVs" */

    }                            /* End of if condition for request type "Specific Service Name" */

    /* If the request type is neither specified service name nor all PW informations,
     * then it must be for specified PWs.
     * So fill the PW ID information and pass it to L2VPn module for processing */
    else
    {
        /* Parse the buffer and fetch the PW ID/Gen PW ID TLVs,
         * till the Synchronization Request TLV length*/
        while ((u2SubTlvsLen + LDP_ICCP_SYNC_RQST_TYPE_FIELD_LEN +
                LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN) < u2RqstTlvLen)
        {
            u2TlvType = 0;

            /* Allocate memory and Init the SLL node to store the service name info */
            pPwRedRqstPwFec = L2VpnLdpGetEventPwFec ();
            if (pPwRedRqstPwFec == NULL)
            {
                LDP_DBG (LDP_MAIN_MISC, "PRCS: INTMD-EXIT 5: "
                         "Mem Alloc from Msg MemPool Failed for tPwRedDataEvtPwFec\n");
                return LDP_FAILURE;
            }
            TMO_SLL_Init_Node (&(pPwRedRqstPwFec->ListNode));

            /* Fetch the sub-TLV type and increment the offset */
            LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2TlvType);
            u2SubTlvsLen = (UINT2) (u2SubTlvsLen + LDP_TLV_TYPE_LEN);
            *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);

            if (LdpIccpExtractPwIdTlvFrmBuf
                (u2TlvType, pMsg, &(pPwRedRqstPwFec->u.Fec), pu2OffSet,
                 &u2SubTlvsLen) == LDP_FAILURE)
            {
                LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 6\n");
                L2VpnLdpCleanEventPwFec (pPwRedRqstPwFec);
                return LDP_FAILURE;
            }
            /* Add the SLL node to the request list */
            TMO_SLL_Add (&(pPwRedData->RequestList),
                         &(pPwRedRqstPwFec->ListNode));

        }                        /* End of while loop for parsing "PW ID/Gen PW ID TLVs" */

    }                            /* End of else if condition for request type "Specific PW" */

    LDP_DBG (LDP_MAIN_MISC, "EXIT 7\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessPwRedSyncDataTlv                                */
/* Description   : This routine is to process the received Synchronization   */
/*                 Data message and fill in the infos to notify L2VPN        */
/* Input(s)      : pMsg - Points to the ICCP message received                */
/*                 pu2OffSet - pointer to the offset value used to retrieve  */
/*                             data from the buffer                          */
/* Output(s)     : pPwRedData - pointer to the updated PW-RED information    */
/*                              which needs to be sent to L2VPN for          */
/*                              processing                                   */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpProcessPwRedSyncDataTlv (UINT1 *pMsg, UINT2 *pu2OffSet,
                            tPwRedDataEvtArgs * pPwRedData)
{
    UINT2               u2DataTlvLen = 0;
    UINT2               u2SubTlvsLen = 0;
    UINT2               u2RqstNum = 0;
    UINT2               u2TempLen = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2Flags = 0;

    LDP_DBG (LDP_MAIN_MISC, "ENTRY\n");

    /* Fetch the Synchronization Data TLV length and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2DataTlvLen);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);

    /* Fetch the Request Number and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2RqstNum);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN);

    pPwRedData->u2ResponseNum = u2RqstNum;

    /* Fetch the Synchronization Data Flags and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2Flags);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_PW_RED_FLAG_FIELD_LEN);

    /* The flag value must represent the Start of the DATA (0x00) */
    if (u2Flags != 0)
    {
        LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 1\n");
        return LDP_FAILURE;
    }

    /* Parse the buffer and fetch the sub TLVs,
     * till the Synchronization DATA TLV length*/
    while ((u2SubTlvsLen + LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN +
            LDP_ICCP_PW_RED_FLAG_FIELD_LEN) < u2DataTlvLen)
    {
        u2TlvType = 0;

        /* Fetch the next TLV type and increment the offset */
        LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2TlvType);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);
        u2SubTlvsLen = (UINT2) (u2SubTlvsLen + LDP_TLV_TYPE_LEN);

        if (u2TlvType == LDP_ICCP_PW_RED_CONFIG_TLV)
        {
            if (LdpIccpExtractPwConfigTlv
                (pMsg, pPwRedData, pu2OffSet, &u2SubTlvsLen) == LDP_FAILURE)
            {
                LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 2\n");
                return LDP_FAILURE;
            }
        }                        /* End of if condition for PW Config TLV */
        else if (u2TlvType == LDP_ICCP_PW_RED_STATE_TLV)
        {
            if (LdpIccpExtractPwStateTlv
                (pMsg, pPwRedData, pu2OffSet, &u2SubTlvsLen) == LDP_FAILURE)
            {
                LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 3\n");
                return LDP_FAILURE;
            }
        }                        /*End of else if condition for PW State TLV */
        else
        {
            /* The sub-TLVs with in Synchronization Data TLV must be
             * either PW Config TLV or PW State TLV. Else return Failure*/
            LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 4\n");
            return LDP_FAILURE;
        }
    }                            /* End of while */

    /* Initialize the local variables */
    u2TlvType = 0;
    u2TempLen = 0;
    u2Flags = 0;
    u2RqstNum = 0;

    /* Fetch the next TLV type and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2TlvType);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);

    /* Synchronization Data should be delimited by a set of Synchronization Data TLvs
     * So the next TLV must be Synchronization Data TLV */
    if (u2TlvType != LDP_ICCP_PW_RED_SYNC_DATA_TLV)
    {
        LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 5\n");
        return LDP_FAILURE;
    }

    /* Fetch the Data TLV length and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2TempLen);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);

    /* The length of the Synchronization Data TLV used for delimit the data must be 4 */
    if (u2TempLen !=
        (LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN + LDP_ICCP_PW_RED_FLAG_FIELD_LEN))
    {
        LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 6\n");
        return LDP_FAILURE;
    }

    /* Fetch the request number and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2RqstNum);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_SYNC_RQST_NUM_FIELD_LEN);

    /* The Request number at the bottom Data TLV should match with the Request number at
     * the top Data TLV*/
    if (u2RqstNum != pPwRedData->u2ResponseNum)
    {
        LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 7\n");
        return LDP_FAILURE;
    }

    /* Fetch the Synchronization Data Flags and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2Flags);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_PW_RED_FLAG_FIELD_LEN);

    /* The flag value must represent the End of the DATA (0x01) */
    if (u2Flags != LDP_ICCP_SYNC_DATA_END)
    {
        LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 8\n");
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_MAIN_MISC, "EXIT 9\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpIccpExtractPwIdTlvFrmBuf                               */
/* Description   : This routine is to extract the PW ID TLV/Gen PW ID TLV    */
/*                 information from the buffer                               */
/* Input(s)      : u2TlvType - Tlv type                                      */
/*                 pMsg - Pointer to the buffer message                      */
/*                 pu2OffSet - pointer to the offset value used to retrieve  */
/*                             data from the buffer                          */
/*                 pu2SubTlvsLen - pointer to the sub tlv length             */
/* Output(s)     : pPwFec - pointer to the updated PW FEC information        */
/*                          which needs to be sent to L2VPN for processing   */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpIccpExtractPwIdTlvFrmBuf (UINT2 u2TlvType, UINT1 *pMsg, tL2vpnPwFec * pPwFec,
                             UINT2 *pu2OffSet, UINT2 *pu2SubTlvsLen)
{
    UINT2               u2TmpLen = 0;

    LDP_DBG (LDP_MAIN_MISC, "ENTRY\n");

    /* The Sub-TLV must be either PW ID TLV or GEneralized PW ID TLV */
    if (u2TlvType == LDP_ICCP_PW_ID_TLV)
    {
        /* Fetch the PW ID TLV length and increment the offset */
        LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2TmpLen);
        *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_TLV_TYPE_LEN);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);

        /* Length of PW ID TLV should be 12 always */
        if (u2TmpLen != (LDP_ICCP_PW_ID_TLV_LEN - LDP_TLV_HDR_LEN))
        {
            LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 1\n");
            return LDP_FAILURE;
        }

        pPwFec->u1Type = LDP_FEC_PWVC_TYPE;

        /* Fetch the Peer ID and increment the offset */
        MEMCPY (pPwFec->u.Fec128.au1PeerId,
                (pMsg + *pu2OffSet), LDP_LSR_ID_LEN);
        *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_LSR_ID_LEN);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_LSR_ID_LEN);

        /* Fetch the Group ID and increment the offset */
        MEMCPY (pPwFec->u.Fec128.au1GroupId,
                (pMsg + *pu2OffSet), LDP_PWVC_FEC_ELEM_GROUP_ID_LEN);
        *pu2SubTlvsLen =
            (UINT2) (*pu2SubTlvsLen + LDP_PWVC_FEC_ELEM_GROUP_ID_LEN);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_PWVC_FEC_ELEM_GROUP_ID_LEN);

        /* Fetch the PW ID and increment the offset */
        MEMCPY (pPwFec->u.Fec128.au1GroupId,
                (pMsg + *pu2OffSet), LDP_PWVC_FEC_ELEM_PWVCID_LEN);
        *pu2SubTlvsLen =
            (UINT2) (*pu2SubTlvsLen + LDP_PWVC_FEC_ELEM_PWVCID_LEN);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_PWVC_FEC_ELEM_PWVCID_LEN);

    }                            /* End of if condition for PW ID TLV */

    else if (u2TlvType == LDP_ICCP_GEN_PW_ID_TLV)
    {
        pPwFec->u1Type = LDP_FEC_GEN_PWVC_TYPE;

        /* Fetch the Gen PW ID TLV length and increment the offset */
        LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2TmpLen);
        *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_TLV_TYPE_LEN);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);

        /* Fetch the AGI Type and increment the offset */
        MEMCPY (&(pPwFec->u.Fec129.Agi.u1Type), (pMsg + *pu2OffSet), LDP_ONE);
        *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ONE);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ONE);

        /* Fetch the AGI Length and increment the offset */
        MEMCPY (&u2TmpLen, (pMsg + *pu2OffSet), LDP_ONE);
        *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ONE);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ONE);

        /* Fetch the AGI value and increment the offset */
        MEMCPY (pPwFec->u.Fec129.Agi.u.au1Agi1, (pMsg + *pu2OffSet), u2TmpLen);
        *pu2SubTlvsLen += u2TmpLen;
        *pu2OffSet += u2TmpLen;

        /* Fetch the SAII Type and increment the offset */
        MEMCPY (&(pPwFec->u.Fec129.SAii.u1Type), (pMsg + *pu2OffSet), LDP_ONE);
        *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ONE);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ONE);

        /* Fetch the SAII Length and increment the offset */
        MEMCPY (&u2TmpLen, (pMsg + *pu2OffSet), LDP_ONE);
        *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ONE);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ONE);

        /* Fetch the SAII value and increment the offset */
        if (pPwFec->u.Fec129.SAii.u1Type == LDP_L2VPN_GEN_FEC_AII_TYPE_1)
        {
            MEMCPY (pPwFec->u.Fec129.SAii.u.au1Aii1,
                    (pMsg + *pu2OffSet), u2TmpLen);
        }
        else if (pPwFec->u.Fec129.SAii.u1Type == LDP_L2VPN_GEN_FEC_AII_TYPE_2)
        {
            MEMCPY (pPwFec->u.Fec129.SAii.u.au1Aii2,
                    (pMsg + *pu2OffSet), u2TmpLen);
        }
        else
        {
            LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 2\n");
            return LDP_FAILURE;
        }
        *pu2SubTlvsLen += u2TmpLen;
        *pu2OffSet += u2TmpLen;

        /* Fetch the TAII Type and increment the offset */
        MEMCPY (&(pPwFec->u.Fec129.TAii.u1Type), (pMsg + *pu2OffSet), LDP_ONE);
        *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ONE);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ONE);

        /* Fetch the SAII Length and increment the offset */
        MEMCPY (&u2TmpLen, (pMsg + *pu2OffSet), LDP_ONE);
        *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ONE);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ONE);

        /* Fetch the TAII value and increment the offset */
        if (pPwFec->u.Fec129.TAii.u1Type == LDP_L2VPN_GEN_FEC_AII_TYPE_1)
        {
            MEMCPY (pPwFec->u.Fec129.TAii.u.au1Aii1,
                    (pMsg + *pu2OffSet), u2TmpLen);
        }
        else if (pPwFec->u.Fec129.TAii.u1Type == LDP_L2VPN_GEN_FEC_AII_TYPE_2)
        {
            MEMCPY (pPwFec->u.Fec129.TAii.u.au1Aii2,
                    (pMsg + *pu2OffSet), u2TmpLen);
        }
        else
        {
            LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 3\n");
            return LDP_FAILURE;
        }
        *pu2SubTlvsLen += u2TmpLen;
        *pu2OffSet += u2TmpLen;

    }                            /*End of else if condition for Gen PW ID TLV */
    else
    {
        /* Sub-TLV is neither PW ID TLV nor GEN PW ID TLV.
         * So return FAILURE */
        LDP_DBG (LDP_MAIN_MISC, "[ICCP] : FEC 128/129 TLV is missing in "
                 "the PW-RED Config TLV\n");
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_MAIN_MISC, "EXIT 5\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpIccpExtractPwConfigTlv                                 */
/* Description   : This routine is to extract the PW Config TLV from the     */
/*                 buffer                                                    */
/* Input(s)      : pMsg - Pointer to the buffer message                      */
/*                 pDataList - pointer to the single linked list             */
/*                 pu2OffSet - pointer to the offset value used to retrieve  */
/*                             data from the buffer                          */
/*                 pu2SubTlvsLen - pointer to the sub tlv length             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpIccpExtractPwConfigTlv (UINT1 *pMsg, tPwRedDataEvtArgs * pPwData,
                           UINT2 *pu2OffSet, UINT2 *pu2SubTlvsLen)
{
    tPwRedDataEvtPwData *pPwRedSyncData = NULL;
    tL2vpnIccpRoId      RoId;
    UINT2               u2Len = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2Flags = 0;
    BOOL1               b1NewEntry = FALSE;

    LDP_DBG (LDP_MAIN_MISC, "ENTRY\n");

    /* Fetch the Config TLV length and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2Len);
    *pu2OffSet = (UINT2) (LDP_TLV_TYPE_LEN + *pu2OffSet);
    *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_TLV_TYPE_LEN);

    /* Fetch the ROID and increment the offset */
    MEMCPY (&(RoId), (pMsg + *pu2OffSet), LDP_ICCP_ROID_LEN);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_ROID_LEN);
    *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ICCP_ROID_LEN);

    /* Get earlier entry with same ROID */
    if (LdpIccpGetPwDataByRoId (&pPwRedSyncData, pPwData, &RoId) == LDP_FAILURE)
    {
        /* Allocate memory and Init the SLL node to store the service name info */
        pPwRedSyncData = L2VpnLdpGetEventPwData ();
        if (pPwRedSyncData == NULL)
        {
            LDP_DBG (LDP_MAIN_MISC, "PRCS: INTMD-EXIT 1: "
                     "Mem Alloc from Msg MemPool Failed for tPwRedDataEvtPwData\n");
            return LDP_FAILURE;
        }
        b1NewEntry = TRUE;

        TMO_SLL_Init_Node (&(pPwRedSyncData->ListNode));

        pPwRedSyncData->Data.RoId = RoId;
    }

    pPwRedSyncData->u1Type |= L2VPNRED_ICCP_DATA_CONFIG;

    /* Fetch the PW priority and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), pPwRedSyncData->Data.u2Priority);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_PW_PRIORITY_FIELD_LEN);
    *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ICCP_PW_PRIORITY_FIELD_LEN);

    /* Fetch the flags and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2Flags);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_PW_RED_FLAG_FIELD_LEN);
    *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ICCP_PW_RED_FLAG_FIELD_LEN);

    if (u2Flags == LDP_ICCP_PW_SYNCHRONIZED)
    {
        pPwRedSyncData->u1Type |= L2VPNRED_ICCP_DATA_SYNCHRONIZED;
    }
    else if (u2Flags == LDP_ICCP_PW_PURGED)
    {
        pPwRedSyncData->u1Type |= L2VPNRED_ICCP_DATA_PURGED;
    }

    /* Fetch the next sub-TLV type and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2TlvType);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);
    *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_TLV_TYPE_LEN);

    if (u2TlvType == LDP_ICCP_SERVICE_NAME_TLV)
    {
        /* Fetch the Service name TLV length and increment the offset */
        LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2Len);
        *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_TLV_TYPE_LEN);
        *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);

        /* Service name TLV length must be less than or equal to 80 */
        if (u2Len > L2VPN_PWVC_ICCP_NAME_MAX_LEN)
        {
            LDP_DBG1 (LDP_MAIN_MISC, "INTMD-EXIT 3. u2Len(%u)\n", u2Len);
            L2VpnLdpCleanEventPwData (pPwRedSyncData);
            return LDP_FAILURE;
        }
        else
        {
            MEMCPY (pPwRedSyncData->Data.Name, (pMsg + *pu2OffSet), u2Len);
            *pu2SubTlvsLen += u2Len;
            *pu2OffSet += u2Len;
        }
    }
    else
    {
        LDP_DBG1 (LDP_MAIN_MISC, "INTMD-EXIT 4. u2TlvType(%#X)\n", u2TlvType);
        L2VpnLdpCleanEventPwData (pPwRedSyncData);
        return LDP_FAILURE;
    }

    u2TlvType = LDP_ZERO;
    /* Fetch the next sub-TLV type and increment the offset */
    LDP_GET_2_BYTES ((pMsg + *pu2OffSet), u2TlvType);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);
    *pu2SubTlvsLen = (UINT2) (LDP_TLV_TYPE_LEN + *pu2SubTlvsLen);

    if (LdpIccpExtractPwIdTlvFrmBuf
        (u2TlvType, pMsg, &(pPwRedSyncData->Data.Fec), pu2OffSet,
         &*pu2SubTlvsLen) == LDP_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 5\n");
        L2VpnLdpCleanEventPwData (pPwRedSyncData);
        return LDP_FAILURE;
    }

    if (b1NewEntry)
    {
        TMO_SLL_Add (&(pPwData->DataList), &(pPwRedSyncData->ListNode));
    }

    LDP_DBG (LDP_MAIN_MISC, "EXIT 6\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpIccpExtractPwStateTlv                                  */
/* Description   : This routine is to extract the PW Config TLV from the     */
/*                 buffer                                                    */
/* Input(s)      : pMsg - Pointer to the buffer message                      */
/*                 pDataList - pointer to the single linked list             */
/*                 pu2OffSet - pointer to the offset value used to retrieve  */
/*                             data from the buffer                          */
/*                 pu2SubTlvsLen - pointer to the sub tlv length             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpIccpExtractPwStateTlv (UINT1 *pMsg, tPwRedDataEvtArgs * pPwData,
                          UINT2 *pu2OffSet, UINT2 *pu2SubTlvsLen)
{
    tPwRedDataEvtPwData *pPwRedSyncData = NULL;
    tL2vpnIccpRoId      RoId;
    BOOL1               b1NewEntry = FALSE;

    LDP_DBG (LDP_MAIN_MISC, "ENTRY\n");

    /* Fetch the State TLV length and increment the offset */
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_TLV_TYPE_LEN);
    *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_TLV_TYPE_LEN);

    /* Fetch the ROID and increment the offset */
    MEMCPY (&(RoId), (pMsg + *pu2OffSet), LDP_ICCP_ROID_LEN);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_ROID_LEN);
    *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ICCP_ROID_LEN);

    /* Get earlier entry with same ROID */
    if (LdpIccpGetPwDataByRoId (&pPwRedSyncData, pPwData, &RoId) == LDP_FAILURE)
    {
        /* Allocate memory and Init the SLL node to store the service name info */
        pPwRedSyncData = L2VpnLdpGetEventPwData ();
        if (pPwRedSyncData == NULL)
        {
            LDP_DBG (LDP_MAIN_MISC, "PRCS: INTMD-EXIT 1: "
                     "Mem Alloc from Msg MemPool Failed for tPwRedDataEvtPwData\n");
            return LDP_FAILURE;
        }
        b1NewEntry = TRUE;

        TMO_SLL_Init_Node (&(pPwRedSyncData->ListNode));

        pPwRedSyncData->Data.RoId = RoId;
    }

    pPwRedSyncData->u1Type |= L2VPNRED_ICCP_DATA_STATE;

    /* Fetch the Local PW state and increment the offset */
    LDP_GET_4_BYTES ((pMsg + *pu2OffSet), pPwRedSyncData->Data.u4LocalStatus);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_PW_STATE_FIELD_LEN);
    *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ICCP_PW_STATE_FIELD_LEN);

    /* Fetch the Remote PW state and increment the offset */
    LDP_GET_4_BYTES ((pMsg + *pu2OffSet), pPwRedSyncData->Data.u4RemoteStatus);
    *pu2OffSet = (UINT2) (*pu2OffSet + LDP_ICCP_PW_STATE_FIELD_LEN);
    *pu2SubTlvsLen = (UINT2) (*pu2SubTlvsLen + LDP_ICCP_PW_STATE_FIELD_LEN);

    if (b1NewEntry)
    {
        TMO_SLL_Add (&(pPwData->DataList), &(pPwRedSyncData->ListNode));
    }

    LDP_DBG (LDP_MAIN_MISC, "EXIT 2\n");
    return LDP_SUCCESS;
}

UINT1
LdpIccpGetPwDataByRoId (tPwRedDataEvtPwData ** ppPwData,
                        tPwRedDataEvtArgs * pPwRgData, tL2vpnIccpRoId * pRoId)
{
    VOID               *pSllNode = NULL;
    tPwRedDataEvtPwData *pPwData = NULL;

    LDP_DBG8 (LDP_MAIN_MISC,
              "ENTRY. pRoId( %02X%02X%02X%02X _ %02X%02X%02X%02X )\n",
              pRoId->au1NodeId[0], pRoId->au1NodeId[1],
              pRoId->au1NodeId[2], pRoId->au1NodeId[3],
              pRoId->au1PwIndex[0], pRoId->au1PwIndex[1],
              pRoId->au1PwIndex[2], pRoId->au1PwIndex[3]);

    if (ppPwData == NULL)
    {
        LDP_DBG1 (LDP_MAIN_MISC, "INTMD-EXIT 1. ppPwData(%p)\n", ppPwData);
        return LDP_FAILURE;
    }

    TMO_SLL_Scan (&pPwRgData->DataList, pSllNode, tPwRedDataEvtPwData *)
    {
        pPwData = CONTAINER_OF (pSllNode, tPwRedDataEvtPwData, ListNode);

        if (MEMCMP (&pPwData->Data.RoId, pRoId, sizeof (*pRoId)) == 0)
        {
            *ppPwData = pPwData;
            break;
        }
    }

    LDP_DBG1 (LDP_MAIN_MISC, "EXIT 2. ppPwData(%p)\n", *ppPwData);
    return *ppPwData == NULL ? LDP_FAILURE : LDP_SUCCESS;
}

