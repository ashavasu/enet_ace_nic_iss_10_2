/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: ldppwsnd.c,v 1.22 2014/11/08 11:40:55 siva Exp $
 *
 * Description: This file contains the code for sending PW msgs to 
 *              L2VPN Module.
 *******************************************************************/
#include "ldpincs.h"

/*****************************************************************************/
/* Function Name : LdpSendSsnNotification                                    */
/* Description   : This function send notifications to l2vpn module          */
/* Input(s)      : pLdpSession - Pointer to the session in which the message */
/*                               is received                                 */
/*                 u1NotifType - type of Notification (Session Up/Down)      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
/* ldppwsnd.c */
VOID
LdpSendSsnNotification (tLdpSession * pSessionEntry, UINT4 u4EvtType)
{

    tLdpEntity         *pLdpEntity = SSN_GET_ENTITY (pSessionEntry);
    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;
    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

    if (SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_UNSOLICIT)
    {
        LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = u4EvtType;
        LDP_L2VPN_EVT_SSN_RECOVERY_TIME (&PwVcEvtInfo) =
            pSessionEntry->pLdpPeer->u2GrRecoveryTime;

	LdpCopyAAddr(&(LDP_L2VPN_EVT_SSN_PEER_ADDR (&PwVcEvtInfo)),
			&(LDP_PEER_NET_ADDR (pSessionEntry->pLdpPeer).Addr),
			LDP_PEER_NET_ADDR (pSessionEntry->pLdpPeer).u2AddrType);

	LDP_L2VPN_EVT_SSN_ADDR_TYPE(&PwVcEvtInfo)=LDP_PEER_NET_ADDR (pSessionEntry->pLdpPeer).u2AddrType;

        MEMCPY (&LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&PwVcEvtInfo),
                &pLdpEntity->LdpId, IPV4_ADDR_LENGTH);

        LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&PwVcEvtInfo) =
            OSIX_NTOHL (LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&PwVcEvtInfo));
        LDP_L2VPN_EVT_SSN_LCL_ENT_INDEX (&PwVcEvtInfo) =
            pLdpEntity->u4EntityIndex;

        MEMCPY ((UINT1 *) &LDP_L2VPN_EVT_SSN_PEER_ENTITYID (&PwVcEvtInfo),
                (UINT1 *) &LDP_PEER_ID (pSessionEntry->pLdpPeer),
                IPV4_ADDR_LENGTH);
        LDP_L2VPN_EVT_SSN_PEER_ENTITYID (&PwVcEvtInfo) =
            OSIX_NTOHL (LDP_L2VPN_EVT_SSN_PEER_ENTITYID (&PwVcEvtInfo));

        LDP_L2VPN_EVT_SSN_LCL_ROLE (&PwVcEvtInfo) =
            pSessionEntry->u1SessionRole == LDP_ACTIVE;

        if (LdpL2VpnPostEvent (&PwVcEvtInfo) == LDP_FAILURE)
        {
            LDP_DBG (LDP_PRCS_PRCS, "Post To Application Failed \n");
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpHandleFecMsg                                           */
/* Description   : This function handles the Label messages for a PwVc FEC   */
/* Input(s)      : pLdpSession - Pointer to the session in which the message */
/*                               is received                                 */
/*                 pMsg        - Pointer to the Message                      */
/*                 u4MsgType   - type of Label Message Received              */
/*                 u1FecType   - type of Fec Element (i.e 128/129)           */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpHandleFecMsg (tLdpSession * pSessionEntry, UINT1 *pMsg, UINT4 u4MsgType,
                 UINT1 u1FecType)
{
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT4               u4RegIndex;

    for (u4RegIndex = 0; u4RegIndex < MAX_LDP_APPLICATIONS; u4RegIndex++)
    {
        if ((LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u1FecType
             == u1FecType)
            && (LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u1RegFlag
                == LDP_REGISTER))
        {
            if (LDP_REGISTRATION_TABLE
                (u2IncarnId, u4RegIndex).pCallBackFnAppPktIn != NULL)
            {
                LDP_REGISTRATION_TABLE
                    (u2IncarnId, u4RegIndex).pCallBackFnAppPktIn
                    (pSessionEntry, pMsg, u4MsgType);
                return LDP_SUCCESS;
            }
            else
            {
                break;
            }
        }
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpHandleFecL2VpnMsg                                      */
/* Description   : This function handles the Label messages for a PwVc FEC   */
/* Input(s)      : pLdpSession - Pointer to the session in which the message */
/*                               is received                                 */
/*                 pMsg        - Pointer to the Message                      */
/*                 u4MsgType   - type of Label Message Received              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
/* ldppwif.c */
VOID
LdpHandleFecL2VpnMsg (tLdpSession * pSessionEntry, UINT1 *pMsg, UINT4 u4MsgType)
{
    UINT1               u1RetStatus = LDP_SUCCESS;

    switch (u4MsgType)
    {
        case LDP_LBL_MAPPING_MSG:
            u1RetStatus = LdpPwVcHandleLblMap (pSessionEntry, pMsg);
            break;

        case LDP_LBL_RELEASE_MSG:
            u1RetStatus = LdpPwVcHandleLblRel (pSessionEntry, pMsg);
            break;

        case LDP_LBL_WITHDRAW_MSG:
            u1RetStatus = LdpPwVcHandleLblWdraw (pSessionEntry, pMsg);
            break;

        case LDP_LBL_REQUEST_MSG:
            u1RetStatus = LdpPwVcHandleLblReq (pSessionEntry, pMsg);
            break;

        case LDP_NOTIF_MSG:
            u1RetStatus = LdpPwVcHandleNotifMsg (pSessionEntry, pMsg);
            break;

        case LDP_ADDR_WITHDRAW_MSG:
            u1RetStatus = LdpPwVcHandleAddrWdraw (pSessionEntry, pMsg);
            break;

        default:
            u1RetStatus = LDP_FAILURE;
            break;
    }

    if (u1RetStatus == LDP_FAILURE)
    {
        LDP_DBG (LDP_PRCS_PRCS, "Handling Fec 128 or Fec 129 Msg Failed.\n");
    }
    else
    {
        LDP_DBG (LDP_PRCS_PRCS,
                 "Handling Fec 128 or Fec 129 Msg Successful.\n");
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpPwVcHandleLblReq                                       */
/* Description   : This function handles the Label Req message for a PwVc FEC*/
/*                 and Sends the received Lbl Req Msg Info to the Application*/
/*                 registered.                                               */
/* Input(s)      : pLdpSession - Pointer to the session in which the message */
/*                               is received                                 */
/*                 pMsg        - Pointer to the Message                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpPwVcHandleLblReq (tLdpSession * pLdpSession, UINT1 *pMsg)
{
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1MsgPtr = pMsg;
    UINT1              *pu1FecMsgPtr = NULL;
    UINT1              *pu1IfMsgPtr = NULL;
    INT1               *pi1IfDescr = NULL;
    tL2VpnLdpPwVcEvtInfo *pPwVcEvtInfo = NULL;
    UINT1               au1IfDescStr[MAX_LDP_L2VPN_IF_STR_LEN];

    tGenU4Addr          PeerAddr;
    UINT4               u4GroupID = LDP_ZERO;
    UINT4               u4PwVcID = LDP_ZERO;

    UINT2               u2IfMtu = LDP_ZERO;

    UINT2               u2MsgLen = LDP_ZERO;
    UINT2               u2TlvType = LDP_ZERO;
    UINT2               u2TlvLen = LDP_ZERO;
    UINT2               u2FecTlvLen = LDP_ZERO;
    UINT2               u2PwVcType = LDP_ZERO;

    UINT1               u1VcTlv = LDP_ZERO;
    UINT1               u1PwVcInfoLen = LDP_ZERO;
    UINT1               u1IfParamId = LDP_ZERO;
    UINT1               u1IfParamLen = LDP_ZERO;

    INT1                i1CBit = LDP_ZERO;
    UINT4               u4LabelOrReqId;
    UINT1               u1AgiType = LDP_ZERO;
    UINT1               u1AiiType = LDP_ZERO;
    UINT1               u1AgiLen = LDP_ZERO;
    UINT1               u1SaiiLen = LDP_ZERO;
    UINT1               u1TaiiLen = LDP_ZERO;
    UINT1               u1UnknownTlvFlag = LDP_ZERO;

    MEMSET(&PeerAddr,LDP_ZERO,sizeof(tGenU4Addr));

    pPwVcEvtInfo =
        (tL2VpnLdpPwVcEvtInfo *) MemAllocMemBlk (LDP_L2VPN_EVT_INFO_POOL_ID);
    if (pPwVcEvtInfo == NULL)
    {
        LDP_DBG ((LDP_APP_MEM | LDP_IF_MEM),
                 "Memory allocation failed for L2VPN Event Info while "
                 "processing LDP label request for PW FEC\r\n");
        return LDP_FAILURE;
    }
    MEMSET (pPwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    MEMSET (au1IfDescStr, LDP_ZERO, MAX_LDP_L2VPN_IF_STR_LEN);

    pLdpPeer = pLdpSession->pLdpPeer;

    LdpCopyAAddr(&PeerAddr.Addr,&(LDP_PEER_NET_ADDR (pLdpPeer).Addr),
		    LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType);

    PeerAddr.u2AddrType=LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType;

    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Message ID is obtained */
    LDP_EXT_4_BYTES (pu1MsgPtr, u4LabelOrReqId);

    /* Extracting FEC param TLV */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
    u2FecTlvLen = u2TlvLen;

    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "Notif Msg Snt - BAD TLV LEN - %x \n", LDP_STAT_BAD_TLV_LEN);
        LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN, pMsg,
                                 LDP_FALSE);
        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, (UINT1 *) pPwVcEvtInfo);
        return LDP_FAILURE;
    }

    /* FecMsgPtr points to the contents of Fec */
    pu1FecMsgPtr = pu1MsgPtr;

    /* Extract FEC Element Type & PW Type */
    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1VcTlv);
    LDP_EXT_2_BYTES (pu1FecMsgPtr, u2PwVcType);

    if (((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) != LDP_PWVC_TYPE_ETHERNET) &&
        ((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) !=
         LDP_PWVC_TYPE_ETHERNET_VLAN))
    {
        LDP_DBG (LDP_PRCS_PRCS, "Received non ethernet type PwVc.\n");
        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, (UINT1 *) pPwVcEvtInfo);
        return LDP_FAILURE;
    }

    /* Extract C Bit */
    LDP_GET_PWVC_C_BIT (u2PwVcType, i1CBit);

    /* Extract Pw Info length */
    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1PwVcInfoLen);

    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (pPwVcEvtInfo) = LDP_PWID_FEC_SIGNALING;
        if (u1PwVcInfoLen == LDP_ZERO)
        {
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received Wild Card PWVC FEC Element Req message.\n");
            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                (UINT1 *) pPwVcEvtInfo);
            return LDP_FAILURE;
        }

        if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN +
                            LDP_PWVC_FEC_ELEM_GROUP_ID_LEN))
        {
            /* Only a single PW FEC element MUST be advertised per
             * LDP PW label
             * The length mismatch implies there are more FEC Elements
             * or it is a corrupted TLV */
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received FEC Tlv with more than one PWVC FEC Element.\n");
            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                (UINT1 *) pPwVcEvtInfo);
            return LDP_FAILURE;
        }

        LDP_EXT_4_BYTES (pu1FecMsgPtr, u4GroupID);
        LDP_EXT_4_BYTES (pu1FecMsgPtr, u4PwVcID);

        if (u1PwVcInfoLen > LDP_PWVC_FEC_ELEM_PWVCID_LEN)
        {
            /* case of Interface Parameter present */
            while (pu1FecMsgPtr < (pu1MsgPtr + LDP_PWVC_FEC_ELEM_HDR_LEN +
                                   LDP_PWVC_FEC_ELEM_GROUP_ID_LEN +
                                   u1PwVcInfoLen))
            {
                LDP_EXT_1_BYTES (pu1FecMsgPtr, u1IfParamId);
                LDP_EXT_1_BYTES (pu1FecMsgPtr, u1IfParamLen);

                /* Check for bad TLV Length performed here */
                if ((pu1FecMsgPtr - LDP_PWVC_FEC_IF_PARAM_HDR_LEN +
                     u1IfParamLen) >
                    (pu1MsgPtr + LDP_PWVC_FEC_ELEM_HDR_LEN +
                     LDP_PWVC_FEC_ELEM_GROUP_ID_LEN + u1PwVcInfoLen))
                {
                    /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                    LDP_DBG1 (LDP_PRCS_PRCS,
                              "Notif Msg Snt - BAD TLV LEN - %x \n",
                              LDP_STAT_BAD_TLV_LEN);
                    LdpSsmSendNotifCloseSsn (pLdpSession,
                                             LDP_STAT_TYPE_BAD_TLV_LEN, pMsg,
                                             LDP_FALSE);
                    MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                        (UINT1 *) pPwVcEvtInfo);
                    return LDP_FAILURE;
                }

                u1IfParamLen -= LDP_PWVC_FEC_IF_PARAM_HDR_LEN;

                switch (u1IfParamId)
                {
                    case LDP_IFPARAM_IF_MTU:

                        LDP_EXT_2_BYTES (pu1FecMsgPtr, u2IfMtu);
                        break;

                    case LDP_IFPARAM_IF_DESC_STR:

                        MEMCPY (&au1IfDescStr, pu1FecMsgPtr, u1IfParamLen);
                        pu1FecMsgPtr += u1IfParamLen;
                        break;

                    case LDP_IFPARAM_REQ_VLAN_ID:

                        LDP_EXT_2_BYTES (pu1FecMsgPtr,
                                         LDP_L2VPN_EVT_LBL_REQ_VLANID
                                         (pPwVcEvtInfo));
                        break;

                    case LDP_IFPARAM_MAX_CONC_ATM_CELLS:
                    case LDP_IFPARAM_CEM_PAYLOAD_BYTES:
                    case LDP_IFPARAM_CEP_OPTIONS:
                    case LDP_IFPARAM_FR_DLCI_LEN:
                    case LDP_IFPARAM_FRAG_IND:
                    case LDP_IFPARAM_FCS_RETN_IND:
                        pu1FecMsgPtr += LDP_IFPARAM_IF_TYPE1_LEN;
                        break;

                    case LDP_IFPARAM_CEP_BIT_RATE:
                        pu1FecMsgPtr += LDP_IFPARAM_IF_TYPE2_LEN;
                        break;

                    case LDP_IFPARAM_TDM_OPTIONS:
                        if (u1IfParamLen == LDP_IFPARAM_IF_TYPE1_LEN)
                        {
                            pu1FecMsgPtr += LDP_IFPARAM_IF_TYPE1_LEN;
                        }
                        else if (u1IfParamLen == LDP_IFPARAM_IF_TYPE3_LEN)
                        {
                            pu1FecMsgPtr += LDP_IFPARAM_IF_TYPE3_LEN;
                        }
                        else if (u1IfParamLen == LDP_IFPARAM_IF_TYPE5_LEN)
                        {
                            pu1FecMsgPtr += LDP_IFPARAM_IF_TYPE5_LEN;
                        }
                        break;
                    default:
                        LDP_DBG (LDP_PRCS_PRCS, "Encountered unknown If Param, "
                                 "silently discarding it.\n");
                }
            }
        }
    }
    /* FEC Element type is FEC 129 */
    if (u1VcTlv == LDP_FEC_GEN_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (pPwVcEvtInfo) = LDP_GEN_FEC_SIGNALING;

        if (u1PwVcInfoLen == LDP_ZERO)
        {
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received Wild Card Gen FEC Element Req message.\n");
            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                (UINT1 *) pPwVcEvtInfo);
            return LDP_FAILURE;
        }

        if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN))
        {
            /* Only a single Gen FEC element MUST be advertised per
             * LDP PW label
             * The length mismatch implies there are more FEC Elements
             * or it is a corrupted TLV */
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received FEC Tlv with more than one Gen FEC Element.\n");
            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                (UINT1 *) pPwVcEvtInfo);
            return LDP_FAILURE;
        }

        if (u1PwVcInfoLen > LDP_ZERO)
        {
            /* Extract AGI Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiLen);
            LDP_L2VPN_EVT_LBL_AGI_TYPE (pPwVcEvtInfo) = u1AgiType;
            if (u1AgiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_AGI (pPwVcEvtInfo), pu1FecMsgPtr,
                        u1AgiLen);
                pu1FecMsgPtr += u1AgiLen;
            }
            LDP_L2VPN_EVT_LBL_AGI_LEN (pPwVcEvtInfo) = u1AgiLen;

            /* Extract SAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1SaiiLen);
            LDP_L2VPN_EVT_LBL_SAII_TYPE (pPwVcEvtInfo) = u1AiiType;
            if (u1SaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_SAII (pPwVcEvtInfo), pu1FecMsgPtr,
                        u1SaiiLen);
                pu1FecMsgPtr += u1SaiiLen;
            }
            LDP_L2VPN_EVT_LBL_SAII_LEN (pPwVcEvtInfo) = u1SaiiLen;

            /* Extract TAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1TaiiLen);
            LDP_L2VPN_EVT_LBL_TAII_TYPE (pPwVcEvtInfo) = u1AiiType;
            if (u1TaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_TAII (pPwVcEvtInfo), pu1FecMsgPtr,
                        u1TaiiLen);
                pu1FecMsgPtr += u1TaiiLen;
            }
            LDP_L2VPN_EVT_LBL_TAII_LEN (pPwVcEvtInfo) = u1TaiiLen;

            /* Check for bad TLV Length performed here */
            if (pu1FecMsgPtr >
                (pu1MsgPtr + LDP_PWVC_FEC_ELEM_HDR_LEN + u1PwVcInfoLen))
            {
                /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                LDP_DBG1 (LDP_PRCS_PRCS,
                          "Notif Msg Snt - BAD TLV LEN - %x \n",
                          LDP_STAT_BAD_TLV_LEN);
                LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                         pMsg, LDP_FALSE);
                MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                    (UINT1 *) pPwVcEvtInfo);
                return LDP_FAILURE;
            }
        }
    }

    /* FEC Element type is FEC 129 */
    if (u1VcTlv == LDP_FEC_GEN_PWVC_TYPE)
    {
        /* Extract optional parameters TLV */
        while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
        {
            LDP_GET_2_BYTES (pu1MsgPtr, u2TlvType);
            switch (u2TlvType & LDP_PWVC_GET_TLV_TYPE_MASK)
            {
                case LDP_PW_IF_PARAMS_TLV:
                    /* Extract Interface Parameter TLV present */
                    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                    pu1IfMsgPtr = pu1MsgPtr;

                    /* Extract Interface Parameter sub-TLV present */
                    while (pu1MsgPtr < (pu1IfMsgPtr + u2TlvLen))
                    {
                        LDP_EXT_1_BYTES (pu1MsgPtr, u1IfParamId);
                        LDP_EXT_1_BYTES (pu1MsgPtr, u1IfParamLen);

                        /* Check for bad TLV Length performed here */
                        if ((pu1MsgPtr - LDP_PWVC_FEC_IF_PARAM_HDR_LEN +
                             u1IfParamLen) > (pu1IfMsgPtr + u2TlvLen))
                        {
                            /* Notification message with 
                             * "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                            LDP_DBG1 (LDP_PRCS_PRCS,
                                      "Notif Msg Snt - BAD TLV LEN - %x \n",
                                      LDP_STAT_BAD_TLV_LEN);
                            LdpSsmSendNotifCloseSsn (pLdpSession,
                                                     LDP_STAT_TYPE_BAD_TLV_LEN,
                                                     pMsg, LDP_FALSE);
                            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                                (UINT1 *) pPwVcEvtInfo);
                            return LDP_FAILURE;
                        }

                        u1IfParamLen -= LDP_PWVC_FEC_IF_PARAM_HDR_LEN;

                        switch (u1IfParamId)
                        {
                            case LDP_IFPARAM_IF_MTU:

                                LDP_EXT_2_BYTES (pu1MsgPtr, u2IfMtu);
                                break;

                            case LDP_IFPARAM_IF_DESC_STR:

                                MEMCPY (&au1IfDescStr, pu1MsgPtr, u1IfParamLen);
                                pu1MsgPtr += u1IfParamLen;
                                break;

                            case LDP_IFPARAM_REQ_VLAN_ID:

                                LDP_EXT_2_BYTES (pu1MsgPtr,
                                                 LDP_L2VPN_EVT_LBL_REQ_VLANID
                                                 (pPwVcEvtInfo));
                                break;

                            case LDP_IFPARAM_MAX_CONC_ATM_CELLS:
                            case LDP_IFPARAM_CEM_PAYLOAD_BYTES:
                            case LDP_IFPARAM_CEP_OPTIONS:
                            case LDP_IFPARAM_FR_DLCI_LEN:
                            case LDP_IFPARAM_FRAG_IND:
                            case LDP_IFPARAM_FCS_RETN_IND:
                                pu1MsgPtr += LDP_IFPARAM_IF_TYPE1_LEN;
                                break;

                            case LDP_IFPARAM_CEP_BIT_RATE:
                                pu1MsgPtr += LDP_IFPARAM_IF_TYPE2_LEN;
                                break;

                            case LDP_IFPARAM_TDM_OPTIONS:
                                if (u1IfParamLen == LDP_IFPARAM_IF_TYPE1_LEN)
                                {
                                    pu1MsgPtr += LDP_IFPARAM_IF_TYPE1_LEN;
                                }
                                else if (u1IfParamLen ==
                                         LDP_IFPARAM_IF_TYPE3_LEN)
                                {
                                    pu1MsgPtr += LDP_IFPARAM_IF_TYPE3_LEN;
                                }
                                else if (u1IfParamLen ==
                                         LDP_IFPARAM_IF_TYPE5_LEN)
                                {
                                    pu1MsgPtr += LDP_IFPARAM_IF_TYPE5_LEN;
                                }
                                break;

                            default:
                                LDP_DBG (LDP_PRCS_PRCS,
                                         "Encountered unknown If Param, "
                                         "silently discarding it.\n");
                        }
                    }
                    break;

                case LDP_PW_GROUPING_ID_TLV:

                    /* Extract PW Grouping ID TLV */
                    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                    LDP_EXT_4_BYTES (pu1MsgPtr,
                                     LDP_L2VPN_EVT_LBL_GRP_ID (pPwVcEvtInfo));
                    break;

                default:
                    LDP_GET_STAT_U_BIT (u2TlvType, u1UnknownTlvFlag);
                    if (u1UnknownTlvFlag == LDP_ONE)
                    {
                        /* U bit =1, then ignore the TLV without sending the notification */
                        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                        pu1MsgPtr += u2TlvLen;
                        break;
                    }
                    else
                    {
                        /* Notification message with
                         * "LDP_STAT_TYPE_UNKNOWN_TLV" sent. */
                        LdpSendNotifMsg (pLdpSession, pMsg, LDP_FALSE,
                                         LDP_STAT_TYPE_UNKNOWN_TLV, NULL);
                        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                            (UINT1 *) pPwVcEvtInfo);
                        return LDP_FAILURE;
                    }
            }
        }
    }

    /* No Label TLV present in Label Request Msg */

    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "Notif Msg Snt - BAD TLV LEN - %x \n", LDP_STAT_BAD_TLV_LEN);
        LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                 pMsg, LDP_FALSE);
        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, (UINT1 *) pPwVcEvtInfo);
        return LDP_FAILURE;
    }

    /* NOTE :
     * Optional TLVs like Hop count and path vector can be
     * ignored as they are not going to be propagated.
     * And also the message is exchanged only between two
     * peers and there is no forwarding.
     *
     * TLV processing Ends here.
     */

    LdpCopyAddr(&LDP_L2VPN_EVT_LBL_PEER (pPwVcEvtInfo),&PeerAddr.Addr,PeerAddr.u2AddrType);
    LDP_L2VPN_EVT_LBL_ADDR_TYPE(pPwVcEvtInfo)=PeerAddr.u2AddrType;

    LDP_L2VPN_EVT_LBL_LBLVAL (pPwVcEvtInfo) = LDP_INVALID_LABEL;

    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_GRP_ID (pPwVcEvtInfo) = u4GroupID;
        LDP_L2VPN_EVT_LBL_PWVC_ID (pPwVcEvtInfo) = u4PwVcID;
    }
    LDP_L2VPN_EVT_LBL_IF_MTU (pPwVcEvtInfo) = u2IfMtu;
    LDP_L2VPN_EVT_LBL_CNTRL_WORD (pPwVcEvtInfo) = i1CBit;
    LDP_L2VPN_EVT_LBL_PW_TYPE (pPwVcEvtInfo) = (INT1) u2PwVcType;
    LDP_L2VPN_EVT_LBL_MSG_TYPE (pPwVcEvtInfo) = L2VPN_LDP_LBL_MAP_REQ_MSG;
    LDP_L2VPN_EVT_LBL_REQ_MSGID (pPwVcEvtInfo) = u4LabelOrReqId;

    if (STRCMP (au1IfDescStr, "") != LDP_ZERO)
    {
        pi1IfDescr = (INT1 *) MemAllocMemBlk (LDP_L2VPN_IF_STR_POOL_ID);

        if (pi1IfDescr == NULL)
        {
            LDP_DBG ((LDP_APP_MEM | LDP_IF_MEM),
                     "Memory allocation failed for L2VPN IF Str Info while "
                     "processing LDP label request for PW FEC\r\n");
            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                (UINT1 *) pPwVcEvtInfo);
            return LDP_FAILURE;
        }

        MEMCPY (pi1IfDescr, au1IfDescStr, u1IfParamLen);
        LDP_L2VPN_EVT_LBL_LCL_IFSTR (pPwVcEvtInfo) = pi1IfDescr;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_LCL_IFSTR (pPwVcEvtInfo) = NULL;
    }

    LDP_L2VPN_EVT_TYPE (pPwVcEvtInfo) = L2VPN_LDP_PWVC_LBL_MSG;
    LdpL2VpnPostEvent (pPwVcEvtInfo);
    if (pi1IfDescr != NULL)
    {
        MemReleaseMemBlock (LDP_L2VPN_IF_STR_POOL_ID, (UINT1 *) pi1IfDescr);
    }
    MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, (UINT1 *) pPwVcEvtInfo);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpPwVcHandleLblMap                                       */
/* Description   : This function handles the Label Map message for a PwVc FEC*/
/*                 and Sends the received Lbl Map Msg Info to the Application*/
/*                 registered.                                               */
/* Input(s)      : pLdpSession - Pointer to the session in which the message */
/*                               is received                                 */
/*                 pMsg        - Pointer to the Message                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpPwVcHandleLblMap (tLdpSession * pLdpSession, UINT1 *pMsg)
{
    tLdpEntity         *pLdpEntity = SSN_GET_ENTITY (pLdpSession);
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1MsgPtr = pMsg;
    UINT1              *pu1FecMsgPtr = NULL;
    UINT1              *pu1IfMsgPtr = NULL;
    INT1               *pi1IfDescr = NULL;
    tL2VpnLdpPwVcEvtInfo *pPwVcEvtInfo = NULL;
    UINT1               au1IfDescStr[MAX_LDP_L2VPN_IF_STR_LEN];

    tGenU4Addr          PeerAddr;
    UINT4               u4GroupID = LDP_ZERO;
    UINT4               u4PwVcID = LDP_ZERO;
    UINT4               u4GenLabel = LDP_ZERO;
    UINT4               u4LabelOrReqId = LDP_ZERO;
    UINT4               u4LdpEntityIndex = LDP_ZERO;

    UINT2               u2IfMtu = LDP_ZERO;
    UINT2               u2IfParamIfMtu = LDP_FALSE;

    UINT2               u2MsgLen = LDP_ZERO;
    UINT2               u2TlvType = LDP_ZERO;
    UINT2               u2TlvLen = LDP_ZERO;
    UINT2               u2FecTlvLen = LDP_ZERO;
    UINT2               u2PwVcType = LDP_ZERO;

    UINT1               u1VcTlv = LDP_ZERO;
    UINT1               u1PwVcInfoLen = LDP_ZERO;
    UINT1               u1IfParamId = LDP_ZERO;
    UINT1               u1IfParamLen = LDP_ZERO;

    INT1                i1CBit = LDP_ZERO;

    UINT1               u1AgiType = LDP_ZERO;
    UINT1               u1AiiType = LDP_ZERO;
    UINT1               u1AgiLen = LDP_ZERO;
    UINT1               u1SaiiLen = LDP_ZERO;
    UINT1               u1TaiiLen = LDP_ZERO;
    UINT4               u4PwStatusCode = LDP_ZERO;

    UINT1               u1VccvCcType = LDP_ZERO;
    UINT1               u1VccvCvType = LDP_ZERO;
    UINT1               u1UnknownTlvFlag = LDP_ZERO;

    MEMSET(&PeerAddr,LDP_ZERO,sizeof(tGenU4Addr));

    pPwVcEvtInfo =
        (tL2VpnLdpPwVcEvtInfo *) MemAllocMemBlk (LDP_L2VPN_EVT_INFO_POOL_ID);
    if (pPwVcEvtInfo == NULL)
    {
        LDP_DBG ((LDP_APP_MEM | LDP_IF_MEM),
                 "Memory allocation failed for L2VPN Event Info while "
                 "processing LDP label mapping for PW FEC\r\n");
        return LDP_FAILURE;
    }
    MEMSET (pPwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    MEMSET (au1IfDescStr, LDP_ZERO, MAX_LDP_L2VPN_IF_STR_LEN);

    pLdpPeer = pLdpSession->pLdpPeer;

    LdpCopyAAddr(&PeerAddr.Addr,&(LDP_PEER_NET_ADDR (pLdpPeer).Addr),
		    LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType);
    PeerAddr.u2AddrType=LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType;

    u4LdpEntityIndex = SSN_GET_ENTITY (pLdpSession)->u4EntityIndex;
    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Pointer to moved across the message Id field */
    pu1MsgPtr = pu1MsgPtr + LDP_MSGID_LEN;

    /* Extracting FEC param TLV */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
    u2FecTlvLen = u2TlvLen;

    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "Notif Msg Snt - BAD TLV LEN - %x \n", LDP_STAT_BAD_TLV_LEN);
        LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                 pMsg, LDP_FALSE);
        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, (UINT1 *) pPwVcEvtInfo);
        return LDP_FAILURE;
    }

    pu1FecMsgPtr = pu1MsgPtr;

    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1VcTlv);
    LDP_EXT_2_BYTES (pu1FecMsgPtr, u2PwVcType);

    if (((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) != LDP_PWVC_TYPE_ETHERNET) &&
        ((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) !=
         LDP_PWVC_TYPE_ETHERNET_VLAN))
    {
        LDP_DBG (LDP_PRCS_PRCS, "Received non ethernet type PwVc.\n");
        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, (UINT1 *) pPwVcEvtInfo);
        return LDP_FAILURE;
    }

    LDP_GET_PWVC_C_BIT (u2PwVcType, i1CBit);

    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1PwVcInfoLen);

    if ((gi4MplsSimulateFailure == LDP_SIM_FAILURE_GEN_FEC_NOT_SUPPORTED) &&
        (u1VcTlv == LDP_FEC_GEN_PWVC_TYPE))
    {
        /* Notification message with "LDP_STAT_TYPE_UNKNOWN_FEC"  sent. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "Notif Msg Snt - LDP_STAT_TYPE_UNKNOWN_FEC - %x \n",
                  LDP_STAT_BAD_TLV_LEN);
        LdpSendNotifMsg (pLdpSession, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_UNKNOWN_FEC, NULL);
        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, (UINT1 *) pPwVcEvtInfo);
        return LDP_FAILURE;
    }

    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (pPwVcEvtInfo) = LDP_PWID_FEC_SIGNALING;
        if (u1PwVcInfoLen == LDP_ZERO)
        {
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received Wild Card PWVC FEC Element Map message.\n");
            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                (UINT1 *) pPwVcEvtInfo);
            return LDP_FAILURE;
        }

        if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN +
                            LDP_PWVC_FEC_ELEM_GROUP_ID_LEN))
        {
            /* Only a single PW FEC element MUST be advertised per
             * LDP PW label
             * The length mismatch implies there are more FEC Elements
             * or it is a corrupted TLV */
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received FEC Tlv with more than one PWVC FEC Element.\n");
            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                (UINT1 *) pPwVcEvtInfo);
            return LDP_FAILURE;
        }

        LDP_EXT_4_BYTES (pu1FecMsgPtr, u4GroupID);
        LDP_EXT_4_BYTES (pu1FecMsgPtr, u4PwVcID);

        if (u1PwVcInfoLen > LDP_PWVC_FEC_ELEM_PWVCID_LEN)
        {
            /* case of Interface Parameter present */
            while (pu1FecMsgPtr < (pu1MsgPtr + LDP_PWVC_FEC_ELEM_HDR_LEN +
                                   LDP_PWVC_FEC_ELEM_GROUP_ID_LEN +
                                   u1PwVcInfoLen))
            {
                LDP_EXT_1_BYTES (pu1FecMsgPtr, u1IfParamId);
                LDP_EXT_1_BYTES (pu1FecMsgPtr, u1IfParamLen);

                /* Check for bad TLV Length performed here */
                if ((pu1FecMsgPtr - LDP_PWVC_FEC_IF_PARAM_HDR_LEN +
                     u1IfParamLen) >
                    (pu1MsgPtr + LDP_PWVC_FEC_ELEM_HDR_LEN +
                     LDP_PWVC_FEC_ELEM_GROUP_ID_LEN + u1PwVcInfoLen))
                {
                    /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                    LDP_DBG1 (LDP_PRCS_PRCS,
                              "Notif Msg Snt - BAD TLV LEN - %x \n",
                              LDP_STAT_BAD_TLV_LEN);
                    LdpSsmSendNotifCloseSsn (pLdpSession,
                                             LDP_STAT_TYPE_BAD_TLV_LEN,
                                             pMsg, LDP_FALSE);
                    MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                        (UINT1 *) pPwVcEvtInfo);
                    return LDP_FAILURE;
                }

                u1IfParamLen -= LDP_PWVC_FEC_IF_PARAM_HDR_LEN;

                switch (u1IfParamId)
                {
                    case LDP_IFPARAM_IF_MTU:

                        LDP_EXT_2_BYTES (pu1FecMsgPtr, u2IfMtu);
                        u2IfParamIfMtu = LDP_TRUE;
                        break;

                    case LDP_IFPARAM_IF_DESC_STR:

                        MEMCPY (&au1IfDescStr, pu1FecMsgPtr, u1IfParamLen);
                        pu1FecMsgPtr += u1IfParamLen;
                        break;

                    case LDP_IFPARAM_REQ_VLAN_ID:

                        LDP_EXT_2_BYTES (pu1FecMsgPtr,
                                         LDP_L2VPN_EVT_LBL_REQ_VLANID
                                         (pPwVcEvtInfo));
                        break;

                    case LDP_IFPARAM_MAX_CONC_ATM_CELLS:
                    case LDP_IFPARAM_CEM_PAYLOAD_BYTES:
                    case LDP_IFPARAM_CEP_OPTIONS:
                    case LDP_IFPARAM_FR_DLCI_LEN:
                    case LDP_IFPARAM_FRAG_IND:
                    case LDP_IFPARAM_FCS_RETN_IND:
                        pu1FecMsgPtr += LDP_IFPARAM_IF_TYPE1_LEN;
                        break;

                    case LDP_IFPARAM_VCCV_PARAM:
                        LDP_EXT_1_BYTES (pu1FecMsgPtr, u1VccvCcType);
                        LDP_EXT_1_BYTES (pu1FecMsgPtr, u1VccvCvType);
                        break;

                    case LDP_IFPARAM_CEP_BIT_RATE:
                        pu1FecMsgPtr += LDP_IFPARAM_IF_TYPE2_LEN;
                        break;

                    case LDP_IFPARAM_TDM_OPTIONS:
                        if (u1IfParamLen == LDP_IFPARAM_IF_TYPE1_LEN)
                        {
                            pu1FecMsgPtr += LDP_IFPARAM_IF_TYPE1_LEN;
                        }
                        else if (u1IfParamLen == LDP_IFPARAM_IF_TYPE3_LEN)
                        {
                            pu1FecMsgPtr += LDP_IFPARAM_IF_TYPE3_LEN;
                        }
                        else if (u1IfParamLen == LDP_IFPARAM_IF_TYPE5_LEN)
                        {
                            pu1FecMsgPtr += LDP_IFPARAM_IF_TYPE5_LEN;
                        }
                        break;
                    default:
                        LDP_DBG (LDP_PRCS_PRCS, "Encountered unknown If Param, "
                                 "silently discarding it.\n");
                }
            }
        }
    }
    /* FEC Element type is FEC 129 */
    if (u1VcTlv == LDP_FEC_GEN_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (pPwVcEvtInfo) = LDP_GEN_FEC_SIGNALING;

        if (u1PwVcInfoLen == LDP_ZERO)
        {
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received Wild Card Gen FEC Element Req message.\n");
            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                (UINT1 *) pPwVcEvtInfo);
            return LDP_FAILURE;
        }

        if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN))
        {
            /* Only a single Gen FEC element MUST be advertised per
             * LDP PW label
             * The length mismatch implies there are more FEC Elements
             * or it is a corrupted TLV */
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received FEC Tlv with more than one Gen FEC Element.\n");
            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                (UINT1 *) pPwVcEvtInfo);
            return LDP_FAILURE;
        }

        if (u1PwVcInfoLen > LDP_ZERO)
        {
            /* Extract AGI Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiType);
            LDP_L2VPN_EVT_LBL_AGI_TYPE (pPwVcEvtInfo) = u1AgiType;

            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiLen);
            if (u1AgiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_AGI (pPwVcEvtInfo), pu1FecMsgPtr,
                        u1AgiLen);
                pu1FecMsgPtr += u1AgiLen;
            }
            LDP_L2VPN_EVT_LBL_AGI_LEN (pPwVcEvtInfo) = u1AgiLen;

            /* Extract SAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1SaiiLen);
            LDP_L2VPN_EVT_LBL_SAII_TYPE (pPwVcEvtInfo) = u1AiiType;
            if (u1SaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_SAII (pPwVcEvtInfo), pu1FecMsgPtr,
                        u1SaiiLen);
                pu1FecMsgPtr += u1SaiiLen;
            }
            LDP_L2VPN_EVT_LBL_SAII_LEN (pPwVcEvtInfo) = u1SaiiLen;

            /* Extract TAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1TaiiLen);
            LDP_L2VPN_EVT_LBL_TAII_TYPE (pPwVcEvtInfo) = u1AiiType;
            if (u1TaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_TAII (pPwVcEvtInfo), pu1FecMsgPtr,
                        u1TaiiLen);
                pu1FecMsgPtr += u1TaiiLen;
            }
            LDP_L2VPN_EVT_LBL_TAII_LEN (pPwVcEvtInfo) = u1TaiiLen;

            /* Check for bad TLV Length performed here */
            if (pu1FecMsgPtr >
                (pu1MsgPtr + LDP_PWVC_FEC_ELEM_HDR_LEN + u1PwVcInfoLen))
            {
                /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                LDP_DBG1 (LDP_PRCS_PRCS,
                          "Notif Msg Snt - BAD TLV LEN - %x \n",
                          LDP_STAT_BAD_TLV_LEN);
                LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                         pMsg, LDP_FALSE);
                MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                    (UINT1 *) pPwVcEvtInfo);
                return LDP_FAILURE;
            }
        }
    }

    /* Extracting the Label TLV */
    pu1MsgPtr = pu1FecMsgPtr;

    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "Notif Msg Snt - BAD TLV LEN - %x \n", LDP_STAT_BAD_TLV_LEN);
        LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                 pMsg, LDP_FALSE);
        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, (UINT1 *) pPwVcEvtInfo);
        return LDP_FAILURE;
    }

    LDP_EXT_4_BYTES (pu1MsgPtr, u4GenLabel);

    /* Extract optional parameters TLV */
    while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
    {
        LDP_GET_2_BYTES (pu1MsgPtr, u2TlvType);
        switch (u2TlvType)
        {
            case LDP_PW_IF_PARAMS_TLV:
                /* Extract Interface Parameter TLV present */
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                pu1IfMsgPtr = pu1MsgPtr;

                /* Extract Interface Parameter sub-TLV present */
                while (pu1MsgPtr < (pu1IfMsgPtr + u2TlvLen))
                {
                    LDP_EXT_1_BYTES (pu1MsgPtr, u1IfParamId);
                    LDP_EXT_1_BYTES (pu1MsgPtr, u1IfParamLen);

                    /* Check for bad TLV Length performed here */
                    if ((pu1MsgPtr - LDP_PWVC_FEC_IF_PARAM_HDR_LEN +
                         u1IfParamLen) > (pu1IfMsgPtr + u2TlvLen))
                    {
                        /* Notification message with 
                         * "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                        LDP_DBG1 (LDP_PRCS_PRCS,
                                  "Notif Msg Snt - BAD TLV LEN - %x \n",
                                  LDP_STAT_BAD_TLV_LEN);
                        LdpSsmSendNotifCloseSsn (pLdpSession,
                                                 LDP_STAT_TYPE_BAD_TLV_LEN,
                                                 pMsg, LDP_FALSE);
                        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                            (UINT1 *) pPwVcEvtInfo);
                        return LDP_FAILURE;
                    }

                    u1IfParamLen -= LDP_PWVC_FEC_IF_PARAM_HDR_LEN;

                    switch (u1IfParamId)
                    {
                        case LDP_IFPARAM_IF_MTU:

                            LDP_EXT_2_BYTES (pu1MsgPtr, u2IfMtu);
                            u2IfParamIfMtu = LDP_TRUE;
                            break;

                        case LDP_IFPARAM_IF_DESC_STR:

                            MEMCPY (&au1IfDescStr, pu1MsgPtr, u1IfParamLen);
                            pu1MsgPtr += u1IfParamLen;
                            break;

                        case LDP_IFPARAM_REQ_VLAN_ID:

                            LDP_EXT_2_BYTES (pu1MsgPtr,
                                             LDP_L2VPN_EVT_LBL_REQ_VLANID
                                             (pPwVcEvtInfo));
                            break;

                        case LDP_IFPARAM_MAX_CONC_ATM_CELLS:
                        case LDP_IFPARAM_CEM_PAYLOAD_BYTES:
                        case LDP_IFPARAM_CEP_OPTIONS:
                        case LDP_IFPARAM_FR_DLCI_LEN:
                        case LDP_IFPARAM_FRAG_IND:
                        case LDP_IFPARAM_FCS_RETN_IND:
                            pu1MsgPtr += LDP_IFPARAM_IF_TYPE1_LEN;
                            break;

                        case LDP_IFPARAM_VCCV_PARAM:
                            LDP_EXT_1_BYTES (pu1MsgPtr, u1VccvCcType);
                            LDP_EXT_1_BYTES (pu1MsgPtr, u1VccvCvType);
                            break;

                        case LDP_IFPARAM_CEP_BIT_RATE:
                            pu1MsgPtr += LDP_IFPARAM_IF_TYPE2_LEN;
                            break;

                        case LDP_IFPARAM_TDM_OPTIONS:
                            if (u1IfParamLen == LDP_IFPARAM_IF_TYPE1_LEN)
                            {
                                pu1MsgPtr += LDP_IFPARAM_IF_TYPE1_LEN;
                            }
                            else if (u1IfParamLen == LDP_IFPARAM_IF_TYPE3_LEN)
                            {
                                pu1MsgPtr += LDP_IFPARAM_IF_TYPE3_LEN;
                            }
                            else if (u1IfParamLen == LDP_IFPARAM_IF_TYPE5_LEN)
                            {
                                pu1MsgPtr += LDP_IFPARAM_IF_TYPE5_LEN;
                            }
                            break;

                        default:
                            LDP_DBG (LDP_PRCS_PRCS,
                                     "Encountered unknown If Param, "
                                     "silently discarding it.\n");
                    }
                }
                break;
            case LDP_PW_GROUPING_ID_TLV:

                /* Extract PW Grouping ID TLV */
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                LDP_EXT_4_BYTES (pu1MsgPtr,
                                 LDP_L2VPN_EVT_LBL_GRP_ID (pPwVcEvtInfo));
                break;
            case LDP_PW_STATUS_TLV:
                /* Extract PW Status TLV */
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                LDP_EXT_4_BYTES (pu1MsgPtr, u4PwStatusCode);
                LDP_L2VPN_EVT_LBL_RMT_STATUS_CAPABLE (pPwVcEvtInfo) =
                    LDP_RMT_STATUS_CAPABLE;
                break;
            case LDP_LBL_REQ_MSGID_TLV:
                /* Extract Label Request TLV */
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                LDP_EXT_4_BYTES (pu1MsgPtr, u4LabelOrReqId);
                break;
            default:
                LDP_GET_STAT_U_BIT (u2TlvType, u1UnknownTlvFlag);
                if (u1UnknownTlvFlag == LDP_ONE)
                {
                    /* U bit =1, then ignore the TLV without sending the notification */
                    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                    pu1MsgPtr += u2TlvLen;
                    break;
                }
                else
                {
                    /* Notification message with
                     * "LDP_STAT_TYPE_UNKNOWN_TLV" sent. */
                    LdpSendNotifMsg (pLdpSession, pMsg, LDP_FALSE,
                                     LDP_STAT_TYPE_UNKNOWN_TLV, NULL);
                    MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                        (UINT1 *) pPwVcEvtInfo);
                    return LDP_FAILURE;
                }
        }
    }

    if (LDP_L2VPN_EVT_LBL_RMT_STATUS_CAPABLE (pPwVcEvtInfo) !=
        LDP_RMT_STATUS_CAPABLE)
    {
        LDP_L2VPN_EVT_LBL_RMT_STATUS_CAPABLE (pPwVcEvtInfo) =
            LDP_RMT_STATUS_NOT_CAPABLE;
    }

    if (u2IfParamIfMtu == LDP_FALSE)
    {
        /* Notification message with "LDP_STAT_TYPE_MISSING_MSG_PARAM" sent.
         */
        LDP_DBG1 (LDP_PRCS_PRCS, "Notif Msg Snt - Mand Param Missing - %x\n",
                  LDP_STAT_TYPE_MISSING_MSG_PARAM);
        LdpSendNotifMsg (pLdpSession, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, (UINT1 *) pPwVcEvtInfo);
        return LDP_FAILURE;
    }

    /* NOTE :
     * Optional TLVs like Hop count and path vector can be
     * ignored as they are not going to be propagated.
     * And also the message is exchanged only between two
     * peers and there is no forwarding.
     *
     * TLV processing Ends here.
     */

    LdpCopyAddr(&(LDP_L2VPN_EVT_LBL_PEER (pPwVcEvtInfo)),&PeerAddr.Addr,PeerAddr.u2AddrType);
    LDP_L2VPN_EVT_LBL_ADDR_TYPE (pPwVcEvtInfo)=PeerAddr.u2AddrType;

    LDP_L2VPN_EVT_LBL_LBLVAL (pPwVcEvtInfo) = u4GenLabel;

    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_GRP_ID (pPwVcEvtInfo) = u4GroupID;
        LDP_L2VPN_EVT_LBL_PWVC_ID (pPwVcEvtInfo) = u4PwVcID;
    }
    LDP_L2VPN_EVT_LBL_IF_MTU (pPwVcEvtInfo) = u2IfMtu;
    LDP_L2VPN_EVT_LBL_CNTRL_WORD (pPwVcEvtInfo) = i1CBit;
    LDP_L2VPN_EVT_LBL_PW_TYPE (pPwVcEvtInfo) = (INT1) u2PwVcType;
    LDP_L2VPN_EVT_LBL_MSG_TYPE (pPwVcEvtInfo) = L2VPN_LDP_LBL_MAP_MSG;
    LDP_L2VPN_EVT_LBL_REQ_MSGID (pPwVcEvtInfo) = u4LabelOrReqId;
    LDP_L2VPN_EVT_LBL_PW_STATUS_CODE (pPwVcEvtInfo) = u4PwStatusCode;

    LDP_L2VPN_EVT_LBL_LCL_CC_ADVERT (pPwVcEvtInfo) = u1VccvCcType;
    LDP_L2VPN_EVT_LBL_LCL_CV_ADVERT (pPwVcEvtInfo) = u1VccvCvType;

    MEMCPY (&LDP_L2VPN_EVT_LBL_LCL_ENTID (pPwVcEvtInfo),
            &pLdpEntity->LdpId, IPV4_ADDR_LENGTH);
    LDP_L2VPN_EVT_LBL_LCL_ENTID (pPwVcEvtInfo) =
        OSIX_NTOHL (LDP_L2VPN_EVT_LBL_LCL_ENTID (pPwVcEvtInfo));
    LDP_L2VPN_EVT_LBL_LCL_ENT_INDEX (pPwVcEvtInfo) = u4LdpEntityIndex;

    MEMCPY (&LDP_L2VPN_EVT_LBL_PEER_ENTITYID (pPwVcEvtInfo),
            &LDP_PEER_ID (pLdpSession->pLdpPeer), IPV4_ADDR_LENGTH);
    LDP_L2VPN_EVT_LBL_PEER_ENTITYID (pPwVcEvtInfo) =
        OSIX_NTOHL (LDP_L2VPN_EVT_LBL_PEER_ENTITYID (pPwVcEvtInfo));

    LDP_L2VPN_EVT_LBL_LCL_ROLE (pPwVcEvtInfo) =
        pLdpSession->u1SessionRole == LDP_ACTIVE;

    if (STRCMP (au1IfDescStr, "") != LDP_ZERO)
    {
        pi1IfDescr = (INT1 *) MemAllocMemBlk (LDP_L2VPN_IF_STR_POOL_ID);

        if (pi1IfDescr == NULL)
        {
            LDP_DBG ((LDP_APP_MEM | LDP_IF_MEM),
                     "Memory allocation failed for L2VPN IF Str Info while "
                     "processing LDP label map for PW FEC\r\n");

            MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                (UINT1 *) pPwVcEvtInfo);
            return LDP_FAILURE;
        }
        MEMCPY (pi1IfDescr, au1IfDescStr, u1IfParamLen);
        LDP_L2VPN_EVT_LBL_LCL_IFSTR (pPwVcEvtInfo) = pi1IfDescr;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_LCL_IFSTR (pPwVcEvtInfo) = NULL;
    }
    LDP_L2VPN_EVT_TYPE (pPwVcEvtInfo) = L2VPN_LDP_PWVC_LBL_MSG;

    LdpL2VpnPostEvent (pPwVcEvtInfo);
    if (pi1IfDescr != NULL)
    {
        MemReleaseMemBlock (LDP_L2VPN_IF_STR_POOL_ID, (UINT1 *) pi1IfDescr);
    }
    MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, (UINT1 *) pPwVcEvtInfo);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpPwVcHandleLblRel                                       */
/* Description   : This function handles the Label Rel message for a PwVc FEC*/
/*                 and Sends the received Lbl Rel Msg Info to the Application*/
/*                 registered.                                               */
/* Input(s)      : pLdpSession - Pointer to the session in which the message */
/*                               is received                                 */
/*                 pMsg        - Pointer to the Message                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpPwVcHandleLblRel (tLdpSession * pLdpSession, UINT1 *pMsg)
{
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1MsgPtr = pMsg;
    UINT1              *pu1FecMsgPtr = NULL;
    tGenU4Addr          PeerAddr ;
    UINT4               u4GroupID = LDP_ZERO;
    UINT4               u4PwVcID = LDP_ZERO;
    UINT4               u4GenLabel = LDP_INVALID_LABEL;
    UINT4               u4StatusCode = LDP_ZERO;
    UINT4               u4StatusData = LDP_ZERO;

    UINT2               u2MsgLen = LDP_ZERO;
    UINT2               u2TlvType = LDP_ZERO;
    UINT2               u2TlvLen = LDP_ZERO;
    UINT2               u2FecTlvLen = LDP_ZERO;
    UINT2               u2PwVcType = LDP_ZERO;

    UINT1               u1VcTlv = LDP_ZERO;
    UINT1               u1PwVcInfoLen = LDP_ZERO;
    INT1                i1CBit = LDP_ZERO;
    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;
    UINT1               u1AgiType = LDP_ZERO;
    UINT1               u1AiiType = LDP_ZERO;
    UINT1               u1AgiLen = LDP_ZERO;
    UINT1               u1SaiiLen = LDP_ZERO;
    UINT1               u1TaiiLen = LDP_ZERO;
    UINT1               u1UnknownTlvFlag = LDP_ZERO;

    MEMSET(&PeerAddr,LDP_ZERO,sizeof(tGenU4Addr));

    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    pLdpPeer = pLdpSession->pLdpPeer;

    LdpCopyAAddr(&PeerAddr.Addr,&(LDP_PEER_NET_ADDR (pLdpPeer).Addr),
		    LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType);
    PeerAddr.u2AddrType=LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType;

    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Pointer to moved across the message Id field */
    pu1MsgPtr = pu1MsgPtr + LDP_MSGID_LEN;

    /* Extracting FEC param TLV */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
    u2FecTlvLen = u2TlvLen;

    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "Notif Msg Snt - BAD TLV LEN - %x \n", LDP_STAT_BAD_TLV_LEN);
        LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN, pMsg,
                                 LDP_FALSE);
        return LDP_FAILURE;
    }

    /* FecMsgPtr points to the contents of Fec */
    pu1FecMsgPtr = pu1MsgPtr;

    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1VcTlv);
    LDP_EXT_2_BYTES (pu1FecMsgPtr, u2PwVcType);

    if (((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) != LDP_PWVC_TYPE_ETHERNET) &&
        ((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) !=
         LDP_PWVC_TYPE_ETHERNET_VLAN))
    {
        LDP_DBG (LDP_PRCS_PRCS, "Received non ethernet type PwVc.\n");
        return LDP_FAILURE;
    }

    LDP_GET_PWVC_C_BIT (u2PwVcType, i1CBit);

    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1PwVcInfoLen);

    /* Label Release may have wildcard FEC element */

    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&PwVcEvtInfo) = LDP_PWID_FEC_SIGNALING;

        if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN +
                            LDP_PWVC_FEC_ELEM_GROUP_ID_LEN))
        {
            /* Only a single PW FEC element MUST be advertised per
             * LDP PW label
             * The length mismatch implies there are more FEC Elements
             * or it is a corrupted TLV */
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received FEC Tlv with more than one PWVC FEC Element.\n");
            return LDP_FAILURE;
        }

        LDP_EXT_4_BYTES (pu1FecMsgPtr, u4GroupID);
        if (u1PwVcInfoLen > LDP_ZERO)
        {
            LDP_EXT_4_BYTES (pu1FecMsgPtr, u4PwVcID);
        }

        /* NOTE :
         * Interface Parameters and Optional TLV, label TLV are not
         * extracted.
         * Reason being PwVcID and VcType by itself will identify
         * a particular PwVc.
         */
        pu1MsgPtr += (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN +
                      LDP_PWVC_FEC_ELEM_GROUP_ID_LEN);
    }
    /* FEC Element type is FEC 129 */
    if (u1VcTlv == LDP_FEC_GEN_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&PwVcEvtInfo) = LDP_GEN_FEC_SIGNALING;

        if (u1PwVcInfoLen == LDP_ZERO)
        {
            if (u2FecTlvLen != LDP_PWVC_FEC_ELEM_HDR_LEN)
            {
                /* Only a single PW FEC element MUST be advertised per
                 * LDP PW label
                 * The length mismatch implies there are more FEC Elements
                 * or it is a corrupted TLV */
                LDP_DBG (LDP_PRCS_PRCS,
                         "Received FEC Tlv with more than one Gen FEC Element.\n");
                return LDP_FAILURE;
            }
            pu1MsgPtr += LDP_PWVC_FEC_ELEM_HDR_LEN;
        }
        else
        {
            if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN))
            {
                /* Only a single Gen FEC element MUST be advertised per
                 * LDP PW label
                 * The length mismatch implies there are more FEC Elements
                 * or it is a corrupted TLV */
                LDP_DBG (LDP_PRCS_PRCS,
                         "Received FEC Tlv with more than one Gen FEC Element.\n");
                return LDP_FAILURE;
            }

            /* Extract AGI Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiType);
            LDP_L2VPN_EVT_LBL_AGI_TYPE (&PwVcEvtInfo) = u1AgiType;
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiLen);
            if (u1AgiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_AGI (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1AgiLen);
                pu1FecMsgPtr += u1AgiLen;
            }
            LDP_L2VPN_EVT_LBL_AGI_LEN (&PwVcEvtInfo) = u1AgiLen;

            /* Extract SAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1SaiiLen);
            LDP_L2VPN_EVT_LBL_SAII_TYPE (&PwVcEvtInfo) = u1AiiType;
            if (u1SaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_SAII (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1SaiiLen);
                pu1FecMsgPtr += u1SaiiLen;
            }
            LDP_L2VPN_EVT_LBL_SAII_LEN (&PwVcEvtInfo) = u1SaiiLen;

            /* Extract TAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1TaiiLen);
            LDP_L2VPN_EVT_LBL_TAII_TYPE (&PwVcEvtInfo) = u1AiiType;
            if (u1TaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_TAII (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1TaiiLen);
                pu1FecMsgPtr += u1TaiiLen;
            }
            LDP_L2VPN_EVT_LBL_TAII_LEN (&PwVcEvtInfo) = u1TaiiLen;

            /* Check for bad TLV Length performed here */
            if (pu1FecMsgPtr >
                (pu1MsgPtr + LDP_PWVC_FEC_ELEM_HDR_LEN + u1PwVcInfoLen))
            {
                /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                LDP_DBG1 (LDP_PRCS_PRCS,
                          "Notif Msg Snt - BAD TLV LEN - %x \n",
                          LDP_STAT_BAD_TLV_LEN);
                LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                         pMsg, LDP_FALSE);
                return LDP_FAILURE;
            }
            pu1MsgPtr += (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN);
        }
    }

    while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
    {
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        /* Check for bad TLV Length performed here */
        if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
        {
            /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
            LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                     pMsg, LDP_FALSE);
            return LDP_FAILURE;
        }

        LDP_GET_STAT_U_BIT (u2TlvType, u1UnknownTlvFlag);
        u2TlvType = u2TlvType & LDP_PWVC_RESET_U_BIT_MASK;
        switch (u2TlvType)
        {
            case LDP_GEN_LABEL_TLV:

                LDP_GET_4_BYTES (pu1MsgPtr, u4GenLabel);
                pu1MsgPtr += u2TlvLen;
                break;

            case LDP_STATUS_TLV:

                /* Offset to StatusCode */
                LDP_EXT_4_BYTES (pu1MsgPtr, u4StatusCode);
                u4StatusData = (u4StatusCode & LDP_STATUS_DATA_MASK);
                pu1MsgPtr += LDP_MSG_ID_LEN;
                pu1MsgPtr += LDP_MSG_TYPE_LEN;
                break;

            case LDP_PW_GROUPING_ID_TLV:
                /* Extract PW Grouping ID TLV */
                LDP_EXT_4_BYTES (pu1MsgPtr,
                                 LDP_L2VPN_EVT_LBL_GRP_ID (&PwVcEvtInfo));
                break;

            default:
                if (u1UnknownTlvFlag == LDP_ONE)
                {
                    /* U bit =1, then ignore the TLV without sending the notification */
                    pu1MsgPtr += u2TlvLen;
                    break;
                }
                else
                {
                    /* Notification message with
                     * "LDP_STAT_TYPE_UNKNOWN_TLV" sent. */
                    LdpSendNotifMsg (pLdpSession, pMsg, LDP_FALSE,
                                     LDP_STAT_TYPE_UNKNOWN_TLV, NULL);
                    return LDP_FAILURE;
                }
        }
    }
    /* TLV processing Ends here. */
    LdpCopyAddr(&(LDP_L2VPN_EVT_LBL_PEER (&PwVcEvtInfo)),&PeerAddr.Addr,PeerAddr.u2AddrType);
    LDP_L2VPN_EVT_LBL_ADDR_TYPE (&PwVcEvtInfo)=PeerAddr.u2AddrType;

    LDP_L2VPN_EVT_LBL_LBLVAL (&PwVcEvtInfo) = u4GenLabel;
    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_GRP_ID (&PwVcEvtInfo) = u4GroupID;
        LDP_L2VPN_EVT_LBL_PWVC_ID (&PwVcEvtInfo) = u4PwVcID;
    }
    LDP_L2VPN_EVT_LBL_CNTRL_WORD (&PwVcEvtInfo) = i1CBit;
    LDP_L2VPN_EVT_LBL_PW_TYPE (&PwVcEvtInfo) = (INT1) u2PwVcType;
    if (u4StatusData == LDP_STAT_PWVC_WRONG_C_BIT_VAL)
    {
        LDP_L2VPN_EVT_LBL_STAT_CODE (&PwVcEvtInfo) = LDP_STAT_PWVC_WRONG_C_BIT;
    }
    else if (u4StatusData == LDP_STAT_PWVC_ILLEGAL_C_BIT_VAL)
    {
        LDP_L2VPN_EVT_LBL_STAT_CODE (&PwVcEvtInfo) =
            LDP_STAT_PWVC_ILLEGAL_C_BIT;
    }
    else if (u4StatusData == LDP_STAT_PWVC_GEN_MISCONF_ERR)
    {
        LDP_L2VPN_EVT_LBL_STAT_CODE (&PwVcEvtInfo) =
            LDP_STAT_PWVC_GEN_MISCONFIG_ERR;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_STAT_CODE (&PwVcEvtInfo) = LDP_STAT_PWVC_CODE_NONE;
    }

    LDP_L2VPN_EVT_LBL_MSG_TYPE (&PwVcEvtInfo) = L2VPN_LDP_LBL_REL_MSG;
    LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = L2VPN_LDP_PWVC_LBL_MSG;
    LdpL2VpnPostEvent (&PwVcEvtInfo);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpPwVcHandleLblWdraw                                     */
/* Description   : This function handles the Label Wdraw message for a PwVc  */
/*                 FEC and Sends the received Lbl Wdraw Msg Info to the      */
/*                 Application registered.                                   */
/* Input(s)      : pLdpSession - Pointer to the session in which the message */
/*                               is received                                 */
/*                 pMsg        - Pointer to the Message                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpPwVcHandleLblWdraw (tLdpSession * pLdpSession, UINT1 *pMsg)
{
    tLdpPeer           *pLdpPeer = NULL;

    UINT1              *pu1MsgPtr = pMsg;
    UINT1              *pu1FecMsgPtr = NULL;

    tGenU4Addr          PeerAddr;
    UINT4               u4GroupID = LDP_ZERO;
    UINT4               u4PwVcID = LDP_ZERO;
    UINT4               u4GenLabel = LDP_INVALID_LABEL;
    UINT4               u4StatusCode = LDP_ZERO;
    UINT4               u4StatusData = LDP_ZERO;

    UINT2               u2MsgLen = LDP_ZERO;
    UINT2               u2TlvType = LDP_ZERO;
    UINT2               u2TlvLen = LDP_ZERO;
    UINT2               u2FecTlvLen = LDP_ZERO;
    UINT2               u2PwVcType = LDP_ZERO;

    UINT1               u1VcTlv = LDP_ZERO;
    UINT1               u1PwVcInfoLen = LDP_ZERO;
    UINT1               u1WildCardFec = LDP_FALSE;

    INT1                i1CBit = LDP_ZERO;

    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;
    UINT1               u1AgiType = LDP_ZERO;
    UINT1               u1AiiType = LDP_ZERO;
    UINT1               u1AgiLen = LDP_ZERO;
    UINT1               u1SaiiLen = LDP_ZERO;
    UINT1               u1TaiiLen = LDP_ZERO;
    UINT1               u1UnknownTlvFlag = LDP_ZERO;
    
    MEMSET(&PeerAddr,LDP_ZERO,sizeof(tGenU4Addr));

    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    pLdpPeer = pLdpSession->pLdpPeer;

    LdpCopyAAddr(&PeerAddr.Addr,&(LDP_PEER_NET_ADDR (pLdpPeer).Addr),
		    LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType);
    PeerAddr.u2AddrType=LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType;

    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Pointer to moved across the message Id field */
    pu1MsgPtr = pu1MsgPtr + LDP_MSGID_LEN;

    /* Extracting FEC param TLV */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
    u2FecTlvLen = u2TlvLen;

    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "Notif Msg Snt - BAD TLV LEN - %x \n", LDP_STAT_BAD_TLV_LEN);
        LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                 pMsg, LDP_FALSE);
        return LDP_FAILURE;
    }

    /* FecMsgPtr points to the contents of Fec */
    pu1FecMsgPtr = pu1MsgPtr;

    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1VcTlv);
    LDP_EXT_2_BYTES (pu1FecMsgPtr, u2PwVcType);

    if (((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) != LDP_PWVC_TYPE_ETHERNET) &&
        ((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) !=
         LDP_PWVC_TYPE_ETHERNET_VLAN))
    {
        LDP_DBG (LDP_PRCS_PRCS, "Received non ethernet type PwVc.\n");
        return LDP_FAILURE;
    }

    LDP_GET_PWVC_C_BIT (u2PwVcType, i1CBit);

    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1PwVcInfoLen);

    /* Label Withdraw may have wildcard FEC element */

    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&PwVcEvtInfo) = LDP_PWID_FEC_SIGNALING;

        if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN +
                            LDP_PWVC_FEC_ELEM_GROUP_ID_LEN))
        {
            /* Only a single PW FEC element MUST be advertised per
             * LDP PW label
             * The length mismatch implies there are more FEC Elements
             * or it is a corrupted TLV */
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received FEC Tlv with more than one PWVC FEC Element.\n");
            return LDP_FAILURE;
        }

        LDP_EXT_4_BYTES (pu1FecMsgPtr, u4GroupID);
        if (u1PwVcInfoLen > LDP_ZERO)
        {
            LDP_EXT_4_BYTES (pu1FecMsgPtr, u4PwVcID);
        }
        else
        {
            u1WildCardFec = LDP_TRUE;
        }

        /* NOTE :
         * Interface Parameters and Optional TLV, label TLV are not
         * extracted.
         * Reason being PwVcID and VcType by itself will identify
         * a particular PwVc.
         */
        pu1MsgPtr += (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN +
                      LDP_PWVC_FEC_ELEM_GROUP_ID_LEN);
    }
    /* FEC Element type is FEC 129 */
    if (u1VcTlv == LDP_FEC_GEN_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_PW_OWNER (&PwVcEvtInfo) = LDP_GEN_FEC_SIGNALING;

        if (u1PwVcInfoLen == LDP_ZERO)
        {
            if (u2FecTlvLen != LDP_PWVC_FEC_ELEM_HDR_LEN)
            {
                /* Only a single PW FEC element MUST be advertised per
                 * LDP PW label
                 * The length mismatch implies there are more FEC Elements
                 * or it is a corrupted TLV */
                LDP_DBG (LDP_PRCS_PRCS,
                         "Received FEC Tlv with more than one Gen FEC Element.\n");
                return LDP_FAILURE;
            }
            pu1MsgPtr += LDP_PWVC_FEC_ELEM_HDR_LEN;
        }
        else
        {
            if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN))
            {
                /* Only a single Gen FEC element MUST be advertised per
                 * LDP PW label
                 * The length mismatch implies there are more FEC Elements
                 * or it is a corrupted TLV */
                LDP_DBG (LDP_PRCS_PRCS,
                         "Received FEC Tlv with more than one Gen FEC Element.\n");
                return LDP_FAILURE;
            }

            /* Extract AGI Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiType);
            LDP_L2VPN_EVT_LBL_AGI_TYPE (&PwVcEvtInfo) = u1AgiType;
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiLen);
            if (u1AgiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_AGI (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1AgiLen);
                pu1FecMsgPtr += u1AgiLen;
            }
            LDP_L2VPN_EVT_LBL_AGI_LEN (&PwVcEvtInfo) = u1AgiLen;

            /* Extract SAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1SaiiLen);
            LDP_L2VPN_EVT_LBL_SAII_TYPE (&PwVcEvtInfo) = u1AiiType;
            if (u1SaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_SAII (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1SaiiLen);
                pu1FecMsgPtr += u1SaiiLen;
            }
            LDP_L2VPN_EVT_LBL_SAII_LEN (&PwVcEvtInfo) = u1SaiiLen;

            /* Extract TAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1TaiiLen);
            LDP_L2VPN_EVT_LBL_TAII_TYPE (&PwVcEvtInfo) = u1AiiType;
            if (u1TaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_LBL_TAII (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1TaiiLen);
                pu1FecMsgPtr += u1TaiiLen;
            }
            LDP_L2VPN_EVT_LBL_TAII_LEN (&PwVcEvtInfo) = u1TaiiLen;

            /* Check for bad TLV Length performed here */
            if (pu1FecMsgPtr >
                (pu1MsgPtr + LDP_PWVC_FEC_ELEM_HDR_LEN + u1PwVcInfoLen))
            {
                /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                LDP_DBG1 (LDP_PRCS_PRCS,
                          "Notif Msg Snt - BAD TLV LEN - %x \n",
                          LDP_STAT_BAD_TLV_LEN);
                LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                         pMsg, LDP_FALSE);
                return LDP_FAILURE;
            }

            pu1MsgPtr += (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN);
        }

    }

    while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
    {
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        /* Check for bad TLV Length performed here */
        if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
        {
            /* Notification message with
             * "LDP_STAT_TYPE_BAD_TLV_LEN"  sent */
            LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                     pMsg, LDP_FALSE);
            return LDP_FAILURE;
        }

        LDP_GET_STAT_U_BIT (u2TlvType, u1UnknownTlvFlag);
        u2TlvType = u2TlvType & LDP_PWVC_RESET_U_BIT_MASK;
        switch (u2TlvType)
        {
            case LDP_GEN_LABEL_TLV:

                LDP_GET_4_BYTES (pu1MsgPtr, u4GenLabel);
                pu1MsgPtr += u2TlvLen;
                break;

            case LDP_STATUS_TLV:

                /* Offset to StatusCode */
                LDP_EXT_4_BYTES (pu1MsgPtr, u4StatusCode);
                u4StatusData = (u4StatusCode & LDP_STATUS_DATA_MASK);
                pu1MsgPtr += LDP_MSG_ID_LEN;
                pu1MsgPtr += LDP_MSG_TYPE_LEN;
                break;

            case LDP_PW_GROUPING_ID_TLV:
                /* Extract PW Grouping ID TLV */
                LDP_EXT_4_BYTES (pu1MsgPtr,
                                 LDP_L2VPN_EVT_LBL_GRP_ID (&PwVcEvtInfo));
                break;

            default:

                if (u1UnknownTlvFlag == LDP_ONE)
                {
                    /* U bit =1, then ignore the TLV without sending the notification */
                    pu1MsgPtr += u2TlvLen;
                    break;
                }
                else
                {
                    /* Notification message with
                     * "LDP_STAT_TYPE_UNKNOWN_TLV" sent. */
                    LdpSendNotifMsg (pLdpSession, pMsg, LDP_FALSE,
                                     LDP_STAT_TYPE_UNKNOWN_TLV, NULL);
                    return LDP_FAILURE;
                }
        }
    }
    /* TLV processing Ends here. */

    LdpCopyAddr(&(LDP_L2VPN_EVT_LBL_PEER (&PwVcEvtInfo)),&PeerAddr.Addr,PeerAddr.u2AddrType);
    LDP_L2VPN_EVT_LBL_ADDR_TYPE (&PwVcEvtInfo)=PeerAddr.u2AddrType;

    LDP_L2VPN_EVT_LBL_LBLVAL (&PwVcEvtInfo) = u4GenLabel;
    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_GRP_ID (&PwVcEvtInfo) = u4GroupID;
        LDP_L2VPN_EVT_LBL_PWVC_ID (&PwVcEvtInfo) = u4PwVcID;
    }
    if ((u4StatusData == LDP_STAT_PWVC_WRONG_C_BIT_VAL) ||
        ((gi4MplsSimulateFailure == LDP_SIM_FAILURE_WRONG_C_BIT_VAL_RFC_4906) &&
         (u4StatusData == LDP_STAT_PWVC_WRONG_C_BIT_VAL_RFC_4906)))
    {
        LDP_L2VPN_EVT_LBL_STAT_CODE (&PwVcEvtInfo) = LDP_STAT_PWVC_WRONG_C_BIT;
    }
    else if (u4StatusData == LDP_STAT_PWVC_ILLEGAL_C_BIT_VAL)
    {
        LDP_L2VPN_EVT_LBL_STAT_CODE (&PwVcEvtInfo) =
            LDP_STAT_PWVC_ILLEGAL_C_BIT;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_STAT_CODE (&PwVcEvtInfo) = LDP_STAT_PWVC_CODE_NONE;
    }

    LDP_L2VPN_EVT_LBL_CNTRL_WORD (&PwVcEvtInfo) = i1CBit;
    LDP_L2VPN_EVT_LBL_PW_TYPE (&PwVcEvtInfo) = (INT1) u2PwVcType;
    if (u1WildCardFec == LDP_TRUE)
    {
        LDP_L2VPN_EVT_LBL_MSG_TYPE (&PwVcEvtInfo) = L2VPN_LDP_LBL_WC_WDRAW_MSG;
    }
    else
    {
        LDP_L2VPN_EVT_LBL_MSG_TYPE (&PwVcEvtInfo) = L2VPN_LDP_LBL_WDRAW_MSG;
    }
    LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = L2VPN_LDP_PWVC_LBL_MSG;
    LdpL2VpnPostEvent (&PwVcEvtInfo);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpPwVcHandleNotifMsg                                     */
/* Description   : This function handles the Notification message for a PwVc */
/*                 FEC and Sends the received Notification Msg Info to the   */
/*                 Application registered.                                   */
/* Input(s)      : pLdpSession - Pointer to the session in which the message */
/*                               is received                                 */
/*                 pMsg        - Pointer to the Message                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpPwVcHandleNotifMsg (tLdpSession * pLdpSession, UINT1 *pMsg)
{
    tLdpPeer           *pLdpPeer = NULL;

    UINT1              *pu1MsgPtr = pMsg;
    UINT1              *pu1FecMsgPtr = NULL;

    tGenU4Addr          PeerAddr;
    UINT4               u4GroupID = LDP_ZERO;
    UINT4               u4PwVcID = LDP_ZERO;
    UINT4               u4StatusCode = LDP_ZERO;

    UINT2               u2MsgLen = LDP_ZERO;
    UINT2               u2TlvType = LDP_ZERO;
    UINT2               u2TlvLen = LDP_ZERO;
    UINT2               u2FecTlvLen = LDP_ZERO;
    UINT2               u2PwVcType = LDP_ZERO;

    UINT1               u1VcTlv = LDP_ZERO;
    UINT1               u1PwVcInfoLen = LDP_ZERO;

    INT1                i1CBit = LDP_ZERO;

    UINT1               u1AgiType = LDP_ZERO;
    UINT1               u1AiiType = LDP_ZERO;
    UINT1               u1AgiLen = LDP_ZERO;
    UINT1               u1SaiiLen = LDP_ZERO;
    UINT1               u1TaiiLen = LDP_ZERO;
    UINT4               u4TempStatusCode = LDP_ZERO;

    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;

    MEMSET(&PeerAddr,LDP_ZERO,sizeof(tGenU4Addr));

    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    pLdpPeer = pLdpSession->pLdpPeer;

    LdpCopyAAddr(&PeerAddr.Addr,&(LDP_PEER_NET_ADDR (pLdpPeer).Addr),
		    LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType);
    PeerAddr.u2AddrType=LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType;

    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Pointer to moved across the message Id field */
    pu1MsgPtr = pu1MsgPtr + LDP_MSGID_LEN;

    /* Extracting Status TLV */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "Notif Msg Snt - BAD TLV LEN - %x \n", LDP_STAT_BAD_TLV_LEN);
        LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                 pMsg, LDP_FALSE);
        return LDP_FAILURE;
    }

    LDP_EXT_4_BYTES (pu1MsgPtr, u4StatusCode);

    if (u4StatusCode == LDP_STAT_PW_STATUS)
    {
        /* Move the pointer to extract PW Status TLV */
        /* Skip the Message ID and Message Type fields as they are set to zero */
        pu1MsgPtr += (LDP_UINT2_LEN + LDP_UINT2_LEN + LDP_UINT2_LEN);

        /* Extract PW Status TLV */
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        /* Check for bad TLV Length performed here */
        if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
        {
            /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
            LDP_DBG1 (LDP_PRCS_PRCS,
                      "Notif Msg Snt - BAD TLV LEN - %x \n",
                      LDP_STAT_BAD_TLV_LEN);
            LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                     pMsg, LDP_FALSE);
            return LDP_FAILURE;
        }

        LDP_EXT_4_BYTES (pu1MsgPtr, u4TempStatusCode);

        LDP_L2VPN_EVT_LBL_NOTIF_PW_STATUS_CODE (&PwVcEvtInfo) =
            (UINT1) u4TempStatusCode;

    }

    /* Extracting FEC param TLV */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
    u2FecTlvLen = u2TlvLen;

    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "Notif Msg Snt - BAD TLV LEN - %x \n", LDP_STAT_BAD_TLV_LEN);
        LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                 pMsg, LDP_FALSE);
        return LDP_FAILURE;
    }

    /* FecMsgPtr points to the contents of Fec */
    pu1FecMsgPtr = pu1MsgPtr;

    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1VcTlv);
    LDP_EXT_2_BYTES (pu1FecMsgPtr, u2PwVcType);

    if (((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) != LDP_PWVC_TYPE_ETHERNET) &&
        ((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) !=
         LDP_PWVC_TYPE_ETHERNET_VLAN))
    {
        LDP_DBG (LDP_PRCS_PRCS, "Received non ethernet type PwVc.\n");
        return LDP_FAILURE;
    }

    LDP_GET_PWVC_C_BIT (u2PwVcType, i1CBit);

    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1PwVcInfoLen);

    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&PwVcEvtInfo) = LDP_PWID_FEC_SIGNALING;

        if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN +
                            LDP_PWVC_FEC_ELEM_GROUP_ID_LEN))
        {
            /* Only a single PW FEC element MUST be advertised per
             * LDP PW label
             * The length mismatch implies there are more FEC Elements
             * or it is a corrupted TLV */
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received FEC Tlv with more than one PWVC FEC Element.\n");
            return LDP_FAILURE;
        }

        LDP_EXT_4_BYTES (pu1FecMsgPtr, u4GroupID);

        if (u1PwVcInfoLen > LDP_ZERO)
        {
            LDP_EXT_4_BYTES (pu1FecMsgPtr, u4PwVcID);
        }

        /* NOTE :
         * Interface Parameters and Optional TLV, label TLV are not
         * extracted.
         * Reason being PwVcID and VcType by itself will identify
         * a particular PwVc.
         */
        pu1MsgPtr += (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN +
                      LDP_PWVC_FEC_ELEM_GROUP_ID_LEN);
    }
    /* FEC Element type is FEC 129 */
    if (u1VcTlv == LDP_FEC_GEN_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&PwVcEvtInfo) = LDP_GEN_FEC_SIGNALING;

        if (u1PwVcInfoLen == LDP_ZERO)
        {
            if (u2FecTlvLen != LDP_PWVC_FEC_ELEM_HDR_LEN)
            {
                /* Only a single PW FEC element MUST be advertised per
                 * LDP PW label
                 * The length mismatch implies there are more FEC Elements
                 * or it is a corrupted TLV */
                LDP_DBG (LDP_PRCS_PRCS,
                         "Received FEC Tlv with more than one Gen FEC Element.\n");
                return LDP_FAILURE;
            }
            pu1MsgPtr += LDP_PWVC_FEC_ELEM_HDR_LEN;
        }
        else
        {
            if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN))
            {
                /* Only a single Gen FEC element MUST be advertised per
                 * LDP PW label
                 * The length mismatch implies there are more FEC Elements
                 * or it is a corrupted TLV */
                LDP_DBG (LDP_PRCS_PRCS,
                         "Received FEC Tlv with more than one Gen FEC Element.\n");
                return LDP_FAILURE;
            }

            /* Extract AGI Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiType);
            LDP_L2VPN_EVT_NOTIF_AGI_TYPE (&PwVcEvtInfo) = u1AgiType;
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiLen);
            if (u1AgiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_NOTIF_AGI (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1AgiLen);
                pu1FecMsgPtr += u1AgiLen;
            }
            LDP_L2VPN_EVT_NOTIF_AGI_LEN (&PwVcEvtInfo) = u1AgiLen;

            /* Extract SAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1SaiiLen);
            LDP_L2VPN_EVT_NOTIF_SAII_TYPE (&PwVcEvtInfo) = u1AiiType;
            if (u1SaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_NOTIF_SAII (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1SaiiLen);
                pu1FecMsgPtr += u1SaiiLen;
            }
            LDP_L2VPN_EVT_NOTIF_SAII_LEN (&PwVcEvtInfo) = u1SaiiLen;

            /* Extract TAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1TaiiLen);
            LDP_L2VPN_EVT_NOTIF_TAII_TYPE (&PwVcEvtInfo) = u1AiiType;
            if (u1TaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_NOTIF_TAII (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1TaiiLen);
                pu1FecMsgPtr += u1TaiiLen;
            }
            LDP_L2VPN_EVT_NOTIF_TAII_LEN (&PwVcEvtInfo) = u1TaiiLen;

            /* Check for bad TLV Length performed here */
            if (pu1FecMsgPtr >
                (pu1MsgPtr + LDP_PWVC_FEC_ELEM_HDR_LEN + u1PwVcInfoLen))
            {
                /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                LDP_DBG1 (LDP_PRCS_PRCS,
                          "Notif Msg Snt - BAD TLV LEN - %x \n",
                          LDP_STAT_BAD_TLV_LEN);
                LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                         pMsg, LDP_FALSE);
                return LDP_FAILURE;
            }

            pu1MsgPtr += (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN);
        }

        /* Extract PW Grouping ID TLV */
        if (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
        {
            LDP_GET_2_BYTES (pu1MsgPtr, u2TlvType);
            if ((u2TlvType & LDP_PWVC_GET_TLV_TYPE_MASK) ==
                LDP_PW_GROUPING_ID_TLV)
            {
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

                /* Check for bad TLV Length performed here */
                if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
                {
                    /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                    LdpSsmSendNotifCloseSsn (pLdpSession,
                                             LDP_STAT_TYPE_BAD_TLV_LEN,
                                             pMsg, LDP_FALSE);
                    return LDP_FAILURE;
                }
                LDP_EXT_4_BYTES (pu1MsgPtr,
                                 LDP_L2VPN_EVT_LBL_NOTIF_GRP_ID (&PwVcEvtInfo));
            }
        }
    }
    /* TLV processing Ends here. */
    LdpCopyAddr(&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR(&PwVcEvtInfo)),&PeerAddr.Addr,PeerAddr.u2AddrType);
    LDP_L2VPN_EVT_NOTIF_ADDR_TYPE(&PwVcEvtInfo)=PeerAddr.u2AddrType;

    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_GRP_ID (&PwVcEvtInfo) = u4GroupID;
        LDP_L2VPN_EVT_NOTIF_VC_ID (&PwVcEvtInfo) = u4PwVcID;
    }

    LDP_L2VPN_EVT_LBL_NOTIF_CW (&PwVcEvtInfo) = i1CBit;
    LDP_L2VPN_EVT_NOTIF_VC_TYPE (&PwVcEvtInfo) = (INT1) u2PwVcType;

    LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = L2VPN_LDP_PWVC_NOTIF_MSG;
    LDP_L2VPN_EVT_NOTIF_MSG_CODE (&PwVcEvtInfo) = L2VPN_LDP_NOTIFICATION_MSG;
    LdpL2VpnPostEvent (&PwVcEvtInfo);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpPwVcHandleAddrWdraw                                    */
/* Description   : This function handles the Address Withdraw for a PwVc     */
/*                 FEC and Sends the received Notification Msg Info to the   */
/*                 Application registered.                                   */
/* Input(s)      : pLdpSession - Pointer to the session in which the message */
/*                               is received                                 */
/*                 pMsg        - Pointer to the Message                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
UINT1
LdpPwVcHandleAddrWdraw (tLdpSession * pLdpSession, UINT1 *pMsg)
{
    tLdpPeer           *pLdpPeer = NULL;

    UINT1              *pu1MsgPtr = pMsg;
    UINT1              *pu1FecMsgPtr = NULL;

    tGenU4Addr          PeerAddr;
    UINT4               u4GroupID = LDP_ZERO;
    UINT4               u4PwVcID = LDP_ZERO;

    UINT2               u2MsgLen = LDP_ZERO;
    UINT2               u2TlvType = LDP_ZERO;
    UINT2               u2TlvLen = LDP_ZERO;
    UINT2               u2FecTlvLen = LDP_ZERO;
    UINT2               u2PwVcType = LDP_ZERO;

    UINT1               u1VcTlv = LDP_ZERO;
    UINT1               u1PwVcInfoLen = LDP_ZERO;

    INT1                i1CBit = LDP_ZERO;

    UINT1               u1AgiType = LDP_ZERO;
    UINT1               u1AiiType = LDP_ZERO;
    UINT1               u1AgiLen = LDP_ZERO;
    UINT1               u1SaiiLen = LDP_ZERO;
    UINT1               u1TaiiLen = LDP_ZERO;
    UINT1               u1UnknownTlvFlag = LDP_ZERO;

    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;

    MEMSET(&PeerAddr,LDP_ZERO,sizeof(tGenU4Addr));

    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    pLdpPeer = pLdpSession->pLdpPeer;

    LdpCopyAAddr(&PeerAddr.Addr,&(LDP_PEER_NET_ADDR (pLdpPeer).Addr),
		    LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType);
    PeerAddr.u2AddrType=LDP_PEER_NET_ADDR (pLdpPeer).u2AddrType;


    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Pointer to moved across the message Id field */
    pu1MsgPtr = pu1MsgPtr + LDP_MSGID_LEN;

    /* Extracting FEC param TLV */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
    u2FecTlvLen = u2TlvLen;

    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LDP_DBG1 (LDP_PRCS_PRCS,
                  "Notif Msg Snt - BAD TLV LEN - %x \n", LDP_STAT_BAD_TLV_LEN);
        LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                 pMsg, LDP_FALSE);
        return LDP_FAILURE;
    }

    /* FecMsgPtr points to the contents of Fec */
    pu1FecMsgPtr = pu1MsgPtr;

    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1VcTlv);
    LDP_EXT_2_BYTES (pu1FecMsgPtr, u2PwVcType);

    if (((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) != LDP_PWVC_TYPE_ETHERNET) &&
        ((u2PwVcType & LDP_PWVC_GET_PW_TYPE_MASK) !=
         LDP_PWVC_TYPE_ETHERNET_VLAN))
    {
        LDP_DBG (LDP_PRCS_PRCS, "Received non ethernet type PwVc.\n");
        return LDP_FAILURE;
    }

    LDP_GET_PWVC_C_BIT (u2PwVcType, i1CBit);

    LDP_EXT_1_BYTES (pu1FecMsgPtr, u1PwVcInfoLen);

    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&PwVcEvtInfo) = LDP_PWID_FEC_SIGNALING;

        if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN +
                            LDP_PWVC_FEC_ELEM_GROUP_ID_LEN))
        {
            /* Only a single PW FEC element MUST be advertised per
             * LDP PW label
             * The length mismatch implies there are more FEC Elements
             * or it is a corrupted TLV */
            LDP_DBG (LDP_PRCS_PRCS,
                     "Received FEC Tlv with more than one PWVC FEC Element.\n");
            return LDP_FAILURE;
        }

        LDP_EXT_4_BYTES (pu1FecMsgPtr, u4GroupID);

        if (u1PwVcInfoLen > LDP_ZERO)
        {
            LDP_EXT_4_BYTES (pu1FecMsgPtr, u4PwVcID);
        }

        /* NOTE :
         * Interface Parameters and Optional TLV, label TLV are not
         * extracted.
         * Reason being PwVcID and VcType by itself will identify
         * a particular PwVc.
         */
        pu1MsgPtr += (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN +
                      LDP_PWVC_FEC_ELEM_GROUP_ID_LEN);
    }
    /* FEC Element type is FEC 129 */
    if (u1VcTlv == LDP_FEC_GEN_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&PwVcEvtInfo) = LDP_GEN_FEC_SIGNALING;

        if (u1PwVcInfoLen == LDP_ZERO)
        {
            if (u2FecTlvLen != LDP_PWVC_FEC_ELEM_HDR_LEN)
            {
                /* Only a single PW FEC element MUST be advertised per
                 * LDP PW label
                 * The length mismatch implies there are more FEC Elements
                 * or it is a corrupted TLV */
                LDP_DBG (LDP_PRCS_PRCS,
                         "Received FEC Tlv with more than one Gen FEC Element.\n");
                return LDP_FAILURE;
            }
            pu1MsgPtr += LDP_PWVC_FEC_ELEM_HDR_LEN;
        }
        else
        {
            if (u2FecTlvLen != (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN))
            {
                /* Only a single Gen FEC element MUST be advertised per
                 * LDP PW label
                 * The length mismatch implies there are more FEC Elements
                 * or it is a corrupted TLV */
                LDP_DBG (LDP_PRCS_PRCS,
                         "Received FEC Tlv with more than one Gen FEC Element.\n");
                return LDP_FAILURE;
            }

            /* Extract AGI Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiType);
            LDP_L2VPN_EVT_NOTIF_AGI_TYPE (&PwVcEvtInfo) = u1AgiType;
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AgiLen);
            if (u1AgiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_NOTIF_AGI (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1AgiLen);
                pu1FecMsgPtr += u1AgiLen;
            }
            LDP_L2VPN_EVT_NOTIF_AGI_LEN (&PwVcEvtInfo) = u1AgiLen;

            /* Extract SAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1SaiiLen);
            LDP_L2VPN_EVT_NOTIF_SAII_TYPE (&PwVcEvtInfo) = u1AiiType;
            if (u1SaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_NOTIF_SAII (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1SaiiLen);
                pu1FecMsgPtr += u1SaiiLen;
            }
            LDP_L2VPN_EVT_NOTIF_SAII_LEN (&PwVcEvtInfo) = u1SaiiLen;

            /* Extract TAII Type, Length and Value */
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1AiiType);
            LDP_EXT_1_BYTES (pu1FecMsgPtr, u1TaiiLen);
            LDP_L2VPN_EVT_NOTIF_TAII_TYPE (&PwVcEvtInfo) = u1AiiType;
            if (u1TaiiLen > LDP_ZERO)
            {
                MEMCPY (LDP_L2VPN_EVT_NOTIF_TAII (&PwVcEvtInfo), pu1FecMsgPtr,
                        u1TaiiLen);
                pu1FecMsgPtr += u1TaiiLen;
            }
            LDP_L2VPN_EVT_NOTIF_TAII_LEN (&PwVcEvtInfo) = u1TaiiLen;

            /* Check for bad TLV Length performed here */
            if (pu1FecMsgPtr >
                (pu1MsgPtr + LDP_PWVC_FEC_ELEM_HDR_LEN + u1PwVcInfoLen))
            {
                /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                LDP_DBG1 (LDP_PRCS_PRCS,
                          "Notif Msg Snt - BAD TLV LEN - %x \n",
                          LDP_STAT_BAD_TLV_LEN);
                LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                         pMsg, LDP_FALSE);
                return LDP_FAILURE;
            }

            pu1MsgPtr += (u1PwVcInfoLen + LDP_PWVC_FEC_ELEM_HDR_LEN);
        }

    }

    /* Extract MAC List TLV */
    /* Only Empty MAC List TLV is supported */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

    /* Extract PW Grouping ID TLV */
    while (pu1MsgPtr < (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        /* Check for bad TLV Length performed here */
        if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
        {
            /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
            LdpSsmSendNotifCloseSsn (pLdpSession, LDP_STAT_TYPE_BAD_TLV_LEN,
                                     pMsg, LDP_FALSE);
            return LDP_FAILURE;
        }
        switch (u2TlvType & LDP_PWVC_GET_TLV_TYPE_MASK)
        {
            case LDP_PW_GROUPING_ID_TLV:
                LDP_EXT_4_BYTES (pu1MsgPtr,
                                 LDP_L2VPN_EVT_LBL_NOTIF_GRP_ID (&PwVcEvtInfo));
                break;

            default:
                LDP_GET_STAT_U_BIT (u2TlvType, u1UnknownTlvFlag);
                if (u1UnknownTlvFlag == LDP_ONE)
                {
                    /* U bit =1, then ignore the TLV without sending the notification */
                    pu1MsgPtr += u2TlvLen;
                    break;
                }
                else
                {
                    /* Notification message with
                     * "LDP_STAT_TYPE_UNKNOWN_TLV" sent. */
                    LdpSendNotifMsg (pLdpSession, pMsg, LDP_FALSE,
                                     LDP_STAT_TYPE_UNKNOWN_TLV, NULL);
                    return LDP_FAILURE;
                }
        }
    }
    /* TLV processing Ends here. */
	
    LdpCopyAddr(&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR (&PwVcEvtInfo)),&PeerAddr.Addr,PeerAddr.u2AddrType);
    LDP_L2VPN_EVT_NOTIF_ADDR_TYPE(&PwVcEvtInfo)=PeerAddr.u2AddrType;

    /* FEC Element type is FEC 128 */
    if (u1VcTlv == LDP_FEC_PWVC_TYPE)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_GRP_ID (&PwVcEvtInfo) = u4GroupID;
        LDP_L2VPN_EVT_NOTIF_VC_ID (&PwVcEvtInfo) = u4PwVcID;
    }

    LDP_L2VPN_EVT_LBL_NOTIF_CW (&PwVcEvtInfo) = i1CBit;
    LDP_L2VPN_EVT_NOTIF_VC_TYPE (&PwVcEvtInfo) = (INT1) u2PwVcType;

    LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = L2VPN_LDP_PWVC_ADDR_WDRAW_MSG;
    LdpL2VpnPostEvent (&PwVcEvtInfo);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpL2VpnEventHandler                                      */
/* Description   : This function Handles the Events received from L2VPN and  */
/*                 posts them to LDP Queue                                   */
/* Input(s)      : pMsg     - Pointer to the Message from L2VPN              */
/*                 u4Event  - Event Value                                    */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwif.c */
INT4
LdpL2VpnEventHandler (VOID *pMsg, UINT4 u4Event)
{
    tLdpIfMsg           LdpIfMsg;
    INT1               *pi1IfDescr = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;

    if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Ldp Incarn Admin Status is Down\n");
        return LDP_FAILURE;
    }

    if (LDP_REGISTRATION_TABLE (u2IncarnId, 0).u1RegFlag != LDP_REGISTER)
    {
        return LDP_FAILURE;
    }

    LdpIfMsg.u.AppEvt.pData = (VOID *)
        MemAllocMemBlk (LDP_L2VPN_EVT_INFO_POOL_ID);

    if (LdpIfMsg.u.AppEvt.pData == NULL)
    {
        LDP_DBG (LDP_APP_MEM,
                 "L2VPNAPP: Memory Allocation for L2VPN Event Pool Failed\r\n");
        return LDP_FAILURE;
    }

    MEMCPY (LdpIfMsg.u.AppEvt.pData, pMsg, sizeof (tL2VpnLdpPwVcEvtInfo));
    if (((tL2VpnLdpPwVcEvtInfo *) pMsg)->u4EvtType == L2VPN_LDP_LBL_MSG_EVT)
    {
        if (((tL2VpnLdpPwVcEvtInfo *) pMsg)->unEvtInfo.LblInfo.pi1LocalIfString
            != NULL)
        {
            pi1IfDescr = (INT1 *) MemAllocMemBlk (LDP_L2VPN_IF_STR_POOL_ID);
            if (pi1IfDescr == NULL)
            {
                MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                    LdpIfMsg.u.AppEvt.pData);
                LDP_DBG (LDP_APP_MEM,
                         "L2VPNAPP: Memory Allocation for L2VPN IF Str "
                         "Failed\n");
                return LDP_FAILURE;
            }
            MEMCPY (pi1IfDescr, ((tL2VpnLdpPwVcEvtInfo *) pMsg)->
                    unEvtInfo.LblInfo.pi1LocalIfString, STRLEN
                    (((tL2VpnLdpPwVcEvtInfo *) pMsg)->
                     unEvtInfo.LblInfo.pi1LocalIfString));
            ((tL2VpnLdpPwVcEvtInfo *) LdpIfMsg.u.AppEvt.pData)->
                unEvtInfo.LblInfo.pi1LocalIfString = pi1IfDescr;
        }
    }

    LdpIfMsg.u4MsgType = LDP_APP_EVENT;
    LdpIfMsg.u.AppEvt.u4AppId = LDP_APP_L2VPN;
    LdpIfMsg.u.AppEvt.u4EvtType = u4Event;

    if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
    {
        if (pi1IfDescr != NULL)
        {
            MemReleaseMemBlock (LDP_L2VPN_IF_STR_POOL_ID, (UINT1 *) pi1IfDescr);
        }

        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                            LdpIfMsg.u.AppEvt.pData);

        LDP_DBG (LDP_APP_MEM,
                 "L2VPNAPP: Message posting from L2VPN to LDP failed\r\n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/* PW Redundancy */
/*****************************************************************************/
/* Function Name : LdpL2VpnGetEventInfo                                      */
/* Description   : This function allocates the memory for PW Event Info      */
/* Input(s)      : None                                                      */
/* Output(s)     : ppEvtInfo - Pointer to the Event Info                     */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
INT4
LdpL2VpnGetEventInfo (VOID **ppEvtInfo)
{
    tL2VpnLdpPwVcEvtInfo *pEvtInfo = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;

    if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Ldp Incarn Admin Status is Down\n");
        return LDP_FAILURE;
    }

    if (LDP_REGISTRATION_TABLE (u2IncarnId, 0).u1RegFlag != LDP_REGISTER)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Ldp App is not registered\n");
        return LDP_FAILURE;
    }

    pEvtInfo = (VOID *) MemAllocMemBlk (LDP_L2VPN_EVT_INFO_POOL_ID);
    if (pEvtInfo == NULL)
    {
        LDP_DBG (LDP_APP_MEM,
                 "PRCS: Mem Alloc from Msg MemPool Failed for tL2VpnLdpPwVcEvtInfo\n");
        return LDP_FAILURE;
    }
    MEMSET (pEvtInfo, 0, sizeof (*pEvtInfo));

    *(tL2VpnLdpPwVcEvtInfo **) ppEvtInfo = pEvtInfo;

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpL2VpnCleanEventInfo                                    */
/* Description   : This function releaese the memory for PW Event Info and   */
/*                 remove the nodes added in ICCP data list and Request list */
/*                 and releases the memory                                   */
/* Input(s)      : pvEvtInfo - pointer to the PW Event info to be released   */
/*                 b1CleanEvent - if TRUE pvEvtInfo will be released         */
/*                 b1CleanNodes - if TRUE nodes will be removed and memory   */
/*                                will be released                           */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
INT4
LdpL2VpnCleanEventInfo (VOID *pvEvtInfo, BOOL1 b1CleanEvent, BOOL1 b1CleanNodes)
{
    VOID               *pSllNode = NULL;
    VOID               *pTmpSllNode = NULL;
    tPwRedDataEvtPwData *pPwData = NULL;
    tPwRedDataEvtPwFec *pPwFec = NULL;
    tL2VpnLdpPwVcEvtInfo *pEvtInfo = pvEvtInfo;

    if (b1CleanNodes)
    {
        TMO_DYN_SLL_Scan (&pEvtInfo->unEvtInfo.IccpInfo.u.PwData.DataList,
                          pSllNode, pTmpSllNode, tPwRedDataEvtPwData *)
        {
            pPwData = CONTAINER_OF (pSllNode, tPwRedDataEvtPwData, ListNode);
            TMO_SLL_Delete (&pEvtInfo->unEvtInfo.IccpInfo.u.PwData.DataList,
                            (tTMO_SLL_NODE *) pPwData);
            LdpL2VpnCleanEventPwData (pPwData);
        }

        TMO_DYN_SLL_Scan (&pEvtInfo->unEvtInfo.IccpInfo.u.PwData.RequestList,
                          pSllNode, pTmpSllNode, tPwRedDataEvtPwFec *)
        {
            pPwFec = CONTAINER_OF (pSllNode, tPwRedDataEvtPwFec, ListNode);
            TMO_SLL_Delete (&pEvtInfo->unEvtInfo.IccpInfo.u.PwData.RequestList,
                            (tTMO_SLL_NODE *) pPwFec);
            LdpL2VpnCleanEventPwFec (pPwFec);
        }
    }

    if (b1CleanEvent)
    {
        MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID, pvEvtInfo);
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpL2VpnSendEventInfo                                     */
/* Description   : This function posts the message to the LDP queue and send */
/*                 an event to the LDP task                                  */
/* Input(s)      : pvEvtInfo - pointer to the PW Event info                  */
/*                 u4Event   - Event type                                    */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
INT4
LdpL2VpnSendEventInfo (VOID *pEvtInfo, UINT4 u4Event)
{
    tLdpIfMsg           LdpIfMsg;

    MEMSET (&LdpIfMsg, 0, sizeof (LdpIfMsg));

    LdpIfMsg.u4MsgType = LDP_APP_EVENT;
    LdpIfMsg.u.AppEvt.u4AppId = LDP_APP_L2VPN;
    LdpIfMsg.u.AppEvt.u4EvtType = u4Event;
    LdpIfMsg.u.AppEvt.pData = pEvtInfo;

    if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
    {
        LDP_DBG (LDP_IF_PRCS,
                 "IF: Failed to EnQ Application event to LDP Task\n");
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpL2VpnGetEventPwFec                                     */
/* Description   : This function allocates the Event PW Fec from LDP pool    */
/* Input(s)      : None                                                      */
/* Output(s)     : ppEvtPwFec - Pointer to the Event PW Fec                  */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
INT4
LdpL2VpnGetEventPwFec (VOID **ppEvtPwFec)
{
    tPwRedDataEvtPwFec *pEvtPwFec = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;

    if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Ldp Incarn Admin Status is Down\n");
        return LDP_FAILURE;
    }

    if (LDP_REGISTRATION_TABLE (u2IncarnId, 0).u1RegFlag != LDP_REGISTER)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Ldp App is not registered\n");
        return LDP_FAILURE;
    }

    pEvtPwFec = (VOID *) MemAllocMemBlk (LDP_ICCP_PWVC_REQUESTS_POOL_ID);
    if (pEvtPwFec == NULL)
    {
        LDP_DBG (LDP_APP_MEM,
                 "PRCS: Mem Alloc from Msg MemPool Failed for tPwRedDataEvtPwFec\n");
        return LDP_FAILURE;
    }
    MEMSET (pEvtPwFec, 0, sizeof (*pEvtPwFec));

    *(tPwRedDataEvtPwFec **) ppEvtPwFec = pEvtPwFec;
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpL2VpnCleanEventPwFec                                   */
/* Description   : This function releaese the memory allocated for Pw FEC    */
/* Input(s)      : pvEvtPwData - pointer to the PW Data to be released       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
INT4
LdpL2VpnCleanEventPwFec (VOID *pEvtPwFec)
{
    MemReleaseMemBlock (LDP_ICCP_PWVC_REQUESTS_POOL_ID, pEvtPwFec);

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpL2VpnGetEventPwData                                    */
/* Description   : This function allocates the Event PW Data from LDP pool   */
/* Input(s)      : None                                                      */
/* Output(s)     : ppEvtPwData - Pointer to the Event PW Data                */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
INT4
LdpL2VpnGetEventPwData (VOID **ppEvtPwData)
{
    tPwRedDataEvtPwData *pEvtPwData = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;

    if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Ldp Incarn Admin Status is Down\n");
        return LDP_FAILURE;
    }

    if (LDP_REGISTRATION_TABLE (u2IncarnId, 0).u1RegFlag != LDP_REGISTER)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Ldp App is not registered\n");
        return LDP_FAILURE;
    }

    pEvtPwData = (VOID *) MemAllocMemBlk (LDP_ICCP_PWVC_DATA_ENTRIES_POOL_ID);
    if (pEvtPwData == NULL)
    {
        LDP_DBG (LDP_APP_MEM,
                 "PRCS: Mem Alloc from Msg MemPool Failed for tPwRedDataEvtPwData\n");
        return LDP_FAILURE;
    }
    MEMSET (pEvtPwData, 0, sizeof (*pEvtPwData));

    *(tPwRedDataEvtPwData **) ppEvtPwData = pEvtPwData;
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpL2VpnCleanEventPwData                                  */
/* Description   : This function releaese the memory allocated for Pw Data   */
/* Input(s)      : pvEvtPwData - pointer to the PW Data to be released       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
INT4
LdpL2VpnCleanEventPwData (VOID *pEvtPwData)
{
    MemReleaseMemBlock (LDP_ICCP_PWVC_DATA_ENTRIES_POOL_ID, pEvtPwData);
    return LDP_SUCCESS;
}
