/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*  $Id: ldpdudsm.c,v 1.30 2017/06/13 13:19:02 siva Exp $
*                                                                   *
* *******************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*-----------------------------------------------------------------------------
*    FILE  NAME             : ldpdudsm.c
*    PRINCIPAL AUTHOR       : Aricent Inc. 
*    SUBSYSTEM NAME         : MPLS   
*    MODULE NAME            : LDP (ADVT SUB-MODULE)
*    LANGUAGE               : ANSI-C
*    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
*    DATE OF FIRST RELEASE  :
*    DESCRIPTION            : This file contains DU Downstream state
*                             machine routines.
*----------------------------------------------------------------------------*/
#include "ldpincs.h"
#include "ldpdudsm.h"
#include "rtm.h"

/*****************************************************************************/
/* Function Name : LdpDuDnSm                                                 */
/* Description   : This Function is called by other internal modules to      */
/*                 perform the corresponding operations required by the      */
/*                 DU downstream state machine                               */
/* Input(s)      : pLspCtrlBlock  - Pointer to the LSP ctrl block structure  */
/*                 pMsg           - Pointer to Message strcuture             */
/*                 u1Event        - The event to be passed to state machine  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuDnSm (tLspCtrlBlock * pLspCtrlBlock, UINT1 u1Event, tLdpMsgInfo * pMsg)
{
    LDP_DBG2 (LDP_ADVT_SEM,
              "Downstream Control Block Current State: %s Evt %s \n",
              au1LdpDuDnStates[pLspCtrlBlock->u1LspState],
              au1LdpDuDnEvents[u1Event]);

    LDP_DU_DN_FSM[LCB_STATE (pLspCtrlBlock)][u1Event] (pLspCtrlBlock, pMsg);

    LDP_DBG1 (LDP_ADVT_SEM,
              "Downstream Control Block Next  State: %s \n",
              au1LdpDuDnStates[pLspCtrlBlock->u1LspState]);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuDnIdLdpMap                                           */
/* Description   : This routine is called when DU DN state machine is in     */
/*                 IDLE state and LDP Mapping Event received.                */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuDnIdLdpMap (tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT1               u1Event;
    UINT1               u1SsnState;
    UINT1               u1HopCount = 0;
    UINT2               u2PvCount = 0;
    tLdpSession        *pLdpSession = NULL;
    tTMO_SLL           *pEntityList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pTempLspUpCtrlBlock = NULL;
    tGenU4Addr          NextHopAddr;
    UINT4               u4RtPort = 0;
    UINT4               u4DestMask = 0;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    UINT1               u1UpStrAlrPre = LDP_FALSE;
    UINT2               u2RtPort = LDP_ZERO;
    UINT4               u4RtGw = LDP_ZERO;
    tFtnEntry          *pFtnEntry = NULL;
    tRtInfo            *pRt = NULL;
    tRtInfo             InRtInfo;
    UINT4               u4FsIpRouteStatus = 0;
#ifdef LDP_GR_WANTED
    tFTNHwListEntry    *pFTNHwListEntry = NULL;
    tMplsIpAddress      FtnHwListFec;
    tMplsIpAddress      NextHopIp;
    UINT1               u1FtnHwListPrefLen = 0;
#endif
    UINT4               u4L3VlanIf = 0;
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            Ipv6RtGw;
#endif

    MEMSET (&InRtInfo, 0, sizeof (tRtInfo));
#ifdef LDP_GR_WANTED
    MEMSET (&FtnHwListFec, 0, sizeof (tMplsIpAddress));
    MEMSET (&NextHopIp, 0, sizeof (tMplsIpAddress));
#endif
    LDP_GET_MASK_FROM_PRFX_LEN ((LCB_FEC (pLspCtrlBlock).u1PreLen), u4DestMask);
    u2IncarnId = SSN_GET_INCRN_ID (LCB_DSSN (pLspCtrlBlock));

    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED
    MEMSET (&Ipv6RtGw, LDP_ZERO, sizeof (tIp6Addr));
#endif

#ifdef MPLS_IPV6_WANTED
    if (LCB_FEC (pLspCtrlBlock).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        if (LdpIsLsrPartOfIpv6ErHopOrFec
            (&LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr,
             LCB_FEC (pLspCtrlBlock).u1PreLen) == LDP_TRUE)
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Map Recd for Local Addr-LdpDuDnIdLdpMap\n");
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));

            if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) == 0)
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                       MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
            }

            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }
    }
    else
#endif
    {

        LDP_GET_MASK_FROM_PRFX_LEN ((LCB_FEC (pLspCtrlBlock).u1PreLen),
                                    u4DestMask);
        /* send rel msg if recd map msg's fec is one of the local addr */
        if (LdpIsLsrPartOfIpv4ErHopOrFec
            (LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix),
             u4DestMask) == LDP_TRUE)
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Map Recd for Local Addr-LdpDuDnIdLdpMap\n");
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));
            if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) == 0)
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                       MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }

    }
    /* if loop found, send lbl rel msg & del ctrl blk */
    if (LdpIsLoopFound (LDP_MSG_HC_PTR (pLdpMsgInfo),
                        LDP_MSG_PV_PTR (pLdpMsgInfo),
                        (&u2PvCount), (&u1HopCount), u2IncarnId,
                        LCB_DSSN (pLspCtrlBlock), FALSE) == LDP_TRUE)
    {
        /* Label release message has to be sent with Status TLV (Loop detected status
         * code)
         * */
        pLspCtrlBlock->pDStrSession->u1StatusCode = LDP_STAT_TYPE_LOOP_DETECTED;

        LDP_DBG (LDP_ADVT_PRCS, "ADVT: Loop detected - LdpDuDnIdLdpMap\n");
        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock), &LCB_DLBL (pLspCtrlBlock),
                          LCB_DSSN (pLspCtrlBlock));
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }

#ifdef MPLS_IPV6_WANTED
    if (LCB_FEC (pLspCtrlBlock).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        if (LdpIpv6GetExactRoute
            (&LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr,
             LCB_FEC (pLspCtrlBlock).u1PreLen, &u4RtPort,
             &NextHopAddr.Addr.Ip6Addr) == LDP_IP_FAILURE)
        {

            LDP_DBG (LDP_ADVT_PRCS, "ADVT: IP Get Exact Route Not Available\n");
            /*  In LIBERAL retain the mappings and release in case of
             *  CONSERVATIVE */
        }
        NextHopAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
    }
    else
#endif
    {
        if (LdpIpGetExactRoute
            (LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix), u4DestMask,
             &u4RtPort, &LDP_IPV4_U4_ADDR (NextHopAddr.Addr)) == LDP_IP_FAILURE)
        {
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: IP Get Exact Route Not Available\n");
            /*  In LIBERAL retain the mappings and release in case of 
             *  CONSERVATIVE */
        }
        NextHopAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
    }

    InRtInfo.u4DestNet = LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix);
    InRtInfo.u4DestMask = u4DestMask;
    RtmApiGetBestRouteEntryInCxt (u2IncarnId, InRtInfo, &pRt);
    if (pRt != NULL)
    {
        u4FsIpRouteStatus = pRt->u4Flag;
    }

    LCB_HCOUNT (pLspCtrlBlock) = u1HopCount;
    pLspCtrlBlock->pu1PVTlv = LDP_MSG_PV_PTR (pLdpMsgInfo);
    pLspCtrlBlock->u1PVCount = (UINT1) u2PvCount;

    if (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock)))
        == LDP_TRUE)
    {
        if (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->OutStackTnlInfo.
            u4TnlId == LDP_ZERO)
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "Out tunnel is not associated with this targeted session, "
                     "Lbl Map dropped\n");
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }
#ifdef MPLS_IPV6_WANTED
        if (LCB_FEC (pLspCtrlBlock).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
        {
            MEMCPY (LDP_IPV6_U4_ADDR (pLspCtrlBlock->NextHopAddr),
                    &LCB_DSSN (pLspCtrlBlock)->pLdpPeer->TransAddr.Addr.Ip6Addr,
                    LDP_IPV6ADR_LEN);

            pLspCtrlBlock->u2AddrType = LDP_ADDR_TYPE_IPV6;

            LDP_DBG2 (LDP_SSN_PRCS,
                      "Next hop for FEC %s given by IP is %s\n",
                      Ip6PrintAddr (&LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr),
                      Ip6PrintAddr (&NextHopAddr.Addr.Ip6Addr));

            /* Route nexthop and the targeted session's transport address should be in the same network, 
             * otherwise do the following actions */
            if (LdpIpUcastIpv6RouteQuery
                (&pLspCtrlBlock->NextHopAddr.Ip6Addr, &u4RtPort,
                 &Ipv6RtGw) == LDP_IP_SUCCESS)
            {
                LDP_DBG2 (LDP_SSN_PRCS,
                          "Next hop for PEER TRANS ADDR %s is %s\n",
                          Ip6PrintAddr (&pLspCtrlBlock->NextHopAddr.Ip6Addr),
                          Ip6PrintAddr (&Ipv6RtGw));

                if ((LdpIsAddressMatchWithAddrType
                     (&NextHopAddr.Addr, &LCB_FEC (pLspCtrlBlock).Prefix,
                      NextHopAddr.u2AddrType,
                      LCB_FEC (pLspCtrlBlock).u2AddrFmly) == LDP_TRUE)
                    ||
                    (MEMCMP
                     (&Ipv6RtGw, &NextHopAddr.Addr.Ip6Addr,
                      LDP_IPV6ADR_LEN) != LDP_ZERO))
                {
                    if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->
                         u2LabelRet) == LDP_CONSERVATIVE_MODE)
                    {
                        /*  In CONSERVATIVE release the mappings 
                         *  If route is not found or session for next hop is not
                         *  present.
                         */
                        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                          &LCB_DLBL (pLspCtrlBlock),
                                          LCB_DSSN (pLspCtrlBlock));
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    }
                    else
                    {
                        LDP_DBG3 (LDP_SSN_PRCS,
                                  "Route Issue for FEC %s Nexthop %s OutL3If %d in "
                                  "Idle Lbl Map Rcvd\n",
                                  Ip6PrintAddr (&LCB_FEC (pLspCtrlBlock).Prefix.
                                                Ip6Addr),
                                  Ip6PrintAddr (&NextHopAddr.Addr.Ip6Addr),
                                  pLspCtrlBlock->u4OutIfIndex);

                        /*  In LIBERAL retain the mappings */
                        LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
                        /* MLIB update will be done after the prefix route 
                         * notification received from the IP module */
                        LCB_MLIB_UPD_STATUS (pLspCtrlBlock) =
                            LDP_DU_DN_MLIB_UPD_NOT_DONE;
                    }
                    return;
                }
            }
            else
            {
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: Loop detected - LdpDuDnIdLdpMap\n");
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return;
            }
        }
        else
#endif
        {
            SSN_GET_PEER_TRANSADDR (LDP_IPV4_U4_ADDR
                                    (pLspCtrlBlock->NextHopAddr),
                                    LCB_DSSN (pLspCtrlBlock));
            pLspCtrlBlock->u2AddrType = LDP_ADDR_TYPE_IPV4;

            LDP_DBG2 (LDP_SSN_PRCS,
                      "Next hop for FEC %x given by IP is %x\n",
                      LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix),
                      LDP_IPV4_U4_ADDR (NextHopAddr.Addr));

            /* Route nexthop and the targeted session's transport address should be in the same network, 
             * otherwise do the following actions */
            if (LdpIpUcastRouteQuery
                (LDP_IPV4_U4_ADDR (pLspCtrlBlock->NextHopAddr), &u2RtPort,
                 &u4RtGw) == LDP_IP_SUCCESS)
            {
                LDP_DBG2 (LDP_SSN_PRCS,
                          "Next hop for PEER TRANS ADDR %x is %x\n",
                          LDP_IPV4_U4_ADDR (pLspCtrlBlock->NextHopAddr),
                          u4RtGw);

                if ((LdpIsAddressMatchWithAddrType
                     (&NextHopAddr.Addr, &LCB_FEC (pLspCtrlBlock).Prefix,
                      NextHopAddr.u2AddrType,
                      LCB_FEC (pLspCtrlBlock).u2AddrFmly) == LDP_TRUE)
                    || (u4RtGw != LDP_IPV4_U4_ADDR (NextHopAddr.Addr)))

                {
                    if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->
                         u2LabelRet) == LDP_CONSERVATIVE_MODE)
                    {
                        /*  In CONSERVATIVE release the mappings 
                         *  If route is not found or session for next hop is not
                         *  present.
                         */
                        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                          &LCB_DLBL (pLspCtrlBlock),
                                          LCB_DSSN (pLspCtrlBlock));
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    }
                    else
                    {
                        LDP_DBG3 (LDP_SSN_PRCS,
                                  "Route Issue for FEC %x Nexthop %x OutL3If %d in "
                                  "Idle Lbl Map Rcvd\n",
                                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                    Prefix),
                                  LDP_IPV4_U4_ADDR (NextHopAddr.Addr),
                                  pLspCtrlBlock->u4OutIfIndex);

                        /*  In LIBERAL retain the mappings */
                        LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
                        /* MLIB update will be done after the prefix route 
                         * notification received from the IP module */
                        LCB_MLIB_UPD_STATUS (pLspCtrlBlock) =
                            LDP_DU_DN_MLIB_UPD_NOT_DONE;
                    }
                    return;
                }
            }
            else
            {
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: Loop detected - LdpDuDnIdLdpMap\n");
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return;
            }

        }
    }
    else
    {

        if (CfaUtilGetIfIndexFromMplsTnlIf
            (pLspCtrlBlock->u4OutIfIndex, &u4L3VlanIf, TRUE) != CFA_SUCCESS)
        {
            printf
                ("%s : %d CfaUtilGetIfIndexFromMplsTnlIf() Failed pLspCtrlBlock->u4OutIfIndex=%d u4L3VlanIf=%d\n",
                 __FUNCTION__, __LINE__, pLspCtrlBlock->u4OutIfIndex,
                 u4L3VlanIf);
        }

        if ((gi4MplsSimulateFailure != LDP_SIM_FAILURE_SAME_PREFIX_AND_NETWORK)
            &&
            (((LdpIsAddressMatchWithAddrType
               (&NextHopAddr.Addr, &LCB_FEC (pLspCtrlBlock).Prefix,
                NextHopAddr.u2AddrType,
                LCB_FEC (pLspCtrlBlock).u2AddrFmly) == LDP_TRUE))
             ||
             (LdpIsSsnForFec
              (&NextHopAddr, u4L3VlanIf,
               LCB_DSSN (pLspCtrlBlock)) == LDP_FALSE)))
        {
            if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->u2LabelRet) ==
                LDP_CONSERVATIVE_MODE)
            {
                /*  In CONSERVATIVE release the mappings 
                 *  If route is not found or session for next hop is not
                 *  present.
                 */
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return;
            }
            else
            {

                if (((u4FsIpRouteStatus & RTM_ECMP_RT) == RTM_ECMP_RT)
                    && (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) !=
                        LDP_DU_DN_MLIB_UPD_NOT_DONE))
                {
                    LDP_DBG3 (LDP_SSN_PRCS,
                              "ECMP Route for FEC %x Nexthop %x MLIB_UPD_STATUS %d "
                              "in Idle LblMap Rcvd\n",
                              LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix),
                              LDP_IPV4_U4_ADDR (NextHopAddr.Addr),
                              LCB_MLIB_UPD_STATUS (pLspCtrlBlock));
                }
                else
                {

                    /*  In LIBERAL retain the mappings */
                    LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
                    /* MLIB update will be done after the prefix route 
                     * notification received from the IP module */
                    LCB_MLIB_UPD_STATUS (pLspCtrlBlock) =
                        LDP_DU_DN_MLIB_UPD_NOT_DONE;
#ifdef MPLS_IPV6_WANTED
                    if (LCB_FEC (pLspCtrlBlock).u2AddrFmly ==
                        LDP_ADDR_TYPE_IPV6)
                    {
                        LDP_DBG3 (LDP_SSN_PRCS,
                                  "Route issue for FEC %s Nexthop %s OutL3If %d "
                                  "in Idle LblMap Rcvd\n",
                                  Ip6PrintAddr (&LCB_FEC (pLspCtrlBlock).Prefix.
                                                Ip6Addr),
                                  Ip6PrintAddr (&NextHopAddr.Addr.Ip6Addr),
                                  pLspCtrlBlock->u4OutIfIndex);
                    }
                    else
#endif
                    {
                        LDP_DBG3 (LDP_SSN_PRCS,
                                  "Route issue for FEC %x Nexthop %x OutL3If %d "
                                  "in Idle LblMap Rcvd\n",
                                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                    Prefix),
                                  LDP_IPV4_U4_ADDR (NextHopAddr.Addr),
                                  pLspCtrlBlock->u4OutIfIndex);
                    }
                    return;
                }
            }
        }
        /** We have recieved the Label Map message, As per RFC we have to program 
          the label in h/w if it came from best path. Now there can be scenario
          Topology : A---B
          |
          |
          C
          FEC X is rechable via B.
          FTN via B is already programmed in hardware, Now route gets deleted and 
          rechable via C.This NextHop handling is in LDP array, Now the Label map comes 
          and best path is via C, so it tries to add the FTN in harware, but it will fail 
          as FTN is already programmed via B.So we need a check here if any FTN is programmed 
          in h/w, let it clean up first.This clean up occurs via NextHop change event and 
          here we save the Label in control plane only**/
       /** This check also take cares of ECMP scenarios **/

        switch (LCB_FEC (pLspCtrlBlock).u2AddrFmly)
        {
#ifdef MPLS_IPV6_WANTED
            case IPV6:
                pFtnEntry =
                    MplsSignalGetFtnIpv6TableEntry (&LCB_FEC (pLspCtrlBlock).
                                                    Prefix.Ip6Addr);
                break;
#endif
            default:
                pFtnEntry =
                    MplsSignalGetFtnTableEntry (LDP_IPV4_U4_ADDR
                                                (LCB_FEC (pLspCtrlBlock).
                                                 Prefix));
                break;
        };

        if (pFtnEntry != NULL)
        {
            LDP_DBG2 (LDP_DBG_PRCS,
                      "%s : %d FTN is already programmed, Ignoring Label Map\n",
                      __FUNCTION__, __LINE__);

            /*  In LIBERAL retain the mappings */
            LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
            /* MLIB update will be done after the prefix route
             * notification received from the IP module 
             */
            LCB_MLIB_UPD_STATUS (pLspCtrlBlock) = LDP_DU_DN_MLIB_UPD_NOT_DONE;
            LDP_DBG (LDP_SSN_PRCS,
                     "Route issue for FEC" "in Idle LblMap Rcvd\n");

            return;
        }
        LdpCopyAddr (&pLspCtrlBlock->NextHopAddr, &NextHopAddr.Addr,
                     NextHopAddr.u2AddrType);

        pLspCtrlBlock->u2AddrType = NextHopAddr.u2AddrType;

#ifdef LDP_GR_WANTED
        if ((gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability ==
             LDP_GR_CAPABILITY_FULL) &&
            (LCB_DSSN (pLspCtrlBlock)->pLdpPeer->u1GrProgressState !=
             LDP_PEER_GR_NOT_SUPPORTED))
        {
            if (LCB_DSSN (pLspCtrlBlock)->pLdpPeer->u1GrProgressState ==
                LDP_PEER_GR_RECOVERY_IN_PROGRESS)
            {
                if (LdpIsSsnForFec (&(NextHopAddr), u4L3VlanIf,
                                    LCB_DSSN (pLspCtrlBlock)) == LDP_TRUE)
                {
                    /* Get the FTN hw List Entry from the Hw List */
                    FtnHwListFec.i4IpAddrType = pLspCtrlBlock->u2AddrType;
#ifdef MPLS_IPV6_WANTED
                    if (pLspCtrlBlock->u2AddrType == LDP_ADDR_TYPE_IPV6)
                    {
                        MEMCPY (&FtnHwListFec.IpAddress.Ip6Addr,
                                &(LDP_IPV6_U4_ADDR
                                  (LCB_FEC (pLspCtrlBlock).Prefix)),
                                IPV6_ADDR_LENGTH);
                    }
                    else
#endif
                    {
                        MEMCPY (FtnHwListFec.IpAddress.au1Ipv4Addr,
                                &(LDP_IPV4_U4_ADDR
                                  (LCB_FEC (pLspCtrlBlock).Prefix)),
                                LDP_IPV4ADR_LEN);
                    }
                    u1FtnHwListPrefLen = LCB_FEC (pLspCtrlBlock).u1PreLen;

                    pFTNHwListEntry = MplsHwListGetFTNHwListEntry (FtnHwListFec,
                                                                   u1FtnHwListPrefLen);
                    if ((pFTNHwListEntry != NULL)
                        && (MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry) ==
                            MPLS_TRUE))
                    {
                        LDP_DBG2 (GRACEFUL_DEBUG,
                                  "LdpDuDnIdLdpMap: FTN Hw List Entry found for FEC: %x, PrefixLen:%d\n",
                                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                    Prefix),
                                  u1FtnHwListPrefLen);

                        /* Validate the Hw List entry with against the Label and next hop */
                        NextHopIp.i4IpAddrType = pLspCtrlBlock->u2AddrType;

#ifdef MPLS_IPV6_WANTED
                        if (NextHopIp.i4IpAddrType == LDP_ADDR_TYPE_IPV6)
                        {
                            MEMCPY (&NextHopIp.IpAddress.Ip6Addr,
                                    &(LDP_IPV6_U4_ADDR
                                      (pLspCtrlBlock->NextHopAddr)),
                                    LDP_IPV6ADR_LEN);
                        }
                        else
#endif
                        {
                            /* Adddress Type: Take from the Control Block */
                            MEMCPY (NextHopIp.IpAddress.au1Ipv4Addr,
                                    &(LDP_IPV4_U4_ADDR
                                      (pLspCtrlBlock->NextHopAddr)),
                                    LDP_IPV4ADR_LEN);
                        }
                        if (MplsHwListValidateFTNHwListEntry (pFTNHwListEntry,
                                                              LCB_DLBL
                                                              (pLspCtrlBlock),
                                                              NextHopIp) ==
                            MPLS_TRUE)
                        {
                            /* Delete the Tunnel interface allocated from CFA */
                            if (CfaUtilGetIfIndexFromMplsTnlIf
                                (pLspCtrlBlock->u4OutIfIndex, &u4L3VlanIf,
                                 TRUE) == CFA_SUCCESS)
                            {
                                if (CfaIfmDeleteStackMplsTunnelInterface
                                    (u4L3VlanIf,
                                     pLspCtrlBlock->u4OutIfIndex) ==
                                    CFA_FAILURE)
                                {
                                    LDP_DBG3 (GRACEFUL_DEBUG,
                                              "LdpDuDnIdLdpMap: Error in Deleting the CFA Tunnel Interface = %d "
                                              "stacked over the L3 interface = %d for FEC= %x\n",
                                              pLspCtrlBlock->u4OutIfIndex,
                                              u4L3VlanIf,
                                              LDP_IPV4_U4_ADDR (LCB_FEC
                                                                (pLspCtrlBlock).
                                                                Prefix));

                                    LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                                      &LCB_DLBL (pLspCtrlBlock),
                                                      LCB_DSSN (pLspCtrlBlock));
                                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                                    return;
                                }
                            }

                            LDP_DBG2 (GRACEFUL_DEBUG,
                                      "LdpDuDnIdLdpMap: FTN Hw List Entry Validated for FEC: %x, PrefixLen:%d\n",
                                      LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                        Prefix),
                                      u1FtnHwListPrefLen);
                            /* Re-use the Tunnel-interface, from the existing Hw List Entry */
                            pLspCtrlBlock->u4OutIfIndex =
                                MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry);
                        }
                        else
                        {
                            LDP_DBG2 (GRACEFUL_DEBUG,
                                      "LdpDuDnIdLdpMap: FTN Hw List Entry Validation FAILED for FEC: %x, PrefixLen:%d\n",
                                      LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                        Prefix),
                                      u1FtnHwListPrefLen);
                        }
                    }

                }

            }
        }
#endif

    }

#ifdef MPLS_IPV6_WANTED
    if (LCB_FEC (pLspCtrlBlock).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        LDP_DBG3 (LDP_SSN_PRCS,
                  "Idle LblMap rcvd Prefix %x NextHop %x OutL3If %d\n",
                  Ip6PrintAddr (&LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr),
                  Ip6PrintAddr (&NextHopAddr.Addr.Ip6Addr),
                  pLspCtrlBlock->u4OutIfIndex);
    }
    else
#endif
    {
        LDP_DBG3 (LDP_SSN_PRCS,
                  "Idle LblMap rcvd Prefix %x NextHop %x OutL3If %d\n",
                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix),
                  LDP_IPV4_U4_ADDR (NextHopAddr.Addr),
                  pLspCtrlBlock->u4OutIfIndex);
    }
    /* create ftn entry */
    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE, MPLS_OPR_PUSH, pLspCtrlBlock,
                           NULL);
    LDP_MSG_PV_COUNT (pLdpMsgInfo) = (UINT1) u2PvCount;
    LDP_MSG_HC (pLdpMsgInfo) = u1HopCount;

    pEntityList = &LDP_ENTITY_LIST (u2IncarnId);

    /* When reroute happens the downstream control block will
     * be in idle state and already existing DOD Merge upstream 
     * control blocks should be given Map events */

    /* In DU since there are no NAK events the Notif handling
     * for this Next Hop change request is not handled. */

    TMO_DYN_SLL_Scan (&LCB_UPSTR_LIST (pLspCtrlBlock), pSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);

        if (UPSTR_USSN (pLspUpCtrlBlock) != NULL)
        {
            if ((SSN_ADVTYPE (UPSTR_USSN (pLspUpCtrlBlock)) ==
                 LDP_DSTR_ON_DEMAND)
                && ((SSN_MRGTYPE (UPSTR_USSN (pLspUpCtrlBlock)) == LDP_VC_MRG)
                    || (SSN_MRGTYPE (UPSTR_USSN (pLspUpCtrlBlock)) ==
                        LDP_VP_MRG)))
            {
                u1Event = LDP_DU_UP_LSM_EVT_INT_DN_MAP;

                LdpInvCorrStMh (UPSTR_USSN (pLspUpCtrlBlock), pLspUpCtrlBlock,
                                u1Event, pLdpMsgInfo, LDP_TRUE);
            }
        }
    }

    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
        {
            if ((pLdpPeer->pLdpSession) == NULL)
            {
                continue;
            }
            /* Target sessions also we need to 
             * consider for LDP over RSVP case */
            pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

            if ((LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
                 == LDP_TRUE) &&
                (SSN_GET_ENTITY (pLdpSession)->OutStackTnlInfo.u4TnlId ==
                 LDP_ZERO))
            {
                LDP_DBG (LDP_ADVT_PRCS,
                         "\n LdpDuDnIdLdpMap: Labels can't be negotiated when out-tunnel is not "
                         "associated with LDP targeted session\n");
                continue;
            }

            /* if dn ctl block exists chk if the dn ssn of dn ctl 
             * block same as curr ssn */

            if (pLdpSession != LCB_DSSN (pLspCtrlBlock))
            {
                SSN_GET_SSN_STATE (pLdpSession, u1SsnState);
                if ((u1SsnState == LDP_SSM_ST_OPERATIONAL) &&
                    (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT))
                {
                    /* chk if already an up ctrl blk exists for the same fec 
                       and if yes, assign the dn ctrl blk ptr and invoke up
                       ctrl blk s/m */
                    u1UpStrAlrPre = LDP_FALSE;
                    TMO_SLL_Scan (&SSN_ULCB (pLdpSession),
                                  pTempLspUpCtrlBlock, tUstrLspCtrlBlock *)
                    {
                        if (LdpCompareFec (&UPSTR_FEC (pTempLspUpCtrlBlock),
                                           &LCB_FEC (pLspCtrlBlock)) ==
                            LDP_EQUAL)
                        {
                            LDP_DBG (LDP_SSN_PRCS,
                                     "IDLE LMM Estb Upstream exits \n");

                            u1UpStrAlrPre = LDP_TRUE;
                            if (UPSTR_DSTR_PTR (pTempLspUpCtrlBlock) != NULL)
                            {
                                LDP_DBG (LDP_SSN_PRCS,
                                         "IDLE LMM Upstream already "
                                         "associated with Downstream\n");
                                /* Check whether this downstream is actually 
                                 * valid for the upstream LCB */

                                if (LdpIsSsnForFec
                                    (&NextHopAddr, u4RtPort,
                                     LCB_DSSN (UPSTR_DSTR_PTR
                                               (pTempLspUpCtrlBlock)))
                                    == LDP_TRUE)
                                {
                                    LDP_DBG (LDP_SSN_PRCS,
                                             "IDLE LMM Upstream already "
                                             "associated with the correct Downstream\n");
                                    break;
                                }
                                if (UPSTR_USSN (pTempLspUpCtrlBlock) ==
                                    LCB_DSSN (pLspCtrlBlock))
                                {
                                    LDP_DBG (LDP_SSN_PRCS,
                                             "IDLE LMM downstream and upstream in the "
                                             "same session skipped \n");
                                    break;
                                }
                                LdpMlibDeleteForEstbLsps (UPSTR_DSTR_PTR
                                                          (pTempLspUpCtrlBlock));

                                /* create ftn entry */
                                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE,
                                                       MPLS_OPR_PUSH,
                                                       pLspCtrlBlock, NULL);

                                TMO_SLL_Delete (&LCB_UPSTR_LIST (UPSTR_DSTR_PTR
                                                                 (pTempLspUpCtrlBlock)),
                                                UPCB_TO_SLL
                                                (pTempLspUpCtrlBlock));
                            }
                            UPSTR_DSTR_PTR (pTempLspUpCtrlBlock) =
                                pLspCtrlBlock;

                            TMO_SLL_Add ((tTMO_SLL *)
                                         (&(LCB_UPSTR_LIST (pLspCtrlBlock))),
                                         (tTMO_SLL_NODE *)
                                         (&UPSTR_DSTR_LIST_NODE
                                          (pTempLspUpCtrlBlock)));

                            /* invoke up st m/c after setting the event */
                            u1Event = LDP_DU_UP_LSM_EVT_INT_DN_MAP;
                            LdpDuUpSm (pTempLspUpCtrlBlock,
                                       LDP_DU_UP_LSM_EVT_INT_DN_MAP,
                                       pLdpMsgInfo);
                            LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
                        }
                    }
                    if (u1UpStrAlrPre == LDP_TRUE)
                    {
                        /* Mapping is already done between Downstream control
                         * block and downstream control block. But Label mapping
                         * message is not sent to upstream. Only label request message
                         * is sent downstream
                         * */
                        if (pTempLspUpCtrlBlock == NULL)
                        {
                            continue;
                        }

                        if (pTempLspUpCtrlBlock->pDnstrCtrlBlock == NULL)
                        {
                            TMO_SLL_Add ((tTMO_SLL
                                          *) (&(LCB_UPSTR_LIST
                                                (pLspCtrlBlock))),
                                         (tTMO_SLL_NODE
                                          *) (&(pTempLspUpCtrlBlock->
                                                NextUpstrNode)));
                            pTempLspUpCtrlBlock->pDnstrCtrlBlock =
                                pLspCtrlBlock;
                        }

                        if (LCB_STATE (pLspCtrlBlock) != LDP_DU_DN_LSM_ST_EST)
                        {
                            LdpDuUpSm (pTempLspUpCtrlBlock,
                                       LDP_DU_UP_LSM_EVT_INT_DN_MAP,
                                       pLdpMsgInfo);
                            LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
                        }
                        continue;
                    }
                    pLspUpCtrlBlock = NULL;
                    LdpCreateUpstrCtrlBlock (&pLspUpCtrlBlock);
                    if (pLspUpCtrlBlock == NULL)
                    {
                        /* Break is not put here as we need to introduce local 
                         * variable to come out of two loops 
                         * (peer list and entity list). 
                         * Hence scanning is continued. */
                        continue;
                    }
                    UPSTR_LBL_TYPE (pLspUpCtrlBlock) =
                        SSN_GET_LBL_TYPE (pLdpSession);
                    MEMCPY (&UPSTR_FEC (pLspUpCtrlBlock),
                            &LCB_FEC (pLspCtrlBlock), sizeof (tFec));
                    UPSTR_INCOM_IFINDX (pLspUpCtrlBlock) =
                        LdpSessionGetIfIndex (pLdpSession,
                                              LCB_FEC (pLspUpCtrlBlock).
                                              u2AddrFmly);
                    UPSTR_USSN (pLspUpCtrlBlock) = pLdpSession;
                    UPSTR_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_IDLE;
                    UPSTR_DSTR_PTR (pLspUpCtrlBlock) = pLspCtrlBlock;
                    /* add upctlblk to the dnctlblk's upctlblk list */
                    TMO_SLL_Add ((tTMO_SLL *)
                                 (&(LCB_UPSTR_LIST (pLspCtrlBlock))),
                                 (tTMO_SLL_NODE *)
                                 (&UPSTR_DSTR_LIST_NODE (pLspUpCtrlBlock)));

#ifdef MPLS_IPV6_WANTED
                    if (LCB_FEC (pLspUpCtrlBlock).u2AddrFmly ==
                        LDP_ADDR_TYPE_IPV6)
                    {
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "Upstr list-Dn Node %p FEC %s\n",
                                  &UPSTR_DSTR_LIST_NODE (pLspUpCtrlBlock),
                                  Ip6PrintAddr (&LCB_FEC (pLspUpCtrlBlock).
                                                Prefix.Ip6Addr));
                    }
                    else
#endif
                    {
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "Upstr list-Dn Node %p FEC %x\n",
                                  &UPSTR_DSTR_LIST_NODE (pLspUpCtrlBlock),
                                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspUpCtrlBlock).
                                                    Prefix));
                    }
                    /* add upctlblk to upctlblk list of the curr ssn */
                    TMO_SLL_Add ((tTMO_SLL *) (&(SSN_ULCB (pLdpSession))),
                                 (tTMO_SLL_NODE
                                  *) (&(UPSTR_DSSN_LIST_NODE
                                        (pLspUpCtrlBlock))));
                    /* invoke up st m/c after setting the event */
                    u1Event = LDP_DU_UP_LSM_EVT_INT_DN_MAP;
                    LdpDuUpSm (pLspUpCtrlBlock,
                               LDP_DU_UP_LSM_EVT_INT_DN_MAP, pLdpMsgInfo);
                    LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
                }                /* ssn state operational */
            }                    /* not same as dssn */
        }                        /* peer scan */
    }                            /* enty scan */
    LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuDnIdDelFec                                           */
/* Description   : This routine is called when DU DN state machine is in     */
/*                 IDLE state and Delete FEC Event received.                 */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuDnIdDelFec (tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    tTMO_SLL           *pLspUpCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlock);
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    UINT1               u1Event = LDP_DU_UP_LSM_EVT_INT_DN_WR;

    LDP_SUPPRESS_WARNING (pLdpMsgInfo);

    /* As per RFC 5036 section A.1.7,
     * Send Label Abort Req message to downstream, if the
     * label retention mode is conservative
     * */
    if ((pLspCtrlBlock->pDStrSession != NULL) &&
        (((SSN_GET_ENTITY (pLspCtrlBlock->pDStrSession))->u2LabelRet) ==
         LDP_CONSERVATIVE_MODE))
    {
        LdpSendLblAbortReqMsg (pLspCtrlBlock);
    }

    TMO_DYN_SLL_Scan (pLspUpCtrlBlkList, pSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);
        LdpInvCorrStMh (UPSTR_USSN (pLspUpCtrlBlock), pLspUpCtrlBlock,
                        u1Event, pLdpMsgInfo, LDP_TRUE);
    }

    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuDnEstLdpMap                                          */
/* Description   : This routine is called when DU DN state machine is in     */
/*                 ESTABLISHED state and LDP Mapping Event received.         */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuDnEstLdpMap (tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tTMO_SLL           *pUpstCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tTMO_SLL           *pLspUpCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlock);
    tLspInfo            LspInfo;
    UINT4               u4DestMask = 0;
    UINT4               u4RtPort = 0;
    UINT4               u4RtGw = 0;
    tGenU4Addr          NextHopAddr;
    UINT2               u2RtPort = 0;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT2               u2PvCount = 0;
    UINT2               u2LabelOperation = LDP_ZERO;
    UINT1               u1HopCount = 0;
    UINT1               u1Event = LDP_DU_UP_LSM_EVT_INT_DN_MAP;
    tIp6Addr            Ipv6RtGw;
    tGenU4Addr          TempNextHopAddr;
    UINT4               u4L3VlanIf = 0;

    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));
    MEMSET (&TempNextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));
    MEMSET (&Ipv6RtGw, LDP_ZERO, sizeof (tIp6Addr));

#ifdef MPLS_IPV6_WANTED
    if (LCB_FEC (pLspCtrlBlock).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        LDP_DBG1 (LDP_SSN_PRCS,
                  "LdpDuDnEstLdpMap: Route issue for FEC %s in Estb LblMap Rcvd\n",
                  Ip6PrintAddr (&LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr));
    }
    else
#endif
    {
        LDP_DBG1 (LDP_SSN_PRCS,
                  "LdpDuDnEstLdpMap: Route issue for FEC %x in Estb LblMap Rcvd\n",
                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix));
    }

    if (LdpIsLoopFound (LDP_MSG_HC_PTR (pLdpMsgInfo),
                        LDP_MSG_PV_PTR (pLdpMsgInfo), (&u2PvCount),
                        (&u1HopCount), u2IncarnId,
                        LCB_DSSN (pLspCtrlBlock), FALSE) == LDP_TRUE)
    {
        /* Label release message has to be sent with Status TLV (Loop detected status
         * code)
         * */
        pLspCtrlBlock->pDStrSession->u1StatusCode = LDP_STAT_TYPE_LOOP_DETECTED;

        LDP_DBG (LDP_ADVT_PRCS, "ADVT: Loop detected - LdpDuDnEstLdpMap\n");
        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                          &LCB_DLBL (pLspCtrlBlock), LCB_DSSN (pLspCtrlBlock));
        LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                               MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }

    LdpCopyAddr (&TempNextHopAddr.Addr, &pLspCtrlBlock->NextHopAddr,
                 pLspCtrlBlock->Fec.u2AddrFmly);

    TempNextHopAddr.u2AddrType = pLspCtrlBlock->Fec.u2AddrFmly;

    if (CfaUtilGetIfIndexFromMplsTnlIf
        (pLspCtrlBlock->u4OutIfIndex, &u4L3VlanIf, TRUE) != CFA_SUCCESS)
    {
        printf
            ("%s : %d CfaUtilGetIfIndexFromMplsTnlIf() Failed pLspCtrlBlock->u4OutIfIndex=%d u4L3VlanIf=%d\n",
             __FUNCTION__, __LINE__, pLspCtrlBlock->u4OutIfIndex, u4L3VlanIf);
    }

    if (LdpIsSsnForFec (&TempNextHopAddr,
                        u4L3VlanIf, LCB_DSSN (pLspCtrlBlock)) == LDP_FALSE)
    {

        LDP_DBG (LDP_ADVT_PRCS,
                 "LdpDuDnEstLdpMap: Label NOT rcvd from the next hop\n");
        LCB_HCOUNT (pLspCtrlBlock) = u1HopCount;
        pLspCtrlBlock->pu1PVTlv = LDP_MSG_PV_PTR (pLdpMsgInfo);
        pLspCtrlBlock->u1PVCount = (UINT1) u2PvCount;

#ifdef LDP_GR_WANTED
        /* This is the case where we have rececived a Label Map Msg
         * from the peer which is not the next hop (as per the routing table).
         * Since the control block already exists (which was created the first
         * time the label map was received and label retention mode being
         * liberal the control block was moved to established state but
         * not programmed in H/w. Now after GR we will receive the map msg
         * and will find the control block already existing in the established
         * state. Thus we must mark it unstale if it is in stale state and return */

        if (pLspCtrlBlock->u1StaleStatus == LDP_TRUE)
        {
            LDP_DBG1 (GRACEFUL_DEBUG,
                      "LdpDuDnEstLdpMap: Marking the DLCB for FEC: "
                      "%x as UNSTALE\n",
                      LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix));
            pLspCtrlBlock->u1StaleStatus = LDP_FALSE;
        }
#endif
        return;
    }

#ifdef MPLS_IPV6_WANTED
    if (LCB_FEC (pLspCtrlBlock).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        /* send rel msg if recd map msg's fec is one of the local addr */
        if (LdpIsLsrPartOfIpv6ErHopOrFec
            (&LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr,
             LCB_FEC (pLspCtrlBlock).u1PreLen) == LDP_TRUE)
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Map Recd for Local Addr-LdpDuDnIdLdpMap\n");
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }

        if (LdpIpv6GetExactRoute
            (&(LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr),
             LCB_FEC (pLspCtrlBlock).u1PreLen, &u4RtPort,
             &NextHopAddr.Addr.Ip6Addr) == LDP_IP_FAILURE)

        {
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: IP Get Exact Route Not Available\n");
            /*  In LIBERAL retain the mappings and release in case of 
             *  CONSERVATIVE */
        }
        NextHopAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
    }
    else
#endif
    {
        LDP_GET_MASK_FROM_PRFX_LEN ((LCB_FEC (pLspCtrlBlock).u1PreLen),
                                    u4DestMask);
        /* send rel msg if recd map msg's fec is one of the local addr */
        if (LdpIsLsrPartOfIpv4ErHopOrFec
            (LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix),
             u4DestMask) == LDP_TRUE)
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Map Recd for Local Addr-LdpDuDnIdLdpMap\n");
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }

        if (LdpIpGetExactRoute
            ((LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix)), u4DestMask,
             &u4RtPort, &LDP_IPV4_U4_ADDR (NextHopAddr.Addr)) == LDP_IP_FAILURE)
        {
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: IP Get Exact Route Not Available\n");
            /*  In LIBERAL retain the mappings and release in case of 
             *  CONSERVATIVE */
        }
        NextHopAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
    }

    if (LDP_ENTITY_LDP_OVER_RSVP_FLAG
        (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))) == LDP_SNMP_TRUE)
    {
        /* Route nexthop and the targeted session's transport address should be in the same network, 
         * otherwise do the following actions */
#ifdef MPLS_IPV6_WANTED
        if (LCB_FEC (pLspCtrlBlock).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
        {
            if (LdpIpUcastIpv6RouteQuery
                (&pLspCtrlBlock->NextHopAddr.Ip6Addr, &u4RtPort,
                 &Ipv6RtGw) == LDP_IP_SUCCESS)
            {
                if ((LdpIsAddressMatchWithAddrType
                     (&NextHopAddr.Addr, &LCB_FEC (pLspCtrlBlock).Prefix,
                      NextHopAddr.u2AddrType,
                      LCB_FEC (pLspCtrlBlock).u2AddrFmly) == LDP_TRUE)
                    ||
                    (MEMCMP
                     (&Ipv6RtGw, &NextHopAddr.Addr.Ip6Addr,
                      LDP_IPV6ADR_LEN) != LDP_ZERO))

                {
                    if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->
                         u2LabelRet) == LDP_CONSERVATIVE_MODE)
                    {
                        /*  In CONSERVATIVE release the mappings 
                         *  If route is not found or session for next hop is not
                         *  present.
                         */
                        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                          &LCB_DLBL (pLspCtrlBlock),
                                          LCB_DSSN (pLspCtrlBlock));
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    }
                    else
                    {
                        LDP_DBG3 (LDP_SSN_PRCS,
                                  "Route issue for FEC %s Nexthop %s OutL3If: %d in"
                                  " Estb Lbl Map rcvd\n",
                                  Ip6PrintAddr (&LCB_FEC (pLspCtrlBlock).Prefix.
                                                Ip6Addr),
                                  Ip6PrintAddr (&NextHopAddr.Addr.Ip6Addr),
                                  pLspCtrlBlock->u4OutIfIndex);
                        if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) == 0)
                        {
                            MEMSET (&LspInfo, LDP_ZERO, sizeof (tLspInfo));
                            /* create ftn entry */
                            LspInfo.bIsLspDestroy = LDP_TRUE;
                            LdpMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                               MPLS_OPR_PUSH, pLspCtrlBlock,
                                               NULL, &LspInfo);
                            LCB_MLIB_UPD_STATUS (pLspCtrlBlock) = 0;

                            /* Prefix Route does not exist case, we have not done the ILM programming so,
                             * program it here. */
                            pUpstCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlock);
                            TMO_SLL_Scan (pUpstCtrlBlkList, pTempSllNode,
                                          tTMO_SLL_NODE *)
                            {
                                pLspUpCtrlBlock = SLL_TO_UPCB (pTempSllNode);
                                /* Intermediate Node */
                                if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL)
                                    &&
                                    ((LCB_DSSN
                                      (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) !=
                                     NULL))
                                {
                                    u2LabelOperation = MPLS_OPR_POP_PUSH;
                                }
                                else
                                {
                                    /* Egress Node is not possible here,
                                     * so continue*/
                                    continue;
                                }

                                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                                       u2LabelOperation,
                                                       pLspCtrlBlock,
                                                       pLspUpCtrlBlock);
                            }
                        }
                        /*  In LIBERAL retain the mappings */
                        LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
                        /* MLIB update will be done after the prefix route 
                         * notification received from the IP module */
                        LCB_MLIB_UPD_STATUS (pLspCtrlBlock) =
                            LDP_DU_DN_MLIB_UPD_NOT_DONE;
                    }
                    return;
                }
            }
            else
            {
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: Loop detected - LdpDuDnIdLdpMap\n");
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return;
            }
        }
        else
#endif
        {

            if (LdpIpUcastRouteQuery
                (LDP_IPV4_U4_ADDR (pLspCtrlBlock->NextHopAddr), &u2RtPort,
                 &u4RtGw) == LDP_IP_SUCCESS)
            {
                if ((LDP_IPV4_U4_ADDR (NextHopAddr.Addr) ==
                     LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix))
                    || (u4RtGw != LDP_IPV4_U4_ADDR (NextHopAddr.Addr)))
                {
                    if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->
                         u2LabelRet) == LDP_CONSERVATIVE_MODE)
                    {
                        /*  In CONSERVATIVE release the mappings 
                         *  If route is not found or session for next hop is not
                         *  present.
                         */
                        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                          &LCB_DLBL (pLspCtrlBlock),
                                          LCB_DSSN (pLspCtrlBlock));
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    }
                    else
                    {
                        LDP_DBG3 (LDP_SSN_PRCS,
                                  "Route issue for FEC %x Nexthop %x OutL3If: %d in"
                                  " Estb Lbl Map rcvd\n",
                                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                    Prefix),
                                  LDP_IPV4_U4_ADDR (NextHopAddr.Addr),
                                  pLspCtrlBlock->u4OutIfIndex);
                        if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) == 0)
                        {
                            MEMSET (&LspInfo, LDP_ZERO, sizeof (tLspInfo));
                            /* create ftn entry */
                            LspInfo.bIsLspDestroy = LDP_TRUE;
                            LdpMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                               MPLS_OPR_PUSH, pLspCtrlBlock,
                                               NULL, &LspInfo);
                            LCB_MLIB_UPD_STATUS (pLspCtrlBlock) = 0;

                            /* Prefix Route does not exist case, we have not done the ILM programming so,
                             * program it here. */
                            pUpstCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlock);
                            TMO_SLL_Scan (pUpstCtrlBlkList, pTempSllNode,
                                          tTMO_SLL_NODE *)
                            {
                                pLspUpCtrlBlock = SLL_TO_UPCB (pTempSllNode);
                                /* Intermediate Node */
                                if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL)
                                    &&
                                    ((LCB_DSSN
                                      (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) !=
                                     NULL))
                                {
                                    u2LabelOperation = MPLS_OPR_POP_PUSH;
                                }
                                else
                                {
                                    /* Egress Node is not possible here,
                                     * so continue*/
                                    continue;
                                }

                                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                                       u2LabelOperation,
                                                       pLspCtrlBlock,
                                                       pLspUpCtrlBlock);
                            }
                        }
                        /*  In LIBERAL retain the mappings */
                        LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
                        /* MLIB update will be done after the prefix route 
                         * notification received from the IP module */
                        LCB_MLIB_UPD_STATUS (pLspCtrlBlock) =
                            LDP_DU_DN_MLIB_UPD_NOT_DONE;
                    }
                    return;
                }
            }
            else
            {
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: Loop detected - LdpDuDnIdLdpMap\n");
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return;
            }

        }
    }
    else
    {
        if (CfaUtilGetIfIndexFromMplsTnlIf
            (pLspCtrlBlock->u4OutIfIndex, &u4L3VlanIf, TRUE) != CFA_SUCCESS)
        {
            printf
                ("%s : %d CfaUtilGetIfIndexFromMplsTnlIf() Failed pLspCtrlBlock->u4OutIfIndex=%d u4L3VlanIf=%d\n",
                 __FUNCTION__, __LINE__, pLspCtrlBlock->u4OutIfIndex,
                 u4L3VlanIf);
        }

        if ((gi4MplsSimulateFailure != LDP_SIM_FAILURE_SAME_PREFIX_AND_NETWORK)
            &&
            (((LdpIsAddressMatchWithAddrType
               (&NextHopAddr.Addr, &LCB_FEC (pLspCtrlBlock).Prefix,
                NextHopAddr.u2AddrType,
                LCB_FEC (pLspCtrlBlock).u2AddrFmly) == LDP_TRUE))
             ||
             (LdpIsSsnForFec
              (&NextHopAddr, u4L3VlanIf,
               LCB_DSSN (pLspCtrlBlock)) == LDP_FALSE)))

        {
            if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->u2LabelRet) ==
                LDP_CONSERVATIVE_MODE)
            {
                /*  In CONSERVATIVE release the mappings 
                 *  If route is not found or session for next hop is not
                 *  present.
                 */
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            }
            else
            {
                /*  In LIBERAL retain the mappings */
                LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
#ifdef MPLS_IPV6_WANTED
                if (LCB_FEC (pLspCtrlBlock).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
                {
                    LDP_DBG3 (LDP_SSN_PRCS,
                              "Route issue for FEC %s Nexthop %s OutL3If %d "
                              "in Estb LblMap Rcvd\n",
                              Ip6PrintAddr (&LCB_FEC (pLspCtrlBlock).Prefix.
                                            Ip6Addr),
                              Ip6PrintAddr (&NextHopAddr.Addr.Ip6Addr),
                              pLspCtrlBlock->u4OutIfIndex);
                }
                else
#endif
                {
                    LDP_DBG3 (LDP_SSN_PRCS,
                              "Route issue for FEC %x Nexthop %x OutL3If %d "
                              "in Estb LblMap Rcvd\n",
                              LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix),
                              LDP_IPV4_U4_ADDR (NextHopAddr.Addr),
                              pLspCtrlBlock->u4OutIfIndex);
                }
#ifdef LDP_GR_WANTED
                if (pLspCtrlBlock->u1StaleStatus == LDP_TRUE)
                {
                    LDP_DBG1 (GRACEFUL_DEBUG,
                              "LdpDuDnEstLdpMap: Marking the DLCB for FEC: "
                              "%x as UNSTALE\n",
                              LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).
                                                Prefix));
                    pLspCtrlBlock->u1StaleStatus = LDP_FALSE;
                }
#endif
            }
            return;
        }
        if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) != LDP_DU_DN_MLIB_UPD_DONE)
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "Route issue for FEC"
                     "in Estb LblMap Rcvd, FTN is programmed so skipping\n");
            return;
        }
        LdpCopyAddr (&pLspCtrlBlock->NextHopAddr, &NextHopAddr.Addr,
                     NextHopAddr.u2AddrType);
        pLspCtrlBlock->u2AddrType = NextHopAddr.u2AddrType;
    }

    LDP_MSG_PV_COUNT (pLdpMsgInfo) = (UINT1) u2PvCount;
    LDP_MSG_HC (pLdpMsgInfo) = u1HopCount;

#ifdef LDP_GR_WANTED
    if (pLspCtrlBlock->u1StaleStatus == LDP_TRUE)
    {
        LDP_DBG1 (GRACEFUL_DEBUG, "LdpDuDnEstLdpMap: Marking the DLCB for FEC: "
                  "%x as UNSTALE\n",
                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix));
        pLspCtrlBlock->u1StaleStatus = LDP_FALSE;
    }
#endif

    /*  Check if the attributes are changed and invoke the
     *  Up stream St Machine     */
    if ((LCB_HCOUNT (pLspCtrlBlock) != u1HopCount) ||
        (LDP_MSG_PV_PTR (pLdpMsgInfo) != NULL) ||
        (pLdpMsgInfo->u1IsLblChanged == LDP_TRUE))
    {
        TMO_DYN_SLL_Scan (pLspUpCtrlBlkList, pSllNode,
                          pTempSllNode, tTMO_SLL_NODE *)
        {
            pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);
            LdpInvCorrStMh (UPSTR_USSN (pLspUpCtrlBlock), pLspUpCtrlBlock,
                            u1Event, pLdpMsgInfo, LDP_TRUE);
        }
    }

#ifdef MPLS_IPV6_WANTED
    if (LCB_FEC (pLspCtrlBlock).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        LDP_DBG2 (LDP_SSN_PRCS, "Estb Mapping received Prefix %s NextHop %s\n",
                  Ip6PrintAddr (&LCB_FEC (pLspCtrlBlock).Prefix.Ip6Addr),
                  Ip6PrintAddr (&NextHopAddr.Addr.Ip6Addr));
    }
    else
#endif
    {
        LDP_DBG2 (LDP_SSN_PRCS, "Estb Mapping received Prefix %x NextHop %x\n",
                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlock).Prefix),
                  LDP_IPV4_U4_ADDR (NextHopAddr.Addr));
    }
    pLspCtrlBlock->pu1PVTlv = LDP_MSG_PV_PTR (pLdpMsgInfo);

    pLspCtrlBlock->u1PVCount = (UINT1) u2PvCount;

    LCB_HCOUNT (pLspCtrlBlock) = u1HopCount;

    if (pLdpMsgInfo->u1IsLblChanged == LDP_TRUE)
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_MODIFY, MPLS_OPR_PUSH,
                               pLspCtrlBlock, NULL);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuDnEstLdpWR                                           */
/* Description   : This routine is called when DU DN state machine is in     */
/*                 ESTABLISHED state and LDP WithDraw Event received.        */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuDnEstLdpWR (tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    /*   For each Upstream_LSP_control_block for this FEC, pass event
     *  `Internal downstream Withdraw' to its state machine.
     *   Send a LDP Release downstream.
     *   New State:      IDLE */
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tTMO_SLL           *pLspUpCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlock);
    UINT1               u1Event = LDP_DU_UP_LSM_EVT_INT_DN_WR;

    TMO_DYN_SLL_Scan (pLspUpCtrlBlkList, pSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);
        LdpInvCorrStMh (UPSTR_USSN (pLspUpCtrlBlock), pLspUpCtrlBlock,
                        u1Event, pLdpMsgInfo, LDP_TRUE);
    }

    LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock), &LCB_DLBL (pLspCtrlBlock),
                      LCB_DSSN (pLspCtrlBlock));

    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                           MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuDnEstDelFec                                          */
/* Description   : This routine is called when DU DN state machine is in     */
/*                 ESTABLISHED state and Delete FEC Event received.          */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuDnEstDelFec (tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    /*  Send LDP-RELEASE downstream and delete the control block.
       New State:      IDLE */
    tTMO_SLL           *pLspUpCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlock);
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tLspInfo            LspInfo;
    UINT1               u1Event;

    pLdpMsgInfo = (tLdpMsgInfo *) pLdpMsgInfo;

    if (LCB_DSSN (pLspCtrlBlock) != NULL)
    {
        if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->u2LabelRet) !=
            LDP_LIBERAL_MODE)
        {
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));
        }
    }

    TMO_DYN_SLL_Scan (pLspUpCtrlBlkList, pSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);
        if (SSN_ADVTYPE (UPSTR_USSN (pLspUpCtrlBlock)) == LDP_DSTR_UNSOLICIT)
        {
            u1Event = LDP_DU_UP_LSM_EVT_DEL_FEC;
            LdpDuUpSm (pLspUpCtrlBlock, u1Event, pLdpMsgInfo);
        }
        else
        {
            u1Event = LDP_DU_UP_LSM_EVT_INT_DN_WR;
            LdpInvCorrStMh (UPSTR_USSN (pLspUpCtrlBlock), pLspUpCtrlBlock,
                            u1Event, pLdpMsgInfo, LDP_TRUE);
        }
    }
#ifdef MPLS_IPV6_WANTED
    if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        LDP_DBG1 (LDP_SSN_PRCS, "LdpDuDnEstDelFec: Del FEC %s\n",
                  Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.Ip6Addr));
    }
    else
#endif
    {
        LDP_DBG1 (LDP_SSN_PRCS, "LdpDuDnEstDelFec: Del FEC %x\n",
                  LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));
    }

    if (LCB_DSSN (pLspCtrlBlock) != NULL)
    {
        if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->u2LabelRet) !=
            LDP_LIBERAL_MODE)
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                   MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        }
        else
        {
            MEMSET (&LspInfo, LDP_ZERO, sizeof (tLspInfo));
            /* create ftn entry */
            LspInfo.bIsLspDestroy = LDP_TRUE;
            LdpMplsMlibUpdate (MPLS_MLIB_FTN_DELETE, MPLS_OPR_PUSH,
                               pLspCtrlBlock, NULL, &LspInfo);
            LCB_MLIB_UPD_STATUS (pLspCtrlBlock) = LDP_DU_DN_MLIB_UPD_NOT_DONE;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuDnEstNHChg                                           */
/* Description   : This routine is called when DU DN state machine is in     */
/*                 ESTABLISHED state and Next Hop Change Event received.     */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuDnEstNHChg (tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    UINT1               u1Event = LDP_DU_UP_LSM_EVT_INT_DN_WR;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tTMO_SLL           *pLspUpCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlock);
    tLspCtrlBlock      *pNHDnLspCtrlBlock = NULL;
    tLdpSession        *pDssn = NULL;
    tLdpSession        *pLdpSession = NULL;
    tTMO_SLL           *pEntityList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tUstrLspCtrlBlock  *pTempLspUpCtrlBlock = NULL;
    tLdpRouteEntryInfo *pRouteInfo = (tLdpRouteEntryInfo *) pLdpMsgInfo;
    tLdpMsgInfo         LdpMsgInfo;
    UINT1               u1SsnState;
    BOOL1               bUpStrFecFound = LDP_FALSE;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = LDP_ZERO;
#endif
    tGenU4Addr          TempNextHop;
    tLspCtrlBlock      *pFtnHwCtrlBlock = NULL;
    tLdpSession        *pSessionEntry = NULL;
    UINT1               u1Found = LDP_FALSE;

    MEMSET (&LdpMsgInfo, LDP_ZERO, sizeof (tLdpMsgInfo));
    MEMSET (&TempNextHop, LDP_ZERO, sizeof (tGenU4Addr));

#ifdef MPLS_IPV6_WANTED
    if (pRouteInfo->u2AddrType == LDP_ADDR_TYPE_IPV6)
    {
        LDP_DBG1 (LDP_SSN_PRCS, "Next hop change for FEC %s\n",
                  Ip6PrintAddr (&pRouteInfo->DestAddr.Ip6Addr));
    }
    else
#endif
    {
        LDP_DBG1 (LDP_SSN_PRCS, "Next hop change for FEC %x\n",
                  LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr));
    }

    pSessionEntry = LCB_DSSN (pLspCtrlBlock);
    LDP_DBG6 (LDP_ADVT_PRCS,
              "%s::%d In Coming CntrlBlk "
              "with DnStream Peer %d.%d.%d.%d\n",
              __func__, __LINE__,
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);
    LDP_DBG3 (LDP_SSN_PRCS,
              "%s::%d::FEC In Incoming CntlBlock:: %x\n",
              __func__, __LINE__, LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));

    /* get the dn ssn for the new next hop */
    LdpCopyAddr (&TempNextHop.Addr, &pRouteInfo->NextHop,
                 pRouteInfo->u2AddrType);
    TempNextHop.u2AddrType = pRouteInfo->u2AddrType;

    if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->u2LabelRet) ==
        LDP_LIBERAL_MODE)
    {
        LdpReRouteTime (LDP_START_REROUTE);
        /* get the dn ssn for the new next hop */
        if (LdpGetPeerSession (&TempNextHop,
                               pRouteInfo->u4RtIfIndx,
                               &pDssn, u2IncarnId) == LDP_SUCCESS)
        {
            /*  Ignore the Route Change Event if the control block
             *  is there on the new session   */
            if (pDssn == LCB_DSSN (pLspCtrlBlock))
            {
                LDP_DBG3 (LDP_SSN_PRCS, "LSP NH chg for Dest %x "
                          "%s::%d\n", LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr),
                          __func__, __LINE__);
            }
        }
        else if (LdpGetLdpOverRsvpPeerSession
                 (LDP_IPV4_U4_ADDR (pRouteInfo->NextHop),
                  pRouteInfo->u4RtIfIndx, &pDssn, u2IncarnId) == LDP_SUCCESS)
        {
            /* Handle LDP over RSVP scenario here */
            /* Search for LDP over RSVP targeted session 
               which goes via this FEC nexthop */
            /*  Ignore the Route Change Event if the control block
             *  is there on the new session   */
            if (pDssn == LCB_DSSN (pLspCtrlBlock))
            {
                if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) ==
                    LDP_DU_DN_MLIB_UPD_NOT_DONE)
                {
                    LDP_DBG1 (LDP_SSN_PRCS, "LSP NH chg for Dest %x DB "
                              "is not programmed\n",
                              LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr));
                    LDP_IPV4_U4_ADDR (pLspCtrlBlock->NextHopAddr) =
                        LDP_IPV4_U4_ADDR (pRouteInfo->NextHop);
                    pLspCtrlBlock->u2AddrType = LDP_ADDR_TYPE_IPV4;
                    LdpMlibUpdateForEstbLsps (pLspCtrlBlock);
                }
            }
        }
        if (((LDP_ENTITY_IS_TARGET_TYPE
              (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock)))) == LDP_TRUE)
            &&
            ((TMO_SLL_Count
              (LDP_ENTITY_ETH_LBL_RNGE_LIST
               (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))))) > LDP_ONE))
        {
            LDP_DBG (LDP_DBG_PRCS,
                     "LDP Tgt ssn has more than one LDP intf enabled\n");
            LDP_DBG (LDP_DBG_PRCS, "LDP targeted session deletion skipped\n");
            return;
        }
        if (pDssn == NULL)
        {
            /* Delete the FTN and ILM from MPLS DB but maintain the LSP info. 
             * in the LDP as it is liberal label retention mode */
            LDP_DBG (LDP_SSN_PRCS, "LdpDuDnEstNHChg: "
                     "Downstream session is NULL \n");
            LdpMlibDeleteForEstbLsps (pLspCtrlBlock);
        }

        /** Process Session Other than session came in new next Hop **/
        if ((pDssn != NULL) && (pDssn != LCB_DSSN (pLspCtrlBlock)))
        {
            LDP_DBG (LDP_DBG_PRCS,
                     "Process Session Other than session came in new next Hop\n");

            /* chk if a DN ctrl block exists for the same FEC */
            TMO_DYN_SLL_Scan (&SSN_DLCB (pDssn), pSllNode,
                              pTempSllNode, tTMO_SLL_NODE *)
            {
                pNHDnLspCtrlBlock = SLL_TO_LCB (pSllNode);

                LDP_DBG4 (LDP_SSN_PRCS,
                          "%s::%d::FEC Compare Downstream CntlBlock:: %x NewNextHop CntlBlock:: %x\n",
                          __func__, __LINE__,
                          LDP_IPV4_U4_ADDR (pNHDnLspCtrlBlock->Fec.Prefix),
                          LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr));

                if (LdpIsAddressMatch
                    (&((LCB_FEC (pNHDnLspCtrlBlock)).Prefix),
                     &pRouteInfo->DestAddr,
                     (LCB_FEC (pNHDnLspCtrlBlock)).u2AddrFmly) == LDP_TRUE)
                {
                    LDP_DBG (LDP_DBG_PRCS,
                             "Downstream control block is available\n");

                    u1Found = LDP_TRUE;
                    break;
                }
            }

                /** do the processing : when there is no label map recieved 
                    on New Next hop. Delete the FTN. **/
                /** If control block is configured in H/W **/
            if ((LCB_MLIB_UPD_STATUS (pLspCtrlBlock) ==
                 LDP_DU_DN_MLIB_UPD_DONE) && (u1Found == LDP_FALSE))
            {
                        /** Delete the FTN from Hardware **/
                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE, MPLS_OPR_PUSH,
                                       pLspCtrlBlock, NULL);
                LCB_MLIB_UPD_STATUS (pLspCtrlBlock) =
                    LDP_DU_DN_MLIB_UPD_NOT_DONE;

                        /** clear the upstream control block List **/
                pTempSllNode = NULL;
                pSllNode = NULL;
                TMO_DYN_SLL_Scan (pLspUpCtrlBlkList, pSllNode,
                                  pTempSllNode, tTMO_SLL_NODE *)
                {
                    pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);

                    /* FEC X (A)---(FTN(h/w))-- B --(ILM(swap))-- C
                       A---B Link goes down, FEC X is rechable via C,
                       so Next hop change trigger comes on B for FEC X
                       When A-B session is processed first,
                       FEC X(A)---- FTN(S/W)---B---ILM(POP)---C
                       As the route is available via C so this ILM(POP) must be deleted
                     */

                    if (UPSTR_USSN (pLspUpCtrlBlock) == pDssn)
                    {
                        LdpSendLblWithdrawMsg (&LCB_FEC (pLspUpCtrlBlock),
                                               &LCB_ULBL (pLspUpCtrlBlock),
                                               pLspUpCtrlBlock->pUpstrSession);

                        LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_REL_AWT;

                        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                               MPLS_OPR_POP_PUSH,
                                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                               pLspUpCtrlBlock);
                    }
                    else
                    {
                        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                               MPLS_OPR_POP_PUSH,
                                               UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                               pLspUpCtrlBlock);

                        LdpSendMplsMlibUpdate
                            (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP,
                             NULL, pLspUpCtrlBlock);
                    }
                    TMO_SLL_Delete (&LCB_UPSTR_LIST
                                    (UPSTR_DSTR_PTR (pLspUpCtrlBlock)),
                                    UPCB_TO_SLL (pLspUpCtrlBlock));

                    UPSTR_DSTR_PTR (pLspUpCtrlBlock) = NULL;
                }
            }
                /** else control block is not configured in H/W **/
        }
        /** Process Session and control block same in new next hop ***/
        else if ((pDssn != NULL) && (pDssn == LCB_DSSN (pLspCtrlBlock)))
        {
            LDP_DBG (LDP_DBG_PRCS,
                     "Process Session and control block same in new next hop\n");
                /** If control block is configured in H/W **/
            if (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) != LDP_DU_DN_MLIB_UPD_DONE)
            {
                        /** check if any other FTN control block is installed in h/w**/
                pFtnHwCtrlBlock =
                    LdpDuDnGetInstalledFtnCtrlBlock (LCB_FEC (pLspCtrlBlock));

                LdpCopyAddr (&pLspCtrlBlock->NextHopAddr, &TempNextHop.Addr,
                             TempNextHop.u2AddrType);

                pLspCtrlBlock->u2AddrType = TempNextHop.u2AddrType;

                        /** clear it first **/
                if (pFtnHwCtrlBlock != NULL)
                {
                    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_MODIFY, MPLS_OPR_PUSH,
                                           pLspCtrlBlock, NULL);
                    LCB_MLIB_UPD_STATUS (pFtnHwCtrlBlock) =
                        LDP_DU_DN_MLIB_UPD_NOT_DONE;

                    LCB_MLIB_UPD_STATUS (pLspCtrlBlock) =
                        LDP_DU_DN_MLIB_UPD_DONE;
                }
                else
                {
                    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE, MPLS_OPR_PUSH,
                                           pLspCtrlBlock, NULL);
                    LCB_MLIB_UPD_STATUS (pLspCtrlBlock) =
                        LDP_DU_DN_MLIB_UPD_DONE;
                }

                        /** Update the swap entries **/
                if (pFtnHwCtrlBlock != NULL)
                {
                    pTempSllNode = NULL;
                    pSllNode = NULL;
                    pLspUpCtrlBlkList = &LCB_UPSTR_LIST (pFtnHwCtrlBlock);
                    TMO_DYN_SLL_Scan (pLspUpCtrlBlkList, pSllNode,
                                      pTempSllNode, tTMO_SLL_NODE *)
                    {
                        pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);

                        /* FEC X (A)---(FTN(h/w))-- B --(ILM(swap))-- C
                           A---B Link goes down, FEC X is rechable via C,
                           so Next hop change trigger comes on B for FEC X
                           When A-B session is processed first,
                           FEC X(A)---- FTN(S/W)---B---ILM(POP)---C
                           As the route is available via C so this ILM(POP) must be deleted
                         */

                        if (UPSTR_USSN (pLspUpCtrlBlock) == pDssn)
                        {
                            LdpSendLblWithdrawMsg (&LCB_FEC (pLspUpCtrlBlock),
                                                   &LCB_ULBL (pLspUpCtrlBlock),
                                                   pLspUpCtrlBlock->
                                                   pUpstrSession);

                            LCB_STATE (pLspUpCtrlBlock) =
                                LDP_DU_UP_LSM_ST_REL_AWT;

                            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                                   MPLS_OPR_POP_PUSH,
                                                   UPSTR_DSTR_PTR
                                                   (pLspUpCtrlBlock),
                                                   pLspUpCtrlBlock);
                        }
                        else
                        {
                            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                                   MPLS_OPR_POP_PUSH,
                                                   UPSTR_DSTR_PTR
                                                   (pLspUpCtrlBlock),
                                                   pLspUpCtrlBlock);

                            LdpSendMplsMlibUpdate
                                (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP,
                                 NULL, pLspUpCtrlBlock);
                        }

                        TMO_SLL_Delete (&LCB_UPSTR_LIST
                                        (UPSTR_DSTR_PTR (pLspUpCtrlBlock)),
                                        UPCB_TO_SLL (pLspUpCtrlBlock));
                        UPSTR_DSTR_PTR (pLspUpCtrlBlock) = NULL;
                    }
                }

                        /** Send WithDraw if ILM entry is there on Downstream Session of current control block **/

                TMO_SLL_Scan (&SSN_ULCB (pDssn),
                              pLspUpCtrlBlock, tUstrLspCtrlBlock *)
                {
                    if ((LdpCompareFec (&UPSTR_FEC (pLspUpCtrlBlock),
                                        &LCB_FEC (pLspCtrlBlock)) ==
                         LDP_EQUAL) &&
                        (LCB_STATE (pLspUpCtrlBlock) !=
                         LDP_DU_UP_LSM_ST_REL_AWT))
                    {
                        LdpSendLblWithdrawMsg (&LCB_FEC (pLspUpCtrlBlock),
                                               &LCB_ULBL (pLspUpCtrlBlock),
                                               pLspUpCtrlBlock->pUpstrSession);

                        LCB_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_REL_AWT;

                        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                               MPLS_OPR_POP,
                                               NULL, pLspUpCtrlBlock);
                    }
                }
                        /** Create swap entry for other ILMs */
                pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
                TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
                {
                    TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
                    {
                        bUpStrFecFound = LDP_FALSE;
                        if ((pLdpPeer->pLdpSession) == NULL)
                        {
                            continue;
                        }
                        /* Target sessions also we need to 
                         * consider for LDP over RSVP case */
                        pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

                        if ((LDP_ENTITY_IS_TARGET_TYPE
                             (SSN_GET_ENTITY (pLdpSession)) == LDP_TRUE)
                            && (SSN_GET_ENTITY (pLdpSession)->OutStackTnlInfo.
                                u4TnlId == LDP_ZERO))
                        {
                            LDP_DBG (LDP_ADVT_PRCS,
                                     "\n LdpDuDnEstNHChg: Labels can't be negotiated "
                                     " when out-tunnel is not "
                                     "associated with LDP targeted session\n");
                            continue;
                        }
                        SSN_GET_SSN_STATE (pLdpSession, u1SsnState);
                        if ((u1SsnState != LDP_SSM_ST_OPERATIONAL) ||
                            (SSN_ADVTYPE (pLdpSession) != LDP_DSTR_UNSOLICIT))
                        {
                            continue;
                        }

                        if (pLdpSession == LCB_DSSN (pLspCtrlBlock))
                        {
                            continue;
                        }

                        TMO_SLL_Scan (&SSN_ULCB (pLdpSession),
                                      pTempLspUpCtrlBlock, tUstrLspCtrlBlock *)
                        {
                            if (LdpCompareFec (&UPSTR_FEC (pTempLspUpCtrlBlock),
                                               &LCB_FEC (pLspCtrlBlock)) ==
                                LDP_EQUAL)
                            {
                                if ((UPSTR_DSTR_PTR (pTempLspUpCtrlBlock) ==
                                     NULL))
                                {
                                                                /** create the swap entry **/
                                    LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                                           MPLS_OPR_POP,
                                                           NULL,
                                                           pTempLspUpCtrlBlock);

                                    UPSTR_DSTR_PTR (pTempLspUpCtrlBlock) =
                                        pLspCtrlBlock;

                                    LdpSendMplsMlibUpdate
                                        (MPLS_MLIB_ILM_CREATE,
                                         MPLS_OPR_POP_PUSH, pLspCtrlBlock,
                                         pTempLspUpCtrlBlock);

                                    TMO_SLL_Add ((tTMO_SLL *)
                                                 (&
                                                  (LCB_UPSTR_LIST
                                                   (pLspCtrlBlock))),
                                                 (tTMO_SLL_NODE
                                                  *) (&UPSTR_DSTR_LIST_NODE
                                                      (pTempLspUpCtrlBlock)));
                                    bUpStrFecFound = LDP_TRUE;
                                }
                            }
                        }
                        if (bUpStrFecFound == LDP_TRUE)
                        {
                            continue;
                        }
                        pLspUpCtrlBlock = NULL;
                        LDP_DBG (LDP_DBG_PRCS,
                                 "creating Upstream control block\n");
                        LdpCreateUpstrCtrlBlock (&pLspUpCtrlBlock);
                        if (pLspUpCtrlBlock == NULL)
                        {
                            /* Break is not put here as we need to introduce local 
                             * variable to come out of two loops 
                             * (peer list and entity list). 
                             * Hence scanning is continued. */
                            continue;
                        }
                        UPSTR_LBL_TYPE (pLspUpCtrlBlock) =
                            SSN_GET_LBL_TYPE (pLdpSession);
                        /* Create a MPLS Tunnel interface at CFA  and Stack 
                         * over the MPLS l3 interface (which is stacked over 
                         * an L3IPVLAN interface, and use the interface index  
                         * returned as OutIfIndex
                         */
                        MEMCPY (&UPSTR_FEC (pLspUpCtrlBlock),
                                &LCB_FEC (pLspCtrlBlock), sizeof (tFec));

                        UPSTR_INCOM_IFINDX (pLspUpCtrlBlock) =
                            LdpSessionGetIfIndex (pLdpSession,
                                                  LCB_FEC (pLspCtrlBlock).
                                                  u2AddrFmly);
                        UPSTR_USSN (pLspUpCtrlBlock) = pLdpSession;
                        UPSTR_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_IDLE;
                        UPSTR_DSTR_PTR (pLspUpCtrlBlock) = pLspCtrlBlock;
                        /* add upctlblk to the dnctlblk's upctlblk list */
                        TMO_SLL_Add ((tTMO_SLL *)
                                     (&(LCB_UPSTR_LIST (pLspCtrlBlock))),
                                     (tTMO_SLL_NODE *)
                                     (&UPSTR_DSTR_LIST_NODE (pLspUpCtrlBlock)));

                                        /** add the upctrlblk to the upstream session **/
                        TMO_SLL_Add ((tTMO_SLL *) (&(SSN_ULCB (pLdpSession))),
                                     (tTMO_SLL_NODE
                                      *) (&(UPSTR_DSSN_LIST_NODE
                                            (pLspUpCtrlBlock))));
                        /* invoke up st m/c after setting the event */
                        u1Event = LDP_DU_UP_LSM_EVT_INT_DN_MAP;

                        MEMSET (&LdpMsgInfo, LDP_ZERO, sizeof (tLdpMsgInfo));

                        /* update the hop count and path vector values */

                        LDP_MSG_HC (&LdpMsgInfo) = LCB_HCOUNT (pLspCtrlBlock);
                        LDP_MSG_PV_PTR (&LdpMsgInfo) = pLspCtrlBlock->pu1PVTlv;
                        LDP_MSG_PV_COUNT (&LdpMsgInfo) =
                            pLspCtrlBlock->u1PVCount;

                        LdpDuUpSm (pLspUpCtrlBlock, u1Event, &LdpMsgInfo);
                        LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_EST;
                    }
                }
            }
        }
        else
        {
                /** Trace Log : Invalid Scenario **/
            LDP_DBG2 (LDP_DBG_PRCS,
                      "%s::%d,Invalid Scenario\n", __func__, __LINE__);
        }

        LdpReRouteTime (LDP_END_REROUTE);
        return;
    }
    else
    {
        if (LdpGetPeerSession (&TempNextHop,
                               pRouteInfo->u4RtIfIndx,
                               &pDssn, u2IncarnId) == LDP_SUCCESS)
        {
            /*  Ignore the Route Change Event if the control block
             *  is there on the new session   */
            if (pDssn == LCB_DSSN (pLspCtrlBlock))
            {
                return;
            }
        }
        /* send Rel msg to the old downstr session */
        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                          &LCB_DLBL (pLspCtrlBlock), LCB_DSSN (pLspCtrlBlock));

        /* send WR msg to all the upstr sessions */
        TMO_DYN_SLL_Scan (pLspUpCtrlBlkList, pSllNode,
                          pTempSllNode, tTMO_SLL_NODE *)
        {
            pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);

            if (UPSTR_USSN (pLspUpCtrlBlock) != NULL)
            {
                if (SSN_ADVTYPE (UPSTR_USSN (pLspUpCtrlBlock)) ==
                    LDP_DSTR_UNSOLICIT)
                {
                    LdpDuUpSm (pLspUpCtrlBlock, u1Event, &LdpMsgInfo);
                }
            }
        }

        LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE, MPLS_OPR_PUSH,
                               pLspCtrlBlock, NULL);
        LCB_STATE (pLspCtrlBlock) = LDP_DU_DN_LSM_ST_IDLE;

        if (pDssn == NULL)
        {
#ifdef MPLS_IPV6_WANTED
            if (pRouteInfo->u2AddrType == LDP_ADDR_TYPE_IPV6)
            {
                LDP_DBG1 (LDP_DBG_PRCS,
                          "No DnStr ssn for new NH for FEC %s\n",
                          Ip6PrintAddr (&pRouteInfo->DestAddr.Ip6Addr));
            }
            else
#endif
            {
                LDP_DBG1 (LDP_DBG_PRCS,
                          "No DnStr ssn for new NH for FEC %x\n",
                          LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr));
            }
            /* No Down Stream session for the new next hop */
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }

        /* create & init new dn ctrl block */
        u2IncarnId = SSN_GET_INCRN_ID (LCB_DSSN (pLspCtrlBlock));
        pNHDnLspCtrlBlock = (tLspCtrlBlock *) MemAllocMemBlk (LDP_LSP_POOL_ID);

        if (pNHDnLspCtrlBlock == NULL)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed to Alloc memory for LSP blk - "
                     "LdpDuDnEstNHChg\n");
            return;
        }
        LdpInitLspCtrlBlock (pNHDnLspCtrlBlock);
#ifdef CFA_WANTED
        /* Create a MPLS Tunnel interface at CFA  and Stack over the MPLS
         * l3 interface (which is stacked over an L3IPVLAN interface,
         * and use the interface index  returned as OutIfIndex
         */
        if (CfaIfmCreateStackMplsTunnelInterface
            (LdpSessionGetIfIndex (pDssn, pLspCtrlBlock->Fec.u2AddrFmly),
             &u4MplsTnlIfIndex) == CFA_FAILURE)
        {

            LDP_DBG (LDP_ADVT_MEM,
                     "LdpDuDnEstNHChg - ADVT: Mplstunnel IF creation failed"
                     "for NextHop DownStream Cntl block\n");
            MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pNHDnLspCtrlBlock);
            return;
        }
#ifdef MPLS_IPV6_WANTED
        if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
        {
            LDP_DBG6 (LDP_ADVT_PRCS,
                      "LdpDuDnEstNHChg: OutInt %d created for "
                      "FEC: %s with Peer %d.%d.%d.%d\n",
                      u4MplsTnlIfIndex,
                      Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.Ip6Addr),
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);
        }
        else
#endif
        {
            LDP_DBG6 (LDP_ADVT_PRCS,
                      "LdpDuDnEstNHChg: OutInt %d created for "
                      "FEC: %x with Peer %d.%d.%d.%d\n",
                      u4MplsTnlIfIndex,
                      LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix),
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);

        }

        pNHDnLspCtrlBlock->u4OutIfIndex = u4MplsTnlIfIndex;
#else
        pNHDnLspCtrlBlock->u4OutIfIndex =
            LdpSessionGetIfIndex (pDssn, pLspCtrlBlock->Fec.u2AddrFmly);
#endif
        MEMCPY (&(LCB_FEC (pNHDnLspCtrlBlock)),
                &(LCB_FEC (pLspCtrlBlock)), sizeof (tFec));

        LdpCopyAddr (&pNHDnLspCtrlBlock->NextHopAddr, &pRouteInfo->NextHop,
                     pRouteInfo->u2AddrType);
        pNHDnLspCtrlBlock->u2AddrType = pRouteInfo->u2AddrType;

        /* send lbl req msg to the dn ssn */
        LCB_DSSN (pNHDnLspCtrlBlock) = pDssn;
        TMO_SLL_Add ((tTMO_SLL *) (&(SSN_DLCB (pDssn))),
                     (tTMO_SLL_NODE *) (&(LCB_NXT_DLCB (pNHDnLspCtrlBlock))));
        TMO_DYN_SLL_Scan (pLspUpCtrlBlkList, pSllNode,
                          pTempSllNode, tTMO_SLL_NODE *)
        {
            pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);

            if (UPSTR_USSN (pLspUpCtrlBlock) != NULL)
            {
                if ((SSN_ADVTYPE (UPSTR_USSN (pLspUpCtrlBlock))
                     == LDP_DSTR_ON_DEMAND) &&
                    ((SSN_MRGTYPE (UPSTR_USSN (pLspUpCtrlBlock))
                      == LDP_VC_MRG) ||
                     (SSN_MRGTYPE ((UPSTR_USSN (pLspUpCtrlBlock)))
                      == LDP_VP_MRG)))
                {
                    TMO_SLL_Delete ((tTMO_SLL
                                     *) (&LCB_UPSTR_LIST (pLspCtrlBlock)),
                                    UPCB_TO_SLL (pLspUpCtrlBlock));
                    UPSTR_DSTR_PTR (pLspUpCtrlBlock) = pNHDnLspCtrlBlock;
                    TMO_SLL_Add ((tTMO_SLL
                                  *) (&LCB_UPSTR_LIST (pNHDnLspCtrlBlock)),
                                 UPCB_TO_SLL (pLspUpCtrlBlock));
                }
            }
        }
        LdpSendLblReqMsg (pNHDnLspCtrlBlock, 0, NULL, 0, NULL, 0);

        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpDuDnEstDnLost                                          */
/* Description   : This routine is called when DU DN state machine is in     */
/*                 ESTABLISHED state and Downstream Lost Event received.     */
/*                                                                           */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuDnEstDnLost (tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{

    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tTMO_SLL           *pLspUpCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlock);

    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
    TMO_DYN_SLL_Scan (pLspUpCtrlBlkList, pSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);
        if (UPSTR_USSN (pLspUpCtrlBlock) != NULL)
        {
            if ((UPSTR_DSTR_PTR (pLspUpCtrlBlock) != NULL) &&
                ((LCB_DSSN (UPSTR_DSTR_PTR (pLspUpCtrlBlock))) != NULL))
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                       MPLS_OPR_POP_PUSH,
                                       UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                       pLspUpCtrlBlock);

                LdpSendMplsMlibUpdate
                    (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP, NULL, pLspUpCtrlBlock);
            }
            else
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE,
                                       MPLS_OPR_POP,
                                       UPSTR_DSTR_PTR (pLspUpCtrlBlock),
                                       pLspUpCtrlBlock);

            }
            TMO_SLL_Delete (&LCB_UPSTR_LIST (UPSTR_DSTR_PTR (pLspUpCtrlBlock)),
                            UPCB_TO_SLL (pLspUpCtrlBlock));

            UPSTR_DSTR_PTR (pLspUpCtrlBlock) = NULL;
        }
    }

    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                           MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
    LdpDeleteLspCtrlBlock (pLspCtrlBlock);

    return;
}

/*****************************************************************************/
/* Function Name : LdpDuDnInvStEvt                                           */
/* Description   : This routine is called when DU DN state machine is in     */
/*                 ESTABLISHED/IDLE state and an invalid event is received   */
/* Input(s)      : pLspCtrlBlock - Points to down stream ctl blk             */
/*                 pLdpMsgInfo - Points to the Type of Msg rx from DStream.  */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuDnInvStEvt (tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{
    LDP_SUPPRESS_WARNING (pLspCtrlBlock);
    LDP_SUPPRESS_WARNING (pLdpMsgInfo);
}

/*****************************************************************************/
/* Function Name : LdpIsSsnForFec                                            */
/* Description   : This routine is called to check if the session is for the */
/*                 the FEC passed                                            */
/* Input(s)      : u4NextHopAddr - next hop addr                             */
/*                 u4OutIfIndex  - out if index                              */
/*                 pLdpSession - points to the session to be checked         */
/* Output(s)     : LDP_TRUE/LDP_FALSE                                                */
/* Return(s)     : None                                                      */
/*****************************************************************************/
UINT1
LdpIsSsnForFec (tGenU4Addr * pNextHopAddr, UINT4 u4OutIfIndex,
                tLdpSession * pLdpSession)
{
    tLdpSession        *pDssn = NULL;
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pLdpSession);
    if (LdpGetPeerSession (pNextHopAddr, u4OutIfIndex, &pDssn,
                           u2IncarnId) == LDP_SUCCESS)
    {
        if (pDssn == pLdpSession)
        {
            return LDP_TRUE;
        }
    }
    return LDP_FALSE;
}

/*****************************************************************************/
/* Function Name : ldpDulspSetReq                                            */
/* Description   : This routine is called from ProcessIpEvents when a new    */
/*                 route is triggered                                        */
/* Input(s)      : u2IncarnId - Incarn ID                                    */
/*                 Fec  - contains the new Fec                               */
/*                 pRtInfo - points to Route Info                            */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDuLspSetReq (UINT2 u2IncarnId, tLdpIpRtInfo * pRtInfo)
{
/** vishal_1 : Not updated for IPV6, as ithis API is currently not in USE */
    tLdpMsgInfo         LdpMsgInfo;
    tLdpSession        *pLdpSession = NULL;
    tLdpSession        *pTempLdpSession = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tTMO_SLL           *pEntityList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tFec                Fec;
    UINT1               u1IsEgress = LDP_FALSE;
    UINT1               u1SsnState;
    UINT1               u1Event;
    UINT1               u1PrefLen;
    tGenU4Addr          TempNextHop;

    MEMSET (&LdpMsgInfo, LDP_ZERO, sizeof (tLdpMsgInfo));
    MEMSET (&Fec, LDP_ZERO, sizeof (tFec));
    MEMSET (&TempNextHop, LDP_ZERO, sizeof (tGenU4Addr));

    LDP_GET_PRFX_LEN (pRtInfo->u4DestMask, u1PrefLen);

    Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
    Fec.u2AddrFmly = IPV4;
    Fec.u1PreLen = u1PrefLen;
    LDP_IPV4_U4_ADDR (Fec.Prefix) = pRtInfo->u4DestNet;

    LDP_IPV4_U4_ADDR (TempNextHop.Addr) = pRtInfo->u4NextHop;
    TempNextHop.u2AddrType = IPV4;

    if (LdpGetPeerSession (&TempNextHop, pRtInfo->u4RtIfIndx,
                           &pTempLdpSession, u2IncarnId) == LDP_FAILURE)
    {
        u1IsEgress = LDP_TRUE;
    }

    pEntityList = &LDP_ENTITY_LIST (u2IncarnId);

    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
        {
            pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

            if (pLdpSession == pTempLdpSession)
            {
                break;
            }

            SSN_GET_SSN_STATE (pLdpSession, u1SsnState);
            if ((u1SsnState == LDP_SSM_ST_OPERATIONAL) &&
                (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT))
            {
                pLspUpCtrlBlock = NULL;
                LdpCreateUpstrCtrlBlock (&pLspUpCtrlBlock);
                if (pLspUpCtrlBlock == NULL)
                {
                    return;
                }
                UPSTR_LBL_TYPE (pLspUpCtrlBlock) =
                    SSN_GET_LBL_TYPE (pLdpSession);
                if (UPSTR_INCOM_IFINDX (pLspUpCtrlBlock) == 0)
                {
                    UPSTR_INCOM_IFINDX (pLspUpCtrlBlock) =
                        LdpSessionGetIfIndex (pLdpSession, Fec.u2AddrFmly);
                }
                UPSTR_USSN (pLspUpCtrlBlock) = pLdpSession;
                UPSTR_STATE (pLspUpCtrlBlock) = LDP_DU_UP_LSM_ST_IDLE;
                MEMCPY (&UPSTR_FEC (pLspUpCtrlBlock), &Fec, sizeof (tFec));
                /* add upctlblk to upctlblk list of the curr ssn */
                TMO_SLL_Add ((tTMO_SLL *) (&(SSN_ULCB (pLdpSession))),
                             (tTMO_SLL_NODE *)
                             (&UPSTR_DSSN_LIST_NODE (pLspUpCtrlBlock)));
                /* invoke up st m/c after setting the event */
                if ((u1IsEgress == LDP_TRUE) ||
                    (LDP_LABEL_ALLOC_METHOD (u2IncarnId) ==
                     LDP_INDEPENDENT_MODE))
                {
                    u1Event = LDP_DU_UP_LSM_EVT_INT_DN_MAP;
                    LdpDuUpSm (pLspUpCtrlBlock, u1Event, &LdpMsgInfo);
                }
            }                    /* ssn state operational */
        }                        /* ssn scan */
    }                            /* enty scan */
}

/*****************************************************************************/
/* Function Name : LdpDuDnGetInstalledFtnCtrlBlock                           */
/* Description   : This routine is called to check Hardware status of        */
/*                 corresponding  ContrlBlock for given FEC                  */
/* Input(s)      : Fec  - contains the new Fec                               */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
tLspCtrlBlock      *
LdpDuDnGetInstalledFtnCtrlBlock (tFec Fec)
{
    tLdpSession        *pLdpSession = NULL;
    tTMO_SLL           *pEntityList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT1               u1SsnState;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;

    pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
        {
            if ((pLdpPeer->pLdpSession) == NULL)
            {
                continue;
            }
            /* Target sessions also we need to
             * consider for LDP over RSVP case */
            pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

            if ((LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
                 == LDP_TRUE) &&
                (SSN_GET_ENTITY (pLdpSession)->OutStackTnlInfo.u4TnlId ==
                 LDP_ZERO))
            {
                continue;
            }

            SSN_GET_SSN_STATE (pLdpSession, u1SsnState);
            if ((u1SsnState != LDP_SSM_ST_OPERATIONAL) ||
                (SSN_ADVTYPE (pLdpSession) != LDP_DSTR_UNSOLICIT))
            {
                continue;
            }

            TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                              pTempLdpSllNode, tTMO_SLL_NODE *)
            {
                pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                if (LdpCompareFec (&LCB_FEC (pLspCtrlBlk), &Fec) == LDP_EQUAL)
                {
                    if (LCB_MLIB_UPD_STATUS (pLspCtrlBlk) ==
                        LDP_DU_DN_MLIB_UPD_DONE)
                    {
                        return pLspCtrlBlk;
                    }

                }

            }

        }
    }

    return NULL;
}

#if 0
#ifdef LDP_GR_WANTED
VOID
LdpDuDnEstIntRefresh (tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pLdpMsgInfo)
{

    UINT4               u4NextHopAddr = 0;
    UINT4               u4DestMask = 0;
    UINT2               u2PvCount = 0;
    UINT1               u1HopCount = 0;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT1               u1Event = LDP_DU_UP_LSM_EVT_INT_DN_MAP;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tTMO_SLL           *pLspUpCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlock);
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    UINT4               u4RtPort = 0;

    LDP_GET_MASK_FROM_PRFX_LEN ((LCB_FEC (pLspCtrlBlock).u1PreLen), u4DestMask);

    if (LdpIsLoopFound (LDP_MSG_HC_PTR (pLdpMsgInfo),
                        LDP_MSG_PV_PTR (pLdpMsgInfo), (&u2PvCount),
                        (&u1HopCount), u2IncarnId,
                        LCB_DSSN (pLspCtrlBlock), FALSE) == LDP_TRUE)
    {
        LDP_DBG (GRACEFUL_DEBUG,
                 "LdpDuDnEstIntRefresh: SOMETHING UNEXPECTED HAPPENED\n");
    }

    if (LdpIpGetExactRoute ((LCB_FEC (pLspCtrlBlock).u4Prefix), u4DestMask,
                            &u4RtPort, &u4NextHopAddr) == LDP_IP_FAILURE)
    {
        LDP_DBG (GRACEFUL_DEBUG,
                 "LdpDuDnEstIntRefresh: IP Get Exact Route Not Available\n");
    }

    if (LdpIsSsnForFec (u4NextHopAddr, pLspCtrlBlock->u4OutIfIndex,
                        LCB_DSSN (pLspCtrlBlock)) == LDP_TRUE)
    {
        if (pLdpMsgInfo->u1IsLblChanged == LDP_TRUE)
        {
            /* Give Map Event to the control block */
            /* In this FTN will be modified and if Loop related params 
               or Label is changed, Upstream control blocks will be notified */
            LdpDuDnSm (pLspCtrlBlock, LDP_DU_DN_LSM_EVT_LDP_MAP, pLdpMsgInfo);
            pLspCtrlBlock->u1StaleStatus = LDP_FALSE;
            return;
        }
        else
        {
            LDP_MSG_PV_COUNT (pLdpMsgInfo) = (UINT1) u2PvCount;
            LDP_MSG_HC (pLdpMsgInfo) = u1HopCount;

            /* Check if Loop Detection params are changed, Give internal map to 
             * the upstream control blocks, No need to MODIFY the FTN */
            if ((LCB_HCOUNT (pLspCtrlBlock) != u1HopCount) ||
                (LDP_MSG_PV_PTR (pLdpMsgInfo) != NULL))
            {
                TMO_DYN_SLL_Scan (pLspUpCtrlBlkList, pSllNode,
                                  pTempSllNode, tTMO_SLL_NODE *)
                {
                    pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);
                    LdpInvCorrStMh (UPSTR_USSN (pLspUpCtrlBlock),
                                    pLspUpCtrlBlock, u1Event, pLdpMsgInfo,
                                    LDP_TRUE);
                }
            }
        }
    }

    LCB_HCOUNT (pLspCtrlBlock) = u1HopCount;
    pLspCtrlBlock->pu1PVTlv = LDP_MSG_PV_PTR (pLdpMsgInfo);
    pLspCtrlBlock->u1PVCount = (UINT1) u2PvCount;

    pLspCtrlBlock->u1StaleStatus = LDP_FALSE;
}
#endif
#endif
