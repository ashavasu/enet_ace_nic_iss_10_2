/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpssnd.c,v 1.38 2016/10/28 08:09:27 siva Exp $
 *
 * Description: This file consists of routines for sending the
 *              Hello and Session realted messages.
 ********************************************************************/
#include "ldpincs.h"

/*****************************************************************************/
/* Function Name : LdpSendHelloMsg                                           */
/* Description   : This routine constructs and sends the Hello Message to all*/
/*                 local peers(multicast pkt) and remote peer associated to  */
/*                 a particular Ldp Entity.                                  */
/* Input(s)      : pLdpEntity - Points to the Ldp Entity for which the Hello */
/*                 Messages are being generated.                             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
UINT1
LdpSendHelloMsg (tLdpEntity * pLdpEntity)
{
    UINT2               u2MsgLen = 0;
    UINT4               u4HTime = 0;
    tLdpPeer           *pLdpPeer = NULL;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLdpIfTableEntry   *pIfEntry = NULL;
    /* MPLS_IPv6 add start*/ 
    tGenU4Addr      IfAddr;
    tGenAddr      AllRouterAddr;
#ifdef MPLS_IPV6_WANTED
    tIp6Addr        LdpZeroV6Addr;
#endif
    /* MPLS_IPv6 add end*/ 	
    UINT4               u4IfAddr = LDP_ZERO;
    UINT4               u4SrcAddr = LDP_ZERO;
    UINT4               u4RouterAddr = LDP_ZERO;
    /* MPLS_IPv6 add start*/ 
    UINT1                u1InterfaceType = LDP_IFTYPE_INVALID;
#ifdef MPLS_IPV6_WANTED
    UINT1               u1GlbUniAddrMatched = LDP_FALSE;
#endif
    MEMSET(&IfAddr, 0, sizeof(IfAddr));
    MEMSET(&AllRouterAddr, 0, sizeof(AllRouterAddr));
#ifdef MPLS_IPV6_WANTED
    MEMSET(&LdpZeroV6Addr, 0, sizeof(LdpZeroV6Addr));
#endif
    /* MPLS_IPv6 add end*/ 
    if (LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE)
    {
        LDP_DBG (LDP_SSN_PRCS,
                 "SESSION: Entity NOT Active , Hello Msg NOT Sent\n");
        return LDP_FAILURE;
    }

#ifdef LDP_GR_WANTED
    if((gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus ==
                LDP_GR_SHUT_DOWN_IN_PROGRESS) &&
            (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) == LDP_GR_CAPABILITY_FULL))
    {
        LDP_DBG (GRACEFUL_DEBUG,"LdpSendHelloMsg: GR Restore is in Progress "
                "LDP Hello Message not sent\r\n");

		pLdpEntity->HelloTimer.u4Event = LDP_HELLO_TMR_EXPRD_EVENT;
		/* Assigning the LDP Entity to which this timer relates to */
		pLdpEntity->HelloTimer.pu1EventInfo = (UINT1 *) pLdpEntity;

		u4HTime = (UINT4) pLdpEntity->u2HelloHoldTime;
		LDP_GET_HELLO_FREQ (u4HTime);

		if (TmrStartTimer (LDP_TIMER_LIST_ID,
					(tTmrAppTimer *) & (pLdpEntity->HelloTimer.AppTimer),
					(LDP_CONVERT_MSEC_TO_STUPS (u4HTime))) == TMR_FAILURE)
		{
			LDP_DBG1 (LDP_ADVT_TMR,
					"ADVT: Failed to start timer inside the function %s \n",
					__func__);
		}

		return LDP_SUCCESS;
	}
#endif

    LDP_DBG4 (LDP_SSN_PRCS,
              "\rHello to sent for Local LDP Entity : %d.%d.%d.%d\n",
              pLdpEntity->LdpId[0], pLdpEntity->LdpId[1],
              pLdpEntity->LdpId[2], pLdpEntity->LdpId[3]);

    /* Check for the Entity if it's configured for the remote peer */
    if ((pLdpEntity->u1TargetFlag == LDP_FALSE) &&
        (MsrGetRestorationStatus () == ISS_FALSE))
    {
        /* Construct and send Basic Hello Messages */
        TMO_SLL_Scan (&(pLdpEntity->IfList), pIfEntry, tLdpIfTableEntry *)
        {
            
	     /* MPLS_IPv6 add start*/ 		
            u1InterfaceType = pIfEntry->u1InterfaceType;
#ifdef MPLS_IPV6_WANTED
	     if(LDP_IFTYPE_IPV6 == u1InterfaceType || LDP_IFTYPE_DUAL == u1InterfaceType)
	     {
	         AllRouterAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
		  IfAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
		  if (pIfEntry->u1OperStatus != LDP_IF_OPER_ENABLE)
                {
                    LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Interface to send Basic IPV6 LDP Hello %x\n"
                          " is DOWN", pIfEntry->u4IfIndex);
                    continue;
                }
		  if ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) == TRUE) &&
			   ( LDP_ENTITY_IPV6TRANSPORT_ADDRKIND (pLdpEntity)== LOOPBACK_ADDRESS_TYPE) &&
				  (pLdpEntity->bIsIpv6TransAddrIntfUp == LDP_FALSE))
		  {
			  LDP_DBG1 (LDP_SSN_PRCS,
					  "SESSION: Transport Interface to send Basic IPV6 LDP Hello %x\n"
					  " is DOWN", pLdpEntity->u4Ipv6TransIfIndex);
			  continue;
		  }
	         LDP_DBG1 (LDP_SSN_PRCS,
                 "SESSION: Interface Type is %x, will construct IPV6 Hello\n",u1InterfaceType);
                MEMCPY(LDP_IPV6_ADDR(IfAddr.Addr),pIfEntry->LnklocalIpv6Addr.u1_addr, LDP_IPV6ADR_LEN);
                LDP_DBG (LDP_SSN_PRCS,
                         "V6 Link SendHello: Link Local Addr would be used as Source Address\n");
                if (LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) == INTERFACE_ADDRESS_TYPE)
                {
                    if(LDP_ZERO != MEMCMP(pIfEntry->GlbUniqueIpv6Addr.u1_addr, LdpZeroV6Addr.u1_addr, LDP_IPV6ADR_LEN))
                    {
                        MEMCPY(LDP_IPV6_ADDR(IfAddr.Addr), pIfEntry->GlbUniqueIpv6Addr.u1_addr, LDP_IPV6ADR_LEN);
                        LDP_DBG (LDP_SSN_PRCS,
                         "V6 Link SendHello: Trans Kind- Interface :Glbl Unicast Addr to be preferred as Src Addr \n");
                    }
                    else if((LDP_ZERO != MEMCMP(pIfEntry->SitelocalIpv6Addr.u1_addr, LdpZeroV6Addr.u1_addr, LDP_IPV6ADR_LEN)))
                    {
                        MEMCPY(LDP_IPV6_ADDR(IfAddr.Addr), pIfEntry->SitelocalIpv6Addr.u1_addr, LDP_IPV6ADR_LEN);
                        LDP_DBG (LDP_SSN_PRCS,
                         "V6 Link SendHello: Trans Kind- Interface :Site Local Addr to be preferred as Src Addr \n");
                    }
                }
	         if(LDP_FAILURE == LdpConstructIpv6HelloMsg( pLdpEntity, LDP_FALSE, &pMsg, 
		 	&u2MsgLen, IfAddr.Addr.Ip6Addr))
	         {
                    LDP_DBG2 (LDP_SSN_PRCS,
                          "SESSION: Error constructing Basic IPV6 Hello Msg"
                          " for the Interface with interface index %x having IP %s\n", pIfEntry->u4IfIndex, Ip6PrintAddr(&(IfAddr.Addr.Ip6Addr)));
                    continue;
	         }
	         if(LDP_FAILURE != INET_ATON6(LDP_ALL_ROUTERS_V6ADDR, & AllRouterAddr.Addr.Ip6Addr))
	         {
                    if (LdpSendMsg (pMsg, u2MsgLen, pLdpEntity, LDP_TRUE,
                            AllRouterAddr, IfAddr, pIfEntry->u4IfIndex) == LDP_FAILURE)
                    {
                        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                        LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Failed to send Basic IPV6 Hello Msg on the"
                          " interface %x\n", pIfEntry->u4IfIndex);
                        continue;
                    }
	     	  }
		  else
		  {
                    LDP_DBG (LDP_IF_MISC,
                                          "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
		  }
		  LDP_DBG1 (LDP_SSN_TX, "SESSION: Basic IPV6 Hello Msg sent on the"
                      " interface %x\n", pIfEntry->u4IfIndex);
	     }
	     if(LDP_IFTYPE_IPV4 == u1InterfaceType || LDP_IFTYPE_DUAL == u1InterfaceType)
	     {
#endif
                AllRouterAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
		  IfAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
		  u4RouterAddr = LDP_ALL_ROUTERS_ADDR;
		  MEMCPY(LDP_IPV4_ADDR(AllRouterAddr.Addr), &u4RouterAddr, LDP_IPV4ADR_LEN);
		  /*AllRouterAddr.Addr.u4Addr = LDP_ALL_ROUTERS_ADDR;*/
	         /* MPLS_IPv6 mod end*/
                MEMCPY(&IfAddr.Addr.u4Addr,pIfEntry->IfAddr,LDP_IPV4ADR_LEN);
                IfAddr.Addr.u4Addr = OSIX_NTOHL (IfAddr.Addr.u4Addr);
                if (pIfEntry->u1OperStatus != LDP_IF_OPER_ENABLE)
                {
                    LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Interface to send Basic IPV4 LDP Hello %x\n"
                          " is DOWN", IfAddr.Addr.u4Addr);
                    continue;
                }
		if ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV(pLdpEntity) == TRUE) &&
			(LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) == LOOPBACK_ADDRESS_TYPE) &&
				(pLdpEntity->bIsTransAddrIntfUp == LDP_FALSE))
		{
			LDP_DBG1 (LDP_SSN_PRCS,
					"SESSION: Transport Interface to send Basic IPV4 LDP Hello %x\n"
					" is DOWN", pLdpEntity->u4TransIfIndex);
			continue;
		}
                if (LdpConstructHelloMsg (pLdpEntity, LDP_FALSE, &pMsg,
                                      &u2MsgLen, IfAddr.Addr.u4Addr) == LDP_FAILURE)
                {
                    LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Error constructing Basic IPV4 Hello Msg"
                          " for the Interface with address %x\n", IfAddr.Addr.u4Addr);
                    continue;
                }

                /* Send the Basic Hello Pkt to 'All Routers on subnet Address' */
                if (LdpSendMsg (pMsg, u2MsgLen, pLdpEntity, LDP_TRUE,
                            AllRouterAddr, IfAddr, LDP_ZERO) == LDP_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Failed to send Basic IPV4 Hello Msg on the"
                          " interface with address %x\n", IfAddr.Addr.u4Addr);
                    continue;
                }

                LDP_DBG1 (LDP_SSN_TX, "SESSION: Basic IPV4 Hello Msg sent on the"
                      " interface with address %x\n", IfAddr.Addr.u4Addr);
#ifdef MPLS_IPV6_WANTED
	     }
#endif
            /* MPLS_IPv6 add end*/ 
            /* Update the Basic Hello Statistics */
            ++((pLdpEntity)->LdpStatsList.u4BHelloPktsSent);
        }
    }

    if ((pLdpEntity->u1TargetFlag == LDP_TRUE) &&
        (MsrGetRestorationStatus () == ISS_FALSE))
    {
        /* As per the draft ldp-11.txt, an entity will have only one peer in its
         * peer list, if the peer is a targeted peer.
         */
        pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
        if (pLdpPeer == NULL)
        {
            LDP_DBG (LDP_SSN_PRCS, "SESSION: LDP Peer is NULL\n");
            return LDP_FAILURE;
        }
        /* Failure check is intentionally skipped since 
         * Source Address for the Destination address may be available at
         * a later point of time. So, failure is skipped and hello timer
         * for targetted hello is started below. */
#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6 == pLdpPeer->NetAddr.u2AddrType)
        {
            LDP_DBG1(LDP_SSN_PRCS, "SESSION: Fetching Source Address "
                  " for the peer Targeted Addr %s\n", Ip6PrintAddr(&(pLdpPeer->NetAddr.Addr.Ip6Addr)));
            if(NETIPV6_FAILURE == NetIpv6GetSrcAddrForDestAddr(LDP_ZERO,&(pLdpPeer->NetAddr.Addr.Ip6Addr), &(IfAddr.Addr.Ip6Addr)))
            {
                LDP_DBG (LDP_SSN_PRCS, "Get source address to use"
                     "for Dest Addr failed\n");
            }
	     LDP_DBG1 (LDP_SSN_PRCS, "Source Add Received NetIPv6GetSrcForDest %s\n",
		 	Ip6PrintAddr(&(IfAddr.Addr.Ip6Addr)));
	     TMO_SLL_Scan (&(pLdpEntity->IfList), pIfEntry, tLdpIfTableEntry *)
            {
                if (pIfEntry->u1OperStatus != LDP_IF_OPER_ENABLE)
                {
                    LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Interface to send Basic LDP Hello %x\n"
                          " is DOWN", u4IfAddr);
                    continue;
                }
		if ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) == TRUE) &&
			( LDP_ENTITY_IPV6TRANSPORT_ADDRKIND (pLdpEntity) == LOOPBACK_ADDRESS_TYPE) &&
				(pLdpEntity->bIsIpv6TransAddrIntfUp == LDP_FALSE))
		{
			LDP_DBG1 (LDP_SSN_PRCS,
					"SESSION: Transport Interface to send Targeted IPV6 LDP Hello %x\n"
					" is DOWN", pLdpEntity->u4Ipv6TransIfIndex);
			continue;
		}
	         if((LDP_ZERO != MEMCMP(LdpZeroV6Addr.u1_addr, pIfEntry->GlbUniqueIpv6Addr.u1_addr, LDP_IPV6ADR_LEN)) &&
                           (LDP_ZERO != MEMCMP(LDP_IPV6_ADDR(IfAddr.Addr), pIfEntry->GlbUniqueIpv6Addr.u1_addr, LDP_IPV6ADR_LEN)))
	         {
                    /* Targeted Hello Source Addr Should be Global Unicast Address*/
                    LDP_DBG1 (LDP_SSN_PRCS, "Compared Global Address over interface %s", 
                         Ip6PrintAddr(&(pIfEntry->GlbUniqueIpv6Addr)));

                    continue;
	         }
                /* All the conditions has been satisfied. So, break from here. */
                LDP_DBG (LDP_SSN_TX, "Ldp Send Hello: Glbl Unicast Address Matched For Targeted Hello \n");
                u1GlbUniAddrMatched = LDP_TRUE;
                break;
            }
            if(LDP_TRUE != u1GlbUniAddrMatched)
            {
                LDP_DBG (LDP_SSN_PRCS, "Send V6 Target Hello ERROR: Addr Rcvd does not match with Glbl Unicast Addr  \n");
                /*return LDP_FAILURE;*/
            }
	     if ((pIfEntry != NULL) || (pLdpEntity->bIsIntfMapped == LDP_FALSE))
	     {
	         IfAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
	         AllRouterAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
	         MEMCPY(LDP_IPV6_ADDR(AllRouterAddr.Addr),LDP_IPV6_ADDR(pLdpPeer->NetAddr.Addr), LDP_IPV6ADR_LEN);
                if (LdpConstructIpv6HelloMsg (pLdpEntity, LDP_TRUE, &pMsg, &u2MsgLen,
                                  IfAddr.Addr.Ip6Addr) == LDP_FAILURE)
                {
                    LDP_DBG1 (LDP_SSN_PRCS,
                      "SESSION: Error forming V6 Targetted Hello Msg "
                      " for the interface %x\n", u4SrcAddr);
                }
                else if (LdpSendMsg (pMsg, u2MsgLen, pLdpEntity, LDP_TRUE,
                            AllRouterAddr,
                             IfAddr, LDP_IP_INVALID_INDEX) == LDP_FAILURE)
                {
                        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    LDP_DBG1 (LDP_SSN_TX,
                     "SESSION: Failed to send V6 Targetted Hello Over %s \n", Ip6PrintAddr(&(IfAddr.Addr.Ip6Addr)));
                }
                else
                {
                    LDP_DBG (LDP_SSN_TX,
                     "SESSION: Sent V6 Targetted Hello Success \n");
                    /* Update the Target Hello Statistics */
                    ++((pLdpEntity)->LdpStatsList.u4THelloPktsSent);
                 }
            }
    	 }
	 else
	 {
#endif
            LDP_DBG1 (LDP_SSN_PRCS, "SESSION: Fetching IPV4 Source Address "
                  " for the peer %#x\n",
                  OSIX_NTOHL (*(UINT4 *) (VOID *) &(pLdpPeer->NetAddr)));
            if (NetIpv4GetSrcAddrToUseForDest (OSIX_NTOHL (*(UINT4 *) (VOID *)
                                                       &(pLdpPeer->NetAddr)),
                                           &u4SrcAddr) == NETIPV4_FAILURE)
            {
                LDP_DBG (LDP_SSN_PRCS, "Get source address to use"
                     "for Dest failed\n");
            }
	     LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Source Add Rcvd:%x\n", u4SrcAddr);
	     TMO_SLL_Scan (&(pLdpEntity->IfList), pIfEntry, tLdpIfTableEntry *)
            {
                 MEMCPY(&u4IfAddr,pIfEntry->IfAddr,LDP_IPV4ADR_LEN);
                u4IfAddr = OSIX_NTOHL(u4IfAddr);
                if (pIfEntry->u1OperStatus != LDP_IF_OPER_ENABLE)
                {
                    LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Interface to send Basic LDP Hello %x\n"
                          " is DOWN", u4IfAddr);
                    continue;
                }
		if ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV(pLdpEntity) == TRUE) &&
			(LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) == LOOPBACK_ADDRESS_TYPE) &&
				(pLdpEntity->bIsTransAddrIntfUp == LDP_FALSE))
		{
			LDP_DBG1 (LDP_SSN_PRCS,
					"SESSION: Transport Interface to send Targeted IPV4 LDP Hello %x\n"
					" is DOWN",pLdpEntity->u4TransIfIndex);
			continue;
		}
                if ((u4IfAddr != LDP_ZERO) && (u4IfAddr != u4SrcAddr))
                {
                    continue;
                }
                /* All the conditions has been satisfied. So, break from here. */
                break;
            }
	     if ((pIfEntry != NULL) || (pLdpEntity->bIsIntfMapped == LDP_FALSE))
            {
                IfAddr.Addr.u4Addr = u4SrcAddr;
	         IfAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
	         AllRouterAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
	         MEMCPY(LDP_IPV4_ADDR(AllRouterAddr.Addr),LDP_IPV4_ADDR(pLdpPeer->NetAddr.Addr), LDP_IPV4ADR_LEN);
                if (LdpConstructHelloMsg (pLdpEntity, LDP_TRUE, &pMsg, &u2MsgLen,
                                      u4SrcAddr) == LDP_FAILURE)
                {
                     LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Error forming Targetted Hello Msg "
                          " for the interface %x\n", u4SrcAddr);
                }
                else if (LdpSendMsg (pMsg, u2MsgLen, pLdpEntity, LDP_TRUE,
                                 AllRouterAddr,
                                 IfAddr, LDP_ZERO) == LDP_FAILURE)
                {
                        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                     LDP_DBG2 (LDP_SSN_TX,
                          "SESSION: Failed to send Targetted Hello for %#x over"
                          " %x\n",
                          OSIX_NTOHL (*(UINT4 *) (VOID *) &(pLdpPeer->NetAddr)),
                          IfAddr.Addr.u4Addr);
                }
                else
                {
                    LDP_DBG2 (LDP_SSN_TX, "SESSION: Targeted Hello sent for"
                          " %#x over %x\n",
                          OSIX_NTOHL (*(UINT4 *) (VOID *) &(pLdpPeer->NetAddr)),
                          u4SrcAddr);
                /* Update the Target Hello Statistics */
                    ++((pLdpEntity)->LdpStatsList.u4THelloPktsSent);
                }
            }
#ifdef MPLS_IPV6_WANTED
	 }
#endif
    }
    /* Hello timer is restarted */
    pLdpEntity->HelloTimer.u4Event = LDP_HELLO_TMR_EXPRD_EVENT;
    /* Assigning the LDP Entity to which this timer relates to */
    pLdpEntity->HelloTimer.pu1EventInfo = (UINT1 *) pLdpEntity;

    u4HTime = (UINT4) pLdpEntity->u2HelloHoldTime;
    LDP_GET_HELLO_FREQ (u4HTime);
    if(pLdpEntity->u2HelloHoldTime != LDP_INFINITE_HTIME)
    {

    if (TmrStartTimer (LDP_TIMER_LIST_ID,
                       (tTmrAppTimer *) & (pLdpEntity->HelloTimer.AppTimer),
                       (LDP_CONVERT_MSEC_TO_STUPS (u4HTime))) == TMR_FAILURE)
    {
        LDP_DBG1 (LDP_ADVT_TMR,
                  "ADVT: Failed to start timer inside the function %s \n",
                  __func__);
    }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpConstructHelloMsg                                      */
/* Description   : This routine constructs and copies the hello message on to*/
/*                 the buffer chain.                                         */
/*                                                                           */
/* Input(s)      : pLdpEntity - Points to the Ldp Entity for which the hello */
/*                              Message is being constructed.                */
/*                 u1IsTrgtHello - A value 'LDP_TRUE' specifies that the     */
/*                                 Msg to be constructed is Targetted Hello. */
/*                                 A value 'LDP_FALSE' specifies that the Msg*/
/*                                 to be constructed is Basic Hello Msg.     */
/*                 u4IfAddr   - Address of the Interface over which LDP      */
/*                              hello is sent. Used to fill the transport    */
/*                              address in Transport Address TLV of LDP Hello*/
/*                              Message.                                     */
/* Output(s)     : ppMsg - Points to the constructed Hello message.          */
/*                 pu2MsgLen - Length of the Hello Msg, excluding Msg HdrLen */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
UINT1
LdpConstructHelloMsg (tLdpEntity * pLdpEntity, UINT1 u1IsTrgtHello,
                      tCRU_BUF_CHAIN_HEADER ** ppMsg, UINT2 *pu2MsgLen,
                      UINT4 u4IfAddr)
{

    /* Initialising the Tlv Header */
    tCmnHelloParmsTlv   CmnHelloParmsTlv =
        { {OSIX_HTONS (LDP_CMN_HELLO_PARM_TLV),
           OSIX_HTONS ((LDP_HELLO_HOLD_TIME_LEN + 2))}, 0, 0
    };
    tTransportAddrTlv   TrAddrTlv;
    tConfSeqNumTlv      CSNumTlv = { {OSIX_HTONS (LDP_CONF_SEQNUMB_TLV),
                                      OSIX_HTONS (LDP_CS_NUM_LEN)}, 0
    };
    tLdpMsgHdrAndId     MsgHdrAndId;
    UINT4               u4Offset = 0;
    UINT2               u2OptParamLen = 0;
    UINT2               u2MandParamLen = (LDP_MSG_ID_LEN + LDP_TLV_HDR_LEN +
                                          LDP_HELLO_HOLD_TIME_LEN + 2);
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;

    if (LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) == LOOPBACK_ADDRESS_TYPE)
    {
        u4IfAddr = pLdpEntity->u4TransportAddress;
    }

    u4IfAddr = OSIX_HTONL (u4IfAddr);

    if ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) == LDP_TRUE) &&
		(LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) == LOOPBACK_ADDRESS_TYPE) &&
        (pLdpEntity->bIsTransAddrIntfUp == LDP_TRUE))
    {
        u2OptParamLen = LDP_TLV_HDR_LEN + LDP_TR_ADDR_LEN;
    }
    if (pLdpEntity->u1ConfSeqNumTlvEnable == TRUE)
    {
        u2OptParamLen += (LDP_TLV_HDR_LEN + LDP_CS_NUM_LEN);
    }

    pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2OptParamLen + u2MandParamLen),
                                         LDP_PDU_HDR_LEN);
    *pu2MsgLen = (UINT2) (u2OptParamLen + u2MandParamLen);
    *ppMsg = pMsg;
    if (*ppMsg == NULL)
    {
        LDP_DBG (LDP_SSN_MEM, "SESSION: Can't Allocate Buf for Hello Msg \n");
        return LDP_FAILURE;
    }

    /*construct and copy Hello Msg Header and Msg Id */
    MsgHdrAndId.u2MsgType = OSIX_HTONS (LDP_HELLO_MSG);
    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2OptParamLen + u2MandParamLen));

    INC_LDP_ENTY_MSG_ID (pLdpEntity);
    MsgHdrAndId.u4MsgId = OSIX_HTONL (pLdpEntity->u4MessageId);

    /*
       MsgHdrAndId.u4MsgId = OSIX_HTONL((pLdpEntity->u4MessageId)++);
     */

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying Hello MsgHdr and MsgId "
                 "to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);
    /* Construct and Copy Common Hello Parameters Tlv */
    if (u1IsTrgtHello == LDP_TRUE)
    {
        CmnHelloParmsTlv.u2TRField =
            OSIX_HTONS ((LDP_TLV_T_BIT | LDP_TLV_R_BIT | LDP_RSVD_FLD));
    }
    else
    {
        CmnHelloParmsTlv.u2TRField = 0;
    }
    CmnHelloParmsTlv.u2HoldTime = OSIX_HTONS (pLdpEntity->u2HelloConfHoldTime);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &CmnHelloParmsTlv, u4Offset,
                                   (LDP_TLV_HDR_LEN + LDP_HELLO_HOLD_TIME_LEN +
                                    LDP_HELLO_T_R_RSVD_LEN)) == CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying CmnHelloParmsTlv to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset +=
        (LDP_TLV_HDR_LEN + LDP_HELLO_HOLD_TIME_LEN + LDP_HELLO_T_R_RSVD_LEN);

    /* construct and copy Transport Address Tlv */
    if ((LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) == LDP_TRUE) &&
		(LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) == LOOPBACK_ADDRESS_TYPE) &&
        (pLdpEntity->bIsTransAddrIntfUp == LDP_TRUE))
    {
        TrAddrTlv.TlvHdr.u2TlvType = OSIX_HTONS (LDP_IPV4_TRANSPORT_ADDR_TLV);
        TrAddrTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_IPV4_TR_ADDR_LEN);
        MEMCPY ((UINT1 *) &(TrAddrTlv.TAddr.au1Ipv4Addr),
                (UINT1 *) &u4IfAddr, LDP_LSR_ID_LEN);
		
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &TrAddrTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_TR_ADDR_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_SSN_MEM,
                     "SESSION: Error copying TrAddrTlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_TR_ADDR_LEN);
    }

    if (pLdpEntity->u1ConfSeqNumTlvEnable == TRUE)
    {
        /* construct and copy Configuration Sequence Number Tlv */
        CSNumTlv.u4ConfSeqNum = OSIX_HTONL (pLdpEntity->u4ConfSeqNum);
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &CSNumTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_CS_NUM_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_SSN_MEM,
                     "SESSION: Error copying Conf Seq Number Tlv "
                     "to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }
    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendKeepAliveMsg                                       */
/* Description   : This routine constructs and sends Keep Alive Message on   */
/*                 the session that is passed to it.                         */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session on which the Keep   */
/*                                 Alive  message has to be sent.            */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
UINT1
LdpSendKeepAliveMsg (tLdpSession * pSessionEntry)
{
    tLdpMsgHdrAndId     MsgHdrAndId;
    UINT2               u2MandParamLen = LDP_MSG_ID_LEN;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    UINT4               u4Offset = 0;
    tLdpId              PeerLdpId;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    MEMSET(&ConnId, LDP_ZERO, sizeof(tGenAddr));
    MEMSET(&LdpIfAddrZero, LDP_ZERO, sizeof(tGenU4Addr));
    pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2MandParamLen), LDP_PDU_HDR_LEN);

    if (pMsg == NULL)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Can't Allocate Buf for Keep Alive Msg \n");
        return LDP_FAILURE;
    }

    /*construct and copy Keep Alive Msg Header and Msg Id */
    MsgHdrAndId.u2MsgType = OSIX_HTONS (LDP_KEEP_ALIVE_MSG);
    MsgHdrAndId.u2MsgLen = OSIX_HTONS (u2MandParamLen);
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pSessionEntry));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying KeepAlive MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pSessionEntry);
    LDP_DBG4 (LDP_SSN_TX,
              "SESSION: Sending Keep Alive Msg To %d.%d.%d.%d \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);
    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    pSessionEntry->pLdpPeer->pLdpEntity, LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_SSN_TX, "SESSION: Failed to send Ldp Keep Alive Msg\n");
        return LDP_FAILURE;
    }

    /* Start a timer for sending periodic KeepAlive Msgs */
    pSessionEntry->KeepAliveTimer.u4Event = LDP_KALIVE_TMR_EVENT;
    /* Assign Session on which the Keep Alive Msg need to be sent on 
     * expiry of this timer */
    pSessionEntry->KeepAliveTimer.pu1EventInfo = (UINT1 *) pSessionEntry;
    if (TmrStartTimer (LDP_TIMER_LIST_ID,
                       (tTmrAppTimer *) & (pSessionEntry->KeepAliveTimer.
                                           AppTimer),
                       (SSN_GET_KALIVE_MSG_FREQ (pSessionEntry) *
                        SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
    {
        LDP_DBG1 (LDP_ADVT_TMR,
                  "ADVT: Failed to start timer inside the function %s \n",
                  __func__);
    }

    LDP_DBG (LDP_SSN_TX, "SESSION: KeepAlive Msg sent\n");

    return LDP_SUCCESS;

}

/*****************************************************************************/
/* Function Name : LdpSendInitMsg                                            */
/* Description   : This routine constructs and sends the Ldp Initialization  */
/*                 Message on the session passed to it.                      */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session on which the        */
/*                                 Initialisation message has to be sent.    */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
UINT1
LdpSendInitMsg (tLdpSession * pSessionEntry)
{

    tLdpMsgHdrAndId     MsgHdrAndId;
    tLdpId              PeerLdpId;

    /* Last 10 bytes include the other fields in the 
     * CommonSession Paramters Tlv */
    UINT2               u2MandParamLen = (LDP_MSG_ID_LEN + LDP_TLV_HDR_LEN +
                                          LDP_PRTCL_VER_LEN +
                                          LDP_KA_HOLD_TIME_LEN + 10);

    /* Initialising the message type */
    tAtmSsnParmsTlv     AtmSsnParmsTlv;
    tCmnSsnParmsTlv     CmnSsnParmsTlv;
    tFTSessionTlv       FTSessionTlv;
    UINT2               u2OptParamLen = 0;
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);
    UINT1               u1LblRangesCount = 0;
    tLdpEntity         *pLdpEntity = SSN_GET_ENTITY (pSessionEntry);
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLdpAtmParams      *pLdpAtmParams = NULL;
    tAtmLdpLblRngEntry *pAtmLblRngEntry = NULL;
    tAtmLblRange        AtmLblRange;
    tLdpTrapInfo        LdpTrapInfo;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    UINT4               u4Offset = 0;
    UINT4               u4MNDResv = 0;
    UINT4               u4RemainingTime = 0;
    MEMSET (&FTSessionTlv, LDP_ZERO, sizeof (tFTSessionTlv));
    AtmSsnParmsTlv.AtmSsnTlvHdr.u2TlvType = OSIX_HTONS (LDP_ATM_SSN_PARAM_TLV);
    AtmSsnParmsTlv.AtmSsnTlvHdr.u2TlvLen = 0;
    CmnSsnParmsTlv.CmnSsnParmsTlvHdr.u2TlvType =
        OSIX_HTONS (LDP_CMN_SSN_PARAM_TLV);
    CmnSsnParmsTlv.CmnSsnParmsTlvHdr.u2TlvLen =
        OSIX_HTONS (LDP_CMN_SSN_TLV_VAL_LEN);
    MEMSET(&ConnId, LDP_ZERO, sizeof(tGenAddr));
    MEMSET(&LdpIfAddrZero, LDP_ZERO, sizeof(tGenU4Addr));
    
    if (LDP_IS_ENTITY_TYPE_ATM (pSessionEntry) == LDP_TRUE)
    {
        /* Construct Atm Session Parameter Tlv */
        LDP_GET_ENTITY_ATM_PARAMS (pSessionEntry, pLdpAtmParams);
        if (pLdpAtmParams == NULL)
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Error Atm Params not init for the Entity\n");
            return LDP_FAILURE;
        }

        /* get number of label range components present for this Ldp Entity 
         * size of each label range component is 8 bytes */
        u1LblRangesCount =
            (UINT1) TMO_SLL_Count (&(pLdpAtmParams->AtmLabelRangeList));
        u2OptParamLen += (LDP_TLV_HDR_LEN + 4 + (u1LblRangesCount) * 8);

        AtmSsnParmsTlv.AtmSsnTlvHdr.u2TlvLen = OSIX_HTONS ((4 +
                                                            (u1LblRangesCount) *
                                                            8));
        LDP_SET_M (u4MNDResv, pLdpAtmParams->u1MergeType);
        LDP_SET_N (u4MNDResv, u1LblRangesCount);
        LDP_SET_D (u4MNDResv, pLdpAtmParams->u1VcDirection);

        AtmSsnParmsTlv.u4MNDResv = OSIX_HTONL (u4MNDResv);
    }

    if (gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability != LDP_GR_CAPABILITY_NONE)
    {
        u2OptParamLen += LDP_FT_SSN_TLV_VAL_LEN + LDP_MSG_HDR_LEN;
    }
    pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2MandParamLen + u2OptParamLen),
                                         LDP_PDU_HDR_LEN +
                                         LDP_CMN_SSN_TLV_VAL_LEN);

    if (pMsg == NULL)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Can't Allocate Buf for Session Init Msg\n");
        return LDP_FAILURE;
    }

    /* Construct and copy Ssn Init Msg Header and Msg Id */
    MsgHdrAndId.u2MsgType = OSIX_HTONS (LDP_INIT_MSG);
    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2MandParamLen + u2OptParamLen));
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pSessionEntry));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying Ssn Init MsgHdr & Id to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct and copy Common Session Parameters Tlv */
    CmnSsnParmsTlv.u2ProtclVer = OSIX_HTONS (LDP_PROTO_VER);
    CmnSsnParmsTlv.u2KeepAliveTime = (UINT2)
        OSIX_HTONS (pLdpEntity->u4KeepAliveHoldTime);
    CmnSsnParmsTlv.u1ADResv = (UINT1) ((pLdpEntity->u1LabelDistrType) << 7);

    if ((LDP_LOOP_DETECT (u2IncarnId) == LDP_LOOP_DETECT_CAPABLE_PV)
        || (LDP_LOOP_DETECT (u2IncarnId) == LDP_LOOP_DETECT_CAPABLE_HCPV))
    {
        CmnSsnParmsTlv.u1ADResv =
            (UINT1) (CmnSsnParmsTlv.u1ADResv | ((0x1) << 6));
    }
    CmnSsnParmsTlv.u1PvLimit = pLdpEntity->u1PathVecLimit;
    CmnSsnParmsTlv.u2MaxPduLen = OSIX_HTONS (pLdpEntity->u2MaxPduLen);
    MEMCPY (CmnSsnParmsTlv.RcvrLdpId, pSessionEntry->pLdpPeer->PeerLdpId,
            LDP_MAX_LDPID_LEN);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &CmnSsnParmsTlv, u4Offset,
                                   (LDP_TLV_HDR_LEN +
                                    LDP_CMN_SSN_TLV_VAL_LEN)) == CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying Common Ssn Parms Tlv to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_CMN_SSN_TLV_VAL_LEN);
    /* FT TLV Formation in Initialization Message */
    if (gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability != LDP_GR_CAPABILITY_NONE)
    {
        FTSessionTlv.FTSessionTlvHdr.u2TlvType = OSIX_HTONS (LDP_FT_SSN_TLV);
        FTSessionTlv.FTSessionTlvHdr.u2TlvLen =
            OSIX_HTONS (LDP_FT_SSN_TLV_VAL_LEN);
        FTSessionTlv.u2FTFlags = OSIX_HTONS (LDP_FT_TLV_FLAG);

        if (gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability ==
            LDP_GR_CAPABILITY_HELPER)
        {
            FTSessionTlv.u4FTReconnectTimeout = LDP_ZERO;
            FTSessionTlv.u4FTRecoverTime = LDP_ZERO;
            LDP_DBG (LDP_SSN_PRCS, "SESSION: Configured as Helper. "
                     "Send Reconnect Timeout as ZERO.\n");
        }
        else
        {
            FTSessionTlv.u4FTReconnectTimeout = OSIX_HTONL
                (LDP_CONVERT_TO_MILLI_SECONDS
                 (gLdpInfo.LdpIncarn[u2IncarnId].u2NbrLivenessTime));
            /* This is for RESTARTING Node:
             *             ---------
             * If Reconnect in Progress, Fill the current
             * value of Forwarding Holding Timer.
             * Else send the Value of Max_Recover_Timer.
             */
            /* For the first adjacency the variable would be set to
               LDP_GR_RECONNECT_IN_PROGRESS and for subsequent adjacencies
               the variable would be LDP_GR_RECOVERY_IN_PROGRESS. Once th
               the FWD hold timer expires this would be set to LDP_GR_COMPLETED */

            /* This is for the restarting node */
            if (((gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus ==
                 LDP_GR_RECONNECT_IN_PROGRESS) ||
                 (gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus ==
                 LDP_GR_RECOVERY_IN_PROGRESS)))
            {

                if (TmrGetRemainingTime (LDP_TIMER_LIST_ID,
                                         &(gLdpInfo.LdpIncarn[u2IncarnId].
                                           FwdHoldingTmr.AppTimer),
                                         &u4RemainingTime) == TMR_FAILURE)
                {
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    return LDP_FAILURE;
                }
                FTSessionTlv.u4FTRecoverTime = OSIX_HTONL (LDP_CONVERT_TO_MILLI_SECONDS 
                                                      (u4RemainingTime/SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
            }
            else
            {
                FTSessionTlv.u4FTRecoverTime = OSIX_HTONL
                    (LDP_CONVERT_TO_MILLI_SECONDS
                     (gLdpInfo.LdpIncarn[u2IncarnId].u2MaxRecoveryTime));
            }
            /* Simulate that MPLS Forwarding State has been Lost */
            if (gi4MplsSimulateFailure == LDP_SIM_FAILURE_MPLS_FWD_STATE_LOST)
            {
                FTSessionTlv.u4FTRecoverTime = LDP_ZERO;
            }
        }
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &FTSessionTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN +
                                        LDP_FT_SSN_TLV_VAL_LEN)) == CRU_FAILURE)
        {
            LDP_DBG (LDP_SSN_MEM,
                     "SESSION: Error copying FT Ssn Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_FT_SSN_TLV_VAL_LEN);
    }
    /* Construct and copy Atm Session parameters Tlv, in case of ATM */
    if (LDP_IS_ENTITY_TYPE_ATM (pSessionEntry) == LDP_TRUE)
    {
        if (pLdpAtmParams == NULL)
        {
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Error Atm Params not init for the Entity\n");
            return LDP_FAILURE;
        }

        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AtmSsnParmsTlv,
                                       u4Offset, (LDP_TLV_HDR_LEN + 4)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_SSN_MEM,
                     "SESSION: Error copying AtmSsnParameterTlvHdr "
                     "to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + 4);
        if (pLdpAtmParams == NULL)
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Error Atm Params not init for the Entity\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        TMO_SLL_Scan (&(pLdpAtmParams->AtmLabelRangeList), pAtmLblRngEntry,
                      tAtmLdpLblRngEntry *)
        {
            AtmLblRange.u2MinVpi = OSIX_HTONS (pAtmLblRngEntry->u2LBVpi);
            AtmLblRange.u2MinVci = OSIX_HTONS (pAtmLblRngEntry->u2LBVci);
            AtmLblRange.u2MaxVpi = OSIX_HTONS (pAtmLblRngEntry->u2UBVpi);
            AtmLblRange.u2MaxVci = OSIX_HTONS (pAtmLblRngEntry->u2UBVci);

            if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AtmLblRange,
                                           u4Offset, 8) == CRU_FAILURE)
            {
                LDP_DBG (LDP_SSN_MEM,
                         "SESSION: Error copying Atm label ranges to Buf "
                         "chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            /* size of each Atm Label Range component is 8 bytes */
            u4Offset += 8;
        }
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pSessionEntry);
    LDP_DBG4 (LDP_SSN_TX,
              "SESSION: Sending Init Msg To %d.%d.%d.%d \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);
    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, (UINT2) OSIX_NTOHS (MsgHdrAndId.u2MsgLen),
                    pSessionEntry->pLdpPeer->pLdpEntity, LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_SSN_TX, "SESSION: Failed to send Init Msg\n");
        return LDP_FAILURE;
    }

    /* The notification should be generated each time this threshold
     * is exceeded. A value of 0 (zero) for this object indicates that
     * the SNMP notification will never be generated.
     */
    if (pSessionEntry->pLdpPeer->pLdpEntity->u4FailInitSessThreshold != 0)
    {
        if (pSessionEntry->u4NoOfInitSent >
            pSessionEntry->pLdpPeer->pLdpEntity->u4FailInitSessThreshold)
        {
            LdpTrapInfo.u1TrapType = LDP_TRAP_SESS_THOLD_EXCD;
            LdpTrapInfo.pSession = pSessionEntry;
            LdpTrapInfo.pEntity = pSessionEntry->pLdpPeer->pLdpEntity;
            LdpNotifySnmp (&LdpTrapInfo);
        }
    }
    pSessionEntry->u4NoOfInitSent++;

    LDP_DBG (LDP_SSN_TX, "SESSION: Init Msg sent\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendAddrMsg                                            */
/* Description   : This routine constructs and sends the Ldp Address Message */
/*                 on the session passed to it.                              */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session on which the Address*/
/*                                 Message has to be sent.                   */
/*                 pAddress - Address to be sent.If NULL Send all            */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
UINT1
LdpSendAddrMsg (tLdpSession * pSessionEntry, tIpv4Addr * pAddress)
{
	tTMO_SLL           *pIfList = NULL;
	UINT2               u2MandParamLen = 0;
	UINT4               u4Offset = 0;
	tLdpMsgHdrAndId     MsgHdrAndId;
	tAddrTlv            AddrTlv;
	tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
	tLdpId              PeerLdpId;

	tIpv4Addr           auIpv4LoopbackAddress[SYS_MAX_LOOPBACK_INTERFACES];
	tIpv4Addr           auIpv4PppInterfaceAddress[SYS_MAX_LOOPBACK_INTERFACES];
	UINT2               u2Ipv4LoopBackCount = 0;
	UINT2               u2Ipv4PppInterfaceCount=0;
	UINT4               u4Count=0;
	UINT1               au1ZeroAddr[IPV4_ADDR_LENGTH] = {0};
	/*MPLS_IPv6 add start*/
	tGenAddr     ConnId;
	tGenU4Addr     LdpIfAddrZero;
	/*MPLS_IPv6 add end*/

#if 0
        printf("%s : %d\n",__FUNCTION__,__LINE__);
#endif

	MEMSET ((UINT1 *) auIpv4LoopbackAddress, LDP_ZERO,
            sizeof(auIpv4LoopbackAddress));

	MEMSET ((UINT1 *) auIpv4PppInterfaceAddress, LDP_ZERO,
            sizeof(auIpv4PppInterfaceAddress));
        MEMSET (&ConnId, LDP_ZERO, sizeof(tGenAddr));
        MEMSET (&LdpIfAddrZero, LDP_ZERO, sizeof(tGenU4Addr));

	/* get the iface list associated to Ldp Entity */
	pIfList = SSN_GET_ENTITY_IFLIST (pSessionEntry);

    if (pAddress == NULL)
    {

        if(LdpGetAllLoopBackIpv4Ip(auIpv4LoopbackAddress,&u2Ipv4LoopBackCount)!= LDP_SUCCESS)
        {
            LDP_DBG (LDP_SSN_TX, "SESSION: Failed to get IPV4 LoopBack Ip\n");
            return LDP_FAILURE;
        }
        if(LdpGetAllPppInterfaceIpv4Ip(pIfList,auIpv4PppInterfaceAddress,&u2Ipv4PppInterfaceCount)!= LDP_SUCCESS)
        {
            LDP_DBG (LDP_SSN_TX, "SESSION: Failed to get IPV4 PPP Interface Ip\n");
            return LDP_FAILURE;
        }
    }
    else if (MEMCMP (pAddress, au1ZeroAddr, sizeof (au1ZeroAddr)) != 0)
    {
        MEMCPY(&auIpv4PppInterfaceAddress[u2Ipv4PppInterfaceCount],pAddress,LDP_IPV4ADR_LEN);
        LDP_DBG4 (LDP_SSN_MEM, "SESSION:Address for Address Msg: %x:%x:%x:%x\n",
                pAddress[0], pAddress[1], pAddress[2], pAddress[3]);
        u2Ipv4PppInterfaceCount++;
    }
    else
    {
        LDP_DBG4 (LDP_SSN_MEM, "SESSION:Address for Address Msg: %x:%x:%x:%x\n",
                pAddress[0], pAddress[1], pAddress[2], pAddress[3]);
        return LDP_FAILURE;

    }

	u2MandParamLen = (UINT2) (LDP_MSG_ID_LEN );

	if((u2Ipv4LoopBackCount != LDP_ZERO) ||(u2Ipv4PppInterfaceCount != LDP_ZERO))
	{
		u2MandParamLen= u2MandParamLen + LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN;
		u2MandParamLen = u2MandParamLen + ((u2Ipv4LoopBackCount+u2Ipv4PppInterfaceCount)*LDP_IPV4ADR_LEN);
	}
	else
	{
		return LDP_FAILURE;
	}

	pMsg = CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
				u2MandParamLen), LDP_PDU_HDR_LEN);
	if (pMsg == NULL)
	{
		LDP_DBG (LDP_SSN_MEM, "SESSION: Can't Allocate Buf for Address Msg \n");
		return LDP_FAILURE;
	}

	/*construct and copy Address Msg Header and Msg Id */
	MsgHdrAndId.u2MsgType = OSIX_HTONS (LDP_ADDR_MSG);
	MsgHdrAndId.u2MsgLen = OSIX_HTONS (u2MandParamLen);
	INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pSessionEntry));
	MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry));

	if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
				(LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
			CRU_FAILURE)
	{
		LDP_DBG (LDP_SSN_MEM,
				"SESSION: Error copying Addr MsgHdr and MsgId to Buf chain\n");
		CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
		return LDP_FAILURE;
	}
	u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

	if((u2Ipv4LoopBackCount != LDP_ZERO) ||(u2Ipv4PppInterfaceCount != LDP_ZERO))
	{
		/* Construct Addr Tlv */
		AddrTlv.AddrTlvHdr.u2TlvType = OSIX_HTONS (LDP_ADDRLIST_TLV);
		AddrTlv.AddrTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_ADDR_FMLY_LEN +
					(u2Ipv4LoopBackCount+u2Ipv4PppInterfaceCount) * (IPV4_ADDR_LENGTH)));
		AddrTlv.u2AddrType = OSIX_HTONS (((UINT2) LDP_ADDR_TYPE_IPV4));

		if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AddrTlv, u4Offset,
					(LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN)) ==
				CRU_FAILURE)
		{
			LDP_DBG (LDP_SSN_MEM,
					"SESSION: Error copying Addr Tlv Hdr to Buf chain\n");
			CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
			return LDP_FAILURE;
		}
		u4Offset += (LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN);

		for (u4Count =0 ; u4Count < u2Ipv4LoopBackCount; u4Count++)
        {
            LDP_DBG5 (LDP_DBG_PRCS, "%s IPV4 LoopBackAddr= %.2x %.2x %.2x %.2x\n" ,__func__ ,
                    auIpv4LoopbackAddress[u4Count][0],
                    auIpv4LoopbackAddress[u4Count][1],
                    auIpv4LoopbackAddress[u4Count][2],
                    auIpv4LoopbackAddress[u4Count][3]);

            if (CRU_BUF_Copy_OverBufChain
                    (pMsg, (UINT1 *)&auIpv4LoopbackAddress[u4Count], u4Offset,IPV4_ADDR_LENGTH) == CRU_FAILURE)
            {
                LDP_DBG (LDP_SSN_MEM,
                        "SESSION: Error copying Iface Addr to Buf chain\n");
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                return LDP_FAILURE;
            }
            u4Offset += IPV4_ADDR_LENGTH;
        }
		for(u4Count=0;u4Count<u2Ipv4PppInterfaceCount;u4Count++)
		{
#if 0
			printf("%s IPV4 PPPAddr= %.2x %.2x %.2x %.2x\n",__FUNCTION__,auIpv4PppInterfaceAddress[u4Count][0],
					auIpv4PppInterfaceAddress[u4Count][1],
					auIpv4PppInterfaceAddress[u4Count][2],
					auIpv4PppInterfaceAddress[u4Count][3]);
#endif

			if (CRU_BUF_Copy_OverBufChain
					(pMsg, (UINT1 *)&auIpv4PppInterfaceAddress[u4Count], u4Offset,IPV4_ADDR_LENGTH) == CRU_FAILURE)
			{
				LDP_DBG (LDP_SSN_MEM,
						"SESSION: Error copying Iface Addr to Buf chain\n");
				CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
				return LDP_FAILURE;
			}
			u4Offset += IPV4_ADDR_LENGTH;
		}
	}

	/* Moving the offset to PDU Header start */
	CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

	SSN_GET_PEERID (PeerLdpId, pSessionEntry);
	LDP_DBG4 (LDP_SSN_TX,
			"SESSION: Sending Address Msg To %d.%d.%d.%d\n",
			PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);
    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, u2MandParamLen, pSessionEntry->pLdpPeer->pLdpEntity,
                    LDP_FALSE, ConnId,
                    LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_SSN_TX, "SESSION: Failed to send Address Msg\n");
        return LDP_FAILURE;
    }
    LDP_DBG (LDP_SSN_TX, "SESSION: Address Msg sent\n");
    return LDP_SUCCESS;
}
#ifdef MPLS_IPV6_WANTED

UINT1
LdpSendIpv6AddrMsg (tLdpSession * pSessionEntry,
		tIp6Addr * pLinkLocalAddr,
		tIp6Addr * pSiteLocalAddr,
		tIp6Addr * pGlbUniqueAddr)
{
	tTMO_SLL           *pIfList = NULL;
	UINT2               u2MandParamLen = 0;
	UINT4               u4Offset = 0;
	tLdpMsgHdrAndId     MsgHdrAndId;

	tAddrTlv            AddrTlv;
	tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
	tLdpId              PeerLdpId;

	UINT4               u4Count=0;

	tIp6Addr            auIpv6LoopbackAddress[SYS_MAX_LOOPBACK_INTERFACES];
	tIp6Addr            auIpv6PppInterfaceAddress[SYS_MAX_LOOPBACK_INTERFACES];
	tIp6Addr           auIpv6LlLoopbackAddress[SYS_MAX_LOOPBACK_INTERFACES];

	UINT2               u2Ipv6LoopBackCount = 0;
	UINT2               u2Ipv6PppInterfaceCount=0;
	UINT2               u2Ipv6LlLoopBackCount = 0;

	/*MPLS_IPv6 add start*/
	tGenAddr     ConnId;
	tGenU4Addr     LdpIfAddrZero;
        UINT1               au1Temp[LDP_IPV6ADR_LEN];
	/*MPLS_IPv6 add end*/
        LDP_DBG1 (LDP_SSN_PRCS, "%s: ENTRY\n", __func__);
        MEMSET (au1Temp, 0, LDP_IPV6ADR_LEN);
	MEMSET ((UINT1 *) auIpv6LoopbackAddress, LDP_ZERO,
			(SYS_MAX_LOOPBACK_INTERFACES * LDP_IPV6ADR_LEN));

	MEMSET ((UINT1 *) auIpv6PppInterfaceAddress, LDP_ZERO,
			(SYS_MAX_LOOPBACK_INTERFACES * LDP_IPV6ADR_LEN));

	MEMSET ((UINT1 *) auIpv6LlLoopbackAddress, LDP_ZERO,
			(SYS_MAX_LOOPBACK_INTERFACES * LDP_IPV6ADR_LEN));
        MEMSET (&LdpIfAddrZero, LDP_ZERO, sizeof(tGenU4Addr));

	/* get the iface list associated to Ldp Entity */
	pIfList = SSN_GET_ENTITY_IFLIST (pSessionEntry);

	if ((pLinkLocalAddr==NULL) && 
			(pSiteLocalAddr==NULL) && (pGlbUniqueAddr==NULL))
	{
		if(LdpGetAllLoopBackIpv6Ip(auIpv6LoopbackAddress,&u2Ipv6LoopBackCount)!= LDP_SUCCESS)
		{
			LDP_DBG (LDP_SSN_TX, "SESSION: Failed to get IPV6 LoopBack Ip\n");
			return LDP_FAILURE;
		}
		if(LdpGetAlllnklLoopBackIpv6Ip(auIpv6LlLoopbackAddress,&u2Ipv6LlLoopBackCount)!=LDP_SUCCESS)
		{

			LDP_DBG (LDP_SSN_TX, "SESSION: Failed to get IPV6 LoopBack Ip\n");
			return LDP_FAILURE;
		}
		if(LdpGetAllPppInterfaceIpv6Ip(pIfList,auIpv6PppInterfaceAddress,&u2Ipv6PppInterfaceCount)!= LDP_SUCCESS)
		{
			LDP_DBG (LDP_SSN_TX, "SESSION: Failed to get IPV6 PPP Interface Ip\n");
			return LDP_FAILURE;
		}
        LDP_DBG3 (LDP_SSN_PRCS, "u2Ipv6LoopBackCount = %u, u2Ipv6LlLoopBackCount = %u, u2Ipv6PppInterfaceCount = %u\n",
                  u2Ipv6LoopBackCount, u2Ipv6LlLoopBackCount, u2Ipv6PppInterfaceCount);
	}
	else
	{
		if((pLinkLocalAddr != NULL ) && (MEMCMP (au1Temp, pLinkLocalAddr, LDP_IPV6ADR_LEN) != 0))
		{
                        MEMCPY(&auIpv6PppInterfaceAddress[u2Ipv6PppInterfaceCount],pLinkLocalAddr,LDP_IPV6ADR_LEN);
			u2Ipv6PppInterfaceCount++;
		}

		if((pSiteLocalAddr != NULL ) && (MEMCMP (au1Temp, pSiteLocalAddr, LDP_IPV6ADR_LEN) != 0))
		{
                        MEMCPY(&auIpv6PppInterfaceAddress[u2Ipv6PppInterfaceCount],pSiteLocalAddr,LDP_IPV6ADR_LEN);
			u2Ipv6PppInterfaceCount++;
		}

		if((pGlbUniqueAddr != NULL ) && (MEMCMP (au1Temp, pGlbUniqueAddr, LDP_IPV6ADR_LEN) != 0))
		{
			MEMCPY(&auIpv6PppInterfaceAddress[u2Ipv6PppInterfaceCount],pGlbUniqueAddr,LDP_IPV6ADR_LEN);
			u2Ipv6PppInterfaceCount++;
		}
	}

	u2MandParamLen = (UINT2) (LDP_MSG_ID_LEN );

#if 0
	printf("vishalPW : %s : %d u2Ipv6PppInterfaceCount=%d u2Ipv6PppInterfaceCount=%d u2Ipv6PppInterfaceCount=%d\n",
			__FUNCTION__,__LINE__,u2Ipv6PppInterfaceCount,u2Ipv6PppInterfaceCount,u2Ipv6PppInterfaceCount
	      );
#endif

	if((u2Ipv6LoopBackCount != LDP_ZERO) ||(u2Ipv6PppInterfaceCount != LDP_ZERO) || (u2Ipv6LlLoopBackCount != LDP_ZERO))
	{
		u2MandParamLen= (UINT2)(u2MandParamLen + LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN);
		u2MandParamLen = (UINT2)(u2MandParamLen + ((u2Ipv6LoopBackCount + u2Ipv6PppInterfaceCount + u2Ipv6LlLoopBackCount)*LDP_IPV6ADR_LEN));
	}
	else
	{
		return LDP_FAILURE;
	}

	pMsg = CRU_BUF_Allocate_MsgBufChain ((UINT4)(LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
				u2MandParamLen), (UINT4)LDP_PDU_HDR_LEN);
	if (pMsg == NULL)
	{
		LDP_DBG (LDP_SSN_MEM, "SESSION: Can't Allocate Buf for Address Msg \n");
		return LDP_FAILURE;
	}

	/*construct and copy Address Msg Header and Msg Id */
	MsgHdrAndId.u2MsgType = OSIX_HTONS (LDP_ADDR_MSG);
	MsgHdrAndId.u2MsgLen = OSIX_HTONS (u2MandParamLen);
	INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pSessionEntry));
	MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry));

	if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
				(LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
			CRU_FAILURE)
	{
		LDP_DBG (LDP_SSN_MEM,
				"SESSION: Error copying Addr MsgHdr and MsgId to Buf chain\n");
		CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
		return LDP_FAILURE;
	}
	u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

	if((u2Ipv6LoopBackCount != LDP_ZERO) ||(u2Ipv6PppInterfaceCount != LDP_ZERO) || (u2Ipv6LlLoopBackCount != LDP_ZERO))
	{
		/* Construct Addr Tlv */
		AddrTlv.AddrTlvHdr.u2TlvType = OSIX_HTONS (LDP_ADDRLIST_TLV);
		AddrTlv.AddrTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_ADDR_FMLY_LEN +
					(u2Ipv6LoopBackCount+u2Ipv6PppInterfaceCount+u2Ipv6LlLoopBackCount) * (LDP_IPV6ADR_LEN)));
		AddrTlv.u2AddrType = OSIX_HTONS (((UINT2) LDP_ADDR_TYPE_IPV6));

		if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AddrTlv, u4Offset,
					(LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN)) ==
				CRU_FAILURE)
		{
			LDP_DBG (LDP_SSN_MEM,
					"SESSION: Error copying Addr Tlv Hdr to Buf chain\n");
			CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
			return LDP_FAILURE;
		}
		u4Offset += (LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN);

		for(u4Count=0;u4Count<u2Ipv6LoopBackCount;u4Count++)
		{
#if 0
			printf(" %s IPV6 LoopBackAddr= %s\n",__FUNCTION__,Ip6PrintAddr(&auIpv6LoopbackAddress[u4Count]));
#endif

			if (CRU_BUF_Copy_OverBufChain
					(pMsg, (UINT1 *)&auIpv6LoopbackAddress[u4Count], u4Offset,LDP_IPV6ADR_LEN) == CRU_FAILURE)
			{
				LDP_DBG (LDP_SSN_MEM,
						"SESSION: Error copying Iface Addr to Buf chain\n");
				CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
				return LDP_FAILURE;
			}
			u4Offset += LDP_IPV6ADR_LEN;
		}
		for(u4Count=0;u4Count<u2Ipv6LlLoopBackCount;u4Count++)
		{
#if 0
			printf("%s IPV6 LinkLocalAddr= %s\n",__FUNCTION__,Ip6PrintAddr(&auIpv6LlLoopbackAddress[u4Count]));
#endif

			if (CRU_BUF_Copy_OverBufChain
					(pMsg, (UINT1 *)&auIpv6LlLoopbackAddress[u4Count], u4Offset,LDP_IPV6ADR_LEN) == CRU_FAILURE)
			{
				LDP_DBG (LDP_SSN_MEM,
						"SESSION: Error copying Iface Addr to Buf chain\n");
				CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
				return LDP_FAILURE;
			}
			u4Offset += LDP_IPV6ADR_LEN;
		}
		for(u4Count=0;u4Count<u2Ipv6PppInterfaceCount;u4Count++)
		{
#if 0
			printf("%s IPV6 PPPBackAddr= %s\n",__FUNCTION__,Ip6PrintAddr(&auIpv6PppInterfaceAddress[u4Count]));
#endif

			if (CRU_BUF_Copy_OverBufChain
					(pMsg, (UINT1 *)&auIpv6PppInterfaceAddress[u4Count], u4Offset,LDP_IPV6ADR_LEN) == CRU_FAILURE)
			{
				LDP_DBG (LDP_SSN_MEM,
						"SESSION: Error copying Iface Addr to Buf chain\n");
				CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
				return LDP_FAILURE;
			}
			u4Offset += LDP_IPV6ADR_LEN;
		}
	}
	/* Moving the offset to PDU Header start */
	CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

	SSN_GET_PEERID (PeerLdpId, pSessionEntry);
	LDP_DBG4 (LDP_SSN_TX,
			"SESSION: Sending Address Msg To %d.%d.%d.%d\n",
			PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);

	/*MPLS_IPv6 add start*/
	MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
	if (LdpSendMsg (pMsg, u2MandParamLen, pSessionEntry->pLdpPeer->pLdpEntity,
				LDP_FALSE,ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
	{
		CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
		LDP_DBG (LDP_SSN_TX, "SESSION: Failed to send Address Msg\n");
		return LDP_FAILURE;
	}
	LDP_DBG (LDP_SSN_TX, "SESSION: Address Msg sent\n");
	return LDP_SUCCESS;
}

#endif

/*****************************************************************************/
/* Function Name : LdpSendAddrWithdrawMsg                                    */
/* Description   : This routine constructs and sends the Ldp Address Withdraw*/
/*                 Message on the session passed to it.                      */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session on which the        */
/*                                 Withdraw Message has to be sent.          */
/*                 u4Addr - Address to be withdrawn                          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
UINT1
LdpSendAddrWithdrawMsg (tLdpSession * pSessionEntry, uGenAddr u4Addr)
{
    UINT2               u2IfaceCount = 1;

    /* default specifies the Ipv4 addr len */
    UINT1               u1AddrLen = LDP_IPV4ADR_LEN;

    UINT2               u2MandParamLen = 0;
    UINT4               u4Offset = 0;
    eGenAddrType        AddrFmly;
    tLdpMsgHdrAndId     MsgHdrAndId;
    tAddrTlv            AddrTlv;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLdpId              PeerLdpId;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/

#if 0
    printf("%s : %d\n",__FUNCTION__,__LINE__);
#endif

    AddrFmly = IPV4;
    MEMSET(&ConnId, LDP_ZERO, sizeof(tGenAddr));
    MEMSET(&LdpIfAddrZero, LDP_ZERO, sizeof(tGenU4Addr));
    u2MandParamLen =
        (UINT2) (LDP_MSG_ID_LEN + LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN +
                 (u2IfaceCount) * (u1AddrLen));

    pMsg = CRU_BUF_Allocate_MsgBufChain ((UINT4)(LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2MandParamLen), (UINT4)LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Can't Allocate Buf for Addr Wthdraw Msg\n");
        return LDP_FAILURE;
    }

    /*construct and copy Address Withdraw Msg Header and Msg Id */
    MsgHdrAndId.u2MsgType = OSIX_HTONS (LDP_ADDR_WITHDRAW_MSG);
    MsgHdrAndId.u2MsgLen = OSIX_HTONS (u2MandParamLen);
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pSessionEntry));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying Addr WDraw MsgHdr & Id "
                 "to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct Addr Tlv */
    AddrTlv.AddrTlvHdr.u2TlvType = OSIX_HTONS (LDP_ADDRLIST_TLV);
    AddrTlv.AddrTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_ADDR_FMLY_LEN +
                                               (u1AddrLen)));
    AddrTlv.u2AddrType = OSIX_HTONS (((UINT2) AddrFmly));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AddrTlv, u4Offset,
                                   (LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying Addr Tlv Hdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN);

    if (CRU_BUF_Copy_OverBufChain
        (pMsg, (UINT1 *) &(u4Addr), u4Offset, u1AddrLen) == CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying Iface Addr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += u1AddrLen;

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pSessionEntry);
    LDP_DBG4 (LDP_SSN_TX,
              "SESSION: Sending Addr WDraw Msg To %d.%d.%d.%d \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, u2MandParamLen, pSessionEntry->pLdpPeer->pLdpEntity,
                    LDP_FALSE, ConnId,
                    LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_SSN_TX, "SESSION: Failed to send Addr WDraw Msg\n");
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_SSN_TX, "SESSION: Addr Wdraw Msg sent\n");
    return LDP_SUCCESS;
}

/* MPLS_IPv6 add start*/

/*****************************************************************************/
/* Function Name : LdpConstructV6HelloMsg                                      */
/* Description   : This routine constructs and copies the hello message on to*/
/*                 the buffer chain.                                         */
/*                                                                           */
/* Input(s)      : pLdpEntity - Points to the Ldp Entity for which the hello */
/*                              Message is being constructed.                */
/*                 u1IsTrgtHello - A value 'LDP_TRUE' specifies that the     */
/*                                 Msg to be constructed is Targetted Hello. */
/*                                 A value 'LDP_FALSE' specifies that the Msg*/
/*                                 to be constructed is Basic Hello Msg.     */
/*                 u4IfAddr   - Address of the Interface over which LDP      */
/*                              hello is sent. Used to fill the transport    */
/*                              address in Transport Address TLV of LDP Hello*/
/*                              Message.                                     */
/* Output(s)     : ppMsg - Points to the constructed Hello message.          */
/*                 pu2MsgLen - Length of the Hello Msg, excluding Msg HdrLen */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
#ifdef MPLS_IPV6_WANTED
UINT1
LdpConstructIpv6HelloMsg (tLdpEntity * pLdpEntity, UINT1 u1IsTrgtHello,
                      tCRU_BUF_CHAIN_HEADER ** ppMsg, UINT2 *pu2MsgLen,
                      tIp6Addr V6IfAddr)
{
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tIp6Addr        V6TempZeroAddr;
    /* Initialising the Tlv Header */
    tCmnHelloParmsTlv   CmnHelloParmsTlv =
        { {OSIX_HTONS (LDP_CMN_HELLO_PARM_TLV),
           OSIX_HTONS ((LDP_HELLO_HOLD_TIME_LEN + 2))}, 0, 0
    };
    tTransportAddrTlv   V6TrAddrTlv;
    tConfSeqNumTlv      CSNumTlv = { {OSIX_HTONS (LDP_CONF_SEQNUMB_TLV),
                                      OSIX_HTONS (LDP_CS_NUM_LEN)}, 0
    };
    tLdpMsgHdrAndId     MsgHdrAndId;
    tIp6Addr                 TempV6IfAddr;
    UINT4               u4Offset = 0;
    UINT2               u2OptParamLen = 0;
    UINT2               u2MandParamLen = (LDP_MSG_ID_LEN + LDP_TLV_HDR_LEN +
                                          LDP_HELLO_HOLD_TIME_LEN + 2);
    MEMSET(&TempV6IfAddr, 0, sizeof(tIp6Addr));
    MEMSET(&V6TempZeroAddr, 0, sizeof(tIp6Addr));
    MEMCPY(TempV6IfAddr.u1_addr, V6IfAddr.u1_addr, LDP_IPV6ADR_LEN);
    if (LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) == LOOPBACK_ADDRESS_TYPE)
    {
        MEMCPY(TempV6IfAddr.u1_addr, pLdpEntity->Ipv6TransportAddress.u1_addr, LDP_IPV6ADR_LEN);
    }
    if ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) == LDP_TRUE) && 
		( LDP_ENTITY_IPV6TRANSPORT_ADDRKIND (pLdpEntity) == LOOPBACK_ADDRESS_TYPE) &&
		pLdpEntity->bIsIpv6TransAddrIntfUp == LDP_TRUE)
    {
        u2OptParamLen = LDP_TLV_HDR_LEN + LDP_V6TR_ADDR_LEN;
    }
    if (pLdpEntity->u1ConfSeqNumTlvEnable == TRUE)
    {
        u2OptParamLen += (UINT2)(LDP_TLV_HDR_LEN + LDP_CS_NUM_LEN);
    }

    pMsg = CRU_BUF_Allocate_MsgBufChain ((UINT4)(LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2OptParamLen + u2MandParamLen),
                                         (UINT4)LDP_PDU_HDR_LEN);
    *pu2MsgLen = (UINT2) (u2OptParamLen + u2MandParamLen);
    *ppMsg = pMsg;
    if (*ppMsg == NULL)
    {
        LDP_DBG (LDP_SSN_MEM, "SESSION: Can't Allocate Buf for IPV6 Hello Msg \n");
        return LDP_FAILURE;
    }

    /*construct and copy Hello Msg Header and Msg Id */
    MsgHdrAndId.u2MsgType = OSIX_HTONS (LDP_HELLO_MSG);
    MsgHdrAndId.u2MsgLen = OSIX_HTONS ((u2OptParamLen + u2MandParamLen));

    INC_LDP_ENTY_MSG_ID (pLdpEntity);
    MsgHdrAndId.u4MsgId = OSIX_HTONL (pLdpEntity->u4MessageId);

    /*
       MsgHdrAndId.u4MsgId = OSIX_HTONL((pLdpEntity->u4MessageId)++);
     */

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying IPV6 Hello MsgHdr and MsgId "
                 "to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);
    /* Construct and Copy Common Hello Parameters Tlv */
    if (u1IsTrgtHello == LDP_TRUE)
    {
        CmnHelloParmsTlv.u2TRField =
            OSIX_HTONS ((LDP_TLV_T_BIT | LDP_TLV_R_BIT | LDP_RSVD_FLD));
    }
    else
    {
        CmnHelloParmsTlv.u2TRField = 0;
    }
    CmnHelloParmsTlv.u2HoldTime = OSIX_HTONS (pLdpEntity->u2HelloConfHoldTime);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &CmnHelloParmsTlv, u4Offset,
                                   (LDP_TLV_HDR_LEN + LDP_HELLO_HOLD_TIME_LEN +
                                    LDP_HELLO_T_R_RSVD_LEN)) == CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying IPV6 CmnHelloParmsTlv to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset +=
        (LDP_TLV_HDR_LEN + LDP_HELLO_HOLD_TIME_LEN + LDP_HELLO_T_R_RSVD_LEN);

    /* construct and copy Transport Address Tlv */
    if ((LDP_ENTITY_IPV6_TRANSPORT_ADDRESS_TLV (pLdpEntity) == TRUE) &&
		( LDP_ENTITY_IPV6TRANSPORT_ADDRKIND (pLdpEntity) == LOOPBACK_ADDRESS_TYPE) &&
		(pLdpEntity->bIsIpv6TransAddrIntfUp == LDP_TRUE))
    {
        V6TrAddrTlv.TlvHdr.u2TlvType = OSIX_HTONS (LDP_IPV6_TRANSPORT_ADDR_TLV);
        V6TrAddrTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_IPV6_TR_ADDR_LEN);

        MEMCPY ((UINT1 *) &(V6TrAddrTlv.TAddr.Ip6Addr.u1_addr),
                (UINT1 *) &TempV6IfAddr.u1_addr, LDP_IPV6ADR_LEN);
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &V6TrAddrTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_V6TR_ADDR_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_SSN_MEM,
                     "SESSION: Error copying IPV6 TrAddrTlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_V6TR_ADDR_LEN);
    }

    if (pLdpEntity->u1ConfSeqNumTlvEnable == TRUE)
    {
        /* construct and copy Configuration Sequence Number Tlv */
        CSNumTlv.u4ConfSeqNum = OSIX_HTONL (pLdpEntity->u4ConfSeqNum);
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &CSNumTlv, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_CS_NUM_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_SSN_MEM,
                     "SESSION: Error copying IPV6 Conf Seq Number Tlv "
                     "to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
    }
    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    KW_FALSEPOSITIVE_FIX(pMsg);
    return LDP_SUCCESS;
    
}
UINT1
LdpSendIpv6AddrWithdrawMsg (tLdpSession * pSessionEntry,tIp6Addr *pLnklocalIpv6Addr,
		tIp6Addr *pSitelocalIpv6Addr,
		tIp6Addr *pGlbUniqueIpv6Addr)
{
    UINT2               u2IfaceCount = 0;

    /* default specifies the Ipv4 addr len */
    UINT1               u1AddrLen = LDP_IPV6ADR_LEN;

    UINT2               u2MandParamLen = 0;
    UINT4               u4Offset = 0;
    eGenAddrType        AddrFmly;
    tLdpMsgHdrAndId     MsgHdrAndId;
    tAddrTlv            AddrTlv;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLdpId              PeerLdpId;

    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/

#if 0
    printf("%s : %d\n",__FUNCTION__,__LINE__);
#endif
    
    MEMSET(&ConnId, LDP_ZERO, sizeof(tGenAddr));
    MEMSET(&LdpIfAddrZero, LDP_ZERO,sizeof(tGenU4Addr));
  
    AddrFmly = IPV6;

    if(pLnklocalIpv6Addr!=NULL)
    {
	    u2IfaceCount++;
    }
    if(pSitelocalIpv6Addr !=NULL)
    {
	    u2IfaceCount++;
    }
    if(pGlbUniqueIpv6Addr != NULL)
    {
	    u2IfaceCount++;
    }

    u2MandParamLen =
        (UINT2) (LDP_MSG_ID_LEN + LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN +
                 (u2IfaceCount) * (u1AddrLen));

    pMsg = CRU_BUF_Allocate_MsgBufChain ((UINT4)(LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                          u2MandParamLen), (UINT4)LDP_PDU_HDR_LEN);
    if (pMsg == NULL)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Can't Allocate Buf for Addr Wthdraw Msg\n");
        return LDP_FAILURE;
    }

    /*construct and copy Address Withdraw Msg Header and Msg Id */
    MsgHdrAndId.u2MsgType = OSIX_HTONS (LDP_ADDR_WITHDRAW_MSG);
    MsgHdrAndId.u2MsgLen = OSIX_HTONS (u2MandParamLen);
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pSessionEntry));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying Addr WDraw MsgHdr & Id "
                 "to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct Addr Tlv */
    AddrTlv.AddrTlvHdr.u2TlvType = OSIX_HTONS (LDP_ADDRLIST_TLV);
    AddrTlv.AddrTlvHdr.u2TlvLen = OSIX_HTONS ((LDP_ADDR_FMLY_LEN +
                                               (u1AddrLen)));
    AddrTlv.u2AddrType = OSIX_HTONS (((UINT2) AddrFmly));

    if(u2IfaceCount<=LDP_ZERO)
    {
	    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
	    LDP_DBG (LDP_SSN_TX, "SESSION: Failed to send IPv6 Addr WDraw Msg, All the Ips are NULL\n");
	    return LDP_FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &AddrTlv, u4Offset,
                                   (LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_SSN_MEM,
                 "SESSION: Error copying Addr Tlv Hdr to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_ADDR_FMLY_LEN);

    if(pLnklocalIpv6Addr!=NULL)
    {
	    if (CRU_BUF_Copy_OverBufChain
			    (pMsg,pLnklocalIpv6Addr->u1_addr, u4Offset, u1AddrLen) == CRU_FAILURE)
	    {
		    LDP_DBG (LDP_SSN_MEM,
				    "SESSION: Error copying Iface Addr to Buf chain\n");
		    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);CRU_BUF_Release_MsgBufChain (pMsg, FALSE); 
		    return LDP_FAILURE;
	    }
#if 0
	    printf(" %s IPV6 LoopBackAddr= %s\n",__FUNCTION__,Ip6PrintAddr(pLnklocalIpv6Addr));
#endif

	    u4Offset += u1AddrLen;

    }

    if(pSitelocalIpv6Addr!=NULL)
    {
	    if (CRU_BUF_Copy_OverBufChain
			    (pMsg,pSitelocalIpv6Addr->u1_addr, u4Offset, u1AddrLen) == CRU_FAILURE)
	    {
		    LDP_DBG (LDP_SSN_MEM,
				    "SESSION: Error copying Iface Addr to Buf chain\n");
		    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);CRU_BUF_Release_MsgBufChain (pMsg, FALSE); 
		    return LDP_FAILURE;
	    }

#if 0
	    printf(" %s IPV6 LoopBackAddr= %s\n",__FUNCTION__,Ip6PrintAddr(pSitelocalIpv6Addr));
#endif

	    u4Offset += u1AddrLen;

    }

    if(pGlbUniqueIpv6Addr!=NULL)
    {
	    if (CRU_BUF_Copy_OverBufChain
			    (pMsg,pGlbUniqueIpv6Addr->u1_addr, u4Offset, u1AddrLen) == CRU_FAILURE)
	    {
		    LDP_DBG (LDP_SSN_MEM,
				    "SESSION: Error copying Iface Addr to Buf chain\n");
		    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);CRU_BUF_Release_MsgBufChain (pMsg, FALSE); 
		    return LDP_FAILURE;
	    }
#if 0
	    printf(" %s IPV6 LoopBackAddr= %s\n",__FUNCTION__,Ip6PrintAddr(pGlbUniqueIpv6Addr));
#endif

	    u4Offset += u1AddrLen;

    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);

    SSN_GET_PEERID (PeerLdpId, pSessionEntry);
    LDP_DBG4 (LDP_SSN_TX,
              "SESSION: Sending Addr WDraw Msg To %d.%d.%d.%d \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);

 /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, u2MandParamLen, pSessionEntry->pLdpPeer->pLdpEntity,
                    LDP_FALSE,ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        LDP_DBG (LDP_SSN_TX, "SESSION: Failed to send Addr WDraw Msg\n");
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_SSN_TX, "SESSION: Addr Wdraw Msg sent\n");
    return LDP_SUCCESS;
}
#endif


UINT1 
LdpGetAllLoopBackIpv4Ip(tIpv4Addr  *pLoopbackAddress,UINT2 *pLoopBackIntCount)
{
	tNetIpv4IfInfo      NetIpIfInfo;
	tCfaIfInfo          CfaIfInfo;
	UINT4               u4Port = LDP_ZERO;
	UINT4               u4TempAddr = LDP_ZERO;
	UINT4               u4CfaIfIndex = LDP_ZERO;
	UINT2               u2IfaceCount = 0;


	MEMSET(&CfaIfInfo,LDP_ZERO, sizeof(tCfaIfInfo));
	MEMSET ((UINT1 *) &NetIpIfInfo, LDP_ZERO, sizeof (tNetIpv4IfInfo));

	if (NetIpv4GetFirstIfInfo (&NetIpIfInfo) == NETIPV4_SUCCESS)
	{
		do
		{
			u4Port = NetIpIfInfo.u4IfIndex;
			u4TempAddr = OSIX_HTONL (NetIpIfInfo.u4Addr);

			MEMSET ((UINT1 *) &NetIpIfInfo, LDP_ZERO,
					sizeof (tNetIpv4IfInfo));

			if (NetIpv4GetCfaIfIndexFromPort (u4Port,
						&u4CfaIfIndex)
					== NETIPV4_FAILURE)
			{
				continue;
			}

			if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
			{
				continue;
			}

			if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
			{
				continue;
			}

            if (u4TempAddr != 0)
            {
                MEMCPY ((UINT1 *)
                        &pLoopbackAddress[u2IfaceCount],
                        (UINT1 *) &u4TempAddr, LDP_IPV4ADR_LEN);
                u2IfaceCount++;
            }
		}
		while (NetIpv4GetNextIfInfo (u4Port, &NetIpIfInfo)
				== NETIPV4_SUCCESS);
	}
	*pLoopBackIntCount=u2IfaceCount;

	return LDP_SUCCESS;
}

#ifdef MPLS_IPV6_WANTED
UINT1
LdpGetAllLoopBackIpv6Ip(tIp6Addr  *pLoopbackAddress,UINT2 *pLoopBackIntCount)
{
	tCfaIfInfo          CfaIfInfo;
	UINT4               u4LoopbackCount = LDP_ZERO;
	UINT4               u4CfaIfIndex = LDP_ZERO;
        UINT4               u4Index=0;

	tNetIpv6IfInfo      NetIpv6IfInfo;
	tNetIpv6AddrInfo    NetIpv6AddrInfo;
	tNetIpv6AddrInfo    NetIpv6NextAddrInfo;

	MEMSET(&NetIpv6IfInfo,LDP_ZERO,sizeof(tNetIpv6IfInfo));
	MEMSET(&NetIpv6AddrInfo,LDP_ZERO,sizeof(tNetIpv6AddrInfo));
	MEMSET(&NetIpv6NextAddrInfo,LDP_ZERO,sizeof(tNetIpv6AddrInfo));
	MEMSET(&CfaIfInfo,LDP_ZERO,sizeof(tCfaIfInfo));

	if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
	{
		*pLoopBackIntCount=(UINT2)u4LoopbackCount;
		return LDP_SUCCESS;
	}

	do
	{
		u4Index = NetIpv6IfInfo.u4IfIndex;
		if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP)
		{

			if (NetIpv6GetCfaIfIndexFromPort (u4Index,
						&u4CfaIfIndex)
					== NETIPV6_FAILURE)
			{
				continue;
			}

			if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
			{
				continue;
			}

			if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
			{
				continue;
			}


			if (NetIpv6GetFirstIfAddr (u4Index, &NetIpv6AddrInfo)
					== NETIPV6_SUCCESS)
			{
				for (;;)
				{
					MEMCPY(&pLoopbackAddress[u4LoopbackCount],&NetIpv6AddrInfo.Ip6Addr,LDP_IPV6ADR_LEN);
					u4LoopbackCount++;

					if (NetIpv6GetNextIfAddr (u4Index, &NetIpv6AddrInfo,
								&NetIpv6NextAddrInfo) ==
							NETIPV6_FAILURE)
					{
						break;
					}
					MEMCPY (&NetIpv6AddrInfo, &NetIpv6NextAddrInfo,
							sizeof (tNetIpv6AddrInfo));
				}
			}
		}
	}while(NetIpv6GetNextIfInfo (u4Index, &NetIpv6IfInfo)
			== NETIPV6_SUCCESS);


	*pLoopBackIntCount=(UINT2)u4LoopbackCount;
	return LDP_SUCCESS;
}

UINT1
LdpGetAlllnklLoopBackIpv6Ip(tIp6Addr  *pLlLoopbackAddress,UINT2 *pLlLoopBackIntCount)
{
	UINT4               u4LoopbackCount = LDP_ZERO;
	UINT4               u4Index=0;
	tCfaIfInfo          CfaIfInfo;
        UINT4               u4CfaIfIndex = LDP_ZERO;

	tNetIpv6IfInfo      NetIpv6IfInfo;

	MEMSET(&NetIpv6IfInfo,LDP_ZERO,sizeof(tNetIpv6IfInfo));
        MEMSET(&CfaIfInfo,LDP_ZERO,sizeof(tCfaIfInfo));

	if (NetIpv6GetFirstIfInfo (&NetIpv6IfInfo) == NETIPV6_FAILURE)
	{
		*pLlLoopBackIntCount=(UINT2)u4LoopbackCount;
		return LDP_SUCCESS;
	}
	do
	{
		u4Index = NetIpv6IfInfo.u4IfIndex;
		if (NetIpv6IfInfo.u4Oper == NETIPV6_OPER_UP)
		{

			if (NetIpv6GetCfaIfIndexFromPort (u4Index,
						&u4CfaIfIndex)
					== NETIPV6_FAILURE)
			{
				continue;
			}

			if (CfaGetIfInfo (u4CfaIfIndex, &CfaIfInfo) == CFA_FAILURE)
			{
				continue;
			}

			if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
			{
				continue;
			}

			MEMCPY(&pLlLoopbackAddress[u4LoopbackCount],&NetIpv6IfInfo.Ip6Addr,LDP_IPV6ADR_LEN);
			u4LoopbackCount++;
			*pLlLoopBackIntCount=(UINT2)u4LoopbackCount;

			return LDP_SUCCESS;
		}
	}while(NetIpv6GetNextIfInfo (u4Index, &NetIpv6IfInfo)
			== NETIPV6_SUCCESS);

	*pLlLoopBackIntCount=(UINT2)u4LoopbackCount;

	return LDP_SUCCESS;
}



UINT1
LdpGetAllPppInterfaceIpv6Ip(tTMO_SLL *pIfList,tIp6Addr  *pPppInterfaceAddress,UINT2 *pPppIntCount)
{

	tLdpIfTableEntry   *pIfEntry = NULL;
        UINT2              u2Count=0;
        tIp6Addr           TempAddr;
       
        MEMSET(&TempAddr,LDP_ZERO,sizeof(tIp6Addr));

    LDP_DBG1 (LDP_SSN_PRCS, "%s: ENTRY\n", __func__);
	TMO_SLL_Scan (pIfList, pIfEntry, tLdpIfTableEntry *)
	{
		if((pIfEntry->u1InterfaceType==LDP_IFTYPE_IPV6) ||
				(pIfEntry->u1InterfaceType== LDP_IFTYPE_DUAL))
		{
			if(MEMCMP(&pIfEntry->LnklocalIpv6Addr,&TempAddr,LDP_IPV6ADR_LEN)!=LDP_ZERO)
			{
                LDP_DBG1 (LDP_SSN_PRCS, "Link Local IP: %s\n", Ip6PrintAddr (&pIfEntry->LnklocalIpv6Addr));
				MEMCPY(&pPppInterfaceAddress[u2Count],&pIfEntry->LnklocalIpv6Addr,LDP_IPV6ADR_LEN);
				u2Count++;
			}

			if(MEMCMP(&pIfEntry->SitelocalIpv6Addr,&TempAddr,LDP_IPV6ADR_LEN)!=LDP_ZERO)
			{
                LDP_DBG1 (LDP_SSN_PRCS, "Site Local IP: %s\n", Ip6PrintAddr (&pIfEntry->SitelocalIpv6Addr));
				MEMCPY(&pPppInterfaceAddress[u2Count],&pIfEntry->SitelocalIpv6Addr,LDP_IPV6ADR_LEN);
				u2Count++;
			}

			if(MEMCMP(&pIfEntry->GlbUniqueIpv6Addr,&TempAddr,LDP_IPV6ADR_LEN)!=LDP_ZERO)
			{
                LDP_DBG1 (LDP_SSN_PRCS, "Global IP: %s\n", Ip6PrintAddr (&pIfEntry->GlbUniqueIpv6Addr));
				MEMCPY(&pPppInterfaceAddress[u2Count],&pIfEntry->GlbUniqueIpv6Addr,LDP_IPV6ADR_LEN);
				u2Count++;
			}
		}
	}
	*pPppIntCount=u2Count;
	return LDP_SUCCESS;
}
#endif

UINT1 LdpGetAllPppInterfaceIpv4Ip(tTMO_SLL *pIfList,tIpv4Addr *pPppInterfaceAddress,UINT2 *pPppIntCount)
{
        tIpv4Addr           TempIfAddr;
	tLdpIfTableEntry   *pIfEntry = NULL;
        UINT2              u2Count=0;
        MEMSET (&TempIfAddr,0,sizeof(tIpv4Addr));  
	TMO_SLL_Scan (pIfList, pIfEntry, tLdpIfTableEntry *)
	{
		if((pIfEntry->u1InterfaceType==LDP_IFTYPE_IPV4) ||
				(pIfEntry->u1InterfaceType== LDP_IFTYPE_DUAL))
		{
			if(MEMCMP(TempIfAddr, pIfEntry->IfAddr, IPV4_ADDR_LENGTH)!= 0)
			{
				MEMCPY(&pPppInterfaceAddress[u2Count],pIfEntry->IfAddr,LDP_IPV4ADR_LEN);
				u2Count++;
			}
		}
	}
        *pPppIntCount=u2Count;
	return LDP_SUCCESS;
}




/*---------------------------------------------------------------------------*/
/*                       End of file ldpssnd.c                               */
/*---------------------------------------------------------------------------*/
