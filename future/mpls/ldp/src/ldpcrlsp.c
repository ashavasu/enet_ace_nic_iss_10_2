/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: ldpcrlsp.c,v 1.29 2016/02/28 10:58:23 siva Exp $
 *                                                                  *
 *******************************************************************/

/*---------------------------------------------------------------------------*/
/* 
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpcrlsp.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains functions that are
 *                             associeated with CR-LSP.
 *----------------------------------------------------------------------------*/
#include "ldpincs.h"

/*****************************************************************************/
/* Function Name : LdpNonMrgIntCrlspSetReq                                   */
/* Description   : This routine is called when a new CRLSP is to be created  */
/*                 in a LSR. The LSR obtains forwarding information for the  */
/*                 First Er Hop from Ip routing table. The LSP is created    */
/*                 for the CRLSP.                                            */
/*                                                                           */
/*                 NOTE: Support for determining the next hop for an ERHOP   */
/*                 Abstract node is currently done based on IP route Input.  */
/*                 The determination can be upated to utilise 'local Policy' */
/*                 subsequently.                                             */
/*                                                                           */
/* Input(s)      : u2IncarnId - Incarnation index                            */
/*                 pCrlspTnlInfo  - Pointer to the CRLSP tunnel information  */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpNonMrgIntCrlspSetReq (UINT2 u2IncarnId, tCrlspTnlInfo * pCrlspTnlInfo)
{
    UINT1               u1Event;
    UINT1               u1IsEgrOfHop = LDP_FALSE;
    UINT2               u2OutIfIndex;
    UINT4               u4GwAddr;

    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tLspTrigCtrlBlock  *pLspTrigCtrlBlock = NULL;
    tLdpTeHopInfo      *pFirstErHop = NULL;
    tLdpSession        *pDStrSsn = NULL;

    tTMO_SLL            pPreEmpList;
    tTMO_SLL           *pCandidatePreemp = (tTMO_SLL *) & pPreEmpList;
    tMplsDiffServTnlInfo *pDiffServParams = NULL;

    /* 
     * Get a new LSP Control block. initialise it with the relevant
     * parameters. 
     */

    pLspCtrlBlk = (tLspCtrlBlock *) MemAllocMemBlk (LDP_LSP_POOL_ID);

    if (pLspCtrlBlk == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't allocate mem for New LSP Ctrl blk. No Lsp estd\n");
        LdpDeleteCrlspTnlInfo (u2IncarnId, pCrlspTnlInfo);
        return LDP_FAILURE;
    }

    pLspTrigCtrlBlock = (tLspTrigCtrlBlock *) MemAllocMemBlk (LDP_TRIG_POOL_ID);

    if (pLspTrigCtrlBlock == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate mem for Trigger blk.No Lsp estd\n");
        LdpDeleteCrlspTnlInfo (u2IncarnId, pCrlspTnlInfo);
        MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlk);
        return LDP_FAILURE;
    }

    MEMSET(pLspTrigCtrlBlock,0,sizeof(tLspTrigCtrlBlock));

    /* Contents of the LSP Control block are suitably initialised */
    LdpInitLspCtrlBlock (pLspCtrlBlk);
    pLspCtrlBlk->u1LspState = LDP_LSM_ST_IDLE;
    pLspCtrlBlk->Fec.u1FecElmntType = LDP_FEC_CRLSP_TYPE;
    pLspCtrlBlk->Fec.u2AddrFmly = IPV4;

    /* This is not HostAddrFecType. It is TnlType. Define new type. */
    pLspTrigCtrlBlock->u1TrgType = HST_ADDR_FEC_LSP_SETUP;
    pLspCtrlBlk->pTrigCtrlBlk = pLspTrigCtrlBlock;

    /* Update the LCB with Tunnel Information */
    pLspCtrlBlk->pCrlspTnlInfo = pCrlspTnlInfo;
    pCrlspTnlInfo->pLspCtrlBlock = pLspCtrlBlk;

    /* Scan the Erhop List and if the Ingress LSR is part of
     * the ErHop which is of IPv4, remove it, as the processing of the
     * First Erhop. If the ErHop is of LSPID break from the loop.
     */

    TMO_SLL_Scan (&CRLSP_ERHOP_LIST (pCrlspTnlInfo), pFirstErHop, tTeHopInfo *)
    {
        if ((CRLSP_TE_ERHOP_ADDR_TYPE (pFirstErHop) ==
             CRLSP_TE_ERHOP_IPV4_TYPE))
        {
            if (LdpIsLsrPartOfErHop (u2IncarnId, pFirstErHop, NULL,
                                     &u1IsEgrOfHop) == LDP_TRUE)
            {
                continue;
            }
            else
            {
                /* Processing of this ErHop is done below */
                break;
            }
        }
        else if ((CRLSP_TE_ERHOP_ADDR_TYPE (pFirstErHop)
                  == CRLSP_TE_ERHOP_LSPID_TYPE))
        {
            /* Processing of this ErHop is done below */
            break;
        }
    }

    /* If all the Configured ErHops are part of Ingress itself, then
     * the Failure is returned 
     */

    if (pFirstErHop == NULL)
    {
        LdpDeleteLspCtrlBlock (pLspCtrlBlk);
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: All the Configured Erhops are part of Ingress Itself\n");
        return LDP_FAILURE;
    }

    /* 
     * Process the configured First Er-Hop and fill LCB with Fwding 
     * Info required for forwarding Cr-Lsp Label Request Msg.
     */

    /* 
     * If Er-Hop Type is other than LspId Type, then just forward to the Er-Hop.
     * Else, check if Ingress belongs to the Er-Hop(of type LspId).
     */

    switch (CRLSP_TE_ERHOP_ADDR_TYPE (pFirstErHop))
    {
        case CRLSP_TE_ERHOP_IPV4_TYPE:

            /* Check the Reachability to the Er-Hop via MPLS Domain. 
             * If Er-Hop is reachable via MPLS Domain then, update the LCB with 
             * essential forwarding information.
             * Updation to LCB is done, to facilitate Fwding of Req msg to DStr.
             */

            if (LdpGetReachabilityToErHop (u2IncarnId, pFirstErHop, &u4GwAddr,
                                           &u2OutIfIndex, &pDStrSsn)
                == LDP_FAILURE)
            {
                /* Configuration Error */
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: Erroneous Confign. First ErHop(IPv4) "
                         "Unreachbl\n");
                LdpDeleteLspCtrlBlock (pLspCtrlBlk);
                return LDP_FAILURE;
            }

            if (CRLSP_TE_ERHOP_TYPE (pFirstErHop) == CRLSP_TE_STRICT_ER)
            {
                /* 
                 * In case where First ErHop is Strict then, Gateway Address 
                 * MUST belong to the First Er-Hop.
                 * For Loose First Hop, there is no such restriction.
                 */
                if (LdpIsGwPartOfErHop (u4GwAddr, pFirstErHop) == LDP_FALSE)
                {
                    /* Gateway Does not belong to the First ErHop.  */
                    LDP_DBG (LDP_ADVT_PRCS,
                             "ADVT: Bad First Strict ErHop. Erroneous Confign.\n");
                    LdpDeleteLspCtrlBlock (pLspCtrlBlk);
                    return LDP_FAILURE;
                }
            }

            if (LdpUpdateCrLCBWithFwdInfo (u2IncarnId, u4GwAddr, u2OutIfIndex,
                                           pDStrSsn, pLspCtrlBlk) ==
                CFA_FAILURE)
            {
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: Update of LSP CtrlBlk - FWDing info FAILED.\n");
                LdpDeleteLspCtrlBlock (pLspCtrlBlk);
                return LDP_FAILURE;
            }
            break;

        case CRLSP_TE_ERHOP_LSPID_TYPE:

            u1IsEgrOfHop = LDP_FALSE;
            if (LdpIsLsrPartOfErHop (u2IncarnId, pFirstErHop, pLspCtrlBlk,
                                     &u1IsEgrOfHop) != LDP_TRUE)
            {
                /* Not Part of the First Er-Hop of Type LspId */
                if (CRLSP_TE_ERHOP_TYPE (pFirstErHop) == CRLSP_TE_STRICT_ER)
                {
                    /* Error : First Er-Hop is Strict */
                    LDP_DBG (LDP_ADVT_PRCS,
                             "ADVT: ConfigError. Not part of strict First"
                             " ErHop(LspId)\n");
                    LdpDeleteLspCtrlBlock (pLspCtrlBlk);
                    return LDP_FAILURE;
                }
                else
                {
                    /* Lsr Does not belong to this Loose Er-Hop. */
                    /* Node does not belong to this Loose Er-Hop. */
                    /* Check the Reachability to the Er-Hop via MPLS Domain. 
                     * If Er-Hop is reachable via MPLS Domain then, update the 
                     * LCB with essential forwarding information.
                     * LCB Updation is done to facilitate Fwding of Req msg 
                     * to DStr.
                     */
                    if (LdpGetReachabilityToErHop (u2IncarnId, pFirstErHop,
                                                   &u4GwAddr, &u2OutIfIndex,
                                                   &pDStrSsn) == LDP_FAILURE)
                    {
                        LDP_DBG (LDP_ADVT_PRCS,
                                 "ADVT: Erroneous Confign. First "
                                 "ErHop(LspId) Unreachbl\n");
                        LdpDeleteLspCtrlBlock (pLspCtrlBlk);
                        return LDP_FAILURE;
                    }

                    if (LdpUpdateCrLCBWithFwdInfo (u2IncarnId, u4GwAddr,
                                                   u2OutIfIndex, pDStrSsn,
                                                   pLspCtrlBlk) == CFA_FAILURE)
                    {
                        LDP_DBG (LDP_ADVT_PRCS,
                                 "ADVT: Update of LSP CtrlBlk - FWDing info FAILED.\n");
                        LdpDeleteLspCtrlBlock (pLspCtrlBlk);
                        return LDP_FAILURE;
                    }

                    break;
                }
            }
            /* 
             * If Ingress is part of the First Er-Hop which of LspIdType 
             * then,Fwding Info would have already been updated into 
             * the new Cr-LCB, if the Target session is found.
             */
            if (CRLSP_TRGT_SSN_FOR_STACK (LCB_TNLINFO (pLspCtrlBlk)) ==
                LDP_FALSE)
            {
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: Target session is not present for this LSPID \n");
                LdpDeleteLspCtrlBlock (pLspCtrlBlk);
                return LDP_FAILURE;
            }
            break;

        default:

            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: ER Hop Type not supported. No Lsp estd\n");
            LdpDeleteLspCtrlBlock (pLspCtrlBlk);
            return LDP_FAILURE;
    }

    pDiffServParams = LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);

    if (pDiffServParams != NULL)
    {

        if (LdpDiffServHandleDSIngressNode (pLspCtrlBlk, u2IncarnId) ==
            LDP_DS_FAILURE)
        {
            LdpDeleteLspCtrlBlock (pLspCtrlBlk);
            return LDP_FAILURE;
        }

        if ((CRLSP_TE_ERHOP_ADDR_TYPE (pFirstErHop)
             != CRLSP_TE_ERHOP_LSPID_TYPE))
        {
            if ((LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServParams) == LDP_DS_ELSP) ||
                (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServParams) == LDP_DS_LLSP))
            {
                if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo)
                    == LDP_DS_PEROABASED_RESOURCES)
                {
                    if (LdpDiffServHandleRMPerOARsrc (pLspCtrlBlk) ==
                        LDP_DS_FAILURE)
                    {
                        LdpDeleteLspCtrlBlock (pLspCtrlBlk);
                        return LDP_FAILURE;
                    }
                    return LDP_SUCCESS;
                }
                else if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo) ==
                         LDP_DS_CLASSTYPE_RESOURCES)
                {
                    /* Do the resource management in ClassType  basis */

                    if (LdpDSTcResvResourcesInClassType (pCrlspTnlInfo) ==
                        LDP_DS_FAILURE)
                    {
                        LdpDeleteLspCtrlBlock (pLspCtrlBlk);

                        return LDP_FAILURE;
                    }

                    u1Event = LDP_LSM_EVT_INT_SETUP;
                    LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)][u1Event]
                        (pLspCtrlBlk, NULL);
                    return LDP_SUCCESS;
                }
            }
        }
    }

    /* 
     * NOTE : Resource Reservation is done here, to check if the 
     * configured Traffic parameters could be met with,at the Ingress.
     * If the Tunnel is making use of another tunnel then no
     * additional resources will be reserved.
     */

    if ((CRLSP_TE_ERHOP_ADDR_TYPE (pFirstErHop) != CRLSP_TE_ERHOP_LSPID_TYPE)
        && (CRLSP_TE_TNL_TRFC_PARAM (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)) !=
            NULL))
    {
        /* Resource Manger will take care of reserving only the 
         * Modified set of resources if it's a crlsp modify.
         */

        if (LdpTcResvResources (pCrlspTnlInfo) != LDP_SUCCESS)
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Configured Rsrc for Cr-Lsp Not Available at Ingress\n");

            /* If the Tunnel is for modify, in order to avoid the bumping 
             * of the tunnel which is getting modified, the priority must 
             * be raised to the same of the modifying Tunnel.
             */

            if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL))
            {
                LdpCrlspModifyTnlPrioList (LCB_OUT_IFINDEX (CRLSP_LCB
                                                            (CRLSP_INS_POIN
                                                             (pCrlspTnlInfo))),
                                           CRLSP_HOLD_PRIO (CRLSP_INS_POIN
                                                            (pCrlspTnlInfo)),
                                           LCB_OUT_IFINDEX (CRLSP_LCB
                                                            ((pCrlspTnlInfo))),
                                           CRLSP_SET_PRIO ((pCrlspTnlInfo)),
                                           CRLSP_INS_POIN (pCrlspTnlInfo));
            }

            /* PreEmption Module is called for bumping if any lower
             * priority tunnels,with which the resources could be met. 
             * Only the Modified set of resources will be looked for
             * if it's a crlsp modify.
             */
            if (LdpCrlspGetTnlFromPrioList
                (LCB_OUT_IFINDEX (CRLSP_LCB (pCrlspTnlInfo)),
                 CRLSP_SET_PRIO (pCrlspTnlInfo), pCrlspTnlInfo,
                 pCandidatePreemp) != LDP_SUCCESS)
            {
                /* If the Tunnel is for modify, since the priority of the 
                 * earlier tunnel is raised, it's brought back to it's 
                 * original holding priority. */
                if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL))
                {
                    LdpCrlspModifyTnlPrioList (LCB_OUT_IFINDEX
                                               (CRLSP_LCB (pCrlspTnlInfo)),
                                               CRLSP_SET_PRIO (pCrlspTnlInfo),
                                               LCB_OUT_IFINDEX
                                               (CRLSP_LCB (CRLSP_INS_POIN
                                                           (pCrlspTnlInfo))),
                                               CRLSP_HOLD_PRIO
                                               (CRLSP_INS_POIN
                                                (pCrlspTnlInfo)),
                                               CRLSP_INS_POIN (pCrlspTnlInfo));
                }
                LdpDeleteLspCtrlBlock (pLspCtrlBlk);
                return LDP_FAILURE;
            }
            else
            {
                LdpPreEmptTunnels (pCandidatePreemp);
                /* Now the Resources will be available for reservation 
                 */
                LdpTcResvResources (pCrlspTnlInfo);
                /* If the Tunnel is for modify, since the priority of the earlier 
                 * tunnel is raised,it's brought back to it's original holding
                 * priority.
                 */
                if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL))
                {
                    LdpCrlspModifyTnlPrioList (LCB_OUT_IFINDEX
                                               (CRLSP_LCB ((pCrlspTnlInfo))),
                                               CRLSP_SET_PRIO ((pCrlspTnlInfo)),
                                               LCB_OUT_IFINDEX (CRLSP_LCB
                                                                (CRLSP_INS_POIN
                                                                 (pCrlspTnlInfo))),
                                               CRLSP_HOLD_PRIO (CRLSP_INS_POIN
                                                                (pCrlspTnlInfo)),
                                               CRLSP_INS_POIN (pCrlspTnlInfo));
                }
            }
        }
    }

    /* Tunnel is added to the Priority List */
    LdpCrlspAddTnlToPrioList (LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo))),
                              CRLSP_HOLD_PRIO (pCrlspTnlInfo), pCrlspTnlInfo);

    /* Invoke Advert State M/c for fwding, Cr-Lsp Label Request Msg */
    u1Event = LDP_LSM_EVT_INT_SETUP;
    LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)][u1Event] (pLspCtrlBlk, NULL);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpUpdateCrlspTnlErHopInfo                                */
/* Description   : This routine is called to update the CRLSP tunnel with    */
/*                 the ER Hops information received in the ER Hop TLV of the */
/*                 the label request message.                                */
/* Input(s)      : pCrlspTnlInfo, pu1ErHopTlv,u2IncarnId,pu1ErrType          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS in case of ERHOP information successfully     */
/*                 updated into the tunnel info structure LDP_FAILURE        */
/*                 otherwise.                                                */
/*****************************************************************************/
UINT1
LdpUpdateCrlspTnlErHopInfo (tCrlspTnlInfo * pCrlspTnlInfo,
                            UINT1 *pu1ErHopTlv,
                            UINT2 u2IncarnId, UINT1 *pu1ErrType)
{
    UINT1              *pu1TlvInfo = pu1ErHopTlv;
    UINT2               u2ErHopTlvLen;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT1               u1Temp;
    UINT4               u4HopIndex = 1;
    tTMO_SLL            HopList;
    tLdpTeHopInfo      *pTeHopInfo = NULL;
    tLdpTePathListInfo *pTePathListInfo = NULL;

    /* Added to suppress warnings */
    LDP_SUPPRESS_WARNING (u2IncarnId);

    /* Offset done to get the Length of the ER Hop TLV */
    pu1TlvInfo += LDP_OFF_TLV_LEN;
    LDP_EXT_2_BYTES (pu1TlvInfo, u2ErHopTlvLen);

    if (u2ErHopTlvLen == 0)
    {
        LDP_DBG (LDP_ADVT_PRCS, "ADVT: ER Hop TLV has no ER Hops !\n");
        *pu1ErrType = NO_ER_HOP_PRESENT;
        return LDP_FAILURE;
    }

    /* Initialising the Temp Hop List and the error type */
    TMO_SLL_Init (&HopList);
    *pu1ErrType = 0;

    while (pu1TlvInfo < (pu1ErHopTlv + u2ErHopTlvLen + LDP_TLV_HDR_LEN))
    {
        LDP_EXT_2_BYTES (pu1TlvInfo, u2TlvType);
        LDP_EXT_2_BYTES (pu1TlvInfo, u2TlvLen);

        if ((pu1TlvInfo + u2TlvLen) >
            (pu1ErHopTlv + u2ErHopTlvLen + LDP_OFF_MSG_ID))
        {
            *pu1ErrType = BAD_ER_HOP_TLV;
            break;
        }

        /* Allocating memory from the Temp TE HOP Pool for the HopInfo */
        pTeHopInfo = (tTeHopInfo *)
            MemAllocMemBlk (LDP_TEMP_TE_HOP_INFO_POOL_ID);

        if (pTeHopInfo == NULL)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Can't allocate mem for Temp teHopInfo.\n");
            *pu1ErrType = NO_ER_HOP_RESOURCE;
            break;
        }
        TMO_SLL_Init_Node (&CRLSP_TE_ERHOP_NEXT_NODE (pTeHopInfo));

        MEMSET ((VOID *) pTeHopInfo, 0, sizeof (tLdpTeHopInfo));

        /* Hop Info is added to the Temp Hop List */
        TMO_SLL_Add (&HopList,
                     (tTMO_SLL_NODE *) &
                     (CRLSP_TE_ERHOP_NEXT_NODE (pTeHopInfo)));

        switch (u2TlvType)
        {
            case CRLDP_ERHOP_TY_IPV4:

                LDP_EXT_1_BYTES (pu1TlvInfo, u1Temp);
                CRLSP_TE_ERHOP_TYPE (pTeHopInfo) =
                    ((u1Temp & LOOSE_CRLSP_PATH) != 0) ? CRLSP_TE_LOOSE_ER
                    : CRLSP_TE_STRICT_ER;
                LDP_EXT_1_BYTES (pu1TlvInfo, u1Temp);
                LDP_EXT_1_BYTES (pu1TlvInfo, u1Temp);
                LDP_EXT_1_BYTES (pu1TlvInfo,
                                 CRLSP_TE_ERHOP_ADDR_PRFX_LEN (pTeHopInfo));
                MEMCPY ((UINT1 *) (&(CRLSP_TE_ERHOP_IPV4_ADDR (pTeHopInfo))),
                        pu1TlvInfo, ROUTER_ID_LENGTH);
                pu1TlvInfo += ROUTER_ID_LENGTH;
                CRLSP_TE_ERHOP_ADDR_TYPE (pTeHopInfo) =
                    CRLSP_TE_ERHOP_IPV4_TYPE;
                CRLSP_TE_ERHOP_ROW_STATUS (pTeHopInfo) = ACTIVE;
                CRLSP_TE_ERHOP_INDEX (pTeHopInfo) = u4HopIndex++;
                break;

            case CRLDP_ER_HOP_LSPID:

                LDP_EXT_1_BYTES (pu1TlvInfo, u1Temp);
                CRLSP_TE_ERHOP_TYPE (pTeHopInfo) =
                    ((u1Temp & LOOSE_CRLSP_PATH) != 0) ? CRLSP_TE_LOOSE_ER
                    : CRLSP_TE_STRICT_ER;
                LDP_EXT_1_BYTES (pu1TlvInfo, u1Temp);
                LDP_EXT_2_BYTES (pu1TlvInfo, CRLSP_TE_ERHOP_LSPID (pTeHopInfo));
                MEMCPY ((UINT1 *) (&(CRLSP_TE_ERHOP_IPV4_ADDR (pTeHopInfo))),
                        pu1TlvInfo, ROUTER_ID_LENGTH);
                pu1TlvInfo += ROUTER_ID_LENGTH;
                CRLSP_TE_ERHOP_ADDR_TYPE (pTeHopInfo) =
                    CRLSP_TE_ERHOP_LSPID_TYPE;
                CRLSP_TE_ERHOP_ROW_STATUS (pTeHopInfo) = ACTIVE;
                CRLSP_TE_ERHOP_INDEX (pTeHopInfo) = u4HopIndex++;
                break;

            case CRLDP_ERHOP_TY_IPV6:
            case CRLDP_ER_HOP_ASNUM:
            default:

                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: These ER HOPS not supported now. !\n");
                *pu1ErrType = UNSUPPRTD_ER_HOP_TLV;
                break;
        }

        if (*pu1ErrType != 0)
        {
            break;
        }
    }

    if (*pu1ErrType == 0)
    {
        if (ldpTeCreateHopListInfo (&pTePathListInfo, &HopList)
            == LDP_TE_FAILURE)
        {
            *pu1ErrType = NO_ER_HOP_RESOURCE;
        }
    }

    /* Releasing all the Temp Hop Infos created and added to the Temp
     * Hop List. 
     */

    pTeHopInfo = (tTeHopInfo *) TMO_SLL_Get (&HopList);
    while (pTeHopInfo != NULL)
    {
        MemReleaseMemBlock (LDP_TEMP_TE_HOP_INFO_POOL_ID, (UINT1 *) pTeHopInfo);

        pTeHopInfo = (tTeHopInfo *) TMO_SLL_Get (&HopList);
    }

    if (*pu1ErrType == 0)
    {
        /* Assigning the HopList and PathOption related information into
         * the tunnel structure. */
        CRLSP_TNL_HOP_LIST_INFO (pCrlspTnlInfo) = pTePathListInfo;
        CRLSP_HOP_LIST_INDEX (pCrlspTnlInfo) =
            CRLSP_TE_HOPLIST_INDEX (pTePathListInfo);

        /* The Path List created will contain only one Path Option */
        CRLSP_TNL_PATH_INFO (pCrlspTnlInfo) =
            (tLdpTePathInfo *) TMO_SLL_First
            ((CRLSP_TE_HOP_PO_LIST (pTePathListInfo)));
        if (CRLSP_TNL_PATH_INFO (pCrlspTnlInfo) == NULL)
        {
            return LDP_FAILURE;
        }
        CRLSP_HOP_PO_INDEX (pCrlspTnlInfo) =
            CRLSP_TE_PO_INDEX (CRLSP_TNL_PATH_INFO (pCrlspTnlInfo));
        /*
         * Flag in Crlsp Tunnel information set with flag to indicate
         * that Tunnel has been assigned its associated ER Hop(s).
         */
        CRLSP_PARM_FLAG (pCrlspTnlInfo) |= LDP_ER_CRLSP;
        return LDP_SUCCESS;
    }
    else
    {
        return LDP_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : LdpUpdateCrlspTnlTrfcInfo                                 */
/* Description   : This routine is called to update the CRLSP tunnel with    */
/*                 the Traffic parameter related information received in the */
/*                 Traffic parameter TLV                                     */
/* Input(s)      : pCrlspTnlInfo, pu1TrfcParmTlv                             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpUpdateCrlspTnlTrfcInfo (tCrlspTnlInfo * pCrlspTnlInfo, UINT1 *pu1TrfcParmTlv)
{
    UINT1              *pu1TlvInfo = pu1TrfcParmTlv;

    /* Offset done to get to field flags */
    pu1TlvInfo += LDP_TLV_HDR_LEN;

    LDP_EXT_1_BYTES (pu1TlvInfo, CRLSP_TRF_FLAGS (pCrlspTnlInfo));
    LDP_EXT_1_BYTES (pu1TlvInfo, CRLSP_TRF_FREQ (pCrlspTnlInfo));
    pu1TlvInfo += LDP_UINT1_LEN;
    LDP_EXT_1_BYTES (pu1TlvInfo, CRLSP_TRF_WGT (pCrlspTnlInfo));
    LDP_EXT_4_BYTES (pu1TlvInfo, CRLSP_TRF_PD (pCrlspTnlInfo));
    LDP_EXT_4_BYTES (pu1TlvInfo, CRLSP_TRF_PB (pCrlspTnlInfo));
    LDP_EXT_4_BYTES (pu1TlvInfo, CRLSP_TRF_CD (pCrlspTnlInfo));
    LDP_EXT_4_BYTES (pu1TlvInfo, CRLSP_TRF_CB (pCrlspTnlInfo));
    LDP_EXT_4_BYTES (pu1TlvInfo, CRLSP_TRF_EB (pCrlspTnlInfo));
    return;
}

/*****************************************************************************/
/* Function Name : LdpUpdateCrlspTnlLspidInfo                                */
/* Description   : This routine is called to update the CRLSP tunnel with    */
/*                 the LSPID related information received in the LSPID TLV.  */
/* Input(s)      : pCrlspTnlInfo, pu1LspidTlv                                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpUpdateCrlspTnlLspidInfo (tCrlspTnlInfo * pCrlspTnlInfo, UINT1 *pu1LspidTlv)
{
    UINT1              *pu1TlvInfo = pu1LspidTlv;
    tCrlspLspIdTlv      LspidTlv;
    tCrlspTnlInfo      *pCrlspEarTnlInfo = NULL;
    UINT4               u4IncarnId = 0;
    UINT4               u4HIndex = 0;
    INT4                i4CrLspIndex = 0;

    /* Offset done to get to field flags */
    pu1TlvInfo += LDP_TLV_HDR_LEN;

    LDP_EXT_2_BYTES (pu1TlvInfo, LspidTlv.u2Reserved);

    LDP_EXT_2_BYTES (pu1TlvInfo, CRLSP_LSPID (pCrlspTnlInfo));
    MEMCPY ((UINT1 *) (&(CRLSP_INGRESS_LSRID (pCrlspTnlInfo))),
            pu1TlvInfo, ROUTER_ID_LENGTH);

    CRLSP_TNL_INDEX (pCrlspTnlInfo) = CRLSP_LSPID (pCrlspTnlInfo);

    /* Assuming this is the first instance of the tunnel. If it is not
     * the action flag will be set and so the instance gets modified
     * accordingly */
    CRLSP_TNL_INSTANCE (pCrlspTnlInfo) = 1;

    /* If Action flag is set the Request is treated as modify and
     * their Instance pointers are updated if the LSR is not the
     * Egress of the original tunnel. 
     */

    if (((LspidTlv.u2Reserved) & CRLSP_ACT_FLAG) == LDP_TRUE)
    {
        /* If the tunnel is a modify of the existing tunnel the
         * modication of the tunnel bit will be set,but the 
         * cross pointer to the earlier tunnel will be linked
         * only when the earlier tunnel is not the egress.
         * (this is done as the Cross pointer functionality is 
         * related to Resource Manager and Preemtion Module).
         */
        CRLSP_IS_MOD_OF_TUNN (pCrlspTnlInfo) = LDP_TRUE;

        /* Validate the Tunnel Index. */
        i4CrLspIndex = ((INT4) (CRLSP_LSPID (pCrlspTnlInfo)));
        u4HIndex = (UINT4)LDP_COMPUTE_HASH_INDEX (i4CrLspIndex);

        u4IncarnId = SSN_GET_INCRN_ID (LCB_USSN (CRLSP_LCB (pCrlspTnlInfo)));

        /* Searching in the Cr-Lsp Tunnel Table */
        TMO_HASH_Scan_Bucket (LDP_CRLSP_TNL_TABLE (u4IncarnId),
                              u4HIndex, pCrlspEarTnlInfo, tCrlspTnlInfo *)
        {
            if ((CRLSP_TNL_INDEX (pCrlspTnlInfo) ==
                 CRLSP_TNL_INDEX (pCrlspEarTnlInfo)) &&
                (MEMCMP (CRLSP_INGRESS_LSRID (pCrlspTnlInfo),
                         CRLSP_INGRESS_LSRID (pCrlspEarTnlInfo),
                         LDP_IPV4ADR_LEN) == 0))
            {
                /* Since the Request is a modification, the two instances
                 * must differ to uniquely identify the tunnel. */
                /* The assumption is there can be only two instance of a
                 * tunnel at any given time. So if a tunnel with instance
                 * '1' is found, the new instance is given a value '2'.
                 * If a tunnel with instance '2' is found, it is given a
                 * value '1'.
                 */
                CRLSP_TNL_INSTANCE (pCrlspTnlInfo) =
                    (CRLSP_TNL_INSTANCE (pCrlspEarTnlInfo) == 1) ? 2 : 1;

                if (LCB_DSSN (CRLSP_LCB (pCrlspEarTnlInfo)) != NULL)
                {
                    /* pCrlspEarTnlInfo is the tunnel Info of an
                     * Intermediate LSR. 
                     */
                    if (CRLSP_INS_POIN (pCrlspEarTnlInfo) != NULL)
                    {
                        LDP_DBG (LDP_ADVT_PRCS,
                                 "ADVT: Third Inst. of an existing tunnel\n");
                        return LDP_FAILURE;
                    }
                    CRLSP_INS_POIN (pCrlspTnlInfo) = pCrlspEarTnlInfo;
                    CRLSP_INS_POIN (pCrlspEarTnlInfo) = pCrlspTnlInfo;
                }
            }
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgCrlspIdRequest                                   */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and Label Request is received and the label    */
/*                 request received is for a CRLSP.                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgCrlspIdRequest (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    UINT1               u1ErrType = LDP_STAT_TYPE_SUCCESS;
    UINT1               u1MandLspIdPrnst = LDP_FALSE;
    UINT2               u2IncarnId = 0;
    UINT1              *pu1LspidTlv = NULL;
    UINT1              *pu1ErHopTlv = NULL;
    UINT1              *pu1TrfcParmTlv = NULL;
    UINT1              *pu1PinningTlv = NULL;
    UINT1              *pu1RsrcClsTlv = NULL;
    UINT1              *pu1PremptTlv = NULL;
    UINT1              *pu1MsgPtr = pMsg;
    UINT2               u2MsgLen = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLen = 0;
    UINT4               u4MsgId = 0;
    UINT4               u4SessIfIndex = 0;
    uLabel              Label;
    tLdpSession        *pUssn = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;
    tLdpTeTnlInfo      *pTeTnlInfo = NULL;
    tLdpTeTnlInfo       TempTeTnlInfo;
    tTMO_HASH_TABLE    *pCrlspTnlHashtbl = NULL;
    UINT4               u4HIndex = 0;
    tLdpTeTrfcParams   *pTeTrfcParams = NULL;
    tLdpTeTrfcParams    TempTeTrfcParams;
    tLdpTeCRLDPTrfcParams TmpLdpTeCRLDPTrfcParams;
    tTMO_SLL            PreEmpList;
    tTMO_SLL           *pCandidatePreemp = (tTMO_SLL *) & PreEmpList;
    UINT1              *pu1FirstDiffServTlv = NULL;
    UINT1              *pu1ElspTPTlv = NULL;
    UINT1              *pu1ClassTypeTlv = NULL;
    UINT1               u1DiffServTlvCount = 0;
    UINT1               u1ClassTypeTlvCount = 0;
    UINT1               u1MaxPscCount = 0;
    tMplsDiffServTnlInfo *pDiffServParams = NULL;
    tTMO_SLL           *pErHopList = NULL;
    tLdpTeHopInfo      *pHop = NULL;

    /* The message length is obtained first */
    pu1MsgPtr += LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Msg pointer now points to the message id, the message id is obtained */
    LDP_EXT_4_BYTES (pu1MsgPtr, u4MsgId);
    LCB_UREQ_ID (pLspCtrlBlock) = u4MsgId;

    /* For FecElmntType == LDP_FEC_CRLSP_TYPE, only IPV4 address family is supported */
    pLspCtrlBlock->Fec.u2AddrFmly = IPV4;

    /* 
     * Mandatory parameter FEC TLV already extracted in this case,
     * The pointer is just made to move to access the next set of TLVS.
     */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
    pu1MsgPtr += u2TlvLen;

    /* optional TLVS if present are extracted  from the message */
    while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
    {
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        /* Check for bad TLV Length performed here */
        if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
        {
            /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
            LDP_DBG1 (LDP_ADVT_MISC,
                      "ADVT: Notif Msg Snt - BAD TLV LEN - %x \n",
                      LDP_STAT_BAD_TLV_LEN);
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_BAD_TLV_LEN, NULL);
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }

        switch (u2TlvType)
        {
            case CRLDP_EXPLROUTE_TLV:
                pu1ErHopTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case CRLDP_TRAFPARM_TLV:
                pu1TrfcParmTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                /* Check done for the correctness of the PDR and 
                 * CDR Value. If found erroneous notificaiton message sent 
                 * with the notification type set to Traffic parameters
                 * unavilable - 0x44000006.
                 */
                {
                    UINT1              *pu1TmpTrfcParmTlv = NULL;
                    UINT4               u4Pdr;
                    UINT4               u4Cdr;
                    pu1TmpTrfcParmTlv = pu1TrfcParmTlv;
                    LDP_GET_4_BYTES (pu1TmpTrfcParmTlv +
                                     LDP_CRLSP_PDR_OFFSET_VAL, u4Pdr);
                    LDP_GET_4_BYTES (pu1TmpTrfcParmTlv +
                                     LDP_CRLSP_CDR_OFFSET_VAL, u4Cdr);
                    if (u4Cdr > u4Pdr)
                    {
                        LDP_DBG (LDP_ADVT_MISC, "ADVT: CDR > PDR.\n");
                        LDP_DBG1 (LDP_ADVT_MISC,
                                  "ADVT: Notif Msg Snt - Trf Prms Unavl - %x \n",
                                  LDP_STAT_TRAF_PARMS_UNAVL);
                        LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg,
                                         LDP_FALSE,
                                         LDP_STAT_TYPE_TRAF_PARMS_UNAVL, NULL);
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                        return;
                    }
                }
                break;

            case CRLDP_LSPID_TLV:
                pu1LspidTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                u1MandLspIdPrnst = LDP_TRUE;
                break;

            case LDP_HOP_COUNT_TLV:
                break;
            case LDP_PATH_VECTOR_TLV:
                break;
            case CRLDP_PINNING_TLV:
                pu1PinningTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case CRLDP_RESCLS_TLV:
                pu1RsrcClsTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case CRLDP_PREEMPT_TLV:
                pu1PremptTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case CRLDP_DIFFSERV_TLV:

                /* In Order to ignore the extra TLVs */
                if (u1DiffServTlvCount == 0)
                {
                    pu1FirstDiffServTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                    u1DiffServTlvCount++;
                }
                break;

            case CRLDP_DIFFSERV_ELSPTP_TLV:
                pu1ElspTPTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case CRLDP_DIFFSERV_CLASSTYPE_TLV:
                if (u1ClassTypeTlvCount == 0)
                {
                    u1ClassTypeTlvCount++;
                }
                pu1ClassTypeTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            default:
                break;

        }
        pu1MsgPtr += u2TlvLen;
    }

    if (u1MandLspIdPrnst == LDP_FALSE)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: No Mandatory LSPID Tlv in Crlsp LblReq msg\n");
        LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }

    /* For FEC Tlvs of Type CR-LSP, Er-Tlv is considered as mandatory */
    if (pu1ErHopTlv == NULL)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: NO Mandatory ERHOP Tlv in Crlsp LblReq msg !\n");
        LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_CRLSP_SETUP_ABORT, NULL);
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }

    /* Incarnation id is obtained for mem pool access and allocations */
    pUssn = LCB_USSN (pLspCtrlBlock);
    u2IncarnId = SSN_GET_INCRN_ID (pUssn);

    /* CRLSP Tunnel structure is obtained from mem pool */
    pCrlspTnlInfo = (tCrlspTnlInfo *) MemAllocMemBlk (LDP_CRLSPTNL_POOL_ID);

    if (pCrlspTnlInfo == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: Can't Allocate Mem for CRLSP Tunnel.No Lsp estd\n");
        LDP_DBG1 (LDP_ADVT_MISC, "ADVT: Notif Msg Snt - No Lbl Rsrc - %x \n",
                  LDP_STAT_NO_LBL_RSRC);
        LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_RESOURCE_UNAVAIL, NULL);
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }

    /* Initialising the CRLSP Tunnel information with default values */
    LDP_INIT_CRLSP_TNL_INFO (pCrlspTnlInfo);

    MEMSET ((VOID *) &TempTeTnlInfo, 0, sizeof (tLdpTeTnlInfo));

    /* Associating the TempTnlInfo with the tunnel. */
    CRLSP_TE_TNL_INFO (pCrlspTnlInfo) = &TempTeTnlInfo;

    CRLSP_TNL_ROLE (pCrlspTnlInfo) = LDP_TE_INTER;
    CRLSP_ROW_STATUS (pCrlspTnlInfo) = ACTIVE;
    CRLSP_ADMIN_STATUS (pCrlspTnlInfo) = LDP_ADMIN_UP;
    CRLSP_OPER_STATUS (pCrlspTnlInfo) = LDP_OPER_UP;
    (CRLSP_TE_TNL_INFO (pCrlspTnlInfo))->u1CPOrMgmtOperStatus = LDP_OPER_UP;
    CRLSP_SGNL_PRTCL (pCrlspTnlInfo) = LDP_TE_PROT_ID;
    CRLSP_TE_TNL_OWNER (CRLSP_TE_TNL_INFO (pCrlspTnlInfo))
        = CRLDP_TE_TNL_OWNER_LDP;

    /* Tunnel obtained. Tunnel information is linked appropriately */
    pLspCtrlBlock->pCrlspTnlInfo = pCrlspTnlInfo;
    pCrlspTnlInfo->pLspCtrlBlock = pLspCtrlBlock;

    /* Update Recevied/Configured Er-Hop Info into TnlInfo data structure. */
    if (LdpUpdateCrlspTnlErHopInfo (pCrlspTnlInfo, pu1ErHopTlv, u2IncarnId,
                                    &u1ErrType) == LDP_FAILURE)
    {
        switch (u1ErrType)
        {
            case NO_ER_HOP_PRESENT:
                /* Case of no ER hop prsnt - Ref Sec 4.8.1 pg 22 Crldp-02.txt */
                LDP_DBG1 (LDP_ADVT_MISC,
                          "ADVT: Notif Msg Snt - No ER Hop Prsnt - %x \n",
                          LDP_STAT_BAD_EROUTE_TLV_ERR);
                LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                      LDP_STAT_TYPE_BAD_EROUTE_TLV_ERR, pMsg);
                break;

            case BAD_ER_HOP_TLV:
                LDP_DBG1 (LDP_ADVT_MISC,
                          "ADVT: Notif Msg Snt - Bad ER Hop - %x \n",
                          LDP_STAT_BAD_EROUTE_TLV_ERR);
                LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                      LDP_STAT_TYPE_BAD_EROUTE_TLV_ERR, pMsg);
                break;

            case UNSUPPRTD_ER_HOP_TLV:
                LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                      LDP_STAT_TYPE_NO_ROUTE, pMsg);
                break;

            case NO_ER_HOP_RESOURCE:
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Can't Allocate Mem for ER HOP info.No"
                         "Lsp estd\n");
                LDP_DBG1 (LDP_ADVT_MISC,
                          "ADVT: Notif Msg Snt - Rsrc Unavl - %x \n",
                          LDP_STAT_NO_LBL_RSRC);
                LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                      LDP_STAT_TYPE_RESOURCE_UNAVAIL, pMsg);
                break;
            default:
                break;
        }
        MemReleaseMemBlock (LDP_CRLSPTNL_POOL_ID, (UINT1 *) pCrlspTnlInfo);
        pLspCtrlBlock->pCrlspTnlInfo = NULL;
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }

    /* Engress Info is required for TE.MIB to create 
     * Entry in Tunnel Table so, get last ErHop address
     * as Egress and use it in Tunnel Table. This Logic
     * will not work if there is no ErHop in the Label
     * Request Message */
    pErHopList = &(CRLSP_TE_HOP_LIST (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)));
    if (pErHopList != NULL)
    {
        pHop = (tLdpTeHopInfo *) TMO_SLL_Last (pErHopList);
        if (pHop == NULL)
        {
            LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                  LDP_STAT_TYPE_INTERNAL_ERROR, pMsg);
            pLspCtrlBlock->pCrlspTnlInfo = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }
    }
    MEMCPY (TempTeTnlInfo.TnlEgressLsrId, &pHop->IpAddr, LDP_NET_ADDR_LEN);

    /* Process the Er-Hop List that is received in the Request Msg. */
    if (LdpPrcsAndUpdateErHopList (u2IncarnId, pLspCtrlBlock, &u1ErrType)
        == LDP_FAILURE)
    {
        /* Error occured while processing the Er-Hop List */
        LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                              u1ErrType, pMsg);
        /* Deleting the HopList created */
        ldpTeDeleteHopListInfo (CRLSP_TNL_HOP_LIST_INFO (pCrlspTnlInfo));
        MemReleaseMemBlock (LDP_CRLSPTNL_POOL_ID, (UINT1 *) pCrlspTnlInfo);
        pLspCtrlBlock->pCrlspTnlInfo = NULL;
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }

    /* Traffic params if present is updated into the tunnel
     * info, only if it is not the egress. */
    if ((pu1TrfcParmTlv != NULL) && (CRLSP_TNL_ROLE (pCrlspTnlInfo)
                                     != LDP_TE_EGRESS))
    {
        MEMSET ((VOID *) &TempTeTrfcParams, 0, sizeof (tLdpTeTrfcParams));
        MEMSET ((VOID *) &TmpLdpTeCRLDPTrfcParams, 0,
                sizeof (tLdpTeCRLDPTrfcParams));

        /* Associating the TempTeTrfcParam with the tunnel. */
        CRLSP_TRF_PARMS (pCrlspTnlInfo) = &TempTeTrfcParams;
        CRLSP_TE_CRLDP_TRFC_PARAMS ((&TempTeTrfcParams)) =
            &TmpLdpTeCRLDPTrfcParams;

        LdpUpdateCrlspTnlTrfcInfo (pCrlspTnlInfo, pu1TrfcParmTlv);

        CRLSP_TE_TRFC_PARAM_ROLE ((&TempTeTrfcParams)) = LDP_TE_INTER;
        CRLSP_TE_TNLRSRC_ROW_STATUS ((&TempTeTrfcParams)) = ACTIVE;
        CRLSP_TE_TRFC_PARAM_NUM_TNLS ((&TempTeTrfcParams)) = 1;

        /* Memory allocation for TE Traffic Param Structure */
        if (ldpTeCreateTrfcParams (LDP_TE_PROT_ID, &pTeTrfcParams,
                                   &TempTeTrfcParams) == LDP_TE_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MISC, "TE Traffic Parms Mem Alloc failure \n");
            LDP_DBG1 (LDP_ADVT_MISC,
                      "ADVT: Notif Msg Snt - No Lbl Rsrc - %x \n",
                      LDP_STAT_NO_LBL_RSRC);
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_RESOURCE_UNAVAIL, NULL);
            ldpTeDeleteHopListInfo (CRLSP_TNL_HOP_LIST_INFO (pCrlspTnlInfo));
            MemReleaseMemBlock (LDP_CRLSPTNL_POOL_ID, (UINT1 *) pCrlspTnlInfo);
            pLspCtrlBlock->pCrlspTnlInfo = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }
        CRLSP_RES_INDEX (pCrlspTnlInfo) =
            CRLSP_TE_TRFC_PARAM_INDEX (pTeTrfcParams);
        CRLSP_TRF_PARMS (pCrlspTnlInfo) = pTeTrfcParams;

        /*
         * Flag in Crlsp Tunnel information set with flag to indicate that 
         * Tunnel traffic paramter values has been assinged.
         */
        CRLSP_PARM_FLAG (pCrlspTnlInfo) |= LDP_TR_CRLSP;
    }
    else
    {
        /* case of traffic parameter not present or egress */
        CRLSP_RES_INDEX (pCrlspTnlInfo) = 0;
        CRLSP_TRF_PARMS (pCrlspTnlInfo) = NULL;
    }

    /* Pinning TLV if present are updated into the tunnel information */
    if (pu1PinningTlv != NULL)
    {
        pu1PinningTlv += LDP_TLV_HDR_LEN;
        LDP_EXT_1_BYTES (pu1PinningTlv, CRLSP_RTPIN_TYPE (pCrlspTnlInfo));
        /*
         * Flag in Crlsp Tunnel information set with flag to indicate that 
         * Tunnel path is pinned or not.
         */
        CRLSP_PARM_FLAG (pCrlspTnlInfo) |= LDP_PN_CRLSP;
    }

    /* ResourceClass TLV if present are updated into the tunnel information */
    if (pu1RsrcClsTlv != NULL)
    {
        pu1RsrcClsTlv += LDP_TLV_HDR_LEN;
        LDP_EXT_4_BYTES (pu1RsrcClsTlv,
                         CRLSP_TE_TNL_INC_ANY_AFFINITY (CRLSP_TE_TNL_INFO
                                                        (pCrlspTnlInfo)));
        /*
         * Flag in Crlsp Tunnel information set with flag to indicate that 
         * Tunnel path is Resource class based or not.
         */
        CRLSP_PARM_FLAG (pCrlspTnlInfo) |= LDP_RSRC_CRLSP;
    }

    /* Premption TLV if present are updated into the tunnel information */
    if (pu1PremptTlv != NULL)
    {
        pu1PremptTlv += LDP_TLV_HDR_LEN;
        LDP_EXT_1_BYTES (pu1PremptTlv, CRLSP_SET_PRIO (pCrlspTnlInfo));
        LDP_EXT_1_BYTES (pu1PremptTlv, CRLSP_HOLD_PRIO (pCrlspTnlInfo));
        CRLSP_PARM_FLAG (pCrlspTnlInfo) |= LDP_PREMPT_CRLSP;
    }

    /* 
     * Ingress router id and lspid from the message are copied into the 
     * tunnel information
     */

    if (LdpUpdateCrlspTnlLspidInfo (pCrlspTnlInfo, pu1LspidTlv) == LDP_FAILURE)
    {
        if (CRLSP_TNL_ROLE (pCrlspTnlInfo) != LDP_TE_EGRESS)
        {
            ldpTeDeleteHopListInfo (CRLSP_TNL_HOP_LIST_INFO (pCrlspTnlInfo));
            ldpTeDeleteTrfcParams (CRLSP_TRF_PARMS (pCrlspTnlInfo));
        }
        MemReleaseMemBlock (LDP_CRLSPTNL_POOL_ID, (UINT1 *) pCrlspTnlInfo);
        pLspCtrlBlock->pCrlspTnlInfo = NULL;
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }

    if (LdpDiffServProcHandleDSTlv (pLspCtrlBlock, pu1FirstDiffServTlv, pMsg,
                                    pu1ClassTypeTlv) == LDP_DS_FAILURE)
    {
        if (CRLSP_TNL_ROLE (pCrlspTnlInfo) != LDP_TE_EGRESS)
        {
            ldpTeDeleteHopListInfo (CRLSP_TNL_HOP_LIST_INFO (pCrlspTnlInfo));
            ldpTeDeleteTrfcParams (CRLSP_TRF_PARMS (pCrlspTnlInfo));
        }
        MemReleaseMemBlock (LDP_CRLSPTNL_POOL_ID, (UINT1 *) pCrlspTnlInfo);
        pLspCtrlBlock->pCrlspTnlInfo = NULL;
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }

    if (pu1ElspTPTlv != NULL)
    {
        if ((LdpDiffServHandleElspTPTlv (pLspCtrlBlock,
                                         pMsg, pu1ElspTPTlv) ==
             LDP_DS_FAILURE) ||
            (LdpDiffServElspTPForRM (pLspCtrlBlock, pMsg) == LDP_FAILURE))
        {
            if (CRLSP_TNL_ROLE (pCrlspTnlInfo) != LDP_TE_EGRESS)
            {
                ldpTeDeleteHopListInfo (CRLSP_TNL_HOP_LIST_INFO
                                        (pCrlspTnlInfo));
                ldpTeDeleteTrfcParams (CRLSP_TRF_PARMS (pCrlspTnlInfo));
            }
            if (LDP_TE_MPLS_DIFFSERV_TNL_INFO
                (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)) != NULL)
            {
                ldpTeDeleteDiffServTnl (LDP_TE_MPLS_DIFFSERV_TNL_INFO
                                        (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)));
            }

            MemReleaseMemBlock (LDP_CRLSPTNL_POOL_ID, (UINT1 *) pCrlspTnlInfo);
            pLspCtrlBlock->pCrlspTnlInfo = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }
    }

    /* Update the information of PerOATrfcProfileEntry */
    if (LdpDiffServTrfcRsrcForLLsp (pLspCtrlBlock, pMsg) == LDP_DS_FAILURE)
    {
        if (CRLSP_TNL_ROLE (pCrlspTnlInfo) != LDP_TE_EGRESS)
        {
            ldpTeDeleteHopListInfo (CRLSP_TNL_HOP_LIST_INFO (pCrlspTnlInfo));
            ldpTeDeleteTrfcParams (CRLSP_TRF_PARMS (pCrlspTnlInfo));
        }
        if (LDP_TE_MPLS_DIFFSERV_TNL_INFO
            (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)) != NULL)
        {
            ldpTeDeleteDiffServTnl (LDP_TE_MPLS_DIFFSERV_TNL_INFO
                                    (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)));
        }
        MemReleaseMemBlock (LDP_CRLSPTNL_POOL_ID, (UINT1 *) pCrlspTnlInfo);
        pLspCtrlBlock->pCrlspTnlInfo = NULL;
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }

    /* Creating the TE Tunnel Info in the TE Module
     * This creates the tunnel info in the TE Module and adds it to
     * its hash table */
    if (ldpTeCreateNewTnl (&pTeTnlInfo, &TempTeTnlInfo) == LDP_TE_FAILURE)
    {
        LDP_DBG (LDP_ADVT_MISC, "TE Tunnel creation failure \n");
        LDP_DBG1 (LDP_ADVT_MISC, "ADVT: Notif Msg Snt - No Lbl Rsrc - %x \n",
                  LDP_STAT_NO_LBL_RSRC);
        LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_RESOURCE_UNAVAIL, NULL);
        if (CRLSP_TNL_HOP_LIST_INFO (pCrlspTnlInfo) != NULL)
        {
            ldpTeDeleteHopListInfo (CRLSP_TNL_HOP_LIST_INFO (pCrlspTnlInfo));
        }
        if (CRLSP_TRF_PARMS (pCrlspTnlInfo) != NULL)
        {
            ldpTeDeleteTrfcParams (CRLSP_TRF_PARMS (pCrlspTnlInfo));
        }
        if (LDP_TE_MPLS_DIFFSERV_TNL_INFO
            (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)) != NULL)
        {
            ldpTeDeleteDiffServTnl (LDP_TE_MPLS_DIFFSERV_TNL_INFO
                                    (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)));
        }
        if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
        {
            CRLSP_INS_POIN (CRLSP_INS_POIN (pCrlspTnlInfo)) = NULL;
        }
        MemReleaseMemBlock (LDP_CRLSPTNL_POOL_ID, (UINT1 *) pCrlspTnlInfo);
        pLspCtrlBlock->pCrlspTnlInfo = NULL;
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        return;
    }
    CRLSP_TE_TNL_INFO (pCrlspTnlInfo) = pTeTnlInfo;

    /* Add Tunnel Information to the Tunnel Table */
    u4HIndex = LDP_COMPUTE_HASH_INDEX ((UINT4) CRLSP_TNL_INDEX (pCrlspTnlInfo));
    pCrlspTnlHashtbl = LDP_CRLSP_TNL_TBL (u2IncarnId);
    TMO_HASH_Add_Node (pCrlspTnlHashtbl,
                       (tTMO_HASH_NODE *) (&(pCrlspTnlInfo->HashLinkNode)),
                       u4HIndex, NULL);

    if (LCB_DSSN (pLspCtrlBlock) == NULL)
    {
        /*  Lsr is Egress. Send Label Mapping to Upstream and
         *  set LCB state to Established.
         *  Resources need not be reserved.
         */

        u4SessIfIndex = LdpSessionGetIfIndex(LCB_USSN (pLspCtrlBlock),pLspCtrlBlock->Fec.u2AddrFmly);

        if ((LDP_ENTITY_PHP_CONF (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock)))
             == MPLS_IMPLICIT_NULL_LABEL) ||
            (LDP_ENTITY_PHP_CONF (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock)))
             == MPLS_IPV4_EXPLICIT_NULL_LABEL))
        {
            Label.u4GenLbl
                =
                (UINT4)LDP_ENTITY_PHP_CONF (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock)));
            LDP_DBG1 (LDP_ADVT_MISC,
                      "ADVT: PHP Configured with Label Value - %x \n",
                      Label.u4GenLbl);

            MEMCPY (&LCB_ULBL (pLspCtrlBlock), &Label, sizeof (uLabel));

            LdpSendCrlspLblMappingMsg (pLspCtrlBlock);

            LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;
            return;
        }

        if (LdpGetLabel (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock)), &Label,
                         u4SessIfIndex) != LDP_SUCCESS)
        {
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: Label allocation failed !\n");
            LDP_DBG1 (LDP_ADVT_MISC,
                      "ADVT: Notif Msg Snt - No Lbl Rsrc - %x \n",
                      LDP_STAT_NO_LBL_RSRC);
            LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                  LDP_STAT_TYPE_NO_LBL_RSRC, pMsg);
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }
        MEMCPY (&LCB_ULBL (pLspCtrlBlock), &Label, sizeof (uLabel));
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP,
                               pLspCtrlBlock, NULL);

        LdpSendCrlspLblMappingMsg (pLspCtrlBlock);

        LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;
        return;
    }

    pDiffServParams = LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);

    if ((LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServParams) == LDP_DS_ELSP) ||
        (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServParams) == LDP_DS_LLSP))
    {
        if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo) ==
            LDP_DS_PEROABASED_RESOURCES)
        {
            if ((pu1TrfcParmTlv == NULL) || (pu1ElspTPTlv == NULL))
            {
                /*No need of doing reservation */
                LdpSendCrlspLblReqMsg (pLspCtrlBlock);
                LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_RSP_AWT;
                return;
            }
            LdpDiffServHandlePerOARsrc (pLspCtrlBlock, pMsg);
            return;

        }
        else if (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo) ==
                 LDP_DS_CLASSTYPE_RESOURCES)
        {
            if (pu1TrfcParmTlv != NULL)
            {
                /*Do the resource management in ClassType basis */
                if (LdpDSTcResvResourcesInClassType (pCrlspTnlInfo)
                    == LDP_DS_FAILURE)
                {
                    LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                          pLspCtrlBlock,
                                          LDP_STAT_TYPE_RESOURCE_UNAVAIL, pMsg);
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    return;
                }
            }
            LdpSendCrlspLblReqMsg (pLspCtrlBlock);
            LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_RSP_AWT;
            return;
        }
    }

    /* When the request is forwarded at the intermediate LSR the resources 
     * won't be reserved inside the stacked tunnel i.e until the egress of the
     * original tunnel. 
     */
    /* If the Traffic Parameter is not present, then no allocation is
     * done. */

    if ((pu1TrfcParmTlv != NULL) &&
        ((CRLSP_STACK_TUNN (pCrlspTnlInfo) == LDP_FALSE) ||
         (CRLSP_IS_EGR_OF_TUNN (pCrlspTnlInfo) == LDP_TRUE)))
    {
        if (LdpTcResvResources (pCrlspTnlInfo) != LDP_SUCCESS)
        {
            /* If the Tunnel is for modify, in order to avoid the
             * bumping of the tunnel which is getting modified, the
             * priority must be raised to the same of the modifying
             * Tunnel.
             */
            if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
            {
                LdpCrlspModifyTnlPrioList (LCB_OUT_IFINDEX
                                           (CRLSP_LCB
                                            (CRLSP_INS_POIN
                                             (pCrlspTnlInfo))),
                                           CRLSP_HOLD_PRIO
                                           (CRLSP_INS_POIN
                                            (pCrlspTnlInfo)),
                                           LCB_OUT_IFINDEX
                                           (CRLSP_LCB (pCrlspTnlInfo)),
                                           CRLSP_SET_PRIO
                                           (pCrlspTnlInfo),
                                           CRLSP_INS_POIN (pCrlspTnlInfo));
            }

            /* PreEmption Module is called for bumping if any lower
             * priority tunnels,with which the resources could be met. 
             * Only the Modified set of resources will be looked for
             * if it's a crlsp modify.
             */
            if (LdpCrlspGetTnlFromPrioList
                (LCB_OUT_IFINDEX (CRLSP_LCB (pCrlspTnlInfo)),
                 CRLSP_SET_PRIO (pCrlspTnlInfo), pCrlspTnlInfo,
                 pCandidatePreemp) != LDP_SUCCESS)
            {
                /*  Failed to get resources even by preemption
                 *  Send NAK UpStrm, if Lsr is not Ingress.
                 */
                if (LCB_USSN (pLspCtrlBlock) != NULL)
                {
                    LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                          pLspCtrlBlock,
                                          LDP_STAT_TYPE_RESOURCE_UNAVAIL, pMsg);
                }

                if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
                {
                    LdpCrlspModifyTnlPrioList (LCB_OUT_IFINDEX
                                               (CRLSP_LCB
                                                ((pCrlspTnlInfo))),
                                               CRLSP_SET_PRIO
                                               (pCrlspTnlInfo),
                                               LCB_OUT_IFINDEX
                                               (CRLSP_LCB
                                                (CRLSP_INS_POIN
                                                 (pCrlspTnlInfo))),
                                               CRLSP_HOLD_PRIO
                                               (CRLSP_INS_POIN
                                                (pCrlspTnlInfo)),
                                               CRLSP_INS_POIN (pCrlspTnlInfo));
                }
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return;
            }
        }
        else
        {
            /* Tunnel is added to the Priority List */
            LdpCrlspAddTnlToPrioList (LCB_OUT_IFINDEX
                                      (CRLSP_LCB ((pCrlspTnlInfo))),
                                      CRLSP_HOLD_PRIO (pCrlspTnlInfo),
                                      pCrlspTnlInfo);

            /* 
             * Lsr is Intermediate to the Cr-Lsp being estblished.
             * Forward Cr-Lsp Label Req Msg DownStream and set LCB 
             * state to ResponseAwait.
             */
            LdpSendCrlspLblReqMsg (pLspCtrlBlock);
            LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_RSP_AWT;
            return;
        }
        LdpPreEmptTunnels (pCandidatePreemp);
        /* Now the Resources will be available for reservation */
        LdpTcResvResources (pCrlspTnlInfo);
        if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
        {
            LdpCrlspModifyTnlPrioList (LCB_OUT_IFINDEX
                                       (CRLSP_LCB (pCrlspTnlInfo)),
                                       CRLSP_SET_PRIO (pCrlspTnlInfo),
                                       LCB_OUT_IFINDEX
                                       (CRLSP_LCB
                                        (CRLSP_INS_POIN
                                         (pCrlspTnlInfo))),
                                       CRLSP_HOLD_PRIO
                                       (CRLSP_INS_POIN
                                        (pCrlspTnlInfo)),
                                       CRLSP_INS_POIN (pCrlspTnlInfo));
        }

        /* Tunnel is added to the Priority List */
        LdpCrlspAddTnlToPrioList (LCB_OUT_IFINDEX
                                  (CRLSP_LCB (pCrlspTnlInfo)),
                                  CRLSP_HOLD_PRIO (pCrlspTnlInfo),
                                  pCrlspTnlInfo);
        /* 
         * Lsr is Intermediate to the Cr-Lsp being estblished.
         * Forward Cr-Lsp Label Req Msg DownStream and set LCB 
         * state to ResponseAwait.
         */
        LdpSendCrlspLblReqMsg (pLspCtrlBlock);
        LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_RSP_AWT;
        return;
    }
    else
    {
        /* Stacked tunnel request is Simply forwarded */
        /* Lsr is Intermediate to the Cr-Lsp being estblished.
         * Forward Cr-Lsp Label Req Msg DownStream and set LCB 
         * state to ResponseAwait.
         */
        /* OR Request for tunnel with no traffic parameter is
         * simply forwarded */
        LdpSendCrlspLblReqMsg (pLspCtrlBlock);
        LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_RSP_AWT;
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpPrcsAndUpdateErHopList                                 */
/* Description   : This routine is called to process the Er-Hop List that is */
/*                 recvd from UpStr(or Er-Hop List configured at Ingress).   */
/*                 Processing of a Er-Hop list includes the following :      */
/*                   - Delete the Er-Hops to which the present Lsr belongs.  */
/*                   - Update the Er-Hop List in LCB's TnlInfo.              */
/*                   - Update LCB with DStr Ssn over which the Req Msg needs */
/*                     to be forwarded in case Lsr is Intermediate.          */
/*                   - Update whether LCB is acting Active/Passive in Lsp    */
/*                     establishment.                                        */
/*                                                                           */
/* Input(s)      : u2IncarnId, pLCB                                          */
/* Output(s)     : Updated LCB.                                              */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpPrcsAndUpdateErHopList (UINT2 u2IncarnId, tLspCtrlBlock * pLCB,
                           UINT1 *pu1PrcsStatus)
{
    UINT1               u1IsEgrOfHop = LDP_FALSE;
    /* If the First Er-Hop of current Er-Hop List is processed, then
     * u1FirstHopPrcsd is set to LDP_TRUE */
    UINT1               u1FirstHopPrcsd = LDP_FALSE;
    UINT4               u4GwAddr;
    UINT2               u2OutIfIndex;
    tLdpSession        *pDStrSsn = NULL;

    tCrlspTnlInfo      *pCrlspTnlInfo = pLCB->pCrlspTnlInfo;
    tLdpTePathListInfo *pTePathListInfo = CRLSP_TNL_HOP_LIST_INFO
        (pCrlspTnlInfo);
    tLdpTePathInfo     *pTePathInfo = CRLSP_TNL_PATH_INFO (pCrlspTnlInfo);
    tTMO_SLL           *pErHopList = &(CRLSP_TE_HOP_LIST
                                       (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)));
    tLdpTeHopInfo      *pHop = NULL;
    tLdpTeHopInfo      *pNextHop = NULL;

    /*   -------      ---------      */
    /*  | Hop N |--->| Hop N+1 |     */
    /*   -------      ---------      */
    /*   pHop         pNextHop       */

    pHop = (tLdpTeHopInfo *) TMO_SLL_First (pErHopList);
    while (pHop != NULL)
    {
        if (LdpIsLsrPartOfErHop (u2IncarnId, pHop, pLCB, &u1IsEgrOfHop)
            == LDP_TRUE)
        {
            if (u1FirstHopPrcsd == LDP_FALSE)
            {
                u1FirstHopPrcsd = LDP_TRUE;
            }

            /*
             * Normally when an Lsr belongs to a given Er-Hop N 
             * then, we look at how to forward to Er-Hop N+1.
             * Exception here is the case where Er-Hop is LspId.
             * For this case, we should look at the Next Er-Hop, 
             * only when the Lsr is the Egress of the
             * LspId(Er-Hop N).
             */
            if ((CRLSP_TE_ERHOP_ADDR_TYPE (pHop) ==
                 CRLSP_TE_ERHOP_LSPID_TYPE) && (u1IsEgrOfHop == LDP_FALSE))
            {
                /* Lsr is Ingress to Lsp(ErHop) 
                 * For this case, Fwding Info has already been updated 
                 * into the new Cr-LCB, in the routine LdpIsLsrPartOfErHop.
                 * if the target session is found for the given LSPID.
                 * else the failure is returned.
                 */
                if (CRLSP_TRGT_SSN_FOR_STACK (LCB_TNLINFO (pLCB)) == LDP_FALSE)
                {
                    LDP_DBG (LDP_ADVT_PRCS,
                             "ADVT: Target session is not present for"
                             "this LSPID \n");
                    *pu1PrcsStatus = LDP_STAT_TYPE_NO_ROUTE;
                    return LDP_FAILURE;
                }
                else
                {
                    *pu1PrcsStatus = LDP_STAT_TYPE_SUCCESS;
                    return LDP_SUCCESS;
                }
            }

            pNextHop = (tLdpTeHopInfo *) TMO_SLL_Next (pErHopList,
                                                       &(pHop->NextHop));

            /* Look at second Er-hop */
            if (pNextHop != NULL)
            {
                u1IsEgrOfHop = LDP_FALSE;
                if (LdpIsLsrPartOfErHop (u2IncarnId, pNextHop, pLCB,
                                         &u1IsEgrOfHop) == LDP_TRUE)
                {
                    /* Part of the Next Er-Hop.
                     * if (LSR is part of the Next Er-Hop or Abstract Node) 
                     * then Delete the previous Er-Hop or Abstract Node; 
                     * Switch to Next To Next Er-Hop and process;
                     */
                    /* The Hop is deleted from the Hop List and the Memory is
                     * released by calling TE function. */
                    ldpTeDeleteHopInfo (pTePathListInfo, pTePathInfo, pHop);
                    pHop = (tLdpTeHopInfo *) TMO_SLL_First (pErHopList);
                    continue;
                }
                else
                {
                    /* Not Part of Next Er-Hop 
                     * Check the Reachability to Next Er-Hop 
                     */
                    if (LdpGetReachabilityToErHop (u2IncarnId, pNextHop,
                                                   &u4GwAddr, &u2OutIfIndex,
                                                   &pDStrSsn) == LDP_FAILURE)
                    {
                        /* Error: Next Hop Not Reachable via MPLS Domain */
                        if (CRLSP_TE_ERHOP_TYPE (pNextHop) ==
                            CRLSP_TE_STRICT_ER)
                        {
                            *pu1PrcsStatus = LDP_STAT_TYPE_BAD_STRICT_NODE_ERR;
                        }
                        else
                        {
                            /* Loose Next Hop */
                            *pu1PrcsStatus = LDP_STAT_TYPE_BAD_LOOSE_NODE_ERR;
                        }
                        return LDP_FAILURE;
                    }
                    /* 
                     * In case of Strict pNextHop, Gateway Address MUST 
                     * belong to either of pHop or pNextHop.
                     * For Loose Next Hop, there is no such restriction.
                     */
                    if (LdpIsGwPartOfErHop (u4GwAddr, pHop) == LDP_FALSE)
                    {
                        /* 
                         * Gateway Does not belong to pHop. 
                         * Check if Gateway belongs to pNextHop.
                         */
                        if (LdpIsGwPartOfErHop (u4GwAddr, pNextHop) ==
                            LDP_FALSE)
                        {
                            if (CRLSP_TE_ERHOP_TYPE (pNextHop) ==
                                CRLSP_TE_STRICT_ER)
                            {
                                /* Strict NextErHop 
                                 * Gateway does not belong to either of 
                                 * pHop or pNextHop. And pNextHop is a 
                                 * STRICT Er-Hop.This is an Error. ReqMsg 
                                 * cannot be forwarded.Send NAK UpStream.
                                 */
                                *pu1PrcsStatus =
                                    LDP_STAT_TYPE_BAD_STRICT_NODE_ERR;
                                return LDP_FAILURE;
                            }

                            /* pNextHop is Loose. For this case, pHop 
                             * should be deleted and the Req Msg should 
                             * be forwarded towards pNextHop.
                             */
                        }

                        /*  (Gw belongs to pNextHop) or (pNextHop is Loose).
                         *   So Deleting pHop. 
                         */
                        /* The Hop is deleted from the Hop List and the memory 
                         * is released by calling TE function. */
                        ldpTeDeleteHopInfo (pTePathListInfo, pTePathInfo, pHop);
                    }

                    /* Update Cr-LCB with Forwarding Information. */
                    if (LdpUpdateCrLCBWithFwdInfo (u2IncarnId, u4GwAddr,
                                                   u2OutIfIndex,
                                                   pDStrSsn, pLCB) ==
                        CFA_FAILURE)
                    {
                        LDP_DBG (LDP_ADVT_PRCS,
                                 "ADVT: Update of LSP CtrlBlk - FWDing info FAILED.\n");
                        return LDP_FAILURE;
                    }
                    *pu1PrcsStatus = LDP_STAT_TYPE_SUCCESS;
                    return LDP_SUCCESS;
                }
            }
            else
            {
                /* Next Hop NOT present Lsr belongs to the Last Er-Hop ie 
                 * Egress. Update the Er-Hop List, by deleting the last Er-Hop.
                 * Update LCB for sending Label mapping msg UpStrm ie 
                 * make DStr =  NULL.
                 */
                LCB_DSSN (pLCB) = NULL;
                CRLSP_TNL_ROLE (pCrlspTnlInfo) = LDP_TE_EGRESS;

                /* Since the LSR is Egress, Release the HopList, Path 
                 * Option and Hops. */
                ldpTeDeleteHopListInfo (CRLSP_TNL_HOP_LIST_INFO
                                        (pCrlspTnlInfo));

                /* Resetting all the Hop related and Traffic Param 
                 * related fields. */
                CRLSP_HOP_LIST_INDEX (pCrlspTnlInfo) = 0;
                CRLSP_TNL_PATH_INFO (pCrlspTnlInfo) = NULL;
                CRLSP_TNL_HOP_LIST_INFO (pCrlspTnlInfo) = NULL;
                CRLSP_HOP_PO_INDEX (pCrlspTnlInfo) = 0;
                CRLSP_RES_INDEX (pCrlspTnlInfo) = 0;

                *pu1PrcsStatus = LDP_STAT_TYPE_SUCCESS;
                return LDP_SUCCESS;
            }
        }
        else if (CRLSP_TE_ERHOP_TYPE (pHop) == CRLSP_TE_STRICT_ER)
        {
            /* Error : Node does not belong to the Initial Strict Er-Hop. */
            if (u1FirstHopPrcsd == LDP_FALSE)
            {
                /* First Er-Hop received from UStream in Er-Hop List */
                *pu1PrcsStatus = LDP_STAT_TYPE_BAD_INIT_ER_HOP_ERR;
            }
            else
            {
                /* For all subsequent Strict Er-Hops */
                *pu1PrcsStatus = LDP_STAT_TYPE_BAD_STRICT_NODE_ERR;
            }
            return LDP_FAILURE;
        }
        else
        {
            /* Lsr Does not belong to this Loose Er-Hop Node.
             * Check the Reachability to the Er-Hop via MPLS Domain. 
             * If Er-Hop is reachable via MPLS Domain then, update the 
             * LCB with essential forwarding information.
             * Updation to LCB is done, to facilitate either of the 
             * action below :
             *   - Fwding of Req msg to DownStream.
             *   - Generation of Map Msg to UpStream.                    
             */

            if (LdpGetReachabilityToErHop (u2IncarnId, pHop, &u4GwAddr,
                                           &u2OutIfIndex, &pDStrSsn)
                == LDP_FAILURE)
            {
                *pu1PrcsStatus = LDP_STAT_TYPE_BAD_LOOSE_NODE_ERR;
                return LDP_FAILURE;
            }
            if (LdpUpdateCrLCBWithFwdInfo (u2IncarnId, u4GwAddr, u2OutIfIndex,
                                           pDStrSsn, pLCB) == CFA_FAILURE)
            {
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: Update of LSP CtrlBlk - FWDing info FAILED.\n");
                return LDP_FAILURE;
            }

            *pu1PrcsStatus = LDP_STAT_TYPE_SUCCESS;
            return LDP_SUCCESS;
        }
    }
    *pu1PrcsStatus = LDP_STAT_TYPE_SUCCESS;
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpIsGwPartOfErHop                                        */
/* Description   : Checks whether a given GwAddr belongs to the given Er-Hop */
/*                 or not.                                                   */
/*                                                                           */
/* Input(s)      : u4GwAddr - Address of the gateway                         */
/*                 pErHop   - Points to the Er-Hop Information               */
/*                                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : Returns LDP_TRUE if belongs to the Er-Hop, else returns   */
/*                 LDP_FALSE.                                                */
/*****************************************************************************/

UINT1
LdpIsGwPartOfErHop (UINT4 u4GwAddr, tLdpTeHopInfo * pErHop)
{
    UINT4               u4ErHopAddr = 0;
    UINT4               u4ErHopMask;
    UINT4               u4IngressAddr;

    switch (CRLSP_TE_ERHOP_ADDR_TYPE (pErHop))
    {
        case CRLSP_TE_ERHOP_IPV4_TYPE:

            LDP_GET_MASK_FROM_PRFX_LEN (CRLSP_TE_ERHOP_ADDR_PRFX_LEN (pErHop),
                                        u4ErHopMask);
            MEMCPY ((UINT1 *) (&(u4ErHopAddr)),
                    &CRLSP_TE_ERHOP_IPV4_ADDR (pErHop), LDP_IPV4ADR_LEN);
            u4ErHopAddr = OSIX_NTOHL (u4ErHopAddr);
            break;

        case CRLSP_TE_ERHOP_LSPID_TYPE:

            MEMCPY ((UINT1 *) (&u4IngressAddr),
                    &CRLSP_TE_ERHOP_IPV4_ADDR (pErHop), LDP_IPV4ADR_LEN);
            u4ErHopAddr = OSIX_NTOHL (u4IngressAddr);
            u4ErHopMask = LDP_HOST_NETMASK;
            break;

        default:

            LDP_DBG (LDP_ADVT_MISC, "ADVT: Er-Hop Type Not Supported\n");
            return LDP_FALSE;
    }
    return (((u4ErHopAddr & u4ErHopMask) == (u4GwAddr & u4ErHopMask)) ?
            LDP_TRUE : LDP_FALSE);
}

/*****************************************************************************/
/* Function Name : LdpIsLsrPartOfErHop                                       */
/* Description   : This routine is called to check whether the given Node    */
/*                 is part of the Er-Hop or not.                             */
/*                 If the Node is part of Er-Hop(of type LspId), and is      */
/*                 intermediate to the Lsp specified by the Er-Hop, then this*/
/*                 routine also updates the new LCB with DstrSsn, OutIfIndex */
/*                 and LsrRole related details.                              */
/*                                                                           */
/* Input(s)      : u2IncarnId - Incarnation number                           */
/*                 pHop - Er-Hop.                                            */
/*                 pLCB - Lsp Control Block of the new Cr-Lsp.               */
/*                                                                           */
/* Output(s)     : pu1IsEgrOfHop - Indicates whether the Lsr is Egress of the*/
/*                 Er-Hop in context. Set to LDP_TRUE only in the foll case, */
/*                     - Lsr is part of Er-Hop(LspId) and is Egress of the   */
/*                       the Lsp specified by LspId.                         */
/*                 NextHopAddr and DStrSsn are updated into LCB, when        */
/*                     - Lsr is part of Er-Hop(LspId) and is intermediate.   */
/*                     - Lsr is not part of Er-Hop(Ipv4Addr).                */
/*                 u1LsrRole is updated into LCB, with                       */
/*                 LDP_PASSIVE - when Lsr belongs to the Er-Hop(LspId) and   */
/*                               is intermediate node to LspId.              */
/*                                                                           */
/* Return(s)     : LDP_FALSE when NOT part of given Er-Hop.                  */
/*                 LDP_TRUE when part of given Er-Hop.                       */
/*****************************************************************************/

UINT1
LdpIsLsrPartOfErHop (UINT2 u2IncarnId, tLdpTeHopInfo * pHop,
                     tLspCtrlBlock * pLCB, UINT1 *pu1IsEgrOfHop)
{
    UINT4               u4ErHopAddr = 0;
    UINT4               u4ErHopMask = LDP_HOST_NETMASK;
    UINT4               u4IngressAddr;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;
    tLspCtrlBlock      *pCrLCB = NULL;
    UINT4               u4HIndex = 0;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT1               u1SessionFound = LDP_FALSE;

    switch (CRLSP_TE_ERHOP_ADDR_TYPE (pHop))
    {
        case CRLSP_TE_ERHOP_IPV4_TYPE:

            LDP_GET_MASK_FROM_PRFX_LEN (CRLSP_TE_ERHOP_ADDR_PRFX_LEN (pHop),
                                        u4ErHopMask);
            MEMCPY ((UINT1 *) (&(u4ErHopAddr)),
                    &CRLSP_TE_ERHOP_IPV4_ADDR (pHop), LDP_IPV4ADR_LEN);
            u4ErHopAddr = OSIX_NTOHL (u4ErHopAddr);
            if (LdpIsLsrPartOfIpv4ErHopOrFec (u4ErHopAddr, u4ErHopMask)
                == LDP_TRUE)
            {
                *pu1IsEgrOfHop = LDP_TRUE;
                return LDP_TRUE;
            }
            /* NOT Part of Er-Hop(Ipv4Addr Type) */
            break;

        case CRLSP_TE_ERHOP_LSPID_TYPE:

            MEMCPY ((UINT1 *) (&(u4IngressAddr)),
                    &CRLSP_TE_ERHOP_IPV4_ADDR (pHop), LDP_IPV4ADR_LEN);
            u4HIndex = (UINT4)LDP_COMPUTE_HASH_INDEX ((INT4)
                                               CRLSP_TE_ERHOP_LSPID (pHop));
            /* Searching in the Cr-Lsp Tunnel Table */
            TMO_HASH_Scan_Bucket (LDP_CRLSP_TNL_TABLE ((UINT4) u2IncarnId),
                                  u4HIndex, pCrlspTnlInfo, tCrlspTnlInfo *)
            {

                if (((UINT4) CRLSP_TE_ERHOP_LSPID (pHop) ==
                     CRLSP_TNL_INDEX (pCrlspTnlInfo)) &&
                    (MEMCMP ((UINT1 *) &u4IngressAddr,
                             CRLSP_INGRESS_LSRID (pCrlspTnlInfo),
                             LDP_IPV4ADR_LEN) == 0))
                {
                    /* Lsr belongs to the Er-Hop ie Lsp */
                    pCrLCB = CRLSP_LCB (pCrlspTnlInfo);
                    if (IS_LSR_EGRESS_OF_TNL (pCrLCB) == LDP_TRUE)
                    {
                        *pu1IsEgrOfHop = LDP_TRUE;
                        CRLSP_IS_EGR_OF_TUNN (LCB_TNLINFO (pLCB)) = LDP_TRUE;
                    }
                    else
                    {
                        /* Updating the Stack List of the Established 
                         * tunnel 
                         */
                        TMO_SLL_Add (CRLSP_STACK_LIST (pCrlspTnlInfo),
                                     (tTMO_SLL_NODE *) & (LCB_TNLINFO (pLCB)->
                                                          NextStackNode));
                        (LCB_TNLINFO (pLCB)->pStackTnlHead) =
                            (UINT4 *) (&(pCrlspTnlInfo->StackTnlList));

                        /* For Ingress of the Tunnel the related target session 
                         * is found and the tunnel request is forwarded on that 
                         * session, if found.
                         */

                        /* NOTE: For a tunnel request to be forwarded on an 
                         * existing tunnel a target session should have been
                         * configured between the ingress and egress of the
                         * established tunnel prior to any configuration of
                         * an Erhop as an LspId of this tunnel.
                         */
                        TMO_SLL_Scan ((tTMO_SLL *) &
                                      (LDP_ENTITY_LIST (u2IncarnId)),
                                      pLdpEntity, tLdpEntity *)
                        {
                            pLdpPeer =
                                (tLdpPeer *)
                                TMO_SLL_First (&(pLdpEntity->PeerList));
                            if (pLdpPeer == NULL)
                            {
                                continue;
                            }
                            if (MEMCMP (pLdpPeer->PeerLdpId,
                                        CRLSP_EGRESS_LSRID (pCrlspTnlInfo),
                                        LDP_NET_ADDR_LEN) == 0)
                            {
                                if (pLdpPeer->pLdpSession != NULL)
                                {
                                    LCB_DSSN (pLCB) = (tLdpSession *)
                                        pLdpPeer->pLdpSession;
                                    LCB_OUT_IFINDEX (pLCB) =
                                        LdpSessionGetIfIndex (LCB_DSSN (pLCB),LDP_ADDR_TYPE_IPV4);
                                    MEMCPY ((UINT1 *)
                                            (&LDP_IPV4_U4_ADDR(pLCB->NextHopAddr)),
                                            (UINT1 *) &pLdpPeer->NetAddr,
                                            LDP_NET_ADDR_LEN);
                                    pLCB->u2AddrType=LDP_ADDR_TYPE_IPV4;
                                    LDP_IPV4_U4_ADDR(pLCB->NextHopAddr) =
                                        OSIX_NTOHL (LDP_IPV4_U4_ADDR(pLCB->NextHopAddr));
                                    u1SessionFound = LDP_TRUE;
                                    break;
                                }
                            }
                        }

                        if (u1SessionFound == LDP_FALSE)
                        {
                            /* Target Session is not found but is part of
                             * Ingress.
                             */
                            CRLSP_TRGT_SSN_FOR_STACK (LCB_TNLINFO (pLCB)) =
                                LDP_FALSE;
                            *pu1IsEgrOfHop = LDP_FALSE;
                            return LDP_TRUE;
                        }
                        /*
                         * The LSP control block is added to the list of down 
                         * stream LSP Ctrlblock list with respect to the 
                         * downstream session.
                         */
                        TMO_SLL_Insert (&SSN_DLCB (LCB_DSSN (pLCB)), NULL,
                                        (tTMO_SLL_NODE *) & (pLCB->
                                                             NextDSLspCtrlBlk));
                        *pu1IsEgrOfHop = LDP_FALSE;
                    }
                    /* The Stack Tunnel Bit is set to true to avoid the     
                     * the reservation for Ingress Lsr's and Intermediate. 
                     */
                    CRLSP_STACK_TUNN (LCB_TNLINFO (pLCB)) = LDP_TRUE;
                    return LDP_TRUE;
                }
            }
            /* 
             * Lsr does not contain a Tnl identified by given keys - LspId &
             * IngreesRouterID. Implies, Lsr does not belong to this Er-Hop.
             */
            break;

        default:

            LDP_DBG (LDP_ADVT_MISC, "ADVT: Bad Er-Hop Type \n");
            break;
    }
    *pu1IsEgrOfHop = LDP_FALSE;
    return LDP_FALSE;
}

/*****************************************************************************/
/* Function Name : LdpGetReachabilityToErHop                                 */
/* Description   : This routine gets the reachability information for the    */
/*                 given ErHop.                                              */
/* Input(s)      : u2IncarnId - Incarnation Number                           */
/*                 pHop - The ErHop for which the reachability needs to be   */
/*                        found                                              */
/* Output(s)     : pu4GwAddr - Gateway address                               */
/*                 pu2OutIfIndex - The Interface Index                       */
/*                 ppDStrSsn - The downstream session                        */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpGetReachabilityToErHop (UINT2 u2IncarnId, tLdpTeHopInfo * pHop,
                           UINT4 *pu4GwAddr, UINT2 *pu2OutIfIndex,
                           tLdpSession ** ppDStrSsn)
{
    UINT4               u4ErHopAddr = 0;
    UINT4               u4ErHopMask;
    UINT4               u4TempNextHopAddr;
    UINT4               u4HIndex;

    tPeerIfAdrNode     *pIfAddrNode = NULL;

    switch (CRLSP_TE_ERHOP_ADDR_TYPE (pHop))
    {
        case CRLSP_TE_ERHOP_IPV4_TYPE:

            LDP_GET_MASK_FROM_PRFX_LEN (CRLSP_TE_ERHOP_ADDR_PRFX_LEN (pHop),
                                        u4ErHopMask);
            MEMCPY ((UINT1 *) (&(u4ErHopAddr)),
                    &CRLSP_TE_ERHOP_IPV4_ADDR (pHop), LDP_IPV4ADR_LEN);
            u4ErHopAddr = OSIX_NTOHL (u4ErHopAddr);
            break;

        case CRLSP_TE_ERHOP_LSPID_TYPE:

            MEMCPY ((UINT1 *) (&(u4ErHopAddr)),
                    &CRLSP_TE_ERHOP_IPV4_ADDR (pHop), LDP_IPV4ADR_LEN);
            u4ErHopAddr = OSIX_NTOHL (u4ErHopAddr);
            u4ErHopMask = LDP_HOST_NETMASK;
            break;

        default:
            LDP_DBG (LDP_ADVT_MISC, "ADVT: Bad Er-Hop Type \n");
            return LDP_FAILURE;
    }

    /* Check whether IP has a route for the ERHop */
    if (LDP_GET_ROUTE_FOR_DEST (u4ErHopAddr, u4ErHopMask, LDP_DEF_TOS,
                                pu2OutIfIndex, pu4GwAddr) == LDP_IP_FAILURE)
    {

        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: Route for ER Hop not available. LCB Not Updated.\n");
        return LDP_FAILURE;
    }
    /* The Gateway is updated to the Dest Addr when the GW is one
     * of the local addresses in our LSR.This happens when the Dest
     * Addr is a local peer host address where in we have to forward
     * the request.
     */

    if (LdpIpIsAddrLocal (*pu4GwAddr) == LDP_TRUE)
    {
        *pu4GwAddr = u4ErHopAddr;
    }
    /* 
     * Get the session that is associated with the NextHopAddr & ifIndex. 
     * The peer that has advertised the NextHopAddr in its LDP Addr msg
     * is located. If no such peer exists, then the Er-Hop is not
     * reachable. So Tnl which includes this Er-Hop cannot be established.
     * If the peer exists, the session associated with the peer is accessed
     * for further processing.
     */

    u4HIndex = LDP_COMPUTE_HASH_INDEX (*pu4GwAddr);
    u4TempNextHopAddr = OSIX_HTONL (*pu4GwAddr);

    TMO_HASH_Scan_Bucket (LDP_PEER_IFADR_TABLE (u2IncarnId), u4HIndex,
                          pIfAddrNode, tPeerIfAdrNode *)
    {
        if ((MEMCMP ((UINT1 *) (&u4TempNextHopAddr),
                     (UINT1 *) &(pIfAddrNode->IfAddr),
                     LDP_IPV4ADR_LEN) == 0) &&
            (pIfAddrNode->pSession->u1StatusRecord ==
             LDP_STAT_TYPE_LBL_RSRC_AVBL))
        {
            *ppDStrSsn = pIfAddrNode->pSession;
            break;
        }
    }
    if (*ppDStrSsn == NULL)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: Er-Hop is not reachable via an LdpSsn. \n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpUpdateCrLCBWithFwdInfo                                 */
/* Description   : This routine updates the LSP Control Block with           */
/*                 forwarding information                                    */
/* Input(s)      : u2IncarnId - Incarnation Number                           */
/*                 u4GwAddr - Gateway Address                                */
/*                 u2OutIfIndex - Out Interface Index                        */
/*                 pDStrSsn - Pointer to the downstream session              */
/* Output(s)     : pLCB - LSP Control Block. All the Inputs are copied onto  */
/*                        this control blcok.                                */
/* Return(s)     : None                                                      */
/*****************************************************************************/

INT2
LdpUpdateCrLCBWithFwdInfo (UINT2 u2IncarnId, UINT4 u4GwAddr,
                           UINT2 u2OutIfIndex, tLdpSession * pDStrSsn,
                           tLspCtrlBlock * pLCB)
{

#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
    /* Updating the fields in Tnl LCB, that is being newly established */
    LCB_DSSN (pLCB) = pDStrSsn;
    /* Create a MPLS Tunnel interface at CFA  and Stack 
     * over the MPLS l3 interface (which is stacked over 
     * an L3IPVLAN interface, and use the interface index  
     * returned as OutIfIndex
     */
    if (CfaIfmCreateStackMplsTunnelInterface
        (u2OutIfIndex, &u4MplsTnlIfIndex) == CFA_FAILURE)
    {
        return CFA_FAILURE;
    }
    LCB_OUT_IFINDEX (pLCB) = u4MplsTnlIfIndex;

    if ((LCB_TNLINFO (pLCB) != NULL) &&
        ((LCB_TNLINFO (pLCB)->pTeTnlInfo) != NULL))
    {
        (LCB_TNLINFO (pLCB))->pTeTnlInfo->u4TnlIfIndex = u4MplsTnlIfIndex;
    }

#else
    LCB_OUT_IFINDEX (pLCB) = (UINT4) u2OutIfIndex;
#endif
    /* Added to suppress warnings */
    UNUSED_PARAM (u2IncarnId);

    LDP_IPV4_U4_ADDR(pLCB->NextHopAddr) = u4GwAddr;
    pLCB->u2AddrType=LDP_ADDR_TYPE_IPV4;

    /*
     * The LSP control block is added to the list of down stream LSP
     * Ctrlblock list with respect to the downstream session.
     */
    TMO_SLL_Insert (&SSN_DLCB (pDStrSsn), NULL,
                    (tTMO_SLL_NODE *) & (pLCB->NextDSLspCtrlBlk));

    return CFA_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpCrlspResvModTrfcParms                                  */
/* Description   : This routine is called to check whether the traffic       */
/*                 parameters received in the label mapping message are      */
/*                 different from the values sent in the label request       */
/*                 message. IF any one of the parameter is different, the    */
/*                 Resource manager will be requested to reserve for the     */
/*                 modified set of parameters and release the extra resources*/
/* Input(s)      : pLspCtrlBlock, pTrfcPramsTlv                              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

UINT1
LdpCrlspResvModTrfcParms (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pTrfcParmsTlv)
{
    UINT1              *pu1TlvInfo = pTrfcParmsTlv;
    tCrlspTrfcParmsTlv  TrfcParmsTlv;
    tLdpTeTrfcParams   *pTnlTrfcParms = NULL;
    tLdpTeTrfcParams    TempTeTrfcParms;
    tLdpTeCRLDPTrfcParams TmpLdpTeCRLDPTrfcParams;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;

    pu1TlvInfo += LDP_TLV_HDR_LEN;    /* Offset done to get to field flags */

    LDP_EXT_1_BYTES (pu1TlvInfo, TrfcParmsTlv.u1Flags);
    LDP_EXT_1_BYTES (pu1TlvInfo, TrfcParmsTlv.u1Frequency);
    LDP_EXT_1_BYTES (pu1TlvInfo, TrfcParmsTlv.u1Reserved);
    LDP_EXT_1_BYTES (pu1TlvInfo, TrfcParmsTlv.u1Weight);
    LDP_EXT_4_BYTES (pu1TlvInfo, TrfcParmsTlv.u4PeakDataRate);
    LDP_EXT_4_BYTES (pu1TlvInfo, TrfcParmsTlv.u4PeakBurstSize);
    LDP_EXT_4_BYTES (pu1TlvInfo, TrfcParmsTlv.u4CommittedDataRate);
    LDP_EXT_4_BYTES (pu1TlvInfo, TrfcParmsTlv.u4CommittedBurstSize);
    LDP_EXT_4_BYTES (pu1TlvInfo, TrfcParmsTlv.u4ExcessBurstSize);

    pCrlspTnlInfo = LCB_TNLINFO (pLspCtrlBlock);
    pTnlTrfcParms = CRLSP_TRF_PARMS (pCrlspTnlInfo);

    /* Creating a temp Traffic Param structure that duplicates the current
     * Traffic Param structure.
     * Value changes will be done in this structure and TE will be called
     * to update in case of any change. */
    MEMCPY ((UINT1 *) &TempTeTrfcParms, (UINT1 *) pTnlTrfcParms,
            sizeof (tLdpTeTrfcParams));
    MEMCPY ((UINT1 *) &TmpLdpTeCRLDPTrfcParams,
            (UINT1 *) CRLSP_TE_CRLDP_TRFC_PARAMS (pTnlTrfcParms),
            sizeof (tLdpTeCRLDPTrfcParams));
    CRLSP_TE_CRLDP_TRFC_PARAMS ((&TempTeTrfcParms)) = &TmpLdpTeCRLDPTrfcParams;

    if ((CRLSP_TRF_WGT (pCrlspTnlInfo) != TrfcParmsTlv.u1Weight) ||
        (CRLSP_TRF_FREQ (pCrlspTnlInfo) != TrfcParmsTlv.u1Frequency) ||
        (CRLSP_TRF_FLAGS (pCrlspTnlInfo) != TrfcParmsTlv.u1Flags) ||
        (CRLSP_TRF_PD (pCrlspTnlInfo) != TrfcParmsTlv.u4PeakDataRate) ||
        (CRLSP_TRF_PB (pCrlspTnlInfo) != TrfcParmsTlv.u4PeakBurstSize) ||
        (CRLSP_TRF_CD (pCrlspTnlInfo) != TrfcParmsTlv.u4CommittedDataRate) ||
        (CRLSP_TRF_CB (pCrlspTnlInfo) != TrfcParmsTlv.u4CommittedBurstSize) ||
        (CRLSP_TRF_EB (pCrlspTnlInfo) != TrfcParmsTlv.u4ExcessBurstSize))
    {
        /* If the Tunnel has a modification the resources are not modified as
         * they are shared. 
         */

        if (CRLSP_INS_POIN (pLspCtrlBlock->pCrlspTnlInfo) == NULL)
        {
            /* Only the six parameters that can be modified are copied */
            CRLSP_TE_CRLDP_TPARAM_WEIGHT ((&TempTeTrfcParms)) =
                TrfcParmsTlv.u1Weight;
            CRLSP_TE_CRLDP_TPARAM_FREQ ((&TempTeTrfcParms)) =
                TrfcParmsTlv.u1Frequency;
            CRLSP_TE_CRLDP_TPARAM_FLAGS ((&TempTeTrfcParms)) =
                TrfcParmsTlv.u1Flags;
            CRLSP_TE_CRLDP_TPARAM_PDR ((&TempTeTrfcParms)) =
                TrfcParmsTlv.u4PeakDataRate;
            CRLSP_TE_CRLDP_TPARAM_PBS ((&TempTeTrfcParms)) =
                TrfcParmsTlv.u4PeakBurstSize;
            CRLSP_TE_CRLDP_TPARAM_CDR ((&TempTeTrfcParms)) =
                TrfcParmsTlv.u4CommittedDataRate;
            CRLSP_TE_CRLDP_TPARAM_CBS ((&TempTeTrfcParms)) =
                TrfcParmsTlv.u4CommittedBurstSize;
            CRLSP_TE_CRLDP_TPARAM_EBS ((&TempTeTrfcParms)) =
                TrfcParmsTlv.u4ExcessBurstSize;

            if (LdpTcModifyResources (pCrlspTnlInfo) == LDP_FAILURE)
            {
                return LDP_FAILURE;
            }

            /* TE Function call to update the modified trfc parameters 
             * in the actual trfc param structure. */

            /* NOTE : If this function is called at the Intermediate node,
             * just the value gets copied. In the case of Ingress
             * a new row is created and the tunnel is made to point
             * to this row. The tunnel count of the previous traffic param
             * row is decremented by one. */
            ldpTeUpdateTrfcParams (pTnlTrfcParms, &TempTeTrfcParms,
                                   CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgCrlspRspMap                                     */
/* Description   : This routine is called when label state machine is in     */
/*                 RESPONSE_AWAITED state and Label Mapping Message          */
/*                 is received for the FEC type being a CRLSP.               */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgCrlspRspMap (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    UINT2               u2PvTlvLen;
    uLabel              Label;
    UINT4               u4LabelVal;

    UINT1               u1HopCount = LDP_INVALID_HOP_COUNT;
    UINT2               u2MsgLen = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLen = 0;
    UINT1              *pu1PvTlv = NULL;
    UINT1              *pu1HcTlv = NULL;
    UINT1              *pu1TrfcParamTlv = NULL;
    UINT1              *pu1MsgPtr = pMsg;
    UINT1               u1LblLearnt = LDP_FALSE;
    UINT4               u4SessIfIndex = 0;
    UINT1              *pu1ElspTPTlv = NULL;
    UINT1               u1LsrEgrOrIngr = LDP_TRUE;

    /* The message length is obtained first */
    pu1MsgPtr += LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Msg pointer now points to the message id, the message id is moved */
    pu1MsgPtr += LDP_OFF_MSG_ID;

    /* Msg pointer now points to the first TLV in the message */
    while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
    {

        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        /* Check for bad TLV Length performed here */
        if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
        {
            LDP_DBG (LDP_ADVT_MISC, "ADVT: Bad Tlv Len \n");
            if (LCB_USSN (pLspCtrlBlock) != NULL)
            {
                /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  
                 * sent. */
                LDP_DBG1 (LDP_ADVT_MISC,
                          "ADVT: Notif Msg Snt - BAD TLV LEN - %x \n",
                          LDP_STAT_BAD_TLV_LEN);
                LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                      LDP_STAT_TYPE_NO_ROUTE, pMsg);
            }
            if (u1LblLearnt == LDP_TRUE)
            {
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }

        switch (u2TlvType)
        {
            case LDP_GEN_LABEL_TLV:
                if (LCB_USSN (pLspCtrlBlock) != NULL)
                {
                    if (LDP_LBL_SPACE_TYPE
                        (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock))) !=
                        LDP_PER_PLATFORM)
                    {
                        /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                           sent. */
                        LDP_DBG1 (LDP_ADVT_MISC,
                                  "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                                  LDP_STAT_UNKNOWN_TLV);
                        LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                              pLspCtrlBlock,
                                              LDP_STAT_UNKNOWN_TLV, pMsg);

                        return;
                    }
                }
                LDP_GET_4_BYTES (pu1MsgPtr, u4LabelVal);
                Label.u4GenLbl = u4LabelVal;
                MEMCPY (&LCB_DLBL (pLspCtrlBlock), &Label, sizeof (uLabel));
                u1LblLearnt = LDP_TRUE;
                break;

            case LDP_ATM_LABEL_TLV:
                if (LCB_USSN (pLspCtrlBlock) != NULL)
                {
                    if (LDP_LBL_SPACE_TYPE
                        (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock))) !=
                        LDP_PER_IFACE)
                    {
                        /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                           sent. */
                        LDP_DBG1 (LDP_ADVT_MISC,
                                  "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                                  LDP_STAT_UNKNOWN_TLV);
                        LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                              pLspCtrlBlock,
                                              LDP_STAT_UNKNOWN_TLV, pMsg);
                        return;
                    }
                }
                LDP_GET_4_BYTES (pu1MsgPtr, u4LabelVal);
                Label.AtmLbl.u2Vpi =
                    (UINT2) ((u4LabelVal & MPLS_VPI_MASK) >> 16);
                if ((Label.AtmLbl.u2Vpi & MPLS_V_BITS_MASK) != LDP_ATM_V_BITS)
                {
                    /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                       sent. */
                    LDP_DBG1 (LDP_ADVT_MISC,
                              "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                              LDP_STAT_UNKNOWN_TLV);
                    if (LCB_USSN (pLspCtrlBlock) != NULL)
                    {
                        LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                              pLspCtrlBlock,
                                              LDP_STAT_UNKNOWN_TLV, pMsg);
                    }
                    return;

                }
                Label.AtmLbl.u2Vci = (UINT2) (u4LabelVal & MPLS_VCI_MASK);
                MEMCPY (&LCB_DLBL (pLspCtrlBlock), &Label, sizeof (uLabel));
                u1LblLearnt = LDP_TRUE;
                break;

            case LDP_FEC_TLV:
            case LDP_LBL_REQ_MSGID_TLV:
                /* 
                 * FEC values is not required to be extracted as it is available
                 * in the LSPCtrlBlock. The Label Request message id is not 
                 * required, as the required LSPCtrlBlock has already been
                 * accessed using this value in funciton "LdpHandleLblMapMsg"
                 */
                break;

            case LDP_HOP_COUNT_TLV:
                pu1HcTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case LDP_PATH_VECTOR_TLV:
                pu1PvTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case CRLDP_LSPID_TLV:
                break;
            case CRLDP_TRAFPARM_TLV:
                pu1TrfcParamTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                /* Check done for the correctness of the PDR and 
                 * CDR Value. If found erroneous notificaiton message sent 
                 * with the notification type set to Traffic parameters
                 * unavilable - 0x44000006.
                 */
                {
                    UINT1              *pu1TmpTrfcParmTlv = NULL;
                    UINT4               u4Pdr;
                    UINT4               u4Cdr;
                    pu1TmpTrfcParmTlv = pu1TrfcParamTlv;
                    LDP_GET_4_BYTES (pu1TmpTrfcParmTlv +
                                     LDP_CRLSP_PDR_OFFSET_VAL, u4Pdr);
                    LDP_GET_4_BYTES (pu1TmpTrfcParmTlv +
                                     LDP_CRLSP_CDR_OFFSET_VAL, u4Cdr);
                    if (u4Cdr > u4Pdr)
                    {
                        LDP_DBG (LDP_ADVT_MISC,
                                 "ADVT: Trfc Prams Err CDR > PDR \n");
                        if (LCB_USSN (pLspCtrlBlock) != NULL)
                        {
                            LDP_DBG1 (LDP_ADVT_MISC,
                                      "ADVT: Notif Msg Snt - Trfc Prms "
                                      "Unavl - %x \n",
                                      LDP_STAT_TRAF_PARMS_UNAVL);
                            LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                                  pLspCtrlBlock,
                                                  LDP_STAT_TYPE_TRAF_PARMS_UNAVL,
                                                  pMsg);
                        }
                        if (u1LblLearnt == LDP_TRUE)
                        {
                            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                              &LCB_DLBL (pLspCtrlBlock),
                                              LCB_DSSN (pLspCtrlBlock));
                        }
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                        return;
                    }
                }
                break;

            case CRLDP_DIFFSERV_ELSPTP_TLV:
                pu1ElspTPTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            default:
                /* Check for bad TLV Type performed here */
                if (u2TlvType < LDP_MIN_TLV_VAL)
                {
                    if (u1LblLearnt == LDP_TRUE)
                    {
                        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                          &LCB_DLBL (pLspCtrlBlock),
                                          LCB_DSSN (pLspCtrlBlock));
                    }
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    return;
                }
                break;
        }
        pu1MsgPtr += u2TlvLen;
    }

    /* 
     * In case of label mapping message received, and LSPID optional 
     * TLV present in the Label map message contains wrong LSPID value
     * error notification to be generated. - This condition is not mentioned 
     * explicitly in the CRLSP draft. Can be handled in the future.
     */

    /* Pseudo code logic:
     * If (FEC type associated with the LSP ctrl block == CRLSP type ) {
     *    if ( LSPID value present in the tunnel info structure associated
     *         with the CRLSP block is different from the LSPID value
     *         obtained in the label mapping message ) {
     *       Erroneous condition. NAK to be transmitted upstream and 
     *       downstream as required.
     *    }
     * }
     */

    /* 
     * In case of label mapping message received with Trfc optional 
     * TLV present in the Label map message, The Trfc parameters were not 
     * negotiable values, however the label map message contains different/
     * negotiated vlaues, error notification to be generated. - This condition
     * is not mentioned explicitly in the CRLSP draft. Can be handled in the 
     * future.
     */

    /* Pseudo code logic:
     * If (FEC type associated with the LSP ctrl block == CRLSP type ) {
     *    if ( Traffic parameters are initially marked as not negotiable
     *         and (traffic parmaeter values sent in the label request 
     *         message downstream is different from the values received in the
     *         label mapping message from the downstream ) {
     *       Erroneous condition. NAK to be transmitted upstream and 
     *       downstream as required.
     *    }
     * }
     */

    if (LCB_TRIG (pLspCtrlBlock)
        && (LCB_TRIG_TYPE (pLspCtrlBlock) == NEXT_HOP_CHNG_LSP_SET_UP))
    {
        gLdpNonMrgNhopChngFsm[NH_TRGCB_ST (LCB_TRIG
                                           (pLspCtrlBlock))]
            [LDP_NH_LSM_EVT_INT_LSP_UP] (LCB_TRIG (pLspCtrlBlock), NULL);
        if (pu1TrfcParamTlv != NULL)
        {
            if (LdpCrlspResvModTrfcParms (pLspCtrlBlock, pu1TrfcParamTlv)
                == LDP_FAILURE)
            {
                /* Case of LSR being Ingress to the LSP */
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return;
            }
        }

        if (((CRLSP_TNL_ROLE (LCB_TNLINFO (pLspCtrlBlock))) != LDP_TE_INGRESS)
            && (pu1ElspTPTlv != NULL))
        {
            if (LdpDiffServHandleModElspTrfcProfile (pLspCtrlBlock,
                                                     pMsg,
                                                     pu1ElspTPTlv) ==
                LDP_FAILURE)
            {
                /* Case of LSR being Ingress to the LSP */
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return;
            }
        }
        return;
    }

    u1LsrEgrOrIngr = ((pLspCtrlBlock->pUStrSession) == NULL)
        ? LDP_TRUE : LDP_FALSE;
    if (LdpIsLoopFound (pu1HcTlv, pu1PvTlv, (&u2PvTlvLen),
                        (&u1HopCount),
                        SSN_GET_INCRN_ID (LCB_DSSN (pLspCtrlBlock)),
                        LCB_DSSN (pLspCtrlBlock), u1LsrEgrOrIngr) == LDP_TRUE)
    {
        /* NAK message to be sent downstream */
        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            /* Label release message has to be sent with Status TLV (Loop detected status
             * code)
             * */
            pLspCtrlBlock->pDStrSession->u1StatusCode =
                LDP_STAT_TYPE_LOOP_DETECTED;
            /* Case of LSR being Ingress to the LSP */
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }
        else
        {
            /* 
             * Case of LSR being in between the established LSP. 
             * In case of CRLSP the label assignment is done only in the 
             * ORDERED MODE.
             */

            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));

            LDP_DBG1 (LDP_ADVT_MISC, "ADVT: Notif Msg Snt - No Route - %x \n",
                      LDP_STAT_NO_ROUTE);
            if (LCB_USSN (pLspCtrlBlock) != NULL)
            {
                LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                      LDP_STAT_TYPE_NO_ROUTE, pMsg);
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return;
        }
    }
    else
    {
        /* No Loop detected in the received label mapping message */
        /* Updating the HopCount rx in the Map Msg */
        LCB_HCOUNT (pLspCtrlBlock) = u1HopCount;

        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            /* Case of LSR being the Ingress of the LSP */
            LdpSendMplsMlibUpdate (MPLS_MLIB_TNL_CREATE, MPLS_OPR_PUSH,
                                   pLspCtrlBlock, NULL);
            CRLSP_OPER_STATUS (LCB_TNLINFO (pLspCtrlBlock)) = LDP_OPER_UP;
            (CRLSP_TE_TNL_INFO
             (LCB_TNLINFO (pLspCtrlBlock)))->u1CPOrMgmtOperStatus = LDP_OPER_UP;
            if (pu1TrfcParamTlv != NULL)
            {
                if (LdpCrlspResvModTrfcParms (pLspCtrlBlock, pu1TrfcParamTlv)
                    == LDP_FAILURE)
                {
                    LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                      &LCB_DLBL (pLspCtrlBlock),
                                      LCB_DSSN (pLspCtrlBlock));
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    return;
                }
            }

            if (((CRLSP_TNL_ROLE (LCB_TNLINFO (pLspCtrlBlock)))
                 != LDP_TE_INGRESS) && (pu1ElspTPTlv != NULL))
            {
                if (LdpDiffServHandleModElspTrfcProfile (pLspCtrlBlock,
                                                         pMsg,
                                                         pu1ElspTPTlv) ==
                    LDP_FAILURE)
                {
                    LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                      &LCB_DLBL (pLspCtrlBlock),
                                      LCB_DSSN (pLspCtrlBlock));
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    return;
                }
            }
            LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;

            return;
        }
        else
        {
            /* The label allocation for the CRLSP will be ORDERED MODE */
            if (LCB_USSN (pLspCtrlBlock) == NULL)
            {
                return;
            }
            u4SessIfIndex = LdpSessionGetIfIndex(LCB_USSN (pLspCtrlBlock),pLspCtrlBlock->Fec.u2AddrFmly);
            if (LdpGetLabel (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock)), &Label,
                             u4SessIfIndex) == LDP_FAILURE)
            {
                LDP_DBG (LDP_ADVT_PRCS, "ADVT: Label allocation failed !\n");
                LDP_DBG1 (LDP_ADVT_MISC,
                          "ADVT: Notif Msg Snt - No Lbl Rsrc - %x \n",
                          LDP_STAT_NO_LBL_RSRC);
                LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                      LDP_STAT_TYPE_NO_LBL_RSRC, pMsg);
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return;
            }
            else
            {
                MEMCPY (&LCB_ULBL (pLspCtrlBlock), &Label, sizeof (uLabel));

                LdpSendMplsMlibUpdate
                    (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP_PUSH,
                     pLspCtrlBlock, NULL);

                CRLSP_OPER_STATUS (LCB_TNLINFO (pLspCtrlBlock)) = LDP_OPER_UP;
                (CRLSP_TE_TNL_INFO
                 (LCB_TNLINFO (pLspCtrlBlock)))->u1CPOrMgmtOperStatus =
  LDP_OPER_UP;

                /* MLIB update being success */
                LdpSendCrlspLblMappingMsg (pLspCtrlBlock);
            }
            if ((pu1TrfcParamTlv != NULL))
            {
                if (LdpCrlspResvModTrfcParms (pLspCtrlBlock,
                                              pu1TrfcParamTlv) == LDP_FAILURE)
                {
                    LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                          pLspCtrlBlock,
                                          LDP_STAT_TYPE_NO_LBL_RSRC, pMsg);
                    LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                      &LCB_DLBL (pLspCtrlBlock),
                                      LCB_DSSN (pLspCtrlBlock));
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    return;
                }
            }

            if (((CRLSP_TNL_ROLE (LCB_TNLINFO (pLspCtrlBlock)))
                 != LDP_TE_INGRESS) && (pu1ElspTPTlv != NULL))
            {
                if (LdpDiffServHandleModElspTrfcProfile (pLspCtrlBlock,
                                                         pMsg,
                                                         pu1ElspTPTlv) ==
                    LDP_FAILURE)
                {
                    LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                          pLspCtrlBlock,
                                          LDP_STAT_TYPE_NO_LBL_RSRC, pMsg);
                    LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                      &LCB_DLBL (pLspCtrlBlock),
                                      LCB_DSSN (pLspCtrlBlock));
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    return;
                }
            }

            /* For Intermediate LSR the unique cross connect Index
             * is obtained.This is also used to map the insegment
             * and the out segment in the LSR MIB.
             */
            LdpGetCrossConnectIndex ((UINT4 *)
                                     &pLspCtrlBlock->u4CrossConnectIndex);
            LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;
            return;
        }
    }
}

/*****************************************************************************/
/* Function Name : LdpDeleteCrlspTnlInfo                                     */
/* Description   : This routine deletes a CRLSP tunnel related information   */
/*                 that is associated with a LSP Control block.              */
/* Input(s)      : pCrlspTnlInfo                                             */
/*                 u2IncarnId                                                */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDeleteCrlspTnlInfo (UINT2 u2IncarnId, tCrlspTnlInfo * pCrlspTnlInfo)
{
    UINT4               u4HIndex;
    tTMO_HASH_TABLE    *pCrlspTnlHashtbl = NULL;
    tMplsDiffServTnlInfo *pDiffServParams = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    /* If Stack list of this tunnel is non zero,the tunnels which are 
     * being stacked by this tunnel are preempted. 
     */

    if ((TMO_SLL_Count (CRLSP_STACK_LIST (pCrlspTnlInfo))) != LDP_ZERO)
    {
        LdpPreEmptStackTunnels (CRLSP_STACK_LIST (pCrlspTnlInfo));
    }

    /* If this tunnel is stacked to another tunnel then it should be deleted
     * from the stacked list of the earlier tunnel.
     */

    if (pCrlspTnlInfo->pStackTnlHead != NULL)
    {
        TMO_SLL_Delete ((tTMO_SLL *) (VOID *) (pCrlspTnlInfo->pStackTnlHead),
                        (tTMO_SLL_NODE *) (VOID
                                           *) (&(pCrlspTnlInfo->
                                                 NextStackNode)));
    }

    /* Resources reserved are being released.If this tunnel is sharing
     * the resources with it's modification request, only the extra
     * resources are released ,which will be taken care at the Resource
     * Manager. 
     */

    if (CRLSP_TRFPARM_GRPID (pCrlspTnlInfo) != LDP_INVALID_RSRC_GRPID)
    {
        /* PORTING CHANGE FOR TC-API's  - Start */
        LdpTcFreeResources (pCrlspTnlInfo);
        /* PORTING CHANGE FOR TC-API's  - End */
        LdpCrlspRemTnlFromPrioList (LCB_OUT_IFINDEX (CRLSP_LCB (pCrlspTnlInfo)),
                                    CRLSP_HOLD_PRIO (pCrlspTnlInfo),
                                    pCrlspTnlInfo);
    }

    if (CRLSP_STACK_TUNN (pCrlspTnlInfo) == LDP_FALSE)
    {
        pDiffServParams = LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);

        if (pDiffServParams != NULL)
        {
            if ((CRLSP_TNL_ROLE (pCrlspTnlInfo) != LDP_TE_EGRESS)
                && (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo) ==
                    LDP_DS_PEROABASED_RESOURCES)
                && (((LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServParams)
                      == LDP_DS_ELSP) ||
                     (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServParams)
                      == LDP_DS_LLSP))))
            {

                if (((CRLSP_PARM_FLAG (pCrlspTnlInfo) & LDP_RSRC_PEROA_CRLSP)
                     == LDP_RSRC_PEROA_CRLSP)
                    ||
                    (CRLSP_TE_TNL_TRFC_PARAM (CRLSP_TE_TNL_INFO
                                              (pCrlspTnlInfo))) != NULL)
                {
                    LdpDiffServTcFreeResources (pCrlspTnlInfo);
                    LdpDiffServRemTnlFromPrioList (LCB_OUT_IFINDEX
                                                   (CRLSP_LCB (pCrlspTnlInfo)),
                                                   CRLSP_HOLD_PRIO
                                                   (pCrlspTnlInfo),
                                                   pCrlspTnlInfo);

                }

            }
            else if ((CRLSP_TNL_ROLE (pCrlspTnlInfo) != LDP_TE_EGRESS) &&
                     (RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo) ==
                      LDP_DS_CLASSTYPE_RESOURCES)
                     && (((LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServParams) ==
                           LDP_DS_ELSP) ||
                          (LDP_TE_DS_LSP_SERVICE_TYPE (pDiffServParams) ==
                           LDP_DS_LLSP))))

            {
                LdpDSRelResourcesInClassType (pCrlspTnlInfo);

            }
        }
    }
    if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL))
    {
        CRLSP_INS_POIN (CRLSP_INS_POIN (pCrlspTnlInfo)) = NULL;
    }

    pTeTnlInfo = CRLSP_TE_TNL_INFO (pCrlspTnlInfo);

    LdpDiffServDeleteMem (pCrlspTnlInfo, u2IncarnId);

    /* Delete the Tunnel Info from Tunnel Info Table */
    pCrlspTnlHashtbl = LDP_CRLSP_TNL_TBL (u2IncarnId);
    u4HIndex = LDP_COMPUTE_HASH_INDEX ((UINT4) CRLSP_TNL_INDEX (pCrlspTnlInfo));
    TMO_HASH_Delete_Node (pCrlspTnlHashtbl, (tTMO_HASH_NODE *)
                          (&(pCrlspTnlInfo->HashLinkNode)), u4HIndex);

    /* Release of tunnel to the tunnel memory pool */
    MemReleaseMemBlock (LDP_CRLSPTNL_POOL_ID, (UINT1 *) pCrlspTnlInfo);

    /* Deleting the Te Tunnel Info by calling the TE routine.
     * The Te Tunnel Info is deleted from its Hash List and the
     * Memory is released to the Pool.
     * This requires the established tunnel to be brought down and 
     * the local info to be deleted but the TE tunnel info be retained 
     * in the TE module. */

    if (pTeTnlInfo->u1TnlRelStatus == TE_SIGMOD_TNLREL_AWAITED)
    {
        ldpTeDeleteTnlInfo (pTeTnlInfo, TE_TNL_CALL_FROM_ADMIN);
    }
    else
    {
        ldpTeDeleteTnlInfo (pTeTnlInfo, TE_TNL_CALL_FROM_SIG);
    }
}

/*****************************************************************************/
/* Function Name : LdpPreEmptTunnels                                         */
/* Description   : This routine preempts the tunnels present in the list     */
/*                 pCandidatePreemp                                          */
/* Input(s)      : pCandidatePreemp                                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpPreEmptTunnels (tTMO_SLL * pCandidatePreemp)
{
    tTMO_SLL_NODE      *pCandidateNode = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;
    UINT1               u1Event;
    UINT1               u1StatusType = LDP_STAT_TYPE_CRLSP_PREEMPTED;

    /* Scan through the list of preemption tunnels and if any tunnel
     * is found to be a stacked tunnel,LdpPreEmptStackTunnels is called
     * which takes care of deleting all the stacked tunnels,which might
     * be in turn stacked. 
     */

    pCandidateNode = TMO_SLL_First (pCandidatePreemp);
    while (pCandidateNode != NULL)
    {
        pCrlspTnlInfo = (tCrlspTnlInfo *) (VOID *) ((UINT1 *) pCandidateNode -
                                                    VAR_OFFSET (tCrlspTnlInfo,
                                                                NextPreemptTnl));
        /* If the tunnel has a modification and is on the same IfIndex it will 
         * be also preempted.
         */
        if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
        {
            if (LCB_OUT_IFINDEX (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo))) ==
                LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo))))
            {
                u1Event = LDP_LSM_EVT_PREMPTED;
                LdpTcFreeResources (CRLSP_INS_POIN (pCrlspTnlInfo));
                LdpCrlspRemTnlFromPrioList (LCB_OUT_IFINDEX
                                            (CRLSP_LCB
                                             (CRLSP_INS_POIN (pCrlspTnlInfo))),
                                            CRLSP_HOLD_PRIO (CRLSP_INS_POIN
                                                             (pCrlspTnlInfo)),
                                            CRLSP_INS_POIN (pCrlspTnlInfo));
                LDP_NON_MRG_FSM[LCB_STATE
                                (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo)))]
                    [u1Event] (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo)),
                               &u1StatusType);
            }
            CRLSP_INS_POIN (pCrlspTnlInfo) = NULL;
        }
        while ((TMO_SLL_Count (CRLSP_STACK_LIST (pCrlspTnlInfo)) != LDP_ZERO))
        {
            LdpPreEmptStackTunnels (CRLSP_STACK_LIST (pCrlspTnlInfo));
        }
        u1Event = LDP_LSM_EVT_PREMPTED;
        LdpTcFreeResources (pCrlspTnlInfo);
        LdpCrlspRemTnlFromPrioList (LCB_OUT_IFINDEX (CRLSP_LCB (pCrlspTnlInfo)),
                                    CRLSP_HOLD_PRIO (pCrlspTnlInfo),
                                    pCrlspTnlInfo);
        LDP_NON_MRG_FSM[LCB_STATE (CRLSP_LCB (pCrlspTnlInfo))][u1Event]
            (CRLSP_LCB (pCrlspTnlInfo), &u1StatusType);
        TMO_SLL_Delete (pCandidatePreemp, pCandidateNode);
        pCandidateNode = TMO_SLL_First (pCandidatePreemp);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpPreEmptStackTunnels                                    */
/* Description   : This routine preempts the stacked tunnels in the list     */
/*                 pStackedList                                              */
/* Input(s)      : pStackedList                                              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpPreEmptStackTunnels (tTMO_SLL * pStackedList)
{
    tTMO_SLL_NODE      *pStackCandidateNode = NULL;
    tCrlspTnlInfo      *pStackCrlspTnlInfo = NULL;
    UINT1               u1Event;
    UINT1               u1StatusType = LDP_STAT_TYPE_CRLSP_PREEMPTED;

    /* Here the stack List is scanned to preempt the tunnels, but if 
     * any tunnel is found inturn to be stacked it is bumped only after the
     * stacked list of it's own is preempted.
     * This is achieved by calling recursively the function and deleting 
     * the tunnels.(Recursion stack is bound to be limited as the No of 
     * Tunnels supported by any LSR is configured).
     */

    pStackCandidateNode = TMO_SLL_First (pStackedList);
    while (pStackCandidateNode != NULL)
    {
        pStackCrlspTnlInfo =
            (tCrlspTnlInfo *) (VOID *) ((UINT1 *) pStackCandidateNode -
                                        VAR_OFFSET (tCrlspTnlInfo,
                                                    NextStackNode));
        while ((TMO_SLL_Count (CRLSP_STACK_LIST (pStackCrlspTnlInfo))) !=
               LDP_ZERO)
        {
            LdpPreEmptStackTunnels (CRLSP_STACK_LIST (pStackCrlspTnlInfo));
        }
        u1Event = LDP_LSM_EVT_PREMPTED;
        TMO_SLL_Delete (pStackedList, pStackCandidateNode);
        LDP_NON_MRG_FSM[LCB_STATE (CRLSP_LCB (pStackCrlspTnlInfo))][u1Event]
            (CRLSP_LCB (pStackCrlspTnlInfo), &u1StatusType);

        pStackCandidateNode = TMO_SLL_First (pStackedList);
    }
}

/*****************************************************************************/
/* Function Name : LdpGetTnlIncarnNode                                       */
/* Description   : This routine checks whether a tTnlIncarnNode is present   */
/*                 in the given incarn with the given tunnel index. If       */
/*                 present a pointer to the node                             */
/*                 is returned.                                              */
/* Input(s)      : u2IncarnId       - Incarn Id.                             */
/*                 u4CrlspTnlIndex  - Tunnel Index.                          */
/* Output(s)     : ppTnlIncarnNode  - Double Pointer to TnlIncarnNode.       */
/* Return(s)     : LDP_SUCCESS  - When the TnlIncarn Node is found.          */
/*                 LDP_FAILURE  - When the TnlIncarn Node is not found.      */
/*****************************************************************************/
UINT1
LdpGetTnlIncarnNode (UINT2 u2IncarnId,
                     UINT4 u4CrlspTnlIndex, tTnlIncarnNode ** ppTnlIncarnNode)
{
    tTMO_SLL           *pList = NULL;
    tTnlIncarnNode     *pTmpTnlIncarnNode = NULL;

    if (LDP_INITIALISED != TRUE)
    {
        return LDP_FAILURE;
    }
    /* pList points to the Tnl Incarn List of the given Incarn. */
    pList = &LDP_TNL_INCARN_LIST (u2IncarnId);

    /* Scanning the List to find the matching tunnel Index. */
    TMO_SLL_Scan (pList, pTmpTnlIncarnNode, tTnlIncarnNode *)
    {
        if (LDP_TNL_INCARN_TNL_INDEX (pTmpTnlIncarnNode) == u4CrlspTnlIndex)
        {
            *ppTnlIncarnNode = pTmpTnlIncarnNode;
            return LDP_SUCCESS;
        }
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetIncarnFromTnlIndex                                  */
/* Description   : This routine returns the Incarn associated with the       */
/*                 given Tunnel Index.                                       */
/* Input(s)      : i4TnlIndex - Tunnel Index.                                */
/* Output(s)     : pu4IncarnId - pointer to the Incarn.                      */
/* Return(s)     : NONE.                                                     */
/*****************************************************************************/
VOID
LdpGetIncarnFromTnlIndex (INT4 i4TnlIndex, UINT4 *pu4IncarnId)
{
    UINT1               u1TnlIncarnNodeFound = LDP_FALSE;
    tTMO_SLL           *pList = NULL;
    tTnlIncarnNode     *pTnlIncarnNode = NULL;

    if (LDP_INITIALISED != TRUE)
    {
        return;
    }

    /* Scanning through Tunnel Incarn List of all the Incarn, to find out the
     * Incarn for the given tunnel Index. */
    for (*pu4IncarnId = 1; *pu4IncarnId <= LDP_MAX_INCARN; (*pu4IncarnId)++)
    {
        pList = &LDP_TNL_INCARN_LIST (*pu4IncarnId - 1);
        TMO_SLL_Scan (pList, pTnlIncarnNode, tTnlIncarnNode *)
        {
            if (LDP_TNL_INCARN_TNL_INDEX (pTnlIncarnNode) == (UINT4) i4TnlIndex)
            {
                u1TnlIncarnNodeFound = LDP_TRUE;
                break;
            }
        }
        if (u1TnlIncarnNodeFound != LDP_FALSE)
        {
            break;
        }
    }

    if (u1TnlIncarnNodeFound == LDP_FALSE)
    {
        /* When the Tunnel Incarn node is not found for the tunnel index, 
         * current incarn is assigned. 
         */
        *pu4IncarnId = LDP_CUR_INCARN + 1;
    }
}

/*****************************************************************************/
/* Function Name : LdpSetupCrlspTunnel                                       */
/* Description   : This routine is called from the TE module when a tunnel   */
/*                 configured with signalling protocol as CRLDP is to be     */
/*                 triggered.                                                */
/*                 The function allocates memory for tCrlspTnlInfo and       */
/*                 links the tTeTnlInfo passed as a parameter with it.       */
/*                 A Tunnel up event is sent to the SNMP event handler which */
/*                 in turn triggers the establishment of the tunnel.         */
/* Input(s)      : pTeTnlInfo - Pointer to the Te Tunnel info.               */
/* Output(s)     : NONE.                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE.                                  */
/*****************************************************************************/
UINT1
LdpSetupCrlspTunnel (tLdpTeTnlInfo * pTeTnlInfo)
{
    UINT1               u1TnlModify = LDP_FALSE;
    UINT4               u4IncarnId = 0;
    UINT4               u4HIndex;
    INT4                i4TnlIndex;
    INT4                i4TnlInstance;
    UINT4               u4IngressId;

    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;
    tCrlspTnlInfo      *pCrlspInstance = NULL;
    tMplsDiffServTnlInfo *pDiffServParams = NULL;

    pDiffServParams = LDP_TE_MPLS_DIFFSERV_TNL_INFO (pTeTnlInfo);

    u4HIndex = LDP_COMPUTE_HASH_INDEX (CRLSP_TE_TNL_INDEX (pTeTnlInfo));
    i4TnlIndex = (INT4) CRLSP_TE_TNL_INDEX (pTeTnlInfo);
    i4TnlInstance = (INT4)CRLSP_TE_TNL_INSTANCE (pTeTnlInfo);
    CONVERT_TO_INTEGER (CRLSP_TE_TNL_INGRESS_RTR_ID (pTeTnlInfo), u4IngressId);

    /* Getting the Incarn from the tunnel Index. */
    LdpGetIncarnFromTnlIndex (i4TnlIndex, &u4IncarnId);

    if (LDP_CUR_INCARN == u4IncarnId)
    {
        return LDP_FAILURE;
    }

    /* NOTE : When the Tunnel Incarn node is not found for the tunnel index, 
     * current incarn is assigned. 
     * The tunnel index and the incarn should be added to the Tunnel 
     * Incarn List to avoid configuration of the same tunnel index with
     * another incarn. Since only one Incarn is supported this addition to
     * the list is not done. This should be taken care when multiple
     * incarnation support is provided. 
     * The Tunnel Incarn node is significant only at the Ingress. At the
     * Intermediate or Egress, the Incarn can be obtained from the Session
     * over which the request message is received and the tunnel is
     * associated with that Incarn.
     */

    /* Searching in the Cr-Lsp Tunnel Table for an Entry
     * with the given Index and Instance.
     *
     * If the Index alone is matched the check is done 
     * whether the tunnel is established or not.
     */

    TMO_HASH_Scan_Bucket (LDP_CRLSP_TNL_TABLE (u4IncarnId - 1),
                          u4HIndex, pCrlspTnlInfo, tCrlspTnlInfo *)
    {
        if (((UINT4) i4TnlIndex == CRLSP_TNL_INDEX (pCrlspTnlInfo)) &&
            (MEMCMP ((UINT1 *) &u4IngressId,
                     CRLSP_INGRESS_LSRID (pCrlspTnlInfo),
                     LDP_IPV4ADR_LEN) == 0))
        {
            if (CRLSP_TNL_INSTANCE (pCrlspTnlInfo) == (UINT1) i4TnlInstance)
            {
                LDP_DBG (LDP_IF_SNMP,
                         "CrLspTunnel Entry, already present for "
                         "the given Index and the Instance\n");
                return LDP_FAILURE;
            }
            else if ((LCB_STATE (CRLSP_LCB (pCrlspTnlInfo))) != LDP_LSM_ST_EST)
            {
                LDP_DBG (LDP_IF_SNMP,
                         "CrLspTunnel Entry present with the same Index is "
                         "not yet established and hence it can't be modified\n");
                return LDP_FAILURE;
            }
            else if ((CRLSP_INS_POIN (pCrlspTnlInfo)) != NULL)
            {
                LDP_DBG (LDP_IF_SNMP,
                         "Two CrLspTuunel Entry already present for the "
                         "given Index\n");
                return LDP_FAILURE;
            }
            else
            {
                /* Tunnel with the same index and Ingress Id is found. 
                 * The Tunnel being established is treated as a  
                 * modification to the already existing tunnel.
                 */
                u1TnlModify = LDP_TRUE;
                pCrlspInstance = pCrlspTnlInfo;
                break;
            }
        }
    }

    /* If Modification, then the modified tunnel info should differ 
     * the actual tunnel info in anyone of the four parameter */
    if (u1TnlModify == LDP_TRUE)
    {
        if ((CRLSP_TE_TNL_SET_PRIO (pTeTnlInfo) ==
             CRLSP_SET_PRIO (pCrlspInstance))
            && (CRLSP_TE_TNL_HOLD_PRIO (pTeTnlInfo) ==
                CRLSP_HOLD_PRIO (pCrlspTnlInfo))
            && (CRLSP_TE_TNL_HOP_LIST_INDEX (pTeTnlInfo) ==
                CRLSP_HOP_LIST_INDEX (pCrlspInstance))
            && (CRLSP_TE_TNL_TPARAM_INDEX (pTeTnlInfo) ==
                CRLSP_TRF_RESINDEX (pCrlspInstance)))
        {
            LDP_DBG (LDP_IF_SNMP,
                     "ADVT: No change in Parameters of the modifying tunnel\n");
            return LDP_FAILURE;
        }
    }

    if (LdpCreateCrlspTnl
        ((UINT2) (u4IncarnId - 1), (UINT4) i4TnlIndex,
         &pCrlspTnlInfo) == LDP_FAILURE)
    {
        LDP_DBG (LDP_IF_SNMP, "CrLspTunnel Entry Creation has failed\n");
        return LDP_FAILURE;
    }

    CRLSP_TE_TNL_INFO (pCrlspTnlInfo) = pTeTnlInfo;

    /* Mappping the Route Pinning Flag from the Session Attribute configured in
     * the TE module */

    CRLSP_RTPIN_TYPE (pCrlspTnlInfo) =
        (CRLSP_TE_TNL_SSN_ATTR (pTeTnlInfo) & CRLSP_TE_SSN_IS_PINNED_BIT)
        ? LDP_TRUE : LDP_FALSE;

    /* Mapping the Resource Affinity value to the Resource Class Color, here
     * since this indicates which links are acceptable the Include any affinity
     * are copied from TE */

    if (CRLSP_TE_TNL_INC_ANY_AFFINITY (pTeTnlInfo) != LDP_ZERO)
    {
        CRLSP_PARM_FLAG (pCrlspTnlInfo) =
            CRLSP_PARM_FLAG (pCrlspTnlInfo) | LDP_RSRC_CRLSP;
    }

    /* Mapping is done to indicate which are the diffServ TLVs present */

    if (pDiffServParams != NULL)
    {
        CRLSP_PARM_FLAG (pCrlspTnlInfo) =
            CRLSP_PARM_FLAG (pCrlspTnlInfo) |
            LDP_TE_DS_PARAM_FLAG (pDiffServParams);

    }

    if (u1TnlModify == LDP_TRUE)
    {
        CRLSP_IS_MOD_OF_TUNN (pCrlspTnlInfo) = LDP_TRUE;
        /* Creating cross-pointer among the two tunnel infos */
        CRLSP_INS_POIN (pCrlspTnlInfo) = pCrlspInstance;
        CRLSP_INS_POIN (pCrlspInstance) = pCrlspTnlInfo;
    }

    /* Sending an internal event to the LDP task, to trigger off
     * the tunnel establishment. */
    LdpSNMPEventHandler (CRLDP_TNL_UP_EVENT, pCrlspTnlInfo,
                         (UINT2) (u4IncarnId - 1));
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDestroyCrlspTunnel                                     */
/* Description   : This routine is called from the TE module when a tunnel   */
/*                 configured with signalling protocol as CRLDP is to be     */
/*                 destroyed.                                                */
/*                 A Tunnel down event is sent to the SNMP event handler     */
/*                 which in turn triggers the deletion of the tunnel.        */
/* Input(s)      : pTeTnlInfo - Pointer to the Te Tunnel info.               */
/* Output(s)     : NONE.                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpDestroyCrlspTunnel (tLdpTeTnlInfo * pTeTnlInfo)
{
    UINT4               u4IncarnId = 0;
    INT4                i4TnlIndex;
    INT4                i4TnlInstance;
    UINT4               u4IngressId;

    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;

    i4TnlIndex = (INT4)CRLSP_TE_TNL_INDEX (pTeTnlInfo);
    i4TnlInstance = (INT4)CRLSP_TE_TNL_INSTANCE (pTeTnlInfo);
    CONVERT_TO_INTEGER (CRLSP_TE_TNL_INGRESS_RTR_ID (pTeTnlInfo), u4IngressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);

    /* Getting the Incarn from the tunnel Index. */
    LdpGetIncarnFromTnlIndex (i4TnlIndex, &u4IncarnId);

    if (LDP_CUR_INCARN == u4IncarnId)
    {
        return LDP_FAILURE;
    }

    if (LdpGetCrLsp (u4IncarnId - 1, u4IngressId, i4TnlIndex,
                     (UINT1) i4TnlInstance, &pCrlspTnlInfo) == LDP_SUCCESS)
    {
        /* This event occurs when the tunnel row status is being made
         * NOT IN SERVICE or the tunnel is deleted. 
         * In the former case then tunnel is brought down,
         * the local CrlspTnlInfo is deleted, but tetunnel info present
         * in the TE module is not deleted.
         */

        /* Sending an internal event to the LDP task, to bring the 
         * tunnel down. */
        return (LdpSNMPEventHandler (CRLDP_TNL_DOWN_EVENT,
                                     pCrlspTnlInfo, (UINT2) (u4IncarnId - 1)));
    }
    else
    {
        LDP_DBG (LDP_IF_SNMP,
                 "ADVT: Tunnel with the given indices is not present.\n");

        TeSigDeleteTnlInfo (pTeTnlInfo, TE_TNL_CALL_FROM_ADMIN);
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpTEEventHandler                                         */
/* Description   : This routine is called by TE module.                      */
/*                 This calls ldpProcessTeEvent, which handles the event     */
/*                 set in u4Event.                                           */
/* Input(s)      : u4Event - Specifies the event given by TE module.         */
/*                 u4TeParams - TE Parameter                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
UINT1
LdpTEEventHandler (UINT4 u4Event, tTeTnlInfo * pTeTnlInfo)
{
    tLdpIfMsg           LdpIfMsg;

    LdpIfMsg.u4MsgType = LDP_TE_EVENT;
    LdpIfMsg.u.TeEvt.u4TeEvt = u4Event;
    LdpIfMsg.u.TeEvt.pTeTnlInfo = pTeTnlInfo;
    if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Failed to EnQ TE Event to LDP Task\n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : ldpProcessTeEvent                                         */
/* Description   : This routine is called by LdpTEEventHandler.              */
/*                 This calls the corresponding functions depending          */
/*                 on the event received.                                    */
/* Input(s)      :  tLdpTeEvtInfo - Te Tunnle Info                           */
/* Output(s)     : NONE.                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE.                                  */
/*****************************************************************************/
UINT1
LdpProcessTeEvent (tLdpTeEvtInfo * pTeEvtInfo)
{
    UINT1               u1RetVal;

    if (pTeEvtInfo->pTeTnlInfo == NULL)
    {
        return LDP_FAILURE;
    }
    switch (pTeEvtInfo->u4TeEvt)
    {
        case LDP_TE_TNL_UP:
            /* Tunnel is being setup */
            u1RetVal = LdpSetupCrlspTunnel (pTeEvtInfo->pTeTnlInfo);
            break;

        case LDP_TE_TNL_DESTROY:
        case LDP_TE_TNL_DOWN:

            /* Tunnel is being brought down */
            u1RetVal = LdpDestroyCrlspTunnel (pTeEvtInfo->pTeTnlInfo);
            break;

        case LDP_TE_DATA_TX_ENABLE:
            /* The tunnel is being enabled to transmit data through it. */
            u1RetVal = LdpProcessDataTxEnableEvent (pTeEvtInfo->pTeTnlInfo);
            break;

        case LDP_TE_DATA_TX_DISABLE:
            /* The tunnel is being disabled to transmit data through it. */
            u1RetVal = LdpProcessDataTxDisableEvent (pTeEvtInfo->pTeTnlInfo);
            break;

        case LDP_TE_GOING_DOWN:

            /* The tunnel is being disabled to transmit data through it. */
            LdpProcessTEGoingDownEvent ();
            TeSigAdminStatus (LDP_TE_PROT_ID, TE_ADMIN_DOWN);
            gu1LdpTeFlag = LDP_FALSE;
            return LDP_SUCCESS;

        default:
            return LDP_FAILURE;
    }

    if (gu1LdpTeFlag == LDP_FALSE)
    {
        TeSigAdminStatus (LDP_TE_PROT_ID, TE_ADMIN_UP);
        gu1LdpTeFlag = LDP_TRUE;
    }
    return u1RetVal;
}

/*****************************************************************************/
/* Function Name   : LdpProcessDataTxEnableEvent                             */
/* Description     : This function processes Tunnel data Transmit Enable     */
/*                   event.                                                  */
/* Input (s)       : pTeTnlInfo - Pointer to the Te Tunnel info through      */
/*                                which the data transfer is to be enabled.  */
/* Output (s)      : None                                                    */
/* Returns         : LDP_SUCCESS/LDP_FAILURE                                 */
/*****************************************************************************/
UINT1
LdpProcessDataTxEnableEvent (tLdpTeTnlInfo * pTeTnlInfo)
{
    UINT4               u4IncarnId = 0;
    INT4                i4TnlIndex;
    INT4                i4TnlInstance;
    UINT4               u4IngressId;

    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;

    i4TnlIndex = (INT4)CRLSP_TE_TNL_INDEX (pTeTnlInfo);
    i4TnlInstance = (INT4)CRLSP_TE_TNL_INSTANCE (pTeTnlInfo);
    CONVERT_TO_INTEGER (CRLSP_TE_TNL_INGRESS_RTR_ID (pTeTnlInfo), u4IngressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);

    /* Getting the Incarn from the tunnel Index. */
    LdpGetIncarnFromTnlIndex (i4TnlIndex, &u4IncarnId);

    if (LDP_CUR_INCARN == u4IncarnId)
    {
        return LDP_FAILURE;
    }

    if (LdpGetCrLsp (u4IncarnId - 1, u4IngressId, i4TnlIndex,
                     (UINT1) i4TnlInstance, &pCrlspTnlInfo) == LDP_SUCCESS)
    {
        if (CRLSP_LCB (pCrlspTnlInfo) != NULL)
        {
            CRLSP_OPER_STATUS (pCrlspTnlInfo) = LDP_OPER_UP;
            pCrlspTnlInfo->pTeTnlInfo->u1CPOrMgmtOperStatus = LDP_OPER_UP;
            /* Calling Mlib update to bring up the data transmission through
             * the tunnel, when the tunnel's oper status is up. */
            LdpSendMplsMlibUpdate (MPLS_MLIB_TNL_MODIFY, CRLDP_DATA_TX_UP,
                                   CRLSP_LCB (pCrlspTnlInfo), NULL);
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name   : LdpProcessDataTxDisableEvent                            */
/* Description     : This function processes Tunnel data Transmit Disable    */
/*                   event.                                                  */
/* Input (s)       : pTeTnlInfo - Pointer to the Te Tunnel info through      */
/*                                which the data transfer is to be disabled. */
/* Output (s)      : None                                                    */
/* Returns         : LDP_SUCCESS/LDP_FAILURE                                 */
/*****************************************************************************/
UINT1
LdpProcessDataTxDisableEvent (tLdpTeTnlInfo * pTeTnlInfo)
{
    UINT4               u4IncarnId = 0;
    INT4                i4TnlIndex;
    INT4                i4TnlInstance;
    UINT4               u4IngressId;

    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;

    i4TnlIndex = (INT4)CRLSP_TE_TNL_INDEX (pTeTnlInfo);
    i4TnlInstance = (INT4)CRLSP_TE_TNL_INSTANCE (pTeTnlInfo);
    CONVERT_TO_INTEGER (CRLSP_TE_TNL_INGRESS_RTR_ID (pTeTnlInfo), u4IngressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);

    /* Getting the Incarn from the tunnel Index. */
    LdpGetIncarnFromTnlIndex (i4TnlIndex, &u4IncarnId);

    if (LDP_CUR_INCARN == u4IncarnId)
    {
        return LDP_FAILURE;
    }

    if (LdpGetCrLsp (u4IncarnId - 1, u4IngressId, i4TnlIndex,
                     (UINT1) i4TnlInstance, &pCrlspTnlInfo) == LDP_SUCCESS)
    {
        if (CRLSP_LCB (pCrlspTnlInfo) != NULL)
        {
            CRLSP_OPER_STATUS (pCrlspTnlInfo) = LDP_OPER_DOWN;
            pCrlspTnlInfo->pTeTnlInfo->u1CPOrMgmtOperStatus = LDP_OPER_DOWN;
            /* Calling Mlib update to bring down the data transmission through
             * the tunnel, when the tunnel's oper status is up. */
            LdpSendMplsMlibUpdate (MPLS_MLIB_TNL_MODIFY,
                                   CRLDP_DATA_TX_DOWN,
                                   CRLSP_LCB (pCrlspTnlInfo), NULL);
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name   : LdpProcessTEGoingDownEvent                              */
/* Description     : This function processes TE ADMIN DOWN event             */
/* Input (s)       : None                                                    */
/* Output (s)      : None                                                    */
/* Returns         : LDP_SUCCESS/LDP_FAILURE                                 */
/*****************************************************************************/
VOID
LdpProcessTEGoingDownEvent ()
{
    UINT4               u4IncarnId;
    UINT4               u4HashIndex;
    tTMO_HASH_TABLE    *pTable = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;

    MPLS_LDP_LOCK ();
    if (LDP_INITIALISED != TRUE)
    {
        TeSigAdminStatus (LDP_TE_PROT_ID, TE_ADMIN_DOWN);
        gu1LdpTeFlag = LDP_FALSE;
        MPLS_LDP_UNLOCK ();
        return;
    }

    for (u4IncarnId = 0; u4IncarnId < LDP_MAX_INCARN; u4IncarnId++)
    {
        u4HashIndex = 0;
        pTable = LDP_CRLSP_TNL_TABLE (u4IncarnId);
        TMO_HASH_Scan_Table (pTable, u4HashIndex)
        {
            /* Searching in the Cr-Lsp Tunnel Table */

            pCrlspTnlInfo =
                (tCrlspTnlInfo *) TMO_HASH_FIRST_NODE (&pTable->
                                                       HashList[u4HashIndex]);
            while (pCrlspTnlInfo != NULL)
            {
                /* Deleting node from the bucket */
                pLspCtrlBlk = pCrlspTnlInfo->pLspCtrlBlock;
                if (pLspCtrlBlk != NULL)
                {
                    LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)]
                        [LDP_LSM_EVT_INT_DSTR] (pLspCtrlBlk, NULL);
                }

                pCrlspTnlInfo =
                    (tCrlspTnlInfo *) TMO_HASH_FIRST_NODE (&pTable->
                                                           HashList
                                                           [u4HashIndex]);
            }
        }
    }

    TeSigAdminStatus (LDP_TE_PROT_ID, TE_ADMIN_DOWN);
    gu1LdpTeFlag = LDP_FALSE;
    MPLS_LDP_UNLOCK ();
    return;
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpcrlsp.c                              */
/*---------------------------------------------------------------------------*/
