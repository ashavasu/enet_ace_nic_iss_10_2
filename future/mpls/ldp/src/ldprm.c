
/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: ldprm.c,v 1.2 2014/11/08 11:40:55 siva Exp $
 ***
 *** Description: Files contails HA implementation of ldp 
 *** NOTE: This file should included in release packaging.
 *** ***********************************************************************/


#include "ldpincs.h"

VOID        LdpRmProcessBulkTail(VOID);
VOID        LdpRmHandleGoActiveEvent(VOID);
VOID        LdpRmHandleGoStandbyEvent(VOID);
VOID        LdpRmHandleStandbyUpEvent(tRmNodeInfo * pRmNode);
VOID        LdpRmHandleStandByDownEvent(tRmNodeInfo * pRmNode);
VOID        LdpRmHandleConfRestoreCompEvt(VOID);
VOID        LdpHandleRmMessageEvent(tLdpRmEvtInfo * pRmLdpMsg);
VOID        LdpRmInitBulkUpdateFlags(VOID);   
VOID        LdpRmProcessBulkUpdateMsg (tRmMsg * pMsg,UINT4 *pOffset);
VOID        LdpRmSendBulkReq(VOID); 
VOID        LdpRmSendBulkTail(VOID);

/*****************************************************************************/
/* Function     : LdpProcessRmEvent                                          */
/*                                                                           */
/* Description  : This function receives the RM events and RM message.       */
/*                Based on the events it will call the corresponding         */
/*                functions to process the events.                           */
/* Input        : pRmLdpMsg - RM Message                                     */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID LdpProcessRmEvent (tLdpRmEvtInfo *pRmLdpMsg)
{
    LDP_DBG (MAIN_DEBUG, "ENTRY:LdpProcessRmEvent\n");

    switch(pRmLdpMsg->u1Event)
    {

        case RM_MESSAGE:
            LDP_DBG (HA_DEBUG, "Received RM_MESSAGE event.\n");
            LdpHandleRmMessageEvent (pRmLdpMsg);
            break;

        case GO_ACTIVE:
            LDP_DBG (HA_DEBUG, "Received GO_ACTIVE event.\n");
            LdpRmHandleGoActiveEvent();
            break;

        case GO_STANDBY:
            LDP_DBG (HA_DEBUG, "Received GO_STANDBY event.\n");
            LdpRmHandleGoStandbyEvent();
            break;

        case RM_PEER_UP:
            LDP_DBG (HA_DEBUG, "Received RM_PEER_UP event.\n");
            LdpRmHandleStandbyUpEvent ((tRmNodeInfo *)
                    (pRmLdpMsg->pBuff));
            break;

        case RM_PEER_DOWN:
            LDP_DBG (HA_DEBUG, "Received RM_PEER_DOWN event.\n");
            LdpRmHandleStandByDownEvent((tRmNodeInfo *)
                    (pRmLdpMsg->pBuff));
            break;

        case L2_INITIATE_BULK_UPDATES:
            LDP_DBG (HA_DEBUG, "Received L2_INITIATE_BULK_UPDATES event.\n");
            LdpRmSendBulkReq();
            break;

        case RM_CONFIG_RESTORE_COMPLETE:
            LDP_DBG (HA_DEBUG, "Received RM_CONFIG_RESTORE_COMPLETE event.\n");
            LdpRmHandleConfRestoreCompEvt();
            break;

        default:
            LDP_DBG (HA_DEBUG, "Received Invalid event.\n");
            break;

    }
    LDP_DBG (MAIN_DEBUG, "EXIT:LdpProcessRmEvent\n");
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmHandleGoActiveEvent                                   */
/*                                                                           */
/* Description  : This function handles the GO_ACTIVE event from RM.         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID LdpRmHandleGoActiveEvent()
{
    tRmProtoEvt         ProtoEvt; 
    UINT4               u4NodeState = gLdpInfo.ldpRmInfo.u4LdpRmState;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));    
    gLdpInfo.ldpRmInfo.u4PeerCount = LdpRmGetStandbyNodeCount ();

     /* if the node count is greater than one standby is up*/
    if(gLdpInfo.ldpRmInfo.u4PeerCount > 0)
    {
        gLdpInfo.ldpRmInfo.u4LdpRmState = LDP_RM_ACTIVE_STANDBY_UP;
    }
    else
    {
        gLdpInfo.ldpRmInfo.u4LdpRmState = LDP_RM_ACTIVE_STANDBY_DOWN;
    }
    /*check the node state to process the event */
    if(u4NodeState == LDP_RM_INIT)
    {
        /* consolidate the HW list and populate to  RBTree*/
        ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
        gLdpInfo.ldpRmInfo.u4LdpRmGoActiveReason = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
    }
    else if (u4NodeState == LDP_RM_STANDBY)
    {
        LdpRmInitBulkUpdateFlags ();
        ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
        gLdpInfo.ldpRmInfo.u4LdpRmGoActiveReason = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
#ifdef MPLS_LDP_BFD_WANTED
	    LdpDeRegisterAllWithBfd();
#endif
        if (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) == LDP_GR_CAPABILITY_FULL)
        {
            /* Reserve TUNNEL interfaces in case of HA*/
            LdpGrReserveResources (LDP_TRUE);
            LdpGrInitProcess();
        }
    }
    else
    {
        LDP_DBG (HA_DEBUG, "Received GO_ACTIVE event while the node is neither in INIT nor STANDBY state\n");
        return;
    }
    ProtoEvt.u4AppId = RM_LDP_APP_ID;
    ProtoEvt.u4Error = RM_NONE;
    /* send event to RM */
    (VOID)LdpRmSendEventToRm (&ProtoEvt);
    LDP_DBG1 (HA_DEBUG, "Send event %u to RM Success\n",ProtoEvt.u4Event);

}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmHandleGoStandbyEvent                                  */
/*                                                                           */
/* Description  : This function handles the GO_STANDBY event from RM         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID LdpRmHandleGoStandbyEvent()
{

    tRmProtoEvt         ProtoEvt;
    UINT4               u4NodeState = gLdpInfo.ldpRmInfo.u4LdpRmState;

    MEMSET (&ProtoEvt, 0, sizeof (tRmProtoEvt));
    if((u4NodeState == LDP_RM_STANDBY)||(u4NodeState == LDP_RM_INIT))
    {
        LDP_DBG1(HA_DEBUG, "Received GO_STANDBY event when the node is :%s state\n", u4NodeState);     
        return;
    }
    else if ((u4NodeState == LDP_RM_ACTIVE_STANDBY_UP)||(u4NodeState == LDP_RM_ACTIVE_STANDBY_DOWN))
    {
        LdpRmInitBulkUpdateFlags();
        gLdpInfo.ldpRmInfo.u4LdpRmState = LDP_RM_STANDBY;
		
		LdpEntityDownAtGoStandBy();
		LdpEntityUpAtGoStandBy();	
        gLdpInfo.ldpRmInfo.u4PeerCount = 0;
        
        ProtoEvt.u4AppId = RM_LDP_APP_ID;
        ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
        ProtoEvt.u4Error = RM_NONE;

        (VOID)LdpRmSendEventToRm (&ProtoEvt);
        LDP_DBG (HA_DEBUG, "Sending event RM_STANDBY_EVT_PROCESSED to RM Success \n");
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmInitBulkUpdateFlags                                   */
/*                                                                           */
/* Description  : This function initialize the flags used to bulk update.    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID LdpRmInitBulkUpdateFlags(VOID)
{

    gLdpInfo.ldpRmInfo.u1BulkUpdModuleStatus = LDP_RM_ILM_GBL_MOD ;
    gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_NOT_STARTED;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmHandleStandbyUpEvent                                  */
/*                                                                           */
/* Description  : This function handles the Standby up event.                */
/*                                                                           */
/* Input        : pRmNode - info contains number of Standby nodes            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID LdpRmHandleStandbyUpEvent (tRmNodeInfo * pRmNode)
{

    
    UINT4  u4NodeStatus = gLdpInfo.ldpRmInfo.u4LdpRmState;

    if (u4NodeStatus == LDP_RM_STANDBY)
    {
        LDP_DBG (HA_DEBUG, "Got the Standby Up event at standby.\n");              
        LdpRmRelRmMsgMem ((UINT1 *) pRmNode);
        return;
    }
    gLdpInfo.ldpRmInfo.u4PeerCount = pRmNode->u1NumStandby;
    if (pRmNode->u1NumStandby > 0)
    {
        gLdpInfo.ldpRmInfo.u4LdpRmState = LDP_RM_ACTIVE_STANDBY_UP;

    }
    else
    {
        gLdpInfo.ldpRmInfo.u4LdpRmState = LDP_RM_ACTIVE_STANDBY_DOWN;
    }

    LdpRmRelRmMsgMem ((UINT1 *) pRmNode); 
   

    if (gLdpInfo.ldpRmInfo.b1IsBulkReqRcvd == LDP_RM_TRUE)
    {
        gLdpInfo.ldpRmInfo.b1IsBulkReqRcvd = LDP_RM_FALSE ;
        LDP_DBG (HA_DEBUG, "Update the flags\n");
        LdpRmInitBulkUpdateFlags ();
        gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_INPROGRESS;     
        LDP_DBG (HA_DEBUG, "send Bulk update  message\n");
        LdpRmProcessBulkReq (); 
    }  

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmHandleStandByDownEvent                                */
/*                                                                           */
/* Description  : This function handles the Standby down event               */
/*                                                                           */
/* Input        : pRmNode - info contains number of Stanby nodes             */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID LdpRmHandleStandByDownEvent (tRmNodeInfo * pRmNode)
{

    gLdpInfo.ldpRmInfo.u4PeerCount = pRmNode->u1NumStandby;

    if(gLdpInfo.ldpRmInfo.u4LdpRmState == LDP_RM_STANDBY)
    {
        LDP_DBG (HA_DEBUG, "Got the Standby Down event at standby\n");
        LdpRmRelRmMsgMem ((UINT1 *) pRmNode);
        return;
    }
    if (pRmNode->u1NumStandby > 0)
    {
        gLdpInfo.ldpRmInfo.u4LdpRmState = LDP_RM_ACTIVE_STANDBY_UP;
    }
    else
    {
        gLdpInfo.ldpRmInfo.u4LdpRmState = LDP_RM_ACTIVE_STANDBY_DOWN;
    }
    LdpRmRelRmMsgMem ((UINT1 *) pRmNode);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmHandleConfRestoreCompEvt                              */
/*                                                                           */
/* Description  : This function handle the event  RM_CONFIG_RESTORE_COMPLETE */
/*                received from the RM.Move the node to STANDBY state from   */
/*                INIT.                                                      */
/*                                                                           */
/* Input        : Nine                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID LdpRmHandleConfRestoreCompEvt()
{ 

    if( (gLdpInfo.ldpRmInfo.u4LdpRmState == LDP_RM_INIT) && (LdpRmGetRmNodeState() == RM_STANDBY))
    {
        gLdpInfo.ldpRmInfo.u4LdpRmState =  LDP_RM_STANDBY;
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpHandleRmMessageEvent                                    */
/*                                                                           */
/* Description  : This function process the messages set by the peer.        */
/*                This function is invoked whenever LDP module receive       */
/*                dynamic sync up/bulk update/bulk request message.          */
/*                This function parses the messages received and updates     */
/*                its database. Following are the messages and related       */
/*                message process functions                                  */
/*                                                                           */
/* Input        : pRmLdpMsg                                                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID LdpHandleRmMessageEvent (tLdpRmEvtInfo * pRmLdpMsg) 
{

    UINT4               u4SeqNum = 0;
    UINT4               u4LdpNodeState = gLdpInfo.ldpRmInfo.u4LdpRmState;
    UINT4               u4Offset = 0;
    UINT2               u2MsgLen = 0;
    UINT1               u1MsgType = 0;

    RM_PKT_GET_SEQNUM (pRmLdpMsg->pBuff, &u4SeqNum);
    RM_STRIP_OFF_RM_HDR (pRmLdpMsg->pBuff, pRmLdpMsg->u2Length);
    LDP_RM_GET_1_BYTE (pRmLdpMsg->pBuff, u4Offset, u1MsgType);
    LDP_RM_GET_2_BYTE (pRmLdpMsg->pBuff, u4Offset, u2MsgLen);

    if (pRmLdpMsg->u2Length != u2MsgLen)
    {
        RM_FREE (pRmLdpMsg->pBuff);
        LdpRmApiSendProtoAckToRM(u4SeqNum);
        return;
    }
    if ((u4LdpNodeState !=LDP_RM_ACTIVE_STANDBY_UP ) &&
            (u1MsgType == LDP_RED_BULK_UPDT_REQ_MSG))
    {
        RM_FREE (pRmLdpMsg->pBuff);
        LdpRmApiSendProtoAckToRM(u4SeqNum);
        return;
    }
    switch (u1MsgType)
    {

        case LDP_RED_BULK_UPDT_REQ_MSG:

            LDP_DBG (HA_DEBUG, "Received LDP_RED_BULK_UPDT_REQ_MSG message\n");  
            if (LDP_IS_STANDBY_UP () == LDP_RM_FALSE)
            {
                gLdpInfo.ldpRmInfo.b1IsBulkReqRcvd = LDP_RM_TRUE;
                break;
            }
            LdpRmInitBulkUpdateFlags();
            gLdpInfo.ldpRmInfo.b1IsBulkReqRcvd = LDP_RM_FALSE;
            gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_INPROGRESS;
            LdpRmProcessBulkReq();
            break;

        case LDP_RED_BULK_UPDATE_MSG:
            LDP_DBG (HA_DEBUG, "Received LDP_RED_BULK_UPDATE_MSG message\n");
            LdpRmProcessBulkUpdateMsg (pRmLdpMsg->pBuff, &u4Offset);
            break;

        case LDP_RED_BULK_UPDT_TAIL_MSG:
            LDP_DBG (HA_DEBUG, "Received LDP_RED_BULK_UPDT_TAIL_MSG message\n");
            LdpRmProcessBulkTail ();
            break;

        case LDP_RED_ILM_HW_LIST_SYNC_MSG:
            LDP_DBG (HA_DEBUG,"Received LDP_RED_SYNC_ILM_MSG message\n");
            LdpRmProcessILMHwListSync (pRmLdpMsg->pBuff, &u4Offset);
            break;

        case LDP_RED_FTN_HW_LIST_SYNC_MSG:
            LDP_DBG (HA_DEBUG,"Received LDP_RED_SYNC_FTN_MSG message\n");
            LdpRmProcessFTNHwListSync (pRmLdpMsg->pBuff, &u4Offset);
            break;

        default:
            LDP_DBG (HA_DEBUG, "Received invalid message\n");
            break;
    }
    RM_FREE (pRmLdpMsg->pBuff);
    LdpRmApiSendProtoAckToRM (u4SeqNum);         
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmProcessBulkReq                                        */
/*                                                                           */
/* Description  : This function handles when active node gets a bulk         */
/*                request from the standby node.It call the corresponing     */
/*                module to send the bulk updates.                           */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID LdpRmProcessBulkReq ()
{

    if (LDP_IS_NODE_ACTIVE() != LDP_RM_TRUE)
    {
        LDP_DBG(HA_DEBUG, "Node is not in active State\n");
        return;
    }
    if (LDP_IS_STANDBY_UP() == LDP_RM_FALSE)
    {
        LDP_DBG(HA_DEBUG, "No standby Node is existing\n");
        return;
    }
    LdpRmSendBulkILMInfo();
    LdpRmSendBulkFTNInfo();

    gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_COMPLETED;
    gLdpInfo.ldpRmInfo.u1BulkUpdModuleStatus = LDP_RM_MOD_COMPLETED;
    LdpRmSendBulkTail();
} 

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmSendBulkTail                                          */
/*                                                                           */
/* Description  : This function is called when active node has completed     */
/*                sending all the bulk updates or it has no bulk updates     */
/*                to send to the standby. This function constucts and        */
/*                sends the bulk tail message.                               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID LdpRmSendBulkTail()
{
    tRmMsg             *pRmMsg = NULL;
    UINT4               u4Offset = LDP_ZERO;
    INT4                i4RetVal = LDP_FAILURE;

    LdpRmSetBulkUpdateStatus();
    pRmMsg = LdpRmAllocForRmMsg (LDP_RM_BULK_TAIL_MSG_LEN);
    if (pRmMsg == NULL)
    {
        LDP_DBG(HA_DEBUG,"Memory Allocation Failed\n");
        return;
    }

    LDP_RM_PUT_1_BYTE (pRmMsg, u4Offset, (UINT1) LDP_RED_BULK_UPDT_TAIL_MSG);
    LDP_RM_PUT_2_BYTE (pRmMsg, u4Offset, (UINT2) LDP_RM_BULK_TAIL_MSG_LEN);

    i4RetVal = LdpRmSendMsgToRm (pRmMsg, (UINT2) LDP_RM_BULK_TAIL_MSG_LEN);
    if (i4RetVal != LDP_SUCCESS)
    {
        LDP_DBG (HA_DEBUG, "Sending bulk tail to RM is failed\n");
    }
    else
    {
        LDP_DBG (HA_DEBUG, "Sending bulk tail to RM is Success\n");
    }
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmProcessBulkUpdateMsg                                  */
/*                                                                           */
/* Description  : This function processes the bulk update message received.  */
/*                                                                           */
/* Input        : pMsg - Bulk update messages                                */
/*                                                                           */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID LdpRmProcessBulkUpdateMsg (tRmMsg * pMsg, UINT4 *pOffset)
{

    UINT1               u1BulkUpdType = 0;
    UINT4               u4Offset = *pOffset;

    gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_INPROGRESS;
    RM_GET_DATA_1_BYTE (pMsg, u4Offset, u1BulkUpdType);
    u4Offset = u4Offset + sizeof(UINT1);

    switch (u1BulkUpdType)
    {
        case LDP_RM_ILM_GBL_MOD:
            LDP_DBG (HA_DEBUG, "Received LDP_RM_ILM_GBL_MOD message\n");
            LdpRmProcessBulkILMGblInfo (pMsg, &u4Offset);
            break; 

        case LDP_RM_FTN_GBL_MOD:
            LDP_DBG (HA_DEBUG, "Received LDP_RM_FTN_GBL_MOD message\n");
            LdpRmProcessBulkFTNGblInfo (pMsg, &u4Offset);
            break;

        default :
            LDP_DBG (HA_DEBUG, "Received Invalid  message\n");
            break;
    }
}

/*****************************************************************************/
/* Function     : LdpRmProcessBulkTail                                       */
/*                                                                           */
/* Description  : This function sends the bulk update complete messages      */
/*                to RM. Sends the RM_PROTOCOL_BULK_UPDT_COMPLETION event    */
/*                to the RM.                                                 */
/*                                                                           */
/* Input        : None.                                                      */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : VOID.                                                      */
/*                                                                           */
/*****************************************************************************/

VOID LdpRmProcessBulkTail()
{
    tRmProtoEvt     ProtoEvt;

    ProtoEvt.u4AppId = RM_LDP_APP_ID;
    ProtoEvt.u4Event = RM_PROTOCOL_BULK_UPDT_COMPLETION;
    ProtoEvt.u4Error = RM_NONE;

    (VOID)LdpRmSendEventToRm (&ProtoEvt);
    gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_COMPLETED;

}
/*****************************************************************************/
/* Function     : LdpRmSendBulkAbort                                         */
/*                                                                           */
/* Description  : This API send the bulk update abort event to RM with the   */
/*                the given error code                                       */
/*                                                                           */
/* Input        : u4ErrCode - Error code.                                    */
/* Output       : None                                                       */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID  LdpRmSendBulkAbort (UINT4 u4ErrCode)
{
    tRmProtoEvt         ProtoEvt;
   

    gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_ABORTED;
    MEMSET (&ProtoEvt, 0, sizeof(tRmProtoEvt));
    ProtoEvt.u4AppId = RM_LDP_APP_ID;
    ProtoEvt.u4Event = RM_BULK_UPDT_ABORT;
    ProtoEvt.u4Error = u4ErrCode;
    (VOID)LdpRmSendEventToRm (&ProtoEvt);

    LDP_DBG1(HA_DEBUG,"Sending event RM_BULK_UPDT_ABORT with error code:%d success\n", u4ErrCode);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmSendBulkReq                                           */
/*                                                                           */
/* Description  : This function send the bulk update request.                */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/

VOID  LdpRmSendBulkReq()
{
    tRmMsg             *pRmMsg = NULL;
    UINT4              u4Offset = 0;
    INT4               i4RetVal = LDP_FAILURE; 
    

    pRmMsg =  LdpRmAllocForRmMsg (LDP_RM_BULK_UPD_REQ_MSG_LEN);
    if (pRmMsg == NULL)
    {
        LDP_DBG (HA_DEBUG, "Memory Allocation failed\n");
        return;
    }
    LDP_RM_PUT_1_BYTE (pRmMsg, u4Offset, (UINT1) LDP_RED_BULK_UPDT_REQ_MSG);
    LDP_RM_PUT_2_BYTE (pRmMsg, u4Offset, (UINT2) LDP_RM_BULK_UPD_REQ_MSG_LEN);

    i4RetVal = LdpRmSendMsgToRm (pRmMsg, (UINT2) LDP_RM_BULK_UPD_REQ_MSG_LEN);

    if (i4RetVal != LDP_SUCCESS)
    {
        LDP_DBG (HA_DEBUG,"Sending bulk request to RM is failed\n");
        return;
    }
    else
    {
        LDP_DBG (HA_DEBUG,"Sending bulk request to RM is Success\n");
        return;
    }
}  
/*****************************************************************************/
/*                                                                           */
/* Function     : LdpRmInit                                                  */
/*                                                                           */
/* Description  : This function initialize the LDP RM related objects.       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID LdpRmInit ()
{
    MEMSET (&gLdpInfo.ldpRmInfo, 0, sizeof(tLdpRMInfo));
    /* Initialize the RM variables */
    gLdpInfo.ldpRmInfo.u4LdpRmState = LDP_RM_INIT; 
    gLdpInfo.ldpRmInfo.u4PeerCount  = 0;
    gLdpInfo.ldpRmInfo.b1IsBulkReqRcvd = LDP_RM_FALSE;
    gLdpInfo.ldpRmInfo.u4DynBulkUpdatStatus = LDP_RM_BLKUPDT_NOT_STARTED;
    gLdpInfo.ldpRmInfo.u1AdminState = LDP_DISABLED; 
}

