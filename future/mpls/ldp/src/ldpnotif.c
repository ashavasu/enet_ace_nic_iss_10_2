
/*---------------------------------------------------------------------------*/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: ldpnotif.c,v 1.28 2016/01/29 12:55:57 siva Exp $
 * -----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpnotif.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : LDP (NOTIF SUB-MODULE)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains fuctions to construct
 *                             and handle the Notification messages
 *----------------------------------------------------------------------------*/
#include "ldpincs.h"

/*****************************************************************************/
/* Function Name : LdpSendNotifMsg                                           */
/* Description   : This routine constucts and sends the Notification Message.*/
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session entry on which this */
/*                                 Notif Message has to be sent.             */
/*                 pu1Msg        - Points to the actual LDP message,         */
/*                                 which resulted in this Notification Msg   */
/*                                 generation.                               */
/*                 u1PDUFlag     - This flag indicates if pu1Msg points to   */
/*                                 LDP PDU or LDP Message.                   */
/*                                 Value '1' indicates LDP PDU.              */
/*                 u1StatusType  - Indicates the event being signalled.      */
/*                 pLspCtrlBlock - Points to the LspCtrlBlock                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS     if Notif Message is constructed and sent  */
/*                 successfully, else LDP_FAILURE is returned                */
/*****************************************************************************/

UINT1
LdpSendNotifMsg (tLdpSession * pSessionEntry, UINT1 *pu1Msg,
                 UINT1 u1PDUFlag, UINT1 u1StatusType,
                 tLspCtrlBlock * pLspCtrlBlock)
{
    UINT1               u1StatusTlvLen =
        (LDP_TLV_HDR_LEN + LDP_STATUS_CODE_LEN + LDP_MSG_ID_LEN +
         LDP_MSG_TYPE_LEN);

    UINT2               u2OptParamLen = 0;
    UINT1              *pu1MsgPtr=pu1Msg;
    UINT1              *pu1TmpMsg = NULL;
    UINT2               u2MsgLen = 0;
    UINT2               u2MsgType;
    UINT2               u2MandParamLen =
        { (UINT2) (LDP_MSG_ID_LEN + u1StatusTlvLen) };
    UINT4               u4Offset = 0;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLdpMsgHdrAndId     MsgHdrAndId = { OSIX_HTONS (LDP_NOTIF_MSG), 0, 0 };
    tStatusTlv          StatusTlv = { {OSIX_HTONS (LDP_STATUS_TLV),
                                       OSIX_HTONS ((LDP_STATUS_CODE_LEN +
                                                    LDP_MSG_ID_LEN +
                                                    LDP_MSG_TYPE_LEN))}, 0, 0,
    0, 0
    };
    tTlvHdr             OptionalTlvHdr;
    tLdpId              PeerLdpId;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    tFec                FecElmnt;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;



    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);
    MEMSET(&FecElmnt,0,sizeof(tFec));

    /* If node is standby then dont send notif message*/
#ifdef LDP_HA_WANTED
	if (gLdpInfo.ldpRmInfo.u4LdpRmState == LDP_RM_STANDBY ||
		gLdpInfo.ldpRmInfo.u4LdpRmState == LDP_RM_INIT)
	{
		return LDP_SUCCESS;
	}
#endif
	/*MPLS_IPv6 add end*/
    MEMSET(&ConnId, LDP_ZERO, sizeof(tGenAddr));
    MEMSET(&LdpIfAddrZero, LDP_ZERO, sizeof (tGenU4Addr));
    MEMSET(&FecElmnt,0,sizeof(tFec));
    /* Constructing the Optional Tlv-RequestMsgId Tlv, if the Nak being
     * constructed is Label Request Aborted.  */
    if (u1StatusType == LDP_STAT_TYPE_LBL_REQ_ABRTD)
    {
        /* Label Request Message Id Tlv Len */
        u2OptParamLen +=(UINT2) (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN);
    }
    else if (pu1Msg != NULL)
    {
        /* Error Msg has to be returned */
        /* Construct Optional TLV for Returned PDU/Message based on u1PDUFlag */
        if (u1PDUFlag == LDP_TRUE)
        {
            u2OptParamLen = LDP_PDU_HDR_LEN;
            OptionalTlvHdr.u2TlvType = OSIX_HTONS (LDP_RETURNED_PDU_TLV);
        }
        else
        {

           /* Message length is obtained */
            pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
            LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);
            u2OptParamLen = (UINT2)(u2MsgLen + LDP_MSG_HDR_LEN);
            if(u2OptParamLen > (UINT2)(LDP_RETURN_MSG_LEN))
            {
                u2OptParamLen = (UINT2)LDP_RETURN_MSG_LEN; 
            }
            OptionalTlvHdr.u2TlvType = OSIX_HTONS (LDP_RETURNED_MSG_TLV);
        }
        OptionalTlvHdr.u2TlvLen = OSIX_HTONS (u2OptParamLen);
        u2OptParamLen += (UINT2)LDP_TLV_HDR_LEN;
    }

    u2MsgLen = (UINT2) (u2MandParamLen + u2OptParamLen);
    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2MsgLen), LDP_PDU_HDR_LEN);

    if (pMsg == NULL)
    {
        LDP_DBG (LDP_NOTIF_MEM,
                 "NOTIF: Buffer Allocation for Notif Msg failed\n");
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS (u2MsgLen);
    INC_LDP_ENTY_MSG_ID (SSN_GET_ENTITY (pSessionEntry));
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry)); 

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_NOTIF_MEM,
                 "NOTIF: Error in copying Notif MsgHdr and MsgId "
                 "to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct Status Tlv */
    StatusTlv.u4StatusCode =
        OSIX_HTONL (LdpConvertLocalValToTLVVal (u1StatusType));

    /* MsgId and Type are by default assigned to zero in case 
     *   - Error is encountered while processing PDU Header
     *   - no returned message is passed to this routine 
     * In other cases, MsgID and Type are extracted from pu1Msg */

    if ((pu1Msg != NULL) && (u1PDUFlag != LDP_TRUE))
    {
        /* Get Msg Type from pu1Msg */
        StatusTlv.u2MsgType = *((UINT2 *) (VOID *) pu1Msg);
        /* Get MsgId */
        StatusTlv.u4MsgId = *((UINT4 *) (VOID *) (pu1Msg + LDP_MSG_HDR_LEN));
    }
    else
    {
        if (pLspCtrlBlock != NULL)
        {
            switch (u1StatusType)
            {
                case LDP_STAT_TYPE_LOOP_DETECTED:
                case LDP_STAT_TYPE_NO_ROUTE:
                case LDP_STAT_LBL_RSRC_AVBL:
                    u2MsgType = LDP_LBL_REQUEST_MSG;
                    StatusTlv.u2MsgType = OSIX_HTONS (u2MsgType);
                    StatusTlv.u4MsgId =
                        OSIX_HTONL (pLspCtrlBlock->u4UStrLblReqId);
                    break;
                default:
                    /* Assign MsgId and MsgType to default zero. */
                    StatusTlv.u2MsgType = 0;
                    StatusTlv.u4MsgId = 0;
                    break;
            }

        }
        else
        {
            /* Assign MsgId and MsgType to default zero. */
            StatusTlv.u2MsgType = 0;
            StatusTlv.u4MsgId = 0;
        }
    }

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &StatusTlv, u4Offset,
                                   u1StatusTlvLen) == CRU_FAILURE)
    {
        LDP_DBG (LDP_NOTIF_MEM,
                 "NOTIF: Error while copying Status Tlv to buffer\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += u1StatusTlvLen;

    /* Construct Fec Element */
    if (pLspCtrlBlock != NULL)
    {
            FecElmnt.u1FecElmntType = pLspCtrlBlock->Fec.u1FecElmntType;
            FecElmnt.u2AddrFmly = OSIX_HTONS (pLspCtrlBlock->Fec.u2AddrFmly);
            if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_HOSTADDR_TYPE)
            {
                    /* When the Fec Element Type is HostAddress, the Address Length
                     * should be in Octect Length.
                     * u1PrefLen has the length in bits.
                     * It is divided by 8 (right shift 3 times), to get the length
                     * in Octect Length.*/
                    FecElmnt.u1PreLen = (UINT1) ((pLspCtrlBlock->Fec.u1PreLen) >> 3);
            }
            else
            {
                    FecElmnt.u1PreLen = pLspCtrlBlock->Fec.u1PreLen;
            }

            LdpCopyAddr(&FecElmnt.Prefix,&pLspCtrlBlock->Fec.Prefix,pLspCtrlBlock->Fec.u2AddrFmly);

            if(pLspCtrlBlock->Fec.u2AddrFmly==LDP_ADDR_TYPE_IPV4)
            {
                    LDP_IPV4_U4_ADDR(FecElmnt.Prefix)=OSIX_HTONL(LDP_IPV4_U4_ADDR(FecElmnt.Prefix));
            }

#ifdef MPLS_IPV6_WANTED
            if(pLspCtrlBlock->Fec.u2AddrFmly==LDP_ADDR_TYPE_IPV6)
            {
                    LDP_DBG2 (LDP_ADVT_TX,
                                    "Label Req Msg for the prefix %s/%d \n",
                                    Ip6PrintAddr(&FecElmnt.Prefix.Ip6Addr),
                                    FecElmnt.u1PreLen);
            }
            else
#endif
            {
                    u4Temp = OSIX_HTONL (FecElmnt.Prefix.u4Addr);
                    MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);
                    LDP_DBG5 (LDP_ADVT_TX,
                                    "Label Req Msg for the prefix %d.%d.%d.%d/%d \n",
                                    au1IpAddr[3], au1IpAddr[2], au1IpAddr[1], au1IpAddr[0],
                                    FecElmnt.u1PreLen);
            }
    }

    /* Construct and copy Label Request Msg Id Tlv */
    if ((pu1Msg != NULL) && (u1StatusType == LDP_STAT_TYPE_LBL_REQ_ABRTD))
    {
        /* Offset to ReqMsgId Tlv, present in pu1Msg(Request Abort Msg) */
        LDP_OFFSET_TO_REQID (pu1Msg, pu1TmpMsg);
        if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) pu1TmpMsg, u4Offset,
                                       (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN)) ==
            CRU_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: Failed copying ReqMsgId Tlv to Buf chain\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (LDP_TLV_HDR_LEN + LDP_MSG_ID_LEN);
    }
    else if (pu1Msg != NULL)
    {
        /* If Nak Type is not 'LabelReqAborted' and the msg/pdu to be 
         * returned is Non-Null, then copying the ReturnMsg/Pdu Tlv 
         * onto Buffer */
        if (CRU_BUF_Copy_OverBufChain
            (pMsg, (UINT1 *) &OptionalTlvHdr, u4Offset,
             LDP_TLV_HDR_LEN) == CRU_FAILURE)
        {
            LDP_DBG (LDP_NOTIF_MEM,
                     "NOTIF: Error while copying Optional Tlv Hdr to buffer\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += LDP_TLV_HDR_LEN;
        if (CRU_BUF_Copy_OverBufChain (pMsg, pu1Msg,
                                       u4Offset,
                                       (u2OptParamLen - LDP_TLV_HDR_LEN))
            == CRU_FAILURE)
        {
            LDP_DBG (LDP_NOTIF_MEM,
                     "NOTIF: Error while copying Returned Msg/PDU to buffer\n");
            CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
            return LDP_FAILURE;
        }
        u4Offset += (u2OptParamLen - LDP_TLV_HDR_LEN);
    }

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);
    SSN_GET_PEERID (PeerLdpId, pSessionEntry);
    LDP_DBG4 (LDP_NOTIF_TX,
              "NOTIF: Sending Notif Msg To %d.%d.%d.%d \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, u2MsgLen, SSN_GET_ENTITY (pSessionEntry), LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        /*MPLS_IPv6 add end*/
        LDP_DBG (LDP_NOTIF_TX, "NOTIF: Failed to send Notif Msg\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }

    switch (u1StatusType)
    {
        case LDP_STAT_TYPE_BAD_LDPID:
            LDP_ENTITY_UPDATE_BAD_LDPID_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_BAD_PDU_LEN:
            LDP_ENTITY_UPDATE_BAD_PDU_LEN_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_BAD_MSG_LEN:
            LDP_ENTITY_UPDATE_BAD_MSG_LEN_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_BAD_TLV_LEN:
            LDP_ENTITY_UPDATE_BAD_TLV_LEN_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_ADVRT_MODE_REJECT:
            LDP_ENTITY_UPDATE_SESSREJ_ADVRT_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_LBL_RANGE_REJECT:
            LDP_ENTITY_UPDATE_SESSREJ_LBLRANGE_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_SHUT_DOWN:
            LDP_ENTITY_UPDATE_SHUTDOWN_NOTIF_SENT (pSessionEntry);
            break;

        case LDP_STAT_TYPE_UNKNOWN_TLV:
            LDP_ENTITY_UPDATE_MALFORMED_TLV_ERR (pSessionEntry);
            LDP_SESSION_UPDATE_UNKNOWN_TLV_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_UNKNOWN_MSG_TYPE:
            LDP_SESSION_UPDATE_UNKNOWN_MSGTYPE_ERR (pSessionEntry);
    }

    LDP_DBG (LDP_NOTIF_TX, "NOTIF: Notif Msg sent\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleNotifMsg                                         */
/* Description   : This routine is called when PDU Process SM receives a     */
/*                 Notification Message.                                     */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the Session on which the Notif  */
/*                                 message is received.                      */
/*                 pu1Msg - Points to the received Notification Message.     */
/*                                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
UINT1
LdpHandleNotifMsg (tLdpSession * pSessionEntry, UINT1 *pu1Msg)
{
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tFec                Fec;
    tLdpMsgInfo         LdpMsgInfo;
    tLdpMsgInfo        *pMsg = &LdpMsgInfo;
    tLdpAdjacency      *pLdpAdj = NULL;
    tLdpAdjacency      *pTempLdpAdj = NULL;
    tTMO_SLL           *pLdpAdjList = NULL;
    tTMO_SLL           *pEntityList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSessionEntry = NULL;
    UINT4               u4MsgId;
    UINT4               u4ReqMsgId = 0;
    UINT4               u4StatusCode;
    UINT4               u4StatusData;
    UINT2               u2Incarn = LDP_ZERO;
    UINT2               u2PrcsLen = 0;
    UINT2               u2MsgLen;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT2               u2MsgType;
    UINT2               u2FecTlvLen = 0;
    UINT1               u1StatusType = 0;
    UINT1               u1ErrTypeIndex;
    UINT1               u1DSErrTypeIndex;
    UINT1              *pu1MsgPtr = pu1Msg;
    UINT1               u1FwdFlag;
    UINT1               u1FatalFlag;
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);
    UINT2               u2StatusTlvLen = LDP_ZERO;
    UINT4               u4TmpConnId=0;

    LDP_DBG (LDP_SSN_PRCS, "LdpHandleNotifMsg: ENTRY\n");

    MEMSET (pMsg, LDP_ZERO, sizeof (tLdpMsgInfo));
    MEMSET (&Fec, LDP_ZERO, sizeof (tFec));
    u2Incarn = SSN_GET_INCRN_ID (pSessionEntry);

    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Extracting Notif MsgId */
    pu1MsgPtr = pu1MsgPtr + LDP_MSGID_LEN;
    u2PrcsLen = LDP_MSGID_LEN;

    /* Processing the Status Tlv */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    if (u2TlvType != LDP_STATUS_TLV)
    {
        /*  First Tlv is not StatusTlv(Mandatory) - Error */
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: StatusTlv(Mandatory) not present\n");
        LdpSendNotifMsg (pSessionEntry, pu1Msg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        return LDP_FAILURE;
    }

    /* Extracting StatusData */

    /* Offset to StatusCode */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2StatusTlvLen);

    if (u2StatusTlvLen < (LDP_STATUS_CODE_LEN + LDP_MSG_TYPE_LEN
                          + LDP_MSGID_LEN))
    {
        LDP_DBG (LDP_PRCS_PRCS, "PRCS: MalFormed TLV Received\n");
        LdpSsmSendNotifCloseSsn (pSessionEntry,
                                 LDP_STAT_TYPE_MALFORMED_TLV_VAL,
                                 (UINT1 *) pMsg, LDP_TRUE);
        return LDP_FAILURE;
    }

    LDP_EXT_4_BYTES (pu1MsgPtr, u4StatusCode);
    u4StatusData = (u4StatusCode & LDP_STATUS_DATA_MASK);

    /* Get FatalNak bit */
    LDP_GET_STAT_E_BIT (u4StatusCode, u1FatalFlag);
    /* Get ForwardNak bit */
    LDP_GET_STAT_F_BIT (u4StatusCode, u1FwdFlag);

    pEntityList = &LDP_ENTITY_LIST (u2IncarnId);

    /* As per RFC 5036 section 3.4.6 if F bit in the status code is set,
     * the notification SHOULD be forwarded to the LSR for the next-hop 
     * or previous-hop for the LSP
     * */

    if (u1FwdFlag == LDP_TRUE)
    {
        TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
        {
            TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
            {
                if ((pLdpPeer->pLdpSession) == NULL)
                {
                    continue;
                }
                pLdpSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;

                if (pLdpSessionEntry != pSessionEntry)
                {
                    LdpSendNotifMsg (pLdpSessionEntry, NULL, LDP_FALSE,
                                     u4StatusData, NULL);
                }
            }
        }
    }

    /* Extracting the MsgType and MsgId from Status Tlv. */
    LDP_EXT_4_BYTES (pu1MsgPtr, u4MsgId);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgType);

    u2PrcsLen += (LDP_TLV_HDR_LEN + LDP_STATUS_CODE_LEN + LDP_MSG_TYPE_LEN +
                  LDP_MSGID_LEN);

    while (u2PrcsLen < u2MsgLen)
    {

        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);

        switch (u2TlvType)
        {
            case LDP_RETURNED_MSG_TLV:
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgType);
                if (u2MsgType == LDP_LBL_REQUEST_MSG)
                {
                    pu1MsgPtr += (LDP_MSG_ID_LEN + LDP_MSG_TYPE_LEN);
                    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                    LDP_EXT_2_BYTES (pu1MsgPtr, u2FecTlvLen);

                    if (LdpStrToFec
                        (pu1MsgPtr, u2FecTlvLen, &Fec,
                         &u1StatusType) == LDP_FAILURE)
                    {
                        if (u1StatusType == LDP_STAT_TYPE_MALFORMED_TLV_VAL)
                        {
                            LDP_DBG2 (LDP_SSN_PRCS, "%s: %d: Status Type is LDP_STAT_TYPE_MALFORMED_TLV_VAL\n",
                                      __func__, __LINE__);
                            LdpSsmSendNotifCloseSsn (pSessionEntry,
                                                     u1StatusType,
                                                     (UINT1 *) pMsg, LDP_TRUE);
                        }
                        /* The FEC TLV has unknown FEC type or unsupported address family.
                           In that case just send the notification message */
                        else if ((u1StatusType == LDP_STAT_TYPE_UNKNOWN_FEC) ||
                                 (u1StatusType ==
                                  LDP_STAT_TYPE_UNSUPP_ADDR_FMLY))
                        {
                            LdpSendNotifMsg (pSessionEntry, (UINT1 *) pMsg,
                                             LDP_FALSE, u1StatusType, NULL);
                        }

                        LDP_DBG2 (LDP_SSN_PRCS, "%s: %d Returning FAILURE\n", __func__, __LINE__);
                        return LDP_FAILURE;
                    }
                    pu1MsgPtr +=
                        (u2TlvLen -
                         (LDP_MSG_TYPE_LEN + LDP_MSG_ID_LEN +
                          +LDP_MSG_TYPE_LEN + LDP_TLV_TYPE_LEN + u2FecTlvLen));

                }
                else
                {
                    pu1MsgPtr += (u2TlvLen - LDP_MSG_TYPE_LEN);
                }
                u2PrcsLen += (LDP_TLV_HDR_LEN + u2TlvLen);
                break;

            case LDP_LBL_REQ_MSGID_TLV:
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                LDP_EXT_4_BYTES (pu1MsgPtr, u4ReqMsgId);
                u2PrcsLen += (LDP_TLV_HDR_LEN + u2TlvLen);
                break;

            case LDP_RETURNED_PDU_TLV:
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                pu1MsgPtr += u2TlvLen;
                u2PrcsLen += (LDP_TLV_HDR_LEN + u2TlvLen);
                break;

            case LDP_EXTND_STATUS_TLV:
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                pu1MsgPtr += LDP_TLV_HDR_LEN;
                u2PrcsLen += (LDP_TLV_HDR_LEN + u2TlvLen);
                break;

            case CRLDP_LSPID_TLV:
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                pu1MsgPtr += LDP_UINT2_LEN;
                pu1MsgPtr += LDP_UINT2_LEN;
                pu1MsgPtr += LDP_LSR_ID_LEN;
                u2PrcsLen += (LDP_TLV_HDR_LEN + u2TlvLen);
                break;

            case LDP_PW_STATUS_TLV:

                if (u4StatusData == LDP_STAT_PW_STATUS)
                {
                    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                    /* Now the pointer points to the FEC Element TLV */
                    /* Refer the LDP extn - PWE3 CNTRL draft version v17 for this
                     * message structure */
                    pu1MsgPtr += (LDP_PW_STATUS_CODE_LEN +    /* PW Status Code */
                                  LDP_TLV_HDR_LEN);    /* FEC TLV Hdr */

                    /* FEC TLV Contents copied into local FEC Structure */

                    if (LdpStrToFec (pu1MsgPtr, u2TlvLen, &Fec, &u1StatusType)
                        == LDP_FAILURE)
                    {
                        if (u1StatusType == LDP_STAT_TYPE_MALFORMED_TLV_VAL)
                        {
                            LDP_DBG2 (LDP_SSN_PRCS, "%s: %d Status Type is LDP_STAT_TYPE_MALFORMED_TLV_VAL\n",
                                      __func__, __LINE__);
                            LdpSsmSendNotifCloseSsn (pSessionEntry,
                                                     u1StatusType,
                                                     (UINT1 *) pMsg, LDP_TRUE);
                        }
                        /* The FEC TLV has unknown FEC type or unsupported address family.
                           In that case just send the notification message */
                        else if ((u1StatusType == LDP_STAT_TYPE_UNKNOWN_FEC) ||
                                 (u1StatusType ==
                                  LDP_STAT_TYPE_UNSUPP_ADDR_FMLY))
                        {
                            LdpSendNotifMsg (pSessionEntry, (UINT1 *) pMsg,
                                             LDP_FALSE, u1StatusType, NULL);
                        }

                        LDP_DBG2 (LDP_SSN_PRCS, "%s: %d Returning FAILURE\n", __func__, __LINE__);
                        return LDP_FAILURE;
                    }

                    if ((Fec.u1FecElmntType == LDP_FEC_PWVC_TYPE) ||
                        (Fec.u1FecElmntType == LDP_FEC_GEN_PWVC_TYPE))
                    {
                        if (LdpRegStatus (Fec.u1FecElmntType) == LDP_REGISTER)
                        {
                            if (LdpHandleFecMsg (pSessionEntry, pu1Msg,
                                                 LDP_NOTIF_MSG,
                                                 Fec.u1FecElmntType) !=
                                LDP_SUCCESS)
                            { 
                                LDP_DBG2 (LDP_SSN_PRCS, "%s: %d Returning FAILURE\n", __func__, __LINE__);
                                return LDP_FAILURE;
                            }
                            LDP_DBG2 (LDP_SSN_PRCS, "%s: %d Returning SUCCESS\n", __func__, __LINE__);
                            return LDP_SUCCESS;
                        }
                        LDP_DBG2 (LDP_SSN_PRCS, "%s: %d Returning FAILURE\n", __func__, __LINE__);
                        return LDP_FAILURE;
                    }
                    else
                    {
                        LDP_DBG2 (LDP_SSN_PRCS, "%s: %d Returning FAILURE\n", __func__, __LINE__);
                        /* Unknown FEC Element Type */
                        return LDP_FAILURE;
                    }
                }
                break;

            default:
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                pu1MsgPtr += u2TlvLen;
                u2PrcsLen += (LDP_TLV_HDR_LEN + u2TlvLen);
                break;

        }
    }

    /* Updating the Enitity Statistics */

    switch (u4StatusData)
    {
        case LDP_STAT_TYPE_NO_HELLO:
            LDP_ENTITY_UPDATE_SESSREJ_NO_HELLO_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_ADVRT_MODE_REJECT:
            LDP_ENTITY_UPDATE_SESSREJ_ADVRT_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_MAX_PDULEN_REJECT:
            LDP_ENTITY_UPDATE_SESSREJ_MAXPDU_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_LBL_RANGE_REJECT:
            LDP_ENTITY_UPDATE_SESSREJ_LBLRANGE_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_KEEPALIVE_EXPRD:
            LDP_ENTITY_UPDATE_KEEPALIVE_TIMER_EXP_ERR (pSessionEntry);
            break;

        case LDP_STAT_TYPE_SHUT_DOWN:
            LDP_ENTITY_UPDATE_SHUTDOWN_NOTIF_RECVD (pSessionEntry);
            break;
    }

    if (u1FatalFlag == LDP_FALSE)
    {
        if (TmrStopTimer (LDP_TIMER_LIST_ID,
                          (tTmrAppTimer *) & (pSessionEntry->SsnHoldTimer.
                                              AppTimer)) == TMR_FAILURE)
        {
            LDP_DBG1 (LDP_NOTIF_TMR,
                      "NOTIF: Failed to stop the timer inside the function %s\n",
                      __func__);
        }
        if (TmrStartTimer (LDP_TIMER_LIST_ID,
                           (tTmrAppTimer *) & (pSessionEntry->SsnHoldTimer.
                                               AppTimer),
                           (SSN_GET_SSN_HTIME (pSessionEntry) *
                            SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
        {
            LDP_DBG1 (LDP_NOTIF_TMR,
                      "NOTIF: Failed to start the timer inside the function %s\n",
                      __func__);
        }
    }

    LDP_DBG1 (LDP_SSN_PRCS, "LdpHandleNotifMsg: Notification message received: %d\n",
              u4StatusData);

    switch (u4StatusData)
    {

        case LDP_STAT_SUCCESS:
            LDP_DBG (LDP_NOTIF_MISC, "NOTIF: Rx Msg with Success Status\n");
            break;
        case LDP_STAT_TEMPORARY_SHUTDOWN:
            /* Peer has been shutdown temporarily.
             * User Have Issued "shutdown ldp" command in the peer
             */
            if ((gLdpInfo.LdpIncarn[u2Incarn].u1GrCapability !=
                 LDP_GR_CAPABILITY_NONE) &&
                (pSessionEntry->pLdpPeer->u2GrReconnectTime != LDP_ZERO))
            {
                pSessionEntry->pLdpPeer->u1GrProgressState =
                    LDP_PEER_GR_RECONNECT_IN_PROGRESS;
            }
            /* Intentional Fall through */
        case LDP_STAT_BAD_LDPID:
        case LDP_STAT_BAD_PROT_VER:
        case LDP_STAT_BAD_PDU_LEN:
        case LDP_STAT_BAD_MSG_LEN:
        case LDP_STAT_BAD_TLV_LEN:
        case LDP_STAT_CRPTD_TLV_VAL:
        case LDP_STAT_HOLD_TMR_EXPRD:
        case LDP_STAT_SHUT_DOWN:
        case LDP_STAT_NO_HELLO:
        case LDP_STAT_ADVRT_MODE_REJECT:
        case LDP_STAT_MAX_PDULEN_REJECT:
        case LDP_STAT_LBL_RANGE_REJECT:
        case LDP_STAT_KEEPALIVE_EXPRD:
        case LDP_STAT_BAD_KEEPALIVE_TMR:
        case LDP_STAT_INTERNAL_ERROR:

            pLdpAdjList = &(pSessionEntry->AdjacencyList);
            u4TmpConnId=pSessionEntry->u4TcpConnId;
			
            TMO_DYN_SLL_Scan (pLdpAdjList, pLdpAdj,
                              pTempLdpAdj, tLdpAdjacency *)
            {
                
                if (LdpHandleAdjExpiry (pLdpAdj, LDP_STAT_TYPE_SHUT_DOWN) ==
                    LDP_FAILURE)
                {
                    continue;
                }

		 /* Check if Session is deleted in handling of LdpHandleAdjExpiry*/	
		  if( NULL == LdpGetSsnFromTcpConnTbl(u2IncarnId, u4TmpConnId))
		  {
		       LDP_DBG (LDP_SSN_PRCS,
                     "LdpHandleNotifMsg: Session entry deleted.\n");
			 break;  
		  }
            }
            break;

        case LDP_STAT_UNKNOWN_MSG_TYPE:
            LDP_DBG (LDP_NOTIF_MISC,
                     "NOTIF: Rx Msg with UnknownMsgType Status\n");
            break;

        case LDP_STAT_UNKNOWN_TLV:
            LDP_DBG (LDP_NOTIF_MISC,
                     "NOTIF: Rx Msg with UnknownTlvType Status\n");
            break;

        case LDP_STAT_BAD_EROUTE_TLV_ERR:
        case LDP_STAT_BAD_STRICT_NODE_ERR:
        case LDP_STAT_BAD_LOOSE_NODE_ERR:
        case LDP_STAT_BAD_INIT_ER_HOP_ERR:
        case LDP_STAT_RESOURCE_UNAVAIL:
        case LDP_STAT_TRAF_PARMS_UNAVL:
        case LDP_STAT_CRLSP_SETUP_ABORT:
        case LDP_STAT_CRLSP_MODIFY_NOTSUP:
        case LDP_STAT_CRLSP_PREEMPTED:

            if (u2MsgType == LDP_LBL_REQUEST_MSG)
            {
                /* Received the Notif Message for the Req, from Downstream */
                u2MsgType = LDP_NOTIF_FOR_LBL_REQ;
            }
            pLspCtrlBlock =
                LdpGetLspCtrlBlock (pSessionEntry, u2MsgType, u4MsgId, Fec);
            if (pLspCtrlBlock == NULL)
            {
                /* 
                 * Failed to identify the LCB to  which the Notification Msg is
                 * refering to. 
                 */
                LDP_DBG2 (LDP_SSN_PRCS, "%s: %d Returning FAILURE\n", __func__, __LINE__);
                return LDP_FAILURE;
            }

            for (u1ErrTypeIndex = 0; u1ErrTypeIndex < LDP_MAX_STATUS_TYPES;
                 u1ErrTypeIndex++)
            {
                if ((u4StatusData & LDP_NOTIF_STATUS_MASK) ==
                    (LdpConvertLocalValToTLVVal (u1ErrTypeIndex) &
                     LDP_NOTIF_STATUS_MASK))
                {
                    if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
                        (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
                    {
                        LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlock)]
                            [LDP_LSM_EVT_DN_NAK] (pLspCtrlBlock,
                                                  &u1ErrTypeIndex);
                        break;
                    }
                    else
                    {
                        pMsg->pu1Msg = &u1ErrTypeIndex;
                        LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                        LDP_LSM_EVT_DN_NAK, pMsg, LDP_FALSE);
                    }
                }
            }
            break;
        case LDP_DS_STAT_UNEXPECTED_DIFFSERV_TLV:
            /*It is used when downstream unsolicited mode is supported */
        case LDP_DS_STAT_UNSUPPORTED_PHB:
        case LDP_DS_STAT_INVALID_EXP_PHB_MAPPING:
        case LDP_DS_STAT_UNSUPPORTED_PSC:
        case LDP_DS_STAT_PER_LSPCONTEXT_ALLOCFAIL:
        case LDP_DS_STAT_UNSUPPORTED_CLASS_TYPE_TLV:
        case LDP_DS_STAT_INVALID_CLASS_TYPE_VALUE:
        case LDP_DS_STAT_ELSPTP_UNSUPPORTED_PSC:
        case LDP_DS_STAT_ELSPTP_UNAVL_OARSRC:
        case LDP_DS_STAT_UNEXP_CLASS_TYPE_TLV:

            if (u2MsgType == LDP_LBL_REQUEST_MSG)
            {
                /* Received the Notif Message for the Req, from Downstream */
                u2MsgType = LDP_NOTIF_FOR_LBL_REQ;
            }

            pLspCtrlBlock =
                LdpGetLspCtrlBlock (pSessionEntry, u2MsgType, u4MsgId, Fec);
            if (pLspCtrlBlock == NULL)
            {
                /* 
                 * Failed to identify the LCB to  which the Notification Msg is
                 * refering to. 
                 */
                LDP_DBG2 (LDP_SSN_PRCS, "%s: %d Returning FAILURE\n", __func__, __LINE__);
                return LDP_DS_FAILURE;
            }
            for (u1ErrTypeIndex = 0; u1ErrTypeIndex < LDP_DS_MAX_STATUS_TYPES;
                 u1ErrTypeIndex++)
            {
                if ((u4StatusData & LDP_NOTIF_STATUS_MASK) ==
                    (au4DiffServStatusData[u1ErrTypeIndex] &
                     LDP_NOTIF_STATUS_MASK))
                {
                    u1DSErrTypeIndex =
                        (UINT1) (u1ErrTypeIndex + LDP_MAX_STATUS_TYPES);
                    if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND)
                        && (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
                    {
                        LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlock)]
                            [LDP_LSM_EVT_DN_NAK] (pLspCtrlBlock,
                                                  &u1DSErrTypeIndex);
                    }
                    break;
                }
            }
            break;

        case LDP_STAT_UNKNOWN_FEC:
            LDP_DBG (LDP_NOTIF_MISC, "NOTIF: Rx Msg with UnknownFec Status\n");
            break;

        case LDP_STAT_LOOP_DETECTED:
        case LDP_STAT_NO_ROUTE:
            /* When received No route from downstream, event for new next hop is
             * generated */
            if (u2MsgType == LDP_LBL_REQUEST_MSG)
            {
                /* Received the Notif Message for the Req, from Downstream */
                u2MsgType = LDP_NOTIF_FOR_LBL_REQ;
            }

            pLspCtrlBlock =
                LdpGetLspCtrlBlock (pSessionEntry, u2MsgType, u4MsgId, Fec);
            if (pLspCtrlBlock == NULL)
            {
                /* 
                 * Failed to identify the LCB to  which the Notification Msg is
                 * refering to. 
                 */
                LDP_DBG (LDP_NOTIF_MISC, "NOTIF: Failed to identify LCB\n");
                return LDP_FAILURE;
            }

            pLspCtrlBlock->u1LblReqFwdFlag = LDP_FALSE;

            if (gu4LdpLspPersist == FALSE)
            {
                if (pLspCtrlBlock->pTrigCtrlBlk != NULL)
                {
                    /* Since a response is given by a peer the tardy timer is 
                     * stopped. 
                     */
                    if (TmrStopTimer (LDP_TIMER_LIST_ID,
                                      (tTmrAppTimer *) & (pLspCtrlBlock->
                                                          TardyTimer.
                                                          AppTimer)) ==
                        TMR_FAILURE)
                    {
                        LDP_DBG1 (LDP_NOTIF_TMR,
                                  "NOTIF: Failed to stop the timer inside the function %s\n",
                                  __func__);
                    }
                }
            }
            if (gu4LdpLspPersist == TRUE)
            {
                /* As per rfc 5036, Section A.1.10 when an LSR receives a No route 
                 * notification from an LDP peer, it either take no further action
                 * or it will defer the label request by starting a timer and 
                 * send another Label Request message to the peer when the timer
                 * later expires */
                pLspCtrlBlock->PerstTimer.u4Event = LDP_PERST_EXPIRED_EVENT;
                pLspCtrlBlock->PerstTimer.pu1EventInfo =
                    (UINT1 *) pLspCtrlBlock;
                if (TmrStartTimer (LDP_TIMER_LIST_ID,
                                   (tTmrAppTimer *) & (pLspCtrlBlock->
                                                       PerstTimer.AppTimer),
                                   (LDP_PERS_TIME_PERD *
                                    SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) ==
                    TMR_FAILURE)
                {
                    LDP_DBG1 (LDP_NOTIF_TMR,
                              "NOTIF: Failed to start the timer inside the function %s\n",
                              __func__);
                }
                /* A Retry is done on Lsp Establishment, for the same NextHop 
                 * and DStr Ssn, for which this LCB has been initialised to. 
                 * If at all there is a change in the NextHop, an Event would 
                 * be generated by the Routing Protocol(Ospf), indicating a 
                 * NH Change. When such an event is generated then, a NH Retry 
                 * would be done on this LCB.
                 */
                LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_IDLE;

            }
            else
            {
                /* 
                 * Invoke the Advert SEM, in case of 
                 *   - Intermediate Always 
                 *   - Ingress, only when No 'Persist Lsp' is required. 
                 */
                u1StatusType = LDP_STAT_TYPE_NO_ROUTE;
                if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
                    (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
                {
                    LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlock)]
                        [LDP_LSM_EVT_DN_NAK] (pLspCtrlBlock,
                                              (UINT1 *) &u1StatusType);
                }
                else
                {
                    pMsg->pu1Msg = &u1StatusType;
                    LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                    LDP_LSM_EVT_DN_NAK, pMsg, LDP_FALSE);
                }
            }
            break;

        case LDP_STAT_NO_LBL_RSRC:

            pSessionEntry->u1StatusRecord = LDP_STAT_TYPE_NO_LBL_RSRC;    /* Look at LLD */
            /* When received No Label Resource from downstream the corresponding
             * Non Mrg FSM is invoked.
             */
            if (u2MsgType == LDP_LBL_REQUEST_MSG)
            {
                /* Received the Notif Message for the Req, from Downstream */
                u2MsgType = LDP_NOTIF_FOR_LBL_REQ;
            }

            pLspCtrlBlock =
                LdpGetLspCtrlBlock (pSessionEntry, u2MsgType, u4MsgId, Fec);
            if (pLspCtrlBlock == NULL)
            {
                /* 
                 * Failed to identify the LCB to  which the Notification Msg is
                 * refering to. 
                 */
                LDP_DBG (LDP_NOTIF_MISC, "NOTIF: Failed to identify LCB\n");
                return LDP_FAILURE;
            }

            u1StatusType = LDP_STAT_TYPE_NO_ROUTE;
            if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
                (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
            {
                LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlock)]
                    [LDP_LSM_EVT_DN_NAK] (pLspCtrlBlock,
                                          (UINT1 *) &u1StatusType);
            }
            else
            {
                pMsg->pu1Msg = &u1StatusType;
                LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                LDP_LSM_EVT_DN_NAK, pMsg, LDP_FALSE);
            }
            break;

        case LDP_STAT_LBL_RSRC_AVBL:
            pSessionEntry->u1StatusRecord = LDP_STAT_TYPE_LBL_RSRC_AVBL;
            if (u2MsgType == LDP_LBL_REQUEST_MSG)
            {
                /* Received the Notif Message for the Req, from Downstream */
                u2MsgType = LDP_NOTIF_FOR_LBL_REQ;
            }
            pLspCtrlBlock =
                LdpGetLspCtrlBlock (pSessionEntry, u2MsgType, u4MsgId, Fec);
            if (pLspCtrlBlock == NULL)
            {
                /*
                 * Failed to identify the LCB to  which the Notification Msg is
                 * refering to.
                 */
                LDP_DBG (LDP_NOTIF_MISC, "NOTIF: Failed to identify LCB\n");
                return LDP_FAILURE;
            }
            u1StatusType = LDP_STAT_TYPE_SUCCESS;
            pMsg->pu1Msg = &u1StatusType;

            LdpSendLblReqMsg (pLspCtrlBlock, LDP_ZERO, NULL, LDP_ZERO, NULL,
                              LDP_ZERO);

            break;

        case LDP_STAT_LBL_REQ_ABRTD:

            pLspCtrlBlock =
                LdpGetLspCtrlBlock (pSessionEntry, LDP_NOTIF_FOR_LBL_REQ_ABORT,
                                    u4ReqMsgId, Fec);
            if (pLspCtrlBlock == NULL)
            {
                /* 
                 * Failed to identify the LCB to  which the Notification Msg is
                 * refering to. 
                 */
                LDP_DBG (LDP_NOTIF_MISC, "NOTIF: Failed to identify LCB\n");
                return LDP_FAILURE;
            }
            LDP_DBG (LDP_NOTIF_MISC,
                     "NOTIF: Rx Msg with LblReqAborted Status\n");
            if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
                (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
            {
                LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlock)][LDP_LSM_EVT_DN_NAK]
                    (pLspCtrlBlock, (UINT1 *) &u4StatusData);
            }
            else
            {
                pMsg->pu1Msg = (UINT1 *) &u4StatusData;
                LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                LDP_LSM_EVT_DN_NAK, pMsg, LDP_FALSE);
            }
            break;

        case LDP_STAT_MISSING_MSG_PARAM:
            LDP_DBG (LDP_NOTIF_MISC,
                     "NOTIF: Rx Msg with MissingMsgPara Status\n");
            break;

        case LDP_STAT_UNSUPP_ADDR_FMLY:
            LDP_DBG (LDP_NOTIF_MISC,
                     "NOTIF: Rx Msg with UnsuppAddrFmly Status\n");
            break;
        default:
            /*  First Tlv is not StatusTlv(Mandatory) - Error */
            LDP_DBG (LDP_PRCS_PRCS, "PRCS: Malformed TLV.\n");
            LdpSsmSendNotifCloseSsn (pSessionEntry,
                                     LDP_STAT_TYPE_MALFORMED_TLV_VAL,
                                     (UINT1 *) pMsg, LDP_TRUE);
            return LDP_FAILURE;

    }
    KW_FALSEPOSITIVE_FIX (pLspCtrlBlock);

    LDP_DBG (LDP_SSN_PRCS, "LdpHandleNotifMsg: EXIT\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendCrLspNotifMsg                                      */
/* Description   : This routine constucts and sends the Notification Message.*/
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session entry on which this */
/*                                 Notif Message has to be sent.             */
/*                 pLspCtrlBlock   - Points to the actual Lsp Control Block  */
/*                                 which resulted in this Notification Msg   */
/*                                 generation.                               */
/*                 u1StatusType  - Indicates the event being signalled.      */
/*                                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS     if Notif Message is constructed and sent  */
/*                 successfully, else LDP_FAILURE is returned                */
/*****************************************************************************/

UINT1
LdpSendCrLspNotifMsg (tLdpSession * pSessionEntry,
                      tLspCtrlBlock * pLspCtrlBlock,
                      UINT1 u1StatusType, UINT1 *pu1Msg)
{
    UINT2               u2OptParamLen = 0;
    UINT2               u2MsgLen = 0;
    UINT4               u4Offset = 0;
    UINT2               u2MandParamLen;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tCrlspTnlInfo      *pCrLsp = NULL;

    UINT1               u1StatusTlvLen =
        (LDP_TLV_HDR_LEN + LDP_STATUS_CODE_LEN + LDP_MSG_ID_LEN +
         LDP_MSG_TYPE_LEN);
    tLdpMsgHdrAndId     MsgHdrAndId = { OSIX_HTONS (LDP_NOTIF_MSG), 0, 0 };
    tStatusTlv          StatusTlv = { {OSIX_HTONS (LDP_STATUS_TLV),
                                       OSIX_HTONS ((LDP_STATUS_CODE_LEN +
                                                    LDP_MSG_ID_LEN +
                                                    LDP_MSG_TYPE_LEN))}, 0, 0,
    0, 0
    };
    tCrlspLspIdTlv      LspIdTlv;
    tLdpId              PeerLdpId;
    /*MPLS_IPv6 add start*/
    tGenAddr     ConnId;
    tGenU4Addr     LdpIfAddrZero;
    /*MPLS_IPv6 add end*/
    UINT2               u2MsgType;
    /*MPLS_IPv6 add start*/
    MEMSET(&ConnId, LDP_ZERO, sizeof(tGenAddr));
    MEMSET(&LdpIfAddrZero, LDP_ZERO, sizeof(tGenU4Addr));    
    /*MPLS_IPv6 add end*/
    LspIdTlv.TlvHdr.u2TlvType = OSIX_HTONS (CRLDP_LSPID_TLV);
    LspIdTlv.TlvHdr.u2TlvLen = OSIX_HTONS (LDP_LSPID_TLV_LEN);

    u2MandParamLen = (UINT2) (LDP_MSG_ID_LEN + u1StatusTlvLen);
    u2OptParamLen = LDP_TLV_HDR_LEN + LDP_LSPID_TLV_LEN;
    u2MsgLen = (UINT2) (u2MandParamLen + u2OptParamLen);
    pMsg =
        CRU_BUF_Allocate_MsgBufChain ((LDP_PDU_HDR_LEN + LDP_MSG_HDR_LEN +
                                       u2MsgLen), LDP_PDU_HDR_LEN);

    if (pMsg == NULL)
    {
        LDP_DBG (LDP_NOTIF_MEM,
                 "NOTIF: Buffer Allocation for Notif Msg failed\n");
        return LDP_FAILURE;
    }

    MsgHdrAndId.u2MsgLen = OSIX_HTONS (u2MsgLen);
    MsgHdrAndId.u4MsgId = OSIX_HTONL (SSN_GET_MSGID (pSessionEntry));

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &MsgHdrAndId, u4Offset,
                                   (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN)) ==
        CRU_FAILURE)
    {
        LDP_DBG (LDP_NOTIF_MEM,
                 "NOTIF: Error in copying Notif MsgHdr and MsgId "
                 "to Buf chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Construct Status Tlv */
    StatusTlv.u4StatusCode =
        OSIX_HTONL (((LdpConvertLocalValToTLVVal (u1StatusType)) |
                     LDP_STAT_F_BIT));

    /* MsgId and Type are by default assigned to zero. 
     * In other cases, MsgID and Type are extracted from pu1Msg or
     * from statusType.
     */

    if (pu1Msg != NULL)
    {
        /* Get Msg Type from pu1Msg */
        StatusTlv.u2MsgType = *((UINT2 *) (VOID *) pu1Msg);
        /* Get MsgId */
        StatusTlv.u4MsgId = *((UINT4 *) (VOID *) (pu1Msg + LDP_MSG_HDR_LEN));
    }
    else
    {
        switch (u1StatusType)
        {
            case LDP_STAT_TYPE_LOOP_DETECTED:
            case LDP_STAT_TYPE_BAD_EROUTE_TLV_ERR:
            case LDP_STAT_TYPE_BAD_STRICT_NODE_ERR:
            case LDP_STAT_TYPE_BAD_LOOSE_NODE_ERR:
            case LDP_STAT_TYPE_BAD_INIT_ER_HOP_ERR:
            case LDP_STAT_TYPE_RESOURCE_UNAVAIL:
            case LDP_STAT_TYPE_TRAF_PARMS_UNAVL:
            case LDP_STAT_TYPE_CRLSP_PREEMPTED:
            case LDP_STAT_TYPE_NO_ROUTE:
            case LDP_STAT_TYPE_CRLSP_MODIFY_NOTSUP:
                u2MsgType = LDP_LBL_REQUEST_MSG;
                StatusTlv.u2MsgType = OSIX_HTONS (u2MsgType);
                StatusTlv.u4MsgId = OSIX_HTONL (pLspCtrlBlock->u4UStrLblReqId);
                break;

            default:
                /* Assign MsgId and MsgType to default zero. */
                StatusTlv.u2MsgType = 0;
                StatusTlv.u4MsgId = 0;
                break;
        }
    }

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &StatusTlv, u4Offset,
                                   u1StatusTlvLen) == CRU_FAILURE)
    {
        LDP_DBG (LDP_NOTIF_MEM,
                 "NOTIF: Error while copying Status Tlv to buffer\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += u1StatusTlvLen;

    /* Constructing and copying LspId Tlv */
    pCrLsp = pLspCtrlBlock->pCrlspTnlInfo;
    if (CRLSP_IS_MOD_OF_TUNN (pCrLsp) == LDP_TRUE)
    {
        LspIdTlv.u2Reserved = (LDP_RSVD_FLD | CRLSP_ACT_FLAG);
    }
    else
    {
        LspIdTlv.u2Reserved = 0;
    }
    LspIdTlv.u2LocalLspid = OSIX_HTONS (pCrLsp->u2LocalLspid);
    MEMCPY (LspIdTlv.IngressLsrRtrId, CRLSP_INGRESS_LSRID (pCrLsp),
            LDP_IPV4ADR_LEN);

    if (CRU_BUF_Copy_OverBufChain (pMsg, (UINT1 *) &LspIdTlv, u4Offset,
                                   (LDP_TLV_HDR_LEN + LDP_LSPID_TLV_LEN))
        == CRU_FAILURE)
    {
        LDP_DBG (LDP_NOTIF_MEM,
                 "NOTIF: Error while copying LspId Tlv to buffer chain\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    u4Offset += (LDP_TLV_HDR_LEN + LDP_LSPID_TLV_LEN);

    /* Moving the offset to PDU Header start */
    CRU_BUF_Prepend_BufChain (pMsg, NULL, LDP_PDU_HDR_LEN);
    SSN_GET_PEERID (PeerLdpId, pSessionEntry);
    LDP_DBG4 (LDP_NOTIF_TX,
              "NOTIF: Sending Notif Msg To %d.%d.%d.%d \n",
              PeerLdpId[0], PeerLdpId[1], PeerLdpId[2], PeerLdpId[3]);

    /*MPLS_IPv6 add start*/
    MEMCPY(LDP_IPV4_ADDR(ConnId.Addr) , &(pSessionEntry->u4TcpConnId), LDP_IPV4ADR_LEN);
    if (LdpSendMsg (pMsg, u2MsgLen, SSN_GET_ENTITY (pSessionEntry), LDP_FALSE,
                    ConnId, LdpIfAddrZero, LDP_ZERO) != LDP_SUCCESS)
    {
        LDP_DBG (LDP_NOTIF_TX, "NOTIF: Failed to send Notif Msg\n");
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
        return LDP_FAILURE;
    }
    switch (u1StatusType)
    {
        case LDP_STAT_BAD_TLV_LEN:
            LDP_ENTITY_UPDATE_BAD_TLV_LEN_ERR (pSessionEntry);
            break;

        case LDP_STAT_UNKNOWN_TLV:
            LDP_ENTITY_UPDATE_MALFORMED_TLV_ERR (pSessionEntry);
            break;
    }
    return LDP_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpnotif.c                              */
/*---------------------------------------------------------------------------*/
