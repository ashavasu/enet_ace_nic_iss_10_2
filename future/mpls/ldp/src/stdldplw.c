/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdldplw.c,v 1.49 2018/01/03 11:31:21 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include  "lr.h"
# include  "cfa.h"
# include  "fssnmp.h"
# include  "stdldplw.h"
# include  "ldpincs.h"
# include  "ldplwinc.h"
# include   "inmgrex.h"
# include   "indexmgr.h"
# include   "mplscli.h"
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpLsrId
 Input       :  The Indices

                The Object 
                retValMplsLdpLsrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpLsrId (tSNMP_OCTET_STRING_TYPE * pRetValMplsLdpLsrId)
{
    CPY_TO_SNMP (pRetValMplsLdpLsrId,
                 (LDP_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr), LDP_IPV4ADR_LEN);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpLsrLoopDetectionCapable
 Input       :  The Indices

                The Object 
                retValMplsLdpLsrLoopDetectionCapable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpLsrLoopDetectionCapable (INT4
                                      *pi4RetValMplsLdpLsrLoopDetectionCapable)
{
    *pi4RetValMplsLdpLsrLoopDetectionCapable =
        (INT4) LDP_LOOP_DETECT (MPLS_DEF_INCARN);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityLastChange
 Input       :  The Indices

                The Object 
                retValMplsLdpEntityLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityLastChange (UINT4 *pu4RetValMplsLdpEntityLastChange)
{
    *pu4RetValMplsLdpEntityLastChange = MplsLdpEntityGetSysTime ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityIndexNext
 Input       :  The Indices

                The Object 
                retValMplsLdpEntityIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityIndexNext (UINT4 *pu4RetValMplsLdpEntityIndexNext)
{
    *pu4RetValMplsLdpEntityIndexNext = LdpEntityTableGetIndex ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsLdpEntityTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpEntityTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpEntityTable (tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);

    if (LDP_INCARN_ADMIN_STATUS (MPLS_DEF_INCARN) != LDP_ADMIN_UP)
    {
        return SNMP_FAILURE;
    }
    if (u4MplsLdpEntityIndex > LDP_UINT2_SIZE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpEntityTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpEntityTable (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 *pu4MplsLdpEntityIndex)
{
    UINT4               u4IncarnId;
    UINT1               u1FirstLoopEntry = LDP_TRUE;
    tLdpEntity         *pLdpEntity = NULL;
    tTMO_SLL           *pList = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    *pu4MplsLdpEntityIndex = LDP_ZERO;
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    for (u4IncarnId = 0; u4IncarnId < LDP_MAX_INCARN; ++u4IncarnId)
    {
        pList = &LDP_ENTITY_LIST (u4IncarnId);

        pLdpEntity = (tLdpEntity *) TMO_SLL_First (pList);

        if (pLdpEntity == NULL)
        {
            continue;
        }

        TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
        {
            /* Only PER PLATFORM LDP Entities will be fetched */
            if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_PLATFORM)
            {
                continue;
            }

            /* Fetching the First 'valid' First LDP Entity */
            if (u1FirstLoopEntry == LDP_TRUE)
            {
                CPY_TO_SNMP (pMplsLdpEntityLdpId, pLdpEntity->LdpId,
                             LDP_MAX_LDPID_LEN);
                *pu4MplsLdpEntityIndex = (pLdpEntity->u4EntityIndex);
                u1FirstLoopEntry = LDP_FALSE;
                continue;
            }

            /* Check whether Fetched Entity has a lower or equal LDP Entity ID 
             * value than the previous 'valid' First LDP Entity */
            if (MEMCMP
                (pLdpEntity->LdpId, pu1LdpEntityId, LDP_MAX_LDPID_LEN) > 0)
            {
                continue;
            }

            /* Check whether Fetched Entity has a lower LDP Entity Index value
             * than the previous 'valid' First LDP Entity */
            if ((MEMCMP (pLdpEntity->LdpId, pu1LdpEntityId,
                         LDP_MAX_LDPID_LEN) == 0) &&
                (pLdpEntity->u4EntityIndex > *pu4MplsLdpEntityIndex))
            {
                continue;
            }

            /* Next Possible 'valid' First LDP Entity is found */
            CPY_TO_SNMP (pMplsLdpEntityLdpId, pLdpEntity->LdpId,
                         LDP_MAX_LDPID_LEN);
            *pu4MplsLdpEntityIndex = (pLdpEntity->u4EntityIndex);
        }
    }

    if (u1FirstLoopEntry == LDP_FALSE)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpEntityTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpEntityTable (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   UINT4 *pu4NextMplsLdpEntityIndex)
{
    UINT1               u1FirstLoopEntry = LDP_TRUE;
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1NextLdpEntityId = NULL;

    *pu4NextMplsLdpEntityIndex = LDP_ZERO;

    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (pMplsLdpEntityLdpId->i4_Length < 0)
    {
        return SNMP_FAILURE;
    }

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1NextLdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);
    pList = &LDP_ENTITY_LIST (MPLS_DEF_INCARN);

    /* The LDP Entity Passed as an Input does not have any more 
     * Ethernet Parameters associated with it. So, trying to fetch the
     * Next Maximum Possible Valid LDP Entity with ethernet parameters 
     * associated with it */
    TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
    {
        /* Only PER PLATFORM LDP Entities will be fetched */
        if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_PLATFORM)
        {
            continue;
        }

        /* Fetched LDP Entity should have LDP Entity ID Value equal or
         * greater than the LDP Entity Passed as Input to this routine */
        if (MEMCMP (pLdpEntity->LdpId, pu1LdpEntityId, LDP_MAX_LDPID_LEN) < 0)
        {
            continue;
        }

        /* Fetched LDP Entity should have LDP Entity Index Value 
         * greater than the LDP Entity Passed as Input to this routine */
        if ((MEMCMP (pLdpEntity->LdpId, pu1LdpEntityId,
                     LDP_MAX_LDPID_LEN) == 0) &&
            (pLdpEntity->u4EntityIndex <= u4MplsLdpEntityIndex))
        {
            continue;
        }

        if (u1FirstLoopEntry == LDP_TRUE)
        {
            /* Fetching the First 'valid' First LDP Entity */
            CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pLdpEntity->LdpId,
                         LDP_MAX_LDPID_LEN);
            *pu4NextMplsLdpEntityIndex = pLdpEntity->u4EntityIndex;
            u1FirstLoopEntry = LDP_FALSE;
            continue;
        }

        /* Fetched LDP Entity should have LDP Entity ID Value equal or 
         * lesser than the 'valid' First LDP Entity */
        if (MEMCMP (pLdpEntity->LdpId, pu1NextLdpEntityId,
                    LDP_MAX_LDPID_LEN) > 0)
        {
            continue;
        }

        /* Fetched LDP Entity should have LDP Entity Index Value 
         * lesser than the 'valid' First LDP Entity */
        if ((MEMCMP (pLdpEntity->LdpId, pu1NextLdpEntityId,
                     LDP_MAX_LDPID_LEN) == 0)
            && (pLdpEntity->u4EntityIndex > *pu4NextMplsLdpEntityIndex))
        {
            continue;
        }

        /* The Next Possible 'valid' Next LDP Entity is found */
        CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pLdpEntity->LdpId,
                     LDP_MAX_LDPID_LEN);
        *pu4NextMplsLdpEntityIndex = pLdpEntity->u4EntityIndex;
    }

    if (u1FirstLoopEntry == LDP_FALSE)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityProtocolVersion
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityProtocolVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityProtocolVersion (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    UINT4
                                    *pu4RetValMplsLdpEntityProtocolVersion)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityProtocolVersion =
        (UINT4) LDP_ENTITY_PROT_VERSION (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAdminStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAdminStatus (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 *pi4RetValMplsLdpEntityAdminStatus)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityAdminStatus = (INT4) (pLdpEntity->u1AdminStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityOperStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityOperStatus (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                               UINT4 u4MplsLdpEntityIndex,
                               INT4 *pi4RetValMplsLdpEntityOperStatus)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityOperStatus = (INT4) (pLdpEntity->u1OperStatus);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityTcpPort
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityTcpPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityTcpPort (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                            UINT4 u4MplsLdpEntityIndex,
                            UINT4 *pu4RetValMplsLdpEntityTcpPort)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityTcpPort = (UINT4) (pLdpEntity->u2TcpPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityUdpDscPort
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityUdpDscPort
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityUdpDscPort (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                               UINT4 u4MplsLdpEntityIndex,
                               UINT4 *pu4RetValMplsLdpEntityUdpDscPort)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityUdpDscPort = (UINT4) (pLdpEntity->u2UdpPort);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityMaxPduLength
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityMaxPduLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityMaxPduLength (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                 UINT4 u4MplsLdpEntityIndex,
                                 UINT4 *pu4RetValMplsLdpEntityMaxPduLength)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityMaxPduLength = (UINT4) (pLdpEntity->u2MaxPduLen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityKeepAliveHoldTimer
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityKeepAliveHoldTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityKeepAliveHoldTimer (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       *pu4RetValMplsLdpEntityKeepAliveHoldTimer)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityKeepAliveHoldTimer = pLdpEntity->u4KeepAliveHoldTime;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityHelloHoldTimer
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityHelloHoldTimer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityHelloHoldTimer (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   UINT4 *pu4RetValMplsLdpEntityHelloHoldTimer)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityHelloHoldTimer =
        (UINT4) (pLdpEntity->u2HelloConfHoldTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityInitSessionThreshold
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityInitSessionThreshold
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityInitSessionThreshold (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         *pi4RetValMplsLdpEntityInitSessionThreshold)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityInitSessionThreshold =
        (INT4) (pLdpEntity->u4FailInitSessThreshold);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityLabelDistMethod
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityLabelDistMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityLabelDistMethod (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    INT4 *pi4RetValMplsLdpEntityLabelDistMethod)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityLabelDistMethod =
        (INT4) (pLdpEntity->u1LabelDistrType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityLabelRetentionMode
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityLabelRetentionMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityLabelRetentionMode (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       *pi4RetValMplsLdpEntityLabelRetentionMode)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityLabelRetentionMode = (INT4) (pLdpEntity->u2LabelRet);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityPathVectorLimit
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityPathVectorLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityPathVectorLimit (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    INT4 *pi4RetValMplsLdpEntityPathVectorLimit)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityPathVectorLimit =
        (INT4) (pLdpEntity->u1PathVecLimit);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityHopCountLimit
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityHopCountLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityHopCountLimit (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                  UINT4 u4MplsLdpEntityIndex,
                                  INT4 *pi4RetValMplsLdpEntityHopCountLimit)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityHopCountLimit = (INT4) (pLdpEntity->u2HopVecLimit);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityTransportAddrKind
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityTransportAddrKind
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityTransportAddrKind (tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 u4MplsLdpEntityIndex,
                                      INT4
                                      *pi4RetValMplsLdpEntityTransportAddrKind)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityTransportAddrKind = (INT4)
        LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityTargetPeer
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityTargetPeer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityTargetPeer (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                               UINT4 u4MplsLdpEntityIndex,
                               INT4 *pi4RetValMplsLdpEntityTargetPeer)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityTargetPeer =
        (INT4) ((pLdpEntity->u1TargetFlag ==
                 LDP_TRUE) ? LDP_SNMP_TRUE : LDP_SNMP_FALSE);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityTargetPeerAddrType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityTargetPeerAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityTargetPeerAddrType (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       *pi4RetValMplsLdpEntityTargetPeerAddrType)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((pLdpEntity->u1TargetFlag) != LDP_TRUE)
    {
        *pi4RetValMplsLdpEntityTargetPeerAddrType = ADDR_OTHER;
        return SNMP_SUCCESS;
    }
    *pi4RetValMplsLdpEntityTargetPeerAddrType =
        (INT4) (pLdpEntity->NetAddrType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityTargetPeerAddr
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityTargetPeerAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityTargetPeerAddr (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValMplsLdpEntityTargetPeerAddr)
{
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));

    if ((pLdpPeer == NULL) || (pLdpEntity->u1TargetFlag != LDP_TRUE))
    {
        pRetValMplsLdpEntityTargetPeerAddr->i4_Length = 0;
    }
    else
    {
#ifdef MPLS_IPV6_WANTED
        if (pLdpPeer->NetAddr.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            MEMCPY (pRetValMplsLdpEntityTargetPeerAddr->pu1_OctetList,
                    LDP_IPV6_ADDR (pLdpPeer->NetAddr.Addr), IPV6_ADDR_LENGTH);

            pRetValMplsLdpEntityTargetPeerAddr->i4_Length = IPV6_ADDR_LENGTH;
        }
        else
        {
#endif
            MEMCPY (pRetValMplsLdpEntityTargetPeerAddr->pu1_OctetList,
                    LDP_IPV4_ADDR (pLdpPeer->NetAddr.Addr), IPV4_ADDR_LENGTH);

            pRetValMplsLdpEntityTargetPeerAddr->i4_Length = IPV4_ADDR_LENGTH;

#ifdef MPLS_IPV6_WANTED
        }
#endif
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityLabelType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityLabelType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityLabelType (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                              UINT4 u4MplsLdpEntityIndex,
                              INT4 *pi4RetValMplsLdpEntityLabelType)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* This variable u1EntityType is right now used to assign the type
     *  of Entity */
    *pi4RetValMplsLdpEntityLabelType = (INT4) (pLdpEntity->u1EntityType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityDiscontinuityTime
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityDiscontinuityTime (tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 u4MplsLdpEntityIndex,
                                      UINT4
                                      *pu4RetValMplsLdpEntityDiscontinuityTime)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityDiscontinuityTime =
        (UINT4) (pLdpEntity->u1DisContTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStorageType (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 *pi4RetValMplsLdpEntityStorageType)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityStorageType = (INT4) (pLdpEntity->u1StorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                              UINT4 u4MplsLdpEntityIndex,
                              INT4 *pi4RetValMplsLdpEntityRowStatus)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityRowStatus =
        (INT4) LDP_ENTITY_ROW_STATUS (pLdpEntity);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityProtocolVersion
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityProtocolVersion
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityProtocolVersion (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    UINT4 u4SetValMplsLdpEntityProtocolVersion)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        (pLdpEntity->u2ProtVersion) =
            (UINT2) u4SetValMplsLdpEntityProtocolVersion;
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         *  NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAdminStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAdminStatus (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 i4SetValMplsLdpEntityAdminStatus)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEntity         *pOtherLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_ENTITY_ADMIN_STATUS (pLdpEntity) ==
        i4SetValMplsLdpEntityAdminStatus)
    {
        return SNMP_SUCCESS;
    }
    if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
    {
        /* Delete the session asociated with this entity. */
        if (i4SetValMplsLdpEntityAdminStatus == LDP_ADMIN_DOWN)
        {
            pOtherLdpEntity = LdpGetOtherEntityForActivation (pLdpEntity);

            LdpSNMPEventHandler (LDP_ENTITY_NOTINSERVICE,
                                 pLdpEntity, MPLS_DEF_INCARN);
            if (pOtherLdpEntity != NULL)
            {
                LdpSNMPEventHandler (LDP_ENTITY_UP, pOtherLdpEntity,
                                     MPLS_DEF_INCARN);
            }
        }
        else
        {
            if (LdpCheckIfEntityBeActivated (pLdpEntity) == TRUE)
            {
                LdpSNMPEventHandler (LDP_ENTITY_UP, pLdpEntity,
                                     MPLS_DEF_INCARN);
            }
        }
    }
    LDP_ENTITY_ADMIN_STATUS (pLdpEntity) =
        (UINT1) i4SetValMplsLdpEntityAdminStatus;
    MplsLdpEntityUpdateSysTime ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityTcpPort
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityTcpPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityTcpPort (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                            UINT4 u4MplsLdpEntityIndex,
                            UINT4 u4SetValMplsLdpEntityTcpPort)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {

        (pLdpEntity->u2TcpPort) = (UINT2) u4SetValMplsLdpEntityTcpPort;
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         *  NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityUdpDscPort
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityUdpDscPort
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityUdpDscPort (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                               UINT4 u4MplsLdpEntityIndex,
                               UINT4 u4SetValMplsLdpEntityUdpDscPort)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        (pLdpEntity->u2UdpPort) = (UINT2) u4SetValMplsLdpEntityUdpDscPort;
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         * NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityMaxPduLength
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityMaxPduLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityMaxPduLength (tSNMP_OCTET_STRING_TYPE *
                                 pMplsLdpEntityLdpId,
                                 UINT4 u4MplsLdpEntityIndex,
                                 UINT4 u4SetValMplsLdpEntityMaxPduLength)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        if (u4SetValMplsLdpEntityMaxPduLength < LDP_UINT1_SIZE)
        {
            (pLdpEntity->u2MaxPduLen) = LDP_DEF_MAX_PDU_LEN;
        }
        else
        {
            (pLdpEntity->u2MaxPduLen) =
                (UINT2) u4SetValMplsLdpEntityMaxPduLength;
        }
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         *  NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityKeepAliveHoldTimer
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityKeepAliveHoldTimer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityKeepAliveHoldTimer (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       u4SetValMplsLdpEntityKeepAliveHoldTimer)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        if ((u4SetValMplsLdpEntityKeepAliveHoldTimer) == 0)
        {
            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
            {
                LDP_ENTITY_KEEP_ALIVE_TIME (pLdpEntity) = LDP_DEF_LSSN_HTIME;
            }
            else
            {
                LDP_ENTITY_KEEP_ALIVE_TIME (pLdpEntity) = LDP_DEF_RSSN_HTIME;
            }
        }
        else
        {
            LDP_ENTITY_KEEP_ALIVE_TIME (pLdpEntity) =
                u4SetValMplsLdpEntityKeepAliveHoldTimer;
        }
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         *          * NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityHelloHoldTimer
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityHelloHoldTimer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityHelloHoldTimer (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   UINT4 u4SetValMplsLdpEntityHelloHoldTimer)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        LDP_ENTITY_HELLO_HOLD_TIME (pLdpEntity) =
            (UINT2) u4SetValMplsLdpEntityHelloHoldTimer;
        pLdpEntity->u2HelloConfHoldTime =
            (UINT2) u4SetValMplsLdpEntityHelloHoldTimer;
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         *          * NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityInitSessionThreshold
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityInitSessionThreshold
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityInitSessionThreshold (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         i4SetValMplsLdpEntityInitSessionThreshold)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        (pLdpEntity->u4FailInitSessThreshold) =
            (UINT4) i4SetValMplsLdpEntityInitSessionThreshold;
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         * NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityLabelDistMethod
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityLabelDistMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityLabelDistMethod (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    INT4 i4SetValMplsLdpEntityLabelDistMethod)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        (pLdpEntity->u1LabelDistrType) =
            (UINT1) i4SetValMplsLdpEntityLabelDistMethod;
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         * NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityLabelRetentionMode
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityLabelRetentionMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityLabelRetentionMode (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       i4SetValMplsLdpEntityLabelRetentionMode)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {

        (pLdpEntity->u2LabelRet) =
            (UINT2) i4SetValMplsLdpEntityLabelRetentionMode;
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         * NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityPathVectorLimit
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityPathVectorLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityPathVectorLimit (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    INT4 i4SetValMplsLdpEntityPathVectorLimit)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LdpSetLoopDetectionMethod (LDP_LOOP_DETECT_CAPABLE_PV,
                                   (UINT1)
                                   i4SetValMplsLdpEntityPathVectorLimit,
                                   MPLS_DEF_INCARN) == LDP_SUCCESS)

    {
        pLdpEntity->u1PathVecLimit =
            (UINT1) i4SetValMplsLdpEntityPathVectorLimit;
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityHopCountLimit
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityHopCountLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityHopCountLimit (tSNMP_OCTET_STRING_TYPE *
                                  pMplsLdpEntityLdpId,
                                  UINT4 u4MplsLdpEntityIndex,
                                  INT4 i4SetValMplsLdpEntityHopCountLimit)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LdpSetLoopDetectionMethod (LDP_LOOP_DETECT_CAPABLE_HC,
                                   (UINT1)
                                   i4SetValMplsLdpEntityHopCountLimit,
                                   MPLS_DEF_INCARN) == LDP_SUCCESS)
    {
        pLdpEntity->u2HopVecLimit = (UINT2) i4SetValMplsLdpEntityHopCountLimit;
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityTransportAddrKind
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityTransportAddrKind
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityTransportAddrKind (tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 u4MplsLdpEntityIndex,
                                      INT4
                                      i4SetValMplsLdpEntityTransportAddrKind)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) =
        (UINT1) i4SetValMplsLdpEntityTransportAddrKind;
    MplsLdpEntityUpdateSysTime ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityTargetPeer
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityTargetPeer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityTargetPeer (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                               UINT4 u4MplsLdpEntityIndex,
                               INT4 i4SetValMplsLdpEntityTargetPeer)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        /* TruthValue
           SNMP Manager - true = 1; false = 2
           LDP Protocol - true = 1; false = 0 */
        if (i4SetValMplsLdpEntityTargetPeer == LDP_SNMP_TRUE)
        {
            (pLdpEntity->u1TargetFlag) = LDP_TRUE;
        }
        else
        {
            (pLdpEntity->u1TargetFlag) = LDP_FALSE;
        }
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;

    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         * NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityTargetPeerAddrType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityTargetPeerAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityTargetPeerAddrType (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       i4SetValMplsLdpEntityTargetPeerAddrType)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        /* First target type must be set to Remote before setting the
         * the target type or the target address
         */
        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
        {
            MEMCPY ((UINT1 *) &pLdpEntity->NetAddrType,
                    (UINT1 *) &i4SetValMplsLdpEntityTargetPeerAddrType,
                    LDP_NET_ADDR_LEN);
            MplsLdpEntityUpdateSysTime ();
            return SNMP_SUCCESS;

        }
        else
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityTargetPeerAddr
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityTargetPeerAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityTargetPeerAddr (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValMplsLdpEntityTargetPeerAddr)
{
    UINT4               u4IPAddr;
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpId              InvalidLdpId;

#ifdef MPLS_IPV6_WANTED
    INT4                i4CmpRet = 0;
    tIp6Addr            Ipv6Addr;
    tIp6Addr            TmpIpv6Addr;
    MEMSET (&Ipv6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&TmpIpv6Addr, 0, sizeof (tIp6Addr));
#endif

    MEMSET (InvalidLdpId, 0, LDP_MAX_LDPID_LEN);

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* First target type must be set to Remote and the target peer Addr
     * type has to be set before configuring the targetted address.
     */
#ifdef MPLS_IPV6_WANTED
    if (pSetValMplsLdpEntityTargetPeerAddr->i4_Length == LDP_IPV6ADR_LEN)
    {
        MEMCPY (Ipv6Addr.u1_addr,
                pSetValMplsLdpEntityTargetPeerAddr->pu1_OctetList,
                pSetValMplsLdpEntityTargetPeerAddr->i4_Length);

        i4CmpRet =
            MEMCMP (Ipv6Addr.u1_addr, TmpIpv6Addr.u1_addr, LDP_IPV6ADR_LEN);

        if ((i4CmpRet == LDP_ZERO) && (pLdpEntity->u1TargetFlag) == LDP_FALSE)
        {
            pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
            if (pLdpPeer == NULL)
            {
                return SNMP_FAILURE;
            }

            TmrStopTimer (LDP_TIMER_LIST_ID,
                          (tTmrAppTimer *) & (pLdpPeer->GrTimer.AppTimer));

            LdpDeleteLdpPeer (pLdpPeer);
            return SNMP_SUCCESS;
        }

        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
        {
            if (LdpCreateLdpPeer
                (pLdpEntity, InvalidLdpId, LDP_TRUE, &pLdpPeer,
                 LDP_ADDR_TYPE_IPV6) != LDP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            MEMSET ((VOID *) pLdpPeer->PeerLdpId, LDP_ZERO, LDP_MAX_LDPID_LEN);
            MEMCPY (LDP_IPV6_ADDR (pLdpPeer->NetAddr.Addr), Ipv6Addr.u1_addr,
                    IPV6_ADDR_LENGTH);
            pLdpEntity->NetAddrType = MPLS_IPV6_ADDR_TYPE;
            MplsLdpEntityUpdateSysTime ();
            return SNMP_SUCCESS;

        }
        else
        {
            return SNMP_FAILURE;
        }

    }

    else
    {
#endif

        MEMCPY ((UINT1 *) &u4IPAddr,
                pSetValMplsLdpEntityTargetPeerAddr->pu1_OctetList,
                pSetValMplsLdpEntityTargetPeerAddr->i4_Length);

        if (u4IPAddr == LDP_ZERO)
        {
            pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
            if (pLdpPeer == NULL)
            {
                return SNMP_FAILURE;
            }

            TmrStopTimer (LDP_TIMER_LIST_ID,
                          (tTmrAppTimer *) & (pLdpPeer->GrTimer.AppTimer));

            LdpDeleteLdpPeer (pLdpPeer);
            return SNMP_SUCCESS;
        }

        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
        {
            if (LdpCreateLdpPeer
                (pLdpEntity, InvalidLdpId, LDP_TRUE, &pLdpPeer,
                 LDP_ADDR_TYPE_IPV4) != LDP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            MEMSET ((VOID *) pLdpPeer->PeerLdpId, LDP_ZERO, LDP_MAX_LDPID_LEN);
            MEMCPY (pLdpPeer->NetAddr.Addr.au1Ipv4Addr, (UINT1 *) &u4IPAddr,
                    LDP_NET_ADDR_LEN);
            pLdpEntity->NetAddrType = MPLS_IPV4_ADDR_TYPE;
            MplsLdpEntityUpdateSysTime ();
            return SNMP_SUCCESS;

        }
        else
        {
            return SNMP_FAILURE;
        }
#ifdef MPLS_IPV6_WANTED
    }
#endif
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityLabelType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityLabelType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityLabelType (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                              UINT4 u4MplsLdpEntityIndex,
                              INT4 i4SetValMplsLdpEntityLabelType)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {

        (pLdpEntity->u1EntityType) = (UINT1) i4SetValMplsLdpEntityLabelType;

        if (i4SetValMplsLdpEntityLabelType == LDP_GEN_LABEL)
        {
            LDP_LBL_SPACE_TYPE (pLdpEntity) = LDP_PER_PLATFORM;
        }
        else
        {
            LDP_LBL_SPACE_TYPE (pLdpEntity) = LDP_PER_IFACE;
        }
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         * NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityStorageType (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 i4SetValMplsLdpEntityStorageType)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((MPLS_DEF_INCARN), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        (pLdpEntity->u1StorageType) = (UINT1) i4SetValMplsLdpEntityStorageType;
        MplsLdpEntityUpdateSysTime ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         * NotInService State.  */
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                              UINT4 u4MplsLdpEntityIndex,
                              INT4 i4SetValMplsLdpEntityRowStatus)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEntity         *pOtherLdpEntity = NULL;
    UINT2               u2IncarnId = (UINT2) MPLS_DEF_INCARN;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    switch (i4SetValMplsLdpEntityRowStatus)
    {
        case ACTIVE:
            /* Check if an Ldp Entity with given indices, exists in NotReady 
               or NotInService State */

            if (LdpGetLdpEntity (u2IncarnId, pu1LdpEntityId,
                                 u4MplsLdpEntityIndex, &pLdpEntity) !=
                LDP_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE) ||
                (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY))
            {

                /* IF Configuration Sequence TLV option is enabled globally,
                 * Enable Configuration Sequence TLV option in entity level also
                 * */

                if (gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1ConfigSeqTlvOption ==
                    LDP_SNMP_TRUE)
                {
                    pLdpEntity->u1ConfSeqNumTlvEnable = TRUE;
                }
                else
                {
                    pLdpEntity->u1ConfSeqNumTlvEnable = FALSE;
                }

                if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE)
                {
                    pLdpEntity->u4ConfSeqNum++;
                }

                /* Associate all the mpls ip capable interfaces to
                 * this entity if the user has not configured the
                 * interface*/
                if (LdpAssociateIfEntry (pLdpEntity) != LDP_SUCCESS)
                {
                    LDP_DBG (LDP_IF_SNMP,
                             "GenericLRRowStatusSet:"
                             "Failed to associate interfaces to"
                             "LDP entity\n");

                    return SNMP_FAILURE;
                }

                LDP_ENTITY_ROW_STATUS (pLdpEntity) =
                    (UINT1) i4SetValMplsLdpEntityRowStatus;

                if (pLdpEntity->u2HelloConfHoldTime == LDP_ZERO)
                {
                    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
                    {
                        LDP_ENTITY_HELLO_HOLD_TIME (pLdpEntity) =
                            LDP_DEF_LHELLO_HTIME;
                        pLdpEntity->u2HelloConfHoldTime = LDP_DEF_LHELLO_HTIME;
                    }
                    else
                    {
                        LDP_ENTITY_HELLO_HOLD_TIME (pLdpEntity) =
                            LDP_DEF_RHELLO_HTIME;
                        pLdpEntity->u2HelloConfHoldTime = LDP_DEF_RHELLO_HTIME;
                    }
                }

                if (LDP_ENTITY_ADMIN_STATUS (pLdpEntity) == LDP_ADMIN_UP)
                {
                    /* Start the Hello Timer for sending Periodic
                     * Hello Messages */
                    LdpSNMPEventHandler (LDP_ENTITY_UP, pLdpEntity, u2IncarnId);
                }
                /*MSR: flag bIsTransAddrIntfUp is not marked True While updating transport address after MSR,
                   as TransAddrInterface remain operational down at that instant.
                   And in case TransAddrInterface comes operational up before Entity is made active,
                   flag bIsTransAddrIntfUp is not updated.
                   Hence LDP Hello processing is skipped and LDP session remains down.
                   Below check is added to update the flag when entity is marked active. */

                if (pLdpEntity->u4TransIfIndex != LDP_ZERO)
                {
                    CfaGetIfOperStatus (pLdpEntity->u4TransIfIndex,
                                        &u1OperStatus);
                    if (u1OperStatus == CFA_IF_UP)
                    {
                        pLdpEntity->bIsTransAddrIntfUp = LDP_TRUE;
                    }
                    else
                    {
                        pLdpEntity->bIsTransAddrIntfUp = LDP_FALSE;
                    }
                }

            }
            return SNMP_SUCCESS;

        case NOT_IN_SERVICE:
            /* Check if an Ldp Entity with given indices, exists */
            if (LdpGetLdpEntity (u2IncarnId, pu1LdpEntityId,
                                 u4MplsLdpEntityIndex, &pLdpEntity)
                != LDP_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == (UINT1) NOT_IN_SERVICE)
            {
                return SNMP_SUCCESS;
            }
            else if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == (UINT1) NOT_READY)
            {
                LDP_ENTITY_ROW_STATUS (pLdpEntity) =
                    (UINT1) i4SetValMplsLdpEntityRowStatus;

                return SNMP_SUCCESS;
            }

            else if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == (UINT1) ACTIVE)
            {
                LDP_ENTITY_ROW_STATUS (pLdpEntity) =
                    (UINT1) i4SetValMplsLdpEntityRowStatus;
                /* Delete the session associated with this entity. */
                if (LDP_ENTITY_ADMIN_STATUS (pLdpEntity) == LDP_ADMIN_UP)
                {
                    pOtherLdpEntity =
                        LdpGetOtherEntityForActivation (pLdpEntity);

                    LdpSNMPEventHandler (LDP_ENTITY_NOTINSERVICE,
                                         pLdpEntity, MPLS_DEF_INCARN);

                    if (pOtherLdpEntity != NULL)
                    {
                        LdpSNMPEventHandler (LDP_ENTITY_UP, pOtherLdpEntity,
                                             MPLS_DEF_INCARN);
                    }
                }
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;

        case CREATE_AND_WAIT:

            if (LdpGetLdpEntity (u2IncarnId, pu1LdpEntityId,
                                 u4MplsLdpEntityIndex, &pLdpEntity)
                == LDP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            /* Manager has to create Entity by calling the get EntityIndexNext
             * function and has to configure that entity Index. */

            /* Here the Label (Entity Index) is allocated and is checked whether
             * the manager has configured the returned value from the get 
             * Entity Index. */
            if (LdpEntityTableSetIndex (u4MplsLdpEntityIndex) !=
                u4MplsLdpEntityIndex)
            {
                return SNMP_FAILURE;
            }
            pList = &LDP_ENTITY_LIST (u2IncarnId);
            if (LdpCreateLdpEntity (pList, u2IncarnId, pu1LdpEntityId,
                                    (UINT2) u4MplsLdpEntityIndex, &pLdpEntity)
                == LDP_FAILURE)
            {
                LdpEntityTableRelIndex (u4MplsLdpEntityIndex);
                return SNMP_FAILURE;
            }
            LDP_ENTITY_ROW_STATUS (pLdpEntity) = NOT_READY;

            /*default values */

            LDP_ENTITY_PROT_VERSION (pLdpEntity) = LDP_ONE;
            pLdpEntity->u1AdminStatus = LDP_ADMIN_UP;
            pLdpEntity->u2TcpPort = LDP_STD_PORT;
            pLdpEntity->u2UdpPort = LDP_STD_PORT;
            pLdpEntity->u2MaxPduLen = LDP_DEF_MAX_PDU_LEN;
            LDP_ENTITY_KEEP_ALIVE_TIME (pLdpEntity) =
                LDP_KEEPALIVE_DEFAULT_TIME;
            LDP_ENTITY_HELLO_HOLD_TIME (pLdpEntity) =
                LDP_HELLOHOLDTIME_DEFAULT_TIME;
            pLdpEntity->u2HelloConfHoldTime = LDP_HELLOHOLDTIME_DEFAULT_TIME;
            LDP_SSN_THRESHOLD (pLdpEntity) = LDP_DEFAULT_THERSHOLD;
            LDP_ENTITY_HC_LIMIT (pLdpEntity) = LDP_DEF_HOP_CNT_LIM;
            LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) = LOOPBACK_ADDRESS_TYPE;
            pLdpEntity->u1TargetFlag = LDP_FALSE;
            pLdpEntity->u1StorageType = LDP_STORAGE_NONVOLATILE;
            pLdpEntity->bIsIntfMapped = LDP_FALSE;
            MplsLdpEntityUpdateSysTime ();
            return SNMP_SUCCESS;

        case DESTROY:
            if (LdpGetLdpEntity (u2IncarnId,
                                 pu1LdpEntityId,
                                 u4MplsLdpEntityIndex,
                                 &pLdpEntity) != LDP_SUCCESS)
            {
                return SNMP_FAILURE;
            }
            LDP_ENTITY_ROW_STATUS (pLdpEntity) =
                (UINT1) i4SetValMplsLdpEntityRowStatus;
            LdpHandleLdpUpDownEntityForLdpOverRsvp (pLdpEntity,
                                                    LDP_ENTITY_DOWN);

            pOtherLdpEntity = LdpGetOtherEntityForActivation (pLdpEntity);

            LdpSNMPEventHandler (LDP_ENTITY_DOWN, pLdpEntity, MPLS_DEF_INCARN);

            if (pOtherLdpEntity != NULL)
            {
                LdpSNMPEventHandler (LDP_ENTITY_UP, pOtherLdpEntity,
                                     MPLS_DEF_INCARN);
            }

            MplsLdpEntityUpdateSysTime ();
            return SNMP_SUCCESS;

        case CREATE_AND_GO:
            return SNMP_FAILURE;

        default:
            return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityProtocolVersion
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityProtocolVersion
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityProtocolVersion (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       u4TestValMplsLdpEntityProtocolVersion)
{

    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (u4TestValMplsLdpEntityProtocolVersion == LDP_PROTO_VER)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAdminStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAdminStatus (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   INT4 i4TestValMplsLdpEntityAdminStatus)
{
    /* Added to suppress warnings */
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    switch (i4TestValMplsLdpEntityAdminStatus)
    {
        case LDP_ADMIN_UP:
        case LDP_ADMIN_DOWN:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityTcpPort
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityTcpPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityTcpPort (UINT4 *pu4ErrorCode,
                               tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                               UINT4 u4MplsLdpEntityIndex,
                               UINT4 u4TestValMplsLdpEntityTcpPort)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((u4TestValMplsLdpEntityTcpPort == LDP_STD_PORT))
    {
        /* The default value is the well-known value (646)
         * as per RFC */
        return SNMP_SUCCESS;
    }
    if (u4TestValMplsLdpEntityTcpPort <= LDP_UINT2_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityUdpDscPort
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityUdpDscPort
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityUdpDscPort (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                  UINT4 u4MplsLdpEntityIndex,
                                  UINT4 u4TestValMplsLdpEntityUdpDscPort)
{

    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((u4TestValMplsLdpEntityUdpDscPort == LDP_STD_PORT))
    {
        /* The default value is the well-known value (646)
         * as per RFC */
        return SNMP_SUCCESS;
    }
    if (u4TestValMplsLdpEntityUdpDscPort <= LDP_UINT2_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityMaxPduLength
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityMaxPduLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityMaxPduLength (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    UINT4 u4TestValMplsLdpEntityMaxPduLength)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* The draft says, if the configured value is less than 255, the default
     * max pdu length - 4096 is taken.
     * The set routine takes care of this condition.
     * So no lower range checking is done .*/

    /* Acceptable Value Range 256-65535 */
    if (u4TestValMplsLdpEntityMaxPduLength <= LDP_UINT2_SIZE &&
        u4TestValMplsLdpEntityMaxPduLength > LDP_UINT1_SIZE)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityKeepAliveHoldTimer
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityKeepAliveHoldTimer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityKeepAliveHoldTimer (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4
                                          u4TestValMplsLdpEntityKeepAliveHoldTimer)
{

    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((u4TestValMplsLdpEntityKeepAliveHoldTimer >=
         LDP_KALIVE_MIN_HTIME) &&
        (u4TestValMplsLdpEntityKeepAliveHoldTimer <= LDP_KALIVE_MAX_HTIME))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityHelloHoldTimer
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityHelloHoldTimer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityHelloHoldTimer (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 u4MplsLdpEntityIndex,
                                      UINT4
                                      u4TestValMplsLdpEntityHelloHoldTimer)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (u4TestValMplsLdpEntityHelloHoldTimer <= LDP_HELLO_MAX_HTIME)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityInitSessionThreshold
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityInitSessionThreshold
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityInitSessionThreshold (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            INT4
                                            i4TestValMplsLdpEntityInitSessionThreshold)
{

    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((i4TestValMplsLdpEntityInitSessionThreshold >= LDP_MIN_THERSHOLD) &&
        (i4TestValMplsLdpEntityInitSessionThreshold <= LDP_MAX_THERSHOLD))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityLabelDistMethod
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityLabelDistMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityLabelDistMethod (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       i4TestValMplsLdpEntityLabelDistMethod)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsLdpEntityLabelDistMethod)
    {
        case LDP_DSTR_ON_DEMAND:
        case LDP_DSTR_UNSOLICIT:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityLabelRetentionMode
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityLabelRetentionMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityLabelRetentionMode (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          INT4
                                          i4TestValMplsLdpEntityLabelRetentionMode)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsLdpEntityLabelRetentionMode)
    {
        case LDP_CONSERVATIVE_MODE:
        case LDP_LIBERAL_MODE:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityPathVectorLimit
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityPathVectorLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityPathVectorLimit (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       i4TestValMplsLdpEntityPathVectorLimit)
{

    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         * NotInService State.  */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValMplsLdpEntityPathVectorLimit >= LDP_MIN_PVLIMIT) &&
        (i4TestValMplsLdpEntityPathVectorLimit <= LDP_MAX_PVLIMIT))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityHopCountLimit
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityHopCountLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityHopCountLimit (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     INT4 i4TestValMplsLdpEntityHopCountLimit)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
    {
        /* Value can be set only when Ldp Entity is in NotReady or
         * NotInService State.  */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValMplsLdpEntityHopCountLimit >= LDP_MIN_HCLIMIT) &&
        (i4TestValMplsLdpEntityHopCountLimit <= LDP_MAX_HCLIMIT))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityTransportAddrKind
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityTransportAddrKind
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityTransportAddrKind (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         i4TestValMplsLdpEntityTransportAddrKind)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValMplsLdpEntityTransportAddrKind != INTERFACE_ADDRESS_TYPE) &&
        (i4TestValMplsLdpEntityTransportAddrKind != LOOPBACK_ADDRESS_TYPE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityTargetPeer
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityTargetPeer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityTargetPeer (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                  UINT4 u4MplsLdpEntityIndex,
                                  INT4 i4TestValMplsLdpEntityTargetPeer)
{

    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEthParams      *pLdpEthParams = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /*Check whether wrong LDP label range is assinged to Targeted Peer */
    TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                  pLdpEthParams, tLdpEthParams *)
    {
        /*Check entity is configured with direct peer lable */
        if ((pLdpEthParams->u4MinLabelVal <
             gSystemSize.MplsSystemSize.u4MinL2VpnLblRange) ||
            (pLdpEthParams->u4MaxLabelVal >
             gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if ((i4TestValMplsLdpEntityTargetPeer == LDP_SNMP_TRUE) ||
        (i4TestValMplsLdpEntityTargetPeer == LDP_SNMP_FALSE))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityTargetPeerAddrType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityTargetPeerAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityTargetPeerAddrType (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          INT4
                                          i4TestValMplsLdpEntityTargetPeerAddrType)
{
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValMplsLdpEntityTargetPeerAddrType == IPV4)
    {
        return SNMP_SUCCESS;
    }
    /* Other Types are currently not supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityTargetPeerAddr
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityTargetPeerAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityTargetPeerAddr (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 u4MplsLdpEntityIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValMplsLdpEntityTargetPeerAddr)
{
    UINT4               u4IPAddr;
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEntity         *pTempLdpEntity = NULL;
    UINT4               u4PeerTempAddr = LDP_ZERO;
    tLdpPeer           *pLdpPeer = NULL;

#ifdef MPLS_IPV6_WANTED
    INT4                i4CmpRet = 0;
    UINT1               u1Scope = 0;
    tIp6Addr            Ipv6Addr;
    MEMSET (&Ipv6Addr, 0, sizeof (tIp6Addr));
#endif

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        CLI_SET_ERR (LDP_CLI_INVALID_ENTITY);
        return SNMP_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED
    if (pTestValMplsLdpEntityTargetPeerAddr->i4_Length == IPV6_ADDR_LENGTH)
    {
        if ((pLdpEntity->NetAddrType != IPV6) ||
            (pTestValMplsLdpEntityTargetPeerAddr->i4_Length > IPV6_ADDR_LENGTH)
            || (pTestValMplsLdpEntityTargetPeerAddr->i4_Length < LDP_ONE))

        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (LDP_CLI_WRONG_ADDR_TYPE);
            return SNMP_FAILURE;
        }

        if (MplsValidateIPv6Addr
            (pu4ErrorCode, pTestValMplsLdpEntityTargetPeerAddr) == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Validation of IPv6Address is Failed \t\n");
            return SNMP_FAILURE;
        }

        MEMCPY (Ipv6Addr.u1_addr,
                pTestValMplsLdpEntityTargetPeerAddr->pu1_OctetList,
                IPV6_ADDR_LENGTH);

        u1Scope = Ip6GetAddrScope (&(Ipv6Addr));

        if (u1Scope != ADDR6_SCOPE_GLOBAL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Address should not be Link Local\t\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (LDP_CLI_WRONG_IPV6_TARG_ADDR_TYPE);
            return SNMP_FAILURE;
        }

        /* First target type must be set to Remote before setting the
         * the target type or the target address
         */
        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (LDP_CLI_INVALID_ENTITY_TYPE);
            return SNMP_FAILURE;
        }
        /* Target Peer Address should not be configured for some other
         * LDP Entity. */
        TMO_SLL_Scan (&LDP_ENTITY_LIST (MPLS_DEF_INCARN), pTempLdpEntity,
                      tLdpEntity *)
        {
            if (pTempLdpEntity == pLdpEntity)
            {
                continue;
            }

            if (pTempLdpEntity->u1TargetFlag != LDP_TRUE)
            {
                continue;
            }

            pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pTempLdpEntity->PeerList));

            if (pLdpPeer == NULL)
            {
                continue;
            }

            if (pLdpPeer->NetAddr.u2AddrType == MPLS_IPV6_ADDR_TYPE)
            {

                i4CmpRet = MEMCMP (&Ipv6Addr.u1_addr,
                                   LDP_IPV6_ADDR (pLdpPeer->NetAddr.Addr),
                                   IPV6_ADDR_LENGTH);

                if (i4CmpRet == 0)
                {
                    *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                    CLI_SET_ERR (LDP_CLI_ADDR_IN_USE);
                    return SNMP_FAILURE;
                }
            }
        }
    }
    else
    {
#endif
        if ((pLdpEntity->NetAddrType != IPV4) ||
            (pTestValMplsLdpEntityTargetPeerAddr->i4_Length > IPV4_ADDR_LENGTH)
            || (pTestValMplsLdpEntityTargetPeerAddr->i4_Length < LDP_ONE))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (LDP_CLI_WRONG_ADDR_TYPE);
            return SNMP_FAILURE;
        }

        MEMCPY (&u4IPAddr, pTestValMplsLdpEntityTargetPeerAddr->pu1_OctetList,
                pTestValMplsLdpEntityTargetPeerAddr->i4_Length);

        u4IPAddr = OSIX_NTOHL (u4IPAddr);

        if (((u4IPAddr == LDP_NULL_LSRID) ||
             (u4IPAddr == LDP_BCAST_LSRID)) ||
            ((u4IPAddr >= LDP_MCAST_NET_ADDR) &&
             (u4IPAddr <= LDP_MCAST_NET_INV_ADDR)))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (LDP_CLI_WRONG_ADDR_VALUE);
            return SNMP_FAILURE;
        }

        if (!((MPLS_IS_ADDR_CLASS_A (u4IPAddr)) ||
              (MPLS_IS_ADDR_CLASS_B (u4IPAddr)) ||
              (MPLS_IS_ADDR_CLASS_C (u4IPAddr))))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (LDP_CLI_WRONG_ADDR_VALUE);
            return SNMP_FAILURE;
        }

        if (MPLS_IS_ADDR_LOOPBACK (u4IPAddr))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CLI_SET_ERR (LDP_CLI_WRONG_ADDR_VALUE);
            return SNMP_FAILURE;
        }

        /* First target type must be set to Remote before setting the
         * the target type or the target address
         */
        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CLI_SET_ERR (LDP_CLI_INVALID_ENTITY_TYPE);
            return SNMP_FAILURE;
        }

        /* Target Peer Address should not be configured for some other
         * LDP Entity. */
        TMO_SLL_Scan (&LDP_ENTITY_LIST (MPLS_DEF_INCARN), pTempLdpEntity,
                      tLdpEntity *)
        {
            if (pTempLdpEntity == pLdpEntity)
            {
                continue;
            }

            if (pTempLdpEntity->u1TargetFlag != LDP_TRUE)
            {
                continue;
            }

            pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pTempLdpEntity->PeerList));

            if (pLdpPeer == NULL)
            {
                continue;
            }

            if (pLdpPeer->NetAddr.u2AddrType == MPLS_IPV4_ADDR_TYPE)
            {
                MEMCPY ((UINT1 *) &u4PeerTempAddr,
                        pLdpPeer->NetAddr.Addr.au1Ipv4Addr, IPV4_ADDR_LENGTH);

                u4PeerTempAddr = OSIX_NTOHL (u4PeerTempAddr);
                if (u4PeerTempAddr == u4IPAddr)
                {
                    *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                    CLI_SET_ERR (LDP_CLI_ADDR_IN_USE);
                    return SNMP_FAILURE;
                }
            }

        }
#ifdef MPLS_IPV6_WANTED
    }
#endif
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityLabelType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityLabelType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityLabelType (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                 UINT4 u4MplsLdpEntityIndex,
                                 INT4 i4TestValMplsLdpEntityLabelType)
{

    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsLdpEntityLabelType)
    {
        case LDP_ATM_LABEL:
        case LDP_GEN_LABEL:
            break;
        case LDP_FR_LABEL:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityStorageType (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   INT4 i4TestValMplsLdpEntityStorageType)
{

    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsLdpEntityStorageType)
    {
        case LDP_STORAGE_VOLATILE:
        case LDP_STORAGE_NONVOLATILE:
        case LDP_STORAGE_OTHER:
        case LDP_STORAGE_PERMANENT:
        case LDP_STORAGE_READONLY:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object
                testValMplsLdpEntityRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityRowStatus (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                 UINT4 u4MplsLdpEntityIndex,
                                 INT4 i4TestValMplsLdpEntityRowStatus)
{
#ifdef MPLS_IPV6_WANTED
    UINT1               u1Scope = 0;
#endif
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEthParams      *pLdpEthParams = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpId              InvalidLdpId = "000000";

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    switch (i4TestValMplsLdpEntityRowStatus)
    {
        case CREATE_AND_WAIT:
            break;
        case ACTIVE:
            if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                                 u4MplsLdpEntityIndex,
                                 &pLdpEntity) != LDP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
            /* Check for Transport Address Type and Targeted Address Type *
             * to ensure Transport and Targeted address of same type      */
            if (pLdpEntity->u1TargetFlag == LDP_TRUE)
            {
                if ((pLdpEntity->NetAddrType == MPLS_IPV6_ADDR_TYPE) &&
                    (pLdpEntity->u1TransAddrTlvEnable != 0) &&
                    (pLdpEntity->u1Ipv6TransAddrTlvEnable == 0))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
                    return SNMP_FAILURE;
                }

                if ((pLdpEntity->NetAddrType == MPLS_IPV4_ADDR_TYPE) &&
                    (pLdpEntity->u1TransAddrTlvEnable == 0) &&
                    (pLdpEntity->u1Ipv6TransAddrTlvEnable != 0))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_TYPE;
                    return SNMP_FAILURE;
                }
                u1Scope = Ip6GetAddrScope (&(pLdpEntity->Ipv6TransportAddress));
                if ((pLdpEntity->u1Ipv6TransAddrTlvEnable != 0) &&
                    (u1Scope != ADDR6_SCOPE_GLOBAL))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }

            }
#endif
            /* Check for Label range values for targeted hello and basic hello */
            TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                          pLdpEthParams, tLdpEthParams *)
            {
                /* Check for Targeted hello label range */
                if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
                {
                    if ((pLdpEthParams->u4MinLabelVal <
                         gSystemSize.MplsSystemSize.u4MinL2VpnLblRange) ||
                        (pLdpEthParams->u4MaxLabelVal <
                         gSystemSize.MplsSystemSize.u4MinL2VpnLblRange) ||
                        (pLdpEthParams->u4MinLabelVal >
                         gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange) ||
                        (pLdpEthParams->u4MaxLabelVal >
                         gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
                else
                {
                    /* Check for Basic hello label range */
                    if ((pLdpEthParams->u4MinLabelVal <
                         gSystemSize.MplsSystemSize.u4MinLdpLblRange) ||
                        (pLdpEthParams->u4MaxLabelVal <
                         gSystemSize.MplsSystemSize.u4MinLdpLblRange) ||
                        (pLdpEthParams->u4MinLabelVal >
                         gSystemSize.MplsSystemSize.u4MaxLdpLblRange) ||
                        (pLdpEthParams->u4MaxLabelVal >
                         gSystemSize.MplsSystemSize.u4MaxLdpLblRange))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }

                /* Either an entity can have label ranges 
                 * with user configured interfaces or it 
                 * can have auto-associated interfaces, Both 
                 * cannot co-exist.*/

                if (((pLdpEthParams->u4MplsIfIndex) == LDP_ZERO) &&
                    (pLdpEntity->bIsIntfMapped == TRUE))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }

            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
            {
                if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_PLATFORM)
                {
                    /* Targetted Entity should be associated with
                     * generic label space.
                     */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

                pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
                if (pLdpPeer == NULL)
                {
                    return SNMP_FAILURE;
                }

                if (MEMCMP ((UINT1 *) pLdpPeer->PeerLdpId,
                            InvalidLdpId, LDP_IPV4ADR_LEN) == 0)
                {
                    /* Targeted Peer Address not configured */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                if (LDP_ENTITY_LDP_OVER_RSVP_FLAG (pLdpEntity) == LDP_TRUE)
                {
                    if ((pLdpEntity->OutStackTnlInfo.u4TnlId == LDP_ZERO) &&
                        (pLdpEntity->InStackTnlInfo.u4TnlId == LDP_ZERO))
                    {
                        /* Tunnels are not associated */
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
            }

            /* 
             * If the label space type is Per-Interface and the LdpEntity
             * Type is ATM, then checking that 
             *  - Atm LabelRange List maintained in LdpEntity is non-null.
             *  - Iface List contains exactly one Interface Node.
             */
            if ((LDP_LBL_SPACE_TYPE (pLdpEntity) == LDP_PER_IFACE) &&
                (LDP_ENTITY_TYPE (pLdpEntity) == LDP_ATM_MODE))
            {

                if (TMO_SLL_Count (&(pLdpEntity->IfList)) != 1)
                {
                    /* Exactly 1 Interface requires to be configured for 
                     *  Entities of Label Space Type - Per-Interface. */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                if (pLdpEntity->pLdpAtmParams == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                if (TMO_SLL_Count
                    (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity)) == 0)
                {
                    /* No Atm Label Range configured for this LdpEntity */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                if (pLdpEntity->pLdpAtmParams->u1RowStatus != ACTIVE)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            /* 
             * If the label space type is Per-Platform, then checking that 
             * the Min and Max labels configured(ie should be non-zero) 
             * and MinLabel < MaxLabel.
             */
            else if (LDP_LBL_SPACE_TYPE (pLdpEntity) == LDP_PER_PLATFORM)
            {
                if (TMO_SLL_Count
                    (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity)) == 0)
                {
                    /* No Ethernet Label Range configured for this 
                     * LdpEntity */
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                              pLdpEthParams, tLdpEthParams *)
                {
                    if (pLdpEthParams->u1RowStatus != ACTIVE)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                }
            }
            break;
        case NOT_IN_SERVICE:
            break;
        case DESTROY:
            if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                                 u4MplsLdpEntityIndex,
                                 &pLdpEntity) != LDP_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }

            if ((pLdpEntity->u4AppMask & LDP_APP_L2VPN_MASK)
                == LDP_APP_L2VPN_MASK)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsLdpEntityTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsLdpEntityTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsLdpEntityStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpEntityStatsTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpEntityStatsTable (tSNMP_OCTET_STRING_TYPE *
                                                 pMplsLdpEntityLdpId,
                                                 UINT4 u4MplsLdpEntityIndex)
{
    if (nmhValidateIndexInstanceMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                                    u4MplsLdpEntityIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpEntityStatsTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpEntityStatsTable (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 *pu4MplsLdpEntityIndex)
{
    *pu4MplsLdpEntityIndex = LDP_ZERO;

    if (nmhGetFirstIndexMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                            pu4MplsLdpEntityIndex)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpEntityStatsTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpEntityStatsTable (tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpEntityLdpId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        UINT4 *pu4NextMplsLdpEntityIndex)
{
    *pu4NextMplsLdpEntityIndex = LDP_ZERO;

    if (nmhGetNextIndexMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                           pNextMplsLdpEntityLdpId,
                                           u4MplsLdpEntityIndex,
                                           pu4NextMplsLdpEntityIndex)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsSessionAttempts
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsSessionAttempts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsSessionAttempts (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         UINT4
                                         *pu4RetValMplsLdpEntityStatsSessionAttempts)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsSessionAttempts =
        LDP_ENTITY_ATTMPT_SESSION (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsSessionRejectedNoHelloErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsSessionRejectedNoHelloErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsSessionRejectedNoHelloErrors (tSNMP_OCTET_STRING_TYPE *
                                                      pMplsLdpEntityLdpId,
                                                      UINT4
                                                      u4MplsLdpEntityIndex,
                                                      UINT4
                                                      *pu4RetValMplsLdpEntityStatsSessionRejectedNoHelloErrors)
{

    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsSessionRejectedNoHelloErrors =
        LDP_ENTITY_SESSREJ_NO_HELLO_ERR (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsSessionRejectedAdErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsSessionRejectedAdErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsSessionRejectedAdErrors (tSNMP_OCTET_STRING_TYPE *
                                                 pMplsLdpEntityLdpId,
                                                 UINT4 u4MplsLdpEntityIndex,
                                                 UINT4
                                                 *pu4RetValMplsLdpEntityStatsSessionRejectedAdErrors)
{

    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsSessionRejectedAdErrors =
        LDP_ENTITY_SESSREJ_ADVRT_ERR (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsSessionRejectedMaxPduErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsSessionRejectedMaxPduErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsSessionRejectedMaxPduErrors (tSNMP_OCTET_STRING_TYPE *
                                                     pMplsLdpEntityLdpId,
                                                     UINT4 u4MplsLdpEntityIndex,
                                                     UINT4
                                                     *pu4RetValMplsLdpEntityStatsSessionRejectedMaxPduErrors)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsSessionRejectedMaxPduErrors =
        LDP_ENTITY_SESSREJ_MAXPDU_ERR (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsSessionRejectedLRErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsSessionRejectedLRErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsSessionRejectedLRErrors (tSNMP_OCTET_STRING_TYPE *
                                                 pMplsLdpEntityLdpId,
                                                 UINT4 u4MplsLdpEntityIndex,
                                                 UINT4
                                                 *pu4RetValMplsLdpEntityStatsSessionRejectedLRErrors)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsSessionRejectedLRErrors =
        LDP_ENTITY_SESSREJ_LBLRANGE_ERR (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsBadLdpIdentifierErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsBadLdpIdentifierErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsBadLdpIdentifierErrors (tSNMP_OCTET_STRING_TYPE *
                                                pMplsLdpEntityLdpId,
                                                UINT4 u4MplsLdpEntityIndex,
                                                UINT4
                                                *pu4RetValMplsLdpEntityStatsBadLdpIdentifierErrors)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsBadLdpIdentifierErrors =
        LDP_ENTITY_BAD_LDPID_ERR (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsBadPduLengthErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsBadPduLengthErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsBadPduLengthErrors (tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            UINT4
                                            *pu4RetValMplsLdpEntityStatsBadPduLengthErrors)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsBadPduLengthErrors =
        LDP_ENTITY_BAD_PDU_LEN_ERR (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsBadMessageLengthErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsBadMessageLengthErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsBadMessageLengthErrors (tSNMP_OCTET_STRING_TYPE *
                                                pMplsLdpEntityLdpId,
                                                UINT4 u4MplsLdpEntityIndex,
                                                UINT4
                                                *pu4RetValMplsLdpEntityStatsBadMessageLengthErrors)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsBadMessageLengthErrors =
        LDP_ENTITY_BAD_MSG_LEN_ERR (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsBadTlvLengthErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsBadTlvLengthErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsBadTlvLengthErrors (tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            UINT4
                                            *pu4RetValMplsLdpEntityStatsBadTlvLengthErrors)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsBadTlvLengthErrors =
        LDP_ENTITY_BAD_TLV_LEN_ERR (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsMalformedTlvValueErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsMalformedTlvValueErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsMalformedTlvValueErrors (tSNMP_OCTET_STRING_TYPE *
                                                 pMplsLdpEntityLdpId,
                                                 UINT4 u4MplsLdpEntityIndex,
                                                 UINT4
                                                 *pu4RetValMplsLdpEntityStatsMalformedTlvValueErrors)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsMalformedTlvValueErrors =
        LDP_ENTITY_MALFORMED_TLV_ERR (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsKeepAliveTimerExpErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsKeepAliveTimerExpErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsKeepAliveTimerExpErrors (tSNMP_OCTET_STRING_TYPE *
                                                 pMplsLdpEntityLdpId,
                                                 UINT4 u4MplsLdpEntityIndex,
                                                 UINT4
                                                 *pu4RetValMplsLdpEntityStatsKeepAliveTimerExpErrors)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsKeepAliveTimerExpErrors =
        LDP_ENTITY_KEEPALIVE_TIMER_EXP_ERR (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsShutdownReceivedNotifications
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsShutdownReceivedNotifications
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsShutdownReceivedNotifications (tSNMP_OCTET_STRING_TYPE *
                                                       pMplsLdpEntityLdpId,
                                                       UINT4
                                                       u4MplsLdpEntityIndex,
                                                       UINT4
                                                       *pu4RetValMplsLdpEntityStatsShutdownReceivedNotifications)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsShutdownReceivedNotifications =
        LDP_ENTITY_SHUTDOWN_NOTIF_RECVD (pLdpEntity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityStatsShutdownSentNotifications
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityStatsShutdownSentNotifications
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityStatsShutdownSentNotifications (tSNMP_OCTET_STRING_TYPE *
                                                   pMplsLdpEntityLdpId,
                                                   UINT4 u4MplsLdpEntityIndex,
                                                   UINT4
                                                   *pu4RetValMplsLdpEntityStatsShutdownSentNotifications)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpEntityStatsShutdownSentNotifications =
        LDP_ENTITY_SHUTDOWN_NOTIF_SENT (pLdpEntity);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpPeerLastChange
 Input       :  The Indices

                The Object 
                retValMplsLdpPeerLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpPeerLastChange (UINT4 *pu4RetValMplsLdpPeerLastChange)
{
    *pu4RetValMplsLdpPeerLastChange = MplsLdpPeerGetSysTime ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsLdpPeerTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpPeerTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpPeerTable (tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpPeerLdpId)
{
    UINT4               u4PeerLdpLsrId;
    UINT1              *pu1LdpPeerId = NULL;

    /* Validating upto LdpEntityID */
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (nmhValidateIndexInstanceMplsLdpEntityTable ((tSNMP_OCTET_STRING_TYPE
                                                     *) pMplsLdpEntityLdpId,
                                                    (UINT2)
                                                    u4MplsLdpEntityIndex) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY ((UINT1 *) &u4PeerLdpLsrId, pu1LdpPeerId, LDP_IPV4ADR_LEN);
    if ((u4PeerLdpLsrId == LDP_NULL_LSRID)
        || (u4PeerLdpLsrId == LDP_BCAST_LSRID)
        || ((u4PeerLdpLsrId >= LDP_MCAST_NET_ADDR)
            && (u4PeerLdpLsrId <= LDP_MCAST_NET_INV_ADDR)))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpPeerTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpPeerTable (tSNMP_OCTET_STRING_TYPE *
                                  pMplsLdpEntityLdpId,
                                  UINT4 *pu4MplsLdpEntityIndex,
                                  tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId)
{
    UINT1               u1FirstLoopEntry = LDP_TRUE;
    UINT1               u1ValidPeerFound = LDP_FALSE;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT4               u4MplsLdpEntityIndex = 0;

    SNMP_DECLARE_ONE_OCTET_STRING (MplsLdpEntityID)
        * pu4MplsLdpEntityIndex = LDP_ZERO;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    while (LDP_ONE)
    {
        if (u1FirstLoopEntry == LDP_TRUE)
        {
            if (nmhGetFirstIndexMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                                    pu4MplsLdpEntityIndex)
                == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            u1FirstLoopEntry = LDP_FALSE;
        }
        /* Entering the loop for more than once */

        else if (nmhGetNextIndexMplsLdpEntityTable ((tSNMP_OCTET_STRING_TYPE
                                                     *) & MplsLdpEntityID,
                                                    pMplsLdpEntityLdpId,
                                                    u4MplsLdpEntityIndex,
                                                    pu4MplsLdpEntityIndex)
                 == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        CPY_TO_SNMP (&MplsLdpEntityID, pu1LdpEntityId, LDP_MAX_LDPID_LEN);
        u4MplsLdpEntityIndex = *pu4MplsLdpEntityIndex;

        /* Getting the Ldp Entity for the given indices */

        if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                             u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        /* The failure case should never arise as GetNext is always supposed
           to give Next Valid Entity. */

        /* Getting Peer with smallest Peer Index from the Entity's Local
           Peer's List. */

        TMO_SLL_Scan (&pLdpEntity->PeerList, pLdpPeer, tLdpPeer *)
        {
            if ((pLdpPeer->pLdpSession == NULL) &&
                ((pLdpPeer->u1GrProgressState == LDP_PEER_GR_NOT_SUPPORTED) ||
                 (pLdpPeer->u1GrProgressState == LDP_PEER_GR_ABORTED)))
            {
                continue;
            }

            if (u1ValidPeerFound == LDP_FALSE)
            {
                CPY_TO_SNMP (pMplsLdpPeerLdpId, pLdpPeer->PeerLdpId,
                             LDP_MAX_LDPID_LEN);
                u1ValidPeerFound = LDP_TRUE;
                continue;
            }
            if (MEMCMP (pLdpPeer->PeerLdpId, pu1LdpPeerId, LDP_MAX_LDPID_LEN)
                < 0)
            {
                CPY_TO_SNMP (pMplsLdpPeerLdpId, pLdpPeer->PeerLdpId,
                             LDP_MAX_LDPID_LEN);
            }
        }

        if (u1ValidPeerFound == LDP_TRUE)
        {
            break;
        }

        /* Valid Peer couldn't be found for the given Entity. Getting the next
         * valid entity.
         * If No Valid Entity is found, then the next loop entry would
         * result in returning SNMP_FAILURE while trying to get next valid
         * Ldp ENtity.
         */
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpPeerTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpPeerLdpId
                nextMplsLdpPeerLdpId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpPeerTable (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextMplsLdpEntityLdpId,
                                 UINT4 u4MplsLdpEntityIndex,
                                 UINT4 *pu4NextMplsLdpEntityIndex,
                                 tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pNextMplsLdpPeerLdpId)
{
    tSNMP_OCTET_STRING_TYPE MplsLdpEntityTmpPeerId;
    UINT1               au1LdpTmpPeerId[LDP_MAX_LDPID_LEN] = { LDP_ZERO };
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1NextLdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT1               u1NextPeerFound = LDP_FALSE;

    if ((pMplsLdpEntityLdpId->i4_Length < 0) ||
        (pMplsLdpPeerLdpId->i4_Length < 0))
    {
        return SNMP_FAILURE;
    }

    MplsLdpEntityTmpPeerId.pu1_OctetList = au1LdpTmpPeerId;
    MplsLdpEntityTmpPeerId.i4_Length = LDP_MAX_LDPID_LEN;

    *pu4NextMplsLdpEntityIndex = LDP_ZERO;
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1NextLdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    /* Getting the Ldp Entity Entry for the given valid indices */
    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_SUCCESS)
    {
        /* Getting the smallest Peer ID greater than the given Peer ID. */
        TMO_SLL_Scan (&pLdpEntity->PeerList, pLdpPeer, tLdpPeer *)
        {
            if ((pLdpPeer->pLdpSession == NULL) &&
                (pLdpPeer->u1GrProgressState == LDP_PEER_GR_NOT_SUPPORTED))
            {
                continue;
            }

            if ((MEMCMP (pLdpPeer->PeerLdpId, pu1LdpPeerId,
                         LDP_MAX_LDPID_LEN) > 0))
            {
                if (u1NextPeerFound == LDP_FALSE)
                {
                    CPY_TO_SNMP (&MplsLdpEntityTmpPeerId, pLdpPeer->PeerLdpId,
                                 LDP_MAX_LDPID_LEN);
                    u1NextPeerFound = LDP_TRUE;
                    continue;
                }
                if (MEMCMP (pLdpPeer->PeerLdpId,
                            MplsLdpEntityTmpPeerId.pu1_OctetList,
                            LDP_MAX_LDPID_LEN) < 0)
                {
                    CPY_TO_SNMP (&MplsLdpEntityTmpPeerId, pLdpPeer->PeerLdpId,
                                 LDP_MAX_LDPID_LEN);
                }
            }
        }

        if (u1NextPeerFound == LDP_TRUE)
        {
            CPY_TO_SNMP (pNextMplsLdpPeerLdpId,
                         MplsLdpEntityTmpPeerId.pu1_OctetList,
                         LDP_MAX_LDPID_LEN);
            CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pu1LdpEntityId,
                         LDP_MAX_LDPID_LEN);
            *pu4NextMplsLdpEntityIndex = u4MplsLdpEntityIndex;
            return SNMP_SUCCESS;
        }
    }

    while (LDP_ONE)
    {

        /* Getting the valid next Ldp Entity's indices */
        if (nmhGetNextIndexMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                               pNextMplsLdpEntityLdpId,
                                               u4MplsLdpEntityIndex,
                                               pu4NextMplsLdpEntityIndex)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        u4MplsLdpEntityIndex = *pu4NextMplsLdpEntityIndex;
        CPY_TO_SNMP (pMplsLdpEntityLdpId, pu1NextLdpEntityId,
                     LDP_MAX_LDPID_LEN);
        /* Getting the Ldp Entity for the given valid indices */
        if (LdpGetLdpEntity (MPLS_DEF_INCARN,
                             pu1NextLdpEntityId,
                             *pu4NextMplsLdpEntityIndex,
                             &pLdpEntity) == LDP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        /* The failure case should never arise as GetNext is always supposed
           to give Next Valid Ldp Entity */

        /* Getting the Peer with smallest Peer ID from the Ldp Entity's
           Peer's(Local/Remote) List. */

        TMO_SLL_Scan (&pLdpEntity->PeerList, pLdpPeer, tLdpPeer *)
        {
            if ((pLdpPeer->pLdpSession == NULL) &&
                (pLdpPeer->u1GrProgressState == LDP_PEER_GR_NOT_SUPPORTED))
            {
                continue;
            }

            if (u1NextPeerFound == LDP_FALSE)
            {
                CPY_TO_SNMP (&MplsLdpEntityTmpPeerId, pLdpPeer->PeerLdpId,
                             LDP_MAX_LDPID_LEN);
                u1NextPeerFound = LDP_TRUE;
                continue;
            }
            if (MEMCMP (pLdpPeer->PeerLdpId,
                        MplsLdpEntityTmpPeerId.pu1_OctetList,
                        LDP_MAX_LDPID_LEN) < 0)
            {
                CPY_TO_SNMP (&MplsLdpEntityTmpPeerId, pLdpPeer->PeerLdpId,
                             LDP_MAX_LDPID_LEN);
            }
        }

        if (u1NextPeerFound == LDP_TRUE)
        {
            CPY_TO_SNMP (pNextMplsLdpPeerLdpId,
                         MplsLdpEntityTmpPeerId.pu1_OctetList,
                         LDP_MAX_LDPID_LEN);
            break;
        }

        /* Valid Peer couldn't be found in the peer. Getting the next
           valid Entity.
           If No Valid Entity is found, then the next loop entry would
           result in returning SNMP_FAILURE while trying to get next valid
           Entity. */
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpPeerLabelDistMethod
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpPeerLabelDistMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpPeerLabelDistMethod (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                  UINT4 u4MplsLdpEntityIndex,
                                  tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                  INT4 *pi4RetValMplsLdpPeerLabelDistMethod)
{
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpPeerLabelDistMethod =
        (INT4) LDP_PEER_LBL_DIST_METHOD (pLdpPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpPeerPathVectorLimit
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpPeerPathVectorLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpPeerPathVectorLimit (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                  UINT4 u4MplsLdpEntityIndex,
                                  tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                  INT4 *pi4RetValMplsLdpPeerPathVectorLimit)
{
    UINT1               u1SessionFound = LDP_FALSE;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT1              *pu1LdpPeerId =
        SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */

    if (pLdpPeer->pLdpSession != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
        u1SessionFound = LDP_TRUE;
    }
    if (u1SessionFound == LDP_TRUE)
    {
        *pi4RetValMplsLdpPeerPathVectorLimit =
            (INT4) ((pLdpSession->CmnSsnParams).u1PvLimit);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpPeerTransportAddrType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpPeerTransportAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpPeerTransportAddrType (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                    INT4 *pi4RetValMplsLdpPeerTransportAddrType)
{
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (pLdpPeer->pLdpEntity == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpPeerTransportAddrType = pLdpPeer->pLdpEntity->NetAddrType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpPeerTransportAddr
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpPeerTransportAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpPeerTransportAddr (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValMplsLdpPeerTransportAddr)
{
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

#ifdef MPLS_IPV6_WANTED
    if (pLdpPeer->TransAddr.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        CPY_TO_SNMP (pRetValMplsLdpPeerTransportAddr,
                     pLdpPeer->TransAddr.Addr.Ip6Addr.u1_addr,
                     IPV6_ADDR_LENGTH);
    }
    else
    {
#endif

        CPY_TO_SNMP (pRetValMplsLdpPeerTransportAddr,
                     pLdpPeer->TransAddr.Addr.au1Ipv4Addr, IPV4_ADDR_LENGTH);
#ifdef MPLS_IPV6_WANTED
    }
#endif
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsLdpSessionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpSessionTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpSessionTable (tSNMP_OCTET_STRING_TYPE *
                                             pMplsLdpEntityLdpId,
                                             UINT4 u4MplsLdpEntityIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pMplsLdpPeerLdpId)
{
    if (nmhValidateIndexInstanceMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                                  u4MplsLdpEntityIndex,
                                                  pMplsLdpPeerLdpId) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpSessionTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpSessionTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 *pu4MplsLdpEntityIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpPeerLdpId)
{
    *pu4MplsLdpEntityIndex = LDP_ZERO;

    if (nmhGetFirstIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                          pu4MplsLdpEntityIndex,
                                          pMplsLdpPeerLdpId) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpSessionTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpPeerLdpId
                nextMplsLdpPeerLdpId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpSessionTable (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    UINT4 *pu4NextMplsLdpEntityIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpPeerLdpId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextMplsLdpPeerLdpId)
{
    *pu4NextMplsLdpEntityIndex = LDP_ZERO;

    if (nmhGetNextIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                         pNextMplsLdpEntityLdpId,
                                         u4MplsLdpEntityIndex,
                                         pu4NextMplsLdpEntityIndex,
                                         pMplsLdpPeerLdpId,
                                         pNextMplsLdpPeerLdpId) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionStateLastChange
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpSessionStateLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionStateLastChange (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpPeerLdpId,
                                     UINT4
                                     *pu4RetValMplsLdpSessionStateLastChange)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */

    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }
    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpSessionStateLastChange =
        MplsLdpSessionGetSysTime (pLdpSession);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionState
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpSessionState
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionState (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                           UINT4 u4MplsLdpEntityIndex,
                           tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                           INT4 *pi4RetValMplsLdpSessionState)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */

    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }
    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpSessionState = (INT4) pLdpSession->u1SessionState;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionRole
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpSessionRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionRole (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                          UINT4 u4MplsLdpEntityIndex,
                          tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                          INT4 *pi4RetValMplsLdpSessionRole)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */

    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }
    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpSessionRole = pLdpSession->u1SessionRole;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionProtocolVersion
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpSessionProtocolVersion
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionProtocolVersion (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpPeerLdpId,
                                     UINT4
                                     *pu4RetValMplsLdpSessionProtocolVersion)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT1              *pu1LdpPeerId =
        SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */

    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }

    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValMplsLdpSessionProtocolVersion =
        pLdpSession->CmnSsnParams.u2ProtocolVer;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionKeepAliveHoldTimeRem
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpSessionKeepAliveHoldTimeRem
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionKeepAliveHoldTimeRem (tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpPeerLdpId,
                                          INT4
                                          *pi4RetValMplsLdpSessionKeepAliveHoldTimeRem)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT1              *pu1LdpPeerId =
        SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */

    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }

    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TmrGetRemainingTime
        (LDP_TIMER_LIST_ID,
         (tTmrAppTimer *) & (pLdpSession->SsnHoldTimer.AppTimer),
         (UINT4 *) pi4RetValMplsLdpSessionKeepAliveHoldTimeRem) == TMR_SUCCESS)
    {
        *pi4RetValMplsLdpSessionKeepAliveHoldTimeRem
            = (*pi4RetValMplsLdpSessionKeepAliveHoldTimeRem /
               SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionKeepAliveTime
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpSessionKeepAliveTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionKeepAliveTime (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                   UINT4 *pu4RetValMplsLdpSessionKeepAliveTime)
{
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT1              *pu1LdpPeerId =
        SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */
    if (pLdpPeer == NULL)
    {
        return SNMP_FAILURE;
    }
    *pu4RetValMplsLdpSessionKeepAliveTime =
        LDP_PEER_KEEP_ALIVE_HOLD_TIMER (pLdpPeer);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionMaxPduLength
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpSessionMaxPduLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionMaxPduLength (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                  UINT4 u4MplsLdpEntityIndex,
                                  tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                  UINT4 *pu4RetValMplsLdpSessionMaxPduLength)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT1              *pu1LdpPeerId =
        SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */
    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }
    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValMplsLdpSessionMaxPduLength =
        (INT4) ((pLdpSession->CmnSsnParams).u2MaxPduLen);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionDiscontinuityTime
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpSessionDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionDiscontinuityTime (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpPeerLdpId,
                                       UINT4
                                       *pu4RetValMplsLdpSessionDiscontinuityTime)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT1              *pu1LdpPeerId =
        SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */
    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }
    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValMplsLdpSessionDiscontinuityTime =
        (INT4) (pLdpSession->u2DisContTime);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsLdpSessionStatsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpSessionStatsTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpSessionStatsTable (tSNMP_OCTET_STRING_TYPE *
                                                  pMplsLdpEntityLdpId,
                                                  UINT4 u4MplsLdpEntityIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pMplsLdpPeerLdpId)
{
    if (nmhValidateIndexInstanceMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                                  u4MplsLdpEntityIndex,
                                                  pMplsLdpPeerLdpId)
        == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpSessionStatsTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpSessionStatsTable (tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 *pu4MplsLdpEntityIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpPeerLdpId)
{
    *pu4MplsLdpEntityIndex = LDP_ZERO;

    if (nmhGetFirstIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                          pu4MplsLdpEntityIndex,
                                          pMplsLdpPeerLdpId) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpSessionStatsTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpPeerLdpId
                nextMplsLdpPeerLdpId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpSessionStatsTable (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         UINT4 *pu4NextMplsLdpEntityIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpPeerLdpId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextMplsLdpPeerLdpId)
{
    *pu4NextMplsLdpEntityIndex = LDP_ZERO;

    if (nmhGetNextIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                         pNextMplsLdpEntityLdpId,
                                         u4MplsLdpEntityIndex,
                                         pu4NextMplsLdpEntityIndex,
                                         pMplsLdpPeerLdpId,
                                         pNextMplsLdpPeerLdpId) == SNMP_SUCCESS)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionStatsUnknownMesTypeErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpSessionStatsUnknownMesTypeErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionStatsUnknownMesTypeErrors (tSNMP_OCTET_STRING_TYPE *
                                               pMplsLdpEntityLdpId,
                                               UINT4 u4MplsLdpEntityIndex,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pMplsLdpPeerLdpId,
                                               UINT4
                                               *pu4RetValMplsLdpSessionStatsUnknownMesTypeErrors)
{
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT1              *pu1LdpPeerId =
        SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpSession (MPLS_DEF_INCARN,
                          pu1LdpEntityId,
                          u4MplsLdpEntityIndex,
                          pu1LdpPeerId, &pLdpSession) == LDP_SUCCESS)
    {
        *pu4RetValMplsLdpSessionStatsUnknownMesTypeErrors =
            LDP_SESSION_UNKNOWN_MSGTYPE_ERR (pLdpSession);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionStatsUnknownTlvErrors
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId

                The Object 
                retValMplsLdpSessionStatsUnknownTlvErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionStatsUnknownTlvErrors (tSNMP_OCTET_STRING_TYPE *
                                           pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsLdpPeerLdpId,
                                           UINT4
                                           *pu4RetValMplsLdpSessionStatsUnknownTlvErrors)
{
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT1              *pu1LdpPeerId =
        SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpSession (MPLS_DEF_INCARN,
                          pu1LdpEntityId,
                          u4MplsLdpEntityIndex,
                          pu1LdpPeerId, &pLdpSession) == LDP_SUCCESS)
    {
        *pu4RetValMplsLdpSessionStatsUnknownTlvErrors =
            LDP_SESSION_UNKNOWN_TLV_ERR (pLdpSession);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : MplsLdpHelloAdjacencyTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpHelloAdjacencyTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpHelloAdjacencyIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpHelloAdjacencyTable (tSNMP_OCTET_STRING_TYPE *
                                                    pMplsLdpEntityLdpId,
                                                    UINT4 u4MplsLdpEntityIndex,
                                                    tSNMP_OCTET_STRING_TYPE *
                                                    pMplsLdpPeerLdpId,
                                                    UINT4
                                                    u4MplsLdpHelloAdjacencyIndex)
{
    if (nmhValidateIndexInstanceMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                                  u4MplsLdpEntityIndex,
                                                  pMplsLdpPeerLdpId)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* The AdjacencyIndex limit is (1..4294967295), the same as of UINT4.
     * So No validation is done.
     */
    /* Added to suppress warnings */
    UNUSED_PARAM (u4MplsLdpHelloAdjacencyIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpHelloAdjacencyTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpHelloAdjacencyIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpHelloAdjacencyTable (tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            UINT4 *pu4MplsLdpEntityIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpPeerLdpId,
                                            UINT4
                                            *pu4MplsLdpHelloAdjacencyIndex)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT1               u1ValidAdjFound = LDP_FALSE;
    UINT1               u1SessionFound = LDP_FALSE;
    tLdpAdjacency      *pLdpAdjacency = NULL;

    *pu4MplsLdpEntityIndex = LDP_ZERO;
    *pu4MplsLdpHelloAdjacencyIndex = LDP_ZERO;
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (nmhGetFirstIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                          pu4MplsLdpEntityIndex,
                                          pMplsLdpPeerLdpId) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Getting the Ldp Peer for the given indices.
     * Failure case in getting ldp peer will never arise as getpeer
     * is done after GetFirstIndex. GetFirstIndex always returns a valid
     * index of an existing Peer. */

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       *pu4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) == LDP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */

    if (pLdpPeer->pLdpSession != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
        u1SessionFound = LDP_TRUE;
    }

    if (u1SessionFound == LDP_TRUE)
    {
        TMO_SLL_Scan (&pLdpSession->AdjacencyList, pLdpAdjacency,
                      tLdpAdjacency *)
        {
            if (u1ValidAdjFound == LDP_FALSE)
            {
                *pu4MplsLdpHelloAdjacencyIndex = pLdpAdjacency->u4Index;
                u1ValidAdjFound = LDP_TRUE;
                continue;
            }
            if (pLdpAdjacency->u4Index < *pu4MplsLdpHelloAdjacencyIndex)
            {
                *pu4MplsLdpHelloAdjacencyIndex = pLdpAdjacency->u4Index;
            }
        }
    }

    if (u1ValidAdjFound == LDP_TRUE)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpHelloAdjacencyTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpPeerLdpId
                nextMplsLdpPeerLdpId
                MplsLdpHelloAdjacencyIndex
                nextMplsLdpHelloAdjacencyIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpHelloAdjacencyTable (tSNMP_OCTET_STRING_TYPE *
                                           pMplsLdpEntityLdpId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pNextMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4 *pu4NextMplsLdpEntityIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsLdpPeerLdpId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pNextMplsLdpPeerLdpId,
                                           UINT4 u4MplsLdpHelloAdjacencyIndex,
                                           UINT4
                                           *pu4NextMplsLdpHelloAdjacencyIndex)
{
    UINT1               u1NextAdjFound = LDP_FALSE;
    UINT1               u1SessionFound = LDP_FALSE;
    tLdpSession        *pLdpSession = NULL;
    tLdpAdjacency      *pLdpAdjacency = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1NextLdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT1              *pu1NextLdpPeerId = NULL;
    UINT1               au1ZeroPeerLdpId[LDP_MAX_LDPID_LEN];

    MEMSET (au1ZeroPeerLdpId, LDP_ZERO, LDP_MAX_LDPID_LEN);

    if ((pMplsLdpEntityLdpId->i4_Length < 0) ||
        (pMplsLdpPeerLdpId->i4_Length < 0))
    {
        return SNMP_FAILURE;
    }

    *pu4NextMplsLdpEntityIndex = LDP_ZERO;
    *pu4NextMplsLdpHelloAdjacencyIndex = LDP_ZERO;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1NextLdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    pu1NextLdpPeerId = SNMP_OCTET_STRING_LIST (pNextMplsLdpPeerLdpId);

    /* Getting the Session entry for the given valid indices */

    if (MEMCMP (pu1LdpPeerId, au1ZeroPeerLdpId, LDP_MAX_LDPID_LEN) != LDP_ZERO)
    {
        if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                           u4MplsLdpEntityIndex,
                           pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        /* Getting the Session from the peer */
        if (pLdpPeer->pLdpSession != NULL)
        {
            pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
            u1SessionFound = LDP_TRUE;
        }
        if (u1SessionFound == LDP_FALSE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                             pNextMplsLdpEntityLdpId,
                                             u4MplsLdpEntityIndex,
                                             pu4NextMplsLdpEntityIndex,
                                             pMplsLdpPeerLdpId,
                                             pNextMplsLdpPeerLdpId)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        u4MplsLdpEntityIndex = *pu4NextMplsLdpEntityIndex;
        MEMCPY (pu1LdpEntityId, pu1NextLdpEntityId, LDP_MAX_LDPID_LEN);
        MEMCPY (pu1LdpPeerId, pu1NextLdpPeerId, LDP_MAX_LDPID_LEN);

        pLdpPeer = NULL;

        if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                           u4MplsLdpEntityIndex,
                           pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (pLdpPeer->pLdpSession != NULL)
        {
            pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
            u1SessionFound = LDP_TRUE;
        }

        if (u1SessionFound == LDP_FALSE)
        {
            return SNMP_FAILURE;
        }
    }

    /* Getting the smallest Adjacency Index greater than the given Adjacency
     * Index. 
     */
    TMO_SLL_Scan (&pLdpSession->AdjacencyList, pLdpAdjacency, tLdpAdjacency *)
    {
        if (pLdpAdjacency->u4Index > u4MplsLdpHelloAdjacencyIndex)
        {
            if (u1NextAdjFound == LDP_FALSE)
            {
                *pu4NextMplsLdpHelloAdjacencyIndex = pLdpAdjacency->u4Index;
                u1NextAdjFound = LDP_TRUE;
                continue;
            }
            if (pLdpAdjacency->u4Index < *pu4NextMplsLdpHelloAdjacencyIndex)
            {
                *pu4NextMplsLdpHelloAdjacencyIndex = pLdpAdjacency->u4Index;
            }
        }
    }

    if (u1NextAdjFound == LDP_TRUE)
    {
        *pu4NextMplsLdpEntityIndex = u4MplsLdpEntityIndex;
        CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pu1LdpEntityId,
                     LDP_MAX_LDPID_LEN);
        CPY_TO_SNMP (pNextMplsLdpPeerLdpId, pu1LdpPeerId, LDP_MAX_LDPID_LEN);
        return SNMP_SUCCESS;
    }

    /* Adjacency couldn't be found with Index greater then the given 
     * Adjacency Index. 
     */

    /* Getting the next valid Peer indices */
    if (nmhGetNextIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                         pNextMplsLdpEntityLdpId,
                                         u4MplsLdpEntityIndex,
                                         pu4NextMplsLdpEntityIndex,
                                         pMplsLdpPeerLdpId,
                                         pNextMplsLdpPeerLdpId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    u4MplsLdpEntityIndex = *pu4NextMplsLdpEntityIndex;
    MEMCPY (pu1LdpEntityId, pu1NextLdpEntityId, LDP_MAX_LDPID_LEN);
    MEMCPY (pu1LdpPeerId, pu1NextLdpPeerId, LDP_MAX_LDPID_LEN);

    /* Getting the Ldp Peer for the given indices.
     * Failure case in getting ldp peer will never arise as getpeer
     * is done after GetNextIndex. GetNextIndex always returns a valid
     * index of an existing Peer. */

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) == LDP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */
    u1SessionFound = LDP_FALSE;

    /* Getting the Session from the peer */
    if (pLdpPeer->pLdpSession != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
        u1SessionFound = LDP_TRUE;
    }

    /* Getting the Adjacency with smallest Adjacency Index from the Session's
     * Adjacency List. 
     */
    if (u1SessionFound == LDP_TRUE)
    {
        TMO_SLL_Scan (&(pLdpSession->AdjacencyList), pLdpAdjacency,
                      tLdpAdjacency *)
        {
            if (u1NextAdjFound == LDP_FALSE)
            {
                *pu4NextMplsLdpHelloAdjacencyIndex = pLdpAdjacency->u4Index;
                u1NextAdjFound = LDP_TRUE;
                continue;
            }
            if (pLdpAdjacency->u4Index < *pu4NextMplsLdpHelloAdjacencyIndex)
            {
                *pu4NextMplsLdpHelloAdjacencyIndex = pLdpAdjacency->u4Index;
            }
        }
    }

    if (u1NextAdjFound == LDP_TRUE)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpHelloAdjacencyHoldTimeRem
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpHelloAdjacencyIndex

                The Object 
                retValMplsLdpHelloAdjacencyHoldTimeRem
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpHelloAdjacencyHoldTimeRem (tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpPeerLdpId,
                                        UINT4 u4MplsLdpHelloAdjacencyIndex,
                                        INT4
                                        *pi4RetValMplsLdpHelloAdjacencyHoldTimeRem)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLdpAdjacency      *pLdpAdjacency = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT1              *pu1LdpPeerId =
        SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Getting the Session from the peer */
    if (pLdpPeer->pLdpSession != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }

    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pLdpSession->AdjacencyList), pLdpAdjacency, tLdpAdjacency *)
    {
        if (pLdpAdjacency->u4Index == u4MplsLdpHelloAdjacencyIndex)
        {
            if (TmrGetRemainingTime
                (LDP_TIMER_LIST_ID,
                 (tTmrAppTimer *) & (pLdpAdjacency->PeerAdjTimer.AppTimer),
                 (UINT4 *) pi4RetValMplsLdpHelloAdjacencyHoldTimeRem)
                == TMR_SUCCESS)
            {
                *pi4RetValMplsLdpHelloAdjacencyHoldTimeRem =
                    (*pi4RetValMplsLdpHelloAdjacencyHoldTimeRem
                     / SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpHelloAdjacencyHoldTime
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpHelloAdjacencyIndex

                The Object 
                retValMplsLdpHelloAdjacencyHoldTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpHelloAdjacencyHoldTime (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpPeerLdpId,
                                     UINT4 u4MplsLdpHelloAdjacencyIndex,
                                     UINT4
                                     *pu4RetValMplsLdpHelloAdjacencyHoldTime)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLdpAdjacency      *pLdpAdjacency = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    UINT1              *pu1LdpPeerId =
        SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Accessing the Session that is created from this peer */

    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }
    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pLdpSession->AdjacencyList), pLdpAdjacency, tLdpAdjacency *)
    {
        if (pLdpAdjacency->u4Index == u4MplsLdpHelloAdjacencyIndex)
        {
            *pu4RetValMplsLdpHelloAdjacencyHoldTime =
                pLdpPeer->pLdpEntity->u2HelloHoldTime;
            return SNMP_SUCCESS;

        }
    }
    return SNMP_FAILURE;
}

/*************************************************************************** 
 Function    :  nmhGetMplsLdpHelloAdjacencyType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpHelloAdjacencyIndex

                The Object 
                retValMplsLdpHelloAdjacencyType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpHelloAdjacencyType (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                 UINT4 u4MplsLdpEntityIndex,
                                 tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                 UINT4 u4MplsLdpHelloAdjacencyIndex,
                                 INT4 *pi4RetValMplsLdpHelloAdjacencyType)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    tLdpAdjacency      *pLdpAdjacency = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex, pu1LdpPeerId,
                       &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */

    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }
    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pLdpSession->AdjacencyList), pLdpAdjacency, tLdpAdjacency *)
    {
        if (pLdpAdjacency->u4Index == u4MplsLdpHelloAdjacencyIndex)
        {
            *pi4RetValMplsLdpHelloAdjacencyType =
                ((ADJ_GET_PEER_TYPE (pLdpAdjacency) == LDP_TRUE) ?
                 LDP_LOCAL_PEER : LDP_REMOTE_PEER);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : MplsInSegmentLdpLspTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsInSegmentLdpLspTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsInSegmentLdpLspIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsInSegmentLdpLspTable (tSNMP_OCTET_STRING_TYPE *
                                                  pMplsLdpEntityLdpId,
                                                  UINT4 u4MplsLdpEntityIndex,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pMplsLdpPeerLdpId,
                                                  tSNMP_OCTET_STRING_TYPE *
                                                  pMplsInSegmentLdpLspIndex)
{
    UINT4               u4InSegmentLdpLspIndex;

    if (nmhValidateIndexInstanceMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                                  u4MplsLdpEntityIndex,
                                                  pMplsLdpPeerLdpId)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentLdpLspIndex,
                                 u4InSegmentLdpLspIndex);
    if ((u4InSegmentLdpLspIndex > LDP_ZERO)
        && (u4InSegmentLdpLspIndex <= LDP_MAX_LSPS (MPLS_DEF_INCARN)))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsInSegmentLdpLspTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsInSegmentLdpLspIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsInSegmentLdpLspTable (tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 *pu4MplsLdpEntityIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpPeerLdpId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsInSegmentLdpLspIndex)
{
    UINT1               u1InLblMinFound = LDP_FALSE;
    tTMO_SLL           *pList = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlk = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT4               u4InLabel = LDP_ZERO;
    UINT4               u4MplsLdpSessionInLabel = 0;
    UINT4               u4MplsLdpSessionInLabelIfIndex = 0;
    UINT4               u4Index = 0;

    SNMP_DECLARE_TWO_OCTET_STRING (LdpEntityId, LdpPeerId);
    *pu4MplsLdpEntityIndex = LDP_ZERO;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (nmhGetFirstIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                          pu4MplsLdpEntityIndex,
                                          pMplsLdpPeerLdpId) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LdpGetLdpSession (MPLS_DEF_INCARN,
                          pu1LdpEntityId,
                          (*pu4MplsLdpEntityIndex),
                          pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    pList = &SSN_ULCB (pLdpSession);
    if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
        (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
        (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
    {
        TMO_SLL_Scan (pList, pUstrCtrlBlk, tUstrLspCtrlBlock *)
        {
            u4InLabel = LDP_ZERO;
            if (LDP_LBL_SPACE_TYPE
                (SSN_GET_ENTITY (UPSTR_USSN (pUstrCtrlBlk))) == LDP_PER_IFACE)
            {
                /* Atm Interface related label space
                 * For Frame relay interface code has to be
                 * modified.
                 */
                u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vpi;
                u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                u4InLabel |= (pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vci);
            }
            else
            {
                u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.u4GenLbl;
            }

            if (u1InLblMinFound == LDP_FALSE)
            {
                u4MplsLdpSessionInLabelIfIndex = pUstrCtrlBlk->u4InComIfIndex;
                u4MplsLdpSessionInLabel = u4InLabel;
                u1InLblMinFound = LDP_TRUE;
                continue;
            }
            if (u1InLblMinFound == LDP_TRUE)
            {
                if ((((pUstrCtrlBlk->u4InComIfIndex) ==
                      u4MplsLdpSessionInLabelIfIndex) &&
                     (u4MplsLdpSessionInLabel > u4InLabel))
                    || ((pUstrCtrlBlk->u4InComIfIndex) <
                        u4MplsLdpSessionInLabelIfIndex))
                {
                    u4MplsLdpSessionInLabelIfIndex =
                        pUstrCtrlBlk->u4InComIfIndex;
                    u4MplsLdpSessionInLabel = u4InLabel;
                    if (MplsSigGetLSRInSegmentIndex
                        (u4MplsLdpSessionInLabelIfIndex,
                         u4MplsLdpSessionInLabel, &u4Index) == MPLS_FAILURE)
                    {

                        return SNMP_FAILURE;
                    }
                    MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                                 pMplsInSegmentLdpLspIndex);
                }
            }
        }
    }
    else
    {
        TMO_SLL_Scan (pList, pLspCtrlBlock, tLspCtrlBlock *)
        {
            u4InLabel = LDP_ZERO;
            if (LDP_LBL_SPACE_TYPE
                (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock))) == LDP_PER_IFACE)
            {
                /* Atm Interface related label space
                 * For Frame relay interface code has to be
                 * modified.
                 */
                u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.AtmLbl.u2Vpi;
                u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                u4InLabel |= (pLspCtrlBlock->UStrLabel.AtmLbl.u2Vci);
            }
            else
            {
                u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.u4GenLbl;
            }

            if (u1InLblMinFound == LDP_FALSE)
            {
                u4MplsLdpSessionInLabelIfIndex = pLspCtrlBlock->u4InComIfIndex;
                u4MplsLdpSessionInLabel = u4InLabel;
                u1InLblMinFound = LDP_TRUE;
                continue;
            }
            if (u1InLblMinFound == LDP_TRUE)
            {
                if ((((pLspCtrlBlock->u4InComIfIndex) ==
                      u4MplsLdpSessionInLabelIfIndex) &&
                     (u4MplsLdpSessionInLabel >
                      u4InLabel))
                    || ((pLspCtrlBlock->u4InComIfIndex) <
                        u4MplsLdpSessionInLabelIfIndex))
                {
                    u4MplsLdpSessionInLabelIfIndex =
                        pLspCtrlBlock->u4InComIfIndex;
                    u4MplsLdpSessionInLabel = u4InLabel;
                    if (MplsSigGetLSRInSegmentIndex
                        (u4MplsLdpSessionInLabelIfIndex,
                         u4MplsLdpSessionInLabel, &u4Index) == MPLS_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                                 pMplsInSegmentLdpLspIndex);
                }
            }
        }
    }

    if (u1InLblMinFound == LDP_TRUE)
    {
        if (MplsSigGetLSRInSegmentIndex (u4MplsLdpSessionInLabelIfIndex,
                                         u4MplsLdpSessionInLabel,
                                         &u4Index) == MPLS_FAILURE)
        {
            return SNMP_FAILURE;
        }
        MPLS_INTEGER_TO_OCTETSTRING (u4Index, pMplsInSegmentLdpLspIndex);
        return SNMP_SUCCESS;
    }

    while (LDP_ONE)
    {

        CPY_TO_SNMP (&LdpEntityId, pu1LdpEntityId, LDP_MAX_LDPID_LEN);
        CPY_TO_SNMP (&LdpPeerId, pu1LdpPeerId, LDP_MAX_LDPID_LEN);

        if (nmhGetNextIndexMplsLdpPeerTable ((tSNMP_OCTET_STRING_TYPE *) &
                                             LdpEntityId,
                                             pMplsLdpEntityLdpId,
                                             *pu4MplsLdpEntityIndex,
                                             pu4MplsLdpEntityIndex,
                                             (tSNMP_OCTET_STRING_TYPE *) &
                                             LdpPeerId,
                                             pMplsLdpPeerLdpId) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (LdpGetLdpSession (MPLS_DEF_INCARN,
                              pu1LdpEntityId,
                              (*pu4MplsLdpEntityIndex),
                              pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        pList = &SSN_ULCB (pLdpSession);
        if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
            (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
            (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
        {
            TMO_SLL_Scan (pList, pUstrCtrlBlk, tUstrLspCtrlBlock *)
            {
                u4InLabel = LDP_ZERO;
                if (LDP_LBL_SPACE_TYPE
                    (SSN_GET_ENTITY (UPSTR_USSN (pUstrCtrlBlk))) ==
                    LDP_PER_IFACE)
                {
                    /* Atm Interface related label space
                     * For Frame relay interface code has to be
                     * modified.
                     */
                    u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vpi;
                    u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                    u4InLabel |= (pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vci);
                }
                else
                {
                    u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.u4GenLbl;
                }

                if (u1InLblMinFound == LDP_FALSE)
                {
                    u4MplsLdpSessionInLabelIfIndex =
                        pUstrCtrlBlk->u4InComIfIndex;
                    u4MplsLdpSessionInLabel = u4InLabel;
                    u1InLblMinFound = LDP_TRUE;
                    continue;
                }
                if (u1InLblMinFound == LDP_TRUE)
                {
                    if ((((pUstrCtrlBlk->u4InComIfIndex) ==
                          u4MplsLdpSessionInLabelIfIndex) &&
                         (u4MplsLdpSessionInLabel > u4InLabel))
                        || ((pUstrCtrlBlk->u4InComIfIndex) <
                            u4MplsLdpSessionInLabelIfIndex))
                    {
                        u4MplsLdpSessionInLabelIfIndex =
                            pUstrCtrlBlk->u4InComIfIndex;
                        u4MplsLdpSessionInLabel = u4InLabel;
                        if (MplsSigGetLSRInSegmentIndex
                            (u4MplsLdpSessionInLabelIfIndex,
                             u4MplsLdpSessionInLabel, &u4Index) == MPLS_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                        MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                                     pMplsInSegmentLdpLspIndex);
                    }
                }
            }

        }
        else
        {
            TMO_SLL_Scan (pList, pLspCtrlBlock, tLspCtrlBlock *)
            {
                u4InLabel = LDP_ZERO;
                if (LDP_LBL_SPACE_TYPE
                    (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock))) ==
                    LDP_PER_IFACE)
                {
                    /* Atm Interface related label space
                     * For Frame relay interface code has to be
                     * modified.
                     */
                    u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.AtmLbl.u2Vpi;
                    u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                    u4InLabel |= (pLspCtrlBlock->UStrLabel.AtmLbl.u2Vci);
                }
                else
                {
                    u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.u4GenLbl;
                }
                if (u1InLblMinFound == LDP_FALSE)
                {
                    u4MplsLdpSessionInLabelIfIndex =
                        pLspCtrlBlock->u4InComIfIndex;
                    u4MplsLdpSessionInLabel = u4InLabel;
                    u1InLblMinFound = LDP_TRUE;
                    continue;
                }
                if (u1InLblMinFound == LDP_TRUE)
                {
                    if ((((pLspCtrlBlock->u4InComIfIndex) ==
                          u4MplsLdpSessionInLabelIfIndex) &&
                         (u4MplsLdpSessionInLabel >
                          u4InLabel))
                        || ((pLspCtrlBlock->u4InComIfIndex) <
                            u4MplsLdpSessionInLabelIfIndex))
                    {
                        u4MplsLdpSessionInLabelIfIndex =
                            pLspCtrlBlock->u4InComIfIndex;
                        u4MplsLdpSessionInLabel = u4InLabel;

                        if (MplsSigGetLSRInSegmentIndex
                            (u4MplsLdpSessionInLabelIfIndex,
                             u4MplsLdpSessionInLabel, &u4Index) == MPLS_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                        MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                                     pMplsInSegmentLdpLspIndex);
                    }
                }
            }
        }
        if (u1InLblMinFound == LDP_TRUE)
        {
            if (MplsSigGetLSRInSegmentIndex
                (u4MplsLdpSessionInLabelIfIndex,
                 u4MplsLdpSessionInLabel, &u4Index) == MPLS_FAILURE)
            {
                return SNMP_FAILURE;
            }
            MPLS_INTEGER_TO_OCTETSTRING (u4Index, pMplsInSegmentLdpLspIndex);
            break;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsInSegmentLdpLspTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpPeerLdpId
                nextMplsLdpPeerLdpId
                MplsInSegmentLdpLspIndex
                nextMplsInSegmentLdpLspIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsInSegmentLdpLspTable (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         UINT4 *pu4NextMplsLdpEntityIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpPeerLdpId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextMplsLdpPeerLdpId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsInSegmentLdpLspIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pNextMplsInSegmentLdpLspIndex)
{
    UINT1               u1InLblMinFound = LDP_FALSE;
    tTMO_SLL           *pList = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlk = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1NextLdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT1              *pu1NextLdpPeerId = NULL;
    UINT4               u4InLabel = LDP_ZERO;
    UINT4               u4MplsLdpSessionInLabelIfIndex = 0;
    UINT4               u4MplsLdpSessionInLabel = 0;
    UINT4               u4NextMplsLdpSessionInLabelIfIndex = 0;
    UINT4               u4NextMplsLdpSessionInLabel = 0;
    UINT4               u4Index = 0;

    if ((pMplsLdpEntityLdpId->i4_Length < 0) ||
        (pMplsLdpPeerLdpId->i4_Length < 0) ||
        (pMplsInSegmentLdpLspIndex->i4_Length < 0))
    {
        return SNMP_FAILURE;
    }

    *pu4NextMplsLdpEntityIndex = LDP_ZERO;
    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentLdpLspIndex, u4Index);
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1NextLdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    pu1NextLdpPeerId = SNMP_OCTET_STRING_LIST (pNextMplsLdpPeerLdpId);

    if (LdpGetLdpSession (MPLS_DEF_INCARN,
                          pu1LdpEntityId,
                          u4MplsLdpEntityIndex,
                          pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MplsSigGetLSRInSegmentInfo (u4Index, &u4MplsLdpSessionInLabelIfIndex,
                                    &u4MplsLdpSessionInLabel) == MPLS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pList = &SSN_ULCB (pLdpSession);
    if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
        (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
        (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
    {
        TMO_SLL_Scan (pList, pUstrCtrlBlk, tUstrLspCtrlBlock *)
        {
            u4InLabel = LDP_ZERO;
            if (LDP_LBL_SPACE_TYPE
                (SSN_GET_ENTITY (UPSTR_USSN (pUstrCtrlBlk))) == LDP_PER_IFACE)
            {
                /* Atm Interface related label space
                 * For Frame relay interface code has to be
                 * modified.
                 */
                u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vpi;
                u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                u4InLabel |= (pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vci);
            }
            else
            {
                u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.u4GenLbl;
            }

            if ((((pUstrCtrlBlk->u4InComIfIndex) ==
                  u4MplsLdpSessionInLabelIfIndex) &&
                 (u4MplsLdpSessionInLabel <
                  u4InLabel)) ||
                ((pUstrCtrlBlk->u4InComIfIndex) >
                 u4MplsLdpSessionInLabelIfIndex))
            {
                if (u1InLblMinFound == LDP_FALSE)
                {
                    u4NextMplsLdpSessionInLabelIfIndex =
                        pUstrCtrlBlk->u4InComIfIndex;
                    u4NextMplsLdpSessionInLabel = u4InLabel;
                    u1InLblMinFound = LDP_TRUE;
                    continue;
                }
                if ((((pUstrCtrlBlk->u4InComIfIndex) ==
                      u4NextMplsLdpSessionInLabelIfIndex) &&
                     (u4NextMplsLdpSessionInLabel >
                      u4InLabel))
                    || ((pUstrCtrlBlk->u4InComIfIndex) <
                        u4NextMplsLdpSessionInLabelIfIndex))
                {
                    u4NextMplsLdpSessionInLabelIfIndex =
                        pUstrCtrlBlk->u4InComIfIndex;
                    u4NextMplsLdpSessionInLabel = u4InLabel;
                    if (MplsSigGetLSRInSegmentIndex
                        (u4NextMplsLdpSessionInLabelIfIndex,
                         u4NextMplsLdpSessionInLabel, &u4Index) == MPLS_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                                 pNextMplsInSegmentLdpLspIndex);
                }
            }
        }
    }
    else
    {
        TMO_SLL_Scan (pList, pLspCtrlBlock, tLspCtrlBlock *)
        {
            u4InLabel = LDP_ZERO;
            if (LDP_LBL_SPACE_TYPE
                (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock))) == LDP_PER_IFACE)
            {
                /* Atm Interface related label space
                 * For Frame relay interface code has to be
                 * modified.
                 */
                u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.AtmLbl.u2Vpi;
                u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                u4InLabel |= (pLspCtrlBlock->UStrLabel.AtmLbl.u2Vci);
            }
            else
            {
                u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.u4GenLbl;
            }

            if ((((pLspCtrlBlock->u4InComIfIndex) ==
                  u4MplsLdpSessionInLabelIfIndex) &&
                 (u4MplsLdpSessionInLabel <
                  u4InLabel)) ||
                ((pLspCtrlBlock->u4InComIfIndex) >
                 u4MplsLdpSessionInLabelIfIndex))
            {
                if (u1InLblMinFound == LDP_FALSE)
                {
                    u4NextMplsLdpSessionInLabelIfIndex =
                        pLspCtrlBlock->u4InComIfIndex;
                    u4NextMplsLdpSessionInLabel = u4InLabel;
                    u1InLblMinFound = LDP_TRUE;
                    continue;
                }
                if ((((pLspCtrlBlock->u4InComIfIndex) ==
                      u4NextMplsLdpSessionInLabelIfIndex) &&
                     (u4NextMplsLdpSessionInLabel > u4InLabel))
                    || ((pLspCtrlBlock->u4InComIfIndex) <
                        u4NextMplsLdpSessionInLabelIfIndex))
                {
                    u4NextMplsLdpSessionInLabelIfIndex =
                        pLspCtrlBlock->u4InComIfIndex;
                    u4NextMplsLdpSessionInLabel = u4InLabel;
                    if (MplsSigGetLSRInSegmentIndex
                        (u4NextMplsLdpSessionInLabelIfIndex,
                         u4NextMplsLdpSessionInLabel, &u4Index) == MPLS_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                                 pNextMplsInSegmentLdpLspIndex);

                }
            }
        }
    }
    if (u1InLblMinFound == LDP_TRUE)
    {
        CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pu1LdpEntityId,
                     LDP_MAX_LDPID_LEN);
        *pu4NextMplsLdpEntityIndex = u4MplsLdpEntityIndex;
        CPY_TO_SNMP (pNextMplsLdpPeerLdpId, pu1LdpPeerId, LDP_MAX_LDPID_LEN);
        if (MplsSigGetLSRInSegmentIndex (u4NextMplsLdpSessionInLabelIfIndex,
                                         u4NextMplsLdpSessionInLabel,
                                         &u4Index) == MPLS_FAILURE)
        {
            return SNMP_FAILURE;
        }
        MPLS_INTEGER_TO_OCTETSTRING (u4Index, pNextMplsInSegmentLdpLspIndex);
        return SNMP_SUCCESS;
    }

    while (LDP_ONE)
    {
        if (nmhGetNextIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                             pNextMplsLdpEntityLdpId,
                                             u4MplsLdpEntityIndex,
                                             pu4NextMplsLdpEntityIndex,
                                             pMplsLdpPeerLdpId,
                                             pNextMplsLdpPeerLdpId)
            != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }
        CPY_TO_SNMP (pMplsLdpEntityLdpId, pu1NextLdpEntityId,
                     LDP_MAX_LDPID_LEN);
        u4MplsLdpEntityIndex = *pu4NextMplsLdpEntityIndex;
        CPY_TO_SNMP (pMplsLdpPeerLdpId, pu1NextLdpPeerId, LDP_MAX_LDPID_LEN);

        if (LdpGetLdpSession ((UINT4) MPLS_DEF_INCARN,
                              pu1LdpEntityId,
                              u4MplsLdpEntityIndex,
                              pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        pList = &SSN_ULCB (pLdpSession);

        if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
            (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
            (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
        {

            TMO_SLL_Scan (pList, pUstrCtrlBlk, tUstrLspCtrlBlock *)
            {
                u4InLabel = LDP_ZERO;
                if (LDP_LBL_SPACE_TYPE
                    (SSN_GET_ENTITY (UPSTR_USSN (pUstrCtrlBlk))) ==
                    LDP_PER_IFACE)
                {
                    /* Atm Interface related label space
                     * For Frame relay interface code has to be
                     * modified.
                     */
                    u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vpi;
                    u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                    u4InLabel |= (pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vci);
                }
                else
                {
                    u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.u4GenLbl;
                }

                if (u1InLblMinFound == LDP_FALSE)
                {
                    u4NextMplsLdpSessionInLabelIfIndex =
                        pUstrCtrlBlk->u4InComIfIndex;
                    u4NextMplsLdpSessionInLabel = u4InLabel;
                    u1InLblMinFound = LDP_TRUE;
                    continue;
                }
                if (u1InLblMinFound == LDP_TRUE)
                {
                    if ((((pUstrCtrlBlk->u4InComIfIndex) ==
                          u4NextMplsLdpSessionInLabelIfIndex) &&
                         (u4NextMplsLdpSessionInLabel >
                          u4InLabel))
                        || ((pUstrCtrlBlk->u4InComIfIndex) <
                            u4NextMplsLdpSessionInLabelIfIndex))
                    {
                        u4NextMplsLdpSessionInLabelIfIndex =
                            pUstrCtrlBlk->u4InComIfIndex;
                        u4NextMplsLdpSessionInLabel = u4InLabel;
                        if (MplsSigGetLSRInSegmentIndex
                            (u4NextMplsLdpSessionInLabelIfIndex,
                             u4NextMplsLdpSessionInLabel,
                             &u4Index) == MPLS_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                        MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                                     pNextMplsInSegmentLdpLspIndex);

                    }
                }
            }
        }
        else
        {
            TMO_SLL_Scan (pList, pLspCtrlBlock, tLspCtrlBlock *)
            {
                u4InLabel = LDP_ZERO;
                if (LDP_LBL_SPACE_TYPE
                    (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock))) ==
                    LDP_PER_IFACE)
                {
                    /* Atm Interface related label space
                     * For Frame relay interface code has to be
                     * modified.
                     */
                    u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.AtmLbl.u2Vpi;
                    u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                    u4InLabel |= (pLspCtrlBlock->UStrLabel.AtmLbl.u2Vci);
                }
                else
                {
                    u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.u4GenLbl;
                }

                if (u1InLblMinFound == LDP_FALSE)
                {
                    u4NextMplsLdpSessionInLabelIfIndex =
                        pLspCtrlBlock->u4InComIfIndex;
                    u4NextMplsLdpSessionInLabel = u4InLabel;
                    u1InLblMinFound = LDP_TRUE;
                    continue;
                }
                if (u1InLblMinFound == LDP_TRUE)
                {
                    if ((((pLspCtrlBlock->u4InComIfIndex) ==
                          u4NextMplsLdpSessionInLabelIfIndex) &&
                         (u4NextMplsLdpSessionInLabel >
                          u4InLabel))
                        || ((pLspCtrlBlock->u4InComIfIndex) <
                            u4NextMplsLdpSessionInLabelIfIndex))
                    {
                        u4NextMplsLdpSessionInLabelIfIndex =
                            pLspCtrlBlock->u4InComIfIndex;
                        u4NextMplsLdpSessionInLabel = u4InLabel;

                        if (MplsSigGetLSRInSegmentIndex
                            (u4NextMplsLdpSessionInLabelIfIndex,
                             u4NextMplsLdpSessionInLabel,
                             &u4Index) == MPLS_FAILURE)
                        {
                            return SNMP_FAILURE;
                        }
                        MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                                     pNextMplsInSegmentLdpLspIndex);
                    }
                }
            }
        }
        if (u1InLblMinFound == LDP_TRUE)
        {
            if (MplsSigGetLSRInSegmentIndex (u4NextMplsLdpSessionInLabelIfIndex,
                                             u4NextMplsLdpSessionInLabel,
                                             &u4Index) == MPLS_FAILURE)
            {
                return SNMP_FAILURE;
            }
            MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                         pNextMplsInSegmentLdpLspIndex);
            break;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsInSegmentLdpLspLabelType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsInSegmentLdpLspIndex

                The Object 
                retValMplsInSegmentLdpLspLabelType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentLdpLspLabelType (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsInSegmentLdpLspIndex,
                                    INT4 *pi4RetValMplsInSegmentLdpLspLabelType)
{
    tTMO_SLL           *pList = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlk = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT4               u4InLabel = LDP_ZERO;
    UINT4               u4Index = 0;
    UINT4               u4MplsLdpSessionInLabelIfIndex = 0;
    UINT4               u4MplsLdpSessionInLabel = 0;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentLdpLspIndex, u4Index);

    if (LdpGetLdpSession ((UINT4) MPLS_DEF_INCARN,
                          pu1LdpEntityId,
                          u4MplsLdpEntityIndex,
                          pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MplsSigGetLSRInSegmentInfo (u4Index, &u4MplsLdpSessionInLabelIfIndex,
                                    &u4MplsLdpSessionInLabel) == MPLS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pList = &SSN_ULCB (pLdpSession);
    if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
        (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
        (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
    {
        TMO_SLL_Scan (pList, pUstrCtrlBlk, tUstrLspCtrlBlock *)
        {
            if ((pUstrCtrlBlk->u4InComIfIndex) ==
                u4MplsLdpSessionInLabelIfIndex)
            {
                u4InLabel = LDP_ZERO;
                if (LDP_LBL_SPACE_TYPE
                    (SSN_GET_ENTITY (UPSTR_USSN (pUstrCtrlBlk)))
                    == LDP_PER_IFACE)
                {
                    /* Atm Interface related label space
                     * For Frame relay interface code has to be
                     * modified.
                     */
                    u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vpi;
                    u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                    u4InLabel |= (pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vci);
                }
                else
                {
                    u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.u4GenLbl;
                }

                if (u4MplsLdpSessionInLabel == u4InLabel)
                {
                    *pi4RetValMplsInSegmentLdpLspLabelType =
                        pUstrCtrlBlk->u1InLblType;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    else
    {
        TMO_SLL_Scan (pList, pLspCtrlBlock, tLspCtrlBlock *)
        {
            if ((pLspCtrlBlock->u4InComIfIndex) ==
                u4MplsLdpSessionInLabelIfIndex)
            {
                u4InLabel = LDP_ZERO;
                if (LDP_LBL_SPACE_TYPE
                    (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock))) ==
                    LDP_PER_IFACE)
                {
                    /* Atm Interface related label space
                     * For Frame relay interface code has to be
                     * modified.
                     */
                    u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.AtmLbl.u2Vpi;
                    u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                    u4InLabel |= (pLspCtrlBlock->UStrLabel.AtmLbl.u2Vci);
                }
                else
                {
                    u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.u4GenLbl;
                }

                if (u4MplsLdpSessionInLabel == u4InLabel)
                {
                    *pi4RetValMplsInSegmentLdpLspLabelType =
                        pLspCtrlBlock->u1InLblType;
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsInSegmentLdpLspType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsInSegmentLdpLspIndex

                The Object 
                retValMplsInSegmentLdpLspType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentLdpLspType (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                               UINT4 u4MplsLdpEntityIndex,
                               tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                               tSNMP_OCTET_STRING_TYPE *
                               pMplsInSegmentLdpLspIndex,
                               INT4 *pi4RetValMplsInSegmentLdpLspType)
{
    tTMO_SLL           *pList = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlk = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT4               u4InLabel = LDP_ZERO;
    UINT4               u4Index = 0;
    UINT4               u4MplsLdpSessionInLabelIfIndex = 0;
    UINT4               u4MplsLdpSessionInLabel = 0;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentLdpLspIndex, u4Index);

    if (LdpGetLdpSession ((UINT4) MPLS_DEF_INCARN,
                          pu1LdpEntityId,
                          u4MplsLdpEntityIndex,
                          pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MplsSigGetLSRInSegmentInfo (u4Index, &u4MplsLdpSessionInLabelIfIndex,
                                    &u4MplsLdpSessionInLabel) == MPLS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pList = &SSN_ULCB (pLdpSession);
    if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
        (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
        (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
    {
        TMO_SLL_Scan (pList, pUstrCtrlBlk, tUstrLspCtrlBlock *)
        {
            if ((pUstrCtrlBlk->u4InComIfIndex) ==
                u4MplsLdpSessionInLabelIfIndex)
            {
                u4InLabel = LDP_ZERO;
                if (LDP_LBL_SPACE_TYPE
                    (SSN_GET_ENTITY (UPSTR_USSN (pUstrCtrlBlk)))
                    == LDP_PER_IFACE)
                {
                    /* Atm Interface related label space
                     * For Frame relay interface code has to be
                     * modified.
                     */
                    u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vpi;
                    u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                    u4InLabel |= (pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vci);
                }
                else
                {
                    u4InLabel = (UINT4) pUstrCtrlBlk->UStrLabel.u4GenLbl;
                }

                if (u4MplsLdpSessionInLabel == u4InLabel)
                {
                    if (pUstrCtrlBlk->u1LspState == LDP_LSM_ST_EST)
                    {
                        /* If the DownStream Session Pointer is NULL, it 
                         * implies that the LSP terminates in this LSR or 
                         * else the LSR is an intermediate LSR and the Label
                         * connection type is xconnect.
                         */
                        if (UPSTR_DSTR_PTR (pUstrCtrlBlk) == NULL)
                        {
                            *pi4RetValMplsInSegmentLdpLspType =
                                LDP_LSP_TERMINATES;
                        }
                        else
                        {
                            *pi4RetValMplsInSegmentLdpLspType =
                                LDP_LSP_XCONNECT;
                        }
                    }
                    else
                    {
                        *pi4RetValMplsInSegmentLdpLspType =
                            LDP_LSP_STATE_UNKNOWN;
                    }
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    else
    {
        TMO_SLL_Scan (pList, pLspCtrlBlock, tLspCtrlBlock *)
        {
            if ((pLspCtrlBlock->u4InComIfIndex) ==
                u4MplsLdpSessionInLabelIfIndex)
            {
                u4InLabel = LDP_ZERO;
                if (LDP_LBL_SPACE_TYPE
                    (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock)))
                    == LDP_PER_IFACE)
                {
                    /* Atm Interface related label space
                     * For Frame relay interface code has to be
                     * modified.
                     */
                    u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.AtmLbl.u2Vpi;
                    u4InLabel = u4InLabel << LDP_2_BYTE_SHIFT;
                    u4InLabel |= (pLspCtrlBlock->UStrLabel.AtmLbl.u2Vci);
                }
                else
                {
                    u4InLabel = (UINT4) pLspCtrlBlock->UStrLabel.u4GenLbl;
                }

                if (u4MplsLdpSessionInLabel == u4InLabel)
                {
                    if (pLspCtrlBlock->u1LspState == LDP_LSM_ST_EST)
                    {
                        /* If the DownStream Session Pointer is NULL, it 
                         * implies that the LSP terminates in this LSR or else
                         * the LSR is an intermediate LSR and the Label 
                         * connection type is xconnect.
                         */
                        if (pLspCtrlBlock->pDStrSession == NULL)
                        {
                            *pi4RetValMplsInSegmentLdpLspType =
                                LDP_LSP_TERMINATES;
                        }
                        else
                        {
                            *pi4RetValMplsInSegmentLdpLspType =
                                LDP_LSP_XCONNECT;
                        }
                    }
                    else
                    {
                        *pi4RetValMplsInSegmentLdpLspType =
                            LDP_LSP_STATE_UNKNOWN;
                    }
                    return SNMP_SUCCESS;
                }
            }
        }
    }
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : MplsOutSegmentLdpLspTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsOutSegmentLdpLspTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsOutSegmentLdpLspIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsOutSegmentLdpLspTable (tSNMP_OCTET_STRING_TYPE *
                                                   pMplsLdpEntityLdpId,
                                                   UINT4 u4MplsLdpEntityIndex,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pMplsLdpPeerLdpId,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pMplsOutSegmentLdpLspIndex)
{
    UINT4               u4OutSegmentLdpLspIndex;

    if (nmhValidateIndexInstanceMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                                  u4MplsLdpEntityIndex,
                                                  pMplsLdpPeerLdpId)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentLdpLspIndex,
                                 u4OutSegmentLdpLspIndex);
    if ((u4OutSegmentLdpLspIndex > LDP_ZERO)
        && (u4OutSegmentLdpLspIndex <= LDP_MAX_LSPS (MPLS_DEF_INCARN)))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsOutSegmentLdpLspTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsOutSegmentLdpLspIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsOutSegmentLdpLspTable (tSNMP_OCTET_STRING_TYPE *
                                           pMplsLdpEntityLdpId,
                                           UINT4 *pu4MplsLdpEntityIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsLdpPeerLdpId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsOutSegmentLdpLspIndex)
{

    UINT1               u1OutLblMinFound = LDP_FALSE;
    tTMO_SLL           *pList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT4               u4OutLabel = LDP_ZERO;
    UINT4               u4MplsLdpSessionOutLabelIfIndex = 0;
    UINT4               u4MplsLdpSessionOutLabel = 0;
    UINT4               u4Index = 0;

    SNMP_DECLARE_TWO_OCTET_STRING (FsLdpEntityId, FsLdpPeerId)
        * pu4MplsLdpEntityIndex = LDP_ZERO;

    if (nmhGetFirstIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                          pu4MplsLdpEntityIndex,
                                          pMplsLdpPeerLdpId) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpSession (MPLS_DEF_INCARN,
                          pu1LdpEntityId,
                          *pu4MplsLdpEntityIndex,
                          pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    pList = &SSN_DLCB (pLdpSession);
    TMO_SLL_Scan (pList, pSllNode, tTMO_SLL_NODE *)
    {
        pLspCtrlBlock = SLL_TO_LCB (pSllNode);
        u4OutLabel = LDP_ZERO;
        if (LDP_LBL_SPACE_TYPE
            (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))) == LDP_PER_IFACE)
        {
            /* Atm Interface related label space
             * For Frame relay interface code has to be
             * modified.
             */
            u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.AtmLbl.u2Vpi;
            u4OutLabel = u4OutLabel << LDP_2_BYTE_SHIFT;
            u4OutLabel |= (pLspCtrlBlock->DStrLabel.AtmLbl.u2Vci);
        }
        else
        {
            u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.u4GenLbl;
            if (u4OutLabel > gSystemSize.MplsSystemSize.u4MaxLdpLblRange)
            {
                continue;
            }
        }

        if (u1OutLblMinFound == LDP_FALSE)
        {
            u4MplsLdpSessionOutLabelIfIndex = pLspCtrlBlock->u4OutIfIndex;
            u4MplsLdpSessionOutLabel = u4OutLabel;
            u1OutLblMinFound = LDP_TRUE;
            continue;
        }
        if (u1OutLblMinFound == LDP_TRUE)
        {
            if ((((pLspCtrlBlock->u4OutIfIndex) ==
                  u4MplsLdpSessionOutLabelIfIndex) &&
                 (u4MplsLdpSessionOutLabel > u4OutLabel))
                || ((pLspCtrlBlock->u4OutIfIndex) <
                    u4MplsLdpSessionOutLabelIfIndex))
            {
                u4MplsLdpSessionOutLabelIfIndex = pLspCtrlBlock->u4OutIfIndex;
                u4MplsLdpSessionOutLabel = u4OutLabel;
                if ((MplsSigGetLSROutSegmentIndex
                     (u4MplsLdpSessionOutLabelIfIndex, u4MplsLdpSessionOutLabel,
                      &u4Index)) == MPLS_FAILURE)
                {
                    return SNMP_FAILURE;
                }
                MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                             pMplsOutSegmentLdpLspIndex);
            }
        }
    }

    if (u1OutLblMinFound == LDP_TRUE)
    {
        if ((MplsSigGetLSROutSegmentIndex (u4MplsLdpSessionOutLabelIfIndex,
                                           u4MplsLdpSessionOutLabel, &u4Index))
            == MPLS_FAILURE)
        {
            return SNMP_FAILURE;
        }
        MPLS_INTEGER_TO_OCTETSTRING (u4Index, pMplsOutSegmentLdpLspIndex);
        return SNMP_SUCCESS;
    }

    while (LDP_ONE)
    {

        CPY_TO_SNMP (&FsLdpEntityId, pu1LdpEntityId, LDP_MAX_LDPID_LEN);
        CPY_TO_SNMP (&FsLdpPeerId, pu1LdpPeerId, LDP_MAX_LDPID_LEN);

        if (nmhGetNextIndexMplsLdpPeerTable ((tSNMP_OCTET_STRING_TYPE *) &
                                             FsLdpEntityId,
                                             pMplsLdpEntityLdpId,
                                             *pu4MplsLdpEntityIndex,
                                             pu4MplsLdpEntityIndex,
                                             (tSNMP_OCTET_STRING_TYPE *) &
                                             FsLdpPeerId,
                                             pMplsLdpPeerLdpId) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        if (LdpGetLdpSession ((UINT4) MPLS_DEF_INCARN,
                              pu1LdpEntityId,
                              *pu4MplsLdpEntityIndex,
                              pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        pList = &SSN_DLCB (pLdpSession);
        TMO_SLL_Scan (pList, pSllNode, tTMO_SLL_NODE *)
        {
            pLspCtrlBlock = SLL_TO_LCB (pSllNode);
            u4OutLabel = LDP_ZERO;
            if (LDP_LBL_SPACE_TYPE
                (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))) == LDP_PER_IFACE)
            {
                /* Atm Interface related label space
                 * For Frame relay interface code has to be
                 * modified.
                 */
                u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.AtmLbl.u2Vpi;
                u4OutLabel = u4OutLabel << LDP_2_BYTE_SHIFT;
                u4OutLabel |= (pLspCtrlBlock->DStrLabel.AtmLbl.u2Vci);
            }
            else
            {
                u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.u4GenLbl;
            }

            if (u1OutLblMinFound == LDP_FALSE)
            {
                u4MplsLdpSessionOutLabelIfIndex = pLspCtrlBlock->u4OutIfIndex;
                u4MplsLdpSessionOutLabel = u4OutLabel;
                u1OutLblMinFound = LDP_TRUE;
                continue;
            }
            if (u1OutLblMinFound == LDP_TRUE)
            {
                if ((((pLspCtrlBlock->u4OutIfIndex) ==
                      u4MplsLdpSessionOutLabelIfIndex) &&
                     (u4MplsLdpSessionOutLabel > u4OutLabel)) ||
                    ((pLspCtrlBlock->u4OutIfIndex) <
                     u4MplsLdpSessionOutLabelIfIndex))
                {
                    u4MplsLdpSessionOutLabelIfIndex =
                        pLspCtrlBlock->u4OutIfIndex;
                    u4MplsLdpSessionOutLabel = u4OutLabel;
                    if ((MplsSigGetLSROutSegmentIndex
                         (u4MplsLdpSessionOutLabelIfIndex,
                          u4MplsLdpSessionOutLabel, &u4Index)) == MPLS_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                                 pMplsOutSegmentLdpLspIndex);
                }
            }
        }

        if (u1OutLblMinFound == LDP_TRUE)
        {
            if ((MplsSigGetLSROutSegmentIndex (u4MplsLdpSessionOutLabelIfIndex,
                                               u4MplsLdpSessionOutLabel,
                                               &u4Index)) == MPLS_FAILURE)
            {
                return SNMP_FAILURE;
            }
            MPLS_INTEGER_TO_OCTETSTRING (u4Index, pMplsOutSegmentLdpLspIndex);
            break;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsOutSegmentLdpLspTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpPeerLdpId
                nextMplsLdpPeerLdpId
                MplsOutSegmentLdpLspIndex
                nextMplsOutSegmentLdpLspIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsOutSegmentLdpLspTable (tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4 *pu4NextMplsLdpEntityIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpPeerLdpId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextMplsLdpPeerLdpId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsOutSegmentLdpLspIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextMplsOutSegmentLdpLspIndex)
{

    UINT1               u1OutLblMinFound = LDP_FALSE;
    tTMO_SLL           *pList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1NextLdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT1              *pu1NextLdpPeerId = NULL;
    UINT4               u4OutLabel = LDP_ZERO;
    UINT4               u4MplsLdpSessionOutLabelIfIndex = 0;
    UINT4               u4MplsLdpSessionOutLabel = 0;
    UINT4               u4NextMplsLdpSessionOutLabelIfIndex = 0;
    UINT4               u4NextMplsLdpSessionOutLabel = 0;
    UINT4               u4Index = 0;

    if ((pMplsLdpEntityLdpId->i4_Length < 0) ||
        (pMplsLdpPeerLdpId->i4_Length < 0) ||
        (pMplsOutSegmentLdpLspIndex->i4_Length < 0))
    {
        return SNMP_FAILURE;
    }

    *pu4NextMplsLdpEntityIndex = LDP_ZERO;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentLdpLspIndex, u4Index);
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1NextLdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    pu1NextLdpPeerId = SNMP_OCTET_STRING_LIST (pNextMplsLdpPeerLdpId);

    if (LdpGetLdpSession (MPLS_DEF_INCARN,
                          pu1LdpEntityId,
                          u4MplsLdpEntityIndex,
                          pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (MplsSigGetLSROutSegmentInfo (u4Index, &u4MplsLdpSessionOutLabelIfIndex,
                                     &u4MplsLdpSessionOutLabel) == MPLS_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pList = &SSN_DLCB (pLdpSession);
    TMO_SLL_Scan (pList, pSllNode, tTMO_SLL_NODE *)
    {
        pLspCtrlBlock = SLL_TO_LCB (pSllNode);
        u4OutLabel = LDP_ZERO;
        if (LDP_LBL_SPACE_TYPE
            (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))) == LDP_PER_IFACE)
        {
            /* Atm Interface related label space
             * For Frame relay interface code has to be
             * modified.
             */
            u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.AtmLbl.u2Vpi;
            u4OutLabel = u4OutLabel << LDP_2_BYTE_SHIFT;
            u4OutLabel |= (pLspCtrlBlock->DStrLabel.AtmLbl.u2Vci);
        }
        else
        {
            u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.u4GenLbl;
        }
        if ((((pLspCtrlBlock->u4OutIfIndex) ==
              u4MplsLdpSessionOutLabelIfIndex) &&
             (u4MplsLdpSessionOutLabel < u4OutLabel)) ||
            ((pLspCtrlBlock->u4OutIfIndex) > u4MplsLdpSessionOutLabelIfIndex))
        {
            if (u1OutLblMinFound == LDP_FALSE)
            {
                u4NextMplsLdpSessionOutLabelIfIndex =
                    pLspCtrlBlock->u4OutIfIndex;
                u4NextMplsLdpSessionOutLabel = u4OutLabel;
                u1OutLblMinFound = LDP_TRUE;
                continue;
            }
            if ((((pLspCtrlBlock->u4OutIfIndex) ==
                  u4NextMplsLdpSessionOutLabelIfIndex) &&
                 (u4NextMplsLdpSessionOutLabel > u4OutLabel)) ||
                ((pLspCtrlBlock->u4OutIfIndex) <
                 u4NextMplsLdpSessionOutLabelIfIndex))
            {
                u4NextMplsLdpSessionOutLabelIfIndex =
                    pLspCtrlBlock->u4OutIfIndex;
                u4NextMplsLdpSessionOutLabel = u4OutLabel;
                if ((MplsSigGetLSROutSegmentIndex
                     (u4NextMplsLdpSessionOutLabelIfIndex,
                      u4NextMplsLdpSessionOutLabel, &u4Index)) == MPLS_FAILURE)
                {
                    return SNMP_FAILURE;
                }
                MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                             pNextMplsOutSegmentLdpLspIndex);
            }
        }
    }

    if (u1OutLblMinFound == LDP_TRUE)
    {
        if ((MplsSigGetLSROutSegmentIndex (u4NextMplsLdpSessionOutLabelIfIndex,
                                           u4NextMplsLdpSessionOutLabel,
                                           &u4Index)) == MPLS_FAILURE)
        {
            return SNMP_FAILURE;
        }
        MPLS_INTEGER_TO_OCTETSTRING (u4Index, pNextMplsOutSegmentLdpLspIndex);
        CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pu1LdpEntityId,
                     LDP_MAX_LDPID_LEN);
        *pu4NextMplsLdpEntityIndex = u4MplsLdpEntityIndex;
        CPY_TO_SNMP (pNextMplsLdpPeerLdpId, pu1LdpPeerId, LDP_MAX_LDPID_LEN);
        return SNMP_SUCCESS;
    }

    while (LDP_ONE)
    {
        if (nmhGetNextIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                             pNextMplsLdpEntityLdpId,
                                             u4MplsLdpEntityIndex,
                                             pu4NextMplsLdpEntityIndex,
                                             pMplsLdpPeerLdpId,
                                             pNextMplsLdpPeerLdpId)
            != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        u4MplsLdpEntityIndex = *pu4NextMplsLdpEntityIndex;
        CPY_TO_SNMP (pMplsLdpEntityLdpId, pu1NextLdpEntityId,
                     LDP_MAX_LDPID_LEN);
        CPY_TO_SNMP (pMplsLdpPeerLdpId, pu1NextLdpPeerId, LDP_MAX_LDPID_LEN);

        if (LdpGetLdpSession (MPLS_DEF_INCARN,
                              pu1LdpEntityId,
                              u4MplsLdpEntityIndex,
                              pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        pList = &SSN_DLCB (pLdpSession);
        TMO_SLL_Scan (pList, pSllNode, tTMO_SLL_NODE *)
        {
            pLspCtrlBlock = SLL_TO_LCB (pSllNode);
            u4OutLabel = LDP_ZERO;
            if (LDP_LBL_SPACE_TYPE
                (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))) == LDP_PER_IFACE)
            {
                /* Atm Interface related label space
                 * For Frame relay interface code has to be
                 * modified.
                 */
                u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.AtmLbl.u2Vpi;
                u4OutLabel = u4OutLabel << LDP_2_BYTE_SHIFT;
                u4OutLabel |= (pLspCtrlBlock->DStrLabel.AtmLbl.u2Vci);
            }
            else
            {
                u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.u4GenLbl;
            }

            if (u1OutLblMinFound == LDP_FALSE)
            {
                u4NextMplsLdpSessionOutLabelIfIndex =
                    pLspCtrlBlock->u4OutIfIndex;
                u4NextMplsLdpSessionOutLabel = u4OutLabel;
                u1OutLblMinFound = LDP_TRUE;
                continue;
            }
            if (u1OutLblMinFound == LDP_TRUE)
            {
                if ((((pLspCtrlBlock->u4OutIfIndex) ==
                      u4NextMplsLdpSessionOutLabelIfIndex) &&
                     (u4NextMplsLdpSessionOutLabel > u4OutLabel))
                    || ((pLspCtrlBlock->u4OutIfIndex) <
                        u4NextMplsLdpSessionOutLabelIfIndex))
                {
                    u4NextMplsLdpSessionOutLabelIfIndex =
                        pLspCtrlBlock->u4OutIfIndex;
                    u4NextMplsLdpSessionOutLabel = u4OutLabel;
                    if ((MplsSigGetLSROutSegmentIndex
                         (u4NextMplsLdpSessionOutLabelIfIndex,
                          u4NextMplsLdpSessionOutLabel, &u4Index)) ==
                        MPLS_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                                 pNextMplsOutSegmentLdpLspIndex);
                }
            }
        }

        if (u1OutLblMinFound == LDP_TRUE)
        {
            if ((MplsSigGetLSROutSegmentIndex
                 (u4NextMplsLdpSessionOutLabelIfIndex,
                  u4NextMplsLdpSessionOutLabel, &u4Index)) == MPLS_FAILURE)
            {
                return SNMP_FAILURE;
            }
            MPLS_INTEGER_TO_OCTETSTRING (u4Index,
                                         pNextMplsOutSegmentLdpLspIndex);
            break;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentLdpLspLabelType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsOutSegmentLdpLspIndex

                The Object 
                retValMplsOutSegmentLdpLspLabelType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentLdpLspLabelType (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpPeerLdpId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsOutSegmentLdpLspIndex,
                                     INT4
                                     *pi4RetValMplsOutSegmentLdpLspLabelType)
{
    tTMO_SLL           *pList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT4               u4OutLabel = 0;
    UINT4               u4Index = 0;
    UINT4               u4MplsLdpSessionOutLabelIfIndex = 0;
    UINT4               u4MplsLdpSessionOutLabel = 0;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentLdpLspIndex, u4Index);
    if (LdpGetLdpSession (MPLS_DEF_INCARN,
                          pu1LdpEntityId,
                          u4MplsLdpEntityIndex,
                          pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MplsSigGetLSROutSegmentInfo (u4Index, &u4MplsLdpSessionOutLabelIfIndex,
                                     &u4MplsLdpSessionOutLabel) == MPLS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pList = &SSN_DLCB (pLdpSession);
    TMO_SLL_Scan (pList, pSllNode, tTMO_SLL_NODE *)
    {
        pLspCtrlBlock = SLL_TO_LCB (pSllNode);
        if ((pLspCtrlBlock->u4OutIfIndex) == u4MplsLdpSessionOutLabelIfIndex)
        {
            u4OutLabel = LDP_ZERO;
            if (LDP_LBL_SPACE_TYPE
                (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))) == LDP_PER_IFACE)
            {
                /* Atm Interface related label space
                 * For Frame relay interface code has to be
                 * modified.
                 */
                u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.AtmLbl.u2Vpi;
                u4OutLabel = u4OutLabel << LDP_2_BYTE_SHIFT;
                u4OutLabel |= (pLspCtrlBlock->DStrLabel.AtmLbl.u2Vci);
            }
            else
            {
                u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.u4GenLbl;
            }

            if (u4MplsLdpSessionOutLabel == u4OutLabel)
            {
                *pi4RetValMplsOutSegmentLdpLspLabelType =
                    pLspCtrlBlock->u1OutLblType;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentLdpLspType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsOutSegmentLdpLspIndex

                The Object 
                retValMplsOutSegmentLdpLspType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentLdpLspType (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                tSNMP_OCTET_STRING_TYPE *
                                pMplsOutSegmentLdpLspIndex,
                                INT4 *pi4RetValMplsOutSegmentLdpLspType)
{
    tTMO_SLL           *pList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT4               u4OutLabel = LDP_ZERO;
    UINT4               u4MplsLdpSessionOutLabelIfIndex = 0;
    UINT4               u4MplsLdpSessionOutLabel = 0;
    UINT4               u4Index = 0;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentLdpLspIndex, u4Index);

    if (LdpGetLdpSession (MPLS_DEF_INCARN,
                          pu1LdpEntityId,
                          u4MplsLdpEntityIndex,
                          pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (MplsSigGetLSROutSegmentInfo (u4Index, &u4MplsLdpSessionOutLabelIfIndex,
                                     &u4MplsLdpSessionOutLabel) == MPLS_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pList = &SSN_DLCB (pLdpSession);
    TMO_SLL_Scan (pList, pSllNode, tTMO_SLL_NODE *)
    {
        pLspCtrlBlock = SLL_TO_LCB (pSllNode);
        if ((pLspCtrlBlock->u4OutIfIndex) == u4MplsLdpSessionOutLabelIfIndex)
        {
            u4OutLabel = LDP_ZERO;
            if (LDP_LBL_SPACE_TYPE
                (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))) == LDP_PER_IFACE)
            {
                /* Atm Interface related label space
                 * For Frame relay interface code has to be
                 * modified.
                 */
                u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.AtmLbl.u2Vpi;
                u4OutLabel = u4OutLabel << LDP_2_BYTE_SHIFT;
                u4OutLabel |= (pLspCtrlBlock->DStrLabel.AtmLbl.u2Vci);
            }
            else
            {
                u4OutLabel = (UINT4) pLspCtrlBlock->DStrLabel.u4GenLbl;
            }

            if (u4MplsLdpSessionOutLabel == u4OutLabel)
            {
                if (pLspCtrlBlock->u1LspState == LDP_LSM_ST_EST)
                {
                    /* If the UpStream Session Pointer is NULL, it implies that
                     * the LSP Starts in this LSR or else the LSR is an
                     * intermediate LSR and the Label connection type is
                     * xconnect.
                     */
                    if (pLspCtrlBlock->pUStrSession == NULL)
                    {
                        *pi4RetValMplsOutSegmentLdpLspType = LDP_LSP_ORIGINATES;
                    }
                    else
                    {
                        *pi4RetValMplsOutSegmentLdpLspType = LDP_LSP_XCONNECT;
                    }
                }
                else
                {
                    *pi4RetValMplsOutSegmentLdpLspType = LDP_LSP_STATE_UNKNOWN;
                }
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsFecLastChange
 Input       :  The Indices

                The Object 
                retValMplsFecLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsFecLastChange (UINT4 *pu4RetValMplsFecLastChange)
{
    *pu4RetValMplsFecLastChange = MplsFecGetSysTime ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsFecIndexNext
 Input       :  The Indices

                The Object 
                retValMplsFecIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsFecIndexNext (UINT4 *pu4RetValMplsFecIndexNext)
{
    *pu4RetValMplsFecIndexNext = LdpFecTableGetIndex ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsFecTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsFecTable
 Input       :  The Indices
                MplsFecIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsFecTable (UINT4 u4MplsFecIndex)
{
    if (LDP_INCARN_ADMIN_STATUS (MPLS_DEF_INCARN) != LDP_ADMIN_UP)
    {
        return SNMP_FAILURE;
    }
    /* Validate the FecTable Index */
    if (u4MplsFecIndex > LDP_ZERO)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsFecTable
 Input       :  The Indices
                MplsFecIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsFecTable (UINT4 *pu4MplsFecIndex)
{
    UINT4               u4IncarnId;
    UINT4               u4FecIndex;

    *pu4MplsFecIndex = LDP_ZERO;

    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    for (u4IncarnId = LDP_ZERO; u4IncarnId < LDP_MAX_INCARN; u4IncarnId++)
    {
        if (LDP_STATICFECPTR (u4IncarnId) == NULL)
        {
            return SNMP_FAILURE;
        }

        for (u4FecIndex = LDP_ONE; u4FecIndex <=
             FsLDPSizingParams[MAX_LDP_FEC_TABLE_SIZING_ID].u4PreAllocatedUnits;
             u4FecIndex++)
        {
            if (LDP_STATICFEC_INDEX (u4IncarnId, u4FecIndex) != LDP_ZERO)
            {
                *pu4MplsFecIndex = u4FecIndex;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsFecTable
 Input       :  The Indices
                MplsFecIndex
                nextMplsFecIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsFecTable (UINT4 u4MplsFecIndex, UINT4 *pu4NextMplsFecIndex)
{
    UINT4               u4IncarnId;
    UINT4               u4FecIndex;

    *pu4NextMplsFecIndex = LDP_ZERO;
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }
    for (u4IncarnId = MPLS_DEF_INCARN; u4IncarnId < LDP_MAX_INCARN;
         ++u4IncarnId)
    {
        if (LDP_STATICFECPTR (u4IncarnId) == NULL)
        {
            return SNMP_FAILURE;
        }

        /* If the u4IncarnId matches with MPLS_DEF_INCARN the search
         * for the Index is made only for the Indexes greater than
         * u4MplsFecIndex
         */
        for (u4FecIndex = (u4IncarnId == u4MplsFecIndex + LDP_ONE);
             u4FecIndex <= FsLDPSizingParams[MAX_LDP_FEC_TABLE_SIZING_ID].
             u4PreAllocatedUnits; u4FecIndex++)
        {
            if (LDP_STATICFEC_INDEX (u4IncarnId, u4FecIndex) != LDP_ZERO)
            {
                *pu4NextMplsFecIndex = u4FecIndex;
                return SNMP_SUCCESS;
            }
        }

        /* For Coverity Fix the below codes have been commented and replaced by above code. 
           If in near future LDP_MAX_INCARN increases then above code should be replaced by 
           below codes */
        /*
           for (u4FecIndex = (u4IncarnId == MPLS_DEF_INCARN) ?
           (u4MplsFecIndex + LDP_ONE) : LDP_ONE;
           u4FecIndex <= FsLDPSizingParams[MAX_LDP_FEC_TABLE_SIZING_ID].
           u4PreAllocatedUnits; u4FecIndex++)
           {
           if (LDP_STATICFEC_INDEX (u4IncarnId, u4FecIndex) != LDP_ZERO)
           {
           *pu4NextMplsFecIndex = u4FecIndex;
           return SNMP_SUCCESS;
           }
           }
         */

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsFecType
 Input       :  The Indices
                MplsFecIndex

                The Object 
                retValMplsFecType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsFecType (UINT4 u4MplsFecIndex, INT4 *pi4RetValMplsFecType)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != LDP_ZERO)
    {
        *pi4RetValMplsFecType =
            (INT4) LDP_STATICFEC_TYPE (MPLS_DEF_INCARN, u4MplsFecIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsFecAddrType
 Input       :  The Indices
                MplsFecIndex

                The Object 
                retValMplsFecAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsFecAddrType (UINT4 u4MplsFecIndex, INT4 *pi4RetValMplsFecAddrType)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != LDP_ZERO)
    {
        *pi4RetValMplsFecAddrType =
            (INT4) LDP_STATICFEC_ADDRFMLY (MPLS_DEF_INCARN, u4MplsFecIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsFecAddr
 Input       :  The Indices
                MplsFecIndex

                The Object 
                retValMplsFecAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsFecAddr (UINT4 u4MplsFecIndex,
                   tSNMP_OCTET_STRING_TYPE * pRetValMplsFecAddr)
{
    tIpv4Addr           ipv4Addr;

    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != LDP_ZERO)
    {
        MEMCPY ((UINT1 *) ipv4Addr,
                (UINT1 *) (LDP_STATICFEC_ADDR (MPLS_DEF_INCARN,
                                               u4MplsFecIndex)),
                IPV4_ADDR_LENGTH);
        CPY_TO_SNMP (pRetValMplsFecAddr, (UINT1 *) ipv4Addr, IPV4_ADDR_LENGTH);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsFecAddrPrefixLength
 Input       :  The Indices
                MplsFecIndex

                The Object 
                retValMplsFecAddrPrefixLength
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsFecAddrPrefixLength (UINT4 u4MplsFecIndex,
                               UINT4 *pu4RetValMplsFecAddrPrefixLength)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != LDP_ZERO)
    {
        *pu4RetValMplsFecAddrPrefixLength =
            (INT4) LDP_STATICFEC_ADDRLEN (MPLS_DEF_INCARN, u4MplsFecIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsFecStorageType
 Input       :  The Indices
                MplsFecIndex

                The Object 
                retValMplsFecStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsFecStorageType (UINT4 u4MplsFecIndex,
                          INT4 *pi4RetValMplsFecStorageType)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != LDP_ZERO)
    {
        *pi4RetValMplsFecStorageType =
            (INT4) LDP_STATICFEC_STORTYPE (MPLS_DEF_INCARN, u4MplsFecIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsFecRowStatus
 Input       :  The Indices
                MplsFecIndex

                The Object 
                retValMplsFecRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsFecRowStatus (UINT4 u4MplsFecIndex, INT4 *pi4RetValMplsFecRowStatus)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != LDP_ZERO)
    {
        *pi4RetValMplsFecRowStatus =
            (INT4) LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN, u4MplsFecIndex);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsFecType
 Input       :  The Indices
                MplsFecIndex

                The Object 
                setValMplsFecType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsFecType (UINT4 u4MplsFecIndex, INT4 i4SetValMplsFecType)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != 0)
    {
        if ((LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                      u4MplsFecIndex) == NOT_READY) ||
            (LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                      u4MplsFecIndex) == NOT_IN_SERVICE))
        {
            LDP_STATICFEC_TYPE (MPLS_DEF_INCARN,
                                u4MplsFecIndex) = (UINT1) i4SetValMplsFecType;
            MplsFecUpdateSysTime ();
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsFecAddrType
 Input       :  The Indices
                MplsFecIndex

                The Object 
                setValMplsFecAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsFecAddrType (UINT4 u4MplsFecIndex, INT4 i4SetValMplsFecAddrType)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != 0)
    {
        if ((LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                      u4MplsFecIndex) == NOT_READY) ||
            (LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                      u4MplsFecIndex) == NOT_IN_SERVICE))
        {
            LDP_STATICFEC_ADDRFMLY (MPLS_DEF_INCARN,
                                    u4MplsFecIndex)
                = (UINT2) i4SetValMplsFecAddrType;
            MplsFecUpdateSysTime ();
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsFecAddr
 Input       :  The Indices
                MplsFecIndex

                The Object 
                setValMplsFecAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsFecAddr (UINT4 u4MplsFecIndex,
                   tSNMP_OCTET_STRING_TYPE * pSetValMplsFecAddr)
{
    tIpv4Addr           ipv4Addr;

    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != 0)
    {
        if ((LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                      u4MplsFecIndex) == NOT_READY) ||
            (LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                      u4MplsFecIndex) == NOT_IN_SERVICE))
        {
            CPY_FROM_SNMP ((UINT1 *) ipv4Addr, pSetValMplsFecAddr,
                           IPV4_ADDR_LENGTH);
            MEMCPY ((UINT1 *) (LDP_STATICFEC_ADDR (MPLS_DEF_INCARN,
                                                   u4MplsFecIndex)),
                    (UINT1 *) ipv4Addr, IPV4_ADDR_LENGTH);
            MplsFecUpdateSysTime ();
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsFecAddrPrefixLength
 Input       :  The Indices
                MplsFecIndex

                The Object 
                setValMplsFecAddrPrefixLength
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsFecAddrPrefixLength (UINT4 u4MplsFecIndex,
                               UINT4 u4SetValMplsFecAddrPrefixLength)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != 0)
    {
        if ((LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                      u4MplsFecIndex) == NOT_READY) ||
            (LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                      u4MplsFecIndex) == NOT_IN_SERVICE))
        {
            LDP_STATICFEC_ADDRLEN (MPLS_DEF_INCARN,
                                   u4MplsFecIndex)
                = (UINT1) u4SetValMplsFecAddrPrefixLength;
            MplsFecUpdateSysTime ();
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsFecStorageType
 Input       :  The Indices
                MplsFecIndex

                The Object 
                setValMplsFecStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsFecStorageType (UINT4 u4MplsFecIndex, INT4 i4SetValMplsFecStorageType)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != 0)
    {
        if ((LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                      u4MplsFecIndex) == NOT_READY) ||
            (LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                      u4MplsFecIndex) == NOT_IN_SERVICE))
        {
            LDP_STATICFEC_STORTYPE (MPLS_DEF_INCARN,
                                    u4MplsFecIndex)
                = (UINT1) i4SetValMplsFecStorageType;
            MplsFecUpdateSysTime ();
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsFecRowStatus
 Input       :  The Indices
                MplsFecIndex

                The Object 
                setValMplsFecRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsFecRowStatus (UINT4 u4MplsFecIndex, INT4 i4SetValMplsFecRowStatus)
{
    UINT2               u2IncarnId = (UINT2) (MPLS_DEF_INCARN);

    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (u2IncarnId) == NULL)
    {
        return SNMP_FAILURE;
    }
    switch (i4SetValMplsFecRowStatus)
    {

        case CREATE_AND_WAIT:
            /* Manager has to create Fectable Row by calling the get
             * FecIndexNext function and has to configure that Fec Index.
             */

            /* Here the Label (Fec Index) is allocated and is checked whether
             * the manager has configured the returned value from the get
             * Fec Index. */
            if (LdpFecTableSetIndex (u4MplsFecIndex) != u4MplsFecIndex)
            {
                return SNMP_FAILURE;
            }
            if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) == 0)
            {
                LDP_STATICFEC_INDEX (MPLS_DEF_INCARN,
                                     u4MplsFecIndex) = u4MplsFecIndex;
                LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                         u4MplsFecIndex) = NOT_READY;
                LDP_STATICFEC_ADDRLEN (MPLS_DEF_INCARN,
                                       u4MplsFecIndex) = LDP_ZERO;
                LDP_STATICFEC_STORTYPE (MPLS_DEF_INCARN,
                                        u4MplsFecIndex) =
                    LDP_STORAGE_NONVOLATILE;
                MplsFecUpdateSysTime ();
                return SNMP_SUCCESS;
            }
            LdpFecTableRelIndex (u4MplsFecIndex);
            return SNMP_FAILURE;

        case ACTIVE:
            if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != 0)
            {
                if ((LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                              u4MplsFecIndex)
                     == NOT_IN_SERVICE) ||
                    (LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                              u4MplsFecIndex) == NOT_READY))
                {
                    if (LDP_STATICFEC_ADDRFMLY (MPLS_DEF_INCARN,
                                                u4MplsFecIndex) != IPV4)
                    {
                        return SNMP_FAILURE;
                    }
                    LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                             u4MplsFecIndex) = ACTIVE;
                    LdpSNMPEventHandler (STATIC_LSP_UP_EVENT,
                                         &(LDP_STATICFEC
                                           (MPLS_DEF_INCARN,
                                            u4MplsFecIndex)),
                                         (UINT2) (MPLS_DEF_INCARN));
                    return SNMP_SUCCESS;
                }
            }
            return SNMP_FAILURE;

        case NOT_IN_SERVICE:
            if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != 0)
            {
                if (LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                             u4MplsFecIndex) == ACTIVE)
                {
                    LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                             u4MplsFecIndex) = NOT_IN_SERVICE;
                    LdpSNMPEventHandler (STATIC_LSP_NOTINSERV_EVENT,
                                         &(LDP_STATICFEC
                                           (MPLS_DEF_INCARN,
                                            u4MplsFecIndex)),
                                         (UINT2) MPLS_DEF_INCARN);
                    return SNMP_SUCCESS;
                }
                else if (LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                                  u4MplsFecIndex)
                         == NOT_IN_SERVICE)
                {
                    return SNMP_SUCCESS;
                }
            }
            return SNMP_FAILURE;

        case DESTROY:
            if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != 0)
            {
                if (LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN,
                                             u4MplsFecIndex) == ACTIVE)
                {
                    LdpSNMPEventHandler (STATIC_LSP_DOWN_EVENT,
                                         &(LDP_STATICFEC
                                           (MPLS_DEF_INCARN,
                                            u4MplsFecIndex)),
                                         (UINT2) (MPLS_DEF_INCARN));
                }
                else
                {
                    MEMSET ((VOID *) &(LDP_STATICFEC
                                       (MPLS_DEF_INCARN,
                                        u4MplsFecIndex)), LDP_ZERO,
                            sizeof (tFecTableEntry));
                }
                LdpFecTableRelIndex (u4MplsFecIndex);
                MplsFecUpdateSysTime ();
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;

        case CREATE_AND_GO:
            return SNMP_FAILURE;

        default:
            return SNMP_FAILURE;
    }
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsFecType
 Input       :  The Indices
                MplsFecIndex

                The Object 
                testValMplsFecType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsFecType (UINT4 *pu4ErrorCode, UINT4 u4MplsFecIndex,
                      INT4 i4TestValMplsFecType)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsFecType)
    {
        case LDP_FEC_PREFIX_TYPE - 1:
        case LDP_FEC_HOSTADDR_TYPE - 1:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsFecAddrType
 Input       :  The Indices
                MplsFecIndex

                The Object 
                testValMplsFecAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsFecAddrType (UINT4 *pu4ErrorCode, UINT4 u4MplsFecIndex,
                          INT4 i4TestValMplsFecAddrType)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValMplsFecAddrType == IPV4)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsFecAddr
 Input       :  The Indices
                MplsFecIndex

                The Object 
                testValMplsFecAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsFecAddr (UINT4 *pu4ErrorCode, UINT4 u4MplsFecIndex,
                      tSNMP_OCTET_STRING_TYPE * pTestValMplsFecAddr)
{
    UINT4               u4MplsFecAddr;

    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    CPY_FROM_SNMP ((UINT1 *) &u4MplsFecAddr, pTestValMplsFecAddr,
                   LDP_IPV4ADR_LEN);
    u4MplsFecAddr = OSIX_NTOHL (u4MplsFecAddr);

    if ((u4MplsFecAddr == LDP_NULL_LSRID) || (u4MplsFecAddr == LDP_BCAST_LSRID)
        || ((u4MplsFecAddr >= LDP_MCAST_NET_ADDR)
            && (u4MplsFecAddr <= LDP_MCAST_NET_INV_ADDR)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    if ((LDP_STATICFEC_ROWSTATUS (MPLS_DEF_INCARN, u4MplsFecIndex) == ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsFecAddrPrefixLength
 Input       :  The Indices
                MplsFecIndex

                The Object 
                testValMplsFecAddrPrefixLength
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsFecAddrPrefixLength (UINT4 *pu4ErrorCode, UINT4 u4MplsFecIndex,
                                  UINT4 u4TestValMplsFecAddrPrefixLength)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    /* Added to suppress warning */
    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* The Address Length should be between 0..255 as per the mib. */
    if (u4TestValMplsFecAddrPrefixLength <= LDP_UINT1_SIZE)
    {
        if (u4TestValMplsFecAddrPrefixLength > IPV4_MAX_PRFX_LEN)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
        else
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2MplsFecStorageType
 Input       :  The Indices
                MplsFecIndex

                The Object 
                testValMplsFecStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsFecStorageType (UINT4 *pu4ErrorCode, UINT4 u4MplsFecIndex,
                             INT4 i4TestValMplsFecStorageType)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) == 0)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsFecStorageType)
    {
        case LDP_STORAGE_VOLATILE:
        case LDP_STORAGE_NONVOLATILE:
            break;
        case LDP_STORAGE_OTHER:
        case LDP_STORAGE_PERMANENT:
        case LDP_STORAGE_READONLY:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsFecRowStatus
 Input       :  The Indices
                MplsFecIndex

                The Object 
                testValMplsFecRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsFecRowStatus (UINT4 *pu4ErrorCode, UINT4 u4MplsFecIndex,
                           INT4 i4TestValMplsFecRowStatus)
{
    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_STATICFECPTR (MPLS_DEF_INCARN) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsFecRowStatus)
    {
        case CREATE_AND_WAIT:
            if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            if (LDP_STATICFEC_INDEX (MPLS_DEF_INCARN, u4MplsFecIndex) == 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            break;
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsFecTable
 Input       :  The Indices
                MplsFecIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsFecTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpLspFecLastChange
 Input       :  The Indices

                The Object 
                retValMplsLdpLspFecLastChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpLspFecLastChange (UINT4 *pu4RetValMplsLdpLspFecLastChange)
{
    *pu4RetValMplsLdpLspFecLastChange = MplsLdpLspFecGetSysTime ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsLdpLspFecTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpLspFecTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpLspFecSegment
                MplsLdpLspFecSegmentIndex
                MplsLdpLspFecIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpLspFecTable (tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpPeerLdpId,
                                            INT4 i4MplsLdpLspFecSegment,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpLspFecSegmentIndex,
                                            UINT4 u4MplsLdpLspFecIndex)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pMplsLdpPeerLdpId);
    UNUSED_PARAM (i4MplsLdpLspFecSegment);
    UNUSED_PARAM (pMplsLdpLspFecSegmentIndex);

    if (LDP_INCARN_ADMIN_STATUS (MPLS_DEF_INCARN) != LDP_ADMIN_UP)
    {
        return SNMP_FAILURE;
    }
    /* Validate the FecTable Index */
    if ((u4MplsLdpLspFecIndex > LDP_ZERO)
        && (u4MplsLdpLspFecIndex <= LDP_MAX_LSPS (MPLS_DEF_INCARN)))
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpLspFecTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpLspFecSegment
                MplsLdpLspFecSegmentIndex
                MplsLdpLspFecIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpLspFecTable (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 *pu4MplsLdpEntityIndex,
                                    tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                    INT4 *pi4MplsLdpLspFecSegment,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpLspFecSegmentIndex,
                                    UINT4 *pu4MplsLdpLspFecIndex)
{
    tTMO_SLL           *pList = NULL;
    tLdpSession        *pLdpSession = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlk = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    tTMO_SLL           *pUstrCtrlBlkList = NULL;

    if (nmhGetFirstIndexMplsInSegmentLdpLspTable (pMplsLdpEntityLdpId,
                                                  pu4MplsLdpEntityIndex,
                                                  pMplsLdpPeerLdpId,
                                                  pMplsLdpLspFecSegmentIndex)
        == SNMP_FAILURE)
    {
        if (nmhGetFirstIndexMplsOutSegmentLdpLspTable (pMplsLdpEntityLdpId,
                                                       pu4MplsLdpEntityIndex,
                                                       pMplsLdpPeerLdpId,
                                                       pMplsLdpLspFecSegmentIndex)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        else
        {
            *pi4MplsLdpLspFecSegment = LDP_OUTSEG_FEC;
        }
    }
    else
    {
        *pi4MplsLdpLspFecSegment = LDP_INSEG_FEC;
    }
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    if (LdpGetLdpSession ((UINT4) (MPLS_DEF_INCARN),
                          pu1LdpEntityId,
                          (*pu4MplsLdpEntityIndex),
                          pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (*pi4MplsLdpLspFecSegment == LDP_INSEG_FEC)
    {
        pUstrCtrlBlkList = &SSN_ULCB (pLdpSession);
        TMO_SLL_Scan (pUstrCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
        {
            pUstrCtrlBlk = (tUstrLspCtrlBlock *) pSllNode;
            if (pUstrCtrlBlk->pUpstrSession != NULL)
            {
                if ((*pUstrCtrlBlk->pUpstrSession->pLdpPeer->NetAddr.Addr.
                     au1Ipv4Addr) == *pu1LdpPeerId)
                {
                    if (pUstrCtrlBlk->Fec.u4Index > 0)
                    {
                        *pu4MplsLdpLspFecIndex = pUstrCtrlBlk->Fec.u4Index;
                        return SNMP_SUCCESS;
                    }
                }
            }
        }

    }
    else
    {
        pList = &SSN_DLCB (pLdpSession);
        TMO_SLL_Scan (pList, pSllNode, tTMO_SLL_NODE *)
        {
            pLspCtrlBlock = SLL_TO_LCB (pSllNode);
            if ((*pLspCtrlBlock->pDStrSession->pLdpPeer->NetAddr.Addr.
                 au1Ipv4Addr) == *pu1LdpPeerId)
            {
                if (pLspCtrlBlock->Fec.u4Index > 0)
                {
                    *pu4MplsLdpLspFecIndex = pLspCtrlBlock->Fec.u4Index;
                    return SNMP_SUCCESS;
                }
            }
        }

    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpLspFecTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpPeerLdpId
                nextMplsLdpPeerLdpId
                MplsLdpLspFecSegment
                nextMplsLdpLspFecSegment
                MplsLdpLspFecSegmentIndex
                nextMplsLdpLspFecSegmentIndex
                MplsLdpLspFecIndex
                nextMplsLdpLspFecIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpLspFecTable (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   UINT4 *pu4NextMplsLdpEntityIndex,
                                   tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextMplsLdpPeerLdpId,
                                   INT4 i4MplsLdpLspFecSegment,
                                   INT4 *pi4NextMplsLdpLspFecSegment,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpLspFecSegmentIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextMplsLdpLspFecSegmentIndex,
                                   UINT4 u4MplsLdpLspFecIndex,
                                   UINT4 *pu4NextMplsLdpLspFecIndex)
{

    tTMO_SLL           *pList = NULL;
    tLdpSession        *pLdpSession = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlk = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    if (i4MplsLdpLspFecSegment == LDP_INSEG_FEC)
    {
        if ((nmhGetNextIndexMplsInSegmentLdpLspTable (pMplsLdpEntityLdpId,
                                                      pNextMplsLdpEntityLdpId,
                                                      u4MplsLdpEntityIndex,
                                                      pu4NextMplsLdpEntityIndex,
                                                      pMplsLdpPeerLdpId,
                                                      pNextMplsLdpPeerLdpId,
                                                      pMplsLdpLspFecSegmentIndex,
                                                      pNextMplsLdpLspFecSegmentIndex))
            == SNMP_FAILURE)

        {
            if ((nmhGetNextIndexMplsOutSegmentLdpLspTable (pMplsLdpEntityLdpId,
                                                           pNextMplsLdpEntityLdpId,
                                                           u4MplsLdpEntityIndex,
                                                           pu4NextMplsLdpEntityIndex,
                                                           pMplsLdpPeerLdpId,
                                                           pNextMplsLdpPeerLdpId,
                                                           pMplsLdpLspFecSegmentIndex,
                                                           pNextMplsLdpLspFecSegmentIndex))
                == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            else
            {
                *pi4NextMplsLdpLspFecSegment = LDP_OUTSEG_FEC;
            }
        }
        else
        {
            *pi4NextMplsLdpLspFecSegment = LDP_INSEG_FEC;
        }
    }
    else
    {
        if ((nmhGetNextIndexMplsOutSegmentLdpLspTable (pMplsLdpEntityLdpId,
                                                       pNextMplsLdpEntityLdpId,
                                                       u4MplsLdpEntityIndex,
                                                       pu4NextMplsLdpEntityIndex,
                                                       pMplsLdpPeerLdpId,
                                                       pNextMplsLdpPeerLdpId,
                                                       pMplsLdpLspFecSegmentIndex,
                                                       pNextMplsLdpLspFecSegmentIndex))
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

    }
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pNextMplsLdpPeerLdpId);
    if (LdpGetLdpSession ((UINT4) (MPLS_DEF_INCARN),
                          pu1LdpEntityId,
                          (*pu4NextMplsLdpEntityIndex),
                          pu1LdpPeerId, &pLdpSession) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (*pi4NextMplsLdpLspFecSegment == LDP_INSEG_FEC)
    {
        pList = &SSN_ULCB (pLdpSession);
        TMO_SLL_Scan (pList, pUstrCtrlBlk, tUstrLspCtrlBlock *)
        {
            if (pUstrCtrlBlk->pUpstrSession != NULL)
            {
                if ((*pUstrCtrlBlk->pUpstrSession->pLdpPeer->NetAddr.Addr.
                     au1Ipv4Addr) == *pu1LdpPeerId)
                {
                    if (pUstrCtrlBlk->Fec.u4Index > u4MplsLdpLspFecIndex)
                    {
                        *pu4NextMplsLdpLspFecIndex = pUstrCtrlBlk->Fec.u4Index;
                        return SNMP_SUCCESS;
                    }
                }
            }
        }
    }
    else
    {
        pList = &SSN_DLCB (pLdpSession);
        TMO_SLL_Scan (pList, pSllNode, tTMO_SLL_NODE *)
        {
            pLspCtrlBlock = SLL_TO_LCB (pSllNode);
            if ((*pLspCtrlBlock->pDStrSession->pLdpPeer->NetAddr.Addr.
                 au1Ipv4Addr) == *pu1LdpPeerId)
            {
                if (pLspCtrlBlock->Fec.u4Index > u4MplsLdpLspFecIndex)
                {
                    *pu4NextMplsLdpLspFecIndex = pLspCtrlBlock->Fec.u4Index;
                    return SNMP_SUCCESS;
                }
            }
        }

    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpLspFecStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpLspFecSegment
                MplsLdpLspFecSegmentIndex
                MplsLdpLspFecIndex

                The Object 
                retValMplsLdpLspFecStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpLspFecStorageType (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                INT4 i4MplsLdpLspFecSegment,
                                tSNMP_OCTET_STRING_TYPE *
                                pMplsLdpLspFecSegmentIndex,
                                UINT4 u4MplsLdpLspFecIndex,
                                INT4 *pi4RetValMplsLdpLspFecStorageType)
{
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT4               u4Index = 0;
    tLdpPeer           *pLdpPeer = NULL;
    UINT4               u4IfIndex = 0, u4Label = 0;
    UINT4               u4MplsLdpSessionOutLabelIfIndex = 0;
    UINT4               u4MplsLdpSessionOutLabel = 0;
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLdpLspFecSegmentIndex, u4Index);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4MplsLdpLspFecSegment == LDP_INSEG_FEC)
    {
        if (MplsSigGetLSRInSegmentInfo (u4Index, &u4IfIndex,
                                        &u4Label) == MPLS_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (MplsSigGetLSROutSegmentInfo
            (u4Index, &u4MplsLdpSessionOutLabelIfIndex,
             &u4MplsLdpSessionOutLabel) == MPLS_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    *pi4RetValMplsLdpLspFecStorageType =
        LDP_STATICFEC_LDPLSP_STORTYPE (MPLS_DEF_INCARN, u4MplsLdpLspFecIndex);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsLdpLspFecRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpLspFecSegment
                MplsLdpLspFecSegmentIndex
                MplsLdpLspFecIndex

                The Object 
                retValMplsLdpLspFecRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpLspFecRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                              UINT4 u4MplsLdpEntityIndex,
                              tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                              INT4 i4MplsLdpLspFecSegment,
                              tSNMP_OCTET_STRING_TYPE *
                              pMplsLdpLspFecSegmentIndex,
                              UINT4 u4MplsLdpLspFecIndex,
                              INT4 *pi4RetValMplsLdpLspFecRowStatus)
{

    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT4               u4Index = 0;
    tLdpPeer           *pLdpPeer = NULL;
    UINT4               u4IfIndex = 0, u4Label = 0;
    UINT4               u4MplsLdpSessionOutLabelIfIndex = 0;
    UINT4               u4MplsLdpSessionOutLabel = 0;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLdpLspFecSegmentIndex, u4Index);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (i4MplsLdpLspFecSegment == LDP_INSEG_FEC)
    {
        if (MplsSigGetLSRInSegmentInfo (u4Index, &u4IfIndex,
                                        &u4Label) == MPLS_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (MplsSigGetLSROutSegmentInfo
            (u4Index, &u4MplsLdpSessionOutLabelIfIndex,
             &u4MplsLdpSessionOutLabel) == MPLS_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    *pi4RetValMplsLdpLspFecRowStatus =
        LDP_STATICFEC_LDPLSP_ROWSTATUS (MPLS_DEF_INCARN, u4MplsLdpLspFecIndex);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsLdpLspFecStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpLspFecSegment
                MplsLdpLspFecSegmentIndex
                MplsLdpLspFecIndex

                The Object 
                setValMplsLdpLspFecStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpLspFecStorageType (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                INT4 i4MplsLdpLspFecSegment,
                                tSNMP_OCTET_STRING_TYPE *
                                pMplsLdpLspFecSegmentIndex,
                                UINT4 u4MplsLdpLspFecIndex,
                                INT4 i4SetValMplsLdpLspFecStorageType)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pMplsLdpPeerLdpId);
    UNUSED_PARAM (i4MplsLdpLspFecSegment);
    UNUSED_PARAM (pMplsLdpLspFecSegmentIndex);
    UNUSED_PARAM (u4MplsLdpLspFecIndex);
    UNUSED_PARAM (i4SetValMplsLdpLspFecStorageType);
    return SNMP_SUCCESS;

    /* TODO This table is not supported in code. Yet to be implemented */
}

/****************************************************************************
 Function    :  nmhSetMplsLdpLspFecRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpLspFecSegment
                MplsLdpLspFecSegmentIndex
                MplsLdpLspFecIndex

                The Object 
                setValMplsLdpLspFecRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpLspFecRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                              UINT4 u4MplsLdpEntityIndex,
                              tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                              INT4 i4MplsLdpLspFecSegment,
                              tSNMP_OCTET_STRING_TYPE *
                              pMplsLdpLspFecSegmentIndex,
                              UINT4 u4MplsLdpLspFecIndex,
                              INT4 i4SetValMplsLdpLspFecRowStatus)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pMplsLdpPeerLdpId);
    UNUSED_PARAM (i4MplsLdpLspFecSegment);
    UNUSED_PARAM (pMplsLdpLspFecSegmentIndex);
    UNUSED_PARAM (u4MplsLdpLspFecIndex);
    UNUSED_PARAM (i4SetValMplsLdpLspFecRowStatus);
    return SNMP_SUCCESS;

    /* TODO This table is not supported in code. Yet to be implemented */
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsLdpLspFecStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpLspFecSegment
                MplsLdpLspFecSegmentIndex
                MplsLdpLspFecIndex

                The Object 
                testValMplsLdpLspFecStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpLspFecStorageType (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                   INT4 i4MplsLdpLspFecSegment,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpLspFecSegmentIndex,
                                   UINT4 u4MplsLdpLspFecIndex,
                                   INT4 i4TestValMplsLdpLspFecStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pMplsLdpPeerLdpId);
    UNUSED_PARAM (i4MplsLdpLspFecSegment);
    UNUSED_PARAM (pMplsLdpLspFecSegmentIndex);
    UNUSED_PARAM (u4MplsLdpLspFecIndex);
    UNUSED_PARAM (i4TestValMplsLdpLspFecStorageType);
    return SNMP_SUCCESS;

    /* TODO This table is not supported in code. Yet to be implemented */
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpLspFecRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpLspFecSegment
                MplsLdpLspFecSegmentIndex
                MplsLdpLspFecIndex

                The Object 
                testValMplsLdpLspFecRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpLspFecRowStatus (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                 UINT4 u4MplsLdpEntityIndex,
                                 tSNMP_OCTET_STRING_TYPE * pMplsLdpPeerLdpId,
                                 INT4 i4MplsLdpLspFecSegment,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pMplsLdpLspFecSegmentIndex,
                                 UINT4 u4MplsLdpLspFecIndex,
                                 INT4 i4TestValMplsLdpLspFecRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pMplsLdpPeerLdpId);
    UNUSED_PARAM (i4MplsLdpLspFecSegment);
    UNUSED_PARAM (pMplsLdpLspFecSegmentIndex);
    UNUSED_PARAM (u4MplsLdpLspFecIndex);
    UNUSED_PARAM (i4TestValMplsLdpLspFecRowStatus);
    return SNMP_SUCCESS;

    /* TODO This table is not supported in code. Yet to be implemented */
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsLdpLspFecTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpLspFecSegment
                MplsLdpLspFecSegmentIndex
                MplsLdpLspFecIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsLdpLspFecTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsLdpSessionPeerAddrTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpSessionPeerAddrTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpSessionPeerAddrIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpSessionPeerAddrTable (tSNMP_OCTET_STRING_TYPE *
                                                     pMplsLdpEntityLdpId,
                                                     UINT4 u4MplsLdpEntityIndex,
                                                     tSNMP_OCTET_STRING_TYPE *
                                                     pMplsLdpPeerLdpId,
                                                     UINT4
                                                     u4MplsLdpSessionPeerAddrIndex)
{
    if (nmhValidateIndexInstanceMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                                  u4MplsLdpEntityIndex,
                                                  pMplsLdpPeerLdpId)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Added to suppress warnings */
    UNUSED_PARAM (u4MplsLdpSessionPeerAddrIndex);

    /* The PeerAddressIndex limit is (1..4294967295), the same as of UINT4.
     * So No validation is done.
     */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpSessionPeerAddrTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpSessionPeerAddrIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpSessionPeerAddrTable (tSNMP_OCTET_STRING_TYPE *
                                             pMplsLdpEntityLdpId,
                                             UINT4 *pu4MplsLdpEntityIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pMplsLdpPeerLdpId,
                                             UINT4
                                             *pu4MplsLdpSessionPeerAddrIndex)
{
    tLdpPeer           *pLdpPeer = NULL;
    tPeerIfAdrNode     *pPeerIfAdrNode = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT1               u1ValidPeerAddrFound = LDP_FALSE;
    UINT4               u4SessionPeerAddressIndex = LDP_ZERO;

    SNMP_DECLARE_TWO_OCTET_STRING (LdpEntityId, LdpPeerId)
        pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (nmhGetFirstIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                          pu4MplsLdpEntityIndex,
                                          pMplsLdpPeerLdpId) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Getting the Ldp Peer for the given indices.
     * Failure case in getting ldp peer will never arise as getpeer
     * is done after GetFirstIndex. GetFirstIndex always returns a valid
     * index of an existing Peer. */

    if (LdpGetLdpPeer (MPLS_DEF_INCARN,
                       pu1LdpEntityId,
                       (*pu4MplsLdpEntityIndex), pu1LdpPeerId,
                       &pLdpPeer) == LDP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (LDP_GET_PEER_ADDR_LIST (pLdpPeer), pPeerIfAdrNode,
                  tPeerIfAdrNode *)
    {
        u4SessionPeerAddressIndex = (LDP_OFFSET
                                     (pPeerIfAdrNode))->u4SessionPeerAddrIndex;
        if (u1ValidPeerAddrFound == LDP_FALSE)
        {
            *pu4MplsLdpSessionPeerAddrIndex = u4SessionPeerAddressIndex;
            u1ValidPeerAddrFound = LDP_TRUE;
            continue;
        }
        if (u1ValidPeerAddrFound == LDP_TRUE)
        {
            if (u4SessionPeerAddressIndex < *pu4MplsLdpSessionPeerAddrIndex)
            {
                *pu4MplsLdpSessionPeerAddrIndex = u4SessionPeerAddressIndex;
            }
        }
    }

    if (u1ValidPeerAddrFound == LDP_TRUE)
    {
        return SNMP_SUCCESS;
    }
    while (LDP_ONE)
    {

        CPY_TO_SNMP (&LdpEntityId, pu1LdpEntityId, LDP_MAX_LDPID_LEN);
        CPY_TO_SNMP (&LdpPeerId, pu1LdpPeerId, LDP_MAX_LDPID_LEN);

        if (nmhGetNextIndexMplsLdpPeerTable ((tSNMP_OCTET_STRING_TYPE *) &
                                             LdpEntityId,
                                             pMplsLdpEntityLdpId,
                                             *pu4MplsLdpEntityIndex,
                                             pu4MplsLdpEntityIndex,
                                             (tSNMP_OCTET_STRING_TYPE *) &
                                             LdpPeerId,
                                             pMplsLdpPeerLdpId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        CPY_FROM_SNMP (pu1LdpEntityId, pMplsLdpEntityLdpId, LDP_MAX_LDPID_LEN);
        CPY_FROM_SNMP (pu1LdpPeerId, pMplsLdpPeerLdpId, LDP_MAX_LDPID_LEN);
        /* Getting the Ldp Peer for the given indices.
         * Failure case in getting ldp peer will never arise as getpeer
         * is done after GetFirstIndex. GetFirstIndex always returns a valid
         * index of an existing Peer. */

        if (LdpGetLdpPeer (MPLS_DEF_INCARN,
                           pu1LdpEntityId,
                           (*pu4MplsLdpEntityIndex), pu1LdpPeerId,
                           &pLdpPeer) == LDP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        TMO_SLL_Scan (LDP_GET_PEER_ADDR_LIST (pLdpPeer), pPeerIfAdrNode,
                      tPeerIfAdrNode *)
        {
            u4SessionPeerAddressIndex = (LDP_OFFSET
                                         (pPeerIfAdrNode))->
                u4SessionPeerAddrIndex;
            if (u1ValidPeerAddrFound == LDP_FALSE)
            {
                *pu4MplsLdpSessionPeerAddrIndex = u4SessionPeerAddressIndex;
                u1ValidPeerAddrFound = LDP_TRUE;
                continue;
            }
            if (u4SessionPeerAddressIndex < *pu4MplsLdpSessionPeerAddrIndex)
            {
                *pu4MplsLdpSessionPeerAddrIndex = u4SessionPeerAddressIndex;
            }
        }

        if (u1ValidPeerAddrFound == LDP_TRUE)
        {
            break;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpSessionPeerAddrTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpPeerLdpId
                nextMplsLdpPeerLdpId
                MplsLdpSessionPeerAddrIndex
                nextMplsLdpSessionPeerAddrIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpSessionPeerAddrTable (tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            UINT4 *pu4NextMplsLdpEntityIndex,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpPeerLdpId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextMplsLdpPeerLdpId,
                                            UINT4 u4MplsLdpSessionPeerAddrIndex,
                                            UINT4
                                            *pu4NextMplsLdpSessionPeerAddrIndex)
{
    UINT1               u1ValidPeerAddrFound = LDP_FALSE;
    UINT4               u4SessionPeerAddressIndex = LDP_ZERO;
    tLdpPeer           *pLdpPeer = NULL;
    tPeerIfAdrNode     *pPeerIfAdrNode = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT1              *pu1NextLdpPeerId = NULL;
    UINT1              *pu1NextLdpEntityId = NULL;

    if ((pMplsLdpEntityLdpId->i4_Length < 0) ||
        (pMplsLdpPeerLdpId->i4_Length < 0))
    {
        return SNMP_FAILURE;
    }

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    pu1NextLdpPeerId = SNMP_OCTET_STRING_LIST (pNextMplsLdpPeerLdpId);
    pu1NextLdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN,
                       pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) == LDP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (LDP_GET_PEER_ADDR_LIST (pLdpPeer), pPeerIfAdrNode,
                  tPeerIfAdrNode *)
    {
        u4SessionPeerAddressIndex = (LDP_OFFSET
                                     (pPeerIfAdrNode))->u4SessionPeerAddrIndex;
        if (u4SessionPeerAddressIndex > u4MplsLdpSessionPeerAddrIndex)
        {
            if (u1ValidPeerAddrFound == LDP_FALSE)
            {
                *pu4NextMplsLdpSessionPeerAddrIndex = u4SessionPeerAddressIndex;
                u1ValidPeerAddrFound = LDP_TRUE;
                continue;
            }
            if (u4SessionPeerAddressIndex < *pu4NextMplsLdpSessionPeerAddrIndex)
            {
                *pu4NextMplsLdpSessionPeerAddrIndex = u4SessionPeerAddressIndex;
            }
        }
    }

    if (u1ValidPeerAddrFound == LDP_TRUE)
    {
        *pu4NextMplsLdpEntityIndex = u4MplsLdpEntityIndex;
        CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pu1LdpEntityId,
                     LDP_MAX_LDPID_LEN);
        CPY_TO_SNMP (pNextMplsLdpPeerLdpId, pu1LdpPeerId, LDP_MAX_LDPID_LEN);
        return SNMP_SUCCESS;
    }

    while (LDP_ONE)
    {
        if (nmhGetNextIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                             pNextMplsLdpEntityLdpId,
                                             u4MplsLdpEntityIndex,
                                             pu4NextMplsLdpEntityIndex,
                                             pMplsLdpPeerLdpId,
                                             pNextMplsLdpPeerLdpId)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        u4MplsLdpEntityIndex = *pu4NextMplsLdpEntityIndex;
        CPY_TO_SNMP (pMplsLdpEntityLdpId, pu1NextLdpEntityId,
                     LDP_MAX_LDPID_LEN);
        CPY_TO_SNMP (pMplsLdpPeerLdpId, pu1NextLdpPeerId, LDP_MAX_LDPID_LEN);

        if (LdpGetLdpPeer (MPLS_DEF_INCARN,
                           pu1LdpEntityId,
                           u4MplsLdpEntityIndex,
                           pu1LdpPeerId, &pLdpPeer) == LDP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        TMO_SLL_Scan (LDP_GET_PEER_ADDR_LIST (pLdpPeer), pPeerIfAdrNode,
                      tPeerIfAdrNode *)
        {
            u4SessionPeerAddressIndex = (LDP_OFFSET
                                         (pPeerIfAdrNode))->
                u4SessionPeerAddrIndex;
            if (u1ValidPeerAddrFound == LDP_FALSE)
            {
                *pu4NextMplsLdpSessionPeerAddrIndex = u4SessionPeerAddressIndex;
                u1ValidPeerAddrFound = LDP_TRUE;
                continue;
            }
            if (u4SessionPeerAddressIndex < *pu4NextMplsLdpSessionPeerAddrIndex)
            {
                *pu4NextMplsLdpSessionPeerAddrIndex = u4SessionPeerAddressIndex;
            }
        }

        if (u1ValidPeerAddrFound == LDP_TRUE)
        {
            break;
        }
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionPeerNextHopAddrType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpSessionPeerAddrIndex

                The Object 
                retValMplsLdpSessionPeerNextHopAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionPeerNextHopAddrType (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpPeerLdpId,
                                         UINT4 u4MplsLdpSessionPeerAddrIndex,
                                         INT4
                                         *pi4RetValMplsLdpSessionPeerNextHopAddrType)
{
    tLdpPeer           *pLdpPeer = NULL;
    tPeerIfAdrNode     *pPeerIfAdrNode = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT4               u4SessionPeerAddressIndex = LDP_ZERO;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN,
                       pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) == LDP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (LDP_GET_PEER_ADDR_LIST (pLdpPeer), pPeerIfAdrNode,
                  tPeerIfAdrNode *)
    {
        u4SessionPeerAddressIndex = (LDP_OFFSET
                                     (pPeerIfAdrNode))->u4SessionPeerAddrIndex;
        if (u4SessionPeerAddressIndex == u4MplsLdpSessionPeerAddrIndex)
        {
            *pi4RetValMplsLdpSessionPeerNextHopAddrType =
                (INT4) ((LDP_OFFSET (pPeerIfAdrNode))->AddrType);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionPeerNextHopAddr
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpSessionPeerAddrIndex

                The Object 
                retValMplsLdpSessionPeerNextHopAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionPeerNextHopAddr (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpPeerLdpId,
                                     UINT4 u4MplsLdpSessionPeerAddrIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValMplsLdpSessionPeerNextHopAddr)
{
    UINT4               u4SessionPeerAddressIndex = LDP_ZERO;
    tLdpPeer           *pLdpPeer = NULL;
    tPeerIfAdrNode     *pPeerIfAdrNode = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (MPLS_DEF_INCARN, pu1LdpEntityId, u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) == LDP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (LDP_GET_PEER_ADDR_LIST (pLdpPeer), pPeerIfAdrNode,
                  tPeerIfAdrNode *)
    {
        u4SessionPeerAddressIndex = (LDP_OFFSET
                                     (pPeerIfAdrNode))->u4SessionPeerAddrIndex;
        if (u4SessionPeerAddressIndex == u4MplsLdpSessionPeerAddrIndex)
        {
            CPY_TO_SNMP (pRetValMplsLdpSessionPeerNextHopAddr,
                         (UINT1 *) &((LDP_OFFSET (pPeerIfAdrNode))->IfAddr),
                         LDP_IPV4ADR_LEN);
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}
