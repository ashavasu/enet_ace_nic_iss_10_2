/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: ldpstats.c,v 1.5 2014/11/08 11:40:54 siva Exp $

*********************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpstats.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : LDP (IFACE SUB-MODULE)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains utility fuctions to display
 *                             the set of Active LSPs, CRLSPs.
 *                             
 *----------------------------------------------------------------------------*/

#include "ldpincs.h"

typedef struct _lspPrintInfo
{
    struct _lspPrintInfo *pNext;
    VOID               *pLspPrintInfo;
}
tLspPrintInfo;

#define LDP_LCB_DUMP 0x08000001
/* ---------------------------------------------------------------------------
 * Function Name : LdpPrintNormLCB                                        
 * Description   : This routine prints the main identifier info of a normal 
 *                 LSP Control block.
 * Input(s)      : u2LspNum,pLspCtrlBlk  
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpPrintNormLCB (UINT2 u2LspNum, tLspCtrlBlock * pLspCtrlBlk)
{
    tLdpId              ULdpId;
    tLdpId              DLdpId;

    /* Printing the LSP Number */
    LDP_DBG_STAT1 (LDP_LCB_DUMP, "%4d| ", u2LspNum);

    /* Printing the LSRID of the UpStream LSR */
    if (pLspCtrlBlk->pUStrSession != NULL)
    {
        MEMCPY (ULdpId, pLspCtrlBlk->pUStrSession->pLdpPeer->PeerLdpId,
                sizeof (tLdpId));
        LDP_DBG_STAT6 (LDP_LCB_DUMP, "%02x.%02x.%02x.%02x.%02x.%02x | ",
                       ULdpId[0], ULdpId[1], ULdpId[2], ULdpId[3], ULdpId[4],
                       ULdpId[5]);
    }
    else
    {
        LDP_DBG_STAT (LDP_LCB_DUMP, "         -        | ");
    }

    /* Printing the LSRID of the DownStream LSR */
    if (pLspCtrlBlk->pDStrSession != NULL)
    {
        MEMCPY (DLdpId, pLspCtrlBlk->pDStrSession->pLdpPeer->PeerLdpId,
                sizeof (tLdpId));
        LDP_DBG_STAT6 (LDP_LCB_DUMP, "%02x.%02x.%02x.%02x.%02x.%02x | ",
                       DLdpId[0], DLdpId[1], DLdpId[2], DLdpId[3], DLdpId[4],
                       DLdpId[5]);
    }
    else
    {
        LDP_DBG_STAT (LDP_LCB_DUMP, "         -        | ");
    }

    /* Printing the LSP State */
    LDP_DBG_STAT1 (LDP_LCB_DUMP, "%1d|", pLspCtrlBlk->u1LspState);

#if VISHAL_FEC
    /* Printing the FEC Network Prefix */
    LDP_DBG_STAT1 (LDP_LCB_DUMP, "%08x|", (int) (pLspCtrlBlk->Fec.u4Prefix));
#endif

    /* Printing the FEC Network Prefix Length */
    LDP_DBG_STAT1 (LDP_LCB_DUMP, "%2x|", pLspCtrlBlk->Fec.u1PreLen);

    /* Printing the UpStream Label */
    if (pLspCtrlBlk->pUStrSession != NULL)
    {
        LDP_DBG_STAT1 (LDP_LCB_DUMP, "%6x  |",
                       (int) (pLspCtrlBlk->UStrLabel.u4GenLbl));
    }
    else
    {
        LDP_DBG_STAT (LDP_LCB_DUMP, "    -   |");
    }

    /* Printing the DownStream Label */
    if (pLspCtrlBlk->pDStrSession != NULL)
    {
        LDP_DBG_STAT1 (LDP_LCB_DUMP, "%6x \n",
                       (int) (pLspCtrlBlk->DStrLabel.u4GenLbl));
    }
    else
    {
        LDP_DBG_STAT (LDP_LCB_DUMP, "    -   \n");
    }
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpPrintNormUstrCtrlBlk                                      
 * Description   : This routine prints the main identifier info of a normal 
 *                 LSP Control block.
 * Input(s)      : u2LspNum,pLspCtrlBlk  
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpPrintNormUstrCtrlBlk (UINT2 u2LspNum, tUstrLspCtrlBlock * pUstrCtrlBlk)
{
    tLdpId              ULdpId;
    tLdpId              DLdpId;

    /* Printing the LSP Number */
    LDP_DBG_STAT1 (LDP_LCB_DUMP, "%4d| ", u2LspNum);

    /* Printing the LSRID of the UpStream LSR */
    if (pUstrCtrlBlk->pUpstrSession != NULL)
    {
        MEMCPY (ULdpId, pUstrCtrlBlk->pUpstrSession->pLdpPeer->PeerLdpId,
                sizeof (tLdpId));
        LDP_DBG_STAT6 (LDP_LCB_DUMP, "%02x.%02x.%02x.%02x.%02x.%02x | ",
                       ULdpId[0], ULdpId[1], ULdpId[2], ULdpId[3], ULdpId[4],
                       ULdpId[5]);
    }
    else
    {
        LDP_DBG_STAT (LDP_LCB_DUMP, "         -        | ");
    }

    /* Printing the LSRID of the DownStream LSR */
    if (UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL)
    {
        MEMCPY (DLdpId,
                (UPSTR_DSTR_PTR (pUstrCtrlBlk))->pDStrSession->pLdpPeer->
                PeerLdpId, sizeof (tLdpId));
        LDP_DBG_STAT6 (LDP_LCB_DUMP, "%02x.%02x.%02x.%02x.%02x.%02x | ",
                       DLdpId[0], DLdpId[1], DLdpId[2], DLdpId[3],
                       DLdpId[4], DLdpId[5]);
    }
    else
    {
        LDP_DBG_STAT (LDP_LCB_DUMP, "          -       | ");
    }

    /* Printing the LSP State */
    LDP_DBG_STAT1 (LDP_LCB_DUMP, "%1d|", pUstrCtrlBlk->u1LspState);

#ifdef VISHAL_
    /* Printing the FEC Network Prefix */
    LDP_DBG_STAT1 (LDP_LCB_DUMP, "%08x|", (int) (pUstrCtrlBlk->Fec.u4Prefix));
#endif

    /* Printing the FEC Network Prefix Length */
    LDP_DBG_STAT1 (LDP_LCB_DUMP, "%2x|", pUstrCtrlBlk->Fec.u1PreLen);

    /* Printing the UpStream Label */

    if (pUstrCtrlBlk->pUpstrSession != NULL)
    {
        if (SSN_MRGTYPE (pUstrCtrlBlk->pUpstrSession) == LDP_ZERO)
        {
            LDP_DBG_STAT1 (LDP_LCB_DUMP, "%6x |",
                           (int) (pUstrCtrlBlk->UStrLabel.u4GenLbl));
            if (UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL)
            {
                LDP_DBG_STAT1 (LDP_LCB_DUMP, "%6x \n",
                               (int) ((UPSTR_DSTR_PTR (pUstrCtrlBlk))->
                                      DStrLabel.u4GenLbl));
            }
            else
            {
                LDP_DBG_STAT (LDP_LCB_DUMP, "   -   \n");
            }
        }
    }
    if (pUstrCtrlBlk->pUpstrSession != NULL)
    {
        if (LDP_IS_ENTITY_TYPE_ATM (pUstrCtrlBlk->pUpstrSession) == LDP_FALSE)
        {
            LDP_DBG_STAT1 (LDP_LCB_DUMP, "%6x  |",
                           (int) (pUstrCtrlBlk->UStrLabel.u4GenLbl));
        }
        else
        {

            LDP_DBG_STAT2 (LDP_LCB_DUMP, "%3x,%3x|",
                           (int) (pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vpi),
                           (int) (pUstrCtrlBlk->UStrLabel.AtmLbl.u2Vci));
        }
    }
    else
    {
        LDP_DBG_STAT (LDP_LCB_DUMP, "    -   |");
    }

    /* Printing the DownStream Label */
    if (UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL)
    {
        if (LDP_IS_ENTITY_TYPE_ATM ((UPSTR_DSTR_PTR
                                     (pUstrCtrlBlk))->pDStrSession) ==
            LDP_FALSE)
        {
            LDP_DBG_STAT1 (LDP_LCB_DUMP, "%6x\n",
                           (int) ((UPSTR_DSTR_PTR (pUstrCtrlBlk))->DStrLabel.
                                  u4GenLbl));
        }
        else
        {
            LDP_DBG_STAT2 (LDP_LCB_DUMP, "%3x,%3x\n",
                           (int) ((UPSTR_DSTR_PTR (pUstrCtrlBlk))->DStrLabel.
                                  AtmLbl.u2Vpi),
                           (int) ((UPSTR_DSTR_PTR (pUstrCtrlBlk))->DStrLabel.
                                  AtmLbl.u2Vci));
        }
    }
    else
    {
        LDP_DBG_STAT (LDP_LCB_DUMP, "    -   \n");
    }
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpPrintCrlspLCB                                        
 * Description   : This routine prints the main identifier info of a Crlsp 
 *                 LSP Control block.
 * Input(s)      : u2CrLspNum,pCrlspTnlInfo  
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpPrintCrlspLCB (UINT2 u2CrLspNum, tCrlspTnlInfo * pCrlspTnlInfo)
{
    tLdpId              ULdpId;
    tLdpId              DLdpId;
    tLspCtrlBlock      *pLspCtrlBlk = pCrlspTnlInfo->pLspCtrlBlock;

    if (pLspCtrlBlk != NULL)
    {

        /* Printing the LSP Number */
        LDP_DBG_STAT1 (LDP_LCB_DUMP, "%4d|", u2CrLspNum);

        /* Printing the Tunnel Id */
        LDP_DBG_STAT1 (LDP_LCB_DUMP, "%5d|", pCrlspTnlInfo->u2LocalLspid);

        /* Printing the Ingress LSR Id */
        LDP_DBG_STAT4 (LDP_LCB_DUMP, "%02x%02x%02x%02x| ",
                       (CRLSP_INGRESS_LSRID (pCrlspTnlInfo))[0],
                       (CRLSP_INGRESS_LSRID (pCrlspTnlInfo))[1],
                       (CRLSP_INGRESS_LSRID (pCrlspTnlInfo))[2],
                       (CRLSP_INGRESS_LSRID (pCrlspTnlInfo))[3]);

        /* Printing the LSRID of the UpStream LSR */
        if (pLspCtrlBlk->pUStrSession != NULL)
        {
            MEMCPY (ULdpId, pLspCtrlBlk->pUStrSession->pLdpPeer->PeerLdpId,
                    sizeof (tLdpId));
            LDP_DBG_STAT6 (LDP_LCB_DUMP, "%02x.%02x.%02x.%02x.%02x.%02x | ",
                           ULdpId[0], ULdpId[1], ULdpId[2], ULdpId[3],
                           ULdpId[4], ULdpId[5]);
        }
        else
        {
            LDP_DBG_STAT (LDP_LCB_DUMP, "         -        | ");
        }

        /* Printing the LSRID of the DownStream LSR */
        if (pLspCtrlBlk->pDStrSession != NULL)
        {
            MEMCPY (DLdpId, pLspCtrlBlk->pDStrSession->pLdpPeer->PeerLdpId,
                    sizeof (tLdpId));
            LDP_DBG_STAT6 (LDP_LCB_DUMP, "%02x.%02x.%02x.%02x.%02x.%02x | ",
                           DLdpId[0], DLdpId[1], DLdpId[2], DLdpId[3],
                           DLdpId[4], DLdpId[5]);
        }
        else
        {
            LDP_DBG_STAT (LDP_LCB_DUMP, "         -        | ");
        }

        /* Printing the LSP State */
        LDP_DBG_STAT1 (LDP_LCB_DUMP, "%1d|", pLspCtrlBlk->u1LspState);

        /* Printing the UpStream Label */
        if (pLspCtrlBlk->pUStrSession != NULL)
        {
            LDP_DBG_STAT1 (LDP_LCB_DUMP, "%5x |",
                           (int) (pLspCtrlBlk->UStrLabel.u4GenLbl));
        }
        else
        {
            LDP_DBG_STAT (LDP_LCB_DUMP, "   -  |");
        }

        /* Printing the DownStream Label */
        if (pLspCtrlBlk->pDStrSession != NULL)
        {
            LDP_DBG_STAT1 (LDP_LCB_DUMP, "%5x\n",
                           (int) (pLspCtrlBlk->DStrLabel.u4GenLbl));
        }
        else
        {
            LDP_DBG_STAT (LDP_LCB_DUMP, "   -  \n");
        }
    }
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpAllNormalLsps                                        
 * Description   : This routine prints the info on all active Lsps when
 *                 requested at that given time. 
 * Input(s)      : None 
 * Output(s)     : pNumLsps
 * Return(s)     : LDP_SUCCESS/LDP_FAILURE
 -------------------------------------------------------------------------- */
UINT1
LdpDumpAllNormalLsps (INT4 *pNumLsps)
{
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT2               u2NumActiveLsp = 0;
    UINT4               u4HIndex = 0;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tTMO_SLL_NODE      *pLdpSllUpstrNode = NULL;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tTMO_SLL_NODE      *pTempLdpSllUpstrNode = NULL;
    tTMO_HASH_NODE     *pSsnHashNode = NULL;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlk = NULL;

    *pNumLsps = (INT4) u2NumActiveLsp;

    if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
    {
        return LDP_FAILURE;
    }

    LDP_DBG_STAT (LDP_LCB_DUMP,
                  "---------------------------------------------------------------------------\n");
    LDP_DBG_STAT (LDP_LCB_DUMP,
                  "LSP |     UStrLdpId     |     DStrLdpId     | S| NetPrfx|PL|  ULbl  |  DLbl\n");

    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

    TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
    {
        TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex, pSsnHashNode,
                              tTMO_HASH_NODE *)
        {
            pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);
            if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
            {
                TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession), pLdpSllNode,
                                  pTempLdpSllNode, tTMO_SLL_NODE *)
                {
                    pUstrCtrlBlk = (tUstrLspCtrlBlock *) (pLdpSllNode);
                    if (UPSTR_DSTR_PTR (pUstrCtrlBlk) != NULL)
                    {
                        if ((UPSTR_STATE (pUstrCtrlBlk) == LDP_LSM_ST_EST) &&
                            (UPSTR_STATE (UPSTR_DSTR_PTR (pUstrCtrlBlk)) ==
                             LDP_LSM_ST_EST))
                        {
                            u2NumActiveLsp++;
                            LdpPrintNormUstrCtrlBlk (u2NumActiveLsp,
                                                     (tUstrLspCtrlBlock *)
                                                     pUstrCtrlBlk);
                        }
                    }
                    else
                    {
                        if (UPSTR_STATE (pUstrCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            u2NumActiveLsp++;
                            LdpPrintNormUstrCtrlBlk (u2NumActiveLsp,
                                                     (tUstrLspCtrlBlock *)
                                                     pUstrCtrlBlk);
                        }
                    }
                }
                if (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG)
                {
                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                      pTempLdpSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                        if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                        {
                            continue;
                        }

                        if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            u2NumActiveLsp++;
                            LdpPrintNormLCB (u2NumActiveLsp,
                                             (tLspCtrlBlock *) pLspCtrlBlk);
                        }
                    }
                }
            }
            else
            {
                TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession), pLdpSllNode,
                                  pTempLdpSllNode, tTMO_SLL_NODE *)
                {
                    pLspCtrlBlk = (tLspCtrlBlock *) (pLdpSllNode);
                    if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                    {
                        continue;
                    }
                    if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                    {
                        u2NumActiveLsp++;
                        LdpPrintNormLCB (u2NumActiveLsp,
                                         (tLspCtrlBlock *) pLspCtrlBlk);
                    }
                }

                TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                  pTempLdpSllNode, tTMO_SLL_NODE *)
                {
                    pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                    if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                    {
                        continue;
                    }

                    if (pLspCtrlBlk->pUStrSession != NULL)
                    {
                        continue;
                    }

                    if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                    {
                        u2NumActiveLsp++;
                        LdpPrintNormLCB (u2NumActiveLsp,
                                         (tLspCtrlBlock *) pLspCtrlBlk);
                    }
                }
            }

        }
    }

    /* When the Lsr is acting as an ingress to the LSP */
    TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
    {
        TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex, pSsnHashNode,
                              tTMO_HASH_NODE *)
        {
            pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);
            if ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG) ||
                (SSN_MRGTYPE (pLdpSession) == LDP_DU_MRG))
            {
                if (TMO_SLL_Count (&SSN_DLCB (pLdpSession)) != LDP_ZERO)
                {
                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                      pTempLdpSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                        if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                        {
                            continue;
                        }

                        if (LCB_STATE (pLspCtrlBlk) == LDP_LSM_ST_EST)
                        {
                            TMO_DYN_SLL_Scan (&LCB_UPSTR_LIST (pLspCtrlBlk),
                                              pLdpSllUpstrNode,
                                              pTempLdpSllUpstrNode,
                                              tTMO_SLL_NODE *)
                            {
                                pUstrCtrlBlk = SLL_TO_UPCB (pLdpSllUpstrNode);
                                if ((UPSTR_STATE (pUstrCtrlBlk) ==
                                     LDP_LSM_ST_EST) &&
                                    (UPSTR_USSN (pUstrCtrlBlk) == NULL))
                                {
                                    u2NumActiveLsp++;
                                    LdpPrintNormUstrCtrlBlk (u2NumActiveLsp,
                                                             (tUstrLspCtrlBlock
                                                              *) pUstrCtrlBlk);

                                }
                            }
                        }
                    }            /*End of Scan */
                }
                else
                {
                    continue;
                }
            }
        }
    }
    LDP_DBG_STAT (LDP_LCB_DUMP,
                  "---------------------------------------------------------------------------\n");
    LDP_DBG_STAT1 (LDP_LCB_DUMP, "Normal Lsps currently at this node = %d\n",
                   u2NumActiveLsp);
    LDP_DBG_STAT (LDP_LCB_DUMP,
                  "---------------------------------------------------------------------------\n");
    *pNumLsps = (INT4) u2NumActiveLsp;
    return LDP_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpAllCrLsps                                        
 * Description   : This routine prints the info on all active CrLsps when
 *                 requested at that given time. 
 * Input(s)      : None 
 * Output(s)     : pNumLsps
 * Return(s)     : LDP_SUCCESS/LDP_FAILURE
 -------------------------------------------------------------------------- */
UINT1
LdpDumpAllCrLsps (INT4 *pNumLsps)
{
    UINT2               u2NumActiveLsp = 0;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT4               u4HIndex = 0;
    tTMO_HASH_TABLE    *pCrlspTnlHashtbl = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;

    *pNumLsps = (INT4) u2NumActiveLsp;

    if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
    {
        return LDP_FAILURE;
    }

    LDP_DBG_STAT (LDP_LCB_DUMP,
                  "---------------------------------------------------------------------------\n");
    LDP_DBG_STAT (LDP_LCB_DUMP,
                  "LSP |TnlId| IngrId |     UStrLdpId     |     DStrLdpId     | S| ULbl | DLbl\n");
    LDP_DBG_STAT (LDP_LCB_DUMP,
                  "---------------------------------------------------------------------------\n");

    pCrlspTnlHashtbl = LDP_CRLSP_TNL_TBL (u2IncarnId);

    TMO_HASH_Scan_Table (pCrlspTnlHashtbl, u4HIndex)
    {
        TMO_HASH_Scan_Bucket (pCrlspTnlHashtbl,
                              u4HIndex, pCrlspTnlInfo, tCrlspTnlInfo *)
        {
            u2NumActiveLsp++;
            LdpPrintCrlspLCB (u2NumActiveLsp, (tCrlspTnlInfo *) pCrlspTnlInfo);
        }
    }
    LDP_DBG_STAT (LDP_LCB_DUMP,
                  "---------------------------------------------------------------------------\n");
    LDP_DBG_STAT1 (LDP_LCB_DUMP, "CR Lsps currently at this node = %d\n",
                   u2NumActiveLsp);
    LDP_DBG_STAT (LDP_LCB_DUMP,
                  "---------------------------------------------------------------------------\n");
    *pNumLsps = (INT4) u2NumActiveLsp;
    return LDP_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpstats.c                              */
/*---------------------------------------------------------------------------*/
