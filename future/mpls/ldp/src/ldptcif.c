/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldptcif.c,v 1.7 2013/06/26 11:37:13 siva Exp $
 *----------------------------------------------------------------------------*/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : ldptcif.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : LDP
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : File which contains the interface routines
 *                             to the Traffic Controller(TC). These routines
 *                             are required for the CRLSP support.
 *----------------------------------------------------------------------------*/

#include "ldpincs.h"

/*****************************************************************************/
/* Function Name : LdpRegisterWithTC                                         */
/* Description   : This routine registers with TC                            */
/* Input(s)      : NONE                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpRegisterWithTC ()
{
    if (TcRegisterApp (TC_CRLDP_APPID, NULL) == TC_FAILURE)
    {
        LDP_DBG (LDP_IF_PRCS, "LDTC: Registration Failed ");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDeRegisterWithTC                                       */
/* Description   : This routine De registers with TC                         */
/* Input(s)      : NONE                                                      */
/* Return(s)     : NONE                                                      */
/*****************************************************************************/
VOID
LdpDeRegisterWithTC ()
{
    TcDeregisterApp (TC_CRLDP_APPID);
    return;
}

/*****************************************************************************/
/* Function Name : LdpTcResvResources                                        */
/* Description   : This routine will be invoked by the LDP. This routine will*/
/*                 invoke the supporting routine provide by the Resource     */
/*                 Manager or this routine might be mapped to the routine    */
/*                 provided by the Resource Manager.                         */
/*                 This routine is expected to check the availability of the */
/*                 resources requested and if they are available, reserve the*/
/*                 resources and return an identifier for the reserved set of*/
/*                 resources.                                                */
/* Input(s)      : pCrlspTnlInfo  - Pointer to the Tunnel for which the      */
/*                          resources needs to be checked and reserved.      */
/* Return(s)     : LDP_SUCCESS in case the resources are available and       */

/*****************************************************************************/
UINT1
LdpTcResvResources (tCrlspTnlInfo * pCrlspTnlInfo)
{
    tuTcResSpec         CrldpResSpec;
    tuTcResSpec        *pTcResSpec = &CrldpResSpec;
    tTcFilterInfo       FilterInfo;
    tTcFilterInfo      *pFilterInfo = &FilterInfo;
    tTcResHandle        ResHandle;
    tTcResHandle       *pResHandle = &ResHandle;
    UINT1               u1ErrorCode = LDP_ZERO;
    UINT1               u1ToRsvRrsc = LDP_FALSE;

    MEMSET (&ResHandle, LDP_ZERO, sizeof (tTcResHandle));

    /* Copy the Required Traffic specifications in to TcResSpec */
    MEMCPY ((UINT1 *) &pTcResSpec->CrldpTrfcParams,
            (UINT1
             *) (CRLSP_TE_CRLDP_TRFC_PARAMS (CRLSP_TE_TNL_TRFC_PARAM
                                             (CRLSP_TE_TNL_INFO
                                              (pCrlspTnlInfo)))),
            sizeof (tCRLDPTrfcParams));

    /* Copy the Tunnel Index table in to FilterSpec */
    pFilterInfo->u4TnlId =
        CRLSP_TE_TNL_INDEX (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    pFilterInfo->u4TnlInstance =
        CRLSP_TE_TNL_INSTANCE (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    MEMCPY ((UINT1 *) &pFilterInfo->u4SrcAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_INGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &pFilterInfo->u4DestAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_EGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);

    /*  If the requested resources are not for modification it's treated
     *  as a new reservation, else the new Filter Info with the Traffic
     *  parameters is used. 
     */
    if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL) &&
        (CRLSP_TE_CRLDP_TRFC_PARAMS
         (CRLSP_TE_TNL_TRFC_PARAM
          (CRLSP_TE_TNL_INFO (CRLSP_INS_POIN (pCrlspTnlInfo)))) != NULL))
    {
        if (LCB_OUT_IFINDEX (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo)))
            != LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo))))
        {
            u1ToRsvRrsc = LDP_TRUE;
        }
        else
        {
            if (TcAssociateFilterInfo
                (CRLSP_TRFPARM_GRPID (CRLSP_INS_POIN (pCrlspTnlInfo)),
                 pFilterInfo, TC_CRLDP_TYPE_RES_SPEC, pTcResSpec,
                 &u1ErrorCode) != TC_SUCCESS)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Filter Spec Association failed \n");
                return LDP_FAILURE;
            }
            else
            {
                *pResHandle = CRLSP_TRFPARM_GRPID
                    (CRLSP_INS_POIN (pCrlspTnlInfo));
            }
        }
    }
    if ((CRLSP_INS_POIN (pCrlspTnlInfo) == NULL) || (u1ToRsvRrsc == LDP_TRUE))
    {
        if (TcResvResources (TC_CRLDP_APPID,
                             LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo))),
                             TC_CRLDP_TYPE_RES_SPEC, pTcResSpec,
                             TC_INTSERV, pResHandle, &u1ErrorCode)
            != TC_SUCCESS)
        {
            LDP_DBG (LDP_ADVT_MEM, "ADVT: Traffic Resources Not Available \n");
            return LDP_FAILURE;
        }
        else
        {
            if (TcAssociateFilterInfo (*pResHandle, pFilterInfo,
                                       TC_CRLDP_TYPE_RES_SPEC, pTcResSpec,
                                       &u1ErrorCode) != TC_SUCCESS)
            {
                TcFreeResources (*pResHandle);
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Filter Spec Association failed \n");
                return LDP_FAILURE;
            }
        }
    }
    CRLSP_TRFPARM_GRPID (pCrlspTnlInfo) = *pResHandle;
    LDP_DBG1 (LDP_ADVT_MEM, "ADVT: Resources reserved. Id returned = %d\n",
              *pResHandle);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpTcFreeResources                                        */
/* Description   : This routine will be invoked by the LDP. This routine will*/
/*                 invoke the supporting routine provide by the Traffic      */
/*                 Controller                                                */
/*                 This routine is expected to release a set of resources    */
/*                 that have been reserved earlier.                          */
/* Input(s)      : pCrlspTnlInfo - Pointer to the corresponding tunnel.      */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
LdpTcFreeResources (tCrlspTnlInfo * pCrlspTnlInfo)
{
    tTcFilterInfo       FilterInfo;
    tTcFilterInfo      *pFilterInfo = &FilterInfo;
    /* If the Resource Handle is associated with several filter specs
     * only the corresponding filter spec and the resource will be
     * released .
     */
    if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL) &&
        (CRLSP_TE_CRLDP_TRFC_PARAMS
         (CRLSP_TE_TNL_TRFC_PARAM
          (CRLSP_TE_TNL_INFO (CRLSP_INS_POIN (pCrlspTnlInfo)))) != NULL))
    {

        /* Copy the Tunnel Index table in to FilterSpec */
        pFilterInfo->u4TnlId =
            CRLSP_TE_TNL_INDEX (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
        pFilterInfo->u4TnlInstance =
            CRLSP_TE_TNL_INSTANCE (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
        MEMCPY ((UINT1 *) &pFilterInfo->u4SrcAddr,
                (UINT1 *)
                &CRLSP_TE_TNL_INGRESS_RTR_ID (CRLSP_TE_TNL_INFO
                                              (pCrlspTnlInfo)),
                IPV4_ADDR_LENGTH);
        MEMCPY ((UINT1 *) &pFilterInfo->u4DestAddr,
                (UINT1 *)
                &CRLSP_TE_TNL_EGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
                IPV4_ADDR_LENGTH);
        TcDissociateFilterInfo (CRLSP_TRFPARM_GRPID (pCrlspTnlInfo),
                                pFilterInfo);
    }
    else
    {
        TcFreeResources (CRLSP_TRFPARM_GRPID (pCrlspTnlInfo));
    }
    LDP_DBG1 (LDP_ADVT_MEM, "ADVT: Resources released. Id released = %d\n",
              CRLSP_TRFPARM_GRPID (pCrlspTnlInfo));
    CRLSP_TRFPARM_GRPID (pCrlspTnlInfo) = LDP_INVALID_RSRC_GRPID;
    return;
}

/*****************************************************************************/
/* Function Name : LdpTcModifyResources                                      */
/* Description   : This routine will be invoked by the LDP. This routine will*/
/*                 invoke the supporting routine provide by the Traffic      */
/*                 Controller or this routine might be mapped to the routine */
/*                 provided by the Traffic Controller.                       */
/*                 This routine is expected to reserve a modified set of     */
/*                 resources compared to the ealier set of reserved resourc- */
/*                 es. The extra resources reserved earlier are to be relea- */
/*                 by the Traffic controller.                                */
/* Input(s)      : pCrlspTnlInfo       - Pointer to Actual Tunnel Info       */
/*                 where the resources are modified.                         */
/* Output(s)     : None.                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpTcModifyResources (tCrlspTnlInfo * pCrlspTnlInfo)
{
    tuTcResSpec         CrldpResSpec;
    tuTcResSpec        *pTcResSpec = &CrldpResSpec;
    UINT1               u1ErrorCode = LDP_ZERO;
    UINT4               u4GrpId = CRLSP_TRFPARM_GRPID (pCrlspTnlInfo);

    /* Copy the Required Traffic specifications in to TcResSpec */
    MEMCPY ((UINT1 *) &pTcResSpec->CrldpTrfcParams,
            (UINT1 *) CRLSP_TE_CRLDP_TRFC_PARAMS
            (CRLSP_TE_TNL_TRFC_PARAM (CRLSP_TE_TNL_INFO
                                      (pCrlspTnlInfo))),
            sizeof (tCRLDPTrfcParams));

    if (LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo) != NULL)
    {
        if ((CRLSP_PARM_FLAG (pCrlspTnlInfo) & LDP_CLASSTYPE_CRLSP)
            == LDP_CLASSTYPE_CRLSP)
        {
            u4GrpId = LDP_DS_PARAMS_CLASSTYPE_GRPID (pCrlspTnlInfo);
        }
    }

    if (TcModifyResources (u4GrpId, TC_CRLDP_TYPE_RES_SPEC,
                           pTcResSpec, TC_INTSERV, &u1ErrorCode) == TC_SUCCESS)
    {
        return LDP_SUCCESS;
    }
    LDP_DBG1 (LDP_ADVT_MEM,
              "ADVT: Modifying Rsrcs Failed  RsrcGrpId = %d\n", u4GrpId);
    return LDP_FAILURE;
}

/*****************************************************************************
* Function Name : LdpDiffServTcResvResources
* Description   : This routine will be invoked by the LDP with diffServ
*                 feature. The routine checks if the resources can be 
*                 availed from the tunnel instance. If no tunnel instance is
*                 present, the reources are availed from the interface on per
*                 OA basis. If the resources for a particular OA is reserved
*                 from the tunnel instance or interface available resources,
*                 then , it sets the u1IsRsrcAvail flag to TRUE and gets the 
*                 appropriate grpId for it. If all the resources are reserved,
*                 it returns success.If even a single OA resource is not
*                 available, it returns the failure.
* Input(s)      : pCrlspTnlInfo  - Pointer to the Tunnel which contains the
*                   per OA  requirement of the traffic profiles.
* Return(s)     : LDP_SUCCESS in case the resources are available for 
*                 all OAs or  LDP_FAILURE otherwise.
*****************************************************************************/
UINT1
LdpDiffServTcResvResources (tCrlspTnlInfo * pCrlspTnlInfo)
{
    tuTcResSpec         CrldpResSpec;
    tuTcResSpec        *pTcResSpec = &CrldpResSpec;
    tTcFilterInfo       FilterInfo;
    tTcFilterInfo      *pFilterInfo = &FilterInfo;
    tTcResHandle        ResHandle;
    tTcResHandle       *pResHandle = &ResHandle;
    UINT2               u2PscIndex;
    UINT1               u1ErrorCode = LDP_ZERO;
    UINT1               u2RetVal = LDP_SUCCESS;

    /* Copy the Tunnel Index table in to FilterSpec */
    pFilterInfo->u4TnlId =
        CRLSP_TE_TNL_INDEX (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    pFilterInfo->u4TnlInstance =
        CRLSP_TE_TNL_INSTANCE (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    MEMCPY ((UINT1 *) &pFilterInfo->u4SrcAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_INGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &pFilterInfo->u4DestAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_EGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);

    if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
        && (LCB_OUT_IFINDEX (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo)))
            == LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo)))))
    {
        for (u2PscIndex = 0; u2PscIndex < LDP_DS_MAX_NO_PSCS; u2PscIndex++)
        {
            if ((LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                 (pCrlspTnlInfo, u2PscIndex) != NULL) &&
                (LDP_DS_GET_PER_OA_RSRC_AVAIL (pCrlspTnlInfo,
                                               u2PscIndex) == FALSE))
            {

                /* Copy the Required Traffic specifications in to 
                 * TcResSpec */

                MEMCPY ((UINT1 *) &pTcResSpec->CrldpTrfcParams,
                        (UINT1 *)
                        TE_CRLDP_TRFC_PARAMS (LDP_DS_PEROATRFC_PARAMS
                                              (pCrlspTnlInfo, u2PscIndex)),
                        sizeof (tCRLDPTrfcParams));

                if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                    (CRLSP_INS_POIN (pCrlspTnlInfo), u2PscIndex) != NULL)
                {
                    if (TcAssociateFilterInfo
                        (CRLSP_TRFPARM_GRPID (CRLSP_INS_POIN (pCrlspTnlInfo)),
                         pFilterInfo, TC_CRLDP_TYPE_RES_SPEC, pTcResSpec,
                         &u1ErrorCode) != TC_SUCCESS)
                    {
                        LDP_DBG (LDP_ADVT_MEM,
                                 "ADVT: Filter Spec Association failed \n");
                        u2RetVal = LDP_FAILURE;
                    }
                    else
                    {
                        LDP_DS_PER_OA_GRP_ID (pCrlspTnlInfo, u2PscIndex) =
                            LDP_DS_PER_OA_GRP_ID ((CRLSP_INS_POIN
                                                   (pCrlspTnlInfo)),
                                                  u2PscIndex);
                        LDP_DS_SET_PER_OA_RSRC_AVAIL (pCrlspTnlInfo,
                                                      u2PscIndex);
                    }
                }
                else
                {
                    if (TcResvResources (TC_CRLDP_APPID,
                                         LCB_OUT_IFINDEX (CRLSP_LCB
                                                          ((pCrlspTnlInfo))),
                                         TC_CRLDP_TYPE_RES_SPEC, pTcResSpec,
                                         (UINT1) u2PscIndex, pResHandle,
                                         &u1ErrorCode) != TC_SUCCESS)
                    {
                        LDP_DBG (LDP_ADVT_MEM,
                                 "ADVT: Reservation of Resources Failed \n");
                        u2RetVal = LDP_FAILURE;
                    }
                    else
                    {
                        if (TcAssociateFilterInfo
                            (*pResHandle, pFilterInfo, TC_CRLDP_TYPE_RES_SPEC,
                             pTcResSpec, &u1ErrorCode) != TC_SUCCESS)
                        {
                            TcFreeResources (*pResHandle);
                            LDP_DBG (LDP_ADVT_MEM,
                                     "ADVT: Filter Spec Association failed \n");
                            u2RetVal = LDP_FAILURE;
                        }
                        else
                        {
                            LDP_DS_PER_OA_GRP_ID (pCrlspTnlInfo, u2PscIndex) =
                                *pResHandle;
                            LDP_DS_SET_PER_OA_RSRC_AVAIL (pCrlspTnlInfo,
                                                          u2PscIndex);
                        }
                    }
                }
            }
        }                        /*  End of For */
    }
    /* If there is no modification of the tunnel or the interface
     *  index is different than all the resources have to be allocated 
     *  from the interface*/
    else
    {
        for (u2PscIndex = 0; u2PscIndex < LDP_DS_MAX_NO_PSCS; u2PscIndex++)
        {
            if ((LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                 (pCrlspTnlInfo, u2PscIndex) != NULL) &&
                (LDP_DS_GET_PER_OA_RSRC_AVAIL
                 (pCrlspTnlInfo, u2PscIndex) == FALSE))
            {

                /* Copy the Required Traffic specifications in to 
                 * TcResSpec */

                MEMCPY ((UINT1 *) &pTcResSpec->CrldpTrfcParams,
                        (UINT1 *)
                        TE_CRLDP_TRFC_PARAMS (LDP_DS_PEROATRFC_PARAMS
                                              (pCrlspTnlInfo, u2PscIndex)),
                        sizeof (tCRLDPTrfcParams));

                if (TcResvResources (TC_CRLDP_APPID,
                                     LCB_OUT_IFINDEX (CRLSP_LCB
                                                      ((pCrlspTnlInfo))),
                                     TC_CRLDP_TYPE_RES_SPEC, pTcResSpec,
                                     (UINT1) u2PscIndex, pResHandle,
                                     &u1ErrorCode) != TC_SUCCESS)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Reservation of Resources Failed \n");
                    u2RetVal = LDP_FAILURE;
                }
                else
                {
                    if (TcAssociateFilterInfo (*pResHandle,
                                               pFilterInfo,
                                               TC_CRLDP_TYPE_RES_SPEC,
                                               pTcResSpec, &u1ErrorCode)
                        != TC_SUCCESS)
                    {
                        TcFreeResources (*pResHandle);
                        LDP_DBG (LDP_ADVT_MEM,
                                 "ADVT: Filter Spec Association failed \n");
                        u2RetVal = LDP_FAILURE;
                    }
                    else
                    {
                        LDP_DS_PER_OA_GRP_ID (pCrlspTnlInfo, u2PscIndex) =
                            *pResHandle;
                        LDP_DS_SET_PER_OA_RSRC_AVAIL (pCrlspTnlInfo,
                                                      u2PscIndex);
                    }
                }
            }
        }
    }
    return u2RetVal;
}

/*****************************************************************************
* Function Name : LdpDiffServTcFreeResources        
* Description   : This routine will be invoked by the LDP with diffServ
*                 feature.This routine is expected to release a set of
*                 resources. that have been reserved earlier on per OA basis.
* Input(s)      : pCrlspTnlInfo - Pointer to the corresponding tunnel.
* Output(s)     : None.
* Return(s)     : None.
*****************************************************************************/
VOID
LdpDiffServTcFreeResources (tCrlspTnlInfo * pCrlspTnlInfo)
{
    UINT2               u2PscIndex;
    tTcFilterInfo       FilterInfo;
    tTcFilterInfo      *pFilterInfo = &FilterInfo;
    UINT1               u1ToFree = FALSE;

    /* Copy the Tunnel Index table in to FilterSpec */
    pFilterInfo->u4TnlId =
        CRLSP_TE_TNL_INDEX (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    pFilterInfo->u4TnlInstance =
        CRLSP_TE_TNL_INSTANCE (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    MEMCPY ((UINT1 *) &pFilterInfo->u4SrcAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_INGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &pFilterInfo->u4DestAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_EGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);

    /* If the Tunnel has a modification, only the difference in resources
     * will be released if it is on the same IfIndex.But if the resorces are 
     * less than the tunnel instance resources , then only the difference in
     * the resources are released
     */
    for (u2PscIndex = 0; u2PscIndex < LDP_DS_MAX_NO_PSCS; u2PscIndex++)
    {
        if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
            (pCrlspTnlInfo, u2PscIndex) == NULL)
        {
            continue;
        }
        /* Check if it has the resources */
        if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
        {
            if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                (CRLSP_INS_POIN (pCrlspTnlInfo), u2PscIndex) != NULL)
            {
                if (LCB_OUT_IFINDEX (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo)))
                    == LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo))))
                {
                    TcDissociateFilterInfo (LDP_DS_PER_OA_GRP_ID (pCrlspTnlInfo,
                                                                  u2PscIndex),
                                            pFilterInfo);
                    LDP_DS_PER_OA_GRP_ID (pCrlspTnlInfo, u2PscIndex) =
                        LDP_INVALID_RSRC_GRPID;
                }
                else
                {
                    u1ToFree = TRUE;
                }
            }
            else
            {
                u1ToFree = TRUE;
            }
        }
        if ((CRLSP_INS_POIN (pCrlspTnlInfo) == NULL) || (u1ToFree == TRUE))
        {
            TcFreeResources (LDP_DS_PER_OA_GRP_ID (pCrlspTnlInfo, u2PscIndex));
            LDP_DBG1 (LDP_DS_MEM,
                      "Msg: DiffServ Resources released. Id released = %d\n",
                      LDP_DS_PER_OA_GRP_ID (pCrlspTnlInfo, u2PscIndex));
            LDP_DS_PER_OA_GRP_ID (pCrlspTnlInfo, u2PscIndex) =
                LDP_INVALID_RSRC_GRPID;
        }
    }
    return;
}

/*****************************************************************************
* Function Name : ldpDSResvTcResourcesInClassType
* Description   : This routine will be invoked by the LDP with diffServ Module
*                 This routine will invoke the supporting routine provide 
*                 by the Resource manager.    
*                 This routine is expected to check the availability of the 
*                 resources requested and if they are available in the 
*                 ClassType, reserve the resources and return an identifier
*                 for the reserved set of
*                 resources.                                                
* Input(s)      : pCrlspTnlInfo  - Pointer to the Tunnel for which the     
*                          resources needs to be checked and reserved.    
* Return(s)     : LDP_SUCCESS in case the resources are available and  
*                 reserved LDP_FAILURE otherwise                      
*****************************************************************************/
UINT1
LdpDSTcResvResourcesInClassType (tCrlspTnlInfo * pCrlspTnlInfo)
{
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        (LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo));
    tMplsDiffServTnlInfo *pTnlDiffServParams = NULL;
    tuTcResSpec         CrldpResSpec;
    tuTcResSpec        *pTcResSpec = &CrldpResSpec;
    tTcFilterInfo       FilterInfo;
    tTcFilterInfo      *pFilterInfo = &FilterInfo;
    tTcResHandle        ResHandle;
    tTcResHandle       *pResHandle = &ResHandle;
    UINT1               u1ResvClass = FALSE;
    UINT1               u1ErrorCode = LDP_ZERO;

    UINT1               u1ClassType = LDP_TE_DS_CLASS_TYPE (pDiffServTnlInfo);
    tTeTnlInfo         *pTeTnlInfo = CRLSP_TE_TNL_INFO (pCrlspTnlInfo);

    if (CRLSP_TE_TNL_TRFC_PARAM (pTeTnlInfo) == NULL)
    {
        return LDP_SUCCESS;
    }

    /* Copy the Tunnel Index table in to FilterSpec */
    pFilterInfo->u4TnlId =
        CRLSP_TE_TNL_INDEX (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    pFilterInfo->u4TnlInstance =
        CRLSP_TE_TNL_INSTANCE (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    MEMCPY ((UINT1 *) &pFilterInfo->u4SrcAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_INGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &pFilterInfo->u4DestAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_EGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);

    /* Copy the Required Traffic specifications in to TcResSpec */
    MEMCPY ((UINT1 *) &pTcResSpec->CrldpTrfcParams,
            (UINT1 *)
            CRLSP_TE_CRLDP_TRFC_PARAMS (CRLSP_TE_TNL_TRFC_PARAM
                                        (CRLSP_TE_TNL_INFO (pCrlspTnlInfo))),
            sizeof (tCRLDPTrfcParams));

    /* If the Tunnel has a modification, only the difference in resources
     * will be reserved if it is on the same IfIndex.
     */

    pTnlDiffServParams = (LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo));

    if (LDP_TE_DS_CLASS_TYPE (pTnlDiffServParams) == u1ClassType)
    {
        if (u1ClassType == 0)
        {
            u1ClassType = TC_DIFFSERV_CLASS_CLASSTYPE_0;
        }
        else if (u1ClassType == 1)
        {
            u1ClassType = TC_DIFFSERV_CLASS_CLASSTYPE_1;
        }
        else
        {
            LDP_DBG (LDP_ADVT_MEM, "ADVT: Invalid Class Type \n");
            return LDP_FAILURE;
        }

        if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL) &&
            (u1ClassType == LDP_TE_DS_CLASS_TYPE
             (LDP_DS_CRLSP_DIFFSERV_PARAMS (CRLSP_INS_POIN (pCrlspTnlInfo)))))
        {

            if (LCB_OUT_IFINDEX (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo)))
                == LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo))))
            {
                if (TcAssociateFilterInfo
                    (LDP_DS_PARAMS_CLASSTYPE_GRPID
                     (CRLSP_INS_POIN (pCrlspTnlInfo)), pFilterInfo,
                     TC_CRLDP_TYPE_RES_SPEC, pTcResSpec,
                     &u1ErrorCode) != TC_SUCCESS)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Filter Spec Association failed \n");
                    return LDP_FAILURE;
                }
                else
                {
                    LDP_DS_PARAMS_CLASSTYPE_GRPID (pCrlspTnlInfo) =
                        LDP_DS_PARAMS_CLASSTYPE_GRPID (CRLSP_INS_POIN
                                                       (pCrlspTnlInfo));
                    LDP_DBG1 (LDP_DS_MEM,
                              "ADVT: Resources reserved. Id returned = %d\n",
                              LDP_DS_PARAMS_CLASSTYPE_GRPID (pCrlspTnlInfo));
                    return LDP_SUCCESS;
                }
            }
            else
            {
                u1ResvClass = TRUE;
            }
        }
        else
        {
            u1ResvClass = TRUE;
        }
        if ((CRLSP_INS_POIN (pCrlspTnlInfo) == NULL) || (u1ResvClass == TRUE))
        {

            if (TcResvResources (TC_CRLDP_APPID,
                                 LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo))),
                                 TC_CRLDP_TYPE_RES_SPEC, pTcResSpec,
                                 u1ClassType, pResHandle, &u1ErrorCode)
                != TC_SUCCESS)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Reservation of Resources Failed \n");
                return LDP_FAILURE;
            }
            else if (TcAssociateFilterInfo (*pResHandle,
                                            pFilterInfo,
                                            TC_CRLDP_TYPE_RES_SPEC,
                                            pTcResSpec, &u1ErrorCode)
                     != TC_SUCCESS)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Filter Spec Association failed \n");
                TcFreeResources (*pResHandle);
                return LDP_FAILURE;
            }
            else
            {
                LDP_DS_PARAMS_CLASSTYPE_GRPID (pCrlspTnlInfo) = *pResHandle;
            }
        }
    }
    else
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: Invalid Class Type \n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDSRelResourcesInClassType
* Description   : This routine will be invoked by the LDP with diffServ 
*                 module. This routine will invoke the supporting routine 
*                 provide by the Resource  Manager.
*                 This routine is expected to release a set of resources    
*                 that have been reserved earlier. The set of resources     
* Input(s)      : pCrlspTnlInfo - Pointer to the corresponding tunnel.      
* Output(s)     : None.                                                     
* Return(s)     : None.                                                     
*****************************************************************************/
VOID
LdpDSRelResourcesInClassType (tCrlspTnlInfo * pCrlspTnlInfo)
{
    tMplsDiffServTnlInfo *pDiffServTnlInfo =
        LDP_DS_CRLSP_DIFFSERV_PARAMS (pCrlspTnlInfo);
    tMplsDiffServTnlInfo *pInsDiffServParams = NULL;
    tTcFilterInfo       FilterInfo;
    tTcFilterInfo      *pFilterInfo = &FilterInfo;
    UINT1               u1ToFree = FALSE;
    UINT1               u1ClassType = LDP_TE_DS_CLASS_TYPE (pDiffServTnlInfo);

    /* Copy the Tunnel Index table in to FilterSpec */
    pFilterInfo->u4TnlId =
        CRLSP_TE_TNL_INDEX (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    pFilterInfo->u4TnlInstance =
        CRLSP_TE_TNL_INSTANCE (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    MEMCPY ((UINT1 *) &pFilterInfo->u4SrcAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_INGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &pFilterInfo->u4DestAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_EGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);

    /* If the Tunnel has a modification, only the difference in resources
     * will be released if it is on the same IfIndex.
     */

    if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
    {
        pInsDiffServParams =
            (LDP_DS_CRLSP_DIFFSERV_PARAMS (CRLSP_INS_POIN (pCrlspTnlInfo)));

        if (LDP_TE_DS_CLASS_TYPE (pInsDiffServParams) == u1ClassType)
        {
            if (LCB_OUT_IFINDEX (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo))) ==
                LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo))))
            {
                TcDissociateFilterInfo (LDP_DS_PARAMS_CLASSTYPE_GRPID
                                        (pCrlspTnlInfo), pFilterInfo);
                LDP_DS_PARAMS_CLASSTYPE_GRPID (pCrlspTnlInfo) =
                    LDP_INVALID_RSRC_GRPID;
                return;
            }
            else
            {
                u1ToFree = TRUE;
            }
        }
        else
        {
            u1ToFree = TRUE;
        }
    }
    if ((CRLSP_INS_POIN (pCrlspTnlInfo) == NULL) || (u1ToFree == TRUE))
    {
        TcFreeResources (LDP_DS_PARAMS_CLASSTYPE_GRPID (pCrlspTnlInfo));
        LDP_DS_PARAMS_CLASSTYPE_GRPID (pCrlspTnlInfo) = LDP_INVALID_RSRC_GRPID;
    }
    return;
}

/******************************************************************************
* Function Name : LdpDiffServHandleModElspTrfcProfile
* Description   : This routine is called to check whether the ELSP-TPtraffic
*                 parameters received in the label mapping message are      
*                 different from the values sent in the label request       
*                 message. IF any one of the parameter is different, the    
*                 Resource manager will be requested to reserve for the     
*                 modified set of parameters and release the extra resources
* Input(s)      : pLspCtrlBlock, Msg , pu1ElspTPTlv
* Output(s)     : None                                                      
* Return(s)     : None                                                      
*******************************************************************************/
UINT1
LdpDiffServHandleModElspTrfcProfile (tLspCtrlBlock * pLspCtrlBlock,
                                     UINT1 *pMsg, UINT1 *pu1ElspTPTlv)
{

    UINT2               u2Count;
    UINT2               u2PscIndex = 0;
    UINT1               u1Dscp;
    UINT1              *pu1ElspTPInfo = pu1ElspTPTlv;
    tLdpTeTrfcParams   *pTeDSTrfcParms = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = pLspCtrlBlock->pCrlspTnlInfo;
    tDiffServElspTPTlvPrefix ElspTPTlvPrefix;
    tLdpDiffServElspTPEntry ElspTPMapEntry;
    tuTcResSpec         CrldpResSpec;
    tuTcResSpec        *pTcResSpec = &CrldpResSpec;
    tTcFilterInfo       FilterInfo;
    tTcFilterInfo      *pFilterInfo = &FilterInfo;
    UINT1               u1ErrorCode = LDP_ZERO;

    LDP_SUPPRESS_WARNING (pMsg);

    /* Copy the Tunnel Index table in to FilterSpec */
    pFilterInfo->u4TnlId =
        CRLSP_TE_TNL_INDEX (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    pFilterInfo->u4TnlInstance =
        CRLSP_TE_TNL_INSTANCE (CRLSP_TE_TNL_INFO (pCrlspTnlInfo));
    MEMCPY ((UINT1 *) &pFilterInfo->u4SrcAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_INGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &pFilterInfo->u4DestAddr,
            (UINT1 *)
            &CRLSP_TE_TNL_EGRESS_RTR_ID (CRLSP_TE_TNL_INFO (pCrlspTnlInfo)),
            IPV4_ADDR_LENGTH);

    pu1ElspTPInfo += LDP_TLV_HDR_LEN;

    LDP_EXT_2_BYTES (pu1ElspTPInfo,
                     LDP_DS_ELSPTP_TLV_PREFIX_RESERVED (ElspTPTlvPrefix));
    LDP_EXT_2_BYTES (pu1ElspTPInfo,
                     LDP_DS_ELSPTP_TLV_PREFIX_NOOFTPENTRIES (ElspTPTlvPrefix));

    for (u2Count = 0;
         u2Count < LDP_DS_ELSPTP_TLV_PREFIX_NOOFTPENTRIES (ElspTPTlvPrefix);
         u2Count++)
    {
        LDP_EXT_2_BYTES (pu1ElspTPInfo,
                         LDP_DS_ELSPTP_MAPENTRY_RESERVED2 (ElspTPMapEntry));
        LDP_EXT_2_BYTES (pu1ElspTPInfo, LDP_DS_ELSPTP_MAPENTRY_PSC
                         (ElspTPMapEntry));

        LDP_DS_DSCP_FOR_PHBID (LDP_DS_ELSPTP_MAPENTRY_PSC (ElspTPMapEntry),
                               u1Dscp);

        LdpDiffServGetIndexForPsc (u1Dscp, &u2PscIndex);

        pTeDSTrfcParms = LDP_DS_PEROATRFC_PARAMS (pCrlspTnlInfo, u2PscIndex);

        LDP_EXT_1_BYTES (pu1ElspTPInfo, ElspTPMapEntry.u1Flags);
        LDP_EXT_1_BYTES (pu1ElspTPInfo, ElspTPMapEntry.u1Frequency);
        LDP_EXT_1_BYTES (pu1ElspTPInfo, ElspTPMapEntry.u1Reserved);
        LDP_EXT_1_BYTES (pu1ElspTPInfo, ElspTPMapEntry.u1Weight);

        LDP_EXT_4_BYTES (pu1ElspTPInfo, ElspTPMapEntry.u4PeakDataRate);
        LDP_EXT_4_BYTES (pu1ElspTPInfo, ElspTPMapEntry.u4PeakBurstSize);
        LDP_EXT_4_BYTES (pu1ElspTPInfo, ElspTPMapEntry.u4CommittedDataRate);
        LDP_EXT_4_BYTES (pu1ElspTPInfo, ElspTPMapEntry.u4CommittedBurstSize);
        LDP_EXT_4_BYTES (pu1ElspTPInfo, ElspTPMapEntry.u4ExcessBurstSize);

        if (ElspTPMapEntry.u4CommittedDataRate > ElspTPMapEntry.u4PeakDataRate)
        {
            LDP_DBG1 (LDP_DS_MISC,
                      "ADVT: Notif Msg Snt - Elsp Trfc Prms Unavl - %x \n",
                      LDP_STAT_TRAF_PARMS_UNAVL);
            return LDP_FAILURE;
        }
        if ((CRLSP_TE_CRLDP_TPARAM_WEIGHT (pTeDSTrfcParms) !=
             ElspTPMapEntry.u1Weight)
            || (CRLSP_TE_CRLDP_TPARAM_PDR (pTeDSTrfcParms) !=
                ElspTPMapEntry.u4PeakDataRate)
            || (CRLSP_TE_CRLDP_TPARAM_PBS (pTeDSTrfcParms) !=
                ElspTPMapEntry.u4PeakBurstSize)
            || (CRLSP_TE_CRLDP_TPARAM_CDR (pTeDSTrfcParms) !=
                ElspTPMapEntry.u4CommittedDataRate)
            || (CRLSP_TE_CRLDP_TPARAM_CBS (pTeDSTrfcParms) !=
                ElspTPMapEntry.u4CommittedBurstSize)
            || (CRLSP_TE_CRLDP_TPARAM_EBS (pTeDSTrfcParms) !=
                ElspTPMapEntry.u4ExcessBurstSize))
        {

            /* Assuming that only PDR changes */
            if (LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfo, u2PscIndex) >
                (ElspTPMapEntry.u4PeakDataRate))
            {
                /* if tunnel instance == NULL,  */
                if (CRLSP_INS_POIN (pCrlspTnlInfo) == NULL)
                {

                    MEMCPY ((UINT1 *) &pTcResSpec->CrldpTrfcParams,
                            (UINT1 *)
                            TE_CRLDP_TRFC_PARAMS (LDP_DS_PEROATRFC_PARAMS
                                                  (pCrlspTnlInfo, u2PscIndex)),
                            sizeof (tCRLDPTrfcParams));

                    if (TcModifyResources (LDP_DS_PER_OA_GRP_ID (pCrlspTnlInfo,
                                                                 u2PscIndex),
                                           TC_CRLDP_TYPE_RES_SPEC,
                                           pTcResSpec, (UINT1) u2PscIndex,
                                           &u1ErrorCode) == TC_FAILURE)
                    {
                        LDP_DBG1 (LDP_ADVT_MEM,
                                  "ADVT: Modifying Rsrcs Failed  "
                                  "RsrcGrpId = %d\n",
                                  LDP_DS_PER_OA_GRP_ID (pCrlspTnlInfo,
                                                        u2PscIndex));
                        return LDP_FAILURE;
                    }
                }
            }
            CRLSP_TE_CRLDP_TPARAM_WEIGHT (pTeDSTrfcParms) =
                ElspTPMapEntry.u1Weight;
            CRLSP_TE_CRLDP_TPARAM_PDR (pTeDSTrfcParms) =
                ElspTPMapEntry.u4PeakDataRate;
            CRLSP_TE_CRLDP_TPARAM_PBS (pTeDSTrfcParms) =
                ElspTPMapEntry.u4PeakBurstSize;
            CRLSP_TE_CRLDP_TPARAM_CDR (pTeDSTrfcParms) =
                ElspTPMapEntry.u4CommittedDataRate;
            CRLSP_TE_CRLDP_TPARAM_CBS (pTeDSTrfcParms) =
                ElspTPMapEntry.u4CommittedBurstSize;
            CRLSP_TE_CRLDP_TPARAM_EBS (pTeDSTrfcParms) =
                ElspTPMapEntry.u4ExcessBurstSize;
        }
    }
    return LDP_SUCCESS;
}

/*----------------------------------------------------------------------------*/
/*                        End of file ldptcif.c                               */
/*----------------------------------------------------------------------------*/
