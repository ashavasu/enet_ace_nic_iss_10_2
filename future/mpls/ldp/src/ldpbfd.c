/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpbfd.c,v 1.3 2016/03/08 11:54:35 siva Exp $
 *
 *********************************************************************/
/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpbfd.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : LDP 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : 
 *----------------------------------------------------------------------------*/

#include "ldpincs.h"


#ifdef MPLS_LDP_BFD_WANTED

/**************************************************************************/
/* Function Name : LdpRegisterWithBfd                                     */
/*                                                                        */
/* Description   : This function is to call the BFD API to register with  */
/*                 BFD module to monitor the Ldp Session IP path          */
/* Inputs        : pLdpSession - Pointer to Ldp Session                   */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : LDP_SUCCESS/LDP_FAILURE                                */
/**************************************************************************/

INT4 LdpRegisterWithBfd (tLdpSession *pLdpSession)
{
   static tBfdReqParams BfdInParams;
   UINT4               u4ContextId = LDP_ZERO;
   UINT1               au1EventMask[1] = { LDP_ZERO };
   tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
   tLdpBfdSession     *pLdpBfdSession;

   LDP_DBG (LDP_MAIN_ALL,"LdpRegisterWithBfd ENTRY\n ");
   MEMSET (au1EventMask, 0, sizeof (au1EventMask));

   if (NULL == pLdpSession)
   {
      LDP_DBG (LDP_MAIN_ALL,"LdpSession not found\n");
      return LDP_FAILURE;
   }
   
   if (LDP_BFD_SESSION_COUNT (MPLS_DEF_INCARN) == LDP_MAX_BFD_SESSIONS)
   {
      LDP_DBG1 (LDP_MAIN_ALL,"BFD IP monitoring for maximum %d LDP sessions \
	             is supported", LDP_MAX_BFD_SESSIONS);
	  return LDP_FAILURE;
   }

   BfdInParams.u4ReqType = BFD_CLIENT_REGISTER_FOR_IP_PATH_MONITORING;
   BfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_LDP;
   BfdInParams.u4ContextId = u4ContextId;

  
   pLdpTcpUdpSockInfo = LdpGetSockInfoFromConnId (pLdpSession->u4TcpConnId);

   if (NULL == pLdpTcpUdpSockInfo)
   {
       LDP_DBG (LDP_MAIN_ALL,"LdpTcpUdpsockInfo not found\n");
       return LDP_FAILURE;
   }

   LDP_DBG4(LDP_MAIN_ALL,"LdpTcpUdpsockInfo :: u4ConnId = %u u4SrcAddr = %u u4DestAddr = %u u4IfIndex = %d\n", 
                   pLdpTcpUdpSockInfo->u4ConnId,pLdpTcpUdpSockInfo->SrcAddr.au1Ipv4Addr, pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, pLdpTcpUdpSockInfo->u4IfIndex);
   BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType =  
                                     pLdpTcpUdpSockInfo->u1AddrType;

#ifdef MPLS_IPV6_WANTED
   if (pLdpTcpUdpSockInfo->u1AddrType == LDP_ADDR_TYPE_IPV4)
   {
#endif
      MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
	           &LDP_PEER_TRANS_ADDR (pLdpSession->pLdpPeer).Addr.au1Ipv4Addr,LDP_NET_ADDR_LEN);
      MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
	           &pLdpSession->SrcTransAddr.Addr.au1Ipv4Addr,LDP_NET_ADDR_LEN);

#ifdef MPLS_IPV6_WANTED   
   }
   else
   {

      MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
              &LDP_PEER_TRANS_ADDR (pLdpSession->pLdpPeer).Addr.Ip6Addr,LDP_NET_ADDR6_LEN);
	  MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
	          &pLdpSession->SrcTransAddr.Addr.Ip6Addr,LDP_NET_ADDR6_LEN);

   }
#endif

   if (LDP_ENTITY_IS_TARGET_TYPE(pLdpSession->pLdpPeer->pLdpEntity) == LDP_TRUE)
   {
      BfdInParams.BfdClntInfo.u1SessionType = 
	                          BFD_SESS_TYPE_MULTIHOP_UNIDIRECTIONAL_LINKS;
   }
   else
   {
      BfdInParams.BfdClntInfo.u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;
   }

   BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex = 
                                           pLdpTcpUdpSockInfo->u4IfIndex;
   
   /* The Desired detection timer is not configurable. hence making
    * this value as default detection multipler as BFD 3Sec*/

   BfdInParams.BfdClntInfoParams.u4DesiredDetectionTime = BFD_DEFAULT_TIME_OUT;
   /* Informing status down notification to OSPF Que.
    * OSIX_BITLIST_SET_BIT utility will not check the bit against Zero,
    * Hence the bit + 1 used to set/check the bit */
   OSIX_BITLIST_SET_BIT (au1EventMask, (BFD_SESS_STATE_DOWN + 1), 1);
   MEMCPY (&(BfdInParams.BfdClntInfoParams.u1EventMask), au1EventMask, 1);

   BfdInParams.BfdClntInfoParams.pBfdNotifyPathStatusChange =
                                LdpHandleNbrPathStatusChange;

   LDP_DBG4(LDP_MAIN_ALL,"LdpTcpUdpsockInfo :: u4ConnId = %u u4SrcAddr = %u u4DestAddr = %u u4IfIndex = %d\n",
            pLdpTcpUdpSockInfo->u4ConnId,pLdpTcpUdpSockInfo->SrcAddr.au1Ipv4Addr, pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, pLdpTcpUdpSockInfo->u4IfIndex);

   /* BFD API call for registering  Ldp session path monitoring */
   if (BfdApiHandleExtRequest (u4ContextId, &BfdInParams,
                               NULL) != OSIX_SUCCESS)
   {
      LDP_DBG1 (LDP_MAIN_ALL, "MAIN: Bfd Registration for session index %d failed\n",
	                  pLdpSession->u4Index);
	  return LDP_FAILURE;
   }
   LDP_BFD_SESSION_COUNT(MPLS_DEF_INCARN)++;
   pLdpSession->i4SessBfdStatus = LDP_BFD_ENABLED;

 if ( BfdInParams.BfdClntInfo.u1SessionType == BFD_SESS_TYPE_SINGLE_HOP)
 {
    if (LDP_SUCCESS ==  LdpGetDisabledLdpBfdSession(pLdpSession->pLdpPeer->pLdpEntity,
	                    LDP_PEER_TRANS_ADDR (pLdpSession->pLdpPeer), pLdpSession->SrcTransAddr,
						pLdpTcpUdpSockInfo->u1AddrType, &pLdpBfdSession))
	{
	    LDP_DBG (LDP_MAIN_ALL,"LdpBfdSession found\n");
		/*  If there already exist a bfd ldp session with different ifindex, 
		 *  delete that session*/
        if (pLdpBfdSession->u4IfIndex != pLdpTcpUdpSockInfo->u4IfIndex)
		{
		    LDP_DBG (LDP_MAIN_ALL,"Ifindex not matching,session deregistered\n");
			LdpDeRegisterDisabledBfdSess(pLdpBfdSession);    
		}
	}
}
   LdpCreateBfdSession (pLdpSession->pLdpPeer->pLdpEntity, pLdpSession,
                        pLdpTcpUdpSockInfo->u4IfIndex, pLdpTcpUdpSockInfo->u1AddrType);

   LDP_DBG1 (LDP_MAIN_ALL, "MAIN: Bfd Registration for session index %d successful\n",
                   pLdpSession->u4Index);
   LDP_DBG (LDP_MAIN_ALL,"LdpRegisterWithBfd EXIT \n");
   return LDP_SUCCESS;
}

/**************************************************************************/
/* Function Name : LdpDeRegisterWithBfd                                   */
/*                                                                        */
/* Description   : This function is to call the BFD API to Deregister or  */
/*                 Disable with BFD module for the Ldp session  IP path   */
/*                 which was already registered                           */
/* Inputs        : pLdpSession - Pointer to Ldp Session                   */
/*                 bflag - Flag to decide whether to de-register/disable  */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : LDP_SUCCESS/LDP_FAILURE                                */
/**************************************************************************/

INT4 LdpDeRegisterWithBfd (tLdpSession *pLdpSession)
{

   static tBfdReqParams BfdInParams;
   UINT4               u4ContextId = LDP_ZERO;
   UINT1               au1EventMask[1] = { LDP_ZERO };
   tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
   tLdpBfdSession     *pLdpBfdSession = NULL;
   tLdpEntity         *pLdpEntity = NULL;
   LDP_DBG (LDP_MAIN_ALL,"LdpDeRegisterWithBfd ENTRY ");
   
   MEMSET (au1EventMask, 0, sizeof (au1EventMask));

   if (NULL == pLdpSession)
   {
      LDP_DBG (LDP_MAIN_ALL,"LdpSession not found\n");
	  return LDP_FAILURE;
   }

   if ((NULL == pLdpSession->pLdpPeer) || (NULL == pLdpSession->pLdpPeer->pLdpEntity))
   {
       LDP_DBG (LDP_MAIN_ALL,"LdpPeer or LdpEntity not found not found\n");
       return LDP_FAILURE;
   }
   if (LDP_FAILURE ==  LdpGetLdpBfdSession(pLdpSession->pLdpPeer->pLdpEntity,
                       pLdpSession->u4TcpConnId, &pLdpBfdSession))
   {  
       LDP_DBG (LDP_MAIN_ALL,"LdpBfdSession not found\n");
       return LDP_FAILURE;
   }
  

   /* Filling LdpSession path info for Registering with BFD */
      BfdInParams.u4ReqType = BFD_CLIENT_DEREGISTER_FROM_IP_PATH_MONITORING;

   BfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_LDP;
   BfdInParams.u4ContextId = u4ContextId;
   
   pLdpTcpUdpSockInfo = LdpGetSockInfoFromConnId (pLdpSession->u4TcpConnId);
   LDP_DBG1(LDP_MAIN_ALL,"LdpTcpUdpsockInfo :: u4ConnId = %d ",
                       pLdpSession->u4TcpConnId);

   if (NULL == pLdpTcpUdpSockInfo)
   {
      LDP_DBG (LDP_MAIN_ALL,"LdpTcpUdpsockInfo not found\n");
	  return LDP_FAILURE;
   }
   
   LDP_DBG4(LDP_MAIN_ALL,"LdpTcpUdpsockInfo :: u4ConnId = %d u4SrcAddr = %d u4DestAddr = %d u4IfIndex = %d\n",
               pLdpTcpUdpSockInfo->u4ConnId,pLdpTcpUdpSockInfo->SrcAddr, pLdpTcpUdpSockInfo->DestAddr, pLdpTcpUdpSockInfo->u4IfIndex);

   BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType =  
                                    pLdpTcpUdpSockInfo->u1AddrType;

#ifdef MPLS_IPV6_WANTED
   if (pLdpTcpUdpSockInfo->u1AddrType == LDP_ADDR_TYPE_IPV4)
   {
#endif
      MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
	           &LDP_PEER_TRANS_ADDR (pLdpSession->pLdpPeer).Addr.au1Ipv4Addr,LDP_NET_ADDR_LEN);
      MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
	           &pLdpSession->SrcTransAddr.Addr.au1Ipv4Addr,LDP_NET_ADDR_LEN);

#ifdef MPLS_IPV6_WANTED   
   }
   else
   {
      MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
              &LDP_PEER_TRANS_ADDR (pLdpSession->pLdpPeer).Addr.Ip6Addr,LDP_NET_ADDR6_LEN);
	  MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
	          &pLdpSession->SrcTransAddr.Addr.Ip6Addr,LDP_NET_ADDR6_LEN);
   }
   
#endif 
   if (LDP_ENTITY_IS_TARGET_TYPE(pLdpSession->pLdpPeer->pLdpEntity) == LDP_TRUE)
   {
      BfdInParams.BfdClntInfo.u1SessionType = 
	                          BFD_SESS_TYPE_MULTIHOP_UNIDIRECTIONAL_LINKS;
   }
   else
   {
      BfdInParams.BfdClntInfo.u1SessionType = BFD_SESS_TYPE_SINGLE_HOP;
   }
  
   BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex =
                                          pLdpTcpUdpSockInfo->u4IfIndex;
   /* The Desired detection timer is not configurable. hence making
    * this value as default detection multipler as BFD 3Sec*/
   
   BfdInParams.BfdClntInfoParams.u4DesiredDetectionTime = BFD_DEFAULT_TIME_OUT;
   /* Informing status down notification to LDP Que.
    * OSIX_BITLIST_SET_BIT utility will not check the bit against Zero,
    * Hence the bit + 1 used to set/check the bit */
   OSIX_BITLIST_SET_BIT (au1EventMask, (BFD_SESS_STATE_DOWN + 1), 1);
   MEMCPY (&(BfdInParams.BfdClntInfoParams.u1EventMask), au1EventMask, 1);

   /* BFD API call for registering Ldp session for path monitoring */
   if (BfdApiHandleExtRequest (u4ContextId, &BfdInParams,
                               NULL) != OSIX_SUCCESS)
   {
      LDP_DBG1 (LDP_MAIN_ALL, "MAIN: Bfd DeRegistration for session index %d failed\n",
	                      pLdpSession->u4Index);
	  return LDP_FAILURE;
   }
   LDP_BFD_SESSION_COUNT(MPLS_DEF_INCARN)--;
   pLdpSession->i4SessBfdStatus = LDP_BFD_DISABLED;

      pLdpEntity = SSN_GET_ENTITY(pLdpSession);
	  TMO_SLL_Delete (&(pLdpEntity->BfdSessList), &(pLdpBfdSession->NextBfdSess));  
      MemReleaseMemBlock (LDP_BFD_SSN_POOL_ID, (UINT1 *)pLdpBfdSession );

   LDP_DBG1 (LDP_MAIN_ALL, "MAIN: Bfd DeRegistration for session index %d successful\n",
                   pLdpSession->u4Index);
   LDP_DBG (LDP_MAIN_ALL,"LdpDeRegisterWithBfd EXIT ");
   return LDP_SUCCESS;
}

/**************************************************************************/
/* Function Name : LdpDeRegisterAllWithBfd                                */
/*                                                                        */
/* Description   : This function is to call the BFD API to Deregister     */
/*                 with BFD module for All Ldp sessions  IP path          */
/*                 which was already registered                           */
/*                                                                        */
/* Return values : LDP_SUCCESS/LDP_FAILURE                                */
/**************************************************************************/

INT4 LdpDeRegisterAllWithBfd (VOID)
{
   static tBfdReqParams BfdInParams;
   UINT4               u4ContextId = 0;

   LDP_DBG (LDP_MAIN_ALL,"LdpDeRegisterAllWithBfd ENTRY ");
   BfdInParams.u4ReqType = BFD_CLIENT_DEREGISTER_ALL_FROM_IP_PATH_MONITORING;
   BfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_LDP;
   BfdInParams.u4ContextId = u4ContextId;
   
   /* BFD API call for registering Ldp session for path monitoring */
   if (BfdApiHandleExtRequest (u4ContextId, &BfdInParams,
                               NULL) != OSIX_SUCCESS)
   {
      LDP_DBG (LDP_MAIN_ALL, "MAIN: Bfd DeRegistration for all LdpSessions failed\n");

      return LDP_FAILURE;
   }
   LDP_DBG (LDP_MAIN_ALL, "MAIN: Bfd DeRegistration for all LdpSessions successful\n");
   LDP_DBG (LDP_MAIN_ALL,"LdpDeRegisterAllWithBfd EXIT ");
   return LDP_SUCCESS;
}

/**************************************************************************/
/* Function Name : LdpHandleNbrPathStatusChange                           */
/*                                                                        */
/* Description   : This function is to call the BFD API to Deregister with*/
/*                 BFD module for the LDP session IP path which was       */
/*                 already registered                                     */
/* Inputs        : u4ContextId - Context Identifier                       */
/*                 pNbrPathInfo  - Pointer to the LDP peer entry          */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : LDP_SUCCESS/LDP_FAILURE                                */
/**************************************************************************/
INT1
LdpHandleNbrPathStatusChange (UINT4 u4ContextId,
                               tBfdClientNbrIpPathInfo *pNbrPathInfo)
{
    tLdpIfMsg           LdpIfMsg; 

	UNUSED_PARAM (u4ContextId);

	if (NULL == pNbrPathInfo)
	{
	    LDP_DBG (LDP_MAIN_ALL, "MAIN: Neighbor path info not found\n");
	    return OSIX_FAILURE;
	}
	LDP_DBG (LDP_MAIN_ALL,"LdpHandleNbrPathStatusChange ENTRY ");
	/* LDP should process only the session DOWN notification from BFD*/ 
    if (pNbrPathInfo->u4PathStatus != BFD_SESS_STATE_DOWN)
    {
        LDP_DBG (LDP_MAIN_ALL, "MAIN: Event recieved from bfd is not session down event\n");
		return OSIX_FAILURE;
    }
    
    MEMSET (&LdpIfMsg, LDP_ZERO, sizeof (tLdpIfMsg));

    LdpIfMsg.u4MsgType = LDP_BFD_EVENT;
  
	MEMSET (&LdpIfMsg.u.BfdEvt.LdpBfdMsgInfo , LDP_ZERO, sizeof(tBfdClientNbrIpPathInfo));

	MEMCPY(&LdpIfMsg.u.BfdEvt.LdpBfdMsgInfo, pNbrPathInfo, 
	                        sizeof (tBfdClientNbrIpPathInfo));

	LdpIfMsg.u.BfdEvt.u4BfdEvt = LDP_HANDLE_BFD_SESSION_DOWN_EVENT;    
    
    if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Failed to EnQ Bfd Event to LDP Task\n");
		return OSIX_FAILURE;
    }
    LDP_DBG (LDP_MAIN_ALL,"LdpHandleNbrPathStatusChange EXIT ");  
    return OSIX_SUCCESS;

}

/*****************************************************************************/
/* Function Name : LdpProcessBfdEvent                                        */
/* Description   : It processes the event related with the                   */
/*                 BFD                                                       */
/* Input(s)      :  tLdpBfdEvtInfo - Bfd msg infomation                      */
/* Output(s)     : NONE.                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE.                                  */
/*****************************************************************************/

UINT1 LdpProcessBfdEvent (tLdpBfdEvtInfo * pBfdEvtInfo)
{

   LDP_DBG (LDP_MAIN_ALL,"LdpProcessBfdEvent ENTRY ");
   if(pBfdEvtInfo->u4BfdEvt == LDP_HANDLE_BFD_SESSION_DOWN_EVENT)
   {
	    LdpProcessPeerDownNotification(&pBfdEvtInfo->LdpBfdMsgInfo); 
   }
   LDP_DBG (LDP_MAIN_ALL,"LdpProcessBfdEvent EXIT ");
   return LDP_SUCCESS;
}

/****************************************************************************/
/* Function Name : LdpProcessPeerDownNotification                           */
/* Description   : This function is to process the session DOWN notification*/
/*                 received from the BFD module.                            */
/* Input(s)      : pPeerEntry  - pointer to bfd message info                */
/* Output(s)     : None.                                                    */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                  */
/****************************************************************************/

INT1 LdpProcessPeerDownNotification (tLdpBfdInfo *pLdpBfdMsgInfo)
{
   tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
   uGenAddr           DestAddr;
   UINT2              u2IncarnId = LDP_CUR_INCARN;
   tLdpSession        *pSessionEntry = NULL;

   LDP_DBG (LDP_MAIN_ALL,"LdpProcessPeerDownNotification ENTRY ");
   MEMSET (&DestAddr, LDP_ZERO, sizeof (uGenAddr));

#ifdef MPLS_IPV6_WANTED
   if (pLdpBfdMsgInfo->u1AddrType == LDP_ADDR_TYPE_IPV4)
   {
       MEMCPY (&DestAddr.au1Ipv4Addr,&pLdpBfdMsgInfo->NbrAddr,IPV4_ADDR_LENGTH);
   }
   else
   {
#endif
       MEMCPY (&DestAddr.Ip6Addr,&pLdpBfdMsgInfo->NbrAddr,IPV4_ADDR_LENGTH);
#ifdef MPLS_IPV6_WANTED   
   }
#endif
   
   pLdpTcpUdpSockInfo = 
        LdpTcpUdpGetSocketFrmDestAdd(DestAddr, pLdpBfdMsgInfo->u1AddrType); 
		 
   if (NULL == pLdpTcpUdpSockInfo)
   { 
       LDP_DBG (LDP_MAIN_ALL,"LdpTcpUdpsockInfo not found\n");
	   return LDP_FAILURE;
   }
 
   pSessionEntry =
       LdpGetSsnFromTcpConnTbl (u2IncarnId,
                                pLdpTcpUdpSockInfo->u4ConnId );
   if (NULL == pSessionEntry)
   {
       LDP_DBG (LDP_MAIN_ALL,"LdpSession not found\n");
       return LDP_FAILURE;
   }
   LDP_DBG1 (LDP_MAIN_ALL, "MAIN: Session down notification recieved for session index %d \
                         from bfd\n", pSessionEntry->u4Index);

   pSessionEntry->bIsBfdSessCtrlPlaneInDep = 
                   pLdpBfdMsgInfo->bIsSessCtrlPlaneInDep;

   pSessionEntry->i4SessBfdStatus = LDP_BFD_DISABLED; 
                  
   if (LdpTcpEventHandler (LDP_TCP_CONN_DOWN_EVENT,
                           CONTROL_DATA,
                           pLdpTcpUdpSockInfo->u4ConnId,
                           NULL) == LDP_FAILURE)
   {
           LDP_DBG (LDP_IF_MISC,
                    "LDPTCP: Failed to Indicate TCP Conn up to LDP\n");
   }
   
   LDP_DBG (LDP_MAIN_ALL,"LdpProcessPeerDownNotification EXIT ");
   return LDP_SUCCESS;
}

/**************************************************************************/
/* Function Name : LdpDeRegisterDisabledBfdSess                           */
/*                                                                        */
/* Description   : This function is to call the BFD API to Deregister or  */
/*                 Disable with BFD module for the Ldp session  IP path   */
/*                 which was already registered                           */
/* Inputs        : pLdpBfdSession - Pointer to Ldp Session                   */
/* Outputs       : None                                                   */
/*                                                                        */
/* Return values : LDP_SUCCESS/LDP_FAILURE                                */
/**************************************************************************/

INT4 LdpDeRegisterDisabledBfdSess (tLdpBfdSession *pLdpBfdSession)
{

   static tBfdReqParams BfdInParams;
   UINT4               u4ContextId = LDP_ZERO;
   UINT1               au1EventMask[1] = { LDP_ZERO };
   LDP_DBG (LDP_MAIN_ALL,"LdpDeRegisterDisabledBfdSess ENTRY ");
   
   MEMSET (au1EventMask, 0, sizeof (au1EventMask));

   if (NULL == pLdpBfdSession)
   {
      LDP_DBG (LDP_MAIN_ALL,"LdpBfdSession not found\n");
	  return LDP_FAILURE;
   }
   

  /* Filling LdpSession path info for Registering with BFD */
   BfdInParams.u4ReqType = BFD_CLIENT_DEREGISTER_FROM_IP_PATH_MONITORING;

   BfdInParams.BfdClntInfo.u1BfdClientId = BFD_CLIENT_ID_LDP;
   BfdInParams.u4ContextId = u4ContextId;
   
   BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u1AddrType =
                                       pLdpBfdSession->u1AddrType;
	
#ifdef MPLS_IPV6_WANTED
   if (pLdpBfdSession->u1AddrType == LDP_ADDR_TYPE_IPV4)
   {
#endif
      MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
	           &pLdpBfdSession->DestAddr.Addr.au1Ipv4Addr,LDP_NET_ADDR_LEN);
      MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
	           &pLdpBfdSession->SrcAddr.Addr.au1Ipv4Addr,LDP_NET_ADDR_LEN);

#ifdef MPLS_IPV6_WANTED   
   }
   else
   {
      MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.NbrAddr,
              &pLdpBfdSession->DestAddr.Addr.Ip6Addr,LDP_NET_ADDR6_LEN);
	  MEMCPY (&BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.SrcAddr,
	          &pLdpBfdSession->SrcAddr.Addr.Ip6Addr,LDP_NET_ADDR6_LEN);
   }
   
#endif 

   BfdInParams.BfdClntInfo.u1SessionType = 
	                          pLdpBfdSession->u1SessionType;
  
   BfdInParams.BfdClntInfo.BfdClientNbrIpPathInfo.u4IfIndex =
                                   pLdpBfdSession->u4IfIndex;
   /* The Desired detection timer is not configurable. hence making
    * this value as default detection multipler as BFD 3Sec*/
   
   BfdInParams.BfdClntInfoParams.u4DesiredDetectionTime = BFD_DEFAULT_TIME_OUT;
   /* Informing status down notification to LDP Que.
    * OSIX_BITLIST_SET_BIT utility will not check the bit against Zero,
    * Hence the bit + 1 used to set/check the bit */
   OSIX_BITLIST_SET_BIT (au1EventMask, (BFD_SESS_STATE_DOWN + 1), 1);
   MEMCPY (&(BfdInParams.BfdClntInfoParams.u1EventMask), au1EventMask, 1);

   /* BFD API call for registering Ldp session for path monitoring */
   if (BfdApiHandleExtRequest (u4ContextId, &BfdInParams,
                               NULL) != OSIX_SUCCESS)
   {
      LDP_DBG (LDP_MAIN_ALL, "MAIN: Bfd DeRegistration failed\n");
	  return LDP_FAILURE;
   }

   LDP_DBG (LDP_MAIN_ALL,"LdpDeRegisterDisabledBfdSess EXIT ");
   return LDP_SUCCESS;
}

#endif
