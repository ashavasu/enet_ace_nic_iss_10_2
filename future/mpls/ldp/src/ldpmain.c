
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpmain.c,v 1.95 2017/06/16 13:35:37 siva Exp $
 *              
 ********************************************************************/

#include "ldpincs.h"
#include "ldpgbl.h"
#include "ldpdsgbl.h"
#include "snmputil.h"
#include "ldplwinc.h"
#include "ldpprot.h"
#include "mplsutil.h"
#include "rtm.h"

#include "inmgrex.h"
#include "indexmgr.h"

#ifdef SYSLOG_WANTED
#include "fssyslog.h"
#endif

UINT4               gFecSetTime = 0;
UINT4               gLdpEntitySetTime = 0;
UINT4               gLdpLspFecSetTime = 0;
UINT4               gLdpPeerSetTime = 0;

VOID                LdpDeleteInterfaceControlBlock (tLdpSession * pSessionEntry,
                                                    UINT4 u4IfIndex);
extern UINT4        MplsLdpEntityGenericLRRowStatus[14];
/*****************************************************************************/
/* Function Name : LdpDeInit                                                 */
/* Description   : This routine does a graceful shut down of the ldp protocol*/
/*                 running on an Lsr.                                        */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDeInit (VOID)
{
    UINT2               u2Incarn;
    UINT1               u1LdpIncarnActive = LDP_FALSE;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    tLdpIfMsg          *pLdpMsg = NULL;
    tLdpTcpUdpEnqMsgInfo LdpTcpUdpEnqMsgInfo;

    /* Incarnation count can become zero after deletion */
    if (LDP_ACTIVE_INCRN_COUNT > LDP_ZERO)
    {
        u1LdpIncarnActive = LDP_TRUE;
    }

    for (u2Incarn = 0; u2Incarn < LDP_MAX_INCARN; u2Incarn++)
    {
        if (LDP_INCARN_STATUS (u2Incarn) == ACTIVE)
        {
            LdpDeleteIncarn (u2Incarn, LDP_INCARN_DOWN);
        }
    }
#ifdef SNMP_2_WANTED
    LdpUnRegisterLDPMibs ();
#endif

    MEMSET (&LdpTcpUdpEnqMsgInfo, 0, sizeof (tLdpTcpUdpEnqMsgInfo));
    while (OsixQueRecv (LDP_TCP_QID, (UINT1 *) &pBuf,
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &LdpTcpUdpEnqMsgInfo,
                                       0, sizeof (tLdpTcpUdpEnqMsgInfo)) !=
            sizeof (tLdpTcpUdpEnqMsgInfo))
        {
            LDP_DBG (LDP_IF_MEM,
                     "LDP_TCP : Error while copying from "
                     "the buffer chain\n");
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            continue;
        }
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }
    while (OsixQueRecv
           (LDP_QID, (UINT1 *) &pLdpMsg, OSIX_DEF_MSG_LEN,
            OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LDP_Q_POOL_ID, (UINT1 *) pLdpMsg);
    }
    /* Incarnation count can become zero after deletion */
    if (LDP_ACTIVE_INCRN_COUNT > LDP_ZERO)
    {
        u1LdpIncarnActive = LDP_TRUE;
    }

    /* Making all the active Incarns Inactive */
    for (u2Incarn = 0; u2Incarn < LDP_MAX_INCARN; u2Incarn++)
    {
        if (LDP_INCARN_STATUS (u2Incarn) == ACTIVE)
        {
            LdpDeleteIncarn (u2Incarn, LDP_INCARN_DOWN);
        }
    }

    if (u1LdpIncarnActive == LDP_TRUE)
    {
        /* Closing the Standard Udp Port for LDP */
        LdpUdpClose (LDP_STD_PORT);
        /*MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
        LdpIpv6UdpClose (LDP_STD_PORT);
        /*Closing IPV6 Standard TCP Port for LDP */
        pLdpTcpUdpSockInfo =
            LdpTcpUdpGetSocket (gu4Ipv6TcpStdServConnId, LDP_SOCK_MASK);
        if (pLdpTcpUdpSockInfo != NULL)
        {
            LdpTcpUdpEnqMsgInfo.u4Event = LDP_TCP_CLOSE_REQ_EVENT;
            LdpTcpUdpEnqMsgInfo.i4SockFdOrConnId =
                (INT4) gu4Ipv6TcpStdServConnId;
            LdpTcpProcCloseReqEvent (&LdpTcpUdpEnqMsgInfo);
            pLdpTcpUdpSockInfo = NULL;
        }
#endif
        /*MPLS_IPv6 add end */

        /* Closing the Standard Tcp Port for LDP */
        pLdpTcpUdpSockInfo =
            LdpTcpUdpGetSocket (gu4TcpStdServConnId, LDP_SOCK_MASK);

        if (pLdpTcpUdpSockInfo != NULL)
        {
            LdpTcpUdpEnqMsgInfo.u4Event = LDP_TCP_CLOSE_REQ_EVENT;
            LdpTcpUdpEnqMsgInfo.i4SockFdOrConnId = (INT4) gu4TcpStdServConnId;
            LdpTcpProcCloseReqEvent (&LdpTcpUdpEnqMsgInfo);
        }
        /* De-Register with TC */
        LdpDeRegisterWithTC ();
    }

#ifdef CLI_WANTED
    CsrMemDeAllocation (LDP_MODULE_ID);
#endif
    /* Release the memory blocks from TCP Sock Table. */
    LdpTcpDeInitSockInfo ();

    /* Delete the memory pools in LDP module. */
    LdpSizingMemDeleteMemPools ();

    /* Delete the timer list associated with LDP module. */
    TmrDeleteTimerList (LDP_TIMER_LIST_ID);
    LDP_TIMER_LIST_ID = LDP_ZERO;
    LDP_INITIALISED = FALSE;
}

/*****************************************************************************/
/* Function Name : ldpMain                                                   */
/* Description   : This is the main routine that handles all the events and  */
/*                 inputs related to Timer, SNMP, UDP, TCP, MPLS and IP.     */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTaskMain (INT1 *pDummy)
{
    UINT4               u4RcvdEvent;
#ifdef LDP_HA_WANTED
    INT4                i4RetVal = LDP_FAILURE;
#endif
    /* Dummy Pointers for system sizing */
    tLdpMinMTUSize     *pLdpMinMTUSize = NULL;
    tFecTableEntrySize *pFecTableEntrySize = NULL;
    tLdpTcpUdpSockInfoSize *pLdpTcpUdpSockInfoSize = NULL;
    tLdpMaxFragmentSize *pLdpMaxFragmentSize = NULL;
    tLdpPendMaxFragmentSize *pLdpPendMaxFragmentSize = NULL;
    tL2VpnLdpIfStrLenSize *pL2VpnLdpIfStrLenSize = NULL;
    tKeyInfoStructSize *pKeyInfoStructSize = NULL;

    UNUSED_PARAM (pLdpMinMTUSize);
    UNUSED_PARAM (pFecTableEntrySize);
    UNUSED_PARAM (pLdpTcpUdpSockInfoSize);
    UNUSED_PARAM (pLdpMaxFragmentSize);
    UNUSED_PARAM (pLdpPendMaxFragmentSize);
    UNUSED_PARAM (pL2VpnLdpIfStrLenSize);
    UNUSED_PARAM (pKeyInfoStructSize);

    LDP_SUPPRESS_WARNING (pDummy);
    /* Important! Assign the TASK id immediately otherwise TCP socket
     * connection initializations will not happen properly.
     * Store the LdpTask Id globally */
    if (OsixTskIdSelf (&LDP_TSK_ID) == OSIX_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MEM, "MAIN : LdpTaskId : Getting Task Id Failed\n");
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixQueCrt ((UINT1 *) LDP_QNAME, OSIX_MAX_Q_MSG_LEN,
                    FsLDPSizingParams[MAX_LDP_QDEPTH_SIZING_ID].
                    u4PreAllocatedUnits, &LDP_QID) != OSIX_SUCCESS)
    {
        LDP_DBG (LDP_MAIN_MEM, "MAIN: LdpInQ, Creation Failed\n");
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixQueCrt ((UINT1 *) LDP_UDP_QNAME, OSIX_MAX_Q_MSG_LEN, LDP_UDP_QDEPTH,
                    &LDP_UDP_QID) != OSIX_SUCCESS)
    {
        OsixQueDel (LDP_QID);
        LDP_DBG (LDP_MAIN_MEM, "MAIN: LdpUdPInQ, Creation Failed\n");
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OsixQueCrt ((UINT1 *) LDPTCP_QNAME, OSIX_MAX_Q_MSG_LEN,
                    LDP_TCP_QDEPTH, &LDP_TCP_QID) != OSIX_SUCCESS)
    {
        OsixQueDel (LDP_QID);
        OsixQueDel (LDP_UDP_QID);
        LDP_DBG (LDP_MAIN_MEM, "MAIN : LDPTCP : Q Creation Failed\n");
        return;
    }

    /* Semaphore created for Mutual exclusion on access of common
     * data structure between LDP and other task */
    if ((OsixCreateSem ((const UINT1 *) MPLS_LDP_SEM_NAME, 1, 0,
                        (tOsixSemId *) & LDP_SEM_ID)) != OSIX_SUCCESS)
    {
        LDP_DBG (LDP_MAIN_MEM, "MAIN: Failed to Create Sem \n");
        OsixQueDel (LDP_QID);
        OsixQueDel (LDP_UDP_QID);
        OsixQueDel (LDP_TCP_QID);
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef SYSLOG_WANTED
    LDP_SYSLOG_ID =
        SYS_LOG_REGISTER ((CONST UINT1 *) "LDP", SYSLOG_DEBUG_LEVEL);
#endif
    if (LdpInit () != LDP_SUCCESS)
    {
        LDP_DBG (LDP_MAIN_MEM, "MAIN: Initialization failed\n");
        OsixQueDel (LDP_QID);
        OsixQueDel (LDP_UDP_QID);
        OsixQueDel (LDP_TCP_QID);
        OsixSemDel (LDP_SEM_ID);
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef SNMP_2_WANTED
    LdpRegisterLDPMibs ();
#endif
#ifdef LDP_HA_WANTED
    LdpRmInit ();
    i4RetVal = LdpRmRegisterWithRM ();
    if (i4RetVal != LDP_SUCCESS)
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif

    MPLS_INIT_COMPLETE (OSIX_SUCCESS);
    IssSetModuleSystemControl (LDP_MODULE_ID, MODULE_START);

    while (LDP_ONE)
    {
        if (OsixEvtRecv
            (LDP_TSK_ID, (LDP_MSG_EVT | LDP_UDP_MSG_EVT | LDP_TCP_MSG_EVT |
                          LDP_TMR_EXPR_EVENT | LDP_UDP6_MSG_EVT |
                          LDP_CSR_RESTORE_COMPLELTE), OSIX_WAIT,
             &u4RcvdEvent) != OSIX_SUCCESS)
        {
            continue;
        }
        MPLS_LDP_LOCK ();
        if ((u4RcvdEvent & LDP_MSG_EVT) == LDP_MSG_EVT)
        {
            LdpProcessQueueMsgs ();
        }

        /*  This Event is defined since the registration 
         *  procedure with future IP doesn't provide the 
         *  similar interface for enq in IfMsg format */

        if ((u4RcvdEvent & LDP_UDP_MSG_EVT) == LDP_UDP_MSG_EVT)
        {
            if (gLdpInfo.u1IncarnUp == LDP_TRUE)
            {
                LdpProcessUdpMsgs ();
            }
        }
#ifdef MPLS_IPV6_WANTED
        /* MPLS_IPv6 add start */
        if ((u4RcvdEvent & LDP_UDP6_MSG_EVT) == LDP_UDP6_MSG_EVT)
        {
            if (gLdpInfo.u1IncarnUp == LDP_TRUE)
            {
                LdpProcessIpv6UdpMsgs ();
            }
        }
        /* MPLS_IPv6 add end */
#endif
        if ((u4RcvdEvent & LDP_TCP_MSG_EVT) == LDP_TCP_MSG_EVT)
        {
            LdpProcessTcpMsg ();
        }
        if ((u4RcvdEvent & LDP_TMR_EXPR_EVENT) == LDP_TMR_EXPR_EVENT)
        {
            LdpProcessTimerEvents ();
        }
#ifdef LDP_GR_WANTED
        if ((u4RcvdEvent & LDP_CSR_RESTORE_COMPLELTE) ==
            LDP_CSR_RESTORE_COMPLELTE)
        {
            /* If the node can preserve the forwarding entries, then only Reserve resources */
            if (LDP_GR_CAPABILITY (MPLS_DEF_INCARN) == LDP_GR_CAPABILITY_FULL)
            {
                /* No need to resrve MPLS_TUNNEL interfaces in GR case */
                LdpGrReserveResources (LDP_FALSE);
                LdpGrInitProcess ();
            }
        }
#endif
        MPLS_LDP_UNLOCK ();
    }
}

/*****************************************************************************/
/* Function Name : LdpProcessQueueMsgs                                       */
/* Description   : This function processes Queue Messages                    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
VOID
LdpProcessQueueMsgs (VOID)
{
    tLdpIfMsg          *pLdpMsg = NULL;
    UINT4               u4DeQSate = OSIX_FAILURE;
    UINT4               u4RelinquishCtr = 0;

    if (LDP_QID == NULL)
    {
        return;
    }

    u4DeQSate =
        (OsixQueRecv
         (LDP_QID, (UINT1 *) &pLdpMsg, OSIX_DEF_MSG_LEN, OSIX_NO_WAIT));

    while (u4DeQSate == OSIX_SUCCESS)
    {
        if (pLdpMsg != NULL)
        {
            if (gLdpInfo.u1IncarnUp == LDP_TRUE)
            {
                switch (pLdpMsg->u4MsgType)
                {
                    case LDP_SNMP_EVENT:
                        LdpProcessSnmpEvents (&pLdpMsg->u.SnmpEvt);
                        break;
                    case LDP_UDP_EVENT:
                        LdpProcessUdpEvents (&pLdpMsg->u.UdpEvt);
                        break;
                    case LDP_TCP_EVENT:
                        LdpProcessTcpEvents (&pLdpMsg->u.TcpEvt);
                        break;
                    case LDP_IP_EVENT:
                        LdpProcessIpEvents (&pLdpMsg->u.IpEvt);
                        break;
                    case LDP_INTERNAL_EVENT:
                        LdpProcessLdpInternalEvents (&pLdpMsg->u.IntEvt);
                        break;
                    case LDP_APP_EVENT:
                        LdpProcessAppEvents (&pLdpMsg->u.AppEvt);
                        break;
                    case LDP_TE_EVENT:
                        LdpProcessTeEvent (&pLdpMsg->u.TeEvt);
                        break;
                    case LDP_OVER_RSVP_EVENT:
                        LdpProcessLdpOverRsvpEvt (&pLdpMsg->u.LdpOverRsvpEvt);
                        break;
#ifdef MPLS_IPV6_WANTED
                    case LDP_UDP6_EVENT:
                        LdpProcessIpv6UdpEvents (&pLdpMsg->u.UdpEvt);
                        break;
                    case LDP_IPV6_EVENT:
                        LdpProcessIpv6Events (&pLdpMsg->u.IpEvt);
                        break;
#endif
#ifdef MPLS_LDP_BFD_WANTED
                    case LDP_BFD_EVENT:
                        LdpProcessBfdEvent (&pLdpMsg->u.BfdEvt);
                        break;
#endif
#ifdef LDP_HA_WANTED
                    case LDP_RM_EVENT:
                        LdpProcessRmEvent (&pLdpMsg->u.LdpRmEvt);
                        break;
#endif
#ifdef LDP_GR_WANTED
                    case LDP_GR_LBL_REL_EVENT:
                        LdpGrProcessLblRelEvent (&pLdpMsg->u.LdpGrLblRelEvt);
                        break;
#endif
                }
            }
            /* 
             * Checking if processing of any EnQ'd Msg, resulted in deletion of 
             * Incarnations.
             */
            if (LDP_ACTIVE_INCRN_COUNT == 0)
            {
                LdpUdpClose (LDP_STD_PORT);
                /*MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
                LdpIpv6UdpClose (LDP_STD_PORT);
                LdpIpv6TcpClose (gu4Ipv6TcpStdServConnId);
#endif
                /*MPLS_IPv6 add end */
                LdpTcpClose (gu4TcpStdServConnId);

                /* De-Register with TC */
                LdpDeRegisterWithTC ();

                gLdpInfo.u1IncarnUp = LDP_FALSE;

            }
            MemReleaseMemBlock (LDP_Q_POOL_ID, (UINT1 *) pLdpMsg);
        }

        u4RelinquishCtr++;

        if (u4RelinquishCtr > LDP_RELINQUISH_CNTR)
        {
            LDP_DBG1 (LDP_IF_RX,
                      "LDPTCP: More than %d messages processed\n",
                      LDP_RELINQUISH_CNTR);

            u4RelinquishCtr = 0;

            /*Process the Timer Expiry Events */
            LdpProcessTimerEvents ();

            /*Process the Udp Messages */
            LdpProcessUdpMsgs ();

            /*Process Udp6 Message */
#ifdef MPLS_IPV6_WANTED
            LdpProcessIpv6UdpMsgs ();
#endif
        }

        u4DeQSate = (OsixQueRecv (LDP_QID, (UINT1 *) &pLdpMsg,
                                  OSIX_DEF_MSG_LEN, OSIX_NO_WAIT));
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpInit                                                   */
/* Description   : This routine creates input queue for LDP task and         */
/*                 intializes configurable parameters to default values.     */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpInit (VOID)
{
    /* MPLS_IPv6 start */
    tGenAddr            DestAddr;
    /* MPLS_IPv6 end */
    UINT2               u2Incarn = 0;
    UINT4               u4IfaceCount = 0;
    UINT2               u2HoldCount = 0;
    UINT1               u1Count = 0;
    UINT4               u4IpAddr = 0;
    /* MPLS_IPv6 start */
    MEMSET (&DestAddr, 0, sizeof (tGenAddr));
    /* MPLS_IPv6 end */
    gLdpInfo.u1IncarnUp = LDP_FALSE;
    gu4LdpDbg = LDP_DEF_DBG_FLAG;
    gLdpInfo.u1Policy = LDP_POLICY_DISABLED;

    LDP_ACTIVE_INCRN_COUNT = 0;

    /* Create Timer list */
    if (TmrCreateTimerList ((const UINT1 *) LDP_TSK_NAME, LDP_TMR_EXPR_EVENT,
                            NULL, &LDP_TIMER_LIST_ID) != TMR_SUCCESS)
    {
        LDP_DBG (LDP_MAIN_MEM, "MAIN: Failed to Create Timer List\n");
        return LDP_FAILURE;
    }

    if (FsLDPSizingParams[MAX_LDP_FEC_TABLE_SIZING_ID].u4PreAllocatedUnits
        != MAX_LDP_FEC_TABLE)
    {
        FsLDPSizingParams[MAX_LDP_FEC_TABLE_SIZING_ID].u4StructSize
            = (sizeof (tFecTableEntry) *
               FsLDPSizingParams[MAX_LDP_FEC_TABLE_SIZING_ID].
               u4PreAllocatedUnits);
        FsLDPSizingParams[MAX_LDP_FEC_TABLE_SIZING_ID].u4PreAllocatedUnits
            = MAX_LDP_FEC_TABLE;
    }

    if (FsLDPSizingParams[MAX_LDP_TCP_UDP_SOCK_INFO_SIZING_ID].
        u4PreAllocatedUnits != MAX_LDP_TCP_UDP_SOCK_INFO)
    {
        FsLDPSizingParams[MAX_LDP_TCP_UDP_SOCK_INFO_SIZING_ID].u4StructSize
            = (sizeof (tLdpTcpUdpSockInfo) *
               FsLDPSizingParams[MAX_LDP_TCP_UDP_SOCK_INFO_SIZING_ID].
               u4PreAllocatedUnits);

        FsLDPSizingParams[MAX_LDP_TCP_UDP_SOCK_INFO_SIZING_ID].
            u4PreAllocatedUnits = MAX_LDP_TCP_UDP_SOCK_INFO;
    }

    if (FsLDPSizingParams[MAX_LDP_ATM_LBL_KEY_INFO_SIZING_ID].
        u4PreAllocatedUnits != MAX_LDP_ATM_LBL_KEY_INFO)
    {
        FsLDPSizingParams[MAX_LDP_ATM_LBL_KEY_INFO_SIZING_ID].u4StructSize
            = (sizeof (tKeyInfoStruct) *
               FsLDPSizingParams[MAX_LDP_ATM_LBL_KEY_INFO_SIZING_ID].
               u4PreAllocatedUnits);
        FsLDPSizingParams[MAX_LDP_ATM_LBL_KEY_INFO_SIZING_ID].
            u4PreAllocatedUnits = MAX_LDP_ATM_LBL_KEY_INFO;
    }

    /* Create memory pools for LDP module. */
    if (LdpSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MEM, "MAIN: Failed to Create MemPool for LDP \n");
        TmrDeleteTimerList (LDP_TIMER_LIST_ID);
        return LDP_FAILURE;
    }

    /* Initialise TCP Sock Information. */
    if (LdpTcpInitSockInfo () == LDP_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MEM,
                 "MAIN: Failed to Initialise TCP sock information \n");
        LdpSizingMemDeleteMemPools ();
        TmrDeleteTimerList (LDP_TIMER_LIST_ID);
        return LDP_FAILURE;
    }

    for (u2Incarn = 0; u2Incarn < LDP_MAX_INCARN; u2Incarn++)
    {
        LDP_MAX_ENTITIES (u2Incarn)
            = FsLDPSizingParams[MAX_LDP_ENTITIES_SIZING_ID].u4PreAllocatedUnits;
        LDP_MAX_LOCAL_PEERS (u2Incarn)
            =
            (UINT2) ((FsLDPSizingParams[MAX_LDP_PEERS_SIZING_ID].
                      u4PreAllocatedUnits * 2) / 3);
        LDP_MAX_REMOTE_PEERS (u2Incarn) =
            (UINT2) (FsLDPSizingParams[MAX_LDP_PEERS_SIZING_ID].
                     u4PreAllocatedUnits / 3);
        LDP_MAX_SESSIONS (u2Incarn) =
            (UINT2) FsLDPSizingParams[MAX_LDP_SESSIONS_SIZING_ID].
            u4PreAllocatedUnits;
        LDP_MAX_INTERFACES (u2Incarn) =
            (UINT2) FsLDPSizingParams[MAX_LDP_IFACES_SIZING_ID].
            u4PreAllocatedUnits;
        LDP_MAX_LSPS (u2Incarn) =
            (UINT2) FsLDPSizingParams[MAX_LDP_LSP_UPSTR_LCB_SIZING_ID].
            u4PreAllocatedUnits;
        LDP_MAX_CRLSP_TNLS (u2Incarn) =
            (UINT2) FsLDPSizingParams[MAX_LDP_CRLSP_TNL_INFO_SIZING_ID].
            u4PreAllocatedUnits;
        LDP_MAX_ADJS (u2Incarn) =
            (UINT2) FsLDPSizingParams[MAX_LDP_ADJS_SIZING_ID].
            u4PreAllocatedUnits;
        LDP_MAX_TEMP_TE_HOP_INFO (u2Incarn) =
            (UINT2) FsLDPSizingParams[MAX_LDP_TEMP_TE_HOP_INFO_SIZING_ID].
            u4PreAllocatedUnits;

        LDP_INIT_INCARN (u2Incarn);
#ifdef MPLS_IPV6_WANTED
        LDP_PEER_IPV6_IFADR_TABLE (u2Incarn) = NULL;
        LDP_LAST_V4IF_DOWN (u2Incarn) = LDP_FALSE;
#endif

        /* If LDP_GR_PROGRESS_STATUS is Zero LDP starting for first time.
         * Else it is GR Scenario, so preserve LDP_GR_PROGRESS_STATUS */
        if (0 == LDP_GR_PROGRESS_STATUS (u2Incarn))
        {
            LDP_GR_PROGRESS_STATUS (u2Incarn) = LDP_GR_NOT_STARTED;
        }
        LDP_INIT_INCARN (u2Incarn);
        /* Hash Table creation for storing Peer's Interface Addresses
         * received in Address Msg */

        LDP_PEER_IFADR_TABLE (u2Incarn) =
            TMO_HASH_Create_Table (LDP_HASH_SIZE, NULL, 0);

        if (LDP_PEER_IFADR_TABLE (u2Incarn) == NULL)
        {
            LDP_DBG (LDP_MAIN_MEM,
                     "MAIN: Failed to Create PeerIfAddr Hash Table\n");
            LdpDeAllocateIncarnTables (u2Incarn);
            return LDP_FAILURE;
        }
#ifdef MPLS_IPV6_WANTED
        LDP_PEER_IPV6_IFADR_TABLE (u2Incarn) =
            TMO_HASH_Create_Table (LDP_HASH_SIZE, NULL, 0);

        if (LDP_PEER_IPV6_IFADR_TABLE (u2Incarn) == NULL)
        {
            LDP_DBG (LDP_MAIN_MEM,
                     "MAIN: Failed to Create PeerIfAddr Hash Table\n");
            LdpDeAllocateIncarnTables (u2Incarn);
            return LDP_FAILURE;
        }
#endif
        /* Hash table creation for storing the LDP supported CRLSP tunnels */
        LDP_CRLSP_TNL_TABLE (u2Incarn) =
            TMO_HASH_Create_Table (LDP_HASH_SIZE, NULL, 0);
        if (LDP_CRLSP_TNL_TABLE (u2Incarn) == NULL)
        {
            LDP_DBG (LDP_MAIN_MEM,
                     "MAIN: Failed to Create CrLspTnl HashTable\n");
            LdpDeAllocateIncarnTables (u2Incarn);
            return LDP_FAILURE;
        }

        /* Hash table creation for storing the TCP connection Id and the
         * associated session in the LDP. */
        LDP_TCPCONN_HSH_TABLE (u2Incarn) =
            TMO_HASH_Create_Table (LDP_HASH_SIZE, NULL, 0);
        if (LDP_TCPCONN_HSH_TABLE (u2Incarn) == NULL)
        {
            LDP_DBG (LDP_MAIN_MEM,
                     "MAIN: Failed to Create TcpConnId HashTable\n");
            LdpDeAllocateIncarnTables (u2Incarn);
            return LDP_FAILURE;
        }

        for (u1Count = 0; u1Count < MAX_LDP_IP_RT_ENTRIES; u1Count++)
        {
            MEMSET ((UINT1 *)
                    &LDP_FECROUTE_ARRAY (u2Incarn, u1Count), 0,
                    sizeof (tRouteFec));
        }
    }

    /* We create the default incarn here and Make Admin UP for that */
    if (LdpCreateIncarn (MPLS_DEF_INCARN) != LDP_SUCCESS)
    {
        /* If Incarn tables allocation are successful, we will get
         * u2Incarn as LDP_MAX_INCARN. 
         * Since initialisation of default incarnation has failed,
         * delete incarn tables of all incarnations. */
        LdpDeAllocateIncarnTables ((UINT2) (u2Incarn - 1));
        return LDP_FAILURE;
    }

    /* Open Udp port */
    if (LdpUdpOpen (LDP_STD_PORT) != LDP_SUCCESS)
    {
        /* Deleting the Incarn that is made active previously, and
         * waiting for next Incarn Up Event. */
        LdpDeleteIncarn (MPLS_DEF_INCARN, LDP_INCARN_DOWN);

        /* If Incarn tables allocation are successful, we will get
         * u2Incarn as LDP_MAX_INCARN. 
         * Since initialisation of default incarnation has failed,
         * delete incarn tables of all incarnations. */
        LdpDeAllocateIncarnTables ((UINT2) (u2Incarn - 1));
        return LDP_FAILURE;
    }

    /* MPLS_IPv6 start */
    /* Open Udp port for IPV6 */
#ifdef MPLS_IPV6_WANTED
    if (LdpIpv6UdpOpen (LDP_STD_PORT) != LDP_SUCCESS)
    {
        /* Closing the Standard IPV4 Udp Port for LDP */
        LdpUdpClose (LDP_STD_PORT);

        /* Deleting the Incarn that is made active previously, and
         * waiting for next Incarn Up Event. */
        LdpDeleteIncarn (MPLS_DEF_INCARN, LDP_INCARN_DOWN);
        /* If Incarn tables allocation are successful, we will get
         * u2Incarn as LDP_MAX_INCARN.
         * Since initialisation of default incarnation has failed,
         * delete incarn tables of all incarnations. */
        LdpDeAllocateIncarnTables ((UINT2) (u2Incarn - 1));
        return LDP_FAILURE;
    }
#endif

    /*Open IPV4 TCP PORT */
    DestAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
    *DestAddr.Addr.au1Ipv4Addr = LDP_INIT_ADDR;
    if (LdpTcpOpen (DestAddr, LDP_STD_PORT, LDP_PASSIVE_OPEN,
                    &gu4TcpStdServConnId, LDP_CUR_INCARN, NULL) == LDP_FAILURE)
    {
        /* MPLS_IPv6 mod end */
        /* Closing the Standard Udp Port for LDP */
        LdpUdpClose (LDP_STD_PORT);
        /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
        LdpIpv6UdpClose (LDP_STD_PORT);
#endif
        /* MPLS_IPv6 add end */
        /* Deleting the Incarn that is made active previously, and
         * waiting for next Incarn Up Event. */

        LdpDeleteIncarn (MPLS_DEF_INCARN, LDP_INCARN_DOWN);

        /* If Incarn tables allocation are successful, we will get
         * u2Incarn as LDP_MAX_INCARN. 
         * Since initialisation of default incarnation has failed,
         * delete incarn tables of all incarnations. */
        LdpDeAllocateIncarnTables ((UINT2) (u2Incarn - 1));
        return LDP_FAILURE;
    }

    /* MPLS_IPv6 start TCP OPEN for IPv6. To receive data send from IPv6 interfaces */
#ifdef MPLS_IPV6_WANTED
    /*Open IPV6 TCP PORT */
    DestAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
    *DestAddr.Addr.Ip6Addr.u1_addr = LDP_INIT_ADDR;
    if (LdpTcpOpen (DestAddr, LDP_STD_PORT, LDP_PASSIVE_OPEN,
                    &gu4Ipv6TcpStdServConnId, LDP_CUR_INCARN,
                    NULL) == LDP_FAILURE)
    {
        /* Closing the Standard Udp Port for LDP */
        LdpUdpClose (LDP_STD_PORT);
        /* MPLS_IPv6 add start */
        LdpIpv6UdpClose (LDP_STD_PORT);
        /* MPLS_IPv6 add end */
        /* Deleting the Incarn that is made active previously, and
         * waiting for next Incarn Up Event. */

        LdpDeleteIncarn (MPLS_DEF_INCARN, LDP_INCARN_DOWN);

        /* If Incarn tables allocation are successful, we will get
         * u2Incarn as LDP_MAX_INCARN. 
         * Since initialisation of default incarnation has failed,
         * delete incarn tables of all incarnations. */
        LdpDeAllocateIncarnTables ((UINT2) (u2Incarn - 1));
        return LDP_FAILURE;
    }
#endif
    /* MPLS_IPv6 end */

    /* Currently Registering with TC is done to handle reservation
     * for CRLDP Tunnels. */
    if (LdpRegisterWithTC () == LDP_FAILURE)
    {
        /* Closing the Standard Udp Port for LDP */
        LdpUdpClose (LDP_STD_PORT);
        /*MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
        LdpIpv6UdpClose (LDP_STD_PORT);
        LdpIpv6TcpClose (gu4Ipv6TcpStdServConnId);
#endif
        /*MPLS_IPv6 add end */

        LdpTcpClose (gu4TcpStdServConnId);

        /* Deleting the Incarn that is made active 
         * previously, and waiting for next Incarn 
         * Up Event. */
        LdpDeleteIncarn (MPLS_DEF_INCARN, LDP_INCARN_DOWN);

        /* If Incarn tables allocation are successful, we will get
         * u2Incarn as LDP_MAX_INCARN. 
         * Since initialisation of default incarnation has failed,
         * delete incarn tables of all incarnations. */
        LdpDeAllocateIncarnTables ((UINT2) (u2Incarn - 1));
        return LDP_FAILURE;
    }
#ifdef CLI_WANTED
    /* Allocate memory for CSR to support Graceful Restart */
    if (CsrMemAllocation (LDP_MODULE_ID) == CSR_FAILURE)
    {
        LdpDeRegisterWithTC ();
        /* Closing the Standard Udp Port for LDP */
        LdpUdpClose (LDP_STD_PORT);
        /*MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
        LdpIpv6UdpClose (LDP_STD_PORT);
        LdpIpv6TcpClose (gu4Ipv6TcpStdServConnId);
#endif
        /*MPLS_IPv6 add end */

        LdpTcpClose (gu4TcpStdServConnId);

        /* Deleting the Incarn that is made active 
         * previously, and waiting for next Incarn 
         * Up Event. */
        LdpDeleteIncarn (MPLS_DEF_INCARN, LDP_INCARN_DOWN);

        /* If Incarn tables allocation are successful, we will get
         * u2Incarn as LDP_MAX_INCARN. 
         * Since initialisation of default incarnation has failed,
         * delete incarn tables of all incarnations. */
        LdpDeAllocateIncarnTables ((UINT2) (u2Incarn - 1));

        return LDP_FAILURE;
    }
#endif

#ifdef ISS_WANTED
    u4IpAddr = IssGetIpAddrFromNvRam ();
    u4IpAddr = OSIX_HTONL (u4IpAddr);
#else
    NetIpv4GetHighestIpAddr (&u4IpAddr);
#endif
    MEMCPY ((LDP_LSR_ID (MPLS_DEF_INCARN).au1Ipv4Addr), (UINT1 *) &u4IpAddr,
            LDP_LSR_ID_LEN);

    gLdpInfo.u1IncarnUp = LDP_TRUE;
    L2vpUpdateLdpUpStatus ();
    LDP_INITIALISED = TRUE;
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpCreateIncarn                                           */
/* Description   : This routine creates an incarnation                       */
/* Input(s)      : u2Incarn                                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpCreateIncarn (UINT2 u2IncarnId)
{
    /* Initialising the passive peer array */
    MEMSET ((VOID *) gLdpInfo.LdpIncarn[u2IncarnId].apPeerEntry, LDP_ZERO,
            (LDP_MAX_PASSIVE_PEERS * sizeof (tLdpPeer *)));

    LDP_STATICFECPTR (u2IncarnId) =
        (tFecTableEntry *) MemAllocMemBlk (LDP_STATICFECPTR_POOL_ID);

    if (LDP_STATICFECPTR (u2IncarnId) == NULL)
    {
        LDP_INCARN_ADMIN_STATUS (u2IncarnId) = LDP_ADMIN_DOWN;

        LDP_DBG (LDP_MAIN_MEM,
                 "MAIN: Memory Allocation Failed for Static Fec Table\r\n");
        return LDP_FAILURE;
    }

    /* Initialising the FEC Entries */
    MEMSET ((VOID *) LDP_STATICFECPTR (u2IncarnId), LDP_ZERO,
            (FsLDPSizingParams[MAX_LDP_FEC_TABLE_SIZING_ID].u4StructSize *
             FsLDPSizingParams[MAX_LDP_FEC_TABLE_SIZING_ID].
             u4PreAllocatedUnits));

    LDP_INCARN_STATUS (u2IncarnId) = ACTIVE;
    LDP_INCARN_ADMIN_STATUS (u2IncarnId) = LDP_ADMIN_UP;
    LDP_ACTIVE_INCRN_COUNT++;
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDeleteIncarn                                           */
/* Description   : This routine deletes an incarnation                       */
/* Input(s)      : u2IncarnId                                                */
/*               : u4Status    (NOT_IN_SERVICE/DESTROY)                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpDeleteIncarn (UINT2 u2IncarnId, UINT4 u4Status)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT4               u4IfaceCount;
    UINT2               u2HoldCount;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;

    TMO_DYN_SLL_Scan (&LDP_ENTITY_LIST (u2IncarnId), pSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        pLdpEntity = (tLdpEntity *) pSllNode;

        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_FALSE)
        {
            LdpDeleteLdpEntity (pLdpEntity);
        }
    }

    TMO_DYN_SLL_Scan (&LDP_ENTITY_LIST (u2IncarnId), pSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        pLdpEntity = (tLdpEntity *) pSllNode;

        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
        {
            LdpDeleteLdpEntity (pLdpEntity);
        }
    }

    /* Delete the HAsh Table that's storing the Peer's Iface addresses */
    LdpDelHashTable (LDP_PEER_IFADR_TABLE (u2IncarnId), u2IncarnId,
                     LDP_PIF_POOL_ID);

    /* Delete the Hash Table storing the Tunnel information */
    LdpDelHashTable (LDP_CRLSP_TNL_TABLE (u2IncarnId), u2IncarnId,
                     LDP_CRLSPTNL_POOL_ID);
#ifdef MPLS_IPV6_WANTED
    LdpDelHashTable (LDP_PEER_IPV6_IFADR_TABLE (u2IncarnId), u2IncarnId,
                     LDP_PIF_POOL_ID);
#endif

    /* Delete the Hash Table storing the Tcp connection information */
    LdpDelHashTable (LDP_TCPCONN_HSH_TABLE (u2IncarnId), u2IncarnId,
                     LDP_SSN_POOL_ID);

    /* Delete the Static Fec table Information  */
    if (LDP_STATICFECPTR (u2IncarnId) != NULL)
    {
        MemReleaseMemBlock (LDP_STATICFECPTR_POOL_ID,
                            (UINT1 *) LDP_STATICFECPTR (u2IncarnId));
        LDP_STATICFECPTR (u2IncarnId) = NULL;
    }

    if (u4Status == LDP_INCARN_DOWN)
    {
        LDP_INIT_INCARN (u2IncarnId);
#ifdef MPLS_IPV6_WANTED
        LDP_PEER_IPV6_IFADR_TABLE (u2IncarnId) = NULL;
#endif
    }
    else if (u4Status == LDP_INCARN_NOTINSERVICE)
    {
        LDP_INCARN_STATUS (u2IncarnId) = NOT_IN_SERVICE;
        LDP_INCARN_ADMIN_STATUS (u2IncarnId) = LDP_ADMIN_DOWN;
        for (u4IfaceCount = 0; u4IfaceCount < MAX_LDP_IFACES; u4IfaceCount++)
        {
            for (u2HoldCount = 0; u2HoldCount < HOLD_PRIO_RANGE; u2HoldCount++)
            {
                TMO_SLL_Init (CRLSP_TNL_HOLD_PRIO_LIST (u2IncarnId,
                                                        u4IfaceCount,
                                                        u2HoldCount));
            }
            LDP_SET_NO_OF_ENT_MCASTCOUNT (u2IncarnId, u4IfaceCount) = 0;
#ifdef MPLS_IPV6_WANTED
            LDP_SET_NO_OF_ENT_V6MCASTCOUNT (u2IncarnId, u4IfaceCount) = 0;
#endif
        }
        TmrStopTimer (LDP_TIMER_LIST_ID,
                      (tTmrAppTimer *) & (gLdpInfo.LdpIncarn[(u2IncarnId)].
                                          FecRouteTimer.AppTimer));
    }

    LDP_ACTIVE_INCRN_COUNT--;
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDelHashTable                                           */
/* Description   : This routine deletes the has table Entries in the         */
/*               : respective pool Ids                                       */
/* Input(s)      : pTable from which entries should be deleted               */
/*                 u2IncarnId  Incarnation                                   */
/*               : PoolId      The Pool Id                                   */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
VOID
LdpDelHashTable (tTMO_HASH_TABLE * pTable, UINT2 u2IncarnId, tMemPoolId PoolId)
{
    UINT4               u4HashIndex = 0;
    tTMO_HASH_NODE     *pNode = NULL;

    LDP_SUPPRESS_WARNING (u2IncarnId);
    TMO_HASH_Scan_Table (pTable, u4HashIndex)
    {
        pNode = TMO_HASH_FIRST_NODE (&pTable->HashList[u4HashIndex]);
        while (pNode != NULL)
        {
            /* Deleting node from the bucket */
            TMO_HASH_Delete_Node (pTable, pNode, u4HashIndex);
            MemReleaseMemBlock (PoolId, (UINT1 *) pNode);
            pNode = TMO_HASH_FIRST_NODE (&pTable->HashList[u4HashIndex]);
        }
        TMO_SLL_Init (&pTable->HashList[u4HashIndex]);
    }
}

/*****************************************************************************/
/* Function Name : LdpDelMemPools                                            */
/* Description   : This routine deletes memory pools                         */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDelMemPools ()
{
    LdpSizingMemDeleteMemPools ();
}

/*****************************************************************************/
/* Function Name : LdpProcessTimerEvents                                     */
/* Description   : This routine processes Timer Events                       */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpProcessTimerEvents ()
{
    tLdpSession        *pSessionEntry = NULL;
    tLspTrigCtrlBlock  *pNHCtrlBlk = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLdpTimer          *pLdpTimer = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpAdjacency      *pAdjEntry = NULL;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    UINT4               u4IncarnNo = 0;
    UINT2               u2Incarn = 0;
    UINT1               u1Event;

    if (LDP_TIMER_LIST_ID == NULL)
    {
        return;
    }
    pLdpTimer = (tLdpTimer *) TmrGetNextExpiredTimer (LDP_TIMER_LIST_ID);
    while (pLdpTimer != NULL)
    {
        switch (pLdpTimer->u4Event)
        {
            case LDP_HELLO_ADJ_EXPRD_EVENT:
                LDP_DBG (LDP_DBG_TIMER,
                         "MAIN: Timeout occurence for Hello Adj\n");
                pAdjEntry = (tLdpAdjacency *) (VOID *) pLdpTimer->pu1EventInfo;
                pSessionEntry = pAdjEntry->pSession;
                u2Incarn = SSN_GET_INCRN_ID (pSessionEntry);
                if ((gLdpInfo.LdpIncarn[u2Incarn].u1GrCapability !=
                     LDP_GR_CAPABILITY_NONE) &&
                    (pSessionEntry->pLdpPeer->u2GrReconnectTime != LDP_ZERO))
                {
                    pSessionEntry->pLdpPeer->u1GrProgressState =
                        LDP_PEER_GR_RECONNECT_IN_PROGRESS;
                }
                LdpHandleAdjExpiry (pAdjEntry, LDP_STAT_TYPE_HOLD_TMR_EXPRD);
                break;

            case LDP_HELLO_TMR_EXPRD_EVENT:
                LDP_DBG (LDP_DBG_TIMER,
                         "MAIN: Timeout occurence for Hello Expiry\n");
                LdpSendHelloMsg ((tLdpEntity *) (VOID *) pLdpTimer->
                                 pu1EventInfo);
                break;
            case LDP_SSN_EXPRD_EVENT:
                LDP_DBG (LDP_DBG_TIMER,
                         "MAIN: Timeout occurence for Session Expiry\n");
                if (SSN_STATE ((tLdpSession *) (VOID *) pLdpTimer->pu1EventInfo)
                    != LDP_SSM_ST_NON_EXISTENT)
                {
                    pLdpSession =
                        (tLdpSession *) (VOID *) pLdpTimer->pu1EventInfo;
                    u2Incarn = SSN_GET_INCRN_ID (pLdpSession);

                    /* Added for GR Purpose.
                     * R1-------R2. R1 goes down. and R2 detects that its Adjacency got
                     * expired and R1 already intimated that its GR FULL router (ie. 
                     * Reconnect Time conveyed by R1 is nonzero)
                     * Do the following.
                     */
#ifdef LDP_GR_WANTED
                    if ((gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrCapability !=
                         LDP_GR_CAPABILITY_NONE) &&
                        (pLdpSession->pLdpPeer->u2GrReconnectTime != LDP_ZERO))
                    {
                        pLdpSession->pLdpPeer->u1GrProgressState =
                            LDP_PEER_GR_RECONNECT_IN_PROGRESS;
                    }
                    LdpDeleteLdpSession (pLdpSession,
                                         LDP_STAT_TYPE_KEEPALIVE_EXPRD);
#else
                    LdpDeleteLdpSession (pLdpSession,
                                         LDP_STAT_TYPE_KEEPALIVE_EXPRD);
                    MEMSET (pLdpSession->pLdpPeer->PeerLdpId, LDP_ZERO,
                            LDP_MAX_LDPID_LEN);
#endif
                }
                break;

            case LDP_KALIVE_TMR_EVENT:
                LDP_DBG (LDP_DBG_TIMER,
                         "MAIN: Timeout occurence for Keep Alive Expiry\n");
                if (SSN_STATE ((tLdpSession *) (VOID *) pLdpTimer->pu1EventInfo)
                    != LDP_SSM_ST_NON_EXISTENT)
                {
                    LdpSendKeepAliveMsg ((tLdpSession *) (VOID *) pLdpTimer->
                                         pu1EventInfo);
                }
                break;
            case LDP_SSN_TX_INIT_MSG_EVENT:
                LDP_DBG (LDP_DBG_TIMER,
                         "MAIN: Timeout occurence for InitMsg ReTx\n");
                pSessionEntry =
                    (tLdpSession *) (VOID *) pLdpTimer->pu1EventInfo;
                /* 
                 * Setting the SessionState to INITIALISED and Invoking FSM 
                 * routine to ReTx Init Msg.
                 */
                SSN_STATE (pSessionEntry) = LDP_SSM_ST_INITIALIZED;
                MplsLdpSessionUpdateSysTime (pSessionEntry);
                LDP_SESSION_FSM[SSN_STATE (pSessionEntry)] (pSessionEntry,
                                                            LDP_SSM_EVT_INT_SEND_INIT,
                                                            NULL);
                break;
            case LDP_SSN_RETRY_EVENT:
                LDP_DBG (LDP_DBG_TIMER,
                         "MAIN: Timeout occurence for Session Retry\n");
                pSessionEntry =
                    (tLdpSession *) (VOID *) pLdpTimer->pu1EventInfo;
                if (SSN_STATE (pSessionEntry) != LDP_SSM_ST_NON_EXISTENT)
                {
                    LDP_SESSION_FSM[SSN_STATE (pSessionEntry)]
                        (pSessionEntry, LDP_SSM_EVT_INT_INIT_CON, NULL);
                }
                break;
            case NHOP_RETRY_TMR_EXPIRED_EVENT:
                LDP_DBG (LDP_DBG_TIMER,
                         "MAIN: Timeout occurence for NHOP Retry Expiry\n");
                pNHCtrlBlk =
                    (tLspTrigCtrlBlock *) (VOID *) pLdpTimer->pu1EventInfo;
#if 0
                /**Here the senario is handled for Merged, Currently its not supported 
                   And we observer crash in this part of code for NON-Merged, Hence It is required to handle
                   at the time of merged type support**/

                if ((LCB_DSSN (pNHCtrlBlk->pOrgLspCtrlBlk)
                     && (SSN_MRGTYPE (LCB_DSSN (pNHCtrlBlk->pOrgLspCtrlBlk)) ==
                         LDP_VP_MRG)) || (LCB_DSSN (pNHCtrlBlk->pOrgLspCtrlBlk)
                                          &&
                                          (SSN_MRGTYPE
                                           (LCB_DSSN
                                            (pNHCtrlBlk->pOrgLspCtrlBlk)) ==
                                           LDP_VC_MRG)))
                {
                    LdpVcMergeNhopSm (pNHCtrlBlk,
                                      LDP_NH_LSM_EVT_INT_RETRY_TMOUT, NULL);
                }
                else
                {
#endif
                    gLdpNonMrgNhopChngFsm[NH_TRGCB_ST (pNHCtrlBlk)]
                        [LDP_NH_LSM_EVT_INT_RETRY_TMOUT] (pNHCtrlBlk, NULL);
#if 0
                }
#endif
                break;

            case LDP_PERST_EXPIRED_EVENT:
                LDP_DBG (LDP_DBG_TIMER, "MAIN: Retry for the LSP formation\n");
                pLspCtrlBlock =
                    (tLspCtrlBlock *) (VOID *) pLdpTimer->pu1EventInfo;
                pLspCtrlBlock->u1LblReqFwdFlag = LDP_TRUE;
                u1Event = LDP_LSM_EVT_INT_SETUP;
                if ((SSN_ADVTYPE (LCB_DSSN (pLspCtrlBlock)) ==
                     LDP_DSTR_ON_DEMAND)
                    && (SSN_MRGTYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_NO_MRG)
                    && (LCB_TRIG (pLspCtrlBlock) != NULL))
                {
                    LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlock)][u1Event]
                        (pLspCtrlBlock, NULL);
                }
                else if (SSN_ADVTYPE (LCB_DSSN (pLspCtrlBlock)) ==
                         LDP_DSTR_UNSOLICIT)
                {
                    LdpSendLblReqMsg (pLspCtrlBlock, pLspCtrlBlock->u1HopCount,
                                      (pLspCtrlBlock->pu1PVTlv +
                                       LDP_TLV_HDR_LEN),
                                      pLspCtrlBlock->u1PVCount, NULL, LDP_ZERO);
                }
                break;

            case LDP_TARDY_PEER_EXPIRED_EVENT:
                /* This Event occurs when a fault peer is detected and the timer
                 * associated with the peer has expired.This timer value is 
                 * assumed to be large. */
                LDP_DBG (LDP_DBG_TIMER, "MAIN: Tardy peer timer expired\n");
                pLspCtrlBlock =
                    (tLspCtrlBlock *) (VOID *) pLdpTimer->pu1EventInfo;
                LdpSendLblAbortReqMsg (pLspCtrlBlock);
                /*LdpDeleteLspCtrlBlock (pLspCtrlBlock); */
                LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
                break;

            case LDP_FEC_REROUTE_EXPIRED_EVT:
                /* This Event occurs when an reroute happens for some
                 * Fecs of the Lsp's established
                 */
                LDP_DBG (LDP_DBG_TIMER,
                         "MAIN: Timeout occurence for FEC ReRoute Expiry\n");
                u4IncarnNo = 0;
                LdpGenerateRtChgEvents ((UINT2) u4IncarnNo);
                break;

            case LDP_GR_RECOVERY_TMR_EXP_EVT:
                LDP_DBG (GRACEFUL_DEBUG,
                         "MAIN: MPLS LDP Recovery Timer Expired.\n");
                /* This event occus when Recovery Timer is expired.
                 * This Event is applicable for the node that is currently
                 * acting as helper. 
                 */
                /* Upon this Event, Delete all the Stale Entries in the
                 * Hardware / Database.
                 * Mark the GR Progress State as completed.
                 */

                pLdpPeer = (tLdpPeer *) (VOID *) pLdpTimer->pu1EventInfo;

#ifdef LDP_GR_WANTED
                LdpGrDeleteStaleCtrlBlockEntries (pLdpPeer);
                gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus =
                    LDP_GR_COMPLETED;

#endif
                LdpSendDeleteStaleNotification
                    (pLdpPeer, L2VPN_LDP_PWVC_GR_DELETE_STALE_ENTRIES);
                pLdpPeer->u1GrProgressState = LDP_PEER_GR_COMPLETED;
                break;

            case LDP_GR_RECONNECT_TMR_EXP_EVT:
                LDP_DBG (GRACEFUL_DEBUG,
                         "MAIN: MPLS LDP Reconnect Timer Expired.\n");
                /* This Event occurs when Reconnect Timer is expired.
                 * This Event is applicable for the node that currently
                 * acting as helper.
                 */
                /* Upon this Event, Delete all the Stale Entries in the 
                 * Hardware / Database
                 */

                pLdpPeer = (tLdpPeer *) (VOID *) pLdpTimer->pu1EventInfo;

#ifdef LDP_GR_WANTED
                LdpGrDeleteStaleCtrlBlockEntries (pLdpPeer);
#endif
                LdpSendDeleteStaleNotification
                    (pLdpPeer, L2VPN_LDP_PWVC_GR_DELETE_STALE_ENTRIES);
                pLdpPeer->u1GrProgressState = LDP_PEER_GR_ABORTED;
#ifdef LDP_GR_WANTED
                if (LDP_ENTITY_IS_TARGET_TYPE (pLdpPeer->pLdpEntity) ==
                    LDP_FALSE)
                {
                    LdpDeleteLdpPeer (pLdpPeer);
                }
                else
                {
                    /* We cannot delete the LDP Peer in case of Targetted session
                     * since the peer information is part of the user configuartion
                     * which must be retained. */
                    LdpDeleteLdpSession (pLdpPeer->pLdpSession,
                                         LDP_STAT_TYPE_SHUT_DOWN);
                    MEMSET (pLdpPeer->PeerLdpId, LDP_ZERO, LDP_MAX_LDPID_LEN);
                }
#endif
                pLdpPeer->u1GrProgressState = LDP_PEER_GR_ABORTED;
                MEMSET (pLdpPeer->PeerLdpId, LDP_ZERO, LDP_MAX_LDPID_LEN);
                break;

            case LDP_GR_FWD_HOLDING_TMR_EXP_EVT:
                LDP_DBG (GRACEFUL_DEBUG,
                         "MAIN: MPLS LDP Forwarding Hold Timer Expired.\n");
                /* This Event occurs when Forward Hold Timer is expired.
                 * This Event is applicable for the node that is
                 * restarted currently
                 */
                /* Upon this Event, All the Entries in the Hardware / 
                 * Database that are still marked stale is
                 * deleted
                 */
#ifdef LDP_GR_WANTED
                LdpProcessFwdHoldTmrExpiry ();
                gLdpInfo.LdpIncarn[LDP_CUR_INCARN].u1GrProgressStatus =
                    LDP_GR_COMPLETED;
#endif
                LdpSendDeleteStaleNotification
                    (NULL, L2VPN_LDP_PWVC_GR_FWD_HOLD_EXPIRY);
                break;

            case LDP_TCP_SOCK_READ_TMR_EVT:

                LDP_DBG (LDP_IF_TMR, "MAIN: SOCK Read Timer Expired\n");
                pLdpTcpUdpSockInfo =
                    (tLdpTcpUdpSockInfo *) (VOID *) pLdpTimer->pu1EventInfo;
                pLdpTcpUdpSockInfo->u1AddrType = LDP_ADDR_TYPE_IPV4;

                SockAddSelAddReadFd (pLdpTcpUdpSockInfo);

                break;
#ifdef MPLS_IPV6_WANTED
            case LDP_TCP6_SOCK_READ_TMR_EVT:
                LDP_DBG (LDP_IF_TMR, "MAIN: IPV6 SOCK Read Timer Expired\n");
                pLdpTcpUdpSockInfo =
                    (tLdpTcpUdpSockInfo *) (VOID *) pLdpTimer->pu1EventInfo;
                pLdpTcpUdpSockInfo->u1AddrType = LDP_ADDR_TYPE_IPV6;
                SockAddSelAddReadFd (pLdpTcpUdpSockInfo);
                break;
#endif

                /*TCP Reconnect Timer */
            case LDP_TCP_RECONNECT_TMR_EVT:
                LDP_DBG ((LDP_IF_TMR | LDP_SSN_TMR),
                         "MAIN: TCP Reconnect Timer Expired\n");
                pLdpTcpUdpSockInfo =
                    (tLdpTcpUdpSockInfo *) (VOID *) pLdpTimer->pu1EventInfo;
                if (pLdpTcpUdpSockInfo->u1SockStatus == SOCK_CONNECTING)
                {
                    if (LdpTcpEventHandler (LDP_TCP_CONN_DOWN_EVENT,
                                            CONTROL_DATA,
                                            pLdpTcpUdpSockInfo->u4ConnId,
                                            NULL) == LDP_FAILURE)
                    {
                        LDP_DBG ((LDP_IF_MEM | LDP_SSN_MEM),
                                 "LDPTCP: Failed to Indicate TCP Conn up to LDP\n");
                    }
                    /*
                     * NOTE: If LDP_TCP_CONN_DOWN_EVENT indication fails, Socket is 
                     * closed.  LDP might infinitely wait for socket conn. up event.
                     * However this condition will not occur.
                     */
                    LdpTcpUdpSockClose (pLdpTcpUdpSockInfo);
                }
                break;
#ifdef MPLS_IPV6_WANTED
            case LDP_IPV6_SSN_PREF_TMR_EVT:
                LDP_DBG ((LDP_IF_TMR | LDP_SSN_TMR),
                         "LDPPrcTmrEvt: IPV6 Session Preference Timer Expired\n");
                pLdpPeer = (tLdpPeer *) (VOID *) pLdpTimer->pu1EventInfo;
                pLdpPeer->u1SsnPrefTmrStatus = LDP_IPV6_SSN_PREF_TMR_EXPIRED;

                if (pLdpPeer->pLdpSession != NULL)
                {
                    if (LDP_SSN_TCP_NON_INIT ==
                        pLdpPeer->pLdpSession->u1SessionInitStatus)
                    {
                        LDP_DBG (LDP_SSN_PRCS,
                                 "LDPPrcTmrEvt: Ssn Tcp Status Non Init \n");
                        pSessionEntry = pLdpPeer->pLdpSession;
                        LDP_DBG5 (LDP_SSN_PRCS,
                                  "LdpPTmrEvent SOURCE TRANSPORT ADDRES: %x:%x:%x:%x at %d",
                                  pSessionEntry->SrcTransAddr.Addr.
                                  au1Ipv4Addr[0],
                                  pSessionEntry->SrcTransAddr.Addr.
                                  au1Ipv4Addr[1],
                                  pSessionEntry->SrcTransAddr.Addr.
                                  au1Ipv4Addr[2],
                                  pSessionEntry->SrcTransAddr.Addr.
                                  au1Ipv4Addr[3], __LINE__);
                        LDP_DBG5 (LDP_SSN_PRCS,
                                  "LdpHHMsg: (DEST ADD) PEER-TRADDR: %x:%x:%x:%x at %d\n",
                                  pLdpPeer->TransAddr.Addr.au1Ipv4Addr[0],
                                  pLdpPeer->TransAddr.Addr.au1Ipv4Addr[1],
                                  pLdpPeer->TransAddr.Addr.au1Ipv4Addr[2],
                                  pLdpPeer->TransAddr.Addr.au1Ipv4Addr[3],
                                  __LINE__);
                        if (LDP_FAILURE ==
                            LdpNeInitTcpConn (pSessionEntry,
                                              pSessionEntry->u1SessionRole,
                                              LDP_CUR_INCARN))
                        {
                            LDP_DBG (LDP_SSN_PRCS,
                                     "LDPPrcTmrEvt: NEINITTCP CONN FAILED \n");
                            return;
                        }
                        LDP_DBG (LDP_SSN_PRCS,
                                 "LDPPrcTmrEvt: NEINITTCP CONN SUCCESS \n");
                    }
                }
                else
                {
                    LDP_DBG1 (LDP_SSN_PRCS,
                              "LDPPrcTmrEvt: Session Deleted. Peer still present:0x%x \n",
                              pLdpPeer);
                }
                break;
#endif
        }
        pLdpTimer = (tLdpTimer *) TmrGetNextExpiredTimer (LDP_TIMER_LIST_ID);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpProcessSnmpEvents                                      */
/* Description   : This routine processes events from SNMP module            */
/* Input(s)      : aTaskEnqMsgInfo - info about events and related stuff     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpProcessSnmpEvents (tLdpSnmpEvtInfo * pLdpSnmpEvt)
{
    UINT4               u4DestAddr;
    UINT4               u4IpAddress = LDP_ZERO;
    UINT4               u4NextHop;
    UINT2               u2IfIndex;
    UINT4               u4DestMask = 0;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tFecTableEntry     *pFecTableEntry = NULL;

    switch (pLdpSnmpEvt->u4SubEvt)
    {
        case LDP_INCARN_UP:
            return LdpCreateIncarn ((UINT2) pLdpSnmpEvt->u4IncarnId);

        case LDP_INCARN_DOWN:
            return LdpDeleteIncarn ((UINT2) pLdpSnmpEvt->u4IncarnId,
                                    LDP_INCARN_DOWN);

        case LDP_INCARN_NOTINSERVICE:
            return LdpDeleteIncarn ((UINT2) pLdpSnmpEvt->u4IncarnId,
                                    LDP_INCARN_NOTINSERVICE);
        case LDP_ENTITY_UP:
            pLdpEntity = (tLdpEntity *) pLdpSnmpEvt->pParams;

            return LdpUpLdpEntity (pLdpEntity);

        case LDP_ENTITY_DOWN:

            pLdpEntity = (tLdpEntity *) pLdpSnmpEvt->pParams;

            return LdpDeleteLdpEntity (pLdpEntity);

        case LDP_ENTITY_NOTINSERVICE:

            pLdpEntity = (tLdpEntity *) pLdpSnmpEvt->pParams;

            return LdpDownLdpEntity (pLdpEntity);

        case CRLDP_TNL_UP_EVENT:

            if (LdpNonMrgIntCrlspSetReq ((UINT2) pLdpSnmpEvt->u4IncarnId,
                                         (tCrlspTnlInfo *) pLdpSnmpEvt->
                                         pParams) == LDP_FAILURE)
            {
                /* Trap to be generated to the SNMP Manager to indicate the
                 * tunnel Setup Failure.
                 */
                return LDP_FAILURE;
            }
            return LDP_SUCCESS;

        case CRLDP_TNL_DOWN_EVENT:
            /* Deleting the Lsp Control associated to this Tunnel */
            pLspCtrlBlk =
                (tLspCtrlBlock *) (((tCrlspTnlInfo *) (pLdpSnmpEvt->pParams))->
                                   pLspCtrlBlock);
            LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)][LDP_LSM_EVT_INT_DSTR]
                (pLspCtrlBlk, NULL);
            return LDP_SUCCESS;

        case STATIC_LSP_UP_EVENT:

            pFecTableEntry = ((tFecTableEntry *) pLdpSnmpEvt->pParams);
            if ((pFecTableEntry->u2AddrFmly) == IPV4)
            {

                MEMCPY ((UINT1 *) (&u4DestAddr),
                        (UINT1 *) &pFecTableEntry->Ipv4Addr, IPV4_ADDR_LENGTH);
                LDP_GET_MASK_FROM_PRFX_LEN ((INT4) (pFecTableEntry->u1PreLen),
                                            u4DestMask);
                u4DestAddr = OSIX_NTOHL (u4DestAddr);
                if (LDP_GET_ROUTE_FOR_DEST (u4DestAddr, u4DestMask,
                                            LDP_DEF_TOS, &u2IfIndex, &u4NextHop)
                    == LDP_IP_FAILURE)
                {
                    LDP_DBG (LDP_MAIN_MISC,
                             "MAIN: IpRoute Not Available for the "
                             "configured Static Fec\n");
                    /* Deleting the Fec Entry as the Route is not found for
                     * the configured Fec Type.
                     * Here a Trap can be generated
                     */
                    return LDP_FAILURE;
                }

                /* Creating New Static Lsp */
                u4NextHop = OSIX_HTONL (u4NextHop);
                u4DestAddr = OSIX_HTONL (u4DestAddr);
                LdpNonMrgIntLspSetReq ((UINT2) pLdpSnmpEvt->u4IncarnId,
                                       (UINT1 *) &u4DestAddr,
                                       (pFecTableEntry->u1PreLen),
                                       (UINT1 *) &u4NextHop,
                                       (UINT2) u2IfIndex, pFecTableEntry);

                if (pFecTableEntry->pLspCtrlBlock == NULL)
                {
                    /* Once the control block is filled, the control
                     * enters the state machine. Error till this point
                     * is handled here. */
                    return LDP_FAILURE;
                }
                return LDP_SUCCESS;
            }
            else
            {
                LDP_DBG (LDP_MAIN_MISC,
                         "MAIN: Configured Fec Type can not be Supported\n");
                return LDP_FAILURE;
            }
            break;

        case STATIC_LSP_DOWN_EVENT:

            /* Deleting the Lsp Control associated to this STATIC Lsp */
            pFecTableEntry = (tFecTableEntry *) (pLdpSnmpEvt->pParams);
            pLspCtrlBlk = pFecTableEntry->pLspCtrlBlock;
            if (pLspCtrlBlk != NULL)
            {
                LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)][LDP_LSM_EVT_INT_DSTR]
                    (pLspCtrlBlk, NULL);
            }
            MEMSET ((VOID *) pFecTableEntry, LDP_ZERO, sizeof (tFecTableEntry));
            break;

        case STATIC_LSP_NOTINSERV_EVENT:
            pFecTableEntry = (tFecTableEntry *) (pLdpSnmpEvt->pParams);
            pLspCtrlBlk = pFecTableEntry->pLspCtrlBlock;
            if (pLspCtrlBlk != NULL)
            {
                LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)][LDP_LSM_EVT_INT_DSTR]
                    (pLspCtrlBlk, NULL);
            }
            break;
        case LDP_ROUTERID_CHANGE:
            u4IpAddress = (UINT4) (*((UINT4 *) pLdpSnmpEvt->pParams));
            LdpRetriggerEntitySessions (u4IpAddress, LDP_ZERO,
                                        LDP_ZERO, LDP_ZERO);

            break;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessUdpEvents                                       */
/* Description   : This routine processes the events received from UDP       */
/* Input(s)      : aTaskEnqMsgInfo - info about events and data received     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpProcessUdpEvents (tLdpUdpEvtInfo * pUdpEvtInfo)
{
    UINT2               u2IncarnId = 0;

    if (pUdpEvtInfo->u4Evt == LDP_UDP_DATA_EVENT)
    {
        IFINDEX_GET_INCRN_ID (pUdpEvtInfo->u4IfIndex, &u2IncarnId);
        LDP_DBG (LDP_DBG_UDP, "MAIN: Processing UDP Event\n");
        if (LdpHandleUdpPkt
            ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pUdpEvtInfo->pPdu,
             pUdpEvtInfo->u4IfIndex, pUdpEvtInfo->u4PeerAddr,
             u2IncarnId) == LDP_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain ((tCRU_BUF_CHAIN_HEADER *) (VOID *)
                                         pUdpEvtInfo->pPdu, FALSE);
            return LDP_FAILURE;
        }
        CRU_BUF_Release_MsgBufChain ((tCRU_BUF_CHAIN_HEADER *) (VOID *)
                                     pUdpEvtInfo->pPdu, FALSE);
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessTcpEvents                                       */
/* Description   : This routine processes events received from TCP           */
/* Input(s)      : aTaskEnqMsgInfo - info about events and data received     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpProcessTcpEvents (tLdpTcpEvtInfo * pLdpTcpEvt)
{
    tLdpSession        *pSessionEntry = NULL;
    UINT4               u4MinEvent = 0;

    LDP_GET_MIN_EVENT (u4MinEvent, pLdpTcpEvt->u4Evt);

    switch (u4MinEvent)
    {
        case LDP_TCP_DATA_RX_EVENT:
            LDP_DBG (LDP_DBG_TCP, "MAIN: Rcvd Data Event from TCP\n");
            LdpHandleTcpPkt ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pLdpTcpEvt->
                             pPdu, pLdpTcpEvt->u4ConnId, LDP_ZERO,
                             (UINT2) pLdpTcpEvt->u4IncarnNo);
            CRU_BUF_Release_MsgBufChain ((tCRU_BUF_CHAIN_HEADER *) (VOID *)
                                         pLdpTcpEvt->pPdu, FALSE);
            break;
        case LDP_TCP_CONN_UP_EVENT:
            LDP_DBG (LDP_DBG_TCP, "MAIN: Rcvd ConnUp Event from TCP\n");
            pSessionEntry =
                LdpGetSsnFromTcpConnTbl ((UINT2) pLdpTcpEvt->u4IncarnNo,
                                         pLdpTcpEvt->u4ConnId);
            if (pSessionEntry != NULL)
            {
                LDP_SESSION_FSM[SSN_STATE (pSessionEntry)]
                    (pSessionEntry, LDP_SSM_EVT_INT_SEND_INIT, NULL);
            }
            else
            {
                LDP_DBG (LDP_DBG_TCP,
                         "MAIN: Failed to Identify Ssn, to Indicate "
                         "TCP ConnUp Event\n");
            }
            break;
        case LDP_TCP_CONN_DOWN_EVENT:
            LDP_DBG (LDP_DBG_TCP, "MAIN: Rcvd ConnDown Event from TCP\n");
            pSessionEntry =
                LdpGetSsnFromTcpConnTbl ((UINT2) pLdpTcpEvt->u4IncarnNo,
                                         pLdpTcpEvt->u4ConnId);
            if (pSessionEntry != NULL)
            {
#ifdef LDP_GR_WANTED
                /* If the peer has the ability to retain the MPLS
                 * forwarding entries after restart and the node 
                 * is configured as helper, then change the GR progress state
                 * of peer to LDP_PEER_GR_RECONNECT_IN_PROGRESS */
                if ((gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrCapability !=
                     LDP_GR_CAPABILITY_NONE) &&
                    (pSessionEntry->pLdpPeer->u2GrReconnectTime != LDP_ZERO))
                {
                    pSessionEntry->pLdpPeer->u1GrProgressState =
                        LDP_PEER_GR_RECONNECT_IN_PROGRESS;
                }
#endif
                LDP_SESSION_FSM[SSN_STATE (pSessionEntry)]
                    (pSessionEntry, LDP_SSM_EVT_INT_DESTROY, NULL);
            }
            else
            {
                LDP_DBG (LDP_DBG_TCP,
                         "MAIN: Failed to Identify Ssn, to Indicate "
                         "TCPConnDown Event\n");
            }
            break;
        case TCP_CLI_OPEN_FAILURE:
            /* Active Open failed. Deleting the Session.
             * NOTE: ConnId(stored in SessionEntry) is got from SLI, which is 
             * given, even before the actual tcp connection is opened. */
            pSessionEntry =
                LdpGetSsnFromTcpConnTbl ((UINT2) pLdpTcpEvt->u4IncarnNo,
                                         pLdpTcpEvt->u4ConnId);
            if (pSessionEntry != NULL)
            {
                LdpDeleteLdpSession (pSessionEntry, LDP_STAT_TYPE_SHUT_DOWN);
            }
            else
            {
                LDP_DBG (LDP_DBG_TCP,
                         "MAIN: Failed to get Ssn, to Indicate ActiveTcpOpen "
                         "Event\n");
            }
            break;

        case TCP_SRVR_OPEN_FAILURE:
            LDP_DBG (LDP_DBG_TCP, "MAIN: Failed to open LDP Server Port\n");
            /* Closing the Standard Udp Port for LDP */
            LdpUdpClose (LDP_STD_PORT);
            /*MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
            LdpIpv6UdpClose (LDP_STD_PORT);
#endif
            /*MPLS_IPv6 add end */
            /* Deleting the Incarn that is made active previously, and
             * waiting for next Incarn Up Event. */
            LdpDeleteIncarn ((UINT2) pLdpTcpEvt->u4IncarnNo, LDP_INCARN_DOWN);
            break;

        default:
            LDP_DBG (LDP_DBG_TCP, "MAIN: Rx Invalid TCP Event\n");
            return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessIpEvents                                        */
/* Description   : This routine processes the events received from IP        */
/* Input(s)      : aTaskEnqMsgInfo - info about events and addresses         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpProcessIpEvents (tLdpIpEvtInfo * pIpEvtInfo)
{
    UINT2               u2IncarnId = 0;
    tLdpRouteEntryInfo *pRouteInfo = NULL;

    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);

    if (pIpEvtInfo == NULL)
    {
        return LDP_FAILURE;
    }

    LDP_DBG ((LDP_IF_MISC | LDP_IF_ALL), "LdpProcessIpEvents: ENTRY\n");

    LDP_DBG1 ((LDP_IF_MISC | LDP_IF_ALL),
              "LdpProcessIpEvents: Event Bit Map: %x\n", pIpEvtInfo->u4BitMap);

    u2IncarnId = (UINT2) pIpEvtInfo->u4IncarnId;
    if (TMO_SLL_Count (pEntityList) == LDP_ZERO)
    {
        pRouteInfo = (tLdpRouteEntryInfo *) (VOID *) pIpEvtInfo->pRouteInfo;
        if (pRouteInfo != NULL)
        {
            MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
        }
        return LDP_SUCCESS;
    }

    switch (pIpEvtInfo->u4BitMap)
    {
        case LDP_IF_OPER_ENABLE:
            if (LdpProcessCfaOperUpEvent (pIpEvtInfo) == LDP_FAILURE)
            {
                return LDP_FAILURE;
            }
            break;
        case LDP_IF_OPER_DISABLE:
            if (LdpProcessCfaOperDownEvent (pIpEvtInfo) == LDP_FAILURE)
            {
                return LDP_FAILURE;
            }
            break;
        case LDP_RT_NEW:
        case LDP_RT_NH_CHG:
        case LDP_RT_DELETED:
            if (LdpProcessRouteChangeEvent (pIpEvtInfo) == LDP_FAILURE)
            {
                return LDP_FAILURE;
            }
            break;
        default:

            break;
    };
    return LDP_SUCCESS;
}

UINT1
LdpProcessRouteChangeEvent (tLdpIpEvtInfo * pIpEvtInfo)
{
    UINT4               u4HIndex = 0;
    UINT2               u2IncarnId = (UINT2) pIpEvtInfo->u4IncarnId;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    UINT1               u1PrefLen = 0;
    UINT1               u1Count = 0;
    UINT1               u1Event = 0;
    tTMO_HASH_NODE     *pSsnHashNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tLdpTeHopInfo      *pCrlspNextHopInfo = NULL;
    UINT4               u4DestAddr;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    uGenU4Addr          TempAddr;
    UINT1               u1RouteFound = LDP_FALSE;
    UINT4               u4RtPort = 0;
    UINT4               u4RtGw = 0;
    UINT4               u4FsIpRouteStatus = 0;
    tFtnEntry          *pFtnEntry = NULL;
    tGenU4Addr          TempRtGw;
    BOOL1               b1IsDownstreamFecExist = LDP_FALSE;
    BOOL1               b1IsEcmpDownstreamFecExist = LDP_FALSE;

    /* Normally route entries are aggregated routes. Hence default Fec Type
     * is assumed to be Host Address Prefix Type */
    tFec                FecClass;

    tLdpRouteEntryInfo *pRouteInfo = NULL;

    MEMSET (&TempAddr, LDP_ZERO, sizeof (uGenU4Addr));
    MEMSET (&FecClass, LDP_ZERO, sizeof (tFec));
    MEMSET (&TempRtGw, LDP_ZERO, sizeof (tGenU4Addr));

    FecClass.u1PreLen = LDP_FEC_PREFIX_TYPE;

    pRouteInfo = (tLdpRouteEntryInfo *) (VOID *) pIpEvtInfo->pRouteInfo;
    /* Get the prefix length from the Destination Mask */
    LDP_GET_PRFX_LEN (LDP_IPV4_U4_ADDR (pRouteInfo->DestMask), u1PrefLen);

    /* Identifying the Fec Type */
    /* Constructing the Fec */
    FecClass.u2AddrFmly = IPV4;
    FecClass.u1PreLen = u1PrefLen;
    LDP_IPV4_U4_ADDR (FecClass.Prefix) =
        LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr);
    u4FsIpRouteStatus = pRouteInfo->u4Flag;

    LDP_DBG3 (LDP_DBG_PRCS,
              "Rt Chg for Dest %x NextHop %x Evt %x\n",
              LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr),
              LDP_IPV4_U4_ADDR (pRouteInfo->NextHop), pIpEvtInfo->u4BitMap);

    LDP_DBG5 (LDP_DBG_PRCS,
              "Rt Chg for Dest %x u1PrefLen=%d Evt %x NxtHop %x\n FsIpRoutestatus= %x",
              LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr), u1PrefLen,
              pIpEvtInfo->u4BitMap, LDP_IPV4_U4_ADDR (pRouteInfo->NextHop),
              pRouteInfo->u4Flag);

    if (NetIpv4GetCfaIfIndexFromPort (pRouteInfo->u4RtIfIndx,
                                      &(pRouteInfo->u4RtIfIndx)) ==
        NETIPV4_FAILURE)
    {
        LDP_DBG (LDP_MAIN_MISC, "MAIN: Cfa Index for IP Index Failed \n");
    }

    if (pIpEvtInfo->u4BitMap == LDP_RT_NEW)
    {
        /*  Any New Route followed by delete event will be stored in 
         *  the better next hop array and route change events will 
         *  be invoked only as the better next hop change upon
         *  the expiry of the next hop change event. 
         */

        for (u1Count = 0; u1Count < MAX_LDP_IP_RT_ENTRIES; u1Count++)
        {
            if (((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count))
                 != LDP_ZERO) &&
                ((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count)
                  == LDP_RT_DELETED)))
            {

                if ((LdpIsAddressMatchWithAddrType
                     (&LDP_FECROUTE_ARRAY_PREFIX (u2IncarnId, u1Count),
                      &FecClass.Prefix, LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId,
                                                                   u1Count),
                      FecClass.u2AddrFmly) == LDP_TRUE)
                    && (LDP_FECROUTE_ARRAY_PRELEN (u2IncarnId, u1Count) ==
                        FecClass.u1PreLen))
                {
                    LDP_FECROUTE_ARRAY_IPV4_NHOP (u2IncarnId, u1Count)
                        = LDP_IPV4_U4_ADDR (pRouteInfo->NextHop);
                    LDP_FECROUTE_ARRAY_OUTIFINDEX (u2IncarnId, u1Count)
                        = pRouteInfo->u4RtIfIndx;
                    LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count)
                        = LDP_RT_NH_CHG;

                    LDP_DBG1 (LDP_DBG_PRCS,
                              "LdpProcessIpEvents: Converting the RT_DELETED "
                              "to RT_NH_CHG for the prefix: %x\n",
                              FecClass.Prefix.u4Addr);
                    /* 
                     * Route info allocated when the IP event was indicated
                     * is released. 
                     */
                    MemReleaseMemBlock (LDP_IP_RT_POOL_ID,
                                        (UINT1 *) pRouteInfo);
                    return LDP_SUCCESS;
                }
            }
            else if (((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count))
                      != LDP_ZERO) &&
                     ((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count)
                       == LDP_RT_NH_CHG)))
            {

                if ((LdpIsAddressMatchWithAddrType
                     (&LDP_FECROUTE_ARRAY_PREFIX (u2IncarnId, u1Count),
                      &FecClass.Prefix, LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId,
                                                                   u1Count),
                      FecClass.u2AddrFmly) == LDP_TRUE)
                    && (LDP_FECROUTE_ARRAY_PRELEN (u2IncarnId, u1Count) ==
                        FecClass.u1PreLen)
                    && (LDP_FECROUTE_ARRAY_IPV4_NHOP (u2IncarnId, u1Count) ==
                        LDP_IPV4_U4_ADDR (pRouteInfo->NextHop)))
                {

                    LDP_DBG1 (LDP_DBG_PRCS,
                              "LdpProcessIpEvents: Ignoring Route Addition trigger %x\n",
                              FecClass.Prefix.u4Addr);
                    MemReleaseMemBlock (LDP_IP_RT_POOL_ID,
                                        (UINT1 *) pRouteInfo);

                    return LDP_SUCCESS;
                }
            }

        }

        if ((u4FsIpRouteStatus & RTM_ECMP_RT) == RTM_ECMP_RT)
        {
            pFtnEntry =
                MplsSignalGetFtnTableEntry (LDP_IPV4_U4_ADDR (FecClass.Prefix));
            if (pFtnEntry != NULL)
            {
                LDP_DBG2 (LDP_DBG_PRCS,
                          "%s : %d ECMP case for route Addition, Ignoring route Addition\n",
                          __FUNCTION__, __LINE__);
                return LDP_SUCCESS;
            }
        }

        /* 
         * New Entry to Routing Table. So creating New Lsp to the 
         * given Destination.
         */
        LdpTriggerRtChgEvent (u2IncarnId, LDP_RT_NEW, pRouteInfo, NULL, NULL);
    }
    else
    {
        /* Modification to existing Route Entry */
        /* 
         * Assumption made: If the Next Hop changes, then the Interface
         * Index over which the Next Hop is reachable, May or May Not 
         * Change.
         * Especially in cases of Multi-Acess domains like Ethernet.
         */
        pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

        if (((pIpEvtInfo->u4BitMap & LDP_RT_NH_CHG) == LDP_RT_NH_CHG)
            || ((pIpEvtInfo->u4BitMap & LDP_RT_IFACE_CHG) == LDP_RT_IFACE_CHG))
        {
            /* 
             * Next Hop or Interface Index changed, for the given Route 
             * Entry.
             * Scan through all the LCBs maintained by the Active sessions 
             * in the given Incarn, and raise NextHop change to relevant 
             * LCBs.
             */

            /* 
             * Scan through all the sessions maintained in the Session 
             * HashTable, to check if any LCB established over a Session, 
             * is getting effected by this NextHop change.
             */
            TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
            {
                TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex,
                                      pSsnHashNode, tTMO_HASH_NODE *)
                {
                    pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);

                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                      pTempLdpSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);
                        /* 
                         * Rising Next Hop Change Event to Lsps matching the
                         * the given Destination and Mask. 
                         */
                        if (!((LCB_TRIG (pLspCtrlBlk)) &&
                              (LCB_TRIG_TYPE (pLspCtrlBlk) ==
                               NEXT_HOP_CHNG_LSP_SET_UP)))
                        {
                            if (pLspCtrlBlk->pCrlspTnlInfo != NULL)
                            {
                                if (CRLSP_RTPIN_TYPE
                                    (pLspCtrlBlk->pCrlspTnlInfo) != LDP_FALSE)
                                {
                                    pCrlspNextHopInfo =
                                        (tLdpTeHopInfo *) TMO_SLL_First
                                        ((tTMO_SLL *)
                                         (&CRLSP_ERHOP_LIST
                                          (pLspCtrlBlk->pCrlspTnlInfo)));

                                    if (pCrlspNextHopInfo == NULL)
                                    {
                                        continue;
                                    }
                                    if (CRLSP_TE_ERHOP_ADDR_TYPE
                                        (pCrlspNextHopInfo)
                                        == CRLSP_TE_ERHOP_IPV4_TYPE)
                                    {
                                        MEMCPY ((UINT1 *) (&(u4DestAddr)),
                                                (UINT1 *)
                                                (&CRLSP_TE_ERHOP_IPV4_ADDR
                                                 (pCrlspNextHopInfo)),
                                                LDP_IPV4ADR_LEN);
                                        u4DestAddr = OSIX_NTOHL (u4DestAddr);
                                        LDP_IPV4_U4_ADDR (TempAddr) =
                                            u4DestAddr;
                                        if (LdpIsAddressMatchWithAddrType
                                            (&TempAddr, &FecClass.Prefix,
                                             LDP_ADDR_TYPE_IPV4,
                                             FecClass.u2AddrFmly) == LDP_TRUE)
                                        {
                                            LdpTriggerRtChgEvent
                                                (u2IncarnId, LDP_RT_NH_CHG,
                                                 pRouteInfo, pLspCtrlBlk,
                                                 pLdpSession);
                                        }

                                    }
                                    else
                                    {
                                        LDP_DBG (LDP_MAIN_MISC,
                                                 "MAIN: AddrType not "
                                                 "supported\n");
                                        continue;
                                    }
                                }
                            }
                            else if ((LdpIsAddressMatchWithAddrType
                                      (&FecClass.Prefix,
                                       &pLspCtrlBlk->Fec.Prefix,
                                       FecClass.u2AddrFmly,
                                       pLspCtrlBlk->Fec.u2AddrFmly) == LDP_TRUE)
                                     && (FecClass.u1PreLen ==
                                         pLspCtrlBlk->Fec.u1PreLen))
                            {

                                LdpTriggerRtChgEvent (u2IncarnId,
                                                      LDP_RT_NH_CHG,
                                                      pRouteInfo,
                                                      pLspCtrlBlk, pLdpSession);
                            }
                        }
                    }
                }
            }
        }
        else if ((pIpEvtInfo->u4BitMap & LDP_RT_DELETED) == LDP_RT_DELETED)
        {
            /*  Any Route delete event will restart the timer
             *  for determining if it's better next hop change
             *  (by waiting for an add event to come for
             *  the same prefix subsequently) 
             */

            LDP_DBG1 (LDP_DBG_PRCS, "Route Delete Event for the Prefix: %x\n",
                      LDP_IPV4_U4_ADDR (FecClass.Prefix));

            LDP_DBG2 (LDP_DBG_PRCS,
                      "Querying route for the prefix: %x and DestMsk: %x\n",
                      LDP_IPV4_U4_ADDR (FecClass.Prefix),
                      LDP_IPV4_U4_ADDR (pRouteInfo->DestMask));

            /* To Handle the case of ECMP routes: Since LDP does not explicitly
             * keep list of multiple equal cost routes, we query for the best
             * route on receiving a route delete event. If a route exists, with
             * a different next hop, we will convert this event to the next
             * hop change event. This will be processed upon Fec Reroute timer
             * expiry */

            if (LdpIpGetExactRoute (LDP_IPV4_U4_ADDR (FecClass.Prefix),
                                    LDP_IPV4_U4_ADDR (pRouteInfo->DestMask),
                                    &u4RtPort, &u4RtGw) == LDP_IP_SUCCESS)
            {
                LDP_IPV4_U4_ADDR (TempRtGw.Addr) = u4RtGw;
                TempRtGw.u2AddrType = LDP_ADDR_TYPE_IPV4;

                if ((u4RtGw != LDP_IPV4_U4_ADDR (pRouteInfo->NextHop)) &&
                    (LdpGetPeerSession (&TempRtGw, u4RtPort, &pLdpSession,
                                        u2IncarnId) == LDP_SUCCESS))
                {
                    LDP_DBG2 (LDP_DBG_PRCS,
                              "Rcvd Route Delete Event for Prefix: %x: Found Another route: NH: %x\n",
                              LDP_IPV4_U4_ADDR (FecClass.Prefix), u4RtGw);
                    u1RouteFound = LDP_TRUE;
                }
                else
                {
                    LDP_DBG1 (LDP_DBG_PRCS,
                              "Route found for prefix: %x, but has the same next hop\n",
                              LDP_IPV4_U4_ADDR (FecClass.Prefix));
                }
            }
            pLdpSession = NULL;

            for (u1Count = 0; u1Count < MAX_LDP_IP_RT_ENTRIES; u1Count++)
            {
                if (((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count))
                     != LDP_ZERO) &&
                    ((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count)
                      == LDP_RT_NH_CHG)))
                {

                    if ((LdpIsAddressMatchWithAddrType
                         (&LDP_FECROUTE_ARRAY_PREFIX (u2IncarnId, u1Count),
                          &FecClass.Prefix,
                          LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId, u1Count),
                          FecClass.u2AddrFmly) == LDP_TRUE)
                        && (LDP_FECROUTE_ARRAY_PRELEN (u2IncarnId, u1Count) ==
                            FecClass.u1PreLen)
                        &&
                        (LdpIsAddressMatchWithAddrType
                         (&LDP_FECROUTE_ARRAY_NHOP (u2IncarnId, u1Count),
                          &pRouteInfo->NextHop,
                          LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId, u1Count),
                          pRouteInfo->u2AddrType) == LDP_TRUE))
                    {

                        LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count)
                            = LDP_RT_DELETED;
                        LDP_DBG1 (LDP_DBG_PRCS,
                                  "converting Nexthop change to route delete for FEC=%x\n",
                                  LDP_IPV4_U4_ADDR (FecClass.Prefix));

                        MemReleaseMemBlock (LDP_IP_RT_POOL_ID,
                                            (UINT1 *) pRouteInfo);
                        return LDP_SUCCESS;
                    }

                }
            }

            TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
            {
                TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex,
                                      pSsnHashNode, tTMO_HASH_NODE *)
                {
                    pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);
#if 0
                    LDP_DBG4 (LDP_SSN_PRCS,
                              "Session with Upstream peer %d.%d.%d.%d is "
                              "processed\n",
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[0],
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[1],
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[2],
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[3]);
#endif

                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pSllNode,
                                      pTempSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pSllNode);
                        if ((u4FsIpRouteStatus & RTM_ECMP_RT) == RTM_ECMP_RT)
                        {
#if 0
                            LDP_DBG4 (LDP_SSN_PRCS,
                                      "FEC=%x PreLen=%d NxtHop=%x Mlibstatus=%d\n",
                                      pLspCtrlBlk->Fec.u4Prefix,
                                      pLspCtrlBlk->Fec.u1PreLen,
                                      pRouteInfo->u4NextHop,
                                      LCB_MLIB_UPD_STATUS (pLspCtrlBlk));
#endif

                            if ((LdpIsAddressMatchWithAddrType
                                 (&FecClass.Prefix,
                                  &LCB_FEC (pLspCtrlBlk).Prefix,
                                  FecClass.u2AddrFmly,
                                  LCB_FEC (pLspCtrlBlk).u2AddrFmly) == LDP_TRUE)
                                && (FecClass.u1PreLen ==
                                    LCB_FEC (pLspCtrlBlk).u1PreLen)
                                &&
                                (LdpIsAddressMatchWithAddrType
                                 (&pLspCtrlBlk->NextHopAddr,
                                  &pRouteInfo->NextHop,
                                  LCB_FEC (pLspCtrlBlk).u2AddrFmly,
                                  pRouteInfo->u2AddrType) == LDP_TRUE)
                                && (LCB_MLIB_UPD_STATUS (pLspCtrlBlk) ==
                                    LDP_DU_DN_MLIB_UPD_DONE))
                            {
                                b1IsEcmpDownstreamFecExist = LDP_TRUE;
                            }
                        }
                        else
                        {

                            if ((LdpIsAddressMatchWithAddrType
                                 (&FecClass.Prefix,
                                  &LCB_FEC (pLspCtrlBlk).Prefix,
                                  FecClass.u2AddrFmly,
                                  LCB_FEC (pLspCtrlBlk).u2AddrFmly) == LDP_TRUE)
                                && (FecClass.u1PreLen ==
                                    LCB_FEC (pLspCtrlBlk).u1PreLen)
                                &&
                                (LdpIsAddressMatchWithAddrType
                                 (&pLspCtrlBlk->NextHopAddr,
                                  &pRouteInfo->NextHop,
                                  LCB_FEC (pLspCtrlBlk).u2AddrFmly,
                                  pRouteInfo->u2AddrType) == LDP_TRUE))
                            {
                                b1IsDownstreamFecExist = LDP_TRUE;
                                break;
                            }
                        }
                        if ((b1IsDownstreamFecExist == LDP_TRUE) ||
                            (b1IsEcmpDownstreamFecExist == TRUE))

                        {
                            break;
                        }
                    }
                }
            }

            if ((((u4FsIpRouteStatus & RTM_ECMP_RT) == RTM_ECMP_RT)) &&
                (b1IsEcmpDownstreamFecExist == LDP_FALSE))
            {

                LDP_DBG2 (LDP_DBG_PRCS,
                          "%s : %d ECMP case for route Deletion, Ignoring route Deletion\n",
                          __FUNCTION__, __LINE__);
                return LDP_SUCCESS;
            }

            if ((b1IsDownstreamFecExist == LDP_TRUE) ||
                (b1IsEcmpDownstreamFecExist == TRUE))
            {
                for (u1Count = 0; u1Count < MAX_LDP_IP_RT_ENTRIES; u1Count++)
                {
                    if ((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count))
                        == LDP_ZERO)
                    {
                        TmrStopTimer (LDP_TIMER_LIST_ID,
                                      (tTmrAppTimer *)
                                      & (gLdpInfo.LdpIncarn[(u2IncarnId)].
                                         FecRouteTimer.AppTimer));

                        LDP_FECROUTE_ARRAY_IPV4_PREFIX (u2IncarnId, u1Count) =
                            LDP_IPV4_U4_ADDR (FecClass.Prefix);
                        LDP_FECROUTE_ARRAY_PRELEN (u2IncarnId, u1Count) =
                            FecClass.u1PreLen;
                        LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId, u1Count) =
                            FecClass.u2AddrFmly;

                        if (u1RouteFound == LDP_FALSE)
                        {
                            LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count) =
                                LDP_RT_DELETED;
                            LDP_FECROUTE_ARRAY_OUTIFINDEX (u2IncarnId, u1Count)
                                = pRouteInfo->u4RtIfIndx;
                            LDP_FECROUTE_ARRAY_IPV4_NHOP (u2IncarnId, u1Count)
                                = LDP_IPV4_U4_ADDR (pRouteInfo->NextHop);
                        }
                        else
                        {
                            LDP_DBG1 (LDP_DBG_PRCS,
                                      "Converting the RT_DEL to NH_CHG event for prefix: %x\n",
                                      LDP_IPV4_U4_ADDR (FecClass.Prefix));

                            LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count)
                                = LDP_RT_NH_CHG;
                            LDP_FECROUTE_ARRAY_OUTIFINDEX (u2IncarnId, u1Count)
                                = u4RtPort;
                            LDP_FECROUTE_ARRAY_IPV4_NHOP (u2IncarnId, u1Count)
                                = u4RtGw;
                        }
                        LDP_FECROUTE_TIMER (u2IncarnId).u4Event =
                            LDP_FEC_REROUTE_EXPIRED_EVT;

                        LDP_FECROUTE_TIMER (u2IncarnId).pu1EventInfo = 0;

                        LDP_DBG3 (LDP_DBG_PRCS,
                                  "Adding the Route Event Fec=%x Operation=%x Index=%d\n",
                                  LDP_IPV4_U4_ADDR (FecClass.Prefix),
                                  LDP_FECROUTE_INDEX_AVBLE (u2IncarnId,
                                                            u1Count), u1Count);

                        TmrStartTimer (LDP_TIMER_LIST_ID,
                                       (tTmrAppTimer *)
                                       & (gLdpInfo.LdpIncarn[(u2IncarnId)].
                                          FecRouteTimer.AppTimer),
                                       (FEC_REROUTE_STABLE_TIME *
                                        SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
                        /* 
                         * Route info allocated when the IP event was indicated
                         * is released. 
                         */
                        MemReleaseMemBlock (LDP_IP_RT_POOL_ID,
                                            (UINT1 *) pRouteInfo);
                        return LDP_SUCCESS;
                    }
                }

            }

            /* 
             * Row Status of the route entry changed to 'Not in Service'. 
             * Deleting all the Lsps that are associated with this 
             * Destination. 
             * 
             * Get the session that is associated with the NextHopAddr & 
             * ifIndex. The peer that has advertised the NextHopAddr in 
             * its LDP Address msg is located. If no such peer exists, 
             * implies no Lsps associated.
             * If the peer exists, the session associated with the peer 
             * is accessed for further processing.
             */

            LDP_DBG1 (LDP_SSN_PRCS,
                      "Route Deletion for the route %x handled\n",
                      LDP_IPV4_U4_ADDR (FecClass.Prefix));

            TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
            {
                TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex,
                                      pSsnHashNode, tTMO_HASH_NODE *)
                {
                    pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);
                    LDP_DBG4 (LDP_SSN_PRCS,
                              "Session with Upstream peer %d.%d.%d.%d is "
                              "processed\n",
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[0],
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[1],
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[2],
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[3]);

                    TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession),
                                      pSllNode, pTempSllNode, tTMO_SLL_NODE *)
                    {
                        pLspUpCtrlBlock = (tUstrLspCtrlBlock *) (pSllNode);
#ifdef MPLS_IPV6_WANTED
                        if (pLspUpCtrlBlock->Fec.u2AddrFmly ==
                            LDP_ADDR_TYPE_IPV6)
                        {
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "LSP for prefix %s is processed\n",
                                      Ip6PrintAddr (&pLspUpCtrlBlock->Fec.
                                                    Prefix.Ip6Addr));
                        }
                        else
#endif
                        {
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "LSP for prefix %x is processed\n",
                                      LDP_IPV4_U4_ADDR (pLspUpCtrlBlock->Fec.
                                                        Prefix));
                        }

                        if (!
                            ((LdpIsAddressMatchWithAddrType (&FecClass.Prefix,
                                                             &pLspUpCtrlBlock->
                                                             Fec.Prefix,
                                                             FecClass.
                                                             u2AddrFmly,
                                                             pLspUpCtrlBlock->
                                                             Fec.u2AddrFmly) ==
                              LDP_TRUE)
                             && (FecClass.u1PreLen ==
                                 pLspUpCtrlBlock->Fec.u1PreLen)))
                        {
                            continue;
                        }
#ifdef MPLS_IPV6_WANTED
                        if (pLspUpCtrlBlock->Fec.u2AddrFmly ==
                            LDP_ADDR_TYPE_IPV6)
                        {
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "Sem for the LSP %s is called\n",
                                      Ip6PrintAddr (&pLspUpCtrlBlock->Fec.
                                                    Prefix.Ip6Addr));
                        }
                        else
#endif
                        {
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "Sem for the LSP %x is called\n",
                                      LDP_IPV4_U4_ADDR (pLspUpCtrlBlock->Fec.
                                                        Prefix));
                        }
                        if (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT)
                        {
                            u1Event = LDP_DU_UP_LSM_EVT_DEL_FEC;
                            LdpDuUpSm (pLspUpCtrlBlock, u1Event,
                                       (tLdpMsgInfo *) (VOID *) pRouteInfo);
                        }
                        else
                        {
                            u1Event = LDP_DU_UP_LSM_EVT_INT_DN_WR;
                            LdpInvCorrStMh (pLdpSession, pLspUpCtrlBlock,
                                            u1Event,
                                            (tLdpMsgInfo *) (VOID *)
                                            pRouteInfo, LDP_TRUE);
                        }
                        break;
                    }

                    /* 
                     * Scanning all the DownStream LSP control Blocks to get 
                     * the Lsps that are effected by the Deletion of this route 
                     * entry. 
                     */
                    LDP_DBG4 (LDP_SSN_PRCS,
                              "Session with Dnstream peer %d.%d.%d.%d is "
                              "processed\n",
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[0],
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[1],
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[2],
                              pLdpSession->pLdpPeer->NetAddr.Addr.
                              au1Ipv4Addr[3]);

                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pSllNode,
                                      pTempSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pSllNode);
                        if ((LdpIsAddressMatchWithAddrType (&FecClass.Prefix,
                                                            &pLspCtrlBlk->Fec.
                                                            Prefix,
                                                            FecClass.u2AddrFmly,
                                                            pLspCtrlBlk->Fec.
                                                            u2AddrFmly) ==
                             LDP_TRUE)
                            && (FecClass.u1PreLen == pLspCtrlBlk->Fec.u1PreLen))

                        {
                            /* 
                             * Destroying the Lsp, that was established using 
                             * the Deleted Route Entry.
                             */
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "Sem is called for LSP %x\n",
                                      LDP_IPV4_U4_ADDR (FecClass.Prefix));

                            LdpTriggerRtChgEvent (u2IncarnId,
                                                  LDP_RT_DELETED,
                                                  pRouteInfo, pLspCtrlBlk,
                                                  pLdpSession);
                            break;
                        }
                    }
                }
            }
        }
    }

    /* Route info allocated when the IP event was indicated is released. */
    MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);

    return LDP_SUCCESS;
}

#ifdef MPLS_IPV6_WANTED

UINT1
LdpProcessIpv6RouteChangeEvent (tLdpIpEvtInfo * pIpEvtInfo)
{

    UINT4               u4HIndex = 0;
    UINT2               u2IncarnId = (UINT2) pIpEvtInfo->u4IncarnId;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    UINT1               u1PrefLen = 0;
    UINT1               u1Count = 0;
    UINT1               u1Event = 0;
    tTMO_HASH_NODE     *pSsnHashNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tIp6Addr            RtGw;
    UINT1               u1RouteFound = LDP_FALSE;
    UINT4               u4RtPort = 0;

    /* Normally route entries are aggregated routes. Hence default Fec Type
     *          *      * is assumed to be Host Address Prefix Type */
    tFec                FecClass;

    tLdpRouteEntryInfo *pRouteInfo = NULL;
    uGenU4Addr          TempAddr;

    MEMSET (&FecClass, LDP_ZERO, sizeof (tFec));
    MEMSET (&RtGw, 0, sizeof (tIp6Addr));
    FecClass.u1PreLen = LDP_FEC_PREFIX_TYPE;

    LDP_DBG1 (LDP_DBG_PRCS, "%s: ENTRY\n", __func__);

    MEMSET (&TempAddr, LDP_ZERO, sizeof (uGenU4Addr));
    pRouteInfo = (tLdpRouteEntryInfo *) (VOID *) pIpEvtInfo->pRouteInfo;
    /* Get the prefix length from the Destination Mask */
    u1PrefLen =
        MplsGetIpv6Subnetmasklen (LDP_IPV6_U4_ADDR (pRouteInfo->DestMask));

    /* Identifying the Fec Type */
    /* Constructing the Fec */
    FecClass.u2AddrFmly = LDP_ADDR_TYPE_IPV6;
    FecClass.u1PreLen = u1PrefLen;

    MEMCPY (&FecClass.Prefix.Ip6Addr, &pRouteInfo->DestAddr.Ip6Addr,
            LDP_IPV6ADR_LEN);

    switch (pIpEvtInfo->u4BitMap)
    {
        case LDP_RT_NEW:

            /*  Any New Route followed by delete event will be stored in 
             *  the better next hop array and route change events will 
             *  be invoked only as the better next hop change upon
             *  the expiry of the next hop change event. 
             */
            for (u1Count = 0; u1Count < MAX_LDP_IP_RT_ENTRIES; u1Count++)
            {
                if (((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count))
                     != LDP_ZERO) &&
                    ((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count)
                      == LDP_RT_DELETED)))
                {
                    if ((LdpIsAddressMatchWithAddrType
                         (&LDP_FECROUTE_ARRAY_PREFIX (u2IncarnId, u1Count),
                          &FecClass.Prefix,
                          LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId, u1Count),
                          FecClass.u2AddrFmly) == LDP_TRUE)
                        && (LDP_FECROUTE_ARRAY_PRELEN (u2IncarnId, u1Count) ==
                            FecClass.u1PreLen))
                    {
                        MEMCPY (LDP_FECROUTE_ARRAY_IPV6_NHOP
                                (u2IncarnId, u1Count),
                                LDP_IPV6_U4_ADDR (pRouteInfo->NextHop),
                                LDP_IPV6ADR_LEN);
                        LDP_FECROUTE_ARRAY_OUTIFINDEX (u2IncarnId, u1Count) =
                            pRouteInfo->u4RtIfIndx;
                        LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count) =
                            LDP_RT_NH_CHG;
                        /* 
                         * Route info allocated when the IP event was indicated
                         * is released. 
                         */
                        MemReleaseMemBlock (LDP_IP_RT_POOL_ID,
                                            (UINT1 *) pRouteInfo);
                        return LDP_SUCCESS;
                    }
                }
            }

            /* 
             * New Entry to Routing Table. So creating New Lsp to the 
             * given Destination.
             */
            LdpTriggerRtChgEvent (u2IncarnId, LDP_RT_NEW, pRouteInfo, NULL,
                                  NULL);
            break;
        case LDP_RT_NH_CHG:
            pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

            /* 
             * Next Hop or Interface Index changed, for the given Route 
             * Entry.
             * Scan through all the LCBs maintained by the Active sessions 
             * in the given Incarn, and raise NextHop change to relevant 
             * LCBs.
             */

            /* 
             * Scan through all the sessions maintained in the Session 
             * HashTable, to check if any LCB established over a Session, 
             * is getting effected by this NextHop change.
             */
            TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
            {
                TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex,
                                      pSsnHashNode, tTMO_HASH_NODE *)
                {
                    pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);

                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                      pTempLdpSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);
                        /* 
                         * Rising Next Hop Change Event to Lsps matching the
                         * the given Destination and Mask. 
                         */
                        if (!((LCB_TRIG (pLspCtrlBlk)) &&
                              (LCB_TRIG_TYPE (pLspCtrlBlk) ==
                               NEXT_HOP_CHNG_LSP_SET_UP)))
                        {
                            if ((LdpIsAddressMatchWithAddrType
                                 (&FecClass.Prefix, &pLspCtrlBlk->Fec.Prefix,
                                  FecClass.u2AddrFmly,
                                  pLspCtrlBlk->Fec.u2AddrFmly) == LDP_TRUE)
                                && (FecClass.u1PreLen ==
                                    pLspCtrlBlk->Fec.u1PreLen))
                            {

                                LdpTriggerRtChgEvent (u2IncarnId,
                                                      LDP_RT_NH_CHG,
                                                      pRouteInfo,
                                                      pLspCtrlBlk, pLdpSession);
                            }
                        }
                    }
                }
            }
            break;
        case LDP_RT_DELETED:
            pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

            /*  Any Route delete event will restart the timer
             *  for determining if it's better next hop change
             *  (by waiting for an add event to come for
             *  the same prefix subsequently) 
             */

            LDP_DBG1 (LDP_DBG_PRCS, "Route Delete Event for the Prefix: %s\n",
                      Ip6PrintAddr (&FecClass.Prefix.Ip6Addr));

            LDP_DBG2 (LDP_DBG_PRCS,
                      "Querying route for the prefix: %s and PrefixLen: %u\n",
                      Ip6PrintAddr (&FecClass.Prefix.Ip6Addr),
                      FecClass.u1PreLen);

            /* To Handle the case of ECMP routes: Since LDP does not explicitly
             * keep list of multiple equal cost routes, we query for the best
             * route on receiving a route delete event. If a route exists, with
             * a different next hop, we will convert this event to the next
             * hop change event. This will be processed upon Fec Reroute timer
             * expiry */

            if (LdpIpv6GetExactRoute (&FecClass.Prefix.Ip6Addr,
                                      FecClass.u1PreLen,
                                      &u4RtPort, &RtGw) == LDP_SUCCESS)
            {
                if (MEMCMP
                    (&RtGw, LDP_IPV6_U4_ADDR (pRouteInfo->NextHop),
                     LDP_IPV6ADR_LEN) != 0)
                {
                    LDP_DBG2 (LDP_DBG_PRCS,
                              "Rcvd Route Delete Event for Prefix: %s: Found Another route: NH: %s\n",
                              Ip6PrintAddr (&FecClass.Prefix.Ip6Addr),
                              Ip6PrintAddr (&RtGw));
                    u1RouteFound = LDP_TRUE;
                }
                else
                {
                    LDP_DBG1 (LDP_DBG_PRCS,
                              "Route found for prefix: %s, but has the same next hop\n",
                              Ip6PrintAddr (&FecClass.Prefix.Ip6Addr));
                }
            }

            for (u1Count = 0; u1Count < MAX_LDP_IP_RT_ENTRIES; u1Count++)
            {
                if ((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count))
                    == LDP_ZERO)
                {
                    TmrStopTimer (LDP_TIMER_LIST_ID,
                                  (tTmrAppTimer *)
                                  & (gLdpInfo.LdpIncarn[(u2IncarnId)].
                                     FecRouteTimer.AppTimer));
                    MEMCPY (LDP_FECROUTE_ARRAY_IPV6_PREFIX
                            (u2IncarnId, u1Count),
                            LDP_IPV6_U4_ADDR (FecClass.Prefix),
                            LDP_IPV6ADR_LEN);
                    LDP_FECROUTE_ARRAY_PRELEN (u2IncarnId, u1Count) =
                        FecClass.u1PreLen;
                    LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId, u1Count) =
                        FecClass.u2AddrFmly;

                    if (u1RouteFound == LDP_FALSE)
                    {
                        LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count) =
                            LDP_RT_DELETED;
                        LDP_FECROUTE_ARRAY_OUTIFINDEX (u2IncarnId, u1Count)
                            = pRouteInfo->u4RtIfIndx;
                        MEMCPY (LDP_FECROUTE_ARRAY_IPV6_NHOP
                                (u2IncarnId, u1Count),
                                LDP_IPV6_U4_ADDR (pRouteInfo->NextHop),
                                LDP_IPV6ADR_LEN);
                    }
                    else
                    {
                        LDP_DBG1 (LDP_DBG_PRCS,
                                  "Converting the RT_DEL to NH_CHG event for prefix: %s\n",
                                  Ip6PrintAddr (&FecClass.Prefix.Ip6Addr));

                        LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count) =
                            LDP_RT_NH_CHG;
                        LDP_FECROUTE_ARRAY_OUTIFINDEX (u2IncarnId, u1Count)
                            = u4RtPort;
                        MEMCPY (LDP_FECROUTE_ARRAY_IPV6_NHOP
                                (u2IncarnId, u1Count), (UINT1 *) &RtGw,
                                LDP_IPV6ADR_LEN);
                    }

                    LDP_FECROUTE_TIMER (u2IncarnId).u4Event =
                        LDP_FEC_REROUTE_EXPIRED_EVT;

                    LDP_FECROUTE_TIMER (u2IncarnId).pu1EventInfo = 0;

                    TmrStartTimer (LDP_TIMER_LIST_ID,
                                   (tTmrAppTimer *)
                                   & (gLdpInfo.LdpIncarn[(u2IncarnId)].
                                      FecRouteTimer.AppTimer),
                                   (FEC_REROUTE_STABLE_TIME *
                                    SYS_NUM_OF_TIME_UNITS_IN_A_SEC));

                    LDP_DBG (LDP_DBG_PRCS, "FEC ReRoute Timer started\n");
                    /* 
                     * Route info allocated when the IP event was indicated
                     * is released. 
                     */
                    MemReleaseMemBlock (LDP_IP_RT_POOL_ID,
                                        (UINT1 *) pRouteInfo);
                    return LDP_SUCCESS;
                }
            }

            /* 
             * Row Status of the route entry changed to 'Not in Service'. 
             * Deleting all the Lsps that are associated with this 
             * Destination. 
             * 
             * Get the session that is associated with the NextHopAddr & 
             * ifIndex. The peer that has advertised the NextHopAddr in 
             * its LDP Address msg is located. If no such peer exists, 
             * implies no Lsps associated.
             * If the peer exists, the session associated with the peer 
             * is accessed for further processing.
             */

            LDP_DBG1 (LDP_SSN_PRCS,
                      "Route Deletion for the route %s handled\n",
                      Ip6PrintAddr (&FecClass.Prefix.Ip6Addr));

            TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
            {
                TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex,
                                      pSsnHashNode, tTMO_HASH_NODE *)
                {
                    pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);
                    LDP_DBG1 (LDP_SSN_PRCS,
                              "Session with Upstream peer %s is "
                              "processed\n",
                              Ip6PrintAddr (&pLdpSession->pLdpPeer->NetAddr.
                                            Addr.Ip6Addr));
                    TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession), pSllNode,
                                      pTempSllNode, tTMO_SLL_NODE *)
                    {
                        pLspUpCtrlBlock = (tUstrLspCtrlBlock *) (pSllNode);

#ifdef MPLS_IPV6_WANTED
                        if (pLspUpCtrlBlock->Fec.u2AddrFmly ==
                            LDP_ADDR_TYPE_IPV6)
                        {
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "LSP for prefix %s is processed\n",
                                      Ip6PrintAddr (&pLspUpCtrlBlock->Fec.
                                                    Prefix.Ip6Addr));
                        }
                        else
#endif
                        {
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "LSP for prefix %x is processed\n",
                                      LDP_IPV4_U4_ADDR (pLspUpCtrlBlock->Fec.
                                                        Prefix));
                        }

                        if (!
                            ((LdpIsAddressMatchWithAddrType (&FecClass.Prefix,
                                                             &pLspUpCtrlBlock->
                                                             Fec.Prefix,
                                                             FecClass.
                                                             u2AddrFmly,
                                                             pLspUpCtrlBlock->
                                                             Fec.u2AddrFmly) ==
                              LDP_TRUE)
                             && (FecClass.u1PreLen ==
                                 pLspUpCtrlBlock->Fec.u1PreLen)))

                        {
                            continue;
                        }
#ifdef MPLS_IPV6_WANTED
                        if (pLspUpCtrlBlock->Fec.u2AddrFmly ==
                            LDP_ADDR_TYPE_IPV6)
                        {
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "Sem for the LSP %s is called\n",
                                      Ip6PrintAddr (&pLspUpCtrlBlock->Fec.
                                                    Prefix.Ip6Addr));
                        }
                        else
#endif
                        {
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "Sem for the LSP %x is called\n",
                                      LDP_IPV4_U4_ADDR (pLspUpCtrlBlock->Fec.
                                                        Prefix));
                        }
                        if (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT)
                        {
                            u1Event = LDP_DU_UP_LSM_EVT_DEL_FEC;
                            LdpDuUpSm (pLspUpCtrlBlock, u1Event,
                                       (tLdpMsgInfo *) (VOID *) pRouteInfo);
                        }
                        else
                        {
                            u1Event = LDP_DU_UP_LSM_EVT_INT_DN_WR;
                            LdpInvCorrStMh (pLdpSession, pLspUpCtrlBlock,
                                            u1Event,
                                            (tLdpMsgInfo *) (VOID *)
                                            pRouteInfo, LDP_TRUE);
                        }
                        break;
                    }
                    /* 
                     * Scanning all the DownStream LSP control Blocks to get 
                     * the Lsps that are effected by the Deletion of this route 
                     * entry. 
                     */
                    LDP_DBG1 (LDP_SSN_PRCS,
                              "Session with Dnstream peer %s is "
                              "processed\n",
                              Ip6PrintAddr (&
                                            (pLdpSession->pLdpPeer->NetAddr.
                                             Addr.Ip6Addr)));

                    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pSllNode,
                                      pTempSllNode, tTMO_SLL_NODE *)
                    {
                        pLspCtrlBlk = SLL_TO_LCB (pSllNode);

                        if ((LdpIsAddressMatchWithAddrType (&FecClass.Prefix,
                                                            &pLspCtrlBlk->Fec.
                                                            Prefix,
                                                            FecClass.u2AddrFmly,
                                                            pLspCtrlBlk->Fec.
                                                            u2AddrFmly) ==
                             LDP_TRUE)
                            && (FecClass.u1PreLen == pLspCtrlBlk->Fec.u1PreLen))
                        {
                            /* 
                             * Destroying the Lsp, that was established using 
                             * the Deleted Route Entry.
                             */
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "Sem is called for LSP %s\n",
                                      Ip6PrintAddr (&FecClass.Prefix.Ip6Addr));

                            LdpTriggerRtChgEvent (u2IncarnId,
                                                  LDP_RT_DELETED,
                                                  pRouteInfo, pLspCtrlBlk,
                                                  pLdpSession);
                            break;
                        }
                    }
                }
            }

            break;

    };

    /* Route info allocated when the IP event was indicated is released. */
    MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
    return LDP_SUCCESS;
}

UINT1
LdpProcessIpv6Events (tLdpIpEvtInfo * pIpEvtInfo)
{
    UINT2               u2IncarnId;
    tLdpRouteEntryInfo *pRouteInfo = NULL;
    tTMO_SLL           *pEntityList = NULL;
    if (pIpEvtInfo == NULL)
    {
        return LDP_FAILURE;
    }

    LDP_DBG1 ((LDP_IF_MISC | LDP_IF_ALL), "%s: ENTRY\n", __func__);

    LDP_DBG2 ((LDP_IF_MISC | LDP_IF_ALL), "%s: Event Bit Map: %x\n",
              __func__, pIpEvtInfo->u4BitMap);

    u2IncarnId = (UINT2) pIpEvtInfo->u4IncarnId;
    pEntityList = &LDP_ENTITY_LIST (u2IncarnId);

    if (TMO_SLL_Count (pEntityList) == LDP_ZERO)
    {
        pRouteInfo = (tLdpRouteEntryInfo *) (VOID *) pIpEvtInfo->pRouteInfo;
        if (pRouteInfo != NULL)
        {
            MemReleaseMemBlock (LDP_IP_RT_POOL_ID, (UINT1 *) pRouteInfo);
        }
        return LDP_SUCCESS;
    }
    switch (pIpEvtInfo->u4BitMap)
    {
        case LDP_IPV6_ADDR_ADD:
            if (LdpProcessAddrAddEvent (pIpEvtInfo) == LDP_FAILURE)
            {
                LDP_DBG1 (LDP_DBG_PRCS,
                          "LdpProcessAddrAddEvent() Failed for Addr=%s\n",
                          Ip6PrintAddr (&pIpEvtInfo->LnklocalIpv6Addr));
                return LDP_FAILURE;
            }
            break;
        case LDP_IPV6_ADDR_DEL:
            if (LdpProcessAddrDelEvent (pIpEvtInfo) == LDP_FAILURE)
            {
                LDP_DBG1 (LDP_DBG_PRCS,
                          "LdpProcessAddrDelEvent() Failed for Addr=%s\n",
                          Ip6PrintAddr (&pIpEvtInfo->LnklocalIpv6Addr));
                return LDP_FAILURE;
            }
            break;
        case LDP_RT_NEW:
        case LDP_RT_NH_CHG:
        case LDP_RT_DELETED:
            if (LdpProcessIpv6RouteChangeEvent (pIpEvtInfo) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_DBG_PRCS,
                         "LdpProcessIpv6RouteChangeEvent Failed\n");
                return LDP_FAILURE;
            }
            break;
        default:

            LDP_DBG1 (LDP_DBG_PRCS,
                      " Default event, pIpEvtInfo->u4BitMap = %d \n",
                      pIpEvtInfo->u4BitMap);
            break;
    };
    return LDP_SUCCESS;
}

UINT1
LdpProcessAddrAddEvent (tLdpIpEvtInfo * pIpEvtInfo)
{
    UINT2               u2IncarnId = (UINT2) pIpEvtInfo->u4IncarnId;
    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpPeer           *pTempLdpPeer = NULL;
    tLdpIfTableEntry   *pIfEntry = NULL;
    tLdpSession        *pLdpSession = NULL;
    INT4                i4RetVal = 0;
    tCfaIfInfo          CfaIfInfo;

    tIp6Addr            LnklocalIpv6Addr;
    tIp6Addr            SitelocalIpv6Addr;
    tIp6Addr            GlbUniqueIpv6Addr;
    tIp6Addr            TempIpv6Addr;
    tIp6Addr            Ipv6IfAddr;
    tIp6Addr            V6MulticastAddr;
    tIp6Addr           *pLnklocalIpv6Addr = NULL;
    tIp6Addr           *pSitelocalIpv6Addr = NULL;
    tIp6Addr           *pGlbUniqueIpv6Addr = NULL;
    UINT1               u1Scope = 0;
    INT4                i4Udp6SockFd = LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);

    MEMSET (&LnklocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&SitelocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&GlbUniqueIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&TempIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&Ipv6IfAddr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&V6MulticastAddr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&CfaIfInfo, LDP_ZERO, sizeof (tCfaIfInfo));

    u1Scope = Ip6GetAddrScope (&pIpEvtInfo->LnklocalIpv6Addr);

    switch (u1Scope)
    {
        case ADDR6_SCOPE_SITELOCAL:
            MEMCPY (&SitelocalIpv6Addr, &pIpEvtInfo->LnklocalIpv6Addr,
                    LDP_IPV6ADR_LEN);
            pSitelocalIpv6Addr = &SitelocalIpv6Addr;
            break;
        case ADDR6_SCOPE_GLOBAL:
            MEMCPY (&GlbUniqueIpv6Addr, &pIpEvtInfo->LnklocalIpv6Addr,
                    LDP_IPV6ADR_LEN);
            pGlbUniqueIpv6Addr = &GlbUniqueIpv6Addr;
            break;
        case ADDR6_SCOPE_LLOCAL:
            MEMCPY (&LnklocalIpv6Addr, &pIpEvtInfo->LnklocalIpv6Addr,
                    LDP_IPV6ADR_LEN);
            pLnklocalIpv6Addr = &LnklocalIpv6Addr;
            break;
    };

    LDP_DBG2 (LDP_SSN_PRCS, "%s Recieved Addr Add Event for Index=%d", __func__,
              pIpEvtInfo->u4IfIndex);

    PrintInterfaceAddr (pIpEvtInfo);

    if (CfaGetIfInfo (pIpEvtInfo->u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        LDP_DBG3 (LDP_SSN_PRCS, " %s : %d CfaGetIfInfo() failed for Index=%d\n",
                  __func__, __LINE__, pIpEvtInfo->u4IfIndex);

        return LDP_FAILURE;
    }

    if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
    {
        /** Entity Interface List Handling **/
        TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
        {
            i4RetVal = LdpGetLdpEntityIfEntry (pIpEvtInfo->u4IncarnId,
                                               pLdpEntity,
                                               pIpEvtInfo->u4IfIndex,
                                               &pIfEntry);

            if (i4RetVal == LDP_SUCCESS)
            {
                if (pIfEntry->u1OperStatus == LDP_IF_OPER_ENABLE)
                {
                    if (LdpUpdateAddrIfTableEntry (pIfEntry,
                                                   &pIpEvtInfo->
                                                   LnklocalIpv6Addr,
                                                   LDP_IPV6_ADDR_ADD) !=
                        LDP_SUCCESS)
                    {
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "LdpUpdateAddrIfTableEntry() Failed for entity = %d IfList-Index=%d\n",
                                  pLdpEntity->u4EntityIndex,
                                  LDP_ENTITY_IF_INDEX (pIfEntry));
                        return LDP_FAILURE;
                    }

                    LDP_DBG2 (LDP_SSN_PRCS,
                              "LdpUpdateAddrIfTableEntry() updated successfully for entity = %d IfList-Index=%d\n",
                              pLdpEntity->u4EntityIndex,
                              LDP_ENTITY_IF_INDEX (pIfEntry));

                    if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
                    {
                        if (ADDR6_SCOPE_LLOCAL == u1Scope)
                        {
                            if ((LDP_SET_NO_OF_ENT_V6MCASTCOUNT
                                 (LDP_CUR_INCARN,
                                  pIfEntry->u4IfIndex)) == LDP_ZERO)
                            {
                                MEMCPY (Ipv6IfAddr.u1_addr,
                                        pIfEntry->LnklocalIpv6Addr.u1_addr,
                                        LDP_IPV6ADR_LEN);
                                if (LDP_FAILURE !=
                                    INET_ATON6 (LDP_ALL_ROUTERS_V6ADDR,
                                                &V6MulticastAddr))
                                {
                                    if (LdpUdpJoinV6Multicast
                                        (i4Udp6SockFd, Ipv6IfAddr,
                                         V6MulticastAddr,
                                         pIfEntry->u4IfIndex) == LDP_FAILURE)
                                    {
                                        LDP_DBG1 (LDP_IF_MISC,
                                                  "IF: Multicast Join Returned Failure for V6 Addr %s \n",
                                                  Ip6PrintAddr (&(Ipv6IfAddr)));

                                        return LDP_FAILURE;
                                    }
                                    LDP_DBG1 (LDP_IF_MISC,
                                              "IPV6: Multicast Join Returned Success For V6 Addr %s\n",
                                              Ip6PrintAddr (&(Ipv6IfAddr)));

                                    LDP_SET_NO_OF_ENT_V6MCASTCOUNT
                                        (LDP_CUR_INCARN, pIfEntry->u4IfIndex)++;

                                }
                                else
                                {
                                    LDP_DBG (LDP_IF_MISC,
                                             "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
                                    return LDP_FAILURE;
                                }
                            }
                            else
                            {
                                LDP_DBG1 (LDP_IF_MISC,
                                          "IPV6: MCOUNT V6 not ZERO  for interface index %x\n",
                                          pIfEntry->u4IfIndex);
                                LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                                pIfEntry->
                                                                u4IfIndex)++;
                            }
                        }
                    }
                }
            }
        }
    }
    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        if ((LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE) &&
            (LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE))
        {
            /* No Need to Process the IF Change Event if Entity
             *                          ** is target type or not in active state */
            continue;
        }
        /** MSR case : In MSR IPV6 addresses are not restored fully when we set the IPV6 transport address via nmhSet API
         *  so we update the transport address via nmhset API but transport interface index would be populated here **/
        if ((pLdpEntity->u1Ipv6TransAddrTlvEnable == TRUE)
            && (pLdpEntity->u4Ipv6TransIfIndex == LDP_ZERO))
        {
            if (!
                (MEMCMP
                 (&pLdpEntity->Ipv6TransportAddress, &TempIpv6Addr,
                  LDP_IPV6ADR_LEN) == LDP_ZERO))
            {
                if ((LDP_ZERO ==
                     MEMCMP (&pLdpEntity->Ipv6TransportAddress,
                             &pIpEvtInfo->LnklocalIpv6Addr, LDP_IPV6ADR_LEN)))
                {
                    pLdpEntity->u4Ipv6TransIfIndex = pIpEvtInfo->u4IfIndex;
                    pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_TRUE;
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "ADDRADDCFAOPERUP: V6:bIsTransAddrIntfUp True TRANSADDR6: %s IfIndex=%x\n",
                              Ip6PrintAddr (&
                                            (pLdpEntity->Ipv6TransportAddress)),
                              pLdpEntity->u4TransIfIndex);
                }
            }
        }
        /** Update the Transport Address **/
        else if (pLdpEntity->u4Ipv6TransIfIndex == pIpEvtInfo->u4IfIndex)
        {
            if (LdpUpdateIpv6TransAddr4AddEvent
                (pLdpEntity, &pIpEvtInfo->LnklocalIpv6Addr,
                 pIpEvtInfo->u4IfIndex, LDP_IPV6_ADDR_ADD) != LDP_SUCCESS)
            {

                LDP_DBG2 (LDP_IF_MISC,
                          "LdpUpdateIpv6TransAddr4AddEvent() Failed for entity = %d Index=%d\n",
                          pLdpEntity->u4EntityIndex, pIpEvtInfo->u4IfIndex);
                return LDP_FAILURE;
            }

            LDP_DBG2 (LDP_IF_MISC,
                      "LdpUpdateIpv6TransAddr4AddEvent() success for entity = %d Index=%d\n",
                      pLdpEntity->u4EntityIndex, pIpEvtInfo->u4IfIndex);
        }
        TMO_DYN_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer,
                          pTempLdpPeer, tLdpPeer *)
        {
            if ((pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession) == NULL)
            {
                continue;
            }
            if (pLdpSession->u1SessionState == LDP_SSM_ST_OPERATIONAL)
            {

                if ((pSitelocalIpv6Addr != NULL)
                    || (pGlbUniqueIpv6Addr != NULL))
                {
                    LdpSendIpv6AddrMsg (pLdpSession, pLnklocalIpv6Addr,
                                        pSitelocalIpv6Addr, pGlbUniqueIpv6Addr);
                }
            }
        }
    }
    return LDP_SUCCESS;
}

UINT1
LdpUpdateIpv6TransAddr4AddEvent (tLdpEntity * pLdpEntity, tIp6Addr * pIp6Addr,
                                 UINT4 u4IfIndex, UINT4 u4Cmd)
{
    UINT1               u1Scope = 0;
    tIp6Addr            Ip6Addr;
    tIp6Addr           *pTempIp6Addr = NULL;
    tIp6Addr            LnklocalIpv6Addr;
    tIp6Addr            SitelocalIpv6Addr;
    tIp6Addr            GlbUniqueIpv6Addr;

    MEMSET (&Ip6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&LnklocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&SitelocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&GlbUniqueIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));

    u1Scope = Ip6GetAddrScope (pIp6Addr);

    switch (u4Cmd)
    {
        case LDP_IPV6_ADDR_ADD:
            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
            {
                if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
                {
                    if (u1Scope == ADDR6_SCOPE_GLOBAL)
                    {
                        /** Transport type only global is valid*/
                        if (MEMCMP
                            (&pLdpEntity->Ipv6TransportAddress, &Ip6Addr,
                             LDP_IPV6ADR_LEN) == 0)
                        {
                            MEMCPY (&pLdpEntity->Ipv6TransportAddress, pIp6Addr,
                                    LDP_IPV6ADR_LEN);
                        }
                        else
                        {
                            return LDP_FAILURE;
                        }
                    }
                    else
                    {
                        LDP_DBG (LDP_SSN_PRCS,
                                 "Targeted Entity Address Addition is not Global Scope type \n");
                    }
                }
                else
                {
                    if (u1Scope == ADDR6_SCOPE_GLOBAL)
                    {
                        MEMCPY (&pLdpEntity->Ipv6TransportAddress, pIp6Addr,
                                LDP_IPV6ADR_LEN);
                    }
                }
            }
            else
            {
                if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
                {
                    if ((u1Scope == ADDR6_SCOPE_GLOBAL) ||
                        (u1Scope == ADDR6_SCOPE_LLOCAL) ||
                        (u1Scope == ADDR6_SCOPE_SITELOCAL))
                    {
                        /** Transport type only global is valid*/
                        if (MEMCMP
                            (&pLdpEntity->Ipv6TransportAddress, &Ip6Addr,
                             LDP_IPV6ADR_LEN) == 0)
                        {
                            MEMCPY (&pLdpEntity->Ipv6TransportAddress, pIp6Addr,
                                    LDP_IPV6ADR_LEN);
                        }
                        else
                        {
                            LDP_DBG (LDP_SSN_PRCS,
                                     "Transport Addresss Type not added !!\n");
                        }
                    }
                }
                else
                {
                    if ((u1Scope == ADDR6_SCOPE_GLOBAL) ||
                        (u1Scope == ADDR6_SCOPE_LLOCAL) ||
                        (u1Scope == ADDR6_SCOPE_SITELOCAL))
                    {
                        /** get all the Available IPs + the IP came in the Address Add */
                        LdpGetIpv6IfAllAddr (u4IfIndex, &LnklocalIpv6Addr,
                                             &SitelocalIpv6Addr,
                                             &GlbUniqueIpv6Addr);
                        /** get the priority **/
                        pTempIp6Addr =
                            LdpGetBasicIpv6TransAddrPref (&LnklocalIpv6Addr,
                                                          &SitelocalIpv6Addr,
                                                          &GlbUniqueIpv6Addr);
                        /** update the transport address **/
                        MEMCPY (&pLdpEntity->Ipv6TransportAddress, pTempIp6Addr,
                                LDP_IPV6ADR_LEN);
                    }

                }
            }
            break;
        case LDP_IPV6_ADDR_DEL:
            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
            {
                if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
                {
                    if (u1Scope == ADDR6_SCOPE_GLOBAL)
                    {
                        /** Transport type only global is valid*/
                        if (MEMCMP
                            (&pLdpEntity->Ipv6TransportAddress, &Ip6Addr,
                             LDP_IPV6ADR_LEN) != 0)
                        {
                            MEMSET (&pLdpEntity->Ipv6TransportAddress, LDP_ZERO,
                                    LDP_IPV6ADR_LEN);
                        }
                        else
                        {
                            return LDP_FAILURE;
                        }
                    }
                    else
                    {
                        LDP_DBG2 (LDP_SSN_PRCS,
                                  "%s:%d Targeted Entity Address Addition is not Global Scope type !!\n",
                                  __func__, __LINE__);
                    }
                }
                else
                {
                    if (u1Scope == ADDR6_SCOPE_GLOBAL)
                    {
                        MEMSET (&pLdpEntity->Ipv6TransportAddress, LDP_ZERO,
                                LDP_IPV6ADR_LEN);
                    }
                }
            }
            else
            {
                if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
                {
                    if ((u1Scope == ADDR6_SCOPE_GLOBAL) ||
                        (u1Scope == ADDR6_SCOPE_LLOCAL) ||
                        (u1Scope == ADDR6_SCOPE_SITELOCAL))
                    {
                        /** Transport type only global is valid*/
                        if (MEMCMP
                            (&pLdpEntity->Ipv6TransportAddress, &Ip6Addr,
                             LDP_IPV6ADR_LEN) != 0)
                        {
                            /** TODO: Need to check, 
                              i think we need to remove the session from here **/
                            MEMSET (&pLdpEntity->Ipv6TransportAddress, LDP_ZERO,
                                    LDP_IPV6ADR_LEN);
                            /** get all the Available IPs + the IP came in the Address Add */
                            LdpGetIpv6IfAllAddr ((INT4) u4IfIndex,
                                                 &LnklocalIpv6Addr,
                                                 &SitelocalIpv6Addr,
                                                 &GlbUniqueIpv6Addr);
                            if (u1Scope == ADDR6_SCOPE_LLOCAL)
                            {
                                MEMSET (&LnklocalIpv6Addr, LDP_ZERO,
                                        (sizeof (tIp6Addr)));
                            }
                            else if (u1Scope == ADDR6_SCOPE_SITELOCAL)
                            {
                                MEMSET (&SitelocalIpv6Addr, LDP_ZERO,
                                        sizeof (tIp6Addr));
                            }
                            else if (u1Scope == ADDR6_SCOPE_GLOBAL)
                            {
                                MEMSET (&GlbUniqueIpv6Addr, LDP_ZERO,
                                        sizeof (tIp6Addr));
                            }
                            /** get the priority **/
                            pTempIp6Addr =
                                LdpGetBasicIpv6TransAddrPref (&LnklocalIpv6Addr,
                                                              &SitelocalIpv6Addr,
                                                              &GlbUniqueIpv6Addr);
                            /** update the transport address **/
                            MEMCPY (&pLdpEntity->Ipv6TransportAddress,
                                    pTempIp6Addr, LDP_IPV6ADR_LEN);
                        }
                        else
                        {
                            LDP_DBG2 (LDP_SSN_PRCS,
                                      "%s:%d Transport Addresss Type not added",
                                      __func__, __LINE__);
                        }
                    }
                }
                else
                {
                    if ((u1Scope == ADDR6_SCOPE_GLOBAL) ||
                        (u1Scope == ADDR6_SCOPE_LLOCAL) ||
                        (u1Scope == ADDR6_SCOPE_SITELOCAL))
                    {
                        /** get all the Available IPs + the IP came in the Address Add */
                        LdpGetIpv6IfAllAddr ((INT4) u4IfIndex,
                                             &LnklocalIpv6Addr,
                                             &SitelocalIpv6Addr,
                                             &GlbUniqueIpv6Addr);
                        if (u1Scope == ADDR6_SCOPE_LLOCAL)
                        {
                            MEMSET (&LnklocalIpv6Addr, LDP_ZERO,
                                    (sizeof (tIp6Addr)));
                        }
                        else if (u1Scope == ADDR6_SCOPE_SITELOCAL)
                        {
                            MEMSET (&SitelocalIpv6Addr, LDP_ZERO,
                                    sizeof (tIp6Addr));
                        }
                        else if (u1Scope == ADDR6_SCOPE_GLOBAL)
                        {
                            MEMSET (&GlbUniqueIpv6Addr, LDP_ZERO,
                                    sizeof (tIp6Addr));
                        }
                        /** get the priority **/
                        pTempIp6Addr =
                            LdpGetBasicIpv6TransAddrPref (&LnklocalIpv6Addr,
                                                          &SitelocalIpv6Addr,
                                                          &GlbUniqueIpv6Addr);
                        /** update the transport address **/
                        MEMCPY (&pLdpEntity->Ipv6TransportAddress, pTempIp6Addr,
                                LDP_IPV6ADR_LEN);
                    }
                }
            }
            break;

    };
    return LDP_SUCCESS;
}

UINT1
LdpUpdateAddrIfTableEntry (tLdpIfTableEntry * pIfEntry, tIp6Addr * pIp6Addr,
                           UINT4 u4Cmd)
{
    UINT1               u1Scope = 0;
    tIpv4Addr           TempIfAddr;
    tIp6Addr            Ip6Addr;
    tIp6Addr            LnklocalIpv6Addr;
    tIp6Addr            SitelocalIpv6Addr;
    tIp6Addr            GlbUniqueIpv6Addr;

    MEMSET (&Ip6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&LnklocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&SitelocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&GlbUniqueIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&TempIfAddr, LDP_ZERO, sizeof (tIpv4Addr));

    LDP_DBG (LDP_IF_MISC, "Entered LdpUpdateAddrIfTableEntry\n");

    u1Scope = Ip6GetAddrScope (pIp6Addr);
    switch (u4Cmd)
    {
        case LDP_IPV6_ADDR_ADD:
            switch (u1Scope)
            {
                case ADDR6_SCOPE_LLOCAL:
                    if (MEMCMP (&pIfEntry->LnklocalIpv6Addr, &Ip6Addr,
                                LDP_IPV6ADR_LEN) != LDP_ZERO)
                    {
                        LDP_DBG (LDP_IF_MISC,
                                 "LDP_IPV6_ADDR_ADD: ADDR6_SCOPE_LLOCAL Failed\n");
                        return LDP_FAILURE;
                    }
                    MEMCPY (&pIfEntry->LnklocalIpv6Addr, pIp6Addr,
                            sizeof (tIp6Addr));
                    break;
                case ADDR6_SCOPE_SITELOCAL:
                    if (MEMCMP (&pIfEntry->SitelocalIpv6Addr, &Ip6Addr,
                                LDP_IPV6ADR_LEN) != LDP_ZERO)
                    {
                        LDP_DBG (LDP_IF_MISC,
                                 "LDP_IPV6_ADDR_ADD: ADDR6_SCOPE_SITELOCAL Failed\n");
                        return LDP_FAILURE;
                    }
                    MEMCPY (&pIfEntry->SitelocalIpv6Addr, pIp6Addr,
                            sizeof (tIp6Addr));
                    break;
                case ADDR6_SCOPE_GLOBAL:
                    if (MEMCMP (&pIfEntry->GlbUniqueIpv6Addr, &Ip6Addr,
                                LDP_IPV6ADR_LEN) != LDP_ZERO)
                    {
                        LDP_DBG (LDP_IF_MISC,
                                 "LDP_IPV6_ADDR_ADD: ADDR6_SCOPE_GLOBAL Failed\n");
                        return LDP_FAILURE;
                    }
                    MEMCPY (&pIfEntry->GlbUniqueIpv6Addr, pIp6Addr,
                            sizeof (tIp6Addr));
                    break;
                default:
                    break;
            };
            break;
        case LDP_IPV6_ADDR_DEL:
            switch (u1Scope)
            {
                case ADDR6_SCOPE_LLOCAL:
                    if (MEMCMP (&pIfEntry->LnklocalIpv6Addr, pIp6Addr,
                                LDP_IPV6ADR_LEN) != LDP_ZERO)
                    {
                        LDP_DBG (LDP_IF_MISC,
                                 "LDP_IPV6_ADDR_DEL: ADDR6_SCOPE_LLOCAL Failed\n");
                        return LDP_FAILURE;
                    }
                    MEMCPY (&pIfEntry->LnklocalIpv6Addr, &Ip6Addr,
                            sizeof (tIp6Addr));
                    break;
                case ADDR6_SCOPE_SITELOCAL:
                    if (MEMCMP (&pIfEntry->SitelocalIpv6Addr, pIp6Addr,
                                LDP_IPV6ADR_LEN) != LDP_ZERO)
                    {
                        LDP_DBG (LDP_IF_MISC,
                                 "LDP_IPV6_ADDR_DEL: ADDR6_SCOPE_SITELOCAL Failed\n");
                        return LDP_FAILURE;
                    }
                    MEMCPY (&pIfEntry->SitelocalIpv6Addr, &Ip6Addr,
                            sizeof (tIp6Addr));
                    break;
                case ADDR6_SCOPE_GLOBAL:
                    if (MEMCMP (&pIfEntry->GlbUniqueIpv6Addr, pIp6Addr,
                                LDP_IPV6ADR_LEN) != LDP_ZERO)
                    {
                        LDP_DBG (LDP_IF_MISC,
                                 "LDP_IPV6_ADDR_DEL: ADDR6_SCOPE_GLOBAL Failed\n");
                        return LDP_FAILURE;
                    }
                    MEMCPY (&pIfEntry->GlbUniqueIpv6Addr, &Ip6Addr,
                            sizeof (tIp6Addr));
                    break;
                default:
                    break;
            };

            break;
        default:

            break;
    };
    /*Updating the interface type. This chunk will be same for both event */
    LdpUpdateInterfaceType (u4Cmd, pIfEntry);
    LDP_DBG4 (LDP_SSN_PRCS,
              "LdpUpdateIfTbleEntry:Interface Table Updated with TYPE: %d \n LLAdd: %s\n SLAdd: %s \n GAdd:%s\n",
              pIfEntry->u1InterfaceType,
              Ip6PrintAddr (&pIfEntry->LnklocalIpv6Addr),
              Ip6PrintAddr (&pIfEntry->SitelocalIpv6Addr),
              Ip6PrintAddr (&pIfEntry->GlbUniqueIpv6Addr));

    return LDP_SUCCESS;
}

UINT1
LdpProcessAddrDelEvent (tLdpIpEvtInfo * pIpEvtInfo)
{
    UINT2               u2IncarnId = (UINT2) pIpEvtInfo->u4IncarnId;
    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    tTMO_SLL           *pAdjList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpPeer           *pTempLdpPeer = NULL;
    tLdpIfTableEntry   *pIfEntry = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLdpAdjacency      *pLdpAdj = NULL;
    tLdpAdjacency      *pTempAdj = NULL;
    tLdpAdjacency      *pTempAdjEntry = NULL;
    INT4                i4RetVal = 0;
    INT4                u4Udp6SockFd = LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
    UINT4               u4TempAdjCount = 0;
    UINT4               u4IfIndex = pIpEvtInfo->u4IfIndex;
    BOOL1               bSsnDeleted = FALSE;
    tIp6Addr            TempIpv6Addr;
    tIp6Addr            V6IfAddr;
    tIp6Addr            AllRouterAddr;
    tIp6Addr           *pLnklocalIpv6Addr = NULL;
    tIp6Addr           *pSitelocalIpv6Addr = NULL;
    tIp6Addr           *pGlbUniqueIpv6Addr = NULL;
    UINT1               u1Scope = 0;
    UINT1               u1ValidRouterAddr = LDP_FALSE;
    tCfaIfInfo          CfaIfInfo;
    MEMSET (&TempIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&V6IfAddr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&AllRouterAddr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&CfaIfInfo, LDP_ZERO, sizeof (tCfaIfInfo));

    u1Scope = Ip6GetAddrScope (&pIpEvtInfo->LnklocalIpv6Addr);

    LDP_DBG2 (LDP_SSN_PRCS, "%s Recieved Addr Del Event for Index=%d\n",
              __FUNCTION__, pIpEvtInfo->u4IfIndex);

    PrintInterfaceAddr (pIpEvtInfo);
    if (LDP_FAILURE != INET_ATON6 (LDP_ALL_ROUTERS_V6ADDR, &AllRouterAddr))
    {
        u1ValidRouterAddr = LDP_TRUE;
    }

    if (CfaGetIfInfo (pIpEvtInfo->u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        LDP_DBG3 (LDP_IF_MISC, "%s : %d CfaGetIfInfo() failed for Index=%d\n",
                  __FUNCTION__, __LINE__, pIpEvtInfo->u4IfIndex);
        return LDP_FAILURE;

    }

    if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
    {
        /** Entity Interface List Handling **/
        TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
        {
            i4RetVal = LdpGetLdpEntityIfEntry (pIpEvtInfo->u4IncarnId,
                                               pLdpEntity,
                                               pIpEvtInfo->u4IfIndex,
                                               &pIfEntry);

            if (i4RetVal == LDP_SUCCESS)
            {
                if (pIfEntry->u1OperStatus == LDP_IF_OPER_ENABLE)
                {
                    if (LdpUpdateAddrIfTableEntry (pIfEntry,
                                                   &pIpEvtInfo->
                                                   LnklocalIpv6Addr,
                                                   LDP_IPV6_ADDR_DEL) !=
                        LDP_SUCCESS)
                    {
                        LDP_DBG2 (LDP_IF_MISC,
                                  "LdpUpdateAddrIfTableEntry() Failed for entity = %d IfList-Index=%d\n",
                                  pLdpEntity->u4EntityIndex,
                                  LDP_ENTITY_IF_INDEX (pIfEntry));
                        return LDP_FAILURE;
                    }

                    LDP_DBG2 (LDP_IF_MISC,
                              "LdpUpdateAddrIfTableEntry() Updated Success for entity = %d IfList-Index=%d\n",
                              pLdpEntity->u4EntityIndex,
                              LDP_ENTITY_IF_INDEX (pIfEntry));

                    /** Disjoin Multicast **/
                    if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
                    {
                        if (ADDR6_SCOPE_LLOCAL == u1Scope)
                        {
                            MEMCPY (V6IfAddr.u1_addr,
                                    pIfEntry->LnklocalIpv6Addr.u1_addr,
                                    LDP_IPV6ADR_LEN);
                            if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT
                                (LDP_CUR_INCARN,
                                 pIfEntry->u4IfIndex) > LDP_ZERO)
                            {
                                --(LDP_SET_NO_OF_ENT_V6MCASTCOUNT
                                   (LDP_CUR_INCARN, pIfEntry->u4IfIndex));
                            }
                            if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                                pIfEntry->
                                                                u4IfIndex) ==
                                LDP_ZERO)
                            {
                                if (LDP_TRUE == u1ValidRouterAddr)
                                {
                                    if (LdpUdpLeaveV6Multicast
                                        ((INT4) u4Udp6SockFd, V6IfAddr,
                                         &AllRouterAddr,
                                         pIfEntry->u4IfIndex) == LDP_FAILURE)
                                    {
                                        /* Error: Iface failed to leave the Mcast group */
                                        LDP_DBG (LDP_IF_MISC,
                                                 "IPV6: Leave V6 Multicast Returned Failure\n");
                                    }

                                    LDP_DBG (LDP_IF_MISC,
                                             "IPV6: Leave V6 Multicast Returned Success\n");
                                }
                                else
                                {
                                    LDP_DBG (LDP_IF_MISC,
                                             "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        if ((LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE) &&
            (LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE))
        {
            /* No Need to Process the IF Change Event if Entity
             *                          ** is target type or not in active state */
            continue;
        }
        TMO_DYN_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer,
                          pTempLdpPeer, tLdpPeer *)
        {
            if ((pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession) == NULL)
            {
                continue;
            }

            if ((LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE) &&
                (TMO_SLL_Count (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity)) >
                 LDP_ONE))
            {
                LDP_DBG (LDP_DBG_PRCS,
                         "LDP targeted session deletion skipped\n");
                continue;
            }
            /* Handling Adjacency Expiry */
            TMO_DYN_SLL_Scan (&(pLdpSession->AdjacencyList),
                              pLdpAdj, pTempAdj, tLdpAdjacency *)
            {
                if ((pLdpAdj->u4IfIndex != u4IfIndex)
                    || (pLdpAdj->u1AddrType != LDP_ADDR_TYPE_IPV6))
                {
                    continue;
                }
                u4TempAdjCount = 0;
                pAdjList = NULL;
                pAdjList = ADJ_GET_ADJ_LIST (pLdpAdj);
                TMO_SLL_Scan (pAdjList, pTempAdjEntry, tLdpAdjacency *)
                {
                    if (u4TempAdjCount > 1)
                    {
                        break;
                    }
                    if (pLdpAdj->u1AddrType == pTempAdjEntry->u1AddrType)
                    {
                        u4TempAdjCount++;
                    }
                }
                if (u4TempAdjCount == LDP_ONE)
                {
                    bSsnDeleted = TRUE;
                }

                LDP_DBG4 (LDP_SSN_PRCS,
                          "The Ldp Peer %d.%d.%d.%d is down\n",
                          pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                          pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                          pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                          pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);

                if (LdpHandleAdjExpiry (pLdpAdj, LDP_STAT_TYPE_SHUT_DOWN) ==
                    LDP_FAILURE)
                {
                    MEMSET (&pLdpPeer->NetAddr, LDP_ZERO, LDP_NET_ADDR_LEN);
                }
                /*No need to traverse Session Adj List as Adjacency Attached has been expired.
                   And only one Adjacency (as checking adjacency type also) is possible with passed interface index */
                break;
            }
            if (bSsnDeleted == TRUE)
            {
                continue;
            }
            /*Sending Address Withdraw Msg  Session is not expected to be deleted as Adjacency is available */
            LdpSendIpv6AddrWithdrawMsg (pLdpSession, pLnklocalIpv6Addr,
                                        pSitelocalIpv6Addr, pGlbUniqueIpv6Addr);
        }
                /** Update the Transport Address **/
        if (pLdpEntity->u4Ipv6TransIfIndex == pIpEvtInfo->u4IfIndex)
        {
            if (LdpUpdateIpv6TransAddr4AddEvent
                (pLdpEntity, &pIpEvtInfo->LnklocalIpv6Addr,
                 pIpEvtInfo->u4IfIndex, LDP_IPV6_ADDR_DEL) != LDP_SUCCESS)
            {
                LDP_DBG2 (LDP_IF_MISC,
                          "LdpUpdateIpv6TransAddr4AddEvent() Updation Failed for entity = %d TransIfIndex=%d\n",
                          pLdpEntity->u4EntityIndex, pIpEvtInfo->u4IfIndex);
                return LDP_FAILURE;
            }

            LDP_DBG2 (LDP_IF_MISC,
                      "LdpUpdateIpv6TransAddr4AddEvent() Updated Success for entity = %d TransIfIndex=%d\n",
                      pLdpEntity->u4EntityIndex, pIpEvtInfo->u4IfIndex);
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpUpdateInterfaceType                                  */
/* Description   : This routine updates the interface type if Address add/del event receivedl*/
/* Input(s)      : u4Cmd: Addr Add or Del                                                      */
/*             :pIfEntry: Pointer to interface for which Addr Add/Del event is received */
/* Output(s)     : Updates the interface entry with correct Interface Type                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpUpdateInterfaceType (UINT4 u4Cmd, tLdpIfTableEntry * pIfEntry)
{
    switch (u4Cmd)
    {
        case LDP_IPV6_ADDR_ADD:
            if (pIfEntry->u1InterfaceType == LDP_IFTYPE_IPV4)
            {
                pIfEntry->u1InterfaceType = LDP_IFTYPE_DUAL;
            }
            if (pIfEntry->u1InterfaceType == LDP_IFTYPE_INVALID)
            {
                pIfEntry->u1InterfaceType = LDP_IFTYPE_IPV6;
            }
            break;
        case LDP_IPV6_ADDR_DEL:
            if (LdpInterfaceIpv6Match (&pIfEntry->LnklocalIpv6Addr,
                                       &pIfEntry->SitelocalIpv6Addr,
                                       &pIfEntry->GlbUniqueIpv6Addr) ==
                LDP_FALSE)
            {
                if (pIfEntry->u1InterfaceType == LDP_IFTYPE_DUAL)
                {
                    pIfEntry->u1InterfaceType = LDP_IFTYPE_IPV4;
                }
                if (pIfEntry->u1InterfaceType == LDP_IFTYPE_IPV6)
                {
                    pIfEntry->u1InterfaceType = LDP_IFTYPE_INVALID;
                }
            }
            break;
    }
}
#endif

UINT1
LdpProcessCfaOperUpEvent (tLdpIpEvtInfo * pIpEvtInfo)
{
    UINT4               u4UnnumAssocIPIf = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4TempIpAddr = 0;
    UINT1               u1InterfaceType = 0;
    UINT2               u2IncarnId = (UINT2) pIpEvtInfo->u4IncarnId;
    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    BOOL1               b1IsGetMplsIfReqd = TRUE;
    INT4                i4RetVal = LDP_FAILURE;
    tLdpIfTableEntry   *pIfEntry = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT4               u4MplsIfIndex = LDP_ZERO;
    BOOL1               bIsEntityPresent = LDP_FALSE;
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    tCfaIfInfo          CfaIfInfo;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    u4IfIndex = pIpEvtInfo->u4IfIndex;
#ifdef MPLS_IPV6_WANTED
    LDP_DBG6 ((LDP_IF_MISC | LDP_IF_ALL),
              "\r LdpProcessCfaOperUpEvent: u4BitMap = %d, u4IfIndex= %d, Ip4Add = 0%x, LL = %s, SL = %s, GAdd = %s \n",
              pIpEvtInfo->u4BitMap, u4IfIndex, pIpEvtInfo->u4IpAddr,
              Ip6PrintAddr (&(pIpEvtInfo->LnklocalIpv6Addr)),
              Ip6PrintAddr (&(pIpEvtInfo->SitelocalIpv6Addr)),
              Ip6PrintAddr (&(pIpEvtInfo->GlbUniqueIpv6Addr)));
#else
    LDP_DBG3 ((LDP_IF_MISC | LDP_IF_ALL),
              "\r LdpProcessCfaOperUpEvent: u4BitMap = %d, u4IfIndex= %d, Ip4Add = 0%x\n",
              pIpEvtInfo->u4BitMap, u4IfIndex, pIpEvtInfo->u4IpAddr);
#endif

    PrintInterfaceAddr (pIpEvtInfo);
    /*** LSR-ID change Handling **/
    MEMCPY ((UINT1 *) &u4IpAddr,
            (UINT1 *) (LDP_LSR_ID (u2IncarnId).au1Ipv4Addr), LDP_LSR_ID_LEN);
    u4IpAddr = OSIX_NTOHL (u4IpAddr);
    MEMCPY ((UINT1 *) &u4TempIpAddr,
            (LDP_NEW_LSR_ID (u2IncarnId).au1Ipv4Addr), LDP_LSR_ID_LEN);
    u4TempIpAddr = OSIX_NTOHL (u4TempIpAddr);
    /*Since interface type is derived on the basis of IP Addresses receivd,There may be a case 
       for unnumbered interfaces when the IP ADDR received is zero that interface is also treated as IPV4 Type */
    CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4UnnumAssocIPIf);
    LDP_DBG3 (LDP_IF_PRCS,
              "LdpProcessCfaOperUpEvent:UnnumAssocIpIf is % d, Ip4Add is %x, IfIndex is %d \n",
              u4UnnumAssocIPIf, pIpEvtInfo->u4IpAddr, u4IfIndex);
    if ((pIpEvtInfo->u4IpAddr != LDP_ZERO) || (LDP_ZERO != u4UnnumAssocIPIf))
    {
        u1InterfaceType = LDP_IFTYPE_IPV4;
    }
#ifdef MPLS_IPV6_WANTED
    if (LdpInterfaceIpv6Match (&pIpEvtInfo->LnklocalIpv6Addr,
                               &pIpEvtInfo->SitelocalIpv6Addr,
                               &pIpEvtInfo->GlbUniqueIpv6Addr) == TRUE)
    {
        u1InterfaceType = u1InterfaceType | LDP_IFTYPE_IPV6;
    }
#endif

    /* If all the entities are made down through OPER_DISABLE case  */
    /*checking in LSR ID is attached to some interface
       bIsEntityOperUp=LdpGetIfAllEntityDownViaIntOperDown((UINT2)pIpEvtInfo->u4IncarnId); */

    /*If Entity is present, it means entity is using LSR_ID, hence 
     * retrigger should be done in the case there is no Entity present
     * i.e LSR_ID is not in use*/
    pLdpEntity = (tLdpEntity *) TMO_SLL_First (&LDP_ENTITY_LIST (u2IncarnId));
    if (pLdpEntity != NULL)
    {
        bIsEntityPresent = LDP_TRUE;
    }

    /* LSR ID is not attached to any interface or Same IP for which Up Event received is same as of LSR ID then Retrigger wouldnot happen */
    /*if ((NetIpv4IfIsOurAddress (u4IpAddr) == NETIPV4_FAILURE) || ((u4IpAddr == pIpEvtInfo->u4IpAddr) && (bIsEntityOperUp == LDP_TRUE))) */
    if ((NetIpv4IfIsOurAddress (u4IpAddr) == NETIPV4_FAILURE)
        && (u4IpAddr != pIpEvtInfo->u4IpAddr))
    {
        LDP_DBG (LDP_SSN_PRCS,
                 " Current LSR Id is not attahced to any Interface Or Same Ip as of LSR ID came up\n");
    }
#ifdef MPLS_IPV6_WANTED
    /*LSR ID is attached to the interface and First Interface came up */
    else if ((LDP_TRUE == LDP_LAST_V4IF_DOWN (u2IncarnId))
             && (pIpEvtInfo->u4IpAddr != LDP_ZERO))
    {
        LDP_DBG (LDP_SSN_PRCS,
                 "First V4 Interface came up and LSR ID was attached to the interface, Retrigger the Entities \n");
        LdpRetriggerEntitySessions (OSIX_HTONL (pIpEvtInfo->u4IpAddr),
                                    u4IfIndex,
                                    OSIX_HTONL (pIpEvtInfo->u4IpAddr),
                                    LDP_IF_OPER_ENABLE);
        LDP_LAST_V4IF_DOWN (u2IncarnId) = LDP_FALSE;
    }
#endif
    /* Retrigger the Entitysession if the router-id was configured with
     * force option and was attached to the interface and that interface went down due to which LSR ID got changed earlier
     and now that interface came up */
    else if ((pIpEvtInfo->u4IpAddr != LDP_ZERO) &&
             (LDP_FORCE_OPTION (MPLS_DEF_INCARN) == LDP_SNMP_TRUE) &&
             (u4TempIpAddr == pIpEvtInfo->u4IpAddr))
    {
        LDP_DBG2 (LDP_SSN_PRCS, "%s: %d\n", __func__, __LINE__);
        LdpRetriggerEntitySessions (OSIX_HTONL (u4TempIpAddr), u4IfIndex,
                                    OSIX_HTONL (u4TempIpAddr),
                                    LDP_IF_OPER_ENABLE);
    }
    /* Retrigger the Entity Session irrespective of FORCE OPTION if no active entity available and valid interface came up  */
    else if ((bIsEntityPresent == LDP_FALSE)
             && (pIpEvtInfo->u4IpAddr != LDP_ZERO))
    {
        LdpRetriggerEntitySessions (OSIX_HTONL (pIpEvtInfo->u4IpAddr),
                                    u4IfIndex,
                                    OSIX_HTONL (pIpEvtInfo->u4IpAddr),
                                    LDP_IF_OPER_ENABLE);

        LDP_DBG2 (LDP_SSN_PRCS, "%s: %d\n", __func__, __LINE__);
    }
    if (CfaGetIfInfo (pIpEvtInfo->u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        LDP_DBG3 (LDP_SSN_PRCS, " %s : %d CfaGetIfInfo() failed for Index=%d\n",
                  __FUNCTION__, __LINE__, pIpEvtInfo->u4IfIndex);
    }
    if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
    {
        /** Entity Interface List Handling **/
        TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
        {
            if ((b1IsGetMplsIfReqd == TRUE) &&
                (CfaUtilGetMplsIfFromIfIndex (pIpEvtInfo->u4IfIndex,
                                              &u4MplsIfIndex, TRUE)
                 == CFA_FAILURE))
            {
                break;
            }

            b1IsGetMplsIfReqd = FALSE;

            i4RetVal = LdpGetLdpEntityIfEntry (pIpEvtInfo->u4IncarnId,
                                               pLdpEntity,
                                               pIpEvtInfo->u4IfIndex,
                                               &pIfEntry);

            if (((pLdpEntity->bIsIntfMapped == LDP_FALSE) &&
                 (i4RetVal == LDP_SUCCESS)) ||
                ((pLdpEntity->bIsIntfMapped == LDP_TRUE) &&
                 (i4RetVal == LDP_SUCCESS)))
            {
                /** If the entry is user configured, do not
                 ** overwrite the configurations. Just continue
                 ** with the next entry.
                 **/
                switch (u1InterfaceType)
                {
                    case LDP_IFTYPE_IPV4:
                        LdpUpdateLdpIfTableEntry (pIfEntry, u1InterfaceType,
                                                  CFA_IF_UP,
                                                  pIpEvtInfo->u4IpAddr);
                        if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE
                            && LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) ==
                            LDP_FALSE)
                        {
                            if (LdpAddrAddUdpJoinMulticast
                                (pIfEntry, u1InterfaceType) != LDP_SUCCESS)
                            {
                                LDP_DBG2 (LDP_SSN_PRCS,
                                          "LdpAddrAddUdpJoinMulticast() Failed for Entity=%d IfList-Index=%d\n",
                                          pLdpEntity->u4EntityIndex,
                                          LDP_ENTITY_IF_INDEX (pIfEntry));
                                return LDP_FAILURE;
                            }
                        }
                        break;
#ifdef MPLS_IPV6_WANTED
                    case LDP_IFTYPE_IPV6:
                    case LDP_IFTYPE_DUAL:
                        LdpUpdateLdpIpv6IfTableEntry (pIfEntry, CFA_IF_UP,
                                                      u1InterfaceType,
                                                      pIpEvtInfo->u4IpAddr,
                                                      &pIpEvtInfo->
                                                      LnklocalIpv6Addr,
                                                      &pIpEvtInfo->
                                                      SitelocalIpv6Addr,
                                                      &pIpEvtInfo->
                                                      GlbUniqueIpv6Addr);
                        if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE
                            && LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) ==
                            LDP_FALSE)
                        {
                            if (LdpAddrAddUdpJoinMulticast
                                (pIfEntry, u1InterfaceType) != LDP_SUCCESS)
                            {
                                LDP_DBG2 (LDP_SSN_PRCS,
                                          "LdpAddrAddUdpJoinMulticast() Failed for Entity=%d IfList-Index=%d\n",
                                          pLdpEntity->u4EntityIndex,
                                          LDP_ENTITY_IF_INDEX (pIfEntry));
                                return LDP_FAILURE;
                            }
                        }
                        break;
#endif
                };
            }
            else if ((TMO_SLL_Count (LDP_ENTITY_ETH_LBL_RNGE_LIST
                                     (pLdpEntity)) != LDP_ZERO)
                     && (pLdpEntity->bIsIntfMapped == LDP_FALSE))
            {
                /** Associate the index to the LDP entity when
                 ** there is a label range configured without
                 ** interface association.
                 **/
                switch (u1InterfaceType)
                {
                    case LDP_IFTYPE_IPV4:
                        pLdpIfTableEntry =
                            LdpCreateLdpIfTableEntry (pIpEvtInfo->u4IfIndex,
                                                      CFA_IF_UP, pLdpEntity,
                                                      pIpEvtInfo->u4IpAddr);
                        if (pLdpIfTableEntry == NULL)
                        {
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "LdpCreateLdpIfTableEntry() return NULL for Index=%d\n",
                                      pIpEvtInfo->u4IfIndex);
                            return LDP_FAILURE;
                        }

                        if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE &&
                            LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_FALSE)
                        {
                            if (LdpAddrAddUdpJoinMulticast
                                (pLdpIfTableEntry,
                                 u1InterfaceType) != LDP_SUCCESS)
                            {
                                LDP_DBG2 (LDP_SSN_PRCS,
                                          "LdpAddrAddUdpJoinMulticast() Failed for Entity=%d IfList-Index=%d\n",
                                          pLdpEntity->u4EntityIndex,
                                          LDP_ENTITY_IF_INDEX
                                          (pLdpIfTableEntry));
                                return LDP_FAILURE;
                            }
                        }
                        break;
#ifdef MPLS_IPV6_WANTED
                    case LDP_IFTYPE_IPV6:
                    case LDP_IFTYPE_DUAL:
                        pLdpIfTableEntry =
                            LdpCreateLdpIpv6IfTableEntry (pIpEvtInfo->u4IfIndex,
                                                          CFA_IF_UP, pLdpEntity,
                                                          u1InterfaceType,
                                                          pIpEvtInfo->u4IpAddr,
                                                          &pIpEvtInfo->
                                                          LnklocalIpv6Addr,
                                                          &pIpEvtInfo->
                                                          SitelocalIpv6Addr,
                                                          &pIpEvtInfo->
                                                          GlbUniqueIpv6Addr);

                        if (pLdpIfTableEntry == NULL)
                        {
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "LdpCreateLdpIfTableEntry() return NULL for Index=%d\n",
                                      pIpEvtInfo->u4IfIndex);
                            return LDP_FAILURE;
                        }
                        if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE &&
                            LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_FALSE)
                        {
                            if (LdpAddrAddUdpJoinMulticast
                                (pLdpIfTableEntry,
                                 u1InterfaceType) != LDP_SUCCESS)
                            {

                                LDP_DBG2 (LDP_SSN_PRCS,
                                          "LdpAddrAddUdpJoinMulticast() Failed for Entity=%d IfList-Index=%d\n",
                                          pLdpEntity->u4EntityIndex,
                                          LDP_ENTITY_IF_INDEX
                                          (pLdpIfTableEntry));
                                return LDP_FAILURE;
                            }
                        }

#endif
                        break;
                };
            }
        }
    }

    if (LdpAddrAddCfaOperUp (pIpEvtInfo, u1InterfaceType) != LDP_SUCCESS)
    {
        LDP_DBG (LDP_SSN_PRCS, "LdpAddrAddCfaOperUp() Failed");
        return LDP_FAILURE;
    }
    LDP_DBG1 (LDP_SSN_PRCS, "%s: EXIT", __func__);
    return LDP_SUCCESS;
}

UINT1
LdpProcessCfaOperDownEvent (tLdpIpEvtInfo * pIpEvtInfo)
{
    UINT2               u2IncarnId = (UINT2) pIpEvtInfo->u4IncarnId;
    UINT4               u4UnnumAssocIPIf = 0;
    UINT4               u4IpAddr = 0;
    UINT4               u4NvRamIpAddr = LDP_ZERO;
    UINT4               u4TempIpAddr = 0;
    UINT4               u4IfIndex = 0;
    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    UINT1               u1InterfaceType = 0;
#ifdef MPLS_IPV6_WANTED
    UINT4               u4IfListAddr = 0;
    BOOL1               bIsIpv6LastInterface = LDP_TRUE;
#endif
    tIp6Addr            LnklocalIpv6Addr;
    tIp6Addr            SitelocalIpv6Addr;
    tIp6Addr            GlbUniqueIpv6Addr;
    tLdpEntity         *pLdpEntity = NULL;
    INT4                i4RetVal = 0;
    tLdpIfTableEntry   *pIfEntry = NULL;
    tCfaIfInfo          CfaIfInfo;

    MEMSET (&LnklocalIpv6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&SitelocalIpv6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&GlbUniqueIpv6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    u4IfIndex = pIpEvtInfo->u4IfIndex;
#ifdef MPLS_IPV6_WANTED
    LDP_DBG6 ((LDP_IF_MISC | LDP_IF_ALL),
              "\r LdpProcessCfaOperDownEvent: u4BitMap = %d, u4IfIndex= %d, Ip4Add = 0%x, LL = %s, SL = %s, GAdd = %s \n",
              pIpEvtInfo->u4BitMap, u4IfIndex, pIpEvtInfo->u4IpAddr,
              Ip6PrintAddr (&(pIpEvtInfo->LnklocalIpv6Addr)),
              Ip6PrintAddr (&(pIpEvtInfo->SitelocalIpv6Addr)),
              Ip6PrintAddr (&(pIpEvtInfo->GlbUniqueIpv6Addr)));
#else
    LDP_DBG3 ((LDP_IF_MISC | LDP_IF_ALL),
              "\r LdpProcessCfaOperDownEvent: u4BitMap = %d, u4IfIndex= %d, Ip4Add = 0%x\n",
              pIpEvtInfo->u4BitMap, u4IfIndex, pIpEvtInfo->u4IpAddr);
#endif
    if (CfaGetIfInfo (pIpEvtInfo->u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        LDP_DBG3 ((LDP_IF_MISC | LDP_IF_ALL),
                  " %s:%d CfaGetIfInfo() failed for Index=%d\n", __func__,
                  __LINE__, pIpEvtInfo->u4IfIndex);
    }

    /* Since interface type is derived on the basis of IP Addresses receivd,
     * There may be a case for unnumbered interfaces when the IP ADDR received 
     * is zero that interface is also treated as IPV4 Type */
    CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4UnnumAssocIPIf);
    LDP_DBG3 (LDP_IF_PRCS,
              "LdpProcessCfaOperDownEvent:UnnumAssocIpIf is % d, Ip4Add is %x, IfIndex is %d \n",
              u4UnnumAssocIPIf, pIpEvtInfo->u4IpAddr, u4IfIndex);
    if ((pIpEvtInfo->u4IpAddr != LDP_ZERO) || (LDP_ZERO != u4UnnumAssocIPIf))
    {
        u1InterfaceType = LDP_IFTYPE_IPV4;
    }
#ifdef MPLS_IPV6_WANTED
    if (LdpInterfaceIpv6Match (&pIpEvtInfo->LnklocalIpv6Addr,
                               &pIpEvtInfo->SitelocalIpv6Addr,
                               &pIpEvtInfo->GlbUniqueIpv6Addr) == TRUE)
    {
        u1InterfaceType = u1InterfaceType | LDP_IFTYPE_IPV6;
    }
#endif

    PrintInterfaceAddr (pIpEvtInfo);

    MEMCPY ((UINT1 *) &u4IpAddr,
            (UINT1 *) (LDP_LSR_ID (u2IncarnId).au1Ipv4Addr), LDP_LSR_ID_LEN);
    u4IpAddr = OSIX_NTOHL (u4IpAddr);

    /* Retrigger the Entitysession with the router-id configured with
     ** force option if the current router-id is changed from the
     ** configured one
     **/
    MEMCPY ((UINT1 *) &u4TempIpAddr,
            (LDP_NEW_LSR_ID (u2IncarnId).au1Ipv4Addr), LDP_LSR_ID_LEN);
    u4TempIpAddr = OSIX_NTOHL (u4TempIpAddr);

    /*** LSR-ID change Handling **/
#ifdef MPLS_IPV6_WANTED
    bIsIpv6LastInterface = LdpGetIsLastIpv6Interface (u2IncarnId, u4IfIndex);
#endif
    /*LSR ID not attached to any interface then Retrigger won't happen */
    if ((NetIpv4IfIsOurAddress (u4IpAddr) == NETIPV4_FAILURE)
        && (u4IpAddr != pIpEvtInfo->u4IpAddr))
    {
        LDP_DBG (LDP_SSN_PRCS,
                 " CFAOPERDOWN:Current LSR Id is not attahced to any Interface \n");
    }
    /*Lsr Id IP went down And IF Active Interface available then retrigger the entities */
    else if ((pIpEvtInfo->u4IpAddr != LDP_ZERO)
             && (u4IpAddr == pIpEvtInfo->u4IpAddr))
    {
        LDP_DBG (LDP_SSN_PRCS,
                 " CFAOPERDOWN:LSR ID attached to interface went down\n");
        if (LdpGetIsIpv4ActiveInterface () == LDP_TRUE)
        {
            LDP_DBG (LDP_SSN_PRCS,
                     " CFAOPERDOWN:Active Interface Available Retriggering the Entities\n");
            LdpRetriggerEntitySessions (OSIX_HTONL (u4TempIpAddr), u4IfIndex,
                                        OSIX_HTONL (pIpEvtInfo->u4IpAddr),
                                        LDP_IF_OPER_DISABLE);
            return LDP_SUCCESS;
        }
        else
        {
            /*If LSR_ID is the the last UP Interface and IF Down is recieved for same IP
             * select the default NVRAM IP*/
            u4NvRamIpAddr = IssGetIpAddrFromNvRam ();
            LDP_DBG (LDP_SSN_PRCS,
                     " CFAOPERDOWN: No Active Interface Available, Retriggering the Entities with NVRAM IP\n");
            LdpRetriggerEntitySessions (OSIX_HTONL (u4NvRamIpAddr), u4IfIndex,
                                        OSIX_HTONL (pIpEvtInfo->u4IpAddr),
                                        LDP_IF_OPER_DISABLE);
            return LDP_SUCCESS;
        }
        LDP_DBG (LDP_SSN_PRCS, " CFAOPERDOWN:No Active Interface Available \n");
#ifdef MPLS_IPV6_WANTED
        LDP_LAST_V4IF_DOWN (u2IncarnId) = LDP_TRUE;
#endif

    }
#ifdef MPLS_IPV6_WANTED
    /*** Last IPV6 Interface && no IPV4 Interface available : mark all the entites as oper down ***/
    else if ((bIsIpv6LastInterface == LDP_TRUE)
             && (pIpEvtInfo->u4IpAddr != LDP_ZERO)
             && (LdpGetIsIpv4ActiveInterface () == FALSE))
    {
        LDP_DBG (LDP_SSN_PRCS,
                 " CFAOPERDOWN:No Active Interface Available Last IPV6 interface went down Retriggering the Entities\n");
        LdpRetriggerEntitySessions (OSIX_HTONL (u4TempIpAddr), u4IfIndex,
                                    OSIX_HTONL (pIpEvtInfo->u4IpAddr),
                                    LDP_IF_OPER_DISABLE);
        return LDP_SUCCESS;
    }
#endif
    if (LdpAddrDelCfaOperDown (pIpEvtInfo, u1InterfaceType) != LDP_SUCCESS)
    {
        LDP_DBG2 (LDP_SSN_PRCS,
                  "%s return LdpAddrDelCfaOperDown() Failed u1InterfaceType = %d !!!\n",
                  __func__, u1InterfaceType);
        return LDP_FAILURE;
    }

    if (CfaIfInfo.u1IfType != CFA_LOOPBACK)
    {
        TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
        {
            i4RetVal =
                LdpGetLdpEntityIfEntry (pIpEvtInfo->u4IncarnId, pLdpEntity,
                                        pIpEvtInfo->u4IfIndex, &pIfEntry);
            if (pLdpEntity->bIsIntfMapped == LDP_FALSE
                && i4RetVal == LDP_SUCCESS)
            {
                /** Remove the Addr from the multicast group **/
                if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
                {
                    if (LdpAddrDelUdpLeaveMulticast (pIfEntry, u1InterfaceType)
                        != LDP_SUCCESS)
                    {
                        LDP_DBG1 (LDP_SSN_PRCS,
                                  "LdpAddrDelUdpLeaveMulticast() Failed u1InterfaceType = %d\n",
                                  u1InterfaceType);
                        return LDP_FAILURE;
                    }
                }
                /* The interface is disassociated from the entity
                 * if it is associated with it
                 */
                TMO_SLL_Delete (&(pLdpEntity->IfList),
                                &(pIfEntry->NextIfEntry));
                MemReleaseMemBlock (LDP_EIF_POOL_ID, (UINT1 *) pIfEntry);
            }
            else if (i4RetVal == LDP_SUCCESS)
            {
                /** Remove the Addr from the multicast group **/
                if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
                {
                    if (LdpAddrDelUdpLeaveMulticast (pIfEntry, u1InterfaceType)
                        != LDP_SUCCESS)
                    {
                        LDP_DBG1 (LDP_SSN_PRCS,
                                  "LdpAddrDelUdpLeaveMulticast() Failed u1InterfaceType = %d\n",
                                  u1InterfaceType);
                        return LDP_FAILURE;
                    }
                }
                switch (u1InterfaceType)
                {
                    case LDP_IFTYPE_IPV4:
                        LdpUpdateLdpIfTableEntry (pIfEntry, u1InterfaceType,
                                                  CFA_IF_DOWN,
                                                  pIpEvtInfo->u4IpAddr);
                        break;
#ifdef MPLS_IPV6_WANTED
                    case LDP_IFTYPE_IPV6:
                    case LDP_IFTYPE_DUAL:
                        LdpUpdateLdpIpv6IfTableEntry (pIfEntry, CFA_IF_DOWN,
                                                      u1InterfaceType,
                                                      u4IfListAddr,
                                                      &LnklocalIpv6Addr,
                                                      &SitelocalIpv6Addr,
                                                      &GlbUniqueIpv6Addr);
                        break;
#endif
                };
            }
        }
    }

    LDP_DBG2 (LDP_IF_ALL, ("%s:%d Exit \n"), __func__, __LINE__);
    return LDP_SUCCESS;
}

BOOL1
LdpGetIsIpv4ActiveInterface (VOID)
{
    tNetIpv4IfInfo      NetIpIfInfo;
    INT4                i4Status = 0;
    tNetIpv4IfInfo      NextNetIpIfInfo;
    MEMSET (&NetIpIfInfo, LDP_ZERO, sizeof (tNetIpv4IfInfo));
    MEMSET (&NextNetIpIfInfo, LDP_ZERO, sizeof (tNetIpv4IfInfo));
    i4Status = NetIpv4GetFirstIfInfo (&NetIpIfInfo);
    if (i4Status != NETIPV6_FAILURE)
    {
        do
        {
            if (NetIpIfInfo.u4Addr != LDP_ZERO
                && NetIpIfInfo.u4Oper == IPIF_OPER_ENABLE)
            {
                return LDP_TRUE;
            }
            i4Status =
                NetIpv4GetNextIfInfo (NetIpIfInfo.u4IfIndex, &NextNetIpIfInfo);
            if (i4Status != NETIPV6_FAILURE)
            {
                MEMCPY (&NetIpIfInfo, &NextNetIpIfInfo,
                        sizeof (tNetIpv4IfInfo));
            }
        }
        while (i4Status != NETIPV6_FAILURE);
    }
    return LDP_FALSE;
}

#ifdef MPLS_IPV6_WANTED
BOOL1
LdpGetIsLastIpv6Interface (UINT2 u2IncarnId, UINT4 u4IfIndex)
{
    UINT4               u4Count = LDP_ZERO;
    UINT4               u4IfEnableCount = LDP_ZERO;
    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity), pLdpIfTableEntry,
                      tLdpIfTableEntry *)
        {
            if (pLdpIfTableEntry->u1OperStatus == LDP_IF_OPER_ENABLE)
            {
                if (LDP_ENTITY_IF_INDEX (pLdpIfTableEntry) != u4IfIndex)
                {
                    if (pLdpIfTableEntry->u1InterfaceType == LDP_IFTYPE_IPV6)
                    {
                        u4Count++;
                    }
                }
                u4IfEnableCount++;
            }
        }
    }

    if (u4IfEnableCount == LDP_ZERO)
    {
        return LDP_FALSE;
    }

    if (u4Count > LDP_ZERO)
    {
        return LDP_FALSE;
    }
    return LDP_TRUE;
}
#endif

UINT1
LdpAddrDelCfaOperDown (tLdpIpEvtInfo * pIpEvtInfo, UINT1 u1InterfaceType)
{
    UINT2               u2IncarnId = (UINT2) pIpEvtInfo->u4IncarnId;
    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    tTMO_SLL           *pAdjList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT4               u4TempAddr = 0;
    UINT4               u4TempAdjCount = 0;
    uGenAddr            uAddr;
    BOOL1               bSsnDeleted = FALSE;
    tLdpPeer           *pTempLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLdpAdjacency      *pLdpAdj = NULL;
    tLdpAdjacency      *pTempAdj = NULL;
    tLdpAdjacency      *pTempAdjEntry = NULL;
    UINT4               u4IfIndex = 0;

#ifdef MPLS_IPV6_WANTED
    tIp6Addr            LnklocalIpv6Addr;
    tIp6Addr            SitelocalIpv6Addr;
    tIp6Addr            GlbUniqueIpv6Addr;
    tIp6Addr            TempIpv6Addr;

    tIp6Addr           *pLnklocalIpv6Addr = NULL;
    tIp6Addr           *pSitelocalIpv6Addr = NULL;
    tIp6Addr           *pGlbUniqueIpv6Addr = NULL;

    MEMSET (&LnklocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&SitelocalIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&GlbUniqueIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
    MEMSET (&TempIpv6Addr, LDP_ZERO, sizeof (tIp6Addr));
#endif

    MEMSET (&uAddr, 0, sizeof (uGenAddr));

    u4IfIndex = pIpEvtInfo->u4IfIndex;

    u4TempAddr = OSIX_HTONL (pIpEvtInfo->u4IpAddr);
    MEMCPY ((UINT1 *) uAddr.au1Ipv4Addr,
            (UINT1 *) &u4TempAddr, LDP_IPV4ADR_LEN);

#ifdef MPLS_IPV6_WANTED
    MEMCPY (&LnklocalIpv6Addr, &pIpEvtInfo->LnklocalIpv6Addr, LDP_IPV6ADR_LEN);
    if (!
        (MEMCMP (&LnklocalIpv6Addr, &TempIpv6Addr, LDP_IPV6ADR_LEN) ==
         LDP_ZERO))
    {
        pLnklocalIpv6Addr = &LnklocalIpv6Addr;
    }
    MEMCPY (&SitelocalIpv6Addr, &pIpEvtInfo->SitelocalIpv6Addr,
            LDP_IPV6ADR_LEN);
    if (!
        (MEMCMP (&SitelocalIpv6Addr, &TempIpv6Addr, LDP_IPV6ADR_LEN) ==
         LDP_ZERO))
    {
        pSitelocalIpv6Addr = &SitelocalIpv6Addr;
    }

    MEMCPY (&GlbUniqueIpv6Addr, &pIpEvtInfo->GlbUniqueIpv6Addr,
            LDP_IPV6ADR_LEN);
    if (!
        (MEMCMP (&GlbUniqueIpv6Addr, &TempIpv6Addr, LDP_IPV6ADR_LEN) ==
         LDP_ZERO))
    {
        pGlbUniqueIpv6Addr = &GlbUniqueIpv6Addr;
    }
#endif

    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        if ((LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE) &&
            (LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE))
        {
            /* No Need to Process the IF Change Event if Entity
             *                          ** is target type or not in active state */
            continue;
        }
        /** Address WithDraw and hello Adjacency expiry **/
        TMO_DYN_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer,
                          pTempLdpPeer, tLdpPeer *)
        {
            if ((pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession) == NULL)
            {
                continue;
            }
            if ((LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE) &&
                (TMO_SLL_Count (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity)) >
                 LDP_ONE))
            {
                LDP_DBG (LDP_DBG_PRCS,
                         "LDP targeted session deletion skipped\n");
                continue;
            }
            LDP_DBG5 (LDP_DBG_PRCS, "%s Peer LDP ID: %d.%d.%d.%d\n", __func__,
                      pLdpSession->pLdpPeer->PeerLdpId[0],
                      pLdpSession->pLdpPeer->PeerLdpId[1],
                      pLdpSession->pLdpPeer->PeerLdpId[2],
                      pLdpSession->pLdpPeer->PeerLdpId[3]);
            TMO_DYN_SLL_Scan (&(pLdpSession->AdjacencyList),
                              pLdpAdj, pTempAdj, tLdpAdjacency *)
            {
                if (pLdpAdj->u4IfIndex != u4IfIndex)
                {
                    continue;
                }
                if (bSsnDeleted == TRUE)
                {
                    break;
                }
                u4TempAdjCount = 0;
                pAdjList = NULL;
                pAdjList = ADJ_GET_ADJ_LIST (pLdpAdj);
                TMO_SLL_Scan (pAdjList, pTempAdjEntry, tLdpAdjacency *)
                {
                    if (u4TempAdjCount > 1)
                    {
                        break;
                    }
                    if (pLdpAdj->u1AddrType == pTempAdjEntry->u1AddrType)
                    {
                        u4TempAdjCount++;
                    }
                }
                if (u4TempAdjCount == LDP_ONE)
                {
                    bSsnDeleted = TRUE;
                }

                LDP_DBG4 (LDP_SSN_PRCS,
                          "The Ldp Peer %d.%d.%d.%d is down\n",
                          pLdpPeer->PeerLdpId[0],
                          pLdpPeer->PeerLdpId[1],
                          pLdpPeer->PeerLdpId[2], pLdpPeer->PeerLdpId[3]);

                if (LdpHandleAdjExpiry (pLdpAdj, LDP_STAT_TYPE_SHUT_DOWN) ==
                    LDP_FAILURE)
                {
                    MEMSET (&pLdpPeer->NetAddr, LDP_ZERO, LDP_NET_ADDR_LEN);
                }
            }
            if (bSsnDeleted == TRUE)
            {
                bSsnDeleted = FALSE;
                continue;
            }

            if (pLdpSession->u1SessionState == LDP_SSM_ST_OPERATIONAL)
            {
                LDP_DBG6 (LDP_SSN_PRCS, "%s Sending Address Wdraw message to "
                          "the Peer: %d.%d.%d.%d for address: %x\n", __func__,
                          pLdpSession->pLdpPeer->PeerLdpId[0],
                          pLdpSession->pLdpPeer->PeerLdpId[1],
                          pLdpSession->pLdpPeer->PeerLdpId[2],
                          pLdpSession->pLdpPeer->PeerLdpId[3], uAddr);
                switch (u1InterfaceType)
                {
                    case LDP_IFTYPE_IPV4:
                        LdpSendAddrWithdrawMsg (pLdpSession, uAddr);
                        break;
#ifdef MPLS_IPV6_WANTED
                    case LDP_IFTYPE_IPV6:
                        LdpSendIpv6AddrWithdrawMsg (pLdpSession,
                                                    pLnklocalIpv6Addr,
                                                    pSitelocalIpv6Addr,
                                                    pGlbUniqueIpv6Addr);
                        break;
                    case LDP_IFTYPE_DUAL:
                        LdpSendAddrWithdrawMsg (pLdpSession, uAddr);

                        LdpSendIpv6AddrWithdrawMsg (pLdpSession,
                                                    pLnklocalIpv6Addr,
                                                    pSitelocalIpv6Addr,
                                                    pGlbUniqueIpv6Addr);

                        break;
#endif
                };
            }
        }

        /** Update the Transport Address **/
#ifdef MPLS_IPV6_WANTED
        if (pLdpEntity->u4Ipv6TransIfIndex == pIpEvtInfo->u4IfIndex)
        {
            pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_FALSE;
        }
#endif
        if (pLdpEntity->u4TransIfIndex == pIpEvtInfo->u4IfIndex)
        {
            pLdpEntity->bIsTransAddrIntfUp = LDP_FALSE;
            /*switch(u1InterfaceType)
               {
               case LDP_IFTYPE_IPV4:
               pLdpEntity->u4TransportAddress = LDP_ZERO;
               break;
               #ifdef MPLS_IPV6_WANTED
               case LDP_IFTYPE_IPV6:
               case LDP_IFTYPE_DUAL:
               pLdpEntity->u4TransportAddress = LDP_ZERO;
               MEMSET(pLdpEntity->Ipv6TransportAddress.u1_addr,LDP_ZERO,LDP_IPV6ADR_LEN);
               break;
               #endif
               }; */
        }
    }
    return LDP_SUCCESS;
}

/** This API will Update Transport Address and send Address Message */
UINT1
LdpAddrAddCfaOperUp (tLdpIpEvtInfo * pIpEvtInfo, UINT1 u1InterfaceType)
{
    UINT2               u2IncarnId = (UINT2) pIpEvtInfo->u4IncarnId;
    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
#ifdef MPLS_IPV6_WANTED
    tIp6Addr           *pAddr = NULL;
#endif
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpPeer           *pTempLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT4               u4TempAddr = 0;
    uGenAddr            uAddr;

#ifdef MPLS_IPV6_WANTED
    tIp6Addr            LnklocalIpv6Addr;
    tIp6Addr            SitelocalIpv6Addr;
    tIp6Addr            GlbUniqueIpv6Addr;

    tIp6Addr           *pLnklocalIpv6Addr = NULL;
    tIp6Addr           *pSitelocalIpv6Addr = NULL;
    tIp6Addr           *pGlbUniqueIpv6Addr = NULL;

    tIp6Addr            TempIpv6Addr;
    tLdpAdjacency      *pLdpAdj = NULL;
    tLdpAdjacency      *pTempAdj = NULL;
    UINT1               u1LdpSendHelloReq = LDP_THREE;

    MEMSET (&LnklocalIpv6Addr, LDP_ZERO, LDP_IPV6ADR_LEN);
    MEMSET (&SitelocalIpv6Addr, LDP_ZERO, LDP_IPV6ADR_LEN);
    MEMSET (&GlbUniqueIpv6Addr, LDP_ZERO, LDP_IPV6ADR_LEN);
    MEMSET (&TempIpv6Addr, LDP_ZERO, LDP_IPV6ADR_LEN);
#endif

    MEMSET (&uAddr, 0, sizeof (uGenAddr));

    u4TempAddr = OSIX_HTONL (pIpEvtInfo->u4IpAddr);
    MEMCPY ((UINT1 *) uAddr.au1Ipv4Addr,
            (UINT1 *) &u4TempAddr, LDP_IPV4ADR_LEN);

#ifdef MPLS_IPV6_WANTED
    MEMCPY (&LnklocalIpv6Addr, &pIpEvtInfo->LnklocalIpv6Addr, LDP_IPV6ADR_LEN);
    if (!
        (MEMCMP (&LnklocalIpv6Addr, &TempIpv6Addr, LDP_IPV6ADR_LEN) ==
         LDP_ZERO))
    {
        pLnklocalIpv6Addr = &LnklocalIpv6Addr;
    }
    MEMCPY (&SitelocalIpv6Addr, &pIpEvtInfo->SitelocalIpv6Addr,
            LDP_IPV6ADR_LEN);
    if (!
        (MEMCMP (&SitelocalIpv6Addr, &TempIpv6Addr, LDP_IPV6ADR_LEN) ==
         LDP_ZERO))
    {
        pSitelocalIpv6Addr = &SitelocalIpv6Addr;
    }

    MEMCPY (&GlbUniqueIpv6Addr, &pIpEvtInfo->GlbUniqueIpv6Addr,
            LDP_IPV6ADR_LEN);
    if (!
        (MEMCMP (&GlbUniqueIpv6Addr, &TempIpv6Addr, LDP_IPV6ADR_LEN) ==
         LDP_ZERO))
    {
        pGlbUniqueIpv6Addr = &GlbUniqueIpv6Addr;
    }
#endif
    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        if ((LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE) &&
            (LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE))
        {
            /* No Need to Process the IF Change Event if Entity
             ** is target type or not in active state */
            continue;
        }
#ifdef MPLS_IPV6_WANTED
        /** MSR case : In MSR IPV6 addresses are not restored fully when we set the IPV6 transport address via nmhSet API
          so we update the transport address via nmhset API but transport interface index would be populated here **/
        if (((pLdpEntity->u1Ipv6TransAddrTlvEnable == TRUE)
             && (pLdpEntity->u4Ipv6TransIfIndex == LDP_ZERO)))
        {
            if (!
                (MEMCMP
                 (&pLdpEntity->Ipv6TransportAddress, &TempIpv6Addr,
                  LDP_IPV6ADR_LEN) == LDP_ZERO))
            {
                if ((LDP_ZERO ==
                     MEMCMP (&pLdpEntity->Ipv6TransportAddress,
                             &LnklocalIpv6Addr, LDP_IPV6ADR_LEN))
                    || (LDP_ZERO ==
                        MEMCMP (&pLdpEntity->Ipv6TransportAddress,
                                &SitelocalIpv6Addr, LDP_IPV6ADR_LEN))
                    || (LDP_ZERO ==
                        MEMCMP (&pLdpEntity->Ipv6TransportAddress,
                                &GlbUniqueIpv6Addr, LDP_IPV6ADR_LEN)))
                {
                    pLdpEntity->u4Ipv6TransIfIndex = pIpEvtInfo->u4IfIndex;
                    pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_TRUE;
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "ADDRADDCFAOPERUP: V6:bIsTransAddrIntfUp True TRANSADDR6: %s IfIndex=%x\n",
                              Ip6PrintAddr (&
                                            (pLdpEntity->Ipv6TransportAddress)),
                              pLdpEntity->u4TransIfIndex);
                }
            }
        }
        /** Update the Transport Address **/
        else if ((pLdpEntity->u4TransIfIndex == pIpEvtInfo->u4IfIndex) ||
                 (pLdpEntity->u4Ipv6TransIfIndex == pIpEvtInfo->u4IfIndex))
#else
        if (pLdpEntity->u4TransIfIndex == pIpEvtInfo->u4IfIndex)
#endif
        {
            switch (u1InterfaceType)
            {
                case LDP_IFTYPE_IPV4:
                    if (pIpEvtInfo->u4IpAddr == LDP_ZERO)
                    {
                        pLdpEntity->bIsTransAddrIntfUp = LDP_FALSE;
                        LDP_DBG (LDP_SSN_PRCS,
                                 "ADDRADDCFAOPERUP:V4:bIsTransAddrIntfUp-False \n");
                    }
                    else
                    {
                        pLdpEntity->u4TransportAddress = pIpEvtInfo->u4IpAddr;
                        pLdpEntity->bIsTransAddrIntfUp = LDP_TRUE;
                        LDP_DBG1 (LDP_SSN_PRCS,
                                  "ADDRADDCFAOPERUP: V4:bIsTransAddrIntfUp True TRANSADDR4: %x\n",
                                  pLdpEntity->u4TransportAddress);
                    }
                    break;
#if MPLS_IPV6_WANTED
                case LDP_IFTYPE_IPV6:
                    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
                    {
                        if (LDP_ZERO ==
                            MEMCMP (&pIpEvtInfo->GlbUniqueIpv6Addr,
                                    &TempIpv6Addr, LDP_IPV6ADR_LEN))
                        {
                            pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_FALSE;
                            LDP_DBG (LDP_SSN_PRCS,
                                     "ADDRADDCFAOPERUP: V6Target:bIsTransAddrIntfUp-False");
                        }
                        else
                        {
                            pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_TRUE;
                            MEMCPY (pLdpEntity->Ipv6TransportAddress.u1_addr,
                                    &GlbUniqueIpv6Addr, LDP_IPV6ADR_LEN);
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "ADDRADDCFAOPERUP: V6Target:bIsTransAddrIntfUp -True TRANSADDR4: %s\n",
                                      Ip6PrintAddr (&
                                                    (pLdpEntity->
                                                     Ipv6TransportAddress)));

                        }
                    }
                    else
                    {
                        pAddr =
                            LdpGetBasicIpv6TransAddrPref (&pIpEvtInfo->
                                                          LnklocalIpv6Addr,
                                                          &pIpEvtInfo->
                                                          SitelocalIpv6Addr,
                                                          &pIpEvtInfo->
                                                          GlbUniqueIpv6Addr);

                        if (NULL == pAddr)
                        {
                            pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_FALSE;
                            LDP_DBG (LDP_SSN_PRCS,
                                     "ADDRADDCFAOPERUP: V6Basic:bIsTransAddrIntfUp-False");

                        }
                        else
                        {
                            pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_TRUE;
                            MEMCPY (pLdpEntity->Ipv6TransportAddress.u1_addr,
                                    pAddr->u1_addr, LDP_IPV6ADR_LEN);
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "ADDRADDCFAOPERUP: V6Basic:bIsTransAddrIntfUp -True TRANSADDR4: %s\n",
                                      Ip6PrintAddr (&
                                                    (pLdpEntity->
                                                     Ipv6TransportAddress)));
                        }

                    }

                    break;
                case LDP_IFTYPE_DUAL:
                    if (pIpEvtInfo->u4IpAddr == LDP_ZERO)
                    {
                        pLdpEntity->bIsTransAddrIntfUp = LDP_FALSE;
                        LDP_DBG (LDP_SSN_PRCS,
                                 "ADDRADDCFAOPERUP: V4:bIsTransAddrIntfUp-False");
                    }
                    else
                    {
                        pLdpEntity->u4TransportAddress = pIpEvtInfo->u4IpAddr;
                        pLdpEntity->bIsTransAddrIntfUp = LDP_TRUE;
                        LDP_DBG1 (LDP_SSN_PRCS,
                                  "ADDRADDCFAOPERUP: V4:bIsTransAddrIntfUp True TRANSADDR4: %x\n",
                                  pLdpEntity->u4TransportAddress);
                    }

                    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
                    {
                        if (LDP_ZERO ==
                            MEMCMP (&pIpEvtInfo->GlbUniqueIpv6Addr,
                                    &TempIpv6Addr, LDP_IPV6ADR_LEN))
                        {
                            pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_FALSE;
                            LDP_DBG (LDP_SSN_PRCS,
                                     "ADDRADDCFAOPERUP: V6Target:bIsTransAddrIntfUp-False");
                        }
                        else
                        {
                            pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_TRUE;
                            MEMCPY (pLdpEntity->Ipv6TransportAddress.u1_addr,
                                    &GlbUniqueIpv6Addr, LDP_IPV6ADR_LEN);
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "ADDRADDCFAOPERUP: V6Target:bIsTransAddrIntfUp -True TRANSADDR4: %s\n",
                                      Ip6PrintAddr (&
                                                    (pLdpEntity->
                                                     Ipv6TransportAddress)));

                        }
                    }
                    else
                    {

                        pAddr =
                            LdpGetBasicIpv6TransAddrPref (&pIpEvtInfo->
                                                          LnklocalIpv6Addr,
                                                          &pIpEvtInfo->
                                                          SitelocalIpv6Addr,
                                                          &pIpEvtInfo->
                                                          GlbUniqueIpv6Addr);
                        if (NULL == pAddr)
                        {
                            pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_FALSE;
                            LDP_DBG (LDP_SSN_PRCS,
                                     "ADDRADDCFAOPERUP: V6Basic:bIsTransAddrIntfUp-False");
                        }
                        else
                        {
                            pLdpEntity->bIsIpv6TransAddrIntfUp = LDP_TRUE;
                            MEMCPY (pLdpEntity->Ipv6TransportAddress.u1_addr,
                                    pAddr->u1_addr, LDP_IPV6ADR_LEN);
                            LDP_DBG1 (LDP_SSN_PRCS,
                                      "ADDRADDCFAOPERUP: V6Basic:bIsTransAddrIntfUp -True TRANSADDR4: %s\n",
                                      Ip6PrintAddr (&
                                                    (pLdpEntity->
                                                     Ipv6TransportAddress)));
                        }
                    }
                    break;
#endif
            };
        }
        TMO_DYN_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer,
                          pTempLdpPeer, tLdpPeer *)
        {
            if ((pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession) == NULL)
            {
                continue;
            }
            if (pLdpSession->u1SessionState == LDP_SSM_ST_OPERATIONAL)
            {
#ifdef MPLS_IPV6_WANTED
                u1LdpSendHelloReq = LDP_THREE;
#endif
                switch (u1InterfaceType)
                {
                    case LDP_IFTYPE_IPV4:

                        LDP_DBG2 (LDP_SSN_PRCS, "%s %d Send AddressMsg \n",
                                  __FUNCTION__, __LINE__);

                        /* Scenraio: Interface comes operup, there is not even a single adjacency available 
                         * of the same address family and the session with peer is operational with different 
                         * Address Family.
                         * IMPACT: The impact was seen when LBL MAP is received after interface comes operup 
                         * and there is no adjacency of the same address family is available, hence that 
                         * Lable map is not processed and label release is sent.*/
#ifdef MPLS_IPV6_WANTED
                        if (pLdpSession->SrcTransAddr.u2AddrType ==
                            LDP_ADDR_TYPE_IPV6)
                        {
                            TMO_DYN_SLL_Scan (&(pLdpSession->AdjacencyList),
                                              pLdpAdj, pTempAdj,
                                              tLdpAdjacency *)
                            {
                                if (pLdpAdj->u1AddrType ==
                                    (UINT1) LDP_ADDR_TYPE_IPV4)
                                {
                                    LDP_DBG1 (LDP_DBG_PRCS,
                                              " LdpAddrAddCfaOperUp: V4 interface"
                                              "came up: V4Adj Found with IfIndex->:%d \n",
                                              pLdpAdj->u4IfIndex);
                                    u1LdpSendHelloReq = LDP_FALSE;
                                    break;
                                }
                                u1LdpSendHelloReq = LDP_TRUE;
                            }
                        }
                        if (LDP_TRUE == u1LdpSendHelloReq
                            && LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) ==
                            LDP_FALSE)
                        {
                            LDP_DBG (LDP_DBG_PRCS,
                                     " LdpAddrAddCfaOperUp: V4 interface came up:"
                                     "V4Adj Not Found Sending Hello\n");
                            LdpSendHelloMsg (pLdpEntity);
                        }
#endif

                        if ((LDP_ENTITY_IS_TARGET_TYPE
                             ((SSN_GET_ENTITY (pLdpSession))) == LDP_TRUE)
                            && (SSN_GET_ENTITY (pLdpSession)->OutStackTnlInfo.
                                u4TnlId != LDP_ZERO))
                        {
                            LdpSendAddrMsg (pLdpSession,
                                            (tIpv4Addr
                                             *) (LDP_IPV4_ADDR (uAddr)));
                        }
                        else if (LDP_ENTITY_IS_TARGET_TYPE
                                 ((SSN_GET_ENTITY (pLdpSession))) == LDP_FALSE)
                        {
                            LdpSendAddrMsg (pLdpSession,
                                            (tIpv4Addr
                                             *) (LDP_IPV4_ADDR (uAddr)));
                        }

                        break;
#ifdef MPLS_IPV6_WANTED
                    case LDP_IFTYPE_IPV6:
                        if (pLdpSession->SrcTransAddr.u2AddrType ==
                            LDP_ADDR_TYPE_IPV4)
                        {
                            TMO_DYN_SLL_Scan (&(pLdpSession->AdjacencyList),
                                              pLdpAdj, pTempAdj,
                                              tLdpAdjacency *)
                            {
                                if (pLdpAdj->u1AddrType ==
                                    (UINT1) LDP_ADDR_TYPE_IPV6)
                                {
                                    LDP_DBG1 (LDP_DBG_PRCS,
                                              " LdpAddrAddCfaOperUp: V6"
                                              "interface came up: V6Adj Found with IfIndex->:%d \n",
                                              pLdpAdj->u4IfIndex);
                                    u1LdpSendHelloReq = LDP_FALSE;
                                    break;
                                }
                                u1LdpSendHelloReq = LDP_TRUE;
                            }
                        }
                        if (LDP_TRUE == u1LdpSendHelloReq
                            && LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) ==
                            LDP_FALSE)
                        {
                            LDP_DBG (LDP_DBG_PRCS,
                                     " LdpAddrAddCfaOperUp: V6 interface came up:"
                                     "V6Adj Not Found Sending Hello\n");
                            LdpSendHelloMsg (pLdpEntity);
                        }

                        LDP_DBG2 (LDP_SSN_PRCS, "%s:%d Sending V6 Addr Msg\n",
                                  __func__, __LINE__);

                        if ((LDP_ENTITY_IS_TARGET_TYPE
                             ((SSN_GET_ENTITY (pLdpSession))) == LDP_TRUE)
                            && (SSN_GET_ENTITY (pLdpSession)->OutStackTnlInfo.
                                u4TnlId != LDP_ZERO))
                        {
                            LdpSendIpv6AddrMsg (pLdpSession, pLnklocalIpv6Addr,
                                                pSitelocalIpv6Addr,
                                                pGlbUniqueIpv6Addr);

                        }
                        else if (LDP_ENTITY_IS_TARGET_TYPE
                                 ((SSN_GET_ENTITY (pLdpSession))) == LDP_FALSE)
                        {
                            LdpSendIpv6AddrMsg (pLdpSession, pLnklocalIpv6Addr,
                                                pSitelocalIpv6Addr,
                                                pGlbUniqueIpv6Addr);

                        }

                        break;
                    case LDP_IFTYPE_DUAL:
                        LDP_DBG2 (LDP_SSN_PRCS, "%s %d Send AddressMsg\n",
                                  __FUNCTION__, __LINE__);
                        TMO_DYN_SLL_Scan (&(pLdpSession->AdjacencyList),
                                          pLdpAdj, pTempAdj, tLdpAdjacency *)
                        {
                            if (pLdpSession->SrcTransAddr.u2AddrType ==
                                LDP_ADDR_TYPE_IPV6)
                            {
                                if (pLdpAdj->u1AddrType ==
                                    (UINT1) LDP_ADDR_TYPE_IPV4)
                                {
                                    LDP_DBG1 (LDP_DBG_PRCS,
                                              " LdpAddrAddCfaOperUp: Dual"
                                              "interface came up: SessionType->V6 V4Adj Found with IfIndex->:%d \n",
                                              pLdpAdj->u4IfIndex);
                                    u1LdpSendHelloReq = LDP_FALSE;
                                    break;
                                }
                                u1LdpSendHelloReq = LDP_TRUE;
                            }
                            else
                            {
                                if (pLdpAdj->u1AddrType ==
                                    (UINT1) LDP_ADDR_TYPE_IPV6)
                                {
                                    LDP_DBG1 (LDP_DBG_PRCS,
                                              " LdpAddrAddCfaOperUp: Dual"
                                              "interface came up: SessionType->V4 V6Adj Found with IfIndex->:%d \n",
                                              pLdpAdj->u4IfIndex);
                                    u1LdpSendHelloReq = LDP_FALSE;
                                    break;
                                }
                                u1LdpSendHelloReq = LDP_TRUE;
                            }
                        }
                        if (LDP_TRUE == u1LdpSendHelloReq
                            && LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) ==
                            LDP_FALSE)
                        {
                            LDP_DBG (LDP_DBG_PRCS,
                                     " LdpAddrAddCfaOperUp: Dual interface came up:"
                                     "either  V4 or V6 Adj Not Found Sending Hello\n");
                            LdpSendHelloMsg (pLdpEntity);
                        }

                        if ((LDP_ENTITY_IS_TARGET_TYPE
                             ((SSN_GET_ENTITY (pLdpSession))) == LDP_TRUE)
                            && (SSN_GET_ENTITY (pLdpSession)->OutStackTnlInfo.
                                u4TnlId != LDP_ZERO))
                        {
                            LdpSendAddrMsg (pLdpSession,
                                            (tIpv4Addr
                                             *) (LDP_IPV4_ADDR (uAddr)));
                        }
                        else if (LDP_ENTITY_IS_TARGET_TYPE
                                 ((SSN_GET_ENTITY (pLdpSession))) == LDP_FALSE)
                        {
                            LdpSendAddrMsg (pLdpSession,
                                            (tIpv4Addr
                                             *) (LDP_IPV4_ADDR (uAddr)));
                        }

                        if ((pSitelocalIpv6Addr != NULL)
                            || (pGlbUniqueIpv6Addr != NULL))
                        {
                            LDP_DBG2 (LDP_DBG_PRCS,
                                      "%s %d Send IPv6AddressMsg\n", __func__,
                                      __LINE__);

                            if ((LDP_ENTITY_IS_TARGET_TYPE
                                 ((SSN_GET_ENTITY (pLdpSession))) == LDP_TRUE)
                                && (SSN_GET_ENTITY (pLdpSession)->
                                    OutStackTnlInfo.u4TnlId != LDP_ZERO))
                            {

                                LdpSendIpv6AddrMsg (pLdpSession,
                                                    pLnklocalIpv6Addr,
                                                    pSitelocalIpv6Addr,
                                                    pGlbUniqueIpv6Addr);
                            }
                            else if (LDP_ENTITY_IS_TARGET_TYPE
                                     ((SSN_GET_ENTITY (pLdpSession))) ==
                                     LDP_FALSE)
                            {

                                LdpSendIpv6AddrMsg (pLdpSession,
                                                    pLnklocalIpv6Addr,
                                                    pSitelocalIpv6Addr,
                                                    pGlbUniqueIpv6Addr);
                            }

                        }
                        break;
#endif
                };
            }
        }
    }
    return LDP_SUCCESS;
}

#ifdef MPLS_IPV6_WANTED
tIp6Addr           *
LdpGetBasicIpv6TransAddrPref (tIp6Addr * pLnklocalIpv6Addr,
                              tIp6Addr * pSitelocalIpv6Addr,
                              tIp6Addr * pGlbUniqueIpv6Addr)
{
    tIp6Addr            Addr;
    MEMSET (&Addr, 0, sizeof (tIp6Addr));
    if (MEMCMP (pGlbUniqueIpv6Addr->u1_addr, Addr.u1_addr, IPV6_ADDR_LENGTH) !=
        LDP_ZERO)
    {
        return pGlbUniqueIpv6Addr;
    }
    else if (MEMCMP
             (pSitelocalIpv6Addr->u1_addr, Addr.u1_addr,
              IPV6_ADDR_LENGTH) != LDP_ZERO)
    {
        return pSitelocalIpv6Addr;
    }
    else
    {
        return pLnklocalIpv6Addr;
    }
}
#endif

UINT1
LdpAddrAddUdpJoinMulticast (tLdpIfTableEntry * pLdpIfTableEntry,
                            UINT1 u1InterfaceType)
{
    UINT4               u4IfAddr = 0;
    INT4                i4UdpSockFd = LDP_UDP_SOCKFD (LDP_CUR_INCARN);
#ifdef MPLS_IPV6_WANTED
    UINT4               u4Udp6SockFd =
        (UINT4) LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
    tIp6Addr            TmpIpv6IfAddr;
    tIp6Addr            Ipv6IfAddr;
    MEMSET (&TmpIpv6IfAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Ipv6IfAddr, 0, sizeof (tIp6Addr));
#endif

    MEMCPY (&u4IfAddr, LDP_ENTITY_IF_ADDRESS (pLdpIfTableEntry),
            sizeof (UINT4));
    u4IfAddr = OSIX_NTOHL (u4IfAddr);
    if (pLdpIfTableEntry->u1OperStatus == LDP_IF_OPER_ENABLE)
    {
        if (LDP_IFTYPE_IPV4 == u1InterfaceType
            || LDP_IFTYPE_DUAL == u1InterfaceType)
        {
            MEMCPY (&u4IfAddr, LDP_ENTITY_IF_ADDRESS (pLdpIfTableEntry),
                    sizeof (UINT4));
            u4IfAddr = OSIX_NTOHL (u4IfAddr);
            if ((LDP_SET_NO_OF_ENT_MCASTCOUNT
                 (LDP_CUR_INCARN, pLdpIfTableEntry->u4IfIndex)) == LDP_ZERO)
            {
                if (LdpUdpJoinMulticast
                    ((UINT4) i4UdpSockFd, u4IfAddr,
                     LDP_ALL_ROUTERS_ADDR) == LDP_FAILURE)
                {
                    LDP_DBG1 (LDP_IF_MISC,
                              "IF:Joined Multicast Failed For V4 Addr: %x \n",
                              u4IfAddr);
                    return LDP_FAILURE;
                }
                LDP_DBG1 (LDP_IF_MISC,
                          "IF:Joined Multicast Success for v4 Addr: %x \n",
                          u4IfAddr);
                LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                              pLdpIfTableEntry->u4IfIndex)++;
            }
            else
            {
                LDP_DBG1 (LDP_IF_MISC,
                          "IF MCOUNT is not ZERO For V4 If Index %x \n",
                          pLdpIfTableEntry->u4IfIndex);
                LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                              pLdpIfTableEntry->u4IfIndex)++;
            }

        }
#ifdef MPLS_IPV6_WANTED
        if (LDP_IFTYPE_IPV6 == u1InterfaceType
            || LDP_IFTYPE_DUAL == u1InterfaceType)
        {
            MEMCPY (Ipv6IfAddr.u1_addr,
                    pLdpIfTableEntry->LnklocalIpv6Addr.u1_addr,
                    LDP_IPV6ADR_LEN);
            if ((LDP_SET_NO_OF_ENT_V6MCASTCOUNT
                 (LDP_CUR_INCARN, pLdpIfTableEntry->u4IfIndex)) == LDP_ZERO)
            {
                u4Udp6SockFd = (UINT4) LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
                if (LDP_FAILURE !=
                    INET_ATON6 (LDP_ALL_ROUTERS_V6ADDR, &TmpIpv6IfAddr))
                {
                    if (LdpUdpJoinV6Multicast ((INT4) u4Udp6SockFd, Ipv6IfAddr,
                                               TmpIpv6IfAddr,
                                               pLdpIfTableEntry->u4IfIndex) ==
                        LDP_FAILURE)
                    {
                        LDP_DBG1 (LDP_IF_MISC,
                                  "IF: Joined Multicast Failed For V6 Addr %x",
                                  Ip6PrintAddr (&
                                                (pLdpIfTableEntry->
                                                 LnklocalIpv6Addr)));
                        return LDP_FAILURE;
                    }
                    LDP_DBG1 (LDP_IF_MISC,
                              "IF:Joined Multicast Success For V6 Addr: %x \n",
                              Ip6PrintAddr (&
                                            (pLdpIfTableEntry->
                                             LnklocalIpv6Addr)));
                    LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                    pLdpIfTableEntry->
                                                    u4IfIndex)++;
                    return LDP_SUCCESS;
                }
                else
                {
                    LDP_DBG (LDP_IF_MISC,
                             "IF: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR %x\n");
                    return LDP_FAILURE;
                }
            }
            else
            {
                LDP_DBG1 (LDP_IF_MISC,
                          "IF MCOUNT is not ZERO For V6 If Index %x \n",
                          pLdpIfTableEntry->u4IfIndex);
                LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                pLdpIfTableEntry->u4IfIndex)++;
            }
        }
#endif
    }
    return LDP_SUCCESS;
}

UINT1
LdpAddrDelUdpLeaveMulticast (tLdpIfTableEntry * pLdpIfTableEntry,
                             UINT1 u1InterfaceType)
{
    UINT4               u4IfAddr = 0;
    INT4                i4UdpSockFd = LDP_UDP_SOCKFD (LDP_CUR_INCARN);
#ifdef MPLS_IPV6_WANTED
    INT4                i4Udp6SockFd = LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
    tIp6Addr            Ipv6IfAddr;
    tIp6Addr            AllRouterAddr;
    MEMSET (&Ipv6IfAddr, 0, sizeof (tIp6Addr));
    MEMSET (&AllRouterAddr, 0, sizeof (tIp6Addr));
#endif
    if (LDP_IFTYPE_IPV4 == u1InterfaceType
        || LDP_IFTYPE_DUAL == u1InterfaceType)
    {
        MEMCPY (&u4IfAddr, LDP_ENTITY_IF_ADDRESS (pLdpIfTableEntry),
                sizeof (UINT4));
        u4IfAddr = OSIX_NTOHL (u4IfAddr);
        if (LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                          pLdpIfTableEntry->u4IfIndex) >
            LDP_ZERO)
        {
            --(LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                             pLdpIfTableEntry->u4IfIndex));
        }
        if (LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                          pLdpIfTableEntry->u4IfIndex) ==
            LDP_ZERO)
        {
            if (LdpUdpLeaveMulticast (i4UdpSockFd, u4IfAddr,
                                      LDP_ALL_ROUTERS_ADDR) == LDP_FAILURE)
            {
                /* Error: Iface failed to leave the Mcast group */
                LDP_DBG1 (LDP_IF_MISC,
                          "IF:Leave Multicast Failed For V4 Addr: %x \n",
                          u4IfAddr);
                return LDP_FAILURE;
            }
        }
    }
#ifdef MPLS_IPV6_WANTED
    if (LDP_IFTYPE_IPV6 == u1InterfaceType
        || LDP_IFTYPE_DUAL == u1InterfaceType)
    {
        MEMCPY (Ipv6IfAddr.u1_addr, pLdpIfTableEntry->LnklocalIpv6Addr.u1_addr,
                LDP_IPV6ADR_LEN);
        if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT
            (LDP_CUR_INCARN, pLdpIfTableEntry->u4IfIndex) > LDP_ZERO)
        {
            --(LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                               pLdpIfTableEntry->u4IfIndex));
        }
        if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                            pLdpIfTableEntry->u4IfIndex) ==
            LDP_ZERO)
        {
            if (LDP_FAILURE !=
                INET_ATON6 (LDP_ALL_ROUTERS_V6ADDR, &AllRouterAddr))
            {
                if (LdpUdpLeaveV6Multicast
                    (i4Udp6SockFd, Ipv6IfAddr,
                     &AllRouterAddr,
                     pLdpIfTableEntry->u4IfIndex) == LDP_FAILURE)
                {
                    /* Error: Iface failed to leave the Mcast group */
                    LDP_DBG1 (LDP_IF_MISC,
                              "IF:Leave Multicast Failed For V6 Addr: %s \n",
                              Ip6PrintAddr (&Ipv6IfAddr));
                    return LDP_FAILURE;
                }
            }
            else
            {
                LDP_DBG (LDP_IF_MISC,
                         "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
                return LDP_FAILURE;
            }
        }
    }
#endif
    return LDP_SUCCESS;
}

BOOL1
LdpGetIfAllEntityDownViaIntOperDown (UINT2 u2IncarnId)
{
    BOOL1               bIsEntityOperUp = LDP_FALSE;
    tLdpEntity         *pLdpEntity = NULL;

    pLdpEntity = (tLdpEntity *) TMO_SLL_First (&LDP_ENTITY_LIST (u2IncarnId));
    while (pLdpEntity != NULL)
    {
        /* if the LDP entity is ACTIVE, then only it is a valid
         *                  *                  * candidate for searching */
        if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
        {
            if (pLdpEntity->u1OperStatus == LDP_OPER_ENABLED)
            {
                bIsEntityOperUp = LDP_TRUE;
                /* LDP entity is found which is oper enabled */
                break;
            }
        }
        pLdpEntity = (tLdpEntity *)
            TMO_SLL_Next (&LDP_ENTITY_LIST (u2IncarnId),
                          &pLdpEntity->NextLdpEntity);
    }

    return bIsEntityOperUp;
}

VOID
LdpUpdateLdpIfTableEntry (tLdpIfTableEntry * pLdpIfTableEntry,
                          UINT1 u1InterfaceType,
                          UINT1 u1IfOperStatus, UINT4 u4IfAddr)
{
    if (pLdpIfTableEntry->u1OperStatus != u1IfOperStatus)
    {
        u4IfAddr = OSIX_HTONL (u4IfAddr);
        MEMCPY ((UINT1 *) &pLdpIfTableEntry->IfAddr,
                (UINT1 *) &u4IfAddr, LDP_IPV4ADR_LEN);
        pLdpIfTableEntry->u1InterfaceType = u1InterfaceType;
        pLdpIfTableEntry->u1OperStatus = u1IfOperStatus;
    }
    return;
}

#ifdef MPLS_IPV6_WANTED
VOID
LdpUpdateLdpIpv6IfTableEntry (tLdpIfTableEntry * pLdpIfTableEntry,
                              UINT1 u1IfOperStatus,
                              UINT1 u1InterfaceType, UINT4 u4IfAddr,
                              tIp6Addr * pLnklocalIpv6Addr,
                              tIp6Addr * pSitelocalIpv6Addr,
                              tIp6Addr * pGlbUniqueIpv6Addr)
{
    if (pLdpIfTableEntry->u1OperStatus != u1IfOperStatus)
    {
        u4IfAddr = OSIX_HTONL (u4IfAddr);
        MEMCPY ((UINT1 *) &pLdpIfTableEntry->IfAddr,
                (UINT1 *) &u4IfAddr, LDP_IPV4ADR_LEN);
        pLdpIfTableEntry->u1InterfaceType = u1InterfaceType;
        pLdpIfTableEntry->u1OperStatus = u1IfOperStatus;
        MEMCPY (&pLdpIfTableEntry->LnklocalIpv6Addr, pLnklocalIpv6Addr,
                LDP_IPV6ADR_LEN);
        MEMCPY (&pLdpIfTableEntry->SitelocalIpv6Addr, pSitelocalIpv6Addr,
                LDP_IPV6ADR_LEN);
        MEMCPY (&pLdpIfTableEntry->GlbUniqueIpv6Addr, pGlbUniqueIpv6Addr,
                LDP_IPV6ADR_LEN);
    }
    return;
}

UINT1
LdpInterfaceIpv6Match (tIp6Addr * pLnklocalIpv6Addr,
                       tIp6Addr * pSitelocalIpv6Addr,
                       tIp6Addr * pGlbUniqueIpv6Addr)
{
    tIp6Addr            TempAddr;
    MEMSET (&TempAddr, 0, sizeof (tIp6Addr));

    if (MEMCMP (pLnklocalIpv6Addr->u1_addr, TempAddr.u1_addr, LDP_IPV6ADR_LEN)
        == LDP_ZERO)
    {
        if (MEMCMP
            (pSitelocalIpv6Addr->u1_addr, TempAddr.u1_addr,
             LDP_IPV6ADR_LEN) == LDP_ZERO)
        {
            if (MEMCMP
                (pGlbUniqueIpv6Addr->u1_addr, TempAddr.u1_addr,
                 LDP_IPV6ADR_LEN) == LDP_ZERO)
            {
                return LDP_FALSE;

            }
        }
    }
    return LDP_TRUE;
}
#endif

/*****************************************************************************/
/* Function Name : LdpProcessLdpInternalEvents                               */
/* Description   : This routine processes the internal Ldp Events.           */
/* Input(s)      : aTaskEnqMsgInfo - info about events and addresses         */
/* Output(s)     : None.                                                     */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpProcessLdpInternalEvents (tLdpIntEvtInfo * pIntEvtInfo)
{
    if (pIntEvtInfo->u4SubEvt == LDP_ESTB_LSPS_EVENT)
    {
        if (pIntEvtInfo->u2AddrType == LDP_ADDR_TYPE_IPV4)
        {
            LdpEstbLspsForAllRouteEntries ((tLdpSession *) (VOID *)
                                           pIntEvtInfo->pSession);
        }
#ifdef MPLS_IPV6_WANTED
        if (pIntEvtInfo->u2AddrType == LDP_ADDR_TYPE_IPV6)
        {
            LdpEstbLspsForAllIpv6RouteEntries ((tLdpSession *) (VOID *)
                                               pIntEvtInfo->pSession);
        }
#endif
    }
    else if (pIntEvtInfo->u4SubEvt == LDP_LBL_RES_AVL_EVENT)
    {
        LdpSendLblResAvlNotifMsg ((tLdpEntity *) (VOID *) pIntEvtInfo->pEntity);
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpCreateLdpEntity                                        */
/* Description   : This routine creates an LDP entity.                       */
/* Input(s)      : pLdpEntList - Pointer to list of LDP entities.            */
/*                 u2IncarnId - Incarnation Id                               */
/*                 LdpId  - Ldp Identifier                                   */
/*                 u4EntityIndex - EntityIndex                               */
/* Output(s)     : ppLdpEntity - Points to the created Ldp Entity            */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpCreateLdpEntity (tTMO_SLL * pLdpEntList, UINT2 u2IncarnId, tLdpId LdpId,
                    UINT4 u4EntityIndex, tLdpEntity ** ppLdpEntity)
{
    tLdpEntity         *pLdpEntity = NULL;

    pLdpEntity = (tLdpEntity *) MemAllocMemBlk (LDP_ENT_POOL_ID);

    if (pLdpEntity == NULL)
    {
        LDP_DBG (LDP_IF_MEM, "IF: Memory Allocation Failed for Ldp Entity\r\n");
        return LDP_FAILURE;
    }

    /* Initialising the Entity's MemBlock to Zero */
    MEMSET ((VOID *) pLdpEntity, LDP_ZERO, sizeof (tLdpEntity));

    /* Initialise entity */
    TMO_SLL_Init_Node (&(pLdpEntity->NextLdpEntity));
    TMO_SLL_Init (&pLdpEntity->IfList);
    TMO_SLL_Init (&pLdpEntity->PeerList);
    TMO_SLL_Init (&pLdpEntity->LdpEthParamsList);
#ifdef MPLS_LDP_BFD_WANTED
    TMO_SLL_Init (&pLdpEntity->BfdSessList);
#endif

    /* Assign the Ldp-Id and the Incarn Id */
    MEMCPY (pLdpEntity->LdpId, LdpId, LDP_MAX_LDPID_LEN);
    pLdpEntity->u2IncarnId = u2IncarnId;
    pLdpEntity->u4EntityIndex = u4EntityIndex;

    /* Initialising the other Ldp Entity fields with default values */
    pLdpEntity->u2TcpPort = LDP_STD_PORT;
    pLdpEntity->u2UdpPort = LDP_STD_PORT;
    pLdpEntity->u2MaxPduLen = LDP_DEF_MAX_PDU_LEN;
    pLdpEntity->u4EntityConfMtu = LDP_DEF_MTU;
    pLdpEntity->u4KeepAliveHoldTime = LDP_DEF_LSSN_HTIME;
    pLdpEntity->u2HelloHoldTime = LDP_ZERO;
    pLdpEntity->u2HelloConfHoldTime = LDP_ZERO;
    pLdpEntity->u4FailInitSessThreshold = LDP_MAX_RETRY_TIME;
    pLdpEntity->u4ConfSeqNum = LDP_ZERO;
#ifdef MPLS_LDP_BFD_WANTED
    pLdpEntity->i4BfdStatus = LDP_ZERO;
#endif

    pLdpEntity->u4MessageId = LDP_ZERO;
    pLdpEntity->u2LabelRangeId = LDP_ZERO;
    pLdpEntity->u1LabelSpaceType = LDP_PER_IFACE;
    pLdpEntity->u1LabelDistrType = LDP_DSTR_ON_DEMAND;
    pLdpEntity->u1LabelAllocType = LDP_ORDERED_MODE;
    pLdpEntity->u1EntityType = LDP_ATM_MODE;
    pLdpEntity->NetAddrType = IPV4;
    pLdpEntity->u1LblResAvl = LDP_TRUE;
    pLdpEntity->u2LabelRet = LDP_CONSERVATIVE_MODE;
    pLdpEntity->u2HopVecLimit = LDP_DEF_HOP_CNT_LIM;
    pLdpEntity->u2IntSessTrapEnable = LDP_DISABLED;
    pLdpEntity->u1PathVecLimit = LDP_ZERO;
    pLdpEntity->u1TransAddrTlvEnable = LDP_FALSE;
#ifdef MPLS_IPV6_WANTED
    pLdpEntity->u1Ipv6TransAddrTlvEnable = LDP_FALSE;
#endif
    pLdpEntity->bIsTransAddrIntfUp = LDP_FALSE;
    pLdpEntity->u1ConfSeqNumTlvEnable = LDP_FALSE;
    pLdpEntity->u1PathVecTrapCond = LDP_DISABLED;
    pLdpEntity->u1EntityIfStatus = LDP_IF_OPER_ENABLE;
    pLdpEntity->u1ConfChgFlag = LDP_FALSE;
    pLdpEntity->i1PhpConf = MPLS_PHP_DISABLED;
    pLdpEntity->u2ProtVersion = LDP_PROTO_VER;
    pLdpEntity->u1TransportAddrKind = INTERFACE_ADDRESS_TYPE;
    /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
    pLdpEntity->u1Ipv6TransportAddrKind = LOOPBACK_ADDRESS_TYPE;
    pLdpEntity->u1Ipv6TransAddrTlvEnable = LDP_FALSE;
#endif
    pLdpEntity->u1OperStatus = LDP_OPER_UNKNOWN;
    TMO_SLL_Insert (pLdpEntList, NULL, &(pLdpEntity->NextLdpEntity));
    *ppLdpEntity = pLdpEntity;
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDeleteLdpEntity                                        */
/* Description   : This routine deletes an LDP entity.                       */
/* Input(s)      : pLdpEntity  - Pointer to LDP entity to be deleted.        */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpDeleteLdpEntity (tLdpEntity * pLdpEntity)
{
    tTMO_SLL           *pList = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tAtmLdpLblRngEntry *pAtmLblRange = NULL;
    tLdpEthParams      *pEthParams = NULL;
    tLdpIfTableEntry   *pIfEntry = NULL;
    /* MPLS_IPv6 add start */
#ifdef MPLS_LDP_BFD_WANTED
    tLdpBfdSession     *pLdpBfdSession = NULL;
#endif
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            V6IfAddr;
#endif
    /* MPLS_IPv6 add end */
    UINT4               u4IfAddr = LDP_ZERO;
    UINT4               u4Count = LDP_ZERO;
    INT4                i4UdpSockFd = LDP_UDP_SOCKFD (LDP_CUR_INCARN);
    /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
    UINT4               u4Udp6SockFd =
        (UINT4) LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
#endif
    /* MPLS_IPv6 add end */
    UINT2               u2IncarnId = pLdpEntity->u2IncarnId;
    /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            AllRouterAddr;
    UINT1               u1ValidRouterAddr = LDP_FALSE;
    MEMSET (&V6IfAddr, 0, sizeof (tIp6Addr));
    MEMSET (&AllRouterAddr, 0, sizeof (tIp6Addr));
#endif

    /* MPLS_IPv6 add end */
    /* 
     * All the interfaces associated with this LdpEntity, leaving 
     * the Multicast group - LDP_ALL_ROUTERS_ADDR
     */
    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
    {
#ifdef MPLS_IPV6_WANTED
        if (LDP_FAILURE != INET_ATON6 (LDP_ALL_ROUTERS_V6ADDR, &AllRouterAddr))
        {
            u1ValidRouterAddr = LDP_TRUE;
        }
#endif
        TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity), pIfEntry,
                      tLdpIfTableEntry *)
        {
            /* MPLS_IPv6 mod start */
            if (LDP_IFTYPE_IPV4 == pIfEntry->u1InterfaceType
                || LDP_IFTYPE_DUAL == pIfEntry->u1InterfaceType)
            {
                MEMCPY (&u4IfAddr, LDP_ENTITY_IF_ADDRESS (pIfEntry),
                        sizeof (UINT4));
                u4IfAddr = OSIX_NTOHL (u4IfAddr);

                if (LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                  pIfEntry->u4IfIndex) >
                    LDP_ZERO)
                {
                    --(LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                     pIfEntry->u4IfIndex));
                }
                if (LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                  pIfEntry->u4IfIndex) ==
                    LDP_ZERO)
                {
                    if (LdpUdpLeaveMulticast
                        (i4UdpSockFd, u4IfAddr,
                         LDP_ALL_ROUTERS_ADDR) == LDP_FAILURE)
                    {
                        /* Error: Iface failed to leave the Mcast group */
                        continue;
                    }
                }
            }
            /* MPLS_IPv6 mod end */
            /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
            if (LDP_IFTYPE_IPV6 == pIfEntry->u1InterfaceType
                || LDP_IFTYPE_DUAL == pIfEntry->u1InterfaceType)
            {
                MEMCPY (V6IfAddr.u1_addr, pIfEntry->LnklocalIpv6Addr.u1_addr,
                        LDP_IPV6ADR_LEN);
                if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT
                    (LDP_CUR_INCARN, pIfEntry->u4IfIndex) > LDP_ZERO)
                {
                    --(LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                       pIfEntry->u4IfIndex));
                }
                if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                    pIfEntry->u4IfIndex) ==
                    LDP_ZERO)
                {
                    if (LDP_TRUE == u1ValidRouterAddr)
                    {
                        if (LdpUdpLeaveV6Multicast
                            ((INT4) u4Udp6SockFd, V6IfAddr,
                             &AllRouterAddr,
                             pIfEntry->u4IfIndex) == LDP_FAILURE)
                        {
                            /* Error: Iface failed to leave the Mcast group */
                            continue;
                        }
                    }
                    else
                    {
                        LDP_DBG (LDP_IF_MISC,
                                 "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
                        continue;
                    }
                }
            }
#endif
            /* MPLS_IPv6 add end */
        }
    }
    /* Stop the Hello Timer */
    TmrStopTimer (LDP_TIMER_LIST_ID,
                  (tTmrAppTimer *) & (pLdpEntity->HelloTimer.AppTimer));

    LdpHandleLdpUpDownEntityForLdpOverRsvp (pLdpEntity, LDP_ENTITY_DOWN);

    /* Delete and free the Local Peer List */
    pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
    while (pLdpPeer != NULL)
    {
        TmrStopTimer (LDP_TIMER_LIST_ID,
                      (tTmrAppTimer *) & (pLdpPeer->GrTimer.AppTimer));

        LDP_DBG (GRACEFUL_DEBUG, "LdpDeleteLdpEntity: "
                 "MPLS LDP Recovery/Reconnect Timer Stopped.\n");
#ifdef LDP_GR_WANTED
        if (pLdpPeer->u1GrProgressState == LDP_PEER_GR_RECONNECT_IN_PROGRESS)
        {
            pLdpPeer->u1GrProgressState = LDP_PEER_GR_NOT_STARTED;
        }
#endif
        LdpDeleteLdpPeer (pLdpPeer);
        pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
    }
    TMO_SLL_Init (&(pLdpEntity->PeerList));

    if (pLdpEntity->pEntityPeerPasswdInfo != NULL)
    {
        MemReleaseMemBlock (LDP_MAX_MD5PASSWD_POOL_ID,
                            (UINT1 *) pLdpEntity->pEntityPeerPasswdInfo);
    }

    /* Deleting the Ethernet Label Range List for the Entity */

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) == LDP_PER_PLATFORM)
    {
        pEthParams = (tLdpEthParams *)
            TMO_SLL_First (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity));
        while (pEthParams != NULL)
        {
            if ((pEthParams->u2EthLblRangeId) != 0)
            {
                ldpDeleteLabelSpaceGroup (pEthParams->u2EthLblRangeId);

                LDP_DBG4 ((LDP_SSN_ALL | LDP_SSN_PRCS),
                          "Label Range Id %d with label range %d %d is "
                          "released for the entity %d\n",
                          pEthParams->u2EthLblRangeId,
                          pEthParams->u4MinLabelVal, pEthParams->u4MaxLabelVal,
                          pLdpEntity->u4EntityIndex);
            }

            TMO_SLL_Delete (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                            &(pEthParams->NextRangeEntry));
            MemReleaseMemBlock (LDP_ETH_LBL_RNG_POOL_ID, (UINT1 *) pEthParams);

            pEthParams = (tLdpEthParams *)
                TMO_SLL_First (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity));
        }
        TMO_SLL_Init (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity));
    }

    /* Deleting the ATM Label Range List in case of ATM type Entity */

    if ((LDP_LBL_SPACE_TYPE (pLdpEntity) == LDP_PER_IFACE) &&
        (LDP_ENTITY_TYPE (pLdpEntity) == LDP_ATM_MODE))
    {

        if (pLdpEntity->pLdpAtmParams != NULL)
        {
            pAtmLblRange = (tAtmLdpLblRngEntry *)
                TMO_SLL_First (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity));
            while (pAtmLblRange != NULL)
            {
                TMO_SLL_Delete (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity),
                                &(pAtmLblRange->NextEntry));
                MemReleaseMemBlock (LDP_ATM_LBL_RNG_POOL_ID,
                                    (UINT1 *) pAtmLblRange);

                pAtmLblRange = (tAtmLdpLblRngEntry *)
                    TMO_SLL_First (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity));
            }
            TMO_SLL_Init (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity));
            MemReleaseMemBlock (LDP_ATM_PARMS_POOL_ID,
                                (UINT1 *) (pLdpEntity->pLdpAtmParams));
        }
    }
    /* Delete and free the Interface List of the Ldp Entity */
    pIfEntry = (tLdpIfTableEntry *) TMO_SLL_First (&(pLdpEntity->IfList));
    while (pIfEntry != NULL)
    {
        /* Changed for NP - End */

        /* Releasing the MD5 password associated with the If Entry */
        if (pIfEntry->pEntityPeerPasswdInfo != NULL)
        {
            MemReleaseMemBlock (LDP_MAX_MD5PASSWD_POOL_ID,
                                (UINT1 *) (pIfEntry->pEntityPeerPasswdInfo));
        }
        TMO_SLL_Delete (&(pLdpEntity->IfList), &(pIfEntry->NextIfEntry));
        MemReleaseMemBlock (LDP_EIF_POOL_ID, (UINT1 *) pIfEntry);
        pIfEntry = (tLdpIfTableEntry *) TMO_SLL_First (&(pLdpEntity->IfList));
    }
    TMO_SLL_Init (&(pLdpEntity->IfList));

    /* Delete and free the bfdSession  List of the Ldp Entity */

#ifdef MPLS_LDP_BFD_WANTED
    pLdpBfdSession =
        (tLdpBfdSession *) TMO_SLL_First (&(pLdpEntity->BfdSessList));
    while (pLdpBfdSession != NULL)
    {
        LdpDeRegisterDisabledBfdSess (pLdpBfdSession);
        TMO_SLL_Delete (&(pLdpEntity->BfdSessList),
                        &(pLdpBfdSession->NextBfdSess));
        MemReleaseMemBlock (LDP_BFD_SSN_POOL_ID, (UINT1 *) pLdpBfdSession);
        pLdpBfdSession =
            (tLdpBfdSession *) TMO_SLL_First (&(pLdpEntity->BfdSessList));
    }
    TMO_SLL_Init (&(pLdpEntity->BfdSessList));

#endif

    /* Deleting the LdpEntity from LDP Entity List and Releasing the Memory */
    TMO_SLL_Delete (&LDP_ENTITY_LIST (u2IncarnId),
                    &(pLdpEntity->NextLdpEntity));
    pList = &LDP_ENTITY_LIST (u2IncarnId);
    u4Count = TMO_SLL_Count (pList);
    if (u4Count == LDP_ZERO)

    {
        /*Stop Fwd Entry Holding Timer if no Entity is present in the 
         * system*/
        TmrStopTimer (LDP_TIMER_LIST_ID,
                      (tTmrAppTimer *) & (gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].
                                          FwdHoldingTmr));
        if (gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus
            != LDP_GR_SHUT_DOWN_IN_PROGRESS)
        {
            gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus =
                LDP_GR_NOT_STARTED;
        }
        LDP_DBG (GRACEFUL_DEBUG, "LdpDeleteLdpEntity: "
                 "Stopped MPLS Forwarding Hold Timer.\n");

    }
    LdpEntityTableRelIndex (pLdpEntity->u4EntityIndex);
    MemReleaseMemBlock (LDP_ENT_POOL_ID, (UINT1 *) pLdpEntity);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDeleteLdpPeer                                          */
/* Description   : Deletes the Ldp peer and it's associated resources.       */
/*                 This routine do not free the Ldp Peer Node.               */
/*                                                                           */
/* Input(s)      : pLdpPeer - Points to the Ldp Peer that needs to be deleted*/
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpDeleteLdpPeer (tLdpPeer * pLdpPeer)
{
    tTMO_SLL           *pList = NULL;
    tLdpSession        *pSessionEntry = NULL;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
#ifdef LDP_GR_WANTED
    UINT4               u4RemainingTime = LDP_ZERO;
#endif

#ifdef MPLS_IPV6_WANTED
    if (LDP_IPV6_SSN_PREF_TMR_STARTED == pLdpPeer->u1SsnPrefTmrStatus)
    {
        TmrStopTimer (LDP_TIMER_LIST_ID,
                      (tTmrAppTimer *) & (pLdpPeer->Ipv6SessPrefTimer.
                                          AppTimer));
        pLdpPeer->u1SsnPrefTmrStatus = LDP_IPV6_SSN_PREF_TMR_NSTARTED;
        LDP_DBG2 (LDP_SSN_PRCS, "LdpDeleteLdpPeer: "
                  "Stopped (Marked NSTARTED) session preference timer for Peer:0x%x, Session: 0x%x\n",
                  pLdpPeer, pLdpPeer->pLdpSession);
    }
#endif

    /* Delete Sessions associated to this Peer */
    pSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;
    if (pSessionEntry != NULL)
    {
        /* 
         * Session being deleted by sending Internal Destroy Event to 
         * LDP Session State Machine.
         */
        LDP_SESSION_FSM[SSN_STATE (pSessionEntry)] (pSessionEntry,
                                                    LDP_SSM_EVT_INT_DESTROY,
                                                    NULL);
    }
    pLdpPeer->pLdpSession = NULL;

#ifdef LDP_GR_WANTED
    /* Stopping the GR Timer if running */
    if (TmrGetRemainingTime (LDP_TIMER_LIST_ID,
                             &(pLdpPeer->GrTimer.AppTimer),
                             &u4RemainingTime) == TMR_FAILURE)
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpDeleteLdpPeer: Failed to get the "
                 "Remaining Time for the GR Timer\n");
        return LDP_FAILURE;
    }

    if (u4RemainingTime > 0)
    {
        LDP_DBG (GRACEFUL_DEBUG, "LdpDeleteLdpPeer: GR Timer Stopped\n");
        TmrStopTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *) & (pLdpPeer->GrTimer.
                                                             AppTimer));
    }
#endif

    /* Delete Atm Label Range List */
    pAtmLblRangeEntry =
        (tAtmLdpLblRngEntry *) TMO_SLL_First (&(pLdpPeer->AtmLblRangeList));
    while (pAtmLblRangeEntry != NULL)
    {
        TMO_SLL_Delete (&(pLdpPeer->AtmLblRangeList),
                        &(pAtmLblRangeEntry->NextEntry));
        MemReleaseMemBlock (LDP_ATM_LBL_RNG_POOL_ID,
                            (UINT1 *) pAtmLblRangeEntry);
        pAtmLblRangeEntry =
            (tAtmLdpLblRngEntry *) TMO_SLL_First (&(pLdpPeer->AtmLblRangeList));
    }
    TMO_SLL_Init (&(pLdpPeer->AtmLblRangeList));

    pList = LDP_GET_PEER_LIST (pLdpPeer);
    /*   Updating the statistics of peers in Entity */
    if (pLdpPeer->u1RemoteFlag == LDP_REMOTE_PEER)
    {
        LDP_ENTITY_ESTB_REMOTE_PEERS (pLdpPeer->pLdpEntity)--;
    }
    else
    {
        LDP_ENTITY_ESTB_LOCAL_PEERS (pLdpPeer->pLdpEntity)--;
    }

    /* Deleting the LdpPeer from LDP Peer List and Releasing the Memory */
    TMO_SLL_Delete (pList, &(pLdpPeer->NextLdpPeer));
    LDP_DBG1 (LDP_SSN_PRCS, "LdpDeleteLdpPeer: "
              "Deleting Peer:0x%x\n", pLdpPeer);

    MemReleaseMemBlock (LDP_PEER_POOL_ID, (UINT1 *) pLdpPeer);
    MplsLdpPeerUpdateSysTime ();
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpCreateCrlspTnl                                         */
/* Description   : This routine creates CRLSP Tunnel.                        */
/* Input(s)      : u2IncarnId - Incarnation Id                               */
/*                 u4CrlspTnlIndex - Tunnel Index                            */
/* Output(s)     : ppCrlspTnl - Points to the created Crlsp Tunnel           */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpCreateCrlspTnl (UINT2 u2IncarnId,
                   UINT4 u4CrlspTnlIndex, tCrlspTnlInfo ** ppCrlspTnl)
{
    UINT4               u4HIndex;
    tTMO_HASH_TABLE    *pCrlspTnlHashtbl = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;
    UINT1               u1MaxPscCount = 0;
    /* CRLSP Tunnel structure is obtained from mem pool */
    pCrlspTnlInfo = (tCrlspTnlInfo *) MemAllocMemBlk (LDP_CRLSPTNL_POOL_ID);

    if (pCrlspTnlInfo == NULL)
    {
        LDP_DBG (LDP_IF_MEM,
                 "Memory Allocation failed for Crlsp Tunnel Create\r\n");
        return LDP_FAILURE;
    }

    /* Initialising the CrLspTnl's MemBlock to Zero */
    MEMSET ((VOID *) pCrlspTnlInfo, LDP_ZERO, sizeof (tCrlspTnlInfo));

    /* Initialise tunnel fields */
    LDP_INIT_CRLSP_TNL_INFO (pCrlspTnlInfo);

    /* Assigning the LSP ID */
    CRLSP_LSPID (pCrlspTnlInfo) = (UINT2) u4CrlspTnlIndex;

    /* A Mib variable for Assigning flags is to be defined in the Crlsp Table */
    CRLSP_PARM_FLAG (pCrlspTnlInfo) |= (LDP_ER_CRLSP | LDP_TR_CRLSP |
                                        LDP_PREMPT_CRLSP | LDP_PN_CRLSP);

    u4HIndex = LDP_COMPUTE_HASH_INDEX ((UINT4) (u4CrlspTnlIndex));
    pCrlspTnlHashtbl = LDP_CRLSP_TNL_TBL (u2IncarnId);
    TMO_HASH_Add_Node (pCrlspTnlHashtbl,
                       (tTMO_HASH_NODE *) (&(pCrlspTnlInfo->HashLinkNode)),
                       u4HIndex, NULL);

    *ppCrlspTnl = pCrlspTnlInfo;
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpGetLdpEntity                                           */
/* Description   : Gets the entity corresponding to the given Incarnation Id */
/*                 and LdpId.                                                */
/*                                                                           */
/* Input(s)      : u4IncarnId - Incarnation Id.                              */
/*                 LdpId      - Ldp Identifier of the Entity that is to be   */
/*                              returned.                                    */
/*                 u4EntityIndex - Entities Index                            */
/* Output(s)     : ppLdpEntity - Ldp Entity corresponding to the given       */
/*                               Incarnation Id and LdpId.                   */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpGetLdpEntity (UINT4 u4IncarnId, tLdpId LdpId, UINT4 u4EntityIndex,
                 tLdpEntity ** ppLdpEntity)
{
    tTMO_SLL           *pList = &LDP_ENTITY_LIST (u4IncarnId);
    tLdpEntity         *pLdpEntity = NULL;

    if (LDP_INITIALISED != TRUE)
    {
        return LDP_FAILURE;
    }
    TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
    {
        if ((MEMCMP (LdpId, pLdpEntity->LdpId, LDP_MAX_LDPID_LEN) == 0)
            && (pLdpEntity->u4EntityIndex) == u4EntityIndex)
        {
            *ppLdpEntity = pLdpEntity;
            return LDP_SUCCESS;
        }
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetLdpPeer                                             */
/* Description   : Gets Ldp Peer   corresponding to the given Incarnation Id */
/*                 LdpId, and PeerId.                                        */
/*                                                                           */
/* Input(s)      : u4IncarnId - Incarnation Id.                              */
/*                 LdpId      - Ldp Identifier of the Entity that is to be   */
/*                              returned.                                    */
/*                 u4EntityIndex - Entities Index                            */
/*                 PeerIndex  - Index into the peer table maintained by the  */
/*                              Ldp Entity specified by the LdpId.           */
/* Output(s)     : ppLdpPeer   - Ldp Peer   corresponding to the given       */
/*                               Incarnation Id, LdpId and Peer Index.       */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpGetLdpPeer (UINT4 u4IncarnId, tLdpId LdpId, UINT4 u4EntityIndex,
               tLdpId PeerId, tLdpPeer ** ppLdpPeer)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;

    if (LdpGetLdpEntity (u4IncarnId, LdpId, u4EntityIndex, &pLdpEntity) !=
        LDP_SUCCESS)
    {
        return LDP_FAILURE;
    }

    /* Scan through the Peer List for peer corresponding to the Peer
     * Index. */
    pList = &pLdpEntity->PeerList;

    TMO_SLL_Scan (pList, pLdpPeer, tLdpPeer *)
    {
        if (MEMCMP (PeerId, pLdpPeer->PeerLdpId, LDP_MAX_LDPID_LEN) == 0)
        {
            *ppLdpPeer = pLdpPeer;
            return LDP_SUCCESS;
        }
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetLdpBfdSession                                         */
/* Description   : Gets Ldp Bfd Session corresponding to the given LdpSession */
/*                 TcpConnId                                               */
/*                                                                           */
/* Input(s)      : UINT4 -  u4TcpConnId                                      */
/*                 tLdpEntity *pLdpEntity                                    */
/* Output(s)     : ppLdpBfdSession   - Ldp Bfd Session corresponding to the given       */
/*                               LdpSession TcpConnId.                       */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
#ifdef MPLS_LDP_BFD_WANTED

UINT1
LdpGetLdpBfdSession (tLdpEntity * pLdpEntity, UINT4 u4TcpConnId,
                     tLdpBfdSession ** ppLdpBfdSession)
{
    tTMO_SLL           *pList = NULL;
    tLdpBfdSession     *pLdpBfdSession = NULL;

    /* Scan through the Bfd Ldp List for peer corresponding to the u4TcpConnId
     * Index. */
    pList = &pLdpEntity->BfdSessList;

    TMO_SLL_Scan (pList, pLdpBfdSession, tLdpBfdSession *)
    {
        if (NULL != pLdpBfdSession->pLdpSession)
        {
            if (u4TcpConnId == pLdpBfdSession->pLdpSession->u4TcpConnId)
            {
                *ppLdpBfdSession = pLdpBfdSession;
                return LDP_SUCCESS;
            }
        }
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetDisabledLdpBfdSession                               */
/* Description   : Gets Ldp Bfd Session corresponding to the given Source and*/
/*                 Destination address                                       */
/*                                                                           */
/* Input(s)      : UINT4 -                                                   */
/*                 tLdpEntity *pLdpEntity                                    */
/*                 tGenAddr DestAddr                                         */
/*                 tGenAddr  SrcAddr                                         */
/*                 UINT1 u1AddrType                                             */
/* Output(s)     : ppLdpBfdSession-Ldp Bfd Session corresponding to the given*/
/*                                  LdpSession TcpConnId.                    */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpGetDisabledLdpBfdSession (tLdpEntity * pLdpEntity, tGenAddr DestAddr,
                             tGenAddr SrcAddr, UINT1 u1AddrType,
                             tLdpBfdSession ** ppLdpBfdSession)
{
    tTMO_SLL           *pList = NULL;
    tLdpBfdSession     *pLdpBfdSession = NULL;

    LDP_DBG2 (LDP_MAIN_ALL, "%s: Line: %d Entered\n", __func__, __LINE__);
    /* Scan through the Bfd Ldp List for peer corresponding to the u4TcpConnId
     * Index. */
    pList = &pLdpEntity->BfdSessList;
#ifndef MPLS_IPV6_WANTED
    UNUSED_PARAM (u1AddrType);
#endif

    TMO_SLL_Scan (pList, pLdpBfdSession, tLdpBfdSession *)
    {
#ifdef MPLS_IPV6_WANTED
        if (u1AddrType == LDP_ADDR_TYPE_IPV6)
        {

            if ((MEMCMP
                 (&pLdpBfdSession->DestAddr.Addr.Ip6Addr,
                  &DestAddr.Addr.Ip6Addr, LDP_IPV6ADR_LEN) == LDP_ZERO)
                &&
                (MEMCMP
                 (&pLdpBfdSession->SrcAddr.Addr.Ip6Addr, &SrcAddr.Addr.Ip6Addr,
                  LDP_IPV6ADR_LEN) == LDP_ZERO)
                && (pLdpBfdSession->pLdpSession == NULL))
            {
                *ppLdpBfdSession = pLdpBfdSession;
                LDP_DBG5 (LDP_MAIN_ALL,
                          "%s : Line:%d returned LDP_SUCCESS with DST: %s, SRC: %s, IFIndex: %d\n",
                          __func__, __LINE__,
                          Ip6PrintAddr (&
                                        (pLdpBfdSession->DestAddr.Addr.
                                         Ip6Addr)),
                          Ip6PrintAddr (&
                                        (pLdpBfdSession->SrcAddr.Addr.Ip6Addr)),
                          pLdpBfdSession->u4IfIndex);

                return LDP_SUCCESS;
            }
        }

        else
        {
#endif
            if ((MEMCMP (&pLdpBfdSession->DestAddr.Addr.au1Ipv4Addr,
                         &DestAddr.Addr.au1Ipv4Addr,
                         LDP_IPV4ADR_LEN) == LDP_ZERO)
                &&
                (MEMCMP
                 (&pLdpBfdSession->SrcAddr.Addr.au1Ipv4Addr,
                  &SrcAddr.Addr.au1Ipv4Addr, LDP_IPV4ADR_LEN) == LDP_ZERO)
                && (pLdpBfdSession->pLdpSession == NULL))
            {
                *ppLdpBfdSession = pLdpBfdSession;
                LDP_DBG6 (LDP_MAIN_ALL,
                          "\n%s : Line:%d returned LDP_SUCCESS with Dst: %x.%x.%x.%x\n",
                          __func__, __LINE__,
                          pLdpBfdSession->DestAddr.Addr.au1Ipv4Addr[0],
                          pLdpBfdSession->DestAddr.Addr.au1Ipv4Addr[1],
                          pLdpBfdSession->DestAddr.Addr.au1Ipv4Addr[2],
                          pLdpBfdSession->DestAddr.Addr.au1Ipv4Addr[3]);

                LDP_DBG5 (LDP_MAIN_ALL, "\nSRC: %x.%x.%x.%x IFIndex: %d",
                          pLdpBfdSession->SrcAddr.Addr.au1Ipv4Addr[0],
                          pLdpBfdSession->SrcAddr.Addr.au1Ipv4Addr[1],
                          pLdpBfdSession->SrcAddr.Addr.au1Ipv4Addr[2],
                          pLdpBfdSession->SrcAddr.Addr.au1Ipv4Addr[3],
                          pLdpBfdSession->u4IfIndex);

                return LDP_SUCCESS;
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }
    LDP_DBG2 (LDP_MAIN_ALL, "%s: Line: %d Returned FAILURE\n", __func__,
              __LINE__);
    return LDP_FAILURE;
}
#endif
/*****************************************************************************/
/* Function Name : LdpGetLdpSession                                          */
/* Description   : Gets Ldp Session corresponding to the given Incarnation   */
/*                 Id, LdpId, PeerId and Session Id.                         */
/*                                                                           */
/* Input(s)      : u4IncarnId - Incarnation Id.                              */
/*                 LdpId      - Ldp Identifier of the Entity that is to be   */
/*                              returned.                                    */
/*                 u4EntityIndex - Entieies Index                            */
/*                                                                           */
/*                 Peer Id    - Peer's Id                                    */
/*                                                                           */
/* Output(s)     : ppLdpSession   - Ldp Ssn    corresponding to the given    */
/*                               Incarnation Id, LdpId,EntityIndex, PeerID   */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpGetLdpSession (UINT4 u4IncarnId,
                  tLdpId LdpId,
                  UINT4 u4EntityIndex,
                  tLdpId PeerId, tLdpSession ** ppLdpSession)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;

    /* Get Local/Remote Peer based on given peer Index */
    if (LdpGetLdpPeer (u4IncarnId,
                       LdpId, u4EntityIndex, PeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return LDP_FAILURE;
    }

    /* Scan through the Session List for peer corresponding to the session
     * Index. */

    pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;
    if (pLdpSession != NULL)
    {
        *ppLdpSession = pLdpSession;
        return LDP_SUCCESS;
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetLdpAdjacency                                        */
/* Description   : Gets Ldp Adjacency corresponding to the given Incarnation */
/*                 Id, LdpId, PeerId and Session Id.                         */
/*                                                                           */
/* Input(s)      : u4IncarnId - Incarnation Id.                              */
/*                 LdpId      - Ldp Identifier of the Entity that is to be   */
/*                              returned.                                    */
/*                 u4EntityIndex - Entities Index                            */
/*                 PeerId       - Peer's Ldp Id                              */
/*                 u4 Adjacency Index                                        */
/*                              - Unique Index for the Adjacency Entry       */
/* Output(s)     : ppLdpAdjacency  - Ldp Peer   corresponding to the given   */
/*                               Incarnation Id, LdpId and Peer Index.       */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpGetLdpAdjacency (UINT4 u4IncarnId,
                    tLdpId LdpId,
                    UINT4 u4EntityIndex,
                    tLdpId PeerId,
                    UINT4 u4AdjacencyIndex, tLdpAdjacency ** ppLdpAdjacency)
{
    tTMO_SLL           *pList = NULL;
    tLdpAdjacency      *pLdpAdjacency = NULL;
    tLdpSession        *pLdpSession = NULL;

    /* Get Local/Remote Peer based on given peer Index */
    if (LdpGetLdpSession (u4IncarnId,
                          LdpId,
                          u4EntityIndex, PeerId, &pLdpSession) != LDP_SUCCESS)
    {
        return LDP_FAILURE;
    }

    /* Scan through the Adjacency List for peer corresponding to the Adjacency
     * Index. */
    pList = &pLdpSession->AdjacencyList;

    TMO_SLL_Scan (pList, pLdpAdjacency, tLdpAdjacency *)
    {
        if (pLdpAdjacency->u4Index == u4AdjacencyIndex)
        {
            *ppLdpAdjacency = pLdpAdjacency;
            return LDP_SUCCESS;
        }
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetCrLsp                                               */
/* Description   : Gets the Cr-Lsp corresponding to the given Incarnation Id,*/
/*                 and Cr-Lsp Tunnel Index.                                  */
/*                                                                           */
/* Input(s)      : u4IncarnId - Incarnation Id.                              */
/*                 u4IngressId - Router Address of the Lsr, which is acting  */
/*                 i4CrLspIndex - Cr-Lsp Tunnel Index.                       */
/*                 u1CrlspTnlInstance - CrLsp Tunnel Instance                */
/* Output(s)     : ppCrlspTnlInfo - Informatin about the tunnel matching the */
/*                                  given indices - u4IngressId, i4CrLspIndex*/
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpGetCrLsp (UINT4 u4IncarnId, UINT4 u4IngressId, INT4 i4CrLspIndex,
             UINT1 u1CrlspTnlInstance, tCrlspTnlInfo ** ppCrlspTnlInfo)
{
    UINT4               u4HIndex;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;

    u4IngressId = OSIX_HTONL (u4IngressId);

    /* Validate the Tunnel Index. */
    u4HIndex = (UINT4) LDP_COMPUTE_HASH_INDEX (i4CrLspIndex);

    /* Searching in the Cr-Lsp Tunnel Table */
    TMO_HASH_Scan_Bucket (LDP_CRLSP_TNL_TABLE (u4IncarnId),
                          u4HIndex, pCrlspTnlInfo, tCrlspTnlInfo *)
    {

        if (((UINT4) i4CrLspIndex == CRLSP_TNL_INDEX (pCrlspTnlInfo)) &&
            (MEMCMP ((UINT1 *) &u4IngressId,
                     CRLSP_INGRESS_LSRID (pCrlspTnlInfo),
                     LDP_IPV4ADR_LEN) == 0) &&
            (CRLSP_TNL_INSTANCE (pCrlspTnlInfo) == (UINT4) u1CrlspTnlInstance))
        {
            *ppCrlspTnlInfo = pCrlspTnlInfo;
            return LDP_SUCCESS;
        }
    }
    /* No Cr-Lsp Tunnel Entry found for the given indices */
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetLdpEntityIfEntry                                    */
/* Description   : Gets the Ldp Entity Interface Entry for the given Index   */
/* Input(s)      : u4IncarnId - Incarnation Id.                              */
/*                 pLdpEntity - Pointer to the LDP Entity                    */
/*                 u4IfIndex  - Interface Index which to be matched          */
/* Output(s)     : ppLdpIfTableEntry - If Table Entry coresponding to the    */
/*                                     given Index.                          */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpGetLdpEntityIfEntry (UINT4 u4IncarnId, tLdpEntity * pLdpEntity,
                        UINT4 u4IfIndex, tLdpIfTableEntry ** ppLdpIfTableEntry)
{
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;

    UNUSED_PARAM (u4IncarnId);

    TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity), pLdpIfTableEntry,
                  tLdpIfTableEntry *)
    {
        if (LDP_ENTITY_IF_INDEX (pLdpIfTableEntry) == u4IfIndex)
        {
            *ppLdpIfTableEntry = pLdpIfTableEntry;
            return LDP_SUCCESS;
        }
    }
    /* No Entity IF Entry present for the given index */
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetLdpEntityEthParams                                   */
/* Description   : Gets the Ldp Entity EthParams Entry for the given label   */
/*                 range and LDP Entity                                      */
/* Input(s)      : u4IncarnId      - Incarnation Id - Currently value        */
/*                                   supported is MPLS_DEF_INCARN.           */
/*                 u4EntityIfIndex - Entity Interface Index.                 */
/*                 u4MinLabel      - Minimum Label.                          */
/*                 u4MaxLabel      - Maximum Label.                          */
/* Output(s)     : ppLdpIfTableEntry - If Table Entry coresponding to the    */
/*                                     given Index.                          */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpGetLdpEntityEthParams (UINT4 u4IncarnId, tLdpEntity * pLdpEntity,
                          UINT4 u4MinLabel, UINT4 u4MaxLabel,
                          tLdpEthParams ** pLdpEthEntry)
{
    tLdpEthParams      *pLdpEthParams = NULL;

    UNUSED_PARAM (u4IncarnId);

    TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                  pLdpEthParams, tLdpEthParams *)
    {
        if ((pLdpEthParams->u4MinLabelVal == u4MinLabel) &&
            (pLdpEthParams->u4MaxLabelVal == u4MaxLabel))
        {
            *pLdpEthEntry = pLdpEthParams;
            return LDP_SUCCESS;
        }
    }

    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpDownLdpEntity                                          */
/* Description   : This routine brings  the entity to not in service state   */
/* Input(s)      : pLdpEntity - Pointer to the Entity to be brought down.    */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpDownLdpEntity (tLdpEntity * pLdpEntity)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpEthParams      *pEthParams = NULL;
    tLdpIfTableEntry   *pIfEntry = NULL;
    /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            V6IfAddr;
#endif
    INT4                i4UdpSockFd = LDP_UDP_SOCKFD (LDP_CUR_INCARN);
    UINT4               u4IfAddr = LDP_ZERO;
    /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
    UINT4               u4Udp6SockFd =
        (UINT4) LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
    UINT1               u1ValidRouterAddr = LDP_FALSE;
    tIp6Addr            AllRouterAddr;
    MEMSET (&V6IfAddr, 0, sizeof (tIp6Addr));
    MEMSET (&AllRouterAddr, 0, sizeof (tIp6Addr));
#endif
    /* MPLS_IPv6 add end */

    LDP_DBG2 ((LDP_SSN_PRCS | LDP_SSN_ALL), "%s: %d ENTRY\n", __func__,
              __LINE__);

    if (pLdpEntity->u1OperStatus == LDP_OPER_DISABLED)
    {
        LDP_DBG1 ((LDP_SSN_PRCS | LDP_SSN_ALL),
                  "\rEntity %d already Oper Enabled\n",
                  pLdpEntity->u4EntityIndex);
        return LDP_SUCCESS;
    }
    /* 
     * All the interfaces associated with this LdpEntity, leaving 
     * the Multicast group - LDP_ALL_ROUTERS_ADDR
     */
    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
    {
#ifdef MPLS_IPV6_WANTED
        if (LDP_FAILURE != INET_ATON6 (LDP_ALL_ROUTERS_V6ADDR, &AllRouterAddr))
        {
            u1ValidRouterAddr = LDP_TRUE;
        }
#endif
        TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity), pIfEntry,
                      tLdpIfTableEntry *)
        {
            /* MPLS_IPv6 mod start */
            if (LDP_IFTYPE_IPV4 == pIfEntry->u1InterfaceType
                || LDP_IFTYPE_DUAL == pIfEntry->u1InterfaceType)
            {
                MEMCPY (&u4IfAddr, LDP_ENTITY_IF_ADDRESS (pIfEntry),
                        sizeof (UINT4));
                u4IfAddr = OSIX_NTOHL (u4IfAddr);
                if (pIfEntry->u1OperStatus == LDP_IF_OPER_ENABLE)
                {
                    if (LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                      pIfEntry->u4IfIndex) >
                        LDP_ZERO)
                    {
                        --(LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                         pIfEntry->u4IfIndex));
                    }
                    if (LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                      pIfEntry->u4IfIndex) ==
                        LDP_ZERO)
                    {
                        if (LdpUdpLeaveMulticast (i4UdpSockFd, u4IfAddr,
                                                  LDP_ALL_ROUTERS_ADDR) ==
                            LDP_FAILURE)
                        {
                            /* Error: Iface failed to leave the Mcast group */
                            continue;
                        }
                    }
                }
            }
            /* MPLS_IPv6 mod end */
            /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
            if (LDP_IFTYPE_IPV6 == pIfEntry->u1InterfaceType
                || LDP_IFTYPE_DUAL == pIfEntry->u1InterfaceType)
            {
                MEMCPY (V6IfAddr.u1_addr, pIfEntry->LnklocalIpv6Addr.u1_addr,
                        LDP_IPV6ADR_LEN);
                if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT
                    (LDP_CUR_INCARN, pIfEntry->u4IfIndex) > LDP_ZERO)
                {
                    --(LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                       pIfEntry->u4IfIndex));
                }
                if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                    pIfEntry->u4IfIndex) ==
                    LDP_ZERO)
                {
                    if (LDP_TRUE == u1ValidRouterAddr)
                    {
                        if (LdpUdpLeaveV6Multicast
                            ((INT4) u4Udp6SockFd, V6IfAddr,
                             &AllRouterAddr,
                             pIfEntry->u4IfIndex) == LDP_FAILURE)
                        {
                            /* Error: Iface failed to leave the Mcast group */
                            continue;
                        }
                    }
                    else
                    {
                        LDP_DBG (LDP_IF_MISC,
                                 "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
                        continue;
                    }
                }
            }
#endif
            /* MPLS_IPv6 add end */
        }
    }
    /* Stop the Hello Timer */
    TmrStopTimer (LDP_TIMER_LIST_ID,
                  (tTmrAppTimer *) & (pLdpEntity->HelloTimer.AppTimer));
    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
    {
        /* Delete and free the Local Peer List */
        pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
        while (pLdpPeer != NULL)
        {
#ifdef LDP_GR_WANTED
            if (pLdpPeer->u1GrProgressState ==
                LDP_PEER_GR_RECONNECT_IN_PROGRESS)
            {
                pLdpPeer->u1GrProgressState = LDP_PEER_GR_NOT_STARTED;
            }
#endif
            LdpDeleteLdpPeer (pLdpPeer);
            pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
        }
    }
    else
    {
        pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
        if (pLdpPeer == NULL)
        {
            return LDP_FAILURE;
        }
        if (pLdpPeer->pLdpSession != NULL)
        {
            LDP_SESSION_FSM[SSN_STATE (pLdpPeer->pLdpSession)]
                (pLdpPeer->pLdpSession, LDP_SSM_EVT_INT_DESTROY, NULL);
        }
        pLdpPeer->pLdpSession = NULL;
        MEMSET ((UINT1 *) pLdpPeer->TransAddr.Addr.au1Ipv4Addr, LDP_ZERO,
                LDP_IPV4ADR_LEN);
        MEMSET (pLdpPeer->PeerLdpId, LDP_ZERO, LDP_MAX_LDPID_LEN);
    }
    /* Deleting the Ethernet Label Ranges Allocated */

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) == LDP_PER_PLATFORM)
    {

        /* Changed for NP - End */

        TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                      pEthParams, tLdpEthParams *)
        {
            if (pEthParams->u2EthLblRangeId != LDP_ZERO)
            {
                ldpDeleteLabelSpaceGroup (pEthParams->u2EthLblRangeId);
            }
            pEthParams->u2EthLblRangeId = LDP_ZERO;
        }
    }

    /* Initialising the LdpEntity Statistics */
    MEMSET ((VOID *) &(pLdpEntity->LdpStatsList), LDP_ZERO,
            sizeof (tLdpEntityStats));

    pLdpEntity->u1OperStatus = LDP_OPER_DISABLED;
    LDP_DBG2 ((LDP_SSN_PRCS | LDP_SSN_ALL), "%s: %d EXIT\n", __func__,
              __LINE__);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpUpLdpEntity                                            */
/* Description   : This routine brings the entity to active state            */
/* Input(s)      : pLdpEntity - Pointer to the Entity to be brought up.    */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpUpLdpEntity (tLdpEntity * pLdpEntity)
{
    tLdpEthParams      *pEthParams = NULL;
    tLdpIfTableEntry   *pIfEntry = NULL;
    tKeyInfoStruct      LblRangeInfo;
    /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            Ipv6IfAddr;
    tIp6Addr            TmpIpv6IfAddr;
#endif
    /* MPLS_IPv6 add end */
    UINT1              *pu1MD5Passwd = NULL;
    UINT4               u4ModuleID = LDP_ZERO;
    UINT4               u4IfAddr = LDP_ZERO;
    UINT1               u1LblRngeAllocated = LDP_FALSE;
    INT4                i4UdpSockFd = LDP_UDP_SOCKFD (LDP_CUR_INCARN);
    INT4                i4MD5KeyCount = LDP_ZERO;
    /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
    INT4                i4Udp6SockFd = LDP_ZERO;
#endif
    UINT1               u1InterfaceType;
#ifdef MPLS_IPV6_WANTED
    MEMSET (&Ipv6IfAddr, 0, sizeof (tIp6Addr));
    MEMSET (&TmpIpv6IfAddr, 0, sizeof (tIp6Addr));
#endif
    /* MPLS_IPv6 add end */

    if (LdpCheckIfEntityBeActivated (pLdpEntity) != TRUE)
    {
        LDP_DBG5 ((LDP_SSN_PRCS | LDP_SSN_ALL),
                  "\r%s: %d Entity cannot be Activated as\n"
                  "pLdpEntity->u1AdminStatus = %d\n"
                  "pLdpEntity->u1RowStatus = %d\n"
                  "pLdpEntity->u1OperStatus = %d\n",
                  __func__, __LINE__, pLdpEntity->u1AdminStatus,
                  pLdpEntity->u1RowStatus, pLdpEntity->u1OperStatus);
        return LDP_FAILURE;
    }

    /* LdpUpLdpEntity may be called if Tunnel is UP for LDP over RSVP case.
     * Here, just associate the Entity Index with the Tunnel and
     * If Entity OperStatus is already Enabled just return. */
    LdpHandleLdpUpDownEntityForLdpOverRsvp (pLdpEntity, LDP_ENTITY_UP);

    if (pLdpEntity->u1OperStatus == LDP_OPER_ENABLED)
    {
        LDP_DBG1 ((LDP_SSN_PRCS | LDP_SSN_ALL),
                  "\rEntity %d already Oper Enabled\n",
                  pLdpEntity->u4EntityIndex);
        return LDP_SUCCESS;
    }

    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        LDP_LBL_SPACE_TYPE (pLdpEntity) = LDP_PER_PLATFORM;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) == LDP_PER_PLATFORM)
    {
        /* Initialising the Label Range Information */
        TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                      pEthParams, tLdpEthParams *)
        {
            LblRangeInfo.u4Key1Min = 0;
            LblRangeInfo.u4Key1Max = 0;
            LblRangeInfo.u4Key2Min = pEthParams->u4MinLabelVal;
            LblRangeInfo.u4Key2Max = pEthParams->u4MaxLabelVal;
            u4ModuleID =
                (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) ==
                 LDP_TRUE) ? VPLS_LBL_MODULE_ID : LDP_LBL_MODULE_ID;

            if (pEthParams->u2EthLblRangeId != LDP_ZERO)
            {
                continue;
            }

            LDP_DBG4 ((LDP_SSN_PRCS | LDP_SSN_ALL),
                      "\r Label Range Id %d with label range %d %d is to "
                      "be created for the entity %d\n",
                      pEthParams->u2EthLblRangeId,
                      pEthParams->u4MinLabelVal, pEthParams->u4MaxLabelVal,
                      pLdpEntity->u4EntityIndex);

            if (ldpCreateLabelSpaceGroup
                (LBL_ALLOC_BOTH_NUM, &LblRangeInfo, 1, u4ModuleID,
                 PER_PLATFORM_INTERFACE_INDEX,
                 &pEthParams->u2EthLblRangeId) == LDP_FAILURE)
            {
                /* Deleting the Ethernet Label Range List for 
                 * the Entity */
                if (u1LblRngeAllocated == LDP_TRUE)
                {
                    LDP_DBG (LDP_MAIN_MEM,
                             "MAIN: Entity Up. Pltfrm LblSpaceAlloc " "Fail\n");
                    /* Trap to be genrated to the manager for which the
                     * label space allocation has failed.
                     */
                }
                else
                {
                    /* Failed to Allocate Label Space */
                    LDP_DBG3 ((LDP_SSN_PRCS | LDP_MAIN_MEM),
                              "\rLabel Range %d %d Allocation failed for the "
                              "entity %d\n", pEthParams->u4MinLabelVal,
                              pEthParams->u4MaxLabelVal,
                              pLdpEntity->u4EntityIndex);
                    /* Trap to be genrated to the manager saying Entity
                     * can not be made up.
                     */
                    return LDP_FAILURE;
                }
                /* Atleast One Label Range is allocated */
            }

            LDP_DBG4 ((LDP_SSN_PRCS | LDP_SSN_ALL),
                      "\r Label Range Id %d with label range %d %d created "
                      "for the entity %d\n", pEthParams->u2EthLblRangeId,
                      pEthParams->u4MinLabelVal, pEthParams->u4MaxLabelVal,
                      pLdpEntity->u4EntityIndex);

            u1LblRngeAllocated = LDP_TRUE;
        }
        /* 
         * Joining all the interfaces associated with this 
         * LdpEntity, to the Multicast group - LDP_ALL_ROUTERS_ADDR
         */
        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
        {
            TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity), pIfEntry,
                          tLdpIfTableEntry *)
            {
                /* MPLS_IPv6 add start */
                u1InterfaceType = LDP_ENTITY_IF_TYPE (pIfEntry);
                /* MPLS_IPv6 add end */
                /* MPLS_IPv6 mod start */
                if (pIfEntry->u1OperStatus == LDP_IF_OPER_ENABLE)
                {
                    if (LDP_IFTYPE_IPV4 == u1InterfaceType
                        || LDP_IFTYPE_DUAL == u1InterfaceType)
                    {
                        /* MPLS_IPv6 mod end */
                        MEMCPY (&u4IfAddr, LDP_ENTITY_IF_ADDRESS (pIfEntry),
                                sizeof (UINT4));
                        u4IfAddr = OSIX_NTOHL (u4IfAddr);
                        if ((LDP_SET_NO_OF_ENT_MCASTCOUNT
                             (LDP_CUR_INCARN, pIfEntry->u4IfIndex)) == LDP_ZERO)
                        {
                            if (LdpUdpJoinMulticast
                                (i4UdpSockFd, u4IfAddr,
                                 LDP_ALL_ROUTERS_ADDR) == LDP_FAILURE)
                            {
                                /* 
                                 * Error: Iface failed to register to 
                                 * Mcast group. All the interfaces that 
                                 * joined previously should now leave 
                                 * the Mcast group 
                                 */
                                LdpDisjointMultiCastIfaces (u4IfAddr,
                                                            pLdpEntity);
                                /* Trap to be genrated to the manager saying 
                                 * Entity can not be made up.
                                 */
                                TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST
                                              (pLdpEntity), pEthParams,
                                              tLdpEthParams *)
                                {
                                    if (pEthParams->u2EthLblRangeId != LDP_ZERO)
                                    {
                                        ldpDeleteLabelSpaceGroup (pEthParams->
                                                                  u2EthLblRangeId);
                                        pEthParams->u2EthLblRangeId = LDP_ZERO;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                /* Trap to be genrated to the manager 
                                 * saying Entity can not be made up.
                                 */
                                return LDP_FAILURE;
                            }
                            LDP_DBG1 (LDP_IF_MISC,
                                      "IF: Multicast Joined Returned Sucees for V4 Addr %x\n",
                                      u4IfAddr);
                            LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                            pIfEntry->
                                                            u4IfIndex)++;
                            LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                          pIfEntry->
                                                          u4IfIndex)++;
                        }

                        else
                        {
                            LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                          pIfEntry->
                                                          u4IfIndex)++;
                        }

                    }
#ifdef MPLS_IPV6_WANTED
                    if (LDP_IFTYPE_IPV6 == u1InterfaceType
                        || LDP_IFTYPE_DUAL == u1InterfaceType)
                    {
                        /*Fetch IPV6 Link Local Address from the interface index */
                        MEMCPY (Ipv6IfAddr.u1_addr,
                                pIfEntry->LnklocalIpv6Addr.u1_addr,
                                LDP_IPV6ADR_LEN);
                        if ((LDP_SET_NO_OF_ENT_V6MCASTCOUNT
                             (LDP_CUR_INCARN, pIfEntry->u4IfIndex)) == LDP_ZERO)
                        {
                            i4Udp6SockFd = LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
                            if (LDP_FAILURE !=
                                INET_ATON6 (LDP_ALL_ROUTERS_V6ADDR,
                                            &TmpIpv6IfAddr))
                            {
                                if (LdpUdpJoinV6Multicast
                                    (i4Udp6SockFd, Ipv6IfAddr,
                                     TmpIpv6IfAddr,
                                     pIfEntry->u4IfIndex) == LDP_FAILURE)
                                {
                                    LDP_DBG1 (LDP_IF_MISC,
                                              "IF: Multicast Join Returned Failure for V6 Addr %s \n",
                                              Ip6PrintAddr (&(Ipv6IfAddr)));
                                    /* Error: Iface failed to register to
                                     * Mcast group. All the interfaces that
                                     * joined previously should now leave the Mcast group*/
                                    LdpDisjointV6MultiCastIfaces (Ipv6IfAddr,
                                                                  pLdpEntity);
                                    /* Trap to be genrated to the manager saying
                                     * Entity can not be made up.*/
                                    TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST
                                                  (pLdpEntity), pEthParams,
                                                  tLdpEthParams *)
                                    {
                                        if (pEthParams->u2EthLblRangeId !=
                                            LDP_ZERO)
                                        {
                                            ldpDeleteLabelSpaceGroup
                                                (pEthParams->u2EthLblRangeId);
                                            pEthParams->u2EthLblRangeId =
                                                LDP_ZERO;
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                    /* Trap to be genrated to the manager
                                     * saying Entity can not be made up.*/
                                    return LDP_FAILURE;
                                }
                                LDP_DBG1 (LDP_IF_MISC,
                                          "IPV6: Multicast Join Returned Success For V6 Addr %s\n",
                                          Ip6PrintAddr (&(Ipv6IfAddr)));
                                LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                                pIfEntry->
                                                                u4IfIndex)++;
                            }
                            else
                            {
                                LDP_DBG (LDP_IF_MISC,
                                         "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
                                LdpDisjointV6MultiCastIfaces (Ipv6IfAddr,
                                                              pLdpEntity);
                                TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST
                                              (pLdpEntity), pEthParams,
                                              tLdpEthParams *)
                                {
                                    if (pEthParams->u2EthLblRangeId != LDP_ZERO)
                                    {
                                        ldpDeleteLabelSpaceGroup (pEthParams->
                                                                  u2EthLblRangeId);
                                        pEthParams->u2EthLblRangeId = LDP_ZERO;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                return LDP_FAILURE;
                            }
                        }
                        else
                        {
                            LDP_DBG1 (LDP_IF_MISC,
                                      "IPV6: MCOUNT V6 not ZERO  for interface index %x\n",
                                      pIfEntry->u4IfIndex);
                            LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                            pIfEntry->
                                                            u4IfIndex)++;
                        }
                    }
#endif
                    /* MPLS_IPv6 add end */
                }
            }                    /* End of TMO_SLL_Scan */
        }
    }                            /* End of LDP_LBL_SPACE_TYPE */

    if (gu4LdpMd5Option == TRUE)
    {
        /* If the Md5 Password is configured for the LDP TCP messages 
         * to be encrypted, which are sent on the IfIndex, then 
         * the socket option is set using the standard 
         * set_socket_option.
         */

        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
        {
            TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity), pIfEntry,
                          tLdpIfTableEntry *)
            {
                if ((pIfEntry->pEntityPeerPasswdInfo) != NULL)
                {
                    pu1MD5Passwd =
                        pIfEntry->pEntityPeerPasswdInfo->aMd5Password;
                    i4MD5KeyCount = (INT4) STRLEN (pu1MD5Passwd);
                    if (LdpSliSetMd5SockOpt (gu4TcpStdServConnId,
                                             (pIfEntry->
                                              pEntityPeerPasswdInfo->
                                              aMd5Password),
                                             i4MD5KeyCount) == LDP_FAILURE)
                    {
                        LDP_DBG (LDP_MAIN_MISC,
                                 "setsockopt failed for the " "MD5 password\n");

                        LdpDisjointMultiCastIfaces (0, pLdpEntity);
                        TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST
                                      (pLdpEntity), pEthParams, tLdpEthParams *)
                        {
                            if (pEthParams->u2EthLblRangeId != LDP_ZERO)
                            {
                                ldpDeleteLabelSpaceGroup (pEthParams->
                                                          u2EthLblRangeId);
                                pEthParams->u2EthLblRangeId = LDP_ZERO;
                            }
                            else
                            {
                                break;
                            }
                        }
                        /* Trap to be genrated to the manager 
                         * saying Entity can not be made up.
                         */
                        return LDP_FAILURE;
                    }
                }
            }
        }
        else
        {
            /* Commenting MD5 related code changes to fix coverity warnings 
             * since MD5 Authentication is not supported for LDP */

            /*
               pu1MD5Passwd = pLdpEntity->pEntityPeerPasswdInfo->aMd5Password;
               i4MD5KeyCount = STRLEN (pu1MD5Passwd);
             */
            if (pIfEntry == NULL)
            {
                return LDP_FAILURE;
            }

            /*

               if (LdpSliSetMd5SockOpt (gu4TcpStdServConnId,
               (pIfEntry->pEntityPeerPasswdInfo->
               aMd5Password),
               i4MD5KeyCount) == LDP_FAILURE)
               {
               LDP_DBG (LDP_MAIN_MISC,
               "setsockopt failed for the MD5 password\n");

               LdpDisjointMultiCastIfaces (0, pLdpEntity);
               TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST
               (pLdpEntity), pEthParams, tLdpEthParams *)
               {
               if (pEthParams->u2EthLblRangeId != LDP_ZERO)
               {
               ldpDeleteLabelSpaceGroup (pEthParams->u2EthLblRangeId);
               pEthParams->u2EthLblRangeId = LDP_ZERO;
               }
               else
               {
               break;
               }
               }

             */
            /* Trap to be genrated to the manager saying Entity
             * can not be made up.
             */
            /*return LDP_FAILURE;
               }

             */
        }
    }
    if (LdpSendHelloMsg ((tLdpEntity *) pLdpEntity) == LDP_FAILURE)
    {
        return LDP_FAILURE;
    }
    pLdpEntity->u1OperStatus = LDP_OPER_ENABLED;

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpGetCrossConnectIndex                                   */
/* Description   : This routine creates an a new Cross Connect Index for LCB */
/* Input(s)      : Pointer to the cross connect Index for the LCB            */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpGetCrossConnectIndex (UINT4 *pu4XConnectIndex)
{
    /* This function will also be called from the SNMP manager when
     * the LSR MIB is implemented for getting a new Index. 
     * This Index is right now returning a unique value.
     *
     * The Key Manager can be used for getting the unique index and
     * releasing it whenever a LSP goes down.
     */
    static UINT4        u4XCIndex = LDP_ONE;

    *pu4XConnectIndex = u4XCIndex++;
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDeallocateEthLabelRnges                                */
/* Description   : This routine deletes the Label Ranges assocaited with an  */
/*               : Entity                                                    */
/* Input(s)      : Pointer to the Entity                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpDeallocateEthLabelRnges (tLdpEntity * pLdpEntity)
{
    tLdpEthParams      *pEthParams = NULL;

    /* Changed for NP - End */

    pEthParams = (tLdpEthParams *)
        TMO_SLL_First (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity));
    while (pEthParams != NULL)
    {
        if ((pEthParams->u2EthLblRangeId) != 0)
        {
            ldpDeleteLabelSpaceGroup (pEthParams->u2EthLblRangeId);
        }
        TMO_SLL_Delete (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                        &(pEthParams->NextRangeEntry));
        MemReleaseMemBlock (LDP_ETH_LBL_RNG_POOL_ID, (UINT1 *) pEthParams);
        pEthParams = (tLdpEthParams *)
            TMO_SLL_First (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity));
    }
    TMO_SLL_Init (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity));
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDisjointMultiCastIfaces                                */
/* Description   : This routine deletes the Interfaces from the Multicast    */
/*                 list for the Entity                                       */
/* Input(s)      : Pointer to the Entity                                     */
/*               : u4IfAddr if NON-ZERO represents the Interface for which   */
/*               :             Multicast joining has Failed                  */
/*               :          if Zero all the interfaces will be disjoined     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpDisjointMultiCastIfaces (UINT4 u4IfAddr, tLdpEntity * pLdpEntity)
{
    tLdpIfTableEntry   *pIfEntry = NULL;
    UINT4               u4TmpIfAddr;
    INT4                i4UdpSockFd = LDP_UDP_SOCKFD (LDP_CUR_INCARN);

    TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity), pIfEntry, tLdpIfTableEntry *)
    {

        MEMCPY (&u4TmpIfAddr, LDP_ENTITY_IF_ADDRESS (pIfEntry), sizeof (UINT4));
        u4TmpIfAddr = OSIX_NTOHL (u4TmpIfAddr);
        if (u4IfAddr != 0)
        {
            if (u4IfAddr == u4TmpIfAddr)
            {
                /* All the interfaces which joined previously have left
                 * the Mcast group */
                return LDP_FAILURE;
            }
            else
            {
                if (LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                  pIfEntry->u4IfIndex) >
                    LDP_ZERO)
                {
                    --(LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                     pIfEntry->u4IfIndex));
                }
                if (LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                  pIfEntry->u4IfIndex) ==
                    LDP_ZERO)
                {
                    LdpUdpLeaveMulticast (i4UdpSockFd, u4TmpIfAddr,
                                          LDP_ALL_ROUTERS_ADDR);
                }
            }
        }
        else
        {
            if (LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                              pIfEntry->u4IfIndex) > LDP_ZERO)
            {
                --(LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                                 pIfEntry->u4IfIndex));
            }
            if (LDP_SET_NO_OF_ENT_MCASTCOUNT (LDP_CUR_INCARN,
                                              pIfEntry->u4IfIndex) == LDP_ZERO)
            {
                LdpUdpLeaveMulticast (i4UdpSockFd, u4TmpIfAddr,
                                      LDP_ALL_ROUTERS_ADDR);
            }
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDeAllocateIncarnTables                                 */
/* Description   : This routine releases the memory allocated for the        */
/*               : Incarn tables                                             */
/* Input(s)      : u2Incarn - Incarnation identifier                         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDeAllocateIncarnTables (UINT2 u2Incarn)
{
    UINT1               u1Index = 0;

    for (; u1Index <= u2Incarn; u1Index++)
    {
        if (LDP_PEER_IFADR_TABLE (u1Index) != LDP_ZERO)
        {
            TMO_HASH_Delete_Table (LDP_PEER_IFADR_TABLE (u1Index), NULL);
#ifdef MPLS_IPV6_WANTED
            TMO_HASH_Delete_Table (LDP_PEER_IPV6_IFADR_TABLE (u1Index), NULL);
#endif
            LDP_PEER_IFADR_TABLE (u1Index) = LDP_ZERO;
        }
        if (LDP_CRLSP_TNL_TABLE (u1Index) != LDP_ZERO)
        {
            TMO_HASH_Delete_Table (LDP_CRLSP_TNL_TABLE (u1Index), NULL);
            LDP_CRLSP_TNL_TABLE (u1Index) = LDP_ZERO;
        }
        if (LDP_TCPCONN_HSH_TABLE (u1Index) != LDP_ZERO)
        {
            TMO_HASH_Delete_Table (LDP_TCPCONN_HSH_TABLE (u1Index), NULL);
            LDP_TCPCONN_HSH_TABLE (u1Index) = LDP_ZERO;
        }
    }

    /* Release memory blocks from TCP Sock Table. */
    LdpTcpDeInitSockInfo ();

    /* Deleting the Timer List associated to the Ldp Task */
    TmrDeleteTimerList (LDP_TIMER_LIST_ID);
    LDP_TIMER_LIST_ID = LDP_ZERO;

    /* Delete Memory pools for LDP module. */
    LdpSizingMemDeleteMemPools ();
}

/*****************************************************************************/
/* Function Name : LdpTriggerRtChgEvent                                      */
/* Description   : This routine triggers the route changes to the            */
/*                 respective state machines                                 */
/* Input(s)      : u2IncarnId  - specifies the Incarn Id                     */
/*               : u4RtEvent   - specifies the Event                         */
/*               : pRouteInfo  - specifies the route Info                    */
/*               : pLspCtrlBlk - specifies the control block                 */
/*               : pLdpSession - specifies the ldp session                   */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTriggerRtChgEvent (UINT2 u2IncarnId, UINT4 u4RtEvent,
                      tLdpRouteEntryInfo * pRouteInfo,
                      tLspCtrlBlock * pLspCtrlBlk, tLdpSession * pSessionEntry)
{
    UINT4               u4TempDestAddr = 0;
    UINT4               u4TempNextHop = 0;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;
    tTMO_SLL           *pUpstCtrlBlkList = NULL;

#ifdef MPLS_IPV6_WANTED
    UINT1               u1PrefixLen = 0;

#endif

    LDP_DBG2 (LDP_DBG_PRCS, "%s:%d Entry\n", __FUNCTION__, __LINE__);

    pUpstCtrlBlkList = &LCB_UPSTR_LIST (pLspCtrlBlk);

    if ((u4RtEvent == LDP_RT_NEW))
    {
#ifdef MPLS_IPV6_WANTED
        if (pRouteInfo->u2AddrType == LDP_ADDR_TYPE_IPV6)
        {
            u1PrefixLen =
                MplsGetIpv6Subnetmasklen (LDP_IPV6_ADDR (pRouteInfo->DestMask));
            LdpIpv6LspSetReq (u2IncarnId, &pRouteInfo->DestAddr.Ip6Addr,
                              u1PrefixLen, &pRouteInfo->NextHop.Ip6Addr,
                              (pRouteInfo->u4RtIfIndx));
        }
        else
#endif
        {
            u4TempDestAddr =
                OSIX_HTONL (LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr));
            u4TempNextHop = OSIX_HTONL (LDP_IPV4_U4_ADDR (pRouteInfo->NextHop));
            LdpLspSetReq (u2IncarnId, (UINT1 *) &u4TempDestAddr,
                          (LDP_IPV4_U4_ADDR (pRouteInfo->DestMask)),
                          (UINT1 *) &u4TempNextHop, (pRouteInfo->u4RtIfIndx));
        }
    }
    else if (((u4RtEvent & LDP_RT_NH_CHG) == LDP_RT_NH_CHG)
             || ((u4RtEvent & LDP_RT_IFACE_CHG) == LDP_RT_IFACE_CHG))
    {
        if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
            (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
        {
            LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)]
                [LDP_LSM_EVT_INT_NEW_NH] (pLspCtrlBlk, (UINT1 *) pRouteInfo);
        }
        else if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
                 ((SSN_MRGTYPE (pSessionEntry) == LDP_VP_MRG) ||
                  (SSN_MRGTYPE (pSessionEntry) == LDP_VC_MRG)))
        {
            TMO_SLL_Scan (pUpstCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
            {
                pUpstrCtrlBlock = SLL_TO_UPCB (pSllNode);
                LdpMergeUpSm (pUpstrCtrlBlock,
                              LDP_USM_EVT_INT_NEW_NH,
                              (tLdpMsgInfo *) (VOID *) pRouteInfo);
            }
        }
        else if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_UNSOLICIT))
        {
            LdpDuDnSm (pLspCtrlBlk, LDP_DU_DN_LSM_EVT_NH_CHG,
                       (tLdpMsgInfo *) (VOID *) pRouteInfo);
        }
    }
    else if ((u4RtEvent & LDP_RT_DELETED) == LDP_RT_DELETED)
    {
        if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
            (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
        {
            LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)]
                [LDP_LSM_EVT_INT_DSTR] (pLspCtrlBlk, NULL);
        }
        else if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
                 ((SSN_MRGTYPE (pSessionEntry) == LDP_VP_MRG) ||
                  (SSN_MRGTYPE (pSessionEntry) == LDP_VC_MRG)))
        {

            gLdpMrgDnFsm[LCB_STATE (pLspCtrlBlk)][LDP_DSM_EVT_DSTR_LOST]
                (pLspCtrlBlk, (tLdpMsgInfo *) (VOID *) pRouteInfo);
        }
        else if (SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_UNSOLICIT)
        {
            LdpDuDnSm (pLspCtrlBlk, LDP_DU_DN_LSM_EVT_DEL_FEC,
                       (tLdpMsgInfo *) (VOID *) pRouteInfo);
        }
    }
    LDP_DBG2 (LDP_DBG_PRCS, "%s:%d Exit\n", __FUNCTION__, __LINE__);
    return;
}

/*****************************************************************************/
/* Function Name : LdpLspSetReq                                              */
/* Description   : This routine is called when a new LSP is to be created in */
/*                 LSR. The LSR obtains information from the IP routing table*/
/*                 for a destination. The LSP is created for the destination.*/
/*                                                                           */
/* Input(s)      : u2IncarnId      - Incarnation Identifier                  */
/*                 NetworkAddr     - Network address to which the LSP is     */
/*                                   to be created.                          */
/*                 u4DestMask      - Conatins the destination mask           */
/*                 NextHopAddr     - Next hop address to reach the network   */
/*                                   indicated by the NetworkAddr            */
/*                 u4IfIndex       - Outgoing interface to rech the network  */
/*                                   indicated by teh NetworkAddr            */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpLspSetReq (UINT2 u2IncarnId, tIpv4Addr NetworkAddr, UINT4 u4DestMask,
              tIpv4Addr NextHopAddr, UINT4 u4IfIndex)
{
    UINT1               u1SsnState;
    UINT4               u4HSessIndex = 0;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tTMO_HASH_NODE     *pSsnHashNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1               u1PrefLen = 0;
    tLdpIpRtInfo        InRtInfo;

    /* Get the prefix length from the Destination Mask */
    LDP_GET_PRFX_LEN (u4DestMask, u1PrefLen);

    MEMSET (&InRtInfo, 0, sizeof (tLdpIpRtInfo));

    InRtInfo.u4DestMask = u4DestMask;
    InRtInfo.u4DestNet = OSIX_NTOHL (*(UINT4 *) (VOID *) (NetworkAddr));
    InRtInfo.u4NextHop = OSIX_NTOHL (*(UINT4 *) (VOID *) (NextHopAddr));
    InRtInfo.u4RtIfIndx = u4IfIndex;

    LDP_DBG2 (LDP_DBG_PRCS, "%s:%d Entry\n", __FUNCTION__, __LINE__);

    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

    TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HSessIndex)
    {
        TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HSessIndex,
                              pSsnHashNode, tTMO_HASH_NODE *)
        {
            pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);

            SSN_GET_SSN_STATE (pLdpSession, u1SsnState);
            if (u1SsnState == LDP_SSM_ST_OPERATIONAL)
            {
                if (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
                    == LDP_FALSE)
                {
                    if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
                        (SSN_MRGTYPE (pLdpSession) == LDP_NO_MRG))
                    {
                        LdpNonMrgIntLspSetReq (u2IncarnId, NetworkAddr,
                                               u1PrefLen, NextHopAddr,
                                               u4IfIndex, NULL);
                        LDP_DBG2 (LDP_DBG_PRCS, "%s:%d Exit\n", __FUNCTION__,
                                  __LINE__);
                        return;
                    }

                    if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
                        ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
                         (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG)))
                    {
                        LdpEstbLspsForDoDMrgSsns (&InRtInfo, pLdpSession);
                    }
                }
                if (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT)
                {
#ifdef LDP_GR_WANTED
                    /* Do not give Event to the stale session */
                    /* When the stale session will come up, LSP for this
                     * route will be established at that point of time */
                    if (pLdpSession->u1StaleStatus == LDP_FALSE)
                    {
                        LdpEstbLspsForDuSsns (pLdpSession, &InRtInfo);
                    }
#else
                    LdpEstbLspsForDuSsns (pLdpSession, &InRtInfo);
#endif
                }
            }
        }
    }

    LDP_DBG2 (LDP_DBG_PRCS, "%s:%d Exit\n", __FUNCTION__, __LINE__);
    return;
}

/*****************************************************************************/
/* Function Name : LdpGenerateRtChgEvents                                    */
/* Description   : This routine is called when NH changes are occured        */
/*                 for an LSP                                                */
/* Input(s)      : u2IncarnId - Incarnation Identifier                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpGenerateRtChgEvents (UINT2 u2IncarnId)
{
    UINT1               u1Count = 0;
    UINT4               u4HIndex = 0;
    UINT1               u1Event = 0;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tLdpRouteEntryInfo  RouteInfo;
    tLdpRouteEntryInfo *pRouteInfo = &RouteInfo;
    tLdpIpRtInfo        InRtInfo;
    tTMO_SLL_NODE      *pSsnHashNode = NULL;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    BOOL1               bIsFecExistInSsn = LDP_FALSE;
    tTMO_SLL           *pEntityList = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpEntity         *pLdpEntity = NULL;
#ifdef MPLS_IPV6_WANTED
    tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6RtInfo      InIpv6RtInfo;
#endif
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;

    UNUSED_PARAM (pTcpConnHshTbl);
    UNUSED_PARAM (pSsnHashNode);
    UNUSED_PARAM (u4HIndex);

    LDP_DBG ((LDP_IF_MISC | LDP_IF_ALL), "LdpGenerateRtChgEvents ENTRY\n");
#ifdef MPLS_IPV6_WANTED
    MEMSET (&InIpv6RtInfo, LDP_ZERO, sizeof (tNetIpv6RtInfo));
    MEMSET (&NetIpv6RtInfoQueryMsg, 0, sizeof (tNetIpv6RtInfoQueryMsg));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
#endif
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    pEntityList = &LDP_ENTITY_LIST (u2IncarnId);

    for (u1Count = 0; u1Count < MAX_LDP_IP_RT_ENTRIES; u1Count++)
    {
        if ((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count)) == LDP_ZERO)
        {
            continue;
        }
        bIsFecExistInSsn = LDP_FALSE;
        pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

        /* 
         * Scan through all the sessions maintained in the Session 
         * HashTable, to check if any LCB established over a Session, 
         * is getting effected by this NextHop change.
         */

#ifdef MPLS_IPV6_WANTED
        if (LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId, u1Count) ==
            LDP_ADDR_TYPE_IPV6)
        {
            MEMCPY (LDP_IPV6_U4_ADDR (pRouteInfo->DestAddr),
                    LDP_FECROUTE_ARRAY_IPV6_PREFIX (u2IncarnId, u1Count),
                    LDP_IPV6ADR_LEN);

            MplsGetIPV6Subnetmask ((INT4)
                                   (LDP_FECROUTE_ARRAY_PRELEN
                                    (u2IncarnId, u1Count)),
                                   LDP_IPV6_U4_ADDR (pRouteInfo->DestMask));

            MEMCPY (LDP_IPV6_U4_ADDR (pRouteInfo->NextHop),
                    LDP_FECROUTE_ARRAY_IPV6_NHOP (u2IncarnId, u1Count),
                    LDP_IPV6ADR_LEN);
            LDP_DBG5 (LDP_DBG_PRCS,
                      "%s : %d DestAddr=%s NextHop=%s IndexValue=%d\n",
                      __FUNCTION__, __LINE__,
                      Ip6PrintAddr (&pRouteInfo->DestAddr.Ip6Addr),
                      Ip6PrintAddr (&pRouteInfo->NextHop.Ip6Addr),
                      LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count));
        }
        else
#endif
        {
            LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr) =
                LDP_FECROUTE_ARRAY_IPV4_PREFIX (u2IncarnId, u1Count);

            LDP_GET_MASK_FROM_PRFX_LEN ((INT4)
                                        (LDP_FECROUTE_ARRAY_PRELEN
                                         (u2IncarnId, u1Count)),
                                        (LDP_IPV4_U4_ADDR
                                         (pRouteInfo->DestMask)));

            LDP_IPV4_U4_ADDR (pRouteInfo->NextHop) =
                LDP_FECROUTE_ARRAY_IPV4_NHOP (u2IncarnId, u1Count);
        }

        pRouteInfo->u4RtIfIndx =
            LDP_FECROUTE_ARRAY_OUTIFINDEX (u2IncarnId, u1Count);

        pRouteInfo->u2AddrType =
            LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId, u1Count);

        TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
        {
            TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
            {
                pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

                if (pLdpSession == NULL)
                {
                    continue;
                }
#ifdef LDP_GR_WANTED
                if (pLdpSession->u1SessionState != LDP_SSM_ST_OPERATIONAL &&
                    pLdpSession->u1StaleStatus == LDP_FALSE)
                {
                    continue;
                }
#else
                if (pLdpSession->u1SessionState != LDP_SSM_ST_OPERATIONAL)
                {
                    continue;
                }
#endif

                pLdpSllNode = NULL;
                pTempLdpSllNode = NULL;
                TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                                  pTempLdpSllNode, tTMO_SLL_NODE *)
                {
                    pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);
                    /* 
                     * Rising Next Hop Change Event to Lsps matching 
                     * the given Destination and Mask. 
                     *
                     */

#ifdef MPLS_IPV6_WANTED
                    if (LCB_FEC (pLspCtrlBlk).u2AddrFmly == LDP_ADDR_TYPE_IPV6)
                    {
                        LDP_DBG2 (LDP_DBG_PRCS,
                                  "RT Evt FEC %s NH %s\n",
                                  Ip6PrintAddr (&LCB_FEC (pLspCtrlBlk).Prefix.
                                                Ip6Addr),
                                  Ip6PrintAddr (&pLspCtrlBlk->NextHopAddr.
                                                Ip6Addr));

                    }
                    else
#endif
                    {
                        LDP_DBG2 (LDP_DBG_PRCS,
                                  "RT Evt FEC %x NH %x\n",
                                  LDP_IPV4_U4_ADDR (LCB_FEC (pLspCtrlBlk).
                                                    Prefix),
                                  LDP_IPV4_U4_ADDR (pLspCtrlBlk->NextHopAddr));
                    }
                    if (!((LCB_TRIG (pLspCtrlBlk))
                          && (LCB_TRIG_TYPE (pLspCtrlBlk) ==
                              NEXT_HOP_CHNG_LSP_SET_UP)))
                    {

                        if ((LdpIsAddressMatchWithAddrType
                             (&LCB_FEC (pLspCtrlBlk).Prefix,
                              &LDP_FECROUTE_ARRAY_PREFIX (u2IncarnId, u1Count),
                              LCB_FEC (pLspCtrlBlk).u2AddrFmly,
                              LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId,
                                                         u1Count)) == LDP_TRUE)
                            && ((LCB_FEC (pLspCtrlBlk).u1PreLen) ==
                                (LDP_FECROUTE_ARRAY_PRELEN
                                 (u2IncarnId, u1Count))))
                        {
                            bIsFecExistInSsn = LDP_TRUE;
                            /* The prefix does exist in the LCB, 
                             * so perform the nexthop change/delete handling */

                            if ((LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count)
                                 == LDP_RT_NH_CHG))
                            {
#ifdef MPLS_IPV6_WANTED
                                if (LCB_FEC (pLspCtrlBlk).u2AddrFmly ==
                                    LDP_ADDR_TYPE_IPV6)
                                {
                                    LDP_DBG2 (LDP_DBG_PRCS,
                                              "NH chg FEC %s NH %s\n",
                                              Ip6PrintAddr (&LCB_FEC
                                                            (pLspCtrlBlk).
                                                            Prefix.Ip6Addr),
                                              Ip6PrintAddr (&pLspCtrlBlk->
                                                            NextHopAddr.
                                                            Ip6Addr));
                                }
                                else
#endif
                                {
                                    LDP_DBG2 (LDP_DBG_PRCS,
                                              "NH chg FEC %x NH %x\n",
                                              LDP_IPV4_U4_ADDR (LCB_FEC
                                                                (pLspCtrlBlk).
                                                                Prefix),
                                              LDP_IPV4_U4_ADDR (pLspCtrlBlk->
                                                                NextHopAddr));
                                }

                                LdpTriggerRtChgEvent (u2IncarnId,
                                                      LDP_RT_NH_CHG,
                                                      pRouteInfo,
                                                      pLspCtrlBlk, pLdpSession);
                            }
                            else if (((LDP_FECROUTE_INDEX_AVBLE
                                       (u2IncarnId, u1Count) == LDP_RT_DELETED))
                                     &&
                                     (LdpIsAddressMatchWithAddrType
                                      (&pLspCtrlBlk->NextHopAddr,
                                       &pRouteInfo->NextHop,
                                       LCB_FEC (pLspCtrlBlk).u2AddrFmly,
                                       pRouteInfo->u2AddrType) == LDP_TRUE))
                            {
#ifdef MPLS_IPV6_WANTED
                                if (LCB_FEC (pLspCtrlBlk).u2AddrFmly ==
                                    LDP_ADDR_TYPE_IPV6)
                                {

                                    LDP_DBG2 (LDP_DBG_PRCS,
                                              "RT Del FEC %s NH %s\n",
                                              Ip6PrintAddr (&LCB_FEC
                                                            (pLspCtrlBlk).
                                                            Prefix.Ip6Addr),
                                              Ip6PrintAddr (&pLspCtrlBlk->
                                                            NextHopAddr.
                                                            Ip6Addr));
                                }
                                else
#endif
                                {

                                    LDP_DBG2 (LDP_DBG_PRCS,
                                              "RT Del FEC %x NH %x\n",
                                              LDP_IPV4_U4_ADDR (LCB_FEC
                                                                (pLspCtrlBlk).
                                                                Prefix),
                                              LDP_IPV4_U4_ADDR (pLspCtrlBlk->
                                                                NextHopAddr));

                                }

                                if (LdpIsAddressMatch
                                    (&pLspCtrlBlk->NextHopAddr,
                                     &pRouteInfo->NextHop,
                                     pRouteInfo->u2AddrType) == LDP_TRUE)
                                {
                                    LdpTriggerRtChgEvent (u2IncarnId,
                                                          LDP_RT_DELETED,
                                                          pRouteInfo,
                                                          pLspCtrlBlk,
                                                          pLdpSession);

                                    LdpCheckAndRetriggerRtAddEvent (u2IncarnId,
                                                                    pRouteInfo);
                                }
                            }
                        }
                    }
                }

                if (LDP_FECROUTE_INDEX_AVBLE (u2IncarnId,
                                              u1Count) == LDP_RT_DELETED)
                {
                    pLdpSllNode = NULL;
                    pTempLdpSllNode = NULL;
                    TMO_DYN_SLL_Scan (&SSN_ULCB (pLdpSession), pLdpSllNode,
                                      pTempLdpSllNode, tTMO_SLL_NODE *)
                    {
                        if (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT)
                        {
                            pLspUpCtrlBlock =
                                (tUstrLspCtrlBlock *) (pLdpSllNode);

                            if (!
                                ((LdpIsAddressMatchWithAddrType
                                  (&pRouteInfo->DestAddr,
                                   &pLspUpCtrlBlock->Fec.Prefix,
                                   pRouteInfo->u2AddrType,
                                   pLspUpCtrlBlock->Fec.u2AddrFmly) == LDP_TRUE)
                                 &&
                                 (LDP_FECROUTE_ARRAY_PRELEN
                                  (u2IncarnId,
                                   u1Count) == pLspUpCtrlBlock->Fec.u1PreLen)))
                            {
                                continue;
                            }
                            if (pLspUpCtrlBlock->pDnstrCtrlBlock != NULL)
                            {
                                LDP_DBG1 (LDP_DBG_PRCS,
                                          "%s: DLCB associated with ULCB is NOT NULL\n",
                                          __func__);
                                if (LdpIsAddressMatchWithAddrType
                                    (&pLspUpCtrlBlock->pDnstrCtrlBlock->
                                     NextHopAddr, &pRouteInfo->NextHop,
                                     LCB_FEC (pLspUpCtrlBlock->pDnstrCtrlBlock).
                                     u2AddrFmly,
                                     pRouteInfo->u2AddrType) == LDP_FALSE)
                                {
                                    continue;
                                }
                            }
                            else
                            {
#ifdef MPLS_IPV6_WANTED
                                if (pRouteInfo->u2AddrType ==
                                    LDP_ADDR_TYPE_IPV6)
                                {
                                    MEMSET (&NetIpv6RtInfoQueryMsg, 0,
                                            sizeof (tNetIpv6RtInfoQueryMsg));
                                    MEMSET (&NetIpv6RtInfo, 0,
                                            sizeof (tNetIpv6RtInfo));

                                    NetIpv6RtInfoQueryMsg.u4ContextId =
                                        VCM_DEFAULT_CONTEXT;
                                    NetIpv6RtInfoQueryMsg.u1QueryFlag =
                                        RTM6_QUERIED_FOR_NEXT_HOP;
                                    Ip6AddrCopy (&NetIpv6RtInfoQueryMsg.Ip6Dst,
                                                 &pRouteInfo->DestAddr.Ip6Addr);
                                    NetIpv6RtInfoQueryMsg.u1Prefixlen =
                                        MplsGetIpv6Subnetmasklen
                                        (LDP_IPV6_U4_ADDR
                                         (pRouteInfo->DestAddr));
                                    if (NetIpv6GetRoute
                                        (&NetIpv6RtInfoQueryMsg,
                                         &NetIpv6RtInfo) == NETIPV6_SUCCESS)
                                    {
                                        if (MEMCMP (&NetIpv6RtInfo.NextHop,
                                                    &pRouteInfo->NextHop.
                                                    Ip6Addr,
                                                    IPV6_ADDR_LENGTH) !=
                                            LDP_ZERO)
                                        {
                                            continue;
                                        }
                                    }
                                }
                                else
#endif
                                {
                                    MEMSET (&RtQuery, 0,
                                            sizeof (tRtInfoQueryMsg));
                                    MEMSET (&NetIpRtInfo, 0,
                                            sizeof (tNetIpv4RtInfo));
                                    RtQuery.u4DestinationIpAddress =
                                        LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr);
                                    RtQuery.u4DestinationSubnetMask =
                                        LDP_IPV4_U4_ADDR (pRouteInfo->DestMask);
                                    RtQuery.u1QueryFlag =
                                        RTM_QUERIED_FOR_NEXT_HOP;
                                    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo)
                                        == NETIPV4_SUCCESS)
                                    {
                                        if (NetIpRtInfo.u4NextHop !=
                                            LDP_IPV4_U4_ADDR (pRouteInfo->
                                                              NextHop))
                                        {
                                            continue;
                                        }
                                    }
                                }
                            }
#ifdef MPLS_IPV6_WANTED
                            if (pRouteInfo->u2AddrType == LDP_ADDR_TYPE_IPV6)
                            {
                                LDP_DBG1 (LDP_DBG_PRCS,
                                          "ULCB FEC %s deletion \n",
                                          Ip6PrintAddr (&pRouteInfo->DestAddr.
                                                        Ip6Addr));
                            }
                            else
#endif
                            {
                                LDP_DBG1 (LDP_DBG_PRCS,
                                          "ULCB FEC %x deletion \n",
                                          LDP_IPV4_U4_ADDR (pRouteInfo->
                                                            DestAddr));

                            }
                            if (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT)
                            {
                                u1Event = LDP_DU_UP_LSM_EVT_DEL_FEC;
                                LdpDuUpSm (pLspUpCtrlBlock, u1Event,
                                           (tLdpMsgInfo *) (VOID *) pRouteInfo);
                            }
                        }
                        else
                        {
                            pLspCtrlBlk = (tLspCtrlBlock *) (pLdpSllNode);

                            if (!
                                ((LdpIsAddressMatchWithAddrType
                                  (&pRouteInfo->DestAddr,
                                   &pLspCtrlBlk->Fec.Prefix,
                                   pRouteInfo->u2AddrType,
                                   pLspCtrlBlk->Fec.u2AddrFmly) == LDP_TRUE)
                                 &&
                                 (LDP_FECROUTE_ARRAY_PRELEN
                                  (u2IncarnId,
                                   u1Count) == pLspCtrlBlk->Fec.u1PreLen)))
                            {
                                continue;
                            }

#ifdef MPLS_IPV6_WANTED
                            if (pRouteInfo->u2AddrType == LDP_ADDR_TYPE_IPV6)
                            {
                                LDP_DBG1 (LDP_DBG_PRCS,
                                          "ULCB FEC %s deletion \n",
                                          Ip6PrintAddr (&pRouteInfo->DestAddr.
                                                        Ip6Addr));
                            }
                            else
#endif
                            {
                                LDP_DBG1 (LDP_DBG_PRCS,
                                          "ULCB FEC %x deletion \n",
                                          LDP_IPV4_U4_ADDR (pRouteInfo->
                                                            DestAddr));
                            }
                            u1Event = LDP_DU_UP_LSM_EVT_INT_DN_WR;
                            LdpInvCorrStMh (pLdpSession, pLspCtrlBlk,
                                            u1Event,
                                            (tLdpMsgInfo *) (VOID *)
                                            pRouteInfo, LDP_TRUE);
                        }
                    }
                }
            }
        }

        pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
        pLdpEntity = NULL;
        pLdpPeer = NULL;

        if ((bIsFecExistInSsn == LDP_FALSE) &&
            (LDP_FECROUTE_INDEX_AVBLE (u2IncarnId, u1Count) == LDP_RT_NH_CHG))
        {
            TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
            {
                TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
                {
                    pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

                    if (pLdpSession == NULL)
                    {
                        continue;
                    }
#ifdef LDP_GR_WANTED
                    if (pLdpSession->u1SessionState != LDP_SSM_ST_OPERATIONAL &&
                        pLdpSession->u1StaleStatus == LDP_FALSE)
                    {
                        continue;
                    }
#else
                    if (pLdpSession->u1SessionState != LDP_SSM_ST_OPERATIONAL)
                    {
                        continue;
                    }
#endif
#ifdef MPLS_IPV6_WANTED
                    if (LDP_FECROUTE_ARRAY_ADDRFM (u2IncarnId, u1Count) ==
                        LDP_ADDR_TYPE_IPV6)
                    {
                        /** TODO : Need to add the code similar to below one ***/
                        MEMSET (&InIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
                        LDP_DBG1 (LDP_DBG_PRCS,
                                  "LdpGenerateRtChgEvents FEC=%s NEW LSP\n",
                                  Ip6PrintAddr (&pRouteInfo->DestAddr.Ip6Addr));

                        InIpv6RtInfo.u4Index = pRouteInfo->u4RtIfIndx;
                        MEMCPY (&InIpv6RtInfo.Ip6Dst,
                                &pRouteInfo->DestAddr.Ip6Addr, LDP_IPV6ADR_LEN);
                        InIpv6RtInfo.u1Prefixlen =
                            MplsGetIpv6Subnetmasklen (LDP_IPV6_U4_ADDR
                                                      (pRouteInfo->DestMask));
                        MEMCPY (&InIpv6RtInfo.NextHop,
                                &pRouteInfo->NextHop.Ip6Addr, LDP_IPV6ADR_LEN);

                        if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
                            (SSN_MRGTYPE (pLdpSession) == LDP_NO_MRG))
                        {
                            LdpGetLspIpv6RouteInfo (&InIpv6RtInfo, pLdpSession);
                        }
                        else if ((SSN_ADVTYPE (pLdpSession) ==
                                  LDP_DSTR_ON_DEMAND)
                                 && ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG)
                                     || (SSN_MRGTYPE (pLdpSession) ==
                                         LDP_VP_MRG)))
                        {
#if 0
                            LdpEstbIpv6LspsForDoDMrgSsns (&InIpv6RtInfo,
                                                          pLdpSession);
#endif
                        }
                        else if (SSN_ADVTYPE (pLdpSession) ==
                                 LDP_DSTR_UNSOLICIT)
                        {
                            LdpEstbIpv6LspsForDuSsns (pLdpSession,
                                                      &InIpv6RtInfo);
                        }
                    }
                    else
#endif
                    {
                        MEMSET (&InRtInfo, 0, sizeof (tLdpIpRtInfo));
                        /* The prefix does not exist in the LCB, 
                         * so create the new LSP now */
                        LDP_DBG1 (LDP_DBG_PRCS,
                                  "LdpGenerateRtChgEvents FEC=%x NEW LSP\n",
                                  LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr));
                        InRtInfo.u4RtIfIndx = pRouteInfo->u4RtIfIndx;
                        InRtInfo.u4DestNet =
                            LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr);
                        InRtInfo.u4DestMask =
                            LDP_IPV4_U4_ADDR (pRouteInfo->DestMask);
                        InRtInfo.u4NextHop =
                            LDP_IPV4_U4_ADDR (pRouteInfo->NextHop);

                        if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
                            (SSN_MRGTYPE (pLdpSession) == LDP_NO_MRG))
                        {
                            LdpGetLspRouteInfo (&InRtInfo, pLdpSession);
                        }
                        else if ((SSN_ADVTYPE (pLdpSession) ==
                                  LDP_DSTR_ON_DEMAND)
                                 && ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG)
                                     || (SSN_MRGTYPE (pLdpSession) ==
                                         LDP_VP_MRG)))
                        {
                            LdpEstbLspsForDoDMrgSsns (&InRtInfo, pLdpSession);

                        }
                        else if (SSN_ADVTYPE (pLdpSession) ==
                                 LDP_DSTR_UNSOLICIT)
                        {
                            LdpEstbLspsForDuSsns (pLdpSession, &InRtInfo);
                        }
                    }
                }
            }
        }

        MEMSET ((UINT1 *) &LDP_FECROUTE_ARRAY (u2IncarnId,
                                               u1Count), 0, sizeof (tRouteFec));
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpSetLoopDetectionMethod                                 */
/* Description   : Function is used to decide whether or not the LSR         */
/*                 supports LoopDetection and the types.                     */
/* Input(s)      : u1LoopDetectMethod - Holds the Values to indicate whether */
/*                                      LoopDetection is Enabled by          */
/*                                      HopCountLimit or PathVectorLimit     */
/*                 u1LoopDetectFlag   - Flag Indicates whether LoopDetection */
/*                                      is Enabled or disabled.              */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpSetLoopDetectionMethod (UINT1 u1LoopDetectMethod, UINT1 u1LoopDetectFlag,
                           UINT4 u4FsMplsLdpLsrIncarnId)
{
    switch (u1LoopDetectMethod)
    {
        case LDP_LOOP_DETECT_CAPABLE_HC:

            if (u1LoopDetectFlag == LDP_ZERO)
            {

                if ((LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                     == LDP_LOOP_DETECT_CAPABLE_PV)
                    || (LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                        == LDP_LOOP_DETECT_CAPABLE_HCPV))
                {
                    LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                        = LDP_LOOP_DETECT_CAPABLE_PV;
                }
                else
                {
                    LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                        = LDP_LOOP_DETECT_NOT_CAPABLE;
                }
                break;
            }

            if (LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                == LDP_LOOP_DETECT_NOT_CAPABLE)
            {
                LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                    = LDP_LOOP_DETECT_CAPABLE_HC;
            }
            else
            {
                if ((LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                     == LDP_LOOP_DETECT_CAPABLE_PV)
                    || (LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                        == LDP_LOOP_DETECT_CAPABLE_HCPV))
                {
                    LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                        = LDP_LOOP_DETECT_CAPABLE_HCPV;
                }
            }

            break;

        case LDP_LOOP_DETECT_CAPABLE_PV:

            if (u1LoopDetectFlag == LDP_ZERO)
            {

                if ((LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                     == LDP_LOOP_DETECT_CAPABLE_HC)
                    || (LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                        == LDP_LOOP_DETECT_CAPABLE_HCPV))
                {
                    LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId) =
                        LDP_LOOP_DETECT_CAPABLE_HC;
                }
                else
                {
                    LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId) =
                        LDP_LOOP_DETECT_NOT_CAPABLE;
                }
                break;
            }

            if (LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId) ==
                LDP_LOOP_DETECT_NOT_CAPABLE)
            {
                LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId) =
                    LDP_LOOP_DETECT_CAPABLE_PV;
            }
            else
            {
                if ((LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                     == LDP_LOOP_DETECT_CAPABLE_HC)
                    || (LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                        == LDP_LOOP_DETECT_CAPABLE_HCPV))
                {
                    LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId)
                        = LDP_LOOP_DETECT_CAPABLE_HCPV;
                }
            }
            break;
        case LDP_LOOP_DETECT_NOT_CAPABLE:

            /*Currently this case will never be satisfied */
            LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId) =
                LDP_LOOP_DETECT_NOT_CAPABLE;
            break;

        default:

            LDP_DBG (LDP_MAIN_MISC,
                     "Loop detection set failure: Invalid method\n");
            return LDP_FAILURE;
    }
    LDP_DBG1 (LDP_MAIN_MISC,
              "Loop detection methd set: %d\n",
              LDP_LOOP_DETECT (u4FsMplsLdpLsrIncarnId));
    return LDP_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsFecUpdateSysTime
 *  Description     : Function to Update Fec Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
MplsFecUpdateSysTime ()
{
    OsixGetSysTime ((tOsixSysTime *) & gFecSetTime);
}

/************************************************************************
 *  Function Name   : MplsFecGetSysTime
 *  Description     : Function to return FEC Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : Last Write Access Time
 ************************************************************************/
UINT4
MplsFecGetSysTime ()
{
    return (gFecSetTime);
}

/************************************************************************
 *  Function Name   : MplsLdpEntityUpdateSysTime
 *  Description     : Function to Update Fec Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
MplsLdpEntityUpdateSysTime ()
{
    OsixGetSysTime ((tOsixSysTime *) & gLdpEntitySetTime);
}

/************************************************************************
 *  Function Name   : MplLdpEntitysGetSysTime
 *  Description     : Function to return FEC Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : Last Write Access Time
 ************************************************************************/
UINT4
MplsLdpEntityGetSysTime ()
{
    return (gLdpEntitySetTime);
}

/************************************************************************
 *  Function Name   : MplsLdpLspFecUpdateSysTime
 *  Description     : Function to Update LDPLDP Fec Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
MplsLdpLspFecUpdateSysTime ()
{
    OsixGetSysTime ((tOsixSysTime *) & gLdpLspFecSetTime);
}

/************************************************************************
 *  Function Name   : MplsLdpLspFecGetSysTime
 *  Description     : Function to return LDPLSP FEC Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : Last Write Access Time
 ************************************************************************/
UINT4
MplsLdpLspFecGetSysTime ()
{
    return (gLdpLspFecSetTime);
}

/************************************************************************
 *  Function Name   : MplsLdPeerpUpdateSysTime
 *  Description     : Function to Update LDP Peer Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
MplsLdpPeerUpdateSysTime ()
{
    OsixGetSysTime ((tOsixSysTime *) & (gLdpPeerSetTime));
}

/************************************************************************
 *  Function Name   : MplsLdpPeerFecGetSysTime
 *  Description     : Function to return LDP Peer Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : Last Write Access Time
 ************************************************************************/
UINT4
MplsLdpPeerGetSysTime ()
{
    return (gLdpPeerSetTime);
}

/************************************************************************
 *  Function Name   : MplsLdpSessionUpdateSysTime
 *  Description     : Function to Update LDP Peer Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
MplsLdpSessionUpdateSysTime (tLdpSession * pSessionEntry)
{
    OsixGetSysTime ((tOsixSysTime *) & (pSessionEntry->u4LastChange));
}

/************************************************************************
 *  Function Name   : MplsLdpSessionFecGetSysTime
 *  Description     : Function to return LDP Peer Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : Last Write Access Time
 ************************************************************************/
UINT4
MplsLdpSessionGetSysTime (tLdpSession * pSessionEntry)
{
    return (pSessionEntry->u4LastChange);
}

/************************************************************************
 *  Function Name   : MplsLdpEntityInitSsnThresholdNotify
 *  Description     : This function sends Notification for each time 
 *                    mplsLdpEntityInitSessionThreshold  is exceeded. 
 *  Input           : pLdpEntity : Pointer to LdpEntity
 *  Output          : NONE
 *  Returns         : LDP_SUCCESS or LDP_FAILURE
 ************************************************************************/
INT1
MplsLdpEntityInitSsnThresholdNotify (tLdpEntity * pLdpEntity)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL, *pOidNext = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4ArrLen;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4MplsLDPNotify[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 4, 0, 1 };
    UINT4               au4MplsLDPSsnThreshold[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 4, 1, 2, 3, 1, 11, 0, 0, 0, 0, 0, 0, 0 };
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    if (pLdpEntity == NULL)
    {
        return LDP_FAILURE;
    }
    /* Trap OID construction. Telling Manager that you have received
     * trap for MplsLDPNotifications.x */
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        return LDP_FAILURE;
    }
    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4MplsLDPNotify) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        free_oid (pOid);
        return LDP_FAILURE;
    }
    MEMCPY (pOidValue->pu4_OidList, au4MplsLDPNotify,
            sizeof (au4MplsLDPNotify));
    pOidValue->u4_Length = sizeof (au4MplsLDPNotify) / sizeof (UINT4);

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, (INT4) 0, NULL, pOidValue,
                                     SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        return LDP_FAILURE;
    }

    pStartVb = pVbList;
    /* Value of the Trap  -> MplsLdpEntityInitSessionThreshold.mplsLdpEntityLdpId.
     * mplsLdpEntityIndex */
    u4ArrLen = sizeof (au4MplsLDPSsnThreshold) / sizeof (UINT4);
    pOidNext = alloc_oid ((INT4) u4ArrLen);
    if (pOidNext == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }
    au4MplsLDPSsnThreshold[u4ArrLen - 7] = pLdpEntity->LdpId[0];
    au4MplsLDPSsnThreshold[u4ArrLen - 6] = pLdpEntity->LdpId[1];
    au4MplsLDPSsnThreshold[u4ArrLen - 5] = pLdpEntity->LdpId[2];
    au4MplsLDPSsnThreshold[u4ArrLen - 4] = pLdpEntity->LdpId[3];
    au4MplsLDPSsnThreshold[u4ArrLen - 3] = pLdpEntity->LdpId[4];
    au4MplsLDPSsnThreshold[u4ArrLen - 2] = pLdpEntity->LdpId[5];
    au4MplsLDPSsnThreshold[u4ArrLen - 1] = pLdpEntity->u4EntityIndex;
    MEMCPY (pOidNext->pu4_OidList, au4MplsLDPSsnThreshold,
            sizeof (au4MplsLDPSsnThreshold));
    pOidNext->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOidNext,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) pLdpEntity->
                                                  u4FailInitSessThreshold, NULL,
                                                  NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOidNext);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }
    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* Sending Trap to SNMP Module */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    UNUSED_PARAM (pLdpEntity);
#endif
    return (LDP_SUCCESS);
}

/************************************************************************
 *  Function Name   : MplsLdpPVLimitNotification
 *  Description     : This function sends Notification for the XC UP and
 *                    DOWN to the Manager
 *  Input           : pLdpEntity : Pointer to XC Entry
 *                    Thersold
 *  Output          : NONE
 *  Returns         : LDP_SUCCESS or LDP_FAILURE
 ************************************************************************/
INT1
MplsLdpPVLimitNotification (tLdpPeer * pLdpPeer)
{
#ifdef SNMP_3_WANTED
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL,
        *pEntityPVLimitOid = NULL, *pPeerPvLimitOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4ArrLen;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4MplsLDPNotify[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 4, 0, 2 };
    UINT4               au4MplsEntityPVLimit[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 4, 1, 2, 3, 1, 14, 0, 0, 0, 0, 0, 0, 0 };
    UINT4               au4MplsPeerPVLimit[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 4, 1, 3, 2, 1, 30, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0
    };
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    if ((pLdpPeer == NULL) || (pLdpPeer->pLdpEntity == NULL))
    {
        return LDP_FAILURE;
    }
    /* Trap OID construction. Telling Manager that you have received
     * trap for MplsLDPNotifications.x */
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        return LDP_FAILURE;
    }
    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4MplsLDPNotify) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        free_oid (pOid);
        return LDP_FAILURE;
    }
    MEMCPY (pOidValue->pu4_OidList, au4MplsLDPNotify,
            sizeof (au4MplsLDPNotify));
    pOidValue->u4_Length = sizeof (au4MplsLDPNotify) / sizeof (UINT4);

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        return LDP_FAILURE;
    }

    pStartVb = pVbList;
    u4ArrLen = sizeof (au4MplsEntityPVLimit) / sizeof (UINT4);
    pEntityPVLimitOid = alloc_oid ((INT4) u4ArrLen);
    if (pEntityPVLimitOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }
    au4MplsEntityPVLimit[u4ArrLen - 7] = pLdpPeer->pLdpEntity->LdpId[0];
    au4MplsEntityPVLimit[u4ArrLen - 6] = pLdpPeer->pLdpEntity->LdpId[1];
    au4MplsEntityPVLimit[u4ArrLen - 5] = pLdpPeer->pLdpEntity->LdpId[2];
    au4MplsEntityPVLimit[u4ArrLen - 4] = pLdpPeer->pLdpEntity->LdpId[3];
    au4MplsEntityPVLimit[u4ArrLen - 3] = pLdpPeer->pLdpEntity->LdpId[4];
    au4MplsEntityPVLimit[u4ArrLen - 2] = pLdpPeer->pLdpEntity->LdpId[5];
    au4MplsEntityPVLimit[u4ArrLen - 1] = pLdpPeer->pLdpEntity->u4EntityIndex;
    MEMCPY (pEntityPVLimitOid->pu4_OidList, au4MplsEntityPVLimit,
            sizeof (au4MplsEntityPVLimit));
    pEntityPVLimitOid->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pEntityPVLimitOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) pLdpPeer->pLdpEntity->
                                                  u1PathVecLimit, NULL, NULL,
                                                  SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pEntityPVLimitOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;
    u4ArrLen = sizeof (au4MplsPeerPVLimit) / sizeof (UINT4);
    pPeerPvLimitOid = alloc_oid ((INT4) u4ArrLen);
    if (pPeerPvLimitOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pEntityPVLimitOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }
    au4MplsPeerPVLimit[u4ArrLen - 13] = pLdpPeer->pLdpEntity->LdpId[0];
    au4MplsPeerPVLimit[u4ArrLen - 12] = pLdpPeer->pLdpEntity->LdpId[1];
    au4MplsPeerPVLimit[u4ArrLen - 11] = pLdpPeer->pLdpEntity->LdpId[2];
    au4MplsPeerPVLimit[u4ArrLen - 10] = pLdpPeer->pLdpEntity->LdpId[3];
    au4MplsPeerPVLimit[u4ArrLen - 9] = pLdpPeer->pLdpEntity->LdpId[4];
    au4MplsPeerPVLimit[u4ArrLen - 8] = pLdpPeer->pLdpEntity->LdpId[5];
    au4MplsPeerPVLimit[u4ArrLen - 7] = pLdpPeer->pLdpEntity->u4EntityIndex;
    au4MplsPeerPVLimit[u4ArrLen - 6] = pLdpPeer->PeerLdpId[0];
    au4MplsPeerPVLimit[u4ArrLen - 5] = pLdpPeer->PeerLdpId[1];
    au4MplsPeerPVLimit[u4ArrLen - 4] = pLdpPeer->PeerLdpId[2];
    au4MplsPeerPVLimit[u4ArrLen - 3] = pLdpPeer->PeerLdpId[3];
    au4MplsPeerPVLimit[u4ArrLen - 2] = pLdpPeer->PeerLdpId[4];
    au4MplsPeerPVLimit[u4ArrLen - 1] = pLdpPeer->PeerLdpId[5];

    MEMCPY (pPeerPvLimitOid->pu4_OidList, au4MplsPeerPVLimit,
            sizeof (au4MplsPeerPVLimit));
    pPeerPvLimitOid->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pPeerPvLimitOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) pLdpPeer->pLdpEntity->
                                                  u1PathVecLimit, NULL,
                                                  NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pEntityPVLimitOid);
        free_oid (pPeerPvLimitOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    pVbList->pNextVarBind = NULL;

    /* Sending Trap to SNMP Module */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    UNUSED_PARAM (pLdpPeer);
#endif
    return (LDP_SUCCESS);
}

/************************************************************************
 *  Function Name   : MplsSessionNotification
 *  Description     : This function sends Notification for the XC UP and
 *                    DOWN to the Manager
 *  Input           : pLdpEntity : Pointer to XC Entry
 *                    Thersold
 *  Output          : NONE
 *  Returns         : LDP_SUCCESS or LDP_FAILURE
 ************************************************************************/
INT1
MplsSessionNotification (tLdpSession * pLdpSession, UINT1 u1State)
{
#ifdef SNMP_3_WANTED
    UINT4               u4ArrLen;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL, *pSsnStateOid = NULL,
        *pDiscTimeOid = NULL, *pMsgErrOid = NULL, *pTlvErrOid = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4MplsSessionNotify[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 4, 0, 0 };
    UINT4               au4MplsSessionState[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 4, 1, 3, 3, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0
    };
    UINT4               au4MplsSessionDiscontinuityTime[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 4, 1, 3, 3, 1, 8, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0
    };
    UINT4               au4MplsSessionUnknownMsgTypeErrors[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 4, 1, 3, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0
    };
    UINT4               au4MplsSessionUnknownTlvErrors[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 4, 1, 3, 4, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0
    };
    UINT1               u1SessionState = 0;
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    if (pLdpSession->pLdpPeer->pLdpEntity == NULL)
    {
        return LDP_FAILURE;
    }
    /* Trap OID construction. Telling Manager that you have received
     * trap for MplsLDPNotifications.x */
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        return LDP_FAILURE;
    }
    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4MplsSessionNotify) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        free_oid (pOid);
        return LDP_FAILURE;
    }
    au4MplsSessionNotify[(sizeof (au4MplsSessionNotify) / sizeof (UINT4)) - 1] =
        u1State;
    MEMCPY (pOidValue->pu4_OidList, au4MplsSessionNotify,
            sizeof (au4MplsSessionNotify));
    pOidValue->u4_Length = sizeof (au4MplsSessionNotify) / sizeof (UINT4);

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, (INT4) 0, NULL, pOidValue,
                                     SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        return LDP_FAILURE;
    }
    pStartVb = pVbList;
    u4ArrLen = sizeof (au4MplsSessionState) / sizeof (UINT4);
    pSsnStateOid = alloc_oid ((INT4) u4ArrLen);
    if (pSsnStateOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }
    au4MplsSessionState[u4ArrLen - 13] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[0];
    au4MplsSessionState[u4ArrLen - 12] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[1];
    au4MplsSessionState[u4ArrLen - 11] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[2];
    au4MplsSessionState[u4ArrLen - 10] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[3];
    au4MplsSessionState[u4ArrLen - 9] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[4];
    au4MplsSessionState[u4ArrLen - 8] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[5];
    au4MplsSessionState[u4ArrLen - 7] =
        pLdpSession->pLdpPeer->pLdpEntity->u4EntityIndex;
    au4MplsSessionState[u4ArrLen - 6] = pLdpSession->pLdpPeer->PeerLdpId[0];
    au4MplsSessionState[u4ArrLen - 5] = pLdpSession->pLdpPeer->PeerLdpId[1];
    au4MplsSessionState[u4ArrLen - 4] = pLdpSession->pLdpPeer->PeerLdpId[2];
    au4MplsSessionState[u4ArrLen - 3] = pLdpSession->pLdpPeer->PeerLdpId[3];
    au4MplsSessionState[u4ArrLen - 2] = pLdpSession->pLdpPeer->PeerLdpId[4];
    au4MplsSessionState[u4ArrLen - 1] = pLdpSession->pLdpPeer->PeerLdpId[5];

    MEMCPY (pSsnStateOid->pu4_OidList, au4MplsSessionState,
            sizeof (au4MplsSessionState));
    pSsnStateOid->u4_Length = u4ArrLen;

    if (u1State == LDP_TRAP_SESS_DN)
    {
        u1SessionState = LDP_SSM_ST_NON_EXISTENT;
    }
    else
    {
        u1SessionState = LDP_SSM_ST_OPERATIONAL;
    }

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pSsnStateOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) u1SessionState,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pSsnStateOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    u4ArrLen = sizeof (au4MplsSessionDiscontinuityTime) / sizeof (UINT4);
    pDiscTimeOid = alloc_oid ((INT4) u4ArrLen);
    if (pDiscTimeOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pSsnStateOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }
    au4MplsSessionDiscontinuityTime[u4ArrLen - 13] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[0];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 12] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[1];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 11] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[2];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 10] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[3];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 9] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[4];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 8] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[5];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 7] =
        pLdpSession->pLdpPeer->pLdpEntity->u4EntityIndex;
    au4MplsSessionDiscontinuityTime[u4ArrLen - 6] =
        pLdpSession->pLdpPeer->PeerLdpId[0];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 5] =
        pLdpSession->pLdpPeer->PeerLdpId[1];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 4] =
        pLdpSession->pLdpPeer->PeerLdpId[2];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 3] =
        pLdpSession->pLdpPeer->PeerLdpId[3];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 2] =
        pLdpSession->pLdpPeer->PeerLdpId[4];
    au4MplsSessionDiscontinuityTime[u4ArrLen - 1] =
        pLdpSession->pLdpPeer->PeerLdpId[5];

    MEMCPY (pDiscTimeOid->pu4_OidList, au4MplsSessionDiscontinuityTime,
            sizeof (au4MplsSessionDiscontinuityTime));
    pDiscTimeOid->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pDiscTimeOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) pLdpSession->
                                                  u2DisContTime, NULL, NULL,
                                                  SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pSsnStateOid);
        free_oid (pDiscTimeOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    u4ArrLen = sizeof (au4MplsSessionUnknownMsgTypeErrors) / sizeof (UINT4);
    pMsgErrOid = alloc_oid ((INT4) u4ArrLen);
    if (pMsgErrOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pSsnStateOid);
        free_oid (pDiscTimeOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 13] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[0];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 12] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[1];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 11] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[2];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 10] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[3];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 9] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[4];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 8] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[5];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 7] =
        pLdpSession->pLdpPeer->pLdpEntity->u4EntityIndex;
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 6] =
        pLdpSession->pLdpPeer->PeerLdpId[0];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 5] =
        pLdpSession->pLdpPeer->PeerLdpId[1];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 4] =
        pLdpSession->pLdpPeer->PeerLdpId[2];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 3] =
        pLdpSession->pLdpPeer->PeerLdpId[3];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 2] =
        pLdpSession->pLdpPeer->PeerLdpId[4];
    au4MplsSessionUnknownMsgTypeErrors[u4ArrLen - 1] =
        pLdpSession->pLdpPeer->PeerLdpId[5];

    MEMCPY (pMsgErrOid->pu4_OidList, au4MplsSessionUnknownMsgTypeErrors,
            sizeof (au4MplsSessionUnknownMsgTypeErrors));
    pMsgErrOid->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pMsgErrOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) pLdpSession->
                                                  u4StatsUnknownMsgTypeErrors,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pSsnStateOid);
        free_oid (pDiscTimeOid);
        free_oid (pMsgErrOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }
    pVbList = pVbList->pNextVarBind;

    u4ArrLen = sizeof (au4MplsSessionUnknownTlvErrors) / sizeof (UINT4);
    pTlvErrOid = alloc_oid ((INT4) u4ArrLen);
    if (pTlvErrOid == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pSsnStateOid);
        free_oid (pDiscTimeOid);
        free_oid (pMsgErrOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 13] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[0];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 12] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[1];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 11] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[2];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 10] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[3];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 9] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[4];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 8] =
        pLdpSession->pLdpPeer->pLdpEntity->LdpId[5];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 7] =
        pLdpSession->pLdpPeer->pLdpEntity->u4EntityIndex;
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 6] =
        pLdpSession->pLdpPeer->PeerLdpId[0];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 5] =
        pLdpSession->pLdpPeer->PeerLdpId[1];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 4] =
        pLdpSession->pLdpPeer->PeerLdpId[2];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 3] =
        pLdpSession->pLdpPeer->PeerLdpId[3];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 2] =
        pLdpSession->pLdpPeer->PeerLdpId[4];
    au4MplsSessionUnknownTlvErrors[u4ArrLen - 1] =
        pLdpSession->pLdpPeer->PeerLdpId[5];

    MEMCPY (pTlvErrOid->pu4_OidList, au4MplsSessionUnknownTlvErrors,
            sizeof (au4MplsSessionUnknownTlvErrors));
    pTlvErrOid->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pTlvErrOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) pLdpSession->
                                                  u4StatsUnknownTlvErrors, NULL,
                                                  NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        free_oid (pSsnStateOid);
        free_oid (pDiscTimeOid);
        free_oid (pMsgErrOid);
        free_oid (pTlvErrOid);
        SNMP_free_snmp_vb_list (pStartVb);
        return LDP_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* Sending Trap to SNMP Module */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);
#else
    UNUSED_PARAM (pLdpSession);
    UNUSED_PARAM (u1State);
#endif
    return (LDP_SUCCESS);
}

/************************************************************************
 *  Function Name   : LdpDeleteInterfaceControlBlock
 *  Description     : Function to delete the LSP on a particular interface
 *                    for a particular Session  
 *                    
 *  Input           : pSessionEntry - Session Entry Pointer
 *                    u4IfIndex - Interface Index
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/
VOID
LdpDeleteInterfaceControlBlock (tLdpSession * pSessionEntry, UINT4 u4IfIndex)
{
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    UINT4               u4TnlOutIfIndex = 0, u4TnlInIfIndex = 0;
    UINT4               u4L3VlanIf = 0;
    UINT4               u4SessIfIndex = 0;
    tTMO_SLL_NODE      *pSllNode = NULL;

    pSllNode = TMO_SLL_First (&SSN_ULCB (pSessionEntry));
    while (pSllNode != NULL)
    {
        pLspCtrlBlock = (tLspCtrlBlock *) pSllNode;
        pSllNode = TMO_SLL_Next (&SSN_ULCB (pSessionEntry), pSllNode);
        u4TnlOutIfIndex = pLspCtrlBlock->u4OutIfIndex;
        u4TnlInIfIndex = pLspCtrlBlock->u4InComIfIndex;
        if (((CfaUtilGetIfIndexFromMplsTnlIf
              (u4TnlOutIfIndex, &u4L3VlanIf, TRUE) == CFA_SUCCESS) &&
             (u4L3VlanIf == u4IfIndex)) ||
            ((CfaUtilGetIfIndexFromMplsTnlIf
              (u4TnlInIfIndex, &u4L3VlanIf, TRUE) == CFA_SUCCESS) &&
             (u4L3VlanIf == u4IfIndex)))

        {
            if (LCB_DSSN (pLspCtrlBlock) != NULL)
            {
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
            }
            if (LCB_USSN (pLspCtrlBlock) != NULL)
            {
                LdpSendLblWithdrawMsg (&LCB_FEC (pLspCtrlBlock),
                                       &LCB_ULBL (pLspCtrlBlock),
                                       LCB_USSN (pLspCtrlBlock));
            }
            if ((LCB_DSSN (pLspCtrlBlock) != NULL)
                && (LCB_USSN (pLspCtrlBlock) != NULL))
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                                       pLspCtrlBlock, NULL);
                u4SessIfIndex =
                    LdpSessionGetIfIndex (pSessionEntry,
                                          pLspCtrlBlock->Fec.u2AddrFmly);
                LdpFreeLabel (SSN_GET_ENTITY (pSessionEntry),
                              &LCB_ULBL (pLspCtrlBlock), u4SessIfIndex);
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        }
    }

    pSllNode = TMO_SLL_First (&SSN_DLCB (pSessionEntry));
    while (pSllNode != NULL)
    {
        pLspCtrlBlock = SLL_TO_LCB ((tLspCtrlBlock *) pSllNode);
        pSllNode = TMO_SLL_Next (&SSN_DLCB (pSessionEntry), pSllNode);
        u4TnlOutIfIndex = pLspCtrlBlock->u4OutIfIndex;
        u4TnlInIfIndex = pLspCtrlBlock->u4InComIfIndex;
        if (((CfaUtilGetIfIndexFromMplsTnlIf
              (u4TnlOutIfIndex, &u4L3VlanIf, TRUE) == CFA_SUCCESS) &&
             (u4L3VlanIf == u4IfIndex)) ||
            ((CfaUtilGetIfIndexFromMplsTnlIf
              (u4TnlInIfIndex, &u4L3VlanIf, TRUE) == CFA_SUCCESS) &&
             (u4L3VlanIf == u4IfIndex)))

        {
            if (LCB_DSSN (pLspCtrlBlock) != NULL)
            {
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
            }
            if (LCB_USSN (pLspCtrlBlock) != NULL)
            {
                LdpSendLblWithdrawMsg (&LCB_FEC (pLspCtrlBlock),
                                       &LCB_ULBL (pLspCtrlBlock),
                                       LCB_USSN (pLspCtrlBlock));
            }
            if ((LCB_DSSN (pLspCtrlBlock) != NULL)
                && (LCB_USSN (pLspCtrlBlock) != NULL))
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                                       pLspCtrlBlock, NULL);
                u4SessIfIndex =
                    LdpSessionGetIfIndex (pSessionEntry,
                                          pLspCtrlBlock->Fec.u2AddrFmly);
                LdpFreeLabel (SSN_GET_ENTITY (pSessionEntry),
                              &LCB_ULBL (pLspCtrlBlock), u4SessIfIndex);
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        }
    }

}

/************************************************************************
 *  Function Name   : LdpLock 
 *  Description     : Function used in SNMP context to lock the LDP 
 *                    module before accessing LDP SNMP APIs 
 *                    (nmh routines)
 *  Input           : None
 *  Output          : None
 *  Returns         : SNMP_SUCCESS on Success otherwise SNMP_FAILURE 
 ************************************************************************/
INT4
LdpLock (VOID)
{
    if (LDP_SEM_ID == NULL)
    {
        return SNMP_FAILURE;
    }
    if (OsixSemTake (LDP_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : LdpUnLock 
 *  Description     : Function used in SNMP context to unlock the LDP 
 *                    module after accessing LDP SNMP APIs 
 *                    (nmh routines)
 *  Input           : None
 *  Output          : None
 *  Returns         : SNMP_SUCCESS on Success otherwise SNMP_FAILURE 
 ************************************************************************/

INT4
LdpUnLock (VOID)
{
    if (LDP_SEM_ID == NULL)
    {
        return SNMP_FAILURE;
    }
    if (OsixSemGive (LDP_SEM_ID) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/******************************************************************************
 *  Function Name   : LdpRetriggerEntitySessions
 *  Description     : Function used to Retrigger the LDP Entity Sessions when
 *                    LDP Router-Id is changed and the interface of the current
 *                    active LDP Router ID goes down. 
 *  Input           : u4IpAddress - LDP Router ID to be changed.
 *                    u4IfIndex   - Interface Index which to be matched
 *                    u4IfaceIp   - Existing LDP Router ID  
 *                    u4Event     - Indicates the type of the event that caused 
 *                                  calling this function.
 *                                  LDP_IF_OPER_DISABLE - Interface of Current
 *                                                        Active LDP Router ID 
 *                                                        has gone down.
 *                                  LDP_ZERO            - Forceful Router ID
 *                                                        Change.
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
LdpRetriggerEntitySessions (UINT4 u4IpAddress, UINT4 u4IfIndex,
                            UINT4 u4IfaceIp, UINT4 u4Event)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT2               u2IncarnId = LDP_ZERO;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4IfInfo      NetIpIfInfo;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4HighestAddr = LDP_ZERO;
    BOOL1               bFlag = FALSE;
    UINT4               u4Mask = 0xffffffff;
    UINT4               u4NvRamIpAddr = LDP_ZERO;
    tLdpPeer           *pLdpPeer = NULL;

    UNUSED_PARAM (u4IfIndex);

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    LDP_DBG ((LDP_IF_MISC | LDP_IF_ALL),
             "\r LdpRetriggerEntitySessions: FUNC ENTRY \n ");

    /* If interface of the Router ID configured previously goes down,
     * Tear down all the LDP Sessions available. */
    pLdpEntity = (tLdpEntity *) TMO_SLL_First (&LDP_ENTITY_LIST (u2IncarnId));
    while (pLdpEntity != NULL)
    {
        LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL | LDP_SSN_PRCS | LDP_SSN_ALL),
                  "\r LdpRetriggerEntitySessions: LDP Entity with Entity "
                  "Index %d is made down \n", pLdpEntity->u4EntityIndex);
        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE)
        {
            /* Delete and free the Local Peer List */
            TMO_SLL_Scan (&pLdpEntity->PeerList, pLdpPeer, tLdpPeer *)
            {
                if ((pLdpPeer != NULL) && (pLdpPeer->pLdpSession != NULL))
                {
                    pLdpPeer->pLdpSession->u1StatusCode =
                        LDP_STAT_TYPE_SHUT_DOWN;
                }
            }
        }
        else
        {
            pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
            if (pLdpPeer == NULL)
            {
                continue;
            }
            if (pLdpPeer->pLdpSession != NULL)
            {
                pLdpPeer->pLdpSession->u1StatusCode = LDP_STAT_TYPE_SHUT_DOWN;
            }
        }

        LdpDownLdpEntity (pLdpEntity);

        pLdpEntity = (tLdpEntity *)
            TMO_SLL_Next (&LDP_ENTITY_LIST (u2IncarnId),
                          &pLdpEntity->NextLdpEntity);
    }

    if (u4Event == LDP_IF_OPER_DISABLE)
    {
        LDP_DBG ((LDP_IF_PRCS | LDP_IF_ALL),
                 "\r LdpRetriggerEntitySessions: Function was triggered "
                 " through Interface down event \n");

        u4NvRamIpAddr = IssGetIpAddrFromNvRam ();
        u4NvRamIpAddr = OSIX_HTONL (u4NvRamIpAddr);

        /*If IF_OPER_DISABLE event is for NVRAM IP, no need to check for OPER Status */
        if (u4IpAddress != u4NvRamIpAddr)
        {
            /* In this case, u4IpAddress is new LSR id. If new LSR id is a
             * valid ip and it is in operationally up state, it should be
             * elected as LSR id
             * */
            RtQuery.u4DestinationIpAddress = OSIX_NTOHL (u4IpAddress);
            RtQuery.u4DestinationSubnetMask = u4Mask;
            RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

            if ((u4IpAddress == LDP_ZERO) ||
                (u4IpAddress == u4IfaceIp) ||
                (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) != NETIPV4_SUCCESS))
            {
                do
                {
                    if ((bFlag == FALSE) &&
                        (NetIpv4GetFirstIfInfo (&NetIpIfInfo) ==
                         NETIPV4_FAILURE))
                    {
                        break;
                    }

                    bFlag = TRUE;

                    RtQuery.u4DestinationIpAddress = NetIpIfInfo.u4Addr;
                    RtQuery.u4DestinationSubnetMask = NetIpIfInfo.u4NetMask;
                    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

                    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) ==
                        NETIPV4_SUCCESS)
                    {
                        u4HighestAddr =
                            LDP_MAX (u4HighestAddr, NetIpIfInfo.u4Addr);
                    }
                }
                while (NetIpv4GetNextIfInfo
                       (NetIpIfInfo.u4IfIndex,
                        &NetIpIfInfo) == NETIPV4_SUCCESS);

                u4IpAddress = OSIX_HTONL (u4HighestAddr);

            }
        }
    }
    else
    {
        LDP_DBG ((LDP_IF_PRCS | LDP_IF_ALL),
                 "\r LdpRetriggerEntitySessions: Function was triggered "
                 " through force LDP LSR ID Change \n");
    }

    if (u4IpAddress != LDP_ZERO)
    {
        LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL),
                  "\r LdpRetriggerEntitySessions: LDP LSR ID is changed"
                  " with the configured value %x\n", u4IpAddress);
        MEMCPY ((UINT1 *) (LDP_LSR_ID (u2IncarnId).au1Ipv4Addr),
                (UINT1 *) &u4IpAddress, LDP_LSR_ID_LEN);

        if (LDP_FORCE_OPTION (MPLS_DEF_INCARN) == LDP_SNMP_FALSE)
        {
            MEMSET ((UINT1 *) (LDP_NEW_LSR_ID (u2IncarnId).au1Ipv4Addr),
                    LDP_ZERO, LDP_LSR_ID_LEN);
        }

        pLdpEntity = (tLdpEntity *)
            TMO_SLL_First (&LDP_ENTITY_LIST (u2IncarnId));
        while (pLdpEntity != NULL)
        {
            /* From RFC 3036, LDP Identifier depends on the LSR
             * Identifier. So, this is required. Otherwise all the LDP
             * Entities will become unusable. */

            MEMCPY ((UINT1 *) pLdpEntity->LdpId,
                    (UINT1 *) (LDP_LSR_ID (u2IncarnId).au1Ipv4Addr),
                    LDP_LSR_ID_LEN);

            LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL | LDP_SSN_PRCS | LDP_SSN_ALL),
                      "\r LdpRetriggerEntitySessions: LDP Entity LDP ID "
                      "changed to the new LDP LSR ID %x \n",
                      LDP_LSR_ID (u2IncarnId).au1Ipv4Addr);

            LDP_DBG1 ((LDP_IF_PRCS | LDP_IF_ALL | LDP_SSN_PRCS | LDP_SSN_ALL),
                      "\r LdpRetriggerEntitySessions: LDP Entity with Entity "
                      "Index %d is made up \n", pLdpEntity->u4EntityIndex);

            LdpUpLdpEntity (pLdpEntity);

            pLdpEntity = (tLdpEntity *)
                TMO_SLL_Next (&LDP_ENTITY_LIST (u2IncarnId),
                              &pLdpEntity->NextLdpEntity);
        }
    }
    else
    {
        LDP_DBG ((LDP_IF_PRCS | LDP_IF_ALL),
                 "\r LdpRetriggerEntitySessions: "
                 "LDP LSR ID is NOT Changed as the IP Address is ZERO \n");
    }
    LDP_DBG ((LDP_IF_PRCS | LDP_IF_ALL),
             "\r LdpRetriggerEntitySessions: FUNC EXIT \n ");
    return;
}

/*****************************************************************************/
/* Function Name : LdpShutDownProcess                                        */
/* Description   : This routine does a graceful shut down of the ldp protocol*/
/*                 running on an Lsr.                                        */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpShutDownProcess (VOID)
{
    UINT2               u2IncarnId = LDP_ZERO;

    MPLS_LDP_LOCK ();

    /* Change the GR Progress Status in All the Incarnation supported */
    for (u2IncarnId = MPLS_DEF_INCARN; u2IncarnId < LDP_MAX_INCARN;
         u2IncarnId++)
    {
        gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus =
            LDP_GR_SHUT_DOWN_IN_PROGRESS;
    }

    LdpDeInit ();
#ifdef LDP_HA_WANTED
    LdpRmDeRegisterWithRM ();
#endif
    /* Delete all the Created Queues */
    OsixDeleteQ (SELF, (const UINT1 *) LDPTCP_QNAME);

    OsixDeleteQ (SELF, (const UINT1 *) LDP_QNAME);

    OsixDeleteQ (SELF, (const UINT1 *) LDP_UDP_QNAME);

    LDP_UDP_QID = LDP_ZERO;
    LDP_QID = LDP_ZERO;
    LDP_TCP_QID = LDP_ZERO;

    gLdpInfo.u1IncarnUp = LDP_FALSE;

    MPLS_LDP_UNLOCK ();

    /* Delete the sem */
    OsixSemDel (LDP_SEM_ID);
    MEMSET (&gLdpInfo, LDP_ZERO, sizeof (tLdpGlobalInfo));

    /* Change the GR Progress Status in All the Incarnation supported */
    /* Check is intentional */
    for (u2IncarnId = MPLS_DEF_INCARN; u2IncarnId < LDP_MAX_INCARN;
         u2IncarnId++)
    {
        gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus =
            LDP_GR_SHUT_DOWN_IN_PROGRESS;
    }

    /* Delete the Created Task */
    OsixDeleteTask (SELF, (const UINT1 *) "LDPT");
}

#ifdef SNMP_2_WANTED
/*****************************************************************************/
/* Function Name : LdpUnRegisterLDPMibs                                      */
/* Description   : This function Unregisters the LDP MIBS                    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpUnRegisterLDPMibs (VOID)
{
    UnRegisterSTDLDP ();
    UnRegisterSTDGNL ();
    UnRegisterFSMPLSLdpObj ();
    UnRegisterFSMPLSScalarObj ();
#ifdef LDP_TEST_WANTED
    UnRegisterFSLDPT ();
#endif
}

/*****************************************************************************/
/* Function Name : LdpRegisterLDPMibs                                        */
/* Description   : This function Registers the LDP MIBS                      */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpRegisterLDPMibs (VOID)
{
    RegisterSTDLDP ();
    RegisterSTDGNL ();
    RegisterFSMPLSLdpObj ();
    RegisterFSMPLSScalarObj ();
#ifdef LDP_TEST_WANTED
    RegisterFSLDPT ();
#endif
}

#endif
/*****************************************************************************/
/* Function Name : LdpConvertLocalValToTLVVal                                */
/* Description   : This function converts local value used for status code   */
/*                 into TLV value carried in status TLV                      */
/* Input(s)      : u1StatusType - Local value for status code                */
/* Output(s)     : None                                                      */
/* Return(s)     : au4StatusData[u1StatusType] - TLV value for status code   */
/*****************************************************************************/

UINT4
LdpConvertLocalValToTLVVal (UINT1 u1StatusType)
{
    return (au4StatusData[u1StatusType]);
}

/*****************************************************************************/
/* Function Name : LdpCheckAndRetriggerRtAddEvent                            */
/* Description   : This function checks if any other route is present        */
/*                 for the deleted FEC and if so triggers Route Add event    */
/*                 to get label. This function is applicable only for ECMP   */
/*                 case                                                      */
/* Input(s)      : u2IncarnId        - Incarn Id                             */
/*                 pRouteInfo        - Route Info deleted                    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpCheckAndRetriggerRtAddEvent (UINT2 u2IncarnId,
                                tLdpRouteEntryInfo * pRouteInfo)
{
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    tLdpRouteEntryInfo  NewRouteInfo;
#ifdef MPLS_IPV6_WANTED
    tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
    tNetIpv6RtInfo      NetIpv6RtInfo;
#endif

#ifdef MPLS_IPV6_WANTED
    if (pRouteInfo->u2AddrType == LDP_ADDR_TYPE_IPV6)
    {
        MEMSET (&NetIpv6RtInfoQueryMsg, 0, sizeof (tNetIpv6RtInfoQueryMsg));
        MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));

        NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
        NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
        Ip6AddrCopy (&NetIpv6RtInfoQueryMsg.Ip6Dst,
                     &pRouteInfo->DestAddr.Ip6Addr);
        NetIpv6RtInfoQueryMsg.u1Prefixlen =
            MplsGetIpv6Subnetmasklen (LDP_IPV6_U4_ADDR (pRouteInfo->DestAddr));

        if (NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
                             &NetIpv6RtInfo) == NETIPV6_SUCCESS)
        {
            if (MEMCMP (&NetIpv6RtInfo.NextHop,
                        &pRouteInfo->NextHop.Ip6Addr,
                        IPV6_ADDR_LENGTH) != LDP_ZERO)
            {
                MEMCPY (&NewRouteInfo.DestAddr.Ip6Addr, &NetIpv6RtInfo.Ip6Dst,
                        IPV6_ADDR_LENGTH);

                MplsGetIPV6Subnetmask (NetIpv6RtInfo.u1Prefixlen,
                                       LDP_IPV6_ADDR (NewRouteInfo.DestMask));

                MEMCPY (&NewRouteInfo.NextHop.Ip6Addr, &NetIpv6RtInfo.NextHop,
                        IPV6_ADDR_LENGTH);
                NewRouteInfo.u4RtIfIndx = NetIpv6RtInfo.u4Index;
                NewRouteInfo.i4Metric1 = (INT4) NetIpv6RtInfo.u4Metric;
                NewRouteInfo.u4RowStatus = NetIpv6RtInfo.u4RowStatus;
                NewRouteInfo.u2AddrType = LDP_ADDR_TYPE_IPV6;

                LdpTriggerRtChgEvent (u2IncarnId, LDP_RT_NEW, &NewRouteInfo,
                                      NULL, NULL);
            }

        }
    }
    else
#endif
    {
        MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
        MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
        MEMSET (&NewRouteInfo, 0, sizeof (tLdpRouteEntryInfo));

        RtQuery.u4DestinationIpAddress =
            LDP_IPV4_U4_ADDR (pRouteInfo->DestAddr);
        RtQuery.u4DestinationSubnetMask =
            LDP_IPV4_U4_ADDR (pRouteInfo->DestMask);
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        if ((NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS) &&
            (NetIpRtInfo.u4NextHop != LDP_IPV4_U4_ADDR (pRouteInfo->NextHop)))
        {
            LDP_IPV4_U4_ADDR (NewRouteInfo.DestAddr) = NetIpRtInfo.u4DestNet;
            LDP_IPV4_U4_ADDR (NewRouteInfo.DestMask) = NetIpRtInfo.u4DestMask;
            LDP_IPV4_U4_ADDR (NewRouteInfo.NextHop) = NetIpRtInfo.u4NextHop;
            NewRouteInfo.u4RtIfIndx = NetIpRtInfo.u4RtIfIndx;
            NewRouteInfo.u4RtNxtHopAS = NetIpRtInfo.u4RtNxtHopAs;
            NewRouteInfo.i4Metric1 = NetIpRtInfo.i4Metric1;
            NewRouteInfo.u4RowStatus = NetIpRtInfo.u4RowStatus;
            NewRouteInfo.u2AddrType = LDP_ADDR_TYPE_IPV4;

            LdpTriggerRtChgEvent (u2IncarnId, LDP_RT_NEW, &NewRouteInfo, NULL,
                                  NULL);
        }
    }

    return;
}

/* MPLS_IPv6 add start*/
/*****************************************************************************/
/* Function Name : LdpProcessIpV6UdpEvents                                       */
/* Description   : This routine processes the events received from UDP       */
/* Input(s)      : aTaskEnqMsgInfo - info about events and data received     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
#ifdef MPLS_IPV6_WANTED
UINT1
LdpProcessIpv6UdpEvents (tLdpUdpEvtInfo * pUdpEvtInfo)
{
    UINT2               u2IncarnId = 0;

    if (pUdpEvtInfo->u4Evt == LDP_UDP6_DATA_EVENT)
    {
        IFINDEX_GET_INCRN_ID (pUdpEvtInfo->u4IfIndex, &u2IncarnId);
        LDP_DBG (LDP_DBG_UDP, "MAIN: Processing UDP Event\n");
        if (LdpHandleIpv6UdpPkt
            ((tCRU_BUF_CHAIN_HEADER *) (VOID *) pUdpEvtInfo->pPdu,
             pUdpEvtInfo->u4IfIndex, pUdpEvtInfo->PeerAddr,
             u2IncarnId, pUdpEvtInfo->u1DiscardV6LHello) == LDP_FAILURE)
        {
            CRU_BUF_Release_MsgBufChain ((tCRU_BUF_CHAIN_HEADER *) (VOID *)
                                         pUdpEvtInfo->pPdu, FALSE);
            return LDP_FAILURE;
        }
        CRU_BUF_Release_MsgBufChain ((tCRU_BUF_CHAIN_HEADER *) (VOID *)
                                     pUdpEvtInfo->pPdu, FALSE);
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpDisjointV6MultiCastIfaces                                */
/* Description   : This routine deletes the Interfaces from the Multicast    */
/*                 list for the Entity                                       */
/* Input(s)      : Pointer to the Entity                                     */
/*               : u4IfAddr if NON-ZERO represents the Interface for which   */
/*               :             Multicast joining has Failed                  */
/*               :          if Zero all the interfaces will be disjoined     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpDisjointV6MultiCastIfaces (tIp6Addr V6IfAddr, tLdpEntity * pLdpEntity)
{
    tLdpIfTableEntry   *pIfEntry = NULL;
    tIp6Addr            TmpV6IfAddr;
    tIp6Addr            TmpV6ZeroAddr;
    tIp6Addr            AllRouterAddr;
    INT4                i4Udp6SockFd = LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
    UINT1               u1ValidRouterAddr = LDP_FALSE;
    MEMSET (&TmpV6IfAddr, 0, sizeof (tIp6Addr));
    MEMSET (&TmpV6ZeroAddr, 0, sizeof (tIp6Addr));
    MEMSET (&AllRouterAddr, 0, sizeof (tIp6Addr));

    if (LDP_FAILURE != INET_ATON6 (LDP_ALL_ROUTERS_V6ADDR, &AllRouterAddr))
    {
        u1ValidRouterAddr = LDP_TRUE;
    }
    TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity), pIfEntry, tLdpIfTableEntry *)
    {
        MEMCPY (TmpV6IfAddr.u1_addr, pIfEntry->LnklocalIpv6Addr.u1_addr,
                LDP_IPV6ADR_LEN);
        if (LDP_ZERO !=
            MEMCMP (TmpV6ZeroAddr.u1_addr, V6IfAddr.u1_addr, LDP_IPV6ADR_LEN))
        {
            if (LDP_ZERO ==
                MEMCMP (TmpV6IfAddr.u1_addr, V6IfAddr.u1_addr, LDP_IPV6ADR_LEN))
            {
                /* All the interfaces which joined previously have left
                 * the Mcast group */
                return LDP_FAILURE;
            }
            else
            {
                if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                    pIfEntry->u4IfIndex) >
                    LDP_ZERO)
                {
                    --(LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                       pIfEntry->u4IfIndex));
                }
                if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                    pIfEntry->u4IfIndex) ==
                    LDP_ZERO)
                {
                    if (LDP_TRUE == u1ValidRouterAddr)
                    {
                        if (LDP_TRUE == u1ValidRouterAddr)
                        {
                            if (LdpUdpLeaveV6Multicast
                                (i4Udp6SockFd, V6IfAddr,
                                 &AllRouterAddr,
                                 pIfEntry->u4IfIndex) == LDP_FAILURE)
                            {
                                /* Error: Iface failed to leave the Mcast group */
                                LDP_DBG (LDP_IF_MISC,
                                         "IPV6: Leave V6 Multicast Returned Failure\n");
                                continue;
                            }
                        }
                        else
                        {
                            LDP_DBG (LDP_IF_MISC,
                                     "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
                            continue;
                        }
                    }
                    else
                    {
                        LDP_DBG (LDP_IF_MISC,
                                 "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
                        continue;
                    }
                }
            }
        }
        else
        {
            if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                pIfEntry->u4IfIndex) > LDP_ZERO)
            {
                --(LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                   pIfEntry->u4IfIndex));
            }
            if (LDP_SET_NO_OF_ENT_V6MCASTCOUNT (LDP_CUR_INCARN,
                                                pIfEntry->u4IfIndex) ==
                LDP_ZERO)
            {
                if (LDP_TRUE == u1ValidRouterAddr)
                {
                    if (LdpUdpLeaveV6Multicast
                        (i4Udp6SockFd, V6IfAddr,
                         &AllRouterAddr, pIfEntry->u4IfIndex) == LDP_FAILURE)
                    {
                        /* Error: Iface failed to leave the Mcast group */
                        LDP_DBG (LDP_IF_MISC,
                                 "IPV6: Leave V6 Multicast Returned Failure\n");
                        continue;
                    }
                }
                else
                {
                    LDP_DBG (LDP_IF_MISC,
                             "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
                    continue;
                }
            }
        }
    }
    return LDP_SUCCESS;
}

VOID
LdpIpv6LspSetReq (UINT2 u2IncarnId, tIp6Addr * pNetworkAddr, UINT1 u1PrefixLen,
                  tIp6Addr * pNextHopAddr, UINT4 u4IfIndex)
{
    UINT1               u1SsnState;
    UINT4               u4HSessIndex = 0;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tTMO_HASH_NODE     *pSsnHashNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    tNetIpv6RtInfo      InIpv6RtInfo;

    MEMSET (&InIpv6RtInfo, LDP_ZERO, sizeof (tNetIpv6RtInfo));

    MEMCPY (&InIpv6RtInfo.Ip6Dst, pNetworkAddr, LDP_IPV6ADR_LEN);
    MEMCPY (&InIpv6RtInfo.NextHop, pNextHopAddr, LDP_IPV6ADR_LEN);
    InIpv6RtInfo.u1Prefixlen = u1PrefixLen;
    InIpv6RtInfo.u4Index = u4IfIndex;
    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

    TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HSessIndex)
    {
        TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HSessIndex,
                              pSsnHashNode, tTMO_HASH_NODE *)
        {
            pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);

            SSN_GET_SSN_STATE (pLdpSession, u1SsnState);
            if (u1SsnState == LDP_SSM_ST_OPERATIONAL)
            {
                if (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
                    == LDP_FALSE)
                {
                    if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
                        (SSN_MRGTYPE (pLdpSession) == LDP_NO_MRG))
                    {
                        LdpNonMrgIntIpv6LspSetReq (u2IncarnId, pNetworkAddr,
                                                   u1PrefixLen, pNextHopAddr,
                                                   u4IfIndex, NULL);
                        return;
                    }
                    if ((SSN_ADVTYPE (pLdpSession) == LDP_DSTR_ON_DEMAND) &&
                        ((SSN_MRGTYPE (pLdpSession) == LDP_VC_MRG) ||
                         (SSN_MRGTYPE (pLdpSession) == LDP_VP_MRG)))
                    {
#if 0
                        LdpEstbIpv6LspsForDoDMrgSsns (&InIpv6RtInfo,
                                                      pLdpSession);
#endif
                    }
                }
                if (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT)
                {
                    LdpEstbIpv6LspsForDuSsns (pLdpSession, &InIpv6RtInfo);
                }
            }
        }
    }
    return;
}

#endif
/* MPLS_IPv6 add end*/

VOID
PrintInterfaceAddr (tLdpIpEvtInfo * pIpEvtInfo)
{
    UNUSED_PARAM (pIpEvtInfo);
#if 0
    printf ("IPV4 Addr= %x\n", pIpEvtInfo->u4IpAddr);
#ifdef MPLS_IPV6_WANTED
    printf ("IPV6 LinkLocalAddr= %s\n",
            Ip6PrintAddr (&pIpEvtInfo->LnklocalIpv6Addr));
    printf ("IPV6 SiteLocalAddr= %s\n",
            Ip6PrintAddr (&pIpEvtInfo->SitelocalIpv6Addr));
    printf ("IPV6 GlobalUniqueAddr= %s\n",
            Ip6PrintAddr (&pIpEvtInfo->GlbUniqueIpv6Addr));
#endif
    return;
#endif
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpmain.c                               */
/*---------------------------------------------------------------------------*/
