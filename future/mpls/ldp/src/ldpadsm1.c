/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpadsm1.c,v 1.60 2017/06/09 12:50:36 siva Exp $
 *
 * Description: This file contains the routines used to create, change,
 *              and delete label mappings for the FEC,
 *              also contains label state machine routines.
 ********************************************************************/

/* 
 * 
 * All relevant routines should be checking for u1LcbRole. Allocate and
 * distribute label only when u1LcbRole is Acitve.
 */
#include "ldpincs.h"
#include "ldpadsm1.h"
/*****************************************************************************/
/* Function Name : LdpInitLspCtrlBlock                                       */
/* Description   : This routine is called to initialise the contents of an   */
/*                 LSP Control block.                                        */
/* Input(s)      : pLspCtrlBlock - Pointer to the LSP Control block to be    */
/*                 initialised.                                              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpInitLspCtrlBlock (tLspCtrlBlock * pLspCtrlBlock)
{

    MEMSET (pLspCtrlBlock, 0, sizeof (tLspCtrlBlock));

    TMO_SLL_Init_Node (&LCB_NXT_ULCB (pLspCtrlBlock));
    TMO_SLL_Init_Node (&LCB_NXT_DLCB (pLspCtrlBlock));
    TMO_SLL_Init (&LCB_UPSTR_LIST (pLspCtrlBlock));
    LCB_UREQ_ID (pLspCtrlBlock) = LDP_INVALID_REQ_ID;
    LCB_DREQ_ID (pLspCtrlBlock) = LDP_INVALID_REQ_ID;
    LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_IDLE;
    LCB_USSN (pLspCtrlBlock) = NULL;
    LCB_DSSN (pLspCtrlBlock) = NULL;
    LCB_TRIG (pLspCtrlBlock) = NULL;
    LCB_NHOP (pLspCtrlBlock) = NULL;
    LCB_OUT_IFINDEX (pLspCtrlBlock) = LDP_ZERO;
    LCB_ULBL (pLspCtrlBlock).u4GenLbl = LDP_INVALID_LABEL;
    LCB_DLBL (pLspCtrlBlock).u4GenLbl = LDP_INVALID_LABEL;
    LCB_FEC (pLspCtrlBlock).u1FecElmntType = LDP_FEC_INVALID_TYPE;
    LCB_TNLINFO (pLspCtrlBlock) = NULL;
    LCB_MLIB_UPD_STATUS (pLspCtrlBlock) = 0;
    pLspCtrlBlock->u1LblReqFwdFlag = LDP_TRUE;
#ifdef LDP_GR_WANTED
    pLspCtrlBlock->u1StaleStatus = LDP_FALSE;
#endif
}

/*****************************************************************************/
/* Function Name : LdpCompLabels                                             */
/* Description   : This routine is called to compare a label value stored    */
/*                 in an LSP control block, with a label value received in   */
/*                 a LDP message.                                            */
/* Input(s)      : Label(store in datastructre type)                         */
/*                 u4RcvLbl - Label value as received in the message.        */
/*                 u1LblType - indicates the type of label, Gen, Atm or FR   */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_EQUAL/LDP_NOT_EQUAL                                   */
/*****************************************************************************/
UINT1
LdpCompLabels (uLabel Label, UINT4 u4RcvLbl, UINT1 u1LblType)
{

    UINT2               u2Val1, u2Val2;

    if (u1LblType == LDP_GEN_LABEL)
    {
        if (Label.u4GenLbl == u4RcvLbl)
        {
            return (LDP_EQUAL);
        }
        else
        {
            return (LDP_NOT_EQUAL);
        }

    }
    else if (u1LblType == LDP_ATM_LABEL)
    {
        u2Val1 = (UINT2) ((u4RcvLbl & MPLS_VPI_MASK) >> 16);
        u2Val2 = (UINT2) (u4RcvLbl & MPLS_VCI_MASK);
        if ((Label.AtmLbl.u2Vpi == u2Val1) && (Label.AtmLbl.u2Vci == u2Val2))
        {
            return (LDP_EQUAL);
        }
        else
        {

            return (LDP_NOT_EQUAL);
        }
    }
    return (LDP_NOT_EQUAL);
}

/*****************************************************************************/
/* Function Name : LdpHandleLblWdrwMsg                                       */
/* Description   : This routine is called to handle a label withdraw message.*/
/*                 This in turn calls label state machine with appropriate   */
/*                 event                                                     */
/* Input(s)      : pSessionEntry, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpHandleLblWdrwMsg (tLdpSession * pSessionEntry, UINT1 *pMsg)
{

    UINT1               u1Event;
    UINT2               u2MsgLen;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT4               u4LabelOrReqId;
    tFec                Fec;

    UINT1              *pu1MsgPtr = pMsg;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tTMO_SLL           *pLspCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    UINT1               u1LabelOrLspID = LDP_TRUE;
    UINT1               u1LabelPresent = LDP_FALSE;
    tLdpMsgInfo         MsgInfo;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;
    UINT1               u1StatusType = 0;
    UINT1               u1FecFound = LDP_FALSE;

    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);
    MEMSET (&MsgInfo, 0, sizeof (tLdpMsgInfo));
    MEMSET (&Fec, 0, sizeof (tFec));
    MsgInfo.pu1Msg = pMsg;

    /* Message type and event type initialised */
    u1Event = LDP_LSM_EVT_LBL_WTH;

    /* With draw message received from downstrem LSR/LER hence ptr points to
     * List of LSP Ctrl blocks for which the session is a downstream session */
    pLspCtrlBlkList = &SSN_DLCB (pSessionEntry);

    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Now the pointer points to the first TLV */
    pu1MsgPtr = pu1MsgPtr + LDP_MSGID_LEN;

    /* FEC TLV  - The mandatory TLV is accessed */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);

    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
    if ((u2TlvType != LDP_FEC_TLV) || (u2MsgLen == LDP_TLV_HDR_LEN))
    {
        /* Error - mandatory TLV not present */
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: Mand Param FEC TLV is missing in LblWdrwMsg."
                 " Sending NotifMsg.\n");
        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        return LDP_FAILURE;
    }

    /* FEC TLV Contents copied into local FEC Structure */

    if (LdpStrToFec (pu1MsgPtr, u2TlvLen, &Fec, &u1StatusType) == LDP_FAILURE)
    {
        if (u1StatusType == LDP_STAT_TYPE_MALFORMED_TLV_VAL)
        {
            LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                     (UINT1 *) pMsg, LDP_TRUE);
        }
        /* The FEC TLV has unknown FEC type or unsupported address family.
           In that case just send the notification message */
        else if ((u1StatusType == LDP_STAT_TYPE_UNKNOWN_FEC) ||
                 (u1StatusType == LDP_STAT_TYPE_UNSUPP_ADDR_FMLY))
        {
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             u1StatusType, NULL);
        }

        return LDP_FAILURE;
    }
    pu1MsgPtr += u2TlvLen;
    if ((Fec.u1FecElmntType == LDP_FEC_PWVC_TYPE) ||
        (Fec.u1FecElmntType == LDP_FEC_GEN_PWVC_TYPE))
    {
        if (LdpRegStatus (Fec.u1FecElmntType) == LDP_REGISTER)
        {
            if (LdpHandleFecMsg (pSessionEntry, pMsg, LDP_LBL_WITHDRAW_MSG,
                                 Fec.u1FecElmntType) != LDP_SUCCESS)
            {
                return LDP_FAILURE;
            }
            return LDP_SUCCESS;
        }
        else
        {
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_UNKNOWN_FEC, NULL);
            return LDP_FAILURE;
        }
    }

#ifdef MPLS_IPV6_WANTED
    if (Fec.u2AddrFmly == IPV6)
    {
        if (u2TlvLen > LDP_IPV6_FEC_ELMNT_LEN)
        {
            /* This implies, the received FEC TLv contains more than one 
             * Fec Element. As of now only a single Fec is supported to 
             * be received in a message*/
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: FecTlv contains more than one FecElmnt\n");
            return LDP_FAILURE;
        }
        LDP_DBG2 (LDP_ADVT_RX,
                  "Rcvd Label withdrw Msg for the prefix %s/%d \n",
                  Ip6PrintAddr (&Fec.Prefix.Ip6Addr), Fec.u1PreLen);
    }
    else
#endif
    {
        if (u2TlvLen > LDP_FEC_ELMNT_LEN)
        {
            /* This implies, the received FEC TLv contains more than one 
             * Fec Element. As of now only a single Fec is supported to 
             * be received in a message*/
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: FecTlv contains more than one FecElmnt\n");
            return LDP_FAILURE;
        }

        u4Temp = OSIX_HTONL (LDP_IPV4_U4_ADDR (Fec.Prefix));

        MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);

        LDP_DBG5 (LDP_ADVT_RX,
                  "Rcvd Label withdrw Msg for the prefix %d.%d.%d.%d/%d \n",
                  au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3],
                  Fec.u1PreLen);

    }

    LDP_DBG4 (LDP_ADVT_RX,
              "Rcvd Label withdrw Msg from %d.%d.%d.%d\n",
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);

    /* Case when FEC is a Wild card TLV */
    if (Fec.u1FecElmntType == LDP_FEC_WILDCARD_TYPE)
    {
        /* Check for Label TLVS if present */
        if (pu1MsgPtr == pMsg + u2MsgLen + LDP_MSG_HDR_LEN)
        {
            /* no labels - release all the FECs and their labels */
            TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
            {
                pLspCtrlBlock = SLL_TO_LCB (pSllNode);
                LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock, u1Event,
                                &MsgInfo, LDP_FALSE);
            }
        }
        else
        {
            /* labels present - release only the labels */
            while (pu1MsgPtr < pMsg + u2MsgLen + LDP_MSG_HDR_LEN)
            {
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                LDP_EXT_4_BYTES (pu1MsgPtr, u4LabelOrReqId);

                TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
                {
                    pLspCtrlBlock = SLL_TO_LCB (pSllNode);
                    if (LdpCompLabels (LCB_DLBL (pLspCtrlBlock), u4LabelOrReqId,
                                       SSN_GET_LBL_TYPE (pSessionEntry))
                        == LDP_EQUAL)
                    {
                        LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock, u1Event,
                                        &MsgInfo, LDP_FALSE);
                        break;
                    }
                }                /* end of TMO_SLL_Scan */
            }                    /* end of while */
            return LDP_SUCCESS;
        }                        /* End of labels present "else" condition */
    }
    else if (Fec.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
    {
        if (pu1MsgPtr == pMsg + u2MsgLen + LDP_OFF_MSG_ID)
        {
            u1LabelOrLspID = LDP_FALSE;
        }

        /* Either LspId or Label Tlv is present */
        if (u1LabelOrLspID == LDP_TRUE)
        {
            while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
            {
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

                switch (u2TlvType)
                {
                    case LDP_GEN_LABEL_TLV:
                    case LDP_ATM_LABEL_TLV:

                        LDP_EXT_4_BYTES (pu1MsgPtr, u4LabelOrReqId);
                        TMO_SLL_Scan (pLspCtrlBlkList, pSllNode,
                                      tTMO_SLL_NODE *)
                        {
                            pLspCtrlBlock = SLL_TO_LCB (pSllNode);
                            if (LdpCompLabels (LCB_DLBL (pLspCtrlBlock),
                                               u4LabelOrReqId,
                                               SSN_GET_LBL_TYPE (pSessionEntry))
                                == LDP_EQUAL)
                            {
                                LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                                u1Event, &MsgInfo, LDP_FALSE);
                                u1LabelPresent = LDP_TRUE;
                                break;
                            }
                        }        /* end of TMO_SLL_Scan */
                        break;
                    case CRLDP_LSPID_TLV:
                        pu1MsgPtr += u2TlvLen;
                        break;
                    default:
                        return LDP_FAILURE;
                        break;
                }
            }
            if (u1LabelPresent == LDP_TRUE)
            {
                return LDP_SUCCESS;
            }
        }

        /* No Label Tlv's are present. 
         * Even though the LSP ID TLV is present, the identification of
         * the control block from the LSPID TLV if the tunnel has a
         * modification can't be achieved. 
         */

        if ((u1LabelOrLspID == LDP_FALSE) || (u1LabelPresent == LDP_FALSE))
        {
            /* 
             * No labels - release all the LSP whose FEC are CRLSP in nature
             * and their labels.
             */
            TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
            {
                pLspCtrlBlock = SLL_TO_LCB (pSllNode);
                if ((LCB_FEC (pLspCtrlBlock)).u1FecElmntType ==
                    LDP_FEC_CRLSP_TYPE)
                {
                    LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                    u1Event, &MsgInfo, LDP_FALSE);
                }
            }
        }
    }
    else
    {
        /* 
         * Case when FEC type is of IP Prefix type or host address type.
         * is not a Wild card TLV.
         */

        /* Check for Label TLVS if present */
        if (pu1MsgPtr == pMsg + u2MsgLen + LDP_MSG_HDR_LEN)
        {
            /* no labels - release all the FECs and their labels */
            TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
            {
                pLspCtrlBlock = SLL_TO_LCB (pSllNode);
                if (LdpCompareFec (&LCB_FEC (pLspCtrlBlock), &Fec) == LDP_EQUAL)
                {
                    LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                    u1Event, &MsgInfo, LDP_FALSE);
                    u1FecFound = LDP_TRUE;
                }
            }
            if (u1FecFound == LDP_FALSE)
            {
                LDP_DBG (LDP_ADVT_RX,
                         "No Corresponding Lbl Mapping Msg Found for the "
                         "Rcvd Lbl Withdraw Msg\n");
                LdpSendLblRelMsg (&Fec, NULL, pSessionEntry);

            }
            return LDP_SUCCESS;
        }
        else
        {
            /* labels present - release only the labels */
            while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
            {
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                LDP_EXT_4_BYTES (pu1MsgPtr, u4LabelOrReqId);

                TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
                {
                    pLspCtrlBlock = SLL_TO_LCB (pSllNode);
                    /* Label control block can be uniquely identified only 
                     * by FEC + Label combination if we have received 
                     * reserved labels like explicit-null or implicit-null 
                     * for more than one FEC. */
                    if ((LdpCompLabels
                         (LCB_DLBL (pLspCtrlBlock), u4LabelOrReqId,
                          SSN_GET_LBL_TYPE (pSessionEntry)) == LDP_EQUAL)
                        && (LdpCompareFec (&LCB_FEC (pLspCtrlBlock), &Fec) ==
                            LDP_EQUAL))
                    {
                        LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                        u1Event, &MsgInfo, LDP_FALSE);
                        u1FecFound = LDP_TRUE;
                        if (SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_UNSOLICIT)
                        {
                            break;
                        }
                    }
                }                /* end of TMO_SLL_Scan */
                if (u1FecFound == LDP_FALSE)
                {
                    LDP_DBG (LDP_ADVT_RX,
                             "No Corresponding Lbl Mapping Msg Found for the "
                             "Rcvd Lbl Withdraw Msg\n");
                    LdpSendLblRelMsg (&Fec, NULL, pSessionEntry);
                }
                return LDP_SUCCESS;
            }
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleLblRelMsg                                        */
/* Description   : This routine is called to handle a Label release Message. */
/* Input(s)      : pSessionEntry, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpHandleLblRelMsg (tLdpSession * pSessionEntry, UINT1 *pMsg)
{
    UINT1               u1Event;
    UINT2               u2MsgLen;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT4               u4LabelOrReqId;
    tFec                Fec;

    UINT1              *pu1MsgPtr = pMsg;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tLspCtrlBlock      *pTempLspCtrlBlock = NULL;
    tTMO_SLL           *pLspCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    UINT1               u1LabelOrLspID = LDP_TRUE;
    UINT1               u1LabelPresent = LDP_FALSE;
    tUstrLspCtrlBlock  *pUsLspCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pTempUsLspCtrlBlock = NULL;

    tLdpMsgInfo         MsgInfo;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;
    UINT1               u1StatusType = 0;

    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);
    MEMSET (&MsgInfo, 0, sizeof (tLdpMsgInfo));
    MEMSET (&Fec, 0, sizeof (Fec));
    MsgInfo.pu1Msg = pMsg;

    /* Message type and event type initialised */
    u1Event = LDP_LSM_EVT_LBL_REL;

    /* Label Release message received from upstream LSR/LER hence ptr points to
     * List of LSP Ctrl blocks for which the session is a upstream session */
    pLspCtrlBlkList = &SSN_ULCB (pSessionEntry);

    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);
    /* After the above extraction now pu1MsgPtr points to pMsg + 4 */

    /* Now the pointer points to the first TLV */

    pu1MsgPtr = pu1MsgPtr + LDP_MSGID_LEN;

    /* FEC TLV  - The mandatory TLV is accessed */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);

    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

    /* check for mandatory TLV performed */
    if (u2TlvType != LDP_FEC_TLV)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: Mand Param FEC TLV is missing in LblRelMsg."
                 " Sending NotifMsg.\n");
        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        return LDP_FAILURE;
    }

    if (LdpStrToFec (pu1MsgPtr, u2TlvLen, &Fec, &u1StatusType) == LDP_FAILURE)
    {
        if (u1StatusType == LDP_STAT_TYPE_MALFORMED_TLV_VAL)
        {
            LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                     (UINT1 *) pMsg, LDP_TRUE);
        }
        /* The FEC TLV has unknown FEC type or unsupported address family.
           In that case just send the notification message */
        else if ((u1StatusType == LDP_STAT_TYPE_UNKNOWN_FEC) ||
                 (u1StatusType == LDP_STAT_TYPE_UNSUPP_ADDR_FMLY))
        {
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             u1StatusType, NULL);
        }
        return LDP_FAILURE;
    }

#ifdef MPLS_IPV6_WANTED
    if (Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        LDP_DBG2 (LDP_ADVT_RX,
                  "Rcvd Label Release Msg for the prefix %s/%d \n",
                  Ip6PrintAddr (&Fec.Prefix.Ip6Addr), Fec.u1PreLen);
    }
    else
#endif
    {
        u4Temp = OSIX_HTONL (LDP_IPV4_U4_ADDR (Fec.Prefix));

        MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);

        LDP_DBG5 (LDP_ADVT_RX,
                  "Rcvd Label Release Msg for the prefix %d.%d.%d.%d/%d \n",
                  au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3],
                  Fec.u1PreLen);
    }

    LDP_DBG4 (LDP_ADVT_RX,
              "Rcvd Label Release Msg from %d.%d.%d.%d\n",
              pSessionEntry->pLdpPeer->PeerLdpId[0],
              pSessionEntry->pLdpPeer->PeerLdpId[1],
              pSessionEntry->pLdpPeer->PeerLdpId[2],
              pSessionEntry->pLdpPeer->PeerLdpId[3]);

    pu1MsgPtr += u2TlvLen;
    if ((Fec.u1FecElmntType == LDP_FEC_PWVC_TYPE) ||
        (Fec.u1FecElmntType == LDP_FEC_GEN_PWVC_TYPE))
    {
        if (LdpRegStatus (Fec.u1FecElmntType) == LDP_REGISTER)
        {
            if (LdpHandleFecMsg (pSessionEntry, pMsg, LDP_LBL_RELEASE_MSG,
                                 Fec.u1FecElmntType) != LDP_SUCCESS)
            {
                return LDP_FAILURE;
            }
            return LDP_SUCCESS;
        }
        else
        {
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_UNKNOWN_FEC, NULL);
            return LDP_FAILURE;
        }
    }

#ifdef MPLS_IPV6_WANTED
    if (Fec.u2AddrFmly == IPV6)
    {
        if (u2TlvLen > LDP_IPV6_FEC_ELMNT_LEN)
        {
            /* This implies, the received FEC TLv contains more than one
             *          * Fec Element. As of now only a single Fec is supported to
             *                   * be received in a message*/
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: FecTlv contains more than one FecElmnt\n");
            return LDP_FAILURE;
        }
    }
    else
#endif
    {
        if (u2TlvLen > LDP_FEC_ELMNT_LEN)
        {
            /* This implies, the received FEC TLv contains more than one
             *          * Fec Element. As of now only a single Fec is supported to
             *                   * be received in a message*/
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: FecTlv contains more than one FecElmnt\n");
            return LDP_FAILURE;
        }
    }

    /* Case when FEC is a Wild card TLV */
    if (Fec.u1FecElmntType == LDP_FEC_WILDCARD_TYPE)
    {
        /* Check for Label TLVS if present */
        if (pu1MsgPtr == pMsg + u2MsgLen + LDP_MSG_HDR_LEN)
        {
            /* 
             * Wild card FEC hence, The list of blocks is traversed and for
             * each block is initiated with the label release event.
             */
            if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
                (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
            {
                TMO_DYN_SLL_Scan (pLspCtrlBlkList, pLspCtrlBlock,
                                  pTempLspCtrlBlock, tLspCtrlBlock *)
                {
                    LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock, u1Event,
                                    &MsgInfo, LDP_FALSE);
                }
            }
            else
            {
                TMO_DYN_SLL_Scan (pLspCtrlBlkList, pUsLspCtrlBlock,
                                  pTempUsLspCtrlBlock, tUstrLspCtrlBlock *)
                {
                    LdpInvCorrStMh (pSessionEntry, pUsLspCtrlBlock, u1Event,
                                    &MsgInfo, LDP_TRUE);
                }
            }
            return LDP_SUCCESS;
        }
        else
        {
            /* labels present - release only the labels */
            while (pu1MsgPtr < pMsg + u2MsgLen + LDP_MSG_HDR_LEN)
            {
                /* TLV Type, TLV Length and label values are obtained */
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                LDP_EXT_4_BYTES (pu1MsgPtr, u4LabelOrReqId);
                /* 
                 * The list of LSP control blocks are checked, and the 
                 * LSP control block with matching Label is initiated with 
                 * label release event 
                 */
                TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
                {
                    if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
                        (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
                    {
                        pLspCtrlBlock = (tLspCtrlBlock *) pSllNode;
                        if (LdpCompLabels (LCB_ULBL (pLspCtrlBlock),
                                           u4LabelOrReqId, SSN_GET_LBL_TYPE
                                           (pSessionEntry)) == LDP_EQUAL)
                        {
                            LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                            u1Event, &MsgInfo, LDP_FALSE);
                        }
                    }
                    else
                    {
                        pUsLspCtrlBlock = (tUstrLspCtrlBlock *) pSllNode;
                        if (LdpCompLabels (UPSTR_ULBL (pUsLspCtrlBlock),
                                           u4LabelOrReqId, SSN_GET_LBL_TYPE
                                           (pSessionEntry)) == LDP_EQUAL)
                        {
                            LdpInvCorrStMh (pSessionEntry, pUsLspCtrlBlock,
                                            u1Event, &MsgInfo, LDP_TRUE);
                            /* 
                             * Required LSP Ctrl Block with label obtained hence 
                             * SLL Scan break.
                             */
                            break;
                        }
                    }
                }                /* end of TMO_SLL_Scan */
            }                    /* end of while */
            return LDP_SUCCESS;
        }                        /* End of labels present "else" condition */
    }
    else if (Fec.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
    {
        /* If Fec is LDP_FEC_CRLSP_TYPE apart from label Tlv we can
         * also get an LspId Tlv 
         */
        if (pu1MsgPtr == pMsg + u2MsgLen + LDP_OFF_MSG_ID)
        {
            u1LabelOrLspID = LDP_FALSE;
        }

        if (u1LabelOrLspID == LDP_TRUE)
        {
            while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
            {
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

                switch (u2TlvType)
                {

                    case LDP_GEN_LABEL_TLV:
                    case LDP_ATM_LABEL_TLV:

                        LDP_EXT_4_BYTES (pu1MsgPtr, u4LabelOrReqId);
                        TMO_SLL_Scan (pLspCtrlBlkList, pSllNode,
                                      tTMO_SLL_NODE *)
                        {
                            pLspCtrlBlock = (tLspCtrlBlock *) pSllNode;
                            if (LdpCompLabels (LCB_ULBL (pLspCtrlBlock),
                                               u4LabelOrReqId,
                                               SSN_GET_LBL_TYPE (pSessionEntry))
                                == LDP_EQUAL)
                            {
                                LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                                u1Event, &MsgInfo, LDP_FALSE);
                                u1LabelPresent = LDP_TRUE;
                                break;
                            }
                        }        /* end of TMO_SLL_Scan */
                        break;

                    case CRLDP_LSPID_TLV:
                        pu1MsgPtr += u2TlvLen;
                        break;
                    default:
                        break;
                }
            }
            if (u1LabelPresent == LDP_TRUE)
            {
                return LDP_SUCCESS;
            }
        }

        /* No Label Tlv's are present. 
         * Even though the LSP ID TLV is present, the identification of
         * the control block from the LSPID TLV if the tunnel has a
         * modification can't be achieved. 
         */

        if ((u1LabelOrLspID == LDP_FALSE) || (u1LabelPresent == LDP_FALSE))
        {
            /* 
             * No labels - release all the LSP whose FEC are CRLSP in nature
             * and their labels.
             */
            TMO_DYN_SLL_Scan (pLspCtrlBlkList, pLspCtrlBlock,
                              pTempLspCtrlBlock, tLspCtrlBlock *)
            {
                if ((LCB_FEC (pLspCtrlBlock)).u1FecElmntType
                    == LDP_FEC_CRLSP_TYPE)
                {
                    LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock, u1Event,
                                    &MsgInfo, LDP_FALSE);
                }
            }
        }
    }
    else
    {
        /* Check for Label TLVS if present */
        if (pu1MsgPtr == pMsg + u2MsgLen + LDP_MSG_HDR_LEN)
        {
            /* 
             * No labels - release all the FECs and their labels.
             * The list of LSP control blocks are checked, and the 
             * LSP control block with matching FEC is initiated with 
             * label release event 
             */

            if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
                (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
            {
                TMO_DYN_SLL_Scan (pLspCtrlBlkList, pLspCtrlBlock,
                                  pTempLspCtrlBlock, tLspCtrlBlock *)
                {
                    if (LdpCompareFec (&LCB_FEC (pLspCtrlBlock), &Fec) ==
                        LDP_EQUAL)
                    {
                        LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                        u1Event, &MsgInfo, LDP_FALSE);
                    }
                }
            }
            else
            {
                TMO_DYN_SLL_Scan (pLspCtrlBlkList, pUsLspCtrlBlock,
                                  pTempUsLspCtrlBlock, tUstrLspCtrlBlock *)
                {
                    if (LdpCompareFec (&UPSTR_FEC (pUsLspCtrlBlock), &Fec) ==
                        LDP_EQUAL)
                    {
                        LdpInvCorrStMh (pSessionEntry, pUsLspCtrlBlock,
                                        u1Event, &MsgInfo, LDP_TRUE);
                    }
                }
            }

            return LDP_SUCCESS;
        }
        else
        {
            /* labels present - release only the labels */
            while (pu1MsgPtr < pMsg + u2MsgLen + LDP_MSG_HDR_LEN)
            {
                /* TLV Type, TLV Length and label values are obtained */
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                LDP_EXT_4_BYTES (pu1MsgPtr, u4LabelOrReqId);
                /* 
                 * The list of LSP control blocks are checked, and the 
                 * LSP control block with matching Label is initiated with 
                 * label release event 
                 */
                TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
                {
                    if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
                        (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
                    {
                        pLspCtrlBlock = (tLspCtrlBlock *) pSllNode;

                        /* Label control block can be uniquely identified only 
                         * by FEC + Label combination if we have distributed 
                         * reserved labels like explicit-null or implicit-null 
                         * for more than one FEC. */

                        if ((LdpCompLabels (LCB_ULBL (pLspCtrlBlock),
                                            u4LabelOrReqId, SSN_GET_LBL_TYPE
                                            (pSessionEntry)) == LDP_EQUAL) &&
                            (LdpCompareFec (&LCB_FEC (pLspCtrlBlock), &Fec) ==
                             LDP_EQUAL))
                        {
                            LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                                            u1Event, &MsgInfo, LDP_FALSE);
                            /* 
                             * Required LSP Ctrl Block with label obtained hence 
                             * SLL Scan break.
                             */
                            break;
                        }
                    }
                    else
                    {
                        pUsLspCtrlBlock = (tUstrLspCtrlBlock *) pSllNode;

                        /* Label control block can be uniquely identified only 
                         * by FEC + Label combination if we have distributed 
                         * reserved labels like explicit-null or implicit-null 
                         * for more than one FEC. */

                        if ((LdpCompLabels (UPSTR_ULBL (pUsLspCtrlBlock),
                                            u4LabelOrReqId, SSN_GET_LBL_TYPE
                                            (pSessionEntry)) == LDP_EQUAL) &&
                            (LdpCompareFec (&LCB_FEC (pUsLspCtrlBlock), &Fec) ==
                             LDP_EQUAL))
                        {
                            LdpInvCorrStMh (pSessionEntry, pUsLspCtrlBlock,
                                            u1Event, &MsgInfo, LDP_TRUE);
                            /* 
                             * Required LSP Ctrl Block with label obtained hence 
                             * SLL Scan break.
                             */
                            break;
                        }
                    }
                }                /* end of TMO_SLL_Scan */
            }
            return LDP_SUCCESS;
        }
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleLblAbrtMsg                                       */
/* Description   : This routine is called to handle Label Abort Message.     */
/* Input(s)      : pSessionEntry, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpHandleLblAbrtMsg (tLdpSession * pSessionEntry, UINT1 *pMsg)
{
    UINT1               u1Event;
    UINT2               u2MsgType;
    UINT2               u2MsgLen;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT4               u4LabelOrReqId;
    UINT1              *pu1MsgPtr = pMsg;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pUsLspCtrlBlock = NULL;
    UINT1              *pu1FecMsgPtr = NULL;
    tLdpMsgInfo         MsgInfo;
    tFec                Fec;
    UINT1               u1StatusType = 0;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;

    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);

    MEMSET (&MsgInfo, 0, sizeof (tLdpMsgInfo));
    MEMSET (&Fec, 0, sizeof (tFec));
    MsgInfo.pu1Msg = pMsg;

    LDP_DBG4 (LDP_ADVT_RX,
              "Rcvd Label Abort Msg from %d.%d.%d.%d\n",
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
              pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);

    u1Event = LDP_LSM_EVT_LBL_ABRT;
    u2MsgType = LDP_LBL_ABORT_REQ_MSG;

    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    pu1MsgPtr += LDP_OFF_MSG_ID;

    /* Check whether the FEC TLV is present */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

    if (u2TlvType != LDP_FEC_TLV)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: Mand Param FEC TLV is missing in LblAbrt Msg."
                 " Sending NotifMsg.\n");
        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        return LDP_FAILURE;
    }

    pu1FecMsgPtr = pu1MsgPtr;
    /* 
     * Mandatory FEC TLV present, the Label message request id TLV prsence is
     * checked.
     */
    pu1MsgPtr += u2TlvLen;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

    if (u2TlvType != LDP_LBL_REQ_MSGID_TLV)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: Mand Param LblReqMsgID TLV is missing in LblAbrtMsg."
                 " Sending NotifMsg.\n");
        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        return LDP_FAILURE;
    }

    /* LblReqMsgId is present and extracted */
    LDP_EXT_4_BYTES (pu1MsgPtr, u4LabelOrReqId);

    if (pu1MsgPtr < (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* If it's an Abort Msg for a CRLSP requset it may carry an
         * LSPID TLV 
         */
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
        if (u2TlvType == CRLDP_LSPID_TLV)
        {
            pu1MsgPtr += u2TlvLen;
        }
    }

    if (pu1MsgPtr > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        LDP_DBG (LDP_ADVT_PRCS, "ADVT: Msg Len Error in ABORT Msg\n");
        return LDP_FAILURE;
    }

    if (LdpStrToFec (pu1FecMsgPtr, u2TlvLen, &Fec, &u1StatusType) ==
        LDP_FAILURE)
    {
        if (u1StatusType == LDP_STAT_TYPE_MALFORMED_TLV_VAL)
        {
            LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                     (UINT1 *) pMsg, LDP_TRUE);
        }
        /* The FEC TLV has unknown FEC type or unsupported address family.
           In that case just send the notification message */
        else if ((u1StatusType == LDP_STAT_TYPE_UNKNOWN_FEC) ||
                 (u1StatusType == LDP_STAT_TYPE_UNSUPP_ADDR_FMLY))
        {
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             u1StatusType, NULL);
        }

        return LDP_FAILURE;
    }

#if MPLS_IPV6_WANTED

    if (Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {

        LDP_DBG2 (LDP_ADVT_RX,
                  "Rcvd Label abort for the prefix %s/%d \n",
                  Ip6PrintAddr (&Fec.Prefix.Ip6Addr), Fec.u1PreLen);
    }
    else
#endif
    {
        u4Temp = OSIX_HTONL (LDP_IPV4_U4_ADDR (Fec.Prefix));

        MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);

        LDP_DBG5 (LDP_ADVT_RX,
                  "Rcvd Label abort for the prefix %d.%d.%d.%d/%d \n",
                  au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3],
                  Fec.u1PreLen);
    }

    if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
        (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
    {
        /* The LSP control block associated with the label request 
         * message accessed */

        pLspCtrlBlock =
            LdpGetLspCtrlBlock (pSessionEntry, u2MsgType, u4LabelOrReqId, Fec);
        if (pLspCtrlBlock == NULL)
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Failed to identify LCB for AbortMsg\n");
            return LDP_FAILURE;
        }
        /* FSM routine is invoked */
        LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock, u1Event,
                        &MsgInfo, LDP_FALSE);
    }
    else
    {
        pUsLspCtrlBlock =
            LdpGetUstrCtrlBlock (pSessionEntry, u2MsgType, u4LabelOrReqId, Fec);
        if (pUsLspCtrlBlock == NULL)
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Failed to identify LCB for AbortMsg\n");
            return LDP_FAILURE;
        }
        /* RFC: 5036. Appendix: A.1.3
         * When an LSR receives a Label Abort Request message from a peer, it
         * checks whether it has already responded to the label request in
         * question.  If it has, it silently ignores the message
         */
        if ((pUsLspCtrlBlock->u1LspState == LDP_LSM_ST_EST) &&
            (gi4MplsSimulateFailure != LDP_SIM_FAILURE_PROPAGATE_LBL_RELEASE))
        {
            return LDP_SUCCESS;
        }
        /* FSM routine is invoked */
        LdpInvCorrStMh (pSessionEntry, pUsLspCtrlBlock, u1Event,
                        &MsgInfo, LDP_TRUE);
    }
    LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                     LDP_STAT_TYPE_LBL_REQ_ABRTD, NULL);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleLblMapMsg                                        */
/* Description   : This routine is called to handle Label Mapping Message.   */
/* Input(s)      : pSessionEntry, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpHandleLblMapMsg (tLdpSession * pSessionEntry, UINT1 *pMsg)
{
    UINT1               u1Event;
    UINT2               u2MsgType;
    UINT2               u2MsgLen;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT4               u4LabelOrReqId = 0;
    UINT1              *pu1MsgPtr = pMsg;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1FecMsgPtr = NULL;
    tLdpMsgInfo         MsgInfo;
    uLabel              Label;
    UINT4               u4LabelVal = 0;
    UINT2               u2FecTlvLen = 0;
    tFec                Fec;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;
    UINT1               u1StatusType = 0;

    LDP_DBG1 (LDP_ADVT_PRCS, "%s ENTRY\n", __func__);

    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);
    MEMSET (&MsgInfo, 0, sizeof (tLdpMsgInfo));
    MEMSET (&Fec, 0, sizeof (tFec));

    MsgInfo.pu1Msg = pMsg;
    MsgInfo.u1IsLblChanged = LDP_FALSE;

    /* FSM Event  and Message types initialised */
    u1Event = LDP_LSM_EVT_LBL_MAP;
    u2MsgType = LDP_LBL_MAPPING_MSG;

    /* Message length is obtained */
    pu1MsgPtr = pu1MsgPtr + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);
    /* After the above extraction now pu1MsgPtr points to pMsg + 4 */

    pu1MsgPtr = pu1MsgPtr + LDP_MSGID_LEN;
    /* check for the presence of FEC TLV is being done */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
    u2FecTlvLen = u2TlvLen;

    if (u2TlvType != LDP_FEC_TLV)
    {
        /* Error - mandatory TLV not present */
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: Mand Param FEC TLV is missing in LblMapMsg."
                 " Sending NotifMsg.\n");
        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        return LDP_FAILURE;
    }
    /* FecMsgPtr points to the contents of Fec */
    pu1FecMsgPtr = pu1MsgPtr;

    pu1MsgPtr += u2TlvLen;

    /* FEC TLV present check for the presence of Label TLV is done */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

    if ((u2TlvType != LDP_GEN_LABEL_TLV) && (u2TlvType != LDP_ATM_LABEL_TLV))
    {
        /* Error - mandatory TLV not present */
        /* Type check for Frame relay TLV to be added once the Frame releay
           label is supported. */

        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: Mand Param Label TLV is missing in LblMapMsg."
                 " Sending NotifMsg.\n");
        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        return LDP_FAILURE;
    }
    pu1MsgPtr += u2TlvLen;

    /*  
     *  If it's initiated in the Label Request, MessageId TLV must be 
     *  present in the mapping message.
     */

    if (SSN_ADVTYPE (pSessionEntry) != LDP_DSTR_UNSOLICIT)
    {
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        if (u2TlvType != LDP_LBL_REQ_MSGID_TLV)
        {
            /* Error - mandatory TLV not present */
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Mand Param ReqId TLV is missing in LblMapMsg."
                     " Sending NotifMsg.\n");
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
            return LDP_FAILURE;
        }
        /* 
         * The request id sent in the label request message is taken from the 
         * label mapping message.
         */
        LDP_GET_4_BYTES (pu1MsgPtr, u4LabelOrReqId);
    }
    else
    {
        if (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
        {
            LDP_GET_2_BYTES (pu1MsgPtr, u2TlvType);
            if (u2TlvType == LDP_LBL_REQ_MSGID_TLV)
            {
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
                LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
                LDP_GET_4_BYTES (pu1MsgPtr, u4LabelOrReqId);
            }
        }
    }

    if (LdpStrToFec (pu1FecMsgPtr, u2FecTlvLen, &Fec, &u1StatusType) ==
        LDP_FAILURE)
    {
        if (u1StatusType == LDP_STAT_TYPE_MALFORMED_TLV_VAL)
        {
            LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                     (UINT1 *) pMsg, LDP_TRUE);
        }
        /* The FEC TLV has unknown FEC type or unsupported address family.
           In that case just send the notification message */
        else if ((u1StatusType == LDP_STAT_TYPE_UNKNOWN_FEC) ||
                 (u1StatusType == LDP_STAT_TYPE_UNSUPP_ADDR_FMLY))
        {
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             u1StatusType, NULL);
        }

        return LDP_FAILURE;
    }

    if ((Fec.u1FecElmntType == LDP_FEC_PWVC_TYPE) ||
        (Fec.u1FecElmntType == LDP_FEC_GEN_PWVC_TYPE))
    {
        if (LdpRegStatus (Fec.u1FecElmntType) == LDP_REGISTER)
        {
            if (LdpHandleFecMsg (pSessionEntry, pMsg, LDP_LBL_MAPPING_MSG,
                                 Fec.u1FecElmntType) != LDP_SUCCESS)
            {
                return LDP_FAILURE;
            }
            return LDP_SUCCESS;
        }
        else
        {
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_UNKNOWN_FEC, NULL);
            return LDP_FAILURE;
        }
    }

    pLspCtrlBlock =
        LdpGetLspCtrlBlock (pSessionEntry, u2MsgType, u4LabelOrReqId, Fec);
    if (pLspCtrlBlock == NULL)
    {
        LDP_DBG (LDP_ADVT_PRCS, "ADVT: Failed to identify LCB for ReqMsg\n");
        LdpSendLblRelMsg (&Fec, NULL, pSessionEntry);
        return LDP_FAILURE;
    }

#if MPLS_IPV6_WANTED
    if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        LDP_DBG2 (LDP_ADVT_RX,
                  "Rcvd Label Mapping Msg for the prefix %s/%d \n",
                  Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.Ip6Addr),
                  pLspCtrlBlock->Fec.u1PreLen);
    }
    else
#endif
    {
        u4Temp = OSIX_HTONL (LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));

        MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);

        LDP_DBG5 (LDP_ADVT_RX,
                  "Rcvd Label Mapping Msg for the prefix %d.%d.%d.%d/%d \n",
                  au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3],
                  pLspCtrlBlock->Fec.u1PreLen);
    }

    LDP_DBG4 (LDP_ADVT_RX,
              "Rcvd Label Mapping Msg from %d.%d.%d.%d\n",
              pSessionEntry->pLdpPeer->PeerLdpId[0],
              pSessionEntry->pLdpPeer->PeerLdpId[1],
              pSessionEntry->pLdpPeer->PeerLdpId[2],
              pSessionEntry->pLdpPeer->PeerLdpId[3]);

    if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
    {

        if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
            (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
        {
            /* The routine to handle the CRLSP request is invoked */
            LdpNonMrgCrlspRspMap (pLspCtrlBlock, pMsg);
            return LDP_SUCCESS;
        }
        else
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Fec of Crlsp Type is invalid in DoD\n");
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            return LDP_FAILURE;
        }
    }

    if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
        (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
    {
        LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock, u1Event,
                        &MsgInfo, LDP_FALSE);
    }
    else
    {
        /* The message length is obtained first */
        pu1MsgPtr = pMsg;
        pu1MsgPtr += LDP_OFF_MSG_LEN;
        LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

        /* Msg pointer now points to the message id, 
         * the message id is obtained */

        pu1MsgPtr += LDP_MSGID_LEN;

        /* Msg pointer now points to the first TLV in the message */
        while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
        {
            LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
            LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

            /* Check for bad TLV Length performed here */
            if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
            {
                /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 LDP_STAT_TYPE_BAD_TLV_LEN, NULL);
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                return LDP_FAILURE;
            }
            switch (u2TlvType)
            {
                case LDP_GEN_LABEL_TLV:
                    if (LCB_DSSN (pLspCtrlBlock) != NULL)
                    {
                        pLdpEntity = SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock));
                    }
                    if (pLdpEntity == NULL)
                    {
                        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                         LDP_STAT_INTERNAL_ERROR, NULL);
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                        return LDP_FAILURE;
                    }
                    if ((LDP_LBL_SPACE_TYPE (pLdpEntity)) != LDP_PER_PLATFORM)
                    {
                        /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                           sent. */
                        LDP_DBG1 (LDP_ADVT_MISC,
                                  "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                                  LDP_STAT_UNKNOWN_TLV);
                        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                         LDP_STAT_UNKNOWN_TLV, NULL);
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                        return LDP_FAILURE;
                    }

                    LDP_GET_4_BYTES (pu1MsgPtr, u4LabelVal);
                    Label.u4GenLbl = u4LabelVal;
                    /* As per RFC 5036 section A.1.2,
                     * In Established State if another map message
                     * is received and the labels are not equal the
                     * message will be ignored and label release message has 
                     * be sent with Status TLV (Loop detected status code)
                     * */
#if 0
                    if (LCB_STATE (pLspCtrlBlock) == LDP_LSM_ST_EST)
                    {
                        if (LCB_DLBL (pLspCtrlBlock).u4GenLbl
                            != LDP_INVALID_LABEL)
                        {
                            if (LdpCompLabels (LCB_DLBL (pLspCtrlBlock),
                                               u4LabelVal,
                                               SSN_GET_LBL_TYPE
                                               (pSessionEntry)) != LDP_EQUAL)
                            {
                                pLspCtrlBlock->pDStrSession->u1StatusCode =
                                    LDP_STAT_TYPE_LOOP_DETECTED;

                                /* Case of LSR being Ingress to the LSP */
                                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                                  &LCB_DLBL (pLspCtrlBlock),
                                                  LCB_DSSN (pLspCtrlBlock));

                                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                                return LDP_FAILURE;
                            }
                        }
                    }
#endif
                    if ((LCB_STATE (pLspCtrlBlock) == LDP_LSM_ST_EST) &&
                        (MEMCMP
                         (&LCB_DLBL (pLspCtrlBlock), &Label,
                          sizeof (uLabel)) != 0))
                    {
                        LDP_DBG3 (LDP_ADVT_PRCS,
                                  "%s: Label has Changed from : %u to %u",
                                  __func__, LCB_DLBL (pLspCtrlBlock),
                                  Label.u4GenLbl);

                        MsgInfo.u1IsLblChanged = LDP_TRUE;

                        /* If the control block is in established state
                         * and it receives a different label from the earlier
                         * one. Send Label release message for the old label */
                        LDP_DBG2 (LDP_ADVT_PRCS,
                                  "%s: Releasing Old label: %u\n", __func__,
                                  LCB_DLBL (pLspCtrlBlock));

                        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                          &LCB_DLBL (pLspCtrlBlock),
                                          LCB_DSSN (pLspCtrlBlock));
                    }

                    LDP_DBG1 (LDP_ADVT_PRCS,
                              "ADVT: Label Rcvd in Label Map Msg = %u\n",
                              u4LabelVal);
                    MEMCPY (&LCB_DLBL (pLspCtrlBlock), &Label, sizeof (uLabel));
                    break;

                case LDP_ATM_LABEL_TLV:
                    if (LCB_DSSN (pLspCtrlBlock) != NULL)
                    {
                        pLdpEntity = SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock));
                    }
                    if (pLdpEntity == NULL)
                    {
                        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                         LDP_STAT_INTERNAL_ERROR, NULL);
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                        return LDP_FAILURE;
                    }
                    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
                    {
                        /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                           sent. */
                        LDP_DBG1 (LDP_ADVT_MISC,
                                  "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                                  LDP_STAT_UNKNOWN_TLV);
                        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                         LDP_STAT_UNKNOWN_TLV, NULL);
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                        return LDP_FAILURE;
                    }
                    LDP_GET_4_BYTES (pu1MsgPtr, u4LabelVal);
                    Label.AtmLbl.u2Vpi =
                        (UINT2) ((u4LabelVal & MPLS_VPI_MASK) >> 16);
                    Label.AtmLbl.u2Vci = (UINT2) (u4LabelVal & MPLS_VCI_MASK);

                    /* In Established State if another map message
                     * is received and the labels are not equal the
                     * message will be ignored */

                    if (LCB_STATE (pLspCtrlBlock) == LDP_LSM_ST_EST)
                    {
                        if (LCB_DLBL (pLspCtrlBlock).u4GenLbl
                            != LDP_INVALID_LABEL)
                        {
                            if (LdpCompLabels (LCB_DLBL (pLspCtrlBlock),
                                               u4LabelVal,
                                               SSN_GET_LBL_TYPE
                                               (pSessionEntry)) != LDP_EQUAL)
                            {
                                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                                return LDP_FAILURE;
                            }
                        }
                    }

                    MEMCPY (&LCB_DLBL (pLspCtrlBlock), &Label, sizeof (uLabel));
                    /* If VP Merge the VCI field is insignificant */
                    if (SSN_MRGTYPE (pSessionEntry) != LDP_VP_MRG)
                    {
                        if ((Label.AtmLbl.u2Vpi & MPLS_V_BITS_MASK)
                            != LDP_ATM_V_BITS)
                        {
                            /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                               sent. */
                            LDP_DBG1 (LDP_ADVT_MISC,
                                      "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                                      LDP_STAT_UNKNOWN_TLV);
                            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                             LDP_STAT_UNKNOWN_TLV, NULL);
                            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                            return LDP_FAILURE;

                        }
                    }
                    else
                    {
                        if ((Label.AtmLbl.u2Vpi & MPLS_VP_BITS_MASK)
                            != LDP_ATM_VP_BITS)
                        {
                            /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                               sent. */
                            LDP_DBG1 (LDP_ADVT_MISC,
                                      "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                                      LDP_STAT_UNKNOWN_TLV);
                            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                             LDP_STAT_UNKNOWN_TLV, NULL);
                            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                            return LDP_FAILURE;

                        }
                        Label.AtmLbl.u2Vpi = Label.AtmLbl.u2Vpi & 0x0fff;

                    }

                    if (ldpCheckLblInLblGroup (pLdpEntity->u2LabelRangeId,
                                               Label.AtmLbl.u2Vpi,
                                               Label.AtmLbl.u2Vci) != LDP_TRUE)
                    {
                        /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                           sent. */
                        LDP_DBG1 (LDP_ADVT_MISC,
                                  "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                                  LDP_STAT_UNKNOWN_TLV);
                        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                         LDP_STAT_UNKNOWN_TLV, NULL);
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                        return LDP_FAILURE;
                    }
                    MEMCPY (&LCB_DLBL (pLspCtrlBlock), &Label, sizeof (uLabel));
                    break;

                case LDP_FEC_TLV:
                case LDP_LBL_REQ_MSGID_TLV:
                    /* 
                     * FEC values is not required to be extracted as it is available
                     * in the LSPCtrlBlock. The Label Request message id is not 
                     * required, as the required LSPCtrlBlock has already been
                     * accessed using this value in funciton "LdpHandleLblMapMsg"
                     */
                    break;

                case LDP_HOP_COUNT_TLV:
                    MsgInfo.pu1HCTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                    break;

                case LDP_PATH_VECTOR_TLV:
                    MsgInfo.pu1PVTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                    break;

                default:
                    /* As per RFC 5036 section 3.3, If both U bit and F bit are set,
                     * the unknown TLV should be forwarded with the containing message
                     * */
                    if ((u2TlvType & LDP_TLV_MSG_U_BIT) &&
                        (u2TlvType & LDP_TLV_F_BIT))
                    {
                        /* Boundary condition check for unknown TLV length
                         * */
                        if ((MsgInfo.u1UnknownTlvLen + LDP_TLV_HDR_LEN +
                             u2TlvLen) < LDP_MAX_UNKNOWN_TLV_LEN)
                        {
                            MEMCPY ((MsgInfo.au1UnknownTlv +
                                     MsgInfo.u1UnknownTlvLen),
                                    (pu1MsgPtr - LDP_TLV_HDR_LEN),
                                    LDP_TLV_HDR_LEN + u2TlvLen);
                            MsgInfo.u1UnknownTlvLen +=
                                (UINT1) (LDP_TLV_HDR_LEN + u2TlvLen);
                        }
                    }
                    break;

            }
            pu1MsgPtr += u2TlvLen;
        }
        LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock,
                        u1Event, &MsgInfo, LDP_FALSE);

    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleLblReqMsg                                        */
/* Description   : This routine is called to handle Label Request Message.   */
/* Input(s)      : pSessionEntry, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpHandleLblReqMsg (tLdpSession * pSessionEntry, UINT1 *pMsg)
{
    UINT1               u1Event;
    UINT1              *pu1MsgPtr = NULL;
    UINT1               u1HopCount = 0;
    UINT2               u2PvCount = 0;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT2               u2MsgType;
    UINT4               u4LabelOrReqId;
    tLdpMsgInfo         MsgInfo;
    UINT1               u1LblReqFec23WithErHop = LDP_FALSE;
    UINT2               u2MsgLen;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pUsLspCtrlBlock = NULL;
    UINT1              *pu2MsgPtr = NULL;
    tFec                Fec;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];
    UINT4               u4Temp = 0;
    UINT1               u1StatusType = 0;
    MEMSET (au1IpAddr, 0, ROUTER_ID_LENGTH);
    MEMSET (&MsgInfo, 0, sizeof (tLdpMsgInfo));
    MEMSET (&Fec, 0, sizeof (tFec));

    MsgInfo.pu1Msg = pMsg;

    /* FSM event initialised. */
    u1Event = LDP_LSM_EVT_LBL_REQ;
    u2MsgType = LDP_LBL_REQUEST_MSG;

    /*  Message Id of the Label Request message accessed */
    pu1MsgPtr = pMsg + LDP_OFF_MSG_ID;
    LDP_EXT_4_BYTES (pu1MsgPtr, u4LabelOrReqId);

    /* Mandatory parameter FEC TLV Presence is checked */
    /* pu1MsgPtr is now made to point to the first TLV */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

    if (u2TlvType != LDP_FEC_TLV)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: Mand Param FEC TLV is missing in LblReqMsg."
                 " Sending NotifMsg.\n");
        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_MISSING_MSG_PARAM, NULL);
        return LDP_FAILURE;
    }

    /* The message length is obtained first */
    pu2MsgPtr = pMsg + LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu2MsgPtr, u2MsgLen);

    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_BAD_TLV_LEN, NULL);
        return LDP_FAILURE;
    }

    if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
        (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
    {
        /* The LSP control block associated with the label request 
         * message accessed */

        if (LdpStrToFec (pu1MsgPtr, u2TlvLen, &Fec, &u1StatusType) ==
            LDP_FAILURE)
        {
            if (u1StatusType == LDP_STAT_TYPE_MALFORMED_TLV_VAL)
            {
                LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                         (UINT1 *) pMsg, LDP_TRUE);
            }
            /* The FEC TLV has unknown FEC type or unsupported address family.
               In that case just send the notification message */
            else if ((u1StatusType == LDP_STAT_TYPE_UNKNOWN_FEC) ||
                     (u1StatusType == LDP_STAT_TYPE_UNSUPP_ADDR_FMLY))
            {
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 u1StatusType, NULL);
            }

            return LDP_FAILURE;
        }

        pLspCtrlBlock =
            LdpGetLspCtrlBlock (pSessionEntry, u2MsgType, u4LabelOrReqId, Fec);

        if (pLspCtrlBlock == NULL)
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Failed to identify LCB for ReqMsg\n");
            return LDP_FAILURE;
        }

#if MPLS_IPV6_WANTED

        if (Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
        {

            LDP_DBG2 (LDP_ADVT_RX,
                      "Rcvd Label Request Msg for the prefix %s/%d \n",
                      Ip6PrintAddr (&Fec.Prefix.Ip6Addr), Fec.u1PreLen);
        }
        else
#endif
        {
            u4Temp = OSIX_HTONL (LDP_IPV4_U4_ADDR (Fec.Prefix));

            MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);

            LDP_DBG5 (LDP_ADVT_RX,
                      "Rcvd Label Request Msg for the prefix %d.%d.%d.%d/%d \n",
                      au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3],
                      Fec.u1PreLen);
        }

        LDP_DBG4 (LDP_ADVT_RX,
                  "Rcvd Label Request Msg from %d.%d.%d.%d\n",
                  pSessionEntry->pLdpPeer->PeerLdpId[0],
                  pSessionEntry->pLdpPeer->PeerLdpId[1],
                  pSessionEntry->pLdpPeer->PeerLdpId[2],
                  pSessionEntry->pLdpPeer->PeerLdpId[3]);

        /* Links between Session entry structure and the LSP control block
         * are initialised. The FSM routine is inovked.
         */
        if (pLspCtrlBlock->u4InComIfIndex == 0)
        {
            pLspCtrlBlock->u4InComIfIndex =
                LdpSessionGetIfIndex (pSessionEntry, Fec.u2AddrFmly);
        }
        LCB_USSN (pLspCtrlBlock) = pSessionEntry;

        if (SSN_GET_LBL_TYPE (pSessionEntry) == LDP_ATM_MODE)
        {
            pLspCtrlBlock->u1LblType = LDP_ATM_LABEL;
        }
        else if (SSN_GET_LBL_TYPE (pSessionEntry) == LDP_GEN_MODE)
        {
            pLspCtrlBlock->u1LblType = LDP_GEN_LABEL;
        }

        TMO_SLL_Insert (&SSN_ULCB (pSessionEntry), NULL,
                        &(pLspCtrlBlock->NextUSLspCtrlBlk));
        LdpInvCorrStMh (pSessionEntry, pLspCtrlBlock, u1Event,
                        &MsgInfo, LDP_FALSE);
        return LDP_SUCCESS;
    }
    else
    {
        if (LdpStrToFec (pu1MsgPtr, u2TlvLen, &Fec, &u1StatusType) ==
            LDP_FAILURE)
        {
            if (u1StatusType == LDP_STAT_TYPE_MALFORMED_TLV_VAL)
            {
                LdpSsmSendNotifCloseSsn (pSessionEntry, u1StatusType,
                                         (UINT1 *) pMsg, LDP_TRUE);
            }
            /* The FEC TLV has unknown FEC type or unsupported address family.
               In that case just send the notification message */
            else if ((u1StatusType == LDP_STAT_TYPE_UNKNOWN_FEC) ||
                     (u1StatusType == LDP_STAT_TYPE_UNSUPP_ADDR_FMLY))
            {
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 u1StatusType, NULL);
            }
            return LDP_FAILURE;
        }

#ifdef MPLS_IPV6_WANTED
        if (Fec.u2AddrFmly == IPV6)
        {
            if (u2TlvLen > LDP_IPV6_FEC_ELMNT_LEN)
            {
                /* This implies, the received FEC TLv contains more than one
                 *                      *              * Fec Element. As of now only a single Fec is supported to
                 *                                           *                           * be received in a message */
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: Ipv6FecTlv contains more than one FecElmnt\n");
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 LDP_STAT_TYPE_UNKNOWN_FEC, NULL);
                return LDP_FAILURE;
            }
        }
        else
#endif
        {
            if (u2TlvLen > LDP_FEC_ELMNT_LEN)
            {
                /* This implies, the received FEC TLv contains more than one
                 *              * Fec Element. As of now only a single Fec is supported to
                 *                           * be received in a message */
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT: FecTlv contains more than one FecElmnt\n");
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 LDP_STAT_TYPE_UNKNOWN_FEC, NULL);
                return LDP_FAILURE;
            }
        }

#if MPLS_IPV6_WANTED

        if (Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
        {

            LDP_DBG2 (LDP_ADVT_RX,
                      "Rcvd Label Request Msg for the prefix %s/%d \n",
                      Ip6PrintAddr (&Fec.Prefix.Ip6Addr), Fec.u1PreLen);
        }
        else
#endif
        {
            u4Temp = OSIX_HTONL (LDP_IPV4_U4_ADDR (Fec.Prefix));

            MEMCPY (au1IpAddr, (UINT1 *) &u4Temp, ROUTER_ID_LENGTH);

            LDP_DBG5 (LDP_ADVT_RX,
                      "Rcvd Label Request Msg for the prefix %d.%d.%d.%d/%d \n",
                      au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3],
                      Fec.u1PreLen);
        }

        LDP_DBG4 (LDP_ADVT_RX,
                  "Rcvd Label Request Msg from %d.%d.%d.%d\n",
                  pSessionEntry->pLdpPeer->PeerLdpId[0],
                  pSessionEntry->pLdpPeer->PeerLdpId[1],
                  pSessionEntry->pLdpPeer->PeerLdpId[2],
                  pSessionEntry->pLdpPeer->PeerLdpId[3]);

        if ((Fec.u1FecElmntType == LDP_FEC_PWVC_TYPE) ||
            (Fec.u1FecElmntType == LDP_FEC_GEN_PWVC_TYPE))
        {
            if (LdpRegStatus (Fec.u1FecElmntType) == LDP_REGISTER)
            {
                if (LdpHandleFecMsg (pSessionEntry, pMsg, LDP_LBL_REQUEST_MSG,
                                     Fec.u1FecElmntType) != LDP_SUCCESS)
                {
                    return LDP_FAILURE;
                }
                return LDP_SUCCESS;
            }
            else
            {
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 LDP_STAT_TYPE_UNKNOWN_FEC, NULL);
                return LDP_FAILURE;
            }
        }
        /* The LSP control block associated with the label request 
         * message accessed */

        pUsLspCtrlBlock =
            LdpGetUstrCtrlBlock (pSessionEntry, u2MsgType, u4LabelOrReqId, Fec);

        if (pUsLspCtrlBlock == NULL)
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Failed to identify LCB for ReqMsg\n");
            return LDP_FAILURE;
        }

        /* Links between Session entry structure and the LSP control block
         * are initialised. The FSM routine is inovked.
         */
        if (UPSTR_INCOM_IFINDX (pUsLspCtrlBlock) == 0)
        {
            UPSTR_INCOM_IFINDX (pUsLspCtrlBlock) =
                LdpSessionGetIfIndex (pSessionEntry, Fec.u2AddrFmly);
        }

        if (UPSTR_USSN (pUsLspCtrlBlock) == NULL)
        {
            UPSTR_USSN (pUsLspCtrlBlock) = pSessionEntry;
            TMO_SLL_Insert (&SSN_ULCB (pSessionEntry), NULL,
                            &(pUsLspCtrlBlock->NextUpstrCtrlBlk));
        }

        UPSTR_UREQ_ID (pUsLspCtrlBlock) = u4LabelOrReqId;

        if (SSN_GET_LBL_TYPE (pSessionEntry) == LDP_ATM_MODE)
        {
            pUsLspCtrlBlock->u1InLblType = LDP_ATM_LABEL;
        }
        else if (SSN_GET_LBL_TYPE (pSessionEntry) == LDP_GEN_MODE)
        {
            pUsLspCtrlBlock->u1InLblType = LDP_GEN_LABEL;
        }

        if (Fec.u1FecElmntType != LDP_FEC_CRLSP_TYPE)
        {
            if (!(Fec.u2AddrFmly == IPV4 || Fec.u2AddrFmly == IPV6))
            {
                LDP_DBG (LDP_ADVT_PRCS,
                         "ADVT : Unsupported AddrFmly in the FEC Tlv "
                         "in the Label ReqMsg.\n");
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 LDP_STAT_TYPE_UNSUPP_ADDR_FMLY, NULL);
                LdpDeleteUpstrCtrlBlock (pUsLspCtrlBlock);
                return LDP_FAILURE;
            }
        }
        else
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT : Crlsp Req Msg Rcvd in non DoD merge session .\n");
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_UNKNOWN_FEC, NULL);
            LdpDeleteUpstrCtrlBlock (pUsLspCtrlBlock);
            return LDP_FAILURE;
        }

        MEMCPY (&UPSTR_FEC (pUsLspCtrlBlock), &Fec, sizeof (tFec));
        pu1MsgPtr += u2TlvLen;

        /* optional TLVS if present are extracted  from the message */
        while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
        {
            LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
            LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

            /* Check for bad TLV Length performed here */
            if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
            {
                /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 LDP_STAT_TYPE_BAD_TLV_LEN, NULL);
                LdpDeleteUpstrCtrlBlock (pUsLspCtrlBlock);
                return LDP_FAILURE;
            }

            switch (u2TlvType)
            {
                case LDP_HOP_COUNT_TLV:
                    MsgInfo.pu1HCTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                    break;

                case LDP_PATH_VECTOR_TLV:
                    MsgInfo.pu1PVTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                    break;

                case CRLDP_EXPLROUTE_TLV:
                    u1LblReqFec23WithErHop = LDP_TRUE;
                    break;

                case CRLDP_TRAFPARM_TLV:
                case CRLDP_PINNING_TLV:
                case CRLDP_RESCLS_TLV:
                case CRLDP_PREEMPT_TLV:
                case CRLDP_LSPID_TLV:
                    break;

                default:
                    /* As per RFC 5036 section 3.3, If both U bit and F bit are set,
                     * the unknown TLV should be forwarded with the containing message
                     * */
                    /* This support is handled for single unknown TLV only.
                     * */
                    if ((u2TlvType & LDP_TLV_MSG_U_BIT) &&
                        (u2TlvType & LDP_TLV_F_BIT))
                    {
                        /* Boundary condition check for unknown TLV length
                         * */
                        if ((MsgInfo.u1UnknownTlvLen + LDP_TLV_HDR_LEN +
                             u2TlvLen) < LDP_MAX_UNKNOWN_TLV_LEN)
                        {
                            MEMCPY ((MsgInfo.au1UnknownTlv +
                                     MsgInfo.u1UnknownTlvLen),
                                    (pu1MsgPtr - LDP_TLV_HDR_LEN),
                                    LDP_TLV_HDR_LEN + u2TlvLen);
                            MsgInfo.u1UnknownTlvLen +=
                                (LDP_TLV_HDR_LEN + u2TlvLen);
                        }
                    }
                    break;
            }
            pu1MsgPtr += u2TlvLen;
        }

        if (u1LblReqFec23WithErHop == LDP_TRUE)
        {

            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT : Crlsp Req Msg Rcvd in non DoD merge session .\n");
            LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_UNKNOWN_FEC, NULL);
            LdpDeleteUpstrCtrlBlock (pUsLspCtrlBlock);
            return LDP_FAILURE;
        }
        if (LdpIsLoopFound (MsgInfo.pu1HCTlv, MsgInfo.pu1PVTlv,
                            (&u2PvCount), (&u1HopCount),
                            SSN_GET_INCRN_ID (pSessionEntry),
                            pSessionEntry, FALSE) == LDP_TRUE)
        {
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: Loop detected - IdleReqMsg\n");
            /* Notification message with "LDP_STAT_TYPE_LOOP_DETECTED" sent. */
            LdpSendNotifMsg (UPSTR_USSN (pUsLspCtrlBlock), pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_LOOP_DETECTED, NULL);
            LdpDeleteUpstrCtrlBlock (pUsLspCtrlBlock);
            return LDP_FAILURE;
        }

        LDP_MSG_HC (&MsgInfo) = u1HopCount;

        LDP_MSG_PV_COUNT (&MsgInfo) = (UINT1) u2PvCount;

        LdpInvCorrStMh (pSessionEntry, pUsLspCtrlBlock, u1Event,
                        &MsgInfo, LDP_TRUE);
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpHandleAdvertMsg                                        */
/* Description   : This routine is called to handle Advertisement Messages.  */
/* Input(s)      : pSessionEntry, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpHandleAdvertMsg (tLdpSession * pSessionEntry, UINT1 *pMsg)
{
    UINT2               u2MsgType;
    UINT1              *pu1MsgPtr = pMsg;

    LDP_GET_2_BYTES (pu1MsgPtr, u2MsgType);

    switch (u2MsgType)
    {
        case LDP_LBL_REQUEST_MSG:
            return (LdpHandleLblReqMsg (pSessionEntry, pMsg));

        case LDP_LBL_MAPPING_MSG:
            return (LdpHandleLblMapMsg (pSessionEntry, pMsg));

        case LDP_LBL_ABORT_REQ_MSG:
            return (LdpHandleLblAbrtMsg (pSessionEntry, pMsg));

        case LDP_LBL_RELEASE_MSG:
            return (LdpHandleLblRelMsg (pSessionEntry, pMsg));

        case LDP_LBL_WITHDRAW_MSG:
            return (LdpHandleLblWdrwMsg (pSessionEntry, pMsg));

        default:
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: Msg with Invalid MsgType\n");
    }
    return (LDP_SUCCESS);
}

/*****************************************************************************/
/* Function Name : LdpGetLspCtrlBlock                                        */
/* Description   : This routine locates the LSP control block corresponding  */
/*                 to the session and message.                               */
/* Input(s)      : pSessionEntry, u2MsgType, u4LabelOrReqId, Fec             */
/* Output(s)     : None                                                      */
/* Return(s)     : pointer to LSP Control Block/NULL                         */
/*****************************************************************************/
tLspCtrlBlock      *
LdpGetLspCtrlBlock (tLdpSession * pSessionEntry,
                    UINT2 u2MsgType, UINT4 u4LabelOrReqId, tFec Fec)
{
    UINT1               u1Found = LDP_FALSE;
    tTMO_SLL           *pLspCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
#endif
    UINT4               u4IfIndex = LDP_ZERO;
    switch (u2MsgType)
    {
        case LDP_LBL_REQUEST_MSG:
            pLspCtrlBlkList = &SSN_ULCB (pSessionEntry);
            TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
            {
                pLspCtrlBlock = (tLspCtrlBlock *) pSllNode;
                if (LCB_UREQ_ID (pLspCtrlBlock) == u4LabelOrReqId)
                {
                    break;
                }
            }
            if (pSllNode != NULL)
            {
                LDP_DBG (LDP_ADVT_PRCS, "ADVT: Duplicate LabelRequestMsg Rx\n");
                return NULL;
            }

            pLspCtrlBlock = (tLspCtrlBlock *) MemAllocMemBlk (LDP_LSP_POOL_ID);
            LDP_DBG3 (LDP_ADVT_MEM,
                      "ADVT: %s: %d Memory allocated for Control Block pLspCtrlBlock=0x%x\n",
                      __FUNCTION__, __LINE__, pLspCtrlBlock);

            if (pLspCtrlBlock == NULL)
            {
                LDP_DBG (LDP_ADVT_MEM, "ADVT: MemAlloc Failed for LCB\n");
                return NULL;
            }
            LdpInitLspCtrlBlock (pLspCtrlBlock);
            pLspCtrlBlock->pTrigCtrlBlk = NULL;
            return pLspCtrlBlock;

        case LDP_LBL_RELEASE_MSG:
        case LDP_LBL_ABORT_REQ_MSG:
        case LDP_NOTIF_FOR_LBL_MAP:
            pLspCtrlBlkList = (tTMO_SLL *) (&SSN_ULCB (pSessionEntry));
            break;

        case LDP_LBL_MAPPING_MSG:
        case LDP_LBL_WITHDRAW_MSG:
        case LDP_NOTIF_FOR_LBL_REQ:
        case LDP_NOTIF_FOR_LBL_REQ_ABORT:
            pLspCtrlBlkList = (tTMO_SLL *) (&SSN_DLCB (pSessionEntry));
            break;

        default:
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: Msg with Invalid MsgType\n");
            return NULL;
    }

    /* Locate LSP Control Block */

    TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
    {
        switch (u2MsgType)
        {
            case LDP_LBL_RELEASE_MSG:
                pLspCtrlBlock = (tLspCtrlBlock *) pSllNode;
                if (LdpCompLabels (LCB_ULBL (pLspCtrlBlock),
                                   u4LabelOrReqId,
                                   SSN_GET_LBL_TYPE (pSessionEntry))
                    == LDP_EQUAL)
                {
                    u1Found = LDP_TRUE;
                }
                break;

            case LDP_LBL_ABORT_REQ_MSG:
            case LDP_NOTIF_FOR_LBL_MAP:
                if (SSN_ADVTYPE (pSessionEntry) != LDP_DSTR_UNSOLICIT)
                {
                    pLspCtrlBlock = (tLspCtrlBlock *) pSllNode;
                    if (LCB_UREQ_ID (pLspCtrlBlock) == u4LabelOrReqId)
                    {
                        u1Found = LDP_TRUE;
                    }
                }
                else
                {
                    pLspCtrlBlock = (tLspCtrlBlock *) pSllNode;
                    if (LdpCompareFec (&LCB_FEC (pLspCtrlBlock), &Fec)
                        == LDP_EQUAL)
                    {
                        u1Found = LDP_TRUE;
                    }
                }
                break;

            case LDP_NOTIF_FOR_LBL_REQ_ABORT:
                pLspCtrlBlock = SLL_TO_LCB (pSllNode);
                if (LCB_DREQ_ID (pLspCtrlBlock) == u4LabelOrReqId)
                {
                    u1Found = LDP_TRUE;
                }
                break;

            case LDP_NOTIF_FOR_LBL_REQ:
            case LDP_LBL_MAPPING_MSG:

                if (SSN_ADVTYPE (pSessionEntry) != LDP_DSTR_UNSOLICIT)
                {
                    pLspCtrlBlock = SLL_TO_LCB (pSllNode);
                    if (LCB_DREQ_ID (pLspCtrlBlock) == u4LabelOrReqId)
                    {
                        u1Found = LDP_TRUE;
                    }
                }
                else
                {
                    pLspCtrlBlock = SLL_TO_LCB (pSllNode);
                    if (LdpCompareFec (&LCB_FEC (pLspCtrlBlock), &Fec)
                        == LDP_EQUAL)
                    {
                        u1Found = LDP_TRUE;
                    }
                }
                break;

            case LDP_LBL_WITHDRAW_MSG:
                pLspCtrlBlock = SLL_TO_LCB (pSllNode);
                if (LdpCompLabels (LCB_DLBL (pLspCtrlBlock), u4LabelOrReqId,
                                   SSN_GET_LBL_TYPE (pSessionEntry))
                    == LDP_EQUAL)
                {
                    u1Found = LDP_TRUE;
                }
                break;
        }
        if (u1Found == LDP_TRUE)
        {
            break;
        }
    }
    if (SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_UNSOLICIT)
    {
        if ((u1Found == LDP_FALSE) && (u2MsgType == LDP_LBL_MAPPING_MSG))
        {
            pLspCtrlBlock = (tLspCtrlBlock *) MemAllocMemBlk (LDP_LSP_POOL_ID);
            LDP_DBG3 (LDP_ADVT_MEM,
                      "ADVT: %s: %d Memory allocated for Control Block pLspCtrlBlock=0x%x\n",
                      __FUNCTION__, __LINE__, pLspCtrlBlock);

            if (pLspCtrlBlock == NULL)
            {
                LDP_DBG (LDP_ADVT_MEM, "ADVT: MemAlloc Failed for LCB\n");
                return NULL;
            }
            LdpInitLspCtrlBlock (pLspCtrlBlock);
            /* Create a MPLS Tunnel interface at CFA  and Stack over the MPLS
             * l3 interface (which is stacked over an L3IPVLAN interface,
             * and use the interface index  returned as OutIfIndex
             */

            if ((LDP_ENTITY_LDP_OVER_RSVP_FLAG
                 (SSN_GET_ENTITY (pSessionEntry)) == LDP_SNMP_TRUE) &&
                (SSN_GET_ENTITY (pSessionEntry)->OutStackTnlInfo.u4TnlId !=
                 LDP_ZERO))
            {
                if (LdpGetL3IfaceFromLdpOverRsvpOutTnl
                    (SSN_GET_ENTITY (pSessionEntry), &u4IfIndex) == LDP_FAILURE)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "LdpGetLspCtrlBlock - ADVT: Mplstunnel "
                             "IF creation failed for LSP Cntl Block \n");
                    MemReleaseMemBlock (LDP_LSP_POOL_ID,
                                        (UINT1 *) pLspCtrlBlock);
                    LDP_DBG3 (LDP_ADVT_MEM,
                              "ADVT: %s: %d Memory release for Control Block pLspCtrlBlock=0x%x\n",
                              __FUNCTION__, __LINE__, pLspCtrlBlock);

                    return NULL;
                }
            }
            else
            {
                u4IfIndex =
                    LdpSessionGetIfIndex (pSessionEntry, Fec.u2AddrFmly);
            }

#ifdef CFA_WANTED
            if (CfaIfmCreateStackMplsTunnelInterface (u4IfIndex,
                                                      &u4MplsTnlIfIndex)
                == CFA_FAILURE)
            {

                LDP_DBG (LDP_ADVT_MEM,
                         "LdpGetLspCtrlBlock - ADVT: Mplstunnel "
                         "IF creation failed for LSP Cntl Block \n");
                MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlock);
                LDP_DBG3 (LDP_ADVT_MEM,
                          "ADVT: %s: %d Memory release for Control Block pLspCtrlBlock=0x%x\n",
                          __FUNCTION__, __LINE__, pLspCtrlBlock);
                return NULL;
            }
#ifdef MPLS_IPV6_WANTED
            if (Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
            {
                LDP_DBG6 (LDP_ADVT_PRCS,
                          "LdpGetLspCtrlBlock: OutInt %d created for "
                          "FEC: %s with Peer %d.%d.%d.%d\n",
                          u4MplsTnlIfIndex, Ip6PrintAddr (&Fec.Prefix.Ip6Addr),
                          pSessionEntry->pLdpPeer->PeerLdpId[0],
                          pSessionEntry->pLdpPeer->PeerLdpId[1],
                          pSessionEntry->pLdpPeer->PeerLdpId[2],
                          pSessionEntry->pLdpPeer->PeerLdpId[3]);
            }
            else
#endif
            {
                LDP_DBG6 (LDP_ADVT_PRCS,
                          "LdpGetLspCtrlBlock: OutInt %d created for "
                          "FEC: %x with Peer %d.%d.%d.%d\n",
                          u4MplsTnlIfIndex, LDP_IPV4_U4_ADDR (Fec.Prefix),
                          pSessionEntry->pLdpPeer->PeerLdpId[0],
                          pSessionEntry->pLdpPeer->PeerLdpId[1],
                          pSessionEntry->pLdpPeer->PeerLdpId[2],
                          pSessionEntry->pLdpPeer->PeerLdpId[3]);
            }

            pLspCtrlBlock->u4OutIfIndex = u4MplsTnlIfIndex;
#else
            /* Update L3 INF Index */
            pLspCtrlBlock->u4OutIfIndex = u4IfIndex;
#endif
            MEMCPY (&LCB_FEC (pLspCtrlBlock), &Fec, sizeof (tFec));
            LCB_DSSN (pLspCtrlBlock) = pSessionEntry;
            TMO_SLL_Add ((tTMO_SLL *) (&(SSN_DLCB (pSessionEntry))),
                         (tTMO_SLL_NODE
                          *) (&(pLspCtrlBlock->NextDSLspCtrlBlk)));
            pLspCtrlBlock->pTrigCtrlBlk = NULL;
            return pLspCtrlBlock;
        }
    }
    if (pSllNode == NULL)
    {
        return NULL;
    }

    return pLspCtrlBlock;
}

/*****************************************************************************/
/* Function Name : LdpStrToFec                                               */
/* Description   : This routine converts  encoded FEC TLV to structure.      */
/*                 NOTE: Currently the routine asssumes the FEC TLV to       */
/*                 contain only one FEC parameter.                           */
/* Input(s)      : pu1Fec, u2FecTlvLen, pFec                                 */
/* Output(s)     : pu1StatusType                                             */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

UINT1
LdpStrToFec (UINT1 *pu1Fec, UINT2 u2FecTlvLen, tFec * pFec,
             UINT1 *pu1StatusType)
{

    /* This variable assigned to the pointer pu1Fec and used internally for 
     * extracting the FEC information. */
    UINT1              *pu1Lfec = pu1Fec;
    /* added to supress warning unused variable u2FecTlvLen */
    u2FecTlvLen = (UINT2) u2FecTlvLen;

    /* Getting FecElement Type */
    LDP_EXT_1_BYTES (pu1Lfec, pFec->u1FecElmntType);

    if ((gi4MplsSimulateFailure == LDP_SIM_FAILURE_CRLSP_FEC_NOT_SUPPORTED) &&
        (pFec->u1FecElmntType == LDP_FEC_CRLSP_TYPE))
    {
        *pu1StatusType = LDP_STAT_TYPE_UNKNOWN_FEC;
        return LDP_FAILURE;
    }

    if ((pFec->u1FecElmntType == LDP_FEC_WILDCARD_TYPE) ||
        (pFec->u1FecElmntType == LDP_FEC_CRLSP_TYPE) ||
        (pFec->u1FecElmntType == LDP_FEC_PWVC_TYPE) ||
        (pFec->u1FecElmntType == LDP_FEC_GEN_PWVC_TYPE))
    {
        /* 
         * Fec Element Type is of WildCard Type => No Value present for the Fec
         * Element.
         * Fec Element Type is of CR-LSP Type => FecElement contains only
         * FecElement Type.
         * Fec Element Type is of Pw VC Element Type.
         * Fec Element Type is of Gen PwVC Element Type.
         */
        return LDP_SUCCESS;
    }

    if ((pFec->u1FecElmntType == LDP_FEC_PREFIX_TYPE) ||
        (pFec->u1FecElmntType == LDP_FEC_HOSTADDR_TYPE))
    {
        LDP_EXT_2_BYTES (pu1Lfec, pFec->u2AddrFmly);
        LDP_EXT_1_BYTES (pu1Lfec, pFec->u1PreLen);

        if (!((pFec->u2AddrFmly == IPV4) || (pFec->u2AddrFmly == IPV6)))
        {
            *pu1StatusType = LDP_STAT_TYPE_UNSUPP_ADDR_FMLY;
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: FEC TLV with malformed TLV Value\n");
            return LDP_FAILURE;

        }

        if ((pFec->u2AddrFmly == IPV4) && (pFec->u1PreLen > IPV4_MAX_PRFX_LEN))
        {
            *pu1StatusType = LDP_STAT_TYPE_MALFORMED_TLV_VAL;
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: FEC TLV with malformed TLV Value\n");
            return LDP_FAILURE;
        }

        if ((pFec->u2AddrFmly == IPV6) && (pFec->u1PreLen > IPV6_MAX_PRFX_LEN))
        {
            *pu1StatusType = LDP_STAT_TYPE_MALFORMED_TLV_VAL;
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: FEC TLV with malformed TLV Value\n");
            return LDP_FAILURE;
        }

        if (pFec->u1FecElmntType == LDP_FEC_HOSTADDR_TYPE)
        {
            /* If the Address is a host address, the Host Address length
             * is Length of the Address in Octets. To extract the Address,
             * the length should be in bits. So the length is multiplied
             * by 8. This is done by doing left shift 3 times.
             */
            pFec->u1PreLen = (UINT1) ((pFec->u1PreLen) << 3);
        }
        if ((pFec->u1FecElmntType == LDP_FEC_PREFIX_TYPE) &&
            (pFec->u1PreLen == 0))
        {
            /* This condition states that the Fec Element is 
             * refering to the default destination. 
             * NOTE: The default value depends on the domain 
             * and needs to be suitably configured/initialised.
             */
            /** vishal_1 : Need to check the use case for this **/
            LDP_IPV4_U4_ADDR (pFec->Prefix) = LDP_DEF_DEST_ADDR;
            return LDP_SUCCESS;
        }
#ifdef MPLS_IPV6_WANTED
        if (pFec->u2AddrFmly == IPV6)
        {
            /* Extract the Fec Prefix from the Msg */
            LDP_EXT_IPV6_FEC_PREFIX (pu1Lfec, LDP_IPV6_U4_ADDR (pFec->Prefix),
                                     pFec->u1PreLen);
        }
        else
#endif
        {
        /*** vishal_1 : to do ***/
            /* Extract the Fec Prefix from the Msg */
            LDP_EXT_FEC_PREFIX (pu1Lfec, LDP_IPV4_U4_ADDR (pFec->Prefix),
                                pFec->u1PreLen);
        }
        return LDP_SUCCESS;
    }
    else
    {
        *pu1StatusType = LDP_STAT_TYPE_UNKNOWN_FEC;
        LDP_DBG (LDP_ADVT_PRCS, "ADVT: FEC TLV with unknown FEC type\n");
        return LDP_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : LdpNonMrgIdRequest                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and Label Request is received.                 */
/*                                                                           */
/*                 Reference - Section 3.1.5.1 - page  9 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgIdRequest (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    UINT1               u1LblReqFec23WithErHop = LDP_FALSE;
    UINT1               u1RouteStatus = LDP_TRUE;
    UINT1               u1HopCount = LDP_INVALID_HOP_COUNT;
    UINT2               u2PvCount = 0;
    UINT1               u1UnknownTlvLen = 0;
    UINT1              *pu1PVTlv = NULL;
    UINT1              *pu1HCTlv = NULL;
    UINT1              *pu1MsgPtr = pMsg;
    UINT1               au1UnknownTlv[LDP_MAX_UNKNOWN_TLV_LEN];
    UINT2               u2MsgLen;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT4               u4MsgId;
    UINT4               u4IfIndex;
    uGenU4Addr          NextHopAddr;
    UINT4               u4SessIfIndex;
    tFec                Fec;
    uLabel              Label;
    tLdpSession        *pLdpSession = NULL;
    tLdpSession        *pDssn = NULL;
    tLspTrigCtrlBlock  *pTrigCtrlBlk = NULL;
    UINT1               u1LsrEgrOrIngr = LDP_TRUE;
    UINT1               u1StatusType = 0;
    tGenU4Addr          TempNextHopAddr;
    UINT1               bLsrIsEgress;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
#endif
    MEMSET (&Fec, LDP_ZERO, sizeof (tFec));
    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (uGenU4Addr));
    MEMSET (&TempNextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));
    /* The message length is obtained first */
    pu1MsgPtr += LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Msg pointer now points to the message id, the message id is obtained */
    LDP_EXT_4_BYTES (pu1MsgPtr, u4MsgId);
    LCB_UREQ_ID (pLspCtrlBlock) = u4MsgId;

    LDP_DBG4 (LDP_ADVT_PRCS, "ADVT: %s:%d (%x)->u4UStrLblReqId=%x\n",
              __FUNCTION__, __LINE__, pLspCtrlBlock,
              LCB_UREQ_ID (pLspCtrlBlock));

    /* 
     * Mandatory parameter FEC TLV extracted from the message. this will be
     * present here as its presence has already been checked.
     */
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
    LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);
    /* Check for bad TLV Length performed here */
    if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
    {
        /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
        LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_BAD_TLV_LEN, NULL);
        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
            LCB_TRIG (pLspCtrlBlock) = NULL;
            NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
            NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                  __LINE__);
        return;
    }

    if (LdpStrToFec (pu1MsgPtr, u2TlvLen, &Fec, &u1StatusType) == LDP_FAILURE)
    {
        if (u1StatusType == LDP_STAT_TYPE_MALFORMED_TLV_VAL)
        {
            LdpSsmSendNotifCloseSsn (LCB_USSN (pLspCtrlBlock), u1StatusType,
                                     (UINT1 *) pMsg, LDP_TRUE);
        }
        /* The FEC TLV has unknown FEC type or unsupported address family.
           In that case just send the notification message */
        else if ((u1StatusType == LDP_STAT_TYPE_UNKNOWN_FEC) ||
                 (u1StatusType == LDP_STAT_TYPE_UNSUPP_ADDR_FMLY))
        {
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                             u1StatusType, NULL);
        }
        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
            LCB_TRIG (pLspCtrlBlock) = NULL;
            NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
            NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                  __LINE__);
        return;
    }

#ifdef MPLS_IPV6_WANTED
    if (Fec.u2AddrFmly == IPV6)
    {
        if (u2TlvLen > LDP_IPV6_FEC_ELMNT_LEN)
        {
            /* This implies, the received FEC TLv contains more than one 
             * Fec Element. As of now only a single Fec is supported to 
             * be received in a message*/
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: FecTlv contains more than one FecElmnt\n");
            return;
        }
    }
    else
#endif
    {
        if (u2TlvLen > LDP_FEC_ELMNT_LEN)
        {
            /* This implies, the received FEC TLv contains more than one 
             * Fec Element. As of now only a single Fec is supported to 
             * be received in a message*/
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: FecTlv contains more than one FecElmnt\n");
            return;
        }
    }

    if (Fec.u1FecElmntType != LDP_FEC_CRLSP_TYPE)
    {
        if (!((Fec.u2AddrFmly == IPV4) || (Fec.u2AddrFmly == IPV6)))
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT : Unsupported AddrFmly in the FEC Tlv in "
                     "the Label ReqMsg.\n");
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_UNSUPP_ADDR_FMLY, NULL);
            if (LCB_TRIG (pLspCtrlBlock) != NULL)
            {
                pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                LCB_TRIG (pLspCtrlBlock) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;

            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }
    }

    MEMCPY (&LCB_FEC (pLspCtrlBlock), &Fec, sizeof (tFec));
    pu1MsgPtr += u2TlvLen;

    if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
    {
        /* The routine to handle the CRLSP request is invoked */
        LdpNonMrgCrlspIdRequest (pLspCtrlBlock, pMsg);
        return;
    }

    /* optional TLVS if present are extracted  from the message */
    while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
    {
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        /* Check for bad TLV Length performed here */
        if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
        {
            /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_BAD_TLV_LEN, NULL);
            if (LCB_TRIG (pLspCtrlBlock) != NULL)
            {
                pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                LCB_TRIG (pLspCtrlBlock) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;

            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }

        switch (u2TlvType)
        {
            case LDP_HOP_COUNT_TLV:
                pu1HCTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case LDP_PATH_VECTOR_TLV:
                pu1PVTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case CRLDP_EXPLROUTE_TLV:
                u1LblReqFec23WithErHop = LDP_TRUE;
                break;

            case CRLDP_TRAFPARM_TLV:
            case CRLDP_PINNING_TLV:
            case CRLDP_RESCLS_TLV:
            case CRLDP_PREEMPT_TLV:
            case CRLDP_LSPID_TLV:
                break;

            default:
                /* As per RFC 5036 section 3.3, If both U bit and F bit are set,
                 * the unknown TLV should be forwarded with the containing message
                 * */
                /* This support is handled for single unknown TLV only.
                 * */
                if ((u2TlvType & LDP_TLV_MSG_U_BIT) &&
                    (u2TlvType & LDP_TLV_F_BIT))
                {
                    /* Boundary condition check for unknown TLV length
                     * */
                    if ((u1UnknownTlvLen + LDP_TLV_HDR_LEN + u2TlvLen) <
                        LDP_MAX_UNKNOWN_TLV_LEN)
                    {
                        MEMCPY ((au1UnknownTlv + u1UnknownTlvLen),
                                (pu1MsgPtr - LDP_TLV_HDR_LEN),
                                LDP_TLV_HDR_LEN + u2TlvLen);
                        u1UnknownTlvLen += ((UINT1) LDP_TLV_HDR_LEN + u2TlvLen);
                    }
                }
                break;
        }
        pu1MsgPtr += u2TlvLen;
    }

    if (u1LblReqFec23WithErHop == LDP_TRUE)
    {
        /* The routine to handle the CRLSP request is invoked */
        LdpNonMrgCrlspIdRequest (pLspCtrlBlock, pMsg);
        return;
    }
    pLdpSession = LCB_USSN (pLspCtrlBlock);
    u1LsrEgrOrIngr = ((pLspCtrlBlock->pDStrSession) == NULL)
        ? LDP_TRUE : LDP_FALSE;
    if (LdpIsLoopFound (pu1HCTlv, pu1PVTlv, (&u2PvCount), (&u1HopCount),
                        SSN_GET_INCRN_ID (pLdpSession),
                        pLdpSession, u1LsrEgrOrIngr) == LDP_TRUE)
    {
        LDP_DBG (LDP_ADVT_PRCS, "ADVT: Loop detected - IdleReqMsg\n");
        /* Notification message with "LDP_STAT_TYPE_LOOP_DETECTED" sent. */
        LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_LOOP_DETECTED, NULL);
        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
            LCB_TRIG (pLspCtrlBlock) = NULL;
            NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
            NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                  __LINE__);
        return;
    }
    /* 
     * LSR is checked whether it is Egress for the FEC. If not the next hop
     * address to reach the FEC and the outgoing interface index are obtained
     * from the IP module.
     */
    bLsrIsEgress = LdpIsLsrEgress ((tFec *) & Fec, &u1RouteStatus, &NextHopAddr,
                                   &u4IfIndex,
                                   (UINT2) SSN_GET_INCRN_ID (pLdpSession));
    MEMCPY (&TempNextHopAddr.Addr, &NextHopAddr, sizeof (uGenAddr));
    TempNextHopAddr.u2AddrType = Fec.u2AddrFmly;
    if ((bLsrIsEgress == LDP_TRUE) ||
        ((u1RouteStatus == LDP_TRUE)
         &&
         (LdpGetPeerSession
          (&TempNextHopAddr, u4IfIndex, &pDssn,
           SSN_GET_INCRN_ID (pLdpSession)) == LDP_FAILURE)))
    {
        u4SessIfIndex = LdpSessionGetIfIndex (pLdpSession, Fec.u2AddrFmly);

        /* The PHP Configuration is verified */

        if ((LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY ((pLdpSession))))
            != LDP_TRUE)
        {
            if (((LdpIsAddressZero (&NextHopAddr, Fec.u2AddrFmly) == LDP_TRUE)
                 ||
                 (LdpIsAddressMatch
                  (&NextHopAddr, &Fec.Prefix, Fec.u2AddrFmly)))
                &&
                ((LDP_ENTITY_PHP_CONF (SSN_GET_ENTITY (pLdpSession)) ==
                  MPLS_IMPLICIT_NULL_LABEL)
                 || (LDP_ENTITY_PHP_CONF (SSN_GET_ENTITY (pLdpSession)) ==
                     MPLS_IPV4_EXPLICIT_NULL_LABEL)))
            {
                Label.u4GenLbl
                    = LDP_ENTITY_PHP_CONF (SSN_GET_ENTITY (pLdpSession));
                LDP_DBG1 (LDP_ADVT_MISC,
                          "ADVT: PHP Configured with Label Value - %x \n",
                          Label.u4GenLbl);

                MEMCPY (&LCB_ULBL (pLspCtrlBlock), &Label, sizeof (uLabel));

                if (Label.u4GenLbl == MPLS_IPV4_EXPLICIT_NULL_LABEL)
                {
                    LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP,
                                           pLspCtrlBlock, NULL);
                }

                LdpSendLblMappingMsg (pLspCtrlBlock, u1HopCount,
                                      (pu1PVTlv + LDP_TLV_HDR_LEN),
                                      (UINT1) u2PvCount);

                LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;
                /* If PHP is configured there is no need to do an FM 
                 * MLIB update at the egress so function returns after 
                 * sending a label mapping message */

                LDP_DBG4 (LDP_ADVT_PRCS,
                          "ADVT: %s:%d (%x)->u4UStrLblReqId=%x\n", __FUNCTION__,
                          __LINE__, pLspCtrlBlock, LCB_UREQ_ID (pLspCtrlBlock));
                return;
            }
        }

        if (LdpGetLabel (SSN_GET_ENTITY (pLdpSession), &Label, u4SessIfIndex)
            != LDP_SUCCESS)
        {
            LDP_DBG (LDP_ADVT_MEM, "ADVT: Alloc Label Failed - IdleReqMsg\n");
            /* Notification message with "LDP_STAT_TYPE_NO_LBL_RSRC"  sent. */
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_NO_LBL_RSRC, NULL);
            if (LCB_TRIG (pLspCtrlBlock) != NULL)
            {
                pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                LCB_TRIG (pLspCtrlBlock) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }

        /* Label successfully allocated */
        MEMCPY (&LCB_ULBL (pLspCtrlBlock), &Label, sizeof (uLabel));
        /* To Connect this upstream label to the local IP 
         * forwarding module; */
        /* MLIB update for ILM, with just only one pop is called. */
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP,
                               pLspCtrlBlock, NULL);
        /* Successful MLIB updation also done */
        LdpSendLblMappingMsg (pLspCtrlBlock, u1HopCount,
                              (pu1PVTlv + LDP_TLV_HDR_LEN), (UINT1) u2PvCount);
        LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;

        LDP_DBG4 (LDP_ADVT_PRCS, "ADVT: %s:%d (%x)->u4UStrLblReqId=%x\n",
                  __FUNCTION__, __LINE__, pLspCtrlBlock,
                  LCB_UREQ_ID (pLspCtrlBlock));
        return;
    }
    else
    {
        if (u1RouteStatus == LDP_FALSE)
        {
            /* 
             * Notification message sent to upstream that no route is present
             * and the LSP Control block is released.
             */
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_NO_ROUTE, NULL);
            if (LCB_TRIG (pLspCtrlBlock) != NULL)
            {
                pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                LCB_TRIG (pLspCtrlBlock) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }

        /* 
         * Case where the LSR is not an egress for the FEC. The next hop
         * address and the outgoing interface index for the FEC were
         * determined during the "IsEgress" check done above. These values
         * are made use of now.
         */
        LdpCopyAddr (&pLspCtrlBlock->NextHopAddr, &NextHopAddr, Fec.u2AddrFmly);
        pLspCtrlBlock->u2AddrType = Fec.u2AddrFmly;

        MEMCPY (&TempNextHopAddr.Addr, &NextHopAddr, sizeof (uGenAddr));
        TempNextHopAddr.u2AddrType = Fec.u2AddrFmly;
        /* 
         * The LDP Peer that has advertised the Next Hop Address in its list
         * of address transmitted via the ldp address message is located.
         */
        if (LdpGetPeerSession (&TempNextHopAddr, u4IfIndex, &pDssn,
                               SSN_GET_INCRN_ID (pLdpSession)) == LDP_FAILURE)
        {
            pLspCtrlBlock->pu1PVTlv = pu1PVTlv;
            pLspCtrlBlock->u1HopCount = u1HopCount;
            pLspCtrlBlock->u1PVCount = (UINT1) u2PvCount;
            return;
        }
        else
        {
            if (pDssn == LCB_USSN (pLspCtrlBlock))
            {
                LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg,
                                 LDP_FALSE, LDP_STAT_TYPE_LOOP_DETECTED, NULL);
                if (LCB_TRIG (pLspCtrlBlock) != NULL)
                {
                    pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                    LCB_TRIG (pLspCtrlBlock) = NULL;
                    NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                    NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                    LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n",
                              __FUNCTION__, __LINE__);
                    return;
                }
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;
            }
            /* 
             * Pointer to the down stream session is obtained in case 
             * of successful LdpGetPeerSession checking.
             */
#ifdef CFA_WANTED
            /* Create a MPLS Tunnel interface at CFA and Stack over the MPLS
             * interface (which is stacked over an L3IPVLAN interface)
             * and use that interface index  as OutIfIndex
             *       ____________
             *      |   L3 MPLS  |  ---> To be created now
             *      |____________|
             *      |   MPLS     |  ---> Already created during Init
             *      |____________| 
             *      |  L3 IPVLAN |
             *      |____________|
             */
            if (CfaIfmCreateStackMplsTunnelInterface
                (LdpSessionGetIfIndex (pDssn, Fec.u2AddrFmly),
                 &u4MplsTnlIfIndex) == CFA_FAILURE)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         " LdpNonMrgIdRequest - ADVT: Mplstunnel"
                         " IF creation failed for LSP Cntl block\n");
                LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                 LDP_STAT_TYPE_INTERNAL_ERROR, NULL);
                if (LCB_TRIG (pLspCtrlBlock) != NULL)
                {
                    pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                    LCB_TRIG (pLspCtrlBlock) = NULL;
                    NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                    NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                    LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n",
                              __FUNCTION__, __LINE__);
                    return;
                }
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;
            }
#ifdef MPLS_IPV6_WANTED
            if (Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
            {
                LDP_DBG6 (LDP_ADVT_PRCS,
                          "LdpNonMrgIdRequest: OutInt %d created for "
                          "FEC: %s with Peer %d.%d.%d.%d\n",
                          u4MplsTnlIfIndex, Ip6PrintAddr (&Fec.Prefix.Ip6Addr),
                          pDssn->pLdpPeer->PeerLdpId[0],
                          pDssn->pLdpPeer->PeerLdpId[1],
                          pDssn->pLdpPeer->PeerLdpId[2],
                          pDssn->pLdpPeer->PeerLdpId[3]);
            }
            else
#endif
            {
                LDP_DBG6 (LDP_ADVT_PRCS,
                          "LdpNonMrgIdRequest: OutInt %d created for "
                          "FEC: %x with Peer %d.%d.%d.%d\n",
                          u4MplsTnlIfIndex, LDP_IPV4_U4_ADDR (Fec.Prefix),
                          pDssn->pLdpPeer->PeerLdpId[0],
                          pDssn->pLdpPeer->PeerLdpId[1],
                          pDssn->pLdpPeer->PeerLdpId[2],
                          pDssn->pLdpPeer->PeerLdpId[3]);

            }

            pLspCtrlBlock->u4OutIfIndex = u4MplsTnlIfIndex;
#else
            /* Update L3 INF Index */
            pLspCtrlBlock->u4OutIfIndex =
                LdpSessionGetIfIndex (pDssn, Fec.u2AddrFmly);
#endif
            LCB_DSSN (pLspCtrlBlock) = pDssn;

            LDP_DBG5 (LDP_ADVT_PRCS,
                      "ADVT: %s:%d (%x)->u4UStrLblReqId= %x, u4DStrLblReqId = %x\n",
                      __FUNCTION__, __LINE__, pLspCtrlBlock,
                      LCB_UREQ_ID (pLspCtrlBlock), LCB_DREQ_ID (pLspCtrlBlock));

            /* 
             * The LSP control block is added to the list of down stream LSP
             * ctrlblock list with respect to the downstream session.
             */
            TMO_SLL_Insert (&SSN_DLCB (pDssn), NULL, (tTMO_SLL_NODE *)
                            & (pLspCtrlBlock->NextDSLspCtrlBlk));

            pLdpSession = LCB_DSSN (pLspCtrlBlock);
            if ((pLdpSession->u1StatusRecord) == LDP_STAT_TYPE_NO_LBL_RSRC)
            {
                LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                 LDP_STAT_TYPE_NO_ROUTE, NULL);
                if (LCB_TRIG (pLspCtrlBlock) != NULL)
                {
                    pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                    LCB_TRIG (pLspCtrlBlock) = NULL;
                    NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                    NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                    LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                    LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n",
                              __FUNCTION__, __LINE__);
                    return;
                }
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;
            }
            LdpSendLblReqMsg (pLspCtrlBlock, u1HopCount,
                              (pu1PVTlv + LDP_TLV_HDR_LEN), (UINT1) u2PvCount,
                              &au1UnknownTlv[0], u1UnknownTlvLen);

            /* The advertisement mode from the controlblock is checked. */
            if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_INDEPENDENT_MODE)
            {
                LDP_DBG2 (LDP_ADVT_PRCS,
                          "%s: %d Label Allocation mode is Independent\n",
                          __func__, __LINE__);
                /* The LSP -State machine state moves to Established State 
                 * in INDEPENDENT Mode */
                LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;

                /* Need to get the free label from the session in which 
                 * label request is received. */
                pLdpSession = LCB_USSN (pLspCtrlBlock);

                u4SessIfIndex =
                    LdpSessionGetIfIndex (pLdpSession, Fec.u2AddrFmly);
                if (LdpGetLabel
                    (SSN_GET_ENTITY (pLdpSession), &Label,
                     u4SessIfIndex) != LDP_SUCCESS)
                {
                    LDP_DBG (LDP_ADVT_MEM,
                             "ADVT: Alloc Label Failed - IdleReqMsg\n");
                    /* Notif msg with status LDP_STAT_TYPE_NO_LBL_RSRC 
                     * is sent. */
                    LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                     LDP_STAT_TYPE_NO_LBL_RSRC, NULL);
                    LdpSendLblAbortReqMsg (pLspCtrlBlock);
                    if (LCB_TRIG (pLspCtrlBlock) != NULL)
                    {
                        pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                        LCB_TRIG (pLspCtrlBlock) = NULL;
                        NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                        NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                        LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                        LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n",
                                  __FUNCTION__, __LINE__);
                        return;
                    }
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);

                    LDP_DBG4 (LDP_ADVT_PRCS,
                              "ADVT: %s:%d (%x)->u4UStrLblReqId=%x\n",
                              __FUNCTION__, __LINE__, pLspCtrlBlock,
                              LCB_UREQ_ID (pLspCtrlBlock));
                    return;
                }
                else
                {
                    LDP_DBG3 (LDP_ADVT_PRCS,
                              "%s: %d Label Allocated for Upstream node: %d\n",
                              __func__, __LINE__, Label.u4GenLbl);
                    /* Label successfully allocated */
                    MEMCPY (&LCB_ULBL (pLspCtrlBlock), &Label, sizeof (uLabel));

                    LdpSendMplsMlibUpdate
                        (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP,
                         pLspCtrlBlock, NULL);
                    /* MLIB update being success */
                    /* 
                     * As the LSR is intermediate and the label assignment
                     * is independent mode, the returned hop count value
                     * must be one, and the path vector must contain only
                     * the local lsr id. Hence the u1HopCount is made zero
                     * and the u2PvCount is made zero.
                     */
                    u1HopCount = 0;
                    u2PvCount = 0;
                    LdpSendLblMappingMsg (pLspCtrlBlock, u1HopCount,
                                          (pu1PVTlv + LDP_TLV_HDR_LEN),
                                          (UINT1) u2PvCount);

                    LDP_DBG4 (LDP_ADVT_PRCS,
                              "ADVT: %s:%d (%x)->u4UStrLblReqId=%x\n",
                              __FUNCTION__, __LINE__, pLspCtrlBlock,
                              LCB_UREQ_ID (pLspCtrlBlock));
                    return;
                }
            }
            else if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_ORDERED_MODE)
            {
                LDP_DBG2 (LDP_ADVT_PRCS,
                          "Label distribution mode is Ordered, making the "
                          "DLCB to Response awaited state: %s: %d\n", __func__,
                          __LINE__);

                /* The LSP -State machine state moves to RESPONSE AWAIT State */
                LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_RSP_AWT;

                LDP_DBG5 (LDP_ADVT_PRCS,
                          "ADVT: %s:%d (%x)->u4UStrLblReqId= %x, u4DStrLblReqId = %x\n",
                          __FUNCTION__, __LINE__, pLspCtrlBlock,
                          LCB_UREQ_ID (pLspCtrlBlock),
                          LCB_DREQ_ID (pLspCtrlBlock));
            }
        }
    }
}

/*****************************************************************************/
/* Function Name : LdpNonMrgIdLdpMap                                         */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and a Label Mapping Msg is received.           */
/*                                                                           */
/*                 Reference - Section 3.5.9.1 - draft-ietf-mpls-ldp-06.txt  */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pMsg - this will be NULL in this case.                    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgIdLdpMap (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    UINT2               u2LblType;
    UINT4               u4Label;
    uLabel              Label;
    tLspTrigCtrlBlock  *pTrigCtrlBlk = NULL;
    UINT1              *pu1Msg = pMsg;
    if ((LCB_SUB_STATE (pLspCtrlBlock) == LDP_IDLE_NAKORMAP) ||
        (LCB_SUB_STATE (pLspCtrlBlock) == LDP_IDLE_NAKORMAP_REL))
    {
        /* 
         * LSR Rx a Label MapMsg in response to a Label ReqMsg, after 
         * it had sent a Label Abort Request Msg to abort the Label 
         * Request. For this case, Lsr chooses to release the Label 
         * with a Label RelMsg.
         */

        /* Extracting the Label Type from the Mapping Msg and offsetting the
         * pointer to the Label present in the Msg.
         */
        LDP_EXT_LBL_TYPE (pu1Msg, u2LblType);

        switch (u2LblType)
        {
            case LDP_GEN_LABEL_TLV:
                LDP_GET_4_BYTES (pu1Msg, u4Label);
                pLspCtrlBlock->u1LblType = LDP_GEN_LABEL;
                Label.u4GenLbl = u4Label;
                break;

            case LDP_ATM_LABEL_TLV:
                LDP_GET_4_BYTES (pu1Msg, u4Label);
                pLspCtrlBlock->u1LblType = LDP_ATM_LABEL;
                Label.AtmLbl.u2Vpi = (UINT2) ((u4Label & MPLS_VPI_MASK) >> 16);
                Label.AtmLbl.u2Vci = (UINT2) (u4Label & MPLS_VCI_MASK);
                break;

            default:
                /* Check for bad TLV Type performed here */
                if (u2LblType < LDP_MIN_TLV_VAL)
                {
                    /* Notification message with 
                     * "LDP_STAT_TYPE_UNKNOWN_TLV" sent. */
                    LdpSendNotifMsg (LCB_DSSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                     LDP_STAT_TYPE_UNKNOWN_TLV, NULL);
                    if (LCB_TRIG (pLspCtrlBlock) != NULL)
                    {
                        pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                        LCB_TRIG (pLspCtrlBlock) = NULL;
                        NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                        NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                        LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                        LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n",
                                  __FUNCTION__, __LINE__);
                        return;
                    }
                    LdpDeleteLspCtrlBlock (pLspCtrlBlock);

                }
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;
        }

        /* Sending Label Release Msg DnStream, and Deleting LCB */
        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock), &Label,
                          LCB_DSSN (pLspCtrlBlock));
    }

    if (LCB_SUB_STATE (pLspCtrlBlock) == LDP_IDLE_NAKORMAP)
    {
        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
            LCB_TRIG (pLspCtrlBlock) = NULL;
            NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
            NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
    }
    else if (LCB_SUB_STATE (pLspCtrlBlock) == LDP_IDLE_NAKORMAP_REL)
    {
        /* Waiting for Rel Msg from UpStream */
        LCB_SUB_STATE (pLspCtrlBlock) = LDP_IDLE_REL;
    }
    LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__, __LINE__);
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgIdRel                                            */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and a Release Msg is received.                 */
/*                                                                           */
/*                 Reference - Section 3.5.9.1 - draft-ietf-mpls-ldp-06.txt  */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pMsg - Points to the rx Rel Msg.                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgIdRel (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    UINT4               u4SessIfIndex;
    tLdpSession        *pLdpSession = LCB_USSN (pLspCtrlBlock);
    tLspTrigCtrlBlock  *pTrigCtrlBlk = NULL;

    /* added to supress warning unused variable pMsg */
    UNUSED_PARAM (pMsg);

    /* Deleting the label from Mlib and freeing it */
    LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP,
                           pLspCtrlBlock, NULL);
    u4SessIfIndex =
        LdpSessionGetIfIndex (pLdpSession, pLspCtrlBlock->Fec.u2AddrFmly);
    LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &LCB_ULBL (pLspCtrlBlock),
                  u4SessIfIndex);

    if (LCB_SUB_STATE (pLspCtrlBlock) == LDP_IDLE_NAKORMAP_REL)
    {
        /* Waiting for either Nak(ReqAborted) or Map Msg, from DownStream */
        LCB_SUB_STATE (pLspCtrlBlock) = LDP_IDLE_NAKORMAP;
    }
    else if (LCB_SUB_STATE (pLspCtrlBlock) == LDP_IDLE_REL)
    {
        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
            LCB_TRIG (pLspCtrlBlock) = NULL;
            NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
            NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
    }
    LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__, __LINE__);
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgIdDnNak                                          */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and a NAK Msg is received.                     */
/*                                                                           */
/*                 Reference - Section 3.5.9.1 - draft-ietf-mpls-ldp-06.txt  */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pMsg - Points to the Type of Notif rx from DStream.       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgIdDnNak (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    UINT4               u4StatusData = *(UINT4 *) (VOID *) pMsg;
    tLspTrigCtrlBlock  *pTrigCtrlBlk = NULL;
    /* 
     * Nak corresponds to an outstanding Label Req Abort.
     * Deleting the LCB. 
     */
    if (u4StatusData == LDP_STAT_LBL_REQ_ABRTD)
    {
        if (LCB_SUB_STATE (pLspCtrlBlock) == LDP_IDLE_NAKORMAP)
        {
            if (LCB_TRIG (pLspCtrlBlock) != NULL)
            {
                pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                LCB_TRIG (pLspCtrlBlock) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;

            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
        }
        else if (LCB_SUB_STATE (pLspCtrlBlock) == LDP_IDLE_NAKORMAP_REL)
        {
            /* Waiting for Rel Msg from UpStream */
            LCB_SUB_STATE (pLspCtrlBlock) = LDP_IDLE_REL;
        }
    }
    LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__, __LINE__);
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgIdIntSetup                                       */
/* Description   : This routine is called when label state machine is in     */
/*                 IDLE state and Internal Setup event is received.          */
/*                                                                           */
/*                 Reference - Section 3.1.5.1 - page 11 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock,                                            */
/*                 pMsg - this will be NULL in this case.                    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgIdIntSetup (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{

    tLspTrigCtrlBlock  *pNHTrigCtrlBlk = NULL;
    tLdpSession        *pDssn = NULL;
    UINT2               u2IncarnId = 0;
    tGenU4Addr          NextHopAddr;
    UINT4               u4L3VlanIf = 0;
    tLspTrigCtrlBlock  *pTrigCtrlBlk = NULL;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
#endif

    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));
    LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Entry\n", __FUNCTION__, __LINE__);

    if ((LCB_TRIG (pLspCtrlBlock)) &&
        (LCB_TRIG_TYPE (pLspCtrlBlock) != NEXT_HOP_CHNG_LSP_SET_UP))
    {
        pDssn = LCB_DSSN (pLspCtrlBlock);
        if ((pDssn->u1StatusRecord) == LDP_STAT_TYPE_NO_LBL_RSRC)
        {
            if (LCB_TRIG (pLspCtrlBlock) != NULL)
            {
                pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                LCB_TRIG (pLspCtrlBlock) = NULL;
                NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;

            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);

            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }

        if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
        {
            /* The routine to send the CRLSP request is invoked */
            LdpSendCrlspLblReqMsg (pLspCtrlBlock);
        }
        else
        {
            LdpSendLblReqMsg (pLspCtrlBlock, 0, NULL, 0, NULL, 0);
        }

        LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_RSP_AWT;
        LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                  __LINE__);
        return;
    }
    else
    {
        if (pMsg == NULL)
        {
            if (LCB_TRIG (pLspCtrlBlock) != NULL)
            {
                pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                LCB_TRIG (pLspCtrlBlock) = NULL;
                if (NH_TRGCB_OLSP (pTrigCtrlBlk) != NULL)
                {
                    LCB_NHOP (NH_TRGCB_OLSP (pTrigCtrlBlk)) = NULL;
                }
                NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;

            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);

            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }

        pNHTrigCtrlBlk = (tLspTrigCtrlBlock *) (VOID *) pMsg;
        /* Set up the Trig control block Ptr */
        LCB_TRIG (pLspCtrlBlock) = pNHTrigCtrlBlk;

        LdpCopyAddr (&NextHopAddr.Addr, &pLspCtrlBlock->NextHopAddr,
                     pLspCtrlBlock->Fec.u2AddrFmly);
        NextHopAddr.u2AddrType = pLspCtrlBlock->Fec.u2AddrFmly;

        if (CfaUtilGetIfIndexFromMplsTnlIf
            (pLspCtrlBlock->u4OutIfIndex, &u4L3VlanIf, TRUE) != CFA_SUCCESS)
        {
            LDP_DBG4 (LDP_ADVT_PRCS,
                      "%s : %d CfaUtilGetIfIndexFromMplsTnlIf() Failed pLspCtrlBlock->u4OutIfIndex=%d u4L3VlanIf=%d\n",
                      __FUNCTION__, __LINE__, pLspCtrlBlock->u4OutIfIndex,
                      u4L3VlanIf);
        }

        if (LdpGetPeerSession (&NextHopAddr,
                               u4L3VlanIf, &pDssn,
                               SSN_GET_INCRN_ID
                               (NH_TRGCB_DSESSION (pNHTrigCtrlBlk)))
            == LDP_FAILURE)
        {
            /*
             * Down Stream session of new next hop is not present. Delete 
             * the LSP Control block. 
             */
            u2IncarnId = 0;
            if (pLspCtrlBlock->pCrlspTnlInfo != NULL)
            {
                LdpDeleteCrlspTnlInfo (u2IncarnId,
                                       pLspCtrlBlock->pCrlspTnlInfo);
            }
            if (LCB_TRIG (pLspCtrlBlock) != NULL)
            {
                pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                LCB_TRIG (pLspCtrlBlock) = NULL;

                if (NH_TRGCB_OLSP (pTrigCtrlBlk) != NULL)
                {
                    LCB_NHOP (NH_TRGCB_OLSP (pTrigCtrlBlk)) = NULL;
                }
                NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);

            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }

#ifdef CFA_WANTED
        /* Create a MPLS Tunnel interface at CFA and Stack over the MPLS
         * interface (which is stacked over an L3IPVLAN interface)
         * and use that interface index  as OutIfIndex
         *       ____________
         *      |   L3 MPLS  |  ---> To be created now
         *      |____________|
         *      |   MPLS     |  ---> Already created during Init
         *      |____________| 
         *      |  L3 IPVLAN |
         *      |____________|
         */
        if (CfaIfmCreateStackMplsTunnelInterface
            (LdpSessionGetIfIndex (pDssn, pLspCtrlBlock->Fec.u2AddrFmly),
             &u4MplsTnlIfIndex) == CFA_FAILURE)
        {

            LDP_DBG (LDP_ADVT_MEM,
                     " LdpNonMrgIdIntSetup- ADVT: Mplstunnel"
                     " IF creation failed for LSP Cntl block\n");
            if (LCB_TRIG (pLspCtrlBlock) != NULL)
            {
                pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                LCB_TRIG (pLspCtrlBlock) = NULL;

                if (NH_TRGCB_OLSP (pTrigCtrlBlk) != NULL)
                {
                    LCB_NHOP (NH_TRGCB_OLSP (pTrigCtrlBlk)) = NULL;
                }

                NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);

            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }

#ifdef MPLS_IPV6_WANTED
        if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
        {
            LDP_DBG6 (LDP_ADVT_PRCS,
                      "LdpNonMrgIdRequest: OutInt %d created for "
                      "FEC: %s with Peer %d.%d.%d.%d\n",
                      u4MplsTnlIfIndex,
                      Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.Ip6Addr),
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);
        }
        else
#endif
        {
            LDP_DBG6 (LDP_ADVT_PRCS,
                      "LdpNonMrgIdRequest: OutInt %d created for "
                      "FEC: %x with Peer %d.%d.%d.%d\n",
                      u4MplsTnlIfIndex,
                      LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix),
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                      pDssn->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);
        }

        pLspCtrlBlock->u4OutIfIndex = u4MplsTnlIfIndex;
#else
        /* Update L3 INF Index */
        pLspCtrlBlock->u4OutIfIndex =
            LdpSessionGetIfIndex (pDssn, pLspCtrlBlock->Fec.u2AddrFmly);
#endif

        LCB_DSSN (pLspCtrlBlock) = pDssn;
        TMO_SLL_Add ((tTMO_SLL *) (&(SSN_DLCB (pDssn))),
                     (tTMO_SLL_NODE *) (&(pLspCtrlBlock->NextDSLspCtrlBlk)));

        pDssn = LCB_DSSN (pLspCtrlBlock);
        if ((pDssn->u1StatusRecord) == LDP_STAT_TYPE_NO_LBL_RSRC)
        {
            if (LCB_TRIG (pLspCtrlBlock) != NULL)
            {
                pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
                LCB_TRIG (pLspCtrlBlock) = NULL;
                if (NH_TRGCB_OLSP (pTrigCtrlBlk) != NULL)
                {
                    LCB_NHOP (NH_TRGCB_OLSP (pTrigCtrlBlk)) = NULL;
                }
                NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
                LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                          __LINE__);
                return;
            }
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);

            LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                      __LINE__);
            return;
        }

        if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
        {
            /* The routine to handle the CRLSP request is invoked */
            LdpSendCrlspLblReqMsg (pLspCtrlBlock);
        }
        else
        {
            LdpSendLblReqMsg (pLspCtrlBlock, 0, NULL, 0, NULL, 0);
        }
        LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_RSP_AWT;
        LDP_DBG2 (LDP_ADVT_PRCS, "ADVT: %s:%d : Exit\n", __FUNCTION__,
                  __LINE__);
        return;
    }
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRspMap                                           */
/* Description   : This routine is called when label state machine is in     */
/*                 RESPONSE_AWAITED state and Label Mapping Message received.*/
/*                                                                           */
/*                 Reference - Section 3.1.5.2 - page 12 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpNonMrgRspMap (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    UINT1               u1HopCount = LDP_INVALID_HOP_COUNT;
    UINT2               u2PvTlvLen = 0;
    UINT1              *pu1PvTlv = NULL;
    UINT1              *pu1HcTlv = NULL;
    UINT1              *pu1MsgPtr = pMsg;
    UINT2               u2MsgLen = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLen = 0;
    UINT4               u4SessIfIndex = 0;
    uLabel              Label;
    UINT4               u4LabelVal;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1               u1LsrEgrOrIngr = LDP_TRUE;
    tGenU4Addr          NextHopAddr;
    UINT4               u4L3VlanIf = 0;

    MEMSET (&Label, 0, sizeof (uLabel));
    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));
    if (gu4LdpLspPersist == FALSE)
    {
        if (pLspCtrlBlock->pTrigCtrlBlk != NULL)
        {
            /* The Retry Timer is only at the Ingress */
            TmrStopTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *)
                          & (pLspCtrlBlock->TardyTimer.AppTimer));
        }
    }

    if (pLspCtrlBlock->Fec.u1FecElmntType == LDP_FEC_CRLSP_TYPE)
    {
        /* The routine to handle the CRLSP request is invoked */
        LdpNonMrgCrlspRspMap (pLspCtrlBlock, pMsg);
        return;
    }

    /* The message length is obtained first */
    pu1MsgPtr += LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Msg pointer now points to the message id, the message id is obtained */
    pu1MsgPtr += LDP_MSGID_LEN;

    /* Msg pointer now points to the first TLV in the message */
    while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
    {
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        /* Check for bad TLV Length performed here */
        if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
        {
            /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_BAD_TLV_LEN, NULL);
            LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
            return;
        }
        switch (u2TlvType)
        {
            case LDP_GEN_LABEL_TLV:

                pLdpEntity = SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock));
                if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_PLATFORM)
                {
                    /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                       sent. */
                    LDP_DBG1 (LDP_ADVT_MISC,
                              "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                              LDP_STAT_UNKNOWN_TLV);
                    LdpSendNotifMsg (LCB_DSSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                     LDP_STAT_UNKNOWN_TLV, NULL);
                    LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
                    return;
                }

                LDP_GET_4_BYTES (pu1MsgPtr, u4LabelVal);
                Label.u4GenLbl = u4LabelVal;
                MEMCPY (&LCB_DLBL (pLspCtrlBlock), &Label, sizeof (uLabel));
                break;

            case LDP_ATM_LABEL_TLV:
                pLdpEntity = SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock));
                if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
                {
                    /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                       sent. */
                    LDP_DBG1 (LDP_ADVT_MISC,
                              "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                              LDP_STAT_UNKNOWN_TLV);
                    LdpSendNotifMsg (LCB_DSSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                     LDP_STAT_UNKNOWN_TLV, NULL);
                    LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
                    return;
                }
                LDP_GET_4_BYTES (pu1MsgPtr, u4LabelVal);
                Label.AtmLbl.u2Vpi =
                    (UINT2) ((u4LabelVal & MPLS_VPI_MASK) >> 16);
                if ((Label.AtmLbl.u2Vpi & MPLS_V_BITS_MASK) != LDP_ATM_V_BITS)
                {
                    /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                       sent. */
                    LDP_DBG1 (LDP_ADVT_MISC,
                              "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                              LDP_STAT_UNKNOWN_TLV);
                    LdpSendNotifMsg (LCB_DSSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                     LDP_STAT_UNKNOWN_TLV, NULL);
                    LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
                    return;

                }
                Label.AtmLbl.u2Vci = (UINT2) (u4LabelVal & MPLS_VCI_MASK);
                if (ldpCheckLblInLblGroup (pLdpEntity->u2LabelRangeId,
                                           Label.AtmLbl.u2Vpi,
                                           Label.AtmLbl.u2Vci) != LDP_TRUE)
                {
                    /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                       sent. */
                    LDP_DBG1 (LDP_ADVT_MISC,
                              "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                              LDP_STAT_UNKNOWN_TLV);
                    LdpSendNotifMsg (LCB_DSSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                     LDP_STAT_UNKNOWN_TLV, NULL);
                    LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
                    return;
                }

                MEMCPY (&LCB_DLBL (pLspCtrlBlock), &Label, sizeof (uLabel));
                break;

            case LDP_FEC_TLV:
            case LDP_LBL_REQ_MSGID_TLV:
                /* 
                 * FEC values is not required to be extracted as it is available
                 * in the LSPCtrlBlock. The Label Request message id is not 
                 * required, as the required LSPCtrlBlock has already been
                 * accessed using this value in funciton "LdpHandleLblMapMsg"
                 */
                break;

            case LDP_HOP_COUNT_TLV:
                pu1HcTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case LDP_PATH_VECTOR_TLV:
                pu1PvTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            default:
                break;

        }
        pu1MsgPtr += u2TlvLen;
    }
    if (LCB_TRIG (pLspCtrlBlock) && (LCB_TRIG_TYPE (pLspCtrlBlock)
                                     == NEXT_HOP_CHNG_LSP_SET_UP))
    {
        gLdpNonMrgNhopChngFsm[NH_TRGCB_ST (LCB_TRIG
                                           (pLspCtrlBlock))]
            [LDP_NH_LSM_EVT_INT_LSP_UP] (LCB_TRIG (pLspCtrlBlock), NULL);
        return;
    }

    if (LCB_USSN (pLspCtrlBlock) != NULL)
    {
        u1LsrEgrOrIngr = LDP_FALSE;
    }
    if (LdpIsLoopFound (pu1HcTlv, pu1PvTlv, (&u2PvTlvLen), (&u1HopCount),
                        SSN_GET_INCRN_ID (LCB_DSSN (pLspCtrlBlock)),
                        LCB_DSSN (pLspCtrlBlock), u1LsrEgrOrIngr) == LDP_TRUE)
    {
        /* Label release message has to be sent with Status TLV (Loop detected status
         * * code)
         * * */
        pLspCtrlBlock->pDStrSession->u1StatusCode = LDP_STAT_TYPE_LOOP_DETECTED;

        /* NAK message to be sent downstream */
        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            /* Case of LSR being Ingress to the LSP */
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));
            LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
            return;
        }
        else
        {
            /* Case of LSR being in between the established LSP */

            if ((LCB_USSN (pLspCtrlBlock) != NULL) &&
                (SSN_GET_LBL_ALLOC_MODE (LCB_USSN (pLspCtrlBlock)) ==
                 LDP_ORDERED_MODE))
            {
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpSendLblWithdrawMsg (&LCB_FEC (pLspCtrlBlock),
                                       &LCB_ULBL (pLspCtrlBlock),
                                       LCB_USSN (pLspCtrlBlock));
                LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
                return;
            }
        }
    }
    else
    {
        /* No Loop detected in the received label mapping message */

        LDP_DBG2 (LDP_ADVT_MISC, "%s %d Loop NOT detected in LBL Map msg.\n",
                  __func__, __LINE__);

        /* Updating the HopCount rx in the Map Msg */
        LCB_HCOUNT (pLspCtrlBlock) = u1HopCount;

        pLspCtrlBlock->pu1PVTlv = pu1PvTlv;
        pLspCtrlBlock->u1PVCount = u2PvTlvLen;

        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            LDP_DBG2 (LDP_ADVT_MISC,
                      "%s %d: Triggered ctrl block is not NULL\n", __func__,
                      __LINE__);
            /* Case of LSR being the Ingress of the LSP */

            if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->u2LabelRet) ==
                LDP_LIBERAL_MODE)
            {

                LDP_DBG2 (LDP_ADVT_MISC, "%s %d: Liberal Mode\n", __func__,
                          __LINE__);

                LdpCopyAddr (&NextHopAddr.Addr, &pLspCtrlBlock->NextHopAddr,
                             pLspCtrlBlock->Fec.u2AddrFmly);
                NextHopAddr.u2AddrType = pLspCtrlBlock->Fec.u2AddrFmly;

                if (CfaUtilGetIfIndexFromMplsTnlIf
                    (pLspCtrlBlock->u4OutIfIndex, &u4L3VlanIf,
                     TRUE) != CFA_SUCCESS)
                {
                    LDP_DBG4 (LDP_ADVT_PRCS,
                              "%s : %d CfaUtilGetIfIndexFromMplsTnlIf() Failed pLspCtrlBlock->u4OutIfIndex=%d u4L3VlanIf=%d\n",
                              __FUNCTION__, __LINE__,
                              pLspCtrlBlock->u4OutIfIndex, u4L3VlanIf);
                }

                if ((LdpIsSsnForFec (&NextHopAddr,
                                     u4L3VlanIf,
                                     LCB_DSSN (pLspCtrlBlock))) == LDP_TRUE)
                {
                    LDP_DBG2 (LDP_ADVT_MISC, "%s %d: Creating FTN Entry\n",
                              __func__, __LINE__);
                    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE, MPLS_OPR_PUSH,
                                           pLspCtrlBlock, NULL);
                }
            }
            else
            {
                LDP_DBG2 (LDP_ADVT_MISC, "%s %d: Conservative Mode\n", __func__,
                          __LINE__);
                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE, MPLS_OPR_PUSH,
                                       pLspCtrlBlock, NULL);
            }

            LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;

            LDP_DBG2 (LDP_ADVT_MISC,
                      "%s: %d Downstream control Block is moved to ESTABLISHED state\n",
                      __func__, __LINE__);
            /* SNMP Trap can be generated to indicate the LSP is now ready. */
            return;

        }
        else if ((LCB_USSN (pLspCtrlBlock) != NULL) &&
                 (SSN_GET_LBL_ALLOC_MODE (LCB_USSN (pLspCtrlBlock))
                  == LDP_ORDERED_MODE))
        {
            LDP_DBG2 (LDP_ADVT_MISC, "%s %d: Ordered Mode\n", __func__,
                      __LINE__);

            u4SessIfIndex =
                LdpSessionGetIfIndex (LCB_USSN (pLspCtrlBlock),
                                      pLspCtrlBlock->Fec.u2AddrFmly);

            if (LdpGetLabel
                (SSN_GET_ENTITY (LCB_USSN (pLspCtrlBlock)), &Label,
                 u4SessIfIndex) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_ADVT_MEM, "ADVT: Alloc Label Failed - RspMap\n");
                LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                 LDP_STAT_TYPE_NO_LBL_RSRC, NULL);
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
                return;
            }
            else
            {
                LDP_DBG2 (LDP_ADVT_MISC, "%s %d: Creating ILM Entry\n",
                          __func__, __LINE__);

                MEMCPY (&LCB_ULBL (pLspCtrlBlock), &Label, sizeof (uLabel));
                LdpSendMplsMlibUpdate
                    (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP_PUSH,
                     pLspCtrlBlock, NULL);

                /* MLIB update being success */
                LdpSendLblMappingMsg (pLspCtrlBlock, u1HopCount,
                                      (pu1PvTlv + LDP_TLV_HDR_LEN),
                                      (UINT1) u2PvTlvLen);

                LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;

                /* For Intermediate LSR the unique cross connect Index
                 * is obtained.This is also used to map the insegment
                 * and the out segment in the LSR MIB.
                 */

                LDP_DBG2 (LDP_ADVT_MISC, "%s %d ILM Programmed in Hardware\n",
                          __func__, __LINE__);
                LdpGetCrossConnectIndex ((UINT4 *) &pLspCtrlBlock->
                                         u4CrossConnectIndex);
                return;
            }
        }
    }
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRspRel                                           */
/* Description   : This routine is called when label state machine is in     */
/*                 RESPONSE_AWAITED state and Label Rel. Message received.   */
/*                                                                           */
/*                 Reference - Section 3.1.5.2 - page 12 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRspRel (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pLdpSession = NULL;
    UINT4               u4SessIfIndex;

    /* added to supress warning unused variable pMsg */
    pMsg = (UINT1 *) pMsg;
    pLdpSession = LCB_USSN (pLspCtrlBlock);
    if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_INDEPENDENT_MODE)
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP,
                               pLspCtrlBlock, NULL);
        u4SessIfIndex =
            LdpSessionGetIfIndex (pLdpSession, pLspCtrlBlock->Fec.u2AddrFmly);
        LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &LCB_ULBL (pLspCtrlBlock),
                      u4SessIfIndex);
    }

    /* Label Abort message sent to the down stream LSR */
    LdpSendLblAbortReqMsg (pLspCtrlBlock);

    /* LSP control block is deleted */
    LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);

    return;
}

/*****************************************************************************/
/* Function Name : ldpNonMrgRspUpAbrt                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 RESPONSE_AWAITED state and Label Abort Message received.  */
/*                                                                           */
/*                 Reference - Section 3.1.5.2 - page 13 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRspUpAbort (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pLdpSession = NULL;

    pLdpSession = LCB_USSN (pLspCtrlBlock);
    /* Sending Abort Msg DStream and going to IDLE State */
    LdpSendLblAbortReqMsg (pLspCtrlBlock);
    LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_IDLE;

    if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_INDEPENDENT_MODE)
    {
        /* Going to IDLE State and waiting for
         *     - AbortNak or Map Msg from DStream
         *     - Label Rel Msg from UpStream
         */
        LCB_SUB_STATE (pLspCtrlBlock) = LDP_IDLE_NAKORMAP_REL;
    }
    else
    {
        /* For Ordered Mode, respond with a Notif Msg stating Label Req has been
         * Aborted. */
        /* Going to IDLE State and waiting for
         *     - AbortNak or Map Msg from DStream
         */
        LdpSendNotifMsg (pLdpSession, pMsg, LDP_FALSE,
                         LDP_STAT_TYPE_LBL_REQ_ABRTD, NULL);
        LCB_SUB_STATE (pLspCtrlBlock) = LDP_IDLE_NAKORMAP;
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRspDnNak                                         */
/* Description   : This routine is called when label state machine is in     */
/*                 RESPONSE_AWAITED state and Downstream sent notification.  */
/*                                                                           */
/*                 Reference - Section 3.1.5.2 - page 13 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRspDnNak (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1StatusType = NULL;
    /* added to supress warning unused variable pMsg */
    pMsg = (UINT1 *) pMsg;

    if (LCB_TRIG (pLspCtrlBlock) != NULL)
    {
        /* Case of LSR being the ingress of the LSP */
        if (LCB_TRIG_TYPE (pLspCtrlBlock) != NEXT_HOP_CHNG_LSP_SET_UP)
        {
            LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
        }
        else
        {
            LdpNonMrgNhopChngIntLspDown (LCB_TRIG (pLspCtrlBlock), NULL);
        }
    }
    else
    {
        pLdpSession = LCB_USSN (pLspCtrlBlock);
        if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_INDEPENDENT_MODE)
        {
            LdpSendLblWithdrawMsg (&LCB_FEC (pLspCtrlBlock),
                                   &LCB_ULBL (pLspCtrlBlock),
                                   LCB_USSN (pLspCtrlBlock));
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP,
                                   pLspCtrlBlock, NULL);
            LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_REL_AWT;

        }
        else if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_ORDERED_MODE)
        {
            pu1StatusType = pMsg;
            if (pLspCtrlBlock->pCrlspTnlInfo != NULL)
            {
                if (*pu1StatusType >= LDP_MAX_STATUS_TYPES)
                {
                    *pu1StatusType = (UINT1) (*pMsg - LDP_MAX_STATUS_TYPES);
                    LdpSendDiffServNotifMsg (LCB_USSN (pLspCtrlBlock), NULL,
                                             NULL, *pu1StatusType,
                                             pLspCtrlBlock);
                }
                else
                {
                    LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock),
                                          pLspCtrlBlock, *pu1StatusType, NULL);
                }
            }
            else
            {
                LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), NULL, LDP_FALSE,
                                 *pu1StatusType, pLspCtrlBlock);
            }
            LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRspUpLost                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 RESPONSE_AWAITED state and Upstream is lost.              */
/*                                                                           */
/*                 Reference - Section 3.1.5.2 - page 14 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRspUpLost (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pLdpSession = NULL;
    UINT4               u4SessIfIndex;

    /* added to supress warning unused variable pMsg */
    pMsg = (UINT1 *) pMsg;

    pLdpSession = LCB_USSN (pLspCtrlBlock);
    if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_INDEPENDENT_MODE)
    {
        u4SessIfIndex =
            LdpSessionGetIfIndex (pLdpSession, pLspCtrlBlock->Fec.u2AddrFmly);
        LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &LCB_ULBL (pLspCtrlBlock),
                      u4SessIfIndex);
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP,
                               pLspCtrlBlock, NULL);
    }

    /* Label Abort message is sent to the downstream LSR */
    LdpSendLblAbortReqMsg (pLspCtrlBlock);
    LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRspDnLost                                         */
/* Description   : This routine is called when label state machine is in     */
/*                 RESPONSE_AWAITED state and Downstream is lost.            */
/*                                                                           */
/*                 Reference - Section 3.1.5.2 - page 14 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRspDnLost (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tTMO_SLL           *pLcbList = NULL;
    tLdpSession        *pLdpSession = NULL;

    /* added to supress warning unused variable pMsg */
    pMsg = (UINT1 *) pMsg;
    if (LCB_TRIG (pLspCtrlBlock) != NULL)
    {
        if (LCB_TRIG_TYPE (pLspCtrlBlock) != NEXT_HOP_CHNG_LSP_SET_UP)
        {
            /** Delete both the control blocks Orignal Downstream Ctrl block 
                and TrigCtrlBlock**/
            LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
            return;
        }
        else
        {
            LdpNonMrgNhopChngIntLspDown (LCB_TRIG (pLspCtrlBlock), NULL);
            return;
        }
    }

    /* Case of LSR being the intermediate LSR of the LSP */

    pLdpSession = LCB_USSN (pLspCtrlBlock);
    if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_INDEPENDENT_MODE)
    {
        LdpSendLblWithdrawMsg (&LCB_FEC (pLspCtrlBlock),
                               &LCB_ULBL (pLspCtrlBlock),
                               LCB_USSN (pLspCtrlBlock));
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP,
                               pLspCtrlBlock, NULL);

        /* 
         * Delete this LSP control block from downstream session's 
         * downstream LSP Control block list
         */
        pLcbList = &SSN_DLCB (LCB_DSSN (pLspCtrlBlock));
        TMO_SLL_Delete (pLcbList, LCB_TO_SLL (pLspCtrlBlock));
        LCB_DSSN (pLspCtrlBlock) = NULL;
        TMO_SLL_Init_Node ((tTMO_SLL_NODE *)
                           (&(pLspCtrlBlock->NextDSLspCtrlBlk)));

        LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_REL_AWT;

    }
    else if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_ORDERED_MODE)
    {
        if (pLspCtrlBlock->pCrlspTnlInfo != NULL)
        {
            LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                                  LDP_STAT_TYPE_NO_ROUTE, NULL);
        }
        else
        {
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), NULL, LDP_FALSE,
                             LDP_STAT_TYPE_NO_ROUTE, pLspCtrlBlock);
        }
        LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRspIntDestroy                                    */
/* Description   : This routine is called when label state machine is in     */
/*                 RESPONSE_AWAITED state and Internal Destroy event is rcvd.*/
/*                                                                           */
/*                 Reference - Section 3.1.5.2 - page 15 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRspIntDestroy (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    /* added to supress warning unused variable pMsg */
    pMsg = (UINT1 *) pMsg;
    if (LCB_DSSN (pLspCtrlBlock) == NULL)
    {
        LdpSendLblAbortReqMsg (pLspCtrlBlock);
    }
    LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRspIntNewNH                                      */
/* Description   : This routine is called when label state machine is in     */
/*                 RESPONSE_AWAITED state and next hop changed.              */
/*                                                                           */
/*                 Reference - Section 3.1.5.2 - page 15 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRspIntNewNH (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pDssn = NULL;
    tLdpRouteEntryInfo *pRtInfo = NULL;
    UINT2               u2IncarnId = LDP_ZERO;
    tGenU4Addr          NextHopAddr;

    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));

    pRtInfo = (tLdpRouteEntryInfo *) (VOID *) pMsg;

    LdpCopyAddr (&NextHopAddr.Addr, &pRtInfo->NextHop, pRtInfo->u2AddrType);
    NextHopAddr.u2AddrType = pRtInfo->u2AddrType;

    /** As the nextHop is getting changed, so the new downstream session must be
        different from the existing one **/

    if (LdpGetPeerSession (&NextHopAddr, pRtInfo->u4RtIfIndx, &pDssn,
                           u2IncarnId) == LDP_FAILURE)
    {
        /*
         * Notification message sent to upstream that no route is present
         * and the LSP Control block is released.
         */

            /*** Send Abort message is as per state machine **/
        /* Send Label Abort message to old down stream */
        LdpSendLblAbortReqMsg (pLspCtrlBlock);
        TMO_SLL_Delete (&SSN_DLCB (LCB_DSSN (pLspCtrlBlock)),
                        LCB_TO_SLL (pLspCtrlBlock));
        u2IncarnId = SSN_GET_INCRN_ID (pLspCtrlBlock->pDStrSession);
        LCB_DSSN (pLspCtrlBlock) = NULL;
        if (LCB_USSN (pLspCtrlBlock) != NULL)
        {
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), NULL, LDP_FALSE,
                             LDP_STAT_TYPE_NO_ROUTE, pLspCtrlBlock);
        }
        LdpNonMrgResAwtDeleteLspCtrlBlk (pLspCtrlBlock);
        return;
    }

   /** As per RFC, if the control block is release awaited state then send the 
       Label Abort on old session and label map on new session.***/

   /** There can be a scenario, both the sessions same, this could be error scenario**/
   /** One scenario is : SW1,SW2,SW3,SW4 are connected in a ring, S5 connected to S2,
       S6 connected to S2 and SW4. There is a loopback IP X on SW4. Reachability of X on
       SW1 is via SW3, SW5 is via SW2, SW2,SW6 and SW3 are one hop away. Now SW4 goes down.
       SW2 have events Route add via SW1,Route Delete via SW1(converted to nextHopChange) then 
       Delete for X. SW6 sends label req. for X and SW2 forwardes the request to SW1.
       SW6 sends the label abort and Label req for X towards SW2. 
       At this time the GenerateRt API gets hit due to timer expiry. NH-Change is handled for
       FEC X, this API gets triggered. At this time two control exists. One for route addition
       for X at SW2 and other label req. from SW6. These two control blocks are on session
       towards A. The new session comes out via NxtHop change is also SW1. The same session is 
       creating problem. In the below code we are deleting the control block from one session
       and adding to other session. Addition and deletion on same session will create loop.
       Because this API is gets called via GenerateRt API and under downstream session control
       block scanning loop **/

    if (LCB_DSSN (pLspCtrlBlock) != pDssn)
    {
        /* Send Label Abort message to old down stream */
        LdpSendLblAbortReqMsg (pLspCtrlBlock);
        TMO_SLL_Delete (&SSN_DLCB (LCB_DSSN (pLspCtrlBlock)),
                        LCB_TO_SLL (pLspCtrlBlock));
        u2IncarnId = SSN_GET_INCRN_ID (pLspCtrlBlock->pDStrSession);
        LCB_DSSN (pLspCtrlBlock) = NULL;

        LdpCopyAddr (&pLspCtrlBlock->NextHopAddr, &pRtInfo->NextHop,
                     pRtInfo->u2AddrType);
        pLspCtrlBlock->u2AddrType = pRtInfo->u2AddrType;

        pLspCtrlBlock->pDStrSession = pDssn;
        TMO_SLL_Add ((tTMO_SLL *) (&(SSN_DLCB (pDssn))),
                     (tTMO_SLL_NODE *) (&(pLspCtrlBlock->NextDSLspCtrlBlk)));

        /* Send Label Request to the new down stream */
        LdpSendLblReqMsg (pLspCtrlBlock, 0, NULL, 0, NULL, 0);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRspPreEmpTnl                                     */
/* Description   : This routine is called when label state machine is in     */
/*                 RESPONSE_AWAITED state and preempted by any tunnel.       */
/*                 Reference - Section 4.4   draft-ietf-mpls-crldp-04.txt    */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRspPreEmpTnl (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    /* added to supress warning unused variable pMsg */
    pMsg = (UINT1 *) pMsg;
    /* Send Label Abort message to down stream and a Notif Msg to
     * Upstream Session. 
     */
    LdpSendLblAbortReqMsg (pLspCtrlBlock);
    LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_IDLE;
    if (LCB_USSN (pLspCtrlBlock) != NULL)
    {
        LdpSendCrLspNotifMsg (LCB_USSN (pLspCtrlBlock), pLspCtrlBlock,
                              LDP_STAT_TYPE_CRLSP_PREEMPTED, NULL);
    }
    LCB_SUB_STATE (pLspCtrlBlock) = LDP_IDLE_NAKORMAP;
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgEstLdpMap                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and Label Mapping Message received.     */
/*                                                                           */
/*                 Reference - Section 3.1.5.3 - page 15 & 16  - draft-ietf- */
/*                 mpls-ldp-state-01.txt                                     */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgEstLdpMap (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    UINT1               u1HopCount = LDP_INVALID_HOP_COUNT;
    UINT2               u2PvTlvLen = 0;
    UINT1              *pu1PvTlv = NULL;
    UINT1              *pu1HcTlv = NULL;
    UINT1              *pu1MsgPtr = pMsg;
    UINT2               u2MsgLen = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLen = 0;
    uLabel              Label;
    UINT4               u4LabelVal;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1               u1LsrEgrOrIngr = LDP_TRUE;
    tGenU4Addr          NextHopAddr;
    UINT4               u4L3VlanIf = 0;

    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));

    MEMSET (&Label, 0, sizeof (uLabel));

    /* The message length is obtained first */
    pu1MsgPtr += LDP_OFF_MSG_LEN;
    LDP_EXT_2_BYTES (pu1MsgPtr, u2MsgLen);

    /* Msg pointer now points to the message id, the message id is moved */
    pu1MsgPtr += LDP_OFF_MSG_ID;

    /* Msg pointer now points to the first TLV in the message */
    while (pu1MsgPtr < pMsg + u2MsgLen + LDP_OFF_MSG_ID)
    {
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvType);
        LDP_EXT_2_BYTES (pu1MsgPtr, u2TlvLen);

        /* Check for bad TLV Length performed here */
        if ((pu1MsgPtr + u2TlvLen) > (pMsg + u2MsgLen + LDP_OFF_MSG_ID))
        {
            /* Notification message with "LDP_STAT_TYPE_BAD_TLV_LEN"  sent. */
            LdpSendNotifMsg (LCB_USSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                             LDP_STAT_TYPE_BAD_TLV_LEN, NULL);
            LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
            return;
        }
        pLdpEntity = SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock));
        switch (u2TlvType)
        {
            case LDP_GEN_LABEL_TLV:
                if ((LDP_LBL_SPACE_TYPE (pLdpEntity)) != LDP_PER_PLATFORM)
                {
                    /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                       sent. */
                    LDP_DBG1 (LDP_ADVT_MISC,
                              "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                              LDP_STAT_UNKNOWN_TLV);
                    LdpSendNotifMsg (LCB_DSSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                     LDP_STAT_UNKNOWN_TLV, NULL);
                    LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
                    return;
                }

                LDP_GET_4_BYTES (pu1MsgPtr, u4LabelVal);
                Label.u4GenLbl = u4LabelVal;
                if (LCB_DLBL (pLspCtrlBlock).u4GenLbl != LDP_INVALID_LABEL)
                {
                    if (LdpCompLabels (LCB_DLBL (pLspCtrlBlock),
                                       u4LabelVal, SSN_GET_LBL_TYPE (LCB_DSSN
                                                                     (pLspCtrlBlock)))
                        != LDP_EQUAL)
                    {
                        return;
                    }
                }
                else
                {
                    MEMCPY (&LCB_DLBL (pLspCtrlBlock), &Label, sizeof (uLabel));
                }
                break;

            case LDP_ATM_LABEL_TLV:
                if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
                {
                    /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                       sent. */
                    LDP_DBG1 (LDP_ADVT_MISC,
                              "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                              LDP_STAT_UNKNOWN_TLV);
                    LdpSendNotifMsg (LCB_DSSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                     LDP_STAT_UNKNOWN_TLV, NULL);
                    LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
                    return;
                }
                LDP_GET_4_BYTES (pu1MsgPtr, u4LabelVal);
                Label.AtmLbl.u2Vpi =
                    (UINT2) ((u4LabelVal & MPLS_VPI_MASK) >> 16);
                if ((Label.AtmLbl.u2Vpi & MPLS_V_BITS_MASK) != LDP_ATM_V_BITS)
                {
                    /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                       sent. */
                    LDP_DBG1 (LDP_ADVT_MISC,
                              "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                              LDP_STAT_UNKNOWN_TLV);
                    LdpSendNotifMsg (LCB_DSSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                     LDP_STAT_UNKNOWN_TLV, NULL);
                    LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
                    return;

                }
                Label.AtmLbl.u2Vci = (UINT2) (u4LabelVal & MPLS_VCI_MASK);
                if (ldpCheckLblInLblGroup (pLdpEntity->u2LabelRangeId,
                                           Label.AtmLbl.u2Vpi,
                                           Label.AtmLbl.u2Vci) != LDP_TRUE)
                {
                    /* Notif message with "LDP_STAT_TYPE_UNKNOWN_TLV" 
                       sent. */
                    LDP_DBG1 (LDP_ADVT_MISC,
                              "ADVT: Notif Msg Snt - Unkwn TLV - %x \n",
                              LDP_STAT_UNKNOWN_TLV);
                    LdpSendNotifMsg (LCB_DSSN (pLspCtrlBlock), pMsg, LDP_FALSE,
                                     LDP_STAT_UNKNOWN_TLV, NULL);
                    LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
                    return;
                }

                if (LCB_DLBL (pLspCtrlBlock).u4GenLbl != LDP_INVALID_LABEL)
                {
                    if (LdpCompLabels (LCB_DLBL (pLspCtrlBlock), u4LabelVal,
                                       SSN_GET_LBL_TYPE (LCB_DSSN
                                                         (pLspCtrlBlock))) !=
                        LDP_EQUAL)
                    {
                        return;
                    }
                }
                else
                {
                    MEMCPY (&LCB_DLBL (pLspCtrlBlock), &Label, sizeof (uLabel));
                }
                break;

            case LDP_FEC_TLV:
            case LDP_LBL_REQ_MSGID_TLV:
                /* 
                 * FEC values is not required to be extracted as it is available
                 * in the LSPCtrlBlock. The Label Request message id is not 
                 * required, as the required LSPCtrlBlock has already been
                 * accessed using this value in funciton "LdpHandleLblMapMsg"
                 */
                break;

            case LDP_HOP_COUNT_TLV:
                pu1HcTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            case LDP_PATH_VECTOR_TLV:
                pu1PvTlv = pu1MsgPtr - LDP_TLV_HDR_LEN;
                break;

            default:
                break;

        }
        pu1MsgPtr += u2TlvLen;
    }
    if (LCB_TRIG (pLspCtrlBlock) && (LCB_TRIG_TYPE (pLspCtrlBlock)
                                     == NEXT_HOP_CHNG_LSP_SET_UP))
    {
        if (NH_TRGCB_ST (LCB_TRIG (pLspCtrlBlock)) <
            LDP_MAX_NON_MRG_NHOP_CHNG_STATES)
        {
            gLdpNonMrgNhopChngFsm[NH_TRGCB_ST (LCB_TRIG
                                               (pLspCtrlBlock))]
                [LDP_NH_LSM_EVT_INT_LSP_UP] (LCB_TRIG (pLspCtrlBlock), NULL);
        }
        return;
    }

    /* In Intermdediate the mapping message must be forwarded either when there 
     * is a path vector TLV (which will be different either in independent or
     * ordered for which it naturally arrives because of next hop change) 
     * OR the hop count is different from previous stored HOP count 
     */

    u1LsrEgrOrIngr = ((pLspCtrlBlock->pUStrSession) == NULL)
        ? LDP_TRUE : LDP_FALSE;
    if (LdpIsLoopFound (pu1HcTlv, pu1PvTlv, (&u2PvTlvLen), (&u1HopCount),
                        SSN_GET_INCRN_ID (LCB_DSSN (pLspCtrlBlock)),
                        LCB_DSSN (pLspCtrlBlock), u1LsrEgrOrIngr) == LDP_TRUE)
    {
        /* NAK message to be sent downstream */
        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            /* Label release message has to be sent with Status TLV (Loop detected status
             * * code)
             * * */
            pLspCtrlBlock->pDStrSession->u1StatusCode =
                LDP_STAT_TYPE_LOOP_DETECTED;

            /* Case of LSR being Ingress to the LSP */
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));
            LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
            return;
        }
        else
        {
            /* Case of LSR being in between the established LSP */

            if (LCB_USSN (pLspCtrlBlock) == NULL)
            {
                return;
            }
            if (SSN_GET_LBL_ALLOC_MODE (LCB_USSN (pLspCtrlBlock)) ==
                LDP_ORDERED_MODE)
            {
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpSendLblWithdrawMsg (&LCB_FEC (pLspCtrlBlock),
                                       &LCB_ULBL (pLspCtrlBlock),
                                       LCB_USSN (pLspCtrlBlock));
                LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
                return;
            }
            else if (SSN_GET_LBL_ALLOC_MODE (LCB_USSN (pLspCtrlBlock)) ==
                     LDP_INDEPENDENT_MODE)
            {
                LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                                  &LCB_DLBL (pLspCtrlBlock),
                                  LCB_DSSN (pLspCtrlBlock));
                LdpSendLblWithdrawMsg (&LCB_FEC (pLspCtrlBlock),
                                       &LCB_ULBL (pLspCtrlBlock),
                                       LCB_USSN (pLspCtrlBlock));
                LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_REL_AWT;
                return;
            }
        }
    }
    else
    {
        /* No Loop detected in the received label mapping message */

        /* In Intermdediate the mapping message must be forwarded either 
         * when there is a path vector TLV (which will be different either
         * in independent or ordered for which it naturally arrives because
         * of next hop change) OR the hop count is different from previous
         * stored HOP count
         */
#if 0
        if (LdpCheckMapAttributes (pu1PvTlv, u1HopCount, pLspCtrlBlock,
                                   NULL) == LDP_FALSE)
        {
            /* Since there are no attributes change there is no generation 
             * of Map message 
             */
            return;
        }
#endif

        /* Updating the HopCount rx in the Map Msg */
        LCB_HCOUNT (pLspCtrlBlock) = u1HopCount;
        pLspCtrlBlock->pu1PVTlv = pu1PvTlv;
        pLspCtrlBlock->u1PVCount = u2PvTlvLen;

        if (LCB_TRIG (pLspCtrlBlock) != NULL)
        {
            /* Case of LSR being the Ingress of the LSP */

            if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->u2LabelRet)
                == LDP_LIBERAL_MODE)
            {

                LdpCopyAddr (&NextHopAddr.Addr, &pLspCtrlBlock->NextHopAddr,
                             pLspCtrlBlock->Fec.u2AddrFmly);
                NextHopAddr.u2AddrType = pLspCtrlBlock->Fec.u2AddrFmly;

                if (CfaUtilGetIfIndexFromMplsTnlIf
                    (pLspCtrlBlock->u4OutIfIndex, &u4L3VlanIf,
                     TRUE) != CFA_SUCCESS)
                {
                    LDP_DBG4 (LDP_ADVT_PRCS,
                              "%s : %d CfaUtilGetIfIndexFromMplsTnlIf() Failed pLspCtrlBlock->u4OutIfIndex=%d u4L3VlanIf=%d\n",
                              __FUNCTION__, __LINE__,
                              pLspCtrlBlock->u4OutIfIndex, u4L3VlanIf);
                }

                if ((LdpIsSsnForFec (&NextHopAddr,
                                     u4L3VlanIf,
                                     LCB_DSSN (pLspCtrlBlock))) == LDP_TRUE)
                {
                    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_MODIFY,
                                           MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
                }
            }
            else
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_MODIFY,
                                       MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
            }
            LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;
            /* SNMP Trap can be generated to indicate the 
             * LSP is now ready. */
            return;

        }
        else
        {
            LdpSendMplsMlibUpdate
                (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP, pLspCtrlBlock, NULL);
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP_PUSH,
                                   pLspCtrlBlock, NULL);
            LdpSendLblMappingMsg (pLspCtrlBlock, u1HopCount,
                                  (pu1PvTlv + LDP_TLV_HDR_LEN),
                                  (UINT1) u2PvTlvLen);

            if (LCB_USSN (pLspCtrlBlock) != NULL)
            {
                /* For Intermediate LSR the unique cross connect Index
                 * is obtained.This is also used to map the insegment
                 * and the out segment in the LSR MIB.
                 */
                LdpGetCrossConnectIndex ((UINT4 *)
                                         &pLspCtrlBlock->u4CrossConnectIndex);
            }
            LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgEstLdpRel                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and Label Release Message received.     */
/*                                                                           */
/*                 Reference - Section 3.1.5.3 - page 16 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpNonMrgEstLdpRel (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pLdpSession = NULL;
    UINT4               u4SessIfIndex;
    pLdpSession = LCB_USSN (pLspCtrlBlock);
    pMsg = (UINT1 *) pMsg;

    u4SessIfIndex =
        LdpSessionGetIfIndex (pLdpSession, pLspCtrlBlock->Fec.u2AddrFmly);
    LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &LCB_ULBL (pLspCtrlBlock),
                  u4SessIfIndex);

    if (LCB_DSSN (pLspCtrlBlock) == NULL)
    {
        /* In case of the LSR being egress */
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP,
                               pLspCtrlBlock, NULL);
    }
    else if (LCB_DSSN (pLspCtrlBlock) != NULL)
    {
        /* In case of the LSR being non egress */
        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock), &LCB_DLBL (pLspCtrlBlock),
                          LCB_DSSN (pLspCtrlBlock));
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                               pLspCtrlBlock, NULL);
    }
    /* Send Internal Destroy to Next hop Trig Block */
    if (LCB_NHOP (pLspCtrlBlock) != NULL)
    {
        gLdpNonMrgNhopChngFsm[LCB_NHOP_ST (pLspCtrlBlock)]
            [LDP_NH_LSM_EVT_INT_DESTROY] (LCB_NHOP (pLspCtrlBlock), NULL);
    }
    LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgEstLdpWth                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and Label Withdraw Message received.    */
/*                                                                           */
/*                 Reference - Section 3.1.5.3 - page 16 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgEstLdpWth (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tGenU4Addr          NextHopAddr;
    UINT4               u4L3VlanIf = 0;

    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));

    pMsg = (UINT1 *) pMsg;

    if (LCB_DSSN (pLspCtrlBlock) != NULL)
    {
        /* Label Release sent to the Downstream LSR */
        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock), &LCB_DLBL (pLspCtrlBlock),
                          LCB_DSSN (pLspCtrlBlock));
    }

    if (LCB_TRIG (pLspCtrlBlock) != NULL)
    {
        /* In case of Down Stream OnDemand and Independent control mode, After
         * Receiving Label Withdraw Message, it should send Label Request Message
         * after responding with Label Release Message.
         * RFC: 5036 Section A.1.5
         */
        if ((LCB_DSSN (pLspCtrlBlock) != NULL) &&
            ((SSN_ADVTYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_DSTR_ON_DEMAND) &&
             (SSN_GET_LBL_ALLOC_MODE (LCB_DSSN (pLspCtrlBlock)) ==
              LDP_INDEPENDENT_MODE) &&
             (LCB_TRIG_TYPE (pLspCtrlBlock) != NEXT_HOP_CHNG_LSP_SET_UP)))
        {
            LdpNonMrgIdIntSetup (pLspCtrlBlock, (UINT1 *) NULL);
            return;
        }

        if (LCB_TRIG_TYPE (pLspCtrlBlock) != NEXT_HOP_CHNG_LSP_SET_UP)
        {
            /* Case the LSR is the ingress of the LSP */
            if ((LCB_DSSN (pLspCtrlBlock) != NULL) &&
                ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->u2LabelRet)
                 == LDP_LIBERAL_MODE))
            {

                LdpCopyAddr (&NextHopAddr.Addr, &pLspCtrlBlock->NextHopAddr,
                             pLspCtrlBlock->Fec.u2AddrFmly);
                NextHopAddr.u2AddrType = pLspCtrlBlock->Fec.u2AddrFmly;

                if (CfaUtilGetIfIndexFromMplsTnlIf
                    (pLspCtrlBlock->u4OutIfIndex, &u4L3VlanIf,
                     TRUE) != CFA_SUCCESS)
                {
                    LDP_DBG4 (LDP_ADVT_PRCS,
                              "%s : %d CfaUtilGetIfIndexFromMplsTnlIf() Failed pLspCtrlBlock->u4OutIfIndex=%d u4L3VlanIf=%d\n",
                              __FUNCTION__, __LINE__,
                              pLspCtrlBlock->u4OutIfIndex, u4L3VlanIf);
                }

                if ((LdpIsSsnForFec (&NextHopAddr,
                                     u4L3VlanIf,
                                     LCB_DSSN (pLspCtrlBlock))) == LDP_TRUE)
                {
                    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                           MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
                }
            }
            else
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                       MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
            }
            LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
            return;
        }
    }
    else
    {
        /* Case when the LSR is an intermeidate LSR in the LSP */
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                               pLspCtrlBlock, NULL);
        LdpSendLblWithdrawMsg (&LCB_FEC (pLspCtrlBlock),
                               &LCB_ULBL (pLspCtrlBlock),
                               LCB_USSN (pLspCtrlBlock));
        LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_REL_AWT;
    }
    if (LCB_NHOP (pLspCtrlBlock) != NULL)
    {
        gLdpNonMrgNhopChngFsm[LCB_NHOP_ST (pLspCtrlBlock)]
            [LDP_NH_LSM_EVT_INT_DESTROY] (LCB_NHOP (pLspCtrlBlock), NULL);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgEstUpAbort                                       */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and Upstream Label Abort received.      */
/*                                                                           */
/*                 Reference - Section 3.5.9.1 - draft-ietf-mpls-ldp-06.txt  */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgEstUpAbort (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    pLspCtrlBlock = (tLspCtrlBlock *) pLspCtrlBlock;
    pMsg = (UINT1 *) pMsg;
    /* 
     * If an LSR receives a Label Abort Request Message after it has 
     * responded to the Label Request in question with a Label Mapping 
     * message, it ignores the abort request. 
     */
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgEstUpLost                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and Upstream is lost.                   */
/*                                                                           */
/*                 Reference - Section 3.1.5.3 - page 17 & 18 - draft-ietf-  */
/*                 npls-ldp-state-01.txt                                     */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgEstUpLost (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pLdpSession = NULL;
    UINT4               u4SessIfIndex;
    pLdpSession = LCB_USSN (pLspCtrlBlock);
    pMsg = (UINT1 *) pMsg;

    if (LCB_DSSN (pLspCtrlBlock) == NULL)
    {
        /* Case LSR Being Egress  - no down stream session */

        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP,
                               pLspCtrlBlock, NULL);

        u4SessIfIndex =
            LdpSessionGetIfIndex (pLdpSession, pLspCtrlBlock->Fec.u2AddrFmly);
        LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &LCB_ULBL (pLspCtrlBlock),
                      u4SessIfIndex);
    }
    else if (LCB_DSSN (pLspCtrlBlock) != NULL)
    {
        /* Case LSR Being non-Egress  - down stream session present */

        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                               pLspCtrlBlock, NULL);

        u4SessIfIndex =
            LdpSessionGetIfIndex (pLdpSession, pLspCtrlBlock->Fec.u2AddrFmly);
        LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &LCB_ULBL (pLspCtrlBlock),
                      u4SessIfIndex);

        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock), &LCB_DLBL (pLspCtrlBlock),
                          LCB_DSSN (pLspCtrlBlock));
    }
    if (LCB_NHOP (pLspCtrlBlock) != NULL)
    {
        gLdpNonMrgNhopChngFsm[LCB_NHOP_ST (pLspCtrlBlock)]
            [LDP_NH_LSM_EVT_INT_DESTROY] (LCB_NHOP (pLspCtrlBlock), NULL);
    }
    LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgEstDnLost                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and Downstream is lost.                 */
/*                                                                           */
/*                 Reference - Section 3.1.5.3 - page 18 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgEstDnLost (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tTMO_SLL           *pLcbList = NULL;
    tGenU4Addr          NextHopAddr;
    UINT4               u4L3VlanIf = 0;
    UINT4               u4TnlIfIndex = 0;

    MEMSET (&NextHopAddr, LDP_ZERO, sizeof (tGenU4Addr));

    pMsg = (UINT1 *) pMsg;

    if (LCB_TRIG (pLspCtrlBlock) != NULL)
    {
        if (LCB_TRIG_TYPE (pLspCtrlBlock) != NEXT_HOP_CHNG_LSP_SET_UP)
        {
            /* Case of the LSR being the ingress of the LSP */
            if ((SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->u2LabelRet)
                == LDP_LIBERAL_MODE)
            {

                LdpCopyAddr (&NextHopAddr.Addr, &pLspCtrlBlock->NextHopAddr,
                             pLspCtrlBlock->Fec.u2AddrFmly);
                NextHopAddr.u2AddrType = pLspCtrlBlock->Fec.u2AddrFmly;

                if (CfaUtilGetIfIndexFromMplsTnlIf
                    (pLspCtrlBlock->u4OutIfIndex, &u4L3VlanIf,
                     TRUE) != CFA_SUCCESS)
                {
                    LDP_DBG4 (LDP_ADVT_PRCS,
                              "%s : %d CfaUtilGetIfIndexFromMplsTnlIf() Failed pLspCtrlBlock->u4OutIfIndex=%d u4L3VlanIf=%d\n",
                              __FUNCTION__, __LINE__,
                              pLspCtrlBlock->u4OutIfIndex, u4L3VlanIf);
                }

                if ((LdpIsSsnForFec (&NextHopAddr,
                                     u4L3VlanIf,
                                     LCB_DSSN (pLspCtrlBlock))) == LDP_TRUE)
                {
                    LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                           MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
                }
            }
            else
            {
                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                       MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
            }
            /** Delete both the control blocks Orignal Downstream Ctrl block
              and TrigCtrlBlock**/
            LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);

        }
    }
    else
    {
        /* Case of the LSR being the intermediate LSR of the LSP */

        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                               pLspCtrlBlock, NULL);

        /*
         * Delete this LSP control block from downstream session's 
         * downstream LSP Control block list
         */

        pLcbList = &SSN_DLCB (LCB_DSSN (pLspCtrlBlock));
        TMO_SLL_Delete (pLcbList, LCB_TO_SLL (pLspCtrlBlock));
        LCB_DSSN (pLspCtrlBlock) = NULL;
        TMO_SLL_Init_Node ((tTMO_SLL_NODE *)
                           (&(pLspCtrlBlock->NextDSLspCtrlBlk)));
        LCB_DLBL (pLspCtrlBlock).u4GenLbl = LDP_INVALID_LABEL;
        if ((pLspCtrlBlock->u4OutIfIndex != 0) &&
            (pLspCtrlBlock->u1IsLspUsedByL2VPN == FALSE))
        {
            u4TnlIfIndex = pLspCtrlBlock->u4OutIfIndex;

            if (CfaUtilGetIfIndexFromMplsTnlIf (u4TnlIfIndex, &u4L3VlanIf, TRUE)
                != CFA_FAILURE)
            {

                if (CfaIfmDeleteStackMplsTunnelInterface
                    (u4L3VlanIf, pLspCtrlBlock->u4OutIfIndex) == CFA_FAILURE)
                {
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "Protected MPLS Tunnel IF %d for FEC %x deletion "
                              "failed\n",
                              u4TnlIfIndex,
                              LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));
                }
            }
            pLspCtrlBlock->u4OutIfIndex = LDP_ZERO;
        }

        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP,
                               pLspCtrlBlock, NULL);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgEstIntDestroy                                    */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and Internal Destroy event received.    */
/*                                                                           */
/*                 Reference - Section 3.1.5.3 - page 18,19 - draft-ietf-    */
/*                 mpls-ldp-state-01.txt                                     */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgEstIntDestroy (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    pMsg = (UINT1 *) pMsg;
    /* Case the LSR is an ingress and no upstream peer present */
    if (LCB_USSN (pLspCtrlBlock) == NULL)
    {
        if (LCB_TNLINFO (pLspCtrlBlock) == NULL)
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_DELETE,
                                   MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
        }
        else
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_TNL_DELETE,
                                   MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
        }
    }
    else if (LCB_USSN (pLspCtrlBlock) != NULL)
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                               pLspCtrlBlock, NULL);
    }

    if (LCB_DSSN (pLspCtrlBlock) != NULL)
    {
        /* Send Label Release Message if the LspCtrlBlock is created
         * in response to STATIC FEC creation OR
         * if the label rentention mode is conservative */
        if ((pLspCtrlBlock->u1IsStaticLsp == LDP_TRUE) ||
            (SSN_GET_ENTITY (LCB_DSSN (pLspCtrlBlock))->u2LabelRet)
            != LDP_LIBERAL_MODE)
        {
            LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock),
                              &LCB_DLBL (pLspCtrlBlock),
                              LCB_DSSN (pLspCtrlBlock));
        }
        else
        {
            /* No Need to Send Label Release Message and delete corresponding 
             * LSP control block if it is liberal mode */
            return;
        }
    }
    LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);

    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgEstIntXCon                                       */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and Internal Cross Connect event rcvd.  */
/*                                                                           */
/*                 Reference - Section 3.1.5.3 - page 19 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgEstIntXCon (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    pMsg = (UINT1 *) pMsg;
    pLspCtrlBlock = (tLspCtrlBlock *) pLspCtrlBlock;
    /*
     * Cross connect event will be initiated via configuration.
     * The set of parameters that are passed as part of this event needs
     * determined.
     *
     * The MPLS-FM's MLIB needs to be updated for the new ILM created.
     * Label mapping message must be sent to the upstream LSR - if needed
     * with any changed attributes.
     *
     * Currently this routine is dummy in nature. This is to be suitably
     * filled subsequently.
     */
    return;

}

/*****************************************************************************/
/* Function Name : LdpNonMrgEstIntNewNH                                      */
/* Description   : This routine is called when label state machine is in     */
/*                 ESTABLISHED state and next hop changed.                   */
/*                                                                           */
/*                 Reference - Section 3.1.5.3 - page 19 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgEstIntNewNH (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLspTrigCtrlBlock  *pNHTrggCtrlBlk = NULL;
    if (LCB_NHOP (pLspCtrlBlock) != NULL)
    {
        gLdpNonMrgNhopChngFsm[LCB_NHOP_ST (pLspCtrlBlock)]
            [LDP_NH_LSM_EVT_INT_NEW_NH] (LCB_NHOP (pLspCtrlBlock), pMsg);
    }
    else
    {
        /* Create the Next Hop Trigg Block */

        pNHTrggCtrlBlk = (tLspTrigCtrlBlock *)
            MemAllocMemBlk (LDP_TRIG_POOL_ID);

        LDP_DBG3 (LDP_ADVT_MEM,
                  "ADVT: %s: %d Memory allocated for Trig Control Block pNHTrggCtrlBlk=0x%x\n",
                  __FUNCTION__, __LINE__, pNHTrggCtrlBlk);

        if (pNHTrggCtrlBlk == NULL)
        {
            LDP_DBG (LDP_ADVT_MEM,
                     "ADVT: MemAlloc Failed for NHCB - EstIntNewNH\n");
            return;
        }
        MEMSET (pNHTrggCtrlBlk, 0, sizeof (tLspTrigCtrlBlock));
        pLspCtrlBlock->pNHCtrlBlk = pNHTrggCtrlBlk;

        /** Initialize the NextHop control block **/
        pNHTrggCtrlBlk->pOrgLspCtrlBlk = pLspCtrlBlock;
        pNHTrggCtrlBlk->pNewNhLspCtrlBlk = NULL;
        pNHTrggCtrlBlk->u1NhLspState = LDP_NH_LSM_ST_IDLE;
        pNHTrggCtrlBlk->u1TrgType = NEXT_HOP_CHNG_LSP_SET_UP;
        NH_TRGCB_DSESSION (pNHTrggCtrlBlk) = LCB_DSSN (pLspCtrlBlock);
        NH_TRGCB_USESSION (pNHTrggCtrlBlk) = LCB_USSN (pLspCtrlBlock);
        NH_TRGCB_ULABEL (pNHTrggCtrlBlk) = LCB_ULBL (pLspCtrlBlock);
        NH_TRGCB_ULABEL_REQID (pNHTrggCtrlBlk) = LCB_UREQ_ID (pLspCtrlBlock);
        MEMCPY (&pNHTrggCtrlBlk->Fec, &LCB_FEC (pLspCtrlBlock), sizeof (tFec));
        MEMCPY (&pNHTrggCtrlBlk->oRgNextHopAddr, &pLspCtrlBlock->NextHopAddr,
                sizeof (uGenU4Addr));
        pNHTrggCtrlBlk->u2AddrType = pLspCtrlBlock->u2AddrType;
        pNHTrggCtrlBlk->pTrigCtrlBlkInOrgLsp = LCB_TRIG (pLspCtrlBlock);

        gLdpNonMrgNhopChngFsm
            [pNHTrggCtrlBlk->u1NhLspState][LDP_NH_LSM_EVT_INT_NEW_NH]
            (pNHTrggCtrlBlk, pMsg);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgEstPreEmpTnl                                     */
/* Description   : This routine is called when label state machine is in     */
/*                 Established state and preempted by any tunnel.            */
/*                 Reference - Section 4.4   draft-ietf-mpls-crldp-04.txt    */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgEstPreEmpTnl (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    pMsg = (UINT1 *) pMsg;

    /* Send Label Release message to down stream and WithDraw message to
     * Upstream Session. 
     */
    if (LCB_DSSN (pLspCtrlBlock) != NULL)
    {
        /* Case of Ingress and Intermediate */
        LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock), &LCB_DLBL (pLspCtrlBlock),
                          LCB_DSSN (pLspCtrlBlock));
    }
    if (LCB_USSN (pLspCtrlBlock) != NULL)
    {
        /* Case of Egress and Intermediate */
        LdpSendLblWithdrawMsg (&LCB_FEC (pLspCtrlBlock),
                               &LCB_ULBL (pLspCtrlBlock),
                               LCB_USSN (pLspCtrlBlock));
        LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_REL_AWT;
    }
    else
    {
        /* Case of Ingress */
        LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRelLdpMap                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 RELEASE_AWAITED state and Label Map. Message is received. */
/*                                                                           */
/*                 Reference - Section 3.1.5.4 - page 20 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRelLdpMap (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    pMsg = (UINT1 *) pMsg;
    LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock), &LCB_DLBL (pLspCtrlBlock),
                      LCB_DSSN (pLspCtrlBlock));
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRelLdpRel                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 RELEASE_AWAITED state and Label Rel. Message is received. */
/*                                                                           */
/*                 Reference - Section 3.1.5.4 - page 20 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRelLdpRel (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pLdpSession = NULL;
    UINT4               u4SessIfIndex;

    pMsg = (UINT1 *) pMsg;
    pLdpSession = LCB_USSN (pLspCtrlBlock);
    u4SessIfIndex =
        LdpSessionGetIfIndex (pLdpSession, pLspCtrlBlock->Fec.u2AddrFmly);
    LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &LCB_ULBL (pLspCtrlBlock),
                  u4SessIfIndex);
    LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);

    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRelLdpWth                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 RELEASE_AWAITED state and Label With. Message is received.*/
/*                                                                           */
/*                 Reference - Section 3.1.5.4 - page 20 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRelLdpWth (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    pMsg = (UINT1 *) pMsg;
    LdpSendLblRelMsg (&LCB_FEC (pLspCtrlBlock), &LCB_DLBL (pLspCtrlBlock),
                      LCB_DSSN (pLspCtrlBlock));
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRelUpAbort                                       */
/* Description   : This routine is called when label state machine is in     */
/*                 RELEASE_AWAITED state and Label Abort Message is received.*/
/*                                                                           */
/*                 Reference - Section 3.1.5.4 - page 20 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRelUpAbort (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pLdpSession = NULL;
    UINT4               u4SessIfIndex;

    pLdpSession = LCB_USSN (pLspCtrlBlock);
    u4SessIfIndex =
        LdpSessionGetIfIndex (pLdpSession, pLspCtrlBlock->Fec.u2AddrFmly);
    LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &LCB_ULBL (pLspCtrlBlock),
                  u4SessIfIndex);
    LdpSendNotifMsg (pLdpSession, pMsg, LDP_FALSE, LDP_STAT_TYPE_LBL_REQ_ABRTD,
                     NULL);
    LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgRelUpLost                                        */
/* Description   : This routine is called when label state machine is in     */
/*                 RELEASE_AWAITED state and Upstream is lost.               */
/*                                                                           */
/*                 Reference - Section 3.1.5.4 - page 21 - draft-ietf-mpls-  */
/*                 ldp-state-01.txt                                          */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgRelUpLost (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    tLdpSession        *pLdpSession = NULL;
    UINT4               u4SessIfIndex;

    pMsg = (UINT1 *) pMsg;
    pLdpSession = LCB_USSN (pLspCtrlBlock);
    u4SessIfIndex =
        LdpSessionGetIfIndex (pLdpSession, pLspCtrlBlock->Fec.u2AddrFmly);

    LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &LCB_ULBL (pLspCtrlBlock),
                  u4SessIfIndex);
    LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgInvStateEvent                                    */
/* Description   : This routine is stub routine that is caled on an occurence*/
/*                 of an invalid event in a state.                           */
/*                                                                           */
/* Input(s)      : pLspCtrlBlock, pMsg                                       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgInvStateEvent (tLspCtrlBlock * pLspCtrlBlock, UINT1 *pMsg)
{
    pMsg = (UINT1 *) pMsg;
    pLspCtrlBlock = (tLspCtrlBlock *) pLspCtrlBlock;
    /* Invalid combination of state and event has occured */
    return;
}

/*****************************************************************************/
/* Function Name : LdpGetLabel                                               */
/* Description   : This routine alocates the next label to be used.          */
/* Input(s)      : pLdpEntity - Pointer to the LDP Entity - one of the label */
/*                 form its label space is to be obtained.                   */
/*                 u4SessIfIndex - Session Interface Index.                  */
/* Output(s)     : pLabel - Pointer to the label in which the label value    */
/*                 to be filled in case of successful label allocation.      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

UINT1
LdpGetLabel (tLdpEntity * pLdpEntity, uLabel * pLabel, UINT4 u4SessIfIndex)
{
    UINT4               u4Label1;
    UINT4               u4Label2;
    UINT1               u1LblResAvl = LDP_FALSE;
    tLdpEthParams      *pLdpEthParams = NULL;

    LDP_DBG2 (LDP_DBG_PRCS, "LdpEntityId: %d, SessionIfIndex: %d\n",
              pLdpEntity->u4EntityIndex, u4SessIfIndex);

    if ((LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE) ||
        (pLdpEntity->u1LabelSpaceType == LDP_PER_PLATFORM))
    {
        TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity), pLdpEthParams,
                      tLdpEthParams *)
        {
            LDP_DBG2 (LDP_DBG_PRCS,
                      "pLdpEthParams->u4IfIndex: %d, u4SessIfIndex: %d\n",
                      pLdpEthParams->u4IfIndex, u4SessIfIndex);

            if ((pLdpEthParams->u4IfIndex == u4SessIfIndex) ||
                (pLdpEntity->bIsIntfMapped == LDP_FALSE))
            {
                if (ldpGetLblFromLblGroup (pLdpEthParams->u2EthLblRangeId,
                                           &u4Label1, &u4Label2) == LDP_SUCCESS)
                {
                    u1LblResAvl = LDP_TRUE;
                    break;
                }
            }
        }
    }
    else if (ldpGetLblFromLblGroup (pLdpEntity->u2LabelRangeId,
                                    &u4Label1, &u4Label2) == LDP_SUCCESS)
    {
        u1LblResAvl = LDP_TRUE;
    }

    if (u1LblResAvl == LDP_FALSE)
    {
        pLdpEntity->u1LblResAvl = LDP_FALSE;
        return LDP_FAILURE;
    }

    /* 
     * The label range type is checked as whether it is per platform based
     * or is it per interface based. In case of per interface based, the label
     * is currently assumed to be of ATM label type. The VPI value would have
     * been obtained in u2label1 and the VCI value would have been obtained 
     * in the u2label2. The VPI and VCI values are to be combined and given
     * back as a single label.
     */
    if (pLdpEntity->u1LabelSpaceType == LDP_PER_PLATFORM)
    {
        pLabel->u4GenLbl = u4Label2;

    }
    else if (pLdpEntity->u1LabelSpaceType == LDP_PER_IFACE)
    {
        pLabel->AtmLbl.u2Vpi = (UINT2) u4Label1;
        pLabel->AtmLbl.u2Vci = (UINT2) u4Label2;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpFreeLabel                                              */
/* Description   : This routine frees the label for reuse.                   */
/* Input(s)      : pLdpEntity - Pointer to the Entity one of its label is to */
/*                 is to be released.                                        */
/*                 pLabel - pointer to the label that is to be released.     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpFreeLabel (tLdpEntity * pLdpEntity, uLabel * pLabel, UINT4 u4SessIfIndex)
{
    UINT4               u4Label1 = 0;
    UINT4               u4Label2 = 0;
    UINT2               u2LabelRangeId = 0;
    UINT1               u1RetVal;
    tLdpIfMsg           LdpIfMsg;
    tLdpEthParams      *pLdpEthParams = NULL;

    if ((LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE) ||
        (pLdpEntity->u1LabelSpaceType == LDP_PER_PLATFORM))
    {
        TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity), pLdpEthParams,
                      tLdpEthParams *)
        {
            if (u4SessIfIndex == LDP_ZERO)
            {
                if ((pLabel->u4GenLbl >= pLdpEthParams->u4MinLabelVal) &&
                    (pLabel->u4GenLbl <= pLdpEthParams->u4MaxLabelVal))
                {
                    u2LabelRangeId = pLdpEthParams->u2EthLblRangeId;
                    u4Label1 = 0;
                    u4Label2 = pLabel->u4GenLbl;
                }
            }
            else if ((pLdpEthParams->u4IfIndex == u4SessIfIndex) ||
                     (pLdpEntity->bIsIntfMapped == LDP_FALSE))
            {
                u2LabelRangeId = pLdpEthParams->u2EthLblRangeId;
                u4Label1 = 0;
                u4Label2 = pLabel->u4GenLbl;
            }
        }
    }
    else
    {
        u2LabelRangeId = pLdpEntity->u2LabelRangeId;
        u4Label1 = (UINT2) pLabel->AtmLbl.u2Vpi;
        u4Label2 = (UINT2) pLabel->AtmLbl.u2Vci;
    }

    u1RetVal = ldpRelLblToLblGroup (u2LabelRangeId, u4Label1, u4Label2);
    if ((pLdpEntity->u1LblResAvl == LDP_FALSE) && (u1RetVal == LDP_SUCCESS))
    {
        pLdpEntity->u1LblResAvl = LDP_TRUE;

        LdpIfMsg.u4MsgType = LDP_INTERNAL_EVENT;
        LdpIfMsg.u.IntEvt.u4SubEvt = LDP_LBL_RES_AVL_EVENT;
        LdpIfMsg.u.IntEvt.pEntity = (UINT1 *) pLdpEntity;
        LdpIfMsg.u.IntEvt.pSession = NULL;
        LdpIfMsg.u.IntEvt.u4IncarnNo = 0;
        LdpIfMsg.u.IntEvt.u2AddrType = LDP_ADDR_TYPE_IPV4;

        if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
        {
            LDP_DBG (LDP_ADVT_MISC,
                     "ADVT : Failed to EnQ LDP_LBL_RES_AVL_EVENT to LDP Task\n");
        }
    }
    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : LdpDeleteLspCtrlBlock                                     */
/* Description   : This routine deletes LSP control block. Up/Downstream     */
/*                 LSP control block list in session entry is also modified. */
/*                 MLIB is updated by deleting the corresponding entry.      */
/* Input(s)      : pLspCtrlBlock                                             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpDeleteLspCtrlBlock (tLspCtrlBlock * pLspCtrlBlock)
{
    tTMO_SLL           *pLcbList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    UINT2               u2IncarnId = 0;
    tLdpSession        *pSessionEntry = LCB_USSN (pLspCtrlBlock);
#ifdef CFA_WANTED
    UINT4               u4TnlIfIndex = 0;
    UINT4               u4L3VlanIf = 0;
#endif
#ifdef LDP_GR_WANTED
    UINT4               u1IsCfaNPBlock = LDP_FALSE;
#endif
    tFecTableEntry     *pFecTableEntry = NULL;

    if (pSessionEntry == NULL)
    {
        pSessionEntry = LCB_DSSN (pLspCtrlBlock);
    }

    /* If this routine is called when both USSN and DSSN are not updated 
     * then, calling of this routine would result in Seg fault. To avoid 
     * this, for the cases mentioned above, default incarn would be assi
     * gned. This fix works fine if it's a single incarnation. For multi
     * ple incarnations, u2IncarnId should be passed as an extra argument
     * to this routine. 
     */

    if (pSessionEntry == NULL)
    {
        u2IncarnId = LDP_CUR_INCARN;
    }
    else
    {
        u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);
    }

#ifdef LDP_GR_WANTED
    if ((gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus ==
         LDP_GR_SHUT_DOWN_IN_PROGRESS) &&
        (gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability ==
         LDP_GR_CAPABILITY_FULL) &&
        (pSessionEntry != NULL) &&
        (pSessionEntry->pLdpPeer->u1GrProgressState !=
         LDP_PEER_GR_NOT_SUPPORTED))
    {
        /* 1) For the IMPLICIT NULL Label, the FTN Entry is not programmed in the H/w.
         * Thus the FTN Entry is not added in the FTN Hw List as well. Hence
         * after GR when Label Mapping msg will come for the FEC which is Implicit 
         * NULL, the FTN hw list entry wont be found and hence, the MPLS Tunnel
         * interface wont be re-used again. This would be leak. Thus, we should
         * delete the MPLS Tunnel interface which are a part of Implicit NULL
         * FTN's. After GR, always a new MPLS Tunnel interface would be allocated.
         *
         * 2) The MPLS Tunnel interfaces will not be present for the control blocks
         * which are not programmed in the H/w. These are the control which are created
         * when LBL Map is received from the non-best next hop peers. (For liberal mode).
         * Do not block the Tunnel interface deletion for such control blocks */

        if ((LCB_DLBL (pLspCtrlBlock).u4GenLbl != MPLS_IMPLICIT_NULL_LABEL) &&
            (LCB_MLIB_UPD_STATUS (pLspCtrlBlock) !=
             LDP_DU_DN_MLIB_UPD_NOT_DONE))
        {
            u1IsCfaNPBlock = LDP_TRUE;
        }
    }
#endif

    /*  The Session can come down under any circumstances and the timer
     *  has to be stopped    */
    if (gu4LdpLspPersist == FALSE)
    {
        if (pLspCtrlBlock->pTrigCtrlBlk != NULL)
        {
            /* The Retry Timer is only at the Ingress */
            TmrStopTimer (LDP_TIMER_LIST_ID,
                          (tTmrAppTimer *)
                          & (pLspCtrlBlock->TardyTimer.AppTimer));
        }
    }

    TmrStopTimer (LDP_TIMER_LIST_ID,
                  (tTmrAppTimer *) & (pLspCtrlBlock->PerstTimer.AppTimer));

#ifdef ONDEMAND
    /** This control block should not delete Trig control block**/
    if (LCB_NHOP (pLspCtrlBlock) != NULL)
    {
        LdpDeleteTrigCtrlBlk (LCB_NHOP (pLspCtrlBlock));
    }
    else if (LCB_TRIG (pLspCtrlBlock) != NULL)
    {
        MemReleaseMemBlock (LDP_TRIG_POOL_ID,
                            (UINT1 *) (pLspCtrlBlock->pTrigCtrlBlk));
        LDP_DBG3 (LDP_ADVT_MEM,
                  "ADVT: %s: %d Memory release for Trig Control Block pNHTrggCtrlBlk=0x%x\n",
                  __FUNCTION__, __LINE__, (pLspCtrlBlock->pTrigCtrlBlk));
    }
#endif

    /*
     * In case the LSP control block is associated with a CRLSP path,
     * the tunnel information needs to be deleted when the LSP Control
     * block is deleted.
     */
    if (pLspCtrlBlock->pCrlspTnlInfo != NULL)
    {
        LdpDeleteCrlspTnlInfo (u2IncarnId, pLspCtrlBlock->pCrlspTnlInfo);
    }

    TMO_DYN_SLL_Scan (&LCB_UPSTR_LIST (pLspCtrlBlock), pSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        pLspUpCtrlBlock = SLL_TO_UPCB (pSllNode);
        UPSTR_DSTR_PTR (pLspUpCtrlBlock) = NULL;
    }

    /* 
     * Delete this LSP control blcok from upstream session's upstream LSP
     * Control block list
     */
    if (LCB_USSN (pLspCtrlBlock) != NULL)
    {
        pLcbList = &SSN_ULCB (LCB_USSN (pLspCtrlBlock));
        TMO_SLL_Delete (pLcbList, &(pLspCtrlBlock->NextUSLspCtrlBlk));
    }

    /* 
     * Delete this LSP control blcok from downstream session's downstream LSP
     * Control block list
     */
    if (LCB_DSSN (pLspCtrlBlock) != NULL)
    {
        pLcbList = &SSN_DLCB (LCB_DSSN (pLspCtrlBlock));
        TMO_SLL_Delete (pLcbList, LCB_TO_SLL (pLspCtrlBlock));
    }
    LDP_DBG2 (LDP_SSN_PRCS,
              "MPLS Tnl Out if index %d Org If Index %d\n",
              pLspCtrlBlock->u4OutIfIndex, pLspCtrlBlock->u4OrgOutIfIndex);

#ifdef CFA_WANTED
#ifdef LDP_GR_WANTED
    if ((pLspCtrlBlock->u4OutIfIndex != 0) &&
        (pLspCtrlBlock->u1IsLspUsedByL2VPN == FALSE) &&
        (u1IsCfaNPBlock == LDP_FALSE))
    {
        u4TnlIfIndex = pLspCtrlBlock->u4OutIfIndex;

        if (CfaUtilGetIfIndexFromMplsTnlIf (u4TnlIfIndex, &u4L3VlanIf, TRUE)
            != CFA_FAILURE)
        {

            if (CfaIfmDeleteStackMplsTunnelInterface
                (u4L3VlanIf, pLspCtrlBlock->u4OutIfIndex) == CFA_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED
                if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
                {
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "Protected MPLS Tunnel IF %d for FEC %s deletion "
                              "failed\n",
                              u4TnlIfIndex,
                              Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.
                                            Ip6Addr));
                }
                else
#endif
                {
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "Protected MPLS Tunnel IF %d for FEC %x deletion "
                              "failed\n",
                              u4TnlIfIndex,
                              LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));
                }
            }
            if (pSessionEntry != NULL)
            {
#ifdef MPLS_IPV6_WANTED
                if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
                {
                    LDP_DBG6 (LDP_ADVT_PRCS,
                              "LdpDeleteLspCtrlBlock: OutInt %d deleted for "
                              "FEC: %s with Peer: %d.%d.%d.%d\n",
                              u4TnlIfIndex,
                              Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.Ip6Addr),
                              pSessionEntry->pLdpPeer->PeerLdpId[0],
                              pSessionEntry->pLdpPeer->PeerLdpId[1],
                              pSessionEntry->pLdpPeer->PeerLdpId[2],
                              pSessionEntry->pLdpPeer->PeerLdpId[3]);
                }
                else
#endif
                {
                    LDP_DBG6 (LDP_ADVT_PRCS,
                              "LdpDeleteLspCtrlBlock: OutInt %d deleted for "
                              "FEC: %x with Peer %d.%d.%d.%d\n",
                              u4TnlIfIndex,
                              LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix),
                              pSessionEntry->pLdpPeer->PeerLdpId[0],
                              pSessionEntry->pLdpPeer->PeerLdpId[1],
                              pSessionEntry->pLdpPeer->PeerLdpId[2],
                              pSessionEntry->pLdpPeer->PeerLdpId[3]);
                }
            }
        }
        pLspCtrlBlock->u4OutIfIndex = 0;

    }
#else
    if ((pLspCtrlBlock->u4OutIfIndex != 0) &&
        (pLspCtrlBlock->u1IsLspUsedByL2VPN == FALSE))
    {
        u4TnlIfIndex = pLspCtrlBlock->u4OutIfIndex;

        if (CfaUtilGetIfIndexFromMplsTnlIf (u4TnlIfIndex, &u4L3VlanIf, TRUE)
            != CFA_FAILURE)
        {

            if (CfaIfmDeleteStackMplsTunnelInterface
                (u4L3VlanIf, pLspCtrlBlock->u4OutIfIndex) == CFA_FAILURE)
            {
                LDP_DBG2 (LDP_SSN_PRCS,
                          "Protected MPLS Tunnel IF %d for FEC %x deletion "
                          "failed\n",
                          u4TnlIfIndex,
                          LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));
            }
            if (pSessionEntry != NULL)
            {

                LDP_DBG6 (LDP_ADVT_PRCS,
                          "LdpDeleteLspCtrlBlock: OutInt %d deleted for "
                          "FEC: %x with Peer %d.%d.%d.%d\n",
                          u4TnlIfIndex,
                          LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix),
                          pSessionEntry->pLdpPeer->PeerLdpId[0],
                          pSessionEntry->pLdpPeer->PeerLdpId[1],
                          pSessionEntry->pLdpPeer->PeerLdpId[2],
                          pSessionEntry->pLdpPeer->PeerLdpId[3]);

            }
        }
        pLspCtrlBlock->u4OutIfIndex = 0;
    }
#endif
    else
    {
        if (pLspCtrlBlock->u4OutIfIndex != 0)
        {
#ifdef MPLS_IPV6_WANTED
            if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
            {
#ifdef LDP_GR_WANTED
                if (u1IsCfaNPBlock == LDP_TRUE)
                {
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "\rMPLS Tunnel IF %d for FEC %x is not deleted by LDP, since Module Shutdown "
                              " is in Progress\n",
                              pLspCtrlBlock->u4OutIfIndex,
                              Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.
                                            Ip6Addr));
                }
#endif
                if (pLspCtrlBlock->u1IsLspUsedByL2VPN == TRUE)
                {
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "\rMPLS Tunnel IF %d for FEC %s is not deleted by LDP - "
                              " Will be deleted by L2VPN\n",
                              pLspCtrlBlock->u4OutIfIndex,
                              Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.
                                            Ip6Addr));
                }
            }
            else
#endif
            {
#ifdef LDP_GR_WANTED
                if (u1IsCfaNPBlock == LDP_TRUE)
                {
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "\rMPLS Tunnel IF %d for FEC %x is not deleted by LDP, since Module Shutdown "
                              " is in Progress\n",
                              pLspCtrlBlock->u4OutIfIndex,
                              LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));
                }
#endif
                if (pLspCtrlBlock->u1IsLspUsedByL2VPN == TRUE)
                {
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "\rMPLS Tunnel IF %d for FEC %x is not deleted by LDP - "
                              " Will be deleted by L2VPN\n",
                              pLspCtrlBlock->u4OutIfIndex,
                              LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));
                }
            }
        }
    }
    if (pLspCtrlBlock->u4OrgOutIfIndex != LDP_ZERO)
    {
        u4TnlIfIndex = pLspCtrlBlock->u4OrgOutIfIndex;

        if (CfaUtilGetIfIndexFromMplsTnlIf (u4TnlIfIndex, &u4L3VlanIf, TRUE)
            != CFA_FAILURE)
        {
            if (CfaIfmDeleteStackMplsTunnelInterface
                (u4L3VlanIf, pLspCtrlBlock->u4OrgOutIfIndex) == CFA_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED
                if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
                {
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "Backup MPLS Tunnel IF %d for FEC %s deletion failed\n",
                              u4TnlIfIndex,
                              Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.
                                            Ip6Addr));
                }
                else
#endif
                {
                    LDP_DBG2 (LDP_SSN_PRCS,
                              "Backup MPLS Tunnel IF %d for FEC %x deletion failed\n",
                              u4TnlIfIndex,
                              LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix));
                }
            }
            if (pSessionEntry != NULL)
            {
#ifdef MPLS_IPV6_WANTED
                if (pLspCtrlBlock->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
                {
                    LDP_DBG6 (LDP_ADVT_PRCS,
                              "LdpDeleteLspCtrlBlock: BkpOutInt %d deleted for "
                              "FEC: %s with Peer %d.%d.%d.%d\n",
                              u4TnlIfIndex,
                              Ip6PrintAddr (&pLspCtrlBlock->Fec.Prefix.Ip6Addr),
                              pSessionEntry->pLdpPeer->PeerLdpId[0],
                              pSessionEntry->pLdpPeer->PeerLdpId[1],
                              pSessionEntry->pLdpPeer->PeerLdpId[2],
                              pSessionEntry->pLdpPeer->PeerLdpId[3]);
                }
                else
#endif
                {

                    LDP_DBG6 (LDP_ADVT_PRCS,
                              "LdpDeleteLspCtrlBlock: BkpOutInt %d deleted for "
                              "FEC: %x with Peer %d.%d.%d.%d\n",
                              u4TnlIfIndex,
                              LDP_IPV4_U4_ADDR (pLspCtrlBlock->Fec.Prefix),
                              pSessionEntry->pLdpPeer->PeerLdpId[0],
                              pSessionEntry->pLdpPeer->PeerLdpId[1],
                              pSessionEntry->pLdpPeer->PeerLdpId[2],
                              pSessionEntry->pLdpPeer->PeerLdpId[3]);
                }
            }
        }
        pLspCtrlBlock->u4OrgOutIfIndex = 0;
    }
#endif
    if (pLspCtrlBlock->u4InComIfIndex != 0)
    {
        pLspCtrlBlock->u4InComIfIndex = 0;
    }
    if ((pLspCtrlBlock->Fec.u4Index != 0) &&
        (pLspCtrlBlock->Fec.u4Index <= LDP_MAX_LSPS (MPLS_DEF_INCARN)))
    {
        pFecTableEntry = &LDP_STATICFEC (MPLS_DEF_INCARN,
                                         pLspCtrlBlock->Fec.u4Index);
        if (pFecTableEntry->pLspCtrlBlock == pLspCtrlBlock)
        {
            pFecTableEntry->pLspCtrlBlock = NULL;
            pLspCtrlBlock->Fec.u4Index = 0;
        }
    }

    /* 
     * Release of LSP Block to the pool of LSP control blocks is always
     * expected to be success. Hence explicitly the return value not
     * checked. Non check improves performance too.
     */
    MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlock);
    LDP_DBG3 (LDP_ADVT_MEM,
              "ADVT: %s: %d Memory release for Control Block pLspCtrlBlock=0x%x\n",
              __FUNCTION__, __LINE__, pLspCtrlBlock);

}

/*****************************************************************************/
/* Function Name : LdpIsLoopFound                                            */
/* Description   : This routine does the checking for loop detection, on     */
/*                 either hop count tlv or on both hop count tlv and path    */
/*                 vector tlvs.                                              */
/* Input(s)      : pu1HCTlv - points to the Hop count tlv received.          */
/*                 pu1PVTlv - points to the Path Vector Tlv                  */
/*                 u2IncarnId - Incarnation Id                               */
/*                 pLdpSession - points to the session                       */
/*                 u1IsLsrEgrOrIngr - true if lsr is not an intermediate     */
/* Output(s)     : pu1HcValue - Value of the Hop Count                       */
/*                 pu2PvCount - No of LSRIDs in the PV TLV                   */
/* Return(s)     : LDP_TRUE  if loop found else returns LDP_FALSE           */
/*****************************************************************************/

UINT1
LdpIsLoopFound (UINT1 *pu1HCTlv, UINT1 *pu1PVTlv, UINT2 *pu2PvCount,
                UINT1 *pu1HcValue, UINT2 u2IncarnId, tLdpSession * pLdpSession,
                UINT1 u1LsrEgrOrIngr)
{
    UINT2               u2TmpLen;
    uGenAddr            LocalLsrId;
    UINT1              *pu1TempPVTlv = NULL;
    UINT1              *pu1TempHCTlv = NULL;

    UINT1               u1PvLimit;
    UINT1               u1HcLimit;

    /* As per RFC: 5036, section: A.2.6
     * If the Received message has Path Vector or Hop Count, then
     * check for the Loop detection.
     */

    u1PvLimit = LDP_ENTITY_PV_LIMIT (SSN_GET_ENTITY (pLdpSession));
    u1HcLimit = (UINT1) LDP_ENTITY_HC_LIMIT (SSN_GET_ENTITY (pLdpSession));

    /* if pvlimit is 0 then no need to chk for loop detection with pv */
    /* if hclimit is 0 the same applies as pvlimit */
    /* if non zero chk against the configured limits */

    pu1TempPVTlv = pu1PVTlv;
    pu1TempHCTlv = pu1HCTlv;

    if ((pu1TempPVTlv != NULL) && (u1PvLimit != 0))
    {
        /* detect loop based on PV Tlv and hop count. */
        pu1TempPVTlv += LDP_TLV_TYPE_LEN;
        LDP_EXT_2_BYTES (pu1TempPVTlv, *pu2PvCount);
        *pu2PvCount = (UINT2) (((*pu2PvCount) / LDP_LSR_ID_LEN));

        if ((*pu2PvCount > u1PvLimit) ||
            ((*pu2PvCount == u1PvLimit) && (u1LsrEgrOrIngr == LDP_FALSE)))
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Loop Found. Source - PV Limit Reached\n");
            return LDP_TRUE;
        }

        /* Getting the Local Lsr Id */
        MEMCPY ((UINT1 *) &LocalLsrId, (UINT1 *) (&(LDP_LSR_ID (u2IncarnId))),
                LDP_LSR_ID_LEN);

        for (u2TmpLen = 0; u2TmpLen < (*pu2PvCount); u2TmpLen++,
             pu1TempPVTlv += LDP_LSR_ID_LEN)
        {
            if (MEMCMP
                ((UINT1 *) &LocalLsrId, (UINT1 *) pu1TempPVTlv,
                 LDP_LSR_ID_LEN) == 0)
            {
                LDP_DBG (LDP_ADVT_PRCS, "ADVT: Loop Found. Source - PV Tlv\n");
                return LDP_TRUE;
            }
        }
    }
    else
    {
        /* Returning the Pv Count */
        *pu2PvCount = 0;
    }

    if (pu1TempHCTlv != NULL)
    {
        /* Getting the Hop count tlv value from the tlv */
        pu1TempHCTlv += LDP_TLV_HDR_LEN;
        *pu1HcValue = *pu1TempHCTlv;

        if ((UINT1) (*pu1HcValue) == LDP_INVALID_HOP_COUNT)
        {
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: Loop Found. Source - HC Tlv\n");
            return LDP_TRUE;
        }
        if ((u1HcLimit != LDP_ZERO) && ((*pu1HcValue > u1HcLimit) ||
                                        ((*pu1HcValue == u1HcLimit)
                                         && (u1LsrEgrOrIngr == LDP_FALSE))))
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: Loop Found. Source - HC Limit Reached\n");
            return LDP_TRUE;
        }
    }
    else
    {
        /* Returning the HcValue */
        *pu1HcValue = LDP_ZERO;

        if (pu1TempHCTlv == NULL)
        {
            *pu1HcValue = LDP_INVALID_HOP_COUNT;
        }
    }
    return LDP_FALSE;
}

/*****************************************************************************/
/* Function Name : LdpIsLsrEgress                                            */
/* Description   : Checks whether for the given Fec, this LSR acts as an     */
/*                 Egress or not. In case the LSR is not an egress for the   */
/*                 for the FEC, the next hop address and the outgoing        */
/*                 interface values will be returned.                        */
/* Input(s)      : pFec - Fec for which the condition of Egress needs to be  */
/*                        checked.                                           */
/*                 pu1RouteStatus - This field is set to LDP_FALSE, if no    */
/*                                   route is found for the given Fec.       */
/*                 pu4NextHopAddr - Stores the value of the next hop address */
/*                 returned by the IP after a route lookup                   */
/*                 pu4IfIndex - Stores the value of the outgoing interface   */
/*                 index to reach the destination. This value is             */
/*                 returned by the IP after a route lookup                   */
/*                 u2IncarnId - Incarnation Id                               */
/* Output(s)     : pu1RouteStatus                                          */
/* Return(s)     : LDP_TRUE / LDP_FALSE                                      */
/*****************************************************************************/

UINT1
LdpIsLsrEgress (tFec * pFec, UINT1 *pu1RouteStatus,
                uGenU4Addr * pNextHopAddr, UINT4 *pu4IfIndex, UINT2 u2IncarnId)
{
    uGenU4Addr          DestAddr;
    uGenU4Addr          DestMask;
    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    tLdpEntity         *pLdpEntity = NULL;
    tLdpIfTableEntry   *pIfEntry = NULL;
    tNetIpv4IfInfo      NetIpIfInfo;

    MEMSET (&DestAddr, LDP_ZERO, sizeof (uGenU4Addr));
    MEMSET (&DestMask, LDP_ZERO, sizeof (uGenU4Addr));
    MEMSET (&NetIpIfInfo, 0, sizeof (tNetIpv4IfInfo));

    /* Getting the Dest Addr from the given Fec. */
#ifdef MPLS_IPV6_WANTED
    if (pFec->u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        MEMCPY (LDP_IPV6_U4_ADDR (DestAddr),
                LDP_IPV6_U4_ADDR (pFec->Prefix), LDP_IPV6ADR_LEN);
    }
    else
#endif
    {
        LDP_IPV4_U4_ADDR (DestAddr) = LDP_IPV4_U4_ADDR (pFec->Prefix);
    }

    *pu4IfIndex = 0;

    if (pFec->u1FecElmntType == LDP_FEC_HOSTADDR_TYPE)
    {
        /* 
         * Checking the interface addresses' list to check if the Destination
         * belongs to one of it's interfaces.
         */
#ifdef MPLS_IPV6_WANTED
        if (pFec->u2AddrFmly == LDP_ADDR_TYPE_IPV6)
        {
            if (LdpIpv6IsAddrLocal (&DestAddr.Ip6Addr) == TRUE)
            {
                return LDP_TRUE;
            }
        }
        else
#endif
        {
            if (LDP_IS_DEST_OUR_ADDR (LDP_IPV4_U4_ADDR (DestAddr)) == TRUE)
            {
                return LDP_TRUE;
            }
        }
    }

    /* 
     * Control comes here when either,
     *    - Fec Type is HostAddressType and when the DestAddr doesn't 
     *      match with any of the Lsr's Interface Addresses.
     *    - FecType is AddressPrefixType.
     *  For the above cases, route look-up is done for the given DestAddr.
     *  If the DestAddr is a Local Route, then this Lsr will be acting as an
     *  Egress, else will be acting as Intermediate Lsr for the Lsp being
     *  established to this Destination.
     */
#ifdef MPLS_IPV6_WANTED
    if (pFec->u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        if (LdpIsLsrPartOfIpv6ErHopOrFec (&DestAddr.Ip6Addr, pFec->u1PreLen) ==
            LDP_TRUE)
        {
            return LDP_TRUE;
        }
        if (LdpIpv6GetExactRoute (&DestAddr.Ip6Addr, pFec->u1PreLen, pu4IfIndex,
                                  &pNextHopAddr->Ip6Addr) == LDP_IP_FAILURE)
        {
            LDP_DBG (LDP_ADVT_PRCS,
                     "ADVT: IPV6 Get Exact Route Not Available\n");
            *pu1RouteStatus = LDP_FALSE;    /* Set Status to indicate that No
                                             * Exact Route found for the given
                                             *                     * Dest Addr */
            return LDP_FALSE;
        }
    }
    else
#endif
    {
        LDP_GET_MASK_FROM_PRFX_LEN (pFec->u1PreLen,
                                    LDP_IPV4_U4_ADDR (DestMask));

        /* Check whether the destination belongs to any local network */

        if (LdpIsLsrPartOfIpv4ErHopOrFec (LDP_IPV4_U4_ADDR (DestAddr),
                                          LDP_IPV4_U4_ADDR (DestMask)) ==
            LDP_TRUE)
        {
            return LDP_TRUE;
        }

        LDP_IPV4_U4_ADDR (DestAddr) =
            (LDP_IPV4_U4_ADDR (DestAddr) & LDP_IPV4_U4_ADDR (DestMask));

        /*  Currently FutureIP provides classful routing , hence the
         *  below function returns properly only when the mask is
         *  8, 16, 24 or 32, However it works in Vxworks with classless
         */
        if (LdpIpGetExactRoute
            (LDP_IPV4_U4_ADDR (DestAddr), LDP_IPV4_U4_ADDR (DestMask),
             pu4IfIndex, &LDP_P_IPV4_U4_ADDR (pNextHopAddr)) == LDP_IP_FAILURE)
        {
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: IP Get Exact Route Not Available\n");
            *pu1RouteStatus = LDP_FALSE;    /* Set Status to indicate that No 
                                             * Exact Route found for the given 
                                             * Dest Addr */
            return LDP_FALSE;
        }
    }

    /* 
     * FEC is AddressPrefix Type and the Destination is reachable through a
     * NextHop.
     * Checking if the index over which the Destination is reachable, is
     * configured with some LdpEntity. If no match is found then, the current
     * LSR will be acting as Proxy Egress.
     */
    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        if (LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE)
        {
            continue;
        }
        TMO_SLL_Scan (&pLdpEntity->IfList, pIfEntry, tLdpIfTableEntry *)
        {
            if (pIfEntry->u4IfIndex == *pu4IfIndex)
            {
                return LDP_FALSE;
            }
        }
    }
    return LDP_TRUE;
}

/*****************************************************************************/
/* Function Name : LdpGetPeerSession                                         */
/* Description   : Checks whether any LDP peer has advertise the next hop    */
/*                 address as one of the addresses in its LDP Address messag-*/
/*                 es. IF such a peer exists, the session of the peer is     */
/*                 returned.                                                 */
/* Input(s)      : u4NextHopAddr - Next hop address, of the FEc for which    */
/*                 label request message is to be sent.                      */
/*                 u4IfIndex - Interface index to reach the next hop address */
/*                 u2IncarnId - Incarnation Id                               */
/* Output(s)     : ppSessionEntry - Pointer to the session entry of the peer  */
/*                 which has advertised the next hop address in its LDP      */
/*                 address messages.                                         */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpGetPeerSession (tGenU4Addr * pNextHopAddr, UINT4 u4IfIndex,
                   tLdpSession ** ppPeerSession, UINT2 u2IncarnId)
{
    UINT4               u4HIndex = 0;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
#ifdef MPLS_IPV6_WANTED
    UINT1               u1Ipv6HIndex = 0;
    tPeerIfAdrNode     *pHTmpIfAddrNode = NULL;
#endif
    u4IfIndex = (UINT4) u4IfIndex;
#ifdef MPLS_IPV6_WANTED
    if (pNextHopAddr->u2AddrType == LDP_ADDR_TYPE_IPV6)
    {

        u1Ipv6HIndex = Ip6AddrHash (&pNextHopAddr->Addr.Ip6Addr);

        TMO_HASH_DYN_Scan_Bucket (LDP_PEER_IPV6_IFADR_TABLE (u2IncarnId),
                                  u1Ipv6HIndex, pIfAddrNode, pHTmpIfAddrNode,
                                  tPeerIfAdrNode *)
        {
            pIfAddrNode = LDP_IPV6_OFFSET (pIfAddrNode);

            if ((MEMCMP
                 (&pNextHopAddr->Addr.Ip6Addr, &pIfAddrNode->IfAddr.Ip6Addr,
                  LDP_IPV6ADR_LEN) == 0)
                && (LdpIfGetAddressIndex (pIfAddrNode, u4IfIndex) == LDP_TRUE))
            {
                /* Getting  peer's session associated to the next hop address. */
                *ppPeerSession = pIfAddrNode->pSession;
                return LDP_SUCCESS;

            }

        }
    }
    else
#endif
    {
        u4HIndex =
            LDP_COMPUTE_HASH_INDEX (LDP_IPV4_U4_ADDR (pNextHopAddr->Addr));

        TMO_HASH_Scan_Bucket (LDP_PEER_IFADR_TABLE (u2IncarnId), u4HIndex,
                              pIfAddrNode, tPeerIfAdrNode *)
        {
            LDP_DBG1 (LDP_SSN_PRCS, "LdpGetPeerSession:%x \n",
                      OSIX_NTOHL (*
                                  ((UINT4 *) (VOID
                                              *) (&(pIfAddrNode->IfAddr)))));
            if (LDP_IPV4_U4_ADDR (pNextHopAddr->Addr) ==
                OSIX_NTOHL (*((UINT4 *) (VOID *) (&(pIfAddrNode->IfAddr)))))
            {
                /* Getting  peer's session associated to the next hop address. */
                *ppPeerSession = pIfAddrNode->pSession;
                return LDP_SUCCESS;
            }
        }
        /* Session not found for the given Next Hop Address */
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetLdpOverRsvpPeerSession                              */
/* Description   : Checks whether any remote LDP peer is reachable via       */
/*                 this next hop or address as one of the addresses in       */
/*                 its LDP Address messages.                                 */
/*                 IF such a peer exists, the session of the peer is         */
/*                 returned.                                                 */
/* Input(s)      : u4NextHopAddr - Next hop address, of the FEc for which    */
/*                 label request message is to be sent.                      */
/*                 u4IfIndex - Interface index to reach the next hop address */
/*                 u2IncarnId - Incarnation Id                               */
/* Output(s)     : ppSessionEntry - Pointer to the session entry of the peer  */
/*                 which has advertised the next hop address in its LDP      */
/*                 address messages.                                         */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpGetLdpOverRsvpPeerSession (UINT4 u4NextHopAddr, UINT4 u4IfIndex,
                              tLdpSession ** ppPeerSession, UINT2 u2IncarnId)
{
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tLdpSession        *pLdpSession = NULL;
    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT4               u4HIndex = 0;
    UINT4               u4PeerAddr = 0;
    UINT4               u4RtGw = 0;
    UINT2               u2RtPort = 0;

    u4IfIndex = (UINT4) u4IfIndex;
    u4HIndex = LDP_COMPUTE_HASH_INDEX (u4NextHopAddr);

    LDP_DBG (LDP_SSN_PRCS, "LdpGetLdpOverRsvpPeerSession Entry\n");

    /* This is useful for the directly connectly LDP over RSVP sessions */
    TMO_HASH_Scan_Bucket (LDP_PEER_IFADR_TABLE (u2IncarnId), u4HIndex,
                          pIfAddrNode, tPeerIfAdrNode *)
    {
        if (u4NextHopAddr ==
            OSIX_NTOHL (*((UINT4 *) (VOID *) (&(pIfAddrNode->IfAddr)))))
        {
            /* Getting  peer's session associated to the next hop address. */
            *ppPeerSession = pIfAddrNode->pSession;
            return LDP_SUCCESS;
        }
    }
    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
        {
            if ((pLdpPeer->pLdpSession) == NULL)
            {
                continue;
            }

            /* Target sessions also we need to
             * consider for LDP over RSVP case */
            pLdpSession = (tLdpSession *) pLdpPeer->pLdpSession;

            if (LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession)) ==
                LDP_FALSE)
            {
                continue;
            }
            if ((LDP_ENTITY_IS_TARGET_TYPE (SSN_GET_ENTITY (pLdpSession))
                 == LDP_TRUE) &&
                (SSN_GET_ENTITY (pLdpSession)->OutStackTnlInfo.u4TnlId ==
                 LDP_ZERO))
            {
                LDP_DBG (LDP_ADVT_PRCS,
                         "\n LdpDuDnIdLdpMap: Labels can't be negotiated when out-tunnel is not "
                         "associated with LDP targeted session\n");
                continue;
            }
            /* Check for the LDP over RSVP gateway */
            SSN_GET_PEER_TRANSADDR (u4PeerAddr, pLdpSession);

            /* Route nexthop and the targeted session's transport address 
             * should be in the same network,
             * otherwise do the following actions */
            if (LdpIpUcastRouteQuery
                (u4PeerAddr, &u2RtPort, &u4RtGw) == LDP_IP_SUCCESS)
            {
                if (u4RtGw == u4NextHopAddr)
                {
                    /* Getting  peer's session associated to the next hop address. */
                    *ppPeerSession = pLdpSession;
                    LDP_DBG (LDP_SSN_PRCS,
                             "LdpGetLdpOverRsvpPeerSession SUCCESS Exit\n");
                    return LDP_SUCCESS;
                }
            }
        }
    }

    LDP_DBG (LDP_SSN_PRCS, "LdpGetLdpOverRsvpPeerSession FAILURE Exit\n");
    /* Session not found for the given Next Hop Address */
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpIntLspSetReq                                           */
/* Description   : This routine is called when a new LSP is to be created in */
/*                 LSR. The LSR obtains information from the IP routing table*/
/*                 for a destination. The LSP is created for the destination.*/
/*                                                                           */
/* Input(s)      : pIntLspSetupInfo  -  Pointer to the structure holding     */
/*                                      route Information to set up Lsp.     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpIntLspSetReq (tIntLspSetupInfo * pIntLspSetupInfo)
{
    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tLdpSession        *pPrevLdpSession = NULL;
    tLspCtrlBlock      *pPrevLspCtrlBlk = NULL;
    tLspCtrlBlock      *pTempLspCtrlBlk = NULL;
    tLspTrigCtrlBlock  *pLspTrigCtrlBlock = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL           *pLspCtrlBlkList = NULL;
    tTMO_HASH_NODE     *pSsnHashNode = NULL;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tLdpSession        *pLdpSession = pIntLspSetupInfo->pLdpSession;
    UINT1               u1PrefLen = pIntLspSetupInfo->u1PrefixLen;
    UINT4               u4HIndex = 0;
    UINT1               u1RouteStatus = LDP_TRUE;
    UINT4               u4IfIndex;
    uGenU4Addr          NextHopAddr;
    tGenU4Addr          Prefix;
    UINT2               u2AddrType = 0;

    MEMSET (&Prefix, LDP_ZERO, sizeof (tGenU4Addr));

    u2AddrType = pIntLspSetupInfo->u2AddrType;

#if 0
    printf ("%s: Entry : %d\n", __FUNCTION__, __LINE__);
#endif

#ifdef MPLS_IPV6_WANTED
    if (u2AddrType == LDP_ADDR_TYPE_IPV6)
    {
        MEMCPY (LDP_IPV6_U4_ADDR (Prefix.Addr),
                LDP_IPV6_U4_ADDR (pIntLspSetupInfo->Dest), LDP_IPV6ADR_LEN);
        Prefix.u2AddrType = LDP_ADDR_TYPE_IPV6;
#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif
    }
    else
#endif
    {
        LDP_IPV4_U4_ADDR (Prefix.Addr) =
            LDP_IPV4_U4_ADDR (pIntLspSetupInfo->Dest);
        Prefix.u2AddrType = LDP_ADDR_TYPE_IPV4;
#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif
    }

    LdpGetPrevLdpSession (pIntLspSetupInfo->u2IncarnId, &Prefix, u1PrefLen,
                          &pPrevLspCtrlBlk, &pPrevLdpSession);

    if ((pPrevLspCtrlBlk != NULL) &&
        (pPrevLdpSession != NULL) &&
        (pPrevLdpSession != pLdpSession) &&
        (SSN_GET_ENTITY (pLdpSession)->u2LabelRet == LDP_CONSERVATIVE_MODE))
    {
        if ((LCB_USSN (pPrevLspCtrlBlk) != NULL) &&
            (pLdpSession == LCB_USSN (pPrevLspCtrlBlk)))
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                                   pPrevLspCtrlBlk, NULL);

            LdpSendLblRelMsg (&LCB_FEC (pPrevLspCtrlBlk),
                              &LCB_DLBL (pPrevLspCtrlBlk),
                              LCB_DSSN (pPrevLspCtrlBlk));

            LdpSendLblWithdrawMsg (&LCB_FEC (pPrevLspCtrlBlk),
                                   &LCB_ULBL (pPrevLspCtrlBlk),
                                   LCB_USSN (pPrevLspCtrlBlk));
            LCB_STATE (pPrevLspCtrlBlk) = LDP_LSM_ST_REL_AWT;
        }

        return;
    }
#if 0
    printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

    /* 
     * Get a new LSP Control block. initialise it with the relevant
     * parameters. 
     */

    pLspCtrlBlk = (tLspCtrlBlock *) MemAllocMemBlk (LDP_LSP_POOL_ID);
    LDP_DBG3 (LDP_ADVT_MEM,
              "ADVT: %s: %d Memory allocated for Control Block pLspCtrlBlock=0x%x\n",
              __FUNCTION__, __LINE__, pLspCtrlBlk);

    if (pLspCtrlBlk == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: MemAlloc Failed for LCB - IntLspSetReq\n");
        return;
    }

    pLspTrigCtrlBlock = (tLspTrigCtrlBlock *) MemAllocMemBlk (LDP_TRIG_POOL_ID);
    LDP_DBG3 (LDP_ADVT_MEM,
              "ADVT: %s: %d Memory allocated for Trig Control Block pNHTrggCtrlBlk=0x%x\n",
              __FUNCTION__, __LINE__, pLspTrigCtrlBlock);

    if (pLspTrigCtrlBlock == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: MemAlloc Failed for TrgCB - IntLspSetReq\n");
        MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlk);
        LDP_DBG3 (LDP_ADVT_MEM,
                  "ADVT: %s: %d Memory release for Control Block pLspCtrlBlock=0x%x\n",
                  __FUNCTION__, __LINE__, pLspCtrlBlk);

#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

        return;
    }

    MEMSET (pLspTrigCtrlBlock, 0, sizeof (tLspTrigCtrlBlock));

    /* Contents of the LSP Control block are suitably initialised */
    LdpInitLspCtrlBlock (pLspCtrlBlk);

    LdpMapCtrlBlkAndDnStrSession (pIntLspSetupInfo, pLspCtrlBlk,
                                  pLspTrigCtrlBlock);
#if 0
    printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

    /* Label request message has been recieved before 
     * Down stream session is established. That Label request message
     * has to be forwarded when the session is available
     * */

    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (MPLS_DEF_INCARN);

    TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
    {
        TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex,
                              pSsnHashNode, tTMO_HASH_NODE *)
        {
            pLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);

            if (pIntLspSetupInfo->pLdpSession == pLdpSession)
            {
#if 0
                printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

                continue;
            }

            pLspCtrlBlkList = &SSN_ULCB (pLdpSession);

            TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
            {
                pTempLspCtrlBlk = (tLspCtrlBlock *) pSllNode;
#if 0
                printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

                if ((LCB_STATE (pTempLspCtrlBlk) == LDP_LSM_ST_IDLE) &&
                    (LdpIsAddressMatchWithAddrType
                     (&pTempLspCtrlBlk->Fec.Prefix, &pIntLspSetupInfo->Dest,
                      pTempLspCtrlBlk->Fec.u2AddrFmly,
                      pIntLspSetupInfo->u2AddrType) == LDP_TRUE)
                    && (pTempLspCtrlBlk->Fec.u1PreLen ==
                        pIntLspSetupInfo->u1PrefixLen))
                {
#if 0
                    printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

                    LdpMapCtrlBlkAndDnStrSession (pIntLspSetupInfo,
                                                  pTempLspCtrlBlk, NULL);
                }
                else if ((LCB_STATE (pTempLspCtrlBlk) == LDP_LSM_ST_EST)
                         &&
                         (LdpIsAddressMatchWithAddrType
                          (&pTempLspCtrlBlk->Fec.Prefix,
                           &pIntLspSetupInfo->Dest,
                           pTempLspCtrlBlk->Fec.u2AddrFmly,
                           pIntLspSetupInfo->u2AddrType) == LDP_TRUE)
                         && (pTempLspCtrlBlk->Fec.u1PreLen ==
                             pIntLspSetupInfo->u1PrefixLen)
                         &&
                         ((LdpIsLsrEgress
                           ((tFec *) & pTempLspCtrlBlk->Fec, &u1RouteStatus,
                            &NextHopAddr, &u4IfIndex,
                            (UINT2) SSN_GET_INCRN_ID (pLdpSession))) ==
                          LDP_FALSE)
                         && (LCB_DLBL (pTempLspCtrlBlk).u4GenLbl ==
                             LDP_INVALID_LABEL))
                {
                    LdpMapCtrlBlkNoStatChngDnStrSsn (pIntLspSetupInfo,
                                                     pTempLspCtrlBlk, NULL);
                }
            }
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpMapCtrlBlkNoStatChngDnStrSsn                           */
/* Description   : This routine maps the control block with the down stream  */
/*                 session to send/forward the label request message         */
/*                 without changing the state of control block               */
/*                                                                           */
/* Input(s)      : pIntLspSetupInfo  -  Pointer to the structure holding     */
/*                                      route Information to set up Lsp.     */
/*                 pLspCtrlBlk       - Control block                         */
/*                 pLspCtrlBlk       - Triggered control block for ingress   */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMapCtrlBlkNoStatChngDnStrSsn (tIntLspSetupInfo * pIntLspSetupInfo,
                                 tLspCtrlBlock * pLspCtrlBlk,
                                 tLspTrigCtrlBlock * pLspTrigCtrlBlock)
{
    UINT1               u1PrefLen = pIntLspSetupInfo->u1PrefixLen;
    tLdpSession        *pLdpSession = pIntLspSetupInfo->pLdpSession;
    UINT1               u1Event;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
#endif

#if 0
    printf ("%s : Entry : %d \n", __FUNCTION__, __LINE__);
#endif

    LDP_DBG1 (LDP_ADVT_PRCS, "ADVT: %s : Entry\n", __FUNCTION__);

#ifdef CFA_WANTED
    /* Create a MPLS Tunnel interface at CFA and Stack over the MPLS
     * interface (which is stacked over an L3IPVLAN interface)
     * and use that interface index  as OutIfIndex
     *       ____________
     *      |   L3 MPLS  |  ---> To be created now
     *      |____________|
     *      |   MPLS     |  ---> Already created during Init
     *      |____________|
     *      |  L3 IPVLAN |
     *      |____________|
     */
    if (CfaIfmCreateStackMplsTunnelInterface
        (LdpSessionGetIfIndex (pLdpSession, pIntLspSetupInfo->u2AddrType),
         &u4MplsTnlIfIndex) == CFA_FAILURE)
    {

        LDP_DBG (LDP_ADVT_MEM,
                 " LdpIntLspSetReq - ADVT: Mplstunnel IF creation failed for "
                 " LSP Cntl block in LdpNonMrgIntLspSetReq\n");
        if (pLspTrigCtrlBlock != NULL)
        {
            MemReleaseMemBlock (LDP_TRIG_POOL_ID, (UINT1 *) pLspTrigCtrlBlock);
            LDP_DBG3 (LDP_ADVT_MEM,
                      "ADVT: %s: %d Memory release for Trig Control Block pNHTrggCtrlBlk=0x%x\n",
                      __FUNCTION__, __LINE__, pLspTrigCtrlBlock);
        }
        MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlk);
        LDP_DBG3 (LDP_ADVT_MEM,
                  "ADVT: %s: %d Memory release for Control Block pLspCtrlBlock=0x%x\n",
                  __FUNCTION__, __LINE__, pLspCtrlBlk);

        LDP_DBG1 (LDP_ADVT_PRCS, "ADVT: %s : Exit\n", __FUNCTION__);
        return;
    }
#if 0
    printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

#if MPLS_IPV6_WANTED
    if (pIntLspSetupInfo->u2AddrType == LDP_ADDR_TYPE_IPV6)
    {

#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

        LDP_DBG6 (LDP_ADVT_PRCS,
                  "LdpIntLspSetReq: OutInt %d created for "
                  "FEC: %s with Peer %d.%d.%d.%d\n",
                  u4MplsTnlIfIndex,
                  Ip6PrintAddr (&pIntLspSetupInfo->Dest.Ip6Addr),
                  pLdpSession->pLdpPeer->PeerLdpId[0],
                  pLdpSession->pLdpPeer->PeerLdpId[1],
                  pLdpSession->pLdpPeer->PeerLdpId[2],
                  pLdpSession->pLdpPeer->PeerLdpId[3]);
    }
    else
#endif
    {

#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

        LDP_DBG6 (LDP_ADVT_PRCS,
                  "LdpIntLspSetReq: OutInt %d created for "
                  "FEC: %x with Peer %d.%d.%d.%d\n",
                  u4MplsTnlIfIndex, LDP_IPV4_U4_ADDR (pIntLspSetupInfo->Dest),
                  pLdpSession->pLdpPeer->PeerLdpId[0],
                  pLdpSession->pLdpPeer->PeerLdpId[1],
                  pLdpSession->pLdpPeer->PeerLdpId[2],
                  pLdpSession->pLdpPeer->PeerLdpId[3]);
    }

    pLspCtrlBlk->u4OutIfIndex = u4MplsTnlIfIndex;
#else
    pLspCtrlBlk->u4OutIfIndex =
        LdpSessionGetIfIndex (pLdpSession, pIntLspSetupInfo->u2AddrType);
#endif
    LdpCopyAddr (&pLspCtrlBlk->NextHopAddr, &pIntLspSetupInfo->NextHop,
                 pIntLspSetupInfo->u2AddrType);
    pLspCtrlBlk->u2AddrType = pIntLspSetupInfo->u2AddrType;

    LdpCopyAddr (&pLspCtrlBlk->Fec.Prefix, &pIntLspSetupInfo->Dest,
                 pIntLspSetupInfo->u2AddrType);

    pLspCtrlBlk->pDStrSession = pLdpSession;

    if (SSN_GET_LBL_TYPE (pLdpSession) == LDP_ATM_MODE)
    {
        pLspCtrlBlk->u1LblType = LDP_ATM_LABEL;
    }
    else if (SSN_GET_LBL_TYPE (pLdpSession) == LDP_GEN_MODE)
    {
        pLspCtrlBlk->u1LblType = LDP_GEN_LABEL;
    }

    pLspCtrlBlk->Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
    if (pLspTrigCtrlBlock != NULL)
    {
        pLspTrigCtrlBlock->u1TrgType = IPPRFX_FEC_LSP_SETUP;
        pLspCtrlBlk->pTrigCtrlBlk = pLspTrigCtrlBlock;
    }
    pLspCtrlBlk->Fec.u1PreLen = u1PrefLen;
    pLspCtrlBlk->Fec.u2AddrFmly = pIntLspSetupInfo->u2AddrType;
    pLspCtrlBlk->pCrlspTnlInfo = NULL;

    /*
     *          * Adding the Lsp Control Block for which the current Session is acting as
     *                   * DownStream.
     *                            */
    TMO_SLL_Add ((tTMO_SLL *) (&(SSN_DLCB (pLdpSession))),
                 (tTMO_SLL_NODE *) (&(pLspCtrlBlk->NextDSLspCtrlBlk)));

    if (pLspTrigCtrlBlock != NULL)
    {
#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

        u1Event = LDP_LSM_EVT_INT_SETUP;
        LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)][u1Event] (pLspCtrlBlk, NULL);
    }
    else
    {

#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

        LdpSendLblReqMsg (pLspCtrlBlk, pLspCtrlBlk->u1HopCount,
                          (pLspCtrlBlk->pu1PVTlv + LDP_TLV_HDR_LEN),
                          pLspCtrlBlk->u1PVCount, NULL, LDP_ZERO);
    }
    LDP_DBG1 (LDP_ADVT_PRCS, "ADVT: %s : Exit\n", __FUNCTION__);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMapCtrlBlkAndDnStrSession                              */
/* Description   : This routine maps the control block with the down stream  */
/*                 session to send/forward the label request message         */
/*                                                                           */
/* Input(s)      : pIntLspSetupInfo  -  Pointer to the structure holding     */
/*                                      route Information to set up Lsp.     */
/*                 pLspCtrlBlk       - Control block                         */
/*                 pLspCtrlBlk       - Triggered control block for ingress   */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMapCtrlBlkAndDnStrSession (tIntLspSetupInfo * pIntLspSetupInfo,
                              tLspCtrlBlock * pLspCtrlBlk,
                              tLspTrigCtrlBlock * pLspTrigCtrlBlock)
{
    UINT1               u1PrefLen = pIntLspSetupInfo->u1PrefixLen;
    tLdpSession        *pLdpSession = pIntLspSetupInfo->pLdpSession;
    UINT1               u1Event;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
#endif

#if 0
    printf ("%s : Entry : %d \n", __FUNCTION__, __LINE__);
#endif

    LDP_DBG1 (LDP_ADVT_PRCS, "ADVT: %s : Entry\n", __FUNCTION__);

#ifdef CFA_WANTED
    /* Create a MPLS Tunnel interface at CFA and Stack over the MPLS
     * interface (which is stacked over an L3IPVLAN interface)
     * and use that interface index  as OutIfIndex
     *       ____________
     *      |   L3 MPLS  |  ---> To be created now
     *      |____________|
     *      |   MPLS     |  ---> Already created during Init
     *      |____________| 
     *      |  L3 IPVLAN |
     *      |____________|
     */
    if (CfaIfmCreateStackMplsTunnelInterface
        (LdpSessionGetIfIndex (pLdpSession, pIntLspSetupInfo->u2AddrType),
         &u4MplsTnlIfIndex) == CFA_FAILURE)
    {

        LDP_DBG (LDP_ADVT_MEM,
                 " LdpIntLspSetReq - ADVT: Mplstunnel IF creation failed for "
                 " LSP Cntl block in LdpNonMrgIntLspSetReq\n");
        if (pLspTrigCtrlBlock != NULL)
        {
            MemReleaseMemBlock (LDP_TRIG_POOL_ID, (UINT1 *) pLspTrigCtrlBlock);
            LDP_DBG3 (LDP_ADVT_MEM,
                      "ADVT: %s: %d Memory release for Trig Control Block pNHTrggCtrlBlk=0x%x\n",
                      __FUNCTION__, __LINE__, pLspTrigCtrlBlock);
        }
        MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlk);
        LDP_DBG3 (LDP_ADVT_MEM,
                  "ADVT: %s: %d Memory release for Control Block pLspCtrlBlock=0x%x\n",
                  __FUNCTION__, __LINE__, pLspCtrlBlk);

        LDP_DBG1 (LDP_ADVT_PRCS, "ADVT: %s : Exit\n", __FUNCTION__);
        return;
    }
#if 0
    printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

#if MPLS_IPV6_WANTED
    if (pIntLspSetupInfo->u2AddrType == LDP_ADDR_TYPE_IPV6)
    {

#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

        LDP_DBG6 (LDP_ADVT_PRCS,
                  "LdpIntLspSetReq: OutInt %d created for "
                  "FEC: %s with Peer %d.%d.%d.%d\n",
                  u4MplsTnlIfIndex,
                  Ip6PrintAddr (&pIntLspSetupInfo->Dest.Ip6Addr),
                  pLdpSession->pLdpPeer->PeerLdpId[0],
                  pLdpSession->pLdpPeer->PeerLdpId[1],
                  pLdpSession->pLdpPeer->PeerLdpId[2],
                  pLdpSession->pLdpPeer->PeerLdpId[3]);
    }
    else
#endif
    {

#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

        LDP_DBG6 (LDP_ADVT_PRCS,
                  "LdpIntLspSetReq: OutInt %d created for "
                  "FEC: %x with Peer %d.%d.%d.%d\n",
                  u4MplsTnlIfIndex, LDP_IPV4_U4_ADDR (pIntLspSetupInfo->Dest),
                  pLdpSession->pLdpPeer->PeerLdpId[0],
                  pLdpSession->pLdpPeer->PeerLdpId[1],
                  pLdpSession->pLdpPeer->PeerLdpId[2],
                  pLdpSession->pLdpPeer->PeerLdpId[3]);
    }

    pLspCtrlBlk->u4OutIfIndex = u4MplsTnlIfIndex;
#else
    pLspCtrlBlk->u4OutIfIndex =
        LdpSessionGetIfIndex (pLdpSession, pIntLspSetupInfo->u2AddrType);
#endif
    LdpCopyAddr (&pLspCtrlBlk->NextHopAddr, &pIntLspSetupInfo->NextHop,
                 pIntLspSetupInfo->u2AddrType);
    pLspCtrlBlk->u2AddrType = pIntLspSetupInfo->u2AddrType;

    LdpCopyAddr (&pLspCtrlBlk->Fec.Prefix, &pIntLspSetupInfo->Dest,
                 pIntLspSetupInfo->u2AddrType);

    pLspCtrlBlk->pDStrSession = pLdpSession;
    pLspCtrlBlk->u1LspState = LDP_LSM_ST_IDLE;

    if (SSN_GET_LBL_TYPE (pLdpSession) == LDP_ATM_MODE)
    {
        pLspCtrlBlk->u1LblType = LDP_ATM_LABEL;
    }
    else if (SSN_GET_LBL_TYPE (pLdpSession) == LDP_GEN_MODE)
    {
        pLspCtrlBlk->u1LblType = LDP_GEN_LABEL;
    }

    pLspCtrlBlk->Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
    if (pLspTrigCtrlBlock != NULL)
    {
        pLspTrigCtrlBlock->u1TrgType = IPPRFX_FEC_LSP_SETUP;
        pLspCtrlBlk->pTrigCtrlBlk = pLspTrigCtrlBlock;
    }
    pLspCtrlBlk->Fec.u1PreLen = u1PrefLen;
    pLspCtrlBlk->Fec.u2AddrFmly = pIntLspSetupInfo->u2AddrType;
    pLspCtrlBlk->pCrlspTnlInfo = NULL;

    /* 
     * Adding the Lsp Control Block for which the current Session is acting as 
     * DownStream.
     */
    TMO_SLL_Add ((tTMO_SLL *) (&(SSN_DLCB (pLdpSession))),
                 (tTMO_SLL_NODE *) (&(pLspCtrlBlk->NextDSLspCtrlBlk)));

    if (pLspTrigCtrlBlock != NULL)
    {

#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

        u1Event = LDP_LSM_EVT_INT_SETUP;
        LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)][u1Event] (pLspCtrlBlk, NULL);
    }
    else
    {

#if 0
        printf ("%s : %d \n", __FUNCTION__, __LINE__);
#endif

        LdpSendLblReqMsg (pLspCtrlBlk, pLspCtrlBlk->u1HopCount,
                          (pLspCtrlBlk->pu1PVTlv + LDP_TLV_HDR_LEN),
                          pLspCtrlBlk->u1PVCount, NULL, LDP_ZERO);
        LCB_STATE (pLspCtrlBlk) = LDP_LSM_ST_RSP_AWT;
    }
    LDP_DBG1 (LDP_ADVT_PRCS, "ADVT: %s : Exit\n", __FUNCTION__);
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgIntLspSetReq                                     */
/* Description   : This routine is called when a new LSP is to be created in */
/*                 LSR. The LSR obtains information from the IP routing table*/
/*                 for a destination. The LSP is created for the destination.*/
/*                                                                           */
/* Input(s)      : u2IncarnId      - Incarnation Identifier                  */
/*                 NetworkAddr     - Network address to which the LSP is     */
/*                                   to be created.                          */
/*                 u1PrefLen       - Prefix length in the NetworkAddr        */
/*                 NextHopAddr     - Next hop address to reach the network   */
/*                                   indicated by the NetworkAddr            */
/*                 u4IfIndex       - Outgoing interface to rech the network  */
/*                                   indicated by teh NetworkAddr            */
/*                 pFecTableEntry -  pointer to tFecTableEntry               */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgIntLspSetReq (UINT2 u2IncarnId,
                       tIpv4Addr NetworkAddr,
                       UINT1 u1PrefLen,
                       tIpv4Addr NextHopAddr,
                       UINT4 u4IfIndex, tFecTableEntry * pFecTableEntry)
{
    UINT1               u1Event;
    UINT4               u4HIndex;
    UINT4               u4NextHopAddr =
        OSIX_NTOHL (*(UINT4 *) (VOID *) (NextHopAddr));
    UINT4               u4TempNextHopAddr;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tLspTrigCtrlBlock  *pLspTrigCtrlBlock = NULL;

    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
#endif
    tLdpSession        *pPrevLdpSession = NULL;
    tLspCtrlBlock      *pPrevLspCtrlBlk = NULL;
    tGenU4Addr          Prefix;

    MEMSET (&Prefix, LDP_ZERO, sizeof (tGenU4Addr));
    UNUSED_PARAM (u4IfIndex);
    /* 
     * Get the session that is associated with the NextHopAddr & ifIndex. 
     * The peer that has advertised the NextHopAddr in its LDP Addrewss message
     * is located. If no such peer exists, the LSP cannot be initiated.
     * If the peer exists, the session associated with the peer is accessed
     * for further processing.
     */

    LDP_DBG2 (LDP_DBG_PRCS, "%s:%d Entry\n", __FUNCTION__, __LINE__);

    u4HIndex = LDP_COMPUTE_HASH_INDEX (u4NextHopAddr);

    TMO_HASH_Scan_Bucket (LDP_PEER_IFADR_TABLE (u2IncarnId), u4HIndex,
                          pIfAddrNode, tPeerIfAdrNode *)
    {
        if (pIfAddrNode->AddrType != MPLS_IPV4_ADDR_TYPE)
        {
            continue;
        }

        u4TempNextHopAddr =
            OSIX_NTOHL (*((UINT4 *) (VOID *) (&(pIfAddrNode->IfAddr))));
        if (u4TempNextHopAddr == u4NextHopAddr)
        {
            pLdpSession = pIfAddrNode->pSession;
            break;
        }
    }

    if (pLdpSession == NULL)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: No Ssn Found with given NH Addr - IntLspSetReq\n");
        return;
    }

    LDP_IPV4_U4_ADDR (Prefix.Addr) =
        (OSIX_NTOHL (*(UINT4 *) (VOID *) (NetworkAddr)));
    Prefix.u2AddrType = LDP_ADDR_TYPE_IPV4;

    LdpGetPrevLdpSession (u2IncarnId, &Prefix, u1PrefLen,
                          &pPrevLspCtrlBlk, &pPrevLdpSession);

    if ((pPrevLspCtrlBlk != NULL) &&
        (pPrevLdpSession != NULL) &&
        (pPrevLdpSession != pLdpSession) &&
        (SSN_GET_ENTITY (pLdpSession)->u2LabelRet == LDP_CONSERVATIVE_MODE))
    {
        if ((LCB_USSN (pPrevLspCtrlBlk) != NULL) &&
            (pLdpSession == LCB_USSN (pPrevLspCtrlBlk)))
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                                   pPrevLspCtrlBlk, NULL);

            LdpSendLblRelMsg (&LCB_FEC (pPrevLspCtrlBlk),
                              &LCB_DLBL (pPrevLspCtrlBlk),
                              LCB_DSSN (pPrevLspCtrlBlk));

            LdpSendLblWithdrawMsg (&LCB_FEC (pPrevLspCtrlBlk),
                                   &LCB_ULBL (pPrevLspCtrlBlk),
                                   LCB_USSN (pPrevLspCtrlBlk));
            LCB_STATE (pPrevLspCtrlBlk) = LDP_LSM_ST_REL_AWT;
        }

        LDP_DBG2 (LDP_DBG_PRCS, "%s:%d Exit\n", __FUNCTION__, __LINE__);
        return;
    }

    /* Check if already it is a ingress for the session */
    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                      pTempLdpSllNode, tTMO_SLL_NODE *)
    {
        pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

        if ((LdpIsAddressMatchWithAddrType
             (&Prefix.Addr, &pLspCtrlBlk->Fec.Prefix, Prefix.u2AddrType,
              pLspCtrlBlk->Fec.u2AddrFmly) == LDP_TRUE)
            && (u1PrefLen == pLspCtrlBlk->Fec.u1PreLen)
            && (pLspCtrlBlk->pTrigCtrlBlk != NULL))
        {
            LDP_DBG1 (LDP_ADVT_PRCS, "LdpNonMrgIntLspSetReq: "
                      "Already it is ingress for the FEC:%x u4Prefix. "
                      "Not initiating the setup request now\n",
                      LDP_IPV4_U4_ADDR (Prefix.Addr));
            LDP_DBG1 (LDP_DBG_PRCS, "FTN Create - For the Prefix: %x Fec: %x\n",
                      LDP_IPV4_U4_ADDR (Prefix.Addr));
            LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE, MPLS_OPR_PUSH,
                                   pLspCtrlBlk, NULL);

            if (LCB_USSN (pLspCtrlBlk) != NULL)
            {
                LDP_DBG1 (LDP_DBG_PRCS,
                          "ILM Create - For the Prefix: %x Fec: %x\n",
                          LDP_IPV4_U4_ADDR (Prefix.Addr));
                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP_PUSH,
                                       pLspCtrlBlk, NULL);
            }
            return;
        }
    }

    /* 
     * Get a new LSP Control block. initialise it with the relevant
     * parameters. 
     */

    pLspCtrlBlk = (tLspCtrlBlock *) MemAllocMemBlk (LDP_LSP_POOL_ID);
    LDP_DBG3 (LDP_ADVT_MEM,
              "ADVT: %s: %d Memory allocated for Control Block pLspCtrlBlock=0x%x\n",
              __FUNCTION__, __LINE__, pLspCtrlBlk);

    if (pLspCtrlBlk == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: MemAlloc Failed for LCB - IntLspSetReq\n");
        return;
    }

    pLspTrigCtrlBlock = (tLspTrigCtrlBlock *) MemAllocMemBlk (LDP_TRIG_POOL_ID);
    LDP_DBG3 (LDP_ADVT_MEM,
              "ADVT: %s: %d Memory allocated for Trig Control Block pNHTrggCtrlBlk=0x%x\n",
              __FUNCTION__, __LINE__, pLspTrigCtrlBlock);

    if (pLspTrigCtrlBlock == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: MemAlloc Failed for TrgCB - IntLspSetReq\n");
        MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlk);
        LDP_DBG3 (LDP_ADVT_MEM,
                  "ADVT: %s: %d Memory release for Control Block pLspCtrlBlock=0x%x\n",
                  __FUNCTION__, __LINE__, pLspCtrlBlk);

        return;
    }

    MEMSET (pLspTrigCtrlBlock, 0, sizeof (tLspTrigCtrlBlock));

    /* Contents of the LSP Control block are suitably initialised */
    LdpInitLspCtrlBlock (pLspCtrlBlk);
#ifdef CFA_WANTED
    /* Create a MPLS Tunnel interface at CFA  and Stack over the MPLS
     * l3 interface (which is stacked over an L3IPVLAN interface,
     * and use the interface index  returned as OutIfIndex
     */
    if (CfaIfmCreateStackMplsTunnelInterface
        (LdpSessionGetIfIndex (pLdpSession, LDP_ADDR_TYPE_IPV4),
         &u4MplsTnlIfIndex) == CFA_FAILURE)
    {

        LDP_DBG (LDP_ADVT_MEM,
                 "LdpNonMrgIntLspSetReq - ADVT: Mplstunnel IF creation failed"
                 " for LSP Cntl block\n");
        MemReleaseMemBlock (LDP_TRIG_POOL_ID, (UINT1 *) pLspTrigCtrlBlock);
        LDP_DBG3 (LDP_ADVT_MEM,
                  "ADVT: %s: %d Memory release for Trig Control Block pNHTrggCtrlBlk=0x%x\n",
                  __FUNCTION__, __LINE__, pLspTrigCtrlBlock);

        MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlk);
        LDP_DBG3 (LDP_ADVT_MEM,
                  "ADVT: %s: %d Memory release for Control Block pLspCtrlBlock=0x%x\n",
                  __FUNCTION__, __LINE__, pLspCtrlBlk);

        return;

    }

#ifdef MPLS_IPV6_WANTED
    if (pLspCtrlBlk->Fec.u2AddrFmly == LDP_ADDR_TYPE_IPV6)
    {
        LDP_DBG6 (LDP_ADVT_PRCS,
                  "LdpNonMrgIntLspSetReq: OutInt %d created for "
                  "FEC: %s with Peer %d.%d.%d.%d\n",
                  u4MplsTnlIfIndex,
                  Ip6PrintAddr (&pLspCtrlBlk->Fec.Prefix.Ip6Addr),
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);
    }
    else
#endif
    {
        LDP_DBG6 (LDP_ADVT_PRCS,
                  "LdpNonMrgIntLspSetReq: OutInt %d created for "
                  "FEC: %s with Peer %d.%d.%d.%d\n",
                  u4MplsTnlIfIndex, LDP_IPV4_U4_ADDR (pLspCtrlBlk->Fec.Prefix),
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                  pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);

    }

    pLspCtrlBlk->u4OutIfIndex = u4MplsTnlIfIndex;
#else
    pLspCtrlBlk->u4OutIfIndex =
        LdpSessionGetIfIndex (pLdpSession, LDP_ADDR_TYPE_IPV4);
#endif
    LDP_IPV4_U4_ADDR (pLspCtrlBlk->NextHopAddr) = u4NextHopAddr;
    pLspCtrlBlk->u2AddrType = LDP_ADDR_TYPE_IPV4;
    pLspCtrlBlk->u1LblType = SSN_GET_LBL_TYPE (pLdpSession);
    pLspCtrlBlk->pDStrSession = pLdpSession;
    pLspCtrlBlk->u1LspState = LDP_LSM_ST_IDLE;

    /* LSP Control Block is linked to the Fec Table if this LSP is 
     * triggered from the SNMP manager 
     */
    if (pFecTableEntry != NULL)
    {
        pLspCtrlBlk->u1IsStaticLsp = LDP_TRUE;
        pFecTableEntry->pLspCtrlBlock = pLspCtrlBlk;
    }

    pLspCtrlBlk->Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
    pLspTrigCtrlBlock->u1TrgType = IPPRFX_FEC_LSP_SETUP;
    pLspCtrlBlk->Fec.u1PreLen = u1PrefLen;
    pLspCtrlBlk->Fec.u2AddrFmly = IPV4;
    if (pFecTableEntry != NULL)
    {
        pLspCtrlBlk->Fec.u4Index = pFecTableEntry->u4FecIndex;
    }
    LDP_IPV4_U4_ADDR (pLspCtrlBlk->Fec.Prefix) =
        OSIX_NTOHL (*(UINT4 *) (VOID *) (NetworkAddr));
    pLspCtrlBlk->pTrigCtrlBlk = pLspTrigCtrlBlock;

    TMO_SLL_Add ((tTMO_SLL *) (&(SSN_DLCB (pLdpSession))),
                 (tTMO_SLL_NODE *) (&(pLspCtrlBlk->NextDSLspCtrlBlk)));

    u1Event = LDP_LSM_EVT_INT_SETUP;
    LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)][u1Event] (pLspCtrlBlk, NULL);
}

/*****************************************************************************/
/* Function Name : LdpInvCorrStMh                                            */
/* Description   : This F'n called to invoke the corresponding state machine */
/*                 based on session advert mode and merge type               */
/* Input(s)      : pSessionEntry   - Pointer to the Ldp Session              */
/*                 pCtrlBlock      - Pointer to the Control Block            */
/*                 u1Event         - event posted to the state machine       */
/*                 pMsgInfo        - Pointer to the received message in DoD  */
/*                                   NonMerge                                */
/*                 u1IsUS          - LDP_TRUE/LDP_FALSE if Merge             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpInvCorrStMh (tLdpSession * pSessionEntry, VOID *pCtrlBlock, UINT1 u1Event,
                tLdpMsgInfo * pMsg, UINT1 u1IsUS)
{
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tUstrLspCtrlBlock  *pUsLspCtrlBlock = NULL;
    UINT1              *pu1Msg = NULL;

    LDP_DBG4 (LDP_ADVT_PRCS, "%s: %d: Ssn Mrg Type: %u, Ssn Adv Type: %u\n",
              __func__, __LINE__, SSN_MRGTYPE (pSessionEntry),
              SSN_ADVTYPE (pSessionEntry));

    if (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG)
    {
        pLspCtrlBlock = (tLspCtrlBlock *) pCtrlBlock;
        pu1Msg = pMsg->pu1Msg;

        LDP_DBG4 (LDP_ADVT_PRCS, "%s: %d Control Block State: %d, Event: %u\n",
                  __func__, __LINE__, LCB_STATE (pLspCtrlBlock), u1Event);
    }
    else
    {
        if (u1IsUS == LDP_TRUE)
        {
            pUsLspCtrlBlock = (tUstrLspCtrlBlock *) pCtrlBlock;
        }
        else
        {
            pLspCtrlBlock = (tLspCtrlBlock *) pCtrlBlock;
            pu1Msg = pMsg->pu1Msg;
            LDP_DBG4 (LDP_ADVT_PRCS,
                      "%s: %d Control Block State: %u, Event: %u\n", __func__,
                      __LINE__, LCB_STATE (pLspCtrlBlock), u1Event);
        }
    }
    if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND) &&
        (SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG))
    {
        LDP_DBG4 (LDP_ADVT_PRCS, "%s: %d Control Block State: %u, Event: %d\n",
                  __func__, __LINE__, LCB_STATE (pLspCtrlBlock), u1Event);

        LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlock)][u1Event]
            (pLspCtrlBlock, pu1Msg);
    }
    else if (SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND &&
             ((SSN_MRGTYPE (pSessionEntry) == LDP_VC_MRG) ||
              (SSN_MRGTYPE (pSessionEntry) == LDP_VP_MRG)))
    {
        LDP_DBG3 (LDP_ADVT_PRCS, "%s: %d  Event: %u\n",
                  __func__, __LINE__, u1Event);

        if ((u1IsUS == LDP_TRUE) && (pUsLspCtrlBlock != NULL))
        {
            LdpMergeUpSm (pUsLspCtrlBlock, u1Event, pMsg);
        }
        else if (pLspCtrlBlock != NULL)
        {
            LdpMergeDnSm (pLspCtrlBlock, u1Event, pMsg);
        }
    }
    else if (SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_UNSOLICIT)
    {
        LDP_DBG3 (LDP_ADVT_PRCS, "%s: %d  Event: %u\n",
                  __func__, __LINE__, u1Event);
        if ((u1IsUS == LDP_TRUE) && (pUsLspCtrlBlock != NULL))
        {
            LdpDuUpSm (pUsLspCtrlBlock, u1Event, pMsg);
        }
        else if (pLspCtrlBlock != NULL)
        {
            LdpDuDnSm (pLspCtrlBlock, u1Event, pMsg);
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpCheckMapAttributes                                     */
/* Description   : This F'n called to check whether the atributes            */
/*                 have changed or not                                       */
/* Input(s)      : pu1PvTlv        - Pointer to the path vector TLV          */
/*                 u1HopCount      - Contains the Hop count                  */
/*                 pLspCtrlBlock   - Points to the control block             */
/*                 tLdpMsgInfo     - pMsg containing the Attrib              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
UINT1
LdpCheckMapAttributes (UINT1 *pu1PvTlv, UINT1 u1HopCount,
                       tLspCtrlBlock * pLspCtrlBlock, tLdpMsgInfo * pMsg)
{
    if ((SSN_ADVTYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_DSTR_ON_DEMAND) &&
        ((SSN_MRGTYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_VC_MRG) ||
         (SSN_MRGTYPE (LCB_DSSN (pLspCtrlBlock)) == LDP_VP_MRG)))
    {
        if (pMsg == NULL)
        {
            return LDP_FALSE;
        }
        if (((LCB_HCOUNT (pLspCtrlBlock) != LDP_MSG_HC (pMsg)) &&
             (LDP_MSG_HC (pMsg) != LDP_ZERO)) ||
            (LDP_MSG_PV_PTR (pMsg) != NULL))
        {
            return LDP_TRUE;
        }
        else
        {
            return LDP_FALSE;
        }
    }
    else
    {
        if ((pu1PvTlv == NULL) && (u1HopCount == LCB_HCOUNT (pLspCtrlBlock)))
        {
            return LDP_FALSE;
        }
    }
    return LDP_TRUE;
}

/*****************************************************************************/
/* Function Name : LdpGetPrevLdpSession                                      */
/* Description   : This function checks if any DLCB is present for this FEC  */
/*                 in any other session and if so returns that DLCB and      */
/*                 that Session                                              */
/* Input(s)      : u2IncarnId        - Incarn Id                             */
/*                 u4Prefix          - Destination Prefix                    */
/*                 u1PrefLen         - Prefix Length                         */
/* Output(s)     : ppPrevLspCtrlBlk  - Previous DLCB                         */
/*                 ppPrevLdpSession  - Previous LDP Session                  */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpGetPrevLdpSession (UINT2 u2IncarnId, tGenU4Addr * pPrefix, UINT1 u1PrefLen,
                      tLspCtrlBlock ** ppPrevLspCtrlBlk,
                      tLdpSession ** ppPrevLdpSession)
{
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tTMO_HASH_NODE     *pSsnHashNode = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tLdpSession        *pPrevLdpSession = NULL;
    tLspCtrlBlock      *pPrevLspCtrlBlk = NULL;
    UINT4               u4HIndex = 0;

    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

    TMO_HASH_Scan_Table (pTcpConnHshTbl, u4HIndex)
    {
        TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex,
                              pSsnHashNode, tTMO_HASH_NODE *)
        {
            pPrevLdpSession = LDP_SSN_OFFSET_HTBL (pSsnHashNode);

            TMO_DYN_SLL_Scan (&SSN_DLCB (pPrevLdpSession), pLdpSllNode,
                              pTempLdpSllNode, tTMO_SLL_NODE *)
            {
                pPrevLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

                if ((LdpIsAddressMatchWithAddrType
                     (&pPrefix->Addr, &pPrevLspCtrlBlk->Fec.Prefix,
                      pPrefix->u2AddrType,
                      pPrevLspCtrlBlk->Fec.u2AddrFmly) == LDP_TRUE)
                    && (u1PrefLen == pPrevLspCtrlBlk->Fec.u1PreLen))
                {

                    *ppPrevLdpSession = pPrevLdpSession;
                    *ppPrevLspCtrlBlk = pPrevLspCtrlBlk;
                    return;
                }

                pPrevLspCtrlBlk = NULL;
            }
            pPrevLdpSession = NULL;
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgResAwtDeleteLspCtrlBlk                           */
/* Description   : This function Delete Lsp Cntrl Blck and their corresponding */
/*                 trigg cntrl block at reponse awaited state                */
/* Input(s)      : tLspCtrlBlock        - pLspCtrlBlock                      */
/* Output(s)     : NA                                                        */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpNonMrgResAwtDeleteLspCtrlBlk (tLspCtrlBlock * pLspCtrlBlock)
{
    tLspTrigCtrlBlock  *pTrigCtrlBlk = NULL;
    LDP_DBG2 (LDP_DBG_PRCS, "%s: :%d: Entry\n", __func__, __LINE__);

    if (LCB_TRIG (pLspCtrlBlock) != NULL)
    {
        if (LCB_TRIG_TYPE (pLspCtrlBlock) == NEXT_HOP_CHNG_LSP_SET_UP)
        {
            pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
            if (NH_TRGCB_OLSP (pTrigCtrlBlk) != NULL)
            {
                LCB_NHOP (NH_TRGCB_OLSP (pTrigCtrlBlk)) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
            }
            NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
        }
        else
        {
            pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
            NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
            NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
        }
    }
    else
    {
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);

    }
    LDP_DBG2 (LDP_DBG_PRCS, "%s: :%d: Exit\n", __func__, __LINE__);
}

/*****************************************************************************/
/* Function Name : LdpNonMrgSsnDnDeleteLspCtrlBlk                            */
/* Description   : This function Delete Lsp Cntrl Blck and their corresponding */
/*                 trigg cntrl block at Estb state and Ssn down               */
/* Input(s)      : tLspCtrlBlock        - pLspCtrlBlock                      */
/* Output(s)     : NA                                                        */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpNonMrgSsnDnDeleteLspCtrlBlk (tLspCtrlBlock * pLspCtrlBlock)
{
    tLspTrigCtrlBlock  *pTrigCtrlBlk = NULL;
    tLspTrigCtrlBlock  *pNHCtrlBlk = NULL;
    LDP_DBG2 (LDP_DBG_PRCS, "%s: :%d: Entry\n", __func__, __LINE__);

    if (LCB_TRIG (pLspCtrlBlock) != NULL)
    {
        if (LCB_TRIG_TYPE (pLspCtrlBlock) == NEXT_HOP_CHNG_LSP_SET_UP)
        {
            pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
            if (NH_TRGCB_OLSP (pTrigCtrlBlk) != NULL)
            {
                LCB_NHOP (NH_TRGCB_OLSP (pTrigCtrlBlk)) = NULL;
                NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
            }
            NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
        }
        else
        {
            if (LCB_NHOP (pLspCtrlBlock) != NULL)
            {
                pNHCtrlBlk = LCB_NHOP (pLspCtrlBlock);
                NH_TRGCB_OLSP (pNHCtrlBlk) = NULL;
            }
            pTrigCtrlBlk = LCB_TRIG (pLspCtrlBlock);
            NH_TRGCB_NLSP (pTrigCtrlBlk) = NULL;
            NH_TRGCB_OLSP (pTrigCtrlBlk) = NULL;
            LdpDeleteLspCtrlBlock (pLspCtrlBlock);
            LdpDeleteTrigCtrlBlk (pTrigCtrlBlk);
        }
    }
    else
    {
        LdpDeleteLspCtrlBlock (pLspCtrlBlock);
    }
    LDP_DBG2 (LDP_DBG_PRCS, "%s: :%d: Exit\n", __func__, __LINE__);
}

#ifdef MPLS_IPV6_WANTED
VOID
LdpNonMrgIntIpv6LspSetReq (UINT2 u2IncarnId,
                           tIp6Addr * pNetworkAddr,
                           UINT1 u1PrefLen,
                           tIp6Addr * pNextHopAddr,
                           UINT4 u4IfIndex, tFecTableEntry * pFecTableEntry)
{
    UINT1               u1Event;
    UINT1               u1Ipv6HIndex;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tLspTrigCtrlBlock  *pLspTrigCtrlBlock = NULL;

    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspCtrlBlk = NULL;
    tTMO_SLL_NODE      *pTempLdpSllNode = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
#endif
    tLdpSession        *pPrevLdpSession = NULL;
    tLspCtrlBlock      *pPrevLspCtrlBlk = NULL;
    tGenU4Addr          Prefix;

    MEMSET (&Prefix, LDP_ZERO, sizeof (tGenU4Addr));
    UNUSED_PARAM (u4IfIndex);

    /* 
     * Get the session that is associated with the NextHopAddr & ifIndex. 
     * The peer that has advertised the NextHopAddr in its LDP Addrewss message
     * is located. If no such peer exists, the LSP cannot be initiated.
     * If the peer exists, the session associated with the peer is accessed
     * for further processing.
     */

    u1Ipv6HIndex = Ip6AddrHash (pNextHopAddr);

    TMO_HASH_Scan_Bucket (LDP_PEER_IPV6_IFADR_TABLE (u2IncarnId), u1Ipv6HIndex,
                          pIfAddrNode, tPeerIfAdrNode *)
    {
        pIfAddrNode = LDP_IPV6_OFFSET (pIfAddrNode);

        if (pIfAddrNode->AddrType != MPLS_IPV6_ADDR_TYPE)
        {
            continue;
        }

        if (MEMCMP (pNextHopAddr, &pIfAddrNode->IfAddr.Ip6Addr,
                    LDP_IPV6ADR_LEN) == 0)
        {
            pLdpSession = pIfAddrNode->pSession;
            break;
        }
    }

    if (pLdpSession == NULL)
    {
        LDP_DBG (LDP_ADVT_PRCS,
                 "ADVT: No Ssn Found with given NH Addr - IntLspSetReq\n");
        return;
    }

    MEMCPY (&Prefix.Addr.Ip6Addr, pNetworkAddr, LDP_IPV6ADR_LEN);
    Prefix.u2AddrType = LDP_ADDR_TYPE_IPV6;

    LdpGetPrevLdpSession (u2IncarnId, &Prefix, u1PrefLen,
                          &pPrevLspCtrlBlk, &pPrevLdpSession);

    if ((pPrevLspCtrlBlk != NULL) &&
        (pPrevLdpSession != NULL) &&
        (pPrevLdpSession != pLdpSession) &&
        (SSN_GET_ENTITY (pLdpSession)->u2LabelRet == LDP_CONSERVATIVE_MODE))
    {
        if ((LCB_USSN (pPrevLspCtrlBlk) != NULL) &&
            (pLdpSession == LCB_USSN (pPrevLspCtrlBlk)))
        {
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                                   pPrevLspCtrlBlk, NULL);

            LdpSendLblRelMsg (&LCB_FEC (pPrevLspCtrlBlk),
                              &LCB_DLBL (pPrevLspCtrlBlk),
                              LCB_DSSN (pPrevLspCtrlBlk));

            LdpSendLblWithdrawMsg (&LCB_FEC (pPrevLspCtrlBlk),
                                   &LCB_ULBL (pPrevLspCtrlBlk),
                                   LCB_USSN (pPrevLspCtrlBlk));
            LCB_STATE (pPrevLspCtrlBlk) = LDP_LSM_ST_REL_AWT;
        }

        return;
    }

    /* Check if already it is a ingress for the session */
    TMO_DYN_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode,
                      pTempLdpSllNode, tTMO_SLL_NODE *)
    {
        pLspCtrlBlk = SLL_TO_LCB (pLdpSllNode);

        if ((LdpIsAddressMatchWithAddrType
             (&Prefix.Addr, &pLspCtrlBlk->Fec.Prefix, Prefix.u2AddrType,
              pLspCtrlBlk->Fec.u2AddrFmly) == LDP_TRUE)
            && (u1PrefLen == pLspCtrlBlk->Fec.u1PreLen)
            && (pLspCtrlBlk->pTrigCtrlBlk != NULL))
        {
            LDP_DBG1 (LDP_ADVT_PRCS, "LdpNonMrgIntLspSetReq: "
                      "Already it is ingress for the FEC:%s Prefix. "
                      "Not initiating the setup request now\n",
                      Ip6PrintAddr (&Prefix.Addr.Ip6Addr));

            LDP_DBG1 (LDP_DBG_PRCS,
                      "FTN Create - For the Prefix: %s \n",
                      Ip6PrintAddr (&Prefix.Addr.Ip6Addr));

            LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_CREATE,
                                   MPLS_OPR_PUSH, pLspCtrlBlk, NULL);

            if (LCB_USSN (pLspCtrlBlk) != NULL)
            {
                LDP_DBG1 (LDP_DBG_PRCS,
                          "ILM Create - For the Prefix: %x Fec: %x\n",
                          Ip6PrintAddr (&Prefix.Addr.Ip6Addr));
                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP_PUSH,
                                       pLspCtrlBlk, NULL);
            }
            return;
        }
    }

    /* 
     * Get a new LSP Control block. initialise it with the relevant
     * parameters. 
     */

    pLspCtrlBlk = (tLspCtrlBlock *) MemAllocMemBlk (LDP_LSP_POOL_ID);

    if (pLspCtrlBlk == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: MemAlloc Failed for LCB - IntLspSetReq\n");
        return;
    }

    pLspTrigCtrlBlock = (tLspTrigCtrlBlock *) MemAllocMemBlk (LDP_TRIG_POOL_ID);

    if (pLspTrigCtrlBlock == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: MemAlloc Failed for TrgCB - IntLspSetReq\n");
        MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlk);
        return;
    }
    MEMSET (pLspTrigCtrlBlock, 0, sizeof (tLspTrigCtrlBlock));

    /* Contents of the LSP Control block are suitably initialised */
    LdpInitLspCtrlBlock (pLspCtrlBlk);
#ifdef CFA_WANTED
    /* Create a MPLS Tunnel interface at CFA  and Stack over the MPLS
     * l3 interface (which is stacked over an L3IPVLAN interface,
     * and use the interface index  returned as OutIfIndex
     */
    if (CfaIfmCreateStackMplsTunnelInterface
        (LdpSessionGetIfIndex (pLdpSession, LDP_ADDR_TYPE_IPV6),
         &u4MplsTnlIfIndex) == CFA_FAILURE)
    {

        LDP_DBG (LDP_ADVT_MEM,
                 "LdpNonMrgIntLspSetReq - ADVT: Mplstunnel IF creation failed"
                 " for LSP Cntl block\n");
        MemReleaseMemBlock (LDP_TRIG_POOL_ID, (UINT1 *) pLspTrigCtrlBlock);
        MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pLspCtrlBlk);
        return;

    }
    LDP_DBG6 (LDP_ADVT_PRCS,
              "LdpNonMrgIntLspSetReq: OutInt %d created for "
              "FEC: %s with Peer %d.%d.%d.%d\n",
              u4MplsTnlIfIndex, Ip6PrintAddr (&pLspCtrlBlk->Fec.Prefix.Ip6Addr),
              pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
              pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
              pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
              pLdpSession->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);

    pLspCtrlBlk->u4OutIfIndex = u4MplsTnlIfIndex;
#else
    pLspCtrlBlk->u4OutIfIndex =
        LdpSessionGetIfIndex (pLdpSession, LDP_ADDR_TYPE_IPV6);
#endif
    MEMCPY (&pLspCtrlBlk->NextHopAddr.Ip6Addr, pNextHopAddr, LDP_IPV6ADR_LEN);
    pLspCtrlBlk->u2AddrType = LDP_ADDR_TYPE_IPV6;
    pLspCtrlBlk->u1LblType = SSN_GET_LBL_TYPE (pLdpSession);
    pLspCtrlBlk->pDStrSession = pLdpSession;
    pLspCtrlBlk->u1LspState = LDP_LSM_ST_IDLE;

    /* LSP Control Block is linked to the Fec Table if this LSP is 
     * triggered from the SNMP manager 
     */
    if (pFecTableEntry != NULL)
    {
        pLspCtrlBlk->u1IsStaticLsp = LDP_TRUE;
        pFecTableEntry->pLspCtrlBlock = pLspCtrlBlk;
    }

    pLspCtrlBlk->Fec.u1FecElmntType = LDP_FEC_PREFIX_TYPE;
    pLspTrigCtrlBlock->u1TrgType = IPPRFX_FEC_LSP_SETUP;
    pLspCtrlBlk->Fec.u1PreLen = u1PrefLen;
    pLspCtrlBlk->Fec.u2AddrFmly = IPV6;
    if (pFecTableEntry != NULL)
    {
        pLspCtrlBlk->Fec.u4Index = pFecTableEntry->u4FecIndex;
    }

    MEMCPY (&pLspCtrlBlk->Fec.Prefix.Ip6Addr, pNetworkAddr, LDP_IPV6ADR_LEN);

    pLspCtrlBlk->pTrigCtrlBlk = pLspTrigCtrlBlock;

    TMO_SLL_Add ((tTMO_SLL *) (&(SSN_DLCB (pLdpSession))),
                 (tTMO_SLL_NODE *) (&(pLspCtrlBlk->NextDSLspCtrlBlk)));

    u1Event = LDP_LSM_EVT_INT_SETUP;
    LDP_NON_MRG_FSM[LCB_STATE (pLspCtrlBlk)][u1Event] (pLspCtrlBlk, NULL);
}
#endif

/*----------------------------------------------------------------------------*/
/*                           End of file ldpadsm1.c                           */
/*----------------------------------------------------------------------------*/
