
/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
 * $Id: ldputil.c,v 1.21 2017/06/09 12:50:37 siva Exp $
*
*******************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldputil.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : LDP (IFACE SUB-MODULE)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains utility fuctions
 *                             
 *----------------------------------------------------------------------------*/

#include "ldpincs.h"

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpPduHdr                                            
 * Description   : This routine dumps an LDP Pdu Header contents.
 * Input(s)      : pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpPduHdr (tLdpPduHdr * pPduHdr)
{
    LDP_DBG (LDP_PDU_DUMP, "LDP PDU HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Version      : %d \n",
              OSIX_NTOHS (pPduHdr->u2Version));
    LDP_DBG1 (LDP_PDU_DUMP, "LDP PduLen       : %d \n",
              OSIX_NTOHS (pPduHdr->u2PduLen));
    LDP_DBG6 (LDP_PDU_DUMP, "LDP Id           : %#x:%x:%x:%x:%x:%x \n",
              pPduHdr->LdpId[0], pPduHdr->LdpId[1], pPduHdr->LdpId[2],
              pPduHdr->LdpId[3], pPduHdr->LdpId[4], pPduHdr->LdpId[5]);
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpHelloMsg                                            
 * Description   : This routine decodes and dumps an LDP Hello Msg.
 * Input(s)      : pMsg, pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpHelloMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{

    UINT4               u4TrAddr = 0;
    UINT4               u4CSNumber = 0;
    UINT2               u2TmpLen = 0;
    UINT2               u2Len = 0;
    UINT2               u2HLen = 0;
    UINT2               u2MsgType = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2HoldTime = 0;
    UINT2               u2TRField = 0;    /* Present in Cmn Hello Params Tlv */
    UINT4               u4MsgId = 0;
    UINT1               u1MTlvPrcd = LDP_FALSE;

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == Hello Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - Hello Msg \n", u2MsgType);

    /* Length of Hello Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);    /* Msg Id */
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    /* Decode the Hello Msg and store the HoldTime, CSNUM and TrAddr */
    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* This length specifies the total length of all the tlvs
     * together contained in this hello Msg */
    u2HLen -= LDP_MSG_ID_LEN;

    while (u2Len < u2HLen)
    {
        LDP_EXT_2_BYTES (pMsg, u2TlvType);    /* Extract the Tlv Type */
        LDP_EXT_2_BYTES (pMsg, u2TmpLen);    /* Extract the Tlv Len */
        /* Check for the bad Tlv length is done here */
        if ((u2Len + u2TmpLen) > u2HLen)
        {
            LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Type     : %#x \n", u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d \n", u2TmpLen);
            LDP_DBG (LDP_PDU_DUMP, "PRCS: Bad Tlv Len in the Hello Msg\n");
            return;
        }

        switch (u2TlvType)
        {
            case LDP_CMN_HELLO_PARM_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "LDP TLV Type     : %#x - CMN SSN PRMS TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d  \n", u2TmpLen);
                LDP_EXT_2_BYTES (pMsg, u2HoldTime);    /* Get the Hold Time from
                                                       the Tlv */
                LDP_DBG1 (LDP_PDU_DUMP, "Hold Time Val    : %d  \n",
                          u2HoldTime);
                LDP_EXT_2_BYTES (pMsg, u2TRField);    /* Get the T & R fields
                                                       from the Tlv */
                LDP_DBG1 (LDP_PDU_DUMP, "TR BIT field     : %#x  \n",
                          u2TRField);
                u1MTlvPrcd = LDP_TRUE;
                break;

            case LDP_IPV4_TRANSPORT_ADDR_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "LDP TLV Type     : %#x - IPV4 TRADDR TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d  \n", u2TmpLen);
                LDP_EXT_4_BYTES (pMsg, u4TrAddr);    /* Check Here. Addr is being 
                                                       read as 4 bytes */
                LDP_DBG1 (LDP_PDU_DUMP, "TR Address       : %#x  \n", u4TrAddr);
                break;

            case LDP_CONF_SEQNUMB_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "LDP TLV Type     : %#x - CNFG SEQ TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d  \n", u2TmpLen);
                LDP_EXT_4_BYTES (pMsg, u4CSNumber);
                LDP_DBG1 (LDP_PDU_DUMP, "Seq Number       : %d  \n",
                          u4CSNumber);
                break;

            case LDP_IPV6_TRANSPORT_ADDR_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "LDP TLV Type     : %#x - IPV6 TRADDR TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d \n", u2TmpLen);
                LDP_DBG (LDP_PRCS_PRCS,
                         "PRCS: Unsupported TlvType in Hello Msg\n");
                return;

            default:
                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Type     : %#x \n", u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d \n", u2TmpLen);
                LDP_DBG (LDP_PRCS_PRCS, "PRCS: Invalid TlvType in Hello Msg\n");
                break;
        }

        /* Check if Mand Tlv is processed or not */
        if (u1MTlvPrcd != LDP_TRUE)
        {
            LDP_DBG (LDP_PDU_DUMP, "CmnHelloParams Tlv(Mndtry) is not prsnt\n");
            return;
        }
        /* Increment by the msg length processed */
        u2Len += (u2TmpLen + LDP_TLV_HDR_LEN);
    }
    /* End of the While loop for decoding the Tlvs in Hello Msg */

    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpInitMsg                                            
 * Description   : This routine decodes and dumps an LDP Init Msg.
 * Input(s)      : pMsg, pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpInitMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{

    UINT1               u1VcMrgDirSprtd = 0;
    /* No. of label range components */
    UINT1               u1LblCompCount = 0;
    UINT1               u1Count = 0;
    UINT1               u1MTlvPrsnt = LDP_FALSE;
    UINT1               u1ADFlag = 0;
    UINT2               u2Len = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLen = 0;
    UINT2               u2MsgType;
    UINT2               u2HLen;
    UINT2               u2LbVpi;
    UINT2               u2LbVci;
    UINT2               u2UbVpi;
    UINT2               u2UbVci;
    UINT4               u4MNDResv = 0;
    UINT4               u4MsgId;
    UINT1              *pu1TmpMsg = NULL;
    tLdpCmnSsnParams    CmnSsnPrms;
    tLdpId              LdpId;

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == LDP Init Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - Ldp Init Msg.\n",
              u2MsgType);

    /* Len of LDP Init Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);    /* Msg Id */
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    /* 
     * Decode the LDP Init Msg and obtain the contents such as Common Session
     * Parameters TLV, and optional TLVs such as ATM Session Parameters TLv,
     * FR Session Parameters TLV.
     */
    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Ptr retained to check the end of message condition */
    pu1TmpMsg = pMsg;

    /* This length specifies the total length of all the tlvs
     * together cntned in this LDP Init Msg */
    u2HLen -= LDP_MSG_ID_LEN;

    while (u2Len < u2HLen)
    {
        LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);    /* Extract the Tlv Type */
        LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);    /* Extract the Tlv Len */
        /* Check for the bad Tlv length is done here */
        if ((u2Len + u2TlvLen) > u2HLen)
        {
            /* 
             *  The Tlv length is too large. Signal the error.
             */
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x\n", u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n", u2TlvLen);
            LDP_DBG (LDP_PDU_DUMP, "Rcvd Msg has Errorneous Tlv Len.\n");
            return;
        }

        switch (u2TlvType)
        {

            case LDP_CMN_SSN_PARAM_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CmnSsnParmsTlv.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n", u2TlvLen);
                u1MTlvPrsnt = LDP_TRUE;

                /* Extract and check the Ldp Version */
                LDP_EXT_2_BYTES (pu1TmpMsg, CmnSsnPrms.u2ProtocolVer);
                LDP_DBG1 (LDP_PDU_DUMP, "LDP Version      : %d\n",
                          CmnSsnPrms.u2ProtocolVer);
                if (CmnSsnPrms.u2ProtocolVer != LDP_PROTO_VER)
                {
                    LDP_DBG (LDP_PDU_DUMP, "LDP Version Mismatch - Error.\n");
                }

                /* Extract the Keep Alive Time */
                LDP_EXT_2_BYTES (pu1TmpMsg, CmnSsnPrms.u2KeepAliveTime);
                LDP_DBG1 (LDP_PDU_DUMP, "Keep Alive Time  : %d\n",
                          CmnSsnPrms.u2KeepAliveTime);

                /* Extract the Advert Mode and Loop Detection */
                LDP_EXT_1_BYTES (pu1TmpMsg, u1ADFlag);
                LDP_DBG1 (LDP_PDU_DUMP, "Advt_Loop Inf    : %#x\n", u1ADFlag);

                LDP_GET_A_BIT (u1ADFlag, CmnSsnPrms.u1LabelAdvertMode);
                if (CmnSsnPrms.u1LabelAdvertMode == LDP_DSTR_ON_DEMAND)
                {
                    LDP_DBG (LDP_PDU_DUMP, "Advt Type        : DOD.\n");
                }
                else
                {
                    LDP_DBG (LDP_PDU_DUMP, "Advt Type        : DU.\n");
                }

                LDP_GET_D_BIT (u1ADFlag, CmnSsnPrms.u1LoopDetect);
                if (CmnSsnPrms.u1LoopDetect == LDP_TRUE)
                {
                    LDP_DBG (LDP_PDU_DUMP, "Loop Detection   : Enabled. \n");
                }
                else
                {
                    LDP_DBG (LDP_PDU_DUMP, "Loop Detection   : Disabled. \n");
                }

                /* Exract Path Vector Limit */
                LDP_EXT_1_BYTES (pu1TmpMsg, CmnSsnPrms.u1PvLimit);
                LDP_DBG1 (LDP_PDU_DUMP, "Path Vect Limit  : %d\n",
                          CmnSsnPrms.u1PvLimit);

                /* Extract Max Pdu Length */
                LDP_EXT_2_BYTES (pu1TmpMsg, CmnSsnPrms.u2MaxPduLen);
                LDP_DBG1 (LDP_PDU_DUMP, "Max PDU Length   : %d\n",
                          CmnSsnPrms.u2MaxPduLen);

                /* Get Receiver's LDP-ID */
                MEMCPY (LdpId, pu1TmpMsg, LDP_MAX_LDPID_LEN);
                LDP_DBG6 (LDP_PDU_DUMP,
                          "Rcvr LDP Id      : %#x:%x:%x:%x:%x:%x \n",
                          LdpId[0], LdpId[1], LdpId[2], LdpId[3], LdpId[4],
                          LdpId[5]);

                /* Points to next Tlv if any present */
                pu1TmpMsg += LDP_MAX_LDPID_LEN;

                break;

            case LDP_ATM_SSN_PARAM_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - ATMSsnParmsTlv.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n", u2TlvLen);

                /* 
                 * Extracting field having Numher of label ranges, Merge support 
                 * and the Directionality support from the ATM Session parms TLV.
                 */
                LDP_EXT_4_BYTES (pu1TmpMsg, u4MNDResv);
                LDP_DBG1 (LDP_PDU_DUMP, "M_N_D_Resv fld   : %#x \n", u4MNDResv);

                /* Extract M - Merge capability support by the Peer */
                LDP_GET_M (u4MNDResv, u1VcMrgDirSprtd);
                switch (u1VcMrgDirSprtd)
                {
                    case LDP_NO_MRG:
                        LDP_DBG (LDP_PDU_DUMP,
                                 "MRG Type Supprtd : No Merge Suprtd.\n");
                        break;
                    case LDP_VP_MRG:
                        LDP_DBG (LDP_PDU_DUMP,
                                 "MRG Type Supprtd : VP Merge Suprtd.\n");
                        break;
                    case LDP_VC_MRG:
                        LDP_DBG (LDP_PDU_DUMP,
                                 "MRG Type Supprtd : VC Merge Suprtd.\n");
                        break;
                    case LDP_VPVC_MRG:
                        LDP_DBG (LDP_PDU_DUMP,
                                 "MRG Type Supprtd : VP&VC Merge Suprtd.\n");
                        break;
                    default:
                        LDP_DBG (LDP_PDU_DUMP,
                                 "MRG Type Supprtd : Wrong Val "
                                 "given - Err. \n");
                        break;
                }

                /* Extract D - VC Directionality support by the peer */
                LDP_GET_D (u4MNDResv, u1VcMrgDirSprtd);
                switch (u1VcMrgDirSprtd)
                {
                    case LDP_VC_UNI_DIR:
                        LDP_DBG (LDP_PDU_DUMP,
                                 "VC Dir Supprtd   : Uni Dirn Suprtd.\n");
                        break;
                    case LDP_VC_BI_DIR:
                        LDP_DBG (LDP_PDU_DUMP,
                                 "VC Dir Supprtd   : Bi Dirn Suprtd.\n");
                        break;
                }

                /* Extract N - no of label range components */
                LDP_GET_N (u4MNDResv, u1LblCompCount);
                LDP_DBG1 (LDP_PDU_DUMP, "No of Lbl Rngs   : %d \n",
                          u1LblCompCount);

                /* Extract the Atm Label Range Components and store in Peer's
                 * Atm Label Range List */
                for (u1Count = 0; u1Count < u1LblCompCount; ++u1Count)
                {
                    LDP_EXT_2_BYTES (pu1TmpMsg, u2LbVpi);
                    LDP_EXT_2_BYTES (pu1TmpMsg, u2LbVci);
                    LDP_EXT_2_BYTES (pu1TmpMsg, u2UbVpi);
                    LDP_EXT_2_BYTES (pu1TmpMsg, u2UbVci);
                    LDP_DBG1 (LDP_PDU_DUMP, "Lbl Rng Compnnt  : %d \n",
                              (u1Count + 1));
                    LDP_DBG1 (LDP_PDU_DUMP, "Minimum VPI      : %d \n",
                              u2LbVpi);
                    LDP_DBG1 (LDP_PDU_DUMP, "Minimum VCI      : %d \n",
                              u2LbVci);
                    LDP_DBG1 (LDP_PDU_DUMP, "Maximum VPI      : %d \n",
                              u2UbVpi);
                    LDP_DBG1 (LDP_PDU_DUMP, "Maximum VCI      : %d \n",
                              u2UbVci);
                }

                break;

            default:
                LDP_DBG (LDP_PDU_DUMP, "SESSION: Invalid Tlv in Init Msg\n");
                return;
        }

        /* Check if Mand Tlv is processed or not */
        if (u1MTlvPrsnt != LDP_TRUE)
        {
            LDP_DBG (LDP_PDU_DUMP, "CmnSsnParams Tlv(Mndtry) not prsnt !\n");
            return;
        }
        /* Increment by the msg length processed */
        u2Len += (u2TlvLen + LDP_TLV_HDR_LEN);
    }
    /* End of the While loop for decoding the Tlvs in Init Msg */

    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpKeepAliveMsg                                         
 * Description   : This routine decodes and dumps an LDP KeepAlive Msg.
 * Input(s)      : pMsg, pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpKeepAliveMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{

    UINT2               u2MsgType;
    UINT2               u2HLen;
    UINT4               u4MsgId;

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == Keep Alive Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - Keep Alive Msg.\n",
              u2MsgType);

    /* Len of KAlive Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);    /* Msg Id */
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpAddrMsg                                            
 * Description   : This routine decodes and dumps an LDP Addr Msg.
 * Input(s)      : pMsg, pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpAddrMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{

    UINT1               u1MTlvPrcd = LDP_FALSE;
    UINT2               u2MsgType;
    UINT2               u2Len = 0;
    UINT2               u2HLen;
    UINT2               u2TlvType;
    UINT2               u2TmpLen;
    UINT2               u2AddrFmly;
    UINT2               u2NumIpV4Addr;
    UINT2               u2NumIpV6Addr;
    UINT2               u2Count;
    UINT4               u4Ipv4Addr;
    UINT4               u4Ipv6Addr1;
    UINT4               u4Ipv6Addr2;
    UINT4               u4Ipv6Addr3;
    UINT4               u4Ipv6Addr4;
    UINT4               u4MsgId;

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == Addr Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - Addr Msg \n", u2MsgType);

    /* Length of Addr Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);    /* Msg  Id */
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    /* Decode the Addr Msg and obtain the Address List Tlvs.  */

    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* This length specifies the total length of all the tlvs
     * together contained in this Addr Msg */
    u2HLen -= LDP_MSG_ID_LEN;

    while (u2Len < u2HLen)
    {
        LDP_EXT_2_BYTES (pMsg, u2TlvType);    /* Extract the Tlv Type */
        LDP_EXT_2_BYTES (pMsg, u2TmpLen);    /* Extract the Tlv Len */
        /* Check for the bad Tlv length is done here */
        if ((u2Len + u2TmpLen) > u2HLen)
        {
            LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Type     : %#x \n", u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d \n", u2TmpLen);
            LDP_DBG (LDP_PDU_DUMP, "PRCS: Bad Tlv Len in the Addr Msg\n");
            return;
        }

        switch (u2TlvType)
        {
            case LDP_ADDRLIST_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "LDP TLV Type     : %#x - AddrList TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d  \n", u2TmpLen);

                /* Extract the AddrFmly type */
                LDP_EXT_2_BYTES (pMsg, u2AddrFmly);
                switch (u2AddrFmly)
                {
                    case IPV4:
                        LDP_DBG1 (LDP_PDU_DUMP,
                                  "Addr Family      : %d - IPv4 \n",
                                  u2AddrFmly);
                        u2NumIpV4Addr = (UINT2) (u2TmpLen / IPV4_ADDR_LENGTH);
                        for (u2Count = 0; u2Count < u2NumIpV4Addr; u2Count++)
                        {
                            LDP_EXT_4_BYTES (pMsg, u4Ipv4Addr);
                            LDP_DBG2 (LDP_PDU_DUMP,
                                      "Addr - %d        : %#x  \n",
                                      (u2Count + 1), u4Ipv4Addr);
                        }
                        break;
                    case IPV6:
                        LDP_DBG1 (LDP_PDU_DUMP,
                                  "Addr Family      : %d - IPv6 \n",
                                  u2AddrFmly);
                        u2NumIpV6Addr = (UINT2) (u2TmpLen / LDP_IPV6ADR_LEN);
                        for (u2Count = 0; u2Count < u2NumIpV6Addr; u2Count++)
                        {
                            LDP_EXT_4_BYTES (pMsg, u4Ipv6Addr1);
                            LDP_EXT_4_BYTES (pMsg, u4Ipv6Addr2);
                            LDP_EXT_4_BYTES (pMsg, u4Ipv6Addr3);
                            LDP_EXT_4_BYTES (pMsg, u4Ipv6Addr4);
                            LDP_DBG5 (LDP_PDU_DUMP,
                                      "Addr - %d        : %#x%x%x%x  \n",
                                      (u2Count + 1), u4Ipv6Addr1, u4Ipv6Addr2,
                                      u4Ipv6Addr3, u4Ipv6Addr4);
                        }
                        break;
                    default:
                        LDP_DBG1 (LDP_PDU_DUMP,
                                  "Addr Family      : %d - Unknown \n",
                                  u2AddrFmly);
                        break;
                }
                u1MTlvPrcd = LDP_TRUE;
                break;
            default:
                LDP_DBG (LDP_PDU_DUMP, "PRCS: Invalid TlvType in Addr Msg\n");
                break;
        }

        /* Check if Mand Tlv is processed or not */
        if (u1MTlvPrcd != LDP_TRUE)
        {
            LDP_DBG (LDP_PDU_DUMP,
                     "Address List Tlv (Mandatory) is not prsnt.\n");
            return;
        }
        /* Increment by the msg length processed */
        u2Len += (u2TmpLen + LDP_TLV_HDR_LEN);
    }
    /* End of the While loop for decoding the Tlvs in Addr Msg */

    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpAddrWithDrawMsg                                      
 * Description   : This routine decodes and dumps an LDP AddrWithDraw Msg.
 * Input(s)      : pMsg, pPduHdr 
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpAddrWithDrawMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{
    UINT1               u1MTlvPrcd = LDP_FALSE;
    UINT2               u2MsgType;
    UINT2               u2Len = 0;
    UINT2               u2HLen;
    UINT2               u2TlvType;
    UINT2               u2TmpLen;
    UINT2               u2NumIpV6Addr;
    UINT2               u2NumIpV4Addr;
    UINT2               u2Count;
    UINT4               u4Ipv4Addr;
    UINT4               u4Ipv6Addr1;
    UINT4               u4Ipv6Addr2;
    UINT4               u4Ipv6Addr3;
    UINT4               u4Ipv6Addr4;
    UINT2               u2AddrFmly;
    UINT4               u4MsgId;

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == Addr Withdraw Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - Addr Withdraw Msg.\n",
              u2MsgType);

    /* Length of Addr Wdraw Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);    /* Msg  Id */
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    /* Decode the Addr Withdraw Msg and obtain the Address List Tlvs.  */

    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);
    /* This length specifies the total length of all the tlvs
     * together cntnd in this AddrWdrw Msg */
    u2HLen -= LDP_MSG_ID_LEN;

    while (u2Len < u2HLen)
    {
        LDP_EXT_2_BYTES (pMsg, u2TlvType);    /* Extract the Tlv Type */
        LDP_EXT_2_BYTES (pMsg, u2TmpLen);    /* Extract the Tlv Len */
        /* Check for the bad Tlv length is done here */
        if ((u2Len + u2TmpLen) > u2HLen)
        {
            LDP_DBG (LDP_PDU_DUMP, "Bad Tlv Len in the Addr Wdraw Msg\n");
            return;
        }

        switch (u2TlvType)
        {
            case LDP_ADDRLIST_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "LDP TLV Type     : %#x - AddrList TLV.\n",
                          u2TlvType);

                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d  \n", u2TmpLen);

                /* Extract the AddrFmly type */
                LDP_EXT_2_BYTES (pMsg, u2AddrFmly);
                switch (u2AddrFmly)
                {
                    case IPV4:
                        LDP_DBG1 (LDP_PDU_DUMP,
                                  "Addr Family      : %d - IPv4 \n",
                                  u2AddrFmly);
                        u2NumIpV4Addr = (UINT2) (u2TmpLen / IPV4_ADDR_LENGTH);
                        for (u2Count = 0; u2Count < u2NumIpV4Addr; u2Count++)
                        {
                            LDP_EXT_4_BYTES (pMsg, u4Ipv4Addr);
                            LDP_DBG2 (LDP_PDU_DUMP,
                                      "Addr - %d        : %#x  \n",
                                      (u2Count + 1), u4Ipv4Addr);
                        }
                        break;
                    case IPV6:
                        LDP_DBG1 (LDP_PDU_DUMP,
                                  "Addr Family      : %d - IPv6 \n",
                                  u2AddrFmly);
                        u2NumIpV6Addr = (UINT2) (u2TmpLen / LDP_IPV6ADR_LEN);
                        for (u2Count = 0; u2Count < u2NumIpV6Addr; u2Count++)
                        {
                            LDP_EXT_4_BYTES (pMsg, u4Ipv6Addr1);
                            LDP_EXT_4_BYTES (pMsg, u4Ipv6Addr2);
                            LDP_EXT_4_BYTES (pMsg, u4Ipv6Addr3);
                            LDP_EXT_4_BYTES (pMsg, u4Ipv6Addr4);
                            LDP_DBG5 (LDP_PDU_DUMP,
                                      "Addr - %d        : %#x%x%x%x  \n",
                                      (u2Count + 1), u4Ipv6Addr1, u4Ipv6Addr2,
                                      u4Ipv6Addr3, u4Ipv6Addr4);
                        }
                        break;
                    default:
                        LDP_DBG1 (LDP_PDU_DUMP,
                                  "Addr Family      : %d - Unknown \n",
                                  u2AddrFmly);
                        break;
                }
                u1MTlvPrcd = LDP_TRUE;
                break;

            default:
                LDP_DBG (LDP_PDU_DUMP,
                         "Invalid TlvType in Addr Withdraw Msg\n");
                break;
        }

        /* Check if Mand Tlv is processed or not */
        if (u1MTlvPrcd != LDP_TRUE)
        {
            LDP_DBG (LDP_PDU_DUMP, "Address List Tlv (Mndtry) is not prsnt.\n");
            return;
        }

        /* Increment by the msg length processed */
        u2Len += (u2TmpLen + LDP_TLV_HDR_LEN);
    }
    /* End of the While loop for decoding the Tlvs in Addr Withdraw Msg */

    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpLblMapMsg                                            
 * Description   : This routine decodes and dumps an LDP Label Map Msg.
 * Input(s)      : pMsg, pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpLblMapMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{
    UINT1               u1FecElmtType;
    UINT1               u1PrfxLen;
    UINT1               u1HopCount;
    UINT1               u1Count = 1;
    UINT2               u2Len = 0;
    UINT2               u2HLen;
    UINT2               u2MsgType;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT2               u2AddrFmly;
    UINT4               u4MsgId;
    UINT4               u4LabelOrReqId;
    UINT1              *pu1TmpMsg = NULL;
    UINT2               u2Val;
    UINT1               u1Val;
    UINT4               u4Val;
    UINT4               u4Prefix;
    UINT2               u2Vpi;
    UINT2               u2Vci;
    UINT1               u1NumberOfElspTPEntries;
    UINT1               u1DiffServCount;
    UINT1               au1IfDescStr[MAX_LDP_L2VPN_IF_STR_LEN];
    UINT1              *pu1TmpFecHdrStart = NULL;
    UINT2               u2PwType = LDP_ZERO;
    UINT2               u2IfMtu;
    UINT2               u2MaxNoOfConcatATMCells;
    UINT2               u2CEMPayloadBytes;
    UINT1               u1InfoLen;
    UINT4               u4GroupID;
    UINT4               u4PwID;
    UINT1               u1IfParamsID;
    UINT1               u1IfParamsLen;
    INT1                i1CBit;

    MEMSET (&au1IfDescStr, LDP_ZERO, MAX_LDP_L2VPN_IF_STR_LEN);

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == LblMap Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - Lbl Map Msg \n",
              u2MsgType);

    /* Len of Lbl Map Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    /* Msg  Id */
    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    /* 
     * Decode the Lbl Map Msg and obtain the contents such as FEC TLV, Label
     * TLV, and optional TLVs such as Label Request Message Id TLV, Hop count
     * TLV and Path Vector TLV.
     */
    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Ptr retained to check the end of message condition */
    pu1TmpMsg = pMsg;

    /* This length specifies the total length of all the tlvs
     * together cntned in this LblWdrw Msg */
    u2HLen -= LDP_MSG_ID_LEN;

    /* FEC TLV  - The mandatory TLV is accessed */
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);
    if (u2TlvType != LDP_FEC_TLV)
    {
        /* Error - mandatory TLV not present */
        LDP_DBG (LDP_PDU_DUMP, "LblMap Msg has no FEC elements- Err. \n");
        LDP_DBG1 (LDP_PDU_DUMP, "TLV Type Rcvd    : %#x \n", u2TlvType);
        return;
    }

    LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x - FEC Tlv  \n", u2TlvType);
    LDP_DBG1 (LDP_PDU_DUMP, "TLV Len          : %d \n", u2TlvLen);
    u2Len += (u2TlvLen + LDP_TLV_HDR_LEN);

    /* Getting FecElement Type */
    pu1TmpFecHdrStart = pu1TmpMsg;
    LDP_EXT_1_BYTES (pu1TmpMsg, u1FecElmtType);
    if ((u2TlvLen > LDP_FEC_ELMNT_LEN) && (u1FecElmtType != LDP_FEC_PWVC_TYPE))
    {
        /* This implies, the received FEC TLv contains more
         * than one Fec Element.
         * As of now only a single Fec is supported to be
         * received in a message */
        LDP_DBG (LDP_PDU_DUMP,
                 "LblMap Msg has More than one FEC Elmnt- Err. \n");
        return;
    }

    switch (u1FecElmtType)
    {
        case LDP_FEC_PWVC_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC 128          : %#x  - PwVc Type\n", u1FecElmtType);

            LDP_EXT_2_BYTES (pu1TmpMsg, u2PwType);
            LDP_GET_PWVC_C_BIT (u2PwType, i1CBit);
            if (i1CBit)
            {
                LDP_DBG1 (LDP_PDU_DUMP, "Control Word     : %#x   - Present\n",
                          i1CBit);
            }
            else
            {
                LDP_DBG1 (LDP_PDU_DUMP,
                          "Control Word     : %#x   - Not present\n", i1CBit);
            }

            u2PwType &= 0x0fff;
            LDP_DBG1 (LDP_PDU_DUMP, "PW-Type          : %d  \n", u2PwType);

            LDP_EXT_1_BYTES (pu1TmpMsg, u1InfoLen);
            LDP_DBG1 (LDP_PDU_DUMP, "VC Info Len      : %d\n", u1InfoLen);

            LDP_EXT_4_BYTES (pu1TmpMsg, u4GroupID);
            LDP_DBG1 (LDP_PDU_DUMP, "VC Group ID      : %d\n", u4GroupID);

            if (u1InfoLen == LDP_PWVC_FEC_ELEM_PWVCID_LEN)
            {
                /*extarct only the PWID NO INTERFC PARAM */
                LDP_EXT_4_BYTES (pu1TmpMsg, u4PwID);
                LDP_DBG1 (LDP_PDU_DUMP, "VC PW ID         : %d\n", u4PwID);
            }
            else if (u1InfoLen > LDP_PWVC_FEC_ELEM_PWVCID_LEN)
            {
                /* extarct the PWID as well as interface params now */
                LDP_EXT_4_BYTES (pu1TmpMsg, u4PwID);
                LDP_DBG1 (LDP_PDU_DUMP, "VC PW ID         : %d\n", u4PwID);
                /* now the interfae para,ms */
                while (pu1TmpMsg <
                       pu1TmpFecHdrStart + u1InfoLen +
                       LDP_PWVC_FEC_ELEM_PWVCID_LEN + LDP_PWVC_FEC_ELEM_HDR_LEN)
                {
                    LDP_EXT_1_BYTES (pu1TmpMsg, u1IfParamsID);

                    LDP_EXT_1_BYTES (pu1TmpMsg, u1IfParamsLen);

                    u1IfParamsLen -= LDP_PWVC_FEC_IF_PARAM_HDR_LEN;
                    /*switch (u1IfParamsLen) */
                    switch (u1IfParamsID)
                    {
                        case LDP_IFPARAM_IF_MTU:
                            LDP_EXT_2_BYTES (pu1TmpMsg, u2IfMtu);
                            LDP_DBG1 (LDP_PDU_DUMP, "Interface MTU    : %#x\n",
                                      u2IfMtu);
                            break;

                        case LDP_IFPARAM_MAX_CONC_ATM_CELLS:
                            LDP_EXT_2_BYTES (pu1TmpMsg,
                                             u2MaxNoOfConcatATMCells);
                            LDP_DBG1 (LDP_PDU_DUMP,
                                      "Max ATM Concatenated Cells   "
                                      " : %#x  .\n", u2MaxNoOfConcatATMCells);
                            break;

                        case LDP_IFPARAM_IF_DESC_STR:
                            MEMCPY (&au1IfDescStr, pu1TmpMsg, u1IfParamsLen);
                            LDP_DBG1 (LDP_PDU_DUMP, "Iface Dsc string : %s  \n",
                                      au1IfDescStr);
                            pu1TmpMsg += u1IfParamsLen;
                            break;

                        case LDP_IFPARAM_CEM_PAYLOAD_BYTES:
                            LDP_EXT_2_BYTES (pu1TmpMsg, u2CEMPayloadBytes);
                            LDP_DBG1 (LDP_PDU_DUMP, "CEM Options    :  %#x  \n",
                                      u2CEMPayloadBytes);
                            break;

                        case LDP_IFPARAM_CEP_OPTIONS:
                        case LDP_IFPARAM_REQ_VLAN_ID:
                            break;

                        default:
                            LDP_DBG (LDP_PDU_DUMP,
                                     "Encountered Unknown IF Param \n");
                            break;

                    }            /* switch */
                }                /* while */
            }                    /* else if */
            else
            {
                LDP_DBG (LDP_PDU_DUMP, " Incorrect FEC 128 Length \n");
            }
            break;

        case LDP_FEC_WILDCARD_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Wild Card Type.\n",
                      u1FecElmtType);
            break;
        case LDP_FEC_CRLSP_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP, "FEC Type         : %#x  - CRLSP Type.\n",
                      u1FecElmtType);
            break;
        case LDP_FEC_PREFIX_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP, "FEC Type         : %#x  - Prefix Type.\n",
                      u1FecElmtType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2AddrFmly);
            LDP_EXT_1_BYTES (pu1TmpMsg, u1PrfxLen);
            switch (u2AddrFmly)
            {
                case IPV4:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv4.\n",
                              u2AddrFmly);
                    break;
                case IPV6:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv6.\n",
                              u2AddrFmly);
                    break;
                default:
                    LDP_DBG1 (LDP_PDU_DUMP,
                              "Addr Fmly        : %#x  - Unknown.\n",
                              u2AddrFmly);
                    return;
            }
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Len       : %#x\n", u1PrfxLen);
            if (u1PrfxLen == 0)
            {
                u4Prefix = LDP_DEF_DEST_ADDR;
                LDP_DBG1 (LDP_PDU_DUMP,
                          "Prefix           : %#x - Default Addr.\n", u4Prefix);
            }
            else
            {
                LDP_EXT_FEC_PREFIX (pu1TmpMsg, u4Prefix, u1PrfxLen);
                LDP_DBG1 (LDP_PDU_DUMP, "Prefix           : %#x\n", u4Prefix);
            }
            break;
        case LDP_FEC_HOSTADDR_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Host Addr Type.\n",
                      u1FecElmtType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2AddrFmly);
            LDP_EXT_1_BYTES (pu1TmpMsg, u1PrfxLen);
            switch (u2AddrFmly)
            {
                case IPV4:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv4.\n",
                              u2AddrFmly);
                    break;
                case IPV6:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv6.\n",
                              u2AddrFmly);
                    break;

                default:
                    LDP_DBG1 (LDP_PDU_DUMP,
                              "Addr Fmly        : %#x  - Unknown.\n",
                              u2AddrFmly);
                    return;
            }
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Len       : %#x\n", u1PrfxLen);
            /* As FEC type is HostAddress, u1PrfxLen will be in Octect 
             * Length. To extract the Address, the length in bits is passed. 
             */
            LDP_EXT_FEC_PREFIX (pu1TmpMsg, u4Prefix, u1PrfxLen << 3);
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix           : %#x\n", u4Prefix);
            break;
        default:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Unknown - Error.\n",
                      u1FecElmtType);
            return;
    }

    /* 
     * Check for the presence of labels done and if labels present they
     * are obtained.
     */

    if ((pMsg + u2HLen) == pu1TmpMsg)
    {
        LDP_DBG (LDP_PDU_DUMP, "LblMap  Msg Has No Labels.  - Error\n");
        return;
    }

    /* Label TLV Present and Label value obtained */
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);
    if ((u2Len + (u2TlvLen + LDP_TLV_HDR_LEN)) > u2HLen)
    {
        LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x \n", u2TlvType);
        LDP_DBG1 (LDP_PDU_DUMP, "TLV Len          : %d \n", u2TlvLen);
        LDP_DBG (LDP_PDU_DUMP, "TLV Len Value - Errorneous.\n");
        return;
    }
    u2Len += (u2TlvLen + LDP_TLV_HDR_LEN);

    switch (u2TlvType)
    {
        case LDP_GEN_LABEL_TLV:
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x  - Gen Lbl TLV.\n",
                      u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d \n", u2TlvLen);
            LDP_EXT_4_BYTES (pu1TmpMsg, u4LabelOrReqId);
            LDP_DBG1 (LDP_PDU_DUMP, "Label Value      : %#x\n", u4LabelOrReqId);
            break;
        case LDP_ATM_LABEL_TLV:
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x  - ATM Lbl TLV.\n",
                      u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d \n", u2TlvLen);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2Vpi);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2Vci);
            LDP_DBG1 (LDP_PDU_DUMP, "V - Bits         : %#x\n",
                      (u2Vpi & LDP_V_BITS_MASK));
            LDP_DBG1 (LDP_PDU_DUMP, "VPI Value        : %d\n",
                      (u2Vpi & LDP_VPI_MASK));
            LDP_DBG1 (LDP_PDU_DUMP, "VCI Value        : %d\n", u2Vci);
            switch ((u2Vpi & LDP_V_BITS_MASK) >> 12)
            {
                case 00:
                    LDP_DBG (LDP_PDU_DUMP,
                             "Both VPI & VCI in rcvd Lbl are sgnfcnt.\n");

                    break;
                case 01:
                    LDP_DBG (LDP_PDU_DUMP,
                             "VPI alone in rcvd Lbl are sgnfcnt.\n");
                    break;
                case 10:
                    LDP_DBG (LDP_PDU_DUMP,
                             "VCI alone in rcvd Lbl are sgnfcnt.\n");
                    break;
                default:
                    LDP_DBG (LDP_PDU_DUMP, "VPI - VCI sgnfcnt info - Error.\n");
                    break;
            }
            break;
        case LDP_FR_LABEL_TLV:
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x  - Fr Lbl TLV.\n",
                      u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
            LDP_EXT_4_BYTES (pu1TmpMsg, u4LabelOrReqId);
            LDP_DBG1 (LDP_PDU_DUMP, "Label Value      : %#x\n", u4LabelOrReqId);
            break;
    }

    if ((pMsg + u2HLen) == pu1TmpMsg)
    {
        LDP_DBG (LDP_PDU_DUMP, "No Optional Tlvs Present.\n");
        return;
    }

    /* Optional TLVs present and extracted */
    while (pu1TmpMsg < (pMsg + u2HLen))
    {
        LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
        LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);

        /* Check for the bad Tlv length is done here */
        if ((u2Len + u2TlvLen + LDP_TLV_HDR_LEN) > u2HLen)
        {
            /* 
             *  The Tlv length is too large. Signal the error.
             */
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x\n", u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n", u2TlvLen);
            LDP_DBG (LDP_PDU_DUMP, "Rcvd Msg has Errorneous Tlv Len.\n");
            return;
        }
        u2Len += (u2TlvLen + LDP_TLV_HDR_LEN);

        switch (u2TlvType)
        {
            case LDP_LBL_REQ_MSGID_TLV:
                LDP_EXT_4_BYTES (pu1TmpMsg, u4LabelOrReqId);
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - LblReqMsgId TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
                LDP_DBG1 (LDP_PDU_DUMP, "LblReqMsgId val  : %d\n",
                          u4LabelOrReqId);
                break;
            case LDP_HOP_COUNT_TLV:
                LDP_EXT_1_BYTES (pu1TmpMsg, u1HopCount);
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - HopCount TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
                LDP_DBG1 (LDP_PDU_DUMP, "Hop Count val    : %d\n", u1HopCount);
                break;
            case LDP_PATH_VECTOR_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - Path Vector TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
                while (pu1TmpMsg < (pMsg + u2HLen))
                {
                    LDP_EXT_4_BYTES (pu1TmpMsg, u4LabelOrReqId);
                    LDP_DBG2 (LDP_PDU_DUMP, "LSR ID - %d      : %#x\n",
                              u1Count++, u4LabelOrReqId);
                }
                break;
            case CRLDP_LSPID_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP LSPID TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
                LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Resv_Act_Flag    : %#x\n", u2Val);
                if (u2Val == 0x00000000)
                {
                    LDP_DBG (LDP_PDU_DUMP,
                             "Act_Flag indn  - Initial LSP Setup.\n");
                }
                else if (u2Val == CRLDP_MODIFY_REQ)
                {
                    LDP_DBG (LDP_PDU_DUMP,
                             "Act_Flag indn  - Modify LSP Setup.\n");
                }
                LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Local CR_LSP_ID  : %d\n", u2Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Ingrss LSR ID    : %#x\n", u4Val);

                break;
            case CRLDP_TRAFPARM_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP TRAFPARM TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Flags value      : %#x\n", u1Val);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Frequency        : %d\n", u1Val);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Reserved field   : %#x\n", u1Val);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Weight           : %d\n", u1Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Peak Data Rate   : %d\n", u4Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Peak Burst Size  : %d\n", u4Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Cmmttd Data Rate : %d\n", u4Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Cmmttd Burst Size: %d\n", u4Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Excess Burst Size: %d\n", u4Val);
                break;

            case CRLDP_DIFFSERV_ELSPTP_TLV:

                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP ELSP_TP TLV.\n",
                          u2TlvType);

                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);

                LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Reserved         : %d\n", u2Val);

                LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "ELSPTP Entries   : %d\n", u2Val);

                u1NumberOfElspTPEntries = (UINT1) u2Val;
                for (u1DiffServCount = 0; u1DiffServCount <
                     u1NumberOfElspTPEntries; u1DiffServCount++)

                {

                    LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Reserved         : %d\n", u2Val);

                    LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "PSC              : %#x\n", u2Val);

                    LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Flags value      : %#x\n", u1Val);

                    LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Frequency        : %d\n", u1Val);

                    LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Reserved field   : %#x\n", u1Val);

                    LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Weight           : %d\n", u1Val);

                    LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Peak Data Rate   : %d\n", u4Val);

                    LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Peak Burst Size  : %d\n", u4Val);

                    LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Cmmttd Data Rate : %d\n", u4Val);

                    LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Cmmttd Burst Size: %d\n", u4Val);

                    LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Excess Burst Size: %d\n", u4Val);

                }
                break;

            default:
                LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x\n", u2TlvType);
                LDP_DBG (LDP_PDU_DUMP,
                         "TLV Type not of the expcte Opt tlv - Err.\n");
                return;
        }
    }
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpLblReqMsg                                            
 * Description   : This routine decodes and dumps an LDP Label Request Msg.
 * Input(s)      : pMsg, pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpLblReqMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{
    UINT1               u1FecElmtType;
    UINT1               u1PrfxLen;
    UINT1               u1Val;
    UINT1               u1HopCount;
    UINT1               u1Count = 1;
    UINT2               u2HLen;
    UINT2               u2Len = 0;
    UINT2               u2MsgType;
    UINT2               u2ErTlvType;
    UINT2               u2ErTlvLen;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT2               u2AddrFmly;
    UINT4               u4MsgId;
    UINT4               u4Prefix;
    UINT2               u2Val;
    UINT4               u4Val;
    UINT4               u4Val1;
    UINT4               u4Val2;
    UINT4               u4Val3;
    UINT4               u4LabelOrReqId;
    UINT1              *pu1TmpMsg = NULL;
    UINT2               u2ErLenCovered = 0;
    UINT1               u1NumberOfElspTPEntries;
    UINT1               u1NumberOfMapEntries;
    UINT1               u1DiffServCount;
    UINT1               au1IfDescStr[MAX_LDP_L2VPN_IF_STR_LEN];
    UINT1              *pu1TmpFecHdrStart = NULL;
    UINT2               u2PwType = LDP_ZERO;
    UINT2               u2IfMtu;
    UINT2               u2MaxNoOfConcatATMCells;
    UINT2               u2CEMPayloadBytes;
    UINT1               u1InfoLen;
    UINT4               u4GroupID;
    UINT4               u4PwID;
    UINT1               u1IfParamsID;
    UINT1               u1IfParamsLen;
    UINT1               u1TBit;
    INT1                i1CBit;

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == Lbl Req Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - Lbl Req Msg \n",
              u2MsgType);

    /* Len of Lbl Req Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    /* Msg  Id */
    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    /* 
     * Decode the Lbl Req Msg and obtain the mandatory FEC TLV and optional
     * TLVs such as Hop Count TLV, Path Vector TLV in case of normal Label
     * Request and in mandatory FEC TLV, LSPID TLV and optional TLVs such as 
     * ER-TLV, Traffic TLV, Pinning TLV, Resource Class TLV, Pre-emption TLV
     * in case of constraint based LSP Setups.
     */
    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Ptr retained to check the end of message condition */
    pu1TmpMsg = pMsg;

    /* This length specifies the total length of all the tlvs
     * together cntned in this LblWdrw Msg */
    u2HLen -= LDP_MSG_ID_LEN;

    /* FEC TLV  - The mandatory TLV is accessed */
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);
    if (u2TlvType != LDP_FEC_TLV)
    {
        /* Error - mandatory TLV not present */
        LDP_DBG (LDP_PDU_DUMP, "LblReq Msg has no FEC elements- Err. \n");
        LDP_DBG1 (LDP_PDU_DUMP, "TLV Type Rcvd    : %#x \n", u2TlvType);
        return;
    }

    if (u2TlvLen > LDP_FEC_ELMNT_LEN)
    {
        /* This implies, the received FEC TLv contains more
         * than one Fec Element.
         * As of now only a single Fec is supported to be
         * received in a message */
        LDP_DBG (LDP_PDU_DUMP,
                 "LblReq Msg has More than one FEC Elmnt- Err. \n");
        return;
    }

    LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x - FEC Tlv  \n", u2TlvType);
    LDP_DBG1 (LDP_PDU_DUMP, "TLV Len          : %d \n", u2TlvLen);
    u2Len += (u2TlvLen + LDP_TLV_HDR_LEN);

    /* Getting FecElement Type */
    pu1TmpFecHdrStart = pu1TmpMsg;
    LDP_EXT_1_BYTES (pu1TmpMsg, u1FecElmtType);
    switch (u1FecElmtType)
    {
        case LDP_FEC_PWVC_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC 128          : %#x  - PWVC Type\n", u1FecElmtType);

            LDP_EXT_2_BYTES (pu1TmpMsg, u2PwType);
            LDP_GET_PWVC_C_BIT (u2PwType, i1CBit);
            if (i1CBit)
            {
                LDP_DBG1 (LDP_PDU_DUMP, "Control Word     : %#x   - Present\n",
                          i1CBit);
            }
            else
            {
                LDP_DBG1 (LDP_PDU_DUMP,
                          "Control Word     : %#x   - Not present\n", i1CBit);
            }
            u2PwType &= 0x0fff;
            LDP_DBG1 (LDP_PDU_DUMP, "PW-Type         : %d  - Type\n", u2PwType);

            LDP_EXT_1_BYTES (pu1TmpMsg, u1InfoLen);
            LDP_DBG1 (LDP_PDU_DUMP, "VC Info Len      : %d\n", u1InfoLen);

            LDP_EXT_4_BYTES (pu1TmpMsg, u4GroupID);
            LDP_DBG1 (LDP_PDU_DUMP, "VC Group ID      : %d\n", u4GroupID);

            if (u1InfoLen == LDP_PWVC_FEC_ELEM_PWVCID_LEN)
            {
                /*extarct only the PWID NO INTERFC PARAM */
                LDP_EXT_4_BYTES (pu1TmpMsg, u4PwID);
                LDP_DBG1 (LDP_PDU_DUMP, "VC PW ID         : %d\n", u4PwID);
            }
            else if (u1InfoLen > LDP_PWVC_FEC_ELEM_PWVCID_LEN)
            {
                /* extarct the PWID as well as interface params now */
                LDP_EXT_4_BYTES (pu1TmpMsg, u4PwID);
                LDP_DBG1 (LDP_PDU_DUMP, "VC PW ID         : %d\n", u4PwID);
                /* now the interfae para,ms */
                while (pu1TmpMsg <
                       pu1TmpFecHdrStart + u1InfoLen +
                       LDP_PWVC_FEC_ELEM_PWVCID_LEN + LDP_PWVC_FEC_ELEM_HDR_LEN)
                {
                    LDP_EXT_1_BYTES (pu1TmpMsg, u1IfParamsID);

                    LDP_EXT_1_BYTES (pu1TmpMsg, u1IfParamsLen);

                    /*switch (u1IfParamsLen) */
                    switch (u1IfParamsID)
                    {
                        case LDP_IFPARAM_IF_MTU:
                            LDP_EXT_2_BYTES (pu1TmpMsg, u2IfMtu);
                            LDP_DBG1 (LDP_PDU_DUMP,
                                      "Interface MTU        :  %#x\n", u2IfMtu);
                            break;

                        case LDP_IFPARAM_MAX_CONC_ATM_CELLS:
                            LDP_EXT_2_BYTES (pu1TmpMsg,
                                             u2MaxNoOfConcatATMCells);
                            LDP_DBG1 (LDP_PDU_DUMP,
                                      "Max ATM Concatenated Cells    :  %#x  .\n",
                                      u2MaxNoOfConcatATMCells);
                            break;

                        case LDP_IFPARAM_IF_DESC_STR:
                            MEMCPY (&au1IfDescStr, pu1TmpMsg, u1IfParamsLen);
                            LDP_DBG1 (LDP_PDU_DUMP,
                                      "Interface Desc. String    :  %#x  .\n",
                                      au1IfDescStr);
                            pu1TmpMsg += u1IfParamsLen;
                            break;

                        case LDP_IFPARAM_CEM_PAYLOAD_BYTES:
                            LDP_EXT_2_BYTES (pu1TmpMsg, u2CEMPayloadBytes);
                            LDP_DBG1 (LDP_PDU_DUMP,
                                      "CEM Options    :  %#x  .\n",
                                      u2CEMPayloadBytes);
                            break;

                        case LDP_IFPARAM_CEP_OPTIONS:
                        case LDP_IFPARAM_REQ_VLAN_ID:
                            break;

                        default:
                            LDP_DBG (LDP_PDU_DUMP,
                                     "Encountered Unknown IF Param \n");
                            break;

                    }            /* switch */
                }                /* while */
            }                    /* else if */
            else
            {
                LDP_DBG (LDP_PDU_DUMP, " Incorrect FEC 128 Length \n");
            }
            break;

        case LDP_FEC_WILDCARD_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Wild Card Type.\n",
                      u1FecElmtType);
            break;
        case LDP_FEC_CRLSP_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP, "FEC Type         : %#x  - CRLSP Type.\n",
                      u1FecElmtType);
            break;
        case LDP_FEC_PREFIX_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP, "FEC Type         : %#x  - Prefix Type.\n",
                      u1FecElmtType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2AddrFmly);
            LDP_EXT_1_BYTES (pu1TmpMsg, u1PrfxLen);
            switch (u2AddrFmly)
            {
                case IPV4:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv4.\n",
                              u2AddrFmly);
                    break;
                case IPV6:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv6.\n",
                              u2AddrFmly);
                    break;
                default:
                    LDP_DBG1 (LDP_PDU_DUMP,
                              "Addr Fmly        : %#x  - Unknown.\n",
                              u2AddrFmly);
                    return;
            }
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Len       : %#x\n", u1PrfxLen);
            if (u1PrfxLen == 0)
            {
                u4Prefix = LDP_DEF_DEST_ADDR;
                LDP_DBG1 (LDP_PDU_DUMP,
                          "Prefix           : %#x - Default Addr.\n", u4Prefix);
            }
            else
            {
                LDP_EXT_FEC_PREFIX (pu1TmpMsg, u4Prefix, u1PrfxLen);
                LDP_DBG1 (LDP_PDU_DUMP, "Prefix           : %#x\n", u4Prefix);
            }
            break;
        case LDP_FEC_HOSTADDR_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Host Addr Type.\n",
                      u1FecElmtType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2AddrFmly);
            LDP_EXT_1_BYTES (pu1TmpMsg, u1PrfxLen);
            switch (u2AddrFmly)
            {
                case IPV4:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv4.\n",
                              u2AddrFmly);
                    break;
                case IPV6:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv6.\n",
                              u2AddrFmly);
                    break;

                default:
                    LDP_DBG1 (LDP_PDU_DUMP,
                              "Addr Fmly        : %#x  - Unknown.\n",
                              u2AddrFmly);
                    return;
            }
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Len       : %#x\n", u1PrfxLen);
            LDP_EXT_FEC_PREFIX (pu1TmpMsg, u4Prefix, u1PrfxLen << 3);
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix           : %#x\n", u4Prefix);
            break;
        default:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Unknown - Error.\n",
                      u1FecElmtType);
            return;
    }

    if (u1FecElmtType == LDP_FEC_CRLSP_TYPE)
    {
        /* 
         * In case of CRLSP Label Request message, the LSPID TLV is mandatory
         * and is checked.
         */
        LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
        LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);
        if ((u2Len + u2TlvLen) > u2HLen)
        {
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x\n", u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n", u2TlvLen);
            LDP_DBG (LDP_PDU_DUMP, "Rcvd Msg has Errorneous Tlv Len.\n");
            return;
        }
        if (u2TlvType != CRLDP_LSPID_TLV)
        {
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x\n", u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n", u2TlvLen);
            LDP_DBG (LDP_PDU_DUMP,
                     "CRLSP LBL Req Msg - Mdtry LSPID TLV Absnt - Err.\n");
            return;
        }
        u2Len += u2TlvLen;
        LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x  - CRLDP LSPID TLV.\n",
                  u2TlvType);
        LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
        LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
        LDP_DBG1 (LDP_PDU_DUMP, "Resv_Act_Flag    : %#x\n", u2Val);
        if (u2Val == 0x00000000)
        {
            LDP_DBG (LDP_PDU_DUMP, "Act_Flag indn  - Initial LSP Setup.\n");
        }
        else if (u2Val == CRLDP_MODIFY_REQ)
        {
            LDP_DBG (LDP_PDU_DUMP, "Act_Flag indn  - Modify LSP Setup.\n");
        }
        LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
        LDP_DBG1 (LDP_PDU_DUMP, "Local CR_LSP_ID  : %d\n", u2Val);

        LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
        LDP_DBG1 (LDP_PDU_DUMP, "Ingrss LSR ID    : %#x\n", u4Val);
    }

    /* Optional TLVs obtained */
    while (pu1TmpMsg < (pMsg + u2HLen))
    {
        LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
        LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);

        /* Check for the bad Tlv length is done here */
        if ((u2Len + u2TlvLen + LDP_TLV_HDR_LEN) > u2HLen)
        {
            /* 
             *  The Tlv length is too large. Signal the error.
             */
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x\n", u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n", u2TlvLen);
            LDP_DBG (LDP_PDU_DUMP, "Rcvd Msg has Errorneous Tlv Len.\n");
            return;
        }

        u2Len += (u2TlvLen + LDP_TLV_HDR_LEN);

        switch (u2TlvType)
        {
            case LDP_HOP_COUNT_TLV:
                LDP_EXT_1_BYTES (pu1TmpMsg, u1HopCount);
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - HopCount TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
                LDP_DBG1 (LDP_PDU_DUMP, "Hop Count val    : %d\n", u1HopCount);
                break;
            case LDP_PATH_VECTOR_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - Path Vector TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
                while (pu1TmpMsg < (pMsg + u2HLen))
                {
                    LDP_EXT_4_BYTES (pu1TmpMsg, u4LabelOrReqId);
                    LDP_DBG2 (LDP_PDU_DUMP, "LSR ID - %d      : %#x\n",
                              u1Count++, u4LabelOrReqId);
                }
                break;
            case CRLDP_TRAFPARM_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP TRAFPARM TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Flags value      : %#x\n", u1Val);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Frequency        : %d\n", u1Val);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Reserved field   : %#x\n", u1Val);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Weight           : %d\n", u1Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Peak Data Rate   : %d\n", u4Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Peak Burst Size  : %d\n", u4Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Cmmttd Data Rate : %d\n", u4Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Cmmttd Burst Size: %d\n", u4Val);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Excess Burst Size: %d\n", u4Val);
                break;
            case CRLDP_EXPLROUTE_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP EXPLROUTE TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
                while (u2ErLenCovered < u2TlvLen)
                {
                    LDP_EXT_2_BYTES (pu1TmpMsg, u2ErTlvType);
                    LDP_EXT_2_BYTES (pu1TmpMsg, u2ErTlvLen);
                    /* Check for the bad Tlv length is done here */
                    if ((u2ErLenCovered + u2ErTlvLen) > u2TlvLen)
                    {
                        /* 
                         *  The Tlv length is too large. Signal the error.
                         */
                        LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x\n",
                                  u2ErTlvType);
                        LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n",
                                  u2ErTlvLen);
                        LDP_DBG (LDP_PDU_DUMP,
                                 "Rcvd Msg has Errorneous Tlv Len.\n");
                        return;
                    }
                    u2ErLenCovered += (u2ErTlvLen + LDP_TLV_HDR_LEN);

                    switch (u2ErTlvType)
                    {
                        case CRLDP_ERHOP_TY_IPV4:
                            LDP_DBG1 (LDP_PDU_DUMP,
                                      "TLV Type         : %#x - "
                                      "ER-HOP 1 - IPv4.\n", u2ErTlvType);
                            LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n",
                                      u2ErTlvLen);
                            LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "L Bit set val    : %#x \n",
                                      u2Val);
                            if (u2Val == LDP_LOOSE_ERHOP_TYPE)
                            {
                                LDP_DBG (LDP_PDU_DUMP,
                                         "Explict route - loose exp route "
                                         "hop type.\n");
                            }
                            else if (u2Val == LDP_STRICT_ERHOP_TYPE)
                            {
                                LDP_DBG (LDP_PDU_DUMP,
                                         "Explict route - strict exp route "
                                         "hop type.\n");
                            }
                            LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "Resv field       : %#x \n",
                                      u1Val);
                            LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Length    : %#x \n",
                                      u1Val);
                            LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "IPv4 Address     : %#x \n",
                                      u4Val);

                            break;
                        case CRLDP_ERHOP_TY_IPV6:
                            LDP_DBG1 (LDP_PDU_DUMP,
                                      "TLV Type         : %#x - "
                                      "ER-HOP 2 - IPv6.\n", u2ErTlvType);
                            LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n",
                                      u2ErTlvLen);
                            LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "L Bit set val    : %#x \n",
                                      u2Val);
                            if (u2Val == LDP_LOOSE_ERHOP_TYPE)
                            {
                                LDP_DBG (LDP_PDU_DUMP,
                                         "Explict route - loose exp route "
                                         "hop type.\n");
                            }
                            else if (u2Val == LDP_STRICT_ERHOP_TYPE)
                            {
                                LDP_DBG (LDP_PDU_DUMP,
                                         "Explict route - strict exp route "
                                         "hop type.\n");
                            }
                            LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "Resv field       : %#x \n",
                                      u1Val);
                            LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Length    : %#x \n",
                                      u1Val);
                            LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                            LDP_EXT_4_BYTES (pu1TmpMsg, u4Val1);
                            LDP_EXT_4_BYTES (pu1TmpMsg, u4Val2);
                            LDP_EXT_4_BYTES (pu1TmpMsg, u4Val3);
                            LDP_DBG4 (LDP_PDU_DUMP,
                                      "IPv6 Address     : %#x%x%x%x \n", u4Val,
                                      u4Val1, u4Val2, u4Val3);
                            break;
                        case CRLDP_ER_HOP_ASNUM:
                            LDP_DBG1 (LDP_PDU_DUMP,
                                      "TLV Type         : %#x - ER-HOP 3 - "
                                      "AS Number.\n", u2ErTlvType);
                            LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n",
                                      u2ErTlvLen);
                            LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "L Bit set val    : %#x \n",
                                      u2Val);
                            if (u2Val == LDP_LOOSE_ERHOP_TYPE)
                            {
                                LDP_DBG (LDP_PDU_DUMP,
                                         "Explict route - loose exp route "
                                         "hop type.\n");
                            }
                            else if (u2Val == LDP_STRICT_ERHOP_TYPE)
                            {
                                LDP_DBG (LDP_PDU_DUMP,
                                         "Explict route - strict exp route "
                                         "hop type.\n");
                            }
                            LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "AS Number        : %#x \n",
                                      u2Val);
                            break;
                        case CRLDP_ER_HOP_LSPID:
                            LDP_DBG1 (LDP_PDU_DUMP,
                                      "TLV Type         : %#x - "
                                      "ER-HOP 4 - LSPID.\n", u2ErTlvType);
                            LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n",
                                      u2ErTlvLen);
                            LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "L Bit set val    : %#x \n",
                                      u2Val);
                            if (u2Val == LDP_LOOSE_ERHOP_TYPE)
                            {
                                LDP_DBG (LDP_PDU_DUMP,
                                         "Explict route - loose exp route "
                                         "hop type.\n");
                            }
                            else if (u2Val == LDP_STRICT_ERHOP_TYPE)
                            {
                                LDP_DBG (LDP_PDU_DUMP,
                                         "Explict route - strict exp route "
                                         "hop type.\n");
                            }
                            LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "Local LSPID      : %#x \n",
                                      u2Val);
                            LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "Ingress LSR ID   : %#x \n",
                                      u4Val);

                            break;
                        default:
                            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x\n",
                                      u2ErTlvType);
                            LDP_DBG1 (LDP_PDU_DUMP, "TLV Length       : %d \n",
                                      u2ErTlvLen);
                            LDP_DBG (LDP_PDU_DUMP,
                                     "Rcvd Msg has Errorneous ER Tlv Type.\n");
                            break;
                    }

                }

                break;
            case CRLDP_PINNING_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP PINNING TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Pnning_field     : %#x\n", u4Val);
                if (u4Val == CRLDP_ROUTE_PIN_REQ)
                {
                    LDP_DBG (LDP_PDU_DUMP, "Route Pinning Requested.\n");
                }
                else if (u4Val == CRLDP_ROUTE_PIN_NOREQ)
                {
                    LDP_DBG (LDP_PDU_DUMP, "Route Pinning Not Requested.\n");
                }

                break;
            case CRLDP_RESCLS_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP RES_CLSS TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);

                LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Resource Class   : %d\n", u4Val);

                break;
            case CRLDP_PREEMPT_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP PRE_EMPT TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Setup Priority   : %d\n", u1Val);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Holding Priority : %d\n", u1Val);

                LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Resv field val   : %d\n", u2Val);

                break;

            case CRLDP_DIFFSERV_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP DIFFSERV TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                u1TBit = (UINT1) (u1Val >> 7);
                LDP_DBG1 (LDP_PDU_DUMP, "T BIT            : %d\n", u1TBit);

                LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Reserved         : %d\n", u1Val);

                switch (u1TBit)
                {
                    case LDP_ZERO:

                        LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                        LDP_DBG1 (LDP_PDU_DUMP, "Num_Map Entries  : %d\n",
                                  u2Val);
                        u1NumberOfMapEntries = (UINT1) u2Val;

                        for (u1DiffServCount = 0; u1DiffServCount <
                             u1NumberOfMapEntries; u1DiffServCount++)
                        {

                            LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "Reserved         : %d\n",
                                      u1Val);

                            LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "EXP              : %d\n",
                                      u1Val);

                            LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                            LDP_DBG1 (LDP_PDU_DUMP, "PHBID            : %#x\n",
                                      u2Val);
                        }
                        break;
                    case LDP_ONE:

                        LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                        LDP_DBG1 (LDP_PDU_DUMP, "PSC              : %#x\n",
                                  u2Val);
                        break;

                }                /*End Of Switch */
                break;
            case CRLDP_DIFFSERV_ELSPTP_TLV:

                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP ELSP_TP TLV.\n",
                          u2TlvType);

                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);

                LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Reserved         : %d\n", u2Val);

                LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "ELSPTP Entries   : %d\n", u2Val);

                u1NumberOfElspTPEntries = (UINT1) u2Val;
                for (u1DiffServCount = 0; u1DiffServCount <
                     u1NumberOfElspTPEntries; u1DiffServCount++)

                {

                    LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Reserved         : %d\n", u2Val);

                    LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "PSC              : %#x\n", u2Val);

                    LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Flags value      : %#x\n", u1Val);

                    LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Frequency        : %d\n", u1Val);

                    LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Reserved field   : %#x\n", u1Val);

                    LDP_EXT_1_BYTES (pu1TmpMsg, u1Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Weight           : %d\n", u1Val);

                    LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Peak Data Rate   : %d\n", u4Val);

                    LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Peak Burst Size  : %d\n", u4Val);

                    LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Cmmttd Data Rate : %d\n", u4Val);

                    LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Cmmttd Burst Size: %d\n", u4Val);

                    LDP_EXT_4_BYTES (pu1TmpMsg, u4Val);
                    LDP_DBG1 (LDP_PDU_DUMP, "Excess Burst Size: %d\n", u4Val);

                }
                break;

            case CRLDP_DIFFSERV_CLASSTYPE_TLV:

                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP CLASS_TYPE TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);

                LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Reserved         : %d\n", u2Val);

                LDP_EXT_2_BYTES (pu1TmpMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "CLASS_TYP        : %d\n", u2Val);
                break;

            default:
                LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x\n", u2TlvType);
                LDP_DBG (LDP_PDU_DUMP,
                         "TLV Type not of the expctd Opt tlv - Err.\n");
                return;
        }
    }
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpLblWithDrawMsg                                       
 * Description   : This routine decodes and dumps an LDP Label Withdraw Msg.
 * Input(s)      : pMsg, pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpLblWithDrawMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{

    UINT1               u1FecElmtType;
    UINT1               u1PrfxLen;
    UINT2               u2HLen;
    UINT2               u2MsgType;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT2               u2AddrFmly;
    UINT4               u4MsgId;
    UINT4               u4Prefix;
    UINT4               u4LabelOrReqId;
    UINT1              *pu1TmpMsg = NULL;
    UINT2               u2PwType = LDP_ZERO;
    UINT1               u1InfoLen;
    UINT4               u4GroupID;
    UINT4               u4PwID;
    INT1                i1CBit;

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == LblWdrw Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - LblWdrw Msg \n",
              u2MsgType);

    /* Len of LblWdrw Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    /* Msg  Id */
    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    /* 
     * Decode the LblWdrw Msg and obtain the  contents and optional 
     * parameters if any.
     */
    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Ptr retained to check the end of message condition */
    pu1TmpMsg = pMsg;

    /* This length specifies the total length of all the tlvs
     * together cntned in this LblWdrw Msg */
    u2HLen -= LDP_MSG_ID_LEN;

    /* FEC TLV  - The mandatory TLV is accessed */
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);
    if (u2TlvType != LDP_FEC_TLV)
    {
        /* Error - mandatory TLV not present */
        LDP_DBG (LDP_PDU_DUMP, "LblWdrw Msg has no FEC elements- Err. \n");
        LDP_DBG1 (LDP_PDU_DUMP, "TLV Type Rcvd    : %#x \n", u2TlvType);
        return;
    }

    LDP_EXT_1_BYTES (pu1TmpMsg, u1FecElmtType);
    if ((u2TlvLen > LDP_FEC_ELMNT_LEN) && (u1FecElmtType != LDP_FEC_PWVC_TYPE))
    {
        /* This implies, the received FEC TLv contains more
         * than one Fec Element.
         * As of now only a single Fec is supported to be received
         * in a message*/
        LDP_DBG (LDP_PDU_DUMP,
                 "LblWdrw Msg has More than one FEC Elmnt- Err. \n");
        return;
    }

    LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x - FEC Tlv  \n", u2TlvType);
    LDP_DBG1 (LDP_PDU_DUMP, "TLV Len          : %d \n", u2TlvLen);

    /* Getting FecElement Type */
    switch (u1FecElmtType)
    {
        case LDP_FEC_PWVC_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC 128          : %#x  - PwVc Type\n", u1FecElmtType);

            LDP_EXT_2_BYTES (pu1TmpMsg, u2PwType);
            LDP_GET_PWVC_C_BIT (u2PwType, i1CBit);
            if (i1CBit)
            {
                LDP_DBG1 (LDP_PDU_DUMP, "Control Word     : %#x   - Present\n",
                          i1CBit);
            }
            else
            {
                LDP_DBG1 (LDP_PDU_DUMP,
                          "Control Word     : %#x   - Not present\n", i1CBit);
            }

            u2PwType &= 0x0fff;
            LDP_DBG1 (LDP_PDU_DUMP, "PW-Type          : %d  \n", u2PwType);

            LDP_EXT_1_BYTES (pu1TmpMsg, u1InfoLen);
            LDP_DBG1 (LDP_PDU_DUMP, "VC Info Len      : %d\n", u1InfoLen);

            LDP_EXT_4_BYTES (pu1TmpMsg, u4GroupID);
            LDP_DBG1 (LDP_PDU_DUMP, "VC Group ID      : %d\n", u4GroupID);
            if (u1InfoLen > LDP_PWVC_FEC_ELEM_PWVCID_LEN)
            {
                LDP_DBG (LDP_PDU_DUMP, "Erronous FEC Element\n");
            }
            else if (u1InfoLen == LDP_PWVC_FEC_ELEM_PWVCID_LEN)
            {
                LDP_EXT_4_BYTES (pu1TmpMsg, u4PwID);
                LDP_DBG1 (LDP_PDU_DUMP, "VC PW ID         : %d\n", u4PwID);
            }

            break;

        case LDP_FEC_WILDCARD_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Wild Card Type.\n",
                      u1FecElmtType);
            break;
        case LDP_FEC_CRLSP_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP, "FEC Type         : %#x  - CRLSP Type.\n",
                      u1FecElmtType);
            break;
        case LDP_FEC_PREFIX_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP, "FEC Type         : %#x  - Prefix Type.\n",
                      u1FecElmtType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2AddrFmly);
            LDP_EXT_1_BYTES (pu1TmpMsg, u1PrfxLen);
            switch (u2AddrFmly)
            {
                case IPV4:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv4.\n",
                              u2AddrFmly);
                    break;
                case IPV6:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv6.\n",
                              u2AddrFmly);
                    break;
                default:
                    LDP_DBG1 (LDP_PDU_DUMP,
                              "Addr Fmly        : %#x  - Unknown.\n",
                              u2AddrFmly);
                    return;
            }
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Len       : %#x\n", u1PrfxLen);
            if (u1PrfxLen == 0)
            {
                u4Prefix = LDP_DEF_DEST_ADDR;
                LDP_DBG1 (LDP_PDU_DUMP,
                          "Prefix           : %#x - Default Addr.\n", u4Prefix);
            }
            else
            {
                LDP_EXT_FEC_PREFIX (pu1TmpMsg, u4Prefix, u1PrfxLen);
                LDP_DBG1 (LDP_PDU_DUMP, "Prefix           : %#x\n", u4Prefix);
            }
            break;
        case LDP_FEC_HOSTADDR_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Host Addr Type.\n",
                      u1FecElmtType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2AddrFmly);
            LDP_EXT_1_BYTES (pu1TmpMsg, u1PrfxLen);
            switch (u2AddrFmly)
            {
                case IPV4:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv4.\n",
                              u2AddrFmly);
                    break;
                case IPV6:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv6.\n",
                              u2AddrFmly);
                    break;

                default:
                    LDP_DBG1 (LDP_PDU_DUMP,
                              "Addr Fmly        : %#x  - Unknown.\n",
                              u2AddrFmly);
                    return;
            }
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Len       : %#x\n", u1PrfxLen);
            LDP_EXT_FEC_PREFIX (pu1TmpMsg, u4Prefix, u1PrfxLen << 3);
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix           : %#x\n", u4Prefix);
            break;
        default:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Unknown - Error.\n",
                      u1FecElmtType);
            return;
    }

    /* 
     * Check for the presence of labels done and if labels present they
     * are obtained.
     */

    if ((pMsg + u2HLen) == pu1TmpMsg)
    {
        LDP_DBG (LDP_PDU_DUMP, "LblWdrw Msg Has No Labels. \n");
    }
    else
    {
        while (pu1TmpMsg < (pMsg + u2HLen))
        {

            LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);
            LDP_EXT_4_BYTES (pu1TmpMsg, u4LabelOrReqId);
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x  - Label TLV.\n",
                      u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
            LDP_DBG1 (LDP_PDU_DUMP, "Label Value      : %#x\n", u4LabelOrReqId);

        }
    }
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpLblRelMsg                                            
 * Description   : This routine decodes and dumps an LDP Label Release Msg.
 * Input(s)      : pMsg, pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpLblRelMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{
    UINT1               u1FecElmtType;
    UINT1               u1PrfxLen;
    UINT2               u2HLen;
    UINT2               u2MsgType;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT2               u2AddrFmly;
    UINT4               u4MsgId;
    UINT4               u4Prefix;
    UINT4               u4LabelOrReqId;
    UINT1              *pu1TmpMsg = NULL;
    UINT2               u2PwType = LDP_ZERO;
    UINT1               u1InfoLen;
    UINT4               u4GroupID;
    UINT4               u4PwID;
    INT1                i1CBit;

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == LblRel  Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - LblRel  Msg \n",
              u2MsgType);

    /* Len of LblRel Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);    /* Msg  Id */
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    /* 
     * Decode the LblRel Msg and obtain the  contents and optional 
     * parameters if any.
     */
    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Ptr retained to check the end of message condition */
    pu1TmpMsg = pMsg;

    /* This length specifies the total length of all the tlvs
     * together cntned in this LblRel Msg */
    u2HLen -= LDP_MSG_ID_LEN;

    /* FEC TLV  - The mandatory TLV is accessed */
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);
    if (u2TlvType != LDP_FEC_TLV)
    {
        /* Error - mandatory TLV not present */
        LDP_DBG (LDP_PDU_DUMP, "LblRel  Msg has no FEC elements- Err. \n");
        LDP_DBG1 (LDP_PDU_DUMP, "TLV Type Rcvd    : %#x \n", u2TlvType);
        return;
    }
    LDP_EXT_1_BYTES (pu1TmpMsg, u1FecElmtType);
    if ((u2TlvLen > LDP_FEC_ELMNT_LEN) && (u1FecElmtType != LDP_FEC_PWVC_TYPE))
    {
        /* This implies, the received FEC TLv contains more 
         * than one Fec Element.
         * As of now only a single Fec is supported to be received
         * in a message*/
        LDP_DBG (LDP_PDU_DUMP,
                 "LblRel  Msg has More than one FEC Elmnt- Err. \n");
        return;
    }

    LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x - FEC Tlv  \n", u2TlvType);
    LDP_DBG1 (LDP_PDU_DUMP, "TLV Len          : %d \n", u2TlvLen);

    /* Getting FecElement Type */
    switch (u1FecElmtType)
    {
        case LDP_FEC_PWVC_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC 128          : %#x  - PwVc Type\n", u1FecElmtType);

            LDP_EXT_2_BYTES (pu1TmpMsg, u2PwType);
            LDP_GET_PWVC_C_BIT (u2PwType, i1CBit);
            if (i1CBit)
            {
                LDP_DBG1 (LDP_PDU_DUMP, "Control Word     : %#x   - Present\n",
                          i1CBit);
            }
            else
            {
                LDP_DBG1 (LDP_PDU_DUMP,
                          "Control Word     : %#x   - Not present\n", i1CBit);
            }

            u2PwType &= 0x0fff;
            LDP_DBG1 (LDP_PDU_DUMP, "PW-Type          : %d  \n", u2PwType);

            LDP_EXT_1_BYTES (pu1TmpMsg, u1InfoLen);
            LDP_DBG1 (LDP_PDU_DUMP, "VC Info Len      : %d\n", u1InfoLen);

            LDP_EXT_4_BYTES (pu1TmpMsg, u4GroupID);
            LDP_DBG1 (LDP_PDU_DUMP, "VC Group ID      : %d\n", u4GroupID);
            if (u1InfoLen > LDP_PWVC_FEC_ELEM_PWVCID_LEN)
            {
                LDP_DBG (LDP_PDU_DUMP, "Erronous FEC Element\n");
            }
            else if (u1InfoLen == LDP_PWVC_FEC_ELEM_PWVCID_LEN)
            {
                LDP_EXT_4_BYTES (pu1TmpMsg, u4PwID);
                LDP_DBG1 (LDP_PDU_DUMP, "VC PW ID         : %d\n", u4PwID);
            }
            break;

        case LDP_FEC_WILDCARD_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Wild Card Type.\n",
                      u1FecElmtType);
            break;
        case LDP_FEC_CRLSP_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP, "FEC Type         : %#x  - CRLSP Type.\n",
                      u1FecElmtType);
            break;
        case LDP_FEC_PREFIX_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP, "FEC Type         : %#x  - Prefix Type.\n",
                      u1FecElmtType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2AddrFmly);
            LDP_EXT_1_BYTES (pu1TmpMsg, u1PrfxLen);
            switch (u2AddrFmly)
            {
                case IPV4:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv4.\n",
                              u2AddrFmly);
                    break;
                case IPV6:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv6.\n",
                              u2AddrFmly);
                    break;
                default:
                    LDP_DBG1 (LDP_PDU_DUMP,
                              "Addr Fmly        : %#x  - Unknown.\n",
                              u2AddrFmly);
                    return;
            }
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Len       : %#x\n", u1PrfxLen);
            if (u1PrfxLen == 0)
            {
                u4Prefix = LDP_DEF_DEST_ADDR;
                LDP_DBG1 (LDP_PDU_DUMP,
                          "Prefix           : %#x - Default Addr.\n", u4Prefix);
            }
            else
            {
                LDP_EXT_FEC_PREFIX (pu1TmpMsg, u4Prefix, u1PrfxLen);
                LDP_DBG1 (LDP_PDU_DUMP, "Prefix           : %#x\n", u4Prefix);
            }
            break;
        case LDP_FEC_HOSTADDR_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Host Addr Type.\n",
                      u1FecElmtType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2AddrFmly);
            LDP_EXT_1_BYTES (pu1TmpMsg, u1PrfxLen);
            switch (u2AddrFmly)
            {
                case IPV4:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv4.\n",
                              u2AddrFmly);
                    break;
                case IPV6:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv6.\n",
                              u2AddrFmly);
                    break;

                default:
                    LDP_DBG1 (LDP_PDU_DUMP,
                              "Addr Fmly        : %#x  - Unknown.\n",
                              u2AddrFmly);
                    return;
            }
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Len       : %#x\n", u1PrfxLen);
            LDP_EXT_FEC_PREFIX (pu1TmpMsg, u4Prefix, u1PrfxLen << 3);
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix           : %#x\n", u4Prefix);
            break;
        default:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Unknown - Error.\n",
                      u1FecElmtType);
            return;
    }

    /* 
     * Check for the presence of labels done and if labels present they
     * are obtained.
     */
    if ((pMsg + u2HLen) == pu1TmpMsg)
    {
        LDP_DBG (LDP_PDU_DUMP, "LblRel Msg Has No Labels. \n");
    }
    else
    {
        while (pu1TmpMsg < (pMsg + u2HLen))
        {
            LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);
            LDP_EXT_4_BYTES (pu1TmpMsg, u4LabelOrReqId);
            LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x  - Label TLV.\n",
                      u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TlvLen);
            LDP_DBG1 (LDP_PDU_DUMP, "Label Value      : %#x\n", u4LabelOrReqId);
        }
    }
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpLblAbortReqMsg                                        
 * Description   : This routine decodes and dumps an LDP Label Abort Msg.
 * Input(s)      : pMsg, pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpLblAbortReqMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{

    UINT1               u1FecElmtType;
    UINT1               u1PrfxLen;
    UINT2               u2HLen;
    UINT2               u2MsgType;
    UINT2               u2TlvType;
    UINT2               u2TlvLen;
    UINT2               u2AddrFmly;
    UINT4               u4MsgId;
    UINT4               u4Prefix;
    UINT4               u4LabelOrReqId;
    UINT1              *pu1TmpMsg = NULL;

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == LblAbrt  Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - LblAbrt  Msg \n",
              u2MsgType);

    /* Len of LblAbrt Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    /* Msg  Id */
    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    /* 
     * Decode the LblAbrt Msg and obtain the  FEC TLV contents and the Label 
     * Request Message ID contents.
     */
    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* Ptr retained to check the end of message condition */
    pu1TmpMsg = pMsg;

    /* This length specifies the total length of all the tlvs
     * together cntned in this LblRel Msg */
    u2HLen -= LDP_MSG_ID_LEN;

    /* FEC TLV  - The mandatory TLV is accessed */
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);
    if (u2TlvType != LDP_FEC_TLV)
    {
        /* Error - mandatory TLV not present */
        LDP_DBG (LDP_PDU_DUMP, "LblAbrt  Msg has no FEC elements- Err. \n");
        LDP_DBG1 (LDP_PDU_DUMP, "TLV Type Rcvd    : %#x \n", u2TlvType);
        return;
    }

    if (u2TlvLen > LDP_FEC_ELMNT_LEN)
    {
        /* This implies, the received FEC TLv contains more than one 
         * Fec Element. As of now only a single Fec is supported to be
         * received in a message*/
        LDP_DBG (LDP_PDU_DUMP,
                 "LblAbrt Msg has More than one FEC Elmnt- Err. \n");
        return;
    }

    LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x - FEC Tlv  \n", u2TlvType);
    LDP_DBG1 (LDP_PDU_DUMP, "TLV Len          : %d \n", u2TlvLen);

    /* Getting FecElement Type */
    LDP_EXT_1_BYTES (pu1TmpMsg, u1FecElmtType);
    switch (u1FecElmtType)
    {
        case LDP_FEC_WILDCARD_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Wild Card Type.\n",
                      u1FecElmtType);
            break;
        case LDP_FEC_CRLSP_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP, "FEC Type         : %#x  - CRLSP Type.\n",
                      u1FecElmtType);
            break;
        case LDP_FEC_PREFIX_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP, "FEC Type         : %#x  - Prefix Type.\n",
                      u1FecElmtType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2AddrFmly);
            LDP_EXT_1_BYTES (pu1TmpMsg, u1PrfxLen);
            switch (u2AddrFmly)
            {
                case IPV4:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv4.\n",
                              u2AddrFmly);
                    break;
                case IPV6:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv6.\n",
                              u2AddrFmly);
                    break;
                default:
                    LDP_DBG1 (LDP_PDU_DUMP,
                              "Addr Fmly        : %#x  - Unknown.\n",
                              u2AddrFmly);
                    return;
            }
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Len       : %#x\n", u1PrfxLen);
            if (u1PrfxLen == LDP_ZERO)
            {
                u4Prefix = LDP_DEF_DEST_ADDR;
                LDP_DBG1 (LDP_PDU_DUMP,
                          "Prefix           : %#x - Default Addr.\n", u4Prefix);
            }
            else
            {
                LDP_EXT_FEC_PREFIX (pu1TmpMsg, u4Prefix, u1PrfxLen);
                LDP_DBG1 (LDP_PDU_DUMP, "Prefix           : %#x\n", u4Prefix);
            }
            break;
        case LDP_FEC_HOSTADDR_TYPE:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Host Addr Type.\n",
                      u1FecElmtType);
            LDP_EXT_2_BYTES (pu1TmpMsg, u2AddrFmly);
            LDP_EXT_1_BYTES (pu1TmpMsg, u1PrfxLen);
            switch (u2AddrFmly)
            {
                case IPV4:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv4.\n",
                              u2AddrFmly);
                    break;
                case IPV6:
                    LDP_DBG1 (LDP_PDU_DUMP, "Addr Fmly        : %#x  - IPv6.\n",
                              u2AddrFmly);
                    break;
                default:
                    LDP_DBG1 (LDP_PDU_DUMP,
                              "Addr Fmly        : %#x  - Unknown.\n",
                              u2AddrFmly);
                    return;
            }
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix Len       : %#x\n", u1PrfxLen);
            LDP_EXT_FEC_PREFIX (pu1TmpMsg, u4Prefix, u1PrfxLen << LDP_THREE);
            LDP_DBG1 (LDP_PDU_DUMP, "Prefix           : %#x\n", u4Prefix);
            break;
        default:
            LDP_DBG1 (LDP_PDU_DUMP,
                      "FEC Type         : %#x  - Unknown - Error.\n",
                      u1FecElmtType);
            return;
    }

    if ((pMsg + u2HLen) == pu1TmpMsg)
    {
        LDP_DBG (LDP_PDU_DUMP, "LblAbrt Msg Has No LblReqMsgId Tlv - Err. \n");
        return;
    }

    /* Obtaining the Label Request Message ID TLV Contents */
    /* FEC TLV  - The mandatory TLV is accessed */
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvType);
    LDP_EXT_2_BYTES (pu1TmpMsg, u2TlvLen);
    if (u2TlvType != LDP_LBL_REQ_MSGID_TLV)
    {
        /* Error - mandatory TLV not present */
        LDP_DBG (LDP_PDU_DUMP, "LblAbrt  Msg has no LblReqMsgId Tlv - Err. \n");
        LDP_DBG1 (LDP_PDU_DUMP, "TLV Type Rcvd    : %#x \n", u2TlvType);
        return;
    }
    LDP_EXT_4_BYTES (pu1TmpMsg, u4LabelOrReqId);
    LDP_DBG1 (LDP_PDU_DUMP, "TLV Type         : %#x - LblReqMsgID Tlv  \n",
              u2TlvType);
    LDP_DBG1 (LDP_PDU_DUMP, "TLV Len          : %d \n", u2TlvLen);
    LDP_DBG1 (LDP_PDU_DUMP, "Label Req Id     : %d \n", u4LabelOrReqId);
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpStatusCode                                            
 * Description   : This routine Prints the details of the received status code.
 * Input(s)      : u4StatusCode 
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpStatusCode (UINT4 u4StatusCode)
{
    if ((u4StatusCode & LDP_STAT_E_BIT) == LDP_STAT_E_BIT)
    {
        LDP_DBG (LDP_PDU_DUMP, "                 : Fatal Err indication. \n");
    }
    if ((u4StatusCode & LDP_STAT_F_BIT) == LDP_STAT_F_BIT)
    {
        LDP_DBG (LDP_PDU_DUMP, "                 : Err indn to be Fwded. \n");
    }
    /* Status code is masked off for the E and F bits. */
    switch (u4StatusCode & LDP_STATUS_DATA_MASK)
    {
        case LDP_STAT_SUCCESS:
            LDP_DBG (LDP_PDU_DUMP, "                 : LDP_STAT_SUCCESS.\n");
            break;
        case LDP_STAT_BAD_LDPID:
            LDP_DBG (LDP_PDU_DUMP, "                 : LDP_STAT_BAD_LDPID.\n");
            break;
        case LDP_STAT_BAD_PROT_VER:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_BAD_PROT_VER.\n");
            break;
        case LDP_STAT_BAD_PDU_LEN:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_BAD_PDU_LEN.\n");
            break;
        case LDP_STAT_BAD_MSG_LEN:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_BAD_MSG_LEN.\n");
            break;
        case LDP_STAT_BAD_TLV_LEN:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_BAD_TLV_LEN.\n");
            break;
        case LDP_STAT_CRPTD_TLV_VAL:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_CRPTD_TLV_VAL.\n");
            break;
        case LDP_STAT_HOLD_TMR_EXPRD:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_HOLD_TMR_EXPRD.\n");
            break;
        case LDP_STAT_SHUT_DOWN:
            LDP_DBG (LDP_PDU_DUMP, "                 : LDP_STAT_SHUT_DOWN.\n");
            break;
        case LDP_STAT_NO_HELLO:
            LDP_DBG (LDP_PDU_DUMP, "                 : LDP_STAT_NO_HELLO.\n");
            break;
        case LDP_STAT_ADVRT_MODE_REJECT:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_ADVRT_MODE_REJECT.\n");
            break;
        case LDP_STAT_MAX_PDULEN_REJECT:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_MAX_PDULEN_REJECT.\n");
            break;
        case LDP_STAT_LBL_RANGE_REJECT:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_LBL_RANGE_REJECT.\n");
            break;
        case LDP_STAT_KEEPALIVE_EXPRD:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_KEEPALIVE_EXPRD.\n");
            break;
        case LDP_STAT_UNKNOWN_MSG_TYPE:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_UNKNOWN_MSG_TYPE.\n");
            break;
        case LDP_STAT_UNKNOWN_TLV:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_UNKNOWN_TLV.\n");
            break;

        case LDP_STAT_LOOP_DETECTED:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_LOOP_DETECTED.\n");
            break;
        case LDP_STAT_BAD_EROUTE_TLV_ERR:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_BAD_EROUTE_TLV_ERR.\n");
            break;
        case LDP_STAT_BAD_STRICT_NODE_ERR:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_BAD_STRICT_NODE_ERR.\n");
            break;
        case LDP_STAT_BAD_LOOSE_NODE_ERR:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_BAD_LOOSE_NODE_ERR.\n");
            break;
        case LDP_STAT_BAD_INIT_ER_HOP_ERR:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_BAD_INIT_ER_HOP_ERR.\n");
            break;
        case LDP_STAT_RESOURCE_UNAVAIL:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_RESOURCE_UNAVAIL.\n");
            break;
        case LDP_STAT_TRAF_PARMS_UNAVL:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_TRAF_PARMS_UNAVL.\n");
            break;
        case LDP_STAT_CRLSP_SETUP_ABORT:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_CRLSP_SETUP_ABORT.\n");
            break;
        case LDP_STAT_CRLSP_MODIFY_NOTSUP:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_CRLSP_MODIFY_NOTSUP.\n");
            break;
        case LDP_STAT_UNKNOWN_FEC:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_UNKNOWN_FEC.\n");
            break;
        case LDP_STAT_NO_ROUTE:
            LDP_DBG (LDP_PDU_DUMP, "                 : LDP_STAT_NO_ROUTE.\n");
            break;
        case LDP_STAT_NO_LBL_RSRC:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_NO_LBL_RSRC.\n");
            break;
        case LDP_STAT_LBL_RSRC_AVBL:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_LBL_RSRC_AVBL.\n");
            break;
        case LDP_STAT_LBL_REQ_ABRTD:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_LBL_REQ_ABRTD.\n");
            break;
        case LDP_STAT_MISSING_MSG_PARAM:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_MISSING_MSG_PARAM.\n");
            break;
        case LDP_STAT_UNSUPP_ADDR_FMLY:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_UNSUPP_ADDR_FMLY.\n");
            break;
        case LDP_STAT_BAD_KEEPALIVE_TMR:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_BAD_KEEPALIVE_TMR.\n");
            break;
        case LDP_STAT_INTERNAL_ERROR:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_INTERNAL_ERROR.\n");
            break;
        case LDP_STAT_CRLSP_PREEMPTED:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : LDP_STAT_CRLSP_PREEMPTED.\n");
            break;
        default:
            LDP_DBG (LDP_PDU_DUMP,
                     "                 : UnKnown status - Err.\n");
            break;
    }                            /* end of Switch */
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpNotifMsg                                            
 * Description   : This routine decodes and dumps an LDP Notif Msg.
 * Input(s)      : pMsg, pPduHdr
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDumpNotifMsg (UINT1 *pMsg, tLdpPduHdr * pPduHdr)
{

    UINT1               u1MTlvPrcd = LDP_FALSE;
    UINT2               u2MsgType;
    UINT2               u2HLen;
    UINT2               u2Len = 0;
    UINT4               u4MsgId;
    UINT4               u4StatusCode;
    UINT2               u2TlvType;
    UINT2               u2TmpLen;
    UINT2               u2Val;
    UINT4               u4Val;

    LdpDumpPduHdr (pPduHdr);

    LDP_GET_2_BYTES (pMsg, u2MsgType);    /* Msg Type == Notif Msg */
    LDP_DBG (LDP_PDU_DUMP, "LDP MSG HDR CONTENTS : \n");
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Type     : %#x - Notif Msg \n", u2MsgType);

    /* Length of Notif Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Len      : %d \n", u2HLen);

    /* Msg  Id */
    LDP_GET_4_BYTES ((pMsg + LDP_MSG_HDR_LEN), u4MsgId);
    LDP_DBG1 (LDP_PDU_DUMP, "LDP Msg Id       : %d \n", u4MsgId);

    /* 
     * Decode the Notif Msg and obtain the status tlv contents and optional 
     * parameters if any.
     */
    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);

    /* This length specifies the total length of all the tlvs
     * together contained in this NotifMsg */
    u2HLen -= LDP_MSG_ID_LEN;

    while (u2Len < u2HLen)
    {
        LDP_EXT_2_BYTES (pMsg, u2TlvType);    /* Extract the Tlv Type */
        LDP_EXT_2_BYTES (pMsg, u2TmpLen);    /* Extract the Tlv Len */
        /* Check for the bad Tlv length is done here */
        if ((u2Len + u2TmpLen + LDP_TLV_HDR_LEN) > u2HLen)
        {
            LDP_DBG (LDP_PDU_DUMP, "PRCS: Bad Tlv Len in the Notif Msg\n");
            LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Type     : %#x \n", u2TlvType);
            LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d  \n", u2TmpLen);
            return;
        }

        switch (u2TlvType)
        {
            case LDP_STATUS_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "LDP TLV Type     : %#x - STATUS TLV.\n", u2TlvType);

                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d  \n", u2TmpLen);

                LDP_EXT_4_BYTES (pMsg, u4StatusCode);    /* Get the Status code from
                                                           the Tlv */
                LDP_DBG1 (LDP_PDU_DUMP, "Status code      : %#x  \n",
                          u4StatusCode);
                LdpDumpStatusCode (u4StatusCode);

                LDP_EXT_4_BYTES (pMsg, u4MsgId);    /* Get the Message ID from
                                                       the Tlv */
                LDP_DBG1 (LDP_PDU_DUMP, "Message Id       : %d  \n", u4MsgId);
                LDP_EXT_2_BYTES (pMsg, u2MsgType);    /* Get the Message ID from
                                                       the Tlv */
                LDP_DBG1 (LDP_PDU_DUMP, "Notf for Msg Type: %#x  \n",
                          u2MsgType);
                u1MTlvPrcd = LDP_TRUE;
                break;

            case LDP_EXTND_STATUS_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "LDP TLV Type     : %#x - Extnd STATUS TLV.\n",
                          u2TlvType);

                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d  \n", u2TmpLen);

                LDP_EXT_4_BYTES (pMsg, u4StatusCode);    /* Get the Status code from
                                                           the Tlv */
                LDP_DBG1 (LDP_PDU_DUMP, "ExtndSts val     : %#x  \n",
                          u4StatusCode);
                break;

            case LDP_RETURNED_PDU_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "LDP TLV Type     : %#x - Rtnd PDU TLV.\n",
                          u2TlvType);

                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d  \n", u2TmpLen);
                LDP_DBG (LDP_PDU_DUMP,
                         "                 : Crntly PDUhdr not dmpd.\n");
                /* currently PDU not dumped. to be done */
                /* Increment is made to move on properly here */
                pMsg += u2TmpLen;

                break;

            case LDP_RETURNED_MSG_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "LDP TLV Type     : %#x - Rtnd MSG TLV.\n",
                          u2TlvType);

                LDP_DBG1 (LDP_PDU_DUMP, "LDP TLV Len      : %d  \n", u2TmpLen);
                LDP_DBG (LDP_PDU_DUMP,
                         "                 : Crntly Msginfo not dmpd.\n");
                /* currently Msg Info Not dumped. to be done */
                /* Increment is made to move on properly here */
                pMsg += u2TmpLen;

                break;

            case CRLDP_LSPID_TLV:
                LDP_DBG1 (LDP_PDU_DUMP,
                          "TLV Type         : %#x  - CRLDP LSPID TLV.\n",
                          u2TlvType);
                LDP_DBG1 (LDP_PDU_DUMP, "Tlv Length       : %d\n", u2TmpLen);

                LDP_EXT_2_BYTES (pMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Resv_Act_Flag    : %#x\n", u2Val);
                if (u2Val == 0x00000000)
                {
                    LDP_DBG (LDP_PDU_DUMP,
                             "Act_Flag indn  - Initial LSP Setup.\n");
                }
                else if (u2Val == CRLDP_MODIFY_REQ)
                {
                    LDP_DBG (LDP_PDU_DUMP,
                             "Act_Flag indn  - Modify LSP Setup.\n");
                }
                LDP_EXT_2_BYTES (pMsg, u2Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Local CR_LSP_ID  : %d\n", u2Val);

                LDP_EXT_4_BYTES (pMsg, u4Val);
                LDP_DBG1 (LDP_PDU_DUMP, "Ingrss LSR ID    : %#x\n", u4Val);
                break;

            default:
                LDP_DBG (LDP_PDU_DUMP, "PRCS: Invalid TlvType in Notif Msg\n");
                break;
        }

        /* Check if Mand Tlv is processed or not */
        if (u1MTlvPrcd != LDP_TRUE)
        {
            LDP_DBG (LDP_PDU_DUMP,
                     "PRCS: Status Tlv(Mandatory) is not present\n");
            return;
        }

        /* Increment by the msg length processed */
        u2Len += (u2TmpLen + LDP_TLV_HDR_LEN);
    }
    /* End of the While loop for decoding the Tlvs in Notif Msg */
    return;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDumpLdpPdu                                            
 * Description   : This routine decodes an LDP PDU and dumps.
 * Input(s)      : pPDU 
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */

VOID
LdpDumpLdpPdu (tCRU_BUF_CHAIN_HEADER * pPDU, INT4 i4DbgMsgDir)
{
    UINT1              *pMsg = NULL;
    UINT2               u2Len = 0;
    UINT2               u2BufSize = 0;
    UINT2               u2Offset = 0;
    UINT2               u2MsgType = 0;
    UINT2               u2MsgLen = 0;
    tLdpPduHdr          PduHdr;
    tLdpMsgHdrAndId     MsgHdr;    /* will be accessing only the msg hdr part */

    MEMSET (&MsgHdr, 0, sizeof (tLdpMsgHdrAndId));
    MEMSET (&PduHdr, 0, sizeof (tLdpPduHdr));
    u2BufSize = (UINT2) CRU_BUF_Get_ChainValidByteCount (pPDU);

    /* Copy the Pdu Hdr to PduHdr Structure */
    if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &PduHdr, LDP_ZERO,
                                   LDP_PDU_HDR_LEN) != LDP_PDU_HDR_LEN)
    {
        return;
    }

    u2BufSize -= LDP_PDU_HDR_LEN;
    u2Offset = LDP_PDU_HDR_LEN;
    /* Extract Messages from the Buffer and store them on Linear buffers */
    for (u2Len = LDP_ZERO; u2Len < u2BufSize;
         u2Offset += (u2MsgLen + LDP_MSG_HDR_LEN))
    {
        /* Copy the Msg hdr from buffer */
        if (CRU_BUF_Copy_FromBufChain (pPDU, (UINT1 *) &MsgHdr, u2Offset,
                                       LDP_MSG_HDR_LEN) != LDP_MSG_HDR_LEN)
        {
            return;
        }

        u2MsgType = OSIX_NTOHS (MsgHdr.u2MsgType);
        u2MsgLen = OSIX_NTOHS (MsgHdr.u2MsgLen);

        pMsg = (UINT1 *) MemAllocMemBlk (LDP_MSG_POOL_ID);
        if (pMsg == NULL)
        {
            LDP_DBG (LDP_MAIN_MEM,
                     "Memory Allocation failed for LDP Message Pool during "
                     "dumping of messages\r\n");
            return;
        }

        /* Copy one msg from the chain to linear buffer */
        if (CRU_BUF_Copy_FromBufChain (pPDU, pMsg, u2Offset,
                                       (u2MsgLen + LDP_MSG_HDR_LEN))
            != (u2MsgLen + LDP_MSG_HDR_LEN))
        {
            MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);
            return;
        }

        switch (u2MsgType)
        {
            case LDP_HELLO_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_HELLO) == LDP_DUMP_HELLO)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpHelloMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;
            case LDP_INIT_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_INIT) == LDP_DUMP_INIT)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpInitMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;
            case LDP_KEEP_ALIVE_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_KEEPALIVE) == LDP_DUMP_KEEPALIVE)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpKeepAliveMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;

            case LDP_ADDR_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_ADDR) == LDP_DUMP_ADDR)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpAddrMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;
            case LDP_ADDR_WITHDRAW_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_ADDRW) == LDP_DUMP_ADDRW)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpAddrWithDrawMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;
            case LDP_LBL_MAPPING_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_LDP_MAP) == LDP_DUMP_LDP_MAP)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpLblMapMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;
            case LDP_LBL_REQUEST_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_LDP_RQ) == LDP_DUMP_LDP_RQ)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpLblReqMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;
            case LDP_LBL_WITHDRAW_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_LDP_WDRAW) == LDP_DUMP_LDP_WDRAW)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpLblWithDrawMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;
            case LDP_LBL_RELEASE_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_LDP_REL) == LDP_DUMP_LDP_REL)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpLblRelMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;
            case LDP_LBL_ABORT_REQ_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_LDP_ABORT) == LDP_DUMP_LDP_ABORT)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpLblAbortReqMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;
            case LDP_NOTIF_MSG:
                if ((gu4LdpDumpType & LDP_DUMP_LDP_NOTIF) == LDP_DUMP_LDP_NOTIF)
                {
                    LdpDisplayDumpHdr (i4DbgMsgDir);
                    LdpDumpNotifMsg (pMsg, &PduHdr);
                    LDP_DBG (LDP_PDU_DUMP,
                             "---------------------------------------------\n");
                }
                break;
            default:
                break;
        }

        MemReleaseMemBlock (LDP_MSG_POOL_ID, pMsg);

        u2Len += (UINT2) (u2MsgLen + LDP_MSG_HDR_LEN);
    }
    return;

}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDisplayDumpHdr                                            
 * Description   : This routine displays the direction of messages.
 * Input(s)      : i4DbgMsgDir
 * Output(s)     : None
 * Return(s)     : None
 -------------------------------------------------------------------------- */
VOID
LdpDisplayDumpHdr (INT4 i4DbgMsgDir)
{
    LDP_DBG (LDP_PDU_DUMP, "---------------------------------------------\n");
    if (i4DbgMsgDir == DUMP_DIR_OUT)
    {
        LDP_DBG (LDP_PDU_DUMP, "                MSG SENT\n");
    }
    else if (i4DbgMsgDir == LDP_DUMP_DIR_IN)
    {
        LDP_DBG (LDP_PDU_DUMP, "                MSG RECEIVED\n");
    }
    LDP_DBG (LDP_PDU_DUMP, "---------------------------------------------\n");
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpCreateLdpIfTableEntry
 * Description   : This routine adds the Interface to the Interface list of the entity.
 * Input(s)      : u4IfIndex, u1IfOperStatus, u4IfAddr
 * Output(s)     : pLdpEntity
 * Return(s)     : None
 -------------------------------------------------------------------------- */
tLdpIfTableEntry   *
LdpCreateLdpIfTableEntry (UINT4 u4IfIndex,
                          UINT1 u1IfOperStatus,
                          tLdpEntity * pLdpEntity, UINT4 u4IfAddr)
{
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    UINT4               u4IncarnId = MPLS_DEF_INCARN;

    LDP_DBG1 (LDP_DBG_PRCS, "%s: ENTRY\n", __func__);
    pLdpIfTableEntry = (tLdpIfTableEntry *) MemAllocMemBlk (LDP_EIF_POOL_ID);

    if (pLdpIfTableEntry == NULL)
    {
        LDP_DBG (LDP_IF_MEM, "Memory allocation failed for LDP If Table\r\n");
        return NULL;
    }

    MEMSET ((VOID *) pLdpIfTableEntry, LDP_ZERO, sizeof (tLdpIfTableEntry));

    TMO_SLL_Init_Node (&(pLdpIfTableEntry->NextIfEntry));
    /* IfList is added to the SLL List */
    pLdpIfTableEntry->u4IfIndex = u4IfIndex;
    LDP_DBG3 (LDP_DBG_PRCS, "%s: OperStatus for If Address: %x is %u\n",
              __func__, u4IfAddr, u1IfOperStatus);

    if (u1IfOperStatus == CFA_IF_UP)
    {
        u4IfAddr = OSIX_HTONL (u4IfAddr);
    }
    else
    {
        u4IfAddr = LDP_ZERO;
    }

    MEMCPY ((UINT1 *) &pLdpIfTableEntry->IfAddr,
            (UINT1 *) &u4IfAddr, LDP_IPV4ADR_LEN);

    pLdpIfTableEntry->u1InterfaceType = LDP_IFTYPE_IPV4;

    (pLdpIfTableEntry->u2Count)++;

    /* The Speed should be ideally filled in by
     * consulting the Ip interface information
     * eg ATM(155), Ethernet(10-100).
     */
    /* Porting code required for configuring
     * the Speed.
     * Here the Speed is considered to be in
     * bits per second.
     */
    if (LDP_GET_IFACE_PD (u4IncarnId, u4IfIndex) == LDP_ZERO)
    {
        LDP_SET_IFACE_SPEED (u4IncarnId, u4IfIndex, LDP_ETH_IF_SPEED);
    }

    if (u1IfOperStatus == CFA_IF_UP)
    {
        pLdpIfTableEntry->u1OperStatus = LDP_IF_OPER_ENABLE;
    }
    else if (u1IfOperStatus == CFA_IF_DOWN)
    {
        pLdpIfTableEntry->u1OperStatus = LDP_IF_OPER_DISABLE;
    }

    TMO_SLL_Add ((tTMO_SLL *) (&(pLdpEntity->IfList)),
                 &(pLdpIfTableEntry->NextIfEntry));

    return pLdpIfTableEntry;
}

#ifdef MPLS_IPV6_WANTED
/* ---------------------------------------------------------------------------
 * Function Name : LdpCreateLdpIpv6IfTableEntry
 * Description   : This routine adds the Interface to the Interface list of the entity.
 * Input(s)      : u4IfIndex, u1IfOperStatus, u4IfAddr
 * Output(s)     : pLdpEntity
 * Return(s)     : None
 -------------------------------------------------------------------------- */
tLdpIfTableEntry   *
LdpCreateLdpIpv6IfTableEntry (UINT4 u4IfIndex,
                              UINT1 u1IfOperStatus,
                              tLdpEntity * pLdpEntity, UINT1 u1InterfaceType,
                              UINT4 u4IfAddr, tIp6Addr * pLnklocalIpv6Addr,
                              tIp6Addr * pSitelocalIpv6Addr,
                              tIp6Addr * pGlbUniqueIpv6Addr)
{
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    UINT4               u4IncarnId = MPLS_DEF_INCARN;

    pLdpIfTableEntry = (tLdpIfTableEntry *) MemAllocMemBlk (LDP_EIF_POOL_ID);

    if (pLdpIfTableEntry == NULL)
    {
        LDP_DBG (LDP_IF_MEM, "Memory allocation failed for LDP If Table\r\n");
        return NULL;
    }

    MEMSET ((VOID *) pLdpIfTableEntry, LDP_ZERO, sizeof (tLdpIfTableEntry));

    TMO_SLL_Init_Node (&(pLdpIfTableEntry->NextIfEntry));
    /* IfList is added to the SLL List */
    pLdpIfTableEntry->u4IfIndex = u4IfIndex;

    if (u1IfOperStatus == CFA_IF_UP)
    {
        u4IfAddr = OSIX_HTONL (u4IfAddr);

        MEMCPY ((UINT1 *) &pLdpIfTableEntry->IfAddr,
                (UINT1 *) &u4IfAddr, LDP_IPV4ADR_LEN);

        MEMCPY (pLdpIfTableEntry->LnklocalIpv6Addr.u1_addr,
                pLnklocalIpv6Addr->u1_addr, LDP_IPV6ADR_LEN);

        MEMCPY (pLdpIfTableEntry->SitelocalIpv6Addr.u1_addr,
                pSitelocalIpv6Addr->u1_addr, LDP_IPV6ADR_LEN);

        MEMCPY (pLdpIfTableEntry->GlbUniqueIpv6Addr.u1_addr,
                pGlbUniqueIpv6Addr->u1_addr, LDP_IPV6ADR_LEN);
    }
    else
    {
        u4IfAddr = LDP_ZERO;
        MEMCPY ((UINT1 *) &pLdpIfTableEntry->IfAddr,
                (UINT1 *) &u4IfAddr, LDP_IPV4ADR_LEN);
        MEMSET (pLdpIfTableEntry->LnklocalIpv6Addr.u1_addr, LDP_ZERO,
                LDP_IPV6ADR_LEN);
        MEMSET (pLdpIfTableEntry->SitelocalIpv6Addr.u1_addr, LDP_ZERO,
                LDP_IPV6ADR_LEN);
        MEMSET (pLdpIfTableEntry->GlbUniqueIpv6Addr.u1_addr, LDP_ZERO,
                LDP_IPV6ADR_LEN);

    }
    pLdpIfTableEntry->u1InterfaceType = u1InterfaceType;

    (pLdpIfTableEntry->u2Count)++;

    /* The Speed should be ideally filled in by
     * consulting the Ip interface information
     * eg ATM(155), Ethernet(10-100).
     */
    /* Porting code required for configuring
     * the Speed.
     * Here the Speed is considered to be in
     * bits per second.
     */
    if (LDP_GET_IFACE_PD (u4IncarnId, u4IfIndex) == LDP_ZERO)
    {
        LDP_SET_IFACE_SPEED (u4IncarnId, u4IfIndex, LDP_ETH_IF_SPEED);
    }

    if (u1IfOperStatus == CFA_IF_UP)
    {
        pLdpIfTableEntry->u1OperStatus = LDP_IF_OPER_ENABLE;
    }
    else
    {
        pLdpIfTableEntry->u1OperStatus = LDP_IF_OPER_DISABLE;
    }

    TMO_SLL_Add ((tTMO_SLL *) (&(pLdpEntity->IfList)),
                 &(pLdpIfTableEntry->NextIfEntry));

    return pLdpIfTableEntry;
}
#endif

/* ---------------------------------------------------------------------------
 * Function Name : LdpAssociateIfEntry
 * Description   : This routine associates all the mpls capable interfaces with
 *                 the Ldp Entity.
 * Input(s)      : None
 * Output(s)     : pLdpEntity
 * Return(s)     : LDP_SUCCESS if success, otherwise LDP_FAILURE
 -------------------------------------------------------------------------- */

UINT1
LdpAssociateIfEntry (tLdpEntity * pLdpEntity)
{

    tCfaIfInfo          CfaIfInfo;
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    UINT4               u4MplsIfIndex = LDP_ZERO;
    UINT4               u4MplsNextIfIndex = LDP_ZERO;
    UINT4               u4IfIndex = LDP_ZERO;
    UINT4               u4IfAddr = LDP_ZERO;
    UINT4               u4IncarnId = MPLS_DEF_INCARN;
    UINT4               u4UnnumAssocIPIf = 0;
    UINT1               u1InterfaceType = 0;
#ifdef MPLS_IPV6_WANTED
    UINT1               u1GetIpv6IfAllAddr = LDP_FALSE;
    tIp6Addr            LnklocalIpv6Addr;
    tIp6Addr            SitelocalIpv6Addr;
    tIp6Addr            GlbUniqueIpv6Addr;
    tIp6Addr            TempIpv6Addr;
#endif

    MEMSET (&CfaIfInfo, LDP_ZERO, sizeof (tCfaIfInfo));
#ifdef MPLS_IPV6_WANTED
    MEMSET (&TempIpv6Addr, 0, sizeof (tIp6Addr));
#endif

    /* Return success in interface is already mapped or
     * if it is a targeted session*/
    if ((pLdpEntity->bIsIntfMapped == LDP_TRUE) ||
        (pLdpEntity->u1TargetFlag == LDP_TRUE))
    {
        return LDP_SUCCESS;
    }

    if (CfaApiGetFirstMplsIfInfo (&u4MplsIfIndex, &CfaIfInfo) != CFA_SUCCESS)
    {
        LDP_DBG (LDP_IF_SNMP, "Failed to fetch mpls capable interface\n");
        return LDP_FAILURE;
    }
    do
    {
        u4IfAddr = 0;
        u1InterfaceType = 0;
#ifdef MPLS_IPV6_WANTED
        u1GetIpv6IfAllAddr = LDP_FALSE;
#endif
#ifdef MPLS_IPV6_WANTED
        MEMSET (&LnklocalIpv6Addr, 0, sizeof (tIp6Addr));
        MEMSET (&SitelocalIpv6Addr, 0, sizeof (tIp6Addr));
        MEMSET (&GlbUniqueIpv6Addr, 0, sizeof (tIp6Addr));
#endif

        u4MplsNextIfIndex = u4MplsIfIndex;

        if (CfaUtilGetL3IfFromMplsIf (u4MplsNextIfIndex, &u4IfIndex, TRUE) !=
            CFA_SUCCESS)
        {
            LDP_DBG1 (LDP_IF_SNMP,
                      "Failed to Fetch L3 Interface from Mpls Interface%d\n",
                      u4MplsNextIfIndex);
            continue;
        }

        if (LdpIpGetIfAddr (u4IfIndex, &u4IfAddr) == LDP_FAILURE)
        {
            LDP_DBG1 (LDP_IF_SNMP,
                      "IP Address do not exist for interface %d\n", u4IfIndex);
#ifndef MPLS_IPV6_WANTED
            continue;
#endif
#ifdef MPLS_IPV6_WANTED
            if (LdpGetIpv6IfAllAddr
                ((INT4) u4IfIndex, &LnklocalIpv6Addr, &SitelocalIpv6Addr,
                 &GlbUniqueIpv6Addr) != LDP_SUCCESS)
            {
                LDP_DBG1 (LDP_IF_SNMP,
                          "IPV6 Address do not exist for interface %d\n",
                          u4IfIndex);
                continue;
            }
#endif
        }
#ifdef MPLS_IPV6_WANTED
        else
        {
            u1GetIpv6IfAllAddr = LDP_TRUE;
        }
#endif
        /*Since interface type is derived on the basis of IP Addresses receivd,There may be a case 
           for unnumbered interfaces when the IP ADDR received is zero that interface is also treated as IPV4 Type */
        CfaGetIfUnnumAssocIPIf (u4IfIndex, &u4UnnumAssocIPIf);
        LDP_DBG3 (LDP_IF_PRCS,
                  "LdpAssociateIfEntry:UnnumAssocIpIf is % d, Ip4Add is %x, IfIndex is %d \n",
                  u4UnnumAssocIPIf, u4IfAddr, u4IfIndex);
        if ((u4IfAddr != LDP_ZERO) || (LDP_ZERO != u4UnnumAssocIPIf))
        {
            u1InterfaceType = LDP_IFTYPE_IPV4;
        }
#ifdef MPLS_IPV6_WANTED
        if (LDP_TRUE == u1GetIpv6IfAllAddr)
        {
            if (LdpGetIpv6IfAllAddr
                ((INT4) u4IfIndex, &LnklocalIpv6Addr, &SitelocalIpv6Addr,
                 &GlbUniqueIpv6Addr) != LDP_SUCCESS)
            {
                LDP_DBG1 (LDP_IF_SNMP,
                          "IPV6 Address do not exist for interface %d\n",
                          u4IfIndex);
            }
        }
        if (LdpInterfaceIpv6Match (&LnklocalIpv6Addr,
                                   &SitelocalIpv6Addr,
                                   &GlbUniqueIpv6Addr) == LDP_TRUE)
        {
            u1InterfaceType = u1InterfaceType | LDP_IFTYPE_IPV6;
        }
#endif
        if (LdpGetLdpEntityIfEntry (u4IncarnId, pLdpEntity,
                                    u4IfIndex,
                                    &pLdpIfTableEntry) != LDP_SUCCESS)
        {

            switch (u1InterfaceType)
            {
                case LDP_IFTYPE_IPV4:
                    /*Add the Interface entry to the interface list
                     *                                  *of the entity*/
                    pLdpIfTableEntry = LdpCreateLdpIfTableEntry (u4IfIndex,
                                                                 CfaIfInfo.
                                                                 u1IfOperStatus,
                                                                 pLdpEntity,
                                                                 u4IfAddr);
                    if (pLdpIfTableEntry == NULL)
                    {
#if 0
                        printf
                            ("Interface entry addition failed to the Interface List : u4IfIndex=%d OperStatus=%d Ipv4Addr=%d\n",
                             u4IfIndex, CfaIfInfo.u1IfOperStatus, u4IfAddr);
#endif

                        return LDP_FAILURE;
                    }
#if 0
                    printf
                        ("Interface entry added successfully to the Interface List : u4IfIndex=%d OperStatus=%d Ipv4Addr=%d\n",
                         u4IfIndex, CfaIfInfo.u1IfOperStatus, u4IfAddr);
#endif
                    break;
#if MPLS_IPV6_WANTED
                case LDP_IFTYPE_IPV6:
                case LDP_IFTYPE_DUAL:
                    if ((u4IfAddr != LDP_ZERO)
                        ||
                        (MEMCMP
                         (&LnklocalIpv6Addr, &TempIpv6Addr,
                          LDP_IPV6ADR_LEN) != LDP_ZERO)
                        ||
                        (MEMCMP
                         (&SitelocalIpv6Addr, &TempIpv6Addr,
                          LDP_IPV6ADR_LEN) != LDP_ZERO)
                        ||
                        (MEMCMP
                         (&GlbUniqueIpv6Addr, &TempIpv6Addr,
                          LDP_IPV6ADR_LEN) != LDP_ZERO))
                    {
                        pLdpIfTableEntry =
                            LdpCreateLdpIpv6IfTableEntry (u4IfIndex,
                                                          CfaIfInfo.
                                                          u1IfOperStatus,
                                                          pLdpEntity,
                                                          u1InterfaceType,
                                                          u4IfAddr,
                                                          &LnklocalIpv6Addr,
                                                          &SitelocalIpv6Addr,
                                                          &GlbUniqueIpv6Addr);

                        if (pLdpIfTableEntry == NULL)
                        {
#if 0
                            printf
                                ("Interface entry addition failed to the Interface List : u4IfIndex=%d OperStatus=%d Ipv4Addr=%d LinkLocalAddr=%s SiteLocalAddr=%s GlblUniqueAddr=%s\n",
                                 u4IfIndex, CfaIfInfo.u1IfOperStatus, u4IfAddr,
                                 Ip6PrintAddr (&LnklocalIpv6Addr),
                                 Ip6PrintAddr (&SitelocalIpv6Addr),
                                 Ip6PrintAddr (&GlbUniqueIpv6Addr));
#endif
                            return LDP_FAILURE;
                        }

#if 0
                        printf
                            ("Interface entry addition success to the Interface List : u4IfIndex=%d OperStatus=%d Ipv4Addr=%d LinkLocalAddr=%s SiteLocalAddr=%s GlblUniqueAddr=%s\n",
                             u4IfIndex, CfaIfInfo.u1IfOperStatus, u4IfAddr,
                             Ip6PrintAddr (&LnklocalIpv6Addr),
                             Ip6PrintAddr (&SitelocalIpv6Addr),
                             Ip6PrintAddr (&GlbUniqueIpv6Addr));
#endif
                    }
                    break;
#endif
            };
        }
    }
    while (CfaApiGetNextMplsIfInfo
           (u4MplsNextIfIndex, &u4MplsIfIndex, &CfaIfInfo) == CFA_SUCCESS);

    return LDP_SUCCESS;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpIsIntfMapped
 * Description   : This routine checks whether the L3 Interface is mapped to
 *                 LDP entity irrespective of whether the interface is user 
 *                 configured or automatically associated.
 *                 
 * Input(s)      : u4L3IfIndex 
 * Return(s)     : LDP_TRUE if success, otherwise LDP_FALSE
 -------------------------------------------------------------------------- */

BOOL1
LdpIsIntfMapped (UINT4 u4L3IfIndex)
{
    UINT2               u2IncarnId = MPLS_DEF_INCARN;
    tTMO_SLL           *pEntityList = &LDP_ENTITY_LIST (u2IncarnId);
    tLdpEntity         *pLdpEntity = NULL;
    tLdpIfTableEntry   *pIfEntry = NULL;

    TMO_SLL_Scan (pEntityList, pLdpEntity, tLdpEntity *)
    {
        /* check whether the interface is associated with
         * a LDP entity, If yes, return TRUE */
        if (LDP_SUCCESS == LdpGetLdpEntityIfEntry ((UINT4) u2IncarnId,
                                                   pLdpEntity,
                                                   u4L3IfIndex, &pIfEntry))
        {
            return LDP_TRUE;
        }
    }
    return LDP_FALSE;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpDisassociateIfEntry 
 * Description   : This routine disassociates the interfaces from the entity
 * Input(s)      : None
 * Output(s)     : pLdpEntity
 * Return(s)     : None
 -------------------------------------------------------------------------- */

VOID
LdpDisassociateIfEntry (tLdpEntity * pLdpEntity)
{

    tLdpIfTableEntry   *pIfEntry = NULL;

    pIfEntry = (tLdpIfTableEntry *) TMO_SLL_First (&(pLdpEntity->IfList));

    while (pIfEntry != NULL)
    {
        TMO_SLL_Delete (&(pLdpEntity->IfList), &(pIfEntry->NextIfEntry));
        MemReleaseMemBlock (LDP_EIF_POOL_ID, (UINT1 *) pIfEntry);
        pIfEntry = (tLdpIfTableEntry *) TMO_SLL_First (&(pLdpEntity->IfList));
    }
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpGetOtherEntityForActivation
 * Description   : This routine scans the LDP Entity Database and checks if
 *                 any other entity can be made OPER_UP.
 *                 This is applicable only for those entities which are not
 *                 mapped with any interfaces
 * Input(s)      : None
 * Output(s)     : pLdpEntity
 * Return(s)     : NULL or Valid pointer to LdpEntity
 -------------------------------------------------------------------------- */

tLdpEntity         *
LdpGetOtherEntityForActivation (tLdpEntity * pLdpEntity)
{
    tLdpEntity         *pTempLdpEntity = NULL;

    if (pLdpEntity->bIsIntfMapped == LDP_TRUE)
    {
        return NULL;
    }

    TMO_SLL_Scan (&LDP_ENTITY_LIST (MPLS_DEF_INCARN), pTempLdpEntity,
                  tLdpEntity *)
    {
        if (pTempLdpEntity == pLdpEntity)
        {
            continue;
        }

        if ((pTempLdpEntity->bIsIntfMapped == LDP_TRUE) ||
            (pTempLdpEntity->u1AdminStatus != LDP_ADMIN_UP) ||
            (pTempLdpEntity->u1RowStatus != ACTIVE) ||
            (pTempLdpEntity->u1OperStatus == LDP_OPER_ENABLED))
        {
            continue;
        }

        return (pTempLdpEntity);
    }

    return NULL;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpCheckIfEntityBeActivated
 * Description   : This routine scans the LDP Entity Database and checks if
 *                 the entity can be made OPER_UP
 *                 This is applicable only for those entities which are not
 *                 mapped with any interfaces
 * Input(s)      : None
 * Output(s)     : pLdpEntity
 * Return(s)     : TRUE / FALSE
 -------------------------------------------------------------------------- */
BOOL1
LdpCheckIfEntityBeActivated (tLdpEntity * pLdpEntity)
{
    tLdpEntity         *pTempLdpEntity = NULL;

    if (pLdpEntity->bIsIntfMapped == LDP_TRUE)
    {
        return TRUE;
    }

    TMO_SLL_Scan (&LDP_ENTITY_LIST (MPLS_DEF_INCARN), pTempLdpEntity,
                  tLdpEntity *)
    {
        if ((pTempLdpEntity != pLdpEntity))
        {
            continue;
        }

        if ((pTempLdpEntity->u1AdminStatus == LDP_ADMIN_UP) &&
            (pTempLdpEntity->u1RowStatus == ACTIVE) &&
            (pTempLdpEntity->u1OperStatus == LDP_OPER_ENABLED))
        {
            return FALSE;
        }

    }

    return TRUE;
}

VOID
LdpCopyAddr (uGenU4Addr * pGenU4Addr1, uGenU4Addr * pGenU4Addr2,
             UINT2 u2AddrType)
{

    switch (u2AddrType)
    {
        case LDP_ADDR_TYPE_IPV4:
            LDP_P_IPV4_U4_ADDR (pGenU4Addr1) = LDP_P_IPV4_U4_ADDR (pGenU4Addr2);
            break;
#ifdef MPLS_IPV6_WANTED
        case LDP_ADDR_TYPE_IPV6:
            MEMCPY (LDP_P_IPV6_U4_ADDR (pGenU4Addr1),
                    LDP_P_IPV6_U4_ADDR (pGenU4Addr2), LDP_IPV6ADR_LEN);
            break;
#endif
        default:
            break;

    };
}

VOID
LdpCopyAAddr (uGenU4Addr * pGenU4Addr1, uGenAddr * pGenAddr2, UINT2 u2AddrType)
{

    switch (u2AddrType)
    {
        case LDP_ADDR_TYPE_IPV4:
            MEMCPY (&LDP_P_IPV4_U4_ADDR (pGenU4Addr1),
                    LDP_P_IPV4_ADDR (pGenAddr2), LDP_IPV4ADR_LEN);
            break;
#ifdef MPLS_IPV6_WANTED
        case LDP_ADDR_TYPE_IPV6:
            MEMCPY (LDP_P_IPV6_U4_ADDR (pGenU4Addr1),
                    LDP_P_IPV6_ADDR (pGenAddr2), LDP_IPV6ADR_LEN);
            break;
#endif
        default:
            break;

    };
}

BOOL1
LdpIsAddressMatchWithAAddrType (uGenAddr * pGenAddr1, uGenU4Addr * pGenU4Addr2,
                                UINT2 u2AddrType1, UINT2 u2AddrType2)
{
    BOOL1               bRetVal = LDP_FALSE;

    if (u2AddrType1 == u2AddrType2)
    {
        bRetVal = LdpIsAAddressMatch (pGenAddr1, pGenU4Addr2, u2AddrType1);
    }

    return bRetVal;
}

BOOL1
LdpIsAAddressMatch (uGenAddr * pGenAddr1, uGenU4Addr * pGenU4Addr2,
                    UINT2 u2AddrType)
{
    switch (u2AddrType)
    {
        case LDP_ADDR_TYPE_IPV4:
            if (MEMCMP (LDP_P_IPV4_ADDR (pGenAddr1),
                        &LDP_P_IPV4_U4_ADDR (pGenU4Addr2),
                        LDP_IPV4ADR_LEN) == LDP_ZERO)
            {
                return LDP_TRUE;
            }
            break;
#ifdef MPLS_IPV6_WANTED
        case LDP_ADDR_TYPE_IPV6:
            if (MEMCMP (LDP_P_IPV6_ADDR (pGenAddr1),
                        LDP_P_IPV6_U4_ADDR (pGenU4Addr2),
                        LDP_IPV6ADR_LEN) == LDP_ZERO)
            {
                return LDP_TRUE;
            }
            break;
#endif
    };
    return LDP_FALSE;
}

BOOL1
LdpIsAddressZero (uGenU4Addr * pGenU4Addr, UINT2 u2AddrType)
{
    uGenU4Addr          TempAddr;
    MEMSET (&TempAddr, LDP_ZERO, sizeof (uGenU4Addr));
    switch (u2AddrType)
    {
        case LDP_ADDR_TYPE_IPV4:
            if (LDP_P_IPV4_U4_ADDR (pGenU4Addr) == LDP_ZERO)
            {
                return LDP_TRUE;
            }
            break;
#ifdef MPLS_IPV6_WANTED
        case LDP_ADDR_TYPE_IPV6:
            if (MEMCMP (pGenU4Addr, &TempAddr, LDP_IPV6ADR_LEN) == LDP_ZERO)
            {
                return LDP_TRUE;
            }
            break;
#endif
    };
    return LDP_FALSE;
}

BOOL1
LdpIsAddressMatchWithAddrType (uGenU4Addr * pGenU4Addr1,
                               uGenU4Addr * pGenU4Addr2, UINT2 u2AddrType1,
                               UINT2 u2AddrType2)
{
    BOOL1               bRetVal = LDP_FALSE;

    if (u2AddrType1 == u2AddrType2)
    {
        bRetVal = LdpIsAddressMatch (pGenU4Addr1, pGenU4Addr2, u2AddrType1);
    }

    return bRetVal;
}

BOOL1
LdpIsAddressMatch (uGenU4Addr * pGenU4Addr1, uGenU4Addr * pGenU4Addr2,
                   UINT2 u2AddrType)
{
    switch (u2AddrType)
    {
        case LDP_ADDR_TYPE_IPV4:
            if (LDP_P_IPV4_U4_ADDR (pGenU4Addr1) ==
                LDP_P_IPV4_U4_ADDR (pGenU4Addr2))
            {
                return LDP_TRUE;
            }
            break;
#ifdef MPLS_IPV6_WANTED
        case LDP_ADDR_TYPE_IPV6:
            if (MEMCMP (LDP_P_IPV6_U4_ADDR (pGenU4Addr1),
                        LDP_P_IPV6_U4_ADDR (pGenU4Addr2),
                        LDP_IPV6ADR_LEN) == LDP_ZERO)
            {
                return LDP_TRUE;
            }
            break;
#endif
    };
    return LDP_FALSE;
}

BOOL1
LdpCompareFec (tFec * pFec1, tFec * pFec2)
{
    if ((pFec1->u1FecElmntType == pFec2->u1FecElmntType) &&
        (pFec1->u2AddrFmly == pFec2->u2AddrFmly) &&
        (pFec1->u1PreLen == pFec2->u1PreLen))
    {
        switch (pFec1->u2AddrFmly)
        {
            case LDP_ADDR_TYPE_IPV4:
                if (pFec1->Prefix.u4Addr == pFec2->Prefix.u4Addr)
                {
                    return LDP_EQUAL;
                }
                break;
#ifdef MPLS_IPV6_WANTED
            case LDP_ADDR_TYPE_IPV6:
                if (MEMCMP
                    (&pFec1->Prefix.Ip6Addr, &pFec2->Prefix.Ip6Addr,
                     LDP_IPV6ADR_LEN) == LDP_ZERO)
                {
                    return LDP_EQUAL;
                }

                break;
#endif
        };

    }
    return LDP_NOT_EQUAL;
}

UINT4
LdpSessionGetIfIndex (tLdpSession * pSessionEntry, UINT2 u2AddrType)
{

    tLdpAdjacency      *pLdpAdj = NULL;
    tLdpAdjacency      *pTempAdj = NULL;
    UINT4               u4IfIndex = 0;

    if (TMO_SLL_Count (SSN_GET_ADJ_LIST (pSessionEntry)) != LDP_ZERO)
    {

        TMO_DYN_SLL_Scan (&(pSessionEntry->AdjacencyList),
                          pLdpAdj, pTempAdj, tLdpAdjacency *)
        {
            if (pLdpAdj->u1AddrType == u2AddrType)
            {
                u4IfIndex = pLdpAdj->u4IfIndex;
                break;
            }

        }
    }
#if 0
    printf ("%s : %d u4IfIndex=%d\n", __FUNCTION__, __LINE__, u4IfIndex);
#endif
    return u4IfIndex;
}

/* ---------------------------------------------------------------------------
 * Function Name : LdpCheckIfDnCtrlBlkExistForFec
 * Description   : This routine scans the Dn Stream LSP Cntrl Blk for give FEC
 *                 and corresponding LDP session.
 * Input(s)      : tLdpSession
 *                 uGenU4Addr
 * Return(s)     : TRUE / FALSE
 *-------------------------------------------------------------------------- */

BOOL1
LdpCheckIfDnCtrlBlkExistForFec (tLdpSession * pLdpSession, uGenU4Addr * Dest)
{
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tTMO_SLL_NODE      *pLdpSllNode = NULL;

    TMO_SLL_Scan (&SSN_DLCB (pLdpSession), pLdpSllNode, tTMO_SLL_NODE *)
    {
        pLspCtrlBlock = SLL_TO_LCB (pLdpSllNode);

        /* This function is called in All route establish scenario.
         * If Control Block  with similar prefix exist and its UpReqId is Invalid,
         * then Control blk is found(label req for that prefix already sent)
         * and donot resend Label Req */

        if ((MEMCMP (&pLspCtrlBlock->Fec.Prefix, Dest,
                     sizeof (uGenU4Addr))) == 0
            && (pLspCtrlBlock->u4UStrLblReqId == LDP_INVALID_REQ_ID))
        {
            LDP_DBG3 (LDP_ADVT_MEM,
                      "ADVT: %s: %d LCB for Fec 0x%x is already available in Downstream LCB list\n",
                      __FUNCTION__, __LINE__, pLspCtrlBlock->Fec.Prefix.u4Addr);
            return LDP_TRUE;
        }
    }
    return LDP_FALSE;
}

/*------------------------------------------------------------------------*/
/*                      End of file ldputil.c                             */
/*------------------------------------------------------------------------*/
