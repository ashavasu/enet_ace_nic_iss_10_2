/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldppw.c,v 1.29 2015/04/25 12:24:27 siva Exp $
 *
 ********************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldppw.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : LDP 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : 
 *----------------------------------------------------------------------------*/

#include "ldpincs.h"

UINT4               gau4IpIfaceList[IPIF_MAX_LOGICAL_IFACES + 1];

/*****************************************************************************/
/* Function Name : LdpFecAppInit                                             */
/* Description   : This routine creates memory pools. If any memory alloc    */
/*                 failure occurs, it does the deletion of the previoulsy    */
/*                 alloted memory pools and then returns failure.            */
/* Input(s)      : u2IncarnId                                                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpFecAppInit (UINT2 u2IncarnId)
{
    UNUSED_PARAM (u2IncarnId);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpFecAppDeInit                                           */
/* Description   : This routine deletes memory pools                         */
/* Input(s)      : u2IncarnId                                                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*****************************************************************************/
UINT1
LdpFecAppDeInit (UINT2 u2IncarnId)
{
    UNUSED_PARAM (u2IncarnId);
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessAppEvents                                       */
/* Description   : This routine processes events received from Application   */
/* Input(s)      : pAppEvtInfo - info about events and data received         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppw.c */
UINT1
LdpProcessAppEvents (tLdpAppEvtInfo * pAppEvtInfo)
{
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT1               u1RetStatus = LDP_FAILURE;

    switch (pAppEvtInfo->u4AppId)
    {
        case LDP_APP_L2VPN:
            LDP_DBG (LDP_MAIN_MISC, "MAIN: Rcvd Event from L2VPN\n");
            /*if l2vpn is registered with ldp, call proper processing
             * function else return failure */
            if (LDP_REGISTRATION_TABLE (u2IncarnId,
                                        (pAppEvtInfo->u4AppId - 1)).u1RegFlag ==
                LDP_REGISTER)
            {
                u1RetStatus = LdpProcessL2vpnEvents ((tL2VpnLdpPwVcEvtInfo *)
                                                     pAppEvtInfo->pData);
                MemReleaseMemBlock (LDP_L2VPN_EVT_INFO_POOL_ID,
                                    (UINT1 *) pAppEvtInfo->pData);
            }
            else
            {
                LDP_DBG (LDP_MAIN_MISC,
                         "MAIN: Appln not Registered for Fec 128 Or Fec 129\n");
                return LDP_FAILURE;
            }
            break;
        default:
            LDP_DBG (LDP_MAIN_MISC, "MAIN: Rcvd Invalid Appln Event\n");
            return LDP_FAILURE;
    }
    return u1RetStatus;
}

/*****************************************************************************/
/* Function Name : LdpRegisterApplication                                    */
/* Description   : Registeres the Application with LDP                       */
/* Input(s)      : u1FecId       - FEC Identifier (128 for PW-VC processing) */
/*                 pCallBackFnAppPktIn - Pointer to the function that        */
/*                                       validates the FEC-128 and sends it  */
/*                                       to L2-VPN application.              */
/*                 pCallBackFnSsnStatusChng - Pointer to the function that is*/
/*                                       used to notify the application about*/
/*                                       the session status change.          */
/* Output(s)     : u2AppId       - Application ID assigned  by LDP after     */
/*                                 successful registration for L2-VPN.       */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
/* ldppw.c */
INT4
LdpRegisterApplication (UINT1 u1FecId, UINT4 *pu4AppId)
{
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT4               u4RegIndex = LDP_ZERO;
    UINT1               u1Status = LDP_FAILURE;
    MPLS_LDP_LOCK ();
    if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Ldp Incarn is Down\n");
        MPLS_LDP_UNLOCK ();
        return LDP_FAILURE;
    }

    for (u4RegIndex = 0; u4RegIndex < MAX_LDP_APPLICATIONS; u4RegIndex++)
    {
        if ((LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u1FecType
             == u1FecId)
            && (LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u1RegFlag
                == LDP_DEREGISTER))
        {
            if ((LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).
                 pCallBackFnAppInit) != NULL)
            {
                u1Status = LDP_REGISTRATION_TABLE (u2IncarnId,
                                                   u4RegIndex).
                    pCallBackFnAppInit (u2IncarnId);
                if (u1Status == LDP_FAILURE)
                {
                    MPLS_LDP_UNLOCK ();
                    return LDP_FAILURE;
                }
            }

            LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u1RegFlag
                = LDP_REGISTER;
            *pu4AppId
                =
                (UINT4) LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u2AppId;
            LDP_DBG1 (LDP_IF_PRCS,
                      "MAIN: Successfully Registered the Fec type " "; %x \n",
                      u1FecId);
            MPLS_LDP_UNLOCK ();
            return LDP_SUCCESS;
        }
    }
    MPLS_LDP_UNLOCK ();
    LDP_DBG1 (LDP_IF_PRCS, "IF: Fail to Register Fec type ; %x \n", u1FecId);
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpDeRegisterApplication                                  */
/* Description   : DeRegisteres the Application with LDP                     */
/* Input(s)      : u4AppId       - Application Id of Application to          */
/*                                 de-register                               */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
/* ldppw.c */
INT4
LdpDeRegisterApplication (UINT4 u4AppId)
{
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT4               u4RegIndex;
    UINT1               u1Status = LDP_FAILURE;

    MPLS_LDP_LOCK ();
    if (LDP_INCARN_STATUS (u2IncarnId) != ACTIVE)
    {
        MPLS_LDP_UNLOCK ();
        LDP_DBG (LDP_MAIN_MISC, "IF: Ldp Incarn is Down\n");
        return LDP_FAILURE;
    }

    for (u4RegIndex = 0; u4RegIndex < MAX_LDP_APPLICATIONS; u4RegIndex++)
    {
        if (LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u2AppId
            == (UINT2) u4AppId)
        {
            if ((LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).
                 pCallBackFnAppDeInit) != NULL)
            {
                u1Status =
                    LDP_REGISTRATION_TABLE (u2IncarnId,
                                            u4RegIndex).
                    pCallBackFnAppDeInit (u2IncarnId);
                if (u1Status == LDP_FAILURE)
                {
                    MPLS_LDP_UNLOCK ();
                    return (LDP_FAILURE);
                }
            }

            LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u1RegFlag
                = LDP_DEREGISTER;
        }
    }

    LDP_DBG (LDP_IF_PRCS, "IF: Application De-Registered Successfully\n");
    MPLS_LDP_UNLOCK ();
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpRegStatus                                              */
/* Description   : Provides the registration status of the Fec type          */
/*                 by the supporting application.                            */
/* Input(s)      : u1FecType       - Fec type                                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_REGISTER/LDP_DEREGISTER                               */
/*****************************************************************************/
/* ldppw.c */
UINT1
LdpRegStatus (UINT1 u1FecType)
{
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT4               u4RegIndex;

    for (u4RegIndex = 0; u4RegIndex < MAX_LDP_APPLICATIONS; u4RegIndex++)
    {
        if (LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u1FecType
            == u1FecType)
        {
            return (LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u1RegFlag);
        }
    }
    return (LDP_DEREGISTER);
}

/*****************************************************************************/
/* Function Name : LdpGetSessionWithPeer                                     */
/* Description   : Gets Ldp Peer session corresponding                       */
/*                 to the given Incarnation Id,  LdpId,                      */
/*                 and Peeraddr with Label                                   */
/*                 Advertisement mode as u1LblAdvtMode. Gives a targeted     */
/*                 session if u1IsTgtType is Set to LDP_TRUE                 */
/*                                                                           */
/* Input(s)      : u4IncarnId - Incarnation Id.                              */
/*                 u4LdpEntityID - Entity identifier                         */
/*                 u4PeerAddr - Peer Ldp Address                             */
/* Output(s)     : ppLdpSession   - Ldp Session  corresponding to the given  */
/*                 Incarnation Id, Peer address.                             */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpGetSessionWithPeer (UINT4 u4IncarnId, UINT4 u4LdpEntityID,
                       uGenU4Addr *pPeerAddr,UINT2 u2AddrType,UINT1 u1LblAdvtMode,
                       UINT1 u1IsTgtType, tLdpSession ** ppLdpSession)
{
    tLdpEntity         *pLdpEntity = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLdpPeer           *pLdpPeer = NULL;

    UNUSED_PARAM (u4LdpEntityID);
    UNUSED_PARAM (u1IsTgtType);

#ifdef MPLS_IPV6_WANTED
if(u2AddrType==LDP_ADDR_TYPE_IPV6)
{
    LDP_DBG5 (LDP_MAIN_MISC, "ENTRY. u4IncarnId(%u), u4LdpEntityID(%#X), "
              "PeerAddr(%s), u1LblAdvtMode(%u), u1IsTgtType(%u)\n",
              u4IncarnId, u4LdpEntityID, Ip6PrintAddr(&pPeerAddr->Ip6Addr),
              u1LblAdvtMode, u1IsTgtType);
}
else
#endif
{
    LDP_DBG5 (LDP_MAIN_MISC, "ENTRY. u4IncarnId(%u), u4LdpEntityID(%#X), "
              "u4PeerAddr(%#X), u1LblAdvtMode(%u), u1IsTgtType(%u)\n",
              u4IncarnId, u4LdpEntityID, OSIX_NTOHL (LDP_P_IPV4_U4_ADDR(pPeerAddr)),
              u1LblAdvtMode, u1IsTgtType);

}

    /* Scan through the Peer List for peer corresponding to the Peer
     * Index. */
    TMO_SLL_Scan (&LDP_ENTITY_LIST (u4IncarnId), pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&pLdpEntity->PeerList, pLdpPeer, tLdpPeer *)
        {

		if (LdpIsAddressMatchWithAAddrType(&pLdpPeer->NetAddr.Addr,pPeerAddr,u2AddrType,
					pLdpPeer->NetAddr.u2AddrType)== LDP_TRUE)
		{
			pLdpSession = pLdpPeer->pLdpSession;
			if (pLdpSession != NULL)
			{
				if ((SSN_ADVTYPE (pLdpSession) == u1LblAdvtMode) &&
						(SSN_STATE (pLdpSession) == LDP_SSM_ST_OPERATIONAL))
				{
					*ppLdpSession = pLdpSession;
					LDP_DBG (LDP_MAIN_MISC, "EXIT 1\n");
					return LDP_SUCCESS;
				}
			}
		}
	}
    }

    LDP_DBG (LDP_MAIN_MISC, "INTMD-EXIT 2\n");
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetInLabelFrmPrefix                                    */
/* Description   : Gets In Label corresponding to the given prefix given     */
/*                                                                           */
/* Input(s)      : pL2vpnLabelArgs                                           */
/* Output(s)     : pu4InLabel, pu4TnlXcIndex, pu4InIfIndex                   */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT4
LdpGetInLabelFrmPrefix (tL2vpnLabelArgs * pL2vpnLabelArgs)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    tUstrLspCtrlBlock  *pLspUpCtrlBlock = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    UINT4               u4PwRtGw = 0;
    UINT2               u2PwRtPort = 0;
#ifdef MPLS_IPV6_WANTED
    UINT4               u4PwRtPort = 0;
#endif
    tIp6Addr           Ipv6PwRtGw;
    tIp6Addr           Ipv6LdpPeerAddr;
    tIp6Addr           Ipv6LdpRtGw;
    BOOL1              bPortFound=LDP_FALSE;
    BOOL1              bIsEntityFound = LDP_FALSE;
    tLdpAdjacency      *pLdpAdj = NULL;
    tLdpAdjacency      *pTempAdj = NULL;



    MEMSET(&Ipv6PwRtGw,LDP_ZERO,sizeof(tIp6Addr));
    MEMSET(&Ipv6LdpPeerAddr,LDP_ZERO,sizeof(tIp6Addr));
    MEMSET(&Ipv6LdpRtGw,LDP_ZERO,sizeof(tIp6Addr));

    pList = &LDP_ENTITY_LIST (LDP_CUR_INCARN);

    LDP_DBG2 (LDP_DBG_PRCS, "pL2vpnLabelArgs->Prefix = %x, pL2vpnLabelArgs->u2AddrType = %u\n", 
                            pL2vpnLabelArgs->Prefix, pL2vpnLabelArgs->u2AddrType);
    TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
    {
        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
        {
            if ((LDP_ENTITY_LDP_OVER_RSVP_FLAG (pLdpEntity) == LDP_SNMP_FALSE)
                || (pLdpEntity->InStackTnlInfo.u4TnlId == LDP_ZERO)
                || (pLdpEntity->OutStackTnlInfo.u4TnlId == LDP_ZERO))
            {
                continue;
            }
        }
        
        bIsEntityFound = LDP_TRUE;
        LDP_DBG2 (LDP_SSN_PRCS,
                  "\rEntity id %d Prefered for IlmHwAddForNonTePsn\n"
                  "\rEntity id %d choosed for IlmHwAddForNonTePsn\n",
                  pL2vpnLabelArgs->u4EntityId, pLdpEntity->u4EntityIndex);
         
        if ((pL2vpnLabelArgs->u4EntityId != 0)
            && (pLdpEntity->u4EntityIndex != pL2vpnLabelArgs->u4EntityId))
        {
            continue;
        }
        
        TMO_SLL_Scan (&((pLdpEntity)->PeerList), pLdpPeer, tLdpPeer *)
        {
            pLdpSession = pLdpPeer->pLdpSession;
            if (pLdpSession == NULL)
            {
                continue;
            }
            if (SSN_STATE (pLdpSession) != LDP_SSM_ST_OPERATIONAL)
            {
                continue;
            }
            /* Incase of Unsolicited mode, search the Upstream control block, 
             * otherwise search in the Downstream control block */
            if (SSN_ADVTYPE (pLdpSession) == LDP_DSTR_UNSOLICIT)
            {
                TMO_SLL_Scan (&SSN_ULCB (pLdpSession), pSllNode,
                              tTMO_SLL_NODE *)
                {
                    pLspUpCtrlBlock = (tUstrLspCtrlBlock *) (pSllNode);

#ifdef MPLS_IPV6_WANTED
		    if(pLspUpCtrlBlock->Fec.u2AddrFmly==LDP_ADDR_TYPE_IPV6)
		    {
			    LDP_DBG4 (LDP_SSN_PRCS,
					    "\rUpStr FEC %s Label: %d Intf: %d for the "
					    "Peer %x is Checked\n",
					    Ip6PrintAddr(&pLspUpCtrlBlock->Fec.Prefix.Ip6Addr),
					    pLspUpCtrlBlock->UStrLabel.u4GenLbl,
					    pLspUpCtrlBlock->u4InComIfIndex,
					    OSIX_NTOHL (*(UINT4 *) (VOID *)
						    &(pLdpPeer->NetAddr)));
		    }
		    else
#endif
		    {
			    LDP_DBG4 (LDP_SSN_PRCS,
					    "\rUpStr FEC %x Label: %d Intf: %d for the "
					    "Peer %x is Checked\n",
					    LDP_IPV4_U4_ADDR(pLspUpCtrlBlock->Fec.Prefix),
					    pLspUpCtrlBlock->UStrLabel.u4GenLbl,
					    pLspUpCtrlBlock->u4InComIfIndex,
					    OSIX_NTOHL (*(UINT4 *) (VOID *)
						    &(pLdpPeer->NetAddr)));
		    }

		    if ((LdpIsAddressMatchWithAddrType(&pLspUpCtrlBlock->Fec.Prefix,
						    &pL2vpnLabelArgs->Prefix,
						    pLspUpCtrlBlock->Fec.u2AddrFmly,
						    pL2vpnLabelArgs->u2AddrType))!=LDP_TRUE)
		    {
#if 0
			    printf("%s : %d\n",__FUNCTION__,__LINE__);
                
#endif
                LDP_DBG6 (LDP_DBG_PRCS, "%s, %d: Skipping on Address mismatch: %x <> %x "
                          "Addr Type: %u <> %u\n", __func__, __LINE__, pLspUpCtrlBlock->Fec.Prefix, pL2vpnLabelArgs->Prefix,
                           pLspUpCtrlBlock->Fec.u2AddrFmly, pL2vpnLabelArgs->u2AddrType);

			    continue;
		    }
#if 0
		    printf("%s : %d\n",__FUNCTION__,__LINE__);
#endif
		    LDP_DBG4 (LDP_SSN_PRCS, "LSP Fec=%s Prefix=%s Fec Addr Type=%d LSP Addr Type=%d\n",
				    Ip6PrintAddr(&pLspUpCtrlBlock->Fec.Prefix.Ip6Addr),
				    Ip6PrintAddr(&pL2vpnLabelArgs->Prefix.Ip6Addr),pLspUpCtrlBlock->Fec.u2AddrFmly,
				    pL2vpnLabelArgs->u2AddrType);
#ifdef MPLS_IPV6_WANTED
		    if(pL2vpnLabelArgs->u2AddrType==LDP_ADDR_TYPE_IPV6)
		    {
			    MEMSET(&Ipv6PwRtGw,LDP_ZERO,LDP_IPV6ADR_LEN);
			    MEMSET(&Ipv6LdpPeerAddr,LDP_ZERO,LDP_IPV6ADR_LEN);
			    MEMSET(&Ipv6LdpRtGw,LDP_ZERO,LDP_IPV6ADR_LEN);

			    if (LdpIpUcastIpv6RouteQuery
					    (&pL2vpnLabelArgs->PeerAddr.Ip6Addr, &u4PwRtPort, &Ipv6PwRtGw)
					    != LDP_SUCCESS)
			    {
#if 0
				    printf("%s : %d\n",__FUNCTION__,__LINE__);
#endif
				    continue;
			    }

			    if(TMO_SLL_Count(SSN_GET_ADJ_LIST(pLdpSession)) != LDP_ZERO)
			    {

				    TMO_DYN_SLL_Scan (&(pLdpSession->AdjacencyList),
						    pLdpAdj, pTempAdj, tLdpAdjacency *)
				    {
					    if(pLdpAdj->u4IfIndex==u4PwRtPort)
					    {
                                                    bPortFound=TRUE;
			                            LDP_DBG3 (LDP_SSN_PRCS, "IPv6 PW target IP:%s, NextHop:%s, In-Intf:%d\n",
                                                             Ip6PrintAddr(&pL2vpnLabelArgs->PeerAddr.Ip6Addr),
                                                             Ip6PrintAddr(&Ipv6PwRtGw),u4PwRtPort);

						    break;
					    }

				    }
			    }
			    if(bPortFound==LDP_FALSE)
			    {
				    continue;
			    }
		    }
		    else
#endif
		    {
			    if (LdpIpUcastRouteQuery
					    (LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->PeerAddr), &u2PwRtPort, &u4PwRtGw)
					    != LDP_IP_SUCCESS)
			    {
                    LDP_DBG1 (LDP_SSN_PRCS, "Route Not found for the prefix: %x\n", LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->PeerAddr));
				    continue;
			    }

			    if(TMO_SLL_Count(SSN_GET_ADJ_LIST(pLdpSession)) != LDP_ZERO)
			    {

				    TMO_DYN_SLL_Scan (&(pLdpSession->AdjacencyList),
						    pLdpAdj, pTempAdj, tLdpAdjacency *)
				    {
                        if(pLdpAdj->u4IfIndex==u2PwRtPort)
                        {
                            bPortFound=TRUE;
                            LDP_DBG3 (LDP_SSN_PRCS, "IPv4 PW target IP:%s, NextHop:%d, In-Intf:%d\n",
                                    Ip6PrintAddr(&pL2vpnLabelArgs->PeerAddr.Ip6Addr),
                                    u4PwRtGw, u2PwRtPort);
                            break;
					    }

				    }
			    }
			    if(bPortFound==LDP_FALSE)
			    {
                    LDP_DBG (LDP_SSN_PRCS, "Port Found is FALSE\n");
				    continue;
			    }

		    }

		    /*pL2vpnLabelArgs->u4InLabel =
			    pLspUpCtrlBlock->UStrLabel.u4GenLbl;*/
		    pL2vpnLabelArgs->u4InIfIndex =
			    pLspUpCtrlBlock->u4InComIfIndex;
		    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
		    {
			    pL2vpnLabelArgs->bIsLspOnTgtSsn = TRUE;
		    }

		    if ((LDP_ENTITY_LDP_OVER_RSVP_FLAG (pLdpEntity) ==
					    LDP_SNMP_TRUE)
				    && (pLdpEntity->InStackTnlInfo.u4TnlId != LDP_ZERO))
		    {
			    MPLS_CMN_LOCK ();

			    if (TeGetTunnelInfoByPrimaryTnlInst
					    (pLdpEntity->InStackTnlInfo.u4TnlId,
					     pLdpEntity->InStackTnlInfo.u4TnlInstance,
					     pLdpEntity->InStackTnlInfo.u4IngressId,
					     pLdpEntity->InStackTnlInfo.u4EgressId,
					     &pTeTnlInfo) == TE_FAILURE)
			    {
				    LDP_DBG5 (LDP_SSN_PRCS,
						    "\n SESSION: In tunnel %d %d %x %x is not up for "
						    "handling remote peers for the entity %d\n",
						    pLdpEntity->InStackTnlInfo.u4TnlId,
						    pLdpEntity->InStackTnlInfo.u4TnlInstance,
						    pLdpEntity->InStackTnlInfo.u4IngressId,
						    pLdpEntity->InStackTnlInfo.u4EgressId,
						    pLdpEntity->u4EntityIndex);
				    MPLS_CMN_UNLOCK ();
#if 0
				    printf("%s : %d\n",__FUNCTION__,__LINE__);
#endif
				    return LDP_FAILURE;
			    }
			    if (TE_TNL_OPER_STATUS (pTeTnlInfo) != TE_OPER_UP)
			    {
				    MPLS_CMN_UNLOCK ();
#if 0
				    printf("%s : %d\n",__FUNCTION__,__LINE__);
#endif
				    continue;
			    }
			    pL2vpnLabelArgs->u4TnlXcIndex =
				    pTeTnlInfo->u4TnlXcIndex;
			    pL2vpnLabelArgs->u4BkpTnlXcIndex1 =
				    pTeTnlInfo->u4BkpTnlMPXcIndex1;
			    pL2vpnLabelArgs->u4BkpTnlXcIndex2 =
				    pTeTnlInfo->u4BkpTnlMPXcIndex2;
			    MPLS_CMN_UNLOCK ();
		    }
#if 0
		    printf("%s : %d\n",__FUNCTION__,__LINE__);
#endif
		    return LDP_SUCCESS;
		}
	    }
	    else
	    {
		    /* Dod and No_MRG case handling */
		    TMO_SLL_Scan (&(pLdpSession->USLspCtrlBlkList),
				    pLspCtrlBlock, tLspCtrlBlock *)
		    {

			    if ((pLspCtrlBlock != NULL) &&
					    (LdpIsAddressMatchWithAddrType(&pLspCtrlBlock->Fec.Prefix,
									   &pL2vpnLabelArgs->Prefix, 
									   pLspCtrlBlock->Fec.u2AddrFmly,
									   pL2vpnLabelArgs->u2AddrType)==LDP_TRUE))
			    {
				    /* pL2vpnLabelArgs->u4InLabel =
					    pLspCtrlBlock->UStrLabel.u4GenLbl; */
				    pL2vpnLabelArgs->u4InIfIndex =
					    pLspCtrlBlock->u4InComIfIndex;
				    return LDP_SUCCESS;
			    }
		    }
	    }
	}
    }
    /* For the case of Pseudo wires over static LSPS with LDP OVER RSVP Flag DISABLED 
     * this api will always return Failure due to which futher processing of ILM HW ADDITION 
     * is skipped and hence pseudo wire does not come up. To handle this scenario added this check */
    if(!bIsEntityFound)
    {
        LDP_DBG(LDP_SSN_PRCS,
                   "\nLdpGetInLabelFrmPrefix: This may be the case of Static Lsps, Returning Success\n");
        return LDP_SUCCESS;
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpRegOrDeRegNonTeLspWithL2Vpn                            */
/* Description   : Registers the NON-TE outer tunnel LSP with L2VPN          */
/*                 This function helps in ensuring NON-TE tunnel.            */
/*                 outgoing interface is deleted after the L2VPN deletion    */
/*                                                                           */
/* Input(s)      : u4Prefix - NON-TE LSP                                     */
/*                 u4OutIfIndex - MPLS tnl interface                         */
/*                 bIsRegFlag - TRUE => Registration,                        */
/*                              FALSE => Deregistration                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT4
LdpRegOrDeRegNonTeLspWithL2Vpn (tGenU4Addr *pPrefix, UINT4 u4OutIfIndex,
                                BOOL1 bIsRegFlag)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    tLspCtrlBlock      *pLspDnCtrlBlock = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;

    if(gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus
            == LDP_GR_SHUT_DOWN_IN_PROGRESS)
    {
        return LDP_SUCCESS;		
    }
    pList = &LDP_ENTITY_LIST (LDP_CUR_INCARN);
    MPLS_LDP_LOCK ();
    TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&((pLdpEntity)->PeerList), pLdpPeer, tLdpPeer *)
        {
            pLdpSession = pLdpPeer->pLdpSession;
            if (pLdpSession == NULL)
            {
                continue;
            }
#ifdef LDP_GR_WANTED
            if ( LDP_FALSE == pLdpSession->u1StaleStatus )
            {
                if (SSN_STATE (pLdpSession) != LDP_SSM_ST_OPERATIONAL)
                {
                    continue;
                }
            }
#else
            if (SSN_STATE (pLdpSession) != LDP_SSM_ST_OPERATIONAL)
            {
                continue;
            }
#endif

            TMO_SLL_Scan (&SSN_DLCB (pLdpSession), pSllNode, tTMO_SLL_NODE *)
            {
                pLspDnCtrlBlock = SLL_TO_LCB (pSllNode);
                if(LdpIsAddressMatchWithAddrType(&LCB_FEC (pLspDnCtrlBlock).Prefix,&pPrefix->Addr,
                            LCB_FEC (pLspDnCtrlBlock).u2AddrFmly,
                            pPrefix->u2AddrType)!=LDP_TRUE)
                {
                    continue;
                }
                if (u4OutIfIndex != pLspDnCtrlBlock->u4OutIfIndex)
                {
                    continue;
                }
                if (bIsRegFlag)
                {
#ifdef MPLS_IPV6_WANTED
                    if(pPrefix->u2AddrType==LDP_ADDR_TYPE_IPV6)
                    {
                        LDP_DBG1 (LDP_IF_PRCS, "\n LdpRegOrDeRegNonTeLspWithL2Vpn: "
                                "Registration done for FEC=%s\n",Ip6PrintAddr(&pPrefix->Addr.Ip6Addr));
                    }
                    else
#endif
                    {

                        LDP_DBG1 (LDP_IF_PRCS, "\n LdpRegOrDeRegNonTeLspWithL2Vpn: "
                                "Registration done for FEC=%x\n", LDP_IPV4_U4_ADDR(pPrefix->Addr));

                    }
                    pLspDnCtrlBlock->u1IsLspUsedByL2VPN = TRUE;
                }
                else
                {
#ifdef MPLS_IPV6_WANTED
                    if(pPrefix->u2AddrType==LDP_ADDR_TYPE_IPV6)
                    {

                        LDP_DBG1 (LDP_IF_PRCS, "\n LdpRegOrDeRegNonTeLspWithL2Vpn: "
                                "Deregistration done for FEC=%s\n", Ip6PrintAddr(&pPrefix->Addr.Ip6Addr));
                    }
                    else
#endif
                    {

                        LDP_DBG1 (LDP_IF_PRCS, "\n LdpRegOrDeRegNonTeLspWithL2Vpn: "
                                "Deregistration done for FEC=%x\n", LDP_IPV4_U4_ADDR(pPrefix->Addr));
                    }
                    pLspDnCtrlBlock->u1IsLspUsedByL2VPN = FALSE;
                }
                MPLS_LDP_UNLOCK ();
                return LDP_SUCCESS;
            }
        }
    }
    MPLS_LDP_UNLOCK ();

    /* Explicitly returning success to handle case of static PW */
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetSrcTransAddrFromDestAddr                            */
/* Description   : Gets Ldp Entity address from the peer address.            */
/*                                                                           */
/* Input(s)      : u4DestTransAddr - Peer address,                           */
/*                 bIsManualPwVc - It indicates the Pwvc manual or not       */
/* Output(s)     : pu4SrcTransAddr                                           */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT4
LdpGetSrcTransAddrFromDestAddr (tGenU4Addr *pDestTransAddr,tGenU4Addr *pSrcTransAddr,
                                BOOL1 bIsManualPwVc)
{
	tTMO_SLL           *pList = NULL;
	tLdpEntity         *pLdpEntity = NULL;
	tLdpPeer           *pLdpPeer = NULL;
	/** vishal_1 : Remove hard coding */
#ifdef MPLS_IPV6_WANTED
	UINT4 u4ContextId=0;
#endif

    pList = &LDP_ENTITY_LIST (LDP_CUR_INCARN);
#if 0
	if(pList == NULL)
	{
		return LDP_FAILURE;
	}
#endif
    MPLS_LDP_LOCK ();
    TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
    {
        TMO_SLL_Scan (&((pLdpEntity)->PeerList), pLdpPeer, tLdpPeer *)
        {
            if (!bIsManualPwVc)
            {
            /* TODO For L2VPN manual NON-TE PW to work, 
             * we should have only one LDP targeted entity in the router. 
             * This issue needs to be solved in the future releases. 
             * This issue arised when we test the 
             * L2VPN on LDP over RSVP scenario.
             VLAN 4             VLAN 2             VLAN 3           VLAN 4
             PC1------ Switch A --------- Switch B ---------- Switch C ----------PC2
             <== Tunnel RSVP ==>
             <====== LDP ======>    <====== LDP ======>
             <================ L2VPN =================>
             Here, we have to select the targeted entity of Switch A 
             which goes to reach the L2VPN peer at Switch C. 
             As long as we have a single LDP targeted entity, no issue
             but if we have more than one targeted entity then there is an issue.
             */

				if (LdpIsAddressMatchWithAAddrType(&pLdpPeer->NetAddr.Addr,
							&pDestTransAddr->Addr,
							pLdpPeer->NetAddr.u2AddrType,
							pDestTransAddr->u2AddrType) != LDP_TRUE)
				{
					continue;
				}
			}
			/* TODO For L2VPN manual NON-TE PW to work, 
			 * we should have only one LDP targeted entity in the router. 
			 * This issue needs to be solved in the future releases. 
			 * This issue arised when we test the 
			 * L2VPN on LDP over RSVP scenario.
			 VLAN 4             VLAN 2             VLAN 3           VLAN 4
			 PC1------ Switch A --------- Switch B ---------- Switch C ----------PC2
			 <== Tunnel RSVP ==>
			 <====== LDP ======>    <====== LDP ======>
			 <================ L2VPN =================>
			 Here, we have to select the targeted entity of Switch A 
			 which goes to reach the L2VPN peer at Switch C. 
			 As long as we have a single LDP targeted entity, no issue
			 but if we have more than one targeted entity then there is an issue.
			 */

			/* For loopback transport address kind, the entity's transport 
			 * address would be stored in Entity itself. Otherwise, we need
			 * to get the source address from the NETIP */
#ifdef MPLS_IPV6_WANTED
			if(pDestTransAddr->u2AddrType==LDP_ADDR_TYPE_IPV6)
			{
				if ((LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) ==
							LOOPBACK_ADDRESS_TYPE)
						&& (LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) == LDP_TRUE))
				{
					MEMCPY(&pSrcTransAddr->Addr.Ip6Addr,&pLdpEntity->Ipv6TransportAddress,
							LDP_IPV6ADR_LEN);
					pSrcTransAddr->u2AddrType=LDP_ADDR_TYPE_IPV6;
				}
				else
				{

					if(NetIpv6GetSrcAddrForDestAddr(u4ContextId,
								&pDestTransAddr->Addr.Ip6Addr,
								&pSrcTransAddr->Addr.Ip6Addr)!=IP6_SUCCESS)
					{
						MPLS_LDP_UNLOCK ();
						return LDP_FAILURE;
					}
					pSrcTransAddr->u2AddrType=LDP_ADDR_TYPE_IPV6;
				}
			}
			else
#endif
			{
				if ((LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) ==
							LOOPBACK_ADDRESS_TYPE)
						&& (LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) == LDP_TRUE))
				{
					LDP_IPV4_U4_ADDR(pSrcTransAddr->Addr) = pLdpEntity->u4TransportAddress;
					pSrcTransAddr->u2AddrType=LDP_ADDR_TYPE_IPV4;

				}
				else
				{
					if (NetIpv4GetSrcAddrToUseForDest (OSIX_NTOHL (LDP_IPV4_U4_ADDR(pDestTransAddr->Addr)),
								&LDP_IPV4_U4_ADDR(pSrcTransAddr->Addr))
							== NETIPV4_FAILURE)
					{
						MPLS_LDP_UNLOCK ();
						return LDP_FAILURE;
					}
					pSrcTransAddr->u2AddrType=LDP_ADDR_TYPE_IPV4;
				}
			}
			MPLS_LDP_UNLOCK ();
			return LDP_SUCCESS;
		}
	}
	MPLS_LDP_UNLOCK ();
        /* Explicitly returning success to handle case of static PW */
	return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpIsLdpInitialised                                       */
/* Description   : This Function returns whether LDP is intialised or not    */
/*                                                                           */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_TRUE/LDP_FALSE                                        */
/*****************************************************************************/
INT1
LdpIsLdpInitialised (VOID)
{
    if (LDP_INITIALISED == TRUE)
    {
        return LDP_SUCCESS;
    }

    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetFecInLabelFromLoopbackIntf                          */
/* Description   : Gets FEC in-label for the loopback which has LSP          */
/*                 configured on it.                                         */
/*                                                                           */
/* Input(s)      : uIsSrcAddMatchLdpIpFound - It indiacted if the fetched    */
/*                              loopback address matches the LDP ID or not   */
/*                 bIsManualPwVc - It indicates the Pwvc manual or not       */
/*                 u4LdpIp - LDP id of the LDP session configured in case of */
/*                           dynamic LSP                                     */
/*                 pL2vpnLabelArgs - contains source transport address prefix*/
/* Output(s)     : pu4SrcTransAddr                                           */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
INT1
LdpGetFecInLabelFromLoopbackIntf (BOOL1 bIsManualPwVc,
                                  tL2vpnLabelArgs * pL2vpnLabelArgs,
                                  UINT4 u4LdpIp)
{
    UINT2               u2Var = 0;
    UINT1               i4IfaceType = 0;
    uGenU4Addr          Prefix;
    UINT4               u4InLabel = 0;
    UINT4               u4InIfIndex = 0;
    tIpConfigInfo       IpIfInfo;

#ifdef MPLS_IPV6_WANTED
    uGenU4Addr TempDestAddr;
    uGenU4Addr TempMask;
    uGenU4Addr TempPrefix;
    tNetIpv6AddrInfo GlobalUniqAddr;
    tIp6Addr GlobalUniqMask;

    MEMSET(&TempDestAddr,LDP_ZERO,sizeof(uGenU4Addr));
    MEMSET(&TempMask,LDP_ZERO,sizeof(uGenU4Addr));
    MEMSET(&TempPrefix,LDP_ZERO,sizeof(uGenU4Addr));
    MEMSET(&GlobalUniqAddr,LDP_ZERO,sizeof(tNetIpv6AddrInfo));
    MEMSET(&GlobalUniqMask,LDP_ZERO,sizeof(tIp6Addr));
#endif

    MEMSET(&Prefix,LDP_ZERO,sizeof(uGenU4Addr));
    MEMSET (gau4IpIfaceList, LDP_ZERO, IPIF_MAX_LOGICAL_IFACES + 1);
    if (VcmGetCxtIpIfaceList (VCM_DEFAULT_CONTEXT, gau4IpIfaceList) ==
        VCM_FAILURE)
    {
        LDP_DBG1 (LDP_IF_PRCS,
                  "Failed to get the list of interface mapped to %u\n",
                  VCM_DEFAULT_CONTEXT);
    }
    if (gau4IpIfaceList[0] == 0)
    {
        return LDP_FAILURE;
    }

    for (u2Var = 1; ((u2Var <= gau4IpIfaceList[0]) &&
                     (u2Var < IPIF_MAX_LOGICAL_IFACES + 1)); u2Var++)
    {
	    if (CfaGetIfType ((UINT4) gau4IpIfaceList[u2Var], &i4IfaceType) ==
			    CFA_SUCCESS)
	    {
		    if (i4IfaceType == CFA_LOOPBACK)
		    {
#ifdef MPLS_IPV6_WANTED
			    if(pL2vpnLabelArgs->u2AddrType==LDP_ADDR_TYPE_IPV6)
			    {
				    if(LdpGetIpv6IfGlbUniqAddr((UINT4) gau4IpIfaceList[u2Var],&GlobalUniqAddr)
						    ==LDP_SUCCESS)
				    {
						    MplsGetIPV6Subnetmask((UINT1)GlobalUniqAddr.u4PrefixLength,(UINT1*)&GlobalUniqMask);
                                                    MEMCPY(&TempDestAddr.Ip6Addr,&GlobalUniqAddr.Ip6Addr,LDP_IPV6ADR_LEN);
                                                    MEMCPY(&TempMask.Ip6Addr,&GlobalUniqMask,LDP_IPV6ADR_LEN);
						    MplsGetPrefix(&TempDestAddr,&TempMask,MPLS_IPV6_ADDR_TYPE,&TempPrefix);

						    MEMCPY(&pL2vpnLabelArgs->Prefix.Ip6Addr,&TempPrefix.Ip6Addr,LDP_IPV6ADR_LEN);
						    pL2vpnLabelArgs->u2AddrType=LDP_ADDR_TYPE_IPV6;
						    if (LdpGetInLabelFrmPrefix (pL2vpnLabelArgs) != LDP_SUCCESS)
						    {
							    LDP_DBG (LDP_IF_PRCS,
									    "LdpGetFecInLabelFromLoopbackIntf: "
									    "LdpGetInLabelFrmPrefix failed. Label not found on loopback.\n");
							    continue;
						    }
						    else
						    {
                                if (bIsManualPwVc == TRUE)
                                {
                                    LDP_DBG (LDP_IF_PRCS,
                                            "LdpGetFecInLabelFromLoopbackIntf: "
                                            "Loopback address matches LDP ID/PW is manual. Success\n");
                                    return LDP_SUCCESS;
                                }
 
							    MEMCPY(&Prefix.Ip6Addr,&pL2vpnLabelArgs->Prefix.Ip6Addr,LDP_IPV6ADR_LEN);
							    u4InLabel = pL2vpnLabelArgs->u4InLabel;
							    u4InIfIndex = pL2vpnLabelArgs->u4InIfIndex;
						    }
				    }
			    }
			    else
#endif
			    {

				    if (CfaIpIfGetIfInfo ((UINT4) gau4IpIfaceList[u2Var], &IpIfInfo)
						    == CFA_SUCCESS)
				    {
                        if (IpIfInfo.u4Addr != 0)
                        {
                            LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->Prefix) = IpIfInfo.u4Addr;
                            LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->Prefix) &= IpIfInfo.u4NetMask;
                            pL2vpnLabelArgs->u2AddrType=LDP_ADDR_TYPE_IPV4;

                            LDP_DBG2 (LDP_DBG_PRCS, "Loopback IP: %x, Netmask: %x\n", IpIfInfo.u4Addr, IpIfInfo.u4NetMask);
                            if (LdpGetInLabelFrmPrefix (pL2vpnLabelArgs) != LDP_SUCCESS)
                            {
                                LDP_DBG (LDP_IF_PRCS,
                                        "LdpGetFecInLabelFromLoopbackIntf: "
                                        "LdpGetInLabelFrmPrefix failed. Label not found on loopback.\n");
                                continue;
                            }
                            else
                            {
                                LDP_DBG (LDP_IF_PRCS,
                                        "LdpGetFecInLabelFromLoopbackIntf: "
                                        "LdpGetInLabelFrmPrefix success\n");
                                if ((bIsManualPwVc == TRUE)
                                        || (LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->Prefix) == u4LdpIp))
                                {
                                    LDP_DBG (LDP_IF_PRCS,
                                            "LdpGetFecInLabelFromLoopbackIntf: "
                                            "Loopback address matches LDP ID/PW is manual. Success\n");
                                    return LDP_SUCCESS;
                                }
                                LDP_IPV4_U4_ADDR(Prefix) = LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->Prefix);
                                u4InLabel = pL2vpnLabelArgs->u4InLabel;
                                u4InIfIndex = pL2vpnLabelArgs->u4InIfIndex;
                            }
                        }
				    }
			    }
		    }
	    }
    }
    if (u4InIfIndex != 0)
    {
#ifdef MPLS_IPV6_WANTED
	    if(pL2vpnLabelArgs->u2AddrType==LDP_ADDR_TYPE_IPV6)
	    {
		    MEMCPY(&pL2vpnLabelArgs->Prefix.Ip6Addr,&Prefix.Ip6Addr,LDP_IPV6ADR_LEN);
                    pL2vpnLabelArgs->u4InLabel = u4InLabel;
                    pL2vpnLabelArgs->u4InIfIndex = u4InIfIndex;
                    pL2vpnLabelArgs->u2AddrType=LDP_ADDR_TYPE_IPV6;
	    }
	    else
#endif
	    {
		    LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->Prefix) = LDP_IPV4_U4_ADDR(Prefix);
		    pL2vpnLabelArgs->u4InLabel = u4InLabel;
		    pL2vpnLabelArgs->u4InIfIndex = u4InIfIndex;
		    pL2vpnLabelArgs->u2AddrType=LDP_ADDR_TYPE_IPV4;
	    }
	    LDP_DBG (LDP_IF_PRCS,
			    "LdpGetFecInLabelFromLoopbackIntf: "
			    "Returning loopback IP with LSP label.\n");
	    return LDP_SUCCESS;
    }
    /*If no loopback exists in the system, use interface IP to look for in-LSP label */
#ifdef MPLS_IPV6_WANTED
    if(pL2vpnLabelArgs->u2AddrType==LDP_ADDR_TYPE_IPV6)
    {
	    if(NetIpv6GetSrcAddrForDestAddr(0,&pL2vpnLabelArgs->PeerAddr.Ip6Addr,
				    &pL2vpnLabelArgs->Prefix.Ip6Addr)==IP6_SUCCESS)
	    {
		    if (LdpGetInLabelFrmPrefix (pL2vpnLabelArgs) == LDP_SUCCESS)
		    {
			    LDP_DBG (LDP_IF_PRCS,
					    "LdpGetFecInLabelFromLoopbackIntf: "
					    "LdpGetInLabelFrmPrefix found label for interface IP. Success\n");
			    return LDP_SUCCESS;
		    }
		    LDP_DBG (LDP_IF_PRCS,
				    "LdpGetFecInLabelFromLoopbackIntf: "
				    "LdpGetInLabelFrmPrefix: No label found for interface IP. Failure\n");
	    }
    }
    else
#endif
    {
	    if (NetIpv4GetSrcAddrToUseForDest (LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->PeerAddr),
				    &LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->Prefix))
			    == NETIPV4_SUCCESS)
	    {
		    if (LdpGetInLabelFrmPrefix (pL2vpnLabelArgs) == LDP_SUCCESS)
		    {
			    LDP_DBG (LDP_IF_PRCS,
					    "LdpGetFecInLabelFromLoopbackIntf: "
					    "LdpGetInLabelFrmPrefix found label for interface IP. Success\n");
			    return LDP_SUCCESS;
		    }
		    LDP_DBG (LDP_IF_PRCS,
				    "LdpGetFecInLabelFromLoopbackIntf: "
				    "LdpGetInLabelFrmPrefix: No label found for interface IP. Failure\n");
	    }
    }
    LDP_DBG (LDP_IF_PRCS, "LdpGetFecInLabelFromLoopbackIntf: " "Failed\n");
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpGetFecInLabelForDestAddr                               */
/* Description   : Gets FEC in-label for destination address                 */
/*                                                                           */
/* Input(s)      : bIsManualPwVc - It indicates the Pwvc manual or not       */
/*                 u4LdpIp - LDP id of the LDP session configured in case of */
/*                           dynamic LSP                                     */
/*                 pL2vpnLabelArgs - contains source transport address prefix*/
/* Output(s)     : pu4SrcTransAddr                                           */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
INT1
LdpGetFecInLabelForDestAddr (tL2vpnLabelArgs * pL2vpnLabelArgs,
                             BOOL1 bIsManualPwVc)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT4               u4LdpIp = 0;
    UINT4               u4DestTransAddr = 0;
    INT1                i1Ret = 0;
    UINT4               IfIndex = 0;
    tIpConfigInfo       IpIfInfo;

#ifdef MPLS_IPV6_WANTED
    uGenU4Addr TempDestAddr;
    uGenU4Addr TempMask;
    uGenU4Addr TempPrefix;
    tNetIpv6AddrInfo GlobalUniqAddr;
    tIp6Addr GlobalUniqMask;

    MEMSET(&TempDestAddr,LDP_ZERO,sizeof(uGenU4Addr));
    MEMSET(&TempMask,LDP_ZERO,sizeof(uGenU4Addr));
    MEMSET(&TempPrefix,LDP_ZERO,sizeof(uGenU4Addr));
    MEMSET(&GlobalUniqAddr,LDP_ZERO,sizeof(tNetIpv6AddrInfo));
    MEMSET(&GlobalUniqMask,LDP_ZERO,sizeof(tIp6Addr));
#endif

    MPLS_LDP_LOCK ();
    if (!bIsManualPwVc)
    {                            /* For signalled PW, scan through the entities and fetch the in label for the entity with required destination address. */
        pList = &LDP_ENTITY_LIST (LDP_CUR_INCARN);
        TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
        {
            TMO_SLL_Scan (&((pLdpEntity)->PeerList), pLdpPeer, tLdpPeer *)
	    {
#ifdef MPLS_IPV6_WANTED
		    if(pL2vpnLabelArgs->u2AddrType==LDP_ADDR_TYPE_IPV6)
		    {
			    if (MEMCMP(&pLdpPeer->NetAddr.Addr.Ip6Addr,
						    &pL2vpnLabelArgs->PeerAddr.Ip6Addr,LDP_IPV6ADR_LEN) != 0)
			    {
				    continue;
			    }
		    }
		    else
#endif
		    {
			    u4DestTransAddr = OSIX_HTONL (LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->PeerAddr));
			    if (MEMCMP ((UINT1 *) &LDP_IPV4_ADDR(pLdpPeer->NetAddr.Addr),
						    (UINT1 *) &u4DestTransAddr, LDP_NET_ADDR_LEN) != 0)
			    {
				    continue;
			    }
		    }
#ifdef MPLS_IPV6_WANTED
		    if(pL2vpnLabelArgs->u2AddrType==LDP_ADDR_TYPE_IPV6)
		    {
			    if ((LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) ==
						    LOOPBACK_ADDRESS_TYPE)
					    && (LDP_ENTITY_TRANSPORT_ADDRESS_IPV6_TLV (pLdpEntity) ==
						    LDP_TRUE))
			    {
				    MEMCPY(&pL2vpnLabelArgs->Prefix.Ip6Addr,&pLdpEntity->Ipv6TransportAddress,
						    LDP_IPV6ADR_LEN);
				    if(NetIpv6IsOurAddress(&pL2vpnLabelArgs->Prefix.Ip6Addr,&IfIndex)!= NETIPV6_SUCCESS)
				    {
					    MPLS_LDP_UNLOCK();
					    return LDP_FAILURE;
				    }

				    if(LdpGetIpv6IfGlbUniqAddr(IfIndex,&GlobalUniqAddr)!=LDP_SUCCESS)
				    {
					    MPLS_LDP_UNLOCK();
					    return LDP_FAILURE;
				    }
				    MplsGetIPV6Subnetmask((UINT1)GlobalUniqAddr.u4PrefixLength,(UINT1*)&GlobalUniqMask);
				    MEMCPY(&TempDestAddr.Ip6Addr,&GlobalUniqAddr.Ip6Addr,LDP_IPV6ADR_LEN);
				    MEMCPY(&TempMask.Ip6Addr,&GlobalUniqMask,LDP_IPV6ADR_LEN);
				    MplsGetPrefix(&TempDestAddr,&TempMask,MPLS_IPV6_ADDR_TYPE,&TempPrefix);
				    MEMCPY(&pL2vpnLabelArgs->Prefix.Ip6Addr,&TempPrefix.Ip6Addr,LDP_IPV6ADR_LEN);

				    if (LdpGetInLabelFrmPrefix (pL2vpnLabelArgs) ==
						    LDP_SUCCESS)
				    {
					    LDP_DBG (LDP_IF_PRCS,
							    "LdpGetFecInLabelForDestAddr: "
							    "LdpGetInLabelFrmPrefix success\n");
					    MPLS_LDP_UNLOCK ();
					    return LDP_SUCCESS;
				    }
			    }
			    /* Else scan through all the configured loopback addresses and use the loopback 
			       address which has LSP configured on it */
			    else
			    {
				    MEMCPY (&u4LdpIp, pLdpEntity->LdpId, 4);
				    i1Ret =
					    LdpGetFecInLabelFromLoopbackIntf (bIsManualPwVc,
							    pL2vpnLabelArgs,
							    u4LdpIp);
				    MPLS_LDP_UNLOCK ();
				    return i1Ret;

			    }

		    }
		    else
#endif
		    {
			    if ((LDP_ENTITY_TRANSPORT_ADDRKIND (pLdpEntity) ==
						    LOOPBACK_ADDRESS_TYPE)
					    && (LDP_ENTITY_TRANSPORT_ADDRESS_TLV (pLdpEntity) ==
						    LDP_TRUE))
			    {
				    LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->Prefix) = pLdpEntity->u4TransportAddress;
				    CfaIpIfGetIfIndexFromIpAddress (LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->Prefix),
						    &IfIndex);
				    if (CfaIpIfGetIfInfo ((UINT4) IfIndex, &IpIfInfo) ==
						    CFA_SUCCESS)
				    {
					    LDP_IPV4_U4_ADDR(pL2vpnLabelArgs->Prefix) &= IpIfInfo.u4NetMask;
					    if (LdpGetInLabelFrmPrefix (pL2vpnLabelArgs) ==
							    LDP_SUCCESS)
					    {
						    LDP_DBG (LDP_IF_PRCS,
								    "LdpGetFecInLabelForDestAddr: "
								    "LdpGetInLabelFrmPrefix success\n");
						    MPLS_LDP_UNLOCK ();
						    return LDP_SUCCESS;
					    }
				    }
			    }
			    /*Else scan through all the configured loopback addresses and use the loopback address 
			     * which has LSP configured on it */
			    else
			    {
				    MEMCPY (&u4LdpIp, pLdpEntity->LdpId, 4);
				    i1Ret =
					    LdpGetFecInLabelFromLoopbackIntf (bIsManualPwVc,
							    pL2vpnLabelArgs,
							    u4LdpIp);
				    MPLS_LDP_UNLOCK ();
				    return i1Ret;
			    }
		    }
	    }
	}
    }
    else
    {                            /*For static PW, fetch the loopback addresses and use the first address which has LSP in label on it */
	    i1Ret =
		    LdpGetFecInLabelFromLoopbackIntf (bIsManualPwVc, pL2vpnLabelArgs,
				    u4LdpIp);
	    MPLS_LDP_UNLOCK ();
	    return i1Ret;
    }
    MPLS_LDP_UNLOCK ();
    LDP_DBG (LDP_IF_PRCS,
		    "LdpGetFecInLabelForDestAddr: "
		    "Failure: Couldnot fetch source transport address.\n");
    return LDP_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldppw.c                                 */
/*---------------------------------------------------------------------------*/
