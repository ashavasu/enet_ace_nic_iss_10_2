/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpssm.c,v 1.43 2016/03/08 11:54:35 siva Exp $
 *
 * Description: This file contains the Session  State Machine routines.
 ********************************************************************/
#include "ldpincs.h"

/*****************************************************************************/
/* Function Name : LdpSsmNonExistentStateHandler                             */
/* Description   : This routine is called when any message is received when  */
/*                 the session is in Non-Existent State.                     */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session to which the event  */
/*                                 is referring to.                          */
/*                 u1Event - Specifies the Session state machine event ie,   */
/*                           whether relating InternalDestroy,               */
/*                           InternalSendInitMsg, RecvInitMsg, KeepAliveMsg, */
/*                           OtherLdpMsg, Timeout RecvNotifMsg.              */
/*                 pMsg - Points to the message received on this session.    */
/* Output(s)     : None                                                      */
/* Return(s)     : None.                                                     */
/*****************************************************************************/

VOID
LdpSsmNonExistentStateHandler (tLdpSession * pSessionEntry, UINT1 u1Event,
                               UINT1 *pMsg)
{

    UINT4               u4TcpConnId = 0;
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);
    pMsg = (UINT1 *) pMsg;

    switch (u1Event)
    {
        case LDP_SSM_EVT_INT_INIT_CON:
            if (LdpTcpOpen
                 ((pSessionEntry->pLdpPeer->TransAddr),
                 SERV_STD_PORT, LDP_ACTIVE_OPEN, &u4TcpConnId, u2IncarnId,
                 pSessionEntry) != LDP_SUCCESS)
            {
                LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Failed to open tcp connection for the"
                          " Transport Address %#x\n",
                          OSIX_NTOHL (*(UINT4 *) (VOID *)
                                      &(pSessionEntry->pLdpPeer->TransAddr)));
                if (LdpDeleteLdpSession (pSessionEntry,
                                         LDP_STAT_TYPE_SHUT_DOWN) ==
                    LDP_FAILURE)
                {
                    LDP_DBG1 (LDP_SSN_PRCS,
                              "SESSION: Failed to Delete Session for the "
                              "transport address\n",
                              OSIX_NTOHL (*(UINT4 *) (VOID *)
                                          &(pSessionEntry->pLdpPeer->
                                            TransAddr)));
                }
                return;
            }
            MplsLdpSessionUpdateSysTime (pSessionEntry);
            pSessionEntry->u4TcpConnId = u4TcpConnId;
            LdpAddSsnToTcpConnTbl (pSessionEntry);
            break;

        case LDP_SSM_EVT_INT_SEND_INIT:
            pSessionEntry->u1SessionState = LDP_SSM_ST_INITIALIZED;
            LDP_SESSION_FSM[SSN_STATE (pSessionEntry)]
                (pSessionEntry, LDP_SSM_EVT_INT_SEND_INIT, NULL);
            break;

        case LDP_SSM_EVT_INT_DESTROY:
            if (LdpDeleteLdpSession (pSessionEntry,
                                     pSessionEntry->u1StatusCode) ==
                LDP_FAILURE)
            {
                LDP_DBG (LDP_SSN_PRCS, "SESSION: Failed to Delete Session\n");
            }
            break;

        default:

            /* All other events InternalDestroy, InternalSendInitMsg,
             * RecvInitMsg, KeepAliveMsg,  OtherLdpMsg, Timeout,
             * RecvNotifMsg. */

            LDP_DBG1 (LDP_SSN_SEM,
                      "SESSION: Rx Invalid Evt %d, when ssn in Non-Exist St\n",
                      u1Event);
            break;
    }
}

/*****************************************************************************/
/* Function Name : LdpSsmInitializedStateHandler                             */
/* Description   : This routine is called when any message is received when  */
/*                 the session is in Initialized State.                      */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session to which the event  */
/*                                 is referring to.                          */
/*                 u1Event - Specifies the Session state machine event ie,   */
/*                     whether relating InternalDestroy, InternalSendInitMsg,*/
/*                           RecvInitMsg, KeepAliveMsg,  OtherLdpMsg, Timeout*/
/*                           RecvNotifMsg.                                   */
/*                 pMsg - Points to the message received on this session.    */
/* Output(s)     : None                                                      */
/* Return(s)     : None.                                                     */
/*****************************************************************************/

VOID
LdpSsmInitializedStateHandler (tLdpSession * pSessionEntry, UINT1 u1Event,
                               UINT1 *pMsg)
{
    UINT1               u1StatType = LDP_STAT_TYPE_SUCCESS;
    UINT4               u4ReTxInterval = LDP_ZERO;

    switch (u1Event)
    {
        case LDP_SSM_EVT_INT_SEND_INIT:
            /* Internal Send Init Msg Event */
            /* 
             * Starting a timer with some back-off time. 
             * The Timer is started to ensure that the Passive Peer 
             * responds to the InitMsg Sent, within certain time.
             * If no response from PassivePeer, then InitMsg is again 
             * Tx. This msg ReTx happens till, either When PassivePeer 
             * responds with an InitMsg or When back-off time crosses 
             * threshold value.
             */
            u4ReTxInterval = LDP_BACK_OFF_TIME (pSessionEntry->u4HoldTimeRem);
            if (u4ReTxInterval > LDP_MAX_RETRY_TIME)
            {
                /* InitMsg ReTx, crossed the threshold value, so deleting the
                 * session. */
                LDP_DBG (LDP_SSN_PRCS,
                         "NoResponse from PassivePeer. Deleting Session\n");
                if (LdpDeleteLdpSession (pSessionEntry,
                                         LDP_STAT_TYPE_SHUT_DOWN) ==
                    LDP_FAILURE)
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "SESSION: Failed to Delete Session\n");
                }
                break;
            }
            else
            {
                /* 
                 * Starting Timer to ReTx InitMsg on it's expiry.
                 * At the expiry of this timer, 
                 * FSM routine(State->INITIALISED, Event->SendInitMsg)
                 * will be called.
                 */
                pSessionEntry->SsnHoldTimer.u4Event = LDP_SSN_TX_INIT_MSG_EVENT;
                pSessionEntry->SsnHoldTimer.pu1EventInfo = (UINT1 *)
                    pSessionEntry;
                pSessionEntry->u4HoldTimeRem = u4ReTxInterval;

                if (TmrStartTimer (LDP_TIMER_LIST_ID,
                                   (tTmrAppTimer *)
                                   & (pSessionEntry->SsnHoldTimer.AppTimer),
                                   (u4ReTxInterval *
                                    SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) ==
                    TMR_FAILURE)
                {
                    LDP_DBG1 (LDP_ADVT_TMR,
                              "ADVT: Failed to start timer inside the function %s \n",
                              __func__);
                }
            }

            LdpSendInitMsg (pSessionEntry);

            pSessionEntry->u1SessionState = LDP_SSM_ST_OPEN_SENT;
            MplsLdpSessionUpdateSysTime (pSessionEntry);
            break;

        case LDP_SSM_EVT_INT_DESTROY:
            /* Internal Destroy Event */
            if (LdpSsmSendNotifCloseSsn
                (pSessionEntry, pSessionEntry->u1StatusCode, pMsg,
                 LDP_FALSE) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_SSN_TX,
                         "SESSION: Failed to send Notif Msg, close Ssn.\n");
            }
            break;

        case LDP_SSM_EVT_RECV_INIT:    /* Received Init Msg Event */
            if (pSessionEntry->u1SessionRole == LDP_ACTIVE)
            {
                /* Error, Active LSR is not supposed to receive Init Msg in
                 * Initialised State  */
                LDP_DBG (LDP_SSN_SEM,
                         "SESSION: Active Lsr Rx Init Msg in Ssn "
                         "Init State!\n");
                if (LdpSsmSendNotifCloseSsn
                    (pSessionEntry, LDP_STAT_TYPE_SHUT_DOWN, pMsg,
                     LDP_FALSE) != LDP_SUCCESS)
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "SESSION: Failed to send Notif Msg and "
                             "close session.\n");
                }
                break;
            }
            /* Lsr acts Passive */
            if ((LdpProcessInitMsg (pSessionEntry, pMsg, &u1StatType)
                 != LDP_SUCCESS) && (u1StatType != LDP_STAT_TYPE_SUCCESS))
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "SESSION: Processing of Init Msg Failed\n");

                /* Generating Notification Msg */
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE, u1StatType,
                                 NULL);
                if (LdpDeleteLdpSession (pSessionEntry,
                                         LDP_STAT_TYPE_SHUT_DOWN) ==
                    LDP_FAILURE)
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "SESSION: Failed to Delete Session\n");
                }
                break;
            }

            /* Rcvd valid Init Msg */
            LdpSendInitMsg (pSessionEntry);
            LdpSendKeepAliveMsg (pSessionEntry);
            pSessionEntry->u1SessionState = LDP_SSM_ST_OPEN_RECV;
            MplsLdpSessionUpdateSysTime (pSessionEntry);
            break;

        case LDP_SSM_EVT_RECV_NOTIF_MSG:
            /* Received Notification Msg Event */
            LdpHandleNotifMsg (pSessionEntry, pMsg);
            break;

        case LDP_SSM_EVT_KEEP_ALIVE_MSG:
        case LDP_SSM_EVT_OTH_LDP_MSG:
        case LDP_SSM_EVT_TIMEOUT:
        case LDP_SSM_EVT_ICCP_MSG:
            /* Received Keep Alive Msg Event or */
            /* Received Other Ldp Messages - Label Req, Label Mapping, Label Req
             * Abort etc., or */
            /* Session Timeout event */
            if (LdpSsmSendNotifCloseSsn (pSessionEntry,
                                         LDP_STAT_TYPE_SHUT_DOWN, pMsg,
                                         LDP_FALSE) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "SESSION: Failed to send Notif Msg & close "
                         "the Ssn.\n");
            }
            break;

        default:
            /* Event specifying Internal Initiate Session */
            LDP_DBG (LDP_SSN_PRCS, "Rx Invalid Evt when Ssn in Init State\n");
            break;
    }
}

/*****************************************************************************/
/* Function Name : LdpSsmOpenSentStateHandler                                */
/* Description   : This routine is called when any message is received when  */
/*                 the session is in Open Sent State.                        */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session to which the event  */
/*                                 is referring to.                          */
/*                 u1Event - Specifies the Session state machine event ie,   */
/*                     whether relating InternalDestroy, InternalSendInitMsg,*/
/*                           RecvInitMsg, KeepAliveMsg,  OtherLdpMsg, Timeout*/
/*                           RecvNotifMsg.                                   */
/*                 pMsg - Points to the message received on this session.    */
/* Output(s)     : None                                                      */
/* Return(s)     : None.                                                     */
/*****************************************************************************/

VOID
LdpSsmOpenSentStateHandler (tLdpSession * pSessionEntry, UINT1 u1Event,
                            UINT1 *pMsg)
{
    UINT1               u1StatType = LDP_STAT_TYPE_SUCCESS;
    UINT1               u1BTime = 0;
    tAtmLdpLblRngEntry *pAtmLblRange = NULL;

    switch (u1Event)
    {

        case LDP_SSM_EVT_INT_DESTROY:
            /* Internal Destroy Event */
            if (LdpSsmSendNotifCloseSsn
                (pSessionEntry, pSessionEntry->u1StatusCode, pMsg,
                 LDP_FALSE) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "SESSION: Failed to send Notif Msg & close "
                         "the Ssn.\n");
            }
            break;

        case LDP_SSM_EVT_RECV_INIT:
            /* Received Init Msg Event */
            /* 
             * Stopping the timer, being used for Syncronisation between Active
             * and Passive Lsrs.
             */
            TmrStopTimer (LDP_TIMER_LIST_ID,
                          (tTmrAppTimer *) & (pSessionEntry->SsnHoldTimer.
                                              AppTimer));
            pSessionEntry->u4HoldTimeRem = LDP_ZERO;

            if (LdpProcessInitMsg (pSessionEntry, pMsg, &u1StatType) !=
                LDP_SUCCESS)
            {

                LDP_DBG (LDP_SSN_PRCS,
                         "SESSION: Processing of Init Msg Failed\n");
                /* 
                 * Freeing all the Resources associated with this Session and
                 * doing to Retry Attemp for Session Establishment. 
                 */

                /* Free the Intersected Label Ranges List */
                pAtmLblRange = (tAtmLdpLblRngEntry *)
                    TMO_SLL_First (SSN_GET_ATM_LBLRNG_LIST (pSessionEntry));
                while (pAtmLblRange != NULL)
                {
                    TMO_SLL_Delete (SSN_GET_ATM_LBLRNG_LIST (pSessionEntry),
                                    &(pAtmLblRange->NextEntry));
                    MemReleaseMemBlock (LDP_ATM_LBL_RNG_POOL_ID,
                                        (UINT1 *) pAtmLblRange);
                    pAtmLblRange = (tAtmLdpLblRngEntry *)
                        TMO_SLL_First (SSN_GET_ATM_LBLRNG_LIST (pSessionEntry));
                }
                TMO_SLL_Init (SSN_GET_ATM_LBLRNG_LIST (pSessionEntry));

                if (LDP_IS_ENTITY_TYPE_ATM (pSessionEntry) == LDP_TRUE)
                {
/* Changed for NP - End */
                    /* Releasing the Label Resources, allocated for this Ssn */
                    ldpDeleteLabelSpaceGroup (SSN_GET_LBL_RNGID
                                              (pSessionEntry));
                }

                /* Notifying Peer of the Error occured while processing 
                 * InitMsg */
                LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                 u1StatType, NULL);

                /* As the session is rejected based on the paramters 
                 * mismatch, try to retry the session after some back-off 
                 * time */
                u1BTime = (UINT1) LDP_BACK_OFF_TIME (pSessionEntry->u1BTime);
                if (u1BTime > LDP_MAX_RETRY_TIME)
                {
                    if (LdpDeleteLdpSession (pSessionEntry,
                                             LDP_STAT_TYPE_SHUT_DOWN) ==
                        LDP_FAILURE)
                    {
                        LDP_DBG (LDP_SSN_PRCS,
                                 "SESSION: Failed to delete Session\n");
                        break;
                    }
                }
                else
                {
                    /* Starting the Retry Timer.
                     * At the expiry of this timer, FSM routine
                     * (State-NON_EXISTENT, Event-Start Ssn Establshmnt)
                     * will be called, which starts the Session Establishment
                     * process fresh. 
                     */

                    /* Closing the Tcp Connection */
                    if (LdpTcpClose (pSessionEntry->u4TcpConnId) != LDP_SUCCESS)
                    {
                        LDP_DBG (LDP_SSN_PRCS,
                                 "SESSION: Failed to close the Tcp "
                                 "Connection\n");
                    }

                    /* Delete the Ssn Entry from Tcp connection hash table */
                    LdpDelSsnFromTcpConnTbl (pSessionEntry);

                    pSessionEntry->SsnHoldTimer.u4Event = LDP_SSN_RETRY_EVENT;
                    pSessionEntry->SsnHoldTimer.pu1EventInfo =
                        (UINT1 *) pSessionEntry;
                    if (TmrStartTimer (LDP_TIMER_LIST_ID,
                                       (tTmrAppTimer *) & (pSessionEntry->
                                                           SsnHoldTimer.
                                                           AppTimer),
                                       (u1BTime *
                                        SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) ==
                        TMR_FAILURE)

                    {
                        LDP_DBG1 (LDP_ADVT_TMR,
                                  "ADVT: Failed to start timer inside the function %s \n",
                                  __func__);
                    }

                    /* Setting the Session state to NON_EXISTENT */
                    pSessionEntry->u1SessionState = LDP_SSM_ST_NON_EXISTENT;
                    MplsLdpSessionUpdateSysTime (pSessionEntry);
                    pSessionEntry->u1BTime = u1BTime;

                }
            }
            else
            {
                /* Init Msg Acceptable */
                if (LdpSendKeepAliveMsg (pSessionEntry) != LDP_SUCCESS)
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "SESSION: Failed to send KAlive Msg \n");
                    break;
                }
                /* Going to Open Recvd State and waiting for a Keep Alive Msg */
                pSessionEntry->u1SessionState = LDP_SSM_ST_OPEN_RECV;
                MplsLdpSessionUpdateSysTime (pSessionEntry);
            }
            break;

        case LDP_SSM_EVT_RECV_NOTIF_MSG:
            /* Received Notification Msg Event */
            LdpHandleNotifMsg (pSessionEntry, pMsg);
            break;

        case LDP_SSM_EVT_KEEP_ALIVE_MSG:
        case LDP_SSM_EVT_OTH_LDP_MSG:
        case LDP_SSM_EVT_TIMEOUT:
        case LDP_SSM_EVT_ICCP_MSG:
            /* Received Keep Alive Msg Event or */
            /* Received Other Ldp Messages - Label Req, Label Mapping, Label Req
             * Abort etc., or */
            /* Session Timeout event */
            if (LdpSsmSendNotifCloseSsn (pSessionEntry, LDP_STAT_TYPE_SHUT_DOWN,
                                         pMsg, LDP_FALSE) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "SESSION: Failed to send Notif Msg & close "
                         "Session.\n");
            }
            break;

        default:
            /* InternalInitCon, InternalSendInitMsg - Invalid combination */
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Rx Invalid Event in Open Sent State\n");
            break;
    }
}

/*****************************************************************************/
/* Function Name : LdpSsmOpenRecvStateHandler                                */
/* Description   : This routine is called when any message is received when  */
/*                 the session is in Open Receive State.                     */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session to which the event  */
/*                                 is referring to.                          */
/*                 u1Event - Specifies the Session state machine event ie,   */
/*                     whether relating InternalDestroy, InternalSendInitMsg,*/
/*                           RecvInitMsg, KeepAliveMsg,  OtherLdpMsg, Timeout*/
/*                           RecvNotifMsg.                                   */
/*                 pMsg - Points to the message received on this session.    */
/* Output(s)     : None                                                      */
/* Return(s)     : None.                                                     */
/*****************************************************************************/

VOID
LdpSsmOpenRecvStateHandler (tLdpSession * pSessionEntry, UINT1 u1Event,
                            UINT1 *pMsg)
{
    tLdpTrapInfo        LdpTrapInfo;
    UINT2               u2IncarnId = LDP_ZERO;
    UINT4               u4RemainingTime = LDP_ZERO;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    UINT4               u4TmpSrcAddr = 0;
    UINT4               u4TempDestAddr;
    UINT4               u4IfIndex = 0;
    UINT4               u4DestAddr = 0;
#ifdef MPLS_IPV6_WANTED
    INT4                i4Retval = NETIPV4_SUCCESS;
    tIp6Addr    Ipv6SrcAddr;
    UINT1       u1Scope = 0;
    MEMSET(&Ipv6SrcAddr,LDP_ZERO,sizeof(tIp6Addr));
#endif

    switch (u1Event)
    {

        case LDP_SSM_EVT_INT_DESTROY:
            /* Internal Destroy Event */
            if (LdpSsmSendNotifCloseSsn
                (pSessionEntry, pSessionEntry->u1StatusCode, pMsg,
                 LDP_FALSE) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "SESSION: Failed to send Notif Msg & close Ssn.\n");
            }
            break;

        case LDP_SSM_EVT_KEEP_ALIVE_MSG:
            /* Received Keep Alive Msg Event */
            /* Starting Session Hold Timer */
            pSessionEntry->SsnHoldTimer.u4Event = LDP_SSN_EXPRD_EVENT;
            pSessionEntry->SsnHoldTimer.pu1EventInfo = (UINT1 *) pSessionEntry;
            if (TmrStartTimer (LDP_TIMER_LIST_ID,
                               (tTmrAppTimer *) & (pSessionEntry->SsnHoldTimer.
                                                   AppTimer),
                               (SSN_GET_SSN_HTIME (pSessionEntry) *
                                SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
            {
                LDP_DBG1 (LDP_SSN_TMR,
                          "SESSION: Failed to start the timer inside function %s.\n",
                          __func__);
            }
            pSessionEntry->u1SessionState = LDP_SSM_ST_OPERATIONAL;
            pLdpTcpUdpSockInfo = LdpGetSockInfoFromConnId (pSessionEntry->u4TcpConnId);            
            if ((NULL != pLdpTcpUdpSockInfo) && (pLdpTcpUdpSockInfo->u4IfIndex == LDP_ZERO))
            {
#ifdef MPLS_IPV6_WANTED
                 if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
                 {
                     u1Scope=Ip6GetAddrScope(&pLdpTcpUdpSockInfo->DestAddr.Ip6Addr);
                     if (ADDR6_SCOPE_LLOCAL != u1Scope)
                     {
                         NetIpv6GetSrcAddrForDestAddr(LDP_ZERO, &pLdpTcpUdpSockInfo->DestAddr.Ip6Addr,
                                                      &Ipv6SrcAddr);
                         i4Retval = NetIpv6IsOurAddress(&Ipv6SrcAddr, &u4IfIndex);
                         if(NETIPV6_FAILURE == i4Retval)
                         {
                             LDP_DBG (LDP_IF_MISC, "NETIPV6_FAILURE retuned by function NetIpv6IsOurAddress \n");
                         }
                         pLdpTcpUdpSockInfo->u4IfIndex = u4IfIndex;
#if 0
                         printf("%s : %d Srcip=%s IfIndex=%d\n",__FUNCTION__,__LINE__,
                         Ip6PrintAddr(&Ipv6SrcAddr),u4IfIndex);
#endif
                     }
                 }
                 else
#endif
                 {
                     MEMCPY(&u4DestAddr, &pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, LDP_IPV4ADR_LEN);
                     u4TempDestAddr = OSIX_NTOHL (u4DestAddr);
                     if(NETIPV4_FAILURE == NetIpv4GetSrcAddrToUseForDest (u4TempDestAddr, &u4TmpSrcAddr))
                     {
                         LDP_DBG2 (LDP_IF_MISC, "FAILURE in NetIpv4GetSrcAddrToUseForDest u4TempDestAddr :%u u4TmpSrcAddr:%u\n",u4TempDestAddr, u4TmpSrcAddr);
                     }
                     CfaIpIfGetIfIndexFromIpAddress(u4TmpSrcAddr, &u4IfIndex);
                     if (u4IfIndex != LDP_ZERO)
                     {
                         pLdpTcpUdpSockInfo->u4IfIndex = u4IfIndex;
                     }
                 }
            }
#ifdef LDP_GR_WANTED
            if (pSessionEntry->u1StaleStatus == LDP_TRUE)
            {
                pSessionEntry->u1StaleStatus = LDP_FALSE;
                LDP_DBG (GRACEFUL_DEBUG, "LdpSsmOpenRecvStateHandler: Marked the session UNSTALE\n");
            }
#endif
            MplsLdpSessionUpdateSysTime (pSessionEntry);
#ifdef MPLS_LDP_BFD_WANTED

            if (pSessionEntry->pLdpPeer->pLdpEntity->i4BfdStatus == LDP_BFD_ENABLED )
			{
			    LDP_DBG1 (LDP_MAIN_ALL, "\n MAIN: LdpSsmOpenRecvStateHandler :: register LDP session index %d with bfd\n",
				          pSessionEntry->u4Index);
                if(LdpRegisterWithBfd (pSessionEntry) != LDP_SUCCESS)
                {
                     LDP_DBG1 (LDP_MAIN_ALL, "MAIN: Unable to register LDP session index %d with bfd\n",
                                  pSessionEntry->u4Index);
                }
			}
#endif
            /* GR Purpose.
             * If the Peer is Restarted, and successfully established
             * session with the node acting as helper, then
             * start Recovery Timer 
             */
            u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);
            if ((gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability !=
                 LDP_GR_CAPABILITY_NONE) &&
                (gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus !=
                 LDP_GR_RECONNECT_IN_PROGRESS))
            {
 
                if (pSessionEntry->pLdpPeer->u1GrProgressState == 
                       LDP_PEER_GR_RECOVERY_IN_PROGRESS)
                {
                    /* Now on the Global varibale wont be updated on the Helper
                     * node to the state LDP_GR_RECOVERY_IN_PROGRESS. This varaible
                     * will be updated to LDP_GR_RECOVERY_IN_PROGRESS only on restarting
                     * node */
#ifndef LDP_GR_WANTED
                    gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus =
                        LDP_GR_RECOVERY_IN_PROGRESS;
#endif
                    /* Transmit the Recovery Time to L2VPN */
                    LdpSsnStatusChngNotif (pSessionEntry,
                                           L2VPN_LDP_PWVC_GR_SET_RECOVER_TMR);
                    LdpGrStartRecoveryTimer (pSessionEntry);
                }
                else if (pSessionEntry->pLdpPeer->u1GrProgressState == 
                           LDP_PEER_GR_ABORTED) /* If Peer has sent the Recovery time as 0 */
                {
                    /* Stop the Reconnect Timer */
                    if (TmrGetRemainingTime (LDP_TIMER_LIST_ID,
                                        &(pSessionEntry->pLdpPeer->GrTimer.AppTimer),
                                         &u4RemainingTime) == TMR_FAILURE)
                    {
                        LDP_DBG (GRACEFUL_DEBUG, "LdpSsmOpenRecvStateHandler: FAILURE in getting "
                                                 "the Remaining GR Time\n");
                        return;
                    }
                    if (u4RemainingTime > LDP_ZERO)
                    {

                        TmrStopTimer (LDP_TIMER_LIST_ID,
                                     (tTmrAppTimer *) & (pSessionEntry->pLdpPeer->GrTimer.
                                      AppTimer));

                         LDP_DBG (GRACEFUL_DEBUG, "LdpSsmOpenRecvStateHandler: "
                                                  "Reconnect Timer Stopped.\n");
                    }
                    gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus = LDP_GR_ABORTED; 
                }
#ifdef MPLS_IPV6_WANTED
                if(LDP_ADDR_TYPE_IPV6 == pSessionEntry->pLdpPeer->NetAddr.u2AddrType)
                {
                    LDP_DBG1 (GRACEFUL_DEBUG, "LdpSsmOpenRecvStateHandler: "
                          "Rcvd Keep Alive Msg from the Restarted Ldp Peer "
                          "%s\n ",
                          Ip6PrintAddr(&(pSessionEntry->pLdpPeer->NetAddr.Addr.Ip6Addr)));
                }
                else
#endif
                {
                    LDP_DBG4 (GRACEFUL_DEBUG, "LdpSsmOpenRecvStateHandler: "
                          "Rcvd Keep Alive Msg from the Restarted Ldp Peer "
                          "%d.%d.%d.%d\n ",
                          pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                          pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                          pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                          pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);
                }
            }
#ifdef LDP_GR_WANTED
            if (gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus == LDP_GR_RECONNECT_IN_PROGRESS)
            {
                gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus = LDP_GR_RECOVERY_IN_PROGRESS;
            }
#endif
            LdpTrapInfo.u1TrapType = LDP_TRAP_SESS_UP;
            LdpTrapInfo.pSession = pSessionEntry;
            LdpNotifySnmp (&LdpTrapInfo);

            LDP_ENTITY_UPDATE_EST_SESSION (pSessionEntry);

            if (pSessionEntry->pLdpPeer->u1GrProgressState ==
                LDP_PEER_GR_RECOVERY_IN_PROGRESS)
            {
                LDP_DBG1 (GRACEFUL_DEBUG,
                          "GR SESSION: Sending Session UP event to L2VPN %s.\n",
							__func__);
                LdpSsnStatusChngNotif (pSessionEntry, L2VPN_LDP_GR_PWVC_SSN_UP);
            }
            else
            {
                LDP_DBG1 (LDP_SSN_PRCS,
                          "SESSION: Sending Session UP event to L2VPN %s.\n",
							__func__);
                LdpSsnStatusChngNotif (pSessionEntry, L2VPN_LDP_PWVC_SSN_UP);
            }

            /* Sending Address Msg to the Peer */
#if 0
            printf("vishalPw: %s %d Send AddressMsg\n",__FUNCTION__,__LINE__);
#endif
             if((LDP_ENTITY_IS_TARGET_TYPE ((SSN_GET_ENTITY (pSessionEntry))) == LDP_TRUE) &&
                     (SSN_GET_ENTITY (pSessionEntry)->OutStackTnlInfo.u4TnlId != LDP_ZERO))
             {
                 LdpSendAddrMsg (pSessionEntry, NULL);
             }
             else if (LDP_ENTITY_IS_TARGET_TYPE ((SSN_GET_ENTITY (pSessionEntry))) == LDP_FALSE)
             {
                 LdpSendAddrMsg (pSessionEntry, NULL);
             }

#ifdef MPLS_IPV6_WANTED
#if 0
            printf("vishalPw: %s %d Send IPV6AddressMsg\n",__FUNCTION__,__LINE__);
#endif
              if((LDP_ENTITY_IS_TARGET_TYPE ((SSN_GET_ENTITY (pSessionEntry))) == LDP_TRUE) &&
                     (SSN_GET_ENTITY (pSessionEntry)->OutStackTnlInfo.u4TnlId != LDP_ZERO))
             {
                LdpSendIpv6AddrMsg(pSessionEntry,NULL,NULL,NULL);
             }
             else if (LDP_ENTITY_IS_TARGET_TYPE ((SSN_GET_ENTITY (pSessionEntry))) == LDP_FALSE)
             {
              LdpSendIpv6AddrMsg(pSessionEntry,NULL,NULL,NULL);
             }

#endif

            break;

        case LDP_SSM_EVT_RECV_NOTIF_MSG:
            /* Received Notification Msg Event */
            LdpHandleNotifMsg (pSessionEntry, pMsg);
            break;

        case LDP_SSM_EVT_RECV_INIT:
        case LDP_SSM_EVT_OTH_LDP_MSG:
        case LDP_SSM_EVT_TIMEOUT:
        case LDP_SSM_EVT_ICCP_MSG:
            /* Received Inti Msg Event or */
            /* Received Other Ldp Messages - Label Req,
             * Label Mapping, Label Req Abort etc., or */
            /* Session Timeout event */
            if (LdpSsmSendNotifCloseSsn (pSessionEntry, LDP_STAT_TYPE_SHUT_DOWN,
                                         pMsg, LDP_FALSE) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "SESSION: Failed to send Notif Msg & close session.\n");
            }
            break;

        default:
            /* InternalInitCon, InternalSendInitMsg Events */
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Rx Invalid Evt in Open Receive State\n");
            break;
    }
}

/*****************************************************************************/
/* Function Name : LdpSsmOperationalStateHandler                             */
/* Description   : This routine is called when any message is received when  */
/*                 the session is in Operational State.                      */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session to which the event  */
/*                                 is referring to.                          */
/*                 u1Event - Specifies the Session state machine event ie,   */
/*                     whether relating InternalDestroy, InternalSendInitMsg,*/
/*                           RecvInitMsg, KeepAliveMsg,  OtherLdpMsg, Timeout*/
/*                           RecvNotifMsg.                                   */
/*                 pMsg - Points to the message received on this session.    */
/* Output(s)     : None                                                      */
/* Return(s)     : None.                                                     */
/*****************************************************************************/

VOID
LdpSsmOperationalStateHandler (tLdpSession * pSessionEntry, UINT1 u1Event,
                               UINT1 *pMsg)
{
    tLdpIfMsg           LdpIfMsg;
    UINT2               u2MsgType = 0;
    UINT2               u2HLen = 0;
    UINT1               u1AddrMsg;
    UINT1               u1AddrWrMsg;
    eGenAddrType        AddrFmly;

    if (pMsg != NULL)
    {
        LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
        if (u2HLen > (&pSessionEntry->CmnSsnParams)->u2MaxPduLen)
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Pdu Len greater than negotiated Max Pdu Len\n");
            return;
        }
    }

    switch (u1Event)
    {
        case LDP_SSM_EVT_OTH_LDP_MSG:
            /* Received Other Ldp Messages - LabelReq,
             * Label Mapping, Label Req Abort etc., */
            /* Restart Session Hold Timer */

            if (TmrStopTimer (LDP_TIMER_LIST_ID,
                              (tTmrAppTimer *) & (pSessionEntry->SsnHoldTimer.
                                                  AppTimer)) == TMR_FAILURE)
            {
                LDP_DBG1 (LDP_SSN_TMR,
                          "SESSION: Failed to stop the timer inside function %s.\n",
                          __func__);
            }
            if (TmrStartTimer (LDP_TIMER_LIST_ID,
                               (tTmrAppTimer *) & (pSessionEntry->SsnHoldTimer.
                                                   AppTimer),
                               (SSN_GET_SSN_HTIME (pSessionEntry) *
                                SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
            {
                LDP_DBG1 (LDP_SSN_TMR,
                          "SESSION: Failed to start the timer inside function %s.\n",
                          __func__);
            }
            if (pMsg == NULL)
            {
                break;
            }
            LDP_GET_2_BYTES (pMsg, u2MsgType);
            u1AddrMsg = (u2MsgType == LDP_ADDR_MSG) ? LDP_TRUE : LDP_FALSE;
            u1AddrWrMsg = (u2MsgType == LDP_ADDR_WITHDRAW_MSG)
                ? LDP_TRUE : LDP_FALSE;
            if (u1AddrMsg == LDP_TRUE)
            {
                if (LdpHandleAddrMsg (pSessionEntry, pMsg) != LDP_SUCCESS)
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "SESSION:Error handling Address Msg\n");
                    break;
		}
		/* Get Adr Family from Adr Msg */
		AddrFmly = (eGenAddrType) LDP_GET_ADDR_FMLY (pMsg);
		/* 
		 * Posting an Internal Event to LDP, for establishing Lsps for
                 * all the Destinations(present in the Routing Table), for which
                 * the NextHop is reachable on the current session.
                 */
                LdpIfMsg.u4MsgType = LDP_INTERNAL_EVENT;
                LdpIfMsg.u.IntEvt.u4SubEvt = LDP_ESTB_LSPS_EVENT;
                LdpIfMsg.u.IntEvt.pSession = (UINT1 *) pSessionEntry;
		LdpIfMsg.u.IntEvt.pEntity = NULL;
		LdpIfMsg.u.IntEvt.u4IncarnNo = LDP_CUR_INCARN;
#ifdef MPLS_IPV6_WANTED
		if(AddrFmly==IPV6)
		{
			LdpIfMsg.u.IntEvt.u2AddrType=LDP_ADDR_TYPE_IPV6;
		}
		else
#endif
		{
			LdpIfMsg.u.IntEvt.u2AddrType=LDP_ADDR_TYPE_IPV4;
		}


                if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "SESSION: Failed to EnQ SNMP evt to LDP Task\n");
                }
            }
            else if (u1AddrWrMsg == LDP_TRUE)
            {
                if (LdpHandleAddrWithdrawMsg (pSessionEntry, pMsg) !=
                    LDP_SUCCESS)
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "SESSION: Error handling Address Wthdraw Msg\n");
                    break;
                }
            }
            else
            {
                /* Advert Msg */
                if (LdpHandleAdvertMsg (pSessionEntry, pMsg) != LDP_SUCCESS)
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "SESSION: Error handling Advt Msg\n");
                    break;
                }

            }
            break;

        case LDP_SSM_EVT_KEEP_ALIVE_MSG:

            /* Received Keep Alive Msg Event */
#ifdef MPLS_IPV6_WANTED
             if(LDP_ADDR_TYPE_IPV6 == pSessionEntry->pLdpPeer->NetAddr.u2AddrType)
             {
                 LDP_DBG1 (LDP_SSN_RX,
                      "SESSION: Rcvd Keep alive Msg from %s\n",
                          Ip6PrintAddr(&(pSessionEntry->pLdpPeer->NetAddr.Addr.Ip6Addr)));
             }
             else
             {
#endif
                 LDP_DBG4 (LDP_SSN_RX,
                      "SESSION: Rcvd Keep alive Msg from %d.%d.%d.%d\n",
                      pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                      pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                      pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                      pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);
#ifdef MPLS_IPV6_WANTED
             }
#endif
            /* Restart the Session Hold Timer */
            if (TmrStopTimer (LDP_TIMER_LIST_ID,
                              (tTmrAppTimer *) & (pSessionEntry->SsnHoldTimer.
                                                  AppTimer)) == TMR_FAILURE)
            {
                LDP_DBG1 (LDP_SSN_TMR,
                          "SESSION: Failed to stop the timer inside function %s.\n",
                          __func__);
            }
            if (TmrStartTimer (LDP_TIMER_LIST_ID,
                               (tTmrAppTimer *) & (pSessionEntry->SsnHoldTimer.
                                                   AppTimer),
                               (SSN_GET_SSN_HTIME (pSessionEntry) *
                                SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
            {
                LDP_DBG1 (LDP_SSN_TMR,
                          "SESSION: Failed to start the timer inside function %s.\n",
                          __func__);
            }
            break;

        case LDP_SSM_EVT_INT_DESTROY:
            /* Internal Destroy Event */
            if (LdpSsmSendNotifCloseSsn
                (pSessionEntry, pSessionEntry->u1StatusCode, pMsg,
                 LDP_FALSE) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "SESSION: Failed to send Notif Msg & close Ssn.\n");
            }
            break;

        case LDP_SSM_EVT_RECV_NOTIF_MSG:
            /* Received Notification Msg Event */
            LdpHandleNotifMsg (pSessionEntry, pMsg);
            break;

        case LDP_SSM_EVT_TIMEOUT:
            /* Session Hold Timer expired */
            if (LdpSsmSendNotifCloseSsn (pSessionEntry, LDP_STAT_TYPE_SHUT_DOWN,
                                         pMsg, LDP_FALSE) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "SESSION: Failed to send Notif Msg and close Ssn.\n");
            }
            break;

        case LDP_SSM_EVT_ICCP_MSG:
            /* Received RG Application Data message Event */
            if (pMsg == NULL)
            {
                break;
            }
            if (LdpHandleIccpMsg (pSessionEntry, pMsg) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_SSN_PRCS, "SESSION: ERROR handling ICCP Msg\n");
            }
            break;

        default:
            /* InternalInitCon, InternalSendInitMsg, RecvInitMsg Event */
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Rx Invalid Evt in Operational State\n");
            break;
    }
}

/*****************************************************************************/
/* Function Name : LdpSsmSessionRetryStateHandler                            */
/* Description   : This routine is called when any message is received when  */
/*                 the session is in Session Retry State.                    */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to the session to which the event  */
/*                                 is referring to.                          */
/*                 u1Event - Specifies the Session state machine event ie,   */
/*                     whether relating InternalDestroy, InternalSendInitMsg,*/
/*                           RecvInitMsg, KeepAliveMsg,  OtherLdpMsg, Timeout*/
/*                           RecvNotifMsg.                                   */
/*                 pMsg - Points to message                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : None.                                                     */
/*****************************************************************************/

VOID
LdpSsmSessionRetryStateHandler (tLdpSession * pSessionEntry, UINT1 u1Event,
                                UINT1 *pMsg)
{
    UINT2               u2HLen = 0;
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    if (u2HLen > (&pSessionEntry->CmnSsnParams)->u2MaxPduLen)
    {
        LDP_DBG (LDP_SSN_PRCS,
                 "SESSION: Pdu Len greater than negotiated Max Pdu Len\n");
        return;
    }

    switch (u1Event)
    {
        case LDP_SSM_EVT_TIMEOUT:
            /* Session Retry Timeout event */
            /* Set Session state to Initialised, and restart the session
             * establishment */
            pSessionEntry->u1SessionState = LDP_SSM_ST_INITIALIZED;
            MplsLdpSessionUpdateSysTime (pSessionEntry);
            LDP_SESSION_FSM[LDP_SSM_ST_INITIALIZED] (pSessionEntry,
                                                     LDP_SSM_EVT_INT_SEND_INIT,
                                                     NULL);
            LDP_DBG (LDP_SSN_MISC, "SESSION: Retry Timeout Event Handled\n");
            break;

        case LDP_SSM_EVT_INT_DESTROY:
            /* Internal Destroy Event */
            if (LdpSsmSendNotifCloseSsn
                (pSessionEntry, pSessionEntry->u1StatusCode, pMsg,
                 LDP_FALSE) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_SSN_PRCS,
                         "SESSION: Failed to send Notif Msg and close Ssn.\n");
            }
            LDP_DBG (LDP_SSN_MISC,
                     "SESSION:Int Destroy Evt handled successfully\n");
            break;

        default:
            /* InternalInitCon, InternalSendInitMsg, RecvInitMsg,
               KeepAliveMsg, RecvNotifMsg, OtherLdpMsg */
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Rx Invalid Evt in Ssn Retry State\n");
            break;
    }
}

/*****************************************************************************/
/* Function Name : LdpSsmSendNotifCloseSsn                                   */
/* Description   : This routine sends the notification message, deletes all  */
/*                 the lsp control blocks associated to this session, and    */
/*                 finally closes the tcp connection.                        */
/* Input(s)      : pSessionEntry - Points to the session to be closed.       */
/*                 u1StatType  - Specifes the type of Notification message   */
/*                               to be generated.                            */
/*                 pMsg - Points to the message received on this session.    */
/*                 u1IsPdu - Specifies if pMsg contains Ldp Pdu or Ldp Msg.  */
/*                           A value of LDP_TRUE specifes Ldp Pdu.           */
/*                                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpSsmSendNotifCloseSsn (tLdpSession * pSessionEntry,
                         UINT1 u1StatType, UINT1 *pMsg, UINT1 u1IsPduFlag)
{
    UNUSED_PARAM (pMsg);
    UNUSED_PARAM (u1IsPduFlag);

    if (LdpDeleteLdpSession (pSessionEntry, u1StatType) == LDP_FAILURE)
    {
        LDP_DBG (LDP_SSN_PRCS, "SESSION: Failed to delete Ldp Session\n");
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;

}

/*****************************************************************************/
/* Function Name : LdpSendLblResAvlNotifMsg                                  */
/* Description   : This routine sends the Label resource available           */
/*                 notification message                                      */
/* Input(s)      : pLdpEntity - Points to the Entity to whose peers' the msg */
/*                 needs to be sent.                                         */
/*                                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpSendLblResAvlNotifMsg (tLdpEntity * pLdpEntity)
{
    UINT1               u1SsnState;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSessionEntry = NULL;

    TMO_SLL_Scan (&(pLdpEntity->PeerList), pLdpPeer, tLdpPeer *)
    {
        pLdpSessionEntry = (tLdpSession *) pLdpPeer->pLdpSession;
        if (pLdpSessionEntry != NULL)
        {
            SSN_GET_SSN_STATE (pLdpSessionEntry, u1SsnState);
            if (u1SsnState == LDP_SSM_ST_OPERATIONAL)
            {
                /* There is no notif handling if the session is DU */
                if (SSN_ADVTYPE (pLdpSessionEntry) != LDP_DSTR_UNSOLICIT)
                {
                    LdpSendNotifMsg (pLdpSessionEntry, NULL, LDP_FALSE,
                                     LDP_STAT_TYPE_LBL_RSRC_AVBL, NULL);
                }
            }
        }
    }
}

/*****************************************************************************/
/* Function Name : LdpDeleteLdpSession                                       */
/* Description   : This routine is called to delete a session entry.         */
/*                 For deleting the Upstream and DownStream LSP control      */
/*                 control blocks, it generates the upstream/downstream NAKs */
/*                 (to itself) respectively.                                 */
/*                                                                           */
/* Input(s)      : pSessionEntry - Points to session that needs to be deleted*/
/*                 u1StatType    - Status Error Code to be sent in           */
/*                                 Notification Message                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpDeleteLdpSession (tLdpSession * pSessionEntry, UINT1 u1StatType)
{
    UINT2               u2IncarnId = 0;
    UINT4               u4Index = 0;
    tTMO_SLL           *pLdpSll = NULL;
    tLspCtrlBlock      *pLspCtrlBlock = NULL;
    tLdpAdjacency      *pSsnAdj = NULL;
    /*tIntLspSetupInfo   *pIntLspSetupEntry = NULL; */
    tAtmLdpLblRngEntry *pAtmLblRange = NULL;
    tAtmLdpLblRngEntry *pPeerLblRngEntry = NULL;
    tLdpMsgInfo         LdpMsgInfo;
    tUstrLspCtrlBlock  *pUsLspCtrlBlock = NULL;
    tLdpTrapInfo        LdpTrapInfo;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tTMO_SLL_NODE      *pTempSllNode = NULL;
    BOOL1               b1Cond1 = FALSE;
    BOOL1               b1Cond2 = FALSE;

    MEMSET (&LdpMsgInfo, LDP_ZERO, sizeof (tLdpMsgInfo));

    LDP_DBG (LDP_SSN_PRCS, "LdpDeleteLdpSession: Entry\n");

    if (pSessionEntry == NULL)
    {
        LDP_DBG (LDP_SSN_PRCS,
                 "SESSION: Session passed is Invalid. Can't delete. \n");
        return LDP_FAILURE;
    }
    u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);

    if (pSessionEntry->u1SessionState == LDP_SSM_ST_OPERATIONAL)
    {
        LdpTrapInfo.u1TrapType = LDP_TRAP_SESS_DN;
        LdpTrapInfo.pSession = pSessionEntry;
        LdpNotifySnmp (&LdpTrapInfo);
    }
#ifdef MPLS_LDP_BFD_WANTED   
    if (pSessionEntry->pLdpPeer->pLdpEntity->i4BfdStatus == LDP_BFD_ENABLED )
    {
        if (pSessionEntry->i4SessBfdStatus != LDP_ZERO) 
		{
		    if (pSessionEntry->i4SessBfdStatus == LDP_BFD_ENABLED)
            {
                /* Deregister LDP Session from IP Path monitoring by BFD*/
                if ((LdpDeRegisterWithBfd(pSessionEntry))!= LDP_SUCCESS)
                {
                    LDP_DBG1 (LDP_MAIN_MEM, "MAIN: Unable to deregister LDP session index %d with bfd\n",
                                    pSessionEntry->u4Index);
                }
            }
        }
	}
#endif 
    
	/* This is incase of issuing shutdown LDP command */
    /* 
     * If GR Shutdown is in progress and its capability is Full 
     * then Mark the PwEntries corresponding to this session as STALE.
     */
#ifdef LDP_GR_WANTED
   	LDP_DBG3 (GRACEFUL_DEBUG, "LdpDeleteLdpSession: "
				"Peer Gr Progress State(%d),GR Capability(%d), "
				"GBL GR Status(%d)\n",
				pSessionEntry->pLdpPeer->u1GrProgressState,
				gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability,
				gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus); 
     /* Helper node: If the node is configured as HELPER and peer has the capability
      * to retain the MPLS forwarding entries after restart, and the session is operating
      * in Unsolicited Mode, Then start the Helper Process.*/
	
#ifdef MPLS_LDP_BFD_WANTED
     if (pSessionEntry->bIsBfdSessCtrlPlaneInDep != TRUE)
	 {
#endif
	     /* if bfd session is not offloaded */
	     if ((pSessionEntry->pLdpPeer->u1GrProgressState == LDP_PEER_GR_RECONNECT_IN_PROGRESS) && 
             (SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_UNSOLICIT)) 
         {
             LDP_DBG4 (GRACEFUL_DEBUG, "LdpDeleteLdpSession: "
                  "GR Helper Process Initiatied for the "
                  "the Ldp Peer %d.%d.%d.%d.\n",
                  pSessionEntry->pLdpPeer->PeerLdpId[0],
                  pSessionEntry->pLdpPeer->PeerLdpId[1],
                  pSessionEntry->pLdpPeer->PeerLdpId[2],
                  pSessionEntry->pLdpPeer->PeerLdpId[3]);

             LdpGrStartHelperProcess (pSessionEntry);
             return LDP_SUCCESS;
         }

#ifdef MPLS_LDP_BFD_WANTED
	 }
#endif

#else
    /* This is in case of Peer Went down */
    if ((gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability !=
              LDP_GR_CAPABILITY_NONE) &&
             (pSessionEntry->pLdpPeer->u2GrReconnectTime != LDP_ZERO) &&
             (pSessionEntry->pLdpPeer->u1GrProgressState ==
              LDP_PEER_GR_RECONNECT_IN_PROGRESS))
    {
        LDP_DBG4 (GRACEFUL_DEBUG, "LdpDeleteLdpSession: "
                  "GR Helper Process Initiatied for the "
                  "the Ldp Peer %d.%d.%d.%d.\n",
                  pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                  pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                  pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                  pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);

        LdpGrStartHelperProcess (pSessionEntry);
        b1Cond2 = TRUE;
       /* Start the Functions of Helper.
         * Start the Reconnect Timer.
         * Marking the Pseudowire Entries as STALE etc.,
         */
    }
#endif
    if ((gLdpInfo.LdpIncarn[u2IncarnId].u1GrProgressStatus ==
         LDP_GR_SHUT_DOWN_IN_PROGRESS) &&
        (gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability ==
         LDP_GR_CAPABILITY_FULL) &&
        (pSessionEntry->pLdpPeer->u1GrProgressState !=
         LDP_PEER_GR_NOT_SUPPORTED))
    {
        LDP_DBG4 (GRACEFUL_DEBUG, "LdpDeleteLdpSession: "
                  "GR shutdown Notification Sent to Application for "
                  "the Ldp Peer %d.%d.%d.%d.\n",
                  pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                  pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                  pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                  pSessionEntry->pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);

        LdpSsnStatusChngNotif (pSessionEntry, L2VPN_LDP_PWVC_GR_SHUTDOWN);
        b1Cond1 = TRUE;
    }

    else
    {
        LdpSsnStatusChngNotif (pSessionEntry, L2VPN_LDP_PWVC_SSN_DOWN);
    }
    /* Generate Downstream Lost to each Lsp Control Block for which 
     * this session acts as Upstream */

    TMO_DYN_SLL_Scan (&SSN_DLCB (pSessionEntry), pSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        pLspCtrlBlock = SLL_TO_LCB (pSllNode);

        LdpInvCorrStMh (pSessionEntry, (tLspCtrlBlock *) pLspCtrlBlock,
                        LDP_LSM_EVT_DN_LOST, &LdpMsgInfo, LDP_FALSE);
    }

    /*  Count won't lead to NULL if we receive invalid state events 
     *  So the Control Blocks are directly deleted 
     *  This condition has to be verified as it may lead to no
     *  updates to Cross connects in MPLSFM */
    pLspCtrlBlock = (tLspCtrlBlock *) TMO_SLL_First (&SSN_DLCB (pSessionEntry));
    while (pLspCtrlBlock != NULL)
    {
        pLspCtrlBlock = SLL_TO_LCB (pLspCtrlBlock);
        if (pLspCtrlBlock != NULL)
        {
            LdpNonMrgSsnDnDeleteLspCtrlBlk(pLspCtrlBlock);
        }
        pLspCtrlBlock =
            (tLspCtrlBlock *) TMO_SLL_First (&SSN_DLCB (pSessionEntry));
    }
    TMO_SLL_Init (&SSN_DLCB (pSessionEntry));

    TMO_DYN_SLL_Scan (&SSN_ULCB (pSessionEntry), pSllNode,
                      pTempSllNode, tTMO_SLL_NODE *)
    {
        LdpInvCorrStMh (pSessionEntry, (tUstrLspCtrlBlock *) pSllNode,
                        LDP_LSM_EVT_UP_LOST, &LdpMsgInfo, LDP_TRUE);
    }

    /*  Count won't lead to NULL if we receive invalid state events 
     *  So the Control Blocks are directly deleted 
     *  This condition has to be verified as it may lead to no
     *  updates to Cross connects in MPLSFM */

    /* Scan the Upstream Lsp Control Block List */
    if ((SSN_MRGTYPE (pSessionEntry) == LDP_NO_MRG) &&
        (SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND))
    {
        pLspCtrlBlock = (tLspCtrlBlock *)
            TMO_SLL_First (&SSN_ULCB (pSessionEntry));
        while (pLspCtrlBlock != NULL)
        {
            LdpNonMrgSsnDnDeleteLspCtrlBlk (pLspCtrlBlock);
            pLspCtrlBlock = (tLspCtrlBlock *)
                TMO_SLL_First (&SSN_ULCB (pSessionEntry));
        }
    }
    else
    {
        pUsLspCtrlBlock =
            (tUstrLspCtrlBlock *) TMO_SLL_First (&SSN_ULCB (pSessionEntry));
        while (pUsLspCtrlBlock != NULL)
        {
            if (SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_ON_DEMAND)
            {
                LdpDeleteUpstrCtrlBlock (pUsLspCtrlBlock);
            }
            else
            {
                LdpDuDeleteUpLspCtrlBlock (pUsLspCtrlBlock);
            }
            pUsLspCtrlBlock = (tUstrLspCtrlBlock *) TMO_SLL_First
                (&SSN_ULCB (pSessionEntry));
        }
    }
    TMO_SLL_Init (&SSN_ULCB (pSessionEntry));

    /* If GR Shutdown is in progress and its capability is Full 
     * then no need to send Notifications to the neighbour / Peer.
     * Similarly if a node has GR capability and has lost its GR 
     * Progress Status is RECONNECT state, no need to send notifications
     * to the neighbour / Peer.
     */


    if (b1Cond1 == TRUE)
    {
        LdpSendNotifMsg (pSessionEntry, NULL, LDP_ZERO,
                      LDP_STAT_TYPE_TEMPORARY_SHUT_DOWN, NULL);
    }

    if (u1StatType == LDP_ZERO)
    {
        u1StatType = LDP_STAT_TYPE_SHUT_DOWN;
    }

    if ((b1Cond1 == FALSE) && (b1Cond2 == FALSE))
    {
        LdpSendNotifMsg (pSessionEntry, NULL, LDP_ZERO, u1StatType, NULL);
    }

    /* Delete the Session entry from the Tcp connection hash table */
    LdpDelSsnFromTcpConnTbl (pSessionEntry);

    /* 
     * Deletes the peers If Address Info received on this session
     * through Address message
     */
    LdpDelPeerIfAdrInfo (pSessionEntry);

    /* Init the passive peer entry to NULL */
    for (u4Index = 0; u4Index < LDP_MAX_PASSIVE_PEERS; u4Index++)
    {
        if (LDP_INCARN_PPEER_ARR (u2IncarnId, u4Index) ==
            pSessionEntry->pLdpPeer)
        {
            LDP_INCARN_PPEER_ARR (u2IncarnId, u4Index) = NULL;
            break;
        }
    }

    /* Close the Tcp Connection */
    if (LdpTcpClose (pSessionEntry->u4TcpConnId) != LDP_SUCCESS)
    {
        LDP_DBG (LDP_SSN_PRCS, "SESSION: Failed to close the Tcp Connection\n");
    }

    pSessionEntry->u4TcpConnId = LDP_INVALID_CONN_ID;
    pSessionEntry->u1SessionInitStatus = LDP_SSN_TCP_NON_INIT;
    /* Stop and delete the timers */
    TmrStopTimer (LDP_TIMER_LIST_ID,
                  (tTmrAppTimer *) & (pSessionEntry->SsnHoldTimer.AppTimer));
    TmrStopTimer (LDP_TIMER_LIST_ID,
                  (tTmrAppTimer *) & (pSessionEntry->KeepAliveTimer.AppTimer));

    /* Free the Adjacency List */
    pSsnAdj = (tLdpAdjacency *) TMO_SLL_First (&(pSessionEntry->AdjacencyList));
    while (pSsnAdj != NULL)
    {
        /* Stop the Adj Expiry Timer */
        TmrStopTimer (LDP_TIMER_LIST_ID,
                      (tTmrAppTimer *) & (pSsnAdj->PeerAdjTimer.AppTimer));
        /* Delete the adj node */
        TMO_SLL_Delete (&(pSessionEntry->AdjacencyList),
                        &(pSsnAdj->NextAdjacency));
        MemReleaseMemBlock (LDP_ADJ_POOL_ID, (UINT1 *) pSsnAdj);
        pSsnAdj =
            (tLdpAdjacency *) TMO_SLL_First (&(pSessionEntry->AdjacencyList));
    }
    TMO_SLL_Init (&(pSessionEntry->AdjacencyList));
    pSessionEntry->u1SessionState = LDP_SSM_ST_NON_EXISTENT;

    /* 
     * Deleting the AtmLabel Ranges learnt from Peer on this session, from the 
     * corresponding LdpPeerEntry.(For Atm case).
     */

    /* Peer's AtmLabelRangeList */
    pLdpSll = SSN_GET_PEER_ATM_LBLRNG_LIST (pSessionEntry);
    pPeerLblRngEntry = (tAtmLdpLblRngEntry *) TMO_SLL_First (pLdpSll);
    while (pPeerLblRngEntry != NULL)
    {
        /* Delete the Atm Label Range Node */
        TMO_SLL_Delete (pLdpSll, &(pPeerLblRngEntry->NextEntry));
        MemReleaseMemBlock (LDP_ATM_LBL_RNG_POOL_ID,
                            (UINT1 *) pPeerLblRngEntry);
        pPeerLblRngEntry = (tAtmLdpLblRngEntry *) TMO_SLL_First (pLdpSll);
    }
    TMO_SLL_Init (pLdpSll);

    /* Free the Intersected Label Ranges List */
    pAtmLblRange = (tAtmLdpLblRngEntry *)
        TMO_SLL_First (SSN_GET_ATM_LBLRNG_LIST (pSessionEntry));
    while (pAtmLblRange != NULL)
    {
        TMO_SLL_Delete (SSN_GET_ATM_LBLRNG_LIST (pSessionEntry),
                        &(pAtmLblRange->NextEntry));
        MemReleaseMemBlock (LDP_ATM_LBL_RNG_POOL_ID, (UINT1 *) pAtmLblRange);
        pAtmLblRange = (tAtmLdpLblRngEntry *)
            TMO_SLL_First (SSN_GET_ATM_LBLRNG_LIST (pSessionEntry));
    }
    TMO_SLL_Init (SSN_GET_ATM_LBLRNG_LIST (pSessionEntry));

    if (LDP_IS_ENTITY_TYPE_ATM (pSessionEntry) == LDP_TRUE)
    {
        /* Releasing the Label Resources, allocated for this Session */
        ldpDeleteLabelSpaceGroup (SSN_GET_LBL_RNGID (pSessionEntry));
    }

    /* Free this Session Node from the Ssn List */
    pSessionEntry->pLdpPeer->pLdpSession = NULL;
    LDP_DBG2(LDP_SSN_PRCS,"LDPPrcTmrEvt: Session 0x%x Deleted from Peer structure:0x%x \n", pSessionEntry, 
		pSessionEntry->pLdpPeer);
    MemReleaseMemBlock (LDP_SSN_POOL_ID, (UINT1 *) pSessionEntry);
    MplsLdpPeerUpdateSysTime ();
    
     LDP_DBG (LDP_SSN_PRCS, "LdpDeleteLdpSession: Exit\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessInitMsg                                         */
/* Description   : Processes the received Init Msg, checks if the Session    */
/*                 paramters mentioned are acceptable. If not acceptable     */
/*                 returns error.                                            */
/* Input(s)      : pSessionEntry - Session on which the msg is received.     */
/*                 pMsg - Points to the msg containing the Init Msg.         */
/* Output(s)     : pu1StatType - Specifies the status type, in case the      */
/*                               ssn paramters are not acceptable.           */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpProcessInitMsg (tLdpSession * pSessionEntry, UINT1 *pMsg, UINT1 *pu1StatType)
{
    UINT2               u2HLen = 0;
    UINT2               u2Len = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TmpLen = 0;

    UINT1               u1MTlvPrsnt = LDP_FALSE;

    /* Process the Cmn Ssn Params */

    /* Length of Init Msg */
    LDP_GET_2_BYTES ((pMsg + LDP_MSG_TYPE_LEN), u2HLen);
    pMsg += (LDP_MSG_HDR_LEN + LDP_MSG_ID_LEN);
    /* This length specifies the total length of all the tlvs together
     * contained in this hello Msg */
    u2HLen -= (UINT2)LDP_MSG_ID_LEN;

    while (u2Len < u2HLen)
    {
        LDP_EXT_2_BYTES (pMsg, u2TlvType);    /* Extract the Tlv Type */
        LDP_EXT_2_BYTES (pMsg, u2TmpLen);    /* Extract the Tlv Len */
        /* Check for the bad Tlv length is done here */
        if ((u2Len + u2TmpLen) > u2HLen)
        {
            /* 
             *  The Tlv length is too large. Signal the error.
             */
            *pu1StatType = LDP_STAT_TYPE_BAD_TLV_LEN;
            return LDP_FAILURE;
        }

        switch (u2TlvType)
        {
            case LDP_CMN_SSN_PARAM_TLV:
                u1MTlvPrsnt = LDP_TRUE;
                /* Process Common Session Parameters Tlv */
                if (LdpPrcsCmnSsnParamsTlv (&pMsg, pSessionEntry, pu1StatType)
                    != LDP_SUCCESS)
                {
                    return LDP_FAILURE;
                }
                break;

            case LDP_ATM_SSN_PARAM_TLV:
                if (LDP_IS_ENTITY_TYPE_ATM (pSessionEntry) != LDP_TRUE)
                {
                    LDP_DBG (LDP_SSN_PRCS,
                             "SESSION: Rx Atm Params Tlv in non-atm mode!\n");
                    /* As ATM session praram TLV is not expected, but
                     * since it is received, the Error Status is indicated 
                     * as MALFORMED Tlv.
                     */
                    *pu1StatType = LDP_STAT_TYPE_CRPTD_TLV_VAL;
                    return LDP_FAILURE;
                }

                if (LdpPrcsAtmSsnParamsTlv (&pMsg, pSessionEntry, pu1StatType)
                    != LDP_SUCCESS)
                {
                    return LDP_FAILURE;
                }
                break;

            case LDP_FT_SSN_TLV:
                LdpPrcsFTSsnTlv (&pMsg, pSessionEntry, u2TmpLen);
                break;
            default:
                /* As per RFC 5036 section 3.3, If both U bit is set
                 * the unknown TLV is silently ignored and the rest of the message
                 * is processed
                 * */
                if (!(u2TlvType & LDP_TLV_MSG_U_BIT))
                {
                        /* Notification message with
                         * "LDP_STAT_TYPE_UNKNOWN_TLV" sent. */
                        LdpSendNotifMsg (pSessionEntry, pMsg, LDP_FALSE,
                                        LDP_STAT_TYPE_UNKNOWN_TLV, NULL);
                        return LDP_FAILURE;
                }
                pMsg += u2TmpLen;
                break;
        }

        /* Check if Mand Tlv is processed or not */
        if (u1MTlvPrsnt != LDP_TRUE)
        {

            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Cmn Ssn Params Tlv(Mandatory) not present !\n");
            *pu1StatType = LDP_STAT_TYPE_MISSING_MSG_PARAM;
            return LDP_FAILURE;
        }

        /* Increment by the msg length processed */
        u2Len += (UINT2)(u2TmpLen + LDP_TLV_HDR_LEN);
    }
    /* End of the While loop for decoding the Tlvs in Init Msg */

    return (LDP_SUCCESS);
}

/*****************************************************************************/
/* Function Name : LdpPrcsCmnSsnParamsTlv                                    */
/* Description   : Processes the common session paramters tlv.               */
/* Input(s)      : ppTlv - Points to the Common session paramters Tlv         */
/*                 pSessionEntry - Points to the session on which the Init   */
/*                                 msg is received.                          */
/* Output(s)     : pu1StatType - Error type encountered is stored in this    */
/*                               field.                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpPrcsCmnSsnParamsTlv (UINT1 **ppTlv, tLdpSession * pSessionEntry,
                        UINT1 *pu1StatType)
{
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);
    UINT1               u1ADFlag = 0;
    tLdpEntity         *pLdpEntity = SSN_GET_ENTITY (pSessionEntry);
    tLdpPeer           *pLdpPeer = pSessionEntry->pLdpPeer;
    tLdpCmnSsnParams   *pCmnSsnParams = &(pSessionEntry->CmnSsnParams);
    tLdpCmnSsnParams    CmnSsnPrms;
    tLdpTrapInfo        LdpTrapInfo;

    /* Extract and check the Ldp Version */
    LDP_EXT_2_BYTES ((*ppTlv), CmnSsnPrms.u2ProtocolVer);

    if (CmnSsnPrms.u2ProtocolVer != pLdpEntity->u2ProtVersion)
    {
        LDP_DBG (LDP_SSN_PRCS,
                 "SESSION: Ldp Version Mismatch !. Stop processing\n");
        *pu1StatType = LDP_STAT_TYPE_BAD_PROTO_VER;
        return LDP_FAILURE;
    }
    pCmnSsnParams->u2ProtocolVer = CmnSsnPrms.u2ProtocolVer;

    /* Extract the Keep Alive Time */
    LDP_EXT_2_BYTES ((*ppTlv), CmnSsnPrms.u2KeepAliveTime);

    /* Checking peers KeepAliveTimer, if zero, send notif mesg. */
    if (CmnSsnPrms.u2KeepAliveTime == 0)
    {
        LDP_DBG (LDP_SSN_PRCS, "SESSION: Bad KeepAlive Time is zero !\n");
        *pu1StatType = LDP_STAT_TYPE_BAD_KEEPALIVE_TMR;
        return LDP_FAILURE;
    }

    LDP_PEER_KEEP_ALIVE_HOLD_TIMER (pLdpPeer) = CmnSsnPrms.u2KeepAliveTime;
    pCmnSsnParams->u2KeepAliveTime =
        LDP_MIN ((UINT2) (pLdpEntity->u4KeepAliveHoldTime),
                 CmnSsnPrms.u2KeepAliveTime);
    /* Extract the Advert Mode and Loop Detection */
    LDP_EXT_1_BYTES ((*ppTlv), u1ADFlag);
    LDP_GET_A_BIT (u1ADFlag, CmnSsnPrms.u1LabelAdvertMode);
    if (CmnSsnPrms.u1LabelAdvertMode == LDP_ZERO)
    {
        CmnSsnPrms.u1LabelAdvertMode = LDP_DSTR_UNSOLICIT;
    }
    if (CmnSsnPrms.u1LabelAdvertMode != pLdpEntity->u1LabelDistrType)
    {
        if (LDP_IS_ENTITY_TYPE_ATM (pSessionEntry) == LDP_TRUE)
        {
            pCmnSsnParams->u1LabelAdvertMode = LDP_DSTR_ON_DEMAND;
        }
        else
        {
            pCmnSsnParams->u1LabelAdvertMode = LDP_DSTR_UNSOLICIT;
        }
    }
    else
    {
        pCmnSsnParams->u1LabelAdvertMode = CmnSsnPrms.u1LabelAdvertMode;
    }

    LDP_DBG3 (LDP_SSN_PRCS, "%s: %d Negotiated Label Adv Mode: %u\n", 
             __func__, __LINE__,  pCmnSsnParams->u1LabelAdvertMode);

    if (((gi4MplsSimulateFailure == LDP_SIM_FAILURE_ON_DEMAND_NOT_SUPPORTED) &&
         (pCmnSsnParams->u1LabelAdvertMode == LDP_DSTR_ON_DEMAND)) ||
        ((gi4MplsSimulateFailure == LDP_SIM_FAILURE_UNSOLICIT_NOT_SUPPORTED) &&
         (pCmnSsnParams->u1LabelAdvertMode == LDP_DSTR_UNSOLICIT)))
    {
        *pu1StatType = LDP_STAT_TYPE_ADVRT_MODE_REJECT;
        return LDP_FAILURE;
    }

    LDP_PEER_LBL_DIST_METHOD (pLdpPeer) = CmnSsnPrms.u1LabelAdvertMode;

    if ((pCmnSsnParams->u1LabelAdvertMode == LDP_DSTR_UNSOLICIT) &&
        (LDP_LBL_SPACE_TYPE (SSN_GET_ENTITY (pSessionEntry)) ==
         LDP_PER_PLATFORM))
    {
        SSN_MRGTYPE (pSessionEntry) = LDP_DU_MRG;
    }
    /* Extract the Loop Detection Flag */
    LDP_GET_D_BIT (u1ADFlag, CmnSsnPrms.u1LoopDetect);

    if (CmnSsnPrms.u1LoopDetect == LDP_TRUE)
    {
        if ((LDP_LOOP_DETECT (u2IncarnId) != LDP_LOOP_DETECT_CAPABLE_PV)
            && (LDP_LOOP_DETECT (u2IncarnId) != LDP_LOOP_DETECT_CAPABLE_HCPV))
        {
            LDP_DBG (LDP_SSN_PRCS,
                     "SESSION: Loop Detection Admin State mismatch!\n");
        }
    }
    pCmnSsnParams->u1LoopDetect = CmnSsnPrms.u1LoopDetect;

    /* Exract Path Vector Limit */
    LDP_EXT_1_BYTES ((*ppTlv), CmnSsnPrms.u1PvLimit);
    /* PvLimit must be '0' if Loop Detection is not enabled */
    if ((CmnSsnPrms.u1LoopDetect == LDP_TRUE) && (CmnSsnPrms.u1PvLimit == 0))
    {
        LDP_DBG (LDP_SSN_PRCS,
                 "SESSION: LoopDetection Enabled. PvLimit holds "
                 "value zero !\n");
    }
    pCmnSsnParams->u1PvLimit = CmnSsnPrms.u1PvLimit;
    if (CmnSsnPrms.u1LoopDetect == LDP_TRUE)
    {
        if (CmnSsnPrms.u1PvLimit != pLdpEntity->u1PathVecLimit)
        {
            LdpTrapInfo.u1TrapType = LDP_TRAP_PVL_MISMH;
            LdpTrapInfo.pEntity = pLdpEntity;
            LdpTrapInfo.pSession = pSessionEntry;
            LdpTrapInfo.PeerPvl = CmnSsnPrms.u1PvLimit;
            LdpNotifySnmp (&LdpTrapInfo);
        }
    }

    /* Extract Max Pdu Length */
    LDP_EXT_2_BYTES ((*ppTlv), CmnSsnPrms.u2MaxPduLen);
    /* Value '255' is specified in the ldp draft-05, Section:
     * Initialisation Message */
    if (CmnSsnPrms.u2MaxPduLen <= 255)
    {
        pCmnSsnParams->u2MaxPduLen = LDP_DEF_MAX_PDU_LEN;
    }
    else
    {
        /* In the present case, CmnSsnPrms.u2MaxPduLen is always the
         * minimum(LDP_DEF_MAX_PDU_LEN) */
        pCmnSsnParams->u2MaxPduLen = LDP_MIN (pLdpEntity->u2MaxPduLen,
                                              CmnSsnPrms.u2MaxPduLen);
    }

    if ((gi4MplsSimulateFailure == LDP_SIM_FAILURE_PDU_LENGTH_500) &&
        (pCmnSsnParams->u2MaxPduLen != LDP_PDU_LENGTH_500))
    {
        *pu1StatType = LDP_STAT_TYPE_MAX_PDULEN_REJECT;
        return LDP_FAILURE;
    }

    /* Get Receiver's LDP-ID */
    if (MEMCMP ((*ppTlv), pSessionEntry->SessionId, LDP_MAX_LDPID_LEN) != 0)
    {
        LDP_DBG (LDP_SSN_PRCS,
                 "SESSION: Receiver's LdpId is not the one negotiated "
                 "on Ssn\n");
        *pu1StatType = LDP_STAT_TYPE_SHUT_DOWN;
        return LDP_FAILURE;
    }
    (*ppTlv) += LDP_MAX_LDPID_LEN;    /* Points to next Tlv if any present */

    *pu1StatType = LDP_STAT_TYPE_SUCCESS;
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpPrcsAtmSsnParamsTlv                                    */
/* Description   : Processes the Atm Session Paramters Tlv.                  */
/* Input(s)      : ppTlv - Points to the Common session paramters Tlv         */
/*                 pSessionEntry - Points to the session on which the Init   */
/*                                 msg is received.                          */
/* Output(s)     : pu1StatType - Error type encountered is stored in this    */
/*                               field.                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpPrcsAtmSsnParamsTlv (UINT1 **ppTlv, tLdpSession * pSessionEntry,
                        UINT1 *pu1StatType)
{
    UINT2               u2LblRngCount = 0;
    UINT4               u4MNDResv = 0;
    UINT1               u1VcMrgDirSprtd = 0;
    UINT1               u1LblAllocMode;

    /* No. of label range components */
    UINT1               u1LblCompCount = 0;
    UINT1               u1Count = 0;
    tLdpEntity         *pLdpEntity = SSN_GET_ENTITY (pSessionEntry);
    tTMO_SLL           *pAtmLblRangeList = NULL;
    tTMO_SLL           *pEntityLblRangeList =
        &(pLdpEntity->pLdpAtmParams->AtmLabelRangeList);
    tAtmLdpLblRngEntry *pLblRangeBlk = NULL;
    tAtmLdpLblRngEntry *pLblRangeEntry = NULL;
    tKeyInfoStruct     *pLblRangeInfo = NULL;
    tKeyInfoStruct     *pTmpLblRangeInfo = NULL;
    UINT4               u4MinLabel = 0;
    UINT4               u4MaxLabel = 0;
    UINT1               u1NpLblRgAlloc = LDP_FALSE;

    UINT4               u4InterfaceIndex = PER_PLATFORM_INTERFACE_INDEX;
    UINT4               u4ModuleID = 0;
    /* 
     * Extracting field having Numher of label ranges, Merge support and the
     * Directionality support fromt he ATM Session parameters TLV.
     */
    LDP_EXT_4_BYTES ((*ppTlv), u4MNDResv);

    /* Extract M - Merge capability support by the Peer */
    LDP_GET_M (u4MNDResv, u1VcMrgDirSprtd);
    if (!((LDP_ENTITY_MRG_TYPE (pLdpEntity) == u1VcMrgDirSprtd) ||
          ((LDP_ENTITY_MRG_TYPE (pLdpEntity) == LDP_NO_MRG) &&
           (u1VcMrgDirSprtd == LDP_VC_MRG)) ||
          ((LDP_ENTITY_MRG_TYPE (pLdpEntity) == LDP_VC_MRG) &&
           (u1VcMrgDirSprtd == LDP_NO_MRG)) ||
          (!((((LDP_ENTITY_MRG_TYPE (pLdpEntity) == LDP_VP_MRG) &&
               (u1VcMrgDirSprtd != LDP_VP_MRG))) ||
             ((u1VcMrgDirSprtd == LDP_VP_MRG) &&
              (LDP_ENTITY_MRG_TYPE (pLdpEntity) != LDP_VP_MRG))))))
    {
        /*
         *  When Merge Capability of both the peers are same OR
         *  when either one of them is non-merge capable and other
         *  VC-merge capable, they can inter-operate.
         *  Refer Section 3.5.3 of Ldp Specification.
         */
        *pu1StatType = LDP_STAT_TYPE_LBL_RANGE_REJECT;
        return LDP_FAILURE;
    }
    else
    {
        if (LDP_ENTITY_MRG_TYPE (pLdpEntity) == u1VcMrgDirSprtd)
        {
            SSN_MRGTYPE (pSessionEntry) = u1VcMrgDirSprtd;
        }
        else
        {
            SSN_MRGTYPE (pSessionEntry) = LDP_NO_MRG;
        }
    }
    /* Extract D - VC Directionality support by the peer */
    LDP_GET_D (u4MNDResv, u1VcMrgDirSprtd);

    /* Extract N - no of label range components */
    LDP_GET_N (u4MNDResv, u1LblCompCount);

    /* check being done as whether the peer has indicated atleast one ATM
     * label range for the session. IF no label range(s) had been indicated
     * by the Peer, then failure to be return, with status type indication as
     * Session Rejected, Parameters Label Range - 0x00000013.
     */
    if (u1LblCompCount == LDP_ZERO)
    {
        *pu1StatType = LDP_STAT_TYPE_LBL_RANGE_REJECT;
        return LDP_FAILURE;
    }

    /* Extract the Atm Label Range Components and store in Peer's
     * Atm Label Range List */
    pAtmLblRangeList = &(pSessionEntry->pLdpPeer->AtmLblRangeList);
    for (u1Count = 0; u1Count < u1LblCompCount; ++u1Count)
    {
        /* create node */
        pLblRangeBlk = (tAtmLdpLblRngEntry *)
            MemAllocMemBlk (LDP_ATM_LBL_RNG_POOL_ID);

        if (pLblRangeBlk == NULL)
        {

            LDP_DBG (LDP_SSN_MEM,
                     "SESSION: Couldn't allocate memory for AtmLbl "
                     "Range blk\n");
            /* Since allocation is failure, the Peer's init message is not
             * accepted, this will lead to session being torn down. Hence the
             * status type is indicated as Shutdown. - 0x0000000A.
             */
            *pu1StatType = LDP_STAT_TYPE_SHUT_DOWN;
            pLblRangeBlk =
                (tAtmLdpLblRngEntry *) TMO_SLL_First (pAtmLblRangeList);
            while (pLblRangeBlk != NULL)
            {
                TMO_SLL_Delete (pAtmLblRangeList, &(pLblRangeBlk->NextEntry));
                MemReleaseMemBlock (LDP_ATM_LBL_RNG_POOL_ID,
                                    (UINT1 *) pLblRangeBlk);
                pLblRangeBlk =
                    (tAtmLdpLblRngEntry *) TMO_SLL_First (pAtmLblRangeList);
            }
            return LDP_FAILURE;
        }
        TMO_SLL_Init_Node (&(pLblRangeBlk->NextEntry));

        /* Assign Atm label Ranges */
        LDP_EXT_2_BYTES ((*ppTlv), pLblRangeBlk->u2LBVpi);
        LDP_EXT_2_BYTES ((*ppTlv), pLblRangeBlk->u2LBVci);
        LDP_EXT_2_BYTES ((*ppTlv), pLblRangeBlk->u2UBVpi);
        LDP_EXT_2_BYTES ((*ppTlv), pLblRangeBlk->u2UBVci);

        /* Add node to the Atm label range list */
        TMO_SLL_Add (pAtmLblRangeList, &(pLblRangeBlk->NextEntry));

    }

    /* Check for label range intersection */
    if (LdpCheckAtmLblIntersection (pEntityLblRangeList, pAtmLblRangeList,
                                    pSessionEntry, pu1StatType) != LDP_TRUE)
    {
        return LDP_FAILURE;
    }

    /* Getting the Number of Atm Label Ranges */
    u2LblRngCount =
        (UINT2) TMO_SLL_Count (SSN_GET_ATM_LBLRNG_LIST (pSessionEntry));

    /* Filling up the Atm Label Range information, for Label Resource Allocation
     * purpose. */
    pLblRangeInfo = MemAllocMemBlk (LDP_ATM_LBL_KEY_INFO_POOL_ID);
    if (pLblRangeInfo == NULL)
    {
        LDP_DBG (LDP_SSN_MEM, "SSN: MemAlloc Failed for LabelRangeBlks\n");
        *pu1StatType = LDP_STAT_TYPE_SHUT_DOWN;
        return LDP_FAILURE;
    }

    pTmpLblRangeInfo = pLblRangeInfo;
    TMO_SLL_Scan (SSN_GET_ATM_LBLRNG_LIST (pSessionEntry), pLblRangeEntry,
                  tAtmLdpLblRngEntry *)
    {
        pTmpLblRangeInfo->u4Key1Min = pLblRangeEntry->u2LBVpi;
        pTmpLblRangeInfo->u4Key1Max = pLblRangeEntry->u2UBVpi;
        pTmpLblRangeInfo->u4Key2Min = pLblRangeEntry->u2LBVci;
        pTmpLblRangeInfo->u4Key2Max = pLblRangeEntry->u2UBVci;
        pTmpLblRangeInfo++;

        if (u1NpLblRgAlloc == LDP_FALSE)
        {
            /* Copying Min Val of the label space. */
            u4MinLabel = pLblRangeEntry->u2LBVci;
            u4MinLabel = u4MinLabel << 16;
            u4MinLabel |= pLblRangeEntry->u2LBVpi;

            /* Copying Max value of the Label space. */
            u4MaxLabel = pLblRangeEntry->u2UBVci;
            u4MaxLabel = u4MaxLabel << 16;
            u4MaxLabel |= pLblRangeEntry->u2UBVpi;

            u1NpLblRgAlloc = LDP_TRUE;
        }
    }

    /* Checking whether Odd/Even VCs are to be allocated in case of VC
     * Uni-direction. */
    if (u1VcMrgDirSprtd == LDP_VC_UNI_DIR)
    {
        if (MEMCMP (pLdpEntity->LdpId, pSessionEntry->pLdpPeer->PeerLdpId,
                    LDP_MAX_LDPID_LEN) > 0)
        {
            /* Entity with greater LdpId value should allocate Odd Number VCs */
            u1LblAllocMode = LBL_ALLOC_ODD_NUM;
        }
        else
        {
            /* Entity with Lower LdpId value should allocate Even Number VCs */
            u1LblAllocMode = LBL_ALLOC_EVEN_NUM;
        }
    }
    else
    {
        /* VC Bi-Direction */
        u1LblAllocMode = LBL_ALLOC_BOTH_NUM;
    }

    /* Allocating Label Resources for the Intersected Label Range */
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_PLATFORM)
    {
        u4InterfaceIndex = pLdpEntity->pLdpAtmParams->u4IfIndex;
    }

    u4ModuleID = (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE) ?
        VPLS_LBL_MODULE_ID : LDP_LBL_MODULE_ID;
    if (ldpCreateLabelSpaceGroup
        (u1LblAllocMode, pLblRangeInfo, u2LblRngCount, u4ModuleID,
         u4InterfaceIndex, &pLdpEntity->u2LabelRangeId) == LDP_FAILURE)
    {
        MemReleaseMemBlock (LDP_ATM_LBL_KEY_INFO_POOL_ID,
                            (UINT1 *) pLblRangeInfo);
        *pu1StatType = LDP_STAT_TYPE_NO_LBL_RSRC;
        return LDP_FAILURE;
    }

    /* Vpi Vci values have to be filled in the Session Entry, in case Peer and 
     * Entity are not having the assumed set of Vpi and Vci values ie 
     *     LBVpi = UBVpi = LBVci = 1 and UBVci = 100. */
    MemReleaseMemBlock (LDP_ATM_LBL_KEY_INFO_POOL_ID, (UINT1 *) pLblRangeInfo);
    *pu1StatType = LDP_STAT_TYPE_SUCCESS;
    return LDP_SUCCESS;

}

/*****************************************************************************/
/* Function Name : LdpCheckAtmLblIntersection                                */
/* Description   : Calculate the intersection between one set of Atm label   */
/*                 ranges and the other.                                     */
/* Input(s)      : pEntityLblRangeList - Points to the list of Atm label     */
/*                 ranges supported by an Entity in a Lsr.                   */
/*                 pPeerLblRangeList - Points to the list of Atm Label ranges*/
/*                 supported by the Peer Lsr.                                */
/* Output(s)     : The Entity's Range will be updated with the set of        */
/*                 intersected label range, if proper intersection is        */
/*                 present.                                                  */
/*                 pu1StatType - The Status type filled appropriately if the */
/*                 intersection is a failure.                                */
/* Return(s)     : LDP_TRUE        if intersection present                   */
/*                 or LDP_FALSE    if intersection of ranges is NULL.        */
/*****************************************************************************/

UINT1
LdpCheckAtmLblIntersection (tTMO_SLL * pEntityLblRangeList,
                            tTMO_SLL * pPeerLblRangeList,
                            tLdpSession * pSessionEntry, UINT1 *pu1StatType)
{

    /* Label intersection - logic implemented
     *
     * FOR ( each label range specified by peer )
     * BEGIN
     *    IF ( Peer's Label Range MIN VPI is GREATER THAN MAX VPI or
     *         Peer's Label Range MIN VCI is GREATER THAN MAX VCI )
     *
     *         IF by now any intersection had been determined, and 
     *         memory had been allocated for the determined intersections
     *         they are to be released.
     *
     *         Status type = Session rejected/Parameters Label Range
     *         return LDP_FALSE.
     *    END
     *
     *    Now - Detemine the intersection value.
     *    For ( each label range associated with the entity )
     *    BEGIN
     *        IntersectMIN VPI = MAX ( Entity MIN VPI, Peer MIN VPI)
     *        IntersectMAX VPI = MIN ( Entity MAX VPI, Peer MAX VPI)
     *        IntersectMIN VCI = MAX ( Entity MIN VCI, Peer MIN VCI)
     *        IntersectMAX VCI = MIN ( Entity MAX VCI, Peer MAX VCI)
     *
     *        IF ( IntersectMIN VPI is GREATER THAN IntersectMAX VPI or
     *             IntersectMIN VCI is GREATER THAN IntersectMAX VCI )
     *
     *             CONTINUE, Check with the next label range of the 
     *             entity.
     *        END
     *
     *        VALID intersection found, the Intersected value added
     *        to the list of valid intersected label ranges.
     *
     *        Allocate memory to store the valid intersected label range
     *
     *        IF (memory cannot be allocated )
     *
     *           IF by now any intersection had been determined, and 
     *           memory had been allocated for the determined intersections
     *           they are to be released.
     *
     *           Status type = Shutdown
     *           return LDP_FALSE.
     *        END
     *
     *        Add the intersected value to the list of already
     *        determined valid label intersections.
     *
     *        CONTINUE, Check with the next label range of the entity.
     *   END
     * END
     *       
     * IF  (Atleast one valid intersection exists )
     * BEGIN
     *    return LDP_TRUE;
     * END
     * ELSE IF ( no valid intersection )
     * BEGIN
     *    Status type = Session Rejected /Parameters Label Range
     *    return LDP_FALSE.
     * END
     */

    UINT2               u2IntMinVpi;
    UINT2               u2IntMaxVpi;
    UINT2               u2IntMinVci;
    UINT2               u2IntMaxVci;
    tTMO_SLL           *pIntLblRngList = NULL;
    tAtmLdpLblRngEntry *pPeerLblRngEntry = NULL;
    tAtmLdpLblRngEntry *pEntyLblRngEntry = NULL;
    tAtmLdpLblRngEntry *pIntLblRngEntry = NULL;
    tAtmLdpLblRngEntry *pLblRangeBlk = NULL;

    pIntLblRngList = SSN_GET_ATM_LBLRNG_LIST (pSessionEntry);

    TMO_SLL_Scan (pPeerLblRangeList, pPeerLblRngEntry, tAtmLdpLblRngEntry *)
    {

        if ((pPeerLblRngEntry->u2LBVpi > pPeerLblRngEntry->u2UBVpi) ||
            (pPeerLblRngEntry->u2LBVci > pPeerLblRngEntry->u2UBVci))
        {

            pIntLblRngEntry = (tAtmLdpLblRngEntry *)
                TMO_SLL_First (pIntLblRngList);
            while (pIntLblRngEntry != NULL)
            {
                TMO_SLL_Delete (pIntLblRngList, &(pIntLblRngEntry->NextEntry));
                MemReleaseMemBlock (LDP_ATM_LBL_RNG_POOL_ID,
                                    (UINT1 *) pIntLblRngEntry);
                pIntLblRngEntry = (tAtmLdpLblRngEntry *)
                    TMO_SLL_First (pIntLblRngList);
            }

            *pu1StatType = LDP_STAT_TYPE_LBL_RANGE_REJECT;
            return LDP_FALSE;
        }

        TMO_SLL_Scan (pEntityLblRangeList, pEntyLblRngEntry,
                      tAtmLdpLblRngEntry *)
        {
            u2IntMinVpi = LDP_MAX (pEntyLblRngEntry->u2LBVpi,
                                   pPeerLblRngEntry->u2LBVpi);
            u2IntMaxVpi = LDP_MIN (pEntyLblRngEntry->u2UBVpi,
                                   pPeerLblRngEntry->u2UBVpi);
            u2IntMinVci = LDP_MAX (pEntyLblRngEntry->u2LBVci,
                                   pPeerLblRngEntry->u2LBVci);
            u2IntMaxVci = LDP_MIN (pEntyLblRngEntry->u2UBVci,
                                   pPeerLblRngEntry->u2UBVci);

            if ((u2IntMinVpi > u2IntMaxVpi) || (u2IntMinVci > u2IntMaxVci))
            {
                /* Intersection is not valid hence continue */
                continue;
            }

            /* 
             * Valid intersection updated to the list of valid intersection 
             * label ranges.
             */
            pLblRangeBlk = (tAtmLdpLblRngEntry *)
                MemAllocMemBlk (LDP_ATM_LBL_RNG_POOL_ID);

            if (pLblRangeBlk == NULL)
            {
                LDP_DBG (LDP_SSN_MEM,
                         "SESSION: Can't allocate memory for AtmLbl "
                         "Range block\n");
                /* Since allocation is failure, the Peer's init message 
                 * is not accepted, this will lead to session being torn 
                 * down. Hence the status type is indicated as 
                 * Shutdown. - 0x0000000A.
                 */
                pIntLblRngEntry = (tAtmLdpLblRngEntry *)
                    TMO_SLL_First (pIntLblRngList);
                while (pIntLblRngEntry != NULL)
                {
                    TMO_SLL_Delete (pIntLblRngList,
                                    &(pIntLblRngEntry->NextEntry));
                    MemReleaseMemBlock (LDP_ATM_LBL_RNG_POOL_ID,
                                        (UINT1 *) pIntLblRngEntry);
                    pIntLblRngEntry = (tAtmLdpLblRngEntry *)
                        TMO_SLL_First (pIntLblRngList);
                }

                *pu1StatType = LDP_STAT_TYPE_SHUT_DOWN;
                return LDP_FALSE;
            }

            TMO_SLL_Init_Node (&(pLblRangeBlk->NextEntry));

            pLblRangeBlk->u2LBVpi = u2IntMinVpi;
            pLblRangeBlk->u2UBVpi = u2IntMaxVpi;
            pLblRangeBlk->u2LBVci = u2IntMinVci;
            pLblRangeBlk->u2UBVci = u2IntMaxVci;

            TMO_SLL_Add (pIntLblRngList, &(pLblRangeBlk->NextEntry));

        }
        /* End of scan of label ranges of the Entity */
    }
    /* End of scan of label ranges of the Peer */

    if (TMO_SLL_Count (pIntLblRngList) == LDP_ZERO)
    {
        /* No valid intersected label ranges */
        *pu1StatType = LDP_STAT_TYPE_LBL_RANGE_REJECT;
        return LDP_FALSE;
    }
    else
    {
        return LDP_TRUE;
    }
}

/*****************************************************************************/
/* Function Name : LdpHandleAdjExpiry                                        */
/* Description   : Handles the Adjacency Expiry Event.                       */
/*                 Deletes the Adj from the Adj List. If no further Adj      */
/*                 are present associated to the given Session, then         */
/*                 LdpDeleteLdpSession will be called.                       */
/*                                                                           */
/* Input(s)      : pAdjEntry - Points to the Adjacency that expired          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpHandleAdjExpiry (tLdpAdjacency * pAdjEntry, UINT1 u1StatusCode)
{
    tTMO_SLL           *pAdjList = NULL;
    tLdpSession        *pSessionEntry = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    /* MPLS_IPv6 mod start*/
    tLdpAdjacency      *pTempAdjEntry = NULL;
    UINT4                   u4TempAdjCount =0;
    /* MPLS_IPv6 mod end*/
    pAdjList = ADJ_GET_ADJ_LIST (pAdjEntry);
    pSessionEntry = pAdjEntry->pSession;
    if(pSessionEntry == NULL)
    {
      return LDP_FAILURE; 
    }
    pSessionEntry->u1StatusCode = u1StatusCode;
    /* MPLS_IPv6 mod start*/
    TMO_SLL_Scan (pAdjList, pTempAdjEntry, tLdpAdjacency *)
    {
        if(u4TempAdjCount > 1 )
        {
            break;
        }
        if(pAdjEntry->u1AddrType == pTempAdjEntry->u1AddrType)
        {
            u4TempAdjCount ++;
        }
    }
    if(LDP_ADDR_TYPE_IPV6 == pAdjEntry->u1AddrType)
    {
        LDP_DBG1(LDP_SSN_PRCS,
			"AdjExp: Expired IPV6 Adjacency with Address: %s\n", Ip6PrintAddr(&(pAdjEntry->PeerIpAddr.Ip6Addr)));
    }
    else
    {
        LDP_DBG4(LDP_SSN_PRCS,
			"AdjExp: Expired IPv4 Adjacency with Address %d:%d:%d:%d\n", pAdjEntry->PeerIpAddr.au1Ipv4Addr[0],
			pAdjEntry->PeerIpAddr.au1Ipv4Addr[1],	pAdjEntry->PeerIpAddr.au1Ipv4Addr[2],
			pAdjEntry->PeerIpAddr.au1Ipv4Addr[3]);
    }
    /* Deleting Session, if this is the only adjacency present for 
     * this session */
    if (u4TempAdjCount == 0)
    {
        LDP_DBG1 (LDP_SSN_PRCS,
                 "AdjExp: All Adj(s) has already expired of the same ADD FAM type: Adj Count is %x \n", u4TempAdjCount);
        return (LDP_SUCCESS);
    }
    else if(u4TempAdjCount == 1)
    {
        LDP_DBG (LDP_SSN_PRCS,
                 "SESSION: - Expired Adjacency was alone of it's Address Family Type \n");
        pLdpEntity = SSN_GET_ENTITY (pSessionEntry);

#ifdef LDP_GR_WANTED
        /* If the peer node can accept the Help from Helper Node (configured as FULL),
         * then only change the progress state to LDP_PEER_GR_RECONNECT_IN_PROGRESS
         * and delete only the LDP session for targetted as weel as Link LDP 
         * otherwise delete the LDP Peer for Link LDP and Delete only LDP session
         * for T-LDP */
        if ((gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrCapability !=
                         LDP_GR_CAPABILITY_NONE) &&
                        (pSessionEntry->pLdpPeer->u2GrReconnectTime != LDP_ZERO))
        {

            /* This section handles the LDP Shutdown on the peer node */
            if (pSessionEntry->pLdpPeer->u1GrProgressState ==
                              LDP_PEER_GR_RECONNECT_IN_PROGRESS)
            {
                 LdpDeleteLdpSession (pSessionEntry, pSessionEntry->u1StatusCode);
                 return LDP_SUCCESS;    
            }
              
            /* The Node should start the helper process only if the adjacency
             * expires via the timer event. Forceful call of the function should
             * not trigger GR on helper. Forecful call of this function is done
             * when the entity is made shut on the Peer node and it is triggered
             * from LdpHandleNotifMsg when it receives the entity shut notification
             * from the peer. On module shutdown on the peer, a notification msg is
             * sent LDP_STAT_TEMPORARY_SHUTDOWN, where the Peer GR progress state
             * is already set to LDP_PEER_GR_RECONNECT_IN_PROGRESS */
            if (LDP_STAT_TYPE_HOLD_TMR_EXPRD == u1StatusCode)
            {  
                pSessionEntry->pLdpPeer->u1GrProgressState =
                                             LDP_PEER_GR_RECONNECT_IN_PROGRESS;
                LdpDeleteLdpSession (pSessionEntry, pSessionEntry->u1StatusCode);
                return LDP_SUCCESS;
            }
        }
#endif
            if(pSessionEntry->SrcTransAddr.u2AddrType == pAdjEntry->u1AddrType)
            {
                LDP_DBG (LDP_SSN_PRCS,
                     "AdjExp: Deleting the Session - Session Addr Type and Addr Type of Last Expired Adj is same\n");
                pLdpEntity = SSN_GET_ENTITY (pSessionEntry);
                if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_FALSE)
                {
                    LDP_DBG (LDP_SSN_PRCS,
                     "AdjExp: Basic Entity Deleting LDP PEER\n");
                    LdpDeleteLdpPeer (pSessionEntry->pLdpPeer);
                }
                else
                {
                    LDP_DBG (LDP_SSN_PRCS,
                            "AdjExp: Targeted Entity Deleting LDP Session\n");
                    LdpDeleteLdpSession (pSessionEntry, pSessionEntry->u1StatusCode);

					if (pSessionEntry->pLdpPeer->u1GrProgressState !=
									LDP_PEER_GR_RECONNECT_IN_PROGRESS)
					{
							MEMSET (pSessionEntry->pLdpPeer->PeerLdpId,
											LDP_ZERO, LDP_MAX_LDPID_LEN);

							/* If after the Restart of the Peer, who was in Helper mode before
							 * restart becomes GR incapable, It wont send the FT TLV after restart.
							 * Hence we should initialize the u1GrProgressState from 
							 * GR_NOT_STARTED to GR_NOT_SUPPORTED. If the GR capability of the  
							 * restarted does not change after the restart, u1GrProgressState
							 * will be updated appropriately in the function LdpPrcsFTSsnTlv */

							pSessionEntry->pLdpPeer->u1GrProgressState = LDP_PEER_GR_NOT_SUPPORTED;
					}
				}
            }
	     else
            {
                /* Deleting the expired Adj from the Adj list */
                LDP_DBG (LDP_SSN_PRCS, "SESSION: Deleting the Expired Adjacency Last Adjacency of it's Address Family\n");
                /* Stop the Adj Expiry Timer */
                TmrStopTimer (LDP_TIMER_LIST_ID,
                          (tTmrAppTimer *) & (pAdjEntry->PeerAdjTimer.AppTimer));
                TMO_SLL_Delete (pAdjList, &(pAdjEntry->NextAdjacency));
                MemReleaseMemBlock (LDP_ADJ_POOL_ID, (UINT1 *) pAdjEntry);
            }
    }
    else
    {
        /* Deleting the expired Adj from the Adj list */
        LDP_DBG (LDP_SSN_PRCS, "SESSION: Deleting the Expired Adjacency\n");
        /* Stop the Adj Expiry Timer */
        TmrStopTimer (LDP_TIMER_LIST_ID,
                      (tTmrAppTimer *) & (pAdjEntry->PeerAdjTimer.AppTimer));

        TMO_SLL_Delete (pAdjList, &(pAdjEntry->NextAdjacency));
        MemReleaseMemBlock (LDP_ADJ_POOL_ID, (UINT1 *) pAdjEntry);
    }
    return (LDP_SUCCESS);
}

/****************************************************************************/
/* Function Name : ldpUpdateTcpConnTbl                                      */
/* Description   : Adds the Tcp Connection associated with this session     */
/*                 to the Tcp Connection hash Table.                        */
/* Input(s)      : pLdpSession - Points to the Session                      */
/* Output(s)     : None                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                  */
/****************************************************************************/

VOID
LdpAddSsnToTcpConnTbl (tLdpSession * pLdpSession)
{
    UINT4               u4HIndex;
    UINT2               u2IncarnId;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;

    u2IncarnId = SSN_GET_INCRN_ID (pLdpSession);
    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);
    u4HIndex = LDP_COMPUTE_HASH_INDEX (pLdpSession->u4TcpConnId);

    TMO_HASH_Add_Node (pTcpConnHshTbl, (tTMO_HASH_NODE *)
                       & (pLdpSession->NextHashNode), u4HIndex, NULL);
}

/****************************************************************************/
/* Function Name : LdpDelSsnFromTcpConnTbl                                  */
/* Description   : Delete the Tcp Connection associated with this session   */
/*                 from the Tcp Connection hash Table.                      */
/* Input(s)      : pLdpSession - Points to the Session                      */
/* Output(s)     : None                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                  */
/****************************************************************************/

VOID
LdpDelSsnFromTcpConnTbl (tLdpSession * pLdpSession)
{
    UINT4               u4HIndex;
    UINT2               u2IncarnId;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;

    u2IncarnId = SSN_GET_INCRN_ID (pLdpSession);
    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);
    u4HIndex = LDP_COMPUTE_HASH_INDEX (pLdpSession->u4TcpConnId);

    TMO_HASH_Delete_Node (pTcpConnHshTbl, (tTMO_HASH_NODE *)
                          & (pLdpSession->NextHashNode), u4HIndex);
}

/***************************************************************************/
/* Function Name : LdpGetSsnFromTcpConnTbl                                  */
/* Description   : Gets the Session Entry of this Tcp Connection. Returns   */
/*               NULL if no session is found with the corresponding ConnId. */
/* Input(s)      : u4TcpConnId - Tcp Connection Id                          */
/*                 u2IncarnId  - Incarnation                                */
/* Output(s)     : None                                                     */
/* Return(s)     : pLdpSession - Pointer to the Session Entry.              */
/****************************************************************************/

tLdpSession        *
LdpGetSsnFromTcpConnTbl (UINT2 u2IncarnId, UINT4 u4TcpConnId)
{
    UINT4               u4HIndex;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tLdpSession        *pLdpSession = NULL;
    tTMO_HASH_NODE     *pHashNode = NULL;

    u4HIndex = LDP_COMPUTE_HASH_INDEX (u4TcpConnId);
    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (u2IncarnId);

    TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4HIndex, pHashNode, tTMO_HASH_NODE *)
    {
        pLdpSession = LDP_SSN_OFFSET_HTBL (pHashNode);

        if (pLdpSession->u4TcpConnId == u4TcpConnId)
        {
            return pLdpSession;
        }
    }
    return NULL;
}

/**************************************************************************/
/* Function Name : LdpDelPeerIfAdrInfo                                    */
/* Description   : Deletes the Peer's If Address Info received on this    */
/*                 session through Address message.                       */
/* Input(s)      : pSessionEntry - Session                                */
/* Output(s)     : None                                                   */
/* Return(s)     : None                                                   */
/**************************************************************************/

VOID
LdpDelPeerIfAdrInfo (tLdpSession * pSessionEntry)
{
    tTMO_SLL           *pPeerIfList = NULL;
    tPeerIfAdrNode     *pIfAddrNode = NULL;
    tPeerIfAdrNode     *pTempSllNode = NULL;
    UINT4               u4HIndex;
    UINT4               u4Address = 0;
    UINT2               u2IncarnId;
#ifdef MPLS_IPV6_WANTED
    UINT1               u1Ipv6HIndex=0;
#endif
    if ((pSessionEntry->pLdpPeer == NULL) ||
        (pSessionEntry->pLdpPeer->pLdpEntity == NULL))
    {
        return;
    }

    u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);

    pPeerIfList = SSN_GET_PEER_IFADR_LIST (pSessionEntry);
    if (pPeerIfList == NULL)
    {
        return;
    }

    TMO_DYN_SLL_Scan (pPeerIfList, pIfAddrNode, pTempSllNode, tPeerIfAdrNode *)
    {
        pIfAddrNode = LDP_OFFSET (pIfAddrNode);

#if MPLS_IPV6_WANTED
	if(pIfAddrNode->AddrType==IPV6)
	{
		/* Deleting the IfAdr Info from the HashTable maintained at Incarn */
		u1Ipv6HIndex =  Ip6AddrHash (&pIfAddrNode->IfAddr.Ip6Addr);
		TMO_HASH_Delete_Node (LDP_PEER_IPV6_IFADR_TABLE (u2IncarnId),
				&(pIfAddrNode->NextIpv6HashNode), u1Ipv6HIndex);
	}
	else
#endif
	{
		CONVERT_TO_INTEGER (pIfAddrNode->IfAddr.au1Ipv4Addr, u4Address);
		/* Deleting the IfAdr Info from the HashTable maintained at Incarn */
		u4HIndex = LDP_COMPUTE_HASH_INDEX (u4Address);
		TMO_HASH_Delete_Node (LDP_PEER_IFADR_TABLE (u2IncarnId),
				&(pIfAddrNode->NextHashNode), u4HIndex);
	}

	/* Deleting the IfAdr Info from the SLL maintained at Peer Level */
	TMO_SLL_Delete (pPeerIfList, &(pIfAddrNode->NextSllNode));

        MemReleaseMemBlock (LDP_PIF_POOL_ID, (UINT1 *) pIfAddrNode);
    }

    return;
}

/**************************************************************************/
/* Function Name : LdpNotifySnmp                                          */
/* Description   : This function should be ported by the customer to      */
/*                 to send a trap to the SNMP Manager                     */
/* Input(s)      : pSessionEntry - Session                                */
/* Output(s)     : None                                                   */
/* Return(s)     : None                                                   */
/**************************************************************************/

VOID
LdpNotifySnmp (tLdpTrapInfo * pLdpTrapInfo)
{
    switch (pLdpTrapInfo->u1TrapType)
    {
        case LDP_TRAP_SESS_DN:
            LDP_DBG (LDP_NOTIF_PRCS, "Session Down\n");
            MplsSessionNotification (pLdpTrapInfo->pSession,
                                     pLdpTrapInfo->u1TrapType);
            break;
        case LDP_TRAP_SESS_UP:
            LDP_DBG (LDP_NOTIF_PRCS, "Session Up\n");
            MplsSessionNotification (pLdpTrapInfo->pSession,
                                     pLdpTrapInfo->u1TrapType);
            break;
        case LDP_TRAP_PVL_MISMH:
            MplsLdpPVLimitNotification (pLdpTrapInfo->pSession->pLdpPeer);
            break;
        case LDP_TRAP_SESS_THOLD_EXCD:
            MplsLdpEntityInitSsnThresholdNotify (pLdpTrapInfo->pEntity);
            break;
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpSsnStatusChngNotif                                     */
/* Description   : This function send notifications to l2vpn module          */
/* Input(s)      : pLdpSession - Pointer to the session in which the message */
/*                               is received                                 */
/*                 u1NotifType - type of Notification (Session Up/Down)      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
/* ldppwsnd.c */
VOID
LdpSsnStatusChngNotif (tLdpSession * pSessionEntry, UINT4 u4NotifType)
{

    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT4               u4RegIndex;

    for (u4RegIndex = 0; u4RegIndex < MAX_LDP_APPLICATIONS; u4RegIndex++)
    {
        /* Application using this session needs to be notified */
        /* TODO associate application id with the mask flag */

        if (LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).u1RegFlag
            == LDP_REGISTER)
        {
            if (LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).
                pCallBackFnSsnStatusChng != NULL)
            {
                LDP_REGISTRATION_TABLE (u2IncarnId, u4RegIndex).
                    pCallBackFnSsnStatusChng (pSessionEntry, u4NotifType);
            }
        }
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpPrcsFTSsnTlv                                           */
/* Description   : Processes the Fault Tolerance Session tlv.                */
/* Input(s)      : ppTlv - Points to the Fault Tolerance Session Tlv         */
/*                 pSessionEntry - Points to the session on which the Init   */
/*                                 msg is received.                          */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpPrcsFTSsnTlv (UINT1 **ppTlv, tLdpSession * pSessionEntry, UINT2 u2TmpLen)
{

    tFTSessionTlv       FTSessionTlv;
    tLdpPeer           *pLdpPeer = NULL;
    UINT2               u2IncarnId = SSN_GET_INCRN_ID (pSessionEntry);

    LDP_DBG (LDP_SSN_PRCS,
             "LdpPrcsFTSsnTlv: FT Session TLV in the Init Msg.\n");

    MEMSET (&FTSessionTlv, LDP_ZERO, sizeof (tFTSessionTlv));

    if (gLdpInfo.LdpIncarn[u2IncarnId].u1GrCapability == LDP_GR_CAPABILITY_NONE)
    {
        /* No need to Process the TLV, if the receiver has no 
         * GR Capability
         */
        LDP_DBG (LDP_SSN_PRCS,
                 "LdpPrcsFTSsnTlv: Configured as not GR Capable. "
                 "So Ignore the FT session TLV in Init Msg.\n");
        (*ppTlv) += u2TmpLen;
        return;
    }

    pLdpPeer = pSessionEntry->pLdpPeer;
    LDP_EXT_2_BYTES ((*ppTlv), FTSessionTlv.u2FTFlags);
    LDP_EXT_2_BYTES ((*ppTlv), FTSessionTlv.u2Rsvd);

    if (FTSessionTlv.u2FTFlags == LDP_ONE)
    {
        LDP_EXT_4_BYTES ((*ppTlv), FTSessionTlv.u4FTReconnectTimeout);
        LDP_EXT_4_BYTES ((*ppTlv), FTSessionTlv.u4FTRecoverTime);
        pLdpPeer->u2GrReconnectTime = (UINT2)
            (LDP_CONVERT_TO_SECONDS (FTSessionTlv.u4FTReconnectTimeout));
        pLdpPeer->u2GrRecoveryTime = (UINT2)
            (LDP_CONVERT_TO_SECONDS (FTSessionTlv.u4FTRecoverTime));

        if ((gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus !=
             LDP_GR_RECONNECT_IN_PROGRESS) &&
            (pLdpPeer->u1GrProgressState == LDP_PEER_GR_RECOVERY_IN_PROGRESS))
        {
            /* If Restarting has transmitted u2RecoveryTime as 0, then it
             * means that peer has lost its forwarding states.
             * And we should also remove the Stale Entries
             * */

            if (pLdpPeer->u2GrRecoveryTime == LDP_ZERO)
            {
#ifdef LDP_GR_WANTED
                LdpGrDeleteStaleCtrlBlockEntries (pLdpPeer);
#endif
                LdpSendDeleteStaleNotification
                    (pLdpPeer, L2VPN_LDP_PWVC_GR_DELETE_STALE_ENTRIES);
                pLdpPeer->u1GrProgressState = LDP_PEER_GR_ABORTED;

            }
        }
        else if (pLdpPeer->u1GrProgressState == LDP_PEER_GR_NOT_SUPPORTED)
        {
            pLdpPeer->u1GrProgressState = LDP_PEER_GR_NOT_STARTED;
        }

#ifdef LDP_GR_WANTED
        /* For the first adjacency the variable would be set to
           LDP_GR_RECONNECT_IN_PROGRESS and for subsequent adjacencies
           the variable would be LDP_GR_RECOVERY_IN_PROGRESS. Once th
           the FWD hold timer expires this would be set to LDP_GR_COMPLETED */

        /* This is for the restarting node */
        if (((gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus ==
             LDP_GR_RECONNECT_IN_PROGRESS) ||
             (gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus ==
             LDP_GR_RECOVERY_IN_PROGRESS)) &&
             (pLdpPeer->u1GrProgressState != LDP_PEER_GR_NOT_SUPPORTED))
        {
            pLdpPeer->u1GrProgressState =
                LDP_PEER_GR_RECOVERY_IN_PROGRESS;

            LDP_DBG4 (GRACEFUL_DEBUG, "LdpPrcsFTSsnTlv: The Ldp Peer %d.%d.%d.%d "
                      "is set to GR Recovery in Progress State\n",
                      pLdpPeer->NetAddr.Addr.au1Ipv4Addr[0],
                      pLdpPeer->NetAddr.Addr.au1Ipv4Addr[1],
                      pLdpPeer->NetAddr.Addr.au1Ipv4Addr[2],
                      pLdpPeer->NetAddr.Addr.au1Ipv4Addr[3]);
        }
#endif
    }
    else
    {
        (*ppTlv) += (u2TmpLen - 4);
    }

    return;
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpssm.c                                */
/*---------------------------------------------------------------------------*/
