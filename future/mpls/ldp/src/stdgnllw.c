/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgnllw.c,v 1.33 2015/11/04 09:38:55 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdgnllw.h"
# include  "stdldplw.h"
# include  "ldpincs.h"
# include  "ldplwinc.h"

/* LOW LEVEL Routines for Table : MplsLdpEntityGenericLRTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpEntityGenericLRTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpEntityGenericLRTable (tSNMP_OCTET_STRING_TYPE *
                                                     pMplsLdpEntityLdpId,
                                                     UINT4 u4MplsLdpEntityIndex,
                                                     UINT4
                                                     u4MplsLdpEntityGenericLRMin,
                                                     UINT4
                                                     u4MplsLdpEntityGenericLRMax)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (nmhValidateIndexInstanceMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                                    u4MplsLdpEntityIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (u4MplsLdpEntityGenericLRMin > u4MplsLdpEntityGenericLRMax)
    {
        return SNMP_FAILURE;
    }
    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         (u4MplsLdpEntityIndex), &pLdpEntity) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) != LDP_TRUE) &&
        (LDP_LBL_SPACE_TYPE (pLdpEntity) == LDP_PER_PLATFORM))
    {
        if (((u4MplsLdpEntityGenericLRMin) <
             gSystemSize.MplsSystemSize.u4MinLdpLblRange) ||
            ((u4MplsLdpEntityGenericLRMin) >
             gSystemSize.MplsSystemSize.u4MaxLdpLblRange) ||
            ((u4MplsLdpEntityGenericLRMax) >
             gSystemSize.MplsSystemSize.u4MaxLdpLblRange) ||
            ((u4MplsLdpEntityGenericLRMax) <
             gSystemSize.MplsSystemSize.u4MinLdpLblRange))
        {
            return SNMP_FAILURE;
        }
    }
    else if ((LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE) &&
             (LDP_LBL_SPACE_TYPE (pLdpEntity) == LDP_PER_PLATFORM))
    {
        if (((u4MplsLdpEntityGenericLRMin) <
             gSystemSize.MplsSystemSize.u4MinL2VpnLblRange) ||
            ((u4MplsLdpEntityGenericLRMin) >
             gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange) ||
            ((u4MplsLdpEntityGenericLRMax) >
             gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange) ||
            ((u4MplsLdpEntityGenericLRMax) <
             gSystemSize.MplsSystemSize.u4MinL2VpnLblRange))
        {
            return SNMP_FAILURE;
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpEntityGenericLRTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpEntityGenericLRTable (tSNMP_OCTET_STRING_TYPE *
                                             pMplsLdpEntityLdpId,
                                             UINT4 *pu4MplsLdpEntityIndex,
                                             UINT4
                                             *pu4MplsLdpEntityGenericLRMin,
                                             UINT4
                                             *pu4MplsLdpEntityGenericLRMax)
{
    UINT4               u4IncarnId;
    UINT1               u1FirstLoopEntry = LDP_TRUE;
    UINT1               u1FirstInnerLoopEntry = LDP_TRUE;
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEntity         *pTempLdpEntity = NULL;
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    *pu4MplsLdpEntityIndex = LDP_ZERO;
    *pu4MplsLdpEntityGenericLRMin = LDP_ZERO;
    *pu4MplsLdpEntityGenericLRMax = LDP_ZERO;

    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }
    for (u4IncarnId = 0; u4IncarnId < LDP_MAX_INCARN; ++u4IncarnId)
    {
        pList = &LDP_ENTITY_LIST (u4IncarnId);

        pTempLdpEntity = (tLdpEntity *) TMO_SLL_First (pList);

        if (pTempLdpEntity == NULL)
        {
            continue;
        }

        TMO_SLL_Scan (pList, pTempLdpEntity, tLdpEntity *)
        {
            /* Only PER PLATFORM LDP Entities will be fetched */
            if (LDP_LBL_SPACE_TYPE (pTempLdpEntity) != LDP_PER_PLATFORM)
            {
                continue;
            }

            /* Only LDP Entities that is associated with a MPLS Interface
             * will be fetched */
            if (TMO_SLL_Count (LDP_ENTITY_ETH_LBL_RNGE_LIST (pTempLdpEntity))
                == LDP_ZERO)
            {
                continue;
            }

            /* Fetching the First 'valid' First LDP Entity */
            if (u1FirstLoopEntry == LDP_TRUE)
            {
                CPY_TO_SNMP (pMplsLdpEntityLdpId, pTempLdpEntity->LdpId,
                             LDP_MAX_LDPID_LEN);
                *pu4MplsLdpEntityIndex = (pTempLdpEntity->u4EntityIndex);
                u1FirstLoopEntry = LDP_FALSE;
                pLdpEntity = pTempLdpEntity;
                continue;
            }

            /* Check whether Fetched Entity has a lower or equal LDP Entity ID 
             * value than the previous 'valid' First LDP Entity */
            if (MEMCMP
                (pTempLdpEntity->LdpId, pu1LdpEntityId, LDP_MAX_LDPID_LEN) > 0)
            {
                continue;
            }

            /* Check whether Fetched Entity has a lower LDP Entity Index value
             * than the previous 'valid' First LDP Entity */
            if ((MEMCMP (pTempLdpEntity->LdpId, pu1LdpEntityId,
                         LDP_MAX_LDPID_LEN) == 0) &&
                (pTempLdpEntity->u4EntityIndex > *pu4MplsLdpEntityIndex))
            {
                continue;
            }

            /* Next Possible 'valid' First LDP Entity is found */
            CPY_TO_SNMP (pMplsLdpEntityLdpId, pTempLdpEntity->LdpId,
                         LDP_MAX_LDPID_LEN);
            *pu4MplsLdpEntityIndex = (pTempLdpEntity->u4EntityIndex);
            pLdpEntity = pTempLdpEntity;
        }
    }

    if (pLdpEntity == NULL)
    {
        return SNMP_FAILURE;
    }

    /* With the 'valid' First LDP Entity, fetch the 'valid' First Ethernet 
     * Parameters associated */
    TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                  pLdpEthParams, tLdpEthParams *)
    {
        if (u1FirstInnerLoopEntry == LDP_TRUE)
        {
            *pu4MplsLdpEntityGenericLRMin = pLdpEthParams->u4MinLabelVal;
            *pu4MplsLdpEntityGenericLRMax = pLdpEthParams->u4MaxLabelVal;
            u1FirstInnerLoopEntry = LDP_FALSE;
            continue;
        }

        if (pLdpEthParams->u4MinLabelVal < *pu4MplsLdpEntityGenericLRMin)
        {
            *pu4MplsLdpEntityGenericLRMin = pLdpEthParams->u4MinLabelVal;
            *pu4MplsLdpEntityGenericLRMax = pLdpEthParams->u4MaxLabelVal;
        }
    }
    if (u1FirstInnerLoopEntry == LDP_FALSE)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpEntityGenericLRTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                nextMplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax
                nextMplsLdpEntityGenericLRMax
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpEntityGenericLRTable (tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pNextMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            UINT4 *pu4NextMplsLdpEntityIndex,
                                            UINT4 u4MplsLdpEntityGenericLRMin,
                                            UINT4
                                            *pu4NextMplsLdpEntityGenericLRMin,
                                            UINT4 u4MplsLdpEntityGenericLRMax,
                                            UINT4
                                            *pu4NextMplsLdpEntityGenericLRMax)
{
    UINT4               u4IncarnId = MPLS_DEF_INCARN;
    UINT1               u1FirstLoopEntry = LDP_TRUE;
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEntity         *pTempLdpEntity = NULL;
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1NextLdpEntityId = NULL;

    *pu4NextMplsLdpEntityIndex = LDP_ZERO;
    *pu4NextMplsLdpEntityGenericLRMin = LDP_ZERO;
    *pu4NextMplsLdpEntityGenericLRMax = LDP_ZERO;

    /* Added to suppress warning */
    UNUSED_PARAM (u4MplsLdpEntityGenericLRMax);

    if (LDP_INITIALISED != TRUE)
    {
        return SNMP_FAILURE;
    }

    if (pMplsLdpEntityLdpId->i4_Length < 0)
    {
        return SNMP_FAILURE;
    }

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1NextLdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);

    /* Get the LDP Entity associated with the Given LDP Entity ID and
     * LDP Entity Index value */
    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pList = &LDP_ENTITY_LIST (u4IncarnId);
    /* Fetch Next Maximum Ethernet Parameters associated with 
     * the Given LDP Entity */
    TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                  pLdpEthParams, tLdpEthParams *)
    {
        /* Condition to Fetch the Next Maximum Ethernet Parameters associated
         * with the LDP Entity */
        if ((pLdpEthParams->u4MinLabelVal <= u4MplsLdpEntityGenericLRMin))
        {
            continue;
        }
        if (u1FirstLoopEntry == LDP_TRUE)
        {
            *pu4NextMplsLdpEntityGenericLRMin = pLdpEthParams->u4MinLabelVal;
            *pu4NextMplsLdpEntityGenericLRMax = pLdpEthParams->u4MaxLabelVal;
            u1FirstLoopEntry = LDP_FALSE;
            continue;
        }
        if ((pLdpEthParams->u4MinLabelVal < *pu4NextMplsLdpEntityGenericLRMin))
        {
            *pu4NextMplsLdpEntityGenericLRMin = pLdpEthParams->u4MinLabelVal;
            *pu4NextMplsLdpEntityGenericLRMax = pLdpEthParams->u4MaxLabelVal;
        }
    }
    if (u1FirstLoopEntry == LDP_FALSE)
    {
        /* Next Maximum Ethernet Parameters associated with the given LDP 
         * Entity is found. So, Returning Success */
        CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pu1LdpEntityId,
                     LDP_MAX_LDPID_LEN);
        *pu4NextMplsLdpEntityIndex = u4MplsLdpEntityIndex;
        return SNMP_SUCCESS;
    }

    /* The LDP Entity Passed as an Input does not have any more 
     * Ethernet Parameters associated with it. So, trying to fetch the
     * Next Maximum Possible Valid LDP Entity with ethernet parameters 
     * associated with it */
    TMO_SLL_Scan (pList, pTempLdpEntity, tLdpEntity *)
    {
        /* Only PER PLATFORM LDP Entities will be fetched */
        if (LDP_LBL_SPACE_TYPE (pTempLdpEntity) != LDP_PER_PLATFORM)
        {
            continue;
        }

        /* Only LDP Entities that is associated with a MPLS Interface
         * will be fetched */
        if (TMO_SLL_Count (LDP_ENTITY_ETH_LBL_RNGE_LIST (pTempLdpEntity))
            == LDP_ZERO)
        {
            continue;
        }

        /* Fetched LDP Entity should have LDP Entity ID Value equal or
         * greater than the LDP Entity Passed as Input to this routine */
        if (MEMCMP
            (pTempLdpEntity->LdpId, pu1LdpEntityId, LDP_MAX_LDPID_LEN) < 0)
        {
            continue;
        }

        /* Fetched LDP Entity should have LDP Entity Index Value 
         * greater than the LDP Entity Passed as Input to this routine */
        if ((MEMCMP (pTempLdpEntity->LdpId, pu1LdpEntityId,
                     LDP_MAX_LDPID_LEN) == 0) &&
            (pTempLdpEntity->u4EntityIndex <= u4MplsLdpEntityIndex))
        {
            continue;
        }

        if (u1FirstLoopEntry == LDP_TRUE)
        {
            /* Fetching the First 'valid' First LDP Entity */
            CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pTempLdpEntity->LdpId,
                         LDP_MAX_LDPID_LEN);
            *pu4NextMplsLdpEntityIndex = pTempLdpEntity->u4EntityIndex;
            u1FirstLoopEntry = LDP_FALSE;
            pLdpEntity = pTempLdpEntity;
            continue;
        }

        /* Fetched LDP Entity should have LDP Entity ID Value equal or 
         * lesser than the 'valid' First LDP Entity */
        if (MEMCMP (pTempLdpEntity->LdpId, pu1NextLdpEntityId,
                    LDP_MAX_LDPID_LEN) > 0)
        {
            continue;
        }

        /* Fetched LDP Entity should have LDP Entity Index Value 
         * lesser than the 'valid' First LDP Entity */
        if ((MEMCMP (pTempLdpEntity->LdpId, pu1NextLdpEntityId,
                     LDP_MAX_LDPID_LEN) == 0)
            && (pTempLdpEntity->u4EntityIndex > *pu4NextMplsLdpEntityIndex))
        {
            continue;
        }

        /* The Next Possible 'valid' Next LDP Entity is found */
        CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pTempLdpEntity->LdpId,
                     LDP_MAX_LDPID_LEN);
        *pu4NextMplsLdpEntityIndex = pTempLdpEntity->u4EntityIndex;
        pLdpEntity = pTempLdpEntity;
    }

    if (u1FirstLoopEntry == LDP_TRUE)
    {
        return SNMP_FAILURE;
    }

    u1FirstLoopEntry = LDP_TRUE;
    /* With the 'valid' Next LDP Entity, fetch the 'valid' First Ethernet 
     * Parameters associated */
    TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                  pLdpEthParams, tLdpEthParams *)
    {
        if (u1FirstLoopEntry == LDP_TRUE)
        {
            *pu4NextMplsLdpEntityGenericLRMin = pLdpEthParams->u4MinLabelVal;
            *pu4NextMplsLdpEntityGenericLRMax = pLdpEthParams->u4MaxLabelVal;
            u1FirstLoopEntry = LDP_FALSE;
            continue;
        }
        if ((pLdpEthParams->u4MinLabelVal < *pu4NextMplsLdpEntityGenericLRMin))
        {
            *pu4NextMplsLdpEntityGenericLRMin = pLdpEthParams->u4MinLabelVal;
            *pu4NextMplsLdpEntityGenericLRMax = pLdpEthParams->u4MaxLabelVal;
        }
    }

    if (u1FirstLoopEntry == LDP_FALSE)
    {
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityGenericLabelSpace
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                retValMplsLdpEntityGenericLabelSpace
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityGenericLabelSpace (tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 u4MplsLdpEntityIndex,
                                      UINT4 u4MplsLdpEntityGenericLRMin,
                                      UINT4 u4MplsLdpEntityGenericLRMax,
                                      INT4
                                      *pi4RetValMplsLdpEntityGenericLabelSpace)
{
    tLdpEthParams      *pLdpEthParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rLabelSpaceGet: Entity %d do not exist\n",
                  u4MplsLdpEntityIndex);
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                  u4MplsLdpEntityGenericLRMin,
                                  u4MplsLdpEntityGenericLRMax, &pLdpEthParams)
        == LDP_FAILURE)
    {
        LDP_DBG3 (LDP_IF_SNMP,
                  "\rLabelSpaceGet: Generic LR %d %d %d do not exist\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityGenericLabelSpace =
        (INT4) (pLdpEthParams->u1LabelSpace);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityGenericIfIndexOrZero
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                retValMplsLdpEntityGenericIfIndexOrZero
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityGenericIfIndexOrZero (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         UINT4 u4MplsLdpEntityGenericLRMin,
                                         UINT4 u4MplsLdpEntityGenericLRMax,
                                         INT4
                                         *pi4RetValMplsLdpEntityGenericIfIndexOrZero)
{
    tLdpEthParams      *pLdpEthParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rGenericIfIndexGet: Entity %d do not exist\n",
                  u4MplsLdpEntityIndex);
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                  u4MplsLdpEntityGenericLRMin,
                                  u4MplsLdpEntityGenericLRMax, &pLdpEthParams)
        == LDP_FAILURE)
    {
        LDP_DBG3 (LDP_IF_SNMP,
                  "\rGenericIfIndexGet: Generic LR %d %d %d do not exist\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityGenericIfIndexOrZero =
        (INT4) (pLdpEthParams->u4MplsIfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityGenericLRStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                retValMplsLdpEntityGenericLRStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityGenericLRStorageType (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         UINT4 u4MplsLdpEntityGenericLRMin,
                                         UINT4 u4MplsLdpEntityGenericLRMax,
                                         INT4
                                         *pi4RetValMplsLdpEntityGenericLRStorageType)
{
    tLdpEthParams      *pLdpEthParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rGenericLRStorageTypeGet: "
                  "Entity %d do not exist\n", u4MplsLdpEntityIndex);
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                  u4MplsLdpEntityGenericLRMin,
                                  u4MplsLdpEntityGenericLRMax, &pLdpEthParams)
        == LDP_FAILURE)
    {
        LDP_DBG3 (LDP_IF_SNMP, "\rGenericLRStorageTypeGet: "
                  "Generic LR %d %d %d do not exist\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityGenericLRStorageType =
        (INT4) (pLdpEthParams->u1StorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityGenericLRRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                retValMplsLdpEntityGenericLRRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityGenericLRRowStatus (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4 u4MplsLdpEntityGenericLRMin,
                                       UINT4 u4MplsLdpEntityGenericLRMax,
                                       INT4
                                       *pi4RetValMplsLdpEntityGenericLRRowStatus)
{
    tLdpEthParams      *pLdpEthParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rGenericLRRowStatusGet: "
                  "Entity %d do not exist\n", u4MplsLdpEntityIndex);
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                  u4MplsLdpEntityGenericLRMin,
                                  u4MplsLdpEntityGenericLRMax, &pLdpEthParams)
        == LDP_FAILURE)
    {
        LDP_DBG3 (LDP_IF_SNMP,
                  "\rGenericLRRowStatusGet: Generic LR %d %d %d do not exist\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityGenericLRRowStatus =
        (INT4) (pLdpEthParams->u1RowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityGenericLabelSpace
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                setValMplsLdpEntityGenericLabelSpace
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityGenericLabelSpace (tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 u4MplsLdpEntityIndex,
                                      UINT4 u4MplsLdpEntityGenericLRMin,
                                      UINT4 u4MplsLdpEntityGenericLRMax,
                                      INT4
                                      i4SetValMplsLdpEntityGenericLabelSpace)
{
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rLabelSpaceSet: Entity %d do not exist\n",
                  u4MplsLdpEntityIndex);
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                  u4MplsLdpEntityGenericLRMin,
                                  u4MplsLdpEntityGenericLRMax,
                                  &pLdpEthParams) == LDP_FAILURE)
    {
        LDP_DBG3 (LDP_IF_SNMP,
                  "\rLabelSpaceSet: Generic LR %d %d %d do not exist\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        return SNMP_FAILURE;
    }

    pLdpEthParams->u1LabelSpace =
        (UINT1) i4SetValMplsLdpEntityGenericLabelSpace;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityGenericIfIndexOrZero
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                setValMplsLdpEntityGenericIfIndexOrZero
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityGenericIfIndexOrZero (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         UINT4 u4MplsLdpEntityGenericLRMin,
                                         UINT4 u4MplsLdpEntityGenericLRMax,
                                         INT4
                                         i4SetValMplsLdpEntityGenericIfIndexOrZero)
{
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT4               u4L3Intf = LDP_ZERO;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rGenericIfIndexSet: Entity %d do not exist\n",
                  u4MplsLdpEntityIndex);
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                  u4MplsLdpEntityGenericLRMin,
                                  u4MplsLdpEntityGenericLRMax,
                                  &pLdpEthParams) == LDP_FAILURE)
    {
        LDP_DBG3 (LDP_IF_SNMP,
                  "\rGenericIfIndexSet: Generic LR %d %d %d do not exist\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        return SNMP_FAILURE;
    }

    if (CfaUtilGetL3IfFromMplsIf (i4SetValMplsLdpEntityGenericIfIndexOrZero,
                                  &u4L3Intf, TRUE) == CFA_FAILURE)
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rGenericIfIndexSet: L3VlanIf Get failed for"
                  "MPLS IF\n", i4SetValMplsLdpEntityGenericIfIndexOrZero);
        return SNMP_FAILURE;
    }

    pLdpEthParams->u4IfIndex = u4L3Intf;
    pLdpEthParams->u4MplsIfIndex =
        (UINT4) i4SetValMplsLdpEntityGenericIfIndexOrZero;
    pLdpEntity->bIsIntfMapped = LDP_TRUE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityGenericLRStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                setValMplsLdpEntityGenericLRStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityGenericLRStorageType (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         UINT4 u4MplsLdpEntityGenericLRMin,
                                         UINT4 u4MplsLdpEntityGenericLRMax,
                                         INT4
                                         i4SetValMplsLdpEntityGenericLRStorageType)
{
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rGenericStoragetTypeSet: "
                  "Entity %d do not exist\n", u4MplsLdpEntityIndex);
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                  u4MplsLdpEntityGenericLRMin,
                                  u4MplsLdpEntityGenericLRMax,
                                  &pLdpEthParams) == LDP_FAILURE)
    {
        LDP_DBG3 (LDP_IF_SNMP,
                  "\rGenericStoragetTypeSet: Generic LR %d %d %d do not exist\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        return SNMP_FAILURE;
    }

    pLdpEthParams->u1StorageType =
        (UINT1) i4SetValMplsLdpEntityGenericLRStorageType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityGenericLRRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                setValMplsLdpEntityGenericLRRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityGenericLRRowStatus (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4 u4MplsLdpEntityGenericLRMin,
                                       UINT4 u4MplsLdpEntityGenericLRMax,
                                       INT4
                                       i4SetValMplsLdpEntityGenericLRRowStatus)
{
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT4               u4IncarnId = MPLS_DEF_INCARN;
    UINT4               u4UnnumAssocIPIf = 0;
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT4               u4IfAddr = LDP_ZERO;
    tCfaIfInfo          CfaIfInfo;

    UINT1 u1InterfaceType=0;
#ifdef MPLS_IPV6_WANTED
    UINT1      u1GetIpv6IfAllAddr = LDP_FALSE;
    tIp6Addr LnklocalIpv6Addr;
    tIp6Addr SitelocalIpv6Addr;
    tIp6Addr GlbUniqueIpv6Addr;
    tIp6Addr TempIpv6Addr;
#endif

    MEMSET ((UINT1 *) &CfaIfInfo, LDP_ZERO, sizeof (tCfaIfInfo));

#ifdef MPLS_IPV6_WANTED
    MEMSET(&TempIpv6Addr,0,sizeof(tIp6Addr));
    MEMSET(&LnklocalIpv6Addr,0,sizeof(tIp6Addr));
    MEMSET(&SitelocalIpv6Addr,0,sizeof(tIp6Addr));
    MEMSET(&GlbUniqueIpv6Addr,0,sizeof(tIp6Addr));
#endif

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    if (LdpGetLdpEntity (u4IncarnId, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rGenericLRRowStatusSet: "
                  "Entity %d do not exist\n", u4MplsLdpEntityIndex);
        return SNMP_FAILURE;
    }

    if (i4SetValMplsLdpEntityGenericLRRowStatus != CREATE_AND_WAIT)
    {
        if (LdpGetLdpEntityEthParams (u4IncarnId, pLdpEntity,
                                      u4MplsLdpEntityGenericLRMin,
                                      u4MplsLdpEntityGenericLRMax,
                                      &pLdpEthParams) == LDP_FAILURE)
        {
            LDP_DBG3 (LDP_IF_SNMP,
                      "\rGenericLRRowStatusSet: "
                      "Generic LR %d %d %d do not exist\n",
                      u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                      u4MplsLdpEntityGenericLRMax);
            return SNMP_FAILURE;
        }
    }

    switch (i4SetValMplsLdpEntityGenericLRRowStatus)
    {
        case ACTIVE:

            if (pLdpEthParams->u4IfIndex == LDP_ZERO)
            {
                pLdpEthParams->u1RowStatus = ACTIVE;
                return SNMP_SUCCESS;
            }
            else
            {
                if (LdpIpGetIfAddr (pLdpEthParams->u4IfIndex,
                                    &u4IfAddr) == LDP_FAILURE)
                {
                    LDP_DBG1 (LDP_IF_SNMP,
                              "\rGenericLRRowStatusSet: "
                              "IP Address do not exist for interface %d\n",
                              pLdpEthParams->u4IfIndex);
#ifndef MPLS_IPV6_WANTED
					return SNMP_FAILURE;
#endif
#ifdef MPLS_IPV6_WANTED
			if(LdpGetIpv6IfAllAddr((INT4)pLdpEthParams->u4IfIndex,&LnklocalIpv6Addr,&SitelocalIpv6Addr,&GlbUniqueIpv6Addr)!=LDP_SUCCESS)
			{
				LDP_DBG1 (LDP_IF_SNMP,  "\rGenericLRRowStatusSet: "
                              "IV6P Addresses do not exist for interface %d\n",
                              pLdpEthParams->u4IfIndex);
				return SNMP_FAILURE;
			}
#endif
                }
#ifdef MPLS_IPV6_WANTED
		  else
		  {
		      u1GetIpv6IfAllAddr = LDP_TRUE;
		  }
#endif
                if (CfaGetIfInfo (pLdpEthParams->u4MplsIfIndex, &CfaIfInfo)
                    == CFA_FAILURE)
                {
                    LDP_DBG1 (LDP_IF_SNMP,
                              "\rGenericLRRowStatusSet: "
                              "CFA Information not present for IfIndex\n",
                              pLdpEthParams->u4IfIndex);
                    return SNMP_FAILURE;
                }

                if (LdpGetLdpEntityIfEntry (u4IncarnId, pLdpEntity,
                                            pLdpEthParams->u4IfIndex,
                                            &pLdpIfTableEntry) == LDP_SUCCESS)
                {
                    (pLdpIfTableEntry->u2Count)++;
                    pLdpEthParams->u1RowStatus = ACTIVE;
                    return SNMP_SUCCESS;
                }
                
                /*Since interface type is derived on the basis of IP Addresses receivd,There may be a case 
                for unnumbered interfaces when the IP ADDR received is zero that interface is also treated as IPV4 Type */
                CfaGetIfUnnumAssocIPIf (pLdpEthParams->u4IfIndex, &u4UnnumAssocIPIf);
                LDP_DBG3(LDP_IF_PRCS,"nmhSetMplsLdpEntityGenericLRRowStatus:UnnumAssocIpIf is % d, Ip4Add is %x, IfIndex is %d \n",
                     u4UnnumAssocIPIf,u4IfAddr,pLdpEthParams->u4IfIndex);
	        if((u4IfAddr!=LDP_ZERO) || (LDP_ZERO != u4UnnumAssocIPIf))
		{
			u1InterfaceType=LDP_IFTYPE_IPV4;
		}
#ifdef MPLS_IPV6_WANTED
		if(LDP_TRUE == u1GetIpv6IfAllAddr)
		{
			if(LdpGetIpv6IfAllAddr((INT4)pLdpEthParams->u4IfIndex,&LnklocalIpv6Addr,&SitelocalIpv6Addr,&GlbUniqueIpv6Addr)!= LDP_SUCCESS)
			{
				LDP_DBG(LDP_SSN_PRCS, " API GetIpv6IfAllAddr Failed to fetch IPV6 Addresses\n");
			}
            	}
		if(LdpInterfaceIpv6Match(&LnklocalIpv6Addr,
						&SitelocalIpv6Addr,
					&GlbUniqueIpv6Addr)==LDP_TRUE)
		{
			u1InterfaceType=u1InterfaceType|LDP_IFTYPE_IPV6;
		}
#endif
		switch(u1InterfaceType)
		{
			case LDP_IFTYPE_IPV4:
				/*Add the Interface entry to the interface list
				 *of the entity*/
					pLdpIfTableEntry=LdpCreateLdpIfTableEntry (pLdpEthParams->u4IfIndex,
							CfaIfInfo.u1IfOperStatus,
							pLdpEntity, u4IfAddr);
					if(pLdpIfTableEntry==NULL)
					{
#if 0
						printf("Interface entry addition failed to the Interface List : u4IfIndex=%d OperStatus=%d Ipv4Addr=%d\n",
								pLdpEthParams->u4IfIndex,CfaIfInfo.u1IfOperStatus,u4IfAddr);
#endif

						return SNMP_FAILURE;
					}
#if 0
					printf("Interface entry added successfully to the Interface List : u4IfIndex=%d OperStatus=%d Ipv4Addr=%d\n",
							pLdpEthParams->u4IfIndex,CfaIfInfo.u1IfOperStatus,u4IfAddr);
#endif
				break;
#if MPLS_IPV6_WANTED
			case LDP_IFTYPE_IPV6:
			case LDP_IFTYPE_DUAL:
				if((u4IfAddr!=LDP_ZERO)||(MEMCMP(&LnklocalIpv6Addr,&TempIpv6Addr,LDP_IPV6ADR_LEN)!=LDP_ZERO)
						||(MEMCMP(&SitelocalIpv6Addr,&TempIpv6Addr,LDP_IPV6ADR_LEN)!=LDP_ZERO)||
						(MEMCMP(&GlbUniqueIpv6Addr,&TempIpv6Addr,LDP_IPV6ADR_LEN)!=LDP_ZERO))
				{ 
					pLdpIfTableEntry=LdpCreateLdpIpv6IfTableEntry(pLdpEthParams->u4IfIndex,
							CfaIfInfo.u1IfOperStatus,
							pLdpEntity,u1InterfaceType,
							u4IfAddr,
							&LnklocalIpv6Addr,
							&SitelocalIpv6Addr,
							&GlbUniqueIpv6Addr);
					if(pLdpIfTableEntry==NULL)
					{
#if 0
						printf("Interface entry addition fail to the Interface List : u4IfIndex=%d OperStatus=%d Ipv4Addr=%d LinkLocalAddr=%s SiteLocalAddr=%s GlblUniqueAddr=%s\n",
								pLdpEthParams->u4IfIndex,CfaIfInfo.u1IfOperStatus,u4IfAddr,
								Ip6PrintAddr(&LnklocalIpv6Addr),
								Ip6PrintAddr(&SitelocalIpv6Addr),
								Ip6PrintAddr(&GlbUniqueIpv6Addr));
#endif

						return SNMP_FAILURE;
					}
#if 0
					printf("Interface entry addition success to the Interface List : u4IfIndex=%d OperStatus=%d Ipv4Addr=%d LinkLocalAddr=%s SiteLocalAddr=%s GlblUniqueAddr=%s\n",
							pLdpEthParams->u4IfIndex,CfaIfInfo.u1IfOperStatus,u4IfAddr,
							Ip6PrintAddr(&LnklocalIpv6Addr),
							Ip6PrintAddr(&SitelocalIpv6Addr),
							Ip6PrintAddr(&GlbUniqueIpv6Addr));
#endif

				}

				break;
#endif
		};
		pLdpEthParams->u1RowStatus = ACTIVE;
		return SNMP_SUCCESS;
	    }
            break;

        case DESTROY:
            if (pLdpEthParams->u4IfIndex != LDP_ZERO)
            {
                if (LdpGetLdpEntityIfEntry (u4IncarnId, pLdpEntity,
                                            pLdpEthParams->u4IfIndex,
                                            &pLdpIfTableEntry) == LDP_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                /* If the no.of Label Ranges configured for the
                 * * Interface Index is 1, then after deleting it, the
                 * * Interface List is deleted. */
                if ((pLdpIfTableEntry->u2Count) == 1)
                {
                    TMO_SLL_Delete (&pLdpEntity->IfList,
                                    &(pLdpIfTableEntry->NextIfEntry));
                    MemReleaseMemBlock (LDP_EIF_POOL_ID,
                                        (UINT1 *) (pLdpIfTableEntry));
                }
                else
                {
                    (pLdpIfTableEntry->u2Count)--;
                }
            }
            /* If different label range is added for same interface,
             * pLdpEthParams->u4IfIndex would be zero for second label
             * range configuration.
             * Association created when first label range configuration
             * should not be deleted in this situation.
             * Thatswhy pLdpEntity->bIsIntfMapped == FALSE condition is
             * added here*/

            else if (pLdpEntity->bIsIntfMapped == FALSE)
            {
                LdpDisassociateIfEntry (pLdpEntity);
            }
            TMO_SLL_Delete (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                            &(pLdpEthParams->NextRangeEntry));
            MemReleaseMemBlock (LDP_ETH_LBL_RNG_POOL_ID,
                                (UINT1 *) (pLdpEthParams));

            /* A label range is deleted and the flag needs to be reset.
             * Re-setting the flag to TRUE/FALSE on scanning 
             * the list of label ranges.
             */

            if (TMO_SLL_Count (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity)) ==
                LDP_ZERO)
            {
                pLdpEntity->bIsIntfMapped = FALSE;
            }
            else
            {
                TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                              pLdpEthParams, tLdpEthParams *)
                {
                    if (pLdpEthParams->u4MplsIfIndex != LDP_ZERO)
                    {
                        pLdpEntity->bIsIntfMapped = TRUE;
                        break;
                    }
                    else
                    {
                        pLdpEntity->bIsIntfMapped = FALSE;
                    }
                }
            }
            break;

        case NOT_IN_SERVICE:
            if (pLdpEthParams->u4IfIndex != LDP_ZERO)
            {
                if (LdpGetLdpEntityIfEntry (u4IncarnId, pLdpEntity,
                                            pLdpEthParams->u4IfIndex,
                                            &pLdpIfTableEntry) == LDP_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                /* If the no.of Label Ranges configured for the
                 * Interface Index is 1, then after deleting it, the
                 * Interface List is deleted. */
                if ((pLdpIfTableEntry->u2Count) == 1)
                {
                    TMO_SLL_Delete (&pLdpEntity->IfList,
                                    &(pLdpIfTableEntry->NextIfEntry));
                    MemReleaseMemBlock (LDP_EIF_POOL_ID,
                                        (UINT1 *) pLdpIfTableEntry);
                }
                else
                {
                    (pLdpIfTableEntry->u2Count)--;
                }
            }
            pLdpEthParams->u1RowStatus = NOT_IN_SERVICE;
            break;

        case CREATE_AND_WAIT:

            pLdpEthParams = (tLdpEthParams *)
                MemAllocMemBlk (LDP_ETH_LBL_RNG_POOL_ID);

            if (pLdpEthParams == NULL)
            {
                LDP_DBG ((LDP_IF_SNMP | LDP_IF_MEM),
                         "Entity Generic Range Mem Alloc failure \n");
                return SNMP_FAILURE;
            }

            TMO_SLL_Init_Node (&(pLdpEthParams->NextRangeEntry));
            MEMSET ((VOID *) pLdpEthParams, LDP_ZERO, sizeof (tLdpEthParams));
            pLdpEthParams->u4MinLabelVal = u4MplsLdpEntityGenericLRMin;
            pLdpEthParams->u4MaxLabelVal = u4MplsLdpEntityGenericLRMax;
            pLdpEthParams->u1RowStatus = NOT_READY;

            TMO_SLL_Add (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                         &(pLdpEthParams->NextRangeEntry));
            break;

        case CREATE_AND_GO:
              return SNMP_FAILURE;
              break;
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityGenericLabelSpace
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                testValMplsLdpEntityGenericLabelSpace
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityGenericLabelSpace (UINT4 *pu4ErrorCode,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         UINT4 u4MplsLdpEntityGenericLRMin,
                                         UINT4 u4MplsLdpEntityGenericLRMax,
                                         INT4
                                         i4TestValMplsLdpEntityGenericLabelSpace)
{
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    switch (i4TestValMplsLdpEntityGenericLabelSpace)
    {
        case LDP_GEN_PER_PLATFORM:
            break;
        case LDP_GEN_PER_IFACE:
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rLabelSpaceTest: Entity %d do not exist\n",
                  u4MplsLdpEntityIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY))
    {
        LDP_DBG1 (LDP_IF_SNMP, "\rLabelSpaceTest: Entity %d is ACTIVE\n",
                  u4MplsLdpEntityIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                  u4MplsLdpEntityGenericLRMin,
                                  u4MplsLdpEntityGenericLRMax,
                                  &pLdpEthParams) == LDP_FAILURE)
    {
        LDP_DBG3 (LDP_IF_SNMP,
                  "\rLabelSpaceTest: Generic LR %d %d %d do not exist\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((pLdpEthParams->u1RowStatus != NOT_IN_SERVICE) &&
        (pLdpEthParams->u1RowStatus != NOT_READY))
    {
        LDP_DBG3 (LDP_IF_SNMP,
                  "\rLabelSpaceTest: Generic LR %d %d %d is ACTIVE\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityGenericIfIndexOrZero
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                testValMplsLdpEntityGenericIfIndexOrZero
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityGenericIfIndexOrZero (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            UINT4 u4MplsLdpEntityGenericLRMin,
                                            UINT4 u4MplsLdpEntityGenericLRMax,
                                            INT4
                                            i4TestValMplsLdpEntityGenericIfIndexOrZero)
{
    UINT4               u4L3Intf = LDP_ZERO;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEthParams      *pLdpEthParams = NULL;
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    tLdpPeer           *pLdpPeer = NULL;
    UINT4               u4PeerAddr = LDP_ZERO;
    UINT4               u4PeerTempAddr = LDP_ZERO;

    if ((i4TestValMplsLdpEntityGenericIfIndexOrZero < LDP_ONE)
        || (i4TestValMplsLdpEntityGenericIfIndexOrZero >
            LDP_MAX_INTERFACES (MPLS_DEF_INCARN)))
    {
        LDP_DBG1 (LDP_IF_SNMP,
                  "\rGenericIfIndexTest: Value %d is not in range\n",
                  i4TestValMplsLdpEntityGenericIfIndexOrZero);
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /* This MPLS Interface has not yet stacked over any L3 interface */
    if (CfaUtilGetL3IfFromMplsIf
        (i4TestValMplsLdpEntityGenericIfIndexOrZero, &u4L3Intf,
         TRUE) == CFA_FAILURE)
    {
        LDP_DBG (LDP_IF_SNMP, "L3VlanIf Get failed \n");
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        LDP_DBG1 (LDP_IF_SNMP,
                  "\rGenericIfIndexTest: Entity %d do not exist\n",
                  u4MplsLdpEntityIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY))
    {
        LDP_DBG1 (LDP_IF_SNMP,
                  "\rGenericIfIndexTest: Entity %d is ACTIVE\n",
                  u4MplsLdpEntityIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                  u4MplsLdpEntityGenericLRMin,
                                  u4MplsLdpEntityGenericLRMax,
                                  &pLdpEthParams) == LDP_FAILURE)
    {
        LDP_DBG3 (LDP_IF_SNMP,
                  "\rGenericIfIndexTest: Entity %d %d %d do not exist\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((pLdpEthParams->u1RowStatus != NOT_IN_SERVICE) &&
        (pLdpEthParams->u1RowStatus != NOT_READY))
    {
        LDP_DBG3 (LDP_IF_SNMP,
                  "\rGenericIfIndexTest: Entity %d %d %d is ACTIVE\n",
                  u4MplsLdpEntityIndex, u4MplsLdpEntityGenericLRMin,
                  u4MplsLdpEntityGenericLRMax);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
    {
        pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
        if (pLdpPeer != NULL)
        {
            MEMCPY ((UINT1 *) &u4PeerAddr, pLdpPeer->NetAddr.Addr.au1Ipv4Addr,
                    IPV4_ADDR_LENGTH);
            u4PeerAddr = OSIX_NTOHL (u4PeerAddr);
        }
    }

    /* Validation is done to check whether the configured 
     * interface index is being used by any other LDP Link Entity. */
    TMO_SLL_Scan (&LDP_ENTITY_LIST (MPLS_DEF_INCARN), pLdpEntity, tLdpEntity *)
    {
        if ((LdpGetLdpEntityIfEntry (MPLS_DEF_INCARN, pLdpEntity,
                                     u4L3Intf,
                                     &pLdpIfTableEntry) == LDP_SUCCESS))
        {
            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
            {
                pLdpPeer = (tLdpPeer *) TMO_SLL_First (&(pLdpEntity->PeerList));
                if (pLdpPeer != NULL)
                {
                    MEMCPY ((UINT1 *) &u4PeerTempAddr,
                            pLdpPeer->NetAddr.Addr.au1Ipv4Addr, IPV4_ADDR_LENGTH);
                    u4PeerTempAddr = OSIX_NTOHL (u4PeerTempAddr);
                }
                if ((u4PeerAddr == LDP_ZERO) || (u4PeerTempAddr == LDP_ZERO)
                    || (u4PeerAddr != u4PeerTempAddr) || (pLdpEntity->u4EntityIndex == u4MplsLdpEntityIndex))
                {
                    continue;
                }
                LDP_DBG2 (LDP_IF_SNMP,
                          "\rGenericIfIndexTest: Interface %d is"
                          "already used by another entity %d\n",
                          i4TestValMplsLdpEntityGenericIfIndexOrZero,
                          pLdpEntity->u4EntityIndex);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            else
            {
                if (u4PeerAddr == LDP_ZERO)
                {
                    LDP_DBG2 (LDP_IF_SNMP,
                              "\rGenericIfIndexTest: Interface %d is"
                              "already used by another entity %d\n",
                              i4TestValMplsLdpEntityGenericIfIndexOrZero,
                              pLdpEntity->u4EntityIndex);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
        }

    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityGenericLRStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                testValMplsLdpEntityGenericLRStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityGenericLRStorageType (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            UINT4 u4MplsLdpEntityGenericLRMin,
                                            UINT4 u4MplsLdpEntityGenericLRMax,
                                            INT4
                                            i4TestValMplsLdpEntityGenericLRStorageType)
{
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    switch (i4TestValMplsLdpEntityGenericLRStorageType)
    {
        case LDP_STORAGE_VOLATILE:
        case LDP_STORAGE_NONVOLATILE:
            break;
        case LDP_STORAGE_OTHER:
        case LDP_STORAGE_PERMANENT:
        case LDP_STORAGE_READONLY:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                  u4MplsLdpEntityGenericLRMin,
                                  u4MplsLdpEntityGenericLRMax,
                                  &pLdpEthParams) == LDP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (pLdpEthParams->u1RowStatus != NOT_READY)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityGenericLRRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax

                The Object 
                testValMplsLdpEntityGenericLRRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityGenericLRRowStatus (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4 u4MplsLdpEntityGenericLRMin,
                                          UINT4 u4MplsLdpEntityGenericLRMax,
                                          INT4
                                          i4TestValMplsLdpEntityGenericLRRowStatus)
{
    tLdpEntity         *pLdpEntity = NULL;
    tLdpEthParams      *pLdpEthParams = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT4               u4MinLabel = u4MplsLdpEntityGenericLRMin;
    UINT4               u4MaxLabel = u4MplsLdpEntityGenericLRMax;

    if ((i4TestValMplsLdpEntityGenericLRRowStatus == CREATE_AND_GO) ||
        (i4TestValMplsLdpEntityGenericLRRowStatus == NOT_READY))
    {
        LDP_DBG (LDP_IF_SNMP,
                 "\rGenericLRRowStatusTest: CreateAndGo and NotReady "
                 "are not allowed to set\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (MPLS_DEF_INCARN, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) == LDP_FAILURE)
    {
        LDP_DBG1 (LDP_IF_SNMP,
                  "\rGenericLRRowStatusTest: Entity %d do not exist\n",
                  u4MplsLdpEntityIndex);
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_IN_SERVICE) &&
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) != NOT_READY))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (i4TestValMplsLdpEntityGenericLRRowStatus != CREATE_AND_WAIT)
    {
        if (LdpGetLdpEntityEthParams (MPLS_DEF_INCARN, pLdpEntity,
                                      u4MinLabel, u4MaxLabel, &pLdpEthParams)
            == LDP_FAILURE)
        {
            LDP_DBG3 (LDP_IF_SNMP,
                      "\rGenericLRRowStatusTest: "
                      "Generic LR Entity %d %d %d do not exist\n",
                      u4MplsLdpEntityIndex, u4MinLabel, u4MaxLabel);
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            return SNMP_FAILURE;
        }

        if (i4TestValMplsLdpEntityGenericLRRowStatus == ACTIVE)
        {
            if ((pLdpEthParams->u1RowStatus != NOT_IN_SERVICE) &&
                (pLdpEthParams->u1RowStatus != NOT_READY))
            {
                LDP_DBG3 (LDP_IF_SNMP,
                          "\rGenericLRRowStatusTest: "
                          "Generic LR Entity %d %d %d is ACTIVE\n",
                          u4MplsLdpEntityIndex, u4MinLabel, u4MaxLabel);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
    }
    else
    {
        /* Check the label range */
        if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
        {
            /* Check for Targeted hello label range */
            if ((u4MinLabel < gSystemSize.MplsSystemSize.u4MinL2VpnLblRange) ||
                (u4MaxLabel > gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange))
            {
                LDP_DBG2 (LDP_IF_SNMP, "\r%%Label Range should be "
                          "%d - %d for Targeted Hello\n",
                          gSystemSize.MplsSystemSize.u4MinL2VpnLblRange,
                          gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        else
        {
            /* Check for Basic hello label range */
            if ((u4MinLabel < gSystemSize.MplsSystemSize.u4MinLdpLblRange) ||
                (u4MaxLabel > gSystemSize.MplsSystemSize.u4MaxLdpLblRange))
            {
                LDP_DBG2 (LDP_IF_SNMP, "\r%%Label Range should be "
                          "%d - %d for Basic discovery\n",
                          gSystemSize.MplsSystemSize.u4MinLdpLblRange,
                          gSystemSize.MplsSystemSize.u4MaxLdpLblRange);
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        /* Check Whether Minimum Label is greater than Maximum Label */
        if (u4MinLabel > u4MaxLabel)
        {
            LDP_DBG (LDP_IF_SNMP, "\r%% Min Label is greater"
                     "than Max Label\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        /* Label Disjointness Validation Done Here. 
         * It is done for all the entities in the platform. */
        TMO_SLL_Scan (&LDP_ENTITY_LIST (MPLS_DEF_INCARN),
                      pLdpEntity, tLdpEntity *)
        {
            TMO_SLL_Scan (LDP_ENTITY_ETH_LBL_RNGE_LIST (pLdpEntity),
                          pLdpEthParams, tLdpEthParams *)
            {
                /* check if u4Minlabel/MaxLabel lies between the range 
                 * specified in label specified in pLdpEthParams. If it is 
                 * throw error*/
                if (((u4MinLabel >= pLdpEthParams->u4MinLabelVal) &&
                     (u4MinLabel <= pLdpEthParams->u4MaxLabelVal)) ||
                    ((u4MaxLabel >= pLdpEthParams->u4MinLabelVal) &&
                     (u4MaxLabel <= pLdpEthParams->u4MaxLabelVal)))
                {
                    LDP_DBG3 (LDP_IF_SNMP,
                              "\rGenericLRRowStatusTest: "
                              "Generic LR Entity %d %d %d is "
                              "in a disjoint range with another entity\n",
                              u4MplsLdpEntityIndex, u4MinLabel, u4MaxLabel);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                /* check if the u4MaxLabelVal/MinLabel of Ethparams lies
                 * between the Minlabel and the Max label configured.
                 * If it is, throw error
                 */
                if (((pLdpEthParams->u4MaxLabelVal >= u4MinLabel) &&
                     (pLdpEthParams->u4MaxLabelVal <= u4MaxLabel)) ||
                    ((pLdpEthParams->u4MinLabelVal >= u4MinLabel) &&
                     (pLdpEthParams->u4MinLabelVal <= u4MaxLabel)))
                {
                    LDP_DBG3 (LDP_IF_SNMP,
                              "\rGenericLRRowStatusTest: "
                              "Generic LR Entity %d %d %d is "
                              "in a disjoint range with another entity\n",
                              u4MplsLdpEntityIndex, u4MinLabel, u4MaxLabel);
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }

            }
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsLdpEntityGenericLRTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityGenericLRMin
                MplsLdpEntityGenericLRMax
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsLdpEntityGenericLRTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
