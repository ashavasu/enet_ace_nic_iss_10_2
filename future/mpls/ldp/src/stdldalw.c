/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdldalw.c,v 1.8 2013/09/05 15:05:44 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdldalw.h"
# include  "stdldplw.h"
# include  "ldpincs.h"
# include  "ldplwinc.h"

/* LOW LEVEL Routines for Table : MplsLdpEntityAtmTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpEntityAtmTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpEntityAtmTable (tSNMP_OCTET_STRING_TYPE *
                                               pMplsLdpEntityLdpId,
                                               UINT4 u4MplsLdpEntityIndex)
{

    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMplsLdpEntityLdpId, u4MplsLdpEntityIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpEntityAtmTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpEntityAtmTable (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 *pu4MplsLdpEntityIndex)
{
    UINT4               u4IncarnId = 0;

    UINT1               u1FirstLoopEntry = LDP_TRUE;
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    *pu4MplsLdpEntityIndex = LDP_ZERO;

    for (u4IncarnId = 0; u4IncarnId < LDP_MAX_INCARN; ++u4IncarnId)
    {
        pList = &LDP_ENTITY_LIST (u4IncarnId);
        pLdpEntity = (tLdpEntity *) TMO_SLL_First (pList);

        if (pLdpEntity == NULL)
        {
            continue;
        }

        TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
        {
            if (LDP_LBL_SPACE_TYPE (pLdpEntity) == LDP_PER_IFACE)
            {
                if (u1FirstLoopEntry == LDP_TRUE)
                {
                    CPY_TO_SNMP (pMplsLdpEntityLdpId, pLdpEntity->LdpId,
                                 LDP_MAX_LDPID_LEN);
                    *pu4MplsLdpEntityIndex = pLdpEntity->u4EntityIndex;
                    u1FirstLoopEntry = LDP_FALSE;
                    continue;
                }
                if (MEMCMP
                    (pLdpEntity->LdpId, pu1LdpEntityId, LDP_MAX_LDPID_LEN) < 0)
                {
                    CPY_TO_SNMP (pMplsLdpEntityLdpId, pLdpEntity->LdpId,
                                 LDP_MAX_LDPID_LEN);
                }
            }
        }

        /* Under the found minimum EntityID the search is made for getting
         * the minimum Entity Index present
         */

        if (u1FirstLoopEntry == LDP_FALSE)
        {
            TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
            {
                if (LDP_LBL_SPACE_TYPE (pLdpEntity) == LDP_PER_IFACE)
                {
                    if (MEMCMP
                        (pLdpEntity->LdpId, pu1LdpEntityId,
                         LDP_MAX_LDPID_LEN) == LDP_ZERO)
                    {
                        if ((pLdpEntity->u4EntityIndex)
                            < (*pu4MplsLdpEntityIndex))
                        {
                            *pu4MplsLdpEntityIndex =
                                (pLdpEntity->u4EntityIndex);
                        }
                    }
                }
            }
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpEntityAtmTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpEntityAtmTable (tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextMplsLdpEntityLdpId,
                                      UINT4 u4MplsLdpEntityIndex,
                                      UINT4 *pu4NextMplsLdpEntityIndex)
{
    UINT1               u1FirstLoopEntry = LDP_TRUE;
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1NextLdpEntityId = NULL;
    UINT4               u4EntityIndex = u4MplsLdpEntityIndex;

    *pu4NextMplsLdpEntityIndex = LDP_ZERO;

    if (pMplsLdpEntityLdpId->i4_Length < 0)
    {
        return SNMP_FAILURE;
    }

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1NextLdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);
    pList = &LDP_ENTITY_LIST (MPLS_DEF_INCARN);

    /* loop for finding the next Index in the Incarnation for the 
     * Entity LdpId. 
     */
    TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
    {
        if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
        {
            continue;
        }
        if (MEMCMP (pLdpEntity->LdpId, pu1LdpEntityId, LDP_MAX_LDPID_LEN) == 0)
        {
            if (pLdpEntity->u4EntityIndex > u4EntityIndex)
            {
                if (u1FirstLoopEntry == LDP_TRUE)
                {
                    *pu4NextMplsLdpEntityIndex = pLdpEntity->u4EntityIndex;
                    u1FirstLoopEntry = LDP_FALSE;
                    continue;
                }
                if (pLdpEntity->u4EntityIndex < *pu4NextMplsLdpEntityIndex)
                {
                    *pu4NextMplsLdpEntityIndex = pLdpEntity->u4EntityIndex;
                }
            }
        }
    }
    if (u1FirstLoopEntry == LDP_FALSE)
    {
        CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pu1LdpEntityId,
                     LDP_MAX_LDPID_LEN);
        return SNMP_SUCCESS;
    }

    /* For the Incarnation and for the Entity LdpId there is no Entity Index 
     * which is greater than the current Entity Index. 
     * Loop to find the Next Entity LdpId just greater then the current one. 
     */
    TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
    {
        if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
        {
            continue;
        }
        if (MEMCMP (pLdpEntity->LdpId, pu1LdpEntityId, LDP_MAX_LDPID_LEN) > 0)
        {
            if (u1FirstLoopEntry == LDP_TRUE)
            {
                CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pLdpEntity->LdpId,
                             LDP_MAX_LDPID_LEN);
                *pu4NextMplsLdpEntityIndex = pLdpEntity->u4EntityIndex;
                u1FirstLoopEntry = LDP_FALSE;
                continue;
            }
            if (MEMCMP (pLdpEntity->LdpId, pu1NextLdpEntityId,
                        LDP_MAX_LDPID_LEN) < 0)
            {
                CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pLdpEntity->LdpId,
                             LDP_MAX_LDPID_LEN);
            }
        }
    }

    /* For the Next Entity LdpId found from the previous loop, the least Entity 
     * Index is found.
     */
    if (u1FirstLoopEntry == LDP_FALSE)
    {
        TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
        {
            if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
            {
                continue;
            }
            if (MEMCMP ((pLdpEntity->LdpId), pu1NextLdpEntityId,
                        LDP_MAX_LDPID_LEN) == 0)
            {
                if ((pLdpEntity->u4EntityIndex) < (*pu4NextMplsLdpEntityIndex))
                {
                    *pu4NextMplsLdpEntityIndex = (pLdpEntity->u4EntityIndex);
                }
            }
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmIfIndexOrZero
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmIfIndexOrZero
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmIfIndexOrZero (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     INT4
                                     *pi4RetValMplsLdpEntityAtmIfIndexOrZero)
{

    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityAtmIfIndexOrZero =
        (INT4) (pLdpEntity->pLdpAtmParams->u4IfIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmMergeCap
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmMergeCap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmMergeCap (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 *pi4RetValMplsLdpEntityAtmMergeCap)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }
    *pi4RetValMplsLdpEntityAtmMergeCap =
        (INT4) (pLdpEntity->pLdpAtmParams->u1MergeType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmLRComponents
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmLRComponents
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmLRComponents (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    UINT4
                                    *pu4RetValMplsLdpEntityAtmLRComponents)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    *pu4RetValMplsLdpEntityAtmLRComponents =
        (UINT4) pLdpEntity->pLdpAtmParams->u2NumOfLblRnges;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmVcDirectionality
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmVcDirectionality
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmVcDirectionality (tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        INT4
                                        *pi4RetValMplsLdpEntityAtmVcDirectionality)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityAtmVcDirectionality =
        (INT4) (pLdpEntity->pLdpAtmParams->u1VcDirection);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmLsrConnectivity
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmLsrConnectivity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmLsrConnectivity (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       *pi4RetValMplsLdpEntityAtmLsrConnectivity)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityAtmLsrConnectivity =
        (INT4) (pLdpEntity->pLdpAtmParams->u2Connectivity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmDefaultControlVpi
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmDefaultControlVpi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmDefaultControlVpi (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         *pi4RetValMplsLdpEntityAtmDefaultControlVpi)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityAtmDefaultControlVpi =
        (INT4) (pLdpEntity->pLdpAtmParams->u2EntityConfDefVpi);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmDefaultControlVci
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmDefaultControlVci
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmDefaultControlVci (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         *pi4RetValMplsLdpEntityAtmDefaultControlVci)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityAtmDefaultControlVci =
        (INT4) (pLdpEntity->pLdpAtmParams->u2EntityConfDefVci);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmUnlabTrafVpi
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmUnlabTrafVpi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmUnlabTrafVpi (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    INT4 *pi4RetValMplsLdpEntityAtmUnlabTrafVpi)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityAtmUnlabTrafVpi =
        (INT4) (pLdpEntity->pLdpAtmParams->u2UnLblTrafVpi);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmUnlabTrafVci
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmUnlabTrafVci
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmUnlabTrafVci (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    INT4 *pi4RetValMplsLdpEntityAtmUnlabTrafVci)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityAtmUnlabTrafVci =
        (INT4) (pLdpEntity->pLdpAtmParams->u2UnLblTrafVci);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmStorageType (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   INT4 *pi4RetValMplsLdpEntityAtmStorageType)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityAtmStorageType =
        (INT4) (pLdpEntity->pLdpAtmParams->u1StorageType);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                retValMplsLdpEntityAtmRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                 UINT4 u4MplsLdpEntityIndex,
                                 INT4 *pi4RetValMplsLdpEntityAtmRowStatus)
{
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValMplsLdpEntityAtmRowStatus =
        (INT4) (pLdpEntity->pLdpAtmParams->u1RowStatus);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmIfIndexOrZero
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAtmIfIndexOrZero
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmIfIndexOrZero (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     INT4 i4SetValMplsLdpEntityAtmIfIndexOrZero)
{
    tLdpAtmParams      *pLdpAtmParams = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    tLdpEntity         *pLdpEntity = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        pLdpAtmParams = (tLdpAtmParams *) (pLdpEntity->pLdpAtmParams);

        if (pLdpAtmParams != NULL)
        {
            if (((pLdpAtmParams->u1RowStatus) == NOT_READY) ||
                ((pLdpAtmParams->u1RowStatus) == NOT_IN_SERVICE))
            {
                pLdpAtmParams->u4IfIndex =
                    (UINT4) i4SetValMplsLdpEntityAtmIfIndexOrZero;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmMergeCap
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAtmMergeCap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmMergeCap (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 i4SetValMplsLdpEntityAtmMergeCap)
{
    tLdpAtmParams      *pLdpAtmParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        pLdpAtmParams = (tLdpAtmParams *) (pLdpEntity->pLdpAtmParams);
        if (pLdpAtmParams != NULL)
        {
            if (((pLdpAtmParams->u1RowStatus) == NOT_READY) ||
                ((pLdpAtmParams->u1RowStatus) == NOT_IN_SERVICE))
            {
                pLdpAtmParams->u1MergeType =
                    (UINT1) i4SetValMplsLdpEntityAtmMergeCap;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmVcDirectionality
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAtmVcDirectionality
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmVcDirectionality (tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        INT4
                                        i4SetValMplsLdpEntityAtmVcDirectionality)
{
    tLdpAtmParams      *pLdpAtmParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        pLdpAtmParams = (tLdpAtmParams *) (pLdpEntity->pLdpAtmParams);

        if (pLdpAtmParams != NULL)
        {
            if (((pLdpAtmParams->u1RowStatus) == NOT_READY) ||
                ((pLdpAtmParams->u1RowStatus) == NOT_IN_SERVICE))
            {
                pLdpAtmParams->u1VcDirection =
                    (UINT1) i4SetValMplsLdpEntityAtmVcDirectionality;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmLsrConnectivity
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAtmLsrConnectivity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmLsrConnectivity (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       i4SetValMplsLdpEntityAtmLsrConnectivity)
{
    tLdpAtmParams      *pLdpAtmParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        pLdpAtmParams = (tLdpAtmParams *) (pLdpEntity->pLdpAtmParams);

        if (pLdpAtmParams != NULL)
        {
            if (((pLdpAtmParams->u1RowStatus) == NOT_READY) ||
                ((pLdpAtmParams->u1RowStatus) == NOT_IN_SERVICE))
            {
                pLdpAtmParams->u2Connectivity =
                    (UINT1) i4SetValMplsLdpEntityAtmLsrConnectivity;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmDefaultControlVpi
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAtmDefaultControlVpi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmDefaultControlVpi (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         i4SetValMplsLdpEntityAtmDefaultControlVpi)
{
    tLdpAtmParams      *pLdpAtmParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        pLdpAtmParams = (tLdpAtmParams *) (pLdpEntity->pLdpAtmParams);

        if (pLdpAtmParams != NULL)
        {
            if (((pLdpAtmParams->u1RowStatus) == NOT_READY) ||
                ((pLdpAtmParams->u1RowStatus) == NOT_IN_SERVICE))
            {
                pLdpAtmParams->u2EntityConfDefVpi =
                    (UINT2) i4SetValMplsLdpEntityAtmDefaultControlVpi;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmDefaultControlVci
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAtmDefaultControlVci
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmDefaultControlVci (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         i4SetValMplsLdpEntityAtmDefaultControlVci)
{
    tLdpAtmParams      *pLdpAtmParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        pLdpAtmParams = (tLdpAtmParams *) (pLdpEntity->pLdpAtmParams);

        if (pLdpAtmParams != NULL)
        {
            if (((pLdpAtmParams->u1RowStatus) == NOT_READY) ||
                ((pLdpAtmParams->u1RowStatus) == NOT_IN_SERVICE))
            {
                pLdpAtmParams->u2EntityConfDefVci =
                    (UINT2) i4SetValMplsLdpEntityAtmDefaultControlVci;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmUnlabTrafVpi
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAtmUnlabTrafVpi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmUnlabTrafVpi (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    INT4 i4SetValMplsLdpEntityAtmUnlabTrafVpi)
{
    tLdpAtmParams      *pLdpAtmParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        pLdpAtmParams = (tLdpAtmParams *) (pLdpEntity->pLdpAtmParams);

        if (pLdpAtmParams != NULL)
        {
            if (((pLdpAtmParams->u1RowStatus) == NOT_READY) ||
                ((pLdpAtmParams->u1RowStatus) == NOT_IN_SERVICE))
            {
                pLdpAtmParams->u2UnLblTrafVpi =
                    (UINT2) i4SetValMplsLdpEntityAtmUnlabTrafVpi;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmUnlabTrafVci
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAtmUnlabTrafVci
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmUnlabTrafVci (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    INT4 i4SetValMplsLdpEntityAtmUnlabTrafVci)
{
    tLdpAtmParams      *pLdpAtmParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        pLdpAtmParams = (tLdpAtmParams *) (pLdpEntity->pLdpAtmParams);

        if (pLdpAtmParams != NULL)
        {
            if (((pLdpAtmParams->u1RowStatus) == NOT_READY) ||
                ((pLdpAtmParams->u1RowStatus) == NOT_IN_SERVICE))
            {
                pLdpAtmParams->u2UnLblTrafVci =
                    (UINT2) i4SetValMplsLdpEntityAtmUnlabTrafVci;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAtmStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmStorageType (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   INT4 i4SetValMplsLdpEntityAtmStorageType)
{
    tLdpAtmParams      *pLdpAtmParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1              *pu1LdpEntityId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }

    if ((LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_READY) ||
        (LDP_ENTITY_ROW_STATUS (pLdpEntity) == NOT_IN_SERVICE))
    {
        pLdpAtmParams = (tLdpAtmParams *) (pLdpEntity->pLdpAtmParams);
        if (pLdpAtmParams != NULL)
        {
            if (((pLdpAtmParams->u1RowStatus) == NOT_READY) ||
                ((pLdpAtmParams->u1RowStatus) == NOT_IN_SERVICE))
            {
                pLdpAtmParams->u1StorageType =
                    (UINT1) i4SetValMplsLdpEntityAtmStorageType;
                return SNMP_SUCCESS;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                setValMplsLdpEntityAtmRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                 UINT4 u4MplsLdpEntityIndex,
                                 INT4 i4SetValMplsLdpEntityAtmRowStatus)
{
    UINT2               u2IncarnId = (UINT2) (LDP_MIN_INCARN - 1);
    tLdpAtmParams      *pLdpAtmParams = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tAtmLdpLblRngEntry *pAtmLblRngEntry = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    tLdpIfTableEntry   *pLdpIfTableEntry = NULL;
    UINT4               u4IfAddr;
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity ((UINT2) (LDP_MIN_INCARN - 1), pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }

    if (LDP_ENTITY_ROW_STATUS (pLdpEntity) == ACTIVE)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValMplsLdpEntityAtmRowStatus)
    {

        case ACTIVE:
            if (pLdpEntity->pLdpAtmParams == NULL)
            {
                return SNMP_FAILURE;
            }
            pLdpAtmParams = pLdpEntity->pLdpAtmParams;
            if (pLdpEntity->pLdpAtmParams->u1RowStatus == ACTIVE)
            {
                return SNMP_SUCCESS;
            }
            if ((TMO_SLL_Count (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity))
                 == LDP_ZERO) ||
                (TMO_SLL_Count (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity))
                 != pLdpEntity->pLdpAtmParams->u2NumOfLblRnges))
            {
                /* Atleast one Atm Label range Entry has to be configured for
                 * the Atm Params and it must be equal to no of ranges 
                 * configured for the row status to be made active
                 */
                return SNMP_FAILURE;
            }
            TMO_SLL_Scan (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity),
                          pAtmLblRngEntry, tAtmLdpLblRngEntry *)
            {
                if (pAtmLblRngEntry->u1RowStatus != ACTIVE)
                {
                    return SNMP_FAILURE;
                }
            }
            if (pLdpAtmParams->u4IfIndex == LDP_ZERO)
            {
                return SNMP_FAILURE;
            }
            pLdpIfTableEntry = (tLdpIfTableEntry *)
                MemAllocMemBlk (LDP_EIF_POOL_ID);
            if (pLdpIfTableEntry == NULL)
            {
                LDP_DBG ((LDP_IF_SNMP | LDP_IF_MEM),
                         "Interface structure allocation failed \n");
                return SNMP_FAILURE;
            }
            MEMSET ((VOID *) pLdpIfTableEntry, LDP_ZERO,
                    sizeof (tLdpIfTableEntry));
            TMO_SLL_Init_Node (&(pLdpIfTableEntry->NextIfEntry));
            pLdpIfTableEntry->u4IfIndex = pLdpAtmParams->u4IfIndex;
            if (LdpIpGetIfAddr (pLdpIfTableEntry->u4IfIndex, &u4IfAddr) ==
                LDP_FAILURE)
            {
                MemReleaseMemBlock (LDP_EIF_POOL_ID,
                                    (UINT1 *) pLdpIfTableEntry);
                return SNMP_FAILURE;
            }
            MEMCPY ((UINT1 *) &pLdpIfTableEntry->IfAddr,
                    (UINT1 *) &u4IfAddr, LDP_IPV4ADR_LEN);
            /* The Speed should be ideally filled in by consulting the
             * the Ip interface information
             * eg ATM(155), Ethernet(10-100).
             */
            if (LDP_GET_IFACE_PD (u2IncarnId, pLdpAtmParams->u4IfIndex) == 0)
            {
                LDP_SET_IFACE_SPEED (u2IncarnId, pLdpAtmParams->u4IfIndex,
                                     LDP_ATM_IF_SPEED);
            }
            /* IfList is added to the SLL List */
            TMO_SLL_Add ((tTMO_SLL *) (&(pLdpEntity->IfList)),
                         (tTMO_SLL_NODE *) pLdpIfTableEntry);
            /* End NOTE */

            pLdpEntity->pLdpAtmParams->u1RowStatus = ACTIVE;
            return SNMP_SUCCESS;

        case NOT_IN_SERVICE:

            if (LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE)
            {
                if (pLdpEntity->pLdpAtmParams == NULL)
                {
                    return SNMP_FAILURE;
                }
                pLdpAtmParams = pLdpEntity->pLdpAtmParams;
                if ((pLdpEntity->pLdpAtmParams->u1RowStatus != NOT_IN_SERVICE)
                    && (pLdpEntity->pLdpAtmParams->u1RowStatus != ACTIVE))
                {
                    return SNMP_FAILURE;
                }

                TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity),
                              pLdpIfTableEntry, tLdpIfTableEntry *)
                {
                    if ((pLdpIfTableEntry->u4IfIndex) ==
                        (pLdpAtmParams->u4IfIndex))
                    {
                        TMO_SLL_Delete ((tTMO_SLL *) (&pLdpEntity->IfList),
                                        (tTMO_SLL_NODE *) pLdpIfTableEntry);
                        MemReleaseMemBlock (LDP_EIF_POOL_ID,
                                            (UINT1 *) (pLdpIfTableEntry));
                    }
                }

                pLdpEntity->pLdpAtmParams->u1RowStatus = NOT_IN_SERVICE;
                return SNMP_SUCCESS;
            }
            break;

        case DESTROY:

            if (LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE)
            {

                if (pLdpEntity->pLdpAtmParams == NULL)
                {
                    return SNMP_FAILURE;
                }
                pLdpAtmParams = pLdpEntity->pLdpAtmParams;
                /* Before Deleting the Atm Params any label Ranges 
                 * configured under this structure are released 
                 */
                pAtmLblRngEntry = (tAtmLdpLblRngEntry *)
                    TMO_SLL_First (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity));

                while (pAtmLblRngEntry != NULL)
                {
                    TMO_SLL_Delete ((tTMO_SLL *) (LDP_ENTITY_ATM_LBL_RANGE_LIST
                                                  (pLdpEntity)),
                                    (tTMO_SLL_NODE *) pAtmLblRngEntry);
                    MemReleaseMemBlock (LDP_ATM_LBL_RNG_POOL_ID,
                                        (UINT1 *) (pAtmLblRngEntry));
                    pAtmLblRngEntry = (tAtmLdpLblRngEntry *)
                        TMO_SLL_First (LDP_ENTITY_ATM_LBL_RANGE_LIST
                                       (pLdpEntity));
                }

                TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity),
                              pLdpIfTableEntry, tLdpIfTableEntry *)
                {
                    if ((pLdpIfTableEntry->u4IfIndex) ==
                        (pLdpAtmParams->u4IfIndex))
                    {
                        TMO_SLL_Delete ((tTMO_SLL *) (&pLdpEntity->IfList),
                                        (tTMO_SLL_NODE *) pLdpIfTableEntry);
                        MemReleaseMemBlock (LDP_EIF_POOL_ID,
                                            (UINT1 *) (pLdpIfTableEntry));
                    }
                }

                MemReleaseMemBlock (LDP_ATM_PARMS_POOL_ID,
                                    (UINT1 *) (pLdpAtmParams));
                pLdpEntity->pLdpAtmParams = NULL;
                return SNMP_SUCCESS;
            }
            break;

        case CREATE_AND_WAIT:

            /* Create&wait and Creata&go are treated the same way. */
            if (pLdpEntity->pLdpAtmParams != NULL)
            {
                return SNMP_FAILURE;
            }
            pLdpAtmParams = (tLdpAtmParams *)
                MemAllocMemBlk (LDP_ATM_PARMS_POOL_ID);
            if (pLdpAtmParams == NULL)
            {
                LDP_DBG ((LDP_IF_SNMP | LDP_IF_MEM),
                         "Couldn't allocate memory for Atm params\n");
                return SNMP_FAILURE;
            }

            MEMSET ((VOID *) pLdpAtmParams, LDP_ZERO, sizeof (tLdpAtmParams));
            pLdpEntity->pLdpAtmParams = pLdpAtmParams;
            pLdpEntity->pLdpAtmParams->u2Connectivity = LDP_ATM_DIR_CONN;
            TMO_SLL_Init (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity));
            pLdpAtmParams->u1RowStatus = NOT_READY;
            return SNMP_SUCCESS;

        case CREATE_AND_GO:
            return SNMP_FAILURE;

        default:
            break;
    }
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmIfIndexOrZero
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAtmIfIndexOrZero
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmIfIndexOrZero (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        INT4
                                        i4TestValMplsLdpEntityAtmIfIndexOrZero)
{
    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);

    if ((i4TestValMplsLdpEntityAtmIfIndexOrZero < LDP_ONE)
        || (i4TestValMplsLdpEntityAtmIfIndexOrZero > LDP_DEF_MAX_IFACES))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmMergeCap
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAtmMergeCap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmMergeCap (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   INT4 i4TestValMplsLdpEntityAtmMergeCap)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);

    switch (i4TestValMplsLdpEntityAtmMergeCap)
    {
        case LDP_NO_MRG:
        case LDP_VC_MRG:
        case LDP_VP_MRG:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmVcDirectionality
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAtmVcDirectionality
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmVcDirectionality (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           INT4
                                           i4TestValMplsLdpEntityAtmVcDirectionality)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);

    switch (i4TestValMplsLdpEntityAtmVcDirectionality)
    {
        case LDP_VC_BI_DIR:
        case LDP_VC_UNI_DIR:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmLsrConnectivity
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAtmLsrConnectivity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmLsrConnectivity (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          INT4
                                          i4TestValMplsLdpEntityAtmLsrConnectivity)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);

    switch (i4TestValMplsLdpEntityAtmLsrConnectivity)
    {
        case LDP_ATM_DIR_CONN:
            break;

        case LDP_ATM_INDIR_CONN:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmDefaultControlVpi
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAtmDefaultControlVpi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmDefaultControlVpi (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            INT4
                                            i4TestValMplsLdpEntityAtmDefaultControlVpi)
{
    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);

    if ((i4TestValMplsLdpEntityAtmDefaultControlVpi >= LDP_ENTITY_MIN_VPI) &&
        (i4TestValMplsLdpEntityAtmDefaultControlVpi <= LDP_ENTITY_MAX_VPI))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmDefaultControlVci
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAtmDefaultControlVci
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmDefaultControlVci (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            INT4
                                            i4TestValMplsLdpEntityAtmDefaultControlVci)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);

    if ((i4TestValMplsLdpEntityAtmDefaultControlVci >= LDP_ENTITY_MIN_VCI) &&
        (i4TestValMplsLdpEntityAtmDefaultControlVci <= LDP_ENTITY_MAX_VCI))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmUnlabTrafVpi
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAtmUnlabTrafVpi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmUnlabTrafVpi (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       i4TestValMplsLdpEntityAtmUnlabTrafVpi)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);

    if ((i4TestValMplsLdpEntityAtmUnlabTrafVpi >= LDP_ENTITY_MIN_VPI) &&
        (i4TestValMplsLdpEntityAtmUnlabTrafVpi <= LDP_ENTITY_MAX_VPI))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmUnlabTrafVci
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAtmUnlabTrafVci
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmUnlabTrafVci (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       i4TestValMplsLdpEntityAtmUnlabTrafVci)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);

    if ((i4TestValMplsLdpEntityAtmUnlabTrafVci >= LDP_ENTITY_MIN_VCI) &&
        (i4TestValMplsLdpEntityAtmUnlabTrafVci <= LDP_ENTITY_MAX_VCI))
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAtmStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmStorageType (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 u4MplsLdpEntityIndex,
                                      INT4 i4TestValMplsLdpEntityAtmStorageType)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);

    switch (i4TestValMplsLdpEntityAtmStorageType)
    {
        case LDP_STORAGE_VOLATILE:
        case LDP_STORAGE_NONVOLATILE:
        case LDP_STORAGE_OTHER:
        case LDP_STORAGE_PERMANENT:
        case LDP_STORAGE_READONLY:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex

                The Object 
                testValMplsLdpEntityAtmRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmRowStatus (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    INT4 i4TestValMplsLdpEntityAtmRowStatus)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);

    switch (i4TestValMplsLdpEntityAtmRowStatus)
    {
        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:
        case CREATE_AND_WAIT:
            break;

        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

            /*case CREATE_AND_WAIT:
               case CREATE_AND_GO:
               break; */

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsLdpEntityAtmLRTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpEntityAtmLRTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpEntityAtmLRTable (tSNMP_OCTET_STRING_TYPE *
                                                 pMplsLdpEntityLdpId,
                                                 UINT4 u4MplsLdpEntityIndex,
                                                 INT4
                                                 i4MplsLdpEntityAtmLRMinVpi,
                                                 INT4
                                                 i4MplsLdpEntityAtmLRMinVci)
{

    if (nmhValidateIndexInstanceFsMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                                      u4MplsLdpEntityIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (((i4MplsLdpEntityAtmLRMinVpi) < LDP_ENTITY_MIN_VPI) ||
        ((i4MplsLdpEntityAtmLRMinVpi) > LDP_ENTITY_MAX_VPI) ||
        ((i4MplsLdpEntityAtmLRMinVci) < LDP_ENTITY_MIN_VCI) ||
        ((i4MplsLdpEntityAtmLRMinVci) > LDP_ENTITY_MAX_VCI))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpEntityAtmLRTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpEntityAtmLRTable (tSNMP_OCTET_STRING_TYPE *
                                         pMplsLdpEntityLdpId,
                                         UINT4 *pu4MplsLdpEntityIndex,
                                         INT4 *pi4MplsLdpEntityAtmLRMinVpi,
                                         INT4 *pi4MplsLdpEntityAtmLRMinVci)
{

    UINT4              *pu4MplsLdpLsrIncarnId;
    UINT4               u4Var;
    UINT1               u1GetFirst = LDP_TRUE;
    UINT1               u1FirstLoopEntry = LDP_TRUE;
    UINT1               u1ValidEntityFound = LDP_FALSE;
    UINT4               u4MplsLdpEntityIndex = 0;
    tLdpEntity         *pLdpEntity = NULL;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
    tSNMP_OCTET_STRING_TYPE FsMplsLdpEntityID;
    UINT1               OctectVal[16];
    UINT1              *pu1LdpEntityId = SNMP_OCTET_STRING_LIST
        (pMplsLdpEntityLdpId);

    FsMplsLdpEntityID.pu1_OctetList = OctectVal;

    pu4MplsLdpLsrIncarnId = &u4Var;
    *pu4MplsLdpLsrIncarnId = LDP_ZERO;
    *pu4MplsLdpEntityIndex = LDP_ZERO;

    *pi4MplsLdpEntityAtmLRMinVpi = LDP_ZERO;
    *pi4MplsLdpEntityAtmLRMinVci = LDP_ZERO;

    while (u1GetFirst == LDP_TRUE)
    {
        if (u1FirstLoopEntry == LDP_TRUE)
        {
            if (nmhGetFirstIndexFsMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                                      pu4MplsLdpEntityIndex)
                == SNMP_FAILURE)
            {
                return SNMP_FAILURE;
            }
            u1FirstLoopEntry = LDP_FALSE;
        }
        /* Entering the loop for more than once */
        else if (nmhGetNextIndexFsMplsLdpEntityTable ((tSNMP_OCTET_STRING_TYPE
                                                       *) & FsMplsLdpEntityID,
                                                      pMplsLdpEntityLdpId,
                                                      u4MplsLdpEntityIndex,
                                                      pu4MplsLdpEntityIndex)
                 == SNMP_FAILURE)
        {
            return SNMP_FAILURE;

        }
        CPY_TO_SNMP (&FsMplsLdpEntityID,
                     (UINT1 *) pu1LdpEntityId, LDP_MAX_LDPID_LEN);

        u4MplsLdpEntityIndex = *pu4MplsLdpEntityIndex;
        /* Getting the Ldp Entity for the given indices. 
         * Failure case in getting ldp entity will never arise as getentity 
         * is done after GetNextIndex. GetNextIndex always returns a valid 
         * index of an existing Entity. */

        LdpGetLdpEntity ((*pu4MplsLdpLsrIncarnId - 1), pu1LdpEntityId,
                         *pu4MplsLdpEntityIndex, &pLdpEntity);

        if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
        {
            continue;
        }
        /* Only when the Label space type is per platform, it is an ATM entity
         * only then the ATM Label Range List is accessed. */
        if (pLdpEntity->pLdpAtmParams == NULL)
        {
            continue;
        }

        TMO_SLL_Scan (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity),
                      pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
        {
            if (u1ValidEntityFound == LDP_FALSE)
            {
                *pi4MplsLdpEntityAtmLRMinVpi = pAtmLblRangeEntry->u2LBVpi;
                *pi4MplsLdpEntityAtmLRMinVci = pAtmLblRangeEntry->u2LBVci;
                u1ValidEntityFound = LDP_TRUE;
            }
            if (u1ValidEntityFound == LDP_TRUE)
            {
                if (((pAtmLblRangeEntry->u2LBVpi ==
                      *pi4MplsLdpEntityAtmLRMinVpi) &&
                     (pAtmLblRangeEntry->u2LBVci <
                      *pi4MplsLdpEntityAtmLRMinVci)) ||
                    (pAtmLblRangeEntry->u2LBVpi < *pi4MplsLdpEntityAtmLRMinVpi))
                {
                    *pi4MplsLdpEntityAtmLRMinVpi = pAtmLblRangeEntry->u2LBVpi;
                    *pi4MplsLdpEntityAtmLRMinVci = pAtmLblRangeEntry->u2LBVci;
                }
            }
        }
        if (u1ValidEntityFound == LDP_TRUE)
        {
            return SNMP_SUCCESS;
        }
        /* If No Valid Entity is found, then the next loop entry would 
           result in returning SNMP_FAILURE while trying to get next valid 
           Ldp ENtity. */
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpEntityAtmLRTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                nextMplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci
                nextMplsLdpEntityAtmLRMinVci
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpEntityAtmLRTable (tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpEntityLdpId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        UINT4 *pu4NextMplsLdpEntityIndex,
                                        INT4 i4MplsLdpEntityAtmLRMinVpi,
                                        INT4 *pi4NextMplsLdpEntityAtmLRMinVpi,
                                        INT4 i4MplsLdpEntityAtmLRMinVci,
                                        INT4 *pi4NextMplsLdpEntityAtmLRMinVci)
{
    UINT4               u4MplsLdpLsrIncarnId = 0;
    UINT1               u1ValidEntityFound = LDP_FALSE;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1               u1GetNext = LDP_TRUE;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1NextLdpEntityId = NULL;

    *pu4NextMplsLdpEntityIndex = LDP_ZERO;

    *pi4NextMplsLdpEntityAtmLRMinVpi = LDP_ZERO;
    *pi4NextMplsLdpEntityAtmLRMinVci = LDP_ZERO;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1NextLdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);

    /* Getting the Ldp Entity for the given valid indices */
    if (LdpGetLdpEntity
        (u4MplsLdpLsrIncarnId, pu1LdpEntityId, u4MplsLdpEntityIndex,
         &pLdpEntity) == LDP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams != NULL)
    {

        TMO_SLL_Scan (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity),
                      pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
        {
            if (((pAtmLblRangeEntry->u2LBVpi ==
                  (UINT2) (i4MplsLdpEntityAtmLRMinVpi)) &&
                 (pAtmLblRangeEntry->u2LBVci >
                  (UINT2) i4MplsLdpEntityAtmLRMinVci)) ||
                (pAtmLblRangeEntry->u2LBVpi >
                 (UINT2) (i4MplsLdpEntityAtmLRMinVpi)))
            {
                if (u1ValidEntityFound == LDP_FALSE)
                {
                    *pi4NextMplsLdpEntityAtmLRMinVpi =
                        (INT4) (pAtmLblRangeEntry->u2LBVpi);
                    *pi4NextMplsLdpEntityAtmLRMinVci =
                        (INT4) (pAtmLblRangeEntry->u2LBVci);
                    u1ValidEntityFound = LDP_TRUE;
                    continue;
                }
                if (((pAtmLblRangeEntry->u2LBVpi ==
                      (UINT2)
                      (*pi4NextMplsLdpEntityAtmLRMinVpi))
                     && (pAtmLblRangeEntry->u2LBVci <
                         (UINT2)
                         (*pi4NextMplsLdpEntityAtmLRMinVci)))
                    || (pAtmLblRangeEntry->u2LBVpi <
                        (UINT2) (*pi4NextMplsLdpEntityAtmLRMinVpi)))
                {

                    *pi4NextMplsLdpEntityAtmLRMinVpi =
                        (INT4) (pAtmLblRangeEntry->u2LBVpi);
                    *pi4NextMplsLdpEntityAtmLRMinVci =
                        (INT4) (pAtmLblRangeEntry->u2LBVci);
                }
            }
        }
    }
    if (u1ValidEntityFound == LDP_TRUE)
    {
        CPY_TO_SNMP (pNextMplsLdpEntityLdpId, pu1LdpEntityId,
                     LDP_MAX_LDPID_LEN);
        *pu4NextMplsLdpEntityIndex = u4MplsLdpEntityIndex;
        return SNMP_SUCCESS;
    }
    /* Only one Label Range Entry is assumed to be present per Ldp Entity. 
       Hence getting the next valid Ldp Entity and checking if it contains 
       a valid Atm Label Ramge Entry. */

    while (u1GetNext == LDP_TRUE)
    {

        /* Getting the valid next Ldp Entity's indices */
        if (nmhGetNextIndexFsMplsLdpEntityTable (pMplsLdpEntityLdpId,
                                                 pNextMplsLdpEntityLdpId,
                                                 u4MplsLdpEntityIndex,
                                                 pu4NextMplsLdpEntityIndex)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        CPY_TO_SNMP (pMplsLdpEntityLdpId,
                     (UINT1 *) pu1NextLdpEntityId, LDP_MAX_LDPID_LEN);
        u4MplsLdpEntityIndex = *pu4NextMplsLdpEntityIndex;
        /* Getting the Ldp Entity for the given valid indices */
        if (LdpGetLdpEntity (u4MplsLdpLsrIncarnId,
                             pu1NextLdpEntityId,
                             *pu4NextMplsLdpEntityIndex,
                             &pLdpEntity) == LDP_FAILURE)
        {
            return SNMP_FAILURE;
        }

        if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
        {
            continue;
        }
        /* Only when the Label space type is per platform, it is an ATM entity
         * only then the ATM Label Range List is accessed. */
        if (pLdpEntity->pLdpAtmParams == NULL)
        {
            continue;
        }

        TMO_SLL_Scan (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity),
                      pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
        {
            if (u1ValidEntityFound == LDP_FALSE)
            {
                *pi4NextMplsLdpEntityAtmLRMinVpi = pAtmLblRangeEntry->u2LBVpi;
                *pi4NextMplsLdpEntityAtmLRMinVci = pAtmLblRangeEntry->u2LBVci;
                u1ValidEntityFound = LDP_TRUE;
                continue;
            }
            if (((pAtmLblRangeEntry->u2LBVpi ==
                  *pi4NextMplsLdpEntityAtmLRMinVpi) &&
                 (pAtmLblRangeEntry->u2LBVci <
                  *pi4NextMplsLdpEntityAtmLRMinVci)) ||
                (pAtmLblRangeEntry->u2LBVpi < *pi4NextMplsLdpEntityAtmLRMinVpi))
            {
                *pi4NextMplsLdpEntityAtmLRMinVpi = pAtmLblRangeEntry->u2LBVpi;
                *pi4NextMplsLdpEntityAtmLRMinVci = pAtmLblRangeEntry->u2LBVci;
            }
        }
        if (u1ValidEntityFound == LDP_TRUE)
        {
            return SNMP_SUCCESS;
        }
    }
    /* If No Valid Entity is found, then the next loop entry would 
     * result in returning SNMP_FAILURE while trying to get next valid 
     * Ldp ENtity. */
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmLRMaxVpi
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                retValMplsLdpEntityAtmLRMaxVpi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmLRMaxVpi (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 i4MplsLdpEntityAtmLRMinVpi,
                                INT4 i4MplsLdpEntityAtmLRMinVci,
                                INT4 *pi4RetValMplsLdpEntityAtmLRMaxVpi)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (LDP_MIN_INCARN - 1, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    pList = LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity);

    TMO_SLL_Scan (pList, pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
    {
        if ((pAtmLblRangeEntry->u2LBVpi ==
             (UINT2) i4MplsLdpEntityAtmLRMinVpi) &&
            (pAtmLblRangeEntry->u2LBVci == (UINT2) i4MplsLdpEntityAtmLRMinVci))
        {
            *pi4RetValMplsLdpEntityAtmLRMaxVpi =
                (INT4) pAtmLblRangeEntry->u2UBVpi;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmLRMaxVci
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                retValMplsLdpEntityAtmLRMaxVci
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmLRMaxVci (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 i4MplsLdpEntityAtmLRMinVpi,
                                INT4 i4MplsLdpEntityAtmLRMinVci,
                                INT4 *pi4RetValMplsLdpEntityAtmLRMaxVci)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (LDP_MIN_INCARN - 1, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    pList = LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity);

    TMO_SLL_Scan (pList, pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
    {
        if ((pAtmLblRangeEntry->u2LBVpi ==
             (UINT2) i4MplsLdpEntityAtmLRMinVpi) &&
            (pAtmLblRangeEntry->u2LBVci == (UINT2) i4MplsLdpEntityAtmLRMinVci))
        {
            *pi4RetValMplsLdpEntityAtmLRMaxVci =
                (INT4) pAtmLblRangeEntry->u2UBVci;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmLRStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                retValMplsLdpEntityAtmLRStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmLRStorageType (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     INT4 i4MplsLdpEntityAtmLRMinVpi,
                                     INT4 i4MplsLdpEntityAtmLRMinVci,
                                     INT4
                                     *pi4RetValMplsLdpEntityAtmLRStorageType)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (LDP_MIN_INCARN - 1, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    pList = LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity);

    TMO_SLL_Scan (pList, pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
    {
        if ((pAtmLblRangeEntry->u2LBVpi ==
             (UINT2) i4MplsLdpEntityAtmLRMinVpi) &&
            (pAtmLblRangeEntry->u2LBVci == (UINT2) i4MplsLdpEntityAtmLRMinVci))
        {
            *pi4RetValMplsLdpEntityAtmLRStorageType =
                (INT4) pAtmLblRangeEntry->u1StorageType;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpEntityAtmLRRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                retValMplsLdpEntityAtmLRRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpEntityAtmLRRowStatus (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   INT4 i4MplsLdpEntityAtmLRMinVpi,
                                   INT4 i4MplsLdpEntityAtmLRMinVci,
                                   INT4 *pi4RetValMplsLdpEntityAtmLRRowStatus)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
    UINT1              *pu1LdpEntityId =
        SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (LDP_MIN_INCARN - 1, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }

    pList = LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity);

    TMO_SLL_Scan (pList, pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
    {
        if ((pAtmLblRangeEntry->u2LBVpi ==
             (UINT2) i4MplsLdpEntityAtmLRMinVpi) &&
            (pAtmLblRangeEntry->u2LBVci == (UINT2) i4MplsLdpEntityAtmLRMinVci))
        {
            *pi4RetValMplsLdpEntityAtmLRRowStatus =
                (INT4) pAtmLblRangeEntry->u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmLRMaxVpi
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                setValMplsLdpEntityAtmLRMaxVpi
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmLRMaxVpi (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 i4MplsLdpEntityAtmLRMinVpi,
                                INT4 i4MplsLdpEntityAtmLRMinVci,
                                INT4 i4SetValMplsLdpEntityAtmLRMaxVpi)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (LDP_MIN_INCARN - 1, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams->u1RowStatus == ACTIVE)
    {
        return SNMP_FAILURE;
    }

    pList = LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity);

    TMO_SLL_Scan (pList, pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
    {
        if ((pAtmLblRangeEntry->u2LBVpi ==
             (UINT2) i4MplsLdpEntityAtmLRMinVpi) &&
            (pAtmLblRangeEntry->u2LBVci == (UINT2) i4MplsLdpEntityAtmLRMinVci))
        {
            if ((pAtmLblRangeEntry->u1RowStatus == NOT_READY) ||
                (pAtmLblRangeEntry->u1RowStatus == NOT_IN_SERVICE))
            {
                if (pAtmLblRangeEntry->u2LBVpi >
                    i4SetValMplsLdpEntityAtmLRMaxVpi)
                {
                    return SNMP_FAILURE;
                }
                else
                {
                    pAtmLblRangeEntry->u2UBVpi =
                        (UINT2) i4SetValMplsLdpEntityAtmLRMaxVpi;
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmLRMaxVci
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                setValMplsLdpEntityAtmLRMaxVci
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmLRMaxVci (tSNMP_OCTET_STRING_TYPE * pMplsLdpEntityLdpId,
                                UINT4 u4MplsLdpEntityIndex,
                                INT4 i4MplsLdpEntityAtmLRMinVpi,
                                INT4 i4MplsLdpEntityAtmLRMinVci,
                                INT4 i4SetValMplsLdpEntityAtmLRMaxVci)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (LDP_MIN_INCARN - 1, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams->u1RowStatus == ACTIVE)
    {
        return SNMP_FAILURE;
    }

    pList = LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity);

    TMO_SLL_Scan (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity),
                  pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
    {
        if ((pAtmLblRangeEntry->u2LBVpi ==
             (UINT2) i4MplsLdpEntityAtmLRMinVpi) &&
            (pAtmLblRangeEntry->u2LBVci == (UINT2) i4MplsLdpEntityAtmLRMinVci))
        {
            if ((pAtmLblRangeEntry->u1RowStatus == NOT_READY) ||
                (pAtmLblRangeEntry->u1RowStatus == NOT_IN_SERVICE))

            {
                if (pAtmLblRangeEntry->u2LBVci >
                    i4SetValMplsLdpEntityAtmLRMaxVci)
                {
                    return SNMP_FAILURE;
                }
                else
                {
                    pAtmLblRangeEntry->u2UBVci =
                        (UINT2) i4SetValMplsLdpEntityAtmLRMaxVci;
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmLRStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                setValMplsLdpEntityAtmLRStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmLRStorageType (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     INT4 i4MplsLdpEntityAtmLRMinVpi,
                                     INT4 i4MplsLdpEntityAtmLRMinVci,
                                     INT4 i4SetValMplsLdpEntityAtmLRStorageType)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (LDP_MIN_INCARN - 1, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams->u1RowStatus == ACTIVE)
    {
        return SNMP_FAILURE;
    }

    pList = LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity);

    TMO_SLL_Scan (LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity),
                  pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
    {
        if ((pAtmLblRangeEntry->u2LBVpi ==
             (UINT2) i4MplsLdpEntityAtmLRMinVpi) &&
            (pAtmLblRangeEntry->u2LBVci == (UINT2) i4MplsLdpEntityAtmLRMinVci))
        {
            if ((pAtmLblRangeEntry->u1RowStatus == NOT_READY) ||
                (pAtmLblRangeEntry->u1RowStatus == NOT_IN_SERVICE))
            {
                pAtmLblRangeEntry->u1StorageType =
                    (UINT1) i4SetValMplsLdpEntityAtmLRStorageType;
                return SNMP_SUCCESS;
            }
            else
            {
                return SNMP_FAILURE;
            }
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLdpEntityAtmLRRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                setValMplsLdpEntityAtmLRRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLdpEntityAtmLRRowStatus (tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   INT4 i4MplsLdpEntityAtmLRMinVpi,
                                   INT4 i4MplsLdpEntityAtmLRMinVci,
                                   INT4 i4SetValMplsLdpEntityAtmLRRowStatus)
{
    UINT4               u4IncarnId = 0;
    UINT1               u1LabelRangePresent = LDP_FALSE;
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tAtmLdpLblRngEntry *pAtmLblRangeEntry = NULL;
    tAtmLdpLblRngEntry *pTmpAtmLblRangeEntry1 = NULL;
    tAtmLdpLblRngEntry *pTmpAtmLblRangeEntry2 = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);

    if (LdpGetLdpEntity (u4IncarnId, pu1LdpEntityId,
                         u4MplsLdpEntityIndex, &pLdpEntity) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    if (LDP_LBL_SPACE_TYPE (pLdpEntity) != LDP_PER_IFACE)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams == NULL)
    {
        return SNMP_FAILURE;
    }
    if (pLdpEntity->pLdpAtmParams->u1RowStatus == ACTIVE)
    {
        return SNMP_FAILURE;
    }
    pList = LDP_ENTITY_ATM_LBL_RANGE_LIST (pLdpEntity);

    TMO_SLL_Scan (pList, pAtmLblRangeEntry, tAtmLdpLblRngEntry *)
    {
        if ((pAtmLblRangeEntry->u2LBVpi ==
             (UINT2) i4MplsLdpEntityAtmLRMinVpi) &&
            (pAtmLblRangeEntry->u2LBVci == (UINT2) i4MplsLdpEntityAtmLRMinVci))
        {
            u1LabelRangePresent = LDP_TRUE;
            break;
        }
    }

    switch (i4SetValMplsLdpEntityAtmLRRowStatus)
    {
        case ACTIVE:
            if (u1LabelRangePresent == LDP_FALSE)
            {
                /* Atm Label Range Entry doesn't exist. */
                return SNMP_FAILURE;
            }
            if ((pAtmLblRangeEntry->u2LBVpi >
                 pAtmLblRangeEntry->u2UBVpi) ||
                (pAtmLblRangeEntry->u2LBVci > pAtmLblRangeEntry->u2UBVci))
            {
                /* The Atm Label range has misconfigured
                 * Max and Min values. */
                return SNMP_FAILURE;
            }
            TMO_SLL_Scan (pList, pTmpAtmLblRangeEntry1, tAtmLdpLblRngEntry *)
            {
                if ((pTmpAtmLblRangeEntry1->u1RowStatus != ACTIVE) &&
                    (pTmpAtmLblRangeEntry1 != pAtmLblRangeEntry))
                {
                    /* Checking for intersection only between active 
                     * rows and current row. */
                    continue;
                }
                TMO_SLL_Scan (pList, pTmpAtmLblRangeEntry2,
                              tAtmLdpLblRngEntry *)
                {
                    if ((pTmpAtmLblRangeEntry2->u1RowStatus != ACTIVE) &&
                        (pTmpAtmLblRangeEntry2 != pAtmLblRangeEntry))
                    {
                        continue;
                    }
                    if (pTmpAtmLblRangeEntry1 == pTmpAtmLblRangeEntry2)
                    {
                        continue;
                    }
                    if ((pTmpAtmLblRangeEntry1->u2LBVpi >
                         pTmpAtmLblRangeEntry2->u2UBVpi) ||
                        (pTmpAtmLblRangeEntry2->u2LBVpi >
                         pTmpAtmLblRangeEntry1->u2UBVpi))
                    {
                        /* Since Vpi Ranges are disjoint, no need to check
                         * for Vci ranges. */
                        continue;
                    }
                    if ((pTmpAtmLblRangeEntry1->u2LBVci >
                         pTmpAtmLblRangeEntry2->u2UBVci) ||
                        (pTmpAtmLblRangeEntry2->u2LBVci >
                         pTmpAtmLblRangeEntry1->u2UBVci))
                    {
                        /* Since Vpi ranges are not disjoint, Vci Ranges
                         * needs to be disjoint. */
                        continue;
                    }
                    return SNMP_FAILURE;
                }
            }
            pAtmLblRangeEntry->u1RowStatus = ACTIVE;
            break;

        case NOT_IN_SERVICE:
            if (u1LabelRangePresent == LDP_FALSE)
            {
                /* Atm Label Range Entry doesn't exist. */
                return SNMP_FAILURE;
            }
            else
            {
                if ((pAtmLblRangeEntry->u1RowStatus == NOT_IN_SERVICE) ||
                    (pAtmLblRangeEntry->u1RowStatus == ACTIVE))
                {
                    pAtmLblRangeEntry->u1RowStatus = NOT_IN_SERVICE;
                    return SNMP_SUCCESS;
                }
                else
                {
                    return SNMP_FAILURE;
                }
            }
            break;

        case CREATE_AND_WAIT:

            if (u1LabelRangePresent == LDP_TRUE)
            {
                /* Atm Label Range Entry already exists. 
                 * Entry cannot be created. */
                return SNMP_FAILURE;
            }
            /* Creating a new Atm Label Range Entry */
            pAtmLblRangeEntry = (tAtmLdpLblRngEntry *)
                MemAllocMemBlk (LDP_ATM_LBL_RNG_POOL_ID);
            if (pAtmLblRangeEntry == NULL)
            {
                LDP_DBG ((LDP_IF_SNMP | LDP_IF_MEM),
                         "Couldn't allocate memory for AtmLbl Range block\n");
                return SNMP_FAILURE;
            }
            TMO_SLL_Init_Node ((tTMO_SLL_NODE *) pAtmLblRangeEntry);
            MEMSET ((VOID *) pAtmLblRangeEntry, LDP_ZERO,
                    sizeof (tAtmLdpLblRngEntry));
            pAtmLblRangeEntry->u2LBVpi = (UINT2) i4MplsLdpEntityAtmLRMinVpi;
            pAtmLblRangeEntry->u2LBVci = (UINT2) i4MplsLdpEntityAtmLRMinVci;

            pAtmLblRangeEntry->u1RowStatus = NOT_READY;
            TMO_SLL_Insert (pList, NULL, (tTMO_SLL_NODE *) pAtmLblRangeEntry);
            break;

        case DESTROY:

            if (u1LabelRangePresent == LDP_FALSE)
            {
                /* Atm Label Range Entry for the given range doesn't exist */
                return SNMP_FAILURE;
            }
            else
            {
                /* Atm Label Range Entry exists. Delete the entry */
                TMO_SLL_Delete ((tTMO_SLL *) pList,
                                (tTMO_SLL_NODE *) pAtmLblRangeEntry);
                MemReleaseMemBlock (LDP_ATM_LBL_RNG_POOL_ID,
                                    (UINT1 *) (pAtmLblRangeEntry));
                return SNMP_SUCCESS;
            }
            break;

        case CREATE_AND_GO:
            return SNMP_FAILURE;

        default:
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmLRMaxVpi
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                testValMplsLdpEntityAtmLRMaxVpi
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmLRMaxVpi (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   INT4 i4MplsLdpEntityAtmLRMinVpi,
                                   INT4 i4MplsLdpEntityAtmLRMinVci,
                                   INT4 i4TestValMplsLdpEntityAtmLRMaxVpi)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4MplsLdpEntityAtmLRMinVpi);
    UNUSED_PARAM (i4MplsLdpEntityAtmLRMinVci);

    if ((i4TestValMplsLdpEntityAtmLRMaxVpi
         >= LDP_MAXVPI_MINVAL) &&
        (i4TestValMplsLdpEntityAtmLRMaxVpi <= LDP_MAXVPI_MAXVAL))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmLRMaxVci
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                testValMplsLdpEntityAtmLRMaxVci
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmLRMaxVci (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsLdpEntityLdpId,
                                   UINT4 u4MplsLdpEntityIndex,
                                   INT4 i4MplsLdpEntityAtmLRMinVpi,
                                   INT4 i4MplsLdpEntityAtmLRMinVci,
                                   INT4 i4TestValMplsLdpEntityAtmLRMaxVci)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4MplsLdpEntityAtmLRMinVpi);
    UNUSED_PARAM (i4MplsLdpEntityAtmLRMinVci);

    if ((i4TestValMplsLdpEntityAtmLRMaxVci
         >= LDP_MAXVCI_MINVAL) &&
        (i4TestValMplsLdpEntityAtmLRMaxVci <= LDP_MAXVCI_MAXVAL))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmLRStorageType
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                testValMplsLdpEntityAtmLRStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmLRStorageType (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        INT4 i4MplsLdpEntityAtmLRMinVpi,
                                        INT4 i4MplsLdpEntityAtmLRMinVci,
                                        INT4
                                        i4TestValMplsLdpEntityAtmLRStorageType)
{

    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4MplsLdpEntityAtmLRMinVpi);
    UNUSED_PARAM (i4MplsLdpEntityAtmLRMinVci);

    switch (i4TestValMplsLdpEntityAtmLRStorageType)
    {
        case LDP_STORAGE_VOLATILE:
        case LDP_STORAGE_NONVOLATILE:
        case LDP_STORAGE_OTHER:
        case LDP_STORAGE_PERMANENT:
        case LDP_STORAGE_READONLY:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLdpEntityAtmLRRowStatus
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci

                The Object 
                testValMplsLdpEntityAtmLRRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLdpEntityAtmLRRowStatus (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 u4MplsLdpEntityIndex,
                                      INT4 i4MplsLdpEntityAtmLRMinVpi,
                                      INT4 i4MplsLdpEntityAtmLRMinVci,
                                      INT4 i4TestValMplsLdpEntityAtmLRRowStatus)
{
    /* Added to suppress warning */

    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4MplsLdpEntityAtmLRMinVpi);
    UNUSED_PARAM (i4MplsLdpEntityAtmLRMinVci);

    switch (i4TestValMplsLdpEntityAtmLRRowStatus)
    {
        case ACTIVE:
        case DESTROY:
        case NOT_IN_SERVICE:
        case CREATE_AND_WAIT:
            break;

        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsLdpAtmSessionTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLdpAtmSessionTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpSessionAtmLRLowerBoundVpi
                MplsLdpSessionAtmLRLowerBoundVci
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLdpAtmSessionTable (tSNMP_OCTET_STRING_TYPE *
                                                pMplsLdpEntityLdpId,
                                                UINT4 u4MplsLdpEntityIndex,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pMplsLdpPeerLdpId,
                                                INT4
                                                i4MplsLdpSessionAtmLRLowerBoundVpi,
                                                INT4
                                                i4MplsLdpSessionAtmLRLowerBoundVci)
{

    if (nmhValidateIndexInstanceMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                                  u4MplsLdpEntityIndex,
                                                  pMplsLdpPeerLdpId)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (((i4MplsLdpSessionAtmLRLowerBoundVpi) < LDP_ENTITY_MIN_VPI) ||
        ((i4MplsLdpSessionAtmLRLowerBoundVpi) > LDP_ENTITY_MAX_VPI) ||
        ((i4MplsLdpSessionAtmLRLowerBoundVci) < LDP_ENTITY_MIN_VCI) ||
        ((i4MplsLdpSessionAtmLRLowerBoundVci) > LDP_ENTITY_MAX_VCI))
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLdpAtmSessionTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpSessionAtmLRLowerBoundVpi
                MplsLdpSessionAtmLRLowerBoundVci
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLdpAtmSessionTable (tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpEntityLdpId,
                                        UINT4 *pu4MplsLdpEntityIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpPeerLdpId,
                                        INT4
                                        *pi4MplsLdpSessionAtmLRLowerBoundVpi,
                                        INT4
                                        *pi4MplsLdpSessionAtmLRLowerBoundVci)
{
    UINT4              *pu4MplsLdpLsrIncarnId;
    UINT4               u4Var;
    UINT1               u1LblRngeFound = LDP_FALSE;
    UINT1               u1GetNext = LDP_TRUE;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    tAtmLdpLblRngEntry *pIntLblRngEntry = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    SNMP_DECLARE_TWO_OCTET_STRING (FsLdpEntityId, FsLdpPeerId)
        pu4MplsLdpLsrIncarnId = &u4Var;
    *pu4MplsLdpLsrIncarnId = LDP_ZERO;

    *pu4MplsLdpEntityIndex = LDP_ZERO;

    *pi4MplsLdpSessionAtmLRLowerBoundVpi = LDP_ZERO;
    *pi4MplsLdpSessionAtmLRLowerBoundVci = LDP_ZERO;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (nmhGetFirstIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                          pu4MplsLdpEntityIndex,
                                          pMplsLdpPeerLdpId) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if (LdpGetLdpPeer (*pu4MplsLdpLsrIncarnId - 1, pu1LdpEntityId,
                       *pu4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */

    if (pLdpPeer->pLdpSession != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }

    if (pLdpSession != NULL)
    {
        if (SSN_GET_LBL_TYPE (pLdpSession) == LDP_PER_IFACE)
        {
            TMO_SLL_Scan (SSN_GET_ATM_LBLRNG_LIST (pLdpSession),
                          pIntLblRngEntry, tAtmLdpLblRngEntry *)
            {
                pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
                if (u1LblRngeFound == LDP_FALSE)
                {
                    *pi4MplsLdpSessionAtmLRLowerBoundVpi =
                        (INT4) pIntLblRngEntry->u2LBVpi;
                    *pi4MplsLdpSessionAtmLRLowerBoundVci =
                        (INT4) pIntLblRngEntry->u2LBVci;
                    u1LblRngeFound = LDP_TRUE;
                    continue;
                }
                if (u1LblRngeFound == LDP_TRUE)
                {
                    if (((pIntLblRngEntry->u2LBVpi ==
                          *pi4MplsLdpSessionAtmLRLowerBoundVpi) &&
                         (pIntLblRngEntry->u2LBVci <
                          *pi4MplsLdpSessionAtmLRLowerBoundVci)) ||
                        (pIntLblRngEntry->u2LBVpi <
                         *pi4MplsLdpSessionAtmLRLowerBoundVpi))
                    {
                        *pi4MplsLdpSessionAtmLRLowerBoundVpi =
                            pIntLblRngEntry->u2LBVpi;
                        *pi4MplsLdpSessionAtmLRLowerBoundVci =
                            pIntLblRngEntry->u2LBVci;
                    }
                }
            }
            if (u1LblRngeFound == LDP_TRUE)
            {
                return SNMP_SUCCESS;
            }
        }
    }

    while (u1GetNext == LDP_TRUE)
    {

        /* Getting the valid next Ldp Peer's indices */
        CPY_TO_SNMP (&FsLdpEntityId, pu1LdpEntityId, LDP_MAX_LDPID_LEN);
        CPY_TO_SNMP (&FsLdpPeerId, pu1LdpPeerId, LDP_MAX_LDPID_LEN);

        if (nmhGetNextIndexMplsLdpPeerTable ((tSNMP_OCTET_STRING_TYPE *) &
                                             FsLdpEntityId,
                                             pMplsLdpEntityLdpId,
                                             *pu4MplsLdpEntityIndex,
                                             pu4MplsLdpEntityIndex,
                                             (tSNMP_OCTET_STRING_TYPE *) &
                                             FsLdpPeerId,
                                             pMplsLdpPeerLdpId) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        CPY_FROM_SNMP (pu1LdpEntityId, pMplsLdpEntityLdpId, LDP_MAX_LDPID_LEN);
        CPY_FROM_SNMP (pu1LdpPeerId, pMplsLdpPeerLdpId, LDP_MAX_LDPID_LEN);

        if (LdpGetLdpPeer ((*pu4MplsLdpLsrIncarnId) - 1, pu1LdpEntityId,
                           (*pu4MplsLdpEntityIndex),
                           pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        /* Accessing the Session that is created from this peer */
        if ((pLdpPeer->pLdpSession) != NULL)
        {
            pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
        }
        else
        {
            continue;
        }

        if (SSN_GET_LBL_TYPE (pLdpSession) != LDP_PER_IFACE)
        {
            continue;
        }

        TMO_SLL_Scan (SSN_GET_ATM_LBLRNG_LIST (pLdpSession), pIntLblRngEntry,
                      tAtmLdpLblRngEntry *)
        {
            if (u1LblRngeFound == LDP_FALSE)
            {
                *pi4MplsLdpSessionAtmLRLowerBoundVpi = pIntLblRngEntry->u2LBVpi;
                *pi4MplsLdpSessionAtmLRLowerBoundVci = pIntLblRngEntry->u2LBVci;
                u1LblRngeFound = LDP_TRUE;
                continue;

            }
            if ((u1LblRngeFound == LDP_TRUE) &&
                (((pIntLblRngEntry->u2LBVpi ==
                   *pi4MplsLdpSessionAtmLRLowerBoundVpi) &&
                  (pIntLblRngEntry->u2LBVci <
                   *pi4MplsLdpSessionAtmLRLowerBoundVci)) ||
                 (pIntLblRngEntry->u2LBVpi <
                  *pi4MplsLdpSessionAtmLRLowerBoundVpi)))
            {
                *pi4MplsLdpSessionAtmLRLowerBoundVpi = pIntLblRngEntry->u2LBVpi;
                *pi4MplsLdpSessionAtmLRLowerBoundVci = pIntLblRngEntry->u2LBVci;
            }
        }

        if (u1LblRngeFound == LDP_TRUE)
        {
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLdpAtmSessionTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                nextMplsLdpEntityLdpId
                MplsLdpEntityIndex
                nextMplsLdpEntityIndex
                MplsLdpPeerLdpId
                nextMplsLdpPeerLdpId
                MplsLdpSessionAtmLRLowerBoundVpi
                nextMplsLdpSessionAtmLRLowerBoundVpi
                MplsLdpSessionAtmLRLowerBoundVci
                nextMplsLdpSessionAtmLRLowerBoundVci
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLdpAtmSessionTable (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4 *pu4NextMplsLdpEntityIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpPeerLdpId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextMplsLdpPeerLdpId,
                                       INT4 i4MplsLdpSessionAtmLRLowerBoundVpi,
                                       INT4
                                       *pi4NextMplsLdpSessionAtmLRLowerBoundVpi,
                                       INT4 i4MplsLdpSessionAtmLRLowerBoundVci,
                                       INT4
                                       *pi4NextMplsLdpSessionAtmLRLowerBoundVci)
{
    UINT4               u4MplsLdpLsrIncarnId = 0;
    UINT1               u1GetNext = LDP_TRUE;
    UINT1               u1LblRngeFound = LDP_FALSE;
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    tAtmLdpLblRngEntry *pIntLblRngEntry = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1NextLdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    UINT1              *pu1NextLdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1NextLdpEntityId = SNMP_OCTET_STRING_LIST (pNextMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);
    pu1NextLdpPeerId = SNMP_OCTET_STRING_LIST (pNextMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (u4MplsLdpLsrIncarnId, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }

    if (pLdpSession != NULL)
    {
        TMO_SLL_Scan (SSN_GET_ATM_LBLRNG_LIST (pLdpSession), pIntLblRngEntry,
                      tAtmLdpLblRngEntry *)
        {
            if (((pIntLblRngEntry->u2LBVpi ==
                  i4MplsLdpSessionAtmLRLowerBoundVpi) &&
                 (pIntLblRngEntry->u2LBVci >
                  i4MplsLdpSessionAtmLRLowerBoundVci)) ||
                (pIntLblRngEntry->u2LBVpi > i4MplsLdpSessionAtmLRLowerBoundVpi))
            {
                if (u1LblRngeFound == LDP_FALSE)
                {
                    *pi4NextMplsLdpSessionAtmLRLowerBoundVpi =
                        pIntLblRngEntry->u2LBVpi;
                    *pi4NextMplsLdpSessionAtmLRLowerBoundVci =
                        pIntLblRngEntry->u2LBVci;
                    u1LblRngeFound = LDP_TRUE;
                    continue;
                }
                if ((u1LblRngeFound == LDP_TRUE) &&
                    (((pIntLblRngEntry->u2LBVpi ==
                       *pi4NextMplsLdpSessionAtmLRLowerBoundVpi) &&
                      (pIntLblRngEntry->u2LBVci <
                       *pi4NextMplsLdpSessionAtmLRLowerBoundVci)) ||
                     (pIntLblRngEntry->u2LBVpi <
                      *pi4NextMplsLdpSessionAtmLRLowerBoundVpi)))
                {
                    *pi4NextMplsLdpSessionAtmLRLowerBoundVpi =
                        pIntLblRngEntry->u2LBVpi;
                    *pi4NextMplsLdpSessionAtmLRLowerBoundVci =
                        pIntLblRngEntry->u2LBVci;
                }
            }
        }
        if (u1LblRngeFound == LDP_TRUE)
        {
            return SNMP_SUCCESS;
        }
    }

    while (u1GetNext == LDP_TRUE)
    {

        /* Getting the valid next Ldp Peer's indices */
        if (nmhGetNextIndexMplsLdpPeerTable (pMplsLdpEntityLdpId,
                                             pNextMplsLdpEntityLdpId,
                                             u4MplsLdpEntityIndex,
                                             pu4NextMplsLdpEntityIndex,
                                             pMplsLdpPeerLdpId,
                                             pNextMplsLdpPeerLdpId)
            == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
        u4MplsLdpEntityIndex = *pu4NextMplsLdpEntityIndex;
        CPY_TO_SNMP (pMplsLdpEntityLdpId, pu1NextLdpEntityId,
                     LDP_MAX_LDPID_LEN);
        CPY_TO_SNMP (pMplsLdpPeerLdpId, pu1NextLdpPeerId, LDP_MAX_LDPID_LEN);

        if (LdpGetLdpPeer (u4MplsLdpLsrIncarnId, pu1LdpEntityId,
                           (*pu4NextMplsLdpEntityIndex),
                           pu1NextLdpPeerId, &pLdpPeer) != LDP_SUCCESS)
        {
            return SNMP_FAILURE;
        }

        /* Accessing the Session that is created from this peer */

        if ((pLdpPeer->pLdpSession) != NULL)
        {
            pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
        }
        else
        {
            continue;
        }
        if (SSN_GET_LBL_TYPE (pLdpSession) != LDP_PER_IFACE)
        {
            continue;
        }

        TMO_SLL_Scan (SSN_GET_ATM_LBLRNG_LIST (pLdpSession), pIntLblRngEntry,
                      tAtmLdpLblRngEntry *)
        {
            if (u1LblRngeFound == LDP_FALSE)
            {
                *pi4NextMplsLdpSessionAtmLRLowerBoundVpi =
                    pIntLblRngEntry->u2LBVpi;
                *pi4NextMplsLdpSessionAtmLRLowerBoundVci =
                    pIntLblRngEntry->u2LBVci;
                u1LblRngeFound = LDP_TRUE;
                continue;
            }
            if ((u1LblRngeFound == LDP_TRUE) &&
                (((pIntLblRngEntry->u2LBVpi ==
                   *pi4NextMplsLdpSessionAtmLRLowerBoundVpi) &&
                  (pIntLblRngEntry->u2LBVci <
                   *pi4NextMplsLdpSessionAtmLRLowerBoundVci)) ||
                 (pIntLblRngEntry->u2LBVpi <
                  *pi4NextMplsLdpSessionAtmLRLowerBoundVpi)))
            {
                *pi4NextMplsLdpSessionAtmLRLowerBoundVpi =
                    pIntLblRngEntry->u2LBVpi;
                *pi4NextMplsLdpSessionAtmLRLowerBoundVci =
                    pIntLblRngEntry->u2LBVci;
            }
        }

        if (u1LblRngeFound == LDP_TRUE)
        {
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
    /* If No Valid Peer is found, then the next loop entry would 
     * result in returning SNMP_FAILURE while trying to get next valid 
     * Ldp Peer. 
     */
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionAtmLRUpperBoundVpi
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpSessionAtmLRLowerBoundVpi
                MplsLdpSessionAtmLRLowerBoundVci

                The Object 
                retValMplsLdpSessionAtmLRUpperBoundVpi
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionAtmLRUpperBoundVpi (tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpPeerLdpId,
                                        INT4 i4MplsLdpSessionAtmLRLowerBoundVpi,
                                        INT4 i4MplsLdpSessionAtmLRLowerBoundVci,
                                        INT4
                                        *pi4RetValMplsLdpSessionAtmLRUpperBoundVpi)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    tAtmLdpLblRngEntry *pIntLblRngEntry = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (LDP_MIN_INCARN - 1, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */

    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }

    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (SSN_GET_ATM_LBLRNG_LIST (pLdpSession), pIntLblRngEntry,
                  tAtmLdpLblRngEntry *)
    {
        if ((pIntLblRngEntry->u2LBVpi ==
             i4MplsLdpSessionAtmLRLowerBoundVpi) &&
            (pIntLblRngEntry->u2LBVci == i4MplsLdpSessionAtmLRLowerBoundVci))
        {
            *pi4RetValMplsLdpSessionAtmLRUpperBoundVpi =
                pIntLblRngEntry->u2UBVpi;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsLdpSessionAtmLRUpperBoundVci
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpPeerLdpId
                MplsLdpSessionAtmLRLowerBoundVpi
                MplsLdpSessionAtmLRLowerBoundVci

                The Object 
                retValMplsLdpSessionAtmLRUpperBoundVci
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLdpSessionAtmLRUpperBoundVci (tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsLdpPeerLdpId,
                                        INT4 i4MplsLdpSessionAtmLRLowerBoundVpi,
                                        INT4 i4MplsLdpSessionAtmLRLowerBoundVci,
                                        INT4
                                        *pi4RetValMplsLdpSessionAtmLRUpperBoundVci)
{
    tLdpPeer           *pLdpPeer = NULL;
    tLdpSession        *pLdpSession = NULL;
    UINT1              *pu1LdpEntityId = NULL;
    UINT1              *pu1LdpPeerId = NULL;
    tAtmLdpLblRngEntry *pIntLblRngEntry = NULL;

    pu1LdpEntityId = SNMP_OCTET_STRING_LIST (pMplsLdpEntityLdpId);
    pu1LdpPeerId = SNMP_OCTET_STRING_LIST (pMplsLdpPeerLdpId);

    if (LdpGetLdpPeer (LDP_MIN_INCARN - 1, pu1LdpEntityId,
                       u4MplsLdpEntityIndex,
                       pu1LdpPeerId, &pLdpPeer) != LDP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    /* Accessing the Session that is created from this peer */
    if ((pLdpPeer->pLdpSession) != NULL)
    {
        pLdpSession = (tLdpSession *) (pLdpPeer->pLdpSession);
    }

    if (pLdpSession == NULL)
    {
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (SSN_GET_ATM_LBLRNG_LIST (pLdpSession), pIntLblRngEntry,
                  tAtmLdpLblRngEntry *)
    {
        if ((pIntLblRngEntry->u2LBVpi ==
             i4MplsLdpSessionAtmLRLowerBoundVpi) &&
            (pIntLblRngEntry->u2LBVci == i4MplsLdpSessionAtmLRLowerBoundVci))
        {
            *pi4RetValMplsLdpSessionAtmLRUpperBoundVci =
                pIntLblRngEntry->u2UBVci;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhDepv2MplsLdpEntityAtmTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsLdpEntityAtmTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MplsLdpEntityAtmLRTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
                MplsLdpEntityAtmLRMinVpi
                MplsLdpEntityAtmLRMinVci
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsLdpEntityAtmLRTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
