/*---------------------------------------------------------------------------*/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: ldpcrlpm.c,v 1.7 2013/08/27 12:47:12 siva Exp $
 * -----------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpcrlpm.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : PM (IFACE SUB-MODULE)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains entry point function to 
 *                             Pre-Emption Module(PM) and supporting routines. 
 *                             PM keeps track of all the CRLSP tunnels. 
 *----------------------------------------------------------------------------*/
#include "ldpincs.h"

/******************************************************************************
 *Function Name : ldpCrlspPmAddTnlToPrioList                                  *
 *Description   : This routine is called when a CRLSP tunnel is to be added to*
 *              : the priority list.                                          *
 *              : Reference - Crldp -04.txt; RFC 2702                         *
 *              :                                                             *
 * Input(s)     : u1IfIndex - Interface on which the tunnel is to be added    *
 *              : u1Priority - Priority of the tunnel                         *
 *              : pCrlspTnlInfoInput- Pointer to tunnel info structure of the *
 *              : tunnel to be added                                          *
 * Output(s)    : None                                                        *
 ******************************************************************************/
VOID
LdpCrlspAddTnlToPrioList (UINT4 u4IfIndex, UINT1 u1Priority,
                          tCrlspTnlInfo * pCrlspTnlInfoInput)
{
    tTMO_SLL           *pList = NULL;

    /* The two pointers defined below are used to traverse the priority list. 
     */
    tTMO_SLL_NODE      *pTnlListPrevNode = NULL;
    tTMO_SLL_NODE      *pTnlListNextNode = NULL;

    /* The pointer to the node which is added to the priority list */
    tTMO_SLL_NODE      *pTnlListCurNode = NULL;

    tCrlspTnlInfo      *pCrlspTnlNextInfo = NULL;
    UINT2               u2IncarnId = 0;

    IFINDEX_GET_INCRN_ID (u4IfIndex, &u2IncarnId);
    if (u1Priority >= HOLD_PRIO_RANGE)
    {
        return;
    }
    /* Getting the list for the priority and the Interface Index */

    pList = CRLSP_TNL_HOLD_PRIO_LIST (u2IncarnId, u4IfIndex, u1Priority);

    /* Get the node to be added to the priority list */
    pTnlListCurNode = CRLSP_GET_TNL_LIST_HOLD_NODE (pCrlspTnlInfoInput);

    /* If List Count is Zero just add the node to the empty list */
    if (TMO_SLL_Count (pList) == TNL_CRLSP_ZERO)
    {
        TMO_SLL_Add (pList, pTnlListCurNode);
        return;
    }

    /* For listcount more than zero traverse the list node by node and 
     * add the node such that the decreasing order of the traffic params
     * is maintained.
     * This is done to ensure that the least number of tunnels are 
     * preempted.
     */

    TMO_SLL_Scan (pList, pTnlListNextNode, tTMO_SLL_NODE *)
    {

        pCrlspTnlNextInfo = PM_CRLSP_TNL_OFFSET_HTBL (pTnlListNextNode);

        /* Insert the node in between the nodes with the Previous node having
         * peak data rate higher value and the next node having the peak data 
         * rate less than or equal to the peak data rate of the tunnel which 
         * is to be added to the Priority list.
         */

        if (CRLSP_TRF_PD (pCrlspTnlNextInfo) <=
            CRLSP_TRF_PD (pCrlspTnlInfoInput))
        {

            if (pTnlListPrevNode == NULL)
            {
                /* This condition arises when there is only one node in the list 
                 * and the node to be added is as the first node. 
                 */

                TMO_SLL_Insert_In_Middle (pList, &(pList->Head),
                                          pTnlListCurNode, pTnlListNextNode);
                return;
            }
            /* When the nodes are more than one in the priority list */
            TMO_SLL_Insert_In_Middle (pList, pTnlListPrevNode, pTnlListCurNode,
                                      pTnlListNextNode);
            return;
        }

        /* Store the pointer of the next node and the structure pointer
         * as previous node pointer and previous structure pointer.  
         */
        pTnlListPrevNode = pTnlListNextNode;

    }                            /* End of "TMO_SLL_Scan" */

    /* If the Scan is exhausted then it indicates that the node to be added
     * has peak data rate lower than all the nodes in the priority list.
     * Hence It will be just appended to the list. 
     */

    TMO_SLL_Add (pList, pTnlListCurNode);
    return;

}                                /* End of the function */

/******************************************************************************
 *Function Name : LdpCrlspRemTnlFromPrioList                                  *
 *Description   : This routine is called when a CRLSP tunnel is to be removed *
 *              : from  the priority list                                     *
 *              : Reference - Crldp -04.txt; RFC 2702                         *
 *              :                                                             *
 * Input(s)     : u1IfIndex - Interface on which the tunnel is to be added    *
 *              : u1Priority - Priority of the tunnel                         *
 *              : pCrlspTnlInfoInput- Pointer to tunnel info structure of the*
 *              : tunnel to be added                                          *
 * Output(s)    : None                                                        *
 ******************************************************************************/
VOID
LdpCrlspRemTnlFromPrioList (UINT4 u4IfIndex, UINT1 u1Priority,
                            tCrlspTnlInfo * pCrlspTnlInfoInput)
{
    UINT2               u2IncarnId = 0;
    tTMO_SLL           *pList = NULL;

    tTMO_SLL_NODE      *pTnlListCurNode = NULL;

    IFINDEX_GET_INCRN_ID (u4IfIndex, &u2IncarnId);
    if (u1Priority >= HOLD_PRIO_RANGE)
    {
        return;
    }
    /* Getting the list for the priority and the Interface Index */

    pList = CRLSP_TNL_HOLD_PRIO_LIST (u2IncarnId, u4IfIndex, u1Priority);

    /* Node to be deleted from the priority list */
    pTnlListCurNode = CRLSP_GET_TNL_LIST_HOLD_NODE (pCrlspTnlInfoInput);
    TMO_SLL_Delete (pList, pTnlListCurNode);
    return;
}

/******************************************************************************
 *Function Name : LdpCrlspModifyTnlPrioList                                   * 
 *Description   : This routine modifies the  priority of a CRLSP tunnel in the *
 *              : priority list                                               *
 *              : Reference - Crldp -04.txt; RFC 2702                         *
 *              :                                                             *
 * Input(s)     : u4CurrIfIndex - refers to the tunnels current interface     *
 *              : u4Priority - refers to tunnels current priority             *
 *              : u4NewIfIndex - refers to the tunnels new interface          *
 *              : u1NewPriority - refers the tunnels new priority             *
 *              : pCrlspTnlInfoInput- Pointer to tunnel info structure of the*
 *              : tunnel to be added                                          *
 * Output(s)    : None                                                        *
 ******************************************************************************/
VOID
LdpCrlspModifyTnlPrioList (UINT4 u4CurrIfIndex, UINT1 u1CurrPriority,
                           UINT4 u4NewIfIndex, UINT1 u1NewPriority,
                           tCrlspTnlInfo * pCrlspTnlInfoInput)
{
    /* The tunnel is moved from one interface to another and its priority is 
     * modified. 
     * This involves removing of a tunnel from one priority list identified by 
     * the current priority and current Interface Index and adding to the new 
     * priority list determined by the new priority and new interface index.
     */

    LdpCrlspRemTnlFromPrioList (u4CurrIfIndex, u1CurrPriority,
                                pCrlspTnlInfoInput);

    LdpCrlspAddTnlToPrioList (u4NewIfIndex, u1NewPriority, pCrlspTnlInfoInput);

    return;
}

/******************************************************************************
 *Function Name : LdpCrlspGetTnlFromPrioList                                 *
 *Description   : This routine returns the list of the CRLSP tunnels which    *
 *              : can be preempted from the  priority list to meet the        *
 *              : resource requirements.                                      *
 *              : Reference - Crldp -04.txt; RFC 2702                         *
 *              :                                                             *
 * Input(s)     : u1IfIndex - Interface on which the tunnel is to be added    *
 *              : u1Priority - Priority of the tunnel                         *
 *              : pCrlspTnlInfoInput- Pointer to tunnel info structure of the *
 *              : tunnel to be added                                          *
 * Output(s)    : None                                                        *
 ******************************************************************************/
UINT1
LdpCrlspGetTnlFromPrioList (UINT4 u4IfIndex, UINT1 u1Priority,
                            tCrlspTnlInfo * pCrlspTnlInfoInput,
                            tTMO_SLL * pCandidatePreemp)
{
    UINT2               u2IncarnId = 0;
    /* Pointer to the priority list */
    tTMO_SLL           *pList = NULL;

    tTMO_SLL_NODE      *pCandidateNode = NULL;
    tTMO_SLL_NODE      *pPrioTnlInfo = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;

    UINT4               u4DiffTrack = 0;
    UINT4               u4Diffmin = 0;

    UINT1               u1Flag = PM_CRLSP_FALSE;
    UINT4               u4Rate = TNL_CRLSP_ZERO;
    UINT1               u1TempPriority = CRLSP_TNL_MIN_PRIO;
    UINT1               u1IfIndexEqual = LDP_FALSE;

    tTMO_SLL            StackList;

    /* Pointer to the list of tunnels which are stacked up */
    tTMO_SLL           *pStackTnlList = NULL;
    pStackTnlList = &StackList;

    /* Initialise the lists */
    TMO_SLL_Init (pStackTnlList);
    TMO_SLL_Init (pCandidatePreemp);

    /* Assign the pointer to the stack list  */

    IFINDEX_GET_INCRN_ID (u4IfIndex, &u2IncarnId);

    /* Note: If the priority of the tunnel to be established is 7, then
     * no tunnels can be preempted. 
     */
    if (u1Priority == CRLSP_TNL_MIN_PRIO)
    {
        PM_LDP_DBG (LDP_PREMPT_DUMP, " The pre-emption is not possible ");
        return PM_CRLSP_FAILURE;
    }

    /* Best Match Check */
    /* Searches for a single Tunnel having a Peak data Rate greater than
     * or equal to the required Peak Data Rate. The search is started from the
     * lowest priority list .If no tunnel is found in the list with a peak data
     * rate greater than the peak data rate of the tunnel which is to be 
     * established then, the search is carried on to the priority lists of the
     * subsequent higher priorities.
     */

    for (u1TempPriority = CRLSP_TNL_MIN_PRIO; u1TempPriority > u1Priority;
         u1TempPriority--)
    {

        pList =
            CRLSP_TNL_HOLD_PRIO_LIST (u2IncarnId, u4IfIndex, u1TempPriority);

        /* A scan of the list is done in order to search for the best match and
         * if the tunnel is stacked up then it is not considered for the best
         * match search and that tunnel is added to the list of tunnels which 
         * will be scanned if all the other tunnels are exhausted.
         */

        TMO_SLL_Scan (pList, pPrioTnlInfo, tTMO_SLL_NODE *)
        {

            pCrlspTnlInfo = PM_CRLSP_TNL_OFFSET_HTBL (pPrioTnlInfo);

            /* If the Tunnel has a modification request and if the tunnel
             * modify is on the same Interface Index, it should have less 
             * priority compared to the tunnel request priority..  
             */
            if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL))
            {
                if ((LCB_OUT_IFINDEX
                     (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo))) == u4IfIndex))
                {
                    u1IfIndexEqual = LDP_TRUE;
                    if (CRLSP_HOLD_PRIO (CRLSP_INS_POIN (pCrlspTnlInfo)) <=
                        CRLSP_SET_PRIO (pCrlspTnlInfoInput))
                    {
                        continue;
                    }
                }
            }

            /* Check if the tunnel has a stack list associated to it */
            if (TMO_SLL_Count ((tTMO_SLL *) & (pCrlspTnlInfo->StackTnlList))
                != 0)
            {
                TMO_SLL_Add (pStackTnlList,
                             (tTMO_SLL_NODE *) & (pCrlspTnlInfo->
                                                  NextStackNode));
                continue;
            }

            /* If the Tunnel has a modification request and if the tunnel
             * modify is on the same Interface Index and with the  
             * priority less than the input priority the modification tunnel 
             * will be considered at the place where it's holding higher
             * resources.
             * If the Modification tunnel has a stacklist it will be ignored
             * from the count.
             */

            if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL) &&
                (u1IfIndexEqual == LDP_TRUE))
            {
                if (TMO_SLL_Count ((tTMO_SLL *) &
                                   (CRLSP_INS_POIN (pCrlspTnlInfo)->
                                    StackTnlList)) == TNL_CRLSP_ZERO)
                {
                    if (!((CRLSP_TRF_PD ((pCrlspTnlInfo)) >
                           CRLSP_TRF_PD (CRLSP_INS_POIN (pCrlspTnlInfo))) ||
                          ((CRLSP_IS_MOD_OF_TUNN (pCrlspTnlInfo) == LDP_TRUE) &&
                           (CRLSP_TRF_PD (CRLSP_INS_POIN (pCrlspTnlInfo)) ==
                            CRLSP_TRF_PD (pCrlspTnlInfo)))))
                    {
                        continue;
                    }
                }
                else
                {
                    continue;
                }
            }

            if (CRLSP_TRF_PD (pCrlspTnlInfo) <
                CRLSP_TRF_PD (pCrlspTnlInfoInput))
            {
                continue;
            }

            /* Get the difference in the peak data rate */
            u4DiffTrack = (CRLSP_TRF_PD (pCrlspTnlInfo) -
                           CRLSP_TRF_PD (pCrlspTnlInfoInput));

            /* If the peak data rate perfectly matches the peak data rate of 
             * the tunnel in contention at this instance the tunnel is added 
             * to the candidate Preemption list. 
             */
            if (u4DiffTrack == TNL_CRLSP_ZERO)
            {

                /* The nodes of the stack list are deleted because the best 
                 * match has been found and there is no need of the stack list.
                 */

                while (TMO_SLL_Get (pStackTnlList) != NULL)
                {
                    continue;
                }

                /* The best match node is added to the preemption list */
                TMO_SLL_Add (pCandidatePreemp,
                             (tTMO_SLL_NODE *) & ((pCrlspTnlInfo)->
                                                  NextPreemptTnl));
                return PM_CRLSP_SUCCESS;
            }

            /* Here to find the least difference the difference in the
             * Peak data rate is tracked for every unstacked tunnel 
             * under the current priority
             */

            if (u4DiffTrack > TNL_CRLSP_ZERO)
            {
                if (u1Flag == PM_CRLSP_FALSE)
                {
                    u4Diffmin = u4DiffTrack;
                    pCandidateNode = pPrioTnlInfo;
                    u1Flag = PM_CRLSP_TRUE;
                }

                if ((u1Flag == PM_CRLSP_TRUE) && (u4DiffTrack < u4Diffmin))
                {
                    u4Diffmin = u4DiffTrack;
                    pCandidateNode = pPrioTnlInfo;
                }
            }
            u1IfIndexEqual = LDP_FALSE;
        }                        /* End of Sll_Scan */

        /* Traverse the list with the next higher priority if the flag
         * is false which denotes that there is no tunnel which has peak 
         * data rate greater than the tunnel which is to be established.
         */

        if (u1Flag == PM_CRLSP_TRUE)
        {
            while (TMO_SLL_Get (pStackTnlList) != NULL)
            {
                continue;
            }
            pCrlspTnlInfo = PM_CRLSP_TNL_OFFSET_HTBL (pCandidateNode);
            TMO_SLL_Add (pCandidatePreemp,
                         (tTMO_SLL_NODE *) & ((pCrlspTnlInfo)->NextPreemptTnl));
            return PM_CRLSP_SUCCESS;
        }
    }                            /* End of For Loop */

    u1TempPriority = CRLSP_TNL_MIN_PRIO;

    /* If the best match is not found that means that there is no tunnel 
     * in the priority list with the peak data rate greater or equal to 
     * the peak data rate of the tunnel which is to be established.
     */

    while ((u4Rate < CRLSP_TRF_PD (pCrlspTnlInfoInput)) &&
           (u1TempPriority > u1Priority))
    {

        pList =
            CRLSP_TNL_HOLD_PRIO_LIST (u2IncarnId, u4IfIndex, u1TempPriority);

        /* Start of sll scan */
        TMO_SLL_Scan (pList, pPrioTnlInfo, tTMO_SLL_NODE *)
        {
            pCrlspTnlInfo = PM_CRLSP_TNL_OFFSET_HTBL (pPrioTnlInfo);
            if (TMO_SLL_Count ((tTMO_SLL *) & (pCrlspTnlInfo->StackTnlList)) !=
                0)
            {
                continue;
            }

            /* If the Tunnel has a modification request and if the tunnel
             * modify is on the same Interface Index and with the  
             * priority less than the input priority the modification tunnel 
             * will be considered at the place where it's holding higher
             * resources.
             * If the Modification tunnel has a stacklist it will be ignored
             * from the count.
             */

            if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL))
            {
                if ((LCB_OUT_IFINDEX
                     (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo))) == u4IfIndex))
                {
                    if (CRLSP_HOLD_PRIO (CRLSP_INS_POIN (pCrlspTnlInfo)) >
                        CRLSP_SET_PRIO (pCrlspTnlInfoInput))
                    {
                        if (TMO_SLL_Count ((tTMO_SLL *) &
                                           (CRLSP_INS_POIN (pCrlspTnlInfo)->
                                            StackTnlList)) == TNL_CRLSP_ZERO)
                        {
                            if (!((CRLSP_TRF_PD ((pCrlspTnlInfo)) >
                                   CRLSP_TRF_PD (CRLSP_INS_POIN
                                                 (pCrlspTnlInfo)))
                                  ||
                                  ((CRLSP_IS_MOD_OF_TUNN (pCrlspTnlInfo) ==
                                    LDP_TRUE)
                                   &&
                                   (CRLSP_TRF_PD
                                    (CRLSP_INS_POIN (pCrlspTnlInfo)) ==
                                    CRLSP_TRF_PD (pCrlspTnlInfo)))))
                            {
                                continue;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }
                }
            }

            u4Rate = u4Rate + (CRLSP_TRF_PD (pCrlspTnlInfo));
            TMO_SLL_Add (pCandidatePreemp,
                         (tTMO_SLL_NODE *) & ((pCrlspTnlInfo)->NextPreemptTnl));

            if (u4Rate >= CRLSP_TRF_PD (pCrlspTnlInfoInput))
            {
                while (TMO_SLL_Get (pStackTnlList) != NULL)
                {
                    continue;
                }
                return PM_CRLSP_SUCCESS;
            }
        }                        /* End of sll scan */
        u1TempPriority--;
    }                            /* End of While loop */

    /* The requirement of the peak data rate is not satisfied by the tunnels
     * which are not stacked up then the the list of stacked up tunnels which 
     * was obtained while searching for the best match is traversed to meet 
     * the requirements of the peak data rate
     */

    pPrioTnlInfo = TMO_SLL_First (pStackTnlList);
    while (pPrioTnlInfo != NULL)
    {
        TMO_SLL_Delete (pStackTnlList, pPrioTnlInfo);
        pCrlspTnlInfo = ((tCrlspTnlInfo *) (VOID *) ((UINT1 *) pPrioTnlInfo -
                                                     PM_CRLSP_OFFSET
                                                     (tCrlspTnlInfo,
                                                      NextStackNode)));
        if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
            && (LCB_OUT_IFINDEX (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo))) ==
                u4IfIndex))
        {
            if (!((CRLSP_TRF_PD (pCrlspTnlInfo) >
                   CRLSP_TRF_PD (CRLSP_INS_POIN (pCrlspTnlInfo))) ||
                  ((CRLSP_IS_MOD_OF_TUNN (pCrlspTnlInfo) == LDP_TRUE)
                   && (CRLSP_TRF_PD (CRLSP_INS_POIN (pCrlspTnlInfo)) ==
                       CRLSP_TRF_PD (pCrlspTnlInfo)))))
            {
                continue;
            }
            u4Rate = u4Rate + (CRLSP_TRF_PD (pCrlspTnlInfo));
        }
        else
        {
            u4Rate = u4Rate + (CRLSP_TRF_PD (pCrlspTnlInfo));
        }

        /* Add to the pCandidatePreemp list */
        TMO_SLL_Add (pCandidatePreemp,
                     (tTMO_SLL_NODE *) & ((pCrlspTnlInfo)->NextPreemptTnl));
        if (u4Rate >= CRLSP_TRF_PD (pCrlspTnlInfoInput))
        {
            while (TMO_SLL_Get (pStackTnlList) != NULL)
            {
                continue;
            }
            return PM_CRLSP_SUCCESS;
        }

        pPrioTnlInfo = TMO_SLL_First (pStackTnlList);
    }                            /* End Of While */
    while (TMO_SLL_Get (pStackTnlList) != NULL)
    {
        continue;
    }

    pPrioTnlInfo = TMO_SLL_First (pCandidatePreemp);
    while (pPrioTnlInfo != NULL)
    {
        TMO_SLL_Delete (pCandidatePreemp, pPrioTnlInfo);
        pPrioTnlInfo = TMO_SLL_First (pCandidatePreemp);
    }
    PM_LDP_DBG (LDP_PREMPT_DUMP,
                " The existing tunnels doesn't satisfy the resource "
                "requirement ");
    return PM_CRLSP_FAILURE;
}

/*****************************************************************************
* Function Name : LdpDiffServCrlspGetTnlFromPrioList
*
* Description   : This routine will be invoked by the LDP with diffServ
*                 feature. This routine will return the list(s) tunnels which
*                 meet the resource requirement on per OA basis.
* Input(s)      : pCrlspTnlInfoInput  - Pointer to the Tunnel which contains
*                    the per OA requirement of the traffic profiles.
* Output(s)     : pCandidatePreemp - The list of the tunnel(s) which should 
*                    be preempted to meet the resource requirements on per
*                    OA basis
* Return(s)     : LDP_SUCCESS in case the resources are available for 
*                    all OAs by preempting the tunnel(s) or
*                    LDP_FAILURE otherwise.
*****************************************************************************/
UINT1
LdpDiffServCrlspGetTnlFromPrioList (tCrlspTnlInfo * pCrlspTnlInfoInput,
                                    tTMO_SLL * pCandidatePreemp)
{
    UINT4               u4RsrcNeeded[LDP_DS_MAX_NO_PSCS];
    UINT1               u1PreemptCandidate = 0;
    UINT2               u2PscIndex;
    UINT2               u2Count;
    UINT2               u2IncarnId;
    UINT1               u1Priority = CRLSP_SET_PRIO (pCrlspTnlInfoInput);
    UINT4               u4IfIndex =
        LCB_OUT_IFINDEX (CRLSP_LCB (pCrlspTnlInfoInput));
    UINT4               u4DiffTrack = 0;
    UINT4               u4DiffMin = 0;
    UINT1               u1TempPriority;
    UINT1               u1IfIndexEqual;
    UINT1               u1TunnelNotFound;
    UINT1               u1AllResourcesNotFound;
    tTMO_SLL            StackList;
    tTMO_SLL_NODE      *pCandidateNode = NULL;
    tTMO_SLL_NODE      *pPrioTnlInfo = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;
    tTMO_SLL           *pList = NULL;
    UINT1               u1Flag = PM_CRLSP_FALSE;
    UINT1               u1NotTunnelIns;
    /* Pointer to the list of tunnels which are stacked up */
    tTMO_SLL           *pStackTnlList = NULL;

    pStackTnlList = &StackList;
    /* Initialise the lists */
    TMO_SLL_Init (pStackTnlList);
    TMO_SLL_Init (pCandidatePreemp);

    /* Assign the pointer to the stack list  */
    IFINDEX_GET_INCRN_ID (u4IfIndex, &u2IncarnId);

    MEMSET (&u4RsrcNeeded, 0, sizeof (u4RsrcNeeded));
    /* Note: If the priority of the tunnel to be established is 7, then
     * no tunnels can be preempted. 
     */
    if (u1Priority == CRLSP_TNL_MIN_PRIO)
    {
        return LDP_FAILURE;
    }
    for (u1TempPriority = CRLSP_TNL_MIN_PRIO; u1TempPriority > u1Priority;
         u1TempPriority--)
    {
        /* Get the list of the tunnels in that priority */
        pList =
            LDP_DS_TNL_HOLD_PRIO_LIST (u2IncarnId, u4IfIndex, u1TempPriority);
        /* Scan all the tunnels in that priority for that interface */
        TMO_SLL_Scan (pList, pPrioTnlInfo, tTMO_SLL_NODE *)
        {
            pCrlspTnlInfo = PM_CRLSP_TNL_OFFSET_HTBL (pPrioTnlInfo);
            u4DiffTrack = 0;
            u1IfIndexEqual = LDP_DS_FALSE;
            /* If the Tunnel has a modification request and if the tunnel
             * modify is on the same Interface Index, it should have less 
             * priority compared to the tunnel request priority..  
             */
            if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL))
            {
                if ((LCB_OUT_IFINDEX
                     (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo))) == u4IfIndex))
                {
                    u1IfIndexEqual = LDP_TRUE;
                    if (CRLSP_HOLD_PRIO (CRLSP_INS_POIN (pCrlspTnlInfo)) <
                        u1Priority)
                    {
                        continue;
                    }
                }
            }
            /* Check if the tunnel has any stack list associated with it, if yes
             * Add that tunnel to the stack list to scan later , 
             * if no tunnel is found with resources and without stack */
            if (TMO_SLL_Count ((tTMO_SLL *) & (pCrlspTnlInfo->StackTnlList))
                != 0)
            {
                TMO_SLL_Add (pStackTnlList,
                             (tTMO_SLL_NODE *) & (pCrlspTnlInfo->
                                                  NextStackNode));
                continue;
            }

            if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL) &&
                (u1IfIndexEqual == LDP_TRUE))
            {
                if (TMO_SLL_Count ((tTMO_SLL *) &
                                   (CRLSP_INS_POIN (pCrlspTnlInfo)->
                                    StackTnlList)) == TNL_CRLSP_ZERO)
                {
                    if (CRLSP_IS_MOD_OF_TUNN (pCrlspTnlInfo) == LDP_TRUE)
                    {
                        continue;
                    }
                }
                else
                {
                    continue;
                }
            }
            /* Found the tunnel which is to be tested for the availability 
             *of the resources */
            u1TunnelNotFound = 0;
            for (u2PscIndex = 0; u2PscIndex < LDP_DS_MAX_NO_PSCS; u2PscIndex++)
            {

                if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                    (pCrlspTnlInfo, u2PscIndex) != NULL)
                {
                    u4DiffTrack +=
                        LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfo, u2PscIndex);
                }

                if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                    (pCrlspTnlInfoInput, u2PscIndex) == NULL)
                {
                    continue;
                }
                if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                    (pCrlspTnlInfo, u2PscIndex) == NULL)
                {
                    u1TunnelNotFound = 1;
                    break;
                    /* resources cannot be availed from this tunnel */
                }

                u4DiffTrack -= LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfo, u2PscIndex);
                if (LDP_DS_GET_PER_OA_RSRC_AVAIL
                    (pCrlspTnlInfoInput, u2PscIndex) == TRUE)
                {
                    continue;
                }
                /*Resources are availabel , no need of allocating */
                /* Else Reserve the resources  for this Psc */
                if (LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfo, u2PscIndex) >=
                    LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfoInput, u2PscIndex))
                {
                    u4DiffTrack +=
                        LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfo,
                                              u2PscIndex) -
                        LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfoInput, u2PscIndex);
                    continue;
                }
                else
                {
                    u4DiffTrack += LDP_DS_PER_OA_TRF_PD
                        (pCrlspTnlInfo, u2PscIndex);
                    /* The resources available are less */
                    u1TunnelNotFound = 1;
                    break;
                }
            }

            /* This tunnel does not fit the required resources ie.
             *the resources are less or they are empty */
            if (u1TunnelNotFound == 1)
            {
                continue;
            }

            /* Else the tunnel is found , check if the difference is 0.
             * If it is , this is the best match entry , Although the other
             * PSCs may be holding some resources */

            if (u4DiffTrack == 0)    /* The best match entry is found */
            {
                /* The stack should be made empty */
                while (TMO_SLL_Get (pStackTnlList) != NULL)
                {
                    continue;
                }

                /* The best match node is added to the preemption list */
                TMO_SLL_Add (pCandidatePreemp,
                             (tTMO_SLL_NODE *) & ((pCrlspTnlInfo)->
                                                  NextPreemptTnl));
                return LDP_SUCCESS;

            }

            if (u4DiffTrack > TNL_CRLSP_ZERO)
            {
                if (u1Flag == PM_CRLSP_FALSE)
                {
                    u4DiffMin = u4DiffTrack;
                    pCandidateNode = pPrioTnlInfo;
                    u1Flag = PM_CRLSP_TRUE;
                }

                if ((u1Flag == PM_CRLSP_TRUE) && (u4DiffTrack < u4DiffMin))
                {
                    u4DiffMin = u4DiffTrack;
                    pCandidateNode = pPrioTnlInfo;
                }
            }
            /* Check the other tunnel in the same priority after  
             *the following braces*/
            u1IfIndexEqual = LDP_FALSE;
        }
    }
    /* Traverse the list with the next higher priority if the flag
     * is false which denotes that there is no tunnel which has peak 
     * data rate greater than the tunnel which is to be established.
     */

    if (u1Flag == PM_CRLSP_TRUE)
    {
        while (TMO_SLL_Get (pStackTnlList) != NULL)
        {
            continue;
        }
        pCrlspTnlInfo = PM_CRLSP_TNL_OFFSET_HTBL (pCandidateNode);
        TMO_SLL_Add (pCandidatePreemp,
                     (tTMO_SLL_NODE *) & ((pCrlspTnlInfo)->NextPreemptTnl));
        MEMSET (pStackTnlList, 0, sizeof (tTMO_SLL));
        return PM_CRLSP_SUCCESS;
    }

    /* If the best match is not found that means that there is no tunnel 
     * in the priority list with the peak data rate greater or equal to 
     * the peak data rate of the tunnel which is to be established.
     * so we need to find the combinations of the tunnels which can satisfy the
     * required criteria
     */
    u1TempPriority = CRLSP_TNL_MIN_PRIO;
    u1AllResourcesNotFound = 1;

    /* Initialize the  u4RsrcNeeded [] with the resources required */
    for (u2PscIndex = 0; u2PscIndex < LDP_DS_MAX_NO_PSCS; u2PscIndex++)
    {
        if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
            (pCrlspTnlInfoInput, u2PscIndex) == NULL)
        {
            continue;
        }
        if (LDP_DS_GET_PER_OA_RSRC_AVAIL (pCrlspTnlInfoInput, u2PscIndex)
            == TRUE)
        {
            u4RsrcNeeded[u2PscIndex] = 0;
        }
        else
        {
            u4RsrcNeeded[u2PscIndex] =
                LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfoInput, u2PscIndex);
        }
    }
    while ((u1AllResourcesNotFound == 1) && (u1TempPriority > u1Priority))
    {
        /* Get the prioriry tunnel */
        u1AllResourcesNotFound = 1;
        pList
            = LDP_DS_TNL_HOLD_PRIO_LIST (u2IncarnId, u4IfIndex, u1TempPriority);
        /*  Start of sll scan */
        TMO_SLL_Scan (pList, pPrioTnlInfo, tTMO_SLL_NODE *)
        {
            u1PreemptCandidate = LDP_DS_FALSE;
            pCrlspTnlInfo = PM_CRLSP_TNL_OFFSET_HTBL (pPrioTnlInfo);
            if (TMO_SLL_Count ((tTMO_SLL *) & (pCrlspTnlInfo->StackTnlList)) !=
                0)
            {
                continue;
            }
            if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL))
            {
                if ((LCB_OUT_IFINDEX
                     (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo))) != u4IfIndex)
                    || (CRLSP_HOLD_PRIO (CRLSP_INS_POIN (pCrlspTnlInfo)) <
                        CRLSP_SET_PRIO (pCrlspTnlInfoInput))
                    || (TMO_SLL_Count ((tTMO_SLL *) &
                                       (CRLSP_INS_POIN (pCrlspTnlInfo)->
                                        StackTnlList)) != TNL_CRLSP_ZERO)
                    || (CRLSP_IS_MOD_OF_TUNN (pCrlspTnlInfo) == LDP_TRUE))
                {
                    continue;
                }
            }
            /* The tunnel is used to get the available resorces */

            for (u2PscIndex = 0; u2PscIndex < LDP_DS_MAX_NO_PSCS; u2PscIndex++)
            {
                if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                    (pCrlspTnlInfoInput, u2PscIndex) == NULL)
                {
                    continue;
                }

                if (LDP_DS_GET_PER_OA_RSRC_AVAIL
                    (pCrlspTnlInfoInput, u2PscIndex) == TRUE)
                {
                    continue;
                }
                /* Resources are availabel , no need of allocating */

                /* If resources are not available , check if the tunnel 
                 * is having the resources */
                if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                    (pCrlspTnlInfo, u2PscIndex) == NULL)
                {
                    continue;
                }
                /* the resources are available  */
                if (LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfo, u2PscIndex)
                    >= u4RsrcNeeded[u2PscIndex])
                {
                    u4RsrcNeeded[u2PscIndex] = 0;
                    /* Resources are reserved for this OA and set the flags */
                    LDP_DS_SET_PER_OA_RSRC_AVAIL (pCrlspTnlInfoInput,
                                                  u2PscIndex);
                    u1PreemptCandidate = TRUE;

                }
                else
                {
                    u4RsrcNeeded[u2PscIndex] =
                        LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfoInput,
                                              u2PscIndex) -
                        LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfo, u2PscIndex);
                    u1PreemptCandidate = TRUE;
                }

            }                    /* end of for */
            if (u1PreemptCandidate == TRUE)
            {
                /* Add this in the preemption list */
                TMO_SLL_Add (pCandidatePreemp,
                             (tTMO_SLL_NODE *) & ((pCrlspTnlInfo)->
                                                  NextPreemptTnl));
                u1AllResourcesNotFound = 0;
                /*  Check if all the resources are reserved */
                for (u2Count = 0; u2Count < LDP_DS_MAX_NO_PSCS; u2Count++)
                {
                    if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                        (pCrlspTnlInfoInput, u2Count) == NULL)
                    {
                        continue;
                    }

                    if (LDP_DS_GET_PER_OA_RSRC_AVAIL
                        (pCrlspTnlInfoInput, u2Count) == FALSE)
                    {
                        u1AllResourcesNotFound = 1;
                        break;
                    }
                }
            }
            if (u1AllResourcesNotFound == 0)
            {
                break;
            }
        }
        u1TempPriority--;
    }

    /* Going for the stacked tunnels */
    pPrioTnlInfo = TMO_SLL_First (pStackTnlList);
    while ((pPrioTnlInfo != NULL) && (u1AllResourcesNotFound == 1))
    {
        u1PreemptCandidate = FALSE;
        TMO_SLL_Delete (pStackTnlList, pPrioTnlInfo);
        pCrlspTnlInfo = ((tCrlspTnlInfo *) (VOID *) ((UINT1 *) pPrioTnlInfo -
                                                     PM_CRLSP_OFFSET
                                                     (tCrlspTnlInfo,
                                                      NextStackNode)));

        if (CRLSP_IS_MOD_OF_TUNN (pCrlspTnlInfo) == LDP_TRUE)
        {
            continue;
        }
        for (u2PscIndex = 0; u2PscIndex < LDP_DS_MAX_NO_PSCS; u2PscIndex++)
        {
            if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                (pCrlspTnlInfoInput, u2PscIndex) == NULL)
            {
                continue;
            }

            if (LDP_DS_GET_PER_OA_RSRC_AVAIL (pCrlspTnlInfoInput,
                                              u2PscIndex) == TRUE)
            {
                continue;
            }

            u1NotTunnelIns = 1;
            if ((CRLSP_INS_POIN (pCrlspTnlInfo) != NULL) &&
                (LCB_OUT_IFINDEX (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo)))
                 == u4IfIndex) &&
                (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                 (CRLSP_INS_POIN (pCrlspTnlInfo), u2PscIndex) != NULL) &&
                (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                 (pCrlspTnlInfo, u2PscIndex) != NULL))
            {

                if (LDP_DS_PER_OA_TRF_PD
                    (CRLSP_INS_POIN (pCrlspTnlInfo),
                     u2PscIndex) >= LDP_DS_PER_OA_TRF_PD ((pCrlspTnlInfo),
                                                          u2PscIndex))

                {
                    /* Get the resources */
                    /* the resources are available  */
                    if (LDP_DS_PER_OA_TRF_PD
                        (CRLSP_INS_POIN (pCrlspTnlInfo),
                         u2PscIndex) >= u4RsrcNeeded[u2PscIndex])
                    {
                        /* Resources are reserved for this OA and set */
                        /* the flags */
                        u4RsrcNeeded[u2PscIndex] = 0;
                        LDP_DS_SET_PER_OA_RSRC_AVAIL (pCrlspTnlInfoInput,
                                                      u2PscIndex);
                        u1PreemptCandidate = TRUE;
                    }
                    else
                    {
                        u4RsrcNeeded[u2PscIndex] =
                            LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfoInput,
                                                  u2PscIndex) -
                            LDP_DS_PER_OA_TRF_PD (CRLSP_INS_POIN
                                                  (pCrlspTnlInfo), u2PscIndex);
                        u1PreemptCandidate = TRUE;
                    }
                    u1NotTunnelIns = 0;
                }
                else
                    u1NotTunnelIns = 1;

                /* Check of the tunnel instance holds the resources 
                 * for this PSC */

            }
            if (u1NotTunnelIns == 1)
            {
                /* If resources are not available from the tunnel instance, 
                 *check if the tunnel is having the resources */

                if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                    (pCrlspTnlInfo, u2PscIndex) == NULL)
                {
                    continue;
                }
                /* the resources are available  */
                if (LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfo, u2PscIndex)
                    >= u4RsrcNeeded[u2PscIndex])
                {
                    /* Resources are reserved for this OA and set the flags */

                    u4RsrcNeeded[u2PscIndex] = 0;
                    LDP_DS_SET_PER_OA_RSRC_AVAIL (pCrlspTnlInfoInput,
                                                  u2PscIndex);
                    u1PreemptCandidate = LDP_DS_TRUE;

                }
                else
                {
                    u4RsrcNeeded[u2PscIndex] =
                        LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfoInput,
                                              u2PscIndex) -
                        LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfo, u2PscIndex);
                    u1PreemptCandidate = LDP_DS_TRUE;

                }
            }

            pPrioTnlInfo = TMO_SLL_First (pStackTnlList);
        }

        if (u1PreemptCandidate == TRUE)
        {
            /* Add this in the preemption list */
            TMO_SLL_Add (pCandidatePreemp,
                         (tTMO_SLL_NODE *) & ((pCrlspTnlInfo)->NextPreemptTnl));
            u1AllResourcesNotFound = 0;

            /*  Check if all the resources are reserved */
            for (u2Count = 0; u2Count < LDP_DS_MAX_NO_PSCS; u2Count++)
            {
                if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW
                    (pCrlspTnlInfoInput, u2Count) == NULL)
                {
                    continue;
                }

                if (LDP_DS_GET_PER_OA_RSRC_AVAIL
                    (pCrlspTnlInfoInput, u2Count) == FALSE)
                {
                    u1AllResourcesNotFound = 1;
                    break;
                }
            }
        }
    }

    MEMSET (pStackTnlList, 0, sizeof (tTMO_SLL));

    if (u1AllResourcesNotFound == 1)
    {
        pCandidateNode = TMO_SLL_First (pCandidatePreemp);
        while (pCandidateNode != NULL)
        {
            TMO_SLL_Delete (pCandidatePreemp, pCandidateNode);
            pCandidateNode = TMO_SLL_First (pCandidatePreemp);
        }
        MEMSET (pCandidatePreemp, 0, sizeof (tTMO_SLL));
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************
* Function Name : LdpDiffServGetSumOfTrfcParms
*
* Description   : This routine will be invoked by the LDP with diffServ 
*                 module. This routine will return the sum of the PDR in
*                 the tunnel for all OAs.
*
* Input(s)      : pCrlspTnlInfo - Pointer to the corresponding tunnel.      
* Output(s)     : None.                                                     
* Return(s)     : Sum of the PDRs.
*****************************************************************************/
UINT4
LdpDiffServGetSumOfTrfcParms (tCrlspTnlInfo * pCrlspTnlInfo)
{
    UINT2               u2Psc;
    UINT4               u4SumOfTrfcParms = 0;

    for (u2Psc = 0; u2Psc < LDP_DS_MAX_NO_PSCS; u2Psc++)
    {
        if (LDP_DS_PEROA_TRFC_PROFILE_PSCINDEX_ROW (pCrlspTnlInfo, u2Psc) ==
            NULL)
        {
            continue;
        }
        u4SumOfTrfcParms += LDP_DS_PER_OA_TRF_PD (pCrlspTnlInfo, u2Psc);

    }
    return u4SumOfTrfcParms;
}

/******************************************************************************
 *Function Name : ldpDiffServPmAddTnlToPrioList                                  *
 *Description   : This routine is called when a CRLSP DiffSev tunnel is 
 *              : Reference - Crldp -04.txt; RFC 2702                         *
 *              :                                                             *
 * Input(s)     : u1IfIndex - Interface on which the tunnel is to be added    *
 *              : u1Priority - Priority of the tunnel                         *
 *              : pCrlspTnlInfoInput- Pointer to tunnel info structure of the *
 *              : tunnel to be added                                          *
 * Output(s)    : None                                                        *
 ******************************************************************************/
VOID
LdpDiffServAddTnlToPrioList (UINT4 u4IfIndex, UINT1 u1Priority,
                             tCrlspTnlInfo * pCrlspTnlInfoInput)
{
    tTMO_SLL           *pList = NULL;

    /* The two pointers defined below are used to traverse the priority list. 
     */
    tTMO_SLL_NODE      *pTnlListPrevNode = NULL;
    tTMO_SLL_NODE      *pTnlListNextNode = NULL;

    /* The pointer to the node which is added to the priority list */
    tTMO_SLL_NODE      *pTnlListCurNode = NULL;

    tCrlspTnlInfo      *pCrlspTnlNextInfo = NULL;
    UINT2               u2IncarnId = 0;

    IFINDEX_GET_INCRN_ID (u4IfIndex, &u2IncarnId);
    if (u1Priority >= HOLD_PRIO_RANGE)
    {
        return;
    }
    /* Getting the list for the priority and the Interface Index */

    pList = LDP_DS_TNL_HOLD_PRIO_LIST (u2IncarnId, u4IfIndex, u1Priority);

    /* Get the node to be added to the priority list */
    pTnlListCurNode = CRLSP_GET_TNL_LIST_HOLD_NODE (pCrlspTnlInfoInput);

    /* If List Count is Zero just add the node to the empty list */
    if (TMO_SLL_Count (pList) == TNL_CRLSP_ZERO)
    {
        TMO_SLL_Init (pList);
        TMO_SLL_Add (pList, pTnlListCurNode);
        return;
    }

    /* For listcount more than zero traverse the list node by node and 
     * add the node such that the decreasing order of the traffic params
     * is maintained.
     * This is done to ensure that the least number of tunnels are 
     * preempted.
     */

    TMO_SLL_Scan (pList, pTnlListNextNode, tTMO_SLL_NODE *)
    {
        pCrlspTnlNextInfo = PM_CRLSP_TNL_OFFSET_HTBL (pTnlListNextNode);

        /* Insert the node in between the nodes with the Previous node having
         * peak data rate higher value and the next node having the Sum of 
         * the Per OA peak data 
         * rate less than or equal to the peak data rate of the tunnel which 
         * is to be added to the Priority list.
         */
        /* Add the sum here */
        if (LdpDiffServGetSumOfTrfcParms (pCrlspTnlNextInfo) <=
            LdpDiffServGetSumOfTrfcParms (pCrlspTnlInfoInput))
        {
            if (pTnlListPrevNode == NULL)
            {
                /* This condition arises when there is only one node 
                 * in the list and the node to be added is as the 
                 * first node. 
                 */
                TMO_SLL_Insert_In_Middle (pList, &(pList->Head),
                                          pTnlListCurNode, pTnlListNextNode);
                return;
            }
            /* When the nodes are more than one in the priority list */
            TMO_SLL_Insert_In_Middle (pList, pTnlListPrevNode, pTnlListCurNode,
                                      pTnlListNextNode);
            return;
        }
        /* Store the pointer of the next node and the structure pointer
         * as previous node pointer and previous structure pointer.  
         */
        pTnlListPrevNode = pTnlListNextNode;

    }                            /* End of "TMO_SLL_Scan" */

    /* If the Scan is exhausted then it indicates that the node to be added
     * has peak data rate lower than all the nodes in the priority list.
     * Hence It will be just appended to the list. 
     */
    TMO_SLL_Add (pList, pTnlListCurNode);
    return;
}

/******************************************************************************
 *Function Name : LdpDiffServRemTnlFromPrioList                                  *
 *Description   : This routine is called when a CRLSP DiffServ tunnel is to 
 *                be removed from  the priority list
 *              : Reference - Crldp -04.txt; RFC 2702                         *
 *              :                                                             *
 * Input(s)     : u1IfIndex - Interface on which the tunnel is to be added    *
 *              : u1Priority - Priority of the tunnel                         *
 *              : pCrlspTnlInfoInput- Pointer to tunnel info structure of the*
 *              : tunnel to be added                                          *
 * Output(s)    : None                                                        *
 ******************************************************************************/

VOID
LdpDiffServRemTnlFromPrioList (UINT4 u4IfIndex, UINT1 u1Priority,
                               tCrlspTnlInfo * pCrlspTnlInfoInput)
{
    UINT2               u2IncarnId = 0;
    tTMO_SLL           *pList = NULL;

    tTMO_SLL_NODE      *pTnlListCurNode = NULL;

    IFINDEX_GET_INCRN_ID (u4IfIndex, &u2IncarnId);
    if (u1Priority >= HOLD_PRIO_RANGE)
    {
        return;
    }
    /* Getting the list for the priority and the Interface Index */
    pList = LDP_DS_TNL_HOLD_PRIO_LIST (u2IncarnId, u4IfIndex, u1Priority);

    /* Node to be deleted from the priority list */
    pTnlListCurNode = CRLSP_GET_TNL_LIST_HOLD_NODE (pCrlspTnlInfoInput);
    TMO_SLL_Delete (pList, pTnlListCurNode);
    return;
}

/******************************************************************************
 *Function Name : LdpDiffServModifyTnlPrioList                                   * 
 *Description   : This routine modifies the  priority of a DiffServ CRLSP 
 *                tunnel in the
 *              : priority list                                               *
 *              : Reference - Crldp -04.txt; RFC 2702                         *
 *              :                                                             *
 * Input(s)     : u4CurrIfIndex - refers to the tunnels current interface     *
 *              : u4Priority - refers to tunnels current priority             *
 *              : u4NewIfIndex - refers to the tunnels new interface          *
 *              : u1NewPriority - refers the tunnels new priority             *
 *              : pCrlspTnlInfoInput- Pointer to tunnel info structure of the*
 *              : tunnel to be added                                          *
 * Output(s)    : None                                                        *
 ******************************************************************************/
VOID
LdpDiffServModifyTnlPrioList (UINT4 u4CurrIfIndex, UINT1 u1CurrPriority,
                              UINT4 u4NewIfIndex, UINT1 u1NewPriority,
                              tCrlspTnlInfo * pCrlspTnlInfoInput)
{
    /* The tunnel is moved from one interface to another and its priority is 
     * modified. 
     * This involves removing of a tunnel from one priority list identified by 
     * the current priority and current Interface Index and adding to the new 
     * priority list determined by the new priority and new interface index.
     */

    LdpDiffServRemTnlFromPrioList (u4CurrIfIndex, u1CurrPriority,
                                   pCrlspTnlInfoInput);

    LdpDiffServAddTnlToPrioList (u4NewIfIndex, u1NewPriority,
                                 pCrlspTnlInfoInput);

    return;
}

/*****************************************************************************/
/* Function Name : LdpDiffServPreEmptTunnels                                 */
/* Description   : This routine preempts the tunnels present in the list     */
/*                 pCandidatePreemp                                          */
/* Input(s)      : pCandidatePreemp                                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpDiffServPreEmptTunnels (tTMO_SLL * pCandidatePreemp)
{
    tTMO_SLL_NODE      *pCandidateNode = NULL;
    tCrlspTnlInfo      *pCrlspTnlInfo = NULL;
    UINT1               u1Event;
    UINT1               u1StatusType = LDP_STAT_TYPE_CRLSP_PREEMPTED;

    /* Scan through the list of preemption tunnels and if any tunnel
     * is found to be a stacked tunnel,LdpPreEmptStackTunnels is called
     * which takes care of deleting all the stacked tunnels,which might
     * be in turn stacked. 
     */

    pCandidateNode = TMO_SLL_First (pCandidatePreemp);
    while (pCandidateNode != NULL)
    {
        pCrlspTnlInfo = (tCrlspTnlInfo *) (VOID *) ((UINT1 *) pCandidateNode -
                                                    VAR_OFFSET (tCrlspTnlInfo,
                                                                NextPreemptTnl));
        /* If the tunnel has a modification and is on the same IfIndex it will 
         * be also preempted.
         */
        if (CRLSP_INS_POIN (pCrlspTnlInfo) != NULL)
        {
            if (LCB_OUT_IFINDEX (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo))) ==
                LCB_OUT_IFINDEX (CRLSP_LCB ((pCrlspTnlInfo))))
            {
                u1Event = LDP_LSM_EVT_PREMPTED;
                LdpDiffServTcFreeResources (CRLSP_INS_POIN (pCrlspTnlInfo));
                LdpDiffServRemTnlFromPrioList (LCB_OUT_IFINDEX
                                               (CRLSP_LCB
                                                (CRLSP_INS_POIN
                                                 (pCrlspTnlInfo))),
                                               CRLSP_HOLD_PRIO (CRLSP_INS_POIN
                                                                (pCrlspTnlInfo)),
                                               CRLSP_INS_POIN (pCrlspTnlInfo));
                LDP_NON_MRG_FSM[LCB_STATE
                                (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo)))]
                    [u1Event] (CRLSP_LCB (CRLSP_INS_POIN (pCrlspTnlInfo)),
                               &u1StatusType);
            }
            CRLSP_INS_POIN (pCrlspTnlInfo) = NULL;
        }
        while ((TMO_SLL_Count (CRLSP_STACK_LIST (pCrlspTnlInfo)) != LDP_ZERO))
        {
            LdpPreEmptStackTunnels (CRLSP_STACK_LIST (pCrlspTnlInfo));
        }
        u1Event = LDP_LSM_EVT_PREMPTED;
        LdpDiffServTcFreeResources (pCrlspTnlInfo);
        LdpDiffServRemTnlFromPrioList (LCB_OUT_IFINDEX (CRLSP_LCB
                                                        (pCrlspTnlInfo)),
                                       CRLSP_HOLD_PRIO (pCrlspTnlInfo),
                                       pCrlspTnlInfo);
        LDP_NON_MRG_FSM[LCB_STATE (CRLSP_LCB (pCrlspTnlInfo))][u1Event]
            (CRLSP_LCB (pCrlspTnlInfo), &u1StatusType);
        TMO_SLL_Delete (pCandidatePreemp, pCandidateNode);
        pCandidateNode = TMO_SLL_First (pCandidatePreemp);
    }
    return;
}

/*---------------------------------------------------------------------------*/
/*                       End of file ldpcrlpm.c                              */
/*---------------------------------------------------------------------------*/
