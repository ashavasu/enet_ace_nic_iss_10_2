/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpvcusm.c,v 1.20 2016/02/28 10:58:23 siva Exp $
 *
 * Description: This file contains VC Merge Upstream state machine routines.
 ********************************************************************/

#include "ldpincs.h"
#include "ldpgblex.h"
#include "ldpvcusm.h"

/*****************************************************************************/
/* Function Name : LdpInformNewRtToNhSm                                      */
/* Description   : This F'n  sends the New Nh info to the Next Hop Merge     */
/*                 state machine.                                            */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

UINT1
LdpInformNewRtToNhSm (tLspCtrlBlock * pDnstrCtrlBlk,
                      const tLdpEntity * pLdpEntity,
                      tLdpRouteEntryInfo * pRouteInfo)
{
    tTMO_SLL           *pUpstCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tUstrLspCtrlBlock  *pUpstrCtrlBlock = NULL;
    tLdpMsgInfo         LdpMsg;

    MEMSET (&LdpMsg, LDP_ZERO, sizeof (tLdpMsgInfo));
    LDP_MSG_PTR (&LdpMsg) = (UINT1 *) pRouteInfo;
    pUpstCtrlBlkList = &LCB_UPSTR_LIST (pDnstrCtrlBlk);

    if ((LDP_ENTITY_MRG_TYPE (pLdpEntity) == LDP_VC_MRG) ||
        (LDP_ENTITY_MRG_TYPE (pLdpEntity) == LDP_VP_MRG) ||
        (LDP_ENTITY_MRG_TYPE (pLdpEntity) == LDP_VPVC_MRG))
    {
        TMO_SLL_Scan (pUpstCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
        {
            pUpstrCtrlBlock = SLL_TO_UPCB (pSllNode);
            LdpMergeUpSm (pUpstrCtrlBlock, LDP_USM_EVT_INT_NEW_NH, &LdpMsg);
        }
        return LDP_SUCCESS;
    }
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpMergeUpSm                                              */
/* Description   : This F'n  defines Upstream Control Block Merge State      */
/*                 machine.                                                  */
/* Input(s)      : pUStrCtrlBlock, u1Event, pMsg                             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMergeUpSm (tUstrLspCtrlBlock * pUStrCtrlBlock, UINT1 u1Event,
              tLdpMsgInfo * pMsg)
{
    LDP_DBG2 (LDP_ADVT_SEM,
              "Upstream Control Block Current State: %s Evt %s \n",
              au1LdpVcUpStates[pUStrCtrlBlock->u1LspState],
              au1LdpVcUpEvents[u1Event]);

    gLdpMrgUpFsm[LCB_STATE (pUStrCtrlBlock)][u1Event] (pUStrCtrlBlock, pMsg);
    LDP_DBG1 (LDP_ADVT_SEM,
              "Upstream Control Block Next  State: %s \n",
              au1LdpVcUpStates[pUStrCtrlBlock->u1LspState]);
    return;
}

/*****************************************************************************/
/* Function Name : LdpGetUstrCtrlBlock                                       */
/* Description   : This routine locates the Upstream control block           */
/*                 corresponding to the session and message.                 */
/* Input(s)      : pSessionEntry, u2MsgType, u4LabelOrReqId                  */
/* Output(s)     : None                                                      */
/* Return(s)     : pointer to LSP Control Block/NULL                         */
/*****************************************************************************/

tUstrLspCtrlBlock  *
LdpGetUstrCtrlBlock (tLdpSession * pSessionEntry,
                     UINT2 u2MsgType, UINT4 u4LabelOrReqId, tFec Fec)
{
    UINT1               u1Found = LDP_FALSE;
    tTMO_SLL           *pUstrCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tUstrLspCtrlBlock  *pUstrCtrlBlock = NULL;

    switch (u2MsgType)
    {
        case LDP_LBL_REQUEST_MSG:
            pUstrCtrlBlkList = &SSN_ULCB (pSessionEntry);
            TMO_SLL_Scan (pUstrCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
            {
                pUstrCtrlBlock = (tUstrLspCtrlBlock *) pSllNode;
                /* For DU case, LSR sends label mapping message to upstream for
                 * the FEC when it recognizes that FEC. If LSR receives label
                 * request message after sending label mapping message, it has
                 * to use the same upstream block which is used for sending 
                 * label mapping message
                 * */

                if ((SSN_ADVTYPE (pSessionEntry) == LDP_DSTR_UNSOLICIT) &&
                    ((MEMCMP (&LCB_FEC (pUstrCtrlBlock), &Fec, sizeof (tFec)))
                     == 0) && (UPSTR_UREQ_ID (pUstrCtrlBlock) != u4LabelOrReqId)
                    && (LCB_STATE (pUstrCtrlBlock) == LDP_DU_UP_LSM_ST_EST))
                {
                    return pUstrCtrlBlock;
                }

                if (UPSTR_UREQ_ID (pUstrCtrlBlock) == u4LabelOrReqId)
                {
                    break;
                }
            }
            if (pSllNode != NULL)
            {
                LDP_DBG (LDP_ADVT_PRCS, "ADVT: Duplicate LabelRequestMsg Rx\n");
                return NULL;
            }
            pUstrCtrlBlock = NULL;
            LdpCreateUpstrCtrlBlock (&pUstrCtrlBlock);
            if (pUstrCtrlBlock == NULL)
            {
                return NULL;
            }
            UPSTR_TRIG (pUstrCtrlBlock) = NULL;
            return pUstrCtrlBlock;

        case LDP_LBL_RELEASE_MSG:
        case LDP_LBL_ABORT_REQ_MSG:
        case LDP_LBL_MAPPING_MSG:
        case LDP_NOTIF_FOR_LBL_REQ:
        case LDP_NOTIF_FOR_LBL_REQ_ABORT:
            pUstrCtrlBlkList = (tTMO_SLL *) (&SSN_ULCB (pSessionEntry));
            break;

        default:
            LDP_DBG (LDP_ADVT_PRCS, "ADVT: Msg with Invalid MsgType\n");
            return NULL;
    }

    /* Locate LSP Control Block */
    TMO_SLL_Scan (pUstrCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
    {
        switch (u2MsgType)
        {
            case LDP_LBL_RELEASE_MSG:
                pUstrCtrlBlock = (tUstrLspCtrlBlock *) pSllNode;
                if (LdpCompLabels (LCB_ULBL (pUstrCtrlBlock), u4LabelOrReqId,
                                   SSN_GET_LBL_TYPE (pSessionEntry))
                    == LDP_EQUAL)
                {
                    u1Found = LDP_TRUE;
                }
                break;

            case LDP_LBL_ABORT_REQ_MSG:
                pUstrCtrlBlock = (tUstrLspCtrlBlock *) pSllNode;

                if ((UPSTR_UREQ_ID (pUstrCtrlBlock) == u4LabelOrReqId) &&
                    ((MEMCMP (&LCB_FEC (pUstrCtrlBlock), &Fec, sizeof (tFec)))
                     == LDP_ZERO))
                {
                    u1Found = LDP_TRUE;
                }
                break;

            case LDP_LBL_MAPPING_MSG:
            case LDP_NOTIF_FOR_LBL_REQ:
            case LDP_NOTIF_FOR_LBL_REQ_ABORT:
                pUstrCtrlBlock = (tUstrLspCtrlBlock *) (pSllNode);
                if (UPSTR_UREQ_ID (pUstrCtrlBlock) == u4LabelOrReqId)
                {
                    u1Found = LDP_TRUE;
                }
                break;

        }
        if (u1Found == LDP_TRUE)
        {
            break;
        }
    }
    if (pSllNode == NULL)
    {
        return NULL;
    }
    return pUstrCtrlBlock;
}

/*****************************************************************************/
/* Function Name :  LdpCreateUpstrCtrlBlock                                  */
/* Description   :   This fuction creates & initialises an Upstr ctrl block  */
/*                                                                           */
/* Input(s)      : pUstrCtrlBlock - Pointer to the UpStream ctrl block       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpCreateUpstrCtrlBlock (tUstrLspCtrlBlock ** ppUstrCtrlBlock)
{

    *ppUstrCtrlBlock = (tUstrLspCtrlBlock *) MemAllocMemBlk (LDP_UPSTR_POOL_ID);

    LDP_DBG2 (LDP_ADVT_SEM, "%s: %d Creating Upstream control Block\n", __func__, __LINE__);
    if (*ppUstrCtrlBlock == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM, "ADVT: MemAlloc Failed for UPLCB\n");
        return;
    }
    LdpInitUpstrCtrlBlock (*ppUstrCtrlBlock);

    return;
}

/*****************************************************************************/
/* Function Name :  LdpInitUpstrCtrlBlock                                    */
/* Description   :   This fuction initialises the fields in the Upstream     */
/*                   Control Block structure.                                */
/*                                                                           */
/* Input(s)      : pUstrCtrlBlock - Pointer to the UpStream ctrl block       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpInitUpstrCtrlBlock (tUstrLspCtrlBlock * pUstrCtrlBlock)
{
    MEMSET (pUstrCtrlBlock, 0, sizeof (tUstrLspCtrlBlock));

    TMO_SLL_Init_Node (&UPSTR_DSSN_LIST_NODE (pUstrCtrlBlock));
    TMO_SLL_Init_Node (&UPSTR_DSTR_LIST_NODE (pUstrCtrlBlock));
    UPSTR_UREQ_ID (pUstrCtrlBlock) = LDP_INVALID_REQ_ID;
    UPSTR_DSTR_PTR (pUstrCtrlBlock) = NULL;
    UPSTR_STATE (pUstrCtrlBlock) = LDP_LSM_ST_IDLE;
    UPSTR_USSN (pUstrCtrlBlock) = NULL;
    UPSTR_TRIG (pUstrCtrlBlock) = NULL;
    UPSTR_NHOP (pUstrCtrlBlock) = NULL;
    UPSTR_ULBL (pUstrCtrlBlock).u4GenLbl = LDP_INVALID_LABEL;
    UPSTR_FEC (pUstrCtrlBlock).u1FecElmntType = LDP_FEC_INVALID_TYPE;
#ifdef LDP_GR_WANTED
    pUstrCtrlBlock->u1StaleStatus = LDP_FALSE;
#endif
    return;
}

/*****************************************************************************/
/* Function Name :  LdpDeleteUpstrCtrlBlock                                  */
/* Description   :  This function deletes the Upstream control block         */
/*                  structure                                                */
/* Input(s)      :  pUstrCtrlBlock - Pointer to the DownStream ctrl block    */
/* Output(s)     :                                                           */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpDeleteUpstrCtrlBlock (tUstrLspCtrlBlock * pUstrCtrlBlock)
{
    tTMO_SLL           *pLcbList = NULL;
    tLdpMsgInfo         LdpMsg;

    MEMSET (&LdpMsg, LDP_ZERO, sizeof (tLdpMsgInfo));
    /* The LSP Cotrol block's Internal trigger block is checked . IF this
     * is not null It is released to its mem pool.
     */
    if (UPSTR_NHOP (pUstrCtrlBlock) != NULL)
    {
        LdpDeleteTrigCtrlBlk (UPSTR_NHOP (pUstrCtrlBlock));
    }
    /*
     * Delete this LSP control blcok from upstream session's upstream LSP
     * Control block list
     */
    if (UPSTR_USSN (pUstrCtrlBlock) != NULL)
    {
        pLcbList = &SSN_ULCB (UPSTR_USSN (pUstrCtrlBlock));
        TMO_SLL_Delete (pLcbList, &(pUstrCtrlBlock->NextUpstrCtrlBlk));
    }

    if (UPSTR_DSTR_PTR (pUstrCtrlBlock) != NULL)
    {
        LDP_MSG_CTRL_BLK_PTR (&LdpMsg) = pUstrCtrlBlock;
        LdpMergeDnSm (UPSTR_DSTR_PTR (pUstrCtrlBlock),
                      LDP_DSM_EVT_INT_DEL_UPSTR, &LdpMsg);
        UPSTR_DSTR_PTR (pUstrCtrlBlock) = NULL;
    }

    TMO_SLL_Init_Node (&(pUstrCtrlBlock->NextUpstrNode));
    if ((pUstrCtrlBlock->u4InComIfIndex != 0)
        && (UPSTR_USSN (pUstrCtrlBlock) != NULL))
    {
        pUstrCtrlBlock->u4InComIfIndex = 0;
    }
    MemReleaseMemBlock (LDP_UPSTR_POOL_ID, (UINT1 *) pUstrCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpIdlLdpReq                                         */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in IDLE  state and a Label      */
/*                 Request event is received.                                */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgUpIdlLdpReq (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    UINT1               u1Flag = LDP_FALSE;
    UINT1               u1RouteStatus = LDP_TRUE;
    UINT4               u4SessIfIndex = 0;
    UINT4               u4IfIndex;
    tGenU4Addr          NextHopAddr;
    UINT1               u1HopCount;
    uLabel              Label;
    UINT2               u2PvCount;
    UINT1              *pu1PvPtr = NULL;
    tLdpSession        *pDssn = NULL;
    tLdpSession        *pLdpSession = NULL;
    tTMO_SLL           *pLspCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLspCtrlBlock      *pDnstrCtrlBlk = NULL;
    tLdpEntity         *pLdpTempEntity = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT1               u1IsIngress = LDP_FALSE;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
#endif
    /* In the case of ingress, upstream control block is
     * created and label request event is passed.  However
     * upstream control block is not placed in the
     * session control block list
     */

    MEMSET(&NextHopAddr,LDP_ZERO,sizeof(tGenU4Addr));

    if (UPSTR_USSN (pUStrCtrlBlock) != NULL)
    {
        u1IsIngress = LDP_FALSE;
    }
    else
    {
        u1IsIngress = LDP_TRUE;
    }

    if (u1IsIngress == LDP_FALSE)
    {
        pLdpSession = UPSTR_USSN (pUStrCtrlBlock);
        u4SessIfIndex = LdpSessionGetIfIndex(pLdpSession,UPSTR_FEC (pUStrCtrlBlock).u2AddrFmly);
        u2IncarnId = SSN_GET_INCRN_ID (pLdpSession);
    }

    if (LdpIsLsrEgress (&UPSTR_FEC (pUStrCtrlBlock),
                        &u1RouteStatus, &NextHopAddr.Addr, &u4IfIndex,
                        u2IncarnId) == LDP_TRUE)
    {
        /* The downstream control block should not be allocated in case the
         * LSR is EGRESS */
        if (u1IsIngress == LDP_FALSE)
        {
            if (LdpGetLabel (UPSTR_GET_ENTITY (pUStrCtrlBlock), &Label,
                             u4SessIfIndex) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Alloc Label Failed - IdleReqMsg\n");

                LdpSendNotifMsg (UPSTR_USSN (pUStrCtrlBlock),
                                 NULL, LDP_FALSE,
                                 LDP_STAT_TYPE_NO_LBL_RSRC, NULL);
                LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
                return;
            }

            MEMCPY (&UPSTR_ULBL (pUStrCtrlBlock), &Label, sizeof (uLabel));

            /* MLIB update for ILM, with just only one pop is called. */
            LdpSendMplsMlibUpdate
                (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP,
                 UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);

            /* Successful MLIB updation also done */
            if (LDP_MSG_PV_PTR (pMsg) != NULL)
            {
                pu1PvPtr = LDP_MSG_PV_PTR (pMsg) + LDP_TLV_HDR_LEN;
            }

            LdpMrgSendLblMappingMsg (pUStrCtrlBlock,
                                     LDP_MSG_HC (pMsg),
                                     pu1PvPtr, (UINT1) LDP_MSG_PV_COUNT (pMsg),
                                     &pMsg->au1UnknownTlv[0],
                                     pMsg->u1UnknownTlvLen);
            UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_EST;
            return;
        }
    }

    /* Case where the LSR is not an egress for the FEC. 
     * The downstream control block for the same FEC is searched for.
     * If not found a new downstream control block is allocated.  
     */

    if (u1RouteStatus == LDP_FALSE)
    {
        if (u1IsIngress == LDP_FALSE)
        {
            LdpSendNotifMsg (UPSTR_USSN (pUStrCtrlBlock),
                             NULL, LDP_FALSE, LDP_STAT_TYPE_NO_ROUTE, NULL);
        }
        LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
        return;
    }

    /* 
     * The LDP Peer that has advertised the Next Hop Address in its list
     * of address transmitted via the ldp address message is located.
     */
    NextHopAddr.u2AddrType=LDP_ADDR_TYPE_IPV4;
    if (LdpGetPeerSession (&NextHopAddr, u4IfIndex, &pDssn,
                           LDP_CUR_INCARN) == LDP_FAILURE)
    {
        if (u1IsIngress == LDP_FALSE)
        {
            LdpSendNotifMsg (UPSTR_USSN (pUStrCtrlBlock),
                             NULL, LDP_FALSE, LDP_STAT_TYPE_NO_ROUTE, NULL);
        }
        LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
        return;
    }

    /* Search for a downstream control block for the FEC */

    pLspCtrlBlkList = &SSN_DLCB (pDssn);
    pLdpTempEntity = SSN_GET_ENTITY (pDssn);
    u1Flag = LDP_FALSE;
    TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
    {
        pDnstrCtrlBlk = SLL_TO_LCB (pSllNode);

        if (LdpCompareFec (&LCB_FEC (pDnstrCtrlBlk),
                           &UPSTR_FEC (pUStrCtrlBlock)) == LDP_EQUAL)
        {
            if (pLdpTempEntity->pLdpAtmParams != NULL)
            {
                if (LDP_ENTITY_MRG_TYPE (pLdpTempEntity) == LDP_VC_MRG)
                {
                    if (TMO_SLL_Count (&LCB_UPSTR_LIST (pDnstrCtrlBlk)) ==
                        LDP_MAX_ATM_VC_MRG_COUNT (LDP_ENTITY_INCARNID
                                                  (pLdpTempEntity)))
                    {
                        continue;
                    }
                }
                else if (LDP_ENTITY_MRG_TYPE (pLdpTempEntity) == LDP_VP_MRG)
                {
                    if (TMO_SLL_Count (&LCB_UPSTR_LIST (pDnstrCtrlBlk)) ==
                        LDP_MAX_ATM_VP_MRG_COUNT (LDP_ENTITY_INCARNID
                                                  (pLdpTempEntity)))
                    {
                        continue;
                    }
                }
            }
            u1Flag = LDP_TRUE;
            break;
        }
    }

    /* If the incoming label request is received on DoD
     * Merge and the outgoing session is DU  Or
     * there is a downstream control block which is not 
     * in established state, then a notif is sent
     * for the request.
     */

    if (((u1Flag == LDP_FALSE) &&
         (SSN_ADVTYPE (pDssn) == LDP_DSTR_UNSOLICIT)) ||
        ((u1Flag == LDP_TRUE) &&
         (SSN_ADVTYPE (pDssn) == LDP_DSTR_UNSOLICIT) &&
         (LCB_STATE (pDnstrCtrlBlk) != LDP_LSM_ST_EST)))
    {
        if (u1IsIngress == LDP_FALSE)
        {
            LdpSendNotifMsg (UPSTR_USSN (pUStrCtrlBlock),
                             NULL, LDP_FALSE, LDP_STAT_TYPE_NO_ROUTE, NULL);
        }
        LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
        return;
    }

    /* Downstream Control Block doesn't exist */
    if (u1Flag == LDP_FALSE)
    {
        if (LdpCreateDnstrCtrlBlock (&pDnstrCtrlBlk) == LDP_FAILURE)
        {
            LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
            return;
        }
#ifdef CFA_WANTED
        /* Create a MPLS Tunnel interface at CFA  and Stack over the MPLS
         * l3 interface (which is stacked over an L3IPVLAN interface,
         * and use the interface index  returned as OutIfIndex
         */
        if (CfaIfmCreateStackMplsTunnelInterface
            (LdpSessionGetIfIndex(pDssn,UPSTR_FEC (pUStrCtrlBlock).u2AddrFmly), &u4MplsTnlIfIndex) == CFA_FAILURE)
        {

            LDP_DBG (LDP_ADVT_MEM,
                     "LdpMrgUpIdlLdpReq - ADVT: Mplstunnel IF creation failed"
                     "for DownStream Cntl block\n");
            MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pDnstrCtrlBlk);
            return;
        }
        LCB_OUT_IFINDEX (pDnstrCtrlBlk) = u4MplsTnlIfIndex;
#else
        LCB_OUT_IFINDEX (pDnstrCtrlBlk) = LdpSessionGetIfIndex(pDssn,UPSTR_FEC (pUStrCtrlBlock).u2AddrFmly);
#endif

        /* Add the FEC, session, Next Hop Addr and IfIndex to the
         * down stream ctrl block
         */
        MEMCPY ((UINT1 *) &(pDnstrCtrlBlk->Fec),
                (UINT1 *) &(UPSTR_FEC (pUStrCtrlBlock)), sizeof (tFec));
        MEMCPY ((UINT1 *) &LDP_IPV4_U4_ADDR(pDnstrCtrlBlk->NextHopAddr),
                (UINT1 *) &LDP_IPV4_U4_ADDR(NextHopAddr.Addr),IPV4_ADDR_LENGTH);
        LCB_DSSN (pDnstrCtrlBlk) = pDssn;
    }

    UPSTR_DSTR_PTR (pUStrCtrlBlock) = pDnstrCtrlBlk;

    if (LCB_STATE (pDnstrCtrlBlk) == LDP_LSM_ST_EST)
    {
        if (u1IsIngress == LDP_FALSE)
        {
            if (LdpGetLabel (UPSTR_GET_ENTITY (pUStrCtrlBlock), &Label,
                             u4SessIfIndex) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Alloc Label Failed - IdleReqMsg\n");

                LdpSendNotifMsg (UPSTR_USSN (pUStrCtrlBlock),
                                 NULL, LDP_FALSE,
                                 LDP_STAT_TYPE_NO_LBL_RSRC, NULL);
                LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
                return;
            }

            MEMCPY (&UPSTR_ULBL (pUStrCtrlBlock), &Label, sizeof (uLabel));

            LdpSendMplsMlibUpdate
                (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP_PUSH,
                 UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);

            /* Successful MLIB updation also done */

            if (LDP_MSG_PV_PTR (pMsg) != NULL)
            {
                pu1PvPtr = LDP_MSG_PV_PTR (pMsg) + LDP_TLV_HDR_LEN;
            }

            LdpMrgSendLblMappingMsg (pUStrCtrlBlock, LCB_HCOUNT (pDnstrCtrlBlk),
                                     pu1PvPtr, (UINT1) LDP_MSG_PV_COUNT (pMsg),
                                     &pMsg->au1UnknownTlv[0],
                                     pMsg->u1UnknownTlvLen);
            UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_EST;
            LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
            LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                          LDP_DSM_EVT_INT_ADD_UPSTR, pMsg);
            return;
        }
    }

    /* Downstream control block is not in the established state */
    UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_RSP_AWT;

    if (u1IsIngress == LDP_FALSE)
    {
        if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_INDEPENDENT_MODE)
        {
            u4SessIfIndex = LdpSessionGetIfIndex(pLdpSession,pUStrCtrlBlock->Fec.u2AddrFmly);
            if (LdpGetLabel
                (SSN_GET_ENTITY (pLdpSession), &Label,
                 u4SessIfIndex) != LDP_SUCCESS)
            {
                LDP_DBG (LDP_ADVT_MEM,
                         "ADVT: Alloc Label Failed - IdleReqMsg\n");
                LdpSendNotifMsg (UPSTR_USSN (pUStrCtrlBlock),
                                 NULL,
                                 LDP_FALSE, LDP_STAT_TYPE_NO_LBL_RSRC, NULL);
                LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
                return;
            }

            MEMCPY (&LCB_ULBL (pUStrCtrlBlock), &Label, sizeof (uLabel));

            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE,
                                   MPLS_OPR_POP_PUSH,
                                   UPSTR_DSTR_PTR (pUStrCtrlBlock),
                                   pUStrCtrlBlock);

            /* MLIB update being success */
            u1HopCount = 0;
            u2PvCount = 0;

            if (LDP_MSG_PV_PTR (pMsg) != NULL)
            {
                pu1PvPtr = LDP_MSG_PV_PTR (pMsg) + LDP_TLV_HDR_LEN;
            }

            LdpMrgSendLblMappingMsg (pUStrCtrlBlock, u1HopCount,
                                     pu1PvPtr, (UINT1) u2PvCount,
                                     &pMsg->au1UnknownTlv[0],
                                     pMsg->u1UnknownTlvLen);

            UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_EST;

        }

    }
    LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
    LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                  LDP_DSM_EVT_INT_ADD_UPSTR, pMsg);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpRspAwtIntDStrMap                                  */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in IDLE  state and a Label      */
/*                 Request event is received.                                */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgUpRspAwtIntDStrMap (tUstrLspCtrlBlock * pUStrCtrlBlock,
                          tLdpMsgInfo * pMsg)
{
    UINT1               u1HopCount;
    UINT1               u1PVCount;
    UINT4               u4SessIfIndex;
    uLabel              Label;
    UINT1              *pu1PvPtr = NULL;
    tLdpSession        *pLdpSession = NULL;

    pLdpSession = UPSTR_USSN (pUStrCtrlBlock);
    u4SessIfIndex = LdpSessionGetIfIndex(pLdpSession,pUStrCtrlBlock->Fec.u2AddrFmly);

    if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_ORDERED_MODE)
    {
        if (LdpGetLabel (UPSTR_GET_ENTITY (pUStrCtrlBlock),
                         &Label, u4SessIfIndex) != LDP_SUCCESS)
        {
            /* Label Allocation is a failure. */
            LDP_DBG (LDP_ADVT_MEM, "ADVT: Alloc Label Failed - IdleReqMsg\n");
            /* Notification message with "LDP_STAT_TYPE_NO_LBL_RSRC"  sent. */
            LdpSendNotifMsg (UPSTR_USSN (pUStrCtrlBlock),
                             NULL, LDP_FALSE, LDP_STAT_TYPE_NO_LBL_RSRC, NULL);
            LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
            return;
        }
        else
        {
            if (UPSTR_DSTR_PTR (pUStrCtrlBlock) != NULL)
            {
                /* Label successfully allocated */
                LCB_ULBL (UPSTR_DSTR_PTR (pUStrCtrlBlock)) =
                    LCB_ULBL (pUStrCtrlBlock);
            }

            MEMCPY (&LCB_ULBL (pUStrCtrlBlock), &Label, sizeof (uLabel));
            /* To Connect this upstream label to the local IP 
             * forwarding module; */
            LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP,
                                   UPSTR_DSTR_PTR (pUStrCtrlBlock),
                                   pUStrCtrlBlock);

            /* Successful MLIB updation also done */
            u1HopCount = LDP_MSG_HC (pMsg);
            u1PVCount = LDP_MSG_PV_COUNT (pMsg);

            if (LDP_MSG_PV_PTR (pMsg) != NULL)
            {
                pu1PvPtr = LDP_MSG_PV_PTR (pMsg) + LDP_TLV_HDR_LEN;
            }

            LdpMrgSendLblMappingMsg (pUStrCtrlBlock, u1HopCount,
                                     pu1PvPtr, u1PVCount,
                                     &pMsg->au1UnknownTlv[0],
                                     pMsg->u1UnknownTlvLen);
            UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_EST;

        }
    }
    return;

}

/*****************************************************************************/
/* Function Name : LdpMrgUpRspAwtLdpUpAbrt                                   */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in IDLE  state and a Label      */
/*                 Request event is received.                                */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgUpRspAwtLdpUpAbrt (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
    LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                  LDP_DSM_EVT_INT_DEL_UPSTR, pMsg);
    UPSTR_DSTR_PTR (pUStrCtrlBlock) = NULL;
    LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpRspAwtIntDStrNak                                  */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in Response Awaited state and   */
/*                 Downstream Nak event is received.                         */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgUpRspAwtIntDStrNak (tUstrLspCtrlBlock * pUStrCtrlBlock,
                          tLdpMsgInfo * pMsg)
{
    /* send an ldp NAK message upstream */
    LDP_SUPPRESS_WARNING (pMsg);
    if (UPSTR_USSN (pUStrCtrlBlock) != NULL)
    {
        LdpSendNotifMsg (UPSTR_USSN (pUStrCtrlBlock),
                         NULL, LDP_FALSE, LDP_STAT_TYPE_NO_ROUTE, NULL);
    }

    LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpRspAwtUpLost                                      */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in Response Awaited state and   */
/*                 Internal Upstream Lost event is received.                 */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgUpRspAwtUpLost (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
    LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                  LDP_DSM_EVT_INT_DEL_UPSTR, pMsg);
    UPSTR_DSTR_PTR (pUStrCtrlBlock) = NULL;
    LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpRspAwtIntNewNh                                    */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in Response Awaited state and   */
/*                 New Next Hop event is received.                           */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgUpRspAwtIntNewNh (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    UINT1               u1Flag = LDP_FALSE;
    tGenU4Addr          NextHopAddr;
    UINT4               u4IfIndex;
    tFec                Fec;
    UINT1              *pu1PvPtr = NULL;
    tLdpEntity         *pLdpTempEntity = NULL;
    tTMO_SLL           *pLspCtrlBlkList = NULL;
    tTMO_SLL_NODE      *pSllNode = NULL;
    tLspCtrlBlock      *pDnstrCtrlBlk = NULL;
    tLdpSession        *pDssn = NULL;
    tLdpMsgInfo         LdpMsgInfo;
#ifdef CFA_WANTED
    UINT4               u4MplsTnlIfIndex = 0;
#endif
    MEMSET (&LdpMsgInfo, LDP_ZERO, sizeof (tLdpMsgInfo));
    MEMSET (&NextHopAddr,LDP_ZERO,sizeof(tGenU4Addr));

    MEMSET (&Fec, LDP_ZERO, sizeof (tFec));
    pDnstrCtrlBlk = UPSTR_DSTR_PTR (pUStrCtrlBlock);

    LDP_MSG_CTRL_BLK_PTR (&LdpMsgInfo) = pUStrCtrlBlock;

    LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                  LDP_DSM_EVT_INT_DEL_UPSTR, &LdpMsgInfo);
    UPSTR_DSTR_PTR (pUStrCtrlBlock) = NULL;

    /* Get the outgoing interface index and the new next hop address from
     * the "pMsg" parameter passed into this function.
     */

    u4IfIndex = ((tLdpRouteEntryInfo *) (pMsg))->u4RtIfIndx;

    MEMCPY ((UINT1 *) &LDP_IPV4_U4_ADDR(NextHopAddr.Addr),
            (UINT1 *) &(((tLdpRouteEntryInfo *) (pMsg))->LDP_IPV4_U4_ADDR(NextHop)),
            sizeof (((tLdpRouteEntryInfo *) (pMsg))->LDP_IPV4_U4_ADDR(NextHop)));

    NextHopAddr.u2AddrType=LDP_ADDR_TYPE_IPV4;

    /* Get the downstream session */
    if (LdpGetPeerSession (&NextHopAddr, u4IfIndex, &pDssn,
                           LDP_CUR_INCARN) == LDP_FAILURE)
    {
        if (UPSTR_USSN (pUStrCtrlBlock) != NULL)
        {
            LdpSendNotifMsg (UPSTR_USSN (pUStrCtrlBlock),
                             NULL, LDP_FALSE, LDP_STAT_TYPE_NO_ROUTE, NULL);
        }
        LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
        return;
    }
    /* Scan the downstream control block list for the DnsCtrlBlock for 
     * the given FEC.
     */
    pLspCtrlBlkList = &SSN_DLCB (pDssn);

    pLdpTempEntity = SSN_GET_ENTITY (pDssn);

    TMO_SLL_Scan (pLspCtrlBlkList, pSllNode, tTMO_SLL_NODE *)
    {
        pDnstrCtrlBlk = SLL_TO_LCB (pSllNode);

        if (LdpCompareFec(&LCB_FEC (pDnstrCtrlBlk),
                           &UPSTR_FEC (pUStrCtrlBlock)) == LDP_EQUAL)
        {

            if (LDP_ENTITY_MRG_TYPE (pLdpTempEntity) == LDP_VC_MRG)
            {
                if (TMO_SLL_Count (&LCB_UPSTR_LIST (pDnstrCtrlBlk)) ==
                    LDP_MAX_ATM_VC_MRG_COUNT (LDP_ENTITY_INCARNID
                                              (pLdpTempEntity)))
                {
                    continue;
                }
            }
            else if (LDP_ENTITY_MRG_TYPE (pLdpTempEntity) == LDP_VP_MRG)
            {
                if (TMO_SLL_Count (&LCB_UPSTR_LIST (pDnstrCtrlBlk)) ==
                    LDP_MAX_ATM_VP_MRG_COUNT (LDP_ENTITY_INCARNID
                                              (pLdpTempEntity)))
                {
                    continue;
                }
            }
            u1Flag = LDP_TRUE;
            break;
        }
    }

    if (u1Flag == LDP_FALSE)
    {
        if (LdpCreateDnstrCtrlBlock (&pDnstrCtrlBlk) == LDP_FAILURE)
        {
            if (UPSTR_USSN (pUStrCtrlBlock) != NULL)
            {
                LdpSendNotifMsg (UPSTR_USSN (pUStrCtrlBlock),
                                 NULL, LDP_FALSE, LDP_STAT_TYPE_NO_ROUTE, NULL);
            }
            LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
            return;
        }
#ifdef CFA_WANTED
        /* Create a MPLS Tunnel interface at CFA  and Stack over the MPLS
         * l3 interface (which is stacked over an L3IPVLAN interface,
         * and use the interface index  returned as OutIfIndex
         */
        if (CfaIfmCreateStackMplsTunnelInterface
            (LdpSessionGetIfIndex(pDssn,Fec.u2AddrFmly), &u4MplsTnlIfIndex) == CFA_FAILURE)
        {

            LDP_DBG (LDP_ADVT_MEM,
                     " LdpMrgUpRspAwtIntNewNh - ADVT: Mplstunnel"
                     " IF creation failed for DownStream Cntl block \n");
            MemReleaseMemBlock (LDP_LSP_POOL_ID, (UINT1 *) pDnstrCtrlBlk);
            return;
        }
        LCB_OUT_IFINDEX (pDnstrCtrlBlk) = u4MplsTnlIfIndex;
#else
        LCB_OUT_IFINDEX (pDnstrCtrlBlk) = LdpSessionGetIfIndex(pDssn,Fec.u2AddrFmly);
#endif

        /* Add the FEC, session, Next Hop Addr and IfIndex to the
         * down stream ctrl block
         */
        MEMCPY ((UINT1 *) &pDnstrCtrlBlk->Fec, (UINT1 *) &Fec, sizeof (Fec));
        MEMCPY ((UINT1 *) &LDP_IPV4_U4_ADDR(pDnstrCtrlBlk->NextHopAddr),
                (UINT1 *) &LDP_IPV4_U4_ADDR(NextHopAddr.Addr),IPV4_ADDR_LENGTH);
        LCB_DSSN (pDnstrCtrlBlk) = pDssn;
    }

    UPSTR_DSTR_PTR (pUStrCtrlBlock) = pDnstrCtrlBlk;

    if ((pDnstrCtrlBlk != NULL) &&
        (LCB_STATE (pDnstrCtrlBlk) == LDP_LSM_ST_EST))
    {

        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP_PUSH,
                               UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);

        /* Successful MLIB updation also done */

        if (LDP_MSG_PV_PTR (pMsg) != NULL)
        {
            pu1PvPtr = LDP_MSG_PV_PTR (pMsg) + LDP_TLV_HDR_LEN;
        }
        LdpMrgSendLblMappingMsg (pUStrCtrlBlock, LDP_MSG_HC (pMsg),
                                 pu1PvPtr, (UINT1) LDP_MSG_PV_COUNT (pMsg),
                                 &pMsg->au1UnknownTlv[0],
                                 pMsg->u1UnknownTlvLen);
        UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_EST;
        LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
        LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                      LDP_DSM_EVT_INT_ADD_UPSTR, pMsg);
    }
    else
    {
        UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_RSP_AWT;
        LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
        LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                      LDP_DSM_EVT_INT_ADD_UPSTR, pMsg);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpEstIntDStrMap                                     */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in Established state and a      */
/*                 downstream map event is received.                         */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpMrgUpEstIntDStrMap (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    UINT1              *pu1PvPtr = NULL;

    LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_MODIFY, MPLS_OPR_POP_PUSH,
                           UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);

    if (LDP_MSG_PV_PTR (pMsg) != NULL)
    {
        pu1PvPtr = LDP_MSG_PV_PTR (pMsg) + LDP_TLV_HDR_LEN;
    }

    LdpMrgSendLblMappingMsg (pUStrCtrlBlock, LDP_MSG_HC (pMsg),
                             pu1PvPtr, (UINT1) LDP_MSG_PV_COUNT (pMsg),
                             &pMsg->au1UnknownTlv[0], pMsg->u1UnknownTlvLen);

    if (UPSTR_NHOP (pUStrCtrlBlock) != NULL)
    {
        LdpDeleteUpstrCtrlBlock (NH_USTR_CTRL_BLK
                                 (UPSTR_NHOP (pUStrCtrlBlock)));

    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpEstLdpRel                                         */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in Established state and a      */
/*                 Label Release event is received.                          */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgUpEstLdpRel (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    UINT4               u4SessIfIndex;
    tLdpSession        *pLdpSession = NULL;

    pLdpSession = UPSTR_USSN (pUStrCtrlBlock);
    u4SessIfIndex = LdpSessionGetIfIndex(pLdpSession,pUStrCtrlBlock->Fec.u2AddrFmly);

    LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &UPSTR_ULBL (pUStrCtrlBlock),
                  u4SessIfIndex);
    if (UPSTR_DSTR_PTR (pUStrCtrlBlock) != NULL)
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                               UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);

        LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;

        LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                      LDP_DSM_EVT_INT_DEL_UPSTR, pMsg);
        UPSTR_DSTR_PTR (pUStrCtrlBlock) = NULL;
    }
    else
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP,
                               UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);
    }

    if (UPSTR_NHOP (pUStrCtrlBlock) != NULL)
    {
        LdpVcMergeNhopSm (UPSTR_NHOP (pUStrCtrlBlock),
                          LDP_NH_LSM_EVT_INT_DESTROY,
                          (UINT1 *) LDP_MSG_PTR (pMsg));
    }
    LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpEstIntDStrWdraw                                   */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in Established state and a      */
/*                 Downstream WithDraw event is received.                    */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgUpEstIntDStrWdraw (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    uLabel              Label;
    tLdpSession        *pLdpSession = NULL;

    pLdpSession = UPSTR_USSN (pUStrCtrlBlock);

    Label = UPSTR_ULBL (pUStrCtrlBlock);

    if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_INDEPENDENT_MODE)
    {

        LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
        LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                      LDP_DSM_EVT_INT_DEL_UPSTR, (tLdpMsgInfo *) pMsg);

        UPSTR_DSTR_PTR (pUStrCtrlBlock) = NULL;

        UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_IDLE;

        LdpMergeUpSm (pUStrCtrlBlock, LDP_USM_EVT_INT_LDP_REQ,
                      (tLdpMsgInfo *) pMsg);
    }
    else
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                               UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);
        LdpSendLblWithdrawMsg (&UPSTR_FEC (pUStrCtrlBlock),
                               &Label, pLdpSession);
        UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_REL_AWT;

        /* Deleting the upstream node from the dstr list 
         * after wdraw the dstr block is released */

        LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
        LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                      LDP_DSM_EVT_INT_DEL_UPSTR, (tLdpMsgInfo *) pMsg);

        UPSTR_DSTR_PTR (pUStrCtrlBlock) = NULL;
    }
    if (UPSTR_NHOP (pUStrCtrlBlock) != NULL)
    {
        LdpVcMergeNhopSm (UPSTR_NHOP (pUStrCtrlBlock),
                          LDP_NH_LSM_EVT_INT_DESTROY,
                          (UINT1 *) LDP_MSG_PTR (pMsg));
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpEstIntDStrNak                                     */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in Established state and a      */
/*                 downstream Nak event is received.                         */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgUpEstIntDStrNak (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tLdpSession        *pLdpSession = NULL;

    pLdpSession = UPSTR_USSN (pUStrCtrlBlock);

    if (SSN_GET_LBL_ALLOC_MODE (pLdpSession) == LDP_INDEPENDENT_MODE)
    {
        if (UPSTR_NHOP (pUStrCtrlBlock) != NULL)
        {
            LdpVcMergeNhopSm (UPSTR_NHOP (pUStrCtrlBlock),
                              LDP_NH_LSM_EVT_INT_DESTROY, LDP_MSG_PTR (pMsg));
        }
        else
        {

            LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
            LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                          LDP_DSM_EVT_INT_DEL_UPSTR, (tLdpMsgInfo *) pMsg);
        }

        UPSTR_DSTR_PTR (pUStrCtrlBlock) = NULL;

        UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_IDLE;

        LdpMergeUpSm (pUStrCtrlBlock, LDP_USM_EVT_INT_LDP_REQ, pMsg);
    }
    else
    {
        /* Free the resources */
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                               UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);
        LdpSendLblWithdrawMsg (&UPSTR_FEC (pUStrCtrlBlock),
                               &UPSTR_ULBL (pUStrCtrlBlock), pLdpSession);
        UPSTR_STATE (pUStrCtrlBlock) = LDP_LSM_ST_REL_AWT;

        if (UPSTR_NHOP (pUStrCtrlBlock) != NULL)
        {
            LdpVcMergeNhopSm (UPSTR_NHOP (pUStrCtrlBlock),
                              LDP_NH_LSM_EVT_INT_DESTROY, LDP_MSG_PTR (pMsg));
        }
        else
        {

            LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
            LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                          LDP_DSM_EVT_INT_DEL_UPSTR, (tLdpMsgInfo *) pMsg);
        }

        UPSTR_DSTR_PTR (pUStrCtrlBlock) = NULL;
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpEstUpLost                                         */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in Established state and        */
/*                 Upstream lost event is received.                          */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgUpEstUpLost (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    UINT4               u4SessIfIndex;
    tLdpSession        *pLdpSession = NULL;

    pLdpSession = UPSTR_USSN (pUStrCtrlBlock);
    u4SessIfIndex = LdpSessionGetIfIndex(pLdpSession,pUStrCtrlBlock->Fec.u2AddrFmly);

    LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &UPSTR_ULBL (pUStrCtrlBlock),
                  u4SessIfIndex);
    if (UPSTR_DSTR_PTR (pUStrCtrlBlock) != NULL)
    {
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP_PUSH,
                               UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);
    }
    else
    {
        /* Case of the node being an Egress */
        LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_DELETE, MPLS_OPR_POP,
                               UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);
    }
    LDP_MSG_CTRL_BLK_PTR (pMsg) = pUStrCtrlBlock;
    if (UPSTR_DSTR_PTR (pUStrCtrlBlock) != NULL)
    {
        LdpMergeDnSm (UPSTR_DSTR_PTR (pUStrCtrlBlock),
                      LDP_DSM_EVT_INT_DEL_UPSTR, (tLdpMsgInfo *) pMsg);
        UPSTR_DSTR_PTR (pUStrCtrlBlock) = NULL;
    }
    if (UPSTR_NHOP (pUStrCtrlBlock) != NULL)
    {
        LdpVcMergeNhopSm (UPSTR_NHOP (pUStrCtrlBlock),
                          LDP_NH_LSM_EVT_INT_DESTROY,
                          (UINT1 *) LDP_MSG_PTR (pMsg));
    }
    LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpEstReXConnect                                     */
/* Description   : This function performs the operations required when the  */
/*                 Upstream control block is in Established state and a      */
/*                 Internal Re Cross Connect event is received.              */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgUpEstReXConnect (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    UINT1              *pu1PvPtr = NULL;

    LDP_SUPPRESS_WARNING (pMsg);

    LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_MODIFY, MPLS_OPR_POP_PUSH,
                           UPSTR_DSTR_PTR (pUStrCtrlBlock), pUStrCtrlBlock);

    if ((LDP_MSG_PV_PTR (pMsg) != NULL) ||
        (LCB_HCOUNT (NH_TRGCB_OLSP (UPSTR_NHOP (pUStrCtrlBlock))) !=
         LDP_MSG_HC (pMsg)))
    {
        pu1PvPtr = LDP_MSG_PV_PTR (pMsg) + LDP_TLV_HDR_LEN;
        LdpMrgSendLblMappingMsg (pUStrCtrlBlock, LDP_MSG_HC (pMsg),
                                 pu1PvPtr, (UINT1) LDP_MSG_PV_COUNT (pMsg),
                                 &pMsg->au1UnknownTlv[0],
                                 pMsg->u1UnknownTlvLen);
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpEstIntNewNh                                       */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in Established  state and a     */
/*                 New Next Hop event is received.                           */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgUpEstIntNewNh (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tLspTrigCtrlBlock  *pNHCtrlBlk = NULL;
    tUstrLspCtrlBlock  *pNewUStrCtrlBlk = NULL;

    pNHCtrlBlk = (tLspTrigCtrlBlock *) MemAllocMemBlk (LDP_TRIG_POOL_ID);

    if (pNHCtrlBlk == NULL)
    {
        LDP_DBG (LDP_ADVT_MEM,
                 "ADVT: MemAlloc Failed for NHCB - EstIntNewNH\n");
        return;
    }
    MEMSET(pNHCtrlBlk,0,sizeof(tLspTrigCtrlBlock));
    /* Initialise the NH ctrl blk structure */
    pUStrCtrlBlock->pNHCtrlBlk = pNHCtrlBlk;
    pNHCtrlBlk->pUStrLspCtrlBlock = pUStrCtrlBlock;
    pNHCtrlBlk->pOrgLspCtrlBlk = UPSTR_DSTR_PTR (pUStrCtrlBlock);

    pNHCtrlBlk->pNewNhLspCtrlBlk = NULL;
    pNHCtrlBlk->u1NhLspState = LDP_NH_LSM_ST_IDLE;
    pNHCtrlBlk->u1TrgType = NEXT_HOP_CHNG_LSP_SET_UP;
    /*1. Allocate memory for the new Upstream control block and
     *   add it on to the New UstrBlk field of the NHCtrlBlk.
     *2. Copy all the info from the old Upstream Ctrl Blk to the 
     *   New Upstream Control Blk.
     *3. A new field need to be added in the NH Ctrl Blk structure.     
     */
    pNewUStrCtrlBlk = NULL;
    LdpCreateUpstrCtrlBlock (&pNewUStrCtrlBlk);
    if (pNewUStrCtrlBlk == NULL)
    {
        return;
    }
    MEMCPY (pNewUStrCtrlBlk, pUStrCtrlBlock, sizeof (tUstrLspCtrlBlock));
    NH_NEW_UPSTR_PTR (pNHCtrlBlk) = pNewUStrCtrlBlk;
    UPSTR_NHOP (NH_NEW_UPSTR_PTR (pNHCtrlBlk)) = pNHCtrlBlk;

    TMO_SLL_Init_Node (&UPSTR_DSTR_LIST_NODE (pNewUStrCtrlBlk));
    UPSTR_DSTR_PTR (pNewUStrCtrlBlk) = NULL;

    /* "pMsg" param passed as a parameter to this function 
     * contains the route information regarding the new next hop.
     */

    LdpVcMergeNhopSm (UPSTR_NHOP (pUStrCtrlBlock), LDP_NH_LSM_EVT_INT_NEW_NH,
                      (UINT1 *) pMsg);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpRelAwtLdpRel                                      */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in REL AWT  state and a Label   */
/*                 Release event is received.                                */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgUpRelAwtLdpRel (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    UINT4               u4SessIfIndex;
    tLdpSession        *pLdpSession = NULL;

    pLdpSession = UPSTR_USSN (pUStrCtrlBlock);
    u4SessIfIndex = LdpSessionGetIfIndex(pLdpSession,pUStrCtrlBlock->Fec.u2AddrFmly);
    LDP_SUPPRESS_WARNING (pMsg);
    LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &UPSTR_ULBL (pUStrCtrlBlock),
                  u4SessIfIndex);
    LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpRelAwtLdpUpAbrt                                   */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in REL AWT  state and a Label   */
/*                 Request Abort event is received.                          */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgUpRelAwtLdpUpAbrt (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    UINT4               u4SessIfIndex;
    tLdpSession        *pLdpSession = NULL;

    LDP_SUPPRESS_WARNING (pMsg);

    pLdpSession = UPSTR_USSN (pUStrCtrlBlock);

    u4SessIfIndex = LdpSessionGetIfIndex(pLdpSession,pUStrCtrlBlock->Fec.u2AddrFmly);
    LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &UPSTR_ULBL (pUStrCtrlBlock),
                  u4SessIfIndex);
    LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpRelAwtUpLost                                      */
/* Description   : This function performs the operations required when the   */
/*                 Upstream control block is in REL AWT  state and a Upstream*/
/*                 Lost event is received.                                   */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgUpRelAwtUpLost (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    tLdpSession        *pLdpSession = NULL;
    UINT4               u4SessIfIndex;

    LDP_SUPPRESS_WARNING (pMsg);
    pLdpSession = UPSTR_USSN (pUStrCtrlBlock);

    u4SessIfIndex = LdpSessionGetIfIndex(pLdpSession,pUStrCtrlBlock->Fec.u2AddrFmly);
    LdpFreeLabel (SSN_GET_ENTITY (pLdpSession), &UPSTR_ULBL (pUStrCtrlBlock),
                  u4SessIfIndex);

    LdpDeleteUpstrCtrlBlock (pUStrCtrlBlock);
    return;
}

/*****************************************************************************/
/* Function Name : LdpMrgUpInvStateEvt                                       */
/* Description   : This routine is stub routine that's called on an occurence*/
/*                 of an invalid event in a state.                           */
/* Input(s)      : pUStrCtrlBlock,  pMsg                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpMrgUpInvStateEvt (tUstrLspCtrlBlock * pUStrCtrlBlock, tLdpMsgInfo * pMsg)
{
    LDP_DBG (LDP_ADVT_MISC, "ADVT: Up Str Lcb st m/c Invalid State \n");
    LDP_SUPPRESS_WARNING (pMsg);
    LDP_SUPPRESS_WARNING (pUStrCtrlBlock);
    return;
}
