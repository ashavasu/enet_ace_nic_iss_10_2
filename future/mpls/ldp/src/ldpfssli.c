/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldpfssli.c,v 1.55 2015/11/02 09:28:15 siva Exp $
 *
 * Description: This file contains entry point function to LDP to
 *              TCP & UDP Interface supporting routines.
 ********************************************************************/
#define _LDP_SLI_C

#include "ldpincs.h"
#include "fssocket.h"
#include "ipv6.h"
#include "../../../inc/rtm.h"

PRIVATE INT4        LdpSendMessage (UINT1 *pu1Data, UINT2 u2BufLen,
                                    UINT4 u4DestAddr, UINT2 u2DestPort,
                                    UINT4 u4IfAddr);
PRIVATE INT4        LdpSendTo (UINT1 *pu1Data, UINT2 u2BufLen,
                               UINT4 u4DestAddr, UINT2 u2DestPort);

PRIVATE VOID        LdpStoreTcpSourcePort (tLdpTcpUdpSockInfo
                                           * pLdpTcpUdpSockInfo);

/* MPLS_IPv6 mod start*/ 
#ifdef MPLS_IPV6_WANTED
PRIVATE INT4        LdpIpv6SendTo (UINT1 *pu1Data, UINT2 u2BufLen,
                               tIp6Addr u4DestAddr, UINT2 u2DestPort);
PRIVATE INT4        LdpSendIpv6Message (UINT1 *pu1Data, UINT2 u2BufLen,
                                    tIp6Addr u4DestAddr, UINT2 u2DestPort,
                                    tIp6Addr u4IfAddr, UINT4 u4IfIndex);
#endif
/* MPLS_IPv6 mod start*/ 

/*****************************************************************************/
/* Function Name : LdpTcpInitSockInfo                                        */
/* Description   : This function initializes the LDP TCP sockets             */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
INT4
LdpTcpInitSockInfo ()
{
    /* Allocate memory for SockInfo Table */
    gpLdpTcpUdpSockInfoTab = (tLdpTcpUdpSockInfo *)
        MemAllocMemBlk (LDP_SOCK_INFO_TAB_POOL_ID);

    if (gpLdpTcpUdpSockInfoTab == NULL)
    {
        LDP_DBG (LDP_IF_MEM,
                 "LDPTCP: TCP Init Memory Allocation for Sock Info Table\r\n");
        return LDP_FAILURE;
    }

    /* Initialising the SockInfo Table */
    MEMSET (gpLdpTcpUdpSockInfoTab, 0, LDP_MAX_TCP_UDP_SOCK);

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpTcpDeInitSockInfo                                      */
/* Description   : This function deinitializes the LDP TCP sockets           */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTcpDeInitSockInfo ()
{
    MemReleaseMemBlock (LDP_SOCK_INFO_TAB_POOL_ID,
                        (UINT1 *) gpLdpTcpUdpSockInfoTab);
    gpLdpTcpUdpSockInfoTab = NULL;
}

/*****************************************************************************/
/* Function Name : LdpProcessTcpMsg                                          */
/* Description   : This function handles all the events and                  */
/*                 inputs related to LDP - TCP task                          */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpProcessTcpMsg (VOID)
{
    UINT4               u4DeQSate;
    tLdpTcpUdpEnqMsgInfo LdpTcpUdpEnqMsgInfo;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4               u4RelinquishCtr = 0;

    MEMSET (&LdpTcpUdpEnqMsgInfo, LDP_ZERO, sizeof (tLdpTcpUdpEnqMsgInfo));
	if(LDP_TCP_QID == NULL)
	{
		return;
	}
    u4DeQSate = (OsixQueRecv (LDP_TCP_QID, (UINT1 *) &pBuf,
                              OSIX_DEF_MSG_LEN, OSIX_NO_WAIT));
    while (u4DeQSate == OSIX_SUCCESS)
    {
        if (pBuf != NULL)
        {
            if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &LdpTcpUdpEnqMsgInfo,
                                           0, sizeof (tLdpTcpUdpEnqMsgInfo)) !=
                sizeof (LdpTcpUdpEnqMsgInfo))
            {
                LDP_DBG (LDP_IF_MEM,
                         "LDP_TCP : Error while copying from "
                         "the buffer chain\n");
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                continue;
            }

            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

            switch (LdpTcpUdpEnqMsgInfo.u4Event)
            {

                case LDP_TCP_OPEN_REQ_EVENT:
                    /* Case when LDP has requested for a new connection */
		     LdpTcpProcOpenReqEvent (&LdpTcpUdpEnqMsgInfo);
                    break;
                case LDP_TCP_CLOSE_REQ_EVENT:
                    /* Case when LDP has requested for a connection close */
		     LdpTcpProcCloseReqEvent (&LdpTcpUdpEnqMsgInfo);
                    break;

                case LDP_TCP_SEND_REQ_EVENT:
                    /* Case when LDP has requested for data to be sent 
                     * on a conn */
                    break;
                case LDP_TCP_RECV_REQ_EVENT:
                    LdpTcpProcRecvReqEvent (&LdpTcpUdpEnqMsgInfo);
                    break;
#ifdef MPLS_IPV6_WANTED
		  /* MPLS_IPv6 add start*/
		  case LDP_TCP6_RECV_REQ_EVENT:
		  	LdpTcp6ProcRecvReqEvent (&LdpTcpUdpEnqMsgInfo);
			break;
		  /* MPLS_IPv6 add end*/
#endif
                default:
                    break;
            }
        }

        u4RelinquishCtr++;

        if (u4RelinquishCtr > LDP_RELINQUISH_CNTR)
        {
            LDP_DBG1 (LDP_IF_RX,
                      "LDPTCP: More than %d messages processed\n",
                      LDP_RELINQUISH_CNTR);

            u4RelinquishCtr = 0;

            /*Process the Timer Expiry Events */
            LdpProcessTimerEvents ();

            /*Process the Udp Messages */
#ifdef MPLS_IPV6_WANTED
             if(LdpTcpUdpEnqMsgInfo.u1AddrType == LDP_ADDR_TYPE_IPV6)
             {
                 LdpProcessIpv6UdpMsgs ();

             }
	      else
	      {
#endif
                 LdpProcessUdpMsgs ();
#ifdef MPLS_IPV6_WANTED
	      }
#endif

        }

        u4DeQSate = (OsixQueRecv (LDP_TCP_QID, (UINT1 *) (&pBuf),
                                  OSIX_DEF_MSG_LEN, OSIX_NO_WAIT));
    }
}

/*****************************************************************************/
/* Function Name : LdpTcpProcOpenReqEvent                                    */
/* Description   : This routine handles a open request.                      */
/* Input(s)      : aTaskEnqMsgInfo  - Array of UINT4 .                       */
/*                 First byte indicates event.                               */
/*                 Second byte has ptr to Socket info structure.             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTcpProcOpenReqEvent (tLdpTcpUdpEnqMsgInfo * pLdpTcpUdpEnqMsgInfo)
{
    UINT1               u1RetVal;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
#ifdef LNXIP4_WANTED
    struct linger so_linger;
#endif   

    /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED	
    struct sockaddr_in6 LDPTcp6ServAddr;
    tIp6Addr		Temp6SrcAddr;
    tIp6Addr		Temp6DestAddr;
    tIp6Addr  		Temp6ZeroAddr;
#ifdef LNXIP6_WANTED
    INT4                i4OptVal = 1;
#endif
#endif	
    /* MPLS_IPv6 mod end*/    	
    struct sockaddr_in  LdpTcpServAddr;
#ifdef BSDCOMP_SLI_WANTED
    INT4                i4TestData = 1;
#endif

    UINT1              *pu1TempMD5password = NULL;
#ifndef BSDCOMP_SLI_WANTED
    UINT4               u4VrfId = 0;
#endif
    INT4                i4MD5KeyCount;
    UINT4               u4SndBuf = LDP_MAX_FRAGMENT;
    UINT4               u4RcvBuf = LDP_MAX_FRAGMENT;
    UINT4               u4TempSrcAddr = 0;
    UINT4               u4TempDestAddr = 0;

    pLdpTcpUdpSockInfo = LdpTcpUdpGetSocket
        ((UINT4)pLdpTcpUdpEnqMsgInfo->i4SockFdOrConnId, LDP_SOCK_MASK);

    if (pLdpTcpUdpSockInfo == NULL)
    {
        LDP_DBG (LDP_IF_MISC, "LDPTCP : Conn Open Failure.\n");
        return;
    }

#ifdef MPLS_IPV6_WANTED	
    /* MPLS_IPv6 mod start*/
    if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpEnqMsgInfo->u1AddrType)
    {
        pLdpTcpUdpSockInfo->u1AddrType = LDP_ADDR_TYPE_IPV6;
	 MEMSET(&Temp6SrcAddr, 0, sizeof(tIp6Addr));
	 MEMSET(&Temp6DestAddr, 0, sizeof(tIp6Addr));
	 MEMSET(&LDPTcp6ServAddr, 0, sizeof(struct sockaddr_in6));
	 MEMSET(&Temp6ZeroAddr, 0, sizeof(tIp6Addr));
        pLdpTcpUdpSockInfo->i4SockDesc = socket (AF_INET6, SOCK_STREAM, IPPROTO_TCP);
	 MEMCPY(Temp6SrcAddr.u1_addr, LDP_IPV6_ADDR(pLdpTcpUdpSockInfo->SrcAddr), LDP_IPV6ADR_LEN);
	 MEMCPY(Temp6DestAddr.u1_addr, LDP_IPV6_ADDR(pLdpTcpUdpSockInfo->DestAddr), LDP_IPV6ADR_LEN);
    }
    else
    {
 #endif
        pLdpTcpUdpSockInfo->u1AddrType = LDP_ADDR_TYPE_IPV4;
        pLdpTcpUdpSockInfo->i4SockDesc = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
	 MEMCPY(&u4TempSrcAddr, LDP_IPV4_ADDR(pLdpTcpUdpSockInfo->SrcAddr), LDP_IPV4ADR_LEN);
	 MEMCPY(&u4TempDestAddr, LDP_IPV4_ADDR(pLdpTcpUdpSockInfo->DestAddr), LDP_IPV4ADR_LEN);

    /*Set the linger time out to 0 to make the linux to close the socket *
     * created for the tcp connection without waiting in the TIME_WAIT state */
#ifdef LNXIP4_WANTED
    so_linger.l_onoff = TRUE;
    so_linger.l_linger = 0;
    setsockopt(pLdpTcpUdpSockInfo->i4SockDesc, SOL_SOCKET, SO_LINGER,
                                   &so_linger,
                                       sizeof so_linger);
#endif

#ifdef MPLS_IPV6_WANTED	
    }
#endif	
    /* MPLS_IPv6 mod end*/	
    
    if (pLdpTcpUdpSockInfo->i4SockDesc < 0)
    {

	 if (pLdpTcpUdpSockInfo->u1SockType == TCP_CLI_OPEN)
        {
	     LdpTcpEventHandler (TCP_CLI_OPEN_FAILURE, 0,
                                pLdpTcpUdpSockInfo->u4ConnId, NULL);
        }
        else
        {
            /* 
             * The LDP should shut down after receiving this event,
             * as the LDP TCP Server itself is not created.
             */
            LdpTcpEventHandler (TCP_SRVR_OPEN_FAILURE, 0,
                                pLdpTcpUdpSockInfo->u4ConnId, NULL);
        }
        LDP_DBG (LDP_IF_MISC, "LDPTCP : Can't open stream socket\n");
        LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
        return;
    }

    if (gu4LdpMd5Option == TRUE)
    {
        pu1TempMD5password = pLdpTcpUdpEnqMsgInfo->pu1MD5Info;
        i4MD5KeyCount = (INT4)STRLEN (pu1TempMD5password);

        if (setsockopt (pLdpTcpUdpSockInfo->i4SockDesc, SOL_SOCKET,
                        LDP_TCP_MD5_OPTION,
                        (INT1 *) pu1TempMD5password, i4MD5KeyCount) < 0)
        {
            LDP_DBG (LDP_MAIN_MISC, "setsockopt failed for the MD5 password\n");
            return;
        }
    }

    /* For Passive Open */
    if (pLdpTcpUdpSockInfo->u1SockType == TCP_SERV_OPEN)
    {
        MEMSET ((INT1 *) &LdpTcpServAddr, 0, sizeof (LdpTcpServAddr));
	/* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
	 if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpEnqMsgInfo->u1AddrType)
	 {
            LDPTcp6ServAddr.sin6_family = AF_INET6;
            #ifdef BSDCOMP_SLI_WANTED
            LDPTcp6ServAddr.sin6_addr =  in6addr_any;
            #else
            LDPTcp6ServAddr.sin6_addr = (struct in6_addr) IN6_ADDR_ANY;
            #endif
	     LDPTcp6ServAddr.sin6_port = OSIX_HTONS (pLdpTcpUdpSockInfo->u2Port);
	     /* Bind to local device */	 
#ifdef LNXIP6_WANTED
            if (setsockopt
                (pLdpTcpUdpSockInfo->i4SockDesc, IPPROTO_IPV6, IPV6_V6ONLY,
                &i4OptVal, sizeof (INT4)) < 0)
             {
                 /* setsockopt Failed for IP_PKTINFO */
                 LDP_DBG (LDP_IF_UDP, "IF: setsockopt for IPV6_V6ONLY failed \n");
                 return ;
             }
#endif
	     if (bind (pLdpTcpUdpSockInfo->i4SockDesc,
                  (struct sockaddr *) &LDPTcp6ServAddr,
                  sizeof (LDPTcp6ServAddr)) < 0)
                {
                     LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Server : Can't bind local address \n");
                     LDP_SOCK_EVENT_INDICATE (TCP_SRVR_OPEN_FAILURE, pLdpTcpUdpSockInfo);
                     return;
                }		
	 }
	 else
	 {
#endif
            LdpTcpServAddr.sin_family = AF_INET;
            LdpTcpServAddr.sin_addr.s_addr = OSIX_HTONL (IN_ADDR_ANY);
	     LdpTcpServAddr.sin_port = OSIX_HTONS (pLdpTcpUdpSockInfo->u2Port);
	      if (bind (pLdpTcpUdpSockInfo->i4SockDesc,
                  (struct sockaddr *) &LdpTcpServAddr,
                  sizeof (LdpTcpServAddr)) < 0)
            {
                LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Server : Can't bind local address \n");
                LDP_SOCK_EVENT_INDICATE (TCP_SRVR_OPEN_FAILURE, pLdpTcpUdpSockInfo);
                return;
            }	
#ifdef MPLS_IPV6_WANTED
	 }
#endif
	 /* MPLS_IPv6 mod end*/	
        /* Set socket in Non blocking Mode */
        if ((fcntl (pLdpTcpUdpSockInfo->i4SockDesc, F_SETFL, O_NONBLOCK)) < 0)
        {
            LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Server : Can't Set Sckt in NON BLK\n");
            LDP_SOCK_EVENT_INDICATE (TCP_SRVR_OPEN_FAILURE, pLdpTcpUdpSockInfo);
            return;
        }

        /* Listens on socket */
        if (listen (pLdpTcpUdpSockInfo->i4SockDesc, LDP_TCP_BACKLOG) < 0)
        {
            LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Server : Can't listen on local address.\n");
            LDP_SOCK_EVENT_INDICATE (TCP_SRVR_OPEN_FAILURE, pLdpTcpUdpSockInfo);
            return;
        }
        pLdpTcpUdpSockInfo->u1SockStatus = SOCK_SERVER;
        pLdpTcpUdpSockInfo->b1IsSockReadFdReqd = TRUE;
    }                            /* TCP_SERV_OPEN */

    /* For Active Open */
    if (pLdpTcpUdpSockInfo->u1SockType == TCP_CLI_OPEN)
    {
        MEMSET ((INT1 *) &LdpTcpServAddr, 0, sizeof (LdpTcpServAddr));

#ifdef BSDCOMP_SLI_WANTED
        if (setsockopt (pLdpTcpUdpSockInfo->i4SockDesc, SOL_SOCKET,
                        SO_REUSEADDR, (void *) &i4TestData,
                        sizeof (i4TestData)) < 0)
        {
            LDP_DBG (LDP_IF_MISC, "LDPTCP: Client : Can't Set REUSEADDR for "
                     "Socket\n");
            LDP_SOCK_EVENT_INDICATE (TCP_CLI_OPEN_FAILURE, pLdpTcpUdpSockInfo);
            return;
        }
#endif

        /*
         * Fill in the structure 'LdpTcpServAddr' with the address of the
         * client (Transport Address)
         */
               /* MPLS_IPv6 mod start*/

#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpEnqMsgInfo->u1AddrType)
        {
        	LDPTcp6ServAddr.sin6_family = AF_INET6;
        	LDPTcp6ServAddr.sin6_port = OSIX_HTONS (pLdpTcpUdpSockInfo->u2SrcPort);
		LDPTcp6ServAddr.sin6_scope_id = pLdpTcpUdpEnqMsgInfo->u4SsnTcpOpenIndex;
		LDP_DBG1 (LDP_IF_MISC, "LDPTCP: Client : Interface Index Filled to sin6_scope_id %x ",
                     LDPTcp6ServAddr.sin6_scope_id);
	 	MEMCPY(LDPTcp6ServAddr.sin6_addr.s6_addr, Temp6SrcAddr.u1_addr, LDP_IPV6ADR_LEN);
        }
	 else
	 {
#endif
            LdpTcpServAddr.sin_family = AF_INET;
            LdpTcpServAddr.sin_addr.s_addr = (u4TempSrcAddr);
	     /* MPLS_IPv6 mod end*/
            LdpTcpServAddr.sin_port = OSIX_HTONS (pLdpTcpUdpSockInfo->u2SrcPort);
#ifdef MPLS_IPV6_WANTED
	 }
#endif
#ifndef BSDCOMP_SLI_WANTED
#ifdef VCM_WANTED
        VcmIsVrfExist (pLdpTcpUdpSockInfo->au1VrfName, &u4VrfId);
#endif
        if (u4VrfId != LDP_ZERO)
        {
            /* Following piece of code is tested only in FSIP as equivalent option
             * (context id) is not available in LINUX IP
             * */
#ifdef MPLS_IPV6_WANTED
            if(LDP_ADDR_TYPE_IPV6== pLdpTcpUdpEnqMsgInfo->u1AddrType)
            {
                if (setsockopt
                 (pLdpTcpUdpSockInfo->i4SockDesc, IPPROTO_IPV6, IP_PKT_TX_CXTID,
                 (VOID *) &u4VrfId, sizeof (UINT4)) < 0)
                {
                    LDP_DBG (LDP_IF_MISC,
                         "\rLDPTCP: Client : Can't Set CONTEXT ID for "
                         "V6 Socket\n");
                   LDP_SOCK_EVENT_INDICATE (TCP_CLI_OPEN_FAILURE,
                                         pLdpTcpUdpSockInfo);
                  return;
                }
            }
	     else
	     {
#endif
                if (setsockopt
                    (pLdpTcpUdpSockInfo->i4SockDesc, IPPROTO_IP, IP_PKT_TX_CXTID,
                     (VOID *) &u4VrfId, sizeof (UINT4)) < 0)
                {
                    LDP_DBG (LDP_IF_MISC,
                         "\rLDPTCP: Client : Can't Set CONTEXT ID for "
                         "Socket\n");
                LDP_SOCK_EVENT_INDICATE (TCP_CLI_OPEN_FAILURE,
                                         pLdpTcpUdpSockInfo);
                return;
            }
#ifdef MPLS_IPV6_WANTED
	     }
#endif
        }
#endif
#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6== pLdpTcpUdpEnqMsgInfo->u1AddrType)
        {
            if(LDP_ZERO != MEMCMP(Temp6SrcAddr.u1_addr, Temp6ZeroAddr.u1_addr, LDP_IPV6ADR_LEN))
            {
                if (bind (pLdpTcpUdpSockInfo->i4SockDesc,
                  (struct sockaddr *) &LDPTcp6ServAddr,
                  sizeof (LDPTcp6ServAddr)) < 0)
                {
                    LDP_DBG1 (LDP_IF_MISC, "LDPTCP: Client : V6 Bind Failed With Src Addr %s\n",
						Ip6PrintAddr((tIp6Addr *)(VOID *)&(LDPTcp6ServAddr.sin6_addr)));
                    LDP_SOCK_EVENT_INDICATE (TCP_CLI_OPEN_FAILURE,
                                         pLdpTcpUdpSockInfo);
                    return;
                }
            }
        }
	 else
	 {
#endif
            if (u4TempSrcAddr != LDP_ZERO)
        {
            if (bind (pLdpTcpUdpSockInfo->i4SockDesc,
                      (struct sockaddr *) &LdpTcpServAddr,
                      sizeof (LdpTcpServAddr)) < 0)
            {
                    LDP_DBG (LDP_IF_MISC, "LDPTCP: Client : V4 Bind Failed \n");
                LDP_SOCK_EVENT_INDICATE (TCP_CLI_OPEN_FAILURE,
                                         pLdpTcpUdpSockInfo);
                return;
            }
        }
#ifdef MPLS_IPV6_WANTED
	 }
#endif
        /* Issue Socket Connect request */
#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6== pLdpTcpUdpEnqMsgInfo->u1AddrType)
        {
            u1RetVal = Sock6Connect (pLdpTcpUdpSockInfo);
        }
	 else
	 {
#endif
        u1RetVal = SockConnect (pLdpTcpUdpSockInfo);
#ifdef MPLS_IPV6_WANTED
	 }
#endif
	 switch (u1RetVal)
        {
            case CONNECT_SUCCESS:
		LDP_DBG(LDP_IF_MISC, "TcpProcOpReqEvt Connect Success \n");
                if (setsockopt (pLdpTcpUdpSockInfo->i4SockDesc, SOL_SOCKET,
                                SO_SNDBUF, (VOID *) &u4SndBuf, sizeof (UINT4))
                    < 0)
                {
                    LDP_DBG3 (LDP_IF_MEM,
                              "LDPTCP: SetSockOpt failed for Send Buffer %d "
                              "for %d %x\r\n",
                              u4SndBuf, pLdpTcpUdpSockInfo->i4SockDesc,
                              u4TempDestAddr);
                }

                if (setsockopt (pLdpTcpUdpSockInfo->i4SockDesc, SOL_SOCKET,
                                SO_RCVBUF, (VOID *) &u4RcvBuf, sizeof (UINT4))
                    < 0)
                {
                    LDP_DBG3 (LDP_IF_MEM,
                              "LDPTCP: SetSockOpt failed for Recv Buffer %d "
                              "for %d %x\r\n",
                              u4RcvBuf, pLdpTcpUdpSockInfo->i4SockDesc,
                              u4TempDestAddr);
                }

                if (LdpTcpEventHandler (LDP_TCP_CONN_UP_EVENT, CONTROL_DATA,
                                        pLdpTcpUdpSockInfo->u4ConnId,
                                        NULL) == LDP_FAILURE)
                {
                    LDP_DBG (LDP_IF_MISC,
                             "LDPTCP: Failed to Indicate TCP Conn up to LDP\n");
                    /*
                     * NOTE: If LDP_TCP_CONN_UP_EVENT indication fails, Socket 
                     * is closed.  LDP might infinitely wait for socket conn. 
                     * up event.
                     * However this condition will not occur.
                     */
                    LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                    LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
                }
                else
                {
                    pLdpTcpUdpSockInfo->u1SockStatus = SOCK_ESTABLISHED;
                    pLdpTcpUdpSockInfo->b1IsSockReadFdReqd = TRUE;

                    LdpStoreTcpSourcePort (pLdpTcpUdpSockInfo);
                }
                break;
            case CONNECT_INPROGRESS:
                pLdpTcpUdpSockInfo->u1SockStatus = SOCK_CONNECTING;
                pLdpTcpUdpSockInfo->SockReadTimer.u4Event =
                    LDP_TCP_RECONNECT_TMR_EVT;
		  LDP_DBG (LDP_IF_MISC,
                             "LDPTCP: LdpTcpProcOpReqEvt Connect In Progress Reconnnect Tmr Start: \n");
                pLdpTcpUdpSockInfo->SockReadTimer.pu1EventInfo =
                    (UINT1 *) pLdpTcpUdpSockInfo;
                TmrStartTimer (LDP_TIMER_LIST_ID,
                               (tTmrAppTimer *) & (pLdpTcpUdpSockInfo->
                                                   SockReadTimer.
                                                   AppTimer),
                               (LDP_TCP_RECONNECT_TIME *
                                SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
#ifdef MPLS_IPV6_WANTED
                if(LDP_ADDR_TYPE_IPV6== pLdpTcpUdpEnqMsgInfo->u1AddrType)
                {
                    SelAddWrFd (pLdpTcpUdpSockInfo->i4SockDesc, LdpTcp6PktOutSocket);
                }
		  else
		  {
#endif
                    SelAddWrFd (pLdpTcpUdpSockInfo->i4SockDesc, LdpTcpPktOutSocket);
#ifdef MPLS_IPV6_WANTED
		  }
#endif
                
                break;
            case CONNECT_FAIL:
                if (LdpTcpEventHandler (LDP_TCP_CONN_DOWN_EVENT,
                                        CONTROL_DATA,
                                        pLdpTcpUdpSockInfo->u4ConnId,
                                        NULL) == LDP_FAILURE)
                {
                    LDP_DBG (LDP_IF_MISC,
                             "LDPTCP: Failed to Indicate TCP Conn up to LDP\n");
                }
                /*
                 * NOTE: If LDP_TCP_CONN_DOWN_EVENT indication fails, Socket is 
                 * closed.  LDP might infinitely wait for socket conn. up event.
                 * However this condition will not occur.
                 */
                LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
                break;
            default:
                /* This never occurs, ignore it */
                break;
        }
    }
    if ((pLdpTcpUdpSockInfo->u1SockStatus == SOCK_SERVER) ||
        (pLdpTcpUdpSockInfo->u1SockStatus == SOCK_ESTABLISHED))
    {
#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6== pLdpTcpUdpEnqMsgInfo->u1AddrType)
        {
            if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc,
                      LdpTcp6PktInSocket) == OSIX_FAILURE)
            {
                LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Failed to add V6sock to select FD List\n");
                LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
            }
        }
	 else
	 {
#endif
            if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc,
                      LdpTcpPktInSocket) == OSIX_FAILURE)
            {
                LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Failed to add sock to select FD List\n");
                LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
            }
#ifdef MPLS_IPV6_WANTED
	 }
#endif
    }
}

/*****************************************************************************/
/* Function Name : LdpTcpProcCloseReqEvent                                   */
/* Description   : This routine is a used to close the connection.           */
/* Input(s)      : aTaskEnqMsgInfo  - Array of UINT4 .                       */
/*                 First byte indicates event.                               */
/*                 Second byte has ptr to Socket info structure.             */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTcpProcCloseReqEvent (tLdpTcpUdpEnqMsgInfo * pLdpTcpUdpEnqMsgInfo)
{
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;

    pLdpTcpUdpSockInfo =
        LdpTcpUdpGetSocket ((UINT4)pLdpTcpUdpEnqMsgInfo->i4SockFdOrConnId,
                            LDP_SOCK_MASK);
    if (pLdpTcpUdpSockInfo == NULL)
    {
        LDP_DBG (LDP_IF_MISC, "LDPTCP : Conn Close Failure.\n");
        return;
    }

    if (pLdpTcpUdpSockInfo->u1SockStatus == SOCK_FREE)
    {
        return;
    }

    LdpTcpUdpSockClose (pLdpTcpUdpSockInfo);

    return;
}

/*****************************************************************************/
/* Function Name : LdpTcpProcConnIndEvent                                    */
/* Description   : This routine is a used to indicate the new connection.    */
/* Input(s)      : i4SockDesc  - Socket Descriptor                           */
/*                 u4DestAddr  - Destination Address in Network order        */
/*                 u2Port      - Destination TCP Port                        */
/*                 u1SockType - Inidcates UDP or TCP related socket.         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTcpProcConnIndEvent (INT4 i4SockDesc, UINT4 u4DestAddr,
                        UINT2 u2Port, UINT1 u1SockType)
{
    UINT4               u4TempDestAddr=0;
    UINT4               u4ConnId = LDP_ZERO;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    tLdpSession        *pSessionEntry = NULL;
    tLdpPeer           *pPeer = NULL;
    UINT2               u2IncarnId = MPLS_DEF_INCARN;
    UINT1               u1PeerIndex = LDP_ZERO;
    UINT4               u4TmpSrcAddr = 0;
    UINT4               u4IfIndex = 0;
    tLdpAdjacency       *pTempAdjEntry = NULL;

    /* 
     * It is assumed that when the SLI_ACCEPT is successful, atleast one
     * free socket information structure is available.
     */
    u4ConnId = LdpTcpUdpGetFreeSocket (&pLdpTcpUdpSockInfo);
    if (u4ConnId == 0)
    {
        return;
    }

    /* Connection information copied */
    pLdpTcpUdpSockInfo->i4SockDesc = i4SockDesc;
    pLdpTcpUdpSockInfo->u1AddrType = LDP_ADDR_TYPE_IPV4;
    u4TempDestAddr = OSIX_NTOHL (u4DestAddr);
    MEMCPY(pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, &u4DestAddr, LDP_IPV4ADR_LEN);	
    pLdpTcpUdpSockInfo->u2Port = u2Port;
    pLdpTcpUdpSockInfo->u4ConnId = u4ConnId;
    pLdpTcpUdpSockInfo->u1SockType = u1SockType;
    pLdpTcpUdpSockInfo->u1SockStatus = SOCK_ESTABLISHED;
    pLdpTcpUdpSockInfo->b1IsSockReadFdReqd = TRUE;
    pLdpTcpUdpSockInfo->u1AddrType = LDP_ADDR_TYPE_IPV4;

    /* Interface index has to be taken depending on the source address
	   to be used for a particular destination. */

    if(NETIPV4_FAILURE == NetIpv4GetSrcAddrToUseForDest (u4TempDestAddr, &u4TmpSrcAddr))
    {
       LDP_DBG2 (LDP_IF_MEM, "Failed in NetIpv4GetSrcAddrToUseForDest u4TempDestAddr: %u u4TmpSrcAddr:%u\n",u4TempDestAddr, u4TmpSrcAddr);
    }

    CfaIpIfGetIfIndexFromIpAddress(u4TmpSrcAddr, &u4IfIndex);
    
    pLdpTcpUdpSockInfo->u4IfIndex = u4IfIndex;
	
    for (u1PeerIndex = 0; u1PeerIndex < LDP_MAX_PASSIVE_PEERS; u1PeerIndex++)
    {
        pPeer = LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex);
        if ((pPeer == NULL) || (pPeer->pLdpSession == NULL))
        {
            continue;
        }
        pSessionEntry = (tLdpSession *) pPeer->pLdpSession;
 
        /* This is the case the session already has a valid connection Id.
         * In that case, we must delete the session entry from the TCP connection
         * hash table */
        if (pSessionEntry->u4TcpConnId != LDP_INVALID_CONN_ID)
        {
            TMO_SLL_Scan (&pSessionEntry->AdjacencyList, pTempAdjEntry, tLdpAdjacency *)
            {
                LdpHandleAdjExpiry (pTempAdjEntry, LDP_STAT_TYPE_HOLD_TMR_EXPRD);
            }
        }

	 /* MPLS_IPv6 mod start*/	
        if (LDP_ZERO == (MEMCMP(pPeer->TransAddr.Addr.au1Ipv4Addr, pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, LDP_IPV4ADR_LEN)))
        {
	     /* MPLS_IPv6 mod end*/		
            /* set session state to intitialised */
	     LDP_DBG4 (LDP_SSN_MISC, "Session State Marked Initialized For PeerAddr%x:%x:%x:%x\n", 
	     pPeer->TransAddr.Addr.au1Ipv4Addr[0],
	     pPeer->TransAddr.Addr.au1Ipv4Addr[1],
	     pPeer->TransAddr.Addr.au1Ipv4Addr[2],
	     pPeer->TransAddr.Addr.au1Ipv4Addr[3]);
            pSessionEntry->u1SessionState = LDP_SSM_ST_INITIALIZED;
        }
    }

    if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc, LdpTcpPktInSocket)
        == OSIX_FAILURE)
    {
        LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
        LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
        return;
    }

    /* Number of connections opened is incremented */
    gu4CurSocketConns++;
}

/*****************************************************************************/
/* Function Name : SockOpen                                                  */
/* Description   : This routine is a procedural interface from LDP module    */
/* Input(s)      : u4DestAddr   - Destination Address                        */
/*               : u2Port       - Port                                       */
/*               : u1ConnType   - Connection Type for UDP or                 */
/*                                TCP Active/Passive open                    */
/*               : pu1MD5Passwd - Md5 password to be used for this socket    */
/* Output(s)     : pu4ConnId - Connection Id                                 */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

/* MPLS_IPv6 mod start*/	
UINT1
SockOpen (tGenAddr DestAddr, UINT2 u2Port, UINT1 u1ConnType,
          UINT4 *pu4ConnId, UINT1 *pu1MD5Passwd, tGenAddr SrcAddr, UINT4 u4SsnIfIndex)
{
    tLdpTcpUdpEnqMsgInfo LdpTcpUdpEnqMsgInfo;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT4        u4TmpSrcAddr = 0;
	UINT4        u4TempDestAddr;
    UINT4        u4IfIndex = 0;
    UINT4        u4DestAddr = 0;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

#ifdef MPLS_IPV6_WANTED
    INT4                i4Retval = NETIPV4_SUCCESS;
#endif

 #ifdef MPLS_IPV6_WANTED
   tIp6Addr    Ipv6SrcAddr;
   UINT1       u1Scope = 0;
   MEMSET(&Ipv6SrcAddr,LDP_ZERO,sizeof(tIp6Addr));

#endif

   MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
   MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

   MEMSET (&LdpTcpUdpEnqMsgInfo, LDP_ZERO, sizeof (tLdpTcpUdpEnqMsgInfo));

    if (gu4CurSocketConns == MAX_LDP_TCP_UDP_SSN_SUPRTD)
    {
        LDP_DBG (LDP_IF_MEM, "LDPTCP : Max Tcp Conns already opened.\n");
        return LDP_FAILURE;
    }

    /* Since connection is available the pointer assigned will be non NULL */
    *pu4ConnId = LdpTcpUdpGetFreeSocket (&pLdpTcpUdpSockInfo);
    if (*pu4ConnId == 0)
    {
        LDP_DBG (LDP_IF_MEM, "LDPTCP : Free Socket Not Available.\n");
        return LDP_FAILURE;
    }
    
  
    /* Connection information copied */
	
    /* MPLS_IPv6 mod start*/	
    pLdpTcpUdpSockInfo->u1AddrType = (UINT1)DestAddr.u2AddrType;

#ifdef MPLS_IPV6_WANTED	
    if(LDP_ADDR_TYPE_IPV6 == DestAddr.u2AddrType)
    {
        MEMCPY(LDP_IPV6_ADDR(pLdpTcpUdpSockInfo->DestAddr), 
			LDP_IPV6_ADDR(DestAddr.Addr), LDP_IPV6ADR_LEN);
        MEMCPY(LDP_IPV6_ADDR(pLdpTcpUdpSockInfo->SrcAddr), 
			LDP_IPV6_ADDR(SrcAddr.Addr), LDP_IPV6ADR_LEN);
    }
    else
    {
#endif	
        MEMCPY(LDP_IPV4_ADDR(pLdpTcpUdpSockInfo->DestAddr), 
                      LDP_IPV4_ADDR(DestAddr.Addr), LDP_IPV4ADR_LEN);
        MEMCPY(LDP_IPV4_ADDR(pLdpTcpUdpSockInfo->SrcAddr), 
                      LDP_IPV4_ADDR(SrcAddr.Addr), LDP_IPV4ADR_LEN);
#ifdef MPLS_IPV6_WANTED	
    }
#endif

    /* MPLS_IPv6 mod end*/	
	   
	/* Interface index has to be taken depending on the source address 
       to be used for a particular destination.*/ 

#ifdef MPLS_IPV6_WANTED
    if(LDP_ADDR_TYPE_IPV6 == DestAddr.u2AddrType)
    {
	    u1Scope=Ip6GetAddrScope(&pLdpTcpUdpSockInfo->DestAddr.Ip6Addr);
		
		if (ADDR6_SCOPE_LLOCAL != u1Scope)
		{
           NetIpv6GetSrcAddrForDestAddr(LDP_ZERO, &pLdpTcpUdpSockInfo->DestAddr.Ip6Addr,
                                        &Ipv6SrcAddr);
           i4Retval = NetIpv6IsOurAddress(&Ipv6SrcAddr, &u4IfIndex);
           if(NETIPV6_FAILURE == i4Retval)
           {
                LDP_DBG (LDP_IF_MISC, "NETIPV6_FAILURE retuned by function NetIpv6IsOurAddress \n");
           }
           pLdpTcpUdpSockInfo->u4IfIndex = u4IfIndex;
#if 0
           printf("%s : %d Srcip=%s IfIndex=%d\n",__FUNCTION__,__LINE__,
                  Ip6PrintAddr(&Ipv6SrcAddr),u4IfIndex);
#endif
        }
        else
        {
           pLdpTcpUdpSockInfo->u4IfIndex = u4SsnIfIndex; 
#if 0
		   printf("%s : %d Srcip=%s u4SsnIfIndex IfIndex=%d\n",__FUNCTION__,__LINE__,
                  Ip6PrintAddr(&Ipv6SrcAddr),u4IfIndex);
#endif
	    }
    }
    else
    {
#endif
	    MEMCPY(&u4DestAddr, &pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, LDP_IPV4ADR_LEN);
	    u4TempDestAddr = OSIX_NTOHL (u4DestAddr);
		if(NETIPV4_FAILURE == NetIpv4GetSrcAddrToUseForDest (u4TempDestAddr, &u4TmpSrcAddr))
        {
        LDP_DBG2 (LDP_IF_MISC, "FAILURE in NetIpv4GetSrcAddrToUseForDest u4TempDestAddr :%u u4TmpSrcAddr:%u\n",u4TempDestAddr, u4TmpSrcAddr);
        }
	    CfaIpIfGetIfIndexFromIpAddress(u4TmpSrcAddr, &u4IfIndex);
		if (u4IfIndex == LDP_ZERO)
		{
            pLdpTcpUdpSockInfo->u4IfIndex = u4SsnIfIndex;
		}
		else
		{
            pLdpTcpUdpSockInfo->u4IfIndex = u4IfIndex;
        }


#ifdef MPLS_IPV6_WANTED
    }
#endif

    pLdpTcpUdpSockInfo->u2Port = u2Port;
    pLdpTcpUdpSockInfo->u1SockType = u1ConnType;
    pLdpTcpUdpSockInfo->u4ConnId = *pu4ConnId;
    pLdpTcpUdpSockInfo->u1SockStatus = SOCK_UNCONNECTED;
 
    /* Number connections opened incremented */
    gu4CurSocketConns++;

    /* For 64 Bit Change we will define structure that
     * will take Event Type, Connection id, and msginfo will be password */

     /* MPLS_IPv6 mod start*/
    LdpTcpUdpEnqMsgInfo.u1AddrType = (UINT1)DestAddr.u2AddrType ;
    LdpTcpUdpEnqMsgInfo.u4SsnTcpOpenIndex = u4SsnIfIndex;
     /* MPLS_IPv6 mod end*/
    LdpTcpUdpEnqMsgInfo.u4Event = LDP_TCP_OPEN_REQ_EVENT;
    LdpTcpUdpEnqMsgInfo.i4SockFdOrConnId = (INT4) pLdpTcpUdpSockInfo->u4ConnId;
    LdpTcpUdpEnqMsgInfo.pu1MD5Info = pu1MD5Passwd;

    pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tLdpTcpUdpEnqMsgInfo), 0);
    if (pBuf != NULL)
    {
        if ((CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &LdpTcpUdpEnqMsgInfo, 0,
                                        sizeof (tLdpTcpUdpEnqMsgInfo)))
            == CRU_SUCCESS)
        {
            if (OsixQueSend (LDP_TCP_QID, (UINT1 *) (&pBuf),
                             OSIX_DEF_MSG_LEN) == OSIX_SUCCESS)
            {
                if (OsixEvtSend (LDP_TSK_ID, LDP_TCP_MSG_EVT) == OSIX_SUCCESS)
                {
                    LDP_DBG (LDP_IF_MISC,
                             "LDPTCP : Socket connection intiated..\n");
                    return LDP_SUCCESS;
                }
                else
                {
                    LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
                    LDP_DBG (LDP_IF_MEM, "LDPTCP : Socket Open Failure.\n");
                    return LDP_FAILURE;
                }
            }
        }
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
    }

    LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);

    LDP_DBG (LDP_IF_MEM, "LDPTCP : Socket Open Failure.\n");
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : SockConnect                                               */
/* Description   : This routine issues a connect call for the given socket   */
/* Input(s)      : pLdpTcpUdpSockInfo - TCP Socket Info                      */
/* Output(s)     : None                                                      */
/* Return(s)     : CONNECT_SUCCESS, CONNECT_FAIL, CONNECT_INPROGRESS         */
/*****************************************************************************/
UINT1
SockConnect (tLdpTcpUdpSockInfo * pLdpTcpUdpSockInfo)
{
    struct sockaddr_in  LdpTcpServAddr;
    UINT4          u4TempAddr=0;	
    MEMSET ((INT1 *) &LdpTcpServAddr, 0, sizeof (LdpTcpServAddr));

    if (fcntl (pLdpTcpUdpSockInfo->i4SockDesc, F_SETFL, O_NONBLOCK) < 0)
    {
        LDP_DBG (LDP_IF_MISC, "LDPTCP: Client : Can't Set Sckt in NON BLK\n");
        LDP_SOCK_EVENT_INDICATE (TCP_CLI_OPEN_FAILURE, pLdpTcpUdpSockInfo);
        return CONNECT_FAIL;
    }

    /*
     * Fill in the structure 'LdpTcpServAddr' with the address of the
     * server that we want to connect with
     */
    MEMCPY(&u4TempAddr, pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, LDP_IPV4ADR_LEN);	
    LdpTcpServAddr.sin_addr.s_addr = u4TempAddr;
    LdpTcpServAddr.sin_family = AF_INET;
    LdpTcpServAddr.sin_port = OSIX_HTONS (pLdpTcpUdpSockInfo->u2Port);

    /* Connect to the server */
    if (connect
        (pLdpTcpUdpSockInfo->i4SockDesc, (struct sockaddr *) &LdpTcpServAddr,
         sizeof (LdpTcpServAddr)) < 0)
    {
        if (errno == EINPROGRESS || errno == EALREADY)
        {
            LDP_DBG (LDP_IF_MISC, "LDPTCP : Connection in Progress\n");
            return CONNECT_INPROGRESS;
        }
        if (errno == EISCONN)
        {
            LDP_DBG (LDP_IF_MISC, "LDPTCP : Socket already connected \n");
            return CONNECT_SUCCESS;
        }
        LDP_DBG (LDP_IF_MISC, "LDPTCP : Connection Fail \n");
        return CONNECT_FAIL;
    }
    LDP_DBG (LDP_IF_MISC, "LDPTCP : Connection is Success\n");
    return CONNECT_SUCCESS;
}

/*****************************************************************************/
/* Function Name : SockRead                                                  */
/* Description   : This routine issues a Read call for the given socket      */
/* Input(s)      : pLdpTcpUdpSockInfo - Socket Info                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
SockRead (tLdpTcpUdpSockInfo * pLdpTcpUdpSockInfo)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1RcvBuf = NULL;
    UINT1              *pu1Data = NULL;
    UINT1              *pu1Start = NULL;
    UINT2               u2HeaderLen = LDP_MSG_HDR_LEN;
    UINT2               u2DataLen = 0;
    UINT2               u2TotalRead = 0;
    INT4                i4Read = 0;
    UINT4               u4RelinquishCtr = 0;
    UINT4               u4Count = 0;
    /* MPLS_IPv6 mod start*/
    UINT4               u4TempAddr=0;
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            TempV6Addr;
#endif
    UINT1               u1V6SockRead = LDP_FALSE;

#ifdef MPLS_IPV6_WANTED
    if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
    {
        MEMSET(&TempV6Addr, 0, sizeof(tIp6Addr));
        u1V6SockRead = LDP_TRUE;
        MEMCPY(TempV6Addr.u1_addr, LDP_IPV6_ADDR(pLdpTcpUdpSockInfo->DestAddr), LDP_IPV6ADR_LEN);
    }
    else
    {
 #endif
        MEMCPY(&u4TempAddr, pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, LDP_IPV4ADR_LEN);
 #ifdef MPLS_IPV6_WANTED
    }
#endif
    /* MPLS_IPv6 mod end*/	

    pLdpTcpUdpSockInfo->b1IsSockReadFdReqd = TRUE;

    /* Stop the LDP_FRAG_DATA_PRCS_TIME timer */
    TmrStopTimer (LDP_TIMER_LIST_ID,
                  (tTmrAppTimer *) & (pLdpTcpUdpSockInfo->SockReadTimer.
                                      AppTimer));

    pu1RcvBuf = MemAllocMemBlk (LDP_RCV_BUF_POOL_ID);
    if (pu1RcvBuf == NULL)
    {
        LDP_DBG1 ((LDP_IF_RX | LDP_IF_MEM),
                  "LDPTCP: TCP Memory Allocation failed for Receive Buffer "
                  "for Socket %d\n",
                  pLdpTcpUdpSockInfo->i4SockDesc);
        return;
    }

    /* Do Continues Read */
    while (1)
    {
        /* LDP_PDU = LDP_VER(2) + LDP_DATA_LEN(2) + LDP_DATA. 
         * First Read the LDP_VER + LDP_DATA_LEN and based on 
         * LDP_DATA_LEN Read LDP_DATA. If data is not available
         * fully during LDP_DATA_LEN or LDP_DATA the copy the 
         * read data to pLdpTcpUdpSockInfo->pRcvBuf and update
         * pLdpTcpUdpSockInfo->u2ResidualDataLen */

        pu1Data = pu1Start = pu1RcvBuf;
        u2HeaderLen = LDP_MSG_HDR_LEN;
        u2DataLen = 0;

        /* If Residual Data is present then First Copy the residual 
         * data to pu1Data */

        if (pLdpTcpUdpSockInfo->u2ResidualDataLen != 0)
        {
            MEMCPY (pu1Data, pLdpTcpUdpSockInfo->pRcvBuf,
                    pLdpTcpUdpSockInfo->u2ResidualDataLen);
            pu1Data += pLdpTcpUdpSockInfo->u2ResidualDataLen;
            if (pLdpTcpUdpSockInfo->u2ResidualDataLen < LDP_MSG_HDR_LEN)
            {
                u2HeaderLen = (UINT2) (LDP_MSG_HDR_LEN -
                                       pLdpTcpUdpSockInfo->u2ResidualDataLen);
            }
            else
            {
                u2HeaderLen = 0;
                /* If Full Header is present then read the Data Length
                 * from the Header */
                MEMCPY ((UINT1 *) &u2DataLen, (pu1Start + LDP_PRTCL_VER_LEN),
                        sizeof (UINT2));
                u2DataLen = OSIX_NTOHS (u2DataLen);

                if (u2DataLen > (LDP_MAX_FRAGMENT - LDP_MSG_HDR_LEN))
                {
                    /* Data Length is more than our  Buffer size so,
                     * drop the data. This may be peer data send 
                     * issue  */
                    LDP_DBG1 (LDP_IF_RX,
                              "LDPTCP: Error Socket Read: "
                              " LDP PDU is more than Buffer Size %d \n",
                              u2DataLen);
                    LDP_SOCK_EVENT_INDICATE (LDP_TCP_CONN_DOWN_EVENT,
                                             pLdpTcpUdpSockInfo);
                    pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
                    MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID,
                                        (UINT1 *) pu1RcvBuf);
                    pu1RcvBuf = NULL;
                    return;
                }

                /* If pLdpTcpUdpSockInfo->u2ResidualDataLen is greater than
                 * Header length then some part of Data is already read so,
                 * Adjust the Data length */
                if (pLdpTcpUdpSockInfo->u2ResidualDataLen > LDP_MSG_HDR_LEN)
                {
                    u2DataLen -= (UINT2)(pLdpTcpUdpSockInfo->u2ResidualDataLen -
                                  LDP_MSG_HDR_LEN);
                    /* pu1Data is already adjusted  before this IF 
                     * condition */
                }
            }

            if (pLdpTcpUdpSockInfo->bTimerExpired == TRUE)
            {
                u2DataLen = 0;
                u2HeaderLen = 0;
            }
        }

        /* Read Header If it is already read and present in 
         * residual data then this may zero */
        if (u2HeaderLen != 0)
        {
            i4Read =
                recv (pLdpTcpUdpSockInfo->i4SockDesc, pu1Data, u2HeaderLen,
                      MSG_NOSIGNAL);
            if (i4Read == 0)
            {
                LDP_DBG1 (LDP_IF_RX,
                         "LDPTCP: Peer has performed an orderly shutdown Addr Fam (V6:1 V4:0) %x \n", u1V6SockRead);
                LDP_SOCK_EVENT_INDICATE (LDP_TCP_CONN_DOWN_EVENT,
                                         pLdpTcpUdpSockInfo);
                pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
                MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
                pu1RcvBuf = NULL;
                return;
            }
            if ((i4Read < 0) && (errno != EWOULDBLOCK))
            {
                LDP_DBG1 (LDP_IF_RX,
                         "LDPTCP: Error Socket Read "
                         " May be connection closed by Peer  Addr Fam (V6:1 V4:0) %x\n",u1V6SockRead);
                LDP_SOCK_EVENT_INDICATE (LDP_TCP_CONN_DOWN_EVENT,
                                         pLdpTcpUdpSockInfo);
                pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
                MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
                pu1RcvBuf = NULL;
                return;
            }
            if (i4Read < u2HeaderLen)
            {
                pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
                pu1Data = (i4Read < 0) ? pu1Data : (pu1Data + i4Read);
                u2TotalRead = (UINT2) (pu1Data - pu1Start);
                if (u2TotalRead != 0)
                {
                    MEMCPY (pLdpTcpUdpSockInfo->pRcvBuf, pu1Start, u2TotalRead);
                    pLdpTcpUdpSockInfo->u2ResidualDataLen = u2TotalRead;
                }
#ifdef MPLS_IPV6_WANTED
                    if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
                {
                    if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc, 
                          LdpTcp6PktInSocket) == OSIX_FAILURE)
                    {
                        LDP_DBG1 (LDP_IF_RX, 
                                  "SelAddFd failed for socket for Ipv6%d\r\n",
                                  pLdpTcpUdpSockInfo->i4SockDesc);
                    }
                }
			else
			{
#endif
                    if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc,
                                LdpTcpPktInSocket) == OSIX_FAILURE)
                {
                        LDP_DBG1 (LDP_IF_RX,
                                "SelAddFd failed for socket %d\r\n",
                                pLdpTcpUdpSockInfo->i4SockDesc);
                }
#ifdef MPLS_IPV6_WANTED
                }
#endif
                MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
                pu1RcvBuf = NULL;
                return;
            }
            /* If Full Header is present then read the Data Length
             * from the Header */
            MEMCPY ((UINT1 *) &u2DataLen, (pu1Start + LDP_PRTCL_VER_LEN),
                    sizeof (UINT2));
            u2DataLen = OSIX_NTOHS (u2DataLen);

            if (u2DataLen > (LDP_MAX_FRAGMENT - LDP_MSG_HDR_LEN))
            {
                /* Data Length is more than our  Buffer size so,
                 * drop the data. This may be peer data send 
                 * issue  */
                LDP_DBG2 (LDP_IF_RX,
                          "LDPTCP: Error Socket Read: "
                          " LDP PDU is more than Buffer Size %d  Addr Fam (V6:1 V4:0) %x\n", u2DataLen, u1V6SockRead);
                LDP_SOCK_EVENT_INDICATE (LDP_TCP_CONN_DOWN_EVENT,
                                         pLdpTcpUdpSockInfo);
                pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
                MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
                pu1RcvBuf = NULL;
                return;
            }
            pu1Data += u2HeaderLen;
        }

        /* Read Data */
        if (u2DataLen != 0)
        {
            i4Read = recv (pLdpTcpUdpSockInfo->i4SockDesc, pu1Data, u2DataLen,
                           MSG_NOSIGNAL);
            if (i4Read == 0)
            {
                LDP_DBG1 (LDP_IF_RX,
                         "LDPTCP: Peer has performed an orderly shutdown Addr Fam (V6:1 V4:0) %x \n", u1V6SockRead);
                LDP_SOCK_EVENT_INDICATE (LDP_TCP_CONN_DOWN_EVENT,
                                         pLdpTcpUdpSockInfo);
                pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
                MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
                pu1RcvBuf = NULL;
                return;
            }
            if ((i4Read < 0) && (errno != EWOULDBLOCK))
            {
                LDP_DBG1 (LDP_IF_RX,
                         "LDPTCP: Error Socket Read "
                         " May be connection closed by PeerAddr Fam (V6:1 V4:0) %x \n", u1V6SockRead);
                LDP_SOCK_EVENT_INDICATE (LDP_TCP_CONN_DOWN_EVENT,
                                         pLdpTcpUdpSockInfo);
                pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
                MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
                pu1RcvBuf = NULL;
                return;
            }
            if (i4Read < u2DataLen)
            {
                pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
                pu1Data = (i4Read < 0) ? pu1Data : (pu1Data + i4Read);
                u2TotalRead = (UINT2) (pu1Data - pu1Start);
                if (u2TotalRead != 0)
                {
                    MEMCPY (pLdpTcpUdpSockInfo->pRcvBuf, pu1Start, u2TotalRead);
                    pLdpTcpUdpSockInfo->u2ResidualDataLen = u2TotalRead;
                }
#ifdef MPLS_IPV6_WANTED
                    if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
                    {
                    if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc,
                                LdpTcp6PktInSocket) == OSIX_FAILURE)
                    {
                        LDP_DBG1 (LDP_IF_RX,
                                "SelAddFd failed for socket for Ipv6%d\r\n",
                                pLdpTcpUdpSockInfo->i4SockDesc);
                    }
                    }
		      else
                {
#endif
                    if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc,
                                LdpTcpPktInSocket) == OSIX_FAILURE)
                    {
                        LDP_DBG1 (LDP_IF_RX,
                                "SelAddFd failed for socket %d\r\n",
                                pLdpTcpUdpSockInfo->i4SockDesc);
                    }
#ifdef MPLS_IPV6_WANTED
                }
#endif
                MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
                pu1RcvBuf = NULL;
                return;
            }

            pu1Data += u2DataLen;

            if ((i4Read == u2DataLen) &&
                (pLdpTcpUdpSockInfo->u2ResidualDataLen != 0))
            {
                /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
                if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
                {
                    LDP_DBG3 (LDP_IF_RX,
                          "Read residual data %d from socket %d %s\r\n",
                          pLdpTcpUdpSockInfo->i4SockDesc,
                          Ip6PrintAddr(&TempV6Addr),
                          pLdpTcpUdpSockInfo->u2ResidualDataLen);
                }
		  else
		  {
#endif
                    LDP_DBG3 (LDP_IF_RX,
                          "Read residual data %d from socket %d %x\r\n",
                          pLdpTcpUdpSockInfo->i4SockDesc,
                          u4TempAddr,
                          pLdpTcpUdpSockInfo->u2ResidualDataLen);
#ifdef MPLS_IPV6_WANTED
		  }
#endif
		  /* MPLS_IPv6 mod end*/		
            }
        }

        u2TotalRead = (UINT2) (pu1Data - pu1Start);

        if (pLdpTcpUdpSockInfo->u1RowStatus != 0)
        {
            /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
            if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
            {
                LDP_DBG2 (LDP_IF_RX,
                      "This Socket %d %s is created for testing purpose "
                      "No need to allocate CRU Buffer and process the "
                      "packet\r\n",
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      Ip6PrintAddr(&TempV6Addr));
            }
	     else
	     {
#endif
            LDP_DBG2 (LDP_IF_RX,
                      "This Socket %d %x is created for testing purpose "
                      "No need to allocate CRU Buffer and process the "
                      "packet\r\n",
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      u4TempAddr);
#ifdef MPLS_IPV6_WANTED
	     }
#endif
            /* MPLS_IPv6 mod end*/
            /* User created socket. No need to allocate Cru Buffer and
             * process the packet */
            pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
            MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
            pu1RcvBuf = NULL;

            return;
        }

        pBuf = CRU_BUF_Allocate_MsgBufChain (u2TotalRead, 0);
        if (pBuf == NULL)
        {
            LDP_DBG (LDP_IF_RX,
                     "LDPTCP: Unable to Alloc Buffer for read data \n");
            pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
            MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
            pu1RcvBuf = NULL;
            return;
        }

        if (CRU_BUF_Copy_OverBufChain (pBuf, pu1Start, 0, u2TotalRead)
            != CRU_SUCCESS)
        {
            LDP_DBG (LDP_IF_RX,
                     "LDPTCP: Unable to copy read data to Buffer \n");
            pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
            pu1RcvBuf = NULL;
            return;
        }

#ifdef MPLS_IPV6_WANTED
        if(LDP_TRUE == u1V6SockRead)
        {
	     LdpHandleTcp6Pkt (pBuf, pLdpTcpUdpSockInfo->u4ConnId,
                         TempV6Addr, LDP_CUR_INCARN);
        }
	 else
	 {
#endif
            LdpHandleTcpPkt (pBuf, pLdpTcpUdpSockInfo->u4ConnId,
                         u4TempAddr, LDP_CUR_INCARN);
#ifdef MPLS_IPV6_WANTED
	 }
#endif
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        pLdpTcpUdpSockInfo->u2ResidualDataLen = 0;

        if (pLdpTcpUdpSockInfo->bTimerExpired == TRUE)
        {
            pLdpTcpUdpSockInfo->bTimerExpired = FALSE;
            MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
            pu1RcvBuf = NULL;
            break;
        }
        /* The socket has been closed and hence return from here */
        if (pLdpTcpUdpSockInfo->u1SockStatus == SOCK_FREE)
        {
            MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
            pu1RcvBuf = NULL;
            return;
        }

        u4RelinquishCtr++;
        u4Count++;

        if (u4Count >= LDP_MAX_SOCK_READ_COUNT)
        {
            pLdpTcpUdpSockInfo->b1IsSockReadFdReqd = FALSE;
#ifdef MPLS_IPV6_WANTED
            if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
            {
                pLdpTcpUdpSockInfo->SockReadTimer.u4Event =
                    LDP_TCP6_SOCK_READ_TMR_EVT;
            }
	     else
	     {
#endif
pLdpTcpUdpSockInfo->SockReadTimer.u4Event =
                LDP_TCP_SOCK_READ_TMR_EVT;
#ifdef MPLS_IPV6_WANTED
           }
#endif

                    
            pLdpTcpUdpSockInfo->SockReadTimer.pu1EventInfo =
                (UINT1 *) (VOID *) pLdpTcpUdpSockInfo;
            if (TmrStartTimer (LDP_TIMER_LIST_ID,
                               &(pLdpTcpUdpSockInfo->SockReadTimer.AppTimer),
                               (LDP_TCP_SOCK_READ_TIME *
                                SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
            {
                LDP_DBG1 (LDP_IF_TMR,
                          "Interface: Failed to start the timer inside the function %s\n",
                          __func__);
            }
#ifdef MPLS_IPV6_WANTED
            if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
            {
                LDP_DBG2 (LDP_IF_RX,
                      "Enough bytes read from socket %d %s for application "
                      "to process - Socket Read Relinquished\r\n",
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      Ip6PrintAddr(&TempV6Addr));
            }
	     else
	     {
#endif
            LDP_DBG2 (LDP_IF_RX,
                      "Enough bytes read from socket %d %x for application "
                      "to process - Socket Read Relinquished\r\n",
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      u4TempAddr);
#ifdef MPLS_IPV6_WANTED
	     }
#endif
            MemReleaseMemBlock (LDP_RCV_BUF_POOL_ID, (UINT1 *) pu1RcvBuf);
            pu1RcvBuf = NULL;
            return;
        }

        if (u4RelinquishCtr > LDP_RELINQUISH_CNTR)
        {
            LDP_DBG1 (LDP_IF_RX,
                      "LDPTCP: More than %d messages processed\n",
                      LDP_RELINQUISH_CNTR);

            u4RelinquishCtr = 0;

            /*Process the Timer Expiry Events */
            LdpProcessTimerEvents ();

            /*Process the Udp Messages */
#ifdef MPLS_IPV6_WANTED
            if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
            {
                LdpProcessIpv6UdpMsgs ();
            }
	     else
	     {
#endif
                LdpProcessUdpMsgs();
#ifdef MPLS_IPV6_WANTED
	     }
#endif
        }

    }

    return;
}

/*****************************************************************************/
/* Function Name : SockClose                                                 */
/* Description   : Function for closing a Connection.                        */
/* Input(s)      : u4ConnId                                               */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
SockClose (UINT4 u4ConnId)
{
    tLdpTcpUdpEnqMsgInfo LdpTcpUdpEnqMsgInfo;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;

    MEMSET (&LdpTcpUdpEnqMsgInfo, LDP_ZERO, sizeof (tLdpTcpUdpEnqMsgInfo));

    pLdpTcpUdpSockInfo = LdpTcpUdpGetSocket (u4ConnId, LDP_SOCK_MASK);

    if (pLdpTcpUdpSockInfo != NULL)
    {
        LdpTcpUdpEnqMsgInfo.u1AddrType = pLdpTcpUdpSockInfo->u1AddrType;
        LdpTcpUdpEnqMsgInfo.u4Event = LDP_TCP_CLOSE_REQ_EVENT;
        LdpTcpUdpEnqMsgInfo.i4SockFdOrConnId = (INT4) u4ConnId;
        LdpTcpProcCloseReqEvent (&LdpTcpUdpEnqMsgInfo);
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : SockSend                                                  */
/* Description   : Function for sending data.                                */
/* Input(s)      : u4ConnId, pBuf, u2BufLen, u1DataFlag.                     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
SockSend (UINT4 u4ConnId, tCRU_BUF_CHAIN_HEADER * pBuf,
          UINT2 u2BufLen, UINT1 u1DataFlag)
{
    UINT1              *pu1Data = NULL;
    INT4                i4RetVal = 0;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    INT4                i4PendWriteLen = 0;
    /* MPLS_IPv6 add start*/
    UINT4          u4TempAddr=0;
    tIp6Addr      V6TempAddr;
    MEMSET(&V6TempAddr, 0, sizeof(V6TempAddr));
    /* MPLS_IPv6 add end*/	

    pLdpTcpUdpSockInfo = LdpTcpUdpGetSocket (u4ConnId, LDP_SOCK_MASK);

    if (pLdpTcpUdpSockInfo == NULL)
    {
        LDP_DBG1 (LDP_IF_TX,
                  "LDPTCP : Unable to Get SockInfo from"
                  "Connection Id %d\r\n", u4ConnId);

        return LDP_FAILURE;
    }
    /* MPLS_IPv6 add start*/
#ifdef MPLS_IPV6_WANTED
    if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
    {
        MEMCPY(V6TempAddr.u1_addr, LDP_IPV6_ADDR(pLdpTcpUdpSockInfo->DestAddr), LDP_IPV6ADR_LEN);
    }
    else
    {
#endif
        MEMCPY(&u4TempAddr, pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, LDP_IPV4ADR_LEN);	
#ifdef MPLS_IPV6_WANTED
    }
#endif
    /* MPLS_IPv6 add end*/		
    if (pLdpTcpUdpSockInfo->u1SockStatus != SOCK_ESTABLISHED)
    {
        /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
        if (LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
        {
            LDP_DBG3 (LDP_IF_TX,
                  "LDPTCP: Connection Id - Descriptor %d %d %s is not "
                  "in established state\r\n",
                  u4ConnId, pLdpTcpUdpSockInfo->i4SockDesc,
                  Ip6PrintAddr(&V6TempAddr));
        }
	 else
	 {
#endif
        LDP_DBG3 (LDP_IF_TX,
                  "LDPTCP: Connection Id - Descriptor %d %d %x is not "
                  "in established state\r\n",
                  u4ConnId, pLdpTcpUdpSockInfo->i4SockDesc,
                  u4TempAddr);
#ifdef MPLS_IPV6_WANTED
        }
#endif
        /* MPLS_IPv6 mod end*/
        return LDP_FAILURE;
    }

    pu1Data = MemAllocMemBlk (LDP_TCP_SEND_BUF_POOL_ID);

    if (pu1Data == NULL)
    {
        /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
        {
            LDP_DBG2 ((LDP_IF_TX | LDP_IF_MEM),
                  "LDPTCP : Memory Allocation for TX Buffer Failed during "
                  "Socket Send of %d %s\r\n",
                  pLdpTcpUdpSockInfo->i4SockDesc,
                  Ip6PrintAddr(&V6TempAddr));
        }
	else
	{
#endif
        LDP_DBG2 ((LDP_IF_TX | LDP_IF_MEM),
                  "LDPTCP : Memory Allocation for TX Buffer Failed during "
                  "Socket Send of %d %x\r\n",
                  pLdpTcpUdpSockInfo->i4SockDesc,
                  u4TempAddr);
        /* MPLS_IPv6 mod end*/
#ifdef MPLS_IPV6_WANTED
        }
#endif
        return LDP_FAILURE;
    }

    MEMSET (pu1Data, 0, LDP_MIN_MTU);

    if (CRU_BUF_Copy_FromBufChain (pBuf, pu1Data, 0, u2BufLen) != u2BufLen)
    {
        /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
        {
            LDP_DBG2 ((LDP_IF_TX | LDP_IF_MEM),
                      "LDPTCP : TX Buffer copy Failed during send for %d %s\n",
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      Ip6PrintAddr(&V6TempAddr));
        }
        else
        {
#endif
        LDP_DBG2 ((LDP_IF_TX | LDP_IF_MEM),
                  "LDPTCP : TX Buffer copy Failed during send for %d %x\n",
                  pLdpTcpUdpSockInfo->i4SockDesc,
                  u4TempAddr);
#ifdef MPLS_IPV6_WANTED
        }
#endif
        /* MPLS_IPv6 mod end*/
        MemReleaseMemBlock (LDP_TCP_SEND_BUF_POOL_ID, (UINT1 *) pu1Data);
        return LDP_FAILURE;
    }

    if (pLdpTcpUdpSockInfo->b1IsWaitingSockWriteReady == TRUE)
    {
        i4PendWriteLen = pLdpTcpUdpSockInfo->i4PendWriteLen;

        if ((i4PendWriteLen + (INT4) u2BufLen) > LDP_PEND_MAX_FRAGMENT)
        {
            /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
            if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
            {
                LDP_DBG5 ((LDP_IF_TX | LDP_IF_MEM),
                      "LDPTCP : SockSend Write Timer Running Pending Write "
                      "Length %d bytes + Write Length %d bytes > %d for %d "
                      "%s\r\n Connection to be closed\r\n",
                      i4PendWriteLen, u2BufLen, LDP_PEND_MAX_FRAGMENT,
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      Ip6PrintAddr(&V6TempAddr));
            }
            else
            {
#endif
                LDP_DBG5 ((LDP_IF_TX | LDP_IF_MEM),
                      "LDPTCP : SockSend Write Timer Running Pending Write "
                      "Length %d bytes + Write Length %d bytes > %d for %d "
                      "%x\r\n Connection to be closed\r\n",
                      i4PendWriteLen, u2BufLen, LDP_PEND_MAX_FRAGMENT,
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      u4TempAddr);
#ifdef MPLS_IPV6_WANTED
            }
#endif
            /* MPLS_IPv6 mod end*/
            MemReleaseMemBlock (LDP_TCP_SEND_BUF_POOL_ID, (UINT1 *) pu1Data);
            LDP_SOCK_EVENT_INDICATE (LDP_TCP_CONN_DOWN_EVENT,
                                     pLdpTcpUdpSockInfo);
            return LDP_FAILURE;
        }

        MEMCPY (&(pLdpTcpUdpSockInfo->pPendWriteBuf[i4PendWriteLen]),
                pu1Data, u2BufLen);
        pLdpTcpUdpSockInfo->i4PendWriteLen += (INT4) u2BufLen;
        /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
        {
            LDP_DBG4 (LDP_IF_TX,
                  "LDPTCP: SockSend Write Timer Running Buffer %d bytes "
                  "added to Pending Write Buffer %d bytes for socket %d %s\r\n",
                  u2BufLen, pLdpTcpUdpSockInfo->i4PendWriteLen,
                  pLdpTcpUdpSockInfo->i4SockDesc,
                  Ip6PrintAddr(&V6TempAddr));
        }
        else
        {
#endif
            LDP_DBG4 (LDP_IF_TX,
                  "LDPTCP: SockSend Write Timer Running Buffer %d bytes "
                  "added to Pending Write Buffer %d bytes for socket %d %x\r\n",
                  u2BufLen, pLdpTcpUdpSockInfo->i4PendWriteLen,
                  pLdpTcpUdpSockInfo->i4SockDesc,
                  u4TempAddr);
#ifdef MPLS_IPV6_WANTED
        }
#endif
        /* MPLS_IPv6 mod end*/
        MemReleaseMemBlock (LDP_TCP_SEND_BUF_POOL_ID, (UINT1 *) pu1Data);
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

        return LDP_SUCCESS;
    }

    i4RetVal = send (pLdpTcpUdpSockInfo->i4SockDesc, (UINT1 *) pu1Data,
                     u2BufLen, (u1DataFlag | MSG_NOSIGNAL));

    if ((i4RetVal != (INT4) u2BufLen) &&
        ((errno == EAGAIN) || (errno == EWOULDBLOCK)))
    {
        /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
    if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
    {
        LDP_DBG4 (LDP_IF_TX,
                  "LDPTCP : EAGAIN received during sending of %d bytes, "
                  "Actual data sent is %d bytes for %d %s\r\n",
                  u2BufLen, i4RetVal, pLdpTcpUdpSockInfo->i4SockDesc,
                  Ip6PrintAddr(&V6TempAddr));
    }
    else
    {
#endif
        LDP_DBG4 (LDP_IF_TX,
                  "LDPTCP : EAGAIN received during sending of %d bytes, "
                  "Actual data sent is %d bytes for %d %x\r\n",
                  u2BufLen, i4RetVal, pLdpTcpUdpSockInfo->i4SockDesc,
                  u4TempAddr);
#ifdef MPLS_IPV6_WANTED
    }
#endif
        /* MPLS_IPv6 mod end*/
        i4PendWriteLen = pLdpTcpUdpSockInfo->i4PendWriteLen;

        if ((i4PendWriteLen + (INT4) u2BufLen) > LDP_PEND_MAX_FRAGMENT)
        {
            /* MPLS_IPv6 mod start*/
#ifdef MPLS_IPV6_WANTED
            if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
            {
                LDP_DBG5 (LDP_IF_TX,
                      "LDPTCP : SockSend Pending Write Length %d + Write Length"
                      " %d bytes > %d bytes for %d %s\r\n"
                      "Connection to be closed\r\n",
                      i4PendWriteLen, u2BufLen, LDP_PEND_MAX_FRAGMENT,
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      Ip6PrintAddr(&V6TempAddr));
            }
            else
            {
#endif
                LDP_DBG5 (LDP_IF_TX,
                      "LDPTCP : SockSend Pending Write Length %d + Write Length"
                      " %d bytes > %d bytes for %d %x\r\n"
                      "Connection to be closed\r\n",
                      i4PendWriteLen, u2BufLen, LDP_PEND_MAX_FRAGMENT,
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      u4TempAddr);
#ifdef MPLS_IPV6_WANTED
            }
#endif
            /* MPLS_IPv6 mod end*/
            MemReleaseMemBlock (LDP_TCP_SEND_BUF_POOL_ID, (UINT1 *) pu1Data);
            LDP_SOCK_EVENT_INDICATE (LDP_TCP_CONN_DOWN_EVENT,
                                     pLdpTcpUdpSockInfo);
            return LDP_FAILURE;
        }

        MEMCPY (&(pLdpTcpUdpSockInfo->pPendWriteBuf[i4PendWriteLen]),
                &(pu1Data[i4RetVal]), ((INT4) u2BufLen - i4RetVal));
        pLdpTcpUdpSockInfo->i4PendWriteLen += ((INT4) u2BufLen - i4RetVal);

#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
        {
            if (SelAddWrFd (pLdpTcpUdpSockInfo->i4SockDesc, LdpTcp6PktOutSocket) 
                    == OSIX_FAILURE)
            {
                LDP_DBG1 (LDP_IF_RX,
                        "SelAddWrFd failed for socket %d for Ipv6%d\r\n",
                        pLdpTcpUdpSockInfo->i4SockDesc);
        }
	else
	{
                pLdpTcpUdpSockInfo->b1IsWaitingSockWriteReady = TRUE;
            }
	}
        else
            {
#endif
            if (SelAddWrFd (pLdpTcpUdpSockInfo->i4SockDesc, LdpTcpPktOutSocket)
                    == OSIX_FAILURE)
            {
                LDP_DBG1 (LDP_IF_RX,
                        "SelAddWrFd failed for socket %d for Ipv4%d\r\n",
                        pLdpTcpUdpSockInfo->i4SockDesc);
       }
       else
       {
                pLdpTcpUdpSockInfo->b1IsWaitingSockWriteReady = TRUE;
            }
#ifdef MPLS_IPV6_WANTED
       }
#endif
    }
    else if (i4RetVal != (INT4) u2BufLen)
    {
#ifdef MPLS_IPV6_WANTED
        if (LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
        {
        LDP_DBG4 (LDP_IF_TX,
                  "SockSend send failed during sending of %d bytes\r\n Actual "
                  "data to be sent is %d bytes for %d %s\r\n"
                  "Connection to be closed\r\n",
                  u2BufLen, i4RetVal, pLdpTcpUdpSockInfo->i4SockDesc,
                  Ip6PrintAddr(&V6TempAddr));
        }
        else
        {
#endif
            LDP_DBG4 (LDP_IF_TX,
                  "SockSend send failed during sending of %d bytes\r\n Actual "
                  "data to be sent is %d bytes for %d %x\r\n"
                  "Connection to be closed\r\n",
                  u2BufLen, i4RetVal, pLdpTcpUdpSockInfo->i4SockDesc,
                  u4TempAddr);
#ifdef MPLS_IPV6_WANTED
        }
#endif
        MemReleaseMemBlock (LDP_TCP_SEND_BUF_POOL_ID, (UINT1 *) pu1Data);
        LDP_SOCK_EVENT_INDICATE (LDP_TCP_CONN_DOWN_EVENT, pLdpTcpUdpSockInfo);
        return LDP_FAILURE;
    }
    else
    {
#ifdef MPLS_IPV6_WANTED
        if (LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
        {
        LDP_DBG3 (LDP_IF_TX,
                  "Data %d bytes sent for socket %d %s\r\n",
                  u2BufLen, pLdpTcpUdpSockInfo->i4SockDesc,
                  Ip6PrintAddr(&V6TempAddr));
        }
        else
        {
#endif
            LDP_DBG3 (LDP_IF_TX,
                  "Data %d bytes sent for socket %d %x\r\n",
                  u2BufLen, pLdpTcpUdpSockInfo->i4SockDesc,
                  u4TempAddr);
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }

    MemReleaseMemBlock (LDP_TCP_SEND_BUF_POOL_ID, (UINT1 *) pu1Data);
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpTcpUdpGetSocket                                        */
/* Description   : Function for getting the socket associated with a         */
/*                 socket descriptor.                                        */
/* Input(s)      : u4ConnId, i4SockInfo                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : Pointer to the socket structure or NULL                   */
/*****************************************************************************/
tLdpTcpUdpSockInfo *
LdpTcpUdpGetSocket (UINT4 u4ConnId, INT4 i4SockInfo)
{
    UINT4               u4Count;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = gpLdpTcpUdpSockInfoTab;

    for (u4Count = 0; u4Count < MAX_LDP_TCP_UDP_SSN_SUPRTD;
         u4Count++, pLdpTcpUdpSockInfo++)
    {
        if (pLdpTcpUdpSockInfo->u1SockStatus != SOCK_FREE)
        {
            if ((i4SockInfo == LDP_SOCK_MASK) &&
                (pLdpTcpUdpSockInfo->u4ConnId == u4ConnId))
            {
                return pLdpTcpUdpSockInfo;
            }
            else if ((i4SockInfo != LDP_SOCK_MASK) &&
                     (pLdpTcpUdpSockInfo->i4SockDesc == i4SockInfo))
            {
                return pLdpTcpUdpSockInfo;
            }
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name : LdpTcpUdpGetSocketFrmSrcAndDestAdd                                        */
/* Description   : Function for getting the socket associated with a         */
/*                 socket descriptor.                                        */
/* Input(s)      : u4ConnId, i4SockInfo                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : Pointer to the socket structure or NULL                   */
/*****************************************************************************/
tLdpTcpUdpSockInfo *
LdpTcpUdpGetSocketFrmDestAdd (uGenAddr DestAddr,UINT1 u1AddrType)
{
    UINT4               u4Count;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = gpLdpTcpUdpSockInfoTab;

    for (u4Count = 0; u4Count < MAX_LDP_TCP_UDP_SSN_SUPRTD;
         u4Count++, pLdpTcpUdpSockInfo++)
    {

        if (LDP_ADDR_TYPE_IPV4 == u1AddrType)
        {
		    if (MEMCMP (DestAddr.au1Ipv4Addr,pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, IPV4_ADDR_LENGTH)
		                                    == LDP_ZERO)
            {
                return pLdpTcpUdpSockInfo;
            }
        }
		else
		{
            if (MEMCMP (&DestAddr.Ip6Addr,&pLdpTcpUdpSockInfo->DestAddr.Ip6Addr, IPV4_ADDR_LENGTH)
		                                    == LDP_ZERO)
            {
                return pLdpTcpUdpSockInfo;
            }
        }
    }
    return NULL;
}

/*****************************************************************************/
/* Function Name : LdpTcpUdpGetFreeSocket                                    */
/* Description   : Function for getting a free socket structure              */
/* Input(s)      : ppLdpTcpUdpSockInfo.                                      */
/* Output(s)     : ppLdpTcpUdpSockInfo.                                      */
/* Return(s)     : UINT4 - LdpTcpUdpSockConnId                               */
/*****************************************************************************/
UINT4
LdpTcpUdpGetFreeSocket (tLdpTcpUdpSockInfo ** ppLdpTcpUdpSockInfo)
{
    UINT4               u4Count = 0;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = gpLdpTcpUdpSockInfoTab;

    if (pLdpTcpUdpSockInfo != NULL)
    {
        for (u4Count = 0; u4Count < MAX_LDP_TCP_UDP_SSN_SUPRTD;
             u4Count++, pLdpTcpUdpSockInfo++)
        {
            if (pLdpTcpUdpSockInfo->u1SockStatus == SOCK_FREE)
            {
                MEMSET (pLdpTcpUdpSockInfo, 0, sizeof (tLdpTcpUdpSockInfo));
                pLdpTcpUdpSockInfo->pRcvBuf =
                    (INT1 *) MemAllocMemBlk (LDP_SOCK_RECV_BUF_POOL_ID);
                if ((pLdpTcpUdpSockInfo->pRcvBuf) == NULL)
                {
                    LDP_DBG (LDP_IF_MEM,
                             "LDPTCP: Client Connect - "
                             "Memory Allocation Failed for Receive Buffer\r\n");

                    return 0;
                }

                pLdpTcpUdpSockInfo->pPendWriteBuf =
                    (INT1 *) MemAllocMemBlk (LDP_SOCK_SEND_PEND_BUF_POOL_ID);

                if (pLdpTcpUdpSockInfo->pPendWriteBuf == NULL)
                {
                    LDP_DBG (LDP_IF_MEM,
                             "LDPTCP: Client Connect - Memory Allocation Failed"
                             " for Pending Write Buf\r\n");

                    MemReleaseMemBlock (LDP_SOCK_RECV_BUF_POOL_ID,
                                        (UINT1 *) pLdpTcpUdpSockInfo->pRcvBuf);

                    return 0;
                }

                MEMSET (pLdpTcpUdpSockInfo->pRcvBuf, 0, LDP_MAX_FRAGMENT);
                MEMSET (pLdpTcpUdpSockInfo->pPendWriteBuf, 0, LDP_MAX_FRAGMENT);
                pLdpTcpUdpSockInfo->b1IsWaitingSockWriteReady = FALSE;

                pLdpTcpUdpSockInfo->u1SockStatus = SOCK_UNCONNECTED;
                *ppLdpTcpUdpSockInfo = pLdpTcpUdpSockInfo;
                /* Incrementation ensures that TCP ConnId is not zero */

                return u4Count + 1;
            }
        }
    }
    LDP_DBG (LDP_IF_MISC, "LDPTCP: Get free Sckt Fail ..\n");
    return 0;
    /* To avoid warning, this statement will never be reached */
}

/*****************************************************************************/
/* Function Name : LdpTcpProcRecvReqEvent                                    */
/* Description   : Function for handling socket when signal received event is*/
/*                 processed.                                                */
/* Input(s)      : aTaskEnqMsgInfo - Msg Info                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
LdpTcpProcRecvReqEvent (tLdpTcpUdpEnqMsgInfo * pLdpTcpUdpEnqMsgInfo)
{
    INT4                i4NewSockDesc;
    INT4                i4LdpTcpCliLen;
#ifndef BSDCOMP_SLI_WANTED
    UINT1               u1RetVal;
#endif
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    struct sockaddr_in  LdpTcpCliAddr;
    UINT4               u4SndBuf = LDP_MAX_FRAGMENT;
    UINT4               u4RcvBuf = LDP_MAX_FRAGMENT;

    UINT4           u4TempDestAddr;
    LDP_DBG (LDP_SSN_PRCS, "LDPTCPPrcoRecvReqEvent  Entered \n");
    pLdpTcpUdpSockInfo = LdpTcpUdpGetSocket (LDP_SOCK_MASK,
                                             pLdpTcpUdpEnqMsgInfo->
                                             i4SockFdOrConnId);
    MEMSET (&LdpTcpCliAddr, 0, sizeof (struct sockaddr_in));
    if (pLdpTcpUdpSockInfo == NULL)
    {
        LDP_DBG (LDP_IF_MISC, "LDPTCP : Unable to Get Sock Info \n");
        return;
    }
        MEMCPY(&u4TempDestAddr, pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, LDP_IPV4ADR_LEN);
    /* Check, if any Msg is there to be read on this socket */
    if (pLdpTcpUdpEnqMsgInfo->u4MsgType == LDP_READ_FD)
    {

        switch (pLdpTcpUdpSockInfo->u1SockStatus)
        {
            case SOCK_SERVER:
		    LDP_DBG (LDP_SSN_PRCS, "LDPTCPPrcoRecvReqEvent : Read_Fd SockServer Accept\n");
                i4LdpTcpCliLen = sizeof (LdpTcpCliAddr);
                i4NewSockDesc
                    = accept (pLdpTcpUdpEnqMsgInfo->i4SockFdOrConnId,
                              (struct sockaddr *)
                              &LdpTcpCliAddr, (INT4 *) &i4LdpTcpCliLen);
                if (i4NewSockDesc < 0)
                {
                    LDP_DBG (LDP_IF_MISC,
                             "LDPTCP: Some error during accept,"
                             " ignore it\n");
                    break;
                }
                if (fcntl (i4NewSockDesc, F_SETFL, O_NONBLOCK) < 0)
                {
                    close (i4NewSockDesc);
                    LDP_DBG (LDP_IF_MISC,
                             "LDPTCP: Non Blk Set fail for New Sckt\n");
                    break;
                }

                if (setsockopt (i4NewSockDesc, SOL_SOCKET, SO_SNDBUF,
                                (VOID *) &u4SndBuf, sizeof (UINT4)) < 0)
                {
                    LDP_DBG2 (LDP_IF_MEM,
                              "LDPTCP: Server Accept - "
                              "SetSockOpt failed for Send Buffer %d for %d\r\n",
                              u4SndBuf, i4NewSockDesc);
                }

                if (setsockopt (i4NewSockDesc, SOL_SOCKET, SO_RCVBUF,
                                (VOID *) &u4RcvBuf, sizeof (UINT4)) < 0)
                {
                    LDP_DBG2 (LDP_IF_MEM,
                              "LDPTCP: Server Accept - "
                              "SetSockOpt failed for Recv Buffer %d for %d\r\n",
                              u4RcvBuf, i4NewSockDesc);
                }
/*                i4LdpTcpSrvLen = sizeof (LdpTcpSrvAddr);
                if ((getsockname(i4NewSockDesc,
				            (struct sockaddr_in*) &LdpTcpSrvAddr, &i4LdpTcpSrvLen)) < 0)
                {
				     LDP_DBG (LDP_IF_MISC,
					              "LDPTCP: Some error during getsockname,"
					                  " ignore it\n");
                     break;
				}*/
                LdpTcpProcConnIndEvent (i4NewSockDesc,
                                        LdpTcpCliAddr.sin_addr.s_addr,
                                        LdpTcpCliAddr.sin_port, TCP_SERV_OPEN);
                break;

            case SOCK_ESTABLISHED:
                SockRead (pLdpTcpUdpSockInfo);
                break;

            default:
                /* Ignore event */
                break;
        }                        /* switch */
    }                            /* read set socket */

    /* Check, if any Msg is there to be Written to this socket */
    if (pLdpTcpUdpEnqMsgInfo->u4MsgType == LDP_WRITE_FD)
    {
        switch (pLdpTcpUdpSockInfo->u1SockStatus)
        {
            case SOCK_CONNECTING:
#ifndef BSDCOMP_SLI_WANTED
                u1RetVal = SockConnect (pLdpTcpUdpSockInfo);
                switch (u1RetVal)
                {
                    case CONNECT_SUCCESS:
                        if (setsockopt (pLdpTcpUdpSockInfo->i4SockDesc,
                                        SOL_SOCKET, SO_SNDBUF,
                                        (VOID *) &u4SndBuf, sizeof (UINT4)) < 0)
                        {
                            /* MPLS_IPv6 mod start*/
                            LDP_DBG3 (LDP_IF_MEM,
                                      "LDPTCP: Client Connect - "
                                      "SetSockOpt failed for Send Buffer %d "
                                      "for %d %x\r\n",
                                      u4SndBuf, pLdpTcpUdpSockInfo->i4SockDesc,
                                      u4TempDestAddr);
			       /* MPLS_IPv6 mod end*/				
                        }

                        if (setsockopt (pLdpTcpUdpSockInfo->i4SockDesc,
                                        SOL_SOCKET, SO_RCVBUF,
                                        (VOID *) &u4RcvBuf, sizeof (UINT4)) < 0)
                        {
                            LDP_DBG3 (LDP_IF_MEM,
                                      "LDPTCP: Client Accept - "
                                      "SetSockOpt failed for Recv Buffer %d "
                                      "for %d %x\r\n",
                                      u4RcvBuf, pLdpTcpUdpSockInfo->i4SockDesc,
                                      u4TempDestAddr);
                        }

                        if (LdpTcpEventHandler (LDP_TCP_CONN_UP_EVENT,
                                                CONTROL_DATA,
                                                pLdpTcpUdpSockInfo->
                                                u4ConnId, NULL) == LDP_FAILURE)
                        {
                            LDP_DBG (LDP_IF_MISC,
                                     "LDPTCP: Failed to Indicate "
                                     "TCP Conn up to LDP\n");
                            /*
                             * NOTE: If LDP_TCP_CONN_UP_EVENT indication 
                             * fails,Socket is closed.  LDP might infinitely
                             * wait for socket conn. up event.
                             * However this condition will not occur.
                             */
                            LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                            LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
                        }
                        else
                        {
                            if (TmrStopTimer (LDP_TIMER_LIST_ID,
                                              (tTmrAppTimer *) &
                                              (pLdpTcpUdpSockInfo->
                                               SockReadTimer.AppTimer)) ==
                                TMR_FAILURE)
                            {
                                LDP_DBG1 (LDP_IF_TMR,
                                          "Interface: Failed to stop the timer inside the function %s\n",
                                          __func__);
                            }
                            pLdpTcpUdpSockInfo->u1SockStatus = SOCK_ESTABLISHED;
                            pLdpTcpUdpSockInfo->b1IsSockReadFdReqd = TRUE;

                            LdpStoreTcpSourcePort (pLdpTcpUdpSockInfo);
                        }
                        break;
                    case CONNECT_INPROGRESS:
                        /* Timer may have started already. 
                         * Possibilites are there. So stop and Start the
                         * Timer*/
                        if (TmrStopTimer (LDP_TIMER_LIST_ID,
                                          (tTmrAppTimer *) &
                                          (pLdpTcpUdpSockInfo->SockReadTimer.
                                           AppTimer)) == TMR_FAILURE)
                        {
                            LDP_DBG1 (LDP_IF_TMR,
                                      "Interface: Failed to stop the timer inside the function %s\n",
                                      __func__);
                        }
                        pLdpTcpUdpSockInfo->u1SockStatus = SOCK_CONNECTING;
                        pLdpTcpUdpSockInfo->SockReadTimer.u4Event =
                            LDP_TCP_RECONNECT_TMR_EVT;
                        pLdpTcpUdpSockInfo->SockReadTimer.pu1EventInfo =
                            (UINT1 *) pLdpTcpUdpSockInfo;
                        if (TmrStartTimer (LDP_TIMER_LIST_ID,
                                           (tTmrAppTimer *) &
                                           (pLdpTcpUdpSockInfo->SockReadTimer.
                                            AppTimer),
                                           (LDP_TCP_RECONNECT_TIME *
                                            SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) ==
                            TMR_FAILURE)
                        {
                            LDP_DBG1 (LDP_IF_TMR,
                                      "Interface: Failed to stop the timer inside the function %s\n",
                                      __func__);
                        }
                        SelAddWrFd (pLdpTcpUdpSockInfo->i4SockDesc,
                                    LdpTcpPktOutSocket);
                        break;

                    case CONNECT_FAIL:
                        /* Timer may have started already. 
                         * Possibilites are there. So stop and Start the
                         * Timer*/
                        TmrStopTimer (LDP_TIMER_LIST_ID,
                                      (tTmrAppTimer *) & (pLdpTcpUdpSockInfo->
                                                          SockReadTimer.
                                                          AppTimer));
                        if (LdpTcpEventHandler
                            (LDP_TCP_CONN_DOWN_EVENT, CONTROL_DATA,
                             pLdpTcpUdpSockInfo->u4ConnId, NULL) == LDP_FAILURE)
                        {
                            LDP_DBG (LDP_IF_MISC,
                                     "LDPTCP: Failed to Indicate TCP "
                                     "Conn up to LDP\n");
                        }
                        LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                        LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
                        break;
                }
#else
                if (LdpTcpEventHandler (LDP_TCP_CONN_UP_EVENT,
                                        CONTROL_DATA,
                                        pLdpTcpUdpSockInfo->
                                        u4ConnId, NULL) == LDP_FAILURE)
                {
                    LDP_DBG (LDP_IF_MISC,
                             "LDPTCP: Failed to Indicate "
                             "TCP Conn up to LDP\n");
                    /*
                     * NOTE: If LDP_TCP_CONN_UP_EVENT indication 
                     * fails,Socket is closed.  LDP might infinitely
                     * wait for socket conn. up event.
                     * However this condition will not occur.
                     */
                    LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                    LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
                }
                else
                {
                    pLdpTcpUdpSockInfo->u1SockStatus = SOCK_ESTABLISHED;
                    TmrStopTimer (LDP_TIMER_LIST_ID,
                                  (tTmrAppTimer *) & (pLdpTcpUdpSockInfo->
                                                      SockReadTimer.AppTimer));
                    pLdpTcpUdpSockInfo->b1IsSockReadFdReqd = TRUE;
                    LdpStoreTcpSourcePort (pLdpTcpUdpSockInfo);
                }
#endif
                break;
            case SOCK_ESTABLISHED:
                /*
                 * b1IsWaitingSockWriteReady was set when socket was not
                 * ready to accept write and and flag is cleared now
                 * when LDP_WRITE_FD event is received indicating
                 * readiness of socket to accept write requests
                 */
                pLdpTcpUdpSockInfo->b1IsWaitingSockWriteReady = FALSE;
                if (SockSendPendingData (pLdpTcpUdpSockInfo) == LDP_FAILURE)
                {
                    break;
                }

                break;
            default:
                /* Ignore Event */
                break;
        }
    }

    if ((pLdpTcpUdpSockInfo->u1SockStatus == SOCK_SERVER) ||
        ((pLdpTcpUdpSockInfo->u1SockStatus == SOCK_ESTABLISHED) &&
         (pLdpTcpUdpSockInfo->b1IsSockReadFdReqd == TRUE)))
    {
        if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc,
                      LdpTcpPktInSocket) == OSIX_FAILURE)
        {
            LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Failed to add sock to select FD List\n");
            LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
            LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
        }
    }

}

/*****************************************************************************/
/* Function Name : LdpTcpEventHandler                                        */
/* Description   : This routine is called by TCP module when data arrived.   */
/*                 Corresponding event is posted to LDP task along           */
/*                 with necessary parameters.                                */
/*                 Releasing of Data(in case Data is Rx from TCP), is taken  */
/*                 care by this routine for  SUCCESS case alone.             */
/*                 This may not be the case with all kinds of TCP Interfaces */
/*                 in which case, this routine needs modification, to release*/
/*                 Data appropriately.                                       */
/*                                                                           */
/* Input(s)      : u4TcpEvent - Indicates whther data has been received or   */
/*                 a requested coneection has been established.              */
/*                 u4DataType- Normal data or urgent data or error. Current- */
/*                 not used, reserved for future use.                        */
/*                 u4ConnId  - Connection Identifier                         */
/*                 pPdu      - PDU received                                  */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpTcpEventHandler (UINT4 u4TcpEvent, UINT4 u4DataType, UINT4 u4ConnId,
                    tCRU_BUF_CHAIN_HEADER * pPdu)
{

    tLdpIfMsg           LdpIfMsg;
    UINT4               u4Event = 0;

    u4DataType = (UINT4) u4DataType;

    /*  Ignoring the TcpEvent when, the Current Incarn Status is NOT ACTIVE */
    if (LDP_INCARN_STATUS (LDP_CUR_INCARN) != ACTIVE)
    {
        LDP_DBG (LDP_IF_PRCS,
                 "IF: Ignoring the TCP Event. Incarn NOT ACTIVE\n");
        return LDP_FAILURE;
    }

    /* 
     * The First two bytes indicate the sub event and the next two bytes 
     * indicate it as TCP event to the LDP. 
     */
    LDP_SET_EVENT (u4Event, LDP_TCP_EVENT, u4TcpEvent);

    LdpIfMsg.u4MsgType = LDP_TCP_EVENT;
    LdpIfMsg.u.TcpEvt.pPdu = (UINT1 *) pPdu;
    LdpIfMsg.u.TcpEvt.u4IncarnNo = LDP_CUR_INCARN;
    LdpIfMsg.u.TcpEvt.u4ConnId = u4ConnId;
    LdpIfMsg.u.TcpEvt.u4Evt = u4Event;

    /*
     * NOTE: Default Incarnation Id is being passed in the present case. 
     * In case of Multiple Incarnations, Incarnation Id has to be inferred
     * from the ConnectionId.
     */

    if (LdpEnqueueMsgToLdpQ (LDP_MSG_EVT, &LdpIfMsg) == LDP_FAILURE)
    {
        LDP_DBG (LDP_IF_PRCS, "IF: Failed to EnQ TCP Event to LDP Task\n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpTcpOpen                                                */
/* Description   : Function for opening a Tcp port, in ACTIVE or PASSIVE     */
/*                 Mode.                                                     */
/* Input(s)      : DestAddr - Remote Peer's IP Address in case of Active   */
/*                              Open. Zero in case of PassiveOpen.           */
/*                 u2Port - Tcp Port. Local Port, when PassiveOpen and       */
/*                          Remote Port in case of ActiveOpen                */
/*                 u1OpenFlag - Passive Open or Active Open                  */
/*                 pu4TcpConnId - Connetion Identifier                       */
/*                 u2IncarnId - Incarnation Id                               */
/*                 pSessionEntry- Pointer to the Session Entry               */
/* Output(s)     : pu4TcpConnId                                              */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

/* MPLS_IPv6 start*/
UINT1
LdpTcpOpen (tGenAddr DestAddr, UINT2 u2Port, UINT1 u1OpenFlag,
            UINT4 *pu4TcpConnId, UINT2 u2IncarnId, tLdpSession * pSessionEntry)
{
    /* MPLS_IPv6 end */
    /* 
     * The following routine will be called for Open Active Connection
     * Request only. Hence OpenMode Flag is not being passed while calling the
     * routine.
     */
    UINT1              *pu1MD5Passwd = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpIfTableEntry   *pIfEntry = NULL;
    UINT4               u4IfIndex;
    /* MPLS_IPv6 start*/	
    /*UINT4               u4SrcAddr = LDP_ZERO;*/
    UINT4                  u4TcpOpIfIndex = LDP_ZERO;
    tGenAddr              SrcAddr;
    MEMSET(&SrcAddr, 0, sizeof(tGenAddr));
    if(pSessionEntry != NULL)
    {
#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6 == DestAddr.u2AddrType)
        {
	     SrcAddr.u2AddrType = LDP_ADDR_TYPE_IPV6;
	     if (pSessionEntry != NULL)
            {
                MEMCPY(LDP_IPV6_ADDR(SrcAddr.Addr), LDP_IPV6_ADDR(pSessionEntry->SrcTransAddr.Addr), LDP_IPV6ADR_LEN);
            }
        }
	 else
	 {
#endif
            SrcAddr.u2AddrType = LDP_ADDR_TYPE_IPV4;
	     *SrcAddr.Addr.au1Ipv4Addr = LDP_INIT_ADDR;
	     /*MPLS_IPv6 mod start */	
            if (pSessionEntry != NULL)
            {
                MEMCPY(LDP_IPV4_ADDR(SrcAddr.Addr), 
				LDP_IPV4_ADDR(pSessionEntry->SrcTransAddr.Addr), LDP_IPV4ADR_LEN);
	         LDP_DBG4 (LDP_IF_MISC,
                  "IF:  SESS NOT NULL TCP connection opened for Src addr %d.%d.%d.%d\n", SrcAddr.Addr.au1Ipv4Addr[0],
                  SrcAddr.Addr.au1Ipv4Addr[1], SrcAddr.Addr.au1Ipv4Addr[2], 
                  SrcAddr.Addr.au1Ipv4Addr[3]);
            }
#ifdef MPLS_IPV6_WANTED
	 }
#endif
    }
    /* MPLS_IPv6 end*/
    LDP_SUPPRESS_WARNING (u2IncarnId);

    if (gu4LdpMd5Option == TRUE)
    {
        if (pSessionEntry != NULL)
        {
            pLdpEntity = SSN_GET_ENTITY (pSessionEntry);
            if (LDP_ENTITY_IS_TARGET_TYPE (pLdpEntity) == LDP_TRUE)
            {
                pu1MD5Passwd = pLdpEntity->pEntityPeerPasswdInfo->aMd5Password;
            }
            else
            {
                u4IfIndex = LdpSessionGetIfIndex(pSessionEntry,DestAddr.u2AddrType);
                TMO_SLL_Scan (LDP_ENTITY_IF_LIST (pLdpEntity), pIfEntry,
                              tLdpIfTableEntry *)
                {
                    if (pIfEntry->u4IfIndex == u4IfIndex)
                    {
                        if ((pIfEntry->pEntityPeerPasswdInfo) != NULL)
                        {
                            pu1MD5Passwd =
                                pIfEntry->pEntityPeerPasswdInfo->aMd5Password;
                        }
                    }
                }
            }
        }
    }

    if(NULL != pSessionEntry)
    {
        u4TcpOpIfIndex = pSessionEntry->u4SsnTcpIfIndex;

    }

    if (SockOpen (DestAddr, u2Port, u1OpenFlag, pu4TcpConnId,
                  pu1MD5Passwd, SrcAddr, u4TcpOpIfIndex) == LDP_SUCCESS)
    {
    /* MPLS_IPv6 mod end*/	
#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6 == DestAddr.u2AddrType)
        {
            LDP_DBG1(LDP_IF_MISC, "SockOpen:V6 Success for Dest Addr %s\n",
			Ip6PrintAddr(&(DestAddr.Addr.Ip6Addr)));
	     LDP_DBG1(LDP_IF_MISC, "SockOpen:V6 Success for Source Addr %s\n",
			Ip6PrintAddr(&(SrcAddr.Addr.Ip6Addr)));
        }
        else
        {
#endif
            LDP_DBG4 (LDP_IF_MISC,
                  "IF: SockOpen: SockOpen:V4 Success for Dest Addr %d.%d.%d.%d\n", DestAddr.Addr.au1Ipv4Addr[0],
                  DestAddr.Addr.au1Ipv4Addr[1], DestAddr.Addr.au1Ipv4Addr[2], 
                  DestAddr.Addr.au1Ipv4Addr[3]);
	     LDP_DBG4 (LDP_IF_MISC,
                  "IF: SockOpen: SockOpen:V4 Success for Src Addr %d.%d.%d.%d\n", SrcAddr.Addr.au1Ipv4Addr[0],
                  SrcAddr.Addr.au1Ipv4Addr[1], SrcAddr.Addr.au1Ipv4Addr[2], 
                  SrcAddr.Addr.au1Ipv4Addr[3]);
#ifdef MPLS_IPV6_WANTED
        }
#endif
        return LDP_SUCCESS;
    }
    /* MPLS_IPv6 mod end*/	
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpTcpClose                                               */
/* Description   : Function for closing a Tcp Connection.                    */
/*                                                                           */
/* Input(s)      : u4TcpConnId                                               */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpTcpClose (UINT4 u4TcpConnId)
{
    LDP_DBG1 (LDP_IF_MISC,
              "IF: TCP Connection of ConnId  %d closed \n", u4TcpConnId);
    return (SockClose (u4TcpConnId));
}

/*****************************************************************************/
/* Function Name : LdpTcpSend                                                */
/* Description   : Function for sending buffer to Tcp.                       */
/*                                                                           */
/* Input(s)      : u4TcpConnId, pBuf, u2BufLen                               */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/

UINT1
LdpTcpSend (UINT4 u4TcpConnId, tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2BufLen)
{
    UINT1               u1DataFlag;
#ifdef BSDCOMP_SLI_WANTED
    /* Here we are not using data flags in send so, u1DataFlag is zero */
    u1DataFlag = 0;
#else
    u1DataFlag = MSG_PUSH;
#endif

    return (SockSend (u4TcpConnId, pBuf, u2BufLen, u1DataFlag));
}

/*****************************************************************************/
/* Function Name : LdpSliSetMd5SockOpt                                       */
/* Description   : Function for sending buffer to Tcp.                       */
/* Input(s)      : u4TcpConnId, Md5passwd, i4MD5KeyCount                     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
UINT1
LdpSliSetMd5SockOpt (UINT4 u4TcpStdServConnId,
                     UINT1 Md5Passwd[], INT4 i4MD5KeyCount)
{
    if (setsockopt (u4TcpStdServConnId, SOL_SOCKET,
                    LDP_TCP_MD5_OPTION, (INT1 *) Md5Passwd, i4MD5KeyCount) < 0)
    {
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/****************************************************************************/
/* Function Name : LdpUdpOpen                                               */
/* Description   : Function for opening the Udp port                        */
/* Input(s)      : u2Port        - UDP port                                 */
/* Output(s)     : None                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                  */
/****************************************************************************/
UINT1
LdpUdpOpen (UINT2 u2Port)
{
    /* 
     * Opening the Standard Udp Port with Local Address as IP_ANY_ADDRESS which
     * is 0x00000000.
     * All packets received for this Port will be handed over to LDP.
     */
#ifdef BSDCOMP_SLI_WANTED
    INT4                i4MCloopVal = LDP_FALSE;    /*Reset multicast looping */
#endif
    INT4                i4UdpSockFd;
    struct sockaddr_in  LdpUdpLocalAddr;
    UINT1               u1OpnVal = 1;

    if ((i4UdpSockFd = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        LDP_DBG (LDP_IF_UDP, "IF: Can't open UDP socket\n");
        return LDP_FAILURE;
    }
    LDP_UDP_SOCKFD (LDP_CUR_INCARN) = i4UdpSockFd;
    MEMSET ((INT1 *) &LdpUdpLocalAddr, 0, sizeof (struct sockaddr_in));

    LdpUdpLocalAddr.sin_family = AF_INET;
    LdpUdpLocalAddr.sin_addr.s_addr = IN_ADDR_ANY;
    LdpUdpLocalAddr.sin_port = OSIX_HTONS (u2Port);

    /* Bind to Well-Known port */
    if (bind (i4UdpSockFd, (struct sockaddr *) &LdpUdpLocalAddr,
              sizeof (struct sockaddr)) < 0)
    {
        LDP_DBG (LDP_IF_UDP, "IF: Udp bind failed \n");
        close (i4UdpSockFd);
        return LDP_FAILURE;
    }
    if ((fcntl (i4UdpSockFd, F_SETFL, O_NONBLOCK)) < 0)
    {
        LDP_DBG (LDP_IF_MISC, "LDP UDP: Can't Set Sckt in NON BLK\n");
        close (i4UdpSockFd);
        return LDP_FAILURE;
    }
    if (setsockopt (i4UdpSockFd, IPPROTO_IP, IP_PKTINFO, &u1OpnVal,
                    sizeof (UINT1)) < 0)
    {
        LDP_DBG (LDP_IF_MISC, "LDP UDP: Can't Set SetSockOpt for IP_INFO\n");
        close (i4UdpSockFd);
        return LDP_FAILURE;
    }
#ifdef BSDCOMP_SLI_WANTED
    /*Set the option not to receive packet on the interface on which 
     * it is sent*/
    if (setsockopt (i4UdpSockFd, IPPROTO_IP, IP_MULTICAST_LOOP,
                    &i4MCloopVal, sizeof (INT4)) < 0)
    {
        LDP_DBG (LDP_IF_MISC, "LDP UDP: Can't Set SetSockOpt for Mcast Loop\n");
        close (i4UdpSockFd);
        return LDP_FAILURE;
    }
#endif
    if (SelAddFd (i4UdpSockFd, LdpUdpPktInSocket) != OSIX_SUCCESS)
    {

        LDP_DBG (LDP_IF_MISC, "LDP UDP : Can't do SelAddFd\n");
        close (i4UdpSockFd);
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpUdpSend                                                */
/* Description   : Function for sending LDP packet to UDP.                   */
/* Input(s)      : u2SrcPort, u4DestAddr, u2DestPort, pBuf, u2BufLen,        */
/*                 u4IfAddr.                                                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
UINT1
LdpUdpSend (UINT2 u2SrcPort, UINT4 u4DestAddr, UINT2 u2DestPort,
            tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2BufLen, UINT4 u4IfAddr)
{
    UINT1              *pu1Data = NULL;
    UNUSED_PARAM (u2SrcPort);

    pu1Data = MemAllocMemBlk (LDP_DATA_BUF_POOL_ID);

    if (pu1Data != NULL)
    {
        /*
         * A linear buffer is expected to be passed while writing to 
         *  socket. So copying the contents of pBuf to a linear buffer.
         */

        if ((CRU_BUF_Copy_FromBufChain (pBuf, pu1Data, 0, u2BufLen) ==
             u2BufLen))
        {
            if (LdpSendMessage (pu1Data, u2BufLen, u4DestAddr, u2DestPort,
                                u4IfAddr) == LDP_FAILURE)
            {
                MemReleaseMemBlock (LDP_DATA_BUF_POOL_ID, (UINT1 *) pu1Data);
                return LDP_FAILURE;
            }
            if (LdpSendTo (pu1Data, u2BufLen, u4DestAddr, u2DestPort)
                == LDP_FAILURE)
            {
                MemReleaseMemBlock (LDP_DATA_BUF_POOL_ID, (UINT1 *) pu1Data);
                return LDP_FAILURE;
            }
            MemReleaseMemBlock (LDP_DATA_BUF_POOL_ID, (UINT1 *) pu1Data);
            CRU_BUF_Release_MsgBufChain (pBuf, 0);
            LDP_DBG (LDP_IF_MISC, "IF: Sent Data on UDP socket.\n");
            return LDP_SUCCESS;
        }
        LDP_DBG (LDP_IF_UDP, "IF: Failed to send UDP packet \n");
        MemReleaseMemBlock (LDP_DATA_BUF_POOL_ID, (UINT1 *) pu1Data);
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_IF_MEM,
             "LDPUDP: Memory Allocation for UDP Sending Data Buffer\r\n");

    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpSendMessage                                            */
/* Description   : Function to set IP_INFO for UDP Send                      */
/* Input(s)      : pu1Data - Buffer to Send                                  */
/* Input(s)      : u2BufLen - Buffer Size                                    */
/*                 u4DestAddr - DestinationIP                                */
/*                 u2DestPort - Destination Port                             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
PRIVATE INT4
LdpSendMessage (UINT1 *pu1Data, UINT2 u2BufLen,
                UINT4 u4DestAddr, UINT2 u2DestPort, UINT4 u4IfAddr)
{
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo;
    struct cmsghdr     *pCmsgInfo;
    UINT1               au1Cmsg[24];
    struct in_addr      IfAddr;
    struct sockaddr_in  DestInfo;
    INT4                i4UdpSockFd = LDP_UDP_SOCKFD (LDP_CUR_INCARN);
    INT4                i4TtlVal = LDP_ZERO;

    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
    Iov.iov_base = pu1Data;
    Iov.iov_len = u2BufLen;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#else
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2BufLen);
#endif

    if (u4DestAddr == LDP_ALL_ROUTERS_ADDR)
    {
        IfAddr.s_addr = OSIX_HTONL (u4IfAddr);
        if ((setsockopt (i4UdpSockFd, IPPROTO_IP, IP_MULTICAST_IF,
                         (INT1 *) &IfAddr, sizeof (IfAddr))) < 0)
        {
            LDP_DBG (LDP_IF_UDP,
                     "IF: Fail to specify MCast pkt's OutIfIndex. \n");
            return LDP_FAILURE;
        }

        i4TtlVal = LDP_BASIC_HELLO_TTL;
        if ((setsockopt (i4UdpSockFd, IPPROTO_IP, IP_TTL,
                         &i4TtlVal, sizeof (INT4))) < 0)
        {
            LDP_DBG (LDP_IF_UDP, "IF: Fail to set TTL. \n");
            return LDP_FAILURE;
        }
    }
    else
    {
        i4TtlVal = LDP_TARGET_HELLO_TTL;
        if ((setsockopt (i4UdpSockFd, IPPROTO_IP, IP_TTL,
                         &i4TtlVal, sizeof (INT4))) < 0)
        {
            LDP_DBG (LDP_IF_UDP, "IF: Fail to set TTL. \n");
            return LDP_FAILURE;
        }

#ifdef BSDCOMP_SLI_WANTED
        /* No need to sendmsg for Multicast incase of 
         * Linux Socket but we have to set the interface index
         * in case of FS Socket other wise, packet will go with 
         * source address of Multicast enabled interface address
         */
        return LDP_SUCCESS;
#endif
    }

    MEMSET ((INT1 *) &DestInfo, 0, sizeof (struct sockaddr_in));
    DestInfo.sin_family = AF_INET;
    DestInfo.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    DestInfo.sin_port = OSIX_HTONS (u2DestPort);

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    PktInfo.msg_name = (VOID *) &DestInfo;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);
    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo =
        (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

    pIpPktInfo->ipi_ifindex = LDP_IP_INVALID_INDEX;
    if ((u4IfAddr != LDP_IP_INVALID_INDEX) &&
        (NetIpv4GetIfIndexFromAddr (u4IfAddr,
                                    (UINT4 *) &pIpPktInfo->
                                    ipi_ifindex) == NETIPV4_FAILURE))
    {
        LDP_DBG (LDP_IF_UDP, "IF: Fail to get IP If Index\n");
        return LDP_FAILURE;
    }
    LDP_DBG2 (LDP_IF_MISC,
                     "IF: Interface Index fetched is %d for Address %x \n ",
                     pIpPktInfo->ipi_ifindex,u4IfAddr);	

    pIpPktInfo->ipi_addr.s_addr = u4DestAddr;
    if (sendmsg (i4UdpSockFd, &PktInfo, 0) < 0)
    {
        LDP_DBG (LDP_IF_UDP, "IF: Fail to set MCast pkt's OutIfIndex. \n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpSendTo                                                 */
/* Description   : Function Send Packet on UDP Port                          */
/* Input(s)      : pu1Data - Buffer to Send                                  */
/* Input(s)      : u2BufLen - Buffer Size                                    */
/*                 u4DestAddr - DestinationIP                                */
/*                 u2DestPort - Destination Port                             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
PRIVATE INT4
LdpSendTo (UINT1 *pu1Data, UINT2 u2BufLen, UINT4 u4DestAddr, UINT2 u2DestPort)
{
    struct sockaddr_in  DestInfo;
    INT4                i4UdpSockFd = LDP_UDP_SOCKFD (LDP_CUR_INCARN);
#ifdef BSDCOMP_SLI_WANTED
    if (u4DestAddr == LDP_ALL_ROUTERS_ADDR)
    {
        /* No need to do sendto in case of multicast send in
         * Linux Socket but we have to call sendto in case of
         * FS Socket */
        return LDP_SUCCESS;
    }
#endif

    MEMSET ((INT1 *) &DestInfo, 0, sizeof (struct sockaddr_in));
    DestInfo.sin_family = AF_INET;
    DestInfo.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    DestInfo.sin_port = OSIX_HTONS (u2DestPort);

    if (sendto (i4UdpSockFd, (INT1 *) pu1Data, (UINT4) u2BufLen, 0,
                (struct sockaddr *) &DestInfo, sizeof (struct sockaddr)) < 0)
    {

        LDP_DBG (LDP_IF_MISC, "IF: Failed to send Data" "on UDP socket.\n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpUdpClose                                               */
/* Description   : Function for closing the Udp Port.                        */
/* Input(s)      : u2Port - UDP port                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpUdpClose (UINT2 u2Port)
{
    INT4                i4UdpSockFd = LDP_UDP_SOCKFD (LDP_CUR_INCARN);
    UNUSED_PARAM (u2Port);
    SelRemoveFd (i4UdpSockFd);
    if (close (i4UdpSockFd) == 0)
    {
        return LDP_SUCCESS;
    }

    LDP_DBG (LDP_IF_UDP, "IF: Failed in closing socket \n");
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpUdpJoinMulticast                                       */
/* Description   : Make the specific interface join a multicast group        */
/* Input(s)      : u4SockDesc, u4LocalIfAddr, u4MulticastAddr                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpUdpJoinMulticast (INT4 i4UdpSockFd, UINT4 u4LocalIfAddr,
                     UINT4 u4MulticastAddr)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ip_mreqn     IpMreq;
    UINT4               u4IfIndex = 0;
    MEMSET (&IpMreq, 0, sizeof (IpMreq));
    IpMreq.imr_multiaddr.s_addr = OSIX_HTONL (u4MulticastAddr);
    if (NetIpv4GetIfIndexFromAddr (u4LocalIfAddr, &u4IfIndex)
        == NETIPV4_FAILURE)
    {
        LDP_DBG1 (LDP_IF_UDP, "IF: %x Fail to get If for Address \n",
                  u4LocalIfAddr);
        return LDP_FAILURE;
    }

    IpMreq.imr_ifindex = (INT4)(u4IfIndex);
    /* 
     * The Interface(specified by u4LocalIfAddr), joins the multicast group
     * specified by u4MulticastAddr.
     * This enables the interface, to receive a pkts with DestAddr of this 
     * multicast address(u4MulticastAddr).
     */
    if ((setsockopt (i4UdpSockFd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                     (VOID *) &IpMreq, sizeof (struct ip_mreqn))) < 0)
    {
        LDP_DBG1 (LDP_IF_UDP, "IF: %#x Fail to join Multicast group \n",
                  u4LocalIfAddr);
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
#else
    UNUSED_PARAM (i4UdpSockFd);
    UNUSED_PARAM (u4LocalIfAddr);
    UNUSED_PARAM (u4MulticastAddr);
    return LDP_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name : LdpUdpLeaveMulticast                                       */
/* Description   : Make the specific interface leave a multicast group        */
/* Input(s)      : u4SockDesc, u4LocalIfAddr, u4MulticastAddr                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpUdpLeaveMulticast (INT4 i4UdpSockFd, UINT4 u4LocalIfAddr,
                      UINT4 u4MulticastAddr)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ip_mreqn     IpMreq;
    UINT4               u4IfIndex = 0;
    MEMSET (&IpMreq, 0, sizeof (IpMreq));
    IpMreq.imr_multiaddr.s_addr = OSIX_HTONL (u4MulticastAddr);
    if (NetIpv4GetIfIndexFromAddr (u4LocalIfAddr, &u4IfIndex)
        == NETIPV4_FAILURE)
    {
        LDP_DBG1 (LDP_IF_UDP, "IF: %x Fail to get If for Address \n",
                  u4LocalIfAddr);
        return LDP_FAILURE;
    }
    IpMreq.imr_ifindex = (INT4)(u4IfIndex);

    if ((setsockopt (i4UdpSockFd, IPPROTO_IP, IP_DROP_MEMBERSHIP,
                     (VOID *) &IpMreq, sizeof (struct ip_mreqn))) < 0)
    {
        LDP_DBG1 (LDP_IF_UDP, "IF: %#x Fail to join Multicast group \n",
                  u4LocalIfAddr);
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
#else
    /* To Suppress warning */
    UNUSED_PARAM (i4UdpSockFd);
    UNUSED_PARAM (u4LocalIfAddr);
    UNUSED_PARAM (u4MulticastAddr);
#endif
    return LDP_SUCCESS;
}

/************************************************************************/
/*  Function Name   : LdpUdpPktInSocket                               */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/

VOID
LdpUdpPktInSocket (INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    if (LDP_INITIALISED != TRUE)
    {
        return;
    }
    if (OsixEvtSend (LDP_TSK_ID, LDP_UDP_MSG_EVT) == OSIX_FAILURE)
    {
        return;
    }
}


/*****************************************************************************/
/* Function Name : LdpProcessUdpMsgs                                         */
/* Description   : This function processes Queue Messages                    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

VOID
LdpProcessUdpMsgs (VOID)
{
    tLdpIfMsg           LdpIfMsg;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1Data = NULL;
    INT4                i4BytesRcvd = 0;
    struct sockaddr_in  PeerInfo;
    INT4                i4UdpSockFd;
    struct msghdr       PktInfo;
#ifdef BSDCOMP_SLI_WANTED
    struct cmsghdr     *pCmsgInfo;
    UINT1               au1Cmsg[24];
    struct iovec        Iov;
    struct in_pktinfo  *pIpPktInfo;
#else
    tInPktinfo         *pIpPktInfo;
    struct cmsghdr      CmsgInfo;
    UINT4               u4PeerLen = sizeof (PeerInfo);
#endif

    i4UdpSockFd = LDP_UDP_SOCKFD (LDP_CUR_INCARN);
    MEMSET ((INT1 *) &PeerInfo, 0, sizeof (struct sockaddr_in));
    pu1Data = MemAllocMemBlk (LDP_UDP_DATA_BUF_POOL_ID);
    if (pu1Data != NULL)
    {
#ifdef BSDCOMP_SLI_WANTED
        MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

        PktInfo.msg_name = (VOID *) &PeerInfo;
        PktInfo.msg_namelen = sizeof (struct sockaddr_in);
        Iov.iov_base = pu1Data;
        Iov.iov_len = LDP_MIN_MTU;
        PktInfo.msg_iov = &Iov;
        PktInfo.msg_iovlen = 1;
        PktInfo.msg_control = (VOID *) au1Cmsg;
        PktInfo.msg_controllen = sizeof (au1Cmsg);

        pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
        pCmsgInfo->cmsg_level = SOL_IP;
        pCmsgInfo->cmsg_type = IP_PKTINFO;
        pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
        while ((i4BytesRcvd = recvmsg (i4UdpSockFd, &PktInfo, 0)) > 0)
        {
            pBuf = CRU_BUF_Allocate_MsgBufChain (LDP_MIN_MTU, 0);
            if (pBuf != NULL)
            {

                if (CRU_BUF_Copy_OverBufChain (pBuf, pu1Data, 0, (UINT4)i4BytesRcvd)
                    == CRU_FAILURE)
                {

                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    continue;
                }
                pIpPktInfo = (struct in_pktinfo *) (VOID *)
                    CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
                LdpIfMsg.u4MsgType = LDP_UDP_EVENT;
                LdpIfMsg.u.UdpEvt.u4IfIndex = (UINT4)(pIpPktInfo->ipi_ifindex);
                LdpIfMsg.u.UdpEvt.u4PeerAddr =
                    OSIX_NTOHL (PeerInfo.sin_addr.s_addr);
                LdpIfMsg.u.UdpEvt.u4Evt = LDP_UDP_DATA_EVENT;
                LdpIfMsg.u.UdpEvt.pPdu = (UINT1 *) pBuf;

                LdpProcessUdpEvents (&LdpIfMsg.u.UdpEvt);
            }
            MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
            PktInfo.msg_name = (VOID *) &PeerInfo;
            PktInfo.msg_namelen = sizeof (struct sockaddr_in);
            Iov.iov_base = pu1Data;
            Iov.iov_len = LDP_MIN_MTU;
            PktInfo.msg_iov = &Iov;
            PktInfo.msg_iovlen = 1;
            PktInfo.msg_control = (VOID *) au1Cmsg;
            PktInfo.msg_controllen = sizeof (au1Cmsg);

            pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
            pCmsgInfo->cmsg_level = SOL_IP;
            pCmsgInfo->cmsg_type = IP_PKTINFO;
            pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
        }                        /* while end */
#else
        MEMSET ((INT1 *) &CmsgInfo, 0, sizeof (struct cmsghdr));
        PktInfo.msg_control = (VOID *) &CmsgInfo;
        while ((i4BytesRcvd =
                recvfrom (i4UdpSockFd, (INT1 *) pu1Data, LDP_MIN_MTU, 0,
                          (struct sockaddr *) &PeerInfo, &u4PeerLen)) > 0)
        {
            LDP_DBG (LDP_IF_MISC, "IF: Received Data on UDP socket.\n");
            if (recvmsg (i4UdpSockFd, &PktInfo, 0) < 0)
            {
                continue;
            }
            pIpPktInfo =
                (tInPktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

            pBuf = CRU_BUF_Allocate_MsgBufChain (LDP_MIN_MTU, 0);
            if (pBuf != NULL)

            {

                if (CRU_BUF_Copy_OverBufChain (pBuf, pu1Data, 0, (UINT4)i4BytesRcvd)
                    == CRU_FAILURE)
                {

                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    continue;
                }
                LdpIfMsg.u4MsgType = LDP_UDP_EVENT;
                LdpIfMsg.u.UdpEvt.u4IfIndex = (UINT4)pIpPktInfo->ipi_ifindex;
                LdpIfMsg.u.UdpEvt.u4PeerAddr =
                    OSIX_NTOHL (PeerInfo.sin_addr.s_addr);
                LdpIfMsg.u.UdpEvt.u4Evt = LDP_UDP_DATA_EVENT;
                LdpIfMsg.u.UdpEvt.pPdu = (UINT1 *) pBuf;

                LdpProcessUdpEvents (&LdpIfMsg.u.UdpEvt);
            }
        }
#endif
        MemReleaseMemBlock (LDP_UDP_DATA_BUF_POOL_ID, (UINT1 *) pu1Data);
    }
    else
    {
        LDP_DBG (LDP_IF_MEM, "Memory Allocation for Receiving UDP Buffer\r\n");
    }

    if (SelAddFd (i4UdpSockFd, LdpUdpPktInSocket) == OSIX_FAILURE)
    {
        LDP_DBG (LDP_IF_MISC, "LDPUDP: Can't do SelAddFd\n");
        close (i4UdpSockFd);
        return;
    }
    return;
}

/*****************************************************************************/
/* Function Name : LdpTcpPktInSocket                                         */
/* Description   : This function processes select TCP read event             */
/* Input(s)      : i4SockFd - Socket to be Processed                         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTcpPktInSocket (INT4 i4SockFd)
{
    if (LdpTcpEventSend (i4SockFd, LDP_READ_FD) == LDP_FAILURE)
    {
        if (SelAddFd (i4SockFd, LdpTcpPktInSocket) == OSIX_FAILURE)
        {
            LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Failed to process in  sock to select FD List\n");
        }
    }
}

/*****************************************************************************/
/* Function Name : LdpTcpPktOutSocket                                        */
/* Description   : This function processes select TCP write event            */
/* Input(s)      : i4SockFd - Socket to be Processed                         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTcpPktOutSocket (INT4 i4SockFd)
{
    if (LdpTcpEventSend (i4SockFd, LDP_WRITE_FD) == LDP_FAILURE)
    {
        if (SelAddWrFd (i4SockFd, LdpTcpPktOutSocket) == OSIX_FAILURE)
        {
            LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Failed to process out sock to select FD List\n");
        }
    }
}

/*****************************************************************************/
/* Function Name : LdpTcpEventSend                                           */
/* Description   : This function send TCP Select Event to LDP Task           */
/* Input(s)      : i4SockFd - Socket to be Processed                         */
/*                 u4Type - Read/Write Socket Event                          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
INT4
LdpTcpEventSend (INT4 i4SockFd, UINT4 u4Type)
{
    tLdpTcpUdpEnqMsgInfo LdpTcpUdpEnqMsgInfo;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    MEMSET (&LdpTcpUdpEnqMsgInfo, LDP_ZERO, sizeof (tLdpTcpUdpEnqMsgInfo));

    if (LDP_INITIALISED != TRUE)
    {
        return LDP_FAILURE;
    }

    LdpTcpUdpEnqMsgInfo.u4Event = LDP_TCP_RECV_REQ_EVENT;
    LdpTcpUdpEnqMsgInfo.i4SockFdOrConnId = i4SockFd;
    LdpTcpUdpEnqMsgInfo.u4MsgType = u4Type;
    LdpTcpUdpEnqMsgInfo.u1AddrType = LDP_ADDR_TYPE_IPV4;
#ifdef MPLS_IPV6_WANTED
    if(LDP_V6READ_FD == u4Type || LDP_V6WRITE_FD == u4Type)
    {
        LdpTcpUdpEnqMsgInfo.u1AddrType = LDP_ADDR_TYPE_IPV6;
        LdpTcpUdpEnqMsgInfo.u4Event = LDP_TCP6_RECV_REQ_EVENT;
    }
#endif

    pBuf = CRU_BUF_Allocate_MsgBufChain (sizeof (tLdpTcpUdpEnqMsgInfo), 0);
    if (pBuf == NULL)
    {
        LDP_DBG (LDP_IF_MISC, "LDPTCP: Buf Alloc Failed for TCP Event Date \n");
        return LDP_FAILURE;
    }
    if ((CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &LdpTcpUdpEnqMsgInfo, 0,
                                    sizeof (tLdpTcpUdpEnqMsgInfo))) ==
        CRU_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        LDP_DBG (LDP_IF_MISC, "LDPTCP: Buf copy Failed for TCP Event Date \n");
        return LDP_FAILURE;
    }

    if (OsixQueSend (LDP_TCP_QID, (UINT1 *) (&pBuf),
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        LDP_DBG (LDP_IF_MISC,
                 "LDPTCP: TCP Event Data Queue Send to LDP Task Failed\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return LDP_FAILURE;
    }
    if (OsixEvtSend (LDP_TSK_ID, LDP_TCP_MSG_EVT) == OSIX_FAILURE)
    {
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpCheckAndCloseTcpSocket                                 */
/* Description   : This routine closes only socket created ISS and ignores   */
/*                 socket created by Test Application                        */
/* Input(s)      : pLdpTcpUdpSockInfo - Pointer to Sock Info Structure       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpCheckAndCloseTcpSocket (tLdpTcpUdpSockInfo * pLdpTcpUdpSockInfo)
{
    if (pLdpTcpUdpSockInfo->u1RowStatus != LDP_ZERO)
    {
        /* Created by User for testing purpose. So, Return here */
        return;
    }

    TmrStopTimer (LDP_TIMER_LIST_ID,
                  (tTmrAppTimer *) & (pLdpTcpUdpSockInfo->SockReadTimer.
                                      AppTimer));

    close (pLdpTcpUdpSockInfo->i4SockDesc);

    return;
}

/*****************************************************************************/
/* Function Name : LdpCheckAndClearSocketInfo                                */
/* Description   : This routine clears user created socket info              */
/* Input(s)      : pLdpTcpUdpSockInfo - Pointer to Sock Info Structure       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpCheckAndClearSocketInfo (tLdpTcpUdpSockInfo * pLdpTcpUdpSockInfo)
{
    if (pLdpTcpUdpSockInfo->u1RowStatus != LDP_ZERO)
    {
        /* Created by User for testing purpose. So, Return here */
        return;
    }

    TmrStopTimer (LDP_TIMER_LIST_ID,
                  &(pLdpTcpUdpSockInfo->SockReadTimer.AppTimer));


    MemReleaseMemBlock (LDP_SOCK_RECV_BUF_POOL_ID,
                        (UINT1 *) pLdpTcpUdpSockInfo->pRcvBuf);
    MemReleaseMemBlock (LDP_SOCK_SEND_PEND_BUF_POOL_ID,
                        (UINT1 *) pLdpTcpUdpSockInfo->pPendWriteBuf);

    MEMSET (pLdpTcpUdpSockInfo, 0, sizeof (tLdpTcpUdpSockInfo));

    gu4CurSocketConns--;

    return;
}

/*****************************************************************************/
/* Function Name : LdpTcpUdpSockClose                                        */
/* Description   : This routine closes the TCP connection table              */
/* Input(s)      : pLdpTcpUdpSockInfo - Pointer to Sock Info Structure       */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTcpUdpSockClose (tLdpTcpUdpSockInfo * pLdpTcpUdpSockInfo)
{
    if (pLdpTcpUdpSockInfo->u1RowStatus != LDP_ZERO)
    {
        /* Created by User for testing purpose. So, Return here */
        return;
    }
    SelRemoveWrFd (pLdpTcpUdpSockInfo->i4SockDesc);
    SelRemoveFd (pLdpTcpUdpSockInfo->i4SockDesc);
    
    LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
    
    LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
    
    return;
}

/*****************************************************************************/
/* Function Name : LdpGetConnIdFromDestIp                                    */
/* Description   : Function for getting the TCP connection identifier        */
/*                 based on Destination IP from Ssn Table or Connection Table*/
/* Input(s)      : u4DestIp - Destination IP                                 */
/* Output(s)     : None                                                      */
/* Return(s)     : u4ConnId - Next Connection Id or ZERO                     */
/*****************************************************************************/
UINT4
LdpGetConnIdFromDestIp (UINT4 u4DestIp)
{
    UINT4               u4Count = LDP_ZERO;
    UINT4               u4SetDestIp = LDP_ZERO;
    UINT4               u4TempDestIp = LDP_ZERO;
    tTMO_HASH_TABLE    *pTcpConnHshTbl = NULL;
    tLdpSession        *pLdpSession = NULL;
    tTMO_HASH_NODE     *pHashNode = NULL;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    pTcpConnHshTbl = LDP_TCPCONN_HSH_TABLE (MPLS_DEF_INCARN);
    TMO_HASH_Scan_Table (pTcpConnHshTbl, u4Count)
    {
        TMO_HASH_Scan_Bucket (pTcpConnHshTbl, u4Count, pHashNode,
                              tTMO_HASH_NODE *)
        {
            pLdpSession = LDP_SSN_OFFSET_HTBL (pHashNode);
            CONVERT_TO_INTEGER (pLdpSession->pLdpPeer->TransAddr.Addr.au1Ipv4Addr,
                                u4SetDestIp);
            u4SetDestIp = OSIX_NTOHL (u4SetDestIp);
            if (u4SetDestIp == u4DestIp)
            {
                return (pLdpSession->u4TcpConnId);
            }
        }
    }
    pLdpTcpUdpSockInfo = gpLdpTcpUdpSockInfoTab;
    for (u4Count = 0; u4Count < MAX_LDP_TCP_UDP_SSN_SUPRTD;
         u4Count++, pLdpTcpUdpSockInfo++)
    {
        /* MPLS_IPv6 add start*/
        MEMCPY(&u4TempDestIp, LDP_IPV4_ADDR(pLdpTcpUdpSockInfo->DestAddr), LDP_IPV4ADR_LEN);
        u4TempDestIp = OSIX_NTOHL(u4TempDestIp);
        if (u4TempDestIp == u4DestIp)
        {
            /* MPLS_IPv6 add end*/
            return (pLdpTcpUdpSockInfo->u4ConnId);
        }
    }

    return LDP_ZERO;
}

/*****************************************************************************/
/* Function Name : LdpGetNextConnId                                          */
/* Description   : Function for getting the next used connection identifier  */
/* Input(s)      : u4CurrConnId  - Current Connection Id                     */
/* Output(s)     : None                                                      */
/* Return(s)     : u4NextConnId  - Next Connection Id or ZERO                */
/*****************************************************************************/
UINT4
LdpGetNextConnId (UINT4 u4CurrConnId)
{
    UINT4               u4Count = LDP_ZERO;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;

    for (u4Count = (u4CurrConnId + LDP_ONE);
         u4Count <= MAX_LDP_TCP_UDP_SSN_SUPRTD; u4Count++)
    {
        pLdpTcpUdpSockInfo = &(gpLdpTcpUdpSockInfoTab[u4Count - LDP_ONE]);
        if ((pLdpTcpUdpSockInfo->u1SockStatus != SOCK_FREE) &&
            (pLdpTcpUdpSockInfo->u1RowStatus != LDP_ZERO))
        {
            return (pLdpTcpUdpSockInfo->u4ConnId);
        }
    }

    return LDP_ZERO;
}

/*****************************************************************************/
/* Function Name : LdpGetSockInfoFromConnId                                  */
/* Description   : Function for getting the socket associated with a         */
/*                 Connection Identifier                                     */
/* Input(s)      : u4ConnId    - Connection Identifier                       */
/* Output(s)     : None                                                      */
/* Return(s)     : Pointer to the socket info structure or NULL              */
/*****************************************************************************/
tLdpTcpUdpSockInfo *
LdpGetSockInfoFromConnId (UINT4 u4ConnId)
{
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;

    if (u4ConnId >= MAX_LDP_TCP_UDP_SSN_SUPRTD)
    {
        return NULL;
    }

    pLdpTcpUdpSockInfo = &(gpLdpTcpUdpSockInfoTab[u4ConnId - LDP_ONE]);

    if (pLdpTcpUdpSockInfo->u1SockStatus == SOCK_FREE)

    {
        return NULL;
    }

    return pLdpTcpUdpSockInfo;
}

/*****************************************************************************/
/* Function Name : LdpSetNewSockConnection                                   */
/* Description   : Function for setting new Socket Connection for the        */
/*                 passed Connection Identifier                              */
/* Input(s)      : u4ConnId    - Connection Identifier                       */
/* Output(s)     : None                                                      */
/* Return(s)     : u4ConnId    - Same Connection Identifier if Success       */
/*                               0 if Failure                                */
/*****************************************************************************/
UINT4
LdpSetNewSockConnection (UINT4 u4ConnId)
{
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;

    if (u4ConnId >= MAX_LDP_TCP_UDP_SSN_SUPRTD)
    {
        return 0;
    }

    pLdpTcpUdpSockInfo = &(gpLdpTcpUdpSockInfoTab[u4ConnId - LDP_ONE]);

    if (pLdpTcpUdpSockInfo->u1SockStatus == SOCK_FREE)
    {
        MEMSET (pLdpTcpUdpSockInfo, 0, sizeof (tLdpTcpUdpSockInfo));

        pLdpTcpUdpSockInfo->pRcvBuf =
            (INT1 *) MemAllocMemBlk (LDP_SOCK_RECV_BUF_POOL_ID);

        if ((pLdpTcpUdpSockInfo->pRcvBuf) == NULL)
        {
            LDP_DBG (LDP_IF_MEM,
                     "LDPTCP: Server Connection Accept - Memory Allocation "
                     "failed for TCP Receive Buffer\r\n");

            return 0;
        }

        pLdpTcpUdpSockInfo->pPendWriteBuf =
            (INT1 *) MemAllocMemBlk (LDP_SOCK_SEND_PEND_BUF_POOL_ID);

        if (pLdpTcpUdpSockInfo->pPendWriteBuf == NULL)
        {
            LDP_DBG (LDP_IF_MEM,
                     "LDPTCP: Server Connection Accept - Memory Allocation "
                     "failed for TCP Pending Write Buffer\r\n");

            MemReleaseMemBlock (LDP_SOCK_RECV_BUF_POOL_ID,
                                (UINT1 *) pLdpTcpUdpSockInfo->pRcvBuf);

            return 0;
        }

        MEMSET (pLdpTcpUdpSockInfo->pRcvBuf, 0, LDP_MAX_FRAGMENT);
        MEMSET (pLdpTcpUdpSockInfo->pPendWriteBuf, 0, LDP_MAX_FRAGMENT);

        pLdpTcpUdpSockInfo->u4ConnId = u4ConnId;
        pLdpTcpUdpSockInfo->u2Port = LDP_STD_PORT;
        pLdpTcpUdpSockInfo->u1SockStatus = SOCK_UNCONNECTED;
        pLdpTcpUdpSockInfo->u1RowStatus = NOT_READY;

        return (u4ConnId);
    }

    return 0;
}

/*****************************************************************************/
/* Function Name : LdpCloseSockConnection                                    */
/* Description   : Function for closing Socket Connection for the            */
/*                 passed Connection Identifier                              */
/* Input(s)      : u4ConnId    - Connection Identifier                       */
/* Output(s)     : None                                                      */
/* Return(s)     : u4ConnId    - Same Connection Identifier if Success       */
/*                               0 if Failure                                */
/*****************************************************************************/
UINT4
LdpCloseSockConnection (UINT4 u4ConnId)
{
    tLdpTcpUdpEnqMsgInfo LdpTcpUdpEnqMsgInfo;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;

    MEMSET (&LdpTcpUdpEnqMsgInfo, 0, sizeof (tLdpTcpUdpEnqMsgInfo));

    if (u4ConnId >= MAX_LDP_TCP_UDP_SSN_SUPRTD)
    {
        return 0;
    }

    pLdpTcpUdpSockInfo = &(gpLdpTcpUdpSockInfoTab[u4ConnId - LDP_ONE]);

    if (pLdpTcpUdpSockInfo->u1SockStatus == SOCK_FREE)
    {
        return 0;
    }

    pLdpTcpUdpSockInfo->u1RowStatus = LDP_ZERO;
    LdpTcpUdpEnqMsgInfo.u1AddrType = pLdpTcpUdpSockInfo->u1AddrType;
    LdpTcpUdpEnqMsgInfo.u4Event = LDP_TCP_CLOSE_REQ_EVENT;
    LdpTcpUdpEnqMsgInfo.i4SockFdOrConnId = (INT4) u4ConnId;

    LdpTcpProcCloseReqEvent (&LdpTcpUdpEnqMsgInfo);

    return u4ConnId;
}

/*****************************************************************************/
/* Function Name : LdpActivateSockConnection                                 */
/* Description   : Function for activating Socket Connection for th          */
/*                 passed Connection Identifier                              */
/* Input(s)      : u4ConnId    - Connection Identifier                       */
/* Output(s)     : None                                                      */
/* Return(s)     : u4ConnId    - Same Connection Identifier if Success       */
/*                               0 if Failure                                */
/*****************************************************************************/
UINT4
LdpActivateSockConnection (UINT4 u4ConnId)
{
    tLdpTcpUdpEnqMsgInfo LdpTcpUdpEnqMsgInfo;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;

    MEMSET (&LdpTcpUdpEnqMsgInfo, 0, sizeof (tLdpTcpUdpEnqMsgInfo));

    if (u4ConnId >= MAX_LDP_TCP_UDP_SSN_SUPRTD)
    {
        return 0;
    }

    pLdpTcpUdpSockInfo = &(gpLdpTcpUdpSockInfoTab[u4ConnId - LDP_ONE]);

    if (pLdpTcpUdpSockInfo->u1SockStatus == SOCK_FREE)
    {
        return 0;
    }

    pLdpTcpUdpSockInfo->u1SockType = TCP_CLI_OPEN;
    pLdpTcpUdpSockInfo->u1RowStatus = ACTIVE;

    LdpTcpUdpEnqMsgInfo.u4Event = LDP_TCP_OPEN_REQ_EVENT;
    LdpTcpUdpEnqMsgInfo.i4SockFdOrConnId = (INT4) u4ConnId;

    LdpTcpProcOpenReqEvent (&LdpTcpUdpEnqMsgInfo);

    return u4ConnId;
}

/*****************************************************************************/
/* Function Name : LdpSendPktOverConnection                                  */
/* Description   : Function for sending packet over Socket Connection        */
/* Input(s)      : u4ConnId           - Connection Identifier                */
/*                 pMsg               - Pointer to Message                   */
/*                 i4MsgLen           - Message Length                       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_FAILURE / LDP_SUCCESS                                 */
/*****************************************************************************/
INT4
LdpSendPktOverConnection (UINT4 u4ConnId, UINT1 *pMsg, INT4 i4MsgLen)
{
    INT4                i4RetVal = 0;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    INT4                i4PendWriteLen = 0;
    UINT1               u1DataFlag = 0;

#ifdef BSDCOMP_SLI_WANTED
    /* Here we are not using data flags in send so, u1DataFlag is zero */
    u1DataFlag = 0;
#else
    u1DataFlag = MSG_PUSH;
#endif

    pLdpTcpUdpSockInfo = LdpTcpUdpGetSocket (u4ConnId, LDP_SOCK_MASK);
    if (pLdpTcpUdpSockInfo == NULL)
    {
        return LDP_FAILURE;
    }

    if (pLdpTcpUdpSockInfo->u1SockStatus != SOCK_ESTABLISHED)
    {
        return LDP_FAILURE;
    }

    i4RetVal = send (pLdpTcpUdpSockInfo->i4SockDesc, (UINT1 *) pMsg,
                     i4MsgLen, (UINT4)(u1DataFlag | MSG_NOSIGNAL));

    if ((i4RetVal != i4MsgLen) && ((errno == EAGAIN) || (errno == EWOULDBLOCK)))
    {
        i4PendWriteLen = pLdpTcpUdpSockInfo->i4PendWriteLen;

        if ((i4PendWriteLen + i4MsgLen) > LDP_MAX_FRAGMENT)
        {
            return LDP_FAILURE;
        }

        MEMCPY (&(pLdpTcpUdpSockInfo->pPendWriteBuf[i4PendWriteLen]),
                pMsg, i4MsgLen);
        pLdpTcpUdpSockInfo->i4PendWriteLen += i4MsgLen;

#ifdef MPLS_IPV6_WANTED
        if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
	{
            if (SelAddWrFd (pLdpTcpUdpSockInfo->i4SockDesc, LdpTcp6PktOutSocket) == OSIX_FAILURE)
            {
                LDP_DBG1 (LDP_IF_TX,
                        "SelAddWrFd failed for socket for Ipv6%d\r\n",
                        pLdpTcpUdpSockInfo->i4SockDesc);

       }
       else
       {
                pLdpTcpUdpSockInfo->b1IsWaitingSockWriteReady = TRUE;
            }
	    }
        else
        { 
#endif
            if (SelAddWrFd (pLdpTcpUdpSockInfo->i4SockDesc, LdpTcpPktOutSocket) == OSIX_FAILURE)
            {
                LDP_DBG1 (LDP_IF_TX,
                        "SelAddWrFd failed for socket for Ipv4%d\r\n",
                        pLdpTcpUdpSockInfo->i4SockDesc);
            }
            else
            {
                pLdpTcpUdpSockInfo->b1IsWaitingSockWriteReady = TRUE;
       }
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }
    else if (i4RetVal != i4MsgLen)
    {
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpStoreTcpSourcePort                                     */
/* Description   : Function for storing source port corresponding to socket  */
/* Input(s)      : pLdpTcpUdpSockInfo - Pointer to TCP UDP Sock Info         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

PRIVATE VOID
LdpStoreTcpSourcePort (tLdpTcpUdpSockInfo * pLdpTcpUdpSockInfo)
{
    struct sockaddr_in  GetAddr;
    UINT4               u4Addr = 0;

    MEMSET ((INT1 *) &GetAddr, 0, sizeof (GetAddr));

    if (getsockname (pLdpTcpUdpSockInfo->i4SockDesc,
                     ((struct sockaddr *) (VOID *) &GetAddr), &u4Addr) < 0)
    {
        LDP_DBG (LDP_IF_MISC, "LDPTCP: Failed to get sock name\n");
        return;
    }

    pLdpTcpUdpSockInfo->u2SrcPort = OSIX_NTOHS (GetAddr.sin_port);

    return;
}

/*****************************************************************************/
/* Function Name : SockAddSelAddReadFd                                       */
/* Description   : Function for adding sel add FD                            */
/* Input(s)      : pLdpTcpUdpSockInfo                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
VOID
SockAddSelAddReadFd (tLdpTcpUdpSockInfo * pLdpTcpUdpSockInfo)
{
    if ((pLdpTcpUdpSockInfo->u1SockStatus == SOCK_SERVER) ||
        (pLdpTcpUdpSockInfo->u1SockStatus == SOCK_ESTABLISHED))
    {
#ifdef MPLS_IPV6_WANTED
        if(pLdpTcpUdpSockInfo->u1AddrType == LDP_ADDR_TYPE_IPV6)
        {
            if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc,
                      LdpTcp6PktInSocket) == OSIX_FAILURE)
            {
                LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Failed to add sock to select FD List\n");
                LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
            }
        }
        else
        {
#endif
        if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc,
                      LdpTcpPktInSocket) == OSIX_FAILURE)
            {
                LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Failed to add sock to select FD List\n");
                LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }
}

/*****************************************************************************/
/* Function Name : SockSendPendingData                                       */
/* Description   : Function for sending pending buffer data.                 */
/* Input(s)      : pLdpTcpUdpSockInfo - Pointer to LDP TCP Sock Info         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
SockSendPendingData (tLdpTcpUdpSockInfo * pLdpTcpUdpSockInfo)
{
    UINT1              *pu1Data = NULL;
    INT4                i4DataSent = 0;
    INT4                i4PendWriteLen = 0;
    /* MPLS_IPv6 add start*/	
    UINT4              u4TempDestAddr = 0;
    /* MPLS_IPv6 add end*/	
    UINT1               u1DataFlag = 0;
    /* MPLS_IPv6 add start*/
    if(LDP_ADDR_TYPE_IPV4 == pLdpTcpUdpSockInfo->u1AddrType)
    {
        MEMCPY(&u4TempDestAddr, pLdpTcpUdpSockInfo->DestAddr.au1Ipv4Addr, LDP_IPV4ADR_LEN);
    }
    /* MPLS_IPv6 add end*/		

 
#ifdef BSDCOMP_SLI_WANTED
    /* Here we are not using data flags in send so, u1DataFlag is zero */
    u1DataFlag = 0;
#else
    u1DataFlag = MSG_PUSH;
#endif

    pu1Data = MemAllocMemBlk (LDP_TCP_SEND_BUF_POOL_ID);

    if (pu1Data == NULL)
    {
        LDP_DBG2 ((LDP_IF_TX | LDP_IF_MEM),
                  "LDPTCP: SendPendingData Send Buffer Memory Allocation failed "
                  "for %d %x\r\n",
                  pLdpTcpUdpSockInfo->i4SockDesc,
                  u4TempDestAddr);

        return LDP_FAILURE;
    }

    while (pLdpTcpUdpSockInfo->i4PendWriteLen > LDP_ZERO)
    {
        if (pLdpTcpUdpSockInfo->i4PendWriteLen > LDP_MIN_MTU)
        {
            i4PendWriteLen = LDP_MIN_MTU;
        }
        else
        {
            i4PendWriteLen = pLdpTcpUdpSockInfo->i4PendWriteLen;
        }

        LDP_DBG4 (LDP_IF_TX,
                  "Pending Data %d bytes choosen to write from %d bytes for "
                  "%d %x\r\n",
                  i4PendWriteLen, pLdpTcpUdpSockInfo->i4PendWriteLen,
                  pLdpTcpUdpSockInfo->i4SockDesc,
                  u4TempDestAddr);

        MEMSET (pu1Data, 0, LDP_MIN_MTU);
        MEMCPY (pu1Data, pLdpTcpUdpSockInfo->pPendWriteBuf, i4PendWriteLen);

        i4DataSent = send (pLdpTcpUdpSockInfo->i4SockDesc, (UINT1 *) pu1Data,
                           i4PendWriteLen, (UINT4)(u1DataFlag | MSG_NOSIGNAL));

        if ((i4DataSent != i4PendWriteLen) &&
            ((errno == EAGAIN) || (errno == EWOULDBLOCK)))
        {
            LDP_DBG2 ((LDP_IF_TX | LDP_IF_MEM),
                      "EAGAIN received for %d %x\r\n",
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      u4TempDestAddr);

            MEMCPY (&(pLdpTcpUdpSockInfo->pPendWriteBuf[0]),

                    &(pLdpTcpUdpSockInfo->pPendWriteBuf[i4DataSent]), (pLdpTcpUdpSockInfo->i4PendWriteLen - i4DataSent));

            pLdpTcpUdpSockInfo->i4PendWriteLen -= i4DataSent;

#ifdef MPLS_IPV6_WANTED
            if(LDP_ADDR_TYPE_IPV6 == pLdpTcpUdpSockInfo->u1AddrType)
	    {
                if (SelAddWrFd (pLdpTcpUdpSockInfo->i4SockDesc, LdpTcp6PktOutSocket)
                        == OSIX_FAILURE) 
                {
                    LDP_DBG1 (LDP_IF_TX,
                            "SelAddWrFd failed for socket for IPV6%d\r\n",
                            pLdpTcpUdpSockInfo->i4SockDesc);
           }
           else
           {
                    pLdpTcpUdpSockInfo->b1IsWaitingSockWriteReady = TRUE;
                }
            }
            else
            {
#endif
                if (SelAddWrFd (pLdpTcpUdpSockInfo->i4SockDesc, LdpTcpPktOutSocket)
                        == OSIX_FAILURE)
                {
                    LDP_DBG1 (LDP_IF_TX,
                            "SelAddWrFd failed for socket for IPv4 %d\r\n",
                            pLdpTcpUdpSockInfo->i4SockDesc);
                }
                else
                {
                    pLdpTcpUdpSockInfo->b1IsWaitingSockWriteReady = TRUE;
           }

#ifdef MPLS_IPV6_WANTED
            }
#endif   
            break;
        }
        else if (i4DataSent != i4PendWriteLen)
        {
            LDP_DBG4 (LDP_IF_TX,
                      "Pending Data %d bytes not sent, Actual sent %d bytes "
                      "for %d %x\r\n",
                      i4PendWriteLen, i4DataSent,
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      u4TempDestAddr);

            MemReleaseMemBlock (LDP_TCP_SEND_BUF_POOL_ID, (UINT1 *) pu1Data);
            LDP_SOCK_EVENT_INDICATE (LDP_TCP_CONN_DOWN_EVENT,
                                     pLdpTcpUdpSockInfo);

            return LDP_FAILURE;
        }

        pLdpTcpUdpSockInfo->i4PendWriteLen -= i4PendWriteLen;

        LDP_DBG4 (LDP_IF_TX,
                  "Pending data %d bytes sent, Adjusted Pending Write Buffer "
                  "is %d bytes for socket %d %x\r\n",
                  i4PendWriteLen, pLdpTcpUdpSockInfo->i4PendWriteLen,
                  pLdpTcpUdpSockInfo->i4SockDesc,
                  u4TempDestAddr);

        if (pLdpTcpUdpSockInfo->i4PendWriteLen <= LDP_ZERO)
        {
            pLdpTcpUdpSockInfo->i4PendWriteLen = LDP_ZERO;

            LDP_DBG2 (LDP_IF_RX,
                      "No more pending data for %d %x\r\n",
                      pLdpTcpUdpSockInfo->i4SockDesc,
                      u4TempDestAddr);

            break;
        }

        MEMCPY (&(pLdpTcpUdpSockInfo->pPendWriteBuf[0]),
                &(pLdpTcpUdpSockInfo->pPendWriteBuf[i4PendWriteLen]),
                pLdpTcpUdpSockInfo->i4PendWriteLen);
    }

    MemReleaseMemBlock (LDP_TCP_SEND_BUF_POOL_ID, (UINT1 *) pu1Data);

    return LDP_SUCCESS;
}

/* MPLS_IPv6 add start*/

/****************************************************************************/
/* Function Name : LdpIpv6UdpOpen                                           */
/* Description   : Function for opening the Udp port for IPv6 Adr Family    */
/* Input(s)      : u2Port - UDP port                                 		*/
/* Output(s)     : None                                                     */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                  */
/****************************************************************************/
#ifdef MPLS_IPV6_WANTED
UINT1
LdpIpv6UdpOpen (UINT2 u2Port)
{
 	/*
     * Opening the Standard Udp Port with Local Address as IN6ADDR_ANY_INIT which
     * is { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 }.
     * All packets received for this Port will be handed over to LDP.
     */
#ifdef LNXIP6_WANTED
    INT4                i4OptVal = 1;
#endif
#ifdef BSDCOMP_SLI_WANTED
    INT4                i4MCloopVal = LDP_FALSE;    /*Reset multicast looping */
#endif
    INT4                i4Ipv6UdpSockFd = -1;
    struct sockaddr_in6 LdpIpv6UdpLocalAddr;
#ifndef BSDCOMP_SLI_WANTED
    UINT1               u1OpnVal = 1;
#endif
    UINT4               u4SetVal = 1;

    if ((i4Ipv6UdpSockFd = socket (AF_INET6, SOCK_DGRAM, IPPROTO_UDP)) < 0)
    {
        LDP_DBG (LDP_IF_UDP, "IF: Can't open Ipv6 UDP socket\n");
        return LDP_FAILURE;
    }

    LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN) = i4Ipv6UdpSockFd;

    MEMSET ((INT1 *) &LdpIpv6UdpLocalAddr, 0, sizeof (struct sockaddr_in6));

#ifdef LNXIP6_WANTED
    if (setsockopt
        (i4Ipv6UdpSockFd, IPPROTO_IPV6, IPV6_V6ONLY,
         &i4OptVal, sizeof (INT4)) < 0)
    {
        /* setsockopt Failed for IP_PKTINFO */
        LDP_DBG (LDP_IF_UDP, "IF: setsockopt for IPV6_V6ONLY failed \n");
        return LDP_FAILURE;
    }
#endif
    LdpIpv6UdpLocalAddr.sin6_family = AF_INET6;
    #ifdef BSDCOMP_SLI_WANTED
        LdpIpv6UdpLocalAddr.sin6_addr =  in6addr_any;
    #else
        LdpIpv6UdpLocalAddr.sin6_addr = (struct in6_addr) IN6_ADDR_ANY;
    #endif

    LdpIpv6UdpLocalAddr.sin6_port = OSIX_HTONS (u2Port);

    /* Bind to Well-Known port (646) */
    if (bind (i4Ipv6UdpSockFd, (struct sockaddr *) &LdpIpv6UdpLocalAddr,
              sizeof (LdpIpv6UdpLocalAddr)) < 0)
    {
        LDP_DBG (LDP_IF_UDP, "IF: Ipv6 Udp bind failed \n");
        close (i4Ipv6UdpSockFd);
        return LDP_FAILURE;
    }
    if ((fcntl (i4Ipv6UdpSockFd, F_SETFL, O_NONBLOCK)) < 0)
    {
        LDP_DBG (LDP_IF_MISC, "LDP IPv6 UDP: Can't Set Ip_UDP Sckt in NON BLK\n");
        close (i4Ipv6UdpSockFd);
        return LDP_FAILURE;
    }
    if ((setsockopt (i4Ipv6UdpSockFd, IPPROTO_IPV6,
               IPV6_RECVHOPLIMIT, &u4SetVal, sizeof (UINT4))) < 0)
    {
        LDP_DBG (LDP_IF_UDP, "IF: Fail to set IPV6 Recv Hoplimit. \n");
        return LDP_FAILURE;
    }
 #ifndef BSDCOMP_SLI_WANTED
    if (setsockopt (i4Ipv6UdpSockFd, IPPROTO_IPV6, IP_PKTINFO, &u1OpnVal,
                    sizeof (UINT1)) < 0)
    {
        LDP_DBG (LDP_IF_MISC, "LDP IPv6 UDP: Can't Set SetSockOpt for IP_INFO\n");
		close (i4Ipv6UdpSockFd);
        return LDP_FAILURE;
     }
 #endif
 #ifdef BSDCOMP_SLI_WANTED
     /*Set the option not to receive packet on the interface on which
      * it is sent*/
     if (setsockopt (i4Ipv6UdpSockFd, IPPROTO_IPV6, IPV6_MULTICAST_LOOP,
                     &i4MCloopVal, sizeof (INT4)) < 0)
     {
         LDP_DBG (LDP_IF_MISC, "LDP IPv6 UDP: Can't Set SetSockOpt for Mcast Loop\n");
         close (i4Ipv6UdpSockFd);
         return LDP_FAILURE;
     }
 #endif
     if (SelAddFd (i4Ipv6UdpSockFd, LdpIpv6UdpPktInSocket) != OSIX_SUCCESS)
     {

         LDP_DBG (LDP_IF_MISC, "LDP IPv6 UDP : Can't do SelAddFd\n");
         close (i4Ipv6UdpSockFd);
         return LDP_FAILURE;
     }
     return LDP_SUCCESS;
}


/*****************************************************************************/
/* Function Name : LdpProcessIpv6UdpMsgs                                         */
/* Description   : This function processes Queue Messages                    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

VOID
LdpProcessIpv6UdpMsgs (VOID)
{
    tLdpIfMsg           LdpIfMsg;
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;
    UINT1              *pu1Data = NULL;
    UINT1              u1DiscardV6UdpPkt = LDP_FALSE;
    INT4                i4BytesRcvd = 0;
    struct sockaddr_in6  PeerInfo;
    INT4                i4Udp6SockFd;
    struct msghdr       PktInfo;
#ifdef BSDCOMP_SLI_WANTED
    struct cmsghdr     *pCmsgInfo;
    UINT1               au1Cmsg[112];
    struct iovec        Iov;
    struct in6_pktinfo  *pIp6PktInfo;
#else
    tIn6Pktinfo         *pIp6PktInfo;
    struct cmsghdr      CmsgInfo;
    UINT4               u4PeerLen = sizeof (PeerInfo);
    tIp6Addr           DestAddr;
    tIp6Addr           V6AllRouterAddr;
    MEMSET(&DestAddr, 0, sizeof(tIp6Addr));
    MEMSET(&V6AllRouterAddr, 0, sizeof(tIp6Addr));
#endif

    i4Udp6SockFd = LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
    MEMSET ((INT1 *) &PeerInfo, 0, sizeof (struct sockaddr_in6));
    pu1Data = MemAllocMemBlk (LDP_UDP_DATA_BUF_POOL_ID);
    if (pu1Data != NULL)
    {
#ifdef BSDCOMP_SLI_WANTED
        MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

        PktInfo.msg_name = (VOID *) &PeerInfo;
        PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
        Iov.iov_base = pu1Data;
        Iov.iov_len = LDP_MIN_MTU;
        PktInfo.msg_iov = &Iov;
        PktInfo.msg_iovlen = 1;
        PktInfo.msg_control = (VOID *) au1Cmsg;
        PktInfo.msg_controllen = sizeof (au1Cmsg);

        pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
        pCmsgInfo->cmsg_level = SOL_IPV6;
        pCmsgInfo->cmsg_type = IPV6_PKTINFO;
        pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
        while ((i4BytesRcvd = recvmsg (i4Udp6SockFd, &PktInfo, 0)) > 0)
        {
            u1DiscardV6UdpPkt = LDP_FALSE;
            pBuf = CRU_BUF_Allocate_MsgBufChain (LDP_MIN_MTU, 0);
            if (pBuf != NULL)
            {

                if (CRU_BUF_Copy_OverBufChain (pBuf, pu1Data, 0, (UINT4)i4BytesRcvd)
                    == CRU_FAILURE)
                {

                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    continue;
                }
                pIp6PktInfo = (struct in6_pktinfo *) (VOID *)
                   CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
                LdpIfMsg.u4MsgType = LDP_UDP6_EVENT;
                LdpIfMsg.u.UdpEvt.u4IfIndex = (pIp6PktInfo->ipi6_ifindex);
                MEMCPY(LdpIfMsg.u.UdpEvt.PeerAddr.u1_addr, PeerInfo.sin6_addr.s6_addr, LDP_IPV6ADR_LEN);		
                LdpIfMsg.u.UdpEvt.u4Evt = LDP_UDP6_DATA_EVENT;
                LdpIfMsg.u.UdpEvt.pPdu = (UINT1 *) pBuf;

                LdpProcessIpv6UdpEvents (&LdpIfMsg.u.UdpEvt);
            }
            MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
            PktInfo.msg_name = (VOID *) &PeerInfo;
            PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
            Iov.iov_base = pu1Data;
            Iov.iov_len = LDP_MIN_MTU;
            PktInfo.msg_iov = &Iov;
            PktInfo.msg_iovlen = 1;
            PktInfo.msg_control = (VOID *) au1Cmsg;
            PktInfo.msg_controllen = sizeof (au1Cmsg);

            pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
            pCmsgInfo->cmsg_level = SOL_IPV6;
            pCmsgInfo->cmsg_type = IPV6_PKTINFO;
            pCmsgInfo->cmsg_len = sizeof (au1Cmsg);
        }                            /* while end */
       
        UNUSED_PARAM(u1DiscardV6UdpPkt);
#else
        MEMSET ((INT1 *) &CmsgInfo, 0, sizeof (struct cmsghdr));
        PktInfo.msg_control = (VOID *) &CmsgInfo;
        while ((i4BytesRcvd =
                recvfrom (i4Udp6SockFd, (INT1 *) pu1Data, LDP_MIN_MTU, 0,
                          (struct sockaddr *) &PeerInfo, &u4PeerLen)) > 0)
        {
            u1DiscardV6UdpPkt = LDP_FALSE;
            LDP_DBG (LDP_IF_MISC, "IF: Received Data on UDP socket.\n");
            if (recvmsg (i4Udp6SockFd, &PktInfo, 0) < 0)
            {
                continue;
            }
            pIp6PktInfo =
                (tIn6Pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
	     MEMCPY(DestAddr.u1_addr,(tIp6Addr *) (VOID *)&pIp6PktInfo->ipi6_addr.s6_addr, LDP_IPV6ADR_LEN);
	     LDP_DBG2(LDP_IF_MISC, "IF: Received IPV6 UDP Data with Address: %s, with HopLimit %x\n",
		 	Ip6PrintAddr(&DestAddr),pIp6PktInfo->in6_hoplimit);
	     if(LDP_FAILURE != INET_ATON6(LDP_ALL_ROUTERS_V6ADDR, & V6AllRouterAddr))
	     {
	         if(LDP_ZERO == MEMCMP(DestAddr.u1_addr, V6AllRouterAddr.u1_addr, LDP_IPV6ADR_LEN))
		  {
                    LDP_DBG(LDP_IF_MISC,"IF: IPV6 UDP Packet Received with Dest Addr LDP_ALL_ROUTERS_V6ADDR\n");
		      if(IP6_MAX_HLIM != pIp6PktInfo->in6_hoplimit)
		      {
		          LDP_DBG(LDP_IF_MISC,"IF: IPV6 UDP Packet Received with Dest Addr LDP_ALL_ROUTERS_V6ADDR and Hop Limit Not 255 \n");
		          u1DiscardV6UdpPkt = LDP_TRUE;
		      }
		      else
		      {
			    LDP_DBG(LDP_IF_MISC,"IF: IPV6 UDP Packet Received with Dest Addr LDP_ALL_ROUTERS_V6ADDR and Hop Limit Is 255 \n");
			    u1DiscardV6UdpPkt = LDP_FALSE;	
		      }
	         }
		  else
		  {
		      LDP_DBG(LDP_IF_MISC,"IF: IPV6 UDP Packet Not Received with Dest Addr LDP_ALL_ROUTERS_V6ADDR\n");
		      u1DiscardV6UdpPkt = LDP_TRUE;
		  }
	     }
	     else
	     {
	         u1DiscardV6UdpPkt = LDP_FALSE;
	         LDP_DBG (LDP_IF_MISC,
                   "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
		  /*Not Handling this situation so RETRUN not added*/
	     }
            pBuf = CRU_BUF_Allocate_MsgBufChain (LDP_MIN_MTU, 0);
            if (pBuf != NULL)

            {

                if (CRU_BUF_Copy_OverBufChain (pBuf, pu1Data, 0, (UINT4)i4BytesRcvd)
                    == CRU_FAILURE)
                {

                    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    continue;
                }
                LdpIfMsg.u4MsgType = LDP_UDP6_EVENT;
                LdpIfMsg.u.UdpEvt.u4IfIndex = (UINT4)pIp6PktInfo->ipi6_ifindex;
		  MEMCPY(LdpIfMsg.u.UdpEvt.PeerAddr.u1_addr, PeerInfo.sin6_addr.s6_addr, LDP_IPV6ADR_LEN);		
                LdpIfMsg.u.UdpEvt.u4Evt = LDP_UDP6_DATA_EVENT;
		  LdpIfMsg.u.UdpEvt.u1DiscardV6LHello = u1DiscardV6UdpPkt;
                LdpIfMsg.u.UdpEvt.pPdu = (UINT1 *) pBuf;

                LdpProcessIpv6UdpEvents (&LdpIfMsg.u.UdpEvt);
            }
        }
        UNUSED_PARAM(u1DiscardV6UdpPkt);
#endif
        MemReleaseMemBlock (LDP_UDP_DATA_BUF_POOL_ID, (UINT1 *) pu1Data);
    }
    else
    {
        LDP_DBG (LDP_IF_MEM, "Memory Allocation for Receiving UDP Buffer\r\n");
    }

    if (SelAddFd (i4Udp6SockFd, LdpIpv6UdpPktInSocket) == OSIX_FAILURE)
    {
        LDP_DBG (LDP_IF_MISC, "LDPUDP: Can't do SelAddFd\n");
        close (i4Udp6SockFd);
        return;
    }
    return;
}


/************************************************************************/
/*  Function Name   : LdpIpv6UdpPktInSocket                               */
/*  Description     : CallBack Fn Registered with Select Task to get    */
/*                    Notification about Packet Arrival                 */
/*  Input(s)        : Socket Fd                                         */
/*  Output(s)       : None.                                             */
/*  Returns         : DHCP_SUCCESS / DHCP_FAILURE                       */
/************************************************************************/

VOID
LdpIpv6UdpPktInSocket(INT4 i4SockFd)
{
    UNUSED_PARAM (i4SockFd);
    if (LDP_INITIALISED != TRUE)
    {
        return;
    }
    if (OsixEvtSend (LDP_TSK_ID, LDP_UDP6_MSG_EVT) == OSIX_FAILURE)
    {
        return;
    }
}


/*****************************************************************************/
/* Function Name : LdpIpv6UdpClose                                               */
/* Description   : Function for closing the Udp Port.                        */
/* Input(s)      : u2Port - UDP port                                         */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpIpv6UdpClose (UINT2 u2Port)
{
    INT4                i4Udp6SockFd = LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
    UNUSED_PARAM (u2Port);
    SelRemoveFd (i4Udp6SockFd);
    if (close (i4Udp6SockFd) == 0)
    {
        return LDP_SUCCESS;
    }

    LDP_DBG (LDP_IF_UDP, "IF: Failed in closing socket \n");
    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpIPV6TcpClose                                               */
/* Description   : Function for closing a Tcp Connection.                    */
/*                                                                           */
/* Input(s)      : u4TcpConnId                                               */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/

UINT1
LdpIpv6TcpClose (UINT4 u4TcpConnId)
{
    LDP_DBG1 (LDP_IF_MISC,
              "IF: IPV6 TCP Connection of ConnId  %d closed \n", u4TcpConnId);
    return (SockClose (u4TcpConnId));
}


/*****************************************************************************/
/* Function Name : LdpUdpJoinV6Multicast                                       */
/* Description   : Make the specific interface join a multicast group        */
/* Input(s)      : u4SockDesc, u4LocalIfAddr, u4MulticastAddr                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpUdpJoinV6Multicast (INT4 i4UdpSockFd, tIp6Addr LocalIfAddr,
                     tIp6Addr V6MulticastAddr, UINT4 u4IfIndex)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq     Ip6Mreq;
    MEMSET (&Ip6Mreq, 0, sizeof (struct ipv6_mreq));
    MEMCPY(Ip6Mreq.ipv6mr_multiaddr.s6_addr, V6MulticastAddr.u1_addr, LDP_IPV6ADR_LEN);	
    Ip6Mreq.ipv6mr_interface = u4IfIndex;
    /* 
     * The Interface(specified by LocalIfAddr), joins the multicast group
     * specified by u4MulticastAddr.
     * This enables the interface, to receive a pkts with DestAddr of this 
     * multicast address(u4MulticastAddr).
     */
    if ((setsockopt (i4UdpSockFd, IPPROTO_IPV6, IP_ADD_MEMBERSHIP,
                     (VOID *) &Ip6Mreq, sizeof (struct ipv6_mreq))) < 0)
    {
        LDP_DBG1 (LDP_IF_UDP, "IF: %#x Fail to join IPV6 Multicast group \n",
                  LocalIfAddr.u1_addr);
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
#else
    UNUSED_PARAM (i4UdpSockFd);
    UNUSED_PARAM (LocalIfAddr);
    UNUSED_PARAM (V6MulticastAddr);
    UNUSED_PARAM(u4IfIndex);
    return LDP_SUCCESS;
#endif
}

/*****************************************************************************/
/* Function Name : LdpUdpLeaveV6Multicast                                       */
/* Description   : Make the specific interface leave a multicast group        */
/* Input(s)      : u4SockDesc, u4LocalIfAddr, u4MulticastAddr                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
LdpUdpLeaveV6Multicast (INT4 i4UdpSockFd, tIp6Addr LocalIfAddr,
                      tIp6Addr * pV6MulticastAddr, UINT4 u4IfIndex)
{
#ifdef BSDCOMP_SLI_WANTED
    struct ipv6_mreq     Ip6Mreq;
    MEMSET (&Ip6Mreq, 0, sizeof (struct ipv6_mreq));
    MEMCPY(Ip6Mreq.ipv6mr_multiaddr.s6_addr, pV6MulticastAddr->u1_addr, LDP_IPV6ADR_LEN);	
    /*Below API not working, Passing interface index from the callers*/	
    /*if (NetIpv6GetIfIndexFromAddr (LocalIfAddr, &u4IfIndex)
        == NETIPV4_FAILURE)
    {
        LDP_DBG1 (LDP_IF_UDP, "IF: %x Fail to get If for IPV6 Address \n",
                  LocalIfAddr.u1_addr);
        return LDP_FAILURE;
    }*/
    Ip6Mreq.ipv6mr_interface = (u4IfIndex);
    if ((setsockopt (i4UdpSockFd, IPPROTO_IPV6, IP_DROP_MEMBERSHIP,
                     (VOID *) &Ip6Mreq, sizeof (struct ipv6_mreq))) < 0)
    {
        LDP_DBG1 (LDP_IF_UDP, "IF: %#x Fail to dis join IPV6 Multicast group \n",
                  LocalIfAddr.u1_addr);
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
#else
    /* To Suppress warning */
    UNUSED_PARAM (i4UdpSockFd);
    UNUSED_PARAM (LocalIfAddr);
    UNUSED_PARAM (pV6MulticastAddr);
    UNUSED_PARAM(u4IfIndex);
#endif
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpIpv6UdpSend                                                */
/* Description   : Function for sending LDP packet to UDP.                   */
/* Input(s)      : u2SrcPort, V6DestAddr, u2DestPort, pBuf, u2BufLen,        */
/*                 V6IfAddr.                                                */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS                                               */
/*                 or LDP_FAILURE                                            */
/*****************************************************************************/
UINT1
LdpIpv6UdpSend (UINT2 u2SrcPort, tIp6Addr V6DestAddr, UINT2 u2DestPort,
            tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2BufLen, tIp6Addr V6IfAddr, UINT4 u4IfIndex)
{
    UINT1              *pu1Data = NULL;
    UNUSED_PARAM (u2SrcPort);

    pu1Data = MemAllocMemBlk (LDP_DATA_BUF_POOL_ID);

    if (pu1Data != NULL)
    {
        /*
         * A linear buffer is expected to be passed while writing to 
         *  socket. So copying the contents of pBuf to a linear buffer.
         */

        if ((CRU_BUF_Copy_FromBufChain (pBuf, pu1Data, 0, u2BufLen) ==
             u2BufLen))
        {
            if (LdpSendIpv6Message (pu1Data, u2BufLen, V6DestAddr, u2DestPort,
                                V6IfAddr, u4IfIndex) == LDP_FAILURE)
            {
                MemReleaseMemBlock (LDP_DATA_BUF_POOL_ID, (UINT1 *) pu1Data);
                return LDP_FAILURE;
            }
            if (LdpIpv6SendTo (pu1Data, u2BufLen, V6DestAddr, u2DestPort)
                == LDP_FAILURE)
            {
                MemReleaseMemBlock (LDP_DATA_BUF_POOL_ID, (UINT1 *) pu1Data);
                return LDP_FAILURE;
            }
            MemReleaseMemBlock (LDP_DATA_BUF_POOL_ID, (UINT1 *) pu1Data);
            CRU_BUF_Release_MsgBufChain (pBuf, 0);
            LDP_DBG (LDP_IF_MISC, "IF: Sent IPV6 Data on IPV6 UDP socket.\n");
            return LDP_SUCCESS;
        }
        LDP_DBG (LDP_IF_UDP, "IF: Failed to send IPV6 UDP packet \n");
        MemReleaseMemBlock (LDP_DATA_BUF_POOL_ID, (UINT1 *) pu1Data);
        return LDP_FAILURE;
    }

    LDP_DBG (LDP_IF_MEM,
             "LDPUDP: Memory Allocation for IPV6 UDP Sending Data Buffer\r\n");

    return LDP_FAILURE;
}

/*****************************************************************************/
/* Function Name : LdpSendIpv6Message                                            */
/* Description   : Function to set IP_INFO for UDP Send                      */
/* Input(s)      : pu1Data - Buffer to Send                                  */
/* Input(s)      : u2BufLen - Buffer Size                                    */
/*                 V6DestAddr - DestinationIP                                */
/*                 u2DestPort - Destination Port                             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
PRIVATE INT4
LdpSendIpv6Message (UINT1 *pu1Data, UINT2 u2BufLen,
                tIp6Addr V6DestAddr, UINT2 u2DestPort, tIp6Addr V6IfAddr, UINT4 u4IfIndex)
{
    struct msghdr       PktInfo;
    struct in6_pktinfo  *pIp6PktInfo;
    struct cmsghdr     *pCmsgInfo;
    UINT1               au1Cmsg[112];
    struct in6_addr      V6IfAddrInfo;
    struct sockaddr_in6  V6DestInfo;
    tIp6Addr             V6AllRouterAddr;
    INT4                i4Udp6SockFd = LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
    INT4                i4TtlVal = LDP_ZERO;
    INT4              i4McastHopLimit = LDP_V6LINK_HELLO_HLIMIT;
    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
    MEMSET(&V6AllRouterAddr, 0, sizeof(tIp6Addr));
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
    Iov.iov_base = pu1Data;
    Iov.iov_len = u2BufLen;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#else
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u2BufLen);
#endif
    if(LDP_FAILURE != INET_ATON6(LDP_ALL_ROUTERS_V6ADDR, & V6AllRouterAddr))
    {
        if (LDP_ZERO == MEMCMP(V6DestAddr.u1_addr, V6AllRouterAddr.u1_addr, LDP_IPV6ADR_LEN))
        {
            LDP_DBG (LDP_IF_MISC,
                     "IF: ALL ROUTER V6 ADDRESS MATCHED \n");
            MEMCPY(V6IfAddrInfo.s6_addr, V6IfAddr.u1_addr, LDP_IPV6ADR_LEN);		
            if ((setsockopt (i4Udp6SockFd, IPPROTO_IPV6, IPV6_MULTICAST_IF,
                         (INT1 *) &V6IfAddrInfo.s6_addr, sizeof (V6IfAddrInfo))) < 0)
            {
                LDP_DBG (LDP_IF_UDP,
                     "IF: Fail to specify IPV6 MCast pkt's OutIfIndex. \n");
                return LDP_FAILURE;
            }

            i4TtlVal = LDP_BASIC_HELLO_TTL;
            if ((setsockopt (i4Udp6SockFd, IPPROTO_IPV6, IP_TTL,
                         &i4TtlVal, sizeof (INT4))) < 0)
            {
                LDP_DBG (LDP_IF_UDP, "IF: Fail to set IPV6 TTL. \n");
                return LDP_FAILURE;
            }
            if ((setsockopt (i4Udp6SockFd, IPPROTO_IPV6,
               IPV6_MULTICAST_HOPS, &i4McastHopLimit, sizeof (INT4))) < 0)
            {
                LDP_DBG (LDP_IF_UDP, "IF: Fail to set IPV6 Multicast Hoplimit. \n");
                return LDP_FAILURE;
            }
        }
	 else
        {
            i4TtlVal = LDP_TARGET_HELLO_TTL;
            if ((setsockopt (i4Udp6SockFd, IPPROTO_IPV6, IP_TTL,
                     &i4TtlVal, sizeof (INT4))) < 0)
            {
                LDP_DBG (LDP_IF_UDP, "IF: Fail to set TTL. \n");
                return LDP_FAILURE;
            }
	     #ifdef BSDCOMP_SLI_WANTED
            /* No need to sendmsg for Multicast incase of 
            * Linux Socket but we have to set the interface index
            * in case of FS Socket other wise, packet will go with 
            * source address of Multicast enabled interface address
            */
            return LDP_SUCCESS;
            #endif
        }    		
    }
    else
    {
        LDP_DBG (LDP_IF_MISC,
                   "IPV6: INVALID IPV6 ADDRESS LDP_ALL_ROUTERS_V6ADDR\n");
	 return LDP_FAILURE;
    }
    


    MEMSET ((INT1 *) &V6DestInfo, 0, sizeof (struct sockaddr_in6));
    V6DestInfo.sin6_family = AF_INET6;
    MEMCPY(V6DestInfo.sin6_addr.s6_addr, V6DestAddr.u1_addr, LDP_IPV6ADR_LEN);	
    V6DestInfo.sin6_port = OSIX_HTONS (u2DestPort);

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));
    PktInfo.msg_name = (VOID *) &V6DestInfo;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in6);
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);
    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IPV6;
    pCmsgInfo->cmsg_type = IPV6_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIp6PktInfo =
        (struct in6_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

    pIp6PktInfo->ipi6_ifindex = LDP_IP_INVALID_INDEX;
    /* Below API not working, Passing if index from callers*/	
    /*if ((V6IfAddr.u4_addr[0] != LDP_IP_INVALID_INDEX) &&
        (NetIpv6GetIfIndexFromAddr (V6IfAddr,
                                    (UINT4 *) &pIp6PktInfo->
                                    ipi6_ifindex) == NETIPV4_FAILURE))
    {
        LDP_DBG (LDP_IF_UDP, "IF: Fail to get If Index from IPV6 Address\n");
        return LDP_FAILURE;
    }*/
    pIp6PktInfo->ipi6_ifindex = (int)u4IfIndex;
    #ifndef BSDCOMP_SLI_WANTED
    pIp6PktInfo->in6_hoplimit= IP6_MAX_HLIM;
    #endif
    LDP_DBG2 (LDP_IF_MISC, "IPV6: If Index updated to PktInfo %x For If Address %s\n",
		pIp6PktInfo->ipi6_ifindex, Ip6PrintAddr(&(V6IfAddr)));
    MEMCPY(pIp6PktInfo->ipi6_addr.s6_addr, V6IfAddr.u1_addr, LDP_IPV6ADR_LEN);
    if (sendmsg (i4Udp6SockFd, &PktInfo, 0) < 0)
    {
        LDP_DBG (LDP_IF_UDP, "IF: Fail to set IPV6 MCast pkt's OutIfIndex. \n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpIpv6SendTo                                                 */
/* Description   : Function Send Packet on UDP Port                          */
/* Input(s)      : pu1Data - Buffer to Send                                  */
/* Input(s)      : u2BufLen - Buffer Size                                    */
/*                 u4DestAddr - DestinationIP                                */
/*                 u2DestPort - Destination Port                             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
PRIVATE INT4
LdpIpv6SendTo (UINT1 *pu1Data, UINT2 u2BufLen, tIp6Addr V6DestAddr, UINT2 u2DestPort)
{
    struct sockaddr_in6  V6DestInfo;
    tIp6Addr    V6AllRouterAddr;
    INT4                i4Udp6SockFd = LDP_IPV6_UDP_SOCKFD (LDP_CUR_INCARN);
    MEMSET(&V6AllRouterAddr, 0, sizeof(tIp6Addr));
    #ifdef BSDCOMP_SLI_WANTED
    if(LDP_FAILURE != (INET_ATON6(LDP_ALL_ROUTERS_V6ADDR, &V6AllRouterAddr)))
    {
        if (LDP_ZERO == MEMCMP(V6AllRouterAddr.u1_addr, V6DestAddr.u1_addr, LDP_IPV6ADR_LEN))
        {
            /* No need to do sendto in case of multicast send in
            * Linux Socket but we have to call sendto in case of
            * FS Socket */
            return LDP_SUCCESS;
        }
    }
    else
    {
        LDP_DBG (LDP_IF_MISC, "IPV6: INVALID IPV6 LDP_ALL_ROUTERS_V6ADDR\n");
    }
    #endif

    MEMSET ((INT1 *) &V6DestInfo, 0, sizeof (struct sockaddr_in6));
    V6DestInfo.sin6_family = AF_INET6;
    MEMCPY(V6DestInfo.sin6_addr.s6_addr, V6DestAddr.u1_addr, LDP_IPV6ADR_LEN);
    /*V6_HTONL((tIpAddr *) & V6DestInfo.sin6_addr, (tIpAddr *)  & V6DestAddr);*/
    V6DestInfo.sin6_port = OSIX_HTONS (u2DestPort);

    if (sendto (i4Udp6SockFd, (INT1 *) pu1Data, (UINT4) u2BufLen, 0,
                (struct sockaddr *) &V6DestInfo, sizeof (struct sockaddr)) < 0)
    {

        LDP_DBG1 (LDP_IF_MISC, "IF: Sendto Failed to send IPV6 Data to IPV6 Dest Addr.%s\n",
			Ip6PrintAddr((tIp6Addr *) (VOID *)&(V6DestInfo.sin6_addr)));
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : Sock6Connect                                               */
/* Description   : This routine issues a connect call for the given socket   */
/* Input(s)      : pLdpTcpUdpSockInfo - TCP Socket Info                      */
/* Output(s)     : None                                                      */
/* Return(s)     : CONNECT_SUCCESS, CONNECT_FAIL, CONNECT_INPROGRESS         */
/*****************************************************************************/
UINT1
Sock6Connect (tLdpTcpUdpSockInfo * pLdpTcpUdpSockInfo)
{

    struct sockaddr_in6		LdpTcp6ServAddr;
    MEMSET(&LdpTcp6ServAddr, 0, sizeof(struct sockaddr_in6));
    if (fcntl (pLdpTcpUdpSockInfo->i4SockDesc, F_SETFL, O_NONBLOCK) < 0)
    {
        LDP_SOCK_EVENT_INDICATE (TCP_CLI_OPEN_FAILURE, pLdpTcpUdpSockInfo);
	 LDP_DBG (LDP_IF_MISC, "Sock6Connect Can't Set Sckt Opt to NonBlk\n");
        return CONNECT_FAIL;
    }
    MEMCPY(LdpTcp6ServAddr.sin6_addr.s6_addr, LDP_IPV6_ADDR(pLdpTcpUdpSockInfo->DestAddr), LDP_IPV6ADR_LEN);
    LdpTcp6ServAddr.sin6_family = AF_INET6;
    LdpTcp6ServAddr.sin6_port = OSIX_HTONS (pLdpTcpUdpSockInfo->u2Port);
    if (connect
        (pLdpTcpUdpSockInfo->i4SockDesc, (struct sockaddr *) &LdpTcp6ServAddr,
         sizeof (LdpTcp6ServAddr)) < 0)
    {
        if (errno == EINPROGRESS || errno == EALREADY)
        {
            if(errno == EINPROGRESS)
            {
	         LDP_DBG (LDP_IF_MISC, "Sock6Connect EINPROGRESS\n");
            }
	     else
	     {
	         LDP_DBG (LDP_IF_MISC, "Sock6Connect Ealready\n");
	     }
            LDP_DBG (LDP_IF_MISC, "LDPTCP : Connection in Progress\n");
            return CONNECT_INPROGRESS;
        }
        if (errno == EISCONN)
        {
            LDP_DBG (LDP_IF_MISC, "LDPTCP : Socket already connected \n");
            return CONNECT_SUCCESS;
        }
        LDP_DBG (LDP_IF_MISC, "LDPTCP : Connection Fail \n");
        return CONNECT_FAIL;
    }
    LDP_DBG (LDP_IF_MISC, "LDPTCP : Connection is Success\n");
    return CONNECT_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpTcp6PktInSocket                                         */
/* Description   : This function processes select TCP read event             */
/* Input(s)      : i4SockFd - Socket to be Processed                         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTcp6PktInSocket (INT4 i4SockFd)
{
    if (LdpTcpEventSend (i4SockFd, LDP_V6READ_FD) == LDP_FAILURE)
    {
        if (SelAddFd (i4SockFd, LdpTcp6PktInSocket) == OSIX_FAILURE)
        {
            LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Failed to process in  sock to select FD List\n");
        }
    }
}

/*****************************************************************************/
/* Function Name : LdpTcp6PktOutSocket                                        */
/* Description   : This function processes select TCP write event            */
/* Input(s)      : i4SockFd - Socket to be Processed                         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTcp6PktOutSocket (INT4 i4SockFd)
{
    if (LdpTcpEventSend (i4SockFd, LDP_V6WRITE_FD) == LDP_FAILURE)
    {
        if (SelAddWrFd (i4SockFd, LdpTcp6PktOutSocket) == OSIX_FAILURE)
        {
            LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Failed to process out V6 sock to select FD List\n");
        }
    }
}

/*****************************************************************************/
/* Function Name : LdpTcp6ProcRecvReqEvent                                    */
/* Description   : Function for handling socket when signal received event is*/
/*                 processed.                                                */
/* Input(s)      : aTaskEnqMsgInfo - Msg Info                                */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
LdpTcp6ProcRecvReqEvent (tLdpTcpUdpEnqMsgInfo * pLdpTcpUdpEnqMsgInfo)
{
    INT4                i4NewSockDesc;
    INT4                i4LdpTcpCliLen;
	INT4                i4LdpTcpSrvLen = 0;

#ifndef BSDCOMP_SLI_WANTED
    UINT1               u1RetVal;
#endif

	tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    struct sockaddr_in6    LdpTcp6CliAddr;
    struct sockaddr_in6    LdpTcp6SrvAddr;

    UINT4               u4SndBuf = LDP_MAX_FRAGMENT;
    UINT4               u4RcvBuf = LDP_MAX_FRAGMENT;
    tIp6Addr            TempV6CliAddr;
    tIp6Addr            TempV6SrvAddr;
    pLdpTcpUdpSockInfo = LdpTcpUdpGetSocket (LDP_SOCK_MASK,
                                             pLdpTcpUdpEnqMsgInfo->
                                             i4SockFdOrConnId);


    MEMSET (&LdpTcp6SrvAddr, 0, sizeof (struct sockaddr_in6));
    MEMSET(&TempV6CliAddr, 0, sizeof(tIp6Addr));
    MEMSET(&TempV6SrvAddr, 0, sizeof(tIp6Addr));
    MEMSET (&LdpTcp6CliAddr, 0, sizeof (struct sockaddr_in6));
    LDP_DBG (LDP_SSN_PRCS, "LDPTCP6PrcoRecvReqEvent  Entered \n");
    if (pLdpTcpUdpSockInfo == NULL)
    {
        LDP_DBG (LDP_IF_MISC, "LDPTCP : Unable to Get V6 Sock Info \n");
        return;
    }
    /* Check, if any Msg is there to be read on this socket */
    if (pLdpTcpUdpEnqMsgInfo->u4MsgType == LDP_V6READ_FD)
    {

        switch (pLdpTcpUdpSockInfo->u1SockStatus)
        {
            case SOCK_SERVER:
		    LDP_DBG (LDP_SSN_PRCS, "LDPTCP6PrcoRecvReqEvent : V6Read_Fd SockServer Accept\n");
                i4LdpTcpCliLen = sizeof (LdpTcp6CliAddr);
                i4NewSockDesc
                    = accept (pLdpTcpUdpEnqMsgInfo->i4SockFdOrConnId,
                              (struct sockaddr *)
                              &LdpTcp6CliAddr, (INT4 *) &i4LdpTcpCliLen);
                if (i4NewSockDesc < 0)
                {
                    LDP_DBG1 (LDP_IF_MISC,
                             "LDPTCP: Error: V6 Accept Failed With Dest Addr %s \n",
                             Ip6PrintAddr((tIp6Addr *) (VOID *)&(LdpTcp6CliAddr.sin6_addr)));
                    break;
                }
		  MEMCPY(TempV6CliAddr.u1_addr, LdpTcp6CliAddr.sin6_addr.s6_addr, LDP_IPV6ADR_LEN);
                if (fcntl (i4NewSockDesc, F_SETFL, O_NONBLOCK) < 0)
                {
                    close (i4NewSockDesc);
                    LDP_DBG (LDP_IF_MISC,
                             "LDPTCP: Non Blk Set fail for New Sckt\n");
                    break;
                }

                if (setsockopt (i4NewSockDesc, SOL_SOCKET, SO_SNDBUF,
                                (VOID *) &u4SndBuf, sizeof (UINT4)) < 0)
                {
                    LDP_DBG2 (LDP_IF_MEM,
                              "LDPTCP: IPV6 :Server Accept - "
                              "SetSockOpt failed for Send Buffer %d for %d\r\n",
                              u4SndBuf, i4NewSockDesc);
                }

                if (setsockopt (i4NewSockDesc, SOL_SOCKET, SO_RCVBUF,
                                (VOID *) &u4RcvBuf, sizeof (UINT4)) < 0)
                {
                    LDP_DBG2 (LDP_IF_MEM,
                              "LDPTCP: Server Accept - "
                              "SetSockOpt failed for Recv Buffer %d for %d\r\n",
                              u4RcvBuf, i4NewSockDesc);
                }
                i4LdpTcpSrvLen = sizeof (LdpTcp6SrvAddr);
                if ((getsockname(i4NewSockDesc,
				            (struct sockaddr *) &LdpTcp6SrvAddr, &i4LdpTcpSrvLen)) < 0)
                {
				     LDP_DBG (LDP_IF_MISC,
					              "LDPTCP: Some error during getsockname,"
					                  " ignore it\n");
                     break;
				}
		  MEMCPY(TempV6SrvAddr.u1_addr, LdpTcp6SrvAddr.sin6_addr.s6_addr, LDP_IPV6ADR_LEN);

                LdpTcp6ProcConnIndEvent (i4NewSockDesc,
                                        TempV6CliAddr,TempV6SrvAddr,
                                        LdpTcp6CliAddr.sin6_port, TCP_SERV_OPEN);
                break;

            case SOCK_ESTABLISHED:
                SockRead (pLdpTcpUdpSockInfo);
                break;

            default:
                /* Ignore event */
                break;
        }                        /* switch */
    }                            /* read set socket */

    /* Check, if any Msg is there to be Written to this socket */
    if (pLdpTcpUdpEnqMsgInfo->u4MsgType == LDP_V6WRITE_FD)
    {
        switch (pLdpTcpUdpSockInfo->u1SockStatus)
        {
            case SOCK_CONNECTING:
#ifndef BSDCOMP_SLI_WANTED
                u1RetVal = Sock6Connect (pLdpTcpUdpSockInfo);
                switch (u1RetVal)
                {
                    case CONNECT_SUCCESS:
                        if (setsockopt (pLdpTcpUdpSockInfo->i4SockDesc,
                                        SOL_SOCKET, SO_SNDBUF,
                                        (VOID *) &u4SndBuf, sizeof (UINT4)) < 0)
                        {
                            /* MPLS_IPv6 mod start*/
                            LDP_DBG3 (LDP_IF_MEM,
                                      "LDPTCP: Client Connect - "
                                      "SetSockOpt failed for Send Buffer %d "
                                      "for %d %s\r\n",
                                      u4SndBuf, pLdpTcpUdpSockInfo->i4SockDesc,
                                      Ip6PrintAddr(&(pLdpTcpUdpSockInfo->DestAddr.Ip6Addr)));
			       /* MPLS_IPv6 mod end*/				
                        }

                        if (setsockopt (pLdpTcpUdpSockInfo->i4SockDesc,
                                        SOL_SOCKET, SO_RCVBUF,
                                        (VOID *) &u4RcvBuf, sizeof (UINT4)) < 0)
                        {
                            LDP_DBG3 (LDP_IF_MEM,
                                      "LDPTCP: Client Accept - "
                                      "SetSockOpt failed for Recv Buffer %d "
                                      "for %d %s\r\n",
                                      u4RcvBuf, pLdpTcpUdpSockInfo->i4SockDesc,
                                      Ip6PrintAddr(&(pLdpTcpUdpSockInfo->DestAddr.Ip6Addr)));
                        }

                        if (LdpTcpEventHandler (LDP_TCP_CONN_UP_EVENT,
                                                CONTROL_DATA,
                                                pLdpTcpUdpSockInfo->
                                                u4ConnId, NULL) == LDP_FAILURE)
                        {
                            LDP_DBG (LDP_IF_MISC,
                                     "LDPTCP: Failed to Indicate "
                                     "TCP Conn up to LDP\n");
                            /*
                             * NOTE: If LDP_TCP_CONN_UP_EVENT indication 
                             * fails,Socket is closed.  LDP might infinitely
                             * wait for socket conn. up event.
                             * However this condition will not occur.
                             */
                            LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                            LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
                        }
                        else
                        {
                            TmrStopTimer (LDP_TIMER_LIST_ID,
                                          (tTmrAppTimer *) &
                                          (pLdpTcpUdpSockInfo->SockReadTimer.
                                           AppTimer));
                            pLdpTcpUdpSockInfo->u1SockStatus = SOCK_ESTABLISHED;
                            pLdpTcpUdpSockInfo->b1IsSockReadFdReqd = TRUE;

                            LdpStoreTcpSourcePort (pLdpTcpUdpSockInfo);
                        }
                        break;
                    case CONNECT_INPROGRESS:
                        /* Timer may have started already. 
                         * Possibilites are there. So stop and Start the
                         * Timer*/
                        TmrStopTimer (LDP_TIMER_LIST_ID,
                                      (tTmrAppTimer *) & (pLdpTcpUdpSockInfo->
                                                          SockReadTimer.
                                                          AppTimer));
                        pLdpTcpUdpSockInfo->u1SockStatus = SOCK_CONNECTING;
                        pLdpTcpUdpSockInfo->SockReadTimer.u4Event =
                            LDP_TCP_RECONNECT_TMR_EVT;
                        pLdpTcpUdpSockInfo->SockReadTimer.pu1EventInfo =
                            (UINT1 *) pLdpTcpUdpSockInfo;
                        TmrStartTimer (LDP_TIMER_LIST_ID,
                                       (tTmrAppTimer *) & (pLdpTcpUdpSockInfo->
                                                           SockReadTimer.
                                                           AppTimer),
                                       (LDP_TCP_RECONNECT_TIME *
                                        SYS_NUM_OF_TIME_UNITS_IN_A_SEC));
                        SelAddWrFd (pLdpTcpUdpSockInfo->i4SockDesc,
                                    LdpTcp6PktOutSocket);
                        break;

                    case CONNECT_FAIL:
                        /* Timer may have started already. 
                         * Possibilites are there. So stop and Start the
                         * Timer*/
                        TmrStopTimer (LDP_TIMER_LIST_ID,
                                      (tTmrAppTimer *) & (pLdpTcpUdpSockInfo->
                                                          SockReadTimer.
                                                          AppTimer));
                        if (LdpTcpEventHandler
                            (LDP_TCP_CONN_DOWN_EVENT, CONTROL_DATA,
                             pLdpTcpUdpSockInfo->u4ConnId, NULL) == LDP_FAILURE)
                        {
                            LDP_DBG (LDP_IF_MISC,
                                     "LDPTCP: Failed to Indicate TCP "
                                     "Conn up to LDP\n");
                        }
                        LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                        LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
                        break;
                }
#else
                if (LdpTcpEventHandler (LDP_TCP_CONN_UP_EVENT,
                                        CONTROL_DATA,
                                        pLdpTcpUdpSockInfo->
                                        u4ConnId, NULL) == LDP_FAILURE)
                {
                    LDP_DBG (LDP_IF_MISC,
                             "LDPTCP: Failed to Indicate "
                             "TCP Conn up to LDP\n");
                    /*
                     * NOTE: If LDP_TCP_CONN_UP_EVENT indication 
                     * fails,Socket is closed.  LDP might infinitely
                     * wait for socket conn. up event.
                     * However this condition will not occur.
                     */
                    LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
                    LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
                }
                else
                {
                    pLdpTcpUdpSockInfo->u1SockStatus = SOCK_ESTABLISHED;
                    TmrStopTimer (LDP_TIMER_LIST_ID,
                                  (tTmrAppTimer *) & (pLdpTcpUdpSockInfo->
                                                      SockReadTimer.AppTimer));
                    LdpStoreTcpSourcePort (pLdpTcpUdpSockInfo);
                }
#endif
                break;
            case SOCK_ESTABLISHED:
                if (SockSendPendingData (pLdpTcpUdpSockInfo) == LDP_FAILURE)
                {
                    break;
                }

                break;
            default:
                /* Ignore Event */
                break;
        }
    }

    if ((pLdpTcpUdpSockInfo->u1SockStatus == SOCK_SERVER) ||
        ((pLdpTcpUdpSockInfo->u1SockStatus == SOCK_ESTABLISHED) &&
         (pLdpTcpUdpSockInfo->b1IsSockReadFdReqd == TRUE)))
    {
        if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc,
                      LdpTcp6PktInSocket) == OSIX_FAILURE)
        {
            LDP_DBG (LDP_IF_MISC,
                     "LDPTCP: Failed to add sock to select FD List\n");
            LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
            LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
        }
    }

}
/*****************************************************************************/
/* Function Name : LdpTcp6ProcConnIndEvent                                    */
/* Description   : This routine is a used to indicate the new connection.    */
/* Input(s)      : i4SockDesc  - Socket Descriptor                           */
/*                 DestAddr  - Destination Address in Network order        */
/*                 u2Port      - Destination TCP Port                        */
/*                 u1SockType - Inidcates UDP or TCP related socket.         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpTcp6ProcConnIndEvent (INT4 i4SockDesc, tIp6Addr DestAddr, tIp6Addr SrcAddr,
                        UINT2 u2Port, UINT1 u1SockType)
{
    UINT4               u4ConnId = LDP_ZERO;
    tLdpTcpUdpSockInfo *pLdpTcpUdpSockInfo = NULL;
    tLdpSession        *pSessionEntry = NULL;
    tLdpPeer           *pPeer = NULL;
    UINT2               u2IncarnId = MPLS_DEF_INCARN;
    UINT1               u1PeerIndex = LDP_ZERO;
    tIp6Addr            Ipv6SrcAddr;
    UINT4               u4IfIndex=0;
    UINT1       u1Scope = 0;
    INT4        i4retval=0;
    tLdpAdjacency       *pTempAdjEntry = NULL;

    MEMSET(&Ipv6SrcAddr,LDP_ZERO,sizeof(tIp6Addr));

    /* 
     * It is assumed that when the SLI_ACCEPT is successful, atleast one
     * free socket information structure is available.
     */
    u4ConnId = LdpTcpUdpGetFreeSocket (&pLdpTcpUdpSockInfo);

    if (u4ConnId == 0)
    {
        return;
    }

    /* Connection information copied */
    pLdpTcpUdpSockInfo->i4SockDesc = i4SockDesc;
    pLdpTcpUdpSockInfo->u1AddrType = LDP_ADDR_TYPE_IPV6;
    MEMCPY(LDP_IPV6_ADDR(pLdpTcpUdpSockInfo->DestAddr), DestAddr.u1_addr, LDP_IPV6ADR_LEN);
    pLdpTcpUdpSockInfo->u2Port = u2Port;
    pLdpTcpUdpSockInfo->u4ConnId = u4ConnId;
    pLdpTcpUdpSockInfo->u1SockType = u1SockType;
    pLdpTcpUdpSockInfo->u1SockStatus = SOCK_ESTABLISHED;
    pLdpTcpUdpSockInfo->b1IsSockReadFdReqd = TRUE;

    u1Scope=Ip6GetAddrScope(&pLdpTcpUdpSockInfo->DestAddr.Ip6Addr); 
	if (ADDR6_SCOPE_LLOCAL != u1Scope)
	{
       
	    NetIpv6GetSrcAddrForDestAddr(LDP_ZERO, &pLdpTcpUdpSockInfo->DestAddr.Ip6Addr,
		                             &Ipv6SrcAddr);
		i4retval = NetIpv6IsOurAddress(&Ipv6SrcAddr, &u4IfIndex);
        if(NETIPV6_FAILURE == i4retval)
        {
            LDP_DBG (LDP_IF_MISC, "NETIPV6_FAILURE retuned by function NetIpv6IsOurAddress \n");
        }

		pLdpTcpUdpSockInfo->u4IfIndex = u4IfIndex;
    }
    else
   {
        i4retval = NetIpv6IsOurAddress(&SrcAddr, &u4IfIndex);   
        if(NETIPV6_FAILURE == i4retval)
        {
            LDP_DBG (LDP_IF_MISC, "NETIPV6_FAILURE retuned by function NetIpv6IsOurAddress \n");
        }

		pLdpTcpUdpSockInfo->u4IfIndex = u4IfIndex;
   }
    for (u1PeerIndex = 0; u1PeerIndex < LDP_MAX_PASSIVE_PEERS; u1PeerIndex++)
    {
        pPeer = LDP_INCARN_PPEER_ARR (u2IncarnId, u1PeerIndex);
        if ((pPeer == NULL) || (pPeer->pLdpSession == NULL))
        {
            continue;
        }
        pSessionEntry = (tLdpSession *) pPeer->pLdpSession;

        /* This is the case the session already has a valid connection Id.
         * In that case, we must delete the session entry from the TCP connection
         * hash table */
        if (pSessionEntry->u4TcpConnId != LDP_INVALID_CONN_ID)
        {
            TMO_SLL_Scan (&pSessionEntry->AdjacencyList, pTempAdjEntry, tLdpAdjacency *)
            {
                LdpHandleAdjExpiry (pTempAdjEntry, LDP_STAT_TYPE_HOLD_TMR_EXPRD);
            }
        }

        if (LDP_ZERO == (MEMCMP(LDP_IPV6_ADDR(pPeer->TransAddr.Addr), 
			LDP_IPV6_ADDR(pLdpTcpUdpSockInfo->DestAddr), LDP_IPV6ADR_LEN)))
        {
            /* set session state to intitialised */
	    LDP_DBG1 (LDP_SSN_MISC, "Session State Marked Initialized For Peer Address %s\n", Ip6PrintAddr(&(pPeer->TransAddr.Addr.Ip6Addr)));
            pSessionEntry->u1SessionState = LDP_SSM_ST_INITIALIZED;
        }
    }
    if (SelAddFd (pLdpTcpUdpSockInfo->i4SockDesc, LdpTcp6PktInSocket)
        == OSIX_FAILURE)
    {
        LdpCheckAndCloseTcpSocket (pLdpTcpUdpSockInfo);
        LdpCheckAndClearSocketInfo (pLdpTcpUdpSockInfo);
        return;
    }

    /* Number of connections opened is incremented */
    gu4CurSocketConns++;
}

#endif
/* MPLS_IPv6 add end*/
/*---------------------------------------------------------------------------*/
/*                       End of file ldpfssli.c                              */
/*---------------------------------------------------------------------------*/
