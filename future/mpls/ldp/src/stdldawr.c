# include  "lr.h"
# include  "fssnmp.h"
# include  "stdldalw.h"
# include  "stdldawr.h"
# include  "stdldadb.h"
# include  "ldpincs.h"

VOID
RegisterSTDLDA ()
{
    SNMPRegisterMibWithLock (&stdldaOID, &stdldaEntry, LdpLock, LdpUnLock,
                             SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&stdldaOID, (const UINT1 *) "stdldatm");
}

VOID
UnRegisterSTDLDA ()
{
    SNMPUnRegisterMib (&stdldaOID, &stdldaEntry);
    SNMPDelSysorEntry (&stdldaOID, (const UINT1 *) "stdldatm");
}

INT4
GetNextIndexMplsLdpEntityAtmTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpEntityAtmTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpEntityAtmTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpEntityAtmIfIndexOrZeroGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmIfIndexOrZero
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmMergeCapGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmMergeCap
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmLRComponentsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmLRComponents
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsLdpEntityAtmVcDirectionalityGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmVcDirectionality
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmLsrConnectivityGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmLsrConnectivity
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmDefaultControlVpiGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmDefaultControlVpi
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmDefaultControlVciGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmDefaultControlVci
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmUnlabTrafVpiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmUnlabTrafVpi
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmUnlabTrafVciGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmUnlabTrafVci
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmIfIndexOrZeroSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmIfIndexOrZero
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmMergeCapSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmMergeCap
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmVcDirectionalitySet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmVcDirectionality
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmLsrConnectivitySet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmLsrConnectivity
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmDefaultControlVpiSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmDefaultControlVpi
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmDefaultControlVciSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmDefaultControlVci
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmUnlabTrafVpiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmUnlabTrafVpi
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmUnlabTrafVciSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmUnlabTrafVci
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmIfIndexOrZeroTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmIfIndexOrZero (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmMergeCapTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmMergeCap (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmVcDirectionalityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmVcDirectionality (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       pOctetStrValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       i4_SLongValue));

}

INT4
MplsLdpEntityAtmLsrConnectivityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmLsrConnectivity (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
MplsLdpEntityAtmDefaultControlVpiTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmDefaultControlVpi (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        pOctetStrValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
MplsLdpEntityAtmDefaultControlVciTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmDefaultControlVci (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        pOctetStrValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        i4_SLongValue));

}

INT4
MplsLdpEntityAtmUnlabTrafVpiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmUnlabTrafVpi (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmUnlabTrafVciTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmUnlabTrafVci (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmStorageType (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsLdpEntityAtmTable (pu4Error, pSnmpIndexList,
                                           pSnmpvarbinds));
}

INT4
GetNextIndexMplsLdpEntityAtmLRTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpEntityAtmLRTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpEntityAtmLRTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].i4_SLongValue,
             &(pNextMultiIndex->pIndex[2].i4_SLongValue),
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpEntityAtmLRMaxVpiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmLRTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmLRMaxVpi
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmLRMaxVciGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmLRTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmLRMaxVci
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmLRStorageTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmLRTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmLRStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmLRRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpEntityAtmLRTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].i4_SLongValue,
         pMultiIndex->pIndex[3].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpEntityAtmLRRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpEntityAtmLRMaxVpiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmLRMaxVpi
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmLRMaxVciSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmLRMaxVci
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmLRStorageTypeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmLRStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmLRRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLdpEntityAtmLRRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].i4_SLongValue,
             pMultiIndex->pIndex[3].i4_SLongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmLRMaxVpiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmLRMaxVpi (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[3].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmLRMaxVciTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmLRMaxVci (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[3].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmLRStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmLRStorageType (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[3].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmLRRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsLdpEntityAtmLRRowStatus (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  i4_SLongValue,
                                                  pMultiIndex->pIndex[3].
                                                  i4_SLongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
MplsLdpEntityAtmLRTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsLdpEntityAtmLRTable (pu4Error, pSnmpIndexList,
                                             pSnmpvarbinds));
}

INT4
GetNextIndexMplsLdpAtmSessionTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLdpAtmSessionTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLdpAtmSessionTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue,
             pFirstMultiIndex->pIndex[3].i4_SLongValue,
             &(pNextMultiIndex->pIndex[3].i4_SLongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLdpSessionAtmLRUpperBoundVpiGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpAtmSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionAtmLRUpperBoundVpi
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLdpSessionAtmLRUpperBoundVciGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLdpAtmSessionTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue,
         pMultiIndex->pIndex[3].i4_SLongValue,
         pMultiIndex->pIndex[4].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLdpSessionAtmLRUpperBoundVci
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].pOctetStrValue,
             pMultiIndex->pIndex[3].i4_SLongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             &(pMultiData->i4_SLongValue)));

}
