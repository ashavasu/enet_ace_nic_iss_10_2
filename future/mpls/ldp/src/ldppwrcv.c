
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: ldppwrcv.c,v 1.20 2016/02/28 10:58:23 siva Exp $
 *              
 ********************************************************************/

#include "ldpincs.h"

/*****************************************************************************/
/* Function Name : ldpProcessL2VPNEvents                                     */
/* Description   : This routine processes events received from L2VPN         */
/* Input(s)      : pL2vpnAppEvtInfo- info about events and data received from*/
/*                 L2VPN                                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwrcv.c */
UINT1
LdpProcessL2vpnEvents (tL2VpnLdpPwVcEvtInfo * pL2vpnAppEvtInfo)
{
    UINT1               u1RetStatus = LDP_SUCCESS;
    LDP_DBG1 (LDP_MAIN_MISC,
              "MAIN: ENTRY. pL2vpnAppEvtInfo{.u4EvtType(%#X)}\n",
              pL2vpnAppEvtInfo->u4EvtType);

    switch (pL2vpnAppEvtInfo->u4EvtType)
    {
        case L2VPN_LDP_SSN_CREATE_EVT:
            u1RetStatus =
                LdpProcessTgtSsnReqEvent (&pL2vpnAppEvtInfo->unEvtInfo.SsnInfo);
            break;

        case L2VPN_LDP_DEREG_WITH_LDP:
            u1RetStatus =
                LdpProcessTgtEntityDeRegReqEvent (&pL2vpnAppEvtInfo->unEvtInfo.
                                                  SsnInfo);
            break;

        case L2VPN_LDP_SSN_REL_EVT:
            u1RetStatus = LdpProcessTgtSsnReleaseEvent
                (&(pL2vpnAppEvtInfo->unEvtInfo.SsnInfo));
            break;

        case L2VPN_LDP_LBL_MSG_EVT:
            u1RetStatus =
                LdpProcessPwVcLblMsgEvent (&pL2vpnAppEvtInfo->unEvtInfo.
                                           LblInfo);
            if (pL2vpnAppEvtInfo->unEvtInfo.LblInfo.pi1LocalIfString != NULL)
            {
                /* Releasing memory allocated for inteface descriptor */
                MemReleaseMemBlock (LDP_L2VPN_IF_STR_POOL_ID,
                                    (UINT1 *) pL2vpnAppEvtInfo->
                                    unEvtInfo.LblInfo.pi1LocalIfString);
                pL2vpnAppEvtInfo->unEvtInfo.LblInfo.pi1LocalIfString = NULL;
            }
            break;

        case L2VPN_LDP_NOTIF_EVT:
            u1RetStatus = LdpProcessPwVcNotifMsgEvent
                (&pL2vpnAppEvtInfo->unEvtInfo.NotifInfo);
            break;

        case L2VPN_LDP_ADDR_WDRAW_EVT:
            u1RetStatus = LdpProcessPwVcAddrWdrawMsgEvent
                (&pL2vpnAppEvtInfo->unEvtInfo.NotifInfo);
            break;

        case L2VPN_LDP_ICCP_RG_EVT:
            u1RetStatus =
                LdpProcessPwVcIccpMsgEvent (&pL2vpnAppEvtInfo->unEvtInfo.
                                            IccpInfo);
            break;
#ifdef LDP_GR_WANTED
		case L2VPN_LDP_READY_EVENT:
				LdpReserveLabelToLabelManager();
				/* Update u1GrProgressStatus, to initiate creation of LDP 
 				 * Session because processing of hello message is blocked
 				 * if u1GrProgressStatus = LDP_GR_SHUTDOWN_IN_PROGRESS*/
				gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus =
                        LDP_GR_RECONNECT_IN_PROGRESS;
			break;

#endif
        default:
            LDP_DBG (LDP_MAIN_MISC, "MAIN: Rcvd Invalid L2VPN Event\n");
            u1RetStatus = LDP_FAILURE;
    }

    LDP_DBG1 (LDP_MAIN_MISC, "MAIN: EXIT 1. u1RetStatus(%u)\n", u1RetStatus);
    return u1RetStatus;
}

/*****************************************************************************/
/* Function Name : LdpProcessTgtSsnReqEvent                                  */
/* Description   : This routine processes Targeted Session Create Request    */
/*                 from the L2VPN and Post Session Up notification to L2VPN  */
/*                 Once the session is Up                                    */
/* Input(s)      : pSsnEvtInfo- Pointer to info containing Entity Id and Peer*/
/*                 Address to create the session                             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwrcv.c */
UINT1
LdpProcessTgtSsnReqEvent (tPwVcSsnEvtInfo * pSsnEvtInfo)
{

    /* Check whether there is a targetted session with the peer */
    /* Get the Ldp Entity corresponding to the incoming
     * values */
    UINT4               u2IncarnId = LDP_CUR_INCARN;
    tLdpEntity         *pLdpEntity = NULL;
    tLdpSession        *pPeerSession = NULL;
    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;

#ifdef MPLS_IPV6_WANTED
    if(pSsnEvtInfo->u2AddrType==LDP_ADDR_TYPE_IPV6)
    {
	    LDP_DBG2 (LDP_MAIN_MISC, "MAIN: ENTRY. pSsnEvtInfo.u4PeerAddr(%s), .(%u)\n",
			    Ip6PrintAddr(&pSsnEvtInfo->PeerAddr.Ip6Addr), pSsnEvtInfo->u4LocalLdpEntityID);
    }
    else
#endif
    {
	    LDP_DBG2 (LDP_MAIN_MISC, "MAIN: ENTRY. pSsnEvtInfo.u4PeerAddr(%X), .(%u)\n",
			    LDP_IPV4_U4_ADDR(pSsnEvtInfo->PeerAddr), pSsnEvtInfo->u4LocalLdpEntityID);
    }

    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    if (LdpGetSessionWithPeer (u2IncarnId,
                               pSsnEvtInfo->u4LocalLdpEntityID,
                               &pSsnEvtInfo->PeerAddr,
                               pSsnEvtInfo->u2AddrType,
                               LDP_DSTR_UNSOLICIT, LDP_TRUE,
                               &pPeerSession) == LDP_FAILURE)
    {
        /* send ssn down notification to l2vpn with error code as
         * entity not configured */
        LDP_DBG (LDP_MAIN_MISC, "MAIN: INTMD-EXIT 1. Entity not configured\n");
        return LDP_FAILURE;
    }
    if ((pPeerSession->pLdpPeer == NULL) ||
        (pPeerSession->pLdpPeer->pLdpEntity == NULL))
    {
        LDP_DBG (LDP_PRCS_PRCS, "MAIN: INTMD-EXIT 2. LDP Peer is NULL\n");
        return (LDP_FAILURE);
    }

    pLdpEntity = pPeerSession->pLdpPeer->pLdpEntity;
    /* We Found a LDP Entity. Set APP_L2VPN_MASK bit here so that indications
     * can be given to L2VPN when LDP Entity goes down/up. */
    pLdpEntity->u4AppMask |= LDP_APP_L2VPN_MASK;

    if (LDP_ENTITY_ROW_STATUS (pLdpEntity) != ACTIVE)
    {
        LDP_DBG1 (LDP_MAIN_MISC, "MAIN: INTMD-EXIT 3. Entity %d not active\n",
                  pLdpEntity->u4EntityIndex);
        return LDP_FAILURE;
    }

    /* session state is operational */
    LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = L2VPN_LDP_PWVC_SSN_UP;
    if (pPeerSession->pLdpPeer->u1GrProgressState ==
        LDP_PEER_GR_RECOVERY_IN_PROGRESS)
    {
        LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = L2VPN_LDP_GR_PWVC_SSN_UP;
    }
    if (pPeerSession->pLdpPeer == NULL)
    {
        LDP_DBG (LDP_PRCS_PRCS, "MAIN: INTMD-EXIT 4. LDP Peer is NULL\n");
	return (LDP_FAILURE);
    }

    LdpCopyAAddr(&(LDP_L2VPN_EVT_SSN_PEER_ADDR (&PwVcEvtInfo)),
		    &(LDP_PEER_TRANS_ADDR (pPeerSession->pLdpPeer).Addr),
		    LDP_PEER_TRANS_ADDR (pPeerSession->pLdpPeer).u2AddrType);

    LDP_L2VPN_EVT_SSN_ADDR_TYPE(&PwVcEvtInfo)=LDP_PEER_TRANS_ADDR (pPeerSession->pLdpPeer).u2AddrType;

    MEMCPY (&LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&PwVcEvtInfo),
            &pLdpEntity->LdpId, IPV4_ADDR_LENGTH);

    LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&PwVcEvtInfo) =
        OSIX_NTOHL (LDP_L2VPN_EVT_SSN_LCL_ENTITYID (&PwVcEvtInfo));

    LDP_L2VPN_EVT_SSN_LCL_ENT_INDEX (&PwVcEvtInfo) = pLdpEntity->u4EntityIndex;

    MEMCPY ((UINT1 *) &LDP_L2VPN_EVT_SSN_PEER_ENTITYID (&PwVcEvtInfo),
            (UINT1 *) &LDP_PEER_ID (pPeerSession->pLdpPeer), IPV4_ADDR_LENGTH);

    LDP_L2VPN_EVT_SSN_PEER_ENTITYID (&PwVcEvtInfo) =
        OSIX_NTOHL (LDP_L2VPN_EVT_SSN_PEER_ENTITYID (&PwVcEvtInfo));

    LDP_L2VPN_EVT_SSN_LCL_ROLE (&PwVcEvtInfo) =
        pPeerSession->u1SessionRole == LDP_ACTIVE;

    if (LdpL2VpnPostEvent (&PwVcEvtInfo) == LDP_FAILURE)
    {
        LDP_DBG (LDP_PRCS_PRCS, "MAIN: INTMD-EXIT 5. Event Posting  Failed\n");
        return (LDP_FAILURE);
    }

    LDP_DBG (LDP_PRCS_PRCS, "MAIN: EXIT 6\n");
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessTgtEntityDeRegReqEvent                          */
/* Description   : This routine processes deregistration request with        */
/*                 LDP from L2VPN                                            */
/* Input(s)      : pSsnEvtInfo- Pointer to info containing Entity Id and Peer*/
/*                 Address to deregister with LDP                            */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpProcessTgtEntityDeRegReqEvent (VOID *pSsnEventInfo)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    tPwVcSsnEvtInfo    *pSsnEvtInfo = NULL;

	if(gLdpInfo.LdpIncarn[MPLS_DEF_INCARN].u1GrProgressStatus
        == LDP_GR_SHUT_DOWN_IN_PROGRESS)
	{
    	return LDP_SUCCESS;
	}

    if (gLdpInfo.u1LdpInitialised == LDP_ZERO)
    {
        LDP_DBG (LDP_MAIN_MISC, "MAIN: LDP Component is Disabled.\n");
        return LDP_FAILURE;
    }

    MPLS_LDP_LOCK ();
    pSsnEvtInfo = (tPwVcSsnEvtInfo *) pSsnEventInfo;

    pList = &LDP_ENTITY_LIST (LDP_CUR_INCARN);
    TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
    {
        if (pLdpEntity->u4EntityIndex == pSsnEvtInfo->u4LdpEntityIndex)
        {
            break;
        }
    }

    if (pLdpEntity == NULL)
    {
        /* send ssn down notification to l2vpn with error code as
         * entity not configured */
        LDP_DBG (LDP_MAIN_MISC, "MAIN: INTMD-EXIT 1. Entity not configured\n");
        MPLS_LDP_UNLOCK ();
        return LDP_FAILURE;
    }

    pLdpEntity->u4AppMask &=(UINT4) ~(LDP_APP_L2VPN_MASK);

    LDP_DBG (LDP_MAIN_MISC, "MAIN: EXIT 2\n");
    MPLS_LDP_UNLOCK ();
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessTgtSsnReleaseEvent                              */
/* Description   : This routine processes Targeted Session Release Request   */
/*                 from the L2VPN and Post Session Down notification to L2VPN*/
/*                 Once the session is Down                                  */
/* Input(s)      : pSsnEvtInfo- Pointer to info containing Entity Id and Peer*/
/*                 Address to Delete the session                             */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwrcv.c */
UINT1
LdpProcessTgtSsnReleaseEvent (tPwVcSsnEvtInfo * pSsnEvtInfo)
{
    UINT4               u2IncarnId = LDP_CUR_INCARN;
    tLdpSession        *pLdpSession = NULL;

    if (LdpGetSessionWithPeer (u2IncarnId,
                               pSsnEvtInfo->u4LocalLdpEntityID,
                               &pSsnEvtInfo->PeerAddr,
                               pSsnEvtInfo->u2AddrType,
                               LDP_DSTR_UNSOLICIT, LDP_TRUE,
                               &pLdpSession) == LDP_FAILURE)
    {
        /* No session and entity present */
        return LDP_FAILURE;
    }

    /* if entity NULL, Ssn also NULL */
    if (pLdpSession != NULL)
    {
        /* Delete the Targetted LDP session */
        LdpDeleteLdpSession (pLdpSession, LDP_STAT_TYPE_SHUT_DOWN);
    }
    else
    {
        /* No session present */
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessPwVcLblMsgEvent                                 */
/* Description   : This routine processes the Lbl Msg events received from   */
/*                 L2VPN                                                     */
/* Input(s)      : pLblMsgInfo- info about events and data received          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwrcv.c */
UINT1
LdpProcessPwVcLblMsgEvent (tPwVcLblMsgInfo * pLblMsgInfo)
{
    UINT1               u1RetStatus = LDP_SUCCESS;

    switch (pLblMsgInfo->u1MsgType)
    {
        case L2VPN_LDP_LBL_MAP_MSG:
            u1RetStatus = LdpProcessPwVcCreateReqEvent (pLblMsgInfo);
            break;

        case L2VPN_LDP_LBL_WDRAW_MSG:
            u1RetStatus =
                LdpProcessPwVcWthReqEvent (pLblMsgInfo, LDP_NORMAL_WITHDRAW);
            break;

        case L2VPN_LDP_LBL_WC_WDRAW_MSG:
            u1RetStatus =
                LdpProcessPwVcWthReqEvent (pLblMsgInfo, LDP_WCARD_WITHDRAW);
            break;

        case L2VPN_LDP_LBL_REL_MSG:
            u1RetStatus = LdpProcessPwVcReleaseReqEvent (pLblMsgInfo);
            break;

        case L2VPN_LDP_LBL_MAP_REQ_MSG:
            u1RetStatus = LdpProcessPwVcLblReqEvent (pLblMsgInfo);
            break;

        default:
            LDP_DBG (LDP_MAIN_MISC, "MAIN: Rcvd Invalid Message Type\n");
            u1RetStatus = LDP_FAILURE;
    }

    return u1RetStatus;
}

/*****************************************************************************/
/* Function Name : LdpProcessPwVcNotifMsgEvent                               */
/* Description   : This routine processes events received from L2VPN         */
/* Input(s)      : pAppEvtInfo- info about events and data received          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwrcv.c */
UINT1
LdpProcessPwVcNotifMsgEvent (tPwVcNotifEvtInfo * pNotifMsgInfo)
{
    tTMO_SLL           *pList = NULL;
    tLdpEntity         *pLdpEntity = NULL;
    UINT1               u1LblResNotAvl = LDP_FALSE;
    UINT1               u1RetVal = LDP_FAILURE;
    uLabel              Label;

    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;

    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

    switch (pNotifMsgInfo->u1MsgCode)
    {
        case L2VPN_LDP_REL_LBL_RES:

            pList = &LDP_ENTITY_LIST (LDP_CUR_INCARN);
            TMO_SLL_Scan (pList, pLdpEntity, tLdpEntity *)
            {
                if (pLdpEntity->u4EntityIndex ==
                    pNotifMsgInfo->u4LdpEntityIndex)
                {
                    break;
                }
            }

            if (pLdpEntity == NULL)
            {
                return LDP_FAILURE;
            }
            /* Free the label */
            if (pLdpEntity->u1LblResAvl == LDP_FALSE)
            {
                u1LblResNotAvl = LDP_TRUE;
            }

            Label.u4GenLbl = pNotifMsgInfo->u4Label;
            u1RetVal = LdpFreeLabel (pLdpEntity, &Label, LDP_ZERO);
            if ((u1LblResNotAvl == LDP_TRUE) && (u1RetVal == LDP_SUCCESS))
            {
                /* Notify L2VPN of the resource Availability */
                MEMCPY (&PwVcEvtInfo.unEvtInfo.NotifInfo, pNotifMsgInfo,
                        sizeof (tPwVcNotifEvtInfo));
                LDP_L2VPN_EVT_NOTIF_MSG_CODE (&PwVcEvtInfo)
                    = L2VPN_LDP_RSRC_AVAILABLE;
                LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = L2VPN_LDP_PWVC_NOTIF_MSG;
                if (LdpL2VpnPostEvent (&PwVcEvtInfo) == LDP_FAILURE)
                {
                    LDP_DBG (LDP_PRCS_PRCS,
                             "PRCS: Posting Event to Application Failed \n");
                    return LDP_FAILURE;
                }
            }
            break;
        
		case L2VPN_LDP_NOTIF_MSG_EVT:

            u1RetVal = LdpProcessPwVcNotifReqEvent (pNotifMsgInfo);
            break;

        default:
            LDP_DBG (LDP_MAIN_MISC,
                     "MAIN: Rcvd Invalid Notification from L2VPN \n");
            return LDP_FAILURE;
    }
    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : LdpProcessPwVcWthReqEvent                                 */
/* Description   : This routine processes events received from L2VPN         */
/* Input(s)      : pAppEvtInfo- info about events and data received          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

/* ldppwrcv.c */
UINT1
LdpProcessPwVcWthReqEvent (tPwVcLblMsgInfo * pLblMsgInfo, UINT1 u1WithType)
{
    tLdpSession        *pPeerSession = NULL;
    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT1               u1LblResNotAvl = LDP_FALSE;
    UINT1               u1RetVal = LDP_FAILURE;
    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

    if (LdpGetSessionWithPeer (u2IncarnId,
                               pLblMsgInfo->u4LocalLdpEntityID,
                               &pLblMsgInfo->PeerAddr,
                               pLblMsgInfo->u2AddrType,
                               LDP_DSTR_UNSOLICIT, LDP_TRUE,
                               &pPeerSession) == LDP_FAILURE)
    {
        return LDP_FAILURE;
    }

    if ((SSN_GET_ENTITY (pPeerSession))->u1LblResAvl == LDP_FALSE)
    {
        u1LblResNotAvl = LDP_TRUE;
    }

    u1RetVal = LdpSendPwVcLblWdrawMsg (pPeerSession, pLblMsgInfo);
    if (u1RetVal == LDP_SUCCESS)
    {
        LDP_DBG (LDP_PRCS_PRCS, "Successfully send Label Withdraw Msg\n");
    }
    if (u1WithType != LDP_WCARD_WITHDRAW)
    {
        /* Free the label */
        /*LdpFreeLabel (SSN_GET_ENTITY (pPeerSession), &Label, u4SessIfIndex);*/
    }
    else
    {
        /* Withdraw all the label associated with incoming group ID 
         * If group Id is associated with the Entity id the release all the
         * labels for the label group. 
         * if Pw Group Id is associated with tunnel Id then all the labels
         * allocated for the PwVc's associated with the incoming Group Id
         * Needs to be released.*/
    }

    if (u1LblResNotAvl == LDP_TRUE)
    {
	    /* Notify L2VPN of the resource Availability */
	    LdpCopyAddr(&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR (&PwVcEvtInfo)),
			    &pLblMsgInfo->PeerAddr,
			    pLblMsgInfo->u2AddrType);
	    LDP_L2VPN_EVT_NOTIF_ADDR_TYPE(&PwVcEvtInfo)=pLblMsgInfo->u2AddrType;
	    LDP_L2VPN_EVT_NOTIF_MSG_CODE (&PwVcEvtInfo) = L2VPN_LDP_RSRC_AVAILABLE;
	    LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = L2VPN_LDP_PWVC_NOTIF_MSG;
	    if (LdpL2VpnPostEvent (&PwVcEvtInfo) == LDP_FAILURE)
	    {
		    LDP_DBG (LDP_PRCS_PRCS, "Posting Event to Application Failed \n");
		    return LDP_FAILURE;
	    }
    }
    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : LdpProcessPwVcNotifReqEvent                               */
/* Description   : This routine processes events received from L2VPN         */
/* Input(s)      : pAppEvtInfo- info about events and data received          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

/* ldppwrcv.c */
UINT1
LdpProcessPwVcNotifReqEvent (tPwVcNotifEvtInfo * pNotifMsgInfo)
{

    tLdpSession        *pPeerSession = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;

    if (LdpGetSessionWithPeer (u2IncarnId,
			    pNotifMsgInfo->u4LocalLdpEntityID,
			    &pNotifMsgInfo->PeerAddr,
			    pNotifMsgInfo->u2AddrType,
			    LDP_DSTR_UNSOLICIT, LDP_TRUE,
			    &pPeerSession) == LDP_FAILURE)
    {
        return LDP_FAILURE;
    }

    if (LdpSendPwVcNotifMsg (pPeerSession, pNotifMsgInfo) == LDP_FAILURE)
    {
        LDP_DBG (LDP_PRCS_PRCS, "Failed to send Notification Msg \n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessPwVcAddrWdrawMsgEvent                           */
/* Description   : This routine processes events received from L2VPN         */
/* Input(s)      : pAppEvtInfo- info about events and data received          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

/* ldppwrcv.c */
UINT1
LdpProcessPwVcAddrWdrawMsgEvent (tPwVcNotifEvtInfo * pNotifMsgInfo)
{

    tLdpSession        *pPeerSession = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;

    if (LdpGetSessionWithPeer (u2IncarnId,
			    pNotifMsgInfo->u4LocalLdpEntityID,
			    &pNotifMsgInfo->PeerAddr,
			    pNotifMsgInfo->u2AddrType,
			    LDP_DSTR_UNSOLICIT, LDP_TRUE,
			    &pPeerSession) == LDP_FAILURE)
    {
	    return LDP_FAILURE;
    }

    if (LdpSendPwVcAddrWdrawMsg (pPeerSession, pNotifMsgInfo) == LDP_FAILURE)
    {
        LDP_DBG (LDP_PRCS_PRCS, "Failed to send Address Withdraw Msg \n");
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessPwVcLblReqEvent                                 */
/* Description   : This routine processes events received from L2VPN         */
/* Input(s)      : pAppEvtInfo- info about events and data received          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/

/* ldppwrcv.c */
UINT1
LdpProcessPwVcLblReqEvent (tPwVcLblMsgInfo * pLblMsgInfo)
{
    tLdpSession        *pPeerSession = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;

    if (LdpGetSessionWithPeer (u2IncarnId,
                               pLblMsgInfo->u4LocalLdpEntityID,
                               &pLblMsgInfo->PeerAddr,
                               pLblMsgInfo->u2AddrType,
                               LDP_DSTR_UNSOLICIT, LDP_TRUE,
                               &pPeerSession) == LDP_FAILURE)
    {
        return LDP_FAILURE;
    }

    if (LdpSendPwVcLblReqMsg (pPeerSession, pLblMsgInfo) == LDP_FAILURE)
    {
        LDP_DBG (LDP_PRCS_PRCS, "Failed to send Label Request Msg \n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessPwVcCreateReqEvent                              */
/* Description   : This routine processes events received from L2VPN         */
/* Input(s)      : pAppEvtInfo- info about events and data received          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwrcv.c */
UINT1
LdpProcessPwVcCreateReqEvent (tPwVcLblMsgInfo * pLblMsgInfo)
{
    tLdpSession        *pPeerSession = NULL;
    uLabel              Label;
    uLabel              Label1;
    UINT2               u2IncarnId = LDP_CUR_INCARN;
    UINT4               u4SessIfIndex = 0;
    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;

    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));

    if (LdpGetSessionWithPeer (u2IncarnId,
			    pLblMsgInfo->u4LocalLdpEntityID,
			    &pLblMsgInfo->PeerAddr,
			    pLblMsgInfo->u2AddrType,
			    LDP_DSTR_UNSOLICIT, LDP_TRUE,
			    &pPeerSession) == LDP_FAILURE)
    {
        /* Notify L2VPN for PW VC DOWN */
        pLblMsgInfo->u4Label = LDP_INVALID_LABEL;
        LdpNotifEventToL2Vpn (pLblMsgInfo, L2VPN_LDP_PWVC_DOWN, u4SessIfIndex);
        return LDP_FAILURE;
    }
    u4SessIfIndex = LdpSessionGetIfIndex(pPeerSession,pLblMsgInfo->u2AddrType);

    /* Second condition is to simulate failure condition to send new label
     */
    if ((pLblMsgInfo->u4StaleLabel != LDP_INVALID_LABEL) &&
        (gi4MplsSimulateFailure != LDP_SIM_FAILURE_SEND_NEW_LABEL))
    {
#ifdef MPLS_IPV6_WANTED
	    if(pLblMsgInfo->u2AddrType==LDP_ADDR_TYPE_IPV6)
	    {
		    LDP_DBG2 (GRACEFUL_DEBUG, "LdpProcessPwVcCreateReqEvent: "
				    "Received Stale Label %d from the Application for "
				    "the Peer %x\n.", pLblMsgInfo->u4StaleLabel,
				    Ip6PrintAddr(&pLblMsgInfo->PeerAddr.Ip6Addr));
	    }
	    else
#endif
	    {
		    LDP_DBG2 (GRACEFUL_DEBUG, "LdpProcessPwVcCreateReqEvent: "
				    "Received Stale Label %d from the Application for "
				    "the Peer %x\n.", pLblMsgInfo->u4StaleLabel,
				    LDP_IPV4_U4_ADDR(pLblMsgInfo->PeerAddr));
	    }

	    Label.u4GenLbl = pLblMsgInfo->u4StaleLabel;
    }
    else if (pLblMsgInfo->u4StaticLabel != LDP_INVALID_LABEL)
    {
        /* if Static label is set then simulate failure to send 
         * new label cannot be done */
        Label.u4GenLbl = pLblMsgInfo->u4StaticLabel;
    }
    else
    {
		if (gi4MplsSimulateFailure == LDP_SIM_FAILURE_SEND_NEW_LABEL)
		{
			pLblMsgInfo->u4Label = pLblMsgInfo->u4StaleLabel;
		}
        if (LdpGetLabel (SSN_GET_ENTITY (pPeerSession), &Label, u4SessIfIndex)
            != LDP_SUCCESS)
        {
            LDP_DBG (LDP_IF_MEM, "Allocation of PwVc Label Failed\n");
            /* Notify L2VPN of the resource Un-Availability */
            pLblMsgInfo->u4Label = LDP_INVALID_LABEL;
            LdpNotifEventToL2Vpn (pLblMsgInfo, L2VPN_LDP_RSRC_UNAVAILABLE,
                                  u4SessIfIndex);
            return LDP_FAILURE;
        }

        if (Label.u4GenLbl == pLblMsgInfo->u4Label)
        {
            if (LdpGetLabel
                (SSN_GET_ENTITY (pPeerSession), &Label1,
                 u4SessIfIndex) == LDP_SUCCESS)
            {
                LdpFreeLabel (SSN_GET_ENTITY (pPeerSession), &Label,
                              u4SessIfIndex);
                MEMCPY (&Label, &Label1, LDP_LABEL_LEN);
            }
            /* Shortage of Vc-labels, so use the allocated VC-label */
        }
        /* Shortage of Vc-labels, so use the allocated VC-label */
    }

    if (LdpSendPwVcLblMapMsg (pPeerSession, pLblMsgInfo, &Label) == LDP_SUCCESS)
    {
        LDP_DBG (LDP_PRCS_PRCS, "Successfully send Label Map Msg\n");

        /* Notify L2VPN for PW VC UP */
        pLblMsgInfo->u4Label = Label.u4GenLbl;
        if (LdpNotifEventToL2Vpn (pLblMsgInfo, L2VPN_LDP_PWVC_UP,
                                  u4SessIfIndex) == LDP_FAILURE)
        {
            LDP_DBG (LDP_PRCS_PRCS, "Failed to send PW UP notification "
                     "to L2VPN\n");
            return LDP_FAILURE;
        }
    }
    else
    {
        LDP_DBG (LDP_PRCS_PRCS, "Failed to send Label Map Msg\n");

        /* Release the allocated label */
        LdpFreeLabel (SSN_GET_ENTITY (pPeerSession), &Label, u4SessIfIndex);

        /* Notify L2VPN for PW VC DOWN */
        pLblMsgInfo->u4Label = LDP_INVALID_LABEL;
        LdpNotifEventToL2Vpn (pLblMsgInfo, L2VPN_LDP_PWVC_DOWN, u4SessIfIndex);
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpNotifEventToL2Vpn                                      */
/* Description   : This routine sends event to l2VPN                         */
/* Input(s)      : pLblMsgInfo - L2VPN inforamtion                           */
/*                 u1MsgCode - Messge code to be sent L2VPN                  */
/*                 u4SsnIfIndex - Session L3 Interface                       */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
UINT1
LdpNotifEventToL2Vpn (tPwVcLblMsgInfo * pLblMsgInfo, UINT1 u1MsgCode,
                      UINT4 u4SsnIfIndex)
{
    tL2VpnLdpPwVcEvtInfo PwVcEvtInfo;

    MEMSET (&PwVcEvtInfo, LDP_ZERO, sizeof (tL2VpnLdpPwVcEvtInfo));
    /* Notify L2VPN for PW VC DOWN */

    LdpCopyAddr(&(LDP_L2VPN_EVT_NOTIF_PEER_ADDR (&PwVcEvtInfo)),
		    &pLblMsgInfo->PeerAddr,
		    pLblMsgInfo->u2AddrType);

    LDP_L2VPN_EVT_NOTIF_ADDR_TYPE(&PwVcEvtInfo)=pLblMsgInfo->u2AddrType;

    if (pLblMsgInfo->i1PwOwner == LDP_PWID_FEC_SIGNALING)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&PwVcEvtInfo) = LDP_PWID_FEC_SIGNALING;
        LDP_L2VPN_EVT_NOTIF_VC_ID (&PwVcEvtInfo) = pLblMsgInfo->u4PwVcID;
    }
    else if (pLblMsgInfo->i1PwOwner == LDP_GEN_FEC_SIGNALING)
    {
        LDP_L2VPN_EVT_LBL_NOTIF_OWNER (&PwVcEvtInfo) = LDP_GEN_FEC_SIGNALING;
        if (pLblMsgInfo->u1AgiLen > LDP_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_AGI (&PwVcEvtInfo),
                    pLblMsgInfo->au1Agi, pLblMsgInfo->u1AgiLen);
            LDP_L2VPN_EVT_NOTIF_AGI_LEN (&PwVcEvtInfo) = pLblMsgInfo->u1AgiLen;
        }
        else
        {
            LDP_L2VPN_EVT_NOTIF_AGI_LEN (&PwVcEvtInfo) = LDP_ZERO;
        }
        if (pLblMsgInfo->u1SaiiLen > LDP_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_SAII (&PwVcEvtInfo),
                    pLblMsgInfo->au1Saii, pLblMsgInfo->u1SaiiLen);
            LDP_L2VPN_EVT_NOTIF_SAII_LEN (&PwVcEvtInfo) =
                pLblMsgInfo->u1SaiiLen;
        }
        else
        {
            LDP_L2VPN_EVT_NOTIF_SAII_LEN (&PwVcEvtInfo) = LDP_ZERO;
        }

        if (pLblMsgInfo->u1TaiiLen > LDP_ZERO)
        {
            MEMCPY (LDP_L2VPN_EVT_NOTIF_TAII (&PwVcEvtInfo),
                    pLblMsgInfo->au1Taii, pLblMsgInfo->u1TaiiLen);
            LDP_L2VPN_EVT_NOTIF_TAII_LEN (&PwVcEvtInfo) =
                pLblMsgInfo->u1TaiiLen;
        }
        else
        {
            LDP_L2VPN_EVT_NOTIF_TAII_LEN (&PwVcEvtInfo) = LDP_ZERO;
        }
    }
    LDP_L2VPN_EVT_NOTIF_IF_INDEX (&PwVcEvtInfo) = u4SsnIfIndex;
    LDP_L2VPN_EVT_NOTIF_VC_ID (&PwVcEvtInfo) = pLblMsgInfo->u4PwVcID;
    LDP_L2VPN_EVT_NOTIF_VC_TYPE (&PwVcEvtInfo) = (UINT1)pLblMsgInfo->i1PwType;
    LDP_L2VPN_EVT_NOTIF_LABEL (&PwVcEvtInfo) = pLblMsgInfo->u4Label;
    LDP_L2VPN_EVT_NOTIF_MSG_CODE (&PwVcEvtInfo) = u1MsgCode;
    LDP_L2VPN_EVT_TYPE (&PwVcEvtInfo) = L2VPN_LDP_PWVC_NOTIF_MSG;
    if (LdpL2VpnPostEvent (&PwVcEvtInfo) == LDP_FAILURE)
    {
        LDP_DBG (LDP_PRCS_PRCS, "Posting Event to Application Failed \n");
        return LDP_FAILURE;
    }
    return LDP_SUCCESS;
}

/*****************************************************************************/
/* Function Name : LdpProcessPwVcReleaseReqEvent                             */
/* Description   : This routine processes events received from L2VPN         */
/* Input(s)      : pAppEvtInfo- info about events and data received          */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS or LDP_FAILURE                                */
/*****************************************************************************/
/* ldppwrcv.c */
UINT1
LdpProcessPwVcReleaseReqEvent (tPwVcLblMsgInfo * pLblMsgInfo)
{
    tLdpSession        *pPeerSession = NULL;
    UINT2               u2IncarnId = LDP_CUR_INCARN;

    if (LdpGetSessionWithPeer (u2IncarnId,
                               pLblMsgInfo->u4LocalLdpEntityID,
                               &pLblMsgInfo->PeerAddr,
                               pLblMsgInfo->u2AddrType,
                               LDP_DSTR_UNSOLICIT, LDP_TRUE,
                               &pPeerSession) == LDP_FAILURE)
    {
        return LDP_FAILURE;
    }

    if (LdpSendPwVcLblRelMsg (pPeerSession, pLblMsgInfo) == LDP_FAILURE)
    {
        LDP_DBG (LDP_PRCS_PRCS, "Failed to send Label Rel Msg\n");
        return LDP_FAILURE;
    }

    return LDP_SUCCESS;
}
