/*---------------------------------------------------------------------------*/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: ldpnhsm1.c,v 1.24 2016/02/28 10:58:23 siva Exp $
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : ldpnhsm1.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : LDP (ADVT SUB-MODULE)
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains Next hop state machine 
 *                             routines.
 *----------------------------------------------------------------------------*/

#ifndef _LDP_NHSM1_C
#define _LDP_NHSM1_C

#include "ldpincs.h"
#include "ldpgblex.h"
#include "ldpnhsm1.h"
/*****************************************************************************/
/* Function Name :  LdpNonMrgNhopChngIdlNewNh                                */
/* Description   : This F'n is called when the state machine is in Idle  */
/*                 state and New_Next_hop event is received.                 */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpNonMrgNhopChngIdlNewNh (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
        tLdpRouteEntryInfo *pRtInfo = NULL;

        pRtInfo = (tLdpRouteEntryInfo *) (VOID *) pMsg;

#if MPLS_IPV6_WANTED
        if(pRtInfo->u2AddrType==LDP_IPV6ADR_LEN)
        {
                LDP_DBG4 (LDP_ADVT_SEM,
                                "ADVT_NH: CS %d Evt %d NHop %s If %d \n",
                                au1LdpNhEvts[NH_TRGCB_ST (pNHCtrlBlock)],
                                au1LdpNhStates[LDP_NH_LSM_EVT_INT_NEW_NH],
                                Ip6PrintAddr(&pRtInfo->NextHop.Ip6Addr), pRtInfo->u4RtIfIndx);
        }
        else
#endif
        {
                LDP_DBG4 (LDP_ADVT_SEM,
                                "ADVT_NH: CS %d Evt %d NHop %d If %d \n",
                                au1LdpNhEvts[NH_TRGCB_ST (pNHCtrlBlock)],
                                au1LdpNhStates[LDP_NH_LSM_EVT_INT_NEW_NH],
                                LDP_IPV4_U4_ADDR(pRtInfo->NextHop), pRtInfo->u4RtIfIndx);
        }

        LdpCopyAddr(&pNHCtrlBlock->nHopInfo.NHAddr,&pRtInfo->NextHop,
                        pRtInfo->u2AddrType);

        pNHCtrlBlock->nHopInfo.u2AddrType=pRtInfo->u2AddrType;

        pNHCtrlBlock->nHopInfo.u4IfIndex = pRtInfo->u4RtIfIndx;
        /* Start the retry Timer */
        pNHCtrlBlock->retryTimer.u4Event = NHOP_RETRY_TMR_EXPIRED_EVENT;
        pNHCtrlBlock->retryTimer.pu1EventInfo = (UINT1 *) pNHCtrlBlock;
        NH_TRGCB_ST (pNHCtrlBlock) = LDP_NH_LSM_ST_NEWNH_RETRY;
        LDP_DBG1 (LDP_ADVT_SEM, "ADVT_NH: NS %d \n",
                        au1LdpNhStates[LDP_NH_LSM_ST_NEWNH_RETRY]);
        if (TmrStartTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *)
                                & (pNHCtrlBlock->retryTimer.AppTimer),
                                (NHOP_RETRY_TMR_TIMEOUT *
                                 SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
        {
                LDP_DBG (LDP_ADVT_TMR, "ADVT_NH: NH Retry Tmr start Failed \n");

        }

        LDP_DBG (LDP_ADVT_TMR, "ADVT_NH: NH Retry Tmr Started \n");
}

/************************************************************************/
/* Function Name :  LdpNonMrgNhopChngInvStateEvt                        */
/* Description   : This F'n is called, on the occurrence of an invalid  */
/*                 event, in a state.                                   */
/* Input(s)      : pNHCtrlBlock, pMsg                                   */
/* Output(s)     : None                                                 */
/* Return(s)     : None                                                 */
/************************************************************************/

VOID
LdpNonMrgNhopChngInvStateEvt (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{

        pMsg = (UINT1 *) pMsg;
        pNHCtrlBlock = (tLspTrigCtrlBlock *) pNHCtrlBlock;
        LDP_DBG (LDP_ADVT_MISC, "ADVT_NH: NH st m/c Invalid State \n");

        return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgNhopChngRetryNewNh                               */
/* Description   : This F'n is called when the state machine is in       */
/*                  New_NH_retry state and New_Next_hop event is received.   */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgNhopChngRetryNewNh (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
        tLdpRouteEntryInfo *pRtInfo = NULL;
        /* Get the new Next hop address */
        pRtInfo = (tLdpRouteEntryInfo *) (VOID *) pMsg;



        LdpCopyAddr(&pNHCtrlBlock->nHopInfo.NHAddr,&pRtInfo->NextHop,
                        pRtInfo->u2AddrType);
        pNHCtrlBlock->nHopInfo.u2AddrType = pRtInfo->u2AddrType;
        pNHCtrlBlock->nHopInfo.u4IfIndex = pRtInfo->u4RtIfIndx;
        /*  Restart the retry Timer */
        if (TmrStopTimer (LDP_TIMER_LIST_ID,
                                (tTmrAppTimer *) & (pNHCtrlBlock->retryTimer.AppTimer)) ==
                        TMR_FAILURE)
        {
                LDP_DBG (LDP_ADVT_TMR, "ADVT_NH: Failed to stop the timer \n");
        }
        if (TmrStartTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *)
                                & (pNHCtrlBlock->retryTimer.AppTimer),
                                (NHOP_RETRY_TMR_TIMEOUT *
                                 SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
        {
                LDP_DBG (LDP_ADVT_TMR,
                                "ADVT_NH: NH Retry. Failed to start the timer \n");
        }
        LDP_DBG (LDP_ADVT_TMR, "ADVT_NH: NH Retry Tmr Re-started \n");

        return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgNhopChngRetryRetryTmout                          */
/* Description   : This F'n is called when the state machine is in       */
/*                 New_NH_retry state and Internal_Retry_timeout event is    */
/*                 received.                                                 */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpNonMrgNhopChngRetryRetryTmout (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
        tLspCtrlBlock      *pLspCtrlBlock = NULL;
        tCrlspTnlInfo      *pCrlspTnlInfo = NULL;
#if 0
        UINT2               u2IncarnId=0;
        tLdpSession        *pLdpSession = NULL;
        tTMO_SLL_NODE      *pSllNode = NULL;
#endif
        tGenU4Addr         NextHopAddr;

        MEMSET(&NextHopAddr,LDP_ZERO,sizeof(tGenU4Addr));

        pMsg = (UINT1 *) pMsg;
#if 0
ONDEMAND
        if ((NH_TRGCB_OLSP (pNHCtrlBlock) != NULL) &&
                        (LCB_DSSN (NH_TRGCB_OLSP (pNHCtrlBlock))) != NULL)
        {
                u2IncarnId = SSN_GET_INCRN_ID (LCB_DSSN (NH_TRGCB_OLSP (pNHCtrlBlock)));
        }
        else
        {
                return;
        }
#endif

        if(LdpIsAddressMatchWithAddrType(&pNHCtrlBlock->oRgNextHopAddr,
                                &pNHCtrlBlock->nHopInfo.NHAddr,
                                pNHCtrlBlock->u2AddrType,
                                pNHCtrlBlock->nHopInfo.u2AddrType)==LDP_TRUE)
        {
                LDP_DBG (LDP_ADVT_PRCS,
                                "ADVT_NH:New NH same as old.Deleteing Trgg Blk\n");
                if(NH_TRGCB_OLSP (pNHCtrlBlock)!=NULL)
                {
                        LCB_NHOP(NH_TRGCB_OLSP (pNHCtrlBlock))=NULL;
                        NH_TRGCB_OLSP(pNHCtrlBlock)=NULL;
                }
                LdpDeleteTrigCtrlBlk (pNHCtrlBlock);
                return;
        }
        else
        {
                /* The Re-Routing In Non Merge liberal is allowed only at the 
                 * ingress of the Lsps */

                LdpReRouteTime (LDP_START_REROUTE);
#if 0
/**** vishal_1 : Pending ***/
                if (LCB_TRIG (NH_TRGCB_OLSP (pNHCtrlBlock)) != NULL)
                {
                        if ((SSN_GET_ENTITY (LCB_DSSN (NH_TRGCB_OLSP (pNHCtrlBlock)))->
                                                u2LabelRet) == LDP_LIBERAL_MODE)
                        {

                                LdpCopyAddr(&NextHopAddr.Addr,&pNHCtrlBlock->nHopInfo.NHAddr,
                                                pNHCtrlBlock->nHopInfo.u2AddrType);
                                NextHopAddr.u2AddrType=pNHCtrlBlock->nHopInfo.u2AddrType;

                                if (LdpGetPeerSession (&NextHopAddr,
                                                        0, &pLdpSession,
                                                        u2IncarnId) == LDP_SUCCESS)
                                {

                                        /*  If the Control Block is present on the
                                         *  New Route session there is no need for
                                         *  reroute which can happen only in liberal
                                         *  mode
                                         */

                                        if ((LCB_DSSN (NH_TRGCB_OLSP (pNHCtrlBlock)))
                                                        == pLdpSession)
                                        {
                                                LdpDeleteTrigCtrlBlk (pNHCtrlBlock);
                                                return;
                                        }

                                        /* Check if the LspCtrlBlock already exist for the New
                                         * Route */
                                        if ((SSN_MRGTYPE (pLdpSession) == LDP_NO_MRG) &&
                                                        (SSN_GET_ENTITY (pLdpSession)->u2LabelRet ==
                                                         LDP_LIBERAL_MODE))
                                        {
                                                TMO_SLL_Scan (&SSN_DLCB (pLdpSession), pSllNode,
                                                                tTMO_SLL_NODE *)
                                                {
                                                        pLspCtrlBlock = SLL_TO_LCB (pSllNode);

                                                        if ((LdpCompareFec(&LCB_FEC (pLspCtrlBlock),
                                                                                        &LCB_FEC (NH_TRGCB_OLSP
                                                                                                (pNHCtrlBlock))) ==
                                                                                LDP_EQUAL)
                                                                        && (LCB_STATE (pLspCtrlBlock) ==
                                                                                LDP_LSM_ST_EST))
                                                        {
                                                                if (LCB_TRIG (pLspCtrlBlock) != NULL)
                                                                {
                                                                        LdpCopyAddr(&pLspCtrlBlock->NextHopAddr,
                                                                                        &pNHCtrlBlock->nHopInfo.NHAddr,
                                                                                        pNHCtrlBlock->nHopInfo.u2AddrType);
                                                                        pLspCtrlBlock->u2AddrType=pNHCtrlBlock->nHopInfo.u2AddrType;
                                                                        LdpSendMplsMlibUpdate
                                                                                (MPLS_MLIB_FTN_MODIFY,
                                                                                 MPLS_OPR_PUSH, pLspCtrlBlock, NULL);
                                                                        /* Destroy the original LSP control Block 
                                                                         * */
                                                                        LdpDeleteLspCtrlBlock (NH_TRGCB_OLSP
                                                                                        (pNHCtrlBlock));
                                                                        LdpReRouteTime (LDP_END_REROUTE);
                                                                        return;
                                                                }
                                                        }
                                                }
                                        }
                                        else
                                        {
                                                /* Merge to Non Merge rerouting is not addressed */
                                                LdpDeleteLspCtrlBlock (NH_TRGCB_OLSP (pNHCtrlBlock));
                                                return;
                                        }
                                }
                        }
                }
#endif
                pLspCtrlBlock = (tLspCtrlBlock *) MemAllocMemBlk (LDP_LSP_POOL_ID);
                LDP_DBG3(LDP_ADVT_MEM, "ADVT: %s: %d Memory allocated for Control Block pLspCtrlBlock=0x%x\n",
                                __FUNCTION__,__LINE__,pLspCtrlBlock);

                if (pLspCtrlBlock == NULL)
                {
                        LDP_DBG (LDP_ADVT_MEM,
                                        "ADVT_NH: Failed to Alloc memory for LSP blk\n");
                        /* Trigger Control Block to be released by Original LSP. */
                        return;
                }

                LdpInitLspCtrlBlock (pLspCtrlBlock);
                MEMCPY (&(LCB_FEC (pLspCtrlBlock)),
                                &(pNHCtrlBlock->Fec), sizeof (tFec));
                NH_TRGCB_NLSP (pNHCtrlBlock) = pLspCtrlBlock;
                LdpCopyAddr(&pLspCtrlBlock->NextHopAddr,
                                &pNHCtrlBlock->nHopInfo.NHAddr,
                                pNHCtrlBlock->nHopInfo.u2AddrType);
                pLspCtrlBlock->u2AddrType=pNHCtrlBlock->nHopInfo.u2AddrType;
                if ((NH_TRGCB_OLSP (pNHCtrlBlock)!=NULL) &&
                                (NH_TRGCB_OLSP (pNHCtrlBlock)->pCrlspTnlInfo != NULL))
                {
                        pCrlspTnlInfo = (tCrlspTnlInfo *)
                                MemAllocMemBlk (LDP_CRLSPTNL_POOL_ID);
                        if (pCrlspTnlInfo == NULL)
                        {
                                LDP_DBG (LDP_ADVT_MEM,
                                                "ADVT_NH: Failed to Alloc Mem for New "
                                                "CRLSP Tnl. Returning \n");
                                LdpDeleteLspCtrlBlock (pLspCtrlBlock);
                                return;
                        }
                        /* Copy the CRLSP Information */
                        MEMCPY (pCrlspTnlInfo,
                                        (NH_TRGCB_OLSP (pNHCtrlBlock))->pCrlspTnlInfo,
                                        sizeof (tCrlspTnlInfo));
                        pCrlspTnlInfo->pLspCtrlBlock = pLspCtrlBlock;
                        pLspCtrlBlock->pCrlspTnlInfo = pCrlspTnlInfo;
                }
                /* Send event "Internal setup to the st m/c" */
                LDP_DBG (LDP_ADVT_SEM,
                                "ADVT_NH: Send Int-setup evt to Non-Mrg St m/c\n ");
                LdpNonMrgIdIntSetup (pLspCtrlBlock, (UINT1 *) pNHCtrlBlock);

                if (LCB_STATE (pLspCtrlBlock) == LDP_LSM_ST_RSP_AWT)
                {
                        NH_TRGCB_ST (pNHCtrlBlock) = LDP_NH_LSM_ST_RESP_AWAIT;
                }

                return;
        }

}

/*****************************************************************************/
/* Function Name : LdpNonMrgNhopChngRetryDestroy                             */
/* Description   : This F'n is called when the state machine is in       */
/*                 New_NH_retry state and Internal_Destroy event is          */
/*                 received.                                                 */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
LdpNonMrgNhopChngRetryDestroy (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
        pMsg = (UINT1 *) pMsg;

#ifdef ONDEMAND
        /** No need to stop the timer, as we are 
          stoping the timer inside the DeleteTrigCtrlBlk*/
        TmrStopTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *)
                        & (pNHCtrlBlock->retryTimer.AppTimer));
#endif
        LDP_DBG (LDP_ADVT_TMR, "ADVT_NH: Nhop Retry Tmr stopped \n");
        /** Making only Orignal Downstream Control block to NULL*/
        /** NewNextHop LSP control bock must be NULL in this case, so not required**/

        if(NH_TRGCB_OLSP (pNHCtrlBlock)!=NULL)
        {
                LCB_NHOP(NH_TRGCB_OLSP (pNHCtrlBlock))=NULL;
                NH_TRGCB_OLSP(pNHCtrlBlock)=NULL;
        }

        LdpDeleteTrigCtrlBlk (pNHCtrlBlock);
        return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgNhopChngRspAwaitNewNh                            */
/* Description   : This routine is called when the state machine is in       */
/*                 New_NH_Resp_Await state and New_NH event is  received.    */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgNhopChngRspAwaitNewNh (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
        tLdpRouteEntryInfo *pRtInfo = NULL;
        tLspCtrlBlock *pNewNhLspCtrlBlk=NULL;


        /* Get the new Next hop address */
        pRtInfo = (tLdpRouteEntryInfo *) (VOID *) pMsg;

        LdpCopyAddr(&pNHCtrlBlock->nHopInfo.NHAddr,&pRtInfo->NextHop,
                        pRtInfo->u2AddrType);
        pNHCtrlBlock->nHopInfo.u2AddrType = pRtInfo->u2AddrType;

        pNHCtrlBlock->nHopInfo.u4IfIndex = pRtInfo->u4RtIfIndx;

        /*  Restart the retry Timer */
        if (TmrStopTimer (LDP_TIMER_LIST_ID,
                                (tTmrAppTimer *) & (pNHCtrlBlock->retryTimer.AppTimer)) ==
                        TMR_FAILURE)
        {
                LDP_DBG (LDP_ADVT_TMR, "ADVT_NH: Failed to stop the timer \n");
        }
        if (TmrStartTimer (LDP_TIMER_LIST_ID, (tTmrAppTimer *)
                                & (pNHCtrlBlock->retryTimer.AppTimer),
                                (NHOP_RETRY_TMR_TIMEOUT *
                                 SYS_NUM_OF_TIME_UNITS_IN_A_SEC)) == TMR_FAILURE)
        {
                LDP_DBG (LDP_ADVT_TMR, "ADVT_NH: Failed to start the timer \n");
        }

        LDP_DBG (LDP_ADVT_TMR, "ADVT_NH: Nhop retry tmr restarted \n");
        NH_TRGCB_ST (pNHCtrlBlock) = LDP_NH_LSM_ST_NEWNH_RETRY;
        pNewNhLspCtrlBlk=NH_TRGCB_NLSP(pNHCtrlBlock);
        NH_TRGCB_NLSP(pNHCtrlBlock)=NULL;

        /* Send Internal_Destroy to the newly estabilshing LSP */
        gLdpNonMrgFsm[LCB_STATE (pNewNhLspCtrlBlk)]
                [LDP_LSM_EVT_INT_DSTR] (pNewNhLspCtrlBlk, NULL);

        return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgNhopChngRspAwaitLspup                            */
/* Description   : This F'n is called when the state machine is in           */
/*                 New_NH_Resp_Await state and LSP_UP event is  received.    */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgNhopChngRspAwaitLspup (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
        tLspCtrlBlock      *pLspCtrlBlock = NULL;
        tLdpSession        *pLdpSession = NULL;

        LDP_SUPPRESS_WARNING (pMsg);

        pLspCtrlBlock = NH_TRGCB_NLSP (pNHCtrlBlock);
        if (pNHCtrlBlock->pTrigCtrlBlkInOrgLsp != NULL)
        {
                if(NH_TRGCB_OLSP(pNHCtrlBlock)!=NULL)
                {

                        LCB_NHOP(NH_TRGCB_OLSP(pNHCtrlBlock))=NULL;  
                        /** Delete the Old Downstream Control Block**/
                        gLdpNonMrgFsm[LCB_STATE (NH_TRGCB_OLSP (pNHCtrlBlock))]
                                [LDP_LSM_EVT_INT_DSTR] (NH_TRGCB_OLSP (pNHCtrlBlock), NULL);
                }
                LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;
                 
                MEMSET(pNHCtrlBlock,0,sizeof(tLspTrigCtrlBlock));
                NH_TRGCB_OLSP(pNHCtrlBlock)=pLspCtrlBlock;

                LdpSendMplsMlibUpdate (MPLS_MLIB_FTN_MODIFY, MPLS_OPR_PUSH,
                                pLspCtrlBlock, NULL);
        }
        else
        {
                /** Copy the upstream control block from the orignal Control Block **/
                MEMCPY ((UINT1 *) (&(LCB_ULBL (pLspCtrlBlock))),
                                (UINT1 *) (&(NH_TRGCB_ULABEL (pNHCtrlBlock))),
                                sizeof (uLabel));
                /** copy the upstream Request Id from the orignal Control Block **/
                LCB_UREQ_ID (pLspCtrlBlock) =
                        NH_TRGCB_ULABEL_REQID (pNHCtrlBlock);
                /** Copy the upstream session from the orignal Control Block **/
                LCB_USSN (pLspCtrlBlock) = NH_TRGCB_USESSION(pNHCtrlBlock);
                /*  Adding LCB to UpStream Session. */
                TMO_SLL_Insert (&SSN_ULCB (LCB_USSN (pLspCtrlBlock)), NULL,
                                &(pLspCtrlBlock->NextUSLspCtrlBlk));
                if(NH_TRGCB_OLSP(pNHCtrlBlock)!=NULL)
                {
                        LCB_NHOP(NH_TRGCB_OLSP(pNHCtrlBlock))=NULL;
                        gLdpNonMrgFsm[LCB_STATE (NH_TRGCB_OLSP (pNHCtrlBlock))]
                                [LDP_LSM_EVT_INT_DSTR] (NH_TRGCB_OLSP (pNHCtrlBlock), NULL);
                        NH_TRGCB_OLSP(pNHCtrlBlock)=NULL;
                }
                if (pLspCtrlBlock->u4InComIfIndex == 0 &&
                                LCB_USSN (pLspCtrlBlock) != NULL)
                {
                        pLdpSession = LCB_USSN (pLspCtrlBlock);
                        pLspCtrlBlock->u4InComIfIndex = LdpSessionGetIfIndex(pLdpSession,pLspCtrlBlock->Fec.u2AddrFmly);

                }
                LdpSendMplsMlibUpdate (MPLS_MLIB_ILM_CREATE, MPLS_OPR_POP_PUSH,
                                pLspCtrlBlock, NULL);
                LCB_STATE (pLspCtrlBlock) = LDP_LSM_ST_EST;
                LCB_TRIG (pLspCtrlBlock) = NULL;
                LCB_NHOP(pLspCtrlBlock)=NULL;

                /** Although we are going to delete Trig NextHop Ctrl block, even then we are 
                    we are marking it to NULL, just to show that we handled new nextHop Control 
                    Block */
                NH_TRGCB_NLSP(pNHCtrlBlock)=NULL;

                LdpDeleteTrigCtrlBlk (pNHCtrlBlock);
        }
        /* Destroy the original LSP control Block */
        LDP_DBG (LDP_ADVT_MISC, "ADVT_NH: Original LSP CB Deleted \n");
        LdpReRouteTime (LDP_END_REROUTE);
        return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgNhopChngRspAwaitNak                              */
/* Description   : This routine is called when the state machine is in       */
/*                 New_NH_Resp_Await state and LSP_NAK event is  received.   */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

/** currently not in scope of nonMerge ondemand **/
VOID
LdpNonMrgNhopChngRspAwaitNak (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
        pMsg = (UINT1 *) pMsg;

        LdpDeleteTrigCtrlBlk (pNHCtrlBlock);
        return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgNhopChngRspAwaitDestroy                          */
/* Description   : This routine is called when the state machine is in       */
/*                 New_NH_Resp_Await state and Internal_Destroy event is     */
/*                 received.                                                 */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgNhopChngRspAwaitDestroy (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
        pMsg = (UINT1 *) pMsg;

        if(NH_TRGCB_OLSP (pNHCtrlBlock)!=NULL)
        {
                LCB_NHOP(NH_TRGCB_OLSP (pNHCtrlBlock))=NULL;
                NH_TRGCB_OLSP(pNHCtrlBlock)=NULL;
        }


        /* Destroy the Newly Estabilsing LSP */
        gLdpNonMrgFsm[LCB_STATE (NH_TRGCB_NLSP (pNHCtrlBlock))]
                [LDP_LSM_EVT_INT_DSTR] (NH_TRGCB_NLSP (pNHCtrlBlock), NULL);

        LDP_DBG (LDP_ADVT_MISC, "ADVT_NH: New LSP CBlk Deleted \n");
        return;
}

/*****************************************************************************/
/* Function Name : LdpDeleteTrigCtrlBlk                                      */
/* Description   : This routine Deletes the Trigger Control Block.           */
/* Input(s)      : pTrigCtrlBlock                                            */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpDeleteTrigCtrlBlk (tLspTrigCtrlBlock * pTrigCtrlBlock)
{

        TmrStopTimer (LDP_TIMER_LIST_ID,
                        (tTmrAppTimer *) & (pTrigCtrlBlock->retryTimer.AppTimer));

#if 0
        /** Not required, As Trigger control block is deleted after Downstream Control block**/
        LCB_NHOP (NH_TRGCB_OLSP (pTrigCtrlBlock)) = NULL;
#endif

        if ((NH_TRGCB_NLSP (pTrigCtrlBlock) != NULL) || 
                        (NH_TRGCB_OLSP(pTrigCtrlBlock)!=NULL))
        {
                LDP_DBG2(LDP_ADVT_MISC, "ADVT: %s: %d Failure scenario\n",
                                __FUNCTION__,__LINE__);
        }

#if 0
        if ((NH_TRGCB_NLSP (pTrigCtrlBlock) != NULL) &&
                        (LCB_STATE (NH_TRGCB_NLSP (pTrigCtrlBlock)) != LDP_LSM_ST_EST))
        {
                if (LCB_TRIG (NH_TRGCB_NLSP (pTrigCtrlBlock)) != NULL)
                {
                        MemReleaseMemBlock (LDP_TRIG_POOL_ID,
                                        (UINT1 *) (LCB_TRIG (NH_TRGCB_NLSP
                                                        (pTrigCtrlBlock))));
                        LDP_DBG3(LDP_ADVT_MEM, "ADVT: %s: %d Memory release for Trig Control Block pNHTrggCtrlBlk=0x%x\n",
                                        __FUNCTION__,__LINE__,(LCB_TRIG (NH_TRGCB_NLSP(pTrigCtrlBlock))));

                        LCB_TRIG (NH_TRGCB_NLSP (pTrigCtrlBlock)) = NULL;
                }
        }
#endif

#if 0
        /* Related to Merge OnDemand : currently not in scope*/
        if (NH_NEW_UPSTR_PTR (pTrigCtrlBlock) != NULL)
        {
                if (UPSTR_NHOP (NH_NEW_UPSTR_PTR (pTrigCtrlBlock)) != NULL)
                {
                        UPSTR_NHOP (NH_NEW_UPSTR_PTR (pTrigCtrlBlock)) = NULL;
                }
                LdpDeleteUpstrCtrlBlock (NH_NEW_UPSTR_PTR (pTrigCtrlBlock));
                NH_NEW_UPSTR_PTR (pTrigCtrlBlock) = NULL;
        }
#endif

        /** Free the memory of Trig Control Block **/
        MEMSET(pTrigCtrlBlock,0xffffffff,sizeof(tLspTrigCtrlBlock));
        MemReleaseMemBlock (LDP_TRIG_POOL_ID, (UINT1 *) (pTrigCtrlBlock));
        LDP_DBG3(LDP_ADVT_MEM, "ADVT: %s: %d Memory release for Trig Control Block pNHTrggCtrlBlk=0x%x\n",
                        __FUNCTION__,__LINE__,pTrigCtrlBlock);

        LDP_DBG (LDP_ADVT_MISC, "ADVT_NH: Nhop Trgg Ctl Block is Deleted \n");
        LDP_DBG2(LDP_ADVT_PRCS,"\n\tLine::%d,,Function::%s :: EXIT \n",__LINE__,__func__);    

        return;
}

/*****************************************************************************/
/* Function Name : LdpNonMrgNhopChngIntLspDown                               */
/* Description   : F'n to which make Internal LSP down.                      */
/* Input(s)      : pNHCtrlBlock, pMsg                                        */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
LdpNonMrgNhopChngIntLspDown (tLspTrigCtrlBlock * pNHCtrlBlock, UINT1 *pMsg)
{
        pMsg = (UINT1 *) pMsg;  

        LdpNonMrgSsnDnDeleteLspCtrlBlk (NH_TRGCB_NLSP (pNHCtrlBlock));
        LDP_DBG (LDP_ADVT_MISC, "ADVT_NH: New LSP Ctrl blk is Deleted \n");
#if 0
        /** update the NextHopCtrlBlockPointer in 
          Orignal DownStream Ctrl Block to NULL **/

        if(NH_TRGCB_OLSP (pNHCtrlBlock)!=NULL)
        {
                LCB_NHOP(NH_TRGCB_OLSP (pNHCtrlBlock))=NULL;
                NH_TRGCB_OLSP(pNHCtrlBlock)=NULL;
        }

        NH_TRGCB_NLSP(pNHCtrlBlock)=NULL;
        LdpDeleteTrigCtrlBlk (pNHCtrlBlock);
        LDP_DBG2(LDP_ADVT_PRCS,"\n\tLine::%d,,Function::%s\n",__LINE__,__func__);    
#endif
        return;
}

#endif /* _LDP_NHSM1_C */
/*---------------------------------------------------------------------------*/
/*                       End of file ldpnhsm1.c                              */
/*---------------------------------------------------------------------------*/
