/********************************************************************
* Copyright (C) 2012 Aricent Inc . All Rights Reserved
*
* $Id: mplstt.c,v 1.2 2014/11/17 12:00:04 siva Exp $
*
* Description: This file contains the common Test routines for MPLS
*********************************************************************/
#include "mplsincs.h"
#include "mplscli.h"

/****************************************************************************
Function    :  MplsDsTeTestAllClassTypeTable
Input       :  pu4ErrorCode       - Error code
               u4ClassTypeIndex   - Class Type Index
               pTestDsTeClassType - Pointer to Class Type entry
               u4ObjectId         - Object ID
Description :  This function tests the Class Type Table objects
Output      :  None 
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
MplsDsTeTestAllClassTypeTable (UINT4 *pu4ErrorCode, UINT4 u4ClassTypeIndex,
                                  tMplsDsTeClassType *pTestDsTeClassType,
                                  UINT4 u4ObjectId)
{
    tMplsDsTeClassType ClassEntry;
    tMplsDsTeClassType DsTeClassEntry;

    UINT4              u4DsTeClassTypeIndex = MPLS_ZERO;
    INT4               i4DsTeClassTypeTotalBwPercent = MPLS_ZERO;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof(tMplsDsTeClassType));
    MEMSET (&DsTeClassEntry, MPLS_ZERO, sizeof(tMplsDsTeClassType));

    if ((MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP) ||
        (MPLS_DSTE_STATUS (MPLS_DEF_INCARN) != MPLS_DSTE_STATUS_DISABLE)||
        (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    ClassEntry = MPLS_DIFFSERV_CLASS_TYPE_ENTRY (MPLS_DEF_INCARN, 
                                                 u4ClassTypeIndex);

    if (u4ObjectId != MPLS_DSTE_CLASS_TYPE_ROW_STATUS)
    {
        if (ClassEntry.i4DsTeClassTypeRowStatus == MPLS_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (ClassEntry.i4DsTeClassTypeRowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pTestDsTeClassType->i4DsTeClassTypeRowStatus !=
             CREATE_AND_WAIT)
    {
        if (ClassEntry.i4DsTeClassTypeRowStatus == MPLS_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (ClassEntry.i4DsTeClassTypeRowStatus != MPLS_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    switch (u4ObjectId)
    {
        case MPLS_DSTE_CLASS_TYPE_DESCRIPTION:
        {
            if (STRLEN (pTestDsTeClassType->au1DsTeClassTypeDesc)
                >= MPLS_MAX_DSTE_MAX_STRING_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case MPLS_DSTE_CLASS_TYPE_ROW_STATUS:
        {
            if ((pTestDsTeClassType->i4DsTeClassTypeRowStatus < ACTIVE) ||
                (pTestDsTeClassType->i4DsTeClassTypeRowStatus > DESTROY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            if ((pTestDsTeClassType->i4DsTeClassTypeRowStatus ==
                 CREATE_AND_GO) ||
                (pTestDsTeClassType->i4DsTeClassTypeRowStatus == NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;
        } 
        case MPLS_DSTE_CLASS_TYPE_BW_PERCENT:
        {
             if ((pTestDsTeClassType->i4DsTeClassTypeBwPercent < MPLS_ZERO) ||
                 (pTestDsTeClassType->i4DsTeClassTypeBwPercent > 
                  MPLS_MAX_CLASS_TYPE_PERCENT))
             {
                CLI_SET_ERR(MPLS_CLI_TE_ERR_UNABLE_TO_SET_CT_BW);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
             }
             
             /* Calculate the sum of all configured class type bandwidth */
             for(u4DsTeClassTypeIndex = MPLS_ZERO; u4DsTeClassTypeIndex < MPLS_MAX_CLASS_TYPE_ENTRIES; u4DsTeClassTypeIndex++)
             {
                 if ((MplsDsTeGetAllClassTypeTable(u4DsTeClassTypeIndex, &DsTeClassEntry) != SNMP_FAILURE) &&
                     (DsTeClassEntry.i4DsTeClassTypeRowStatus == ACTIVE) &&
                     (DsTeClassEntry.i4DsTeClassTypeBwPercent != MPLS_ZERO))
                 {
                     i4DsTeClassTypeTotalBwPercent += DsTeClassEntry.i4DsTeClassTypeBwPercent;   
                     MEMSET (&DsTeClassEntry, MPLS_ZERO, sizeof(tMplsDsTeClassType));
                 }                 
             }
              
             /* Sum of all class type bandwidth should not exceed than maximum class type bandwidth */
             if ((i4DsTeClassTypeTotalBwPercent != MPLS_ZERO) && 
                ((i4DsTeClassTypeTotalBwPercent += pTestDsTeClassType->i4DsTeClassTypeBwPercent) > MPLS_MAX_CLASS_TYPE_PERCENT))
             {
                 CLI_SET_ERR(MPLS_CLI_TE_ERR_ALL_CT_BW_SUM_EXCEED_MAX_LIMIT);
                 *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                 return SNMP_FAILURE;
             }
             
             break;
        }      
        default:
        {
             return SNMP_FAILURE;
        }
     }
     return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  MplsDsTeTestAllClassTypeTcMapTable
Input       :  pu4ErrorCode           - Error code
               u4ClassTypeIndex       - Class Type Index
               u4ClassTcMapIndex      - Traffic Class Index
               pSetDsTeClassTypeTcMap - Pointer to Traffic Class Map entry
               u4ObjectId             - Object ID
Description :  This function tests the Traffic class map table objects
Output      :  None 
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
MplsDsTeTestAllClassTypeTcMapTable (UINT4 *pu4ErrorCode, UINT4 u4ClassTypeIndex,
                                      UINT4 u4ClassTcMapIndex,
                                  tMplsDsTeClassTypeTcMap *pSetDsTeClassTypeTcMap,
                                  UINT4 u4ObjectId)
{
    tMplsDsTeClassTypeTcMap TeClassTypeTcMap;
    tMplsDsTeClassType ClassEntry;
    UINT1 u1RetVal = MPLS_ZERO;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof(tMplsDsTeClassType));
    MEMSET (&TeClassTypeTcMap, MPLS_ZERO, sizeof(tMplsDsTeClassTypeTcMap));

    if ((MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP) ||
        (MPLS_DSTE_STATUS (MPLS_DEF_INCARN) != MPLS_DSTE_STATUS_DISABLE)||
        (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4ClassTypeIndex >= MPLS_MAX_CLASS_TYPE_ENTRIES) || 
        (u4ClassTcMapIndex >= MPLS_MAX_TRAFFIC_CLASS_ENTRIES))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    ClassEntry = MPLS_DIFFSERV_CLASS_TYPE_ENTRY (MPLS_DEF_INCARN, 
                                                 u4ClassTypeIndex);
    TeClassTypeTcMap = MPLS_DIFFSERV_TRAFFIC_CLASS_MAP (MPLS_DEF_INCARN, 
                                           u4ClassTypeIndex, u4ClassTcMapIndex);

    if (u4ObjectId != MPLS_DSTE_TC_MAP_ROW_STATUS)
    {
        if (TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus == MPLS_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus !=
             CREATE_AND_WAIT)
    {
        if (TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus == MPLS_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus != MPLS_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case MPLS_DSTE_TC_MAP_TYPE:
         {    
             u1RetVal = MplsValidateDiffServTcType (pSetDsTeClassTypeTcMap->u4DsTeTcType);
         
             if(u1RetVal == FALSE)
             {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
             }
            break;
        }
        case MPLS_DSTE_TC_MAP_DESC:
        {
            if (STRLEN (pSetDsTeClassTypeTcMap->au1DsTeClassTypeToTcDesc) 
                 >= MPLS_MAX_DSTE_MAX_STRING_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case MPLS_DSTE_TC_MAP_ROW_STATUS:
        {
            if ((pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus < ACTIVE) ||
                (pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus > DESTROY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            if ((pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus == CREATE_AND_GO) ||
                (pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus == NOT_READY))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (ClassEntry.i4DsTeClassTypeRowStatus != ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;   
        }
        default:
        {
            return SNMP_FAILURE;
        }
     }
     return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  MplsDsTeTestAllTeclassMapTable
Input       :  pu4ErrorCode           - Error code
               u4ClassTypeIndex       - Class Type Index
               u4ClassTeClassPrio     - TE class priority
               pSetDsTeClassPrio      - Pointer to TE Class Map entry
               u4ObjectId             - Object ID
Description :  This function tests the 
Output      :  None 
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
                                               
INT1
MplsDsTeTestAllTeclassMapTable (UINT4 *pu4ErrorCode, UINT4 u4ClassTypeIndex,
                                      UINT4 u4ClassTeClassPrio,
                                  tMplsDsTeClassPrio *pSetDsTeClassPrio,
                                  UINT4 u4ObjectId)
{
    tMplsDsTeClassPrio TeClassTePrio;
    tMplsDsTeClassType TeClassType;

    MEMSET (&TeClassTePrio, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));
    MEMSET (&TeClassType, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    if ((MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP) ||
        (MPLS_DSTE_STATUS (MPLS_DEF_INCARN) != MPLS_DSTE_STATUS_DISABLE)||
        (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((u4ClassTypeIndex >= MPLS_MAX_CLASS_TYPE_ENTRIES) || 
        (u4ClassTeClassPrio >= MPLS_MAX_TE_CLASS_ENTRIES))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    TeClassType = MPLS_DIFFSERV_CLASS_TYPE_ENTRY (MPLS_DEF_INCARN, 
                                                 u4ClassTypeIndex);
    TeClassTePrio = MPLS_DIFFSERV_TE_CLASS_MAP (MPLS_DEF_INCARN, 
                                           u4ClassTypeIndex, u4ClassTeClassPrio);

    if (u4ObjectId != MPLS_DSTE_TE_CLASS_ROW_STATUS)
    {
        if (TeClassTePrio.i4DsTeCRowStatus == MPLS_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (TeClassTePrio.i4DsTeCRowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pSetDsTeClassPrio->i4DsTeCRowStatus !=
             CREATE_AND_WAIT)
    {
        if (TeClassTePrio.i4DsTeCRowStatus == MPLS_ZERO)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (TeClassTePrio.i4DsTeCRowStatus != MPLS_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case MPLS_DSTE_TE_CLASS_DESC:
        {
            if (STRLEN (pSetDsTeClassPrio->au1DsTeCDesc) >= 
                 MPLS_MAX_DSTE_MAX_STRING_LEN)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case MPLS_DSTE_TE_CLASS_NUMBER:
        {
            if (pSetDsTeClassPrio->u4TeClassNumber >= MPLS_MAX_TE_CLASS_ENTRIES) 
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case MPLS_DSTE_TE_CLASS_ROW_STATUS:
        {
            if ((pSetDsTeClassPrio->i4DsTeCRowStatus < ACTIVE) ||
                (pSetDsTeClassPrio->i4DsTeCRowStatus > DESTROY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            if (pSetDsTeClassPrio->i4DsTeCRowStatus == CREATE_AND_GO ||
                pSetDsTeClassPrio->i4DsTeCRowStatus == NOT_READY)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            if (TeClassType.i4DsTeClassTypeRowStatus != ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        default:
        {
            return SNMP_FAILURE;
        }
    }
     return SNMP_SUCCESS;
}

