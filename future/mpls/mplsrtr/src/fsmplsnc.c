
/* $Id: fsmplsnc.c,v 1.2 2016/09/29 09:51:10 siva Exp $
   ISS Wrapper module
   module Aricent-MPLS-MIB

*/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmplslw.h"
# include  "fsmplsnc.h"
# include  "ldpext.h"
# include  "rpteext.h"
# include  "mplssize.h"
# include  "l2vpextn.h"

/********************************************************************
 * FUNCTION NcFsMplsAdminStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsAdminStatusSet (
		INT4 i4FsMplsAdminStatus )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsAdminStatus(
			i4FsMplsAdminStatus);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsAdminStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsAdminStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsAdminStatusTest (UINT4 *pu4Error,
		INT4 i4FsMplsAdminStatus )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsAdminStatus(pu4Error,
			i4FsMplsAdminStatus);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsAdminStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsQosPolicySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsQosPolicySet (
		INT4 i4FsMplsQosPolicy )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsQosPolicy(
			i4FsMplsQosPolicy);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsQosPolicySet */

/********************************************************************
 * FUNCTION NcFsMplsQosPolicyTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsQosPolicyTest (UINT4 *pu4Error,
		INT4 i4FsMplsQosPolicy )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsQosPolicy(pu4Error,
			i4FsMplsQosPolicy);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsQosPolicyTest */

/********************************************************************
 * FUNCTION NcFsMplsFmDebugLevelSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsFmDebugLevelSet (
		INT4 i4FsMplsFmDebugLevel )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsFmDebugLevel(
			i4FsMplsFmDebugLevel);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsFmDebugLevelSet */

/********************************************************************
 * FUNCTION NcFsMplsFmDebugLevelTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsFmDebugLevelTest (UINT4 *pu4Error,
		INT4 i4FsMplsFmDebugLevel )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsFmDebugLevel(pu4Error,
			i4FsMplsFmDebugLevel);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsFmDebugLevelTest */

/********************************************************************
 * FUNCTION NcFsMplsTeDebugLevelSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTeDebugLevelSet (
		UINT4 u4FsMplsTeDebugLevel )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsTeDebugLevel(
			u4FsMplsTeDebugLevel);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsTeDebugLevelSet */

/********************************************************************
 * FUNCTION NcFsMplsTeDebugLevelTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTeDebugLevelTest (UINT4 *pu4Error,
		UINT4 u4FsMplsTeDebugLevel )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsTeDebugLevel(pu4Error,
			u4FsMplsTeDebugLevel);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsTeDebugLevelTest */

/********************************************************************
 * FUNCTION NcFsMplsLsrLabelAllocationMethodSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLsrLabelAllocationMethodSet (
		INT4 i4FsMplsLsrLabelAllocationMethod )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsLsrLabelAllocationMethod(
			i4FsMplsLsrLabelAllocationMethod);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLsrLabelAllocationMethodSet */

/********************************************************************
 * FUNCTION NcFsMplsLsrLabelAllocationMethodTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLsrLabelAllocationMethodTest (UINT4 *pu4Error,
		INT4 i4FsMplsLsrLabelAllocationMethod )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsLsrLabelAllocationMethod(pu4Error,
			i4FsMplsLsrLabelAllocationMethod);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLsrLabelAllocationMethodTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspPreConfExpPhbMapIndexSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspPreConfExpPhbMapIndexSet (
		INT4 i4FsMplsDiffServElspPreConfExpPhbMapIndex )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsDiffServElspPreConfExpPhbMapIndex(
			i4FsMplsDiffServElspPreConfExpPhbMapIndex);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsDiffServElspPreConfExpPhbMapIndexSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspPreConfExpPhbMapIndexTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspPreConfExpPhbMapIndexTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServElspPreConfExpPhbMapIndex )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsDiffServElspPreConfExpPhbMapIndex(pu4Error,
			i4FsMplsDiffServElspPreConfExpPhbMapIndex);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsDiffServElspPreConfExpPhbMapIndexTest */

/********************************************************************
 * FUNCTION NcFsMplsLdpLsrIdSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpLsrIdSet (
		UINT1 *pau1FsMplsLdpLsrId )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	tSNMP_OCTET_STRING_TYPE FsMplsLdpLsrId;
	MEMSET (&FsMplsLdpLsrId, 0,  sizeof (FsMplsLdpLsrId));

	FsMplsLdpLsrId.i4_Length = (INT4) STRLEN (pau1FsMplsLdpLsrId);
	FsMplsLdpLsrId.pu1_OctetList = pau1FsMplsLdpLsrId;

	i1RetVal = nmhSetFsMplsLdpLsrId(
			&FsMplsLdpLsrId);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpLsrIdSet */

/********************************************************************
 * FUNCTION NcFsMplsLdpLsrIdTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpLsrIdTest (UINT4 *pu4Error,
		UINT1 *pau1FsMplsLdpLsrId )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsLdpLsrId;
	MEMSET (&FsMplsLdpLsrId, 0,  sizeof (FsMplsLdpLsrId));

	FsMplsLdpLsrId.i4_Length = (INT4) STRLEN (pau1FsMplsLdpLsrId);
	FsMplsLdpLsrId.pu1_OctetList = pau1FsMplsLdpLsrId;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsLdpLsrId(pu4Error,
			&FsMplsLdpLsrId);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpLsrIdTest */

/********************************************************************
 * FUNCTION NcFsMplsLdpForceOptionSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpForceOptionSet (
		INT4 i4FsMplsLdpForceOption )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsLdpForceOption(
			i4FsMplsLdpForceOption);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpForceOptionSet */

/********************************************************************
 * FUNCTION NcFsMplsLdpForceOptionTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpForceOptionTest (UINT4 *pu4Error,
		INT4 i4FsMplsLdpForceOption )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsLdpForceOption(pu4Error,
			i4FsMplsLdpForceOption);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpForceOptionTest */

/********************************************************************
 * FUNCTION NcFsMplsRsvpTeGrMaxWaitTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsRsvpTeGrMaxWaitTimeSet (
		INT4 i4FsMplsRsvpTeGrMaxWaitTime )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsRsvpTeGrMaxWaitTime(
			i4FsMplsRsvpTeGrMaxWaitTime);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsRsvpTeGrMaxWaitTimeSet */

/********************************************************************
 * FUNCTION NcFsMplsRsvpTeGrMaxWaitTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsRsvpTeGrMaxWaitTimeTest (UINT4 *pu4Error,
		INT4 i4FsMplsRsvpTeGrMaxWaitTime )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsRsvpTeGrMaxWaitTime(pu4Error,
			i4FsMplsRsvpTeGrMaxWaitTime);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsRsvpTeGrMaxWaitTimeTest */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrMaxWaitTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrMaxWaitTimeSet (
		INT4 i4FsMplsLdpGrMaxWaitTime )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsLdpGrMaxWaitTime(
			i4FsMplsLdpGrMaxWaitTime);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpGrMaxWaitTimeSet */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrMaxWaitTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrMaxWaitTimeTest (UINT4 *pu4Error,
		INT4 i4FsMplsLdpGrMaxWaitTime )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsLdpGrMaxWaitTime(pu4Error,
			i4FsMplsLdpGrMaxWaitTime);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpGrMaxWaitTimeTest */

/********************************************************************
 * FUNCTION NcFsMplsMaxIfTableEntriesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxIfTableEntriesGet (
		UINT4 *pu4FsMplsMaxIfTableEntries )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxIfTableEntries(
			pu4FsMplsMaxIfTableEntries );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxIfTableEntriesGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxFTNEntriesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxFTNEntriesGet (
		UINT4 *pu4FsMplsMaxFTNEntries )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxFTNEntries(
			pu4FsMplsMaxFTNEntries );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxFTNEntriesGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxInSegmentEntriesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxInSegmentEntriesGet (
		UINT4 *pu4FsMplsMaxInSegmentEntries )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxInSegmentEntries(
			pu4FsMplsMaxInSegmentEntries );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxInSegmentEntriesGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxOutSegmentEntriesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxOutSegmentEntriesGet (
		UINT4 *pu4FsMplsMaxOutSegmentEntries )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxOutSegmentEntries(
			pu4FsMplsMaxOutSegmentEntries );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxOutSegmentEntriesGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxXCEntriesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxXCEntriesGet (
		UINT4 *pu4FsMplsMaxXCEntries )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxXCEntries(
			pu4FsMplsMaxXCEntries );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxXCEntriesGet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspMapEntriesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspMapEntriesGet (
		UINT4 *pu4FsMplsDiffServElspMapEntries )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsDiffServElspMapEntries(
			pu4FsMplsDiffServElspMapEntries );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsDiffServElspMapEntriesGet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsEntriesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsEntriesGet (
		UINT4 *pu4FsMplsDiffServParamsEntries )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsDiffServParamsEntries(
			pu4FsMplsDiffServParamsEntries );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsDiffServParamsEntriesGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxHopListsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxHopListsGet (
		UINT4 *pu4FsMplsMaxHopLists )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxHopLists(
			pu4FsMplsMaxHopLists );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxHopListsGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxPathOptPerListGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxPathOptPerListGet (
		UINT4 *pu4FsMplsMaxPathOptPerList )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxPathOptPerList(
			pu4FsMplsMaxPathOptPerList );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxPathOptPerListGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxHopsPerPathOptionGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxHopsPerPathOptionGet (
		UINT4 *pu4FsMplsMaxHopsPerPathOption )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxHopsPerPathOption(
			pu4FsMplsMaxHopsPerPathOption );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxHopsPerPathOptionGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxArHopListsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxArHopListsGet (
		UINT4 *pu4FsMplsMaxArHopLists )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxArHopLists(
			pu4FsMplsMaxArHopLists );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxArHopListsGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxHopsPerArHopListGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxHopsPerArHopListGet (
		UINT4 *pu4FsMplsMaxHopsPerArHopList )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxHopsPerArHopList(
			pu4FsMplsMaxHopsPerArHopList );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxHopsPerArHopListGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxRsvpTrfcParamsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxRsvpTrfcParamsGet (
		UINT4 *pu4FsMplsMaxRsvpTrfcParams )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxRsvpTrfcParams(
			pu4FsMplsMaxRsvpTrfcParams );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxRsvpTrfcParamsGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxCrLdpTrfcParamsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxCrLdpTrfcParamsGet (
		UINT4 *pu4FsMplsMaxCrLdpTrfcParams )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxCrLdpTrfcParams(
			pu4FsMplsMaxCrLdpTrfcParams );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxCrLdpTrfcParamsGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxDServElspsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxDServElspsGet (
		UINT4 *pu4FsMplsMaxDServElsps )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxDServElsps(
			pu4FsMplsMaxDServElsps );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxDServElspsGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxDServLlspsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxDServLlspsGet (
		UINT4 *pu4FsMplsMaxDServLlsps )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxDServLlsps(
			pu4FsMplsMaxDServLlsps );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxDServLlspsGet */

/********************************************************************
 * FUNCTION NcFsMplsMaxTnlsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsMaxTnlsGet (
		UINT4 *pu4FsMplsMaxTnls )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsMaxTnls(
			pu4FsMplsMaxTnls );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsMaxTnlsGet */

/********************************************************************
 * FUNCTION NcFsMplsLsrMaxLdpEntitiesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLsrMaxLdpEntitiesGet (
		INT4 *pi4FsMplsLsrMaxLdpEntities )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsLsrMaxLdpEntities(
			pi4FsMplsLsrMaxLdpEntities );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLsrMaxLdpEntitiesGet */

/********************************************************************
 * FUNCTION NcFsMplsLsrMaxLocalPeersGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLsrMaxLocalPeersGet (
		INT4 *pi4FsMplsLsrMaxLocalPeers )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsLsrMaxLocalPeers(
			pi4FsMplsLsrMaxLocalPeers );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLsrMaxLocalPeersGet */

/********************************************************************
 * FUNCTION NcFsMplsLsrMaxRemotePeersGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLsrMaxRemotePeersGet (
		INT4 *pi4FsMplsLsrMaxRemotePeers )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsLsrMaxRemotePeers(
			pi4FsMplsLsrMaxRemotePeers );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLsrMaxRemotePeersGet */

/********************************************************************
 * FUNCTION NcFsMplsLsrMaxIfacesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLsrMaxIfacesGet (
		INT4 *pi4FsMplsLsrMaxIfaces )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsLsrMaxIfaces(
			pi4FsMplsLsrMaxIfaces );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLsrMaxIfacesGet */

/********************************************************************
 * FUNCTION NcFsMplsLsrMaxLspsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLsrMaxLspsGet (
		INT4 *pi4FsMplsLsrMaxLsps )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsLsrMaxLsps(
			pi4FsMplsLsrMaxLsps );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLsrMaxLspsGet */

/********************************************************************
 * FUNCTION NcFsMplsLsrMaxVcMergeCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLsrMaxVcMergeCountGet (
		INT4 *pi4FsMplsLsrMaxVcMergeCount )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsLsrMaxVcMergeCount(
			pi4FsMplsLsrMaxVcMergeCount );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLsrMaxVcMergeCountGet */

/********************************************************************
 * FUNCTION NcFsMplsLsrMaxVpMergeCountGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLsrMaxVpMergeCountGet (
		INT4 *pi4FsMplsLsrMaxVpMergeCount )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsLsrMaxVpMergeCount(
			pi4FsMplsLsrMaxVpMergeCount );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLsrMaxVpMergeCountGet */

/********************************************************************
 * FUNCTION NcFsMplsLsrMaxCrlspTnlsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLsrMaxCrlspTnlsGet (
		INT4 *pi4FsMplsLsrMaxCrlspTnls )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsLsrMaxCrlspTnls(
			pi4FsMplsLsrMaxCrlspTnls );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLsrMaxCrlspTnlsGet */

/********************************************************************
 * FUNCTION NcFsMplsActiveRsvpTeTnlsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsActiveRsvpTeTnlsGet (
		INT4 *pi4FsMplsActiveRsvpTeTnls )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsActiveRsvpTeTnls(
			pi4FsMplsActiveRsvpTeTnls );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsActiveRsvpTeTnlsGet */

/********************************************************************
 * FUNCTION NcFsMplsActiveLspsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsActiveLspsGet (
		INT4 *pi4FsMplsActiveLsps )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsActiveLsps(
			pi4FsMplsActiveLsps );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsActiveLspsGet */

/********************************************************************
 * FUNCTION NcFsMplsActiveCrLspsGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsActiveCrLspsGet (
		INT4 *pi4FsMplsActiveCrLsps )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsActiveCrLsps(
			pi4FsMplsActiveCrLsps );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsActiveCrLspsGet */

/********************************************************************
 * FUNCTION NcFsMplsCrlspDebugLevelSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspDebugLevelSet (
		INT4 i4FsMplsCrlspDebugLevel )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsCrlspDebugLevel(
			i4FsMplsCrlspDebugLevel);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsCrlspDebugLevelSet */

/********************************************************************
 * FUNCTION NcFsMplsCrlspDebugLevelTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspDebugLevelTest (UINT4 *pu4Error,
		INT4 i4FsMplsCrlspDebugLevel )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsCrlspDebugLevel(pu4Error,
			i4FsMplsCrlspDebugLevel);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsCrlspDebugLevelTest */

/********************************************************************
 * FUNCTION NcFsMplsCrlspDumpTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspDumpTypeSet (
		INT4 i4FsMplsCrlspDumpType )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsCrlspDumpType(
			i4FsMplsCrlspDumpType);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsCrlspDumpTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsCrlspDumpTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspDumpTypeTest (UINT4 *pu4Error,
		INT4 i4FsMplsCrlspDumpType )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsCrlspDumpType(pu4Error,
			i4FsMplsCrlspDumpType);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsCrlspDumpTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsCrlspDumpDirectionSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspDumpDirectionSet (
		INT4 i4FsMplsCrlspDumpDirection )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsCrlspDumpDirection(
			i4FsMplsCrlspDumpDirection);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsCrlspDumpDirectionSet */

/********************************************************************
 * FUNCTION NcFsMplsCrlspDumpDirectionTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspDumpDirectionTest (UINT4 *pu4Error,
		INT4 i4FsMplsCrlspDumpDirection )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsCrlspDumpDirection(pu4Error,
			i4FsMplsCrlspDumpDirection);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsCrlspDumpDirectionTest */

/********************************************************************
 * FUNCTION NcFsMplsCrlspPersistanceSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspPersistanceSet (
		INT4 i4FsMplsCrlspPersistance )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsCrlspPersistance(
			i4FsMplsCrlspPersistance);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsCrlspPersistanceSet */

/********************************************************************
 * FUNCTION NcFsMplsCrlspPersistanceTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspPersistanceTest (UINT4 *pu4Error,
		INT4 i4FsMplsCrlspPersistance )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsCrlspPersistance(pu4Error,
			i4FsMplsCrlspPersistance);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsCrlspPersistanceTest */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnTrcFlagSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnTrcFlagSet (
		INT4 i4FsMplsL2VpnTrcFlag )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsL2VpnTrcFlag(
			i4FsMplsL2VpnTrcFlag);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnTrcFlagSet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnTrcFlagTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnTrcFlagTest (UINT4 *pu4Error,
		INT4 i4FsMplsL2VpnTrcFlag )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsL2VpnTrcFlag(pu4Error,
			i4FsMplsL2VpnTrcFlag);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnTrcFlagTest */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnCleanupIntervalSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnCleanupIntervalSet (
		INT4 i4FsMplsL2VpnCleanupInterval )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsL2VpnCleanupInterval(
			i4FsMplsL2VpnCleanupInterval);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnCleanupIntervalSet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnCleanupIntervalTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnCleanupIntervalTest (UINT4 *pu4Error,
		INT4 i4FsMplsL2VpnCleanupInterval )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsL2VpnCleanupInterval(pu4Error,
			i4FsMplsL2VpnCleanupInterval);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnCleanupIntervalTest */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnAdminStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnAdminStatusSet (
		INT4 i4FsMplsL2VpnAdminStatus )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsL2VpnAdminStatus(
			i4FsMplsL2VpnAdminStatus);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnAdminStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnAdminStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnAdminStatusTest (UINT4 *pu4Error,
		INT4 i4FsMplsL2VpnAdminStatus )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsL2VpnAdminStatus(pu4Error,
			i4FsMplsL2VpnAdminStatus);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnAdminStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsLocalCCTypesCapabilitiesSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
/*INT1 NcFsMplsLocalCCTypesCapabilitiesSet (
  ncx_list_t ncx_list_tFsMplsLocalCCTypesCapabilities )
  {

  INT1 i1RetVal;

  i1RetVal = nmhSetFsMplsLocalCCTypesCapabilities(
  ncx_list_tFsMplsLocalCCTypesCapabilities);

  return i1RetVal;


  }*/ /* NcFsMplsLocalCCTypesCapabilitiesSet */


/********************************************************************
 * FUNCTION NcFsMplsLocalCCTypesCapabilitiesTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
/*INT1 NcFsMplsLocalCCTypesCapabilitiesTest (UINT4 *pu4Error,
  ncx_list_t ncx_list_tFsMplsLocalCCTypesCapabilities )
  {

  INT1 i1RetVal;

  i1RetVal = nmhTestv2FsMplsLocalCCTypesCapabilities(pu4Error,
  ncx_list_tFsMplsLocalCCTypesCapabilities);

  return i1RetVal;


  } */ /* NcFsMplsLocalCCTypesCapabilitiesTest */

/********************************************************************
 * FUNCTION NcFsMplsLocalCVTypesCapabilitiesSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
/*INT1 NcFsMplsLocalCVTypesCapabilitiesSet (
  ncx_list_t ncx_list_tFsMplsLocalCVTypesCapabilities )
  {

  INT1 i1RetVal;

  i1RetVal = nmhSetFsMplsLocalCVTypesCapabilities(
  ncx_list_tFsMplsLocalCVTypesCapabilities);

  return i1RetVal;


  }*/ /* NcFsMplsLocalCVTypesCapabilitiesSet */

/********************************************************************
 * FUNCTION NcFsMplsLocalCVTypesCapabilitiesTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
/*INT1 NcFsMplsLocalCVTypesCapabilitiesTest (UINT4 *pu4Error,
  ncx_list_t ncx_list_tFsMplsLocalCVTypesCapabilities )
  {

  INT1 i1RetVal;

  i1RetVal = nmhTestv2FsMplsLocalCVTypesCapabilities(pu4Error,
  ncx_list_tFsMplsLocalCVTypesCapabilities);

  return i1RetVal;


  }*/ /* NcFsMplsLocalCVTypesCapabilitiesTest */

/********************************************************************
 * FUNCTION NcFsMplsRouterIDSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsRouterIDSet (
		UINT1 *pau1FsMplsRouterID )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsRouterID;
	MEMSET (&FsMplsRouterID, 0,  sizeof (FsMplsRouterID));

	FsMplsRouterID.i4_Length = (INT4) STRLEN (pau1FsMplsRouterID);
	FsMplsRouterID.pu1_OctetList = pau1FsMplsRouterID;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsRouterID(
			&FsMplsRouterID);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsRouterIDSet */

/********************************************************************
 * FUNCTION NcFsMplsRouterIDTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsRouterIDTest (UINT4 *pu4Error,
		UINT1 *pau1FsMplsRouterID )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsRouterID;
	MEMSET (&FsMplsRouterID, 0,  sizeof (FsMplsRouterID));

	FsMplsRouterID.i4_Length = (INT4) STRLEN (pau1FsMplsRouterID);
	FsMplsRouterID.pu1_OctetList = pau1FsMplsRouterID;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsRouterID(pu4Error,
			&FsMplsRouterID);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsRouterIDTest */

/********************************************************************
 * FUNCTION NcFsMplsHwCCTypeCapabilitiesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
/*INT1 NcFsMplsHwCCTypeCapabilitiesGet (
  ncx_list_t *pncx_list_tFsMplsHwCCTypeCapabilities )
  {

  INT1 i1RetVal;
  i1RetVal = nmhGetFsMplsHwCCTypeCapabilities(
  ncx_list_tFsMplsHwCCTypeCapabilities );

  return i1RetVal;


  }*/ /* NcFsMplsHwCCTypeCapabilitiesGet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnMaxPwVcEntriesSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnMaxPwVcEntriesSet (
		UINT4 u4FsMplsL2VpnMaxPwVcEntries )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsL2VpnMaxPwVcEntries(
			u4FsMplsL2VpnMaxPwVcEntries);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnMaxPwVcEntriesSet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnMaxPwVcEntriesTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnMaxPwVcEntriesTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL2VpnMaxPwVcEntries )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsL2VpnMaxPwVcEntries(pu4Error,
			u4FsMplsL2VpnMaxPwVcEntries);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnMaxPwVcEntriesTest */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnMaxPwVcMplsEntriesSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnMaxPwVcMplsEntriesSet (
		UINT4 u4FsMplsL2VpnMaxPwVcMplsEntries )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsL2VpnMaxPwVcMplsEntries(
			u4FsMplsL2VpnMaxPwVcMplsEntries);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnMaxPwVcMplsEntriesSet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnMaxPwVcMplsEntriesTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnMaxPwVcMplsEntriesTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL2VpnMaxPwVcMplsEntries )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsL2VpnMaxPwVcMplsEntries(pu4Error,
			u4FsMplsL2VpnMaxPwVcMplsEntries);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnMaxPwVcMplsEntriesTest */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnMaxPwVcMplsInOutEntriesSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnMaxPwVcMplsInOutEntriesSet (
		UINT4 u4FsMplsL2VpnMaxPwVcMplsInOutEntries )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsL2VpnMaxPwVcMplsInOutEntries(
			u4FsMplsL2VpnMaxPwVcMplsInOutEntries);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnMaxPwVcMplsInOutEntriesSet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnMaxPwVcMplsInOutEntriesTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnMaxPwVcMplsInOutEntriesTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL2VpnMaxPwVcMplsInOutEntries )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsL2VpnMaxPwVcMplsInOutEntries(pu4Error,
			u4FsMplsL2VpnMaxPwVcMplsInOutEntries);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnMaxPwVcMplsInOutEntriesTest */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnMaxEthernetPwVcsSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnMaxEthernetPwVcsSet (
		UINT4 u4FsMplsL2VpnMaxEthernetPwVcs )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsL2VpnMaxEthernetPwVcs(
			u4FsMplsL2VpnMaxEthernetPwVcs);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnMaxEthernetPwVcsSet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnMaxEthernetPwVcsTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnMaxEthernetPwVcsTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL2VpnMaxEthernetPwVcs )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsL2VpnMaxEthernetPwVcs(pu4Error,
			u4FsMplsL2VpnMaxEthernetPwVcs);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnMaxEthernetPwVcsTest */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnMaxPwVcEnetEntriesSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnMaxPwVcEnetEntriesSet (
		UINT4 u4FsMplsL2VpnMaxPwVcEnetEntries )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsL2VpnMaxPwVcEnetEntries(
			u4FsMplsL2VpnMaxPwVcEnetEntries);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnMaxPwVcEnetEntriesSet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnMaxPwVcEnetEntriesTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnMaxPwVcEnetEntriesTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL2VpnMaxPwVcEnetEntries )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsL2VpnMaxPwVcEnetEntries(pu4Error,
			u4FsMplsL2VpnMaxPwVcEnetEntries);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnMaxPwVcEnetEntriesTest */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnActivePwVcEntriesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnActivePwVcEntriesGet (
		UINT4 *pu4FsMplsL2VpnActivePwVcEntries )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsL2VpnActivePwVcEntries(
			pu4FsMplsL2VpnActivePwVcEntries );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnActivePwVcEntriesGet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnActivePwVcMplsEntriesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnActivePwVcMplsEntriesGet (
		UINT4 *pu4FsMplsL2VpnActivePwVcMplsEntries )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsL2VpnActivePwVcMplsEntries(
			pu4FsMplsL2VpnActivePwVcMplsEntries );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnActivePwVcMplsEntriesGet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnActivePwVcEnetEntriesGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnActivePwVcEnetEntriesGet (
		UINT4 *pu4FsMplsL2VpnActivePwVcEnetEntries )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsL2VpnActivePwVcEnetEntries(
			pu4FsMplsL2VpnActivePwVcEnetEntries );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnActivePwVcEnetEntriesGet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnNoOfPwVcEntriesCreatedGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnNoOfPwVcEntriesCreatedGet (
		UINT4 *pu4FsMplsL2VpnNoOfPwVcEntriesCreated )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsL2VpnNoOfPwVcEntriesCreated(
			pu4FsMplsL2VpnNoOfPwVcEntriesCreated );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnNoOfPwVcEntriesCreatedGet */

/********************************************************************
 * FUNCTION NcFsMplsL2VpnNoOfPwVcEntriesDeletedGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL2VpnNoOfPwVcEntriesDeletedGet (
		UINT4 *pu4FsMplsL2VpnNoOfPwVcEntriesDeleted )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsL2VpnNoOfPwVcEntriesDeleted(
			pu4FsMplsL2VpnNoOfPwVcEntriesDeleted );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsL2VpnNoOfPwVcEntriesDeletedGet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspInfoListIndexNextGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspInfoListIndexNextGet (
		INT4 *pi4FsMplsDiffServElspInfoListIndexNext )
{

	INT1 i1RetVal;
	MPLS_CMN_LOCK ();
	i1RetVal = nmhGetFsMplsDiffServElspInfoListIndexNext(
			pi4FsMplsDiffServElspInfoListIndexNext );

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsDiffServElspInfoListIndexNextGet */

/********************************************************************
 * FUNCTION NcFsMplsTnlModelSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTnlModelSet (
		INT4 i4FsMplsTnlModel )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsTnlModel(
			i4FsMplsTnlModel);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsTnlModelSet */

/********************************************************************
 * FUNCTION NcFsMplsTnlModelTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTnlModelTest (UINT4 *pu4Error,
		INT4 i4FsMplsTnlModel )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsTnlModel(pu4Error,
			i4FsMplsTnlModel);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsTnlModelTest */

/********************************************************************
 * FUNCTION NcFsMplsRsrcMgmtTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsRsrcMgmtTypeSet (
		INT4 i4FsMplsRsrcMgmtType )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsRsrcMgmtType(
			i4FsMplsRsrcMgmtType);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsRsrcMgmtTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsRsrcMgmtTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsRsrcMgmtTypeTest (UINT4 *pu4Error,
		INT4 i4FsMplsRsrcMgmtType )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsRsrcMgmtType(pu4Error,
			i4FsMplsRsrcMgmtType);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsRsrcMgmtTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsTTLValSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTTLValSet (
		INT4 i4FsMplsTTLVal )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsTTLVal(
			i4FsMplsTTLVal);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsTTLValSet */

/********************************************************************
 * FUNCTION NcFsMplsTTLValTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTTLValTest (UINT4 *pu4Error,
		INT4 i4FsMplsTTLVal )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhTestv2FsMplsTTLVal(pu4Error,
			i4FsMplsTTLVal);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsTTLValTest */

/********************************************************************
 * FUNCTION NcFsMplsDsTeStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeStatusSet (
		INT4 i4FsMplsDsTeStatus )
{

	INT1 i1RetVal;

	MPLS_CMN_LOCK ();
	i1RetVal = nmhSetFsMplsDsTeStatus(
			i4FsMplsDsTeStatus);

	MPLS_CMN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsDsTeStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsDsTeStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeStatusTest (UINT4 *pu4Error,
		INT4 i4FsMplsDsTeStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDsTeStatus(pu4Error,
			i4FsMplsDsTeStatus);

	return i1RetVal;


} /* NcFsMplsDsTeStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsSimulateFailureSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsSimulateFailureSet (
		INT4 i4FsMplsSimulateFailure )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsSimulateFailure(
			i4FsMplsSimulateFailure);

	return i1RetVal;


} /* NcFsMplsSimulateFailureSet */

/********************************************************************
 * FUNCTION NcFsMplsSimulateFailureTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsSimulateFailureTest (UINT4 *pu4Error,
		INT4 i4FsMplsSimulateFailure )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsSimulateFailure(pu4Error,
			i4FsMplsSimulateFailure);

	return i1RetVal;


} /* NcFsMplsSimulateFailureTest */

/********************************************************************
 * FUNCTION NcFsMplsiTTLValGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsiTTLValGet (
		INT4 *pi4FsMplsiTTLVal )
{

	INT1 i1RetVal;
	i1RetVal = nmhGetFsMplsiTTLVal(
			pi4FsMplsiTTLVal );

	return i1RetVal;


} /* NcFsMplsiTTLValGet */

/********************************************************************
 * FUNCTION NcFsMplsOTTLValGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsOTTLValGet (
		INT4 *pi4FsMplsOTTLVal )
{

	INT1 i1RetVal;
	i1RetVal = nmhGetFsMplsOTTLVal(
			pi4FsMplsOTTLVal );

	return i1RetVal;


} /* NcFsMplsOTTLValGet */

/********************************************************************
 * FUNCTION NcFsMplsprviTTLValGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsprviTTLValGet (
		INT4 *pi4FsMplsprviTTLVal )
{

	INT1 i1RetVal;
	i1RetVal = nmhGetFsMplsprviTTLVal(
			pi4FsMplsprviTTLVal );

	return i1RetVal;


} /* NcFsMplsprviTTLValGet */

/********************************************************************
 * FUNCTION NcFsMplsprvOTTLValGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsprvOTTLValGet (
		INT4 *pi4FsMplsprvOTTLVal )
{

	INT1 i1RetVal;
	i1RetVal = nmhGetFsMplsprvOTTLVal(
			pi4FsMplsprvOTTLVal );

	return i1RetVal;


} /* NcFsMplsprvOTTLValGet */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrCapabilitySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrCapabilitySet (
		INT4 i4FsMplsLdpGrCapability )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsLdpGrCapability(
			i4FsMplsLdpGrCapability);

	return i1RetVal;


} /* NcFsMplsLdpGrCapabilitySet */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrCapabilityTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrCapabilityTest (UINT4 *pu4Error,
		INT4 i4FsMplsLdpGrCapability )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK ();
	i1RetVal = nmhTestv2FsMplsLdpGrCapability(pu4Error,
			i4FsMplsLdpGrCapability);
	MPLS_LDP_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpGrCapabilityTest */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrForwardEntryHoldTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrForwardEntryHoldTimeSet (
		INT4 i4FsMplsLdpGrForwardEntryHoldTime )
{

	INT1 i1RetVal;

	MPLS_LDP_LOCK ();
	i1RetVal = nmhSetFsMplsLdpGrForwardEntryHoldTime(
			i4FsMplsLdpGrForwardEntryHoldTime);

	MPLS_LDP_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpGrForwardEntryHoldTimeSet */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrForwardEntryHoldTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrForwardEntryHoldTimeTest (UINT4 *pu4Error,
		INT4 i4FsMplsLdpGrForwardEntryHoldTime )
{

	INT1 i1RetVal;

	MPLS_LDP_LOCK ();
	i1RetVal = nmhTestv2FsMplsLdpGrForwardEntryHoldTime(pu4Error,
			i4FsMplsLdpGrForwardEntryHoldTime);

	MPLS_LDP_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpGrForwardEntryHoldTimeTest */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrMaxRecoveryTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrMaxRecoveryTimeSet (
		INT4 i4FsMplsLdpGrMaxRecoveryTime )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK ();
	i1RetVal = nmhSetFsMplsLdpGrMaxRecoveryTime(
			i4FsMplsLdpGrMaxRecoveryTime);
	MPLS_LDP_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpGrMaxRecoveryTimeSet */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrMaxRecoveryTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrMaxRecoveryTimeTest (UINT4 *pu4Error,
		INT4 i4FsMplsLdpGrMaxRecoveryTime )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK ();
	i1RetVal = nmhTestv2FsMplsLdpGrMaxRecoveryTime(pu4Error,
			i4FsMplsLdpGrMaxRecoveryTime);
	MPLS_LDP_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpGrMaxRecoveryTimeTest */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrNeighborLivenessTimeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrNeighborLivenessTimeSet (
		INT4 i4FsMplsLdpGrNeighborLivenessTime )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK ();
	i1RetVal = nmhSetFsMplsLdpGrNeighborLivenessTime(
			i4FsMplsLdpGrNeighborLivenessTime);
	MPLS_LDP_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpGrNeighborLivenessTimeSet */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrNeighborLivenessTimeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrNeighborLivenessTimeTest (UINT4 *pu4Error,
		INT4 i4FsMplsLdpGrNeighborLivenessTime )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK ();
	i1RetVal = nmhTestv2FsMplsLdpGrNeighborLivenessTime(pu4Error,
			i4FsMplsLdpGrNeighborLivenessTime);
	MPLS_LDP_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsLdpGrNeighborLivenessTimeTest */

/********************************************************************
 * FUNCTION NcFsMplsLdpGrProgressStatusGet
 * 
 * Wrapper for nmhGet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpGrProgressStatusGet (
		INT4 *pi4FsMplsLdpGrProgressStatus )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK();
	i1RetVal = nmhGetFsMplsLdpGrProgressStatus(
			pi4FsMplsLdpGrProgressStatus );

	MPLS_LDP_UNLOCK();
	return i1RetVal;


} /* NcFsMplsLdpGrProgressStatusGet */

/********************************************************************
 * FUNCTION NcFsMplsLdpConfigurationSequenceTLVEnableSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpConfigurationSequenceTLVEnableSet (
		INT4 i4FsMplsLdpConfigurationSequenceTLVEnable )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK();

	i1RetVal = nmhSetFsMplsLdpConfigurationSequenceTLVEnable(
			i4FsMplsLdpConfigurationSequenceTLVEnable);
	MPLS_LDP_UNLOCK();

	return i1RetVal;


} /* NcFsMplsLdpConfigurationSequenceTLVEnableSet */

/********************************************************************
 * FUNCTION NcFsMplsLdpConfigurationSequenceTLVEnableTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsLdpConfigurationSequenceTLVEnableTest (UINT4 *pu4Error,
		INT4 i4FsMplsLdpConfigurationSequenceTLVEnable )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK();
	i1RetVal = nmhTestv2FsMplsLdpConfigurationSequenceTLVEnable(pu4Error,
			i4FsMplsLdpConfigurationSequenceTLVEnable);

	MPLS_LDP_UNLOCK();
	return i1RetVal;


} /* NcFsMplsLdpConfigurationSequenceTLVEnableTest */

/********************************************************************
 * FUNCTION NcFsMplsCrlspTnlRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspTnlRowStatusSet (
		UINT4 u4FsMplsCrlspTnlIndex,
		INT4 i4FsMplsCrlspTnlRowStatus )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK();
	i1RetVal = nmhSetFsMplsCrlspTnlRowStatus(
			u4FsMplsCrlspTnlIndex,
			i4FsMplsCrlspTnlRowStatus);
	MPLS_LDP_UNLOCK();
	return i1RetVal;


} /* NcFsMplsCrlspTnlRowStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsCrlspTnlRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspTnlRowStatusTest (UINT4 *pu4Error,
		UINT4 u4FsMplsCrlspTnlIndex,
		INT4 i4FsMplsCrlspTnlRowStatus )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK();
	i1RetVal = nmhTestv2FsMplsCrlspTnlRowStatus(pu4Error,
			u4FsMplsCrlspTnlIndex,
			i4FsMplsCrlspTnlRowStatus);
	MPLS_LDP_UNLOCK();
	return i1RetVal;


} /* NcFsMplsCrlspTnlRowStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsCrlspTnlStorageTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspTnlStorageTypeSet (
		UINT4 u4FsMplsCrlspTnlIndex,
		INT4 i4FsMplsCrlspTnlStorageType )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK();
	i1RetVal = nmhSetFsMplsCrlspTnlStorageType(
			u4FsMplsCrlspTnlIndex,
			i4FsMplsCrlspTnlStorageType);
	MPLS_LDP_UNLOCK();
	return i1RetVal;


} /* NcFsMplsCrlspTnlStorageTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsCrlspTnlStorageTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsCrlspTnlStorageTypeTest (UINT4 *pu4Error,
		UINT4 u4FsMplsCrlspTnlIndex,
		INT4 i4FsMplsCrlspTnlStorageType )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK();
	i1RetVal = nmhTestv2FsMplsCrlspTnlStorageType(pu4Error,
			u4FsMplsCrlspTnlIndex,
			i4FsMplsCrlspTnlStorageType);
	MPLS_LDP_UNLOCK();
	return i1RetVal;


} /* NcFsMplsCrlspTnlStorageTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsVsiSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsVsiSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsMplsVplsVsi )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK();
	i1RetVal = nmhSetFsMplsVplsVsi(
			u4FsMplsVplsInstanceIndex,
			i4FsMplsVplsVsi);
	MPLS_LDP_UNLOCK();
	return i1RetVal;


} /* NcFsMplsVplsVsiSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsVsiTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsVsiTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsMplsVplsVsi )
{

	INT1 i1RetVal;
	MPLS_LDP_LOCK();
	i1RetVal = nmhTestv2FsMplsVplsVsi(pu4Error,
			u4FsMplsVplsInstanceIndex,
			i4FsMplsVplsVsi);
	MPLS_LDP_UNLOCK();
	return i1RetVal;


} /* NcFsMplsVplsVsiTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsVpnIdSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsVpnIdSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT1 *pau1FsMplsVplsVpnId )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsVplsVpnId;
	MEMSET (&FsMplsVplsVpnId, 0,  sizeof (FsMplsVplsVpnId));

	FsMplsVplsVpnId.i4_Length = (INT4) STRLEN (pau1FsMplsVplsVpnId);
	FsMplsVplsVpnId.pu1_OctetList = pau1FsMplsVplsVpnId;
	MPLS_LDP_LOCK();
	i1RetVal = nmhSetFsMplsVplsVpnId(
			u4FsMplsVplsInstanceIndex,
			&FsMplsVplsVpnId);
	MPLS_LDP_UNLOCK();
	return i1RetVal;


} /* NcFsMplsVplsVpnIdSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsVpnIdTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsVpnIdTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT1 *pau1FsMplsVplsVpnId )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsVplsVpnId;
	MEMSET (&FsMplsVplsVpnId, 0,  sizeof (FsMplsVplsVpnId));

	FsMplsVplsVpnId.i4_Length = (INT4) STRLEN (pau1FsMplsVplsVpnId);
	FsMplsVplsVpnId.pu1_OctetList = pau1FsMplsVplsVpnId;
	MPLS_LDP_LOCK();	
	i1RetVal = nmhTestv2FsMplsVplsVpnId(pu4Error,
			u4FsMplsVplsInstanceIndex,
			&FsMplsVplsVpnId);
	MPLS_LDP_UNLOCK();
	return i1RetVal;


} /* NcFsMplsVplsVpnIdTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsNameSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsNameSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT1 *pFsMplsVplsName )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsVplsName;
	MEMSET (&FsMplsVplsName, 0,  sizeof (FsMplsVplsName));

	FsMplsVplsName.i4_Length = (INT4) STRLEN (pFsMplsVplsName);
	FsMplsVplsName.pu1_OctetList = pFsMplsVplsName;

	i1RetVal = nmhSetFsMplsVplsName(
			u4FsMplsVplsInstanceIndex,
			&FsMplsVplsName);

	return i1RetVal;


} /* NcFsMplsVplsNameSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsNameTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsNameTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT1 *pFsMplsVplsName )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsVplsName;
	MEMSET (&FsMplsVplsName, 0,  sizeof (FsMplsVplsName));

	FsMplsVplsName.i4_Length = (INT4) STRLEN (pFsMplsVplsName);
	FsMplsVplsName.pu1_OctetList = pFsMplsVplsName;

	i1RetVal = nmhTestv2FsMplsVplsName(pu4Error,
			u4FsMplsVplsInstanceIndex,
			&FsMplsVplsName);

	return i1RetVal;


} /* NcFsMplsVplsNameTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsDescrSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsDescrSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT1 *pFsMplsVplsDescr )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsVplsDescr;
	MEMSET (&FsMplsVplsDescr, 0,  sizeof (FsMplsVplsDescr));

	FsMplsVplsDescr.i4_Length = (INT4) STRLEN (pFsMplsVplsDescr);
	FsMplsVplsDescr.pu1_OctetList = pFsMplsVplsDescr;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhSetFsMplsVplsDescr(
			u4FsMplsVplsInstanceIndex,
			&FsMplsVplsDescr);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsVplsDescrSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsDescrTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsDescrTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT1 *pFsMplsVplsDescr )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsVplsDescr;
	MEMSET (&FsMplsVplsDescr, 0,  sizeof (FsMplsVplsDescr));

	FsMplsVplsDescr.i4_Length = (INT4) STRLEN (pFsMplsVplsDescr);
	FsMplsVplsDescr.pu1_OctetList = pFsMplsVplsDescr;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhTestv2FsMplsVplsDescr(pu4Error,
			u4FsMplsVplsInstanceIndex,
			&FsMplsVplsDescr);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsVplsDescrTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsFdbHighWatermarkSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsFdbHighWatermarkSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT4 u4FsMplsVplsFdbHighWatermark )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhSetFsMplsVplsFdbHighWatermark(
			u4FsMplsVplsInstanceIndex,
			u4FsMplsVplsFdbHighWatermark);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsVplsFdbHighWatermarkSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsFdbHighWatermarkTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsFdbHighWatermarkTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT4 u4FsMplsVplsFdbHighWatermark )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhTestv2FsMplsVplsFdbHighWatermark(pu4Error,
			u4FsMplsVplsInstanceIndex,
			u4FsMplsVplsFdbHighWatermark);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsVplsFdbHighWatermarkTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsFdbLowWatermarkSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsFdbLowWatermarkSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT4 u4FsMplsVplsFdbLowWatermark )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhSetFsMplsVplsFdbLowWatermark(
			u4FsMplsVplsInstanceIndex,
			u4FsMplsVplsFdbLowWatermark);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsVplsFdbLowWatermarkSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsFdbLowWatermarkTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsFdbLowWatermarkTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT4 u4FsMplsVplsFdbLowWatermark )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhTestv2FsMplsVplsFdbLowWatermark(pu4Error,
			u4FsMplsVplsInstanceIndex,
			u4FsMplsVplsFdbLowWatermark);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsVplsFdbLowWatermarkTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsRowStatusSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsMplsVplsRowStatus )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhSetFsMplsVplsRowStatus(
			u4FsMplsVplsInstanceIndex,
			i4FsMplsVplsRowStatus);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsVplsRowStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsRowStatusTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsMplsVplsRowStatus )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhTestv2FsMplsVplsRowStatus(pu4Error,
			u4FsMplsVplsInstanceIndex,
			i4FsMplsVplsRowStatus);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsVplsRowStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsL2MapFdbIdSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsL2MapFdbIdSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsMplsVplsL2MapFdbId )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhSetFsMplsVplsL2MapFdbId(
			u4FsMplsVplsInstanceIndex,
			i4FsMplsVplsL2MapFdbId);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsVplsL2MapFdbIdSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsL2MapFdbIdTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsL2MapFdbIdTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsMplsVplsL2MapFdbId )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhTestv2FsMplsVplsL2MapFdbId(pu4Error,
			u4FsMplsVplsInstanceIndex,
			i4FsMplsVplsL2MapFdbId);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsMplsVplsL2MapFdbIdTest */

/********************************************************************
 * FUNCTION NcFsmplsVplsMtuSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsmplsVplsMtuSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT4 u4FsmplsVplsMtu )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhSetFsmplsVplsMtu(
			u4FsMplsVplsInstanceIndex,
			u4FsmplsVplsMtu);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsmplsVplsMtuSet */

/********************************************************************
 * FUNCTION NcFsmplsVplsMtuTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsmplsVplsMtuTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		UINT4 u4FsmplsVplsMtu )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhTestv2FsmplsVplsMtu(pu4Error,
			u4FsMplsVplsInstanceIndex,
			u4FsmplsVplsMtu);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsmplsVplsMtuTest */

/********************************************************************
 * FUNCTION NcFsmplsVplsStorageTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsmplsVplsStorageTypeSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsmplsVplsStorageType )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhSetFsmplsVplsStorageType(
			u4FsMplsVplsInstanceIndex,
			i4FsmplsVplsStorageType);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsmplsVplsStorageTypeSet */

/********************************************************************
 * FUNCTION NcFsmplsVplsStorageTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsmplsVplsStorageTypeTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsmplsVplsStorageType )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhTestv2FsmplsVplsStorageType(pu4Error,
			u4FsMplsVplsInstanceIndex,
			i4FsmplsVplsStorageType);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsmplsVplsStorageTypeTest */

/********************************************************************
 * FUNCTION NcFsmplsVplsSignalingTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsmplsVplsSignalingTypeSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsmplsVplsSignalingType )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhSetFsmplsVplsSignalingType(
			u4FsMplsVplsInstanceIndex,
			i4FsmplsVplsSignalingType);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsmplsVplsSignalingTypeSet */

/********************************************************************
 * FUNCTION NcFsmplsVplsSignalingTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsmplsVplsSignalingTypeTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsmplsVplsSignalingType )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhTestv2FsmplsVplsSignalingType(pu4Error,
			u4FsMplsVplsInstanceIndex,
			i4FsmplsVplsSignalingType);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsmplsVplsSignalingTypeTest */

/********************************************************************
  i1RetVal = NcFsmplsVplsControlWordTest(&u4ErrorCode,
  u4FsMplsVplsInstanceIndex,
  i4FsmplsVplsControlWord);
  u4FsMplsVplsInstanceIndex,

  if (i1RetVal == NETCONF_SNMP_FAILURE)
  {
  res = ERR_NCX_OPERATION_FAILED;
  return res;
  }

 * FUNCTION NcFsmplsVplsControlWordSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsmplsVplsControlWordSet (
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsmplsVplsControlWord )
{

	INT1 i1RetVal;
	MPLS_L2VPN_LOCK ();
	i1RetVal = nmhSetFsmplsVplsControlWord(
			u4FsMplsVplsInstanceIndex,
			i4FsmplsVplsControlWord);
	MPLS_L2VPN_UNLOCK ();
	return i1RetVal;


} /* NcFsmplsVplsControlWordSet */

/********************************************************************
 * FUNCTION NcFsmplsVplsControlWordTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsmplsVplsControlWordTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsInstanceIndex,
		INT4 i4FsmplsVplsControlWord )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsmplsVplsControlWord(pu4Error,
			u4FsMplsVplsInstanceIndex,
			i4FsmplsVplsControlWord);

	return i1RetVal;


} /* NcFsmplsVplsControlWordTest */

/********************************************************************
 * FUNCTION NcFsMplsPortBundleStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsPortBundleStatusSet (
		INT4 i4IfIndex,
		INT4 i4FsMplsPortBundleStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsPortBundleStatus(
			i4IfIndex,
			i4FsMplsPortBundleStatus);

	return i1RetVal;


} /* NcFsMplsPortBundleStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsPortBundleStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsPortBundleStatusTest (UINT4 *pu4Error,
		INT4 i4IfIndex,
		INT4 i4FsMplsPortBundleStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsPortBundleStatus(pu4Error,
			i4IfIndex,
			i4FsMplsPortBundleStatus);

	return i1RetVal;


} /* NcFsMplsPortBundleStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsPortMultiplexStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsPortMultiplexStatusSet (
		INT4 i4IfIndex,
		INT4 i4FsMplsPortMultiplexStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsPortMultiplexStatus(
			i4IfIndex,
			i4FsMplsPortMultiplexStatus);

	return i1RetVal;


} /* NcFsMplsPortMultiplexStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsPortMultiplexStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsPortMultiplexStatusTest (UINT4 *pu4Error,
		INT4 i4IfIndex,
		INT4 i4FsMplsPortMultiplexStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsPortMultiplexStatus(pu4Error,
			i4IfIndex,
			i4FsMplsPortMultiplexStatus);

	return i1RetVal;


} /* NcFsMplsPortMultiplexStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsPortAllToOneBundleStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsPortAllToOneBundleStatusSet (
		INT4 i4IfIndex,
		INT4 i4FsMplsPortAllToOneBundleStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsPortAllToOneBundleStatus(
			i4IfIndex,
			i4FsMplsPortAllToOneBundleStatus);

	return i1RetVal;


} /* NcFsMplsPortAllToOneBundleStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsPortAllToOneBundleStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsPortAllToOneBundleStatusTest (UINT4 *pu4Error,
		INT4 i4IfIndex,
		INT4 i4FsMplsPortAllToOneBundleStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsPortAllToOneBundleStatus(pu4Error,
			i4IfIndex,
			i4FsMplsPortAllToOneBundleStatus);

	return i1RetVal;


} /* NcFsMplsPortAllToOneBundleStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsPortRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsPortRowStatusSet (
		INT4 i4IfIndex,
		INT4 i4FsMplsPortRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsPortRowStatus(
			i4IfIndex,
			i4FsMplsPortRowStatus);

	return i1RetVal;


} /* NcFsMplsPortRowStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsPortRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsPortRowStatusTest (UINT4 *pu4Error,
		INT4 i4IfIndex,
		INT4 i4FsMplsPortRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsPortRowStatus(pu4Error,
			i4IfIndex,
			i4FsMplsPortRowStatus);

	return i1RetVal;


} /* NcFsMplsPortRowStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsAcMapPortIfIndexSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsAcMapPortIfIndexSet (
		UINT4 u4FsMplsVplsAcMapVplsIndex,
		UINT4 u4FsMplsVplsAcMapAcIndex,
		UINT4 u4FsMplsVplsAcMapPortIfIndex )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsVplsAcMapPortIfIndex(
			u4FsMplsVplsAcMapVplsIndex,
			u4FsMplsVplsAcMapAcIndex,
			u4FsMplsVplsAcMapPortIfIndex);

	return i1RetVal;


} /* NcFsMplsVplsAcMapPortIfIndexSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsAcMapPortIfIndexTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsAcMapPortIfIndexTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsAcMapVplsIndex,
		UINT4 u4FsMplsVplsAcMapAcIndex,
		UINT4 u4FsMplsVplsAcMapPortIfIndex )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsVplsAcMapPortIfIndex(pu4Error,
			u4FsMplsVplsAcMapVplsIndex,
			u4FsMplsVplsAcMapAcIndex,
			u4FsMplsVplsAcMapPortIfIndex);

	return i1RetVal;


} /* NcFsMplsVplsAcMapPortIfIndexTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsAcMapPortVlanSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsAcMapPortVlanSet (
		UINT4 u4FsMplsVplsAcMapVplsIndex,
		UINT4 u4FsMplsVplsAcMapAcIndex,
		UINT4 u4FsMplsVplsAcMapPortVlan )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsVplsAcMapPortVlan(
			u4FsMplsVplsAcMapVplsIndex,
			u4FsMplsVplsAcMapAcIndex,
			u4FsMplsVplsAcMapPortVlan);

	return i1RetVal;


} /* NcFsMplsVplsAcMapPortVlanSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsAcMapPortVlanTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsAcMapPortVlanTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsAcMapVplsIndex,
		UINT4 u4FsMplsVplsAcMapAcIndex,
		UINT4 u4FsMplsVplsAcMapPortVlan )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsVplsAcMapPortVlan(pu4Error,
			u4FsMplsVplsAcMapVplsIndex,
			u4FsMplsVplsAcMapAcIndex,
			u4FsMplsVplsAcMapPortVlan);

	return i1RetVal;


} /* NcFsMplsVplsAcMapPortVlanTest */

/********************************************************************
 * FUNCTION NcFsMplsVplsAcMapRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsAcMapRowStatusSet (
		UINT4 u4FsMplsVplsAcMapVplsIndex,
		UINT4 u4FsMplsVplsAcMapAcIndex,
		INT4 i4FsMplsVplsAcMapRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsVplsAcMapRowStatus(
			u4FsMplsVplsAcMapVplsIndex,
			u4FsMplsVplsAcMapAcIndex,
			i4FsMplsVplsAcMapRowStatus);

	return i1RetVal;


} /* NcFsMplsVplsAcMapRowStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsVplsAcMapRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsVplsAcMapRowStatusTest (UINT4 *pu4Error,
		UINT4 u4FsMplsVplsAcMapVplsIndex,
		UINT4 u4FsMplsVplsAcMapAcIndex,
		INT4 i4FsMplsVplsAcMapRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsVplsAcMapRowStatus(pu4Error,
			u4FsMplsVplsAcMapVplsIndex,
			u4FsMplsVplsAcMapAcIndex,
			i4FsMplsVplsAcMapRowStatus);

	return i1RetVal;


} /* NcFsMplsVplsAcMapRowStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsTunnelCRLDPResPeakDataRateSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTunnelCRLDPResPeakDataRateSet (
		UINT4 u4MplsTunnelResourceIndex,
		UINT4 u4FsMplsTunnelCRLDPResPeakDataRate )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsTunnelCRLDPResPeakDataRate(
			u4MplsTunnelResourceIndex,
			u4FsMplsTunnelCRLDPResPeakDataRate);

	return i1RetVal;


} /* NcFsMplsTunnelCRLDPResPeakDataRateSet */

/********************************************************************
 * FUNCTION NcFsMplsTunnelCRLDPResPeakDataRateTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTunnelCRLDPResPeakDataRateTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelResourceIndex,
		UINT4 u4FsMplsTunnelCRLDPResPeakDataRate )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsTunnelCRLDPResPeakDataRate(pu4Error,
			u4MplsTunnelResourceIndex,
			u4FsMplsTunnelCRLDPResPeakDataRate);

	return i1RetVal;


} /* NcFsMplsTunnelCRLDPResPeakDataRateTest */

/********************************************************************
 * FUNCTION NcFsMplsTunnelCRLDPResCommittedDataRateSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTunnelCRLDPResCommittedDataRateSet (
		UINT4 u4MplsTunnelResourceIndex,
		UINT4 u4FsMplsTunnelCRLDPResCommittedDataRate )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsTunnelCRLDPResCommittedDataRate(
			u4MplsTunnelResourceIndex,
			u4FsMplsTunnelCRLDPResCommittedDataRate);

	return i1RetVal;


} /* NcFsMplsTunnelCRLDPResCommittedDataRateSet */

/********************************************************************
 * FUNCTION NcFsMplsTunnelCRLDPResCommittedDataRateTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTunnelCRLDPResCommittedDataRateTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelResourceIndex,
		UINT4 u4FsMplsTunnelCRLDPResCommittedDataRate )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsTunnelCRLDPResCommittedDataRate(pu4Error,
			u4MplsTunnelResourceIndex,
			u4FsMplsTunnelCRLDPResCommittedDataRate);

	return i1RetVal;


} /* NcFsMplsTunnelCRLDPResCommittedDataRateTest */

/********************************************************************
 * FUNCTION NcFsMplsTunnelCRLDPResPeakBurstSizeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTunnelCRLDPResPeakBurstSizeSet (
		UINT4 u4MplsTunnelResourceIndex,
		UINT4 u4FsMplsTunnelCRLDPResPeakBurstSize )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsTunnelCRLDPResPeakBurstSize(
			u4MplsTunnelResourceIndex,
			u4FsMplsTunnelCRLDPResPeakBurstSize);

	return i1RetVal;


} /* NcFsMplsTunnelCRLDPResPeakBurstSizeSet */

/********************************************************************
 * FUNCTION NcFsMplsTunnelCRLDPResPeakBurstSizeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTunnelCRLDPResPeakBurstSizeTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelResourceIndex,
		UINT4 u4FsMplsTunnelCRLDPResPeakBurstSize )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsTunnelCRLDPResPeakBurstSize(pu4Error,
			u4MplsTunnelResourceIndex,
			u4FsMplsTunnelCRLDPResPeakBurstSize);

	return i1RetVal;


} /* NcFsMplsTunnelCRLDPResPeakBurstSizeTest */

/********************************************************************
 * FUNCTION NcFsMplsTunnelCRLDPResCommittedBurstSizeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTunnelCRLDPResCommittedBurstSizeSet (
		UINT4 u4MplsTunnelResourceIndex,
		UINT4 u4FsMplsTunnelCRLDPResCommittedBurstSize )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsTunnelCRLDPResCommittedBurstSize(
			u4MplsTunnelResourceIndex,
			u4FsMplsTunnelCRLDPResCommittedBurstSize);

	return i1RetVal;


} /* NcFsMplsTunnelCRLDPResCommittedBurstSizeSet */

/********************************************************************
 * FUNCTION NcFsMplsTunnelCRLDPResCommittedBurstSizeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTunnelCRLDPResCommittedBurstSizeTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelResourceIndex,
		UINT4 u4FsMplsTunnelCRLDPResCommittedBurstSize )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsTunnelCRLDPResCommittedBurstSize(pu4Error,
			u4MplsTunnelResourceIndex,
			u4FsMplsTunnelCRLDPResCommittedBurstSize);

	return i1RetVal;


} /* NcFsMplsTunnelCRLDPResCommittedBurstSizeTest */

/********************************************************************
 * FUNCTION NcFsMplsTunnelCRLDPResExcessBurstSizeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTunnelCRLDPResExcessBurstSizeSet (
		UINT4 u4MplsTunnelResourceIndex,
		UINT4 u4FsMplsTunnelCRLDPResExcessBurstSize )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsTunnelCRLDPResExcessBurstSize(
			u4MplsTunnelResourceIndex,
			u4FsMplsTunnelCRLDPResExcessBurstSize);

	return i1RetVal;


} /* NcFsMplsTunnelCRLDPResExcessBurstSizeSet */

/********************************************************************
 * FUNCTION NcFsMplsTunnelCRLDPResExcessBurstSizeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsTunnelCRLDPResExcessBurstSizeTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelResourceIndex,
		UINT4 u4FsMplsTunnelCRLDPResExcessBurstSize )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsTunnelCRLDPResExcessBurstSize(pu4Error,
			u4MplsTunnelResourceIndex,
			u4FsMplsTunnelCRLDPResExcessBurstSize);

	return i1RetVal;


} /* NcFsMplsTunnelCRLDPResExcessBurstSizeTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspMapPhbDscpSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspMapPhbDscpSet (
		INT4 i4FsMplsDiffServElspMapIndex,
		INT4 i4FsMplsDiffServElspMapExpIndex,
		INT4 i4FsMplsDiffServElspMapPhbDscp )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServElspMapPhbDscp(
			i4FsMplsDiffServElspMapIndex,
			i4FsMplsDiffServElspMapExpIndex,
			i4FsMplsDiffServElspMapPhbDscp);

	return i1RetVal;


} /* NcFsMplsDiffServElspMapPhbDscpSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspMapPhbDscpTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspMapPhbDscpTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServElspMapIndex,
		INT4 i4FsMplsDiffServElspMapExpIndex,
		INT4 i4FsMplsDiffServElspMapPhbDscp )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServElspMapPhbDscp(pu4Error,
			i4FsMplsDiffServElspMapIndex,
			i4FsMplsDiffServElspMapExpIndex,
			i4FsMplsDiffServElspMapPhbDscp);

	return i1RetVal;


} /* NcFsMplsDiffServElspMapPhbDscpTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspMapStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspMapStatusSet (
		INT4 i4FsMplsDiffServElspMapIndex,
		INT4 i4FsMplsDiffServElspMapExpIndex,
		INT4 i4FsMplsDiffServElspMapStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServElspMapStatus(
			i4FsMplsDiffServElspMapIndex,
			i4FsMplsDiffServElspMapExpIndex,
			i4FsMplsDiffServElspMapStatus);

	return i1RetVal;


} /* NcFsMplsDiffServElspMapStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspMapStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspMapStatusTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServElspMapIndex,
		INT4 i4FsMplsDiffServElspMapExpIndex,
		INT4 i4FsMplsDiffServElspMapStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServElspMapStatus(pu4Error,
			i4FsMplsDiffServElspMapIndex,
			i4FsMplsDiffServElspMapExpIndex,
			i4FsMplsDiffServElspMapStatus);

	return i1RetVal;


} /* NcFsMplsDiffServElspMapStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsServiceTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsServiceTypeSet (
		INT4 i4FsMplsDiffServParamsIndex,
		INT4 i4FsMplsDiffServParamsServiceType )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServParamsServiceType(
			i4FsMplsDiffServParamsIndex,
			i4FsMplsDiffServParamsServiceType);

	return i1RetVal;


} /* NcFsMplsDiffServParamsServiceTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsServiceTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsServiceTypeTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServParamsIndex,
		INT4 i4FsMplsDiffServParamsServiceType )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServParamsServiceType(pu4Error,
			i4FsMplsDiffServParamsIndex,
			i4FsMplsDiffServParamsServiceType);

	return i1RetVal;


} /* NcFsMplsDiffServParamsServiceTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsLlspPscDscpSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsLlspPscDscpSet (
		INT4 i4FsMplsDiffServParamsIndex,
		INT4 i4FsMplsDiffServParamsLlspPscDscp )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServParamsLlspPscDscp(
			i4FsMplsDiffServParamsIndex,
			i4FsMplsDiffServParamsLlspPscDscp);

	return i1RetVal;


} /* NcFsMplsDiffServParamsLlspPscDscpSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsLlspPscDscpTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsLlspPscDscpTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServParamsIndex,
		INT4 i4FsMplsDiffServParamsLlspPscDscp )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServParamsLlspPscDscp(pu4Error,
			i4FsMplsDiffServParamsIndex,
			i4FsMplsDiffServParamsLlspPscDscp);

	return i1RetVal;


} /* NcFsMplsDiffServParamsLlspPscDscpTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsElspTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsElspTypeSet (
		INT4 i4FsMplsDiffServParamsIndex,
		INT4 i4FsMplsDiffServParamsElspType )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServParamsElspType(
			i4FsMplsDiffServParamsIndex,
			i4FsMplsDiffServParamsElspType);

	return i1RetVal;


} /* NcFsMplsDiffServParamsElspTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsElspTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsElspTypeTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServParamsIndex,
		INT4 i4FsMplsDiffServParamsElspType )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServParamsElspType(pu4Error,
			i4FsMplsDiffServParamsIndex,
			i4FsMplsDiffServParamsElspType);

	return i1RetVal;


} /* NcFsMplsDiffServParamsElspTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsElspSigExpPhbMapIndexSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsElspSigExpPhbMapIndexSet (
		INT4 i4FsMplsDiffServParamsIndex,
		INT4 i4FsMplsDiffServParamsElspSigExpPhbMapIndex )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServParamsElspSigExpPhbMapIndex(
			i4FsMplsDiffServParamsIndex,
			i4FsMplsDiffServParamsElspSigExpPhbMapIndex);

	return i1RetVal;


} /* NcFsMplsDiffServParamsElspSigExpPhbMapIndexSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsElspSigExpPhbMapIndexTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsElspSigExpPhbMapIndexTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServParamsIndex,
		INT4 i4FsMplsDiffServParamsElspSigExpPhbMapIndex )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServParamsElspSigExpPhbMapIndex(pu4Error,
			i4FsMplsDiffServParamsIndex,
			i4FsMplsDiffServParamsElspSigExpPhbMapIndex);

	return i1RetVal;


} /* NcFsMplsDiffServParamsElspSigExpPhbMapIndexTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsStatusSet (
		INT4 i4FsMplsDiffServParamsIndex,
		INT4 i4FsMplsDiffServParamsStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServParamsStatus(
			i4FsMplsDiffServParamsIndex,
			i4FsMplsDiffServParamsStatus);

	return i1RetVal;


} /* NcFsMplsDiffServParamsStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServParamsStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServParamsStatusTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServParamsIndex,
		INT4 i4FsMplsDiffServParamsStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServParamsStatus(pu4Error,
			i4FsMplsDiffServParamsIndex,
			i4FsMplsDiffServParamsStatus);

	return i1RetVal;


} /* NcFsMplsDiffServParamsStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServClassTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServClassTypeSet (
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServClassType )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServClassType(
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServClassType);

	return i1RetVal;


} /* NcFsMplsDiffServClassTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServClassTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServClassTypeTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServClassType )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServClassType(pu4Error,
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServClassType);

	return i1RetVal;


} /* NcFsMplsDiffServClassTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServServiceTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServServiceTypeSet (
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServServiceType )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServServiceType(
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServServiceType);

	return i1RetVal;


} /* NcFsMplsDiffServServiceTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServServiceTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServServiceTypeTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServServiceType )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServServiceType(pu4Error,
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServServiceType);

	return i1RetVal;


} /* NcFsMplsDiffServServiceTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServLlspPscSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServLlspPscSet (
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServLlspPsc )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServLlspPsc(
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServLlspPsc);

	return i1RetVal;


} /* NcFsMplsDiffServLlspPscSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServLlspPscTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServLlspPscTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServLlspPsc )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServLlspPsc(pu4Error,
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServLlspPsc);

	return i1RetVal;


} /* NcFsMplsDiffServLlspPscTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspTypeSet (
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServElspType )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServElspType(
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServElspType);

	return i1RetVal;


} /* NcFsMplsDiffServElspTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspTypeTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServElspType )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServElspType(pu4Error,
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServElspType);

	return i1RetVal;


} /* NcFsMplsDiffServElspTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspListIndexSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspListIndexSet (
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServElspListIndex )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServElspListIndex(
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServElspListIndex);

	return i1RetVal;


} /* NcFsMplsDiffServElspListIndexSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspListIndexTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspListIndexTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServElspListIndex )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServElspListIndex(pu4Error,
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServElspListIndex);

	return i1RetVal;


} /* NcFsMplsDiffServElspListIndexTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServRowStatusSet (
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServRowStatus(
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServRowStatus);

	return i1RetVal;


} /* NcFsMplsDiffServRowStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServRowStatusTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServRowStatus(pu4Error,
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServRowStatus);

	return i1RetVal;


} /* NcFsMplsDiffServRowStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServStorageTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServStorageTypeSet (
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServStorageType )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServStorageType(
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServStorageType);

	return i1RetVal;


} /* NcFsMplsDiffServStorageTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServStorageTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServStorageTypeTest (UINT4 *pu4Error,
		UINT4 u4MplsTunnelIndex,
		UINT4 u4MplsTunnelInstance,
		UINT4 u4MplsTunnelIngressLSRId,
		UINT4 u4MplsTunnelEgressLSRId,
		INT4 i4FsMplsDiffServStorageType )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServStorageType(pu4Error,
			u4MplsTunnelIndex,
			u4MplsTunnelInstance,
			u4MplsTunnelIngressLSRId,
			u4MplsTunnelEgressLSRId,
			i4FsMplsDiffServStorageType);

	return i1RetVal;


} /* NcFsMplsDiffServStorageTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspInfoPHBSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspInfoPHBSet (
		INT4 i4FsMplsDiffServElspInfoListIndex,
		INT4 i4FsMplsDiffServElspInfoIndex,
		INT4 i4FsMplsDiffServElspInfoPHB )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServElspInfoPHB(
			i4FsMplsDiffServElspInfoListIndex,
			i4FsMplsDiffServElspInfoIndex,
			i4FsMplsDiffServElspInfoPHB);

	return i1RetVal;


} /* NcFsMplsDiffServElspInfoPHBSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspInfoPHBTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspInfoPHBTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServElspInfoListIndex,
		INT4 i4FsMplsDiffServElspInfoIndex,
		INT4 i4FsMplsDiffServElspInfoPHB )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServElspInfoPHB(pu4Error,
			i4FsMplsDiffServElspInfoListIndex,
			i4FsMplsDiffServElspInfoIndex,
			i4FsMplsDiffServElspInfoPHB);

	return i1RetVal;


} /* NcFsMplsDiffServElspInfoPHBTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspInfoResourcePointerSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspInfoResourcePointerSet (
		INT4 i4FsMplsDiffServElspInfoListIndex,
		INT4 i4FsMplsDiffServElspInfoIndex,
		UINT1 *pFsMplsDiffServElspInfoResourcePointer )
{

	INT1 i1RetVal;
	tSNMP_OID_TYPE FsMplsDiffServElspInfoResourcePointer;
	MEMSET (&FsMplsDiffServElspInfoResourcePointer, 0,  sizeof (FsMplsDiffServElspInfoResourcePointer));

	FsMplsDiffServElspInfoResourcePointer.u4_Length = (UINT4) STRLEN (pFsMplsDiffServElspInfoResourcePointer);
	FsMplsDiffServElspInfoResourcePointer.pu4_OidList = (UINT4 *)pFsMplsDiffServElspInfoResourcePointer;

	i1RetVal = nmhSetFsMplsDiffServElspInfoResourcePointer(
			i4FsMplsDiffServElspInfoListIndex,
			i4FsMplsDiffServElspInfoIndex,
			&FsMplsDiffServElspInfoResourcePointer);

	return i1RetVal;


} /* NcFsMplsDiffServElspInfoResourcePointerSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspInfoResourcePointerTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspInfoResourcePointerTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServElspInfoListIndex,
		INT4 i4FsMplsDiffServElspInfoIndex,
		UINT1 *pFsMplsDiffServElspInfoResourcePointer )
{

	INT1 i1RetVal;
	tSNMP_OID_TYPE FsMplsDiffServElspInfoResourcePointer;
	MEMSET (&FsMplsDiffServElspInfoResourcePointer, 0,  sizeof (FsMplsDiffServElspInfoResourcePointer));

	FsMplsDiffServElspInfoResourcePointer.u4_Length = (UINT4) STRLEN (pFsMplsDiffServElspInfoResourcePointer);
	FsMplsDiffServElspInfoResourcePointer.pu4_OidList = (UINT4 *)pFsMplsDiffServElspInfoResourcePointer;

	i1RetVal = nmhTestv2FsMplsDiffServElspInfoResourcePointer(pu4Error,
			i4FsMplsDiffServElspInfoListIndex,
			i4FsMplsDiffServElspInfoIndex,
			&FsMplsDiffServElspInfoResourcePointer);

	return i1RetVal;


} /* NcFsMplsDiffServElspInfoResourcePointerTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspInfoRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspInfoRowStatusSet (
		INT4 i4FsMplsDiffServElspInfoListIndex,
		INT4 i4FsMplsDiffServElspInfoIndex,
		INT4 i4FsMplsDiffServElspInfoRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServElspInfoRowStatus(
			i4FsMplsDiffServElspInfoListIndex,
			i4FsMplsDiffServElspInfoIndex,
			i4FsMplsDiffServElspInfoRowStatus);

	return i1RetVal;


} /* NcFsMplsDiffServElspInfoRowStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspInfoRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspInfoRowStatusTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServElspInfoListIndex,
		INT4 i4FsMplsDiffServElspInfoIndex,
		INT4 i4FsMplsDiffServElspInfoRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServElspInfoRowStatus(pu4Error,
			i4FsMplsDiffServElspInfoListIndex,
			i4FsMplsDiffServElspInfoIndex,
			i4FsMplsDiffServElspInfoRowStatus);

	return i1RetVal;


} /* NcFsMplsDiffServElspInfoRowStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspInfoStorageTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspInfoStorageTypeSet (
		INT4 i4FsMplsDiffServElspInfoListIndex,
		INT4 i4FsMplsDiffServElspInfoIndex,
		INT4 i4FsMplsDiffServElspInfoStorageType )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDiffServElspInfoStorageType(
			i4FsMplsDiffServElspInfoListIndex,
			i4FsMplsDiffServElspInfoIndex,
			i4FsMplsDiffServElspInfoStorageType);

	return i1RetVal;


} /* NcFsMplsDiffServElspInfoStorageTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsDiffServElspInfoStorageTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDiffServElspInfoStorageTypeTest (UINT4 *pu4Error,
		INT4 i4FsMplsDiffServElspInfoListIndex,
		INT4 i4FsMplsDiffServElspInfoIndex,
		INT4 i4FsMplsDiffServElspInfoStorageType )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDiffServElspInfoStorageType(pu4Error,
			i4FsMplsDiffServElspInfoListIndex,
			i4FsMplsDiffServElspInfoIndex,
			i4FsMplsDiffServElspInfoStorageType);

	return i1RetVal;


} /* NcFsMplsDiffServElspInfoStorageTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsDsTeClassTypeDescriptionSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeClassTypeDescriptionSet (
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT1 *pFsMplsDsTeClassTypeDescription )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsDsTeClassTypeDescription;
	MEMSET (&FsMplsDsTeClassTypeDescription, 0,  sizeof (FsMplsDsTeClassTypeDescription));

	FsMplsDsTeClassTypeDescription.i4_Length = (INT4) STRLEN (pFsMplsDsTeClassTypeDescription);
	FsMplsDsTeClassTypeDescription.pu1_OctetList = pFsMplsDsTeClassTypeDescription;

	i1RetVal = nmhSetFsMplsDsTeClassTypeDescription(
			u4FsMplsDsTeClassTypeIndex,
			&FsMplsDsTeClassTypeDescription);

	return i1RetVal;


} /* NcFsMplsDsTeClassTypeDescriptionSet */

/********************************************************************
 * FUNCTION NcFsMplsDsTeClassTypeDescriptionTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeClassTypeDescriptionTest (UINT4 *pu4Error,
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT1 *pFsMplsDsTeClassTypeDescription )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsDsTeClassTypeDescription;
	MEMSET (&FsMplsDsTeClassTypeDescription, 0,  sizeof (FsMplsDsTeClassTypeDescription));

	FsMplsDsTeClassTypeDescription.i4_Length = (INT4) STRLEN (pFsMplsDsTeClassTypeDescription);
	FsMplsDsTeClassTypeDescription.pu1_OctetList = pFsMplsDsTeClassTypeDescription;

	i1RetVal = nmhTestv2FsMplsDsTeClassTypeDescription(pu4Error,
			u4FsMplsDsTeClassTypeIndex,
			&FsMplsDsTeClassTypeDescription);

	return i1RetVal;


} /* NcFsMplsDsTeClassTypeDescriptionTest */

/********************************************************************
 * FUNCTION NcFsMplsDsTeClassTypeRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeClassTypeRowStatusSet (
		UINT4 u4FsMplsDsTeClassTypeIndex,
		INT4 i4FsMplsDsTeClassTypeRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDsTeClassTypeRowStatus(
			u4FsMplsDsTeClassTypeIndex,
			i4FsMplsDsTeClassTypeRowStatus);

	return i1RetVal;


} /* NcFsMplsDsTeClassTypeRowStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsDsTeClassTypeRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeClassTypeRowStatusTest (UINT4 *pu4Error,
		UINT4 u4FsMplsDsTeClassTypeIndex,
		INT4 i4FsMplsDsTeClassTypeRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDsTeClassTypeRowStatus(pu4Error,
			u4FsMplsDsTeClassTypeIndex,
			i4FsMplsDsTeClassTypeRowStatus);

	return i1RetVal;


} /* NcFsMplsDsTeClassTypeRowStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsDsTeClassTypeBwPercentageSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeClassTypeBwPercentageSet (
		UINT4 u4FsMplsDsTeClassTypeIndex,
		INT4 i4FsMplsDsTeClassTypeBwPercentage )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDsTeClassTypeBwPercentage(
			u4FsMplsDsTeClassTypeIndex,
			i4FsMplsDsTeClassTypeBwPercentage);

	return i1RetVal;


} /* NcFsMplsDsTeClassTypeBwPercentageSet */

/********************************************************************
 * FUNCTION NcFsMplsDsTeClassTypeBwPercentageTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeClassTypeBwPercentageTest (UINT4 *pu4Error,
		UINT4 u4FsMplsDsTeClassTypeIndex,
		INT4 i4FsMplsDsTeClassTypeBwPercentage )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDsTeClassTypeBwPercentage(pu4Error,
			u4FsMplsDsTeClassTypeIndex,
			i4FsMplsDsTeClassTypeBwPercentage);

	return i1RetVal;


} /* NcFsMplsDsTeClassTypeBwPercentageTest */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTcTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTcTypeSet (
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTcIndex,
		INT4 i4FsMplsDsTeTcType )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDsTeTcType(
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTcIndex,
			i4FsMplsDsTeTcType);

	return i1RetVal;


} /* NcFsMplsDsTeTcTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTcTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTcTypeTest (UINT4 *pu4Error,
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTcIndex,
		INT4 i4FsMplsDsTeTcType )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDsTeTcType(pu4Error,
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTcIndex,
			i4FsMplsDsTeTcType);

	return i1RetVal;


} /* NcFsMplsDsTeTcTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTcDescriptionSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTcDescriptionSet (
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTcIndex,
		UINT1 *pFsMplsDsTeTcDescription )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsDsTeTcDescription;
	MEMSET (&FsMplsDsTeTcDescription, 0,  sizeof (FsMplsDsTeTcDescription));

	FsMplsDsTeTcDescription.i4_Length = (INT4) STRLEN (pFsMplsDsTeTcDescription);
	FsMplsDsTeTcDescription.pu1_OctetList = pFsMplsDsTeTcDescription;

	i1RetVal = nmhSetFsMplsDsTeTcDescription(
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTcIndex,
			&FsMplsDsTeTcDescription);

	return i1RetVal;


} /* NcFsMplsDsTeTcDescriptionSet */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTcDescriptionTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTcDescriptionTest (UINT4 *pu4Error,
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTcIndex,
		UINT1 *pFsMplsDsTeTcDescription )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsDsTeTcDescription;
	MEMSET (&FsMplsDsTeTcDescription, 0,  sizeof (FsMplsDsTeTcDescription));

	FsMplsDsTeTcDescription.i4_Length = (INT4) STRLEN (pFsMplsDsTeTcDescription);
	FsMplsDsTeTcDescription.pu1_OctetList = pFsMplsDsTeTcDescription;

	i1RetVal = nmhTestv2FsMplsDsTeTcDescription(pu4Error,
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTcIndex,
			&FsMplsDsTeTcDescription);

	return i1RetVal;


} /* NcFsMplsDsTeTcDescriptionTest */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTcMapRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTcMapRowStatusSet (
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTcIndex,
		INT4 i4FsMplsDsTeTcMapRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDsTeTcMapRowStatus(
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTcIndex,
			i4FsMplsDsTeTcMapRowStatus);

	return i1RetVal;


} /* NcFsMplsDsTeTcMapRowStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTcMapRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTcMapRowStatusTest (UINT4 *pu4Error,
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTcIndex,
		INT4 i4FsMplsDsTeTcMapRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDsTeTcMapRowStatus(pu4Error,
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTcIndex,
			i4FsMplsDsTeTcMapRowStatus);

	return i1RetVal;


} /* NcFsMplsDsTeTcMapRowStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTeClassDescSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTeClassDescSet (
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTeClassPriority,
		UINT1 *pFsMplsDsTeTeClassDesc )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsDsTeTeClassDesc;
	MEMSET (&FsMplsDsTeTeClassDesc, 0,  sizeof (FsMplsDsTeTeClassDesc));

	FsMplsDsTeTeClassDesc.i4_Length = (INT4) STRLEN (pFsMplsDsTeTeClassDesc);
	FsMplsDsTeTeClassDesc.pu1_OctetList = pFsMplsDsTeTeClassDesc;

	i1RetVal = nmhSetFsMplsDsTeTeClassDesc(
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTeClassPriority,
			&FsMplsDsTeTeClassDesc);

	return i1RetVal;


} /* NcFsMplsDsTeTeClassDescSet */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTeClassDescTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTeClassDescTest (UINT4 *pu4Error,
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTeClassPriority,
		UINT1 *pFsMplsDsTeTeClassDesc )
{

	INT1 i1RetVal;
	tSNMP_OCTET_STRING_TYPE FsMplsDsTeTeClassDesc;
	MEMSET (&FsMplsDsTeTeClassDesc, 0,  sizeof (FsMplsDsTeTeClassDesc));

	FsMplsDsTeTeClassDesc.i4_Length = (INT4) STRLEN (pFsMplsDsTeTeClassDesc);
	FsMplsDsTeTeClassDesc.pu1_OctetList = pFsMplsDsTeTeClassDesc;

	i1RetVal = nmhTestv2FsMplsDsTeTeClassDesc(pu4Error,
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTeClassPriority,
			&FsMplsDsTeTeClassDesc);

	return i1RetVal;


} /* NcFsMplsDsTeTeClassDescTest */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTeClassNumberSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTeClassNumberSet (
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTeClassPriority,
		UINT4 u4FsMplsDsTeTeClassNumber )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDsTeTeClassNumber(
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTeClassPriority,
			u4FsMplsDsTeTeClassNumber);

	return i1RetVal;


} /* NcFsMplsDsTeTeClassNumberSet */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTeClassNumberTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTeClassNumberTest (UINT4 *pu4Error,
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTeClassPriority,
		UINT4 u4FsMplsDsTeTeClassNumber )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDsTeTeClassNumber(pu4Error,
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTeClassPriority,
			u4FsMplsDsTeTeClassNumber);

	return i1RetVal;


} /* NcFsMplsDsTeTeClassNumberTest */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTeClassRowStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTeClassRowStatusSet (
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTeClassPriority,
		INT4 i4FsMplsDsTeTeClassRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhSetFsMplsDsTeTeClassRowStatus(
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTeClassPriority,
			i4FsMplsDsTeTeClassRowStatus);

	return i1RetVal;


} /* NcFsMplsDsTeTeClassRowStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsDsTeTeClassRowStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsDsTeTeClassRowStatusTest (UINT4 *pu4Error,
		UINT4 u4FsMplsDsTeClassTypeIndex,
		UINT4 u4FsMplsDsTeTeClassPriority,
		INT4 i4FsMplsDsTeTeClassRowStatus )
{

	INT1 i1RetVal;

	i1RetVal = nmhTestv2FsMplsDsTeTeClassRowStatus(pu4Error,
			u4FsMplsDsTeClassTypeIndex,
			u4FsMplsDsTeTeClassPriority,
			i4FsMplsDsTeTeClassRowStatus);

	return i1RetVal;


} /* NcFsMplsDsTeTeClassRowStatusTest */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfNameSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfNameSet (
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		UINT1 *pau1FsMplsL3VpnVrfName )
{

	INT1 i1RetVal = SNMP_SUCCESS;
#ifdef MPLS_L3VPN_WANTED
	tSNMP_OCTET_STRING_TYPE FsMplsL3VpnVrfName;
	MEMSET (&FsMplsL3VpnVrfName, 0,  sizeof (FsMplsL3VpnVrfName));

	FsMplsL3VpnVrfName.i4_Length = (INT4) STRLEN (pau1FsMplsL3VpnVrfName);
	FsMplsL3VpnVrfName.pu1_OctetList = pau1FsMplsL3VpnVrfName;

	i1RetVal = nmhSetFsMplsL3VpnVrfName(
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			&FsMplsL3VpnVrfName);
#else
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(pau1FsMplsL3VpnVrfName);

#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfNameSet */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfNameTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfNameTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		UINT1 *pau1FsMplsL3VpnVrfName )
{

	INT1 i1RetVal = SNMP_SUCCESS;
#ifdef MPLS_L3VPN_WANTED
	tSNMP_OCTET_STRING_TYPE FsMplsL3VpnVrfName;
	MEMSET (&FsMplsL3VpnVrfName, 0,  sizeof (FsMplsL3VpnVrfName));

	FsMplsL3VpnVrfName.i4_Length = (INT4) STRLEN (pau1FsMplsL3VpnVrfName);
	FsMplsL3VpnVrfName.pu1_OctetList = pau1FsMplsL3VpnVrfName;

	i1RetVal = nmhTestv2FsMplsL3VpnVrfName(pu4Error,
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			&FsMplsL3VpnVrfName);
#else
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(pau1FsMplsL3VpnVrfName);
#endif
	return i1RetVal;


} /* NcFsMplsL3VpnVrfNameTest */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrDestTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrDestTypeSet (
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrDestType )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhSetFsMplsL3VpnVrfEgressRteInetCidrDestType(
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrDestType);
#else
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrDestType);
#endif
	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrDestTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrDestTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrDestTypeTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrDestType )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrDestType(pu4Error,
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrDestType);
#else
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrDestType);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrDestTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrDestSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrDestSet (
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		UINT1 *pau1FsMplsL3VpnVrfEgressRteInetCidrDest )
{

	INT1 i1RetVal = SNMP_SUCCESS;
#ifdef MPLS_L3VPN_WANTED
	tSNMP_OCTET_STRING_TYPE FsMplsL3VpnVrfEgressRteInetCidrDest;
	MEMSET (&FsMplsL3VpnVrfEgressRteInetCidrDest, 0,  sizeof (FsMplsL3VpnVrfEgressRteInetCidrDest));

	FsMplsL3VpnVrfEgressRteInetCidrDest.i4_Length = (INT4) STRLEN (pau1FsMplsL3VpnVrfEgressRteInetCidrDest);
	FsMplsL3VpnVrfEgressRteInetCidrDest.pu1_OctetList = pau1FsMplsL3VpnVrfEgressRteInetCidrDest;

	i1RetVal = nmhSetFsMplsL3VpnVrfEgressRteInetCidrDest(
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			&FsMplsL3VpnVrfEgressRteInetCidrDest);
#else
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(pau1FsMplsL3VpnVrfEgressRteInetCidrDest);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrDestSet */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrDestTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrDestTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		UINT1 *pau1FsMplsL3VpnVrfEgressRteInetCidrDest )
{

	INT1 i1RetVal = SNMP_SUCCESS;
#ifdef MPLS_L3VPN_WANTED
	tSNMP_OCTET_STRING_TYPE FsMplsL3VpnVrfEgressRteInetCidrDest;
	MEMSET (&FsMplsL3VpnVrfEgressRteInetCidrDest, 0,  sizeof (FsMplsL3VpnVrfEgressRteInetCidrDest));

	FsMplsL3VpnVrfEgressRteInetCidrDest.i4_Length = (INT4) STRLEN (pau1FsMplsL3VpnVrfEgressRteInetCidrDest);
	FsMplsL3VpnVrfEgressRteInetCidrDest.pu1_OctetList = pau1FsMplsL3VpnVrfEgressRteInetCidrDest;

	i1RetVal = nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrDest(pu4Error,
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			&FsMplsL3VpnVrfEgressRteInetCidrDest);
#else
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(pau1FsMplsL3VpnVrfEgressRteInetCidrDest);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrDestTest */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrPfxLenSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrPfxLenSet (
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		UINT4 u4FsMplsL3VpnVrfEgressRteInetCidrPfxLen )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhSetFsMplsL3VpnVrfEgressRteInetCidrPfxLen(
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			u4FsMplsL3VpnVrfEgressRteInetCidrPfxLen);
#else
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRteInetCidrPfxLen);
#endif
	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrPfxLenSet */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrPfxLenTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrPfxLenTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		UINT4 u4FsMplsL3VpnVrfEgressRteInetCidrPfxLen )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrPfxLen(pu4Error,
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			u4FsMplsL3VpnVrfEgressRteInetCidrPfxLen);
#else
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRteInetCidrPfxLen);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrPfxLenTest */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrLabelPolicySet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrLabelPolicySet (
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhSetFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy(
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy);
#else
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy);
#endif
	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrLabelPolicySet */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrLabelPolicyTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrLabelPolicyTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy(pu4Error,
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy);
#else
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrLabelPolicyTest */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrNHopTypeSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrNHopTypeSet (
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrNHopType )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhSetFsMplsL3VpnVrfEgressRteInetCidrNHopType(
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrNHopType);
#else
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrNHopType);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrNHopTypeSet */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrNHopTypeTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrNHopTypeTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrNHopType )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrNHopType(pu4Error,
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrNHopType);
#else
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrNHopType);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrNHopTypeTest */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrNextHopSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrNextHopSet (
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		UINT1 *pau1FsMplsL3VpnVrfEgressRteInetCidrNextHop )
{

	INT1 i1RetVal = SNMP_SUCCESS;
#ifdef MPLS_L3VPN_WANTED
	tSNMP_OCTET_STRING_TYPE FsMplsL3VpnVrfEgressRteInetCidrNextHop;
	MEMSET (&FsMplsL3VpnVrfEgressRteInetCidrNextHop, 0,  sizeof (FsMplsL3VpnVrfEgressRteInetCidrNextHop));

	FsMplsL3VpnVrfEgressRteInetCidrNextHop.i4_Length = (INT4) STRLEN (pau1FsMplsL3VpnVrfEgressRteInetCidrNextHop);
	FsMplsL3VpnVrfEgressRteInetCidrNextHop.pu1_OctetList = pau1FsMplsL3VpnVrfEgressRteInetCidrNextHop;

	i1RetVal = nmhSetFsMplsL3VpnVrfEgressRteInetCidrNextHop(
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			&FsMplsL3VpnVrfEgressRteInetCidrNextHop);
#else
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(pau1FsMplsL3VpnVrfEgressRteInetCidrNextHop);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrNextHopSet */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrNextHopTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrNextHopTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		UINT1 *pau1FsMplsL3VpnVrfEgressRteInetCidrNextHop )
{

	INT1 i1RetVal = SNMP_SUCCESS;
#ifdef MPLS_L3VPN_WANTED
	tSNMP_OCTET_STRING_TYPE FsMplsL3VpnVrfEgressRteInetCidrNextHop;
	MEMSET (&FsMplsL3VpnVrfEgressRteInetCidrNextHop, 0,  sizeof (FsMplsL3VpnVrfEgressRteInetCidrNextHop));

	FsMplsL3VpnVrfEgressRteInetCidrNextHop.i4_Length = (INT4) STRLEN (pau1FsMplsL3VpnVrfEgressRteInetCidrNextHop);
	FsMplsL3VpnVrfEgressRteInetCidrNextHop.pu1_OctetList = pau1FsMplsL3VpnVrfEgressRteInetCidrNextHop;

	i1RetVal = nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrNextHop(pu4Error,
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			&FsMplsL3VpnVrfEgressRteInetCidrNextHop);
#else
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(pau1FsMplsL3VpnVrfEgressRteInetCidrNextHop);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrNextHopTest */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrIfIndexSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrIfIndexSet (
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrIfIndex )
{

	INT1 i1RetVal = SNMP_SUCCESS;
#ifdef MPLS_L3VPN_WANTED

	i1RetVal = nmhSetFsMplsL3VpnVrfEgressRteInetCidrIfIndex(
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrIfIndex);
#else
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrIfIndex);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrIfIndexSet */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrIfIndexTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrIfIndexTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrIfIndex )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrIfIndex(pu4Error,
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrIfIndex);
#else
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrIfIndex);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrIfIndexTest */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrMetric1Set
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrMetric1Set (
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrMetric1 )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhSetFsMplsL3VpnVrfEgressRteInetCidrMetric1(
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrMetric1);
#else
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrMetric1);
#endif
	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrMetric1Set */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrMetric1Test
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrMetric1Test (UINT4 *pu4Error,
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrMetric1 )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrMetric1(pu4Error,
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrMetric1);
#else
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrMetric1);
#endif
	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrMetric1Test */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrStatusSet
 * 
 * Wrapper for nmhSet API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrStatusSet (
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrStatus )
{

	INT1 i1RetVal = SNMP_SUCCESS;
#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus(
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrStatus);
#else
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrStatus);
#endif

	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrStatusSet */

/********************************************************************
 * FUNCTION NcFsMplsL3VpnVrfEgressRteInetCidrStatusTest
 * 
 * Wrapper for nmhTest API's
 * 
 * RETURNS:
 *     error status
 ********************************************************************/
INT1 NcFsMplsL3VpnVrfEgressRteInetCidrStatusTest (UINT4 *pu4Error,
		UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
		INT4 i4FsMplsL3VpnVrfEgressRteInetCidrStatus )
{

	INT1 i1RetVal = SNMP_SUCCESS;

#ifdef MPLS_L3VPN_WANTED
	i1RetVal = nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrStatus(pu4Error,
			u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
			i4FsMplsL3VpnVrfEgressRteInetCidrStatus);
#else
    UNUSED_PARAM(pu4Error);
    UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
    UNUSED_PARAM(i4FsMplsL3VpnVrfEgressRteInetCidrStatus);
#endif
	return i1RetVal;


} /* NcFsMplsL3VpnVrfEgressRteInetCidrStatusTest */

/* END i_Aricent_MPLS_MIB.c */
